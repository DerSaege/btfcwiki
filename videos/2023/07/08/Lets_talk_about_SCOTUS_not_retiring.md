---
title: Let's talk about SCOTUS not retiring....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VBleN1izoHo) |
| Published | 2023/07/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the connection between the Supreme Court and electoral politics.
- Urges to think beyond the next election to impact the Supreme Court's makeup.
- Notes the link between electoral politics and the Supreme Court's rulings targeting specific groups.
- Points out that personal considerations may sometimes override justices' decisions to stay in office.
- States that the rulings causing upset are a result of electoral politics.
- Attributes the existence of right-wing camp justices to Trump's time in office.
- Emphasizes the importance of considering these factors when deciding on future elections.

### Quotes

- "Those two things are directly linked. There's no way to separate it."
- "It's something you should remember."
- "The reason we have the rulings that have half the country incredibly upset is because of electoral politics."
- "That's why these rulings exist."
- "It's because Trump was in office and was able to select so many justices that are in the right-wing camp."

### Oneliner

Beau explains the connection between electoral politics and the Supreme Court's makeup, urging to think beyond the next election to impact rulings targeting specific groups, noting personal considerations may override justices' decisions, and attributing upset rulings to Trump's right-wing camp selections.

### Audience

Voters

### On-the-ground actions from transcript

- Keep informed about the Supreme Court's makeup and how electoral politics can influence it (implied).
- Be actively involved in elections and support candidates who prioritize diverse judicial selections (implied).

### Whats missing in summary

Importance of informed voting for long-term impact on Supreme Court decisions. 

### Tags

#SupremeCourt #ElectoralPolitics #JudicialSelection #Trump #Voting


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk a little bit more
about the Supreme Court and electoral politics.
I guess this is the third video
in this little arc of videos,
the first one being the one about optimism,
and then there was one about,
I think it's actually titled,
you know, the Supreme Court and electoralism.
Because after that second one,
There was a large number of people who were like,
but if you elect a Democratic president,
if the Democrats have to control,
well then, Thomas and Alito, they just won't retire.
In that first video,
said one of the important things was not looking
at the short term, not looking at the next election,
looking beyond that.
If that's the case,
If you believe that they won't retire under a Democratic president, which is probably
true, well, then you have to keep the Democratic party in power longer.
You have to continue beyond that next election.
That's why it's important to think beyond that next election.
a Democratic president until they age out of the position. But at the same time,
those who said that, I think most know this, but it is flat-out saying that
electoral politics absolutely impact the makeup of the Supreme Court because
what you're saying is that if a Republican was in office, well then they
would retire. And then who gets to pick them? The Republican administration, right?
You see what that gets you. You see what that gets you. You see what it gets your
friends, your loved ones, those people who are being actively targeted by these
rulings right? It's there is no denying the connection between electoral
politics and the Supreme Court legislating from the bench. The Supreme
Court actively targeting those people you care about. Those two things are
directly linked. There's no way to separate it. It doesn't matter how you
you view it, if you're saying they won't retire under a Democratic president,
electoral politics absolutely really determines the makeup of the Supreme
Court. Now, again, you are talking about people who are getting pretty long in
the tooth. I don't know that that's true in this case. I don't know that they
would stay is, you know, just to avoid being replaced by a more liberal justice.
They might, but sometimes personal considerations override that. At the end
of it, it doesn't matter how you feel about electoral politics from a
philosophical level, or anything like that.
The reason we have the rulings that have half the country incredibly upset is because of
electoral politics.
It's because Trump was in office and was able to select so many justices that are in the
right-wing camp.
That's why these rulings exist.
And there's no denying that.
That's how it works.
So it is something to keep in mind.
When you are weighing what you are going to do in the next election or the election after,
it's something you should remember.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}