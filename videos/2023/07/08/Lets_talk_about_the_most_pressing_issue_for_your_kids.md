---
title: Let's talk about the most pressing issue for your kids....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GGHhkjMu5lY) |
| Published | 2023/07/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The biggest issue facing our children and future records is climate change, not accurately portrayed in headlines.
- The average temperature is increasing, leading to extreme weather, water shortages, and crop failures.
- Candidates without a climate mitigation plan should not be considered for votes, as climate change is the most critical issue.
- Immediate action is necessary to address climate change for mitigation.
- Candidates who ignore or downplay climate change for political reasons cannot be trusted.
- Climate change is a global issue that requires urgent attention.
- Having a climate plan should be a primary consideration when choosing candidates to vote for.
- Inaction on climate change will lead to severe consequences for future generations.
- Climate change is the most significant threat that needs to be acknowledged and acted upon.
- The impact of climate change is not just temporary weather but a long-term global climate shift.

### Quotes

- "If the candidate does not have a climate mitigation plan, I don't know how you can reasonably even consider voting for them."
- "Climate change is the biggest issue we're facing. It's the one that we have to act on now."
- "Inaction on climate change will lead to severe consequences for future generations."
- "The impact of climate change is not just temporary weather but a long-term global climate shift."
- "Having a climate plan should be a primary consideration when choosing candidates to vote for."

### Oneliner

The biggest issue facing our children is climate change; candidates without a climate plan should not earn your vote.

### Audience

Voters

### On-the-ground actions from transcript

- Support and vote for candidates with solid climate mitigation plans (implied).

### Whats missing in summary

The full transcript provides detailed insights into the urgency of addressing climate change and the importance of considering candidates' climate plans before voting.

### Tags

#ClimateChange #Voting #PoliticalResponsibility #GlobalIssue #FutureGenerations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the biggest issue
for the future of our children and records.
Because I had somebody send a message,
and that was the question.
Politically, what's the biggest issue
that's facing our children?
And the answer to that's simple.
It really is.
And it's shown in headlines right now.
Because you see a bunch of them.
Hottest average temperature of your life.
It's going to be the hottest summer of your life.
That's wrong.
That's not true.
Those are lies for headlines.
Completely inaccurate.
It's the hottest average temperature of your life.
going to be the hottest summer of your life so far. It only gets hotter from
here. If you're living somewhere and you feel like the heat the last few days has
been unbearable, please understand that eventually it will be unbearable.
Literally. We are fast approaching a lot of records. We're going to be breaking a
lot of records. As that average temperature increases, a lot of things
come with it to include water shortages, to include crop failures, extreme weather.
It's all on the way. That's the most important issue. If you are looking at
something and trying to gauge importance and you're talking about politics. If the
candidate does not have a climate mitigation plan I don't know how you can
reasonably even consider voting for them because they're that wrong. If they
do not recognize it as an issue worth addressing in their campaign they have
no business in government. Realistically, I think that they should really
acknowledge that it's the biggest problem we're facing. It's the one that
we have to act on now if we want to have any hope of real mitigation. Understand
there's a lot of stuff that is already going to happen.
Disaster flick type stuff. It's going to occur and nothing that can be done right
now can stop it, but it just gets worse from there. We have to act now. If you're
looking for an issue to use as a disqualifier, that's it. If they don't
have a climate plan and a good one, I don't know how they can earn your vote
if you have kids. It is going to get rough within your lifetime. If nothing is
done, it's gonna be really bad within theirs. There's a whole lot of
candidates who push the idea of, you know, we're doing this to protect the children
while ignoring the biggest threat they face because it's not politically
tenable because if they do that well it'll upset their donors and if that's
the case then you can't really trust them with anything right that's the
biggest issue that's the one that has to be addressed it has to be addressed
now. So, when you are really looking at some of the records that are being set
over the last couple of days, you have to take it in, you have to take it in full
view and you really have to understand that's the average temperature worldwide.
wide. It's not weather, it's not something that's going to go away, it's climate.
That's the biggest issue. Your political influence, that's what you have to,
that's a starting point. The good news is that those who typically have a climate
plan, they're also generally speaking they align more with the other things
that most of people watching this channel want. That's where you should
start. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}