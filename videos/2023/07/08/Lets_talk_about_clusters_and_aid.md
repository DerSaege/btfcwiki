---
title: Let's talk about clusters and aid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PE8UtKBD_ic) |
| Published | 2023/07/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States agreed to supply Ukraine with cluster munitions, sparking debates on bans, crimes, and escalations.
- Cluster munitions are banned by many countries due to their destructive nature, but neither Russia, Ukraine, nor the US are signatories to the ban.
- The use of cluster munitions against civilian targets is considered a crime, regardless of whether it is banned or not.
- Russia and Ukraine have both used cluster munitions in the conflict, with Russia's documented use including hitting civilian targets like hospitals.
- The US aid package to Ukraine includes artillery shells designed to target vehicles and trenches, posing less risk to civilians but potentially impacting airfields closer to civilian areas.
- Cluster munitions leave behind unexploded items that can cause harm for years, making their use highly problematic.
- Despite the destructive nature of cluster munitions, major powers continue to possess and use them, akin to nuclear weapons.
- The Ukrainian military must exercise extreme caution in using cluster munitions to minimize harm to civilians.

### Quotes

- "These are bad. They really are. But war is bad."
- "It's one of those things that is, it causes problems far beyond what people really think about."
- "They really shouldn't exist, but they do."
- "The Ukrainian military needs to be incredibly careful when using these."
- "I don't think they need to be told that."

### Oneliner

Beau clarifies misconceptions around the US aid package to Ukraine with cluster munitions, discussing bans, crimes, and escalations, stressing the need for caution in their use.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Contact organizations working on disarmament and peacebuilding to advocate for the ban on cluster munitions (suggested).
- Support efforts to raise awareness about the devastating impact of cluster munitions on civilian populations (suggested).
- Advocate for stricter regulations on the production and use of cluster munitions globally (suggested).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the implications of cluster munitions in the Ukraine conflict and the need for global action against their use.

### Tags

#ClusterMunitions #USaid #UkraineConflict #WarCrimes #GlobalPeace


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about clusters
and information and history and the laws and bans
and escalations and the new US aid package to Ukraine.
Because as soon as news broke, a whole bunch of information
started coming out.
A whole lot of it is less than accurate.
Some of it is justified, and some of it is just wrong.
So we're going to try to clear that up.
OK.
So if you missed the news, the United States
has agreed to supply Ukraine with some cluster munitions.
The words escalation, crime, and banned
started just all over social media.
Okay, let's start with banned.
There is not a party to this conflict
that is a signatory of that ban.
Not Russia, not Ukraine.
The United States hasn't signed it either.
A whole lot of countries around the world
have banned the use, production, stockpile
of cluster munitions.
Why? Because they're bad.
Okay.
The
the banned part of it, it's kinda academic here
because it doesn't
really apply to any of the countries involved.
Okay, let's uh... talk about
the crime aspect.
Just because something isn't banned
doesn't mean you get to use it however you want.
If somebody was to use a cluster munition against civilian targets, yeah that's a crime.
That is a crime.
Now as far as escalation goes, because normally this is coming out as the United States is
escalating the conflict, providing banned weapons that are going to be used in
crimes. Okay. Where have you been for the last year? The first documented use of
cluster munitions in this conflict occurred February 24th, 2022, right when
it started, when Russia hit a hospital. Now, was that really the first use?
Probably not. Probably wasn't. The other uses were probably against military
targets so they didn't get investigated. Russia has used them extensively and
they have hit a lot of civilian targets with it.
Now, does that mean that Ukraine is completely innocent when it comes to
their use. No, Ukraine has used them as well. Cluster munitions have been used
throughout this conflict. Ukraine has used them a whole lot less. Now, is that
because Ukraine is somehow morally opposed to their use? Probably not.
Probably has to do with two other things. One, they didn't have as many. They're
using some of Turkish manufacture and I think some old Soviet stuff, fact check me on the
Soviet stuff.
And then the other reason they're not using them as much is because the fighting is in
Ukraine.
So the civilians that might be impacted are Ukrainian.
Generally speaking, militaries are more careful around people of their own nationality.
So the banned part doesn't really apply.
Escalation, not even close.
The crime aspect, it's happened, but from, to my knowledge, the ones that actually look
like crimes all came from the Russian side.
That doesn't mean that Ukraine didn't do it, I may not be aware of it.
It's also worth noting that some of the times when Russia hit civilian targets, it seems
clear that it was an accident.
There are also times when it's pretty clear that it wasn't.
Okay, so what exactly is the United States providing?
like shells, like artillery shells. If I remember correctly these will basically
take out about six acres more or less. They go off above the ground and send
out a whole bunch of projectiles and I think the charges in the projectiles
have a little bit of shape to them. What are these really used for? The ideal use
would be a concentration of vehicles. Okay, a whole bunch of little little
projectiles coming down poking holes in it. Now it might have to be hit a whole
bunch to be destroyed, but to be damaged to where it really can't be used? That
could just be one. So that is the designed use of these. They'll probably
also be used against trenches. Are either one of these likely to result in a crime?
Probably not. Generally speaking, these things are not close to the civilian
population. Does that mean that this is without risk? No, because another really
good use for them is airfields because it really messes up the airfield. Those
tend to be a little bit closer to civilian areas. That being said, as we
have already talked about, the Ukrainian military has been pretty careful about
this. I have no reason to believe that that would change. Okay, so that's the
information, the factual information that you need. Okay, all of that being said,
these are horrible and they should be banned. They shouldn't be used. It's one
One of those things that is, it causes problems far beyond what people really think about.
People think about them hitting civilian areas.
The thing that often gets overlooked is a whole bunch of very small, hard to detect,
unexploded items that get left behind to be found years later. These are bad. They really
are. But war is bad. It's horrible. It would be great if these were not in use. But that's
probably not going to change anytime soon. Major powers aren't going to want to give
this up. It's a lot like nuclear weapons. They really shouldn't exist, but they do.
The Ukrainian military needs to be incredibly careful when using these, but I think they
know that. I don't think that they need to be told that. I'm certain having been on
On the receiving end of them, they understand how devastating they are.
They don't want that near their people.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}