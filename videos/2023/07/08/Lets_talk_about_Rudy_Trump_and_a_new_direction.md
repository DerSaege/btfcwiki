---
title: Let's talk about Rudy, Trump, and a new direction....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1xmotT8QvJY) |
| Published | 2023/07/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses new information about Rudy and a previous video.
- Contextualizes the importance of the information for the future.
- Mentions Rudy's involvement in schemes, including one about voting machines.
- Talks about a meeting discussing seizing voting machines and declaring martial law.
- Rudy was present at the meeting but allegedly spoke against the illegal actions proposed.
- Emphasizes the seriousness of the allegations and Smith's probe into the matter.
- Rudy's role in mitigating the extreme actions proposed.
- Raises questions about the investigation and the evidence available.
- Speculates on the potential consequences if evidence is found.
- Notes the significance of the failed implementation of certain orders discussed.
- Links the meeting to subsequent events on the 6th.
- Encourages contemplation on the gravity of Rudy's refusal to partake in the extreme actions proposed.

### Quotes

- "Rudy, of all people, was like, you can't do this. This is illegal."
- "Think about how far something has to be outside of the norm for even Rudy Giuliani to be like, no that's too far."
- "This was a big deal."
- "It is worth noting that almost immediately after this meeting, that's when they started talking about having a whole bunch of people at the 6th and that it was going to be wild."
- "Y'all have a good day."

### Oneliner

Beau addresses Rudy's involvement in schemes, particularly a serious one involving voting machines, and raises questions about the investigation's implications and Rudy's role in mitigating extreme actions.

### Audience

Watchers of current events.

### On-the-ground actions from transcript

- Contact authorities if you have relevant information on illegal activities (implied).
- Stay informed about ongoing investigations and developments (implied).
- Advocate for accountability and transparency in political processes (implied).

### What's missing in summary

Full context and detailed analysis can be gained from watching the entire video.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Rudy
and some new information that has come out.
And we're gonna go back and talk about something else.
This isn't really an I told you so video.
This is to put some things into context
to make sure that everybody's on the same page with this
because I have a feeling
this is gonna be super important in the future.
In a recent video about Rudy,
the one Rudy Queen for a day, that video.
Near the end, I say that he knew about a whole lot
of different schemes, alleged schemes, one of which
involved voting machines.
I also said that this is important because there's
a whole lot of schemes that, at least to our knowledge,
the public's knowledge, they aren't being investigated
by Smith yet. We now have information that says Smith is looking into the
meeting that discussed the voting machines, taking the voting machines, and
that it is part of a probe. Okay, so if you're not familiar with this particular
scheme, allegedly there was a meeting and at this meeting the discussion was
well, let's use the military, basically declare martial law, seize the voting
machines, appoint someone a special counsel to find irregularities. Rudy
was at that meeting. If Rudy was relaying this to, hypothetically, a federal
prosecutor, he would probably describe the event as, you know, Trump approached
me one day in distress and disarray and basically begged me to join the fray and
I said, you know, I hate him but let's hear what he has to say. So I arranged
the meeting, the menu, the venue, and the seating and tell him everything about it.
Why? Because by all accounts,
by everybody's statements about this alleged meeting,
Rudy, of all people, was like,
you can't do this. This is illegal.
This is far outside anything that we're talking about.
You absolutely cannot do this.
So, in that instance, it presents him as somebody who mitigated it. So, it would be
something that he would be very likely to bring up, assuming everybody's
statements about him, you know, kind of speaking up against it, assuming that's
true. Because it gives him, it paints him in a light of being the adult in the
room, because he certainly was in the room where it happened. Out of the
allegations, this is probably the most serious. Smith is reportedly probing it.
Rudy knows a whole lot about it and was apparently unwilling to go along with
with it. I want you to think about how far something has to be outside of the
norm for even Rudy Giuliani to be like, no that's too far. This is a big deal. We
don't have a whole lot of information yet other than there is, the reporting
is there's acknowledgement that questions have been asked about this now.
We don't know when they occurred, we don't know what the answers to the
questions were, we don't even know specifically what the questions were. So
this is one of those things that you're gonna see this material again because if
If they have any real form of evidence on this, they're going to pursue it.
This was a big deal.
Just the conversation, and apparently they made some drafts, allegedly made some drafts
of some orders, but it never got implemented.
It is worth noting that almost immediately after this meeting, that's when they started
talking about having a whole bunch of people at the 6th and that it was going
to be wild. This is going to be important later. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}