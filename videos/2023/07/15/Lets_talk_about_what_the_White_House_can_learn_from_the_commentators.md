---
title: Let's talk about what the White House can learn from the commentators....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ih8ekOadTRw) |
| Published | 2023/07/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzes recent rumors and advises on what the White House can learn regarding public relations.
- Addresses the misinformation surrounding the calling up of 3,000 reservists by the Biden administration.
- Points out the lack of critical information in the initial press release that led to false assumptions of combat involvement.
- Urges for better communication strategies to combat intentional disinformation.
- Emphasizes the importance of including context and additional information in press releases to prevent misinterpretation.
- Criticizes military commentators who spread fear-mongering misinformation knowingly.
- Encourages the public to be vigilant against manipulation due to intentional misinformation.
- Calls for improved press release strategies to dispel conspiracies and provide clear context.

### Quotes

- "Don't let them manipulate you because there's an information vacuum."
- "They lied to you. They did it on purpose."
- "The White House needs to tailor their press releases to cut off conspiratorial ideas before they start."

### Oneliner

Beau advises the White House to combat misinformation by providing context in press releases and warns against manipulation due to intentional misinformation.

### Audience

Public, White House staff

### On-the-ground actions from transcript

- Tailor press releases to include context and prevent misinterpretation (suggested)
- Be vigilant against manipulation through intentional misinformation (implied)

### Whats missing in summary

The full transcript provides a comprehensive insight into combating intentional misinformation through effective communication strategies.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about
what the White House can learn,
specifically what the public relations people
in the White House can learn
from all of the recent rumors that came about
and the one change they need to make
to help provide better information consumption
in the United States.
Okay, so what happened?
Yesterday, a press release went out.
The Biden administration signed off on calling up,
up to 3,000 reservists.
A bunch of commentators immediately were like,
it's the beginning of World War III,
and made it seem like this was going to be a massive build-up
and that the end result of this was combat
the ground in Ukraine. Okay, I put out a video breaking down who was going to be
called up, why they were being called up, said they were going to have special
skills, they were going to be instructors, they were going to be supply, logistics,
they were going to be medical. This came out today from the NSC spokesperson
Kirby. These are people that are specialists in things like administrative
functions, logistics, supply, maybe medical, dental, those kinds of things. Okay, this
isn't an I told you so video. I knew this yesterday. So did they. They had this
information yesterday. Right? This information should have been included in
the press release. The information that first went out to stop this before it
starts. And I didn't start thinking about it until somebody was like, yeah I mean
I get what you're saying but you have to admit the optics are bad. And I'm
sitting here and I'm like you know they're kind of right and there was no
reason for the optics to be bad. A couple of extra paragraphs. This operation has
been going on for 10 years. This is not a combat operation. These are the types of
people that are being called up. That would have been the end of it. The
White House, particularly this White House, has to acknowledge that there is
active and concerted disinformation network that is going to take everything
that they release and play on any information vacuum and scare people with
it and they have to start working to counter that because eventually it's
going to create a real problem. Now the other side to this is that some of the
people who, you know, were like, it's going to be World War III, some of them, they were
wrong. They made a mistake, okay? Everybody makes mistakes. Everybody's wrong at some
point in time. If you speak into a camera for a living, at some point, you're going
to be wrong, okay? At the same time, anybody who frames themselves as a military commentator
or a military expert, or anything like that, they knew this yesterday too.
They knew this as soon as they saw that press release, they knew what it really was.
So if you have people who you get information from, who told you that it was going to lead
to combat, or they tried to scare you with it, intentionally omitted information, tried
to spin it in a way that suggested, you know, Biden was, you know, going to back up Ukraine
on the ground or something like that.
They lied to you.
They did it on purpose.
It wasn't an accident.
This is information that anybody who portrays themselves as somebody who is a military expert,
they knew this.
there's no way they couldn't if they really are informed.
Now they could just be portraying themselves in a way that isn't accurate.
But next time anybody who framed this as something to truly be afraid about, to truly worry about,
next time they provide you information that is unnerving or it seems like it's scary,
remember this.
Don't let them manipulate you because there's an information vacuum.
And the White House, they should, they need to tailor their press releases to cut off
conspiratorial ideas before they start.
Because the optics on this should have been simple.
And all it would have taken to stop this kind of commentary is a couple of extra paragraphs.
that context. That thing that is so missing in today's media coverage. Anyway, it's just
Just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}