---
title: Let's talk about DOJ changing its mind about Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Whpxg09LB_4) |
| Published | 2023/07/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Justice (DOJ) changed its position regarding Trump's immunity under the Westfall Act, which could lead to another legal entanglement for him.
- DOJ previously believed Trump's statements about E. Jean Carroll were within the scope of his duties, granting him immunity.
- DOJ now claims Trump's statements were not related to his duties, potentially stripping him of immunity in the E. Jean Carroll defamation case.
- This change in DOJ's stance could allow the defamation case against Trump to proceed after being on hold.
- Trump may face trial in January for the defamation case.
- Trump has dismissed this as part of a political witch hunt, despite being found liable in a previous case.
- The shift in DOJ's position might be linked to uncovering activities by the former president outside the scope of his duties.
- Without immunity, Trump may face unfavorable legal proceedings.
- This situation adds to the ongoing legal challenges for Trump post-presidency.

### Quotes

- "DOJ changed its position, potentially stripping Trump of immunity in the defamation case."
- "Trump may face trial in January for the defamation case."
- "Without immunity, Trump may face unfavorable legal proceedings."

### Oneliner

DOJ's change in position may strip Trump of immunity, leading to a January trial in the E. Jean Carroll defamation case and unfavorable legal proceedings.

### Audience

Legal analysts, political commentators, activists

### On-the-ground actions from transcript

- Follow updates on the E. Jean Carroll defamation case and understand its implications (suggested)
- Stay informed about legal developments surrounding former President Trump (suggested)

### Whats missing in summary

Analysis of the potential impact on Trump's legal battles and political future.

### Tags

#Trump #DOJ #legal #defamation #EJeanCarroll #immunity


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and DOJ changing its mind about
something and how it impacts him and how it will likely lead to yet another
legal entanglement of his moving forward.
Okay.
So, the Department of Justice has held the position that Trump really couldn't be taken
to court over his statements involving aging Carol because he was protected by something
called the Westfall Act, which basically gives federal employees, I mean, pretty much total
immunity when it comes to anything that's within the scope of their duties.
For quite some time DOJ has held that Trump's statements were within the scope of his duties.
Something has changed and they say that they no longer have sufficient basis to believe
that his statements had really anything more than just a trivial amount of interest in
doing his job, that it was about something else.
So if it's not within the scope of his duties, then he's not immune.
DOJ changing its position on this allows the E. Jean Carroll defamation case filed in 2020,
I think, to move forward, it's been on hold for quite some time.
There are a number of legal obstacles that it had to clear.
This, to my knowledge, I think that this is the last big one.
So it seems as though Trump will be heading back to court
for another defamation case with E. Jean Carroll.
If I understand the timelines correctly, it will probably start in January.
The trial would probably start in January.
Trump, for his part, of course, said this was all part of some giant political witch hunt.
It's worth noting that he was found liable in a previous case.
That might have something to do with DOJ's change of heart or as they were investigating
other activities of the former president, they stumbled across something that led them
to believe that, well, that wasn't in the scope of his duties, it had to do with something
else.
Without that immunity, I don't know that he's going to like how things proceed, but that's
where we're at.
It's yet another continuing legal saga for the totally legitimate former president.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}