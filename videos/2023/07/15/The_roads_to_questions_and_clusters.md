---
title: The roads to questions and clusters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=u_5lFUeU5OY) |
| Published | 2023/07/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau kicks off a Q&A session addressing various questions from his audience, including the most asked one about where to find RC Cola.
- He clarifies the misconception about expired MREs in the military, explaining that the dates on MRE boxes are inspection dates, not expiration dates.
- Beau shares a humorous encounter with a deer on his ranch, which unexpectedly ends with him getting kicked by Bambi.
- He talks about addressing creators' mistakes and the importance of assuming that mistakes are unintentional and can be corrected by their audience.
- Beau tackles questions about capitalism, public schools, and cluster munitions, providing insightful responses to each.
- In the context of cluster munitions, Beau explains their use and the ongoing conflict in Ukraine.
- He ends the Q&A by reflecting on the reality of conflicts and urges empathy towards those directly affected.

### Quotes

- "I hate your power coupon analogy. It simply reinforces the idea that money makes people less accountable and more capable."
- "Why do you mispronounce Wagner now in your first video as you pronounced it correctly? Because I'm being mean and petty."
- "When you're talking about conflicts, please remember that to a whole lot of people it's not something that's happening on a screen."

### Oneliner

Beau addresses audience Q&A on various topics, from RC Cola nostalgia to cluster munitions, ending with a poignant reminder on the realities of conflicts today.

### Audience

Creators, viewers

### On-the-ground actions from transcript

- Contact local organizations supporting victims of conflicts (implied)
- Start a fundraiser to aid in conflict relief efforts (implied)

### Whats missing in summary

Insight into Beau's perspective on current events and conflicts, encouraging empathy and understanding.

### Tags

#Q&A #RCcola #MREs #ClusterMunitions #Conflict #Empathy #Community


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to do another Q&A.
This one is not really themed, except for, I guess,
at the end, they put a bunch of questions
that people asked about clusters.
And we'll go through those.
And these are really just your questions
throughout the last couple of weeks, I guess.
And apparently, the number one question that came in
was, where did I find RC Cola?
I had it in the background of a recent video
over on the other channel.
And I guess a whole bunch of people got nostalgic.
I live in the Deep South.
I find RC at the gas station.
I'll try to find a link and put one down below
to where y'all can order it if you're just craving it now.
OK.
My friend went to ROTC training at Knox.
and he said they fed him expired MREs.
Is this a sign that our military has a rations problem?
Expired MREs, but did he die?
Okay, no.
Your friend, hopefully you will tell him,
and he's about to learn something
that a whole bunch of people don't know.
Those dates on the side of MRE boxes,
those are not expiration dates.
They're inspection dates.
If I remember correctly, it is three years
from the date of manufacture.
And the inspection date is the shortest period,
like the earliest they would go bad
if they were stored at 85 degrees, 80 degrees,
something like that.
The cooler they're stored, the longer they last.
And if they're stored properly like they would be at,
like, I don't know, a military-based warehouse,
they'll last years longer than that,
like years and years and years longer than that.
A lot of them, I don't know if they still put them on there,
but a lot of them have TTIs on them,
time, temperature indicator, something like that.
It's a little circle, and it looks like a bullseye.
If the center is lighter than the ring, they're still good.
if it's the same color, they're right at the end of their life. If the center is
darker, don't eat them. As far as them feeding him expired MREs, if one's
really expired, you'll know when you open it. So my guess is they were past their
inspection date, their first inspection date. That doesn't mean that they're bad.
When is Deep Goat coming back?
In the last video you said something about other channels, you need a whole Deep Goat
channel.
I don't know, I'll see if I can round him up sometime soon.
But yeah, I don't know that there's enough content for an entire Deep Goat channel, although
it would be an interesting premise.
Why did you like the comment about Putin just declaring victory?
I have no idea what that means.
This is probably something about him just declaring victory and withdrawing.
That's probably his smartest move at this moment is to say, you know, we went there
to get rid of these elements within the Ukrainian military.
have accomplished that you know have a big banner on a you know submarine or
something saying mission accomplished and come home let's see give us a ranch
story something that has happened on the ranch that I haven't talked about oh
Oh, well, the other morning I was leaving here and I'm walking along and I'm walking
past the garden area that we have set up for that community garden video that I promise
is in production.
And I round the little greenhouse that we built and there's a deer standing there,
like feet away from me.
This deer is just chilling, hanging out.
And it wasn't startled.
just looked at me and I'm looking at it and I'm thinking okay I'm gonna pet it
you know and I reach out and it lets me and I'm sitting here rubbing the side of
this deer's face like you know like you would a horse and I'm just thinking this
is amazing you know it's early in the morning and like still misty out and I
felt like a Disney princess and everything and then this other deer
came out of the treeline and spooked the one that was beside me and it kind of
reared up and I don't know leaned into me hit me punched me like both feet
right in the chest and ran off I fell to the ground wheezing and yeah I got I got
beat up by Bambi is what happened with affirmative action gone what do you
think the best option is? Cash reparations payments. The last TV show
you watched is coming to save you from a kidnapper, I'll say for you. I'll take
Lucifer and the detective, I think I'll be alright. Who would win in a dance
fight Taylor Swift or Michael Jackson? Okay I mean I like Taylor Swift and all
but get real she doesn't stand a chance. Do you have a plan to train a successor
for your channel? No not really that wasn't something I'd given a lot of
thought to maybe I should I guess I don't know I haven't really thought
about that. Why don't you ever correct other creators? A couple of big creators
did a transphobia recently and I kind of expected you to call them out on what
seems like a rightward shift. Okay, so when I say the way I look at this, this
This is the way I look at this.
I believe in a diversity of tactics.
Generally speaking, I think that's between them and their audience.
I think their audience will correct them, and either they take the correction or they
don't.
I like to assume that mistakes like that are mistakes, and that they'll take the correction
from their audience or maybe it is just an opinion they have that they haven't gotten
through yet and they're still working on it or whatever.
But it is this actual last part is the reason I don't call anybody out like that.
The rightward shift, you know, there are, there have been channels that make that pivot
to the right, to get in with the right-wing ecosystem,
you need outrage.
That's the currency.
That's the cover to get in.
You need to generate outrage.
I like to assume that it's something that they don't have
it all figured out yet, or it's a mistake.
They'll take the correction from their audience.
But if it is not, and it is something that's calculated,
I don't want to pay their entry fee.
I don't want to give them the outrage
that they need for credibility on the right.
The worst part about this is that in the time period
that these messages come from, this
be like one of six people. I don't know who you're talking about. But generally speaking,
if you have a left-leaning audience and you do a transphobia, your audience will probably correct
you. Why do you mispronounce Wagner now in your first video as you pronounced it correctly?
Because I'm being mean and petty, somebody told me that they really didn't like it when
they heard Americans mispronounce the name.
I hate your power coupon analogy.
It simply reinforces the idea that money makes people less accountable and more capable.
Do you believe that's not true?
I don't think you hate the analogy, I think you hate capitalism.
I mean realistically it does.
is a power coupon. It provides you power to get goods and services. It does, in many ways,
make you less accountable, as we see with wealthy people when they go to court. I don't
think that it's the analogy you have a problem with. I think you don't like it because it's
That's accurate, which, I mean, I can understand that.
I homeschool my kids.
You've said things in the past that makes me think you're not judgmental of that.
Can you tell me why people think we should pay taxes for public schools, even though
we homeschool?
You know, the easy answer here is like a fire department analogy.
I've never had my house burned down.
I don't use that service, but I still pay for it.
I think a better analogy is to acknowledge that education, everybody's education, benefits
everybody.
Whether you realize it or not, every time you go to the store, every time you interact
with another person who is educated in that school district, you are benefiting from public
schools.
I think that would be the best way to kind of acknowledge that even though your kids
may not go there, you're still benefiting from it.
Can you do one of those Q&A videos for guys for dating advice?
time, all the questions were really addressing problems women had.
Sure, send in the questions.
Question for Bo at GMO.
Let's see.
That's not even a question.
Your thumbnails suck.
I can't believe you have the nerve to give YouTube advice.
OK.
I'm sorry, I'm not changing the way I do my thumbnails.
Okay so I guess this is the start of the questions about cluster munitions.
Can they be used for demining?
Yes, yes they can.
Some of them can.
So, if you missed it over on the other channel, there's a lot of commentary going on about
cluster munitions right now because the United States is providing some artillery shells
that are in fact cluster munitions.
Can they be used for demining?
Yeah.
is there's a geographic area, I want to say it's somewhere between six and a
half and seven acres that gets hit by one of these shells. The mines that are
on the ground would absolutely be disrupted by those. Now the flip side to
that is you also still have inner dud clusters that hit the ground that are
still some of those are going to be unexploded that would have to be cleaned
up. It does speed the process when you're talking about minefield so. Can
cluster bombs be broken up and used individually from drones? If so, how? Okay
so the answer is yes. How? Yeah we're not talking about that on YouTube but it
It's an almost certainty that Ukraine will use a lot of these and use the components
individually.
That is definitely going to happen.
At the same time, if this is something that's being brought up to say they won't be used
as cluster munitions, then that's not really true.
They will absolutely be used against trenches.
There's no doubt in my mind about that.
I was talking to a man who was in the Army and did stuff with artillery.
He said there was no difference between using a cluster shell and pounding the area with
a bunch of normal shells.
Is that true?
I mean, yes, it is.
It is in effect.
is in effect. The difference and the reason people object to the cluster munitions is
because if you have a 155 unexploded, it's pretty visible. The individual clusters are
not. They're harder to see. So they're more of a danger after the fact. But yeah, I mean,
The statement is true.
The cluster munitions are not somehow more horrible than normal artillery.
The issue comes from the small pieces afterward, at least most of what people are worried about.
I can't believe you support or are at least not opposing cluster bombs for Ukraine in five
years when kids are getting blown up.
Enjoy your view from your cushy chair."
Okay.
You do realize that that's happening now.
Right now.
You don't have to wait five years.
That's occurring right now, this minute.
That's happening over there.
And that's not going to stop until the war does.
That's occurring right now.
What a lot of people are saying, in effect, it's kind of similar to telling somebody who's
holding their kid in a room that's on fire, not to run down that hallway where there is
no fire because there's some fumes in there that might give you cancer in five years.
There's a war going on.
All they want, the reason they're willing to use these types of munitions is because
they want what you're worried about happening in five years.
They want it to stop happening now.
And the fortified positions, the trenches that these are likely to be used against,
this is a good tool for getting through them.
You're right, these things shouldn't exist, but they do.
They do.
And people are going to want to use them as long as they exist, because they're effective.
As far as, you know, saying don't use those, I mean, unless you are willing to get out
of your cushy chair now and rush across the field towards the trenches World War I style
to the sound of the bugle marching and step towards slaughter, I mean just keep
the chair warm for me. I don't think you want to do that the same way they don't
want to do that. I don't think it's good, but I am sitting in a cushy chair and
And I'm not going to tell people on the other side of the world how to stop their cities
from getting bombed.
And that was actually the question that my team chose to end on.
That was the last one.
Wow, so much for ending on a light note, I guess.
When you're talking about conflicts, please remember that to a whole lot of people it's
not something that's happening on a screen.
It's not something where they can take the risk that may occur in five years and think
about it in isolation from what's happening around them right now.
at risk right now, today. They're being hit right now, today. And I mean, it is worth
noting that as far as me being in my cushy chair, I can't, you know, do a GoFundMe for
air defense. I could totally do a GoFundMe to help clean it up afterward. Anyway, it's
That's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}