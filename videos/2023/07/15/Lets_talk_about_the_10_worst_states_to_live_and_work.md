---
title: Let's talk about the 10 worst states to live and work....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IazOBiVfCWU) |
| Published | 2023/07/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of discussing the 10 worst states to work and live based on empirical data from CNBC.
- The ranking grades states based on their welcoming environment for workers and families, indicating potential future business developments.
- States like Florida, Arkansas, Tennessee, and Indiana received low grades, with Texas surprisingly ranking as the worst state.
- The methodology behind the ranking considers inclusiveness as a key factor in attracting workers to a state.
- Beau points out the recent changes in inclusiveness levels in many states, impacting their rankings.
- He hints at a common factor among these poorly ranked states that might explain their low scores.

### Quotes

- "And number one will surprise you."
- "Inclusiveness is an important part of whether or not workers want to come to your state."
- "There's an interesting thing about it if you really look."
- "Y'all have a good day."

### Oneliner

Beau reveals the 10 worst states to work and live, with Texas surprisingly ranking at the bottom due to lacking inclusiveness for workers.

### Audience

Employed individuals seeking welcoming work environments.

### On-the-ground actions from transcript

- Research and understand the metrics used by CNBC to rank states (suggested).
- Advocate for inclusivity and diversity in your workplace and community (implied).

### Whats missing in summary

In the full transcript, Beau provides insights into the importance of inclusiveness in attracting workers and families to states, shedding light on the factors contributing to the rankings.

### Tags

#States #WorkEnvironment #Inclusiveness #BusinessDevelopment #Ranking


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the 10 worst states
to work and live, and number one will surprise you.
No.
Okay, so interesting thing about this is
this is from a CNBC thing, they do it every year,
and one of the unique parts about it
is that this isn't an opinion survey.
This is based off of empirical data
that they have gathered across a whole bunch of metrics.
I will try to put the link to the methodology down below.
And if I forget, it's pretty easy to find.
They do this every year.
And this really kind of grades the states
across how welcoming they are to workers and their families.
It's a good indicator of where future business developments
might go.
Short version is they won't be going to a lot of these places.
That's the logic behind the methodology.
OK.
Number 10, Florida.
They got a D, 129 points out of a possible 350.
Number nine, Arkansas, D minus, 118 points.
Number eight, Tennessee, D minus, 115 points.
Number seven, Indiana, D minus, 113 points.
Number six, Missouri, F, 98 points.
Number four is a tie.
So you have Alabama and South Carolina, both with Fs,
both with 86 points.
Number three is Louisiana with an F and 76 points.
Number two is Oklahoma with an F at 75 points.
And number one is Texas with an F and 53 points.
Kind of surprising, right?
There's a whole lot of people that move to Texas.
Remember, the goal of this is kind of to look at trends and how things deteriorate with
the climate.
Inclusiveness is an important part of whether or not workers want to come to your state.
For a whole lot of states, their level of inclusiveness has changed recently.
that is reflected in these results. There's an interesting thing about it if
you really look. I wonder if there's anything that all these states have in
common and maybe that has something to do with their scores and in some cases
with their sudden plunge to the bottom of the list. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}