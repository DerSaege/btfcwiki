---
title: Let's talk about legacy admissions and a meme....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=E-QDbGvVcHM) |
| Published | 2023/07/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Supreme Court struck down affirmative action for college admissions, sparking talk about legacy admissions.
- Legacy admissions offer preferential treatment to students whose parents attended the university.
- Harvard is currently being challenged on legacy admissions.
- Ivy League schools have a high percentage of legacy admissions, ranging from 25-35%.
- Legacy admissions make students 45% more likely to be admitted compared to those who are not legacies.
- Beau references a meme with a wealthy business owner, a white worker, and a black worker at a table with cookies to explain the concept.
- Wealthier individuals, not marginalized groups, benefit from legacy admissions and make admissions less competitive.
- Beau criticizes rich white individuals for misleading conservative white workers into believing that marginalized groups are taking opportunities away from them.
- He urges conservatives to understand that rich individuals are often the ones manipulating biases and bigotry for their advantage.
- Beau encourages people to see beyond these manipulations and recognize commonalities with others rather than being pawns in a cycle of manipulation.

### Quotes

- "They're using your bias, your bigotry, to manipulate you so they can stay up on top."
- "People who have less institutional power than you do are never the source of your institutional issues."
- "Learn from this."
- "You have more in common with the black guy down the road than you are ever going to have with your representative in DC."
- "They're playing you. They're using your bias, your bigotry, to manipulate you so they can stay up on top."

### Oneliner

Supreme Court's affirmative action ruling led to scrutiny of legacy admissions, revealing how biases are manipulated to maintain privilege.

### Audience

Conservative America

### On-the-ground actions from transcript

- Challenge legacy admissions policies (implied)
- Educate others on the manipulation of biases by the privileged (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of how biases and manipulation play out in societal systems, urging individuals to critically analyze power dynamics and commonalities among different groups.

### Tags

#LegacyAdmissions #AffirmativeAction #BiasManipulation #InstitutionalIssues #Privilege


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
legacy admissions at universities and a meme.
And what America, particularly conservative America
should learn from what's about to unfold.
Okay, so if you missed it, it's probably unlikely,
but if you missed it,
recently the Supreme Court basically struck down
affirmative action when it comes to college admissions.
Now in response to that, people have started talking about legacy admissions.
That's preferential treatment if your parent went to that university, okay?
Harvard is being challenged on this now.
Okay, overall this is across all the colleges.
The numbers on this are hard to find and they're mostly estimates, so keep that in mind.
They will vary from school to school.
That at Ivy League schools, the percentage of those admitted who are legacy is 25-35%.
Another way of looking at it would be that you are 45% more likely to be admitted if
you are a legacy. That's probably a whole lot more students than were were
assisted by affirmative action, right? What happened here? What happened? It's
the meme. It's that meme. There's that image of, you know, the wealthy business
owner and then a white worker and a black worker sitting at a table. In the
middle of the table there's a serving dish that was for cookies. Now the
business owner, they've got a whole bunch of cookies on their plate. There's
one cookie left on that serving dish and the business owner looks at the white
worker and says he's going to take your cookie. That's exactly what happened. Who
was actually knocking people out, making it less competitive. The children of, well,
wealthier people. But the wealthy in this country, they looked at the white
worker, the conservative white worker, and said, they're taking your cookie.
they're taking your spot. They knew it wasn't them. They knew that wasn't what
was happening. They lied. To conservatives, please understand this is
everything. It's not just college admissions. Everything. When you get a
bunch of rich white folk telling you that some marginalized group is the
source of your issue. They're lying to you. They're playing you. People who have
less institutional power than you do are never the source of your institutional
issues. It doesn't even make sense, but they play on that bias. They play on that
bigotry and it reinforces that bigotry, it reinforces that bias, and it's a
self-perpetuating cycle. You have more in common with the black guy down the
road than you are ever going to have with your representative in DC. They are
playing you. They're using your bias, your bigotry, to manipulate you so they
can stay up on top. It's what happened here and it's what always happens.
Learn from this.
Anyway, it's just a thought.
Yeah, I have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}