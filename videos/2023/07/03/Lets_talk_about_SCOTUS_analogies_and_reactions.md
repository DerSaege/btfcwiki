---
title: Let's talk about SCOTUS, analogies, and reactions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xfdDw9tm6HE) |
| Published | 2023/07/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reacts to a Supreme Court decision and the exaggerated reactions surrounding it on Twitter.
- Addresses the analogy made to Jim Crow laws in relation to the ruling on certain businesses' ability to openly discriminate.
- Points out the misuse of the Jim Crow analogy and the lack of understanding of its historical context.
- Acknowledges the validity of the emotions behind the reactions to the ruling.
- Expresses concerns about the vague and potentially broad implications of the Supreme Court decision.
- Contrasts the ruling with Jim Crow laws, clarifying that it won't lead to overt discrimination signs like in the past.
- Notes that the ruling's real impact lies in setting a tone that could lead to more oppressive laws in the future.
- Encourages a deeper understanding of the ruling beyond initial emotional responses.
- Concludes by expressing empathy towards those feeling fear and anxiety due to the ruling.

### Quotes

- "A whole lot of people don't really get it. A whole lot of them have no idea that the term Jim Crow was a slur at the time."
- "If you are more familiar with that topic and you understand that it is way more than that, the use of that term might have elicited more of a response from you."
- "It's not a good ruling, it's just not Jim Crow."
- "The fears that they're expressing, they're justified."
- "What people are worried about doesn't happen with this ruling, but this ruling set the tone."

### Oneliner

Beau reacts to Supreme Court decision, clarifies misconceptions about Jim Crow analogy, and acknowledges justified fears due to vague ruling implications.

### Audience

Twitter users, trans community

### On-the-ground actions from transcript

- Educate yourself on the historical context of Jim Crow laws and their impact on society (implied).
- Engage in constructive dialogues to deepen understanding of legal decisions and their implications (implied).
- Support marginalized communities through advocacy and awareness-raising efforts (implied).

### Whats missing in summary

Deep dive into the emotional impact and historical significance of the Supreme Court ruling.

### Tags

#SupremeCourt #Discrimination #JimCrow #TransRights #LegalDecisions


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a Supreme Court decision.
We are going to talk about an analogy.
We're going to talk about the reactions, exaggerations,
overreactions, and realities.
We're going to do this because I got a message.
Today, my Twitter feed has been filled with discussions
various SCOTUS decisions. And I have my own opinion of most of them, but there's
one I'm particularly worried about. A number of people I follow have in summary
said that one of the rulings basically opens the door to old-school blatant
discrimination. And then in parentheses it says a couple of mentions of Jim Crow
included. As a trans person currently stuck in one of the worst states for me,
I've already been really scared and today's Twitter discourse has done a lot
to undermine what hope I had in prior civil rights advances ability to keep it
from going in the worst directions. Are those assessments overflowing the
reality? Moderate exaggerations but some truth or are they mostly accurate? Okay
before I answer two things that I think are really important that kind of jumped
out at me you are in one of the worst states for you okay I have to assume
that means you're somewhere in the south and then in parentheses a couple of
mentions of Jim Crow included the fact that you kind of put it off to the side
and specifically mentioned it, tells me the mention of that term probably meant
a lot to you. Is the ruling about certain businesses being able to
discriminate openly, is that like Jim Crow? Is it opening the door to Jim Crow?
No. And I want to say that flatly because I have a feeling that you probably
understand what Jim Crow was. A whole lot of people who use that as an analogy,
they think the worst part about Jim Crow was signs that say you can't eat in this
restaurant. They don't, they may not understand. So if you have a a better
understanding of how horrible Jim Crow really was, you need to take that into
consideration. A lot of the analogies that get made with that, they're not good.
A lot of them don't understand that those laws, without those laws, the
intimidation and everything else that was horrible that was happening in the
South wouldn't have happened. They see it solely through the lens of access to
restaurants and water fountains. So if you are more familiar with that topic
and you understand that it is way more than that, the use of that term might
have elicited more of a response from you. A whole lot of people don't really
get it. A whole lot of them have no idea that the term Jim Crow was a slur at
the time. Their view of it is less than accurate. So that is not a good
analogy for this. That being said, are they overreacting? No, not at all.
They used a bad analogy but they're not overreacting. I don't know that you
you really can overreact. It's really hard to overreact to the highest court
in the land saying that this group of businesses that is really ill-defined
has the right to discriminate against you openly. So the emotions driving the
analogy, they're totally valid. Now the other thing is that it is a limited set
of businesses. The problem is it's really ill-defined. This was a bad ruling and I
don't just mean like the decision itself, I mean what they wrote. It is so vague
that I'm willing to bet that by the time this is over, what counts as an
expressive business is going to vary from circuit to circuit expressive what
does that mean does a sandwich artist count it's a bad ruling beyond the
decision and because it's vague there are unknowns the unknown causes fear so
So that amplifies the very justified fear that already exists.
So there's a lot of emotions with this one.
If you're looking specifically at this ruling, this in theory should be limited to ad companies,
be photographers, things you would associate with freedom of speech.
In companies that that's their product, well, they can say they don't want to work with
a certain group of people.
Whatever that group of people is, to be clear, it's not limited to LGBTQ people.
It's super broad.
In fact, I've already seen people kind of like, does this open the door to discriminating
based on race as well?
And the way the ruling's written, we don't know.
It's a horrible ruling.
So it's not Jim Crow.
That's not what's starting.
At the same time, the people who used that analogy, they used a bad analogy.
not wrong in what they're saying it allows because it absolutely will allow
pretty open discrimination in certain in certain situations with certain
businesses. This particular ruling is not going to lead to you walking up to a
restaurant or a hotel and seeing a sign that says no LGBTQ. It's not going to
lead to the more, the more oppressive laws of Jim Crow. This ruling isn't, but
the tone of the court and a lot of the people who would be commenting on this, a
lot of them have pretty good grasps of history and they understand how this
stuff progresses. So there's a legitimate fear there. It's not a good
ruling, it's just not Jim Crow. You might want to go back, remove the personal
associations you have with that term if that is, if I'm right about that, and then
to read it again. The ruling is bad. It's horrible. But it's not Jim Crow. And then
the other thing that is kind of worth noting on this is a lot of people's anxiety and
discussions, they're not really from this ruling because I don't think
that most creatives are going to take advantage of this new
right that was given to their businesses. I think a lot of the anxiety is coming
from the fact that this was decided in this way and there are going to be a lot
of cases that are going to end up before the Supreme Court that are very much
going to actually mimic Jim Crow, that are going to be that oppressive.
they'll be it's in a different way but the impact would be there. The impact
would be the the best thing for people who are being targeted by those laws
would be to get out of those states and for a lot of people like you use the
word stuck. Either your job has you there and you can't leave or you don't have the
means, or I get it. There's a lot of fear, there's a lot of anxiety, so I would say
there's a lot of reaction, but I don't think, one, I don't think it's people
intentionally misinforming, which is, I think it's more based on the emotion
of it and where it goes from here. And I think that's the concern. And the terms
that get thrown out, like I said, I'm reading this, I feel like the term Jim
Crow probably meant more to you than the people who are using it.
I would acknowledge that the fear is real, it's not unjustified.
What people are worried about, it doesn't happen with this ruling, but this ruling set
tone. And so the fears that they're expressing, they're justified. Anyway, it's
It's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}