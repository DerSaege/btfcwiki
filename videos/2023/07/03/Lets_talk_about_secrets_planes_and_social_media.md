---
title: Let's talk about secrets, planes, and social media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wvq9x1gOtBY) |
| Published | 2023/07/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Lockheed Martin's social media posts have led to speculation about a new secret aircraft.
- The silhouette tweeted by Lockheed Martin is likely the next-generation air dominance program's aircraft.
- The next-generation air dominance program includes a sixth-generation manned plane and drones controlled by it.
- The top speed of the next-generation air dominance plane is Mach 2.8, slower than the SR-71's Mach 3 cruising speed.
- Beau believes Lockheed Martin is not teasing one secret plane but two different ones.
- The technological leap to the next-generation air dominance platform will be significant, akin to the leap from previous aircraft like the F-117.
- The public often learns about advanced aircraft technology with a significant time lag due to secrecy.
- Lockheed Martin may possess technology far beyond current capabilities, potentially related to stealth technology.
- The next-generation air dominance program involves drone aircraft controlled by a manned aircraft pilot using artificial intelligence.
- The Air Force aims to receive these advanced aircraft by 2030, showcasing significant advancements in military technology.

### Quotes

- "The technological leap between what we have now and the next generation air dominance platform will probably be as big a leap as it was taken from the planes we had before."
- "Lockheed Martin at this point has something that is far beyond the capabilities of anything that we actually know about yet."

### Oneliner

Lockheed Martin's social media posts spark speculation about new secret aircraft, likely the next-generation air dominance program, showcasing significant advancements in military technology.

### Audience

Aviation enthusiasts, defense analysts.

### On-the-ground actions from transcript

- Research and stay updated on advancements in military aircraft technology (implied).
- Monitor official announcements from defense contractors for insights into future developments (implied).

### Whats missing in summary

Beau's engaging storytelling and insights into the advancements in military aircraft technology can be fully appreciated by watching the full video.

### Tags

#Aircraft #LockheedMartin #MilitaryTechnology #NextGeneration #AirDominance


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about aircraft,
planes, and secrets, and how they stay secret,
and social media posts,
and how all of this comes together to prompt a question.
Because a series of social media posts have led people
to wonder what a very large American defense contractor is up to. Question is real simple.
Is Lockheed Martin trolling the world about a new secret aircraft? And the answer to that,
in my opinion, is no. But not the way you think the answer is no, so stick with me.
Okay, so, what prompted all of this?
Lockheed Martin's social media,
complete with, I think it had the Skunk Works logo on it,
which is their super secret division.
They tweeted out a silhouette of an aircraft.
It is not a known aircraft.
This made people say, hey, they're working
on a new aircraft, they have something going on.
obviously. Some people combine that with some very cryptic posts from Lockheed
Martin that started at the end of last year. The first one says under the cover
of darkness pictured is an SR-71 with the moon shining high in the sky and it
has a picture of an SR-71 and a hanger. It also says on December 22nd 1964 the
SR-71 took its first flight as the fastest aircraft of its time. Aviation
nerds all over the world raised their eyebrows of its time. Officially it's
still the fastest plane. So that made people wonder was this a typo or was
this, you know, them trying to signal something. In March they tweeted highway
to the danger zone hashtag real top gun. Not joking, they really tweeted that. The
SR-71 Blackbird is still the fastest acknowledged crude air breathing jet
aircraft. Acknowledged implies the existence of the unacknowledged. So
people have put these three posts together and say, hey, this silhouette is
an aircraft that is super secret and faster than the SR-71. No, I think you're
wrong. I think you're wrong. The silhouette is most likely the silhouette
of the next-generation air dominance program. This is a program that has a
sixth generation plane that is manned and then it is part of the loyal wingman
program which means that manned plane controls a bunch of drones that they
kind of follow the plane along. That's your most likely explanation for that
silhouette. Here's the problem. According to the information that's been released
the top speed of the next-generation air dominance plane is Mach 2.8. The SR-71
had a cruising speed of like Mach 3. That's not it. I don't think Lockheed
Martin is trolling the world about a secret new plane. I think they are
the world about two secret new planes. Two different planes. One being the
next-generation Air Dominance which is, it's well known, we've actually talked
about the Loyal Wingman program on this channel. There's a lot of
information out there about what that is and what it's supposed to accomplish. So I
think there's that, which is the silhouette, and then I think there's
something else that we don't have a clue about yet.
OK, so how big of a deal is this?
The technological leap between what we have now and the next
generation air dominance platform will probably be as
big a leap as it was taken from the planes we had before.
what most people know is the stealth fighter, the F-117.
That's the type of technological jump
we're talking about.
It's worth remembering, and if you weren't
around back then, this is definitely worth noting,
the decision to make the F-117
was made in the 70s,
I think 1978. When did the public
get to see it. 1990, 1990. So more than 10 years and if I'm not mistaken the only reason
they showed it to anybody is because they were going to use it. So there's a lag between
what we know about and what is being produced.
Sometimes that lag can be really big.
It can be a very lengthy, lengthy lag.
If you are super interested in that silhouette,
because it's incredibly unique, go ahead
Google Tester Corp F-19. It's going to bring up a model kit and those silhouettes
look pretty similar, right? That model kit was made by Tester Corp based on
what they called inside information in 1986. Incidentally, the inside
information reportedly came from none other than Lockheed Martin. We
don't know that it's the same. There are a lot of people who believe that
that F-19 model was actually like a vehicle to demonstrate have blue
technology, stealth technology, and we don't really know yet, but what can
safely be assumed is that Lockheed Martin at this point has something that
is far beyond the capabilities of anything that we actually know about yet.
When we talk about foreign policy on this channel, a lot of times when we're
talking about near piers. I often say the US owns two things, the night and the sky.
This is why. This is why. The developments here, especially when you're talking about
the next generation air dominance, what's supposed to be in this program, what it's
supposed to be able to do, it's wild. It's these drone
aircraft controlled by the pilot of the manned aircraft but using artificial
intelligence to select targets. It is straight out of science fiction
movies, you know, with a date far, far, far from here. The Air Force, if I'm not
mistaken, wants to take delivery of these aircraft in 2030, just seven years from now.
So yes, there is something out there that we don't know about.
But I don't think it's a new secret plane, I think it's two.
And you need to remember the SR-71, that incredibly fast aircraft, very hard to detect.
It also, it ran for a really long time.
Incidentally, it's so hard to detect.
There's actually a small piece of one behind me and has been for a long time.
You just can't see it.
It's both a joke and true.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}