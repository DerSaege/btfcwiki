---
title: Let's talk about a question of Russia's strength....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cee_0E142X4) |
| Published | 2023/07/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring a theory about Russia's hidden reserves that has resurfaced after recent events.
- Hypothetically assuming Russia possesses a secret, strong reserve that has been concealed from US and Western intelligence for over a year.
- Considering the theory that Russia is intentionally accepting significant casualties and refraining from using precision-guided munitions.
- Speculating on the strategic purpose of Russia holding back this powerful reserve instead of using it to crush Ukraine.
- Questioning the lack of clear strategic value in Russia withholding these supposed reserves and not utilizing them when needed.
- Contrasting the current Russian military with the Soviets and the level of deception employed by both.
- Emphasizing the lack of logical reasoning behind the theory of Russia's hidden reserves in light of the losses sustained.
- Urging listeners to critically question and analyze the validity of this theory whenever it resurfaces.
- Challenging the notion that there is any strategic benefit in Russia's apparent reluctance to deploy these reserves.
- Concluding with a reminder to ponder the motive behind perpetuating this theory and its lack of substantiated reasoning.

### Quotes

- "Let's just pretend that's true, okay?"
- "Every time you hear it, ask yourself why? Why would they do this?"
- "There is no logical reason for this to occur."
- "The losses they have sustained are too great for this to be a faint."
- "It's just a thought."

### Oneliner

Exploring the questionable theory of Russia's hidden reserves and the lack of strategic rationale behind withholding them.

### Audience

Analytical viewers

### On-the-ground actions from transcript

- Question the validity of theories and rumors before accepting them (implied).

### Whats missing in summary

The detailed analysis and reasoning Beau provides behind questioning the theory of Russia's hidden reserves.

### Tags

#Russia #MilitaryStrategy #Deception #Geopolitics #Analysis


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about Russia
and perceptions and strategy
and numbers and math.
And we're going to go through an idea
that just, it keeps coming up.
And this is something we talked about on the channel
when the whole issue first started.
And I'm like, no, that is not a thing.
Nobody actually does that.
This isn't the video game civilization.
You don't send in your bad units first type of thing.
But this is a theory.
It's a rumor that just won't die.
So we're going to go about it a different way today.
Because ever since the whole Wagner dust-up thing, a whole
bunch of commentators have brought this theory out again.
So rather than explaining how it's bad, okay, like how this idea just doesn't really hold
up to any scrutiny, we're going to say that it's true.
Let's just pretend that's true, okay?
Russia has just a strong, very powerful reserve that has eluded all US intelligence, all Western
intelligence and they've been holding it in reserve for more than a year. Okay? We
are going to assume that the mobilization was completely unnecessary.
They didn't actually need to do that because they have these crack troops
in reserve. We are going to assume that for some reason they are accepting the
just enormous casualties that they are accepting. We are going to assume that
that their lack of precision guided munitions is intentional.
They have warehouses of that stuff somewhere.
They're just choosing not to use it.
And all of that giant attempt and them taking chips out of consumer products so they could
then recycle them to use them for military applications.
That's all just a giant like SIOP.
All the supply issues we've heard about.
all manufactured to get people to believe that Russia is weak when they
have this giant strong reserve ready to pounce. Okay, we're not gonna argue any of
that. All of that is true. Why? Why? What possible reason? The the basis of this
theory is that Russia has the strength to just crush Ukraine. They're just not
using it. They're holding it in reserve for some reason. Why? What possible
strategic value does it serve? Doesn't make any sense, right? You can't actually
articulate a reason for them to do this. Because let's remember the war is not
about the fighting on the ground. The war has geopolitical implications. Russia's
whole goal was to say, hey you know we don't want NATO on our borders. You know
when it would have been a really like wise time to release this super secret
reserve that nobody can find, before other countries started joining NATO
that are on their borders. That would have been a wise time to do it, to show that
they can strike at any time and just take that country. That would have been a
good time. It also probably would have been a good time to use that reserve
when there was an armed column rolling towards Moscow. If that reserve existed
it would have been used then. And realistically that's the reason why it's
being brought up again. My guess is the reason this rumor is coming out is
because the Wagner forces encountered so little resistance. There weren't, they
couldn't get armor on the road to stop them. So they have to resurface this
idea that that armor exists somewhere and it's just completely hidden, you know,
a hollowed-out mountain or something.
I have yet to have anybody give me any possible strategic reason for actually doing this.
The goal is to take Ukraine, or it was, and I know people will argue that, but whatever.
Now the goal is more limited to hold on to what they've taken.
They're not even doing that now.
If that reserve existed, trust me, they would put it into play and I doubt anybody can give
me a real reason why they wouldn't.
It's a myth.
People talk about how the Soviets used deception to this amazing degree.
is true. It's a fact. It's important to remember these aren't Soviets. This is
not an ideologically motivated military. This is the military of a capitalist
oligarchy. They don't do it to that degree because they're not
ideologically motivated anymore. The greatest deception that the Russians have
pulled is convincing everybody that they're as good as the Soviets were. At
this point it's pretty evident that they're not. This rumor undoubtedly will
continue. This theory that they have all of these reserves they're just not using
for whatever reason, it will undoubtedly continue. Every time you hear it, ask
yourself why? Why would they do this? You're not going to be able to come up
with an answer because there's not one. The losses they have sustained are too
great for this to be a faint. Just question it that way. You know, a whole lot
of people asked about this because it became another talking point again. Like
it got brought up again. There is no logical reason for this to occur. There
There is no strategic value in losing what they have lost.
It's just not there.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}