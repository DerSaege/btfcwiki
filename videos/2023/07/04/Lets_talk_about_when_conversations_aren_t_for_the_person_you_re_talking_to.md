---
title: Let's talk about when conversations aren't for the person you're talking to....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zEx3norUAvA) |
| Published | 2023/07/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Returned home to a semi-rural conservative town in Norcal after several years, attending church with his mom.
- Mom's pastor was homophobic, transphobic, and misinformed about homelessness, blaming substance use and laziness.
- Had a lengthy talk with mom about the pastor's sermon and was asked to speak with him directly.
- Views engaging with someone promoting violence and hatred as challenging within an ultra-conservative religious context.
- Recognizes the unlikelihood of changing the pastor's mindset but encourages the attempt regardless.
- Suggests having the difficult talk not to succeed but to show the importance of the issue to his mom.
- Emphasizes that the goal isn't to change the pastor but to influence his mom's church choice.
- Warns of the pastor potentially leading his mom down a harmful path if not addressed early.
- Advocates for having the tough and possibly ineffective talk as a way to protect his mom's beliefs.
- Compares conversing with the pastor to responding to comments online: impact may not be immediate but can influence others.

### Quotes

- "The sad fact is that preaching hatred and violence and bigotry in Jesus' name, that is a moneymaker."
- "Not because you'll succeed, but because you'll fail."
- "A conversation is not for the pastor. A conversation is for your mom."
- "It's probably worth it, probably worth the time."

### Oneliner

Beau returns home to confront a homophobic pastor, urging a difficult talk with mom not to change the pastor's mind but her church choice.

### Audience

Family members, allies

### On-the-ground actions from transcript

- Talk to your family directly about harmful beliefs in their community (suggested)
- Guide family towards more inclusive and welcoming environments (suggested)

### Whats missing in summary

The emotional weight and importance of standing up against harmful beliefs within close-knit communities.

### Tags

#Family #Community #Homophobia #ConservativeTown #Religion


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about a trip home to see mom
and how sometimes a conversation isn't really meant
for the person you're having it with because I got a message
asking for some advice and I think it's,
it might be useful to other people.
I went home to my semi-rural conservative town
Norcal a few weeks ago, and I went to church with my mom for the first time in several years.
While she is a kind and welcoming person, her pastor proved himself to be a homophobic, transphobic,
and massively misinformed human that seems to think homelessness was caused primarily by
substance use and laziness. I was very surprised. My mom was going along with this, and the following
week she and I had a long conversation about why I thought his sermon was ridiculous. She
asked me if I'd be willing to speak with him directly. I suppose there's some merit in
engaging with someone who's abusing a platform to promote violence and hatred, but I can't
bring myself to care about reforming the ultra-conservative sex of a religion that has been used as the
the spear tip of division for so long.
Yeah I get it.
I get it.
And realistically, having this conversation with him is probably not going to change anything.
Not really.
The sad fact is that preaching hatred and violence and bigotry in Jesus' name, that
is a moneymaker.
That's a cash cow.
going to be really hard to get anywhere trying to get him to, I don't know,
like teach the teachings of Christ in his church. That's gonna be super hard.
You're probably not gonna get very far. All that being said, I think you should
totally do it. I really do. I think you should do it. Not because, not because
you'll succeed, but because you'll fail. Because you'll fail. If your mom asks you
talk to the pastor about this, she agrees with you. On some level she has to. She
wouldn't send her son in there to talk to the pastor if she thought you were
completely wrong, unless she thinks he's gonna change your mind. But that seems
unlikely. She thinks you're right, would be my guess. If she thinks you're right
and you go in there and you talk to the pastor and the pastor does not accept
any of your points, which probably won't. It might send your mom to a church that
is more fitting someone who is kind and welcoming if she sees that the pastor
isn't open to a change like that. And I would also say that if you were to go
there and discuss this with him, it might tell your mom how much it means to you.
you, that might also change where she goes to church.
And if you're at the point where right now you're still having these conversations and
they're still friendly, it's probably better to try to get her to a different church before
really changes and that pastor leads her down a road where you won't be able to
have those conversations because she'll, she very well might start mimicking his
beliefs. It's probably worth it, probably worth the conversation, probably worth
the time, as long as you understand that in this very rare instance when you, when
it doesn't go anywhere, it's still okay because a conversation is not for the
pastor. Conversation is for your mom. It's a lot like answering comments down in the
comment section. You're probably not going to change that person's mind, but
you might change somebody who reads it. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}