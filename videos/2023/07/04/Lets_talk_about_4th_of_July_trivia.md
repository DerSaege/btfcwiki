---
title: Let's talk about 4th of July trivia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tQiKsXCYTx0) |
| Published | 2023/07/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing a Fourth of July special video focusing on eight figures central to the founding of the country.
- Each figure is briefly discussed in terms of their historical significance and notable associations.
- The figures include Alexander Hamilton, Pierre Charles L'Enfant, Frederick von Steuben, Lafayette, Charles Adams, John Lorenz, James Buchanan, and Thomas Morton.
- Reveals a common thread among these figures - their confirmed or rumored ties to the LGBTQ community.
- Suggests further research into Jamestown household structures and the origin of the term "log cabin Republicans."
- Reads a message requesting a video celebrating American history without LGBTQ references.
- Responds to the message by honoring the men who contributed to the freedom of speech, as requested.
- Concludes with a wish for a happy Fourth of July.

### Quotes

- "They were all confirmed or rumored by their contemporaries to be part of the LGBTQ community."
- "How about you just pay homage to the men who gave you the freedom of speech you abuse daily."
- "Granted. Happy Fourth of July."

### Oneliner

Beau presents a Fourth of July special video honoring eight historical figures tied to LGBTQ identities, sparking a reflection on American history and freedom of speech.

### Audience

History enthusiasts, LGBTQ+ community.

### On-the-ground actions from transcript

- Research Jamestown household structures and the origin of "log cabin Republicans" (suggested).
- Celebrate and honor LGBTQ figures in American history (exemplified).

### Whats missing in summary

The emotional impact of recognizing LGBTQ individuals in historical contexts. 

### Tags

#FourthOfJuly #AmericanHistory #LGBTQ+ #FreedomOfSpeech #HistoricalFigures


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today is definitely gonna be a little bit different.
Today, we're going to just do the video,
prompted by a message, and then read the message afterward.
The video itself is going to be a Fourth of July special.
We're going to talk about eight figures
that were very important to the founding of this country,
and I'm basically just going to go through
and name the figure, and talk about the thing
they're best known for.
And then I'll give you one piece of trivia
that ties them all together.
And then I'll give you a couple other things
to look up on your own if you wanted to later.
Okay, so the first one, Alexander Hamilton.
Actually incredibly important to the founding
of this country, but realistically best known
for a musical.
You have Pierre Charles L'Enfant,
I'm sure I mispronounced that,
architect of DC. DC would not look like DC without him. You have Frederick von Steuben, who was
instrumental in turning the fledgling American military into something a little bit more modern
by the standards of the day. Of course you have Lafayette, also very important militarily. You
You have Charles Adams, who was important in his relationship to Steuben, but probably
best known for being the son of John Adams.
You have John Lorenz, who was probably best known for recruiting slaves into the war effort.
You have James Buchanan, who probably best known for being the first bachelor president.
And then you have Thomas Morton, who is certainly best known for challenging the social beliefs
of his contemporaries.
Okay, so what do these eight men have in common?
They were all confirmed or rumored by their contemporaries to be part of the LGBTQ community.
And while we're on this topic, if this is something that interests you, you might want
look into how Jamestown, that really important colony, how their households
were structured at times. And you may want to find out why log cabin Republicans
are called log cabin Republicans. Now let's read the message. Hey you did a
lot for pride and I tolerated it. For 4th of July could you just do a video
celebrating America or American history, it would be great to not hear gay stuff,
not his term today. How about you just pay homage to the men who gave you the
freedom of the speech you abuse daily.
Granted. Happy Fourth of July.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}