---
title: Let's talk about SCOTUS, unity, and a talking point....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qAcyEapd5QE) |
| Published | 2023/07/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a talking point emerging about separating the T-Q from L-G-B within the LGBTQ community.
- Notes that this idea is being pushed by those sympathetic to the right, suggesting that distancing from the T is better for the LGB.
- Warns against falling for this tactic of divide and conquer used by authoritarian regimes throughout history.
- Mentions a Supreme Court decision that wasn't just about the T, and urges not to be convinced that the targeting won't extend to others in the LGBTQ community.
- Emphasizes that authoritarian groups always move on to target the next group, so division within the community is not a solution.
- Stresses the importance of not being misled and manipulated by such tactics, referencing the Supreme Court ruling as evidence.
- Concludes by warning against trying to appease authoritarian figures by sacrificing parts of the community.

### Quotes

- "Don't let them convince you that they're not coming for you too."
- "There's always a next group for authoritarians, for people of that mindset, and you will not turn that authoritarian tiger into a vegetarian by feeding it your friends."

### Oneliner

Authoritarian tactics of divide and conquer within the LGBTQ community must be recognized and resisted, as unity is vital against oppressive forces.

### Audience

Advocates for LGBTQ rights.

### On-the-ground actions from transcript

- Resist attempts to divide the LGBTQ community (implied).
- Stay united and support all members of the LGBTQ community (implied).

### Whats missing in summary

The full transcript provides a deeper dive into the dangerous tactic of dividing the LGBTQ community and the importance of unity in resisting oppressive forces.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to provide a little footnote
to a talking point that has emerged recently
when it comes to the idea of unity.
And we're going to kinda go through it,
explain what the talking point is,
and then discuss it a little further.
Right now you have a whole lot of people pushing the idea
that it would be better if it was just L-G-B, that the T-Q, well, they need to separate
off because they, well, it's obviously them, those others that are bringing down the whole
movement and giving everybody a bad name, now this is being floated by people who are
sympathetic to the right. The idea is that the right wing, well they're really
only mad at the T in LGBTQ. And so the best thing to do for the LGB would just
be to like distance yourself. I would hope that most people see through
that. Authoritarian governments, authoritarian people, if they are
successful at scapegoating a group and they get their way with that group, they
They just move on to the next one.
That seems like it should go without saying because that's what's been true through all
of history.
But I want to point something else out and I think it's important to acknowledge this
little footnote as this story gets pushed.
idea of dividing up and just separating, not worrying about them, throw them to the wolves.
That Supreme Court decision, that wasn't just limited to tea.
Do not let them convince you that they're not coming for you too.
they absolutely are. This talking point, I'm sure that almost everybody sees
through it, but anybody who needs any reassurance that it is just a
talking point, it's a method of divide and conquer, look at that Supreme
court ruling. It wasn't about the rights of the T, it was about the rights of the
LGB. And they struck it down, right?
Don't let them lie to you.
There's always a next group for authoritarians, for people of that mindset, and you will not
turn that authoritarian tiger into a vegetarian by feeding it your friends.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}