---
title: Let's talk about a mutual problem with electoral politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hQ7WJ33U5ZY) |
| Published | 2023/07/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a debate between American leftists and liberals about civic engagement.
- Describes a situation where a leftist friend is trying to convince a liberal friend to participate in mutual aid.
- The liberal friend believes in electoral politics and doesn't see the effectiveness of mutual aid.
- Beau suggests engaging in the other's preferred form of civic engagement to bridge the gap.
- Emphasizes the importance of experiencing mutual aid firsthand to understand its effectiveness.
- Encourages trying to change the liberal friend's perspective by involving them in mutual aid activities.
- Advises removing barriers to understanding by allowing the liberal friend to see the impact of mutual aid.
- Acknowledges the different perspectives between leftists and liberals on civic engagement.
- Urges experimentation and understanding between friends with differing political views.
- Recommends taking actions to bridge the gap and show the effectiveness of mutual aid.

### Quotes

- "If she shows up, she will see that mutual aid is absolutely effective. It does work."
- "You won't do the easiest thing, the thing that to her is effective and easy."
- "But oftentimes people need to see that for themselves."
- "Because if she sees it with her own eyes, she'll believe that too, right?"
- "Remove it. that would be my suggestion."

### Oneliner

Beau suggests bridging the gap between leftists and liberals by experiencing each other's preferred forms of civic engagement, aiming to show the effectiveness of mutual aid.

### Audience

Friends with differing political views

### On-the-ground actions from transcript

- Experiment with each other's preferred form of civic engagement (suggested)
- Invite the liberal friend to participate in mutual aid activities (implied)
- Allow the liberal friend to see the impact of mutual aid firsthand (implied)

### Whats missing in summary

The full transcript provides a detailed guide on how to navigate differing political perspectives within friendships and advocates for practical experiences to understand the effectiveness of mutual aid.

### Tags

#CivicEngagement #Leftists #Liberals #MutualAid #PoliticalPerspectives


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about a time-honored tradition
in American politics, a debate that has gone on a very, very
long time and will certainly persist through the ages.
And we're going to talk about some advice that somebody wants
and how maybe there are elements within the American left that
could come to an agreement if they tried. Okay, so here's the message. My friend and
I have an issue. We've been friends through college. I'm a leftist. She's a
liberal. I try to get her to come out and do some mutual aid with my group and she
always uses the excuse of not really believing its effectiveness and says I'm
not serious about politics because I couldn't care less about electoral
politics. She's literally a vote blue no matter who lib. There's another modifier to that.
She's literally a vote blue no matter who lib lecturing me on effectiveness. I was hoping
you had some advice on how to get her to see it works. She just won't come out. We're both
really stubborn. It's why we're friends. I need an edge. I need something to show her that voting
is quote, the least effective form of civic engagement.
I've asked leftist streamer why,
and he said not to waste time on reaching out to blue MAGA.
Since no offense, all you do is waste time
trying to reach liberals, I thought I'd ask you.
Nice, nice, thank you.
Okay, I get it.
natural enemies, you know, liberals and leftists,
like conservatives and leftists,
and centrists and leftists, and leftists and other leftists
with a slightly differing opinion on something.
Okay, so what do you have?
You have two people that have differing political views.
What you're hearing is she doesn't take you seriously
because you won't engage in the least effective form
of civic engagement.
That's the way you're looking at it.
of what you're hearing. Let me tell you what she's really saying and act as the
liberal translator here. What she's saying is she can't take you seriously
because you won't do the simplest form of civic engagement. That's really her
position. You won't do the easiest thing, the thing that to her is effective and
easy. And if you won't do that then the other stuff it's probably just for
show. That's really the opinion. That's how it often, that's how
that opinion is often held. Okay, so as far as reaching out, because again you
have to acknowledge that not everybody came out of the womb knowing the proper
theory. Here's a simple idea. Let's say, hypothetically speaking, that you had a
mutual aid group near you that needed help with something but you didn't want
to do it. And you also needed help with something. Your group needed help with
something and they didn't want to do it. What might you do? There might be an
opportunity for both of you to engage in the form of civic engagement the other
one prefers. Maybe you could try that because here's the thing you believe
you're right. I mean I would assume that if you're out there doing mutual aid
you believe you are correct. It is more effective. The reason you're trying
Trying to get her to show up is what?
Because if she sees it with her own eyes, she'll believe that too, right?
So get her to show up.
Make an agreement there where you both experiment with the other's preferred form of civic
engagement and go from there.
Because you're probably right.
You are probably right.
If she shows up, she will see that mutual aid is absolutely effective.
It does work.
It is definitely what gets the goods.
But oftentimes people need to see that for themselves.
I'm sure deep down you also believe if you get her associated with your group that maybe
Maybe she will no longer be simply a vote blue, no matter who, Lib.
It's right there.
It seems pretty simple.
Because if it is as important to her as you're making it seem, if that's her stumbling block,
remove it.
Remove it.
that would be my suggestion. But again, I don't know much about this. All I do is waste
time reaching out to liberals. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}