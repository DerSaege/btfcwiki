---
title: Let's talk about investigating the investigators of the investigators....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=B2XRPkhJJmY) |
| Published | 2023/07/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Investigating the investigators of the investigators, referencing the U.S. Congress and Attorney General Barr.
- Democrats on the House Judiciary Committee are seeking information from Garland due to concerning evidence.
- Barr allegedly used special counsel regulations to bury a criminal investigation rather than promoting independence and fairness.
- Mr. Durham's decision not to charge former President Trump after investigating alleged financial crimes raises concerns.
- The Durham investigation uncovered information about Trump but did not lead to any charges.
- Beau expresses skepticism about significant outcomes from these types of investigations historically.
- Investigations launched by Congress often do not result in criminal charges or significant changes.
- Despite low expectations, Beau acknowledges the unprecedented nature of the information uncovered about a former president.
- Beau anticipates minimal impact from the ongoing investigation despite various allegations and public interest.
- Generally, investigations like these may not reveal what people hope to find.

### Quotes

- "Investigating the investigators of the investigators."
- "I don't think much."
- "Not much is going to come of this."
- "Generally speaking, these types of investigations don't really uncover what people hope to uncover."
- "It's just a thought."

### Oneliner

Investigating Congress and Barr's actions with skepticism on the outcome, Beau questions the effectiveness of ongoing investigations into Trump.

### Audience

Congressional Watchdogs

### On-the-ground actions from transcript

- Monitor and advocate for transparency in investigations (suggested).
- Stay informed and engaged with developments in ongoing investigations (suggested).

### Whats missing in summary

Insights into potential implications of ongoing investigations and the importance of accountability and transparency in governmental processes.

### Tags

#Investigations #Congress #AttorneyGeneral #Transparency #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about investigating
the investigators of the investigators.
I think that's where we're at in this little cat and mouse
game that is being played out in the U.S. Congress.
Okay, so what's going on?
Democrats on the House Judiciary Committee
are asking Garland for information.
Because of the strength and seriousness of this evidence, Attorney General Barr
acknowledged that the department was compelled to investigate, but instead of
allowing the investigation to proceed through normal channels, he asked Mr.
Durham to handle the investigation, secretly goes on, in this instance, it
appears the attorney general Barr turned the special counsel regulations on their
and use the special counsel not to promote independence and fairness, but
to bury a potentially significant criminal investigation from the public.
Mr. Durham's refusal to explain his decision not to charge former President
Trump after secretly investigating him for alleged financial crimes is highly
concerning, especially considering Mr. Durham's extensive public discussion of
evidence that was rejected by juries which acquitted the defendants that he
charged. Okay, so if you have no idea what that's about, during the Durham
investigation, it seems like they uncovered some stuff about Trump and
they went and checked it out, but it didn't go anywhere. Okay, now the
Democratic Party at the moment is kind of indicating that they believe maybe it
should have gone somewhere, and the then attorney general kind of buried it.
Okay, so what's going to happen?
Honestly, I don't think much.
I could be wrong, but generally speaking, these types of investigations, the reason
they don't proceed, normally there's a good reason.
Normally there's something. Maybe the evidence wasn't credible,
maybe it's, you know, upon actually investigating it,
it might have been unethical but it wasn't illegal, or
it was something that was right on the cusp and because of
the Bureau's desire to, you know, or the Department of Justice's
desire to remain apolitical, they kind of passed on it.
There's a whole bunch of reasons that could be that are legitimate or at least semi-legitimate reasons for doing it.
Most times when these types of investigations get launched by people in Congress, they don't go anywhere.
Just historically speaking, it's pretty rare for an investigation to have occurred
and then Congress to step in, and something change to the point where it matters.
They might find out something that doesn't sit well with the public or something like that,
but it generally doesn't prompt any criminal charges.
That being said, that is how things normally work.
However, this particular investigation stumbled upon information about a former
president that has now been indicted multiple times and is under investigation
for a whole bunch of other stuff.
I don't want to use the word unprecedented, but I mean, here we are.
My gut tells me that not much is going to come of this.
at the same time given everything else going on, I can't really fault them for wanting
to look because there's a lot of allegations, throwing in a few more wouldn't really be
shocking at this point.
But before anybody gets their hopes up and turns into a, you know, donkey version of
somebody screaming butter emails, just remember that generally speaking these
types of investigations don't really uncover what people hope to uncover when
they when they're concluded. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}