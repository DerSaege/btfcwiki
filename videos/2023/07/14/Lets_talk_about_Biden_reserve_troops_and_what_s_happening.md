---
title: Let's talk about Biden, reserve troops, and what's happening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8MOuODkgxgo) |
| Published | 2023/07/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration calling up to 3,000 reservists and 450 IRR for Operation Atlantic Resolve, which has been ongoing since 2014.
- Not an escalation or troop increase but freeing up military for support roles.
- Atlantic Resolve showcases muscle in Europe without engaging in combat.
- US military faces recruitment issues since 2018, not caused by Biden.
- Qualifications for those heading over involve inside jokes, specialty logistics, training national armies, and medical roles.
- Sparse reporting leads to uncertainty among people.
- No indication of gearing up for combat; likely filling specific roles in ongoing operation.
- Rotations of reserve units heading over, but focus on targeted roles.
- Operation Atlantic Resolve has been ongoing for almost a decade, no need to panic.

### Quotes

- "This isn't new, it's just allowing a different pool of troops to be tapped to go over."
- "The reason people are asking about this is because the reporting is pretty sparse."
- "No reason to panic, really."

### Oneliner

Biden authorizes reservists for Operation Atlantic Resolve in Ukraine, focusing on support roles, not combat; sparse reporting causes uncertainty but no need to panic.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Prepare for possible rotations of reserve units heading over for Operation Atlantic Resolve (implied).

### Whats missing in summary

The full transcript provides detailed insights into the Biden administration's decision to call up reservists for Operation Atlantic Resolve, offering clarity on ongoing military operations in Ukraine and dispelling misconceptions about troop increases or combat escalation.

### Tags

#Biden #Ukraine #OperationAtlanticResolve #Military #Reservists


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden and Ukraine
and reserves, and we're going to go through some information.
We are going to clarify some information
that has gone out that is less than accurate
and a perception that may be building among people
who just have an information vacuum and don't know
and they're trying to fill it in.
And then we'll answer some questions
some messages that came in. Okay, so if you have missed the big news, the Biden administration has
signed off on calling up up to I want to say 3,000 reservists and up to 450 IRR for Operation
Atlantic Resolve. First, let's start there. Atlantic Resolve has been around since
2014. This is not an escalation. This is not a move to start a military
operation that would be on the ground in Ukraine. This has been going on for
almost a decade. It's been rotations, okay. Now, it is also not a troop
increase. What this is doing is freeing up the the military so they can pull
reservists in to do certain jobs that are in this support. The idea of Atlantic
Resolve is to basically kind of show a little bit of muscle in Europe, okay? Not
actually doing anything, not actually engaged in combat. That is how it has been
for a decade, almost a decade. Okay, so hopefully that brought the temperature
down a little bit for people. This isn't new, it's just allowing a different pool
of troops to be tapped to go over. Now, why do they need reservists?
Because the US military has had a recruitment issue since 2018. I know a
a whole lot of the coverage is saying it's Biden's fault. There will be a video
down below detailing the recruitment goals and the recruitment numbers year
by year. It started in 2018. So if the military went woke and that's what caused
it, it happened under Trump. Okay, now for all the messages. Those people who sent
end messages asking about, you know, who's gonna be heading over. So we'll go
through some qualifications first. If you understand any of the inside jokes that
are currently on screen, that would be a qualification. If you were involved in
high-end or specialty logistics, that would be a qualification. If you were
part of a group that trained national army a lot in other places, that might be a qualification.
Probably some medical for people who are coming out, who are being transferred out of Ukraine,
that might have something to do with it.
So those are the main ones that we can think of.
The reason people are asking about this is because the reporting is pretty sparse.
So Penny Meish, if you just subconsciously said da and you have one of those other qualifications,
I would get your stuff together.
On top of that, there may be just normal rotations of units, of maybe entire reserve units heading
over just for a period and then to rotate back.
possible, but it seems as though this might be a little bit more targeted to
specific roles. But based on everything that is seen, everything that that's been
reported, what's actually in the order, this doesn't look like it's something
gearing up for combat. This is basically filling in in an operation that's been
going on for almost a decade, so no reason to panic, really.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}