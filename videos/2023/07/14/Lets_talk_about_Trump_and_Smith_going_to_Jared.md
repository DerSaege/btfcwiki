---
title: Let's talk about Trump and Smith going to Jared....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NggDXyc0x-M) |
| Published | 2023/07/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Federal grand jury is investigating election interference efforts to overturn the election.
- Three new people have been reportedly interviewed, including Alyssa Griffin, Hope Hicks, and Kushner.
- Special counsel's office and the individuals' attorneys have declined to comment.
- The focus of the investigation is on Trump's state of mind regarding the election results.
- Intent is critical in many charges; knowing the truth versus spreading falsehoods is a significant distinction.
- Cooperation in investigations can lead to more information from others in the inner circle.
- If someone like Giuliani cooperates, others may become more forthcoming to avoid legal troubles.
- The investigation is closing in on Trump, with individuals who had daily contact with him being interviewed.
- The process of the investigation is accelerating.
- The developments hint at potential shifts within Trump's inner circle.

### Quotes

- "Federal grand jury investigating election interference efforts to overturn the election."
- "Cooperation in investigations can lead to more information from others in the inner circle."

### Oneliner

Federal grand jury investigates election interference efforts and potential shifts within Trump's inner circle, hinting at accelerating processes.

### Audience

Political analysts, investigators, concerned citizens.

### On-the-ground actions from transcript

- Contact local representatives to advocate for transparency and accountability in investigations (implied).
- Stay informed about updates on the investigation and its implications in order to take informed action (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the ongoing investigation into election interference and its potential implications on Trump's inner circle.

### Tags

#Investigation #Trump #ElectionInterference #SpecialCounsel #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some new developments
in one of the Trump entanglements
and some of his former assistants,
those people who used to work closely with him
when he was in the White House,
and some reported developments.
Okay, so it appears
that the federal grand jury, looking into the election interference, so this is not the
documents case, this is the other one, looking into the efforts to overturn the election,
they have talked to three people, according to reporting, three new people. One of which is
Alyssa Griffin, who was the White House Communications Director, if I'm not mistaken, at the time.
Another was Hope Hicks, who was a top aide, and then they also reportedly talked
to Kushner. Kushner, son-in-law and top advisor. Yes, Smith went to Jared. Oh my
God. Now, nobody's commenting. At time of filming, Kushner, Hicks, Griffin, their
attorneys have declined to comment. The special counsel's office has also
declined to comment. Nobody is talking about about this reporting. What happened,
if it happened what was discussed so on and so forth. Realistically what is the
special counsel's office trying to find out? Trump's state of mind. They're trying
to find out what he knew and how he was viewing things. Really what they want to
know is did he know that he lost? Privately was he talking about the fact
that they had lost the election. In a lot of the charges, intent matters. Going out
there and saying a bunch of things that aren't true isn't nearly as big a problem if you
believed they were true. But if he knew he was telling all of these people something
that wasn't true, well, that's a real issue. Now, a lot of people would suggest that those
who were his top advisors would just say that he didn't know and continue that route.
Something that tends to happen is once you have one person who is in a circle who begins
cooperating, or at least appears to be cooperating with an investigation, let's just call it
Lucy Buliani, those who were also in that circle, they remember a lot more because
they don't want to say, I don't know, I didn't do that, or I don't remember that,
if somebody else is already providing information. If Giuliani has actually
flipped, the odds are that those people who are within that inner circle, they're
going to be a lot more forthcoming because they don't want to end up in a
situation in which they weren't culpable for Trump's actions. They didn't have
anything to do with it, but then they lied before grand jury and end up in
trouble. So there's a pretty good chance that their attorneys at this point are
saying, yeah, you just need to tell them, especially if they didn't actually have
any involvement with the efforts to overturn the election. So this shows that
Smith is moving into that inner ring. It's ever smaller constricting
circles getting towards Trump. That's how these investigations are run and they
are now, they're now talking to the people who had daily contact with the
former president. So that process is definitely gaining speed. Anyway, it's
It's just a thought.
I'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}