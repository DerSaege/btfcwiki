---
title: Let's talk about Smith, Trump, and trial dates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KTCwHq6o5ow) |
| Published | 2023/07/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's team asked to indefinitely delay the trial, but the special counsel's office rejected this request and wants a trial date set.
- The special counsel's office criticized Trump's position as frivolous and lacking any basis in law or fact.
- Trump's legal team cited the extensive discovery process as the reason for needing more time, given the large amount of evidence to go through.
- The special counsel's office pointed out that many pages of evidence are irrelevant headers or footers, not requiring real scrutiny.
- The judge in the case faces scrutiny and must carefully weigh the decision regarding setting a trial date.
- Despite a proposed trial date of December 11th, further delays are possible.
- Some commentators believe Trump's team aims for a favorable signal from the judge rather than a complete delay.
- Smith, representing the special counsel's office, seeks a speedy resolution to the case.
- Concerns about potential favoritism towards Trump in the ruling should be considered in the context of other ongoing investigations.
- The main takeaway is that the special counsel's office wants to proceed to trial promptly.

### Quotes

- "No. Absolutely not. That's not how we do things."
- "It's worth remembering that his position and his campaign really shouldn't impact the way the case proceeds."
- "Smith wants to go to trial."
- "There's a lot of people getting their blood pressure up over this and I don't know that it's necessary."
- "It's just a thought, y'all have a good day."

### Oneliner

Trump's team seeks trial delay, special counsel refuses, aiming for prompt trial, despite concerns of favoritism.

### Audience

Legal observers

### On-the-ground actions from transcript

- Wait for the judge's decision (implied)
- Monitor future developments in the case (implied)

### Whats missing in summary

Insight into the potential implications of the judge's ruling and how it may shape future legal strategies.

### Tags

#Trump #Smith #SpecialCounsel #TrialDelay #LegalProceedings


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and Smith and delays and the documents case
and what's going on and what the positions are and just kind of run through it.
If you have missed the developments, the Trump team basically filed asking for the judge to delay the trial
indefinitely.
basically asking not to set a trial date. Smith, the special counsel's office, has
responded. No. I mean, to sum up the position, no. Absolutely not. That's not
how we do things. I mean, obviously that's not how it was phrased, but the
general tone of it was nay-nay, I say. Smith wants a trial date or I guess jury
selection December 11th, I believe. The filing referred to Trump's position
as something that borders on the frivolous. Another key phrase was saying
it had no basis in law or fact. Generally, they don't want to delay indefinitely.
It doesn't seem like they want to delay at all. Now, the Trump legal team put
forth the position that because the discovery was so much, because the
special counsel's office had so much evidence that it was gonna take them a
time to go through it. I mean, if I was, you know, on the Trump team and trying to
win this in the court of public opinion, I'm not sure that saying the special
counsel's office just has a load of evidence is necessarily the best idea.
but realistically they do have a lot of evidence that is paperwork and it does
take time to go through. I mean, fair enough. Just given Trump's habit of
wanting to state things publicly, that argument, true or not, may come back to
Now, the special counsel's office has pointed out that while there's a lot of pages, a large portion of the pages aren't
actually evidence, they don't actually matter.
They're like headers or footers
for emails, cover pages, things that don't actually require any real review, so using that isn't a good metric.  The
idea of not moving forward, not setting a trial at all, is definitely an unusual one.
This is in many ways something that the judge in this case, who is under a lot of scrutiny,
really has to weigh carefully. Now even if they get the December 11th date there
could still be other delays that push it back. The request to simply not set a
trial date, I would suggest that there is a large portion of commentators who
believe that that's not really what they want. What they're wanting is for the
judge to signal, hey I'm still going to be kind of favorable to you because I
like you. This ruling will probably give us a pretty good barometer of how she
is going to respond to a legal strategy from the Trump team that certainly
appears to be shaping up to be delay delay delay delay delay. It's worth
remembering that his position and his campaign really shouldn't impact the way
the case proceeds. So we're gonna have to wait and see what the judge decides
but at the end of this it does the one clear takeaway is that Smith wants to
to trial. That seems pretty apparent that the special counsel's office is
looking for a speedy resolution to this. For those who are overly concerned with
how the judge will rule and whether the judge might, you know, just show a whole
bunch of favoritism to Trump and all of that, just remember there are other cases
that are being investigated. There's a lot of people getting their blood
pressure up over this and I don't know that it's necessary.
So, anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}