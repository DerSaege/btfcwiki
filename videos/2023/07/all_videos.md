# All videos from July, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-07-30: The roads to politics and sci-fi Q&A.... (<a href="https://youtube.com/watch?v=MN2zVoquIfY">watch</a> || <a href="/videos/2023/07/30/The_roads_to_politics_and_sci-fi_Q_A">transcript &amp; editable summary</a>)

Beau delves into various topics, from frozen roundworms to Trump's presidency, providing insights on current events and community networks.

</summary>

"I think that a monument to Till would do better at reminding people of the history rather than the mythology."
"Stuff on YouTube is edited. When you're talking about anything that is permanent, you have to actually try this stuff in a more controlled setting."
"If he can't resolve this problem before he finishes his time in office, his legacy is going to be that of the Senate majority leader who enabled Trump."

### AI summary (High error rate! Edit errors on video page)

Beau is doing another Rhodes Q&A, which will now likely occur weekly due to the influx of questions.
Questions are being organized thematically, with a mix of political and science fiction-related queries.
Topics discussed include frozen roundworms coming back to life, Twitter's rebranding, the idea of a monument to Emmett Till, and the parallels between Star Trek episodes and current events.
Beau shares insights on Ukraine's troop strength, Twitter rebranding, and the potential for a general strike.
Beau also addresses the ICC's limitations in arresting political figures like George W. Bush or Dick Cheney.
Beau expresses surprise at Trump's 2016 win, discussing voter turnout and the impact of negative sentiments towards Hillary Clinton.
Beau touches on the challenges of off-grid living in Colorado based on a tragic incident involving a group's failed attempt.
Beau shares thoughts on Mitch McConnell's handling of Trump within the Republican Party and Hulu's staggered release of Futurama episodes.

Actions:

for viewers interested in diverse perspectives on current events and community engagement.,
Research and support local community networks (suggested)
Advocate for accurate and comprehensive survival education (implied)
Stay informed about political figures' actions and their implications (implied)
</details>
<details>
<summary>
2023-07-23: The roads to a July Q&A.... (<a href="https://youtube.com/watch?v=Vqx84OlqG0M">watch</a> || <a href="/videos/2023/07/23/The_roads_to_a_July_Q_A">transcript &amp; editable summary</a>)

Beau addresses various questions on politicians in videos, harm reduction specialists, country songs, military rumors, architecture, gardening exchanges, and NATO's actions in Ukraine, concluding with insights on an audio glitch in recordings.

</summary>

"Sometimes a glitch is just a glitch."
"It's something that is actually happening as it's being recorded."
"Even though their job, I want to say they're like a tenant advocate or something like that."

### AI summary (High error rate! Edit errors on video page)

Explains why politicians in promotional videos walking through Congress halls aren't actually working; compares it to an old filmmaker's trick to create the illusion of action.
Addresses the question of whether a leftist perspective can work as a harm reduction specialist in a position paid for by law enforcement, touching on the online left's views.
Talks about the controversies surrounding a country song by Jason Aldean and criticizes it for not accurately reflecting small towns.
Delves into rumors surrounding the high military budget for Viagra, mentioning off-label use and cash payments to local officials in conflict zones.
Responds to a question about architecture and authoritarians, hinting at the architectural changes post-1950s in Europe.
Mentions the possibility of setting up a seed tree cutting exchange parallel to gardening videos but notes the legal implications.
Clarifies the meaning behind the phrase "quite a rack" in military context and addresses a question regarding his comments about women.
Explains why NATO wouldn't swiftly destroy Russia's forces in Ukraine, pointing out the complications and risks involved.
Acknowledges an audio glitch in some videos, clarifying that it's not intentional but a physical issue in the recording process.

Actions:

for content creators,
Set up a seed tree cutting exchange to parallel gardening videos (suggested)
Investigate and address any audio glitches in recordings (implied)
</details>
<details>
<summary>
2023-07-20: The roads to Trump's legal entanglements and a Q&A.... (<a href="https://youtube.com/watch?v=EdIazr9Xauo">watch</a> || <a href="/videos/2023/07/20/The_roads_to_Trump_s_legal_entanglements_and_a_Q_A">transcript &amp; editable summary</a>)

Beau provides a detailed update on Trump's legal challenges, addressing various cases and potential implications, while humorously hinting at future video topics beyond the Trump era.

</summary>

"I know there are people worried about the Florida case [...] but right now, it does look like the Feds are gonna indict him again."
"There's a whole bunch of other people who are flirting with Trump's style of leadership, that brand of authoritarianism."
"His ability to do that is why he's kind of the GOP's only hope at this point."
"I'd probably feel a lot better if I didn't have to talk about him all the time."
"Y'all have a good day. Thank you."

### AI summary (High error rate! Edit errors on video page)

Provides a deep dive and update into Trump's legal entanglements, discussing various cases and answering questions related to them.
Talks about the election interference case, where Trump received a target letter and an indictment is expected relatively quickly.
Mentions the New York criminal case, where Trump's attempt to move it to federal court was denied.
Addresses the Eging-Carroll case, where Trump lost an attempt to get a new trial.
Talks about the Documents case, where Trump is already indicted, and his team is fighting over a trial date.
Mentions the Georgia case, where an indictment is expected, raising questions about the breadth of charges.
Indicates ongoing investigations by the feds and states into financial irregularities involving Trump.
Mentions potential legal exposure in states investigating fake elector activities.
Talks about the possibility of Trump facing indictment from the Feds with regards to the Florida case.
Addresses the Secret Service's potential actions regarding Trump's legal situation, including scenarios like home confinement.
Comments on Rudy Giuliani's lawyer's statement about not flipping and the potential evidence he holds.
Talks about the Senate's addition of NATO-related measures to the NDAA and its implications.
Mentions Georgia Governor Kemp's role as the GOP's hope in re-centering the party.
Speculates on potential outcomes for Trump, including home confinement and implications for others like him.
Comments on Marjorie Taylor Greene's stance on accountability for crimes and its potential application to Trump.
Humorously mentions future video topics post-Trump era, like gardening and relief vehicle projects.

Actions:

for political enthusiasts, legal observers,
Reach out to local organizations supporting accountability and transparency in government investigations (implied)
</details>
<details>
<summary>
2023-07-19: Let's talk about Trump's letter and what it means.... (<a href="https://youtube.com/watch?v=VbCG2vOqeUY">watch</a> || <a href="/videos/2023/07/19/Lets_talk_about_Trump_s_letter_and_what_it_means">transcript &amp; editable summary</a>)

Trump receives target letter from Department of Justice, facing potential indictment for insurrection, involving multiple serious charges.

</summary>

"Means he's on the verge of another indictment."
"Do I expect that? No. The politicized nature of that word."
"By the time this is all said and done, he might have quite a rack up there."

### AI summary (High error rate! Edit errors on video page)

Trump has indicated he received a letter from the Department of Justice stating he is a target of an investigation and has four days to report to the grand jury.
The grand jury looking into election interference post-2020 is concluding, indicating a potential indictment for Trump.
Legal minds have suggested various charges Trump could face, including conspiracy to commit sedition, obstruction, and insurrection.
Beau believes Trump will likely be charged with insurrection due to the expansive alleged conduct.
There may be multiple serious charges against Trump, involving other individuals as well.
The use of the word "treason" in the U.S. is specific and unlikely to be the route prosecutors take.
Prosecutors could potentially make a case for insurrection based on Trump's slip between the Espionage Act and the Insurrection Act.
Beau anticipates that the indictment against Trump will not be a single charge and may involve more people.
The grand jury's recent activities suggest that news of a potential indictment for Trump in Georgia and DC may be forthcoming.
Trump has referred to his potential indictment as a "badge of honor," hinting at possible future legal troubles.

Actions:

for legal analysts,
Stay updated on legal developments regarding Trump's potential indictment (suggested)
Support efforts to uphold justice and accountability at all levels (implied)
</details>
<details>
<summary>
2023-07-19: Let's talk about Michigan electors and what's next.... (<a href="https://youtube.com/watch?v=h3s9i0PuWO0">watch</a> || <a href="/videos/2023/07/19/Lets_talk_about_Michigan_electors_and_what_s_next">transcript &amp; editable summary</a>)

Michigan charges "fake electors," expecting more developments in Georgia and DC, with potential cooperation to reduce sentences.

</summary>

"Charges include felonies like conspiracy to commit forgery, forgery, and election law forgery."
"Cooperation with government entities may be a strategy for reducing potential sentences."
"I think it's finally going to start kind of snowballing."

### AI summary (High error rate! Edit errors on video page)

Michigan's Attorney General has charged 16 individuals as "fake electors," marking the first time such charges have been brought anywhere.
Charges include felonies like conspiracy to commit forgery, forgery, and election law forgery.
The accused could face a maximum of 14 years, but actual sentencing outcomes are uncertain.
Expectations for indictments in Georgia and DC are building up, with signs of investigations in other states as well.
The possibility of more charges and defendants emerging is high due to ongoing investigations across different locations.
Cooperation with government entities may be a strategy for reducing potential sentences.
Anticipates a period of consistent news and developments that many have been waiting for.

Actions:

for legal watchers, political analysts.,
Contact local authorities to stay informed about developments in similar cases (suggested).
Support efforts to uphold election integrity through community engagement (exemplified).
</details>
<details>
<summary>
2023-07-18: Let's talk about two minutes on Fox.... (<a href="https://youtube.com/watch?v=mLTEcND18Sw">watch</a> || <a href="/videos/2023/07/18/Lets_talk_about_two_minutes_on_Fox">transcript &amp; editable summary</a>)

Jesse Waters' mother's candid advice on Fox News sets a surprising tone, urging kindness and steering away from conspiracy theories and political bashing.

</summary>

"How much foreknowledge did Fox have about the topics being discussed?"
"Jesse's mother advises him to avoid conspiracy theories, do no harm, be kind, use his voice responsibly, stop Biden-bashing, and let Trump go back to TV."

### AI summary (High error rate! Edit errors on video page)

Jesse Waters takes over Tucker Carlson's time slot on Fox News, and on his first show, his mother calls in live.
Initially, the call starts with a proud mother congratulating her son, but it takes a surprising turn.
Jesse's mother advises him to avoid conspiracy theories, do no harm, be kind, use his voice responsibly, stop Biden-bashing, and let Trump go back to TV.
The advice seems like a subtle dig at other Fox News hosts.
Jesse's mother's unexpected and candid advice makes the moment glorious and entertaining.
Despite the unexpected turn, Jesse tries to maintain his composure during the call.
Beau finds the interaction humorous and chuckles at the exchange.
Beau questions how much foreknowledge Fox had about the topics discussed during the call.
The call may indicate a shift in tone for the time slot, potentially signaling a change in Fox's approach.
Beau hints at the possibility of Fox orchestrating the call to set a specific tone for the show.

Actions:

for media consumers,
Watch the clip of Jesse Waters' mother's call (implied)
</details>
<details>
<summary>
2023-07-18: Let's talk about starting at the bottom electorally.... (<a href="https://youtube.com/watch?v=1S66zqiiJsI">watch</a> || <a href="/videos/2023/07/18/Lets_talk_about_starting_at_the_bottom_electorally">transcript &amp; editable summary</a>)

Beau explains the importance of starting at the bottom in electoral politics to create real change and build a power structure outside traditional parties.

</summary>

"Starting at the bottom is key to creating real progress in the political system."
"You have to build your own power structure."
"A dream without a plan, it's never going to happen."
"Building a power structure outside of a political party [...] That's the way forward."
"It requires a lot more work."

### AI summary (High error rate! Edit errors on video page)

Addressing electoral politics and the need for work to achieve change.
Starting at the bottom is key to creating real progress in the political system.
Progressives moving up through the Democratic Party can lead to a more progressive establishment.
Emphasizing the importance of starting at the bottom rather than jumping straight to the top.
The power lies in Congress, making Senate and House positions vital for progressive change.
Advocating for a strategic approach to implementing change through political careers at local and state levels.
Acknowledging the need for reforms in the US electoral system.
Stating that building one's power structure is necessary to combat existing political machines.
Explaining the significance of building a power structure outside of traditional political parties.
Stressing the importance of more than just voting for a third-party candidate to bring about systemic change.

Actions:

for activists, political organizers,
Join organizations working towards systemic change (implied)
Build a power structure outside traditional political parties (implied)
</details>
<details>
<summary>
2023-07-18: Let's talk about Trump's Georgia Supreme Court petition.... (<a href="https://youtube.com/watch?v=a5aZtuQU8Es">watch</a> || <a href="/videos/2023/07/18/Lets_talk_about_Trump_s_Georgia_Supreme_Court_petition">transcript &amp; editable summary</a>)

Trump's failed petition in Georgia to halt an investigation carries implications for future legal proceedings and potential indictments, despite unanimous dismissal by the Supreme Court.

</summary>

"Trump filed a petition in Georgia to shut down an investigation."
"The petition was dismissed unanimously by all nine Georgia Supreme Court justices."
"The ruling against Trump's petition is seen as a win for the prosecution."

### AI summary (High error rate! Edit errors on video page)

Trump filed a petition in Georgia to shut down an investigation, aiming to prevent evidence use in future criminal or civil proceedings.
The petition was dismissed unanimously by all nine Georgia Supreme Court justices, despite eight being Republican appointees.
The intervention Trump requested required extraordinary circumstances absent in this case.
The grand jury in Georgia, capable of indicting the former president, has been sworn in.
Speculation arises about potential new indictments related to a controversial phone call.
The ruling against Trump's petition is seen as a win for the prosecution, removing a possible roadblock.
The outcome of this case is expected to gain more attention in the news as time progresses.
Trump's chances of success in this petition were low, and the ruling further signals challenges for Team Trump in Georgia.
Despite expectations, the dismissal of Trump's petition is significant in the legal proceedings.

Actions:

for legal observers, political analysts,
Monitor news updates on the legal proceedings in Georgia (suggested)
Stay informed about the developments surrounding the former president's case (suggested)
</details>
<details>
<summary>
2023-07-18: Let's talk about MTG, McCarthy, and plays.... (<a href="https://youtube.com/watch?v=At2xtpajUII">watch</a> || <a href="/videos/2023/07/18/Lets_talk_about_MTG_McCarthy_and_plays">transcript &amp; editable summary</a>)

Marjorie Taylor Greene's rewards and role in the NDAA committee signal broader political maneuvers within the Republican Party to sideline the far right and regain control.

</summary>

"There's probably a lot of truth to that, but I think there's more to it."
"It certainly appears like McCarthy is bringing her under his wing and at the same time increasing his own power."
"It seems like they're trying to get their party back."

### AI summary (High error rate! Edit errors on video page)

Marjorie Taylor Greene's rewards and participation in the conference committee for the NDAA are discussed.
Greene was expelled from the far-right Freedom Caucus, and McCarthy's reward reduces their influence.
McCarthy's move to include Greene in the committee is seen as a way to control her and boost his power.
The shift appears to be part of a broader effort in the Republican Party to sideline the far right and regain control.
The outcome of these political maneuvers remains to be seen.

Actions:

for political observers,
Monitor political developments and shifts within the Republican Party (implied).
Stay informed about the actions and motivations of political figures (implied).
</details>
<details>
<summary>
2023-07-17: Let's talk about bad headlines, Russia, and Ukraine.... (<a href="https://youtube.com/watch?v=gaA5eLVHrq8">watch</a> || <a href="/videos/2023/07/17/Lets_talk_about_bad_headlines_Russia_and_Ukraine">transcript &amp; editable summary</a>)

Be cautious of misleading headlines quoting out of context, as they distort the truth and hinder accurate information consumption.

</summary>

"When you see a quote as a headline, just assume it's not true."
"This is just one that they're happening to do it with right now."
"But man, that's a good headline, isn't it?"
"Assume it's completely inaccurate because there is no fact checking."
"There's a clear example of it happening and a clear example of how not just is it slightly misleading, it's entirely inaccurate."

### AI summary (High error rate! Edit errors on video page)

Explains how bad information spreads using a specific tactic of quoting inaccurately in headlines.
Uses the example of Putin's statement on cluster munitions to illustrate the issue.
Points out misleading headlines that suggest Russia will use cluster bombs in Ukraine, when they have already been extensively used by Russia.
Mentions the challenges in finding unbiased sources and invites the audience to find a single independent organization denying Russia's use of cluster munitions.
Emphasizes that Russia does not deny using cluster munitions but only denies hitting civilian infrastructure.
Criticizes the misleading narrative formed by inaccurate headlines, urging viewers to be cautious of such reporting.
Advises viewers to assume quotes as headlines are often inaccurate and used for emotional impact rather than truth.
Warns that such practices hinder accurate information consumption and encourages vigilance in spotting misleading headlines.
Notes how emotional reactions generated by false quotes can interfere with understanding news accurately.
Concludes by pointing out the prevalence of this misleading tactic in news reporting and the importance of being aware of it.

Actions:

for information consumers,
Verify sources independently before forming opinions (implied)
Challenge misleading narratives by seeking out reliable information (implied)
Share awareness about the impact of inaccurate headlines on information consumption (implied)
</details>
<details>
<summary>
2023-07-17: Let's talk about Ukraine, bridges, and Russia.... (<a href="https://youtube.com/watch?v=WGHdZ1rl-ns">watch</a> || <a href="/videos/2023/07/17/Lets_talk_about_Ukraine_bridges_and_Russia">transcript &amp; editable summary</a>)

Ukraine's strategic moves in targeting a bridge may disrupt Russian troops in Crimea, creating uncertainty and fear among them.

</summary>

"Leaving an avenue of retreat for the enemy is a common strategy in real life to avoid unnecessary conflict."
"Ukraine's moves, including targeting the bridge, are not just tactical but also psychological to keep Russian forces off balance."
"The uncertainty and doubt created among Russian troops may lead them to question their situation and future actions in Crimea."

### AI summary (High error rate! Edit errors on video page)

Ukraine may be intentionally trying to break a rule by targeting a bridge, potentially cutting off Russian troops in Crimea.
Leaving an avenue of retreat for the enemy is a common strategy in real life to avoid unnecessary conflict.
The recent incident involving the bridge has disrupted traffic and sent a clear message that Ukraine wants that avenue of retreat cut off.
Russian troops in Crimea are now facing uncertainty and fear due to disrupted supplies, poor morale, and disarray in command.
Putin's removal of commanders has caused discontent and shaken the resolve of Russian troops.
Ukrainian actions may be aimed at taking advantage of the current vulnerabilities in the Russian military presence in Crimea.
Ukraine's moves, including targeting the bridge, are not just tactical but also psychological to keep Russian forces off balance.
The bridge serves as a symbol of Russian imperialism, adding a symbolic significance to its targeting by Ukraine.
Ukraine's successes so far have put pressure on the Russian troops, who are not in the best morale space.
The uncertainty and doubt created among Russian troops may lead them to question their situation and future actions in Crimea.

Actions:

for military analysts,
Support Ukrainian efforts to disrupt Russian military operations (exemplified)
Stay informed and advocate for diplomatic solutions to the conflict (exemplified)
</details>
<details>
<summary>
2023-07-17: Let's talk about Trump, Kuschner, and Smith.... (<a href="https://youtube.com/watch?v=T6M_sB6W-fk">watch</a> || <a href="/videos/2023/07/17/Lets_talk_about_Trump_Kuschner_and_Smith">transcript &amp; editable summary</a>)

Exploring Kushner's testimony on Trump's belief in election win amidst contradicting statements, the case against Trump remains strong with active cooperation from multiple individuals.

</summary>

"It's worth remembering he was planting the seeds before the election."
"There are clear indications that there are multiple people who are actively cooperating."
"Smith, the special counsel, is pretty far ahead of where legal analysts even think he is."

### AI summary (High error rate! Edit errors on video page)

Exploring Kushner and Trump's connection in relation to election results.
Kushner reportedly testified that Trump genuinely believed he won the election.
Despite this testimony, there are contradicting statements from others like the comms director, Griffin, and Milley.
Bannon was recorded discussing similar sentiments along with other evidence.
The case against Trump won't likely crumble due to conflicting testimonies.
Prior to the election, seeds were planted to support claims of election interference.
Special counsel's office is focusing on obstruction and willful retention in addition to documents.
Defending Trump on matters where there is ample contradictory evidence may not be effective.
Multiple individuals are actively cooperating regarding election interference.
Smith, the special counsel, is likely prepared for testimonies claiming Trump's ignorance.

Actions:

for legal analysts,
Stay informed and updated on the ongoing legal proceedings surrounding Trump and Kushner (implied)
Support transparency and accountability in legal investigations (implied)
</details>
<details>
<summary>
2023-07-17: Let's talk about Christie vs Trump.... (<a href="https://youtube.com/watch?v=IEnm3vi7H2k">watch</a> || <a href="/videos/2023/07/17/Lets_talk_about_Christie_vs_Trump">transcript &amp; editable summary</a>)

Beau analyzes the brewing conflict between Chris Christie and Trump, with Christie seemingly aiming to damage Trump's image rather than win, potentially impacting the Republican primary dynamics.

</summary>

"They don't care about policy. They really don't. They don't care about policy. They don't care about principle. They don't care about party."
"Trump is doing so well because he can own the libs. He can embarrass people."
"Christie, whether or not he does when the time comes, he certainly has the potential to embarrass Trump and hurt that facade that he has put up."
"The more I think about it, the more I think it might be possible, but it'll be a complete accident."
"It depends on how hard Christie swings at Trump."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the unfolding rivalry between Chris Christie and Trump, hinting at potential consequences.
Christie appears to be running in the Republican primary not to win but to make Trump lose or damage him.
Recent actions by Christie include critiquing Trump's foreign policy record, which many Republicans mistakenly view positively.
Christie called out Trump's conduct related to classified national secrets, contrasting Trump's portrayal of himself as a savior.
The impact of Christie's criticisms is expected to resonate more on the debate stage than in early sparring through news soundbites.
Beau notes that Trump's appeal lies in his personality of owning the libs rather than policy, principle, or party allegiance.
Christie's strategy of attacking Trump recklessly could potentially benefit him or at least harm Trump during the debate.
Christie has met the donor threshold to join the debate stage with Trump on August 23rd.
Trump's reluctance to face Christie on stage may stem from the fear of being embarrassed, as Christie's aggressive approach could undermine Trump's facade.
There's a possibility that Christie, despite not aiming to win, might inadvertently succeed if he damages Trump significantly and attracts vindictive voters.

Actions:

for political observers,
Watch the debate between Chris Christie and Trump on August 23rd (implied)
Stay informed about the evolving dynamics in the Republican primary (implied)
</details>
<details>
<summary>
2023-07-16: Let's talk about warming waters in Florida.... (<a href="https://youtube.com/watch?v=rYmRiLpekvk">watch</a> || <a href="/videos/2023/07/16/Lets_talk_about_warming_waters_in_Florida">transcript &amp; editable summary</a>)

South Florida and the East Coast face immediate coral damage from warming waters due to climate change, impacting ecosystems and economies while signaling broader future disasters we must address now.

</summary>

"It's a freight train and it has been barreling along, caution lights flashing and nobody paying attention."
"Rather than the boats taking people to snorkel and look at all the fish, it's going to shift and become eco-disaster tourism."
"It is going to affect us all."

### AI summary (High error rate! Edit errors on video page)

South Florida and the East Coast are experiencing water temperatures in the 90s, leading to coral bleaching.
Human-caused climate change is a major contributor to the warming waters and coral damage.
Coral plays a critical role in the ecosystem by providing food for fish, impacting the entire food chain.
The loss of coral not only devastates the ecosystem but also has economic consequences.
The effects of climate change are not a distant future issue; they are happening right now.
Despite warnings for decades, some continue to deny the impact of climate change and advocate for harmful practices like drilling.
The damage to coral reefs will force boats reliant on tourism to change their profession.
The impacts of climate change are already visible and will continue to worsen over time.
Mitigating climate change now won't prevent all future damage due to the inertia of the process.
Ignoring the signs of climate change is dangerous, as the long-term effects will be severe and irreversible.

Actions:

for climate activists, environmentalists,
Contact local environmental organizations to volunteer for coral reef restoration efforts (suggested)
Support policies and politicians advocating for strong climate change mitigation measures (suggested)
</details>
<details>
<summary>
2023-07-16: Let's talk about electoral options and other parties.... (<a href="https://youtube.com/watch?v=-KX8xddKfeM">watch</a> || <a href="/videos/2023/07/16/Lets_talk_about_electoral_options_and_other_parties">transcript &amp; editable summary</a>)

Beau explains the limitations of third-party presidential candidates, advocating for grassroots power-building outside traditional political structures for meaningful change.

</summary>

"A savior is not coming to fix the United States."
"The real solution is to build your own power structure that isn't linked to a political party."
"You will not usher in some glorious revolution, you will not get deep systemic change by recreating the exact same system that brought us here."
"The system that exists wants to maintain the system that exists because it benefits them."
"You have to start at the bottom and you have to build it up."

### AI summary (High error rate! Edit errors on video page)

Explains the role of third-party candidates in the presidential election process.
Stresses that third-party candidates are mainly about messaging and not actually winning.
Envisions a scenario where a third-party candidate miraculously wins the presidency and poses the question of what happens next.
Analyzes the limitations and challenges a third-party president might face within the existing government structure.
Argues that real change comes from building power structures outside of traditional political parties at the local level.
Advocates for getting involved in the Democratic Party at the grassroots level to push for progressive change.
Points out the success of movements like the Tea Party in influencing party dynamics over time.
Emphasizes the need for long-term, grassroots efforts to bring about meaningful systemic change.
Encourages focusing on community networks to influence electoral outcomes effectively.
Concludes by stressing the importance of building sustainable power structures rather than relying on a presidential candidate to bring about significant change.

Actions:

for activists, political reformers,
Start from local levels: County Commission, City Council, School Board, State Representatives, House of Representatives, Senate (suggested)
Build community networks to influence electoral outcomes (implied)
Get involved in Democratic Party at local levels to push for progressive change (exemplified)
</details>
<details>
<summary>
2023-07-16: Let's talk about Russia, truces, and the Olympics.... (<a href="https://youtube.com/watch?v=EUPNbsD2ZVs">watch</a> || <a href="/videos/2023/07/16/Lets_talk_about_Russia_truces_and_the_Olympics">transcript &amp; editable summary</a>)

The Olympic Committee excludes Russia and Belarus from the games, allowing athletes to compete under a neutral flag, sparking international political consequences.

</summary>

"Ukraine believes Russian and Belarusian athletes shouldn't participate at all, even under a neutral flag."
"The decision not to invite Russian and Belarusian teams is a consequence of their recent actions."
"Expect more international political posturing as we move towards a world that is a little bit more multipolar."

### AI summary (High error rate! Edit errors on video page)

The Olympic Committee decided Russia and Belarus won't be invited to the next year's games in Paris, but athletes can participate under a neutral flag.
Ukraine believes Russian and Belarusian athletes shouldn't participate at all, even under a neutral flag.
A tradition of truce during the Olympics dates back to the 700s BC to ensure the safety of the hosting city and travelers.
The truce was revived in the early 90s, and it has been violated only three times since then.
The tradition of sending out invitations about a year in advance for the Olympics makes it unlikely for changes to occur.
The decision not to invite Russian and Belarusian teams is a consequence of their recent actions, particularly Russia's invasion of Ukraine.
This move signifies international political consequences and is meant as a punishment to Russia and Belarus.
Such actions are reminiscent of Cold War tactics and indicate a shift towards a more multipolar world.
Expect more international political posturing as global power dynamics evolve.
The decision serves as both a statement and a form of punishment.

Actions:

for sports officials,
Contact sports organizations to express support for fair play and sportsmanship (suggested)
Join movements advocating for clean sports and ethical competition (implied)
</details>
<details>
<summary>
2023-07-16: Let's talk about Putin's signals and plans.... (<a href="https://youtube.com/watch?v=jpguIX6Xx4A">watch</a> || <a href="/videos/2023/07/16/Lets_talk_about_Putin_s_signals_and_plans">transcript &amp; editable summary</a>)

Speculation abounds on Putin's plans amid military restructuring, hinting at loyalty shifts and fear of upheaval.

</summary>

"Loyalty, not skill."
"You do realize I can outlaw your company at any moment."
"He's scared, which makes him a little bit more unpredictable."
"He's scared he's going to be deposed."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Speculation surrounds Putin's plans following recent events involving Wagner.
Putin's treatment of individuals hints at a possible political purge within military structures.
Traditional Russian officers are being questioned and relieved of command, signaling a significant realignment.
Loyalty seems to outweigh skill in the selection of replacements, causing concern for Russian troops.
Wagner PMC, according to Putin, does not legally exist in Russia.
Putin's actions suggest attempts to maintain control over Wagner while potentially reorganizing its structure.
Putin's maneuvers indicate a desire to keep Wagner intact for his own viability.
Putin is dealing with these challenges tactfully but may be driven by fear of potential threats to his power.
The uncertainty surrounding Putin's actions stems from his perceived vulnerability and fear of being deposed.

Actions:

for political analysts,
Monitor developments in Russian military structures for potential shifts and implications (implied).
Stay informed about Putin's actions and political maneuvers within the military (implied).
</details>
<details>
<summary>
2023-07-15: The roads to questions and clusters.... (<a href="https://youtube.com/watch?v=u_5lFUeU5OY">watch</a> || <a href="/videos/2023/07/15/The_roads_to_questions_and_clusters">transcript &amp; editable summary</a>)

Beau addresses audience Q&A on various topics, from RC Cola nostalgia to cluster munitions, ending with a poignant reminder on the realities of conflicts today.

</summary>

"I hate your power coupon analogy. It simply reinforces the idea that money makes people less accountable and more capable."
"Why do you mispronounce Wagner now in your first video as you pronounced it correctly? Because I'm being mean and petty."
"When you're talking about conflicts, please remember that to a whole lot of people it's not something that's happening on a screen."

### AI summary (High error rate! Edit errors on video page)

Beau kicks off a Q&A session addressing various questions from his audience, including the most asked one about where to find RC Cola.
He clarifies the misconception about expired MREs in the military, explaining that the dates on MRE boxes are inspection dates, not expiration dates.
Beau shares a humorous encounter with a deer on his ranch, which unexpectedly ends with him getting kicked by Bambi.
He talks about addressing creators' mistakes and the importance of assuming that mistakes are unintentional and can be corrected by their audience.
Beau tackles questions about capitalism, public schools, and cluster munitions, providing insightful responses to each.
In the context of cluster munitions, Beau explains their use and the ongoing conflict in Ukraine.
He ends the Q&A by reflecting on the reality of conflicts and urges empathy towards those directly affected.

Actions:

for creators, viewers,
Contact local organizations supporting victims of conflicts (implied)
Start a fundraiser to aid in conflict relief efforts (implied)
</details>
<details>
<summary>
2023-07-15: Let's talk about what the White House can learn from the commentators.... (<a href="https://youtube.com/watch?v=ih8ekOadTRw">watch</a> || <a href="/videos/2023/07/15/Lets_talk_about_what_the_White_House_can_learn_from_the_commentators">transcript &amp; editable summary</a>)

Beau advises the White House to combat misinformation by providing context in press releases and warns against manipulation due to intentional misinformation.

</summary>

"Don't let them manipulate you because there's an information vacuum."
"They lied to you. They did it on purpose."
"The White House needs to tailor their press releases to cut off conspiratorial ideas before they start."

### AI summary (High error rate! Edit errors on video page)

Analyzes recent rumors and advises on what the White House can learn regarding public relations.
Addresses the misinformation surrounding the calling up of 3,000 reservists by the Biden administration.
Points out the lack of critical information in the initial press release that led to false assumptions of combat involvement.
Urges for better communication strategies to combat intentional disinformation.
Emphasizes the importance of including context and additional information in press releases to prevent misinterpretation.
Criticizes military commentators who spread fear-mongering misinformation knowingly.
Encourages the public to be vigilant against manipulation due to intentional misinformation.
Calls for improved press release strategies to dispel conspiracies and provide clear context.

Actions:

for public, white house staff,
Tailor press releases to include context and prevent misinterpretation (suggested)
Be vigilant against manipulation through intentional misinformation (implied)
</details>
<details>
<summary>
2023-07-15: Let's talk about the House GOP and the defense bill.... (<a href="https://youtube.com/watch?v=4IryL6kD3jE">watch</a> || <a href="/videos/2023/07/15/Lets_talk_about_the_House_GOP_and_the_defense_bill">transcript &amp; editable summary</a>)

House Republicans stir controversy with amendments to the NDAA, facing Senate scrutiny and potential repercussions, risking alienation of supportive voters.

</summary>

"Republicans attached a bunch of amendments to it, a bunch of messaging bills."
"At this point, I have a feeling that many Republican voters are just kind of wanting them to be normal."
"It's still something you have to watch because it is a must-pass bill and weird things happen."

### AI summary (High error rate! Edit errors on video page)

House Republicans stirred controversy by attaching amendments to the NDAA, a must-pass defense bill with bipartisan support.
McCarthy defended the amendments, suggesting Republicans keep their promises, despite facing pushback.
The bill now moves to the Senate, where Democratic senators are unlikely to approve the controversial amendments.
If rejected, the Senate will send back a different version of the bill, leading to further negotiations.
Politically, parties adding contentious amendments to the NDAA often face repercussions later on.
This move by Republicans risks alienating a bloc of voters traditionally supportive of them.
McCarthy's struggle to rein in the far-right elements within the party is evident, influencing the bill's amendments.
Despite potential commentary, it is expected that the Senate will likely remove all controversial amendments.
While the situation warrants attention due to the bill's significance, it may resolve itself without major issues.
Overall, the actions of House Republicans could impact their standing with voters and in future political outcomes.

Actions:

for political observers,
Monitor Senate actions on the NDAA (implied)
Stay informed on political developments regarding the defense bill (implied)
</details>
<details>
<summary>
2023-07-15: Let's talk about the 10 worst states to live and work.... (<a href="https://youtube.com/watch?v=IazOBiVfCWU">watch</a> || <a href="/videos/2023/07/15/Lets_talk_about_the_10_worst_states_to_live_and_work">transcript &amp; editable summary</a>)

Beau reveals the 10 worst states to work and live, with Texas surprisingly ranking at the bottom due to lacking inclusiveness for workers.

</summary>

"And number one will surprise you."
"Inclusiveness is an important part of whether or not workers want to come to your state."
"There's an interesting thing about it if you really look."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of discussing the 10 worst states to work and live based on empirical data from CNBC.
The ranking grades states based on their welcoming environment for workers and families, indicating potential future business developments.
States like Florida, Arkansas, Tennessee, and Indiana received low grades, with Texas surprisingly ranking as the worst state.
The methodology behind the ranking considers inclusiveness as a key factor in attracting workers to a state.
Beau points out the recent changes in inclusiveness levels in many states, impacting their rankings.
He hints at a common factor among these poorly ranked states that might explain their low scores.

Actions:

for employed individuals seeking welcoming work environments.,
Research and understand the metrics used by CNBC to rank states (suggested).
Advocate for inclusivity and diversity in your workplace and community (implied).
</details>
<details>
<summary>
2023-07-15: Let's talk about DOJ changing its mind about Trump.... (<a href="https://youtube.com/watch?v=Whpxg09LB_4">watch</a> || <a href="/videos/2023/07/15/Lets_talk_about_DOJ_changing_its_mind_about_Trump">transcript &amp; editable summary</a>)

DOJ's change in position may strip Trump of immunity, leading to a January trial in the E. Jean Carroll defamation case and unfavorable legal proceedings.

</summary>

"DOJ changed its position, potentially stripping Trump of immunity in the defamation case."
"Trump may face trial in January for the defamation case."
"Without immunity, Trump may face unfavorable legal proceedings."

### AI summary (High error rate! Edit errors on video page)

The Department of Justice (DOJ) changed its position regarding Trump's immunity under the Westfall Act, which could lead to another legal entanglement for him.
DOJ previously believed Trump's statements about E. Jean Carroll were within the scope of his duties, granting him immunity.
DOJ now claims Trump's statements were not related to his duties, potentially stripping him of immunity in the E. Jean Carroll defamation case.
This change in DOJ's stance could allow the defamation case against Trump to proceed after being on hold.
Trump may face trial in January for the defamation case.
Trump has dismissed this as part of a political witch hunt, despite being found liable in a previous case.
The shift in DOJ's position might be linked to uncovering activities by the former president outside the scope of his duties.
Without immunity, Trump may face unfavorable legal proceedings.
This situation adds to the ongoing legal challenges for Trump post-presidency.

Actions:

for legal analysts, political commentators, activists,
Follow updates on the E. Jean Carroll defamation case and understand its implications (suggested)
Stay informed about legal developments surrounding former President Trump (suggested)
</details>
<details>
<summary>
2023-07-14: Let's talk about investigating the investigators of the investigators.... (<a href="https://youtube.com/watch?v=B2XRPkhJJmY">watch</a> || <a href="/videos/2023/07/14/Lets_talk_about_investigating_the_investigators_of_the_investigators">transcript &amp; editable summary</a>)

Investigating Congress and Barr's actions with skepticism on the outcome, Beau questions the effectiveness of ongoing investigations into Trump.

</summary>

"Investigating the investigators of the investigators."
"I don't think much."
"Not much is going to come of this."
"Generally speaking, these types of investigations don't really uncover what people hope to uncover."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Investigating the investigators of the investigators, referencing the U.S. Congress and Attorney General Barr.
Democrats on the House Judiciary Committee are seeking information from Garland due to concerning evidence.
Barr allegedly used special counsel regulations to bury a criminal investigation rather than promoting independence and fairness.
Mr. Durham's decision not to charge former President Trump after investigating alleged financial crimes raises concerns.
The Durham investigation uncovered information about Trump but did not lead to any charges.
Beau expresses skepticism about significant outcomes from these types of investigations historically.
Investigations launched by Congress often do not result in criminal charges or significant changes.
Despite low expectations, Beau acknowledges the unprecedented nature of the information uncovered about a former president.
Beau anticipates minimal impact from the ongoing investigation despite various allegations and public interest.
Generally, investigations like these may not reveal what people hope to find.

Actions:

for congressional watchdogs,
Monitor and advocate for transparency in investigations (suggested).
Stay informed and engaged with developments in ongoing investigations (suggested).
</details>
<details>
<summary>
2023-07-14: Let's talk about Trump and Smith going to Jared.... (<a href="https://youtube.com/watch?v=NggDXyc0x-M">watch</a> || <a href="/videos/2023/07/14/Lets_talk_about_Trump_and_Smith_going_to_Jared">transcript &amp; editable summary</a>)

Federal grand jury investigates election interference efforts and potential shifts within Trump's inner circle, hinting at accelerating processes.

</summary>

"Federal grand jury investigating election interference efforts to overturn the election."
"Cooperation in investigations can lead to more information from others in the inner circle."

### AI summary (High error rate! Edit errors on video page)

Federal grand jury is investigating election interference efforts to overturn the election.
Three new people have been reportedly interviewed, including Alyssa Griffin, Hope Hicks, and Kushner.
Special counsel's office and the individuals' attorneys have declined to comment.
The focus of the investigation is on Trump's state of mind regarding the election results.
Intent is critical in many charges; knowing the truth versus spreading falsehoods is a significant distinction.
Cooperation in investigations can lead to more information from others in the inner circle.
If someone like Giuliani cooperates, others may become more forthcoming to avoid legal troubles.
The investigation is closing in on Trump, with individuals who had daily contact with him being interviewed.
The process of the investigation is accelerating.
The developments hint at potential shifts within Trump's inner circle.

Actions:

for political analysts, investigators, concerned citizens.,
Contact local representatives to advocate for transparency and accountability in investigations (implied).
Stay informed about updates on the investigation and its implications in order to take informed action (implied).
</details>
<details>
<summary>
2023-07-14: Let's talk about Smith, Trump, and trial dates.... (<a href="https://youtube.com/watch?v=KTCwHq6o5ow">watch</a> || <a href="/videos/2023/07/14/Lets_talk_about_Smith_Trump_and_trial_dates">transcript &amp; editable summary</a>)

Trump's team seeks trial delay, special counsel refuses, aiming for prompt trial, despite concerns of favoritism.

</summary>

"No. Absolutely not. That's not how we do things."
"It's worth remembering that his position and his campaign really shouldn't impact the way the case proceeds."
"Smith wants to go to trial."
"There's a lot of people getting their blood pressure up over this and I don't know that it's necessary."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump's team asked to indefinitely delay the trial, but the special counsel's office rejected this request and wants a trial date set.
The special counsel's office criticized Trump's position as frivolous and lacking any basis in law or fact.
Trump's legal team cited the extensive discovery process as the reason for needing more time, given the large amount of evidence to go through.
The special counsel's office pointed out that many pages of evidence are irrelevant headers or footers, not requiring real scrutiny.
The judge in the case faces scrutiny and must carefully weigh the decision regarding setting a trial date.
Despite a proposed trial date of December 11th, further delays are possible.
Some commentators believe Trump's team aims for a favorable signal from the judge rather than a complete delay.
Smith, representing the special counsel's office, seeks a speedy resolution to the case.
Concerns about potential favoritism towards Trump in the ruling should be considered in the context of other ongoing investigations.
The main takeaway is that the special counsel's office wants to proceed to trial promptly.

Actions:

for legal observers,
Wait for the judge's decision (implied)
Monitor future developments in the case (implied)
</details>
<details>
<summary>
2023-07-14: Let's talk about Biden, reserve troops, and what's happening.... (<a href="https://youtube.com/watch?v=8MOuODkgxgo">watch</a> || <a href="/videos/2023/07/14/Lets_talk_about_Biden_reserve_troops_and_what_s_happening">transcript &amp; editable summary</a>)

Biden authorizes reservists for Operation Atlantic Resolve in Ukraine, focusing on support roles, not combat; sparse reporting causes uncertainty but no need to panic.

</summary>

"This isn't new, it's just allowing a different pool of troops to be tapped to go over."
"The reason people are asking about this is because the reporting is pretty sparse."
"No reason to panic, really."

### AI summary (High error rate! Edit errors on video page)

Biden administration calling up to 3,000 reservists and 450 IRR for Operation Atlantic Resolve, which has been ongoing since 2014.
Not an escalation or troop increase but freeing up military for support roles.
Atlantic Resolve showcases muscle in Europe without engaging in combat.
US military faces recruitment issues since 2018, not caused by Biden.
Qualifications for those heading over involve inside jokes, specialty logistics, training national armies, and medical roles.
Sparse reporting leads to uncertainty among people.
No indication of gearing up for combat; likely filling specific roles in ongoing operation.
Rotations of reserve units heading over, but focus on targeted roles.
Operation Atlantic Resolve has been ongoing for almost a decade, no need to panic.

Actions:

for policy analysts,
Prepare for possible rotations of reserve units heading over for Operation Atlantic Resolve (implied).
</details>
<details>
<summary>
2023-07-13: Let's talk about an update on the pillow world.... (<a href="https://youtube.com/watch?v=pnVDGslWMSg">watch</a> || <a href="/videos/2023/07/13/Lets_talk_about_an_update_on_the_pillow_world">transcript &amp; editable summary</a>)

Beau gives an update on MyPillow's financial struggles post-election, Lindell's confidence in lawsuits, and the persistent community still promoting election fraud claims.

</summary>

"He compares his situation to the movie 'My Cousin Vinny.'"
"There's still a pretty active community looking into the voting claims."
"It's the kind of stuff that when you read through it just makes your mind melt."

### AI summary (High error rate! Edit errors on video page)

Beau provides an update on the aftermath of the 2020 election, focusing on the MyPillow CEO, Mike Lindell, and his claims.
MyPillow's financial situation seems dire as they are subleasing manufacturing space and selling surplus equipment.
Major retailers dropped MyPillow products post-January 6th, impacting the company's revenue.
Lindell believes that retailers like Bed Bath and Beyond made decisions based on not carrying MyPillow products.
Lindell remains confident in the lawsuits against Dominion and Smartmatic, claiming he has evidence that will vindicate him.
He compares his situation to the movie "My Cousin Vinny" in terms of evidence presentation.
Beau suggests keeping track of this community as they might reenter the news cycle due to Lindell's financial situation.
Voting machine companies might be willing to settle for less due to Lindell's financial struggles.
Despite overwhelming evidence against the claims, a community continues to believe in the election fraud allegations.
Beau hints at a potential resurgence of this topic in the news cycle soon.

Actions:

for news consumers,
Stay informed on the developments in the aftermath of the 2020 election (implied).
Be critical of misinformation and false claims regarding election fraud (implied).
</details>
<details>
<summary>
2023-07-13: Let's talk about a judge's quotes at a hearing.... (<a href="https://youtube.com/watch?v=uQPbRWr90dk">watch</a> || <a href="/videos/2023/07/13/Lets_talk_about_a_judge_s_quotes_at_a_hearing">transcript &amp; editable summary</a>)

A detained suspect near Obama's residence prompts reflection on failures, mental health, and responsibility, hinting at surprises ahead.

</summary>

"We as a country have failed you. Now you have to pay the price for our failure."
"If our system was fair, you wouldn't be here."
"It leaves a lot of questions about what law enforcement has determined about the state of the suspect, the mental state of the suspect."

### AI summary (High error rate! Edit errors on video page)

A man armed near former President Obama's residence was detained for a hearing to determine release or custody awaiting trial.
The judge expressed that the country had failed the suspect, indicating mental health issues related to service PTSD.
The judge believed the suspect was taking orders, possibly influenced by former President Trump's posts.
The judge indicated frustration and implied a wish for someone else before him, holding those who spread false information responsible.
Despite not severe charges at the moment, the suspect remains detained until trial.
Questions linger about law enforcement's assessment of the suspect's mental state, influencing the case's direction.
Anticipation for surprises in the unfolding case was expressed.

Actions:

for legal observers, mental health advocates,
Follow updates on the case and be prepared to advocate for mental health considerations in legal proceedings (implied)
</details>
<details>
<summary>
2023-07-13: Let's talk about Russian leadership changes.... (<a href="https://youtube.com/watch?v=XK4Li-hsjPw">watch</a> || <a href="/videos/2023/07/13/Lets_talk_about_Russian_leadership_changes">transcript &amp; editable summary</a>)

Beau analyzes Russia's leadership changes, speculates on private deals, and debates whether the shifts amount to a purge or a political realignment.

</summary>

"Is this the P word? Is this a purge?"
"Russian security services loyal to Putin have started the micromanagement."
"But give it a month. We'll see what happens."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of Russia's leadership changes and happenings.
Speculating on the private deal between Putin and the boss of Wagner.
Mentioning a naval officer getting shot while jogging, likely unrelated to command structure changes.
Addressing the detainment of Sir Avikin and the ambiguity around his situation.
Explaining the relief of duty for General Ivan Popov due to criticism of the Ministry of Defense.
Debating whether the ongoing changes in leadership constitute a purge or a political realignment.
Noting Russian security services' increased micromanagement and removal of commanders stepping out of line.
Suggesting that the current actions may not yet qualify as a purge but could develop into one in the future.
Encouraging continued observation of the situation for further developments.

Actions:

for political analysts, russia watchers,
Monitor Russian leadership changes for future developments (implied).
</details>
<details>
<summary>
2023-07-13: Let's talk about Biden's pitch for 2024.... (<a href="https://youtube.com/watch?v=BctAQD7-UhQ">watch</a> || <a href="/videos/2023/07/13/Lets_talk_about_Biden_s_pitch_for_2024">transcript &amp; editable summary</a>)

Biden's 2024 campaign centers on normalcy and economic success, contrasting with Republican policies and framing himself as a stable choice against authoritarianism.

</summary>

"I'm normal."
"Do you really want a Republican in charge of a fragile economy?"
"He has some ideas that actually are pretty progressive, that may be a little bit scary."
"You have authoritarianism, incompetent authoritarianism or you have me and I'm just a grandpa."
"I just want to bring this country back to normalcy."

### AI summary (High error rate! Edit errors on video page)

Biden's 2024 election plan will likely focus on portraying himself as normal and non-divisive.
His campaign may revolve around showcasing the strong economy and job creation under his administration.
Biden might contrast his handling of the economy with the Republican Party's track record and question their ability to manage a fragile economy.
He could point out improvements in foreign policy, especially within NATO, under his administration compared to the previous Republican leadership.
Reproductive rights and LGBTQIA+ rights are being threatened in Republican states, and Biden might advocate for protecting these rights nationwide.
The administration is promoting green energy and infrastructure jobs, contrasting it with the Republicans' stance on these issues.
Biden aims to bring the country back to normalcy, contrasting the chaotic imagery from Trump's America with his vision.
He plans to focus on basic issues rather than his more progressive ideas to appeal to a broader audience.
If the Republican Party continues with a MAGA or Trump-like candidate, Biden's campaign strategy might position him as a stable alternative to authoritarianism.
Overall, Beau suggests that Biden's simple message of normalcy could resonate with voters while avoiding divisive topics.

Actions:

for voters, political analysts,
Support and advocate for reproductive rights and LGBTQIA+ rights in your community (implied)
Stay informed about political developments and policies affecting the economy, foreign relations, and social rights (implied)
</details>
<details>
<summary>
2023-07-12: Let's talk about the Arizona GOP.... (<a href="https://youtube.com/watch?v=SpksEJAaoeI">watch</a> || <a href="/videos/2023/07/12/Lets_talk_about_the_Arizona_GOP">transcript &amp; editable summary</a>)

Arizona Republican Party's financial struggles and donor reluctance could have significant implications for 2024, prompting a push for more "sane" positions.

</summary>

"Nobody wants to donate to a crazy party."
"The fact that the GOP out there is having such a hard time raising money right now, this could spell huge problems for Republicans come 2024."

### AI summary (High error rate! Edit errors on video page)

Arizona Republican Party has $50,000 cash on hand, significantly less than the Florida Democratic Party's half a million.
Arizona GOP spent money defending Trump claims and on unnecessary projects like a victory party for 2022.
Typically, parties spend money in weird ways, but donors usually replace the funding. This isn't happening in Arizona.
Donors see their contributions as an investment for access, not necessarily supporting the party's opinions.
The struggle of the Arizona GOP to raise funds may be due to the fringe takeover of the Republican Party in the state.
Conservative commentators are suggesting that nobody wants to donate to a "crazy party."
There's a push for the party to adopt more "sane" positions to attract donations.
The current situation in Arizona suggests that the Democratic Party should solidify their support as the state seems up for grabs.
The Arizona Republican Party is facing significant challenges, indicating the need for a strategic move.
The financial troubles of the Arizona GOP could have major implications for Republicans in 2024.

Actions:

for political analysts,
Support and donate to political parties or candidates you believe in (implied)
</details>
<details>
<summary>
2023-07-12: Let's talk about Ukraine, NATO, and a rumor.... (<a href="https://youtube.com/watch?v=ERn9TG_P9Ts">watch</a> || <a href="/videos/2023/07/12/Lets_talk_about_Ukraine_NATO_and_a_rumor">transcript &amp; editable summary</a>)

Rumored long-term assistance for Ukraine from NATO countries challenges Russia's aggression and hopes of Western disinterest.

</summary>

"Ukraine wants NATO membership."
"A long-term agreement puts Russia on notice."
"It ruins Russia's hope of the populations in the West getting bored."

### AI summary (High error rate! Edit errors on video page)

Rumors circulating within foreign policy circles suggest a deal for Ukraine with NATO countries, not coming directly from NATO.
The rumored agreement aims to provide intelligence, logistics, training, and supplies to Ukraine.
Unlike NATO membership, the rumored deal won't require NATO countries to defend Ukraine if attacked, as Ukraine is already under attack.
Ukraine already receives logistics, intelligence, supply, and training, but the rumored deal will offer long-term assistance over a specified period.
The assistance Ukraine currently receives is described as piecemeal and may not have a significant impact due to the frequency of aid packages.
Russia is banking on Western resolve diminishing and Ukraine's assistance fading, as they might struggle economically without it.
A multi-year agreement with Ukraine signals to Russia that Western support won't wane, putting more pressure on Russia to rethink its aggression.
The rumored plan could deter Russia's hopes of Western populations losing interest or elections changing foreign policies.
If the rumored deal is confirmed, it could have significant implications for Russia's aggressive actions and Western support for Ukraine.

Actions:

for foreign policy observers,
Advocate for sustained Western support for Ukraine (suggested)
Stay informed about developments in Ukraine and NATO relations (implied)
</details>
<details>
<summary>
2023-07-12: Let's talk about Sweden and NATO.... (<a href="https://youtube.com/watch?v=nn1sCxRwhE8">watch</a> || <a href="/videos/2023/07/12/Lets_talk_about_Sweden_and_NATO">transcript &amp; editable summary</a>)

Sweden's potential NATO membership signifies a strategic blow to Russia rather than a significant military gain for the alliance, impacting geopolitical positioning.

</summary>

"It's not a huge NATO win, it's a huge Russian loss."
"The war in Ukraine is lost. Russia lost the war."
"Sweden joining NATO is a massive loss to Russia."
"It's more about geopolitical positioning than actual military strategy."
"It's not necessarily about the benefit militarily to the alliance."

### AI summary (High error rate! Edit errors on video page)

Sweden is likely to become part of NATO, raising questions about the significance for the alliance and Russia.
The point of alliances like NATO is to divide costs and create a strong military force collectively.
Countries around the Baltic joining NATO helps in securing the Baltic Sea and deterring Russian aggression.
The availability of forces in certain areas provides NATO with immediate defense without a deployment phase.
Russia's desire to rekindle imperial designs has triggered countries to ally with NATO, costing Russia geopolitically.
The war in Ukraine is characterized as lost for Russia, leading to imperialism and geopolitical setbacks.
Russia's actions may lead to a loss of world power status if not altered, impacting their influence and place at the international table.
Sweden joining NATO is viewed more as a loss for Russia than a significant military gain for the alliance.
The impact of Sweden joining NATO lies more in geopolitical positioning rather than direct military strategy.
The message sent to Russia through Sweden joining NATO is more about geopolitical positioning and influence than military benefit.

Actions:

for foreign policy analysts,
Join NATO (suggested)
Monitor geopolitical developments (implied)
</details>
<details>
<summary>
2023-07-12: Let's talk about Biden's briefing booboo.... (<a href="https://youtube.com/watch?v=8EJA5qmrwUk">watch</a> || <a href="/videos/2023/07/12/Lets_talk_about_Biden_s_briefing_booboo">transcript &amp; editable summary</a>)

President Biden's use of profanity sparks controversy, but demanding accurate information and translating technical jargon show a refreshing approach in the White House.

</summary>

"How the heck did you not know that, man?"
"Don't hecking BS me."
"Welcome to a White House that expresses urgency about things other than golf tee times, taking documents and cooing."
"I personally think this is a nice change."
"Y'all have a good heckin' day."

### AI summary (High error rate! Edit errors on video page)

President Biden's use of profanity and shouting has become a topic of interest, particularly among right-wing media.
Fox News claims Biden's profanity undermines his "well curated image," despite instances of him using profanity publicly in the past.
Biden's go-to phrases when frustrated with his staffers include "how the heck did you not know that, man?" and "don't hecking BS me."
Beau defends Biden's demanding approach to accurate information, suggesting it's a positive change from the previous administration's penchant for inaccurate information.
Biden prefers briefings in plain, everyday language, and he requests translations of technical jargon into understandable terms.
Beau humorously points out the absence of reported ketchup incidents in the White House amidst all the focus on Biden's use of profanity.

Actions:

for media consumers,
Translate technical jargon into everyday language (exemplified)
</details>
<details>
<summary>
2023-07-11: Let's talk about a surprising GOP development.... (<a href="https://youtube.com/watch?v=4MwSFOUNx4s">watch</a> || <a href="/videos/2023/07/11/Lets_talk_about_a_surprising_GOP_development">transcript &amp; editable summary</a>)

A twist in a story reveals serious charges against a person linked to influencing policies and political events, sparking debate within the Republican Party.

</summary>

"This person is, y'all, you know, y'all, y'all, y'all, y'all, y'all, y'all, you know, you know, you know."
"How much the Republican Party knew about these events is going to be a matter of debate."
"Rest assured this story is not going away."
"This isn't going to be a short-lived scandal."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A man has been charged with a list of serious crimes, including conspiracy and making false statements, relating to various Acts.
The person charged agreed to recruit and pay a former high-ranking U.S. government official on behalf of principals in China to influence policies with respect to China.
The allegations suggest that they caught an agent of influence who had connections with the president-elect in 2016, Trump.
This person, who is the same as the Republican's lost whistleblower, allegedly had contacts within the Republican Party and was willing to exploit them.
The twist is that the charged individual is believed to be the same person who presented evidence about Biden at a U.S. embassy in Brussels in 2019.
Despite attempts to use this person as a whistleblower, the gravity of the charges suggests that many within the Republican Party may have been unaware of these events.
It is likely that the story will continue to surface, and the extent of the Republican Party's knowledge about these events will be a topic of debate.

Actions:

for political observers, republican party members.,
Stay informed and follow updates on this unfolding story (implied).
Engage in respectful and constructive debates about political events and allegations (implied).
</details>
<details>
<summary>
2023-07-11: Let's talk about Trump, Georgia, and the Grand Jury.... (<a href="https://youtube.com/watch?v=Im-9TRb-VnE">watch</a> || <a href="/videos/2023/07/11/Lets_talk_about_Trump_Georgia_and_the_Grand_Jury">transcript &amp; editable summary</a>)

Georgia grand jury to potentially indict Trump for election interference, signaling more legal troubles ahead.

</summary>

"They're going to pick 23 jurors and three alternates."
"This grand jury is likely to determine whether or not former President Trump will become a thrice indicted former president."
"This isn't the beginning of a trial, it's the grand jury process."

### AI summary (High error rate! Edit errors on video page)

Georgia grand jury selecting 23 jurors and three alternates to look at potential cases, including the election interference case involving former President Trump.
Grand jury might take until August to make a decision, signaling uncertainty in the timeline.
Beau speculates that keeping people off balance and not being forthcoming in plans could be strategic.
Grand jury will determine if Trump will be indicted for a third time.
Uncertainty about the case moving forward due to potential federal indictment for a similar scheme.
Prosecution in Georgia appears likely to move forward regardless of other developments.
Grand jury's role is to decide if an indictment should be issued, not guilt or innocence.
This process marks the start of potential criminal proceedings for Trump.
Beau jokingly mentions running out of space on his whiteboard due to the lengthy list of Trump's potential legal issues.
Beau ends with a casual farewell, wishing everyone a good day.

Actions:

for legal observers,
Stay informed on the developments in Georgia's grand jury proceedings (suggested).
Follow news outlets reporting on the potential indictments and legal proceedings (suggested).
</details>
<details>
<summary>
2023-07-11: Let's talk about Trump's requested delay in the documents case.... (<a href="https://youtube.com/watch?v=-sYPTtbFaSM">watch</a> || <a href="/videos/2023/07/11/Lets_talk_about_Trump_s_requested_delay_in_the_documents_case">transcript &amp; editable summary</a>)

Team Trump's request for a delay in the documents case serves as a strategy to shield Trump from legal consequences and tests the judge's impartiality.

</summary>

"There is a citation of a case, USV Hapful, that is an unpublished opinion out of the Eleventh Circuit."
"The goal here is to create a situation in which Trump can use the presidency as a solution to his legal entanglements."
"The Department of Justice is opposed to the idea of delaying it until whenever as far as they're concerned they're late they're late they're late for an imporant date."
"I really see this more as Trump's legal team testing the judge."
"It may signal to the judge that Team Trump is going to try to exploit her leniency and put her in a position she doesn't want to be in."

### AI summary (High error rate! Edit errors on video page)

Team Trump requested an indefinite delay in the documents case to push it past the 2024 election, aiming to use the presidency as a shield against legal entanglements.
The filing is more than just a delay tactic; it's a test of the judge who has previously ruled in favor of Trump, potentially signaling judicial bias.
The Department of Justice opposes the delay, viewing it as an attempt to circumvent legal accountability.
The strategy of delay may not work in all venues, and Trump's confidence in winning could impact the decision to prolong the trial.
Trump's legal team seems to be testing the judge's boundaries, possibly to gauge her stance and exploit any leniency.

Actions:

for legal analysts,
Monitor the developments in the documents case and the judge's ruling (implied).
Stay informed about legal proceedings and potential implications for the justice system (implied).
</details>
<details>
<summary>
2023-07-11: Let's talk about Fox getting sued again.... (<a href="https://youtube.com/watch?v=8jouQRFGbOU">watch</a> || <a href="/videos/2023/07/11/Lets_talk_about_Fox_getting_sued_again">transcript &amp; editable summary</a>)

Beau examines the potential legal repercussions faced by Fox News following right-wing theories, risking further erosion of trust in the network.

</summary>

"Fox is an outlet that has over the years published a lot of theories."
"The potential payout [for defamation] would be pretty big and that's one more thing that Fox just does not need right now."
"Every instance like this, it will probably erode that trust even further."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the likelihood of another suit against Fox News Network due to theories published by the outlet.
Tucker, although no longer on the network, may still be causing issues for Fox.
Ray Epps, a man at the center of right-wing theories about the events of January 6th, demanded a retraction from Fox through an attorney.
Fox has not responded to Epps' cease and desist letter, potentially leading to a defamation suit.
The inaccurate theories about Epps were prevalent on Tucker's show, adding to the potential legal troubles for Fox.
Beau mentions the impact on Epps and his family, who reportedly had to flee the state due to harassment.
If Epps proceeds with a defamation suit, the potential payout could be substantial for him.
Beau notes the erosion of trust in Fox News due to incidents like these, further jeopardizing the network's credibility.
Fox has the ability to address the issue and potentially rectify the situation, but their response remains uncertain.
Beau hints at the future implications of this situation and speculates on Fox's course of action.

Actions:

for media consumers,
Support organizations advocating for accurate reporting and holding media outlets accountable (implied)
Stay informed about issues related to media ethics and misinformation (implied)
</details>
<details>
<summary>
2023-07-10: Let's talk about the GOP looking for alternatives to Trump.... (<a href="https://youtube.com/watch?v=H41dt4VqmwA">watch</a> || <a href="/videos/2023/07/10/Lets_talk_about_the_GOP_looking_for_alternatives_to_Trump">transcript &amp; editable summary</a>)

The GOP considers new candidates like Youngkin and Kemp, with Kemp potentially offering a more moderate alternative to Trump if he decides to run.

</summary>

"Kemp is different. Kemp is not saddled with a lot of the baggage that makes him only appealing to the fringe."
"Kemp is somebody that a whole lot of people are sleeping on."
"It may be a case of wishful thinking from GOP strategists."
"He might pull a lot of votes from the middle that if Trump was running, certainly be votes for the Democratic candidate."
"It's an interesting development, but at this point, we don't know how much of it is just wishful thinking on the part of GOP strategists."

### AI summary (High error rate! Edit errors on video page)

The GOP is considering new candidates for president due to doubts about current candidates' chances against Trump.
Two new names being considered are Youngkin, governor of Virginia, and Governor Kemp of Georgia.
Beau doubts Youngkin can beat Trump as he can't out-Trump him; believes a successful challenger must be substantially different.
Kemp is viewed favorably for not being MAGA and supporting American democracy, aiming his campaign towards the center.
Kemp's appeal lies in potentially bringing the GOP back towards a more moderate stance.
Despite positives, Beau questions whether Kemp actually wants to run for president.
Kemp's lack of connection with Trump's baggage and his image as standing up to Trump could attract independent voters.
Kemp may have the best chance of winning a general election among the Republican names mentioned, but facing challenges in the primary due to Trump's influence.
Beau suggests the Democratic Party should pay attention to Kemp as a potential alternative if circumstances change in the primary.
There is uncertainty surrounding the likelihood of Kemp running for president and the feasibility of his success.

Actions:

for political analysts, party strategists,
Keep informed about potential new candidates entering the political race (suggested)
Stay attentive to shifts in the political landscape and be prepared to adapt strategies accordingly (suggested)
</details>
<details>
<summary>
2023-07-10: Let's talk about statistics, Russia, and new techniques.... (<a href="https://youtube.com/watch?v=QdfzcRRoeWA">watch</a> || <a href="/videos/2023/07/10/Lets_talk_about_statistics_Russia_and_new_techniques">transcript &amp; editable summary</a>)

Beau dives into the techniques and challenges of obtaining accurate casualty numbers, revealing the gruesome reality behind statistics in conflicts.

</summary>

"It's not an accurate depiction of the whole picture."
"None of this was necessary. This was elective."
"I hope you all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing statistical analysis, Russia, Ukraine, and new techniques in obtaining accurate numbers.
Questioning the accuracy of reported numbers due to countries having incentives to manipulate data.
Exploring the origin of the numbers and the methodologies used to determine casualties.
Contrasting Russia's claim of 6,000 losses with Ukraine's estimate of six figures.
Describing the analysis of military funerals' photos and excess inheritance cases to ascertain casualty figures.
Emphasizing that the numbers obtained represent the minimum, not the total count.
Pointing out that various factors could make the actual casualty count higher than reported.
Expressing disappointment and disapproval towards unnecessary conflict and resulting casualties.
Acknowledging viewer support for Ukraine while expressing concern over the human cost.
Commenting on the effectiveness of the innovative techniques used to estimate casualties.

Actions:

for analytical viewers,
Advocate for transparency and accountability in reporting casualty numbers (implied).
Support organizations working towards peace and conflict resolution (implied).
Raise awareness about the human cost of conflicts through education and activism (implied).
</details>
<details>
<summary>
2023-07-10: Let's talk about Tuberville and the Marines.... (<a href="https://youtube.com/watch?v=qfzzB-DgE7A">watch</a> || <a href="/videos/2023/07/10/Lets_talk_about_Tuberville_and_the_Marines">transcript &amp; editable summary</a>)

Senator Tuberville's obstruction of military promotions jeopardizes civilian control and hands power to the President, undermining constitutional principles and risking military readiness.

</summary>

"His little stunt is jeopardizing civilian control of the military."
"It's going to show that the Senate has no power here."
"Effective tomorrow, Senator Tuberville has handed all control of the military to Biden."
"I'm sure they don't really care about that, but I bet they care about the fact that Tuberville is handing Biden all the power."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Senator Tuberville has been holding up high-level military promotions, including those of the United States Marines.
The senator's actions have been undermining US military readiness because of his opposition to the branches of the military caring for their personnel's reproductive rights.
Effective tomorrow, the United States Marines will not have a confirmed commandant due to Senator Tuberville's actions.
This situation shifts from undermining military readiness to undermining separation of powers and foundational constitutional principles.
A vacant leadership position in the Marines will result in someone becoming the acting commandant, with significant authority despite lacking confirmation.
Senator Tuberville's refusal to confirm nominees for high-profile military positions jeopardizes civilian control of the military and undermines checks and balances.
By not consenting to nominations, Senator Tuberville establishes a precedent where positions are filled by acting officials, diminishing the Senate's role.
This situation ultimately hands control of the military to President Biden, bypassing the Senate's advice and consent.
The precedent set by acting appointments weakens the Senate's authority and consolidates power with the President and Secretary of Defense.
Senator Tuberville's actions may risk U.S. military readiness and the lives of service members and civilians worldwide.

Actions:

for congressional constituents,
Contact Senator Tuberville's office to express concerns about his obstruction of military promotions and its implications for civilian control (suggested).
Support organizations advocating for upholding constitutional principles and the importance of Senate oversight in military appointments (exemplified).
</details>
<details>
<summary>
2023-07-10: Let's talk about Captain Kori.... (<a href="https://youtube.com/watch?v=fJp_zgNb8uw">watch</a> || <a href="/videos/2023/07/10/Lets_talk_about_Captain_Kori">transcript &amp; editable summary</a>)

Updates on Captain Cory's dream fulfillment and passing, with a call for messages of support on his memorial channel.

</summary>

"He really did get his dream for six months."
"I know that this is not the news everybody wanted to hear."
"Take a little bit of solace in the fact that he really did get his dream for six months."
"It was due to the people who are going to be pretty upset by this news."

### AI summary (High error rate! Edit errors on video page)

Updates on Captain Cory's journey are being shared.
Captain Cory, a young man passionate about pirates, received help to fulfill a bucket list item.
He wanted a YouTube play button and support poured in from various sources to make it happen.
Captain Cory has passed away, but he lived his dream in the last six months of his life.
His family is requesting space at the moment to cope with the loss.
His mother intends to keep the channel running as a memorial to him.
Messages of support can be left on his channel, "a crack in the box."
The news might not be what everyone wanted to hear, but it was somewhat expected.
Despite the sad news, there is solace in knowing that Captain Cory lived his dream.
The community's support played a significant role in fulfilling Captain Cory's dream.

Actions:

for online community members,
Leave a message on Captain Cory's channel, "a crack in the box" (suggested)
Visit his channel to wish him a good voyage (suggested)
</details>
<details>
<summary>
2023-07-09: The roads to understanding Youtube.... (<a href="https://youtube.com/watch?v=hQVE6gzmF-0">watch</a> || <a href="/videos/2023/07/09/The_roads_to_understanding_Youtube">transcript &amp; editable summary</a>)

Beau shares insights on YouTube studio challenges, video ideas, channel separation, shirt selection, algorithm insights, Twitter impact, financial sustainability, holiday episodes, and AI concerns.

</summary>

"I thought the new studio meant we'd get full T-shirt views."
"Algorithm is always the same."
"Content is what matters."
"I don't see that happening with me. I really enjoy this."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the new YouTube studio and the challenges with framing for T-shirt views.
He shares insights into potential video ideas, like covering historical events day by day or creating a fictional country paralleling real-world events.
The reasoning behind having two separate channels is explained based on audience preferences for content length.
Beau explains his process of selecting shirts with multiple meanings for his videos.
On the topic of tags, Beau admits to not using them due to initial ignorance and reluctance to change.
He delves into the new algorithm's primary purpose of keeping viewers on YouTube for extended periods.
The decision to separate long-form content into a different channel is justified based on audience engagement metrics.
Beau discloses the minimal impact of leaving Twitter on viewership and the importance of utilizing social media for new creators.
He sheds light on the financial aspects of being a full-time YouTuber and the subscriber count required for sustainability.
Beau's preferences for Halloween episodes over Christmas ones and his thoughts on AI overtaking creators are shared.

Actions:

for content creators,
Experiment with new video ideas inspired by historical events or fictional concepts (suggested).
Choose shirts with multiple meanings for metaphors in videos (exemplified).
Utilize social media platforms to share and grow your content (implied).
</details>
<details>
<summary>
2023-07-09: Let's talk about the Navy and names.... (<a href="https://youtube.com/watch?v=Xj8TwLVNZ-s">watch</a> || <a href="/videos/2023/07/09/Lets_talk_about_the_Navy_and_names">transcript &amp; editable summary</a>)

The US Navy faces scrutiny for historical names, unlikely to change despite ongoing movement and discourse.

</summary>

"I don't think you're wrong."
"Everything's impossible until it's not."
"It seems really unlikely that they change the name of a major ship."

### AI summary (High error rate! Edit errors on video page)

The US Navy is under scrutiny for its historical names following the lead of other military branches.
Generals who joined the Confederacy are being stripped of their names from military installations.
Concerns arise as black fathers question ships named after segregationists where their sons serve.
Unlikely for the Navy to change ship names due to tradition, despite ongoing movement and discourse.
The renaming of army bases took years of debate before action was taken.
Possibility that ship names won't be passed on to new ships upon retirement.
Some sailors serving on these ships may retire before the ship itself.
Navy's tradition and historical importance in naming ships make change unlikely.
Naming after historical figures due to their significance in Navy's history might deter name changes.
Beau suggests starting with senators for any petition or action towards change.

Actions:

for families of service members,
Petition senators for ship name changes (suggested)
Join the movement to change ship names (implied)
Advocate within the Navy for renaming ships (exemplified)
</details>
<details>
<summary>
2023-07-09: Let's talk about Tucker's Twitter tumble.... (<a href="https://youtube.com/watch?v=7XItmSb07K8">watch</a> || <a href="/videos/2023/07/09/Lets_talk_about_Tucker_s_Twitter_tumble">transcript &amp; editable summary</a>)

Tucker's move to Twitter shows declining viewership, potentially signaling a shift away from extreme conservatism.

</summary>

"Twitter is Thunderdome."
"Just because somebody viewed the tweet doesn't mean they watched any of the video."
"There are a lot of people who really express a lot of concern that without Fox holding him back, that he was going to be even more of a right-wing cultural juggernaut."
"It definitely appears that there's a decline and it's pretty substantial."
"This could be a sign that people are moving away from the more extreme factions of the Republican Party."

### AI summary (High error rate! Edit errors on video page)

Tucker moved from Fox to Twitter, sparking a lot of speculation.
Tucker's impact on Twitter is less extreme compared to TV, as Twitter is already inflammatory.
Musk's moves on Twitter may have incentivized more inflammatory behavior.
Numbers show a decline in viewership for Tucker's Twitter episodes.
Views steadily declined: announcement - 137 million, episode one - 120 million, episode two - 60 million, and so on.
Another set of numbers indicates a decline in people watching Tucker's videos for more than two seconds.
Concerns about Tucker becoming more extreme without Fox seem unfounded due to the declining viewership.
The decline could signal a wider issue within the conservative movement.
Viewers of Fox, considered moderate Republicans, may be moving away from extreme factions.
People exposed to extreme content on Fox may not actively seek it out, a potentially positive sign.

Actions:

for political analysts, social media users,
Analyze viewership trends to understand political shifts within the conservative movement (implied).
Encourage critical consumption of media content and its potential impact on political ideologies (suggested).
</details>
<details>
<summary>
2023-07-09: Let's talk about New Jersey and wind wins.... (<a href="https://youtube.com/watch?v=gHNOmFhAqwI">watch</a> || <a href="/videos/2023/07/09/Lets_talk_about_New_Jersey_and_wind_wins">transcript &amp; editable summary</a>)

New Jersey approves Ocean Wind 1, the largest wind farm project, signaling a positive step towards clean energy transition and climate change mitigation.

</summary>

"Transitioning to clean energy is viewed as a positive step towards mitigating climate change."
"Acknowledging and celebrating wins in clean energy is vital to maintaining momentum and positivity."
"Despite criticisms and imperfections, progress towards clean energy is moving in the right direction."

### AI summary (High error rate! Edit errors on video page)

New Jersey has approved another wind farm for construction, Ocean Wind 1, with 98 offshore turbines.
Ocean Wind 1 is expected to provide power for 380,000 homes upon completion.
This project is the largest one approved so far, with more wind farms in the pipeline.
The Biden administration sees clean energy as a key focus for the environment.
Transitioning to clean energy is viewed as a positive step towards mitigating climate change.
The more clean energy projects are successful, the more people will invest in them, speeding up the transition.
Acknowledging and celebrating wins in clean energy is vital to maintaining momentum and positivity.
Despite criticisms and imperfections, progress towards clean energy is moving in the right direction.
The shift towards clean energy can have long-lasting positive effects, as long as the push for transition continues.
Encouraging individuals to support clean energy initiatives by using them in their own homes is key to further progress.

Actions:

for environment advocates, energy enthusiasts, climate activists,
Invest in clean energy initiatives and projects (implied)
Support and advocate for clean energy policies in your community (implied)
Utilize clean energy sources in your own home (implied)
</details>
<details>
<summary>
2023-07-09: Let's talk about Michigan GOP infighting.... (<a href="https://youtube.com/watch?v=_yyGP1xIFzo">watch</a> || <a href="/videos/2023/07/09/Lets_talk_about_Michigan_GOP_infighting">transcript &amp; editable summary</a>)

The state of the Republican Party in Michigan, marked by literal infighting, threatens their success in 2024, against a well-organized Democratic Party.

</summary>

"The blue wall kind of seems to be back and for Republicans, Michigan is going to be incredibly
important in 2024."
"If the Republican party does not find some way to kind of come together on this, it seems pretty likely
that come 2024, well, they'll be kicking themselves."

### AI summary (High error rate! Edit errors on video page)

Overview of the state of the Republican Party in Michigan and its implications for 2024.
Discord and struggles within the Republican Party, characterized by infighting.
Recent literal infighting incident during a closed-door meeting of the State Republican Party.
Upset Republicans outside the closed-door meeting leading to a physical altercation involving a door handle, a single-fingered salute, and a chair.
Involvement of law enforcement and hospitalization of an individual as a result of the altercation.
Historical disputes within the Michigan State Republican Party.
Comparison with the well-organized Democratic Party in the region.
Importance of unity for the Republican Party in Michigan in order to compete effectively in 2024.
Warning that failure to unify could lead to regrets for the Republican Party in the upcoming election.
Final message encouraging listeners to have a good day.

Actions:

for political analysts, michigan voters.,
Reach out to local Republican Party officials to advocate for unity within the party (suggested).
Get involved in local political events and activities to support a cohesive Republican Party in Michigan (implied).
</details>
<details>
<summary>
2023-07-08: Let's talk about the most pressing issue for your kids.... (<a href="https://youtube.com/watch?v=GGHhkjMu5lY">watch</a> || <a href="/videos/2023/07/08/Lets_talk_about_the_most_pressing_issue_for_your_kids">transcript &amp; editable summary</a>)

The biggest issue facing our children is climate change; candidates without a climate plan should not earn your vote.

</summary>

"If the candidate does not have a climate mitigation plan, I don't know how you can reasonably even consider voting for them."
"Climate change is the biggest issue we're facing. It's the one that we have to act on now."
"Inaction on climate change will lead to severe consequences for future generations."
"The impact of climate change is not just temporary weather but a long-term global climate shift."
"Having a climate plan should be a primary consideration when choosing candidates to vote for."

### AI summary (High error rate! Edit errors on video page)

The biggest issue facing our children and future records is climate change, not accurately portrayed in headlines.
The average temperature is increasing, leading to extreme weather, water shortages, and crop failures.
Candidates without a climate mitigation plan should not be considered for votes, as climate change is the most critical issue.
Immediate action is necessary to address climate change for mitigation.
Candidates who ignore or downplay climate change for political reasons cannot be trusted.
Climate change is a global issue that requires urgent attention.
Having a climate plan should be a primary consideration when choosing candidates to vote for.
Inaction on climate change will lead to severe consequences for future generations.
Climate change is the most significant threat that needs to be acknowledged and acted upon.
The impact of climate change is not just temporary weather but a long-term global climate shift.

Actions:

for voters,
Support and vote for candidates with solid climate mitigation plans (implied).
</details>
<details>
<summary>
2023-07-08: Let's talk about clusters and aid.... (<a href="https://youtube.com/watch?v=PE8UtKBD_ic">watch</a> || <a href="/videos/2023/07/08/Lets_talk_about_clusters_and_aid">transcript &amp; editable summary</a>)

Beau clarifies misconceptions around the US aid package to Ukraine with cluster munitions, discussing bans, crimes, and escalations, stressing the need for caution in their use.

</summary>

"These are bad. They really are. But war is bad."
"It's one of those things that is, it causes problems far beyond what people really think about."
"They really shouldn't exist, but they do."
"The Ukrainian military needs to be incredibly careful when using these."
"I don't think they need to be told that."

### AI summary (High error rate! Edit errors on video page)

The United States agreed to supply Ukraine with cluster munitions, sparking debates on bans, crimes, and escalations.
Cluster munitions are banned by many countries due to their destructive nature, but neither Russia, Ukraine, nor the US are signatories to the ban.
The use of cluster munitions against civilian targets is considered a crime, regardless of whether it is banned or not.
Russia and Ukraine have both used cluster munitions in the conflict, with Russia's documented use including hitting civilian targets like hospitals.
The US aid package to Ukraine includes artillery shells designed to target vehicles and trenches, posing less risk to civilians but potentially impacting airfields closer to civilian areas.
Cluster munitions leave behind unexploded items that can cause harm for years, making their use highly problematic.
Despite the destructive nature of cluster munitions, major powers continue to possess and use them, akin to nuclear weapons.
The Ukrainian military must exercise extreme caution in using cluster munitions to minimize harm to civilians.

Actions:

for policy analysts, activists,
Contact organizations working on disarmament and peacebuilding to advocate for the ban on cluster munitions (suggested).
Support efforts to raise awareness about the devastating impact of cluster munitions on civilian populations (suggested).
Advocate for stricter regulations on the production and use of cluster munitions globally (suggested).
</details>
<details>
<summary>
2023-07-08: Let's talk about SCOTUS not retiring.... (<a href="https://youtube.com/watch?v=VBleN1izoHo">watch</a> || <a href="/videos/2023/07/08/Lets_talk_about_SCOTUS_not_retiring">transcript &amp; editable summary</a>)

Beau explains the connection between electoral politics and the Supreme Court's makeup, urging to think beyond the next election to impact rulings targeting specific groups, noting personal considerations may override justices' decisions, and attributing upset rulings to Trump's right-wing camp selections.

</summary>

"Those two things are directly linked. There's no way to separate it."
"It's something you should remember."
"The reason we have the rulings that have half the country incredibly upset is because of electoral politics."
"That's why these rulings exist."
"It's because Trump was in office and was able to select so many justices that are in the right-wing camp."

### AI summary (High error rate! Edit errors on video page)

Explains the connection between the Supreme Court and electoral politics.
Urges to think beyond the next election to impact the Supreme Court's makeup.
Notes the link between electoral politics and the Supreme Court's rulings targeting specific groups.
Points out that personal considerations may sometimes override justices' decisions to stay in office.
States that the rulings causing upset are a result of electoral politics.
Attributes the existence of right-wing camp justices to Trump's time in office.
Emphasizes the importance of considering these factors when deciding on future elections.

Actions:

for voters,
Keep informed about the Supreme Court's makeup and how electoral politics can influence it (implied).
Be actively involved in elections and support candidates who prioritize diverse judicial selections (implied).
</details>
<details>
<summary>
2023-07-08: Let's talk about Rudy, Trump, and a new direction.... (<a href="https://youtube.com/watch?v=1xmotT8QvJY">watch</a> || <a href="/videos/2023/07/08/Lets_talk_about_Rudy_Trump_and_a_new_direction">transcript &amp; editable summary</a>)

Beau addresses Rudy's involvement in schemes, particularly a serious one involving voting machines, and raises questions about the investigation's implications and Rudy's role in mitigating extreme actions.

</summary>

"Rudy, of all people, was like, you can't do this. This is illegal."
"Think about how far something has to be outside of the norm for even Rudy Giuliani to be like, no that's too far."
"This was a big deal."
"It is worth noting that almost immediately after this meeting, that's when they started talking about having a whole bunch of people at the 6th and that it was going to be wild."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses new information about Rudy and a previous video.
Contextualizes the importance of the information for the future.
Mentions Rudy's involvement in schemes, including one about voting machines.
Talks about a meeting discussing seizing voting machines and declaring martial law.
Rudy was present at the meeting but allegedly spoke against the illegal actions proposed.
Emphasizes the seriousness of the allegations and Smith's probe into the matter.
Rudy's role in mitigating the extreme actions proposed.
Raises questions about the investigation and the evidence available.
Speculates on the potential consequences if evidence is found.
Notes the significance of the failed implementation of certain orders discussed.
Links the meeting to subsequent events on the 6th.
Encourages contemplation on the gravity of Rudy's refusal to partake in the extreme actions proposed.

Actions:

for watchers of current events.,
Contact authorities if you have relevant information on illegal activities (implied).
Stay informed about ongoing investigations and developments (implied).
Advocate for accountability and transparency in political processes (implied).
</details>
<details>
<summary>
2023-07-07: Let's talk about the ruble and where it goes from here.... (<a href="https://youtube.com/watch?v=6jzIhxb83qw">watch</a> || <a href="/videos/2023/07/07/Lets_talk_about_the_ruble_and_where_it_goes_from_here">transcript &amp; editable summary</a>)

Russians closely monitor the ruble's value, which, when outside the comfort zone, sparks unrest and challenges for Putin as he struggles to maintain appearances amidst economic turmoil.

</summary>

"Russians look at the exchange rates on the ruble the way Americans look at the stock market."
"If it goes above 90 rubles per dollar, they start to get worried."
"They are cooking the books and they're doing everything that they can to keep the ruble appearing as if it's strong."
"Putin is not sitting in a good position right now."
"The longer he refuses to admit his mistakes, the worse that position is going to get."

### AI summary (High error rate! Edit errors on video page)

Russians view the exchange rates on the ruble as Americans view the stock market, making it an economic indicator.
The comfort zone for the Kremlin is 80 to 90 rubles per dollar; if it surpasses 90, concerns arise.
The ruble has lost 21% of its value this year and is currently trading at more than 90 rubles to a dollar.
The Russian government manipulates the ruble's appearance to counteract sanctions and maintain the illusion of strength.
Recent events have led to the ruble falling outside its comfort zone, alarming the people and angering Putin.
Russia can likely sustain these measures until the end of the year before facing significant repercussions.
Putin may resort to more drastic measures to keep up appearances, depleting reserves faster.
Maintaining the illusion of a strong ruble is unsustainable in the long run.
The longer Putin denies his mistakes, the worse the situation will become.
The current situation is more about appearance than actual economic value.

Actions:

for economic analysts, policymakers,
Monitor the ruble's value and its impact on Russia's economic stability (suggested)
Stay informed about the economic situation in Russia (suggested)
</details>
<details>
<summary>
2023-07-07: Let's talk about Threads, Twitter, Pepsi, and Coca-Cola.... (<a href="https://youtube.com/watch?v=IqBTJ_GU7PQ">watch</a> || <a href="/videos/2023/07/07/Lets_talk_about_Threads_Twitter_Pepsi_and_Coca-Cola">transcript &amp; editable summary</a>)

Beau delves into the emerging social media wars, dissecting Musk's missteps, Threads' differentiation strategies, and the challenge of replicating Twitter's unique influence.

</summary>

"You're the product."
"A normal average American could tweet directly to their representative, their senator, the president."
"The idea of choosing a server for a lot of people is too much."
"Somebody behind Mastodon have any interest in it. They do not seem to be motivated by pursuit of profit."
"Even the slightest error gets amplified."

### AI summary (High error rate! Edit errors on video page)

Comparing the current social media landscape to the Cola Wars of the 80s, with Twitter and Threads (owned by Metta) positioned as key players.
Elon Musk's acquisition of Twitter led to suboptimal management and created an opening for competitors like Threads.
Musk's mistakes include elevating questionable individuals, altering the creator economy, and proposing a subscription-based approach.
Social media sees users as the product, with a focus on selling advertising.
Threads aims to differentiate itself by promoting kindness and warning users about disinformation spreaders.
Major content creators have issues with Threads due to past problems with Facebook, including algorithm issues and throttling pages.
The philosophical question arises about concentrating social media power in one company's hands.
Twitter's unique value lies in its proximity to power, allowing direct interaction with celebrities and politicians.
Other social media platforms like Mastodon lack Twitter's influence due to technical and ideological limitations.
The challenge for competitors is replicating Twitter's proximity to power and broad appeal across demographics.

Actions:

for social media users,
Support platforms that prioritize user well-being and combat disinformation (implied)
Engage in social media platforms that value user privacy and community over profit (implied)
Join or support platforms that strive to maintain a diverse range of voices and perspectives (exemplified)
</details>
<details>
<summary>
2023-07-07: Let's talk about Putin's domestic plays being ignored.... (<a href="https://youtube.com/watch?v=30qOg2WsojQ">watch</a> || <a href="/videos/2023/07/07/Lets_talk_about_Putin_s_domestic_plays_being_ignored">transcript &amp; editable summary</a>)

Russia's strengthening of the National Guard and transfer of special units may indicate plans to target critics of Putin, a concerning development for both Russians and the West.

</summary>

"Russia is strengthening its National Guard, a move commonly made by authoritarian leaders to ensure loyalty."
"The potential use of these units to target critics and opposition should not be ignored by both Russians and the West."

### AI summary (High error rate! Edit errors on video page)

Russia is strengthening its National Guard, a move commonly made by authoritarian leaders to ensure loyalty.
Western focus is on the impact of strengthening the National Guard on the Russian war effort.
Overlooked is the transfer of special units, with skills in undercover operations and dynamic entries, to the National Guard.
These special units possess skills directly transferable to targeting critics of Putin and undermining opposition.
Putin may be planning to target individuals less loyal to him using these special units.
The special units selected are likely chosen for their ability to keep quiet and avoid leaking information.
The potential use of these units to target critics and opposition should not be ignored by both Russians and the West.

Actions:

for russians, western observers,
Stay informed about developments in Russia's National Guard and special unit transfers (implied)
Raise awareness about potential targeting of critics and opposition by Putin's regime (implied)
</details>
<details>
<summary>
2023-07-07: Let's talk about McConnell's plan for 2024.... (<a href="https://youtube.com/watch?v=zRpavFnNN-E">watch</a> || <a href="/videos/2023/07/07/Lets_talk_about_McConnell_s_plan_for_2024">transcript &amp; editable summary</a>)

McConnell strategically focuses on specific Senate races for modest gains, prioritizing electability and majority while Democrats have an opening to capitalize by organizing effectively.

</summary>

"He's going to focus on those four states and hope that they can pull a majority out of it."
"He doesn't want to, he doesn't want a replay of 2022."
"McConnell is ceding. He's going to sacrifice a large chunk of races in hopes of devoting all of those resources to just a few states to ensure a majority."
"A win is a win, a majority is a majority."
"So it's an opportunity for the Democratic Party but only if they work for it."

### AI summary (High error rate! Edit errors on video page)

McConnell is aiming for modest gains in the Senate races, surprising many due to the anticipated all-out push from the Republican Party.
McConnell wants to focus on Montana, Ohio, Pennsylvania, and West Virginia, considering other states as unwinnable due to strong MAGA presence.
He prioritizes electability and a majority, aiming to avoid a repeat of 2022 where being tied to certain candidates hindered potential wins.
McConnell seeks to ensure the survival of the Republican Party rather than focusing on its thriving.
The Democratic Party has an opening to capitalize on McConnell's strategy by organizing in states they haven't paid much attention to.
McConnell appears to be recruiting senatorial candidates who are millionaires with business or military experience and are not MAGA-aligned.
This strategic move by McConnell involves sacrificing some races to concentrate resources on winning a majority in specific states.
The focus is on securing a majority rather than a landslide victory in the Senate races.
McConnell's political experience and calculated decisions are emphasized, contrasting with those who prioritize social media popularity over strategic planning.
The importance of proactive organizing and effort for the Democratic Party to seize the opportunities presented by McConnell's strategy.

Actions:

for political strategists, democratic organizers,
Organize and mobilize effectively in states traditionally overlooked by the Democratic Party (implied).
</details>
<details>
<summary>
2023-07-06: Let's talk about the funniest veto in history.... (<a href="https://youtube.com/watch?v=kTouNX6_BvM">watch</a> || <a href="/videos/2023/07/06/Lets_talk_about_the_funniest_veto_in_history">transcript &amp; editable summary</a>)

Governor Tony Evers creatively manipulates legislation in Wisconsin, cutting tax breaks for the rich and extending school funding to the year 2425, sparking legal battles and Republican discontent.

</summary>

"400 years of increased revenue."
"I'm not even mad. That's impressive."
"I'm sure that some would suggest that that is an abuse of executive power."
"Definitely some interesting news coming out of Wisconsin."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Governor Tony Evers used his super veto power in Wisconsin in a unique and humorous way.
He slashed a GOP tax cut for the rich from 3.5 billion down to 175 million.
The wealthy in Wisconsin are not getting the tax cuts that other people are receiving.
Evers vetoed the cuts for the highest tax brackets, showcasing an interesting use of power.
He creatively manipulated a part of the legislation that allocated extra revenue for schools.
By vetoing specific characters in the legislation, Evers extended the funding until the year 2425.
Some may view Evers' actions as an abuse of executive power, but Beau finds it impressive.
Republicans in Wisconsin are likely unhappy with Evers' innovative use of the veto.
Previous rulings have gone against similar veto maneuvers, but Evers' creativity stands out.
With a new liberal majority in the Supreme Court, there may be a chance for Evers' actions to hold despite expected legal challenges.

Actions:

for wisconsin residents,
Support legal battles against any abuse of power (implied)
</details>
<details>
<summary>
2023-07-06: Let's talk about guarding against confirmation bias.... (<a href="https://youtube.com/watch?v=uaRcNmnf9aE">watch</a> || <a href="/videos/2023/07/06/Lets_talk_about_guarding_against_confirmation_bias">transcript &amp; editable summary</a>)

Beau explains confirmation bias in hiring practices, urging deeper investigation beyond headlines to avoid falling for incomplete information, and encourages putting pronouns on resumes to reduce unknown factors in hiring decisions.

</summary>

"Everybody wants to confirm what they already believe."
"It confirmed what I believe, but that's not really what the information shows."
"The key element to avoiding confirmation bias and falling for bad information is to basically assume it's always going to be wrong and continue to look further into it."
"Read the source material; it's a very good guard against falling for and consuming bad information."
"It just links it to the presence of pronouns."

### AI summary (High error rate! Edit errors on video page)

Talks about confirmation bias and how it can lead people into information silos.
Shares a personal experience related to confirmation bias and hiring practices.
Mentions how confirmation bias can make people believe objectively false things.
Explains how the belief system about gender norms can impact hiring practices.
Mentions a report showing that people with they/them pronouns on their resumes were 8% less likely to get a call back.
Raises questions about whether this bias is specific to non-binary people or just about listing pronouns.
Urges deeper investigation beyond headlines to avoid falling for incomplete or misleading information.
Suggests that putting pronouns on resumes may induce fear in hiring managers leading to potential bias.
Advises against completely omitting pronouns from resumes but acknowledges the slight impact it may have on call-back rates.
Encourages putting pronouns on resumes even for those who fit neatly into traditional gender norms to reduce fear and unknown factors in hiring decisions.

Actions:

for job seekers, hr professionals.,
Read source material (guard against bad information) (suggested).
Put pronouns on resumes (reduce unknown factors in hiring decisions) (suggested).
</details>
<details>
<summary>
2023-07-06: Let's talk about Trump, Obama, Raskin, and McCarthy.... (<a href="https://youtube.com/watch?v=20lmL8YNx_w">watch</a> || <a href="/videos/2023/07/06/Lets_talk_about_Trump_Obama_Raskin_and_McCarthy">transcript &amp; editable summary</a>)

Former President Trump's actions led to a dangerous situation, revealing the consequences of inflammatory rhetoric and the need for the Republican Party to take a stand against radical elements.

</summary>

"The danger posed by those more energized and influenced is not limited to Democrats."
"The Republican Party needs to lead and not condone statements from radical members."
"The suspect seemed like someone who had already decided to do something and was just looking for a reason."

### AI summary (High error rate! Edit errors on video page)

Former President Trump posted what was said to be the address of former President Obama, leading to a man allegedly attempting to go after the family with weapons in his vehicle.
The suspect, living in his van, was already being sought by the feds for activities on January 6th and had made ominous social media posts.
Threats were made towards Representative Raskin and McCarthy, with the suspect entering an elementary school near Raskin's home.
The danger posed by those more energized and influenced is not limited to Democrats, and there's a lot of anger constantly being fed within certain segments of the Republican base.
The Republican Party needs to lead and not condone statements from radical members, as it could directly lead to tragedy.
The severity of the situation demands more media coverage, as it's not just a random incident but something with serious implications.
The suspect seemed like someone who had already decided to do something and was just looking for a reason, pointing towards the dangerous consequences of inflammatory rhetoric.

Actions:

for media consumers, political activists,
Contact media outlets to demand more coverage of this serious incident (suggested)
Start or join community dialogues on the impact of inflammatory rhetoric and political extremism (implied)
</details>
<details>
<summary>
2023-07-06: Let's talk about Smith, subpoenas, and Secretaries of State.... (<a href="https://youtube.com/watch?v=A3YiuNXy4zc">watch</a> || <a href="/videos/2023/07/06/Lets_talk_about_Smith_subpoenas_and_Secretaries_of_State">transcript &amp; editable summary</a>)

Arizona faces scrutiny for election interference as new information surfaces, indicating federal involvement and a broader investigation beyond public knowledge.

</summary>

"That's a wild way to run a witch hunt, I guess."
"When little elements start to surface in the media, the real reason may be that Smith's office already has the information."
"A lot is happening that isn't making the news."
"The order in which news breaks is not necessarily the order in which it occurred."
"Maybe had it for a month or more before we ever find out about it."

### AI summary (High error rate! Edit errors on video page)

Arizona is in the spotlight again due to new information emerging.
Similar to Georgia, there was a phone call in Arizona from Team Trump applying pressure regarding the 2020 election outcome.
People wondered if this new information might prompt the Department of Justice to intervene.
Recently, it was reported that the special counsel's office subpoenaed Arizona's Secretary of State's office in May and spoke with lawmakers.
It's suggested that the Arizona officials may have already talked to federal authorities before this information became public.
The order in which news breaks may not be the order of events that occurred.
The subpoenas received in May seem related to various issues, possibly including lawsuits post the 2020 election.
The scope of the investigation into election interference and January 6 seems broader than what is currently known.
There are significant developments happening behind the scenes that are not making headlines.
The surfacing of information in the media may indicate that the authorities already possess the information before it becomes public.

Actions:

for concerned citizens, activists,
Contact local representatives to demand transparency and accountability (implied)
Stay informed on developments in the Arizona election interference investigation (implied)
Support efforts to uphold election integrity (implied)
</details>
<details>
<summary>
2023-07-05: Let's talk about why they're signaling about the planes.... (<a href="https://youtube.com/watch?v=jVeTnttfHdA">watch</a> || <a href="/videos/2023/07/05/Lets_talk_about_why_they_re_signaling_about_the_planes">transcript &amp; editable summary</a>)

Beau explains how major world powers use different strategies to release information on defense technology to influence spending and production in competitor nations.

</summary>

"It is designed to influence spending and production in competitor nations."
"It's just a thought y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the purpose behind the release of information on secret planes by major world powers with defense research programs.
Major world powers have different approaches to releasing information on defense technology, with the US inducing uncertainty, China revealing products after development, and Russia exaggerating specs.
The US strategy is to create uncertainty to influence other world powers' spending and production decisions.
Russia's strategy of overestimating specs worked well during the Cold War but is less effective now.
Reveals how the US military-industrial complex responds to Russia's announcements by designing products to counter the overestimated specs.
Mentions a situation where Russia's high-tech equipment was countered effectively by the Patriot missile system because it was designed based on Russia's exaggerated specs.
Talks about how Russia's strategy now focuses on inducing fear with an overbearing attitude in their information operations.
Describes how the West sometimes releases false information to make other nations waste resources trying to duplicate impossible technologies.
Points out that the audience for these information releases is not the general public but other world powers, using the tactic of head games and trolling on an international level.
Comments on the difference in attitudes between the aviation world and the Navy regarding the release of information, attributing it to institutional culture differences.

Actions:

for defense policymakers,
Analyze and understand the different strategies major world powers use to release defense information (implied).
Stay informed about international defense strategies and their implications (implied).
</details>
<details>
<summary>
2023-07-05: Let's talk about the AG of AZ, SCOTUS, and Andrew Jackson.... (<a href="https://youtube.com/watch?v=V6nEaNriePQ">watch</a> || <a href="/videos/2023/07/05/Lets_talk_about_the_AG_of_AZ_SCOTUS_and_Andrew_Jackson">transcript &amp; editable summary</a>)

Beau talks about Attorney General Mays embodying Andrew Jackson's spirit against a Supreme Court decision on discrimination complaints in Arizona, doubting businesses' extreme opinions.

</summary>

"John Marshall has made his decision. Now let him enforce it."
"If any Arizonan believes that they have been the victim of discrimination, they should file a complaint with my office."
"It appears that Attorney General Mays has said the Supreme Court has made their decision, now let them enforce it."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the attorney general in Arizona, the Supreme Court, and the spirit of Andrew Jackson, which is not typically desirable.
Andrew Jackson is quoted as saying, "John Marshall has made his decision. Now let him enforce it," after disagreeing with a Supreme Court decision.
The current attorney general in Arizona, Mays, a former Republican, seems to embody Jackson's statement by encouraging complaints of discrimination in public accommodations.
Beau suggests that many viewers may not disagree with the attorney general's position on enforcing Arizona's public accommodation law.
Beau doubts that businesses, especially creative ones, hold bigoted opinions to the extent of discriminating against individuals based on race, gender, or other factors.
He believes that it is unlikely for this issue to escalate because he doesn't think many businesses share such extreme opinions.
If future complaints arise, Beau predicts that they may end up back in the Supreme Court due to Attorney General Mays' stance on enforcement.
The tone suggests that the Supreme Court lacks the ability to enforce decisions beyond bringing matters back to court, requiring broader federal government efforts for enforcement.
Beau hints at deeper philosophical considerations regarding the rule of law and constitutionality but leaves them for another day.
In closing, Beau shares his thoughts and wishes everyone a good day.

Actions:

for legal activists,
File complaints with the Attorney General's office in Arizona if you believe you have faced discrimination (suggested).
Stay informed about developments regarding discrimination complaints and legal actions in Arizona (exemplified).
</details>
<details>
<summary>
2023-07-05: Let's talk about optimism and doom.... (<a href="https://youtube.com/watch?v=kOBz0Mq7imo">watch</a> || <a href="/videos/2023/07/05/Lets_talk_about_optimism_and_doom">transcript &amp; editable summary</a>)

Beau clarifies he's a realist, not an optimist, rejecting doomerism and urging confidence to make a difference in challenging the status quo.

</summary>

"I'm not an optimist. I'm a realist."
"Being a doomer is giving up. It's surrender."
"If you want to change the world, I need you to adopt the absolutely unearned self-confidence of every middle-class white dude on Twitter."
"Being a doomer to the point that the world and the U.S. should have you at is just surrender to the status quo."
"Don't give up."

### AI summary (High error rate! Edit errors on video page)

Beau received a message questioning his optimism after discussing the Supreme Court not being as right-wing as perceived.
The message mentions recent rulings pushing the US closer to discriminatory practices against LGBTQ individuals and expanding Christian rights.
The message also criticizes Beau's mindset, suggesting his optimism may be due to his identity as a white Cishet man compared to the writer, who is trans and living through difficult times.
Beau clarifies that he considers himself a realist, not an optimist.
He dislikes the doomer mindset that accepts a bleak reality without seeking change.
Beau stresses the importance of not succumbing to doomerism, which he sees as surrendering to the status quo.
He encourages adopting confidence to make a difference and change the outcome.
Beau urges against expecting catastrophes constantly and advocates for long-term thinking when aiming for societal change.
He points out the need to challenge the doomer philosophy that can lead to inaction or mere talk without action.
Beau reminds the listener not to give up and to believe in their ability to effect change.

Actions:

for activists, change-makers,
Challenge the doomer mindset by actively seeking ways to make a difference (implied).
Refuse to accept the status quo and believe in your ability to effect change (implied).
</details>
<details>
<summary>
2023-07-05: Let's talk about SCOTUS and electoral politics.... (<a href="https://youtube.com/watch?v=ibxSfGwcDic">watch</a> || <a href="/videos/2023/07/05/Lets_talk_about_SCOTUS_and_electoral_politics">transcript &amp; editable summary</a>)

Beau explains how electoral politics and the Supreme Court are intertwined, showcasing the potential impact of future retirements on court decisions and underscoring the influence of electoral processes.

</summary>

"But you can't separate what the Supreme Court does from electoral politics."
"The Supreme Court, the makeup of the court, is a product of electoral politics."
"The average retirement age for a Supreme Court justice is 80 I think."
"You can't say that electoral politics has nothing to do with the Supreme Court."
"It's worth remembering that the two eldest justices are members of the block that are making the decisions most people watching this channel really disagree with."

### AI summary (High error rate! Edit errors on video page)

Explains the connection between the Supreme Court and electoral politics, addressing a message expressing hopelessness towards electoral politics and the Supreme Court's actions.
Points out that the average retirement age for Supreme Court justices is around 80, with Thomas and Alito being among the oldest.
Mentions the possibility of Thomas and Alito retiring soon, potentially impacting the court's decisions.
Emphasizes that the Supreme Court justices are products of electoral politics, even though individuals do not directly vote for them.
Notes that electoral politics influence the makeup of the court and that future retirements could shift the court's dynamics.

Actions:

for voters, activists, political enthusiasts,
Stay informed about the ages and potential retirements of Supreme Court justices (implied).
Engage in electoral politics to influence future appointments to the Supreme Court (implied).
Support candidates who prioritize judicial appointments that resonate with your values (implied).
</details>
<details>
<summary>
2023-07-04: Let's talk about when conversations aren't for the person you're talking to.... (<a href="https://youtube.com/watch?v=zEx3norUAvA">watch</a> || <a href="/videos/2023/07/04/Lets_talk_about_when_conversations_aren_t_for_the_person_you_re_talking_to">transcript &amp; editable summary</a>)

Beau returns home to confront a homophobic pastor, urging a difficult talk with mom not to change the pastor's mind but her church choice.

</summary>

"The sad fact is that preaching hatred and violence and bigotry in Jesus' name, that is a moneymaker."
"Not because you'll succeed, but because you'll fail."
"A conversation is not for the pastor. A conversation is for your mom."
"It's probably worth it, probably worth the time."

### AI summary (High error rate! Edit errors on video page)

Returned home to a semi-rural conservative town in Norcal after several years, attending church with his mom.
Mom's pastor was homophobic, transphobic, and misinformed about homelessness, blaming substance use and laziness.
Had a lengthy talk with mom about the pastor's sermon and was asked to speak with him directly.
Views engaging with someone promoting violence and hatred as challenging within an ultra-conservative religious context.
Recognizes the unlikelihood of changing the pastor's mindset but encourages the attempt regardless.
Suggests having the difficult talk not to succeed but to show the importance of the issue to his mom.
Emphasizes that the goal isn't to change the pastor but to influence his mom's church choice.
Warns of the pastor potentially leading his mom down a harmful path if not addressed early.
Advocates for having the tough and possibly ineffective talk as a way to protect his mom's beliefs.
Compares conversing with the pastor to responding to comments online: impact may not be immediate but can influence others.

Actions:

for family members, allies,
Talk to your family directly about harmful beliefs in their community (suggested)
Guide family towards more inclusive and welcoming environments (suggested)
</details>
<details>
<summary>
2023-07-04: Let's talk about a mutual problem with electoral politics.... (<a href="https://youtube.com/watch?v=hQ7WJ33U5ZY">watch</a> || <a href="/videos/2023/07/04/Lets_talk_about_a_mutual_problem_with_electoral_politics">transcript &amp; editable summary</a>)

Beau suggests bridging the gap between leftists and liberals by experiencing each other's preferred forms of civic engagement, aiming to show the effectiveness of mutual aid.

</summary>

"If she shows up, she will see that mutual aid is absolutely effective. It does work."
"You won't do the easiest thing, the thing that to her is effective and easy."
"But oftentimes people need to see that for themselves."
"Because if she sees it with her own eyes, she'll believe that too, right?"
"Remove it. that would be my suggestion."

### AI summary (High error rate! Edit errors on video page)

Introduces a debate between American leftists and liberals about civic engagement.
Describes a situation where a leftist friend is trying to convince a liberal friend to participate in mutual aid.
The liberal friend believes in electoral politics and doesn't see the effectiveness of mutual aid.
Beau suggests engaging in the other's preferred form of civic engagement to bridge the gap.
Emphasizes the importance of experiencing mutual aid firsthand to understand its effectiveness.
Encourages trying to change the liberal friend's perspective by involving them in mutual aid activities.
Advises removing barriers to understanding by allowing the liberal friend to see the impact of mutual aid.
Acknowledges the different perspectives between leftists and liberals on civic engagement.
Urges experimentation and understanding between friends with differing political views.
Recommends taking actions to bridge the gap and show the effectiveness of mutual aid.

Actions:

for friends with differing political views,
Experiment with each other's preferred form of civic engagement (suggested)
Invite the liberal friend to participate in mutual aid activities (implied)
Allow the liberal friend to see the impact of mutual aid firsthand (implied)
</details>
<details>
<summary>
2023-07-04: Let's talk about SCOTUS, unity, and a talking point.... (<a href="https://youtube.com/watch?v=qAcyEapd5QE">watch</a> || <a href="/videos/2023/07/04/Lets_talk_about_SCOTUS_unity_and_a_talking_point">transcript &amp; editable summary</a>)

Authoritarian tactics of divide and conquer within the LGBTQ community must be recognized and resisted, as unity is vital against oppressive forces.

</summary>

"Don't let them convince you that they're not coming for you too."
"There's always a next group for authoritarians, for people of that mindset, and you will not turn that authoritarian tiger into a vegetarian by feeding it your friends."

### AI summary (High error rate! Edit errors on video page)

Explains a talking point emerging about separating the T-Q from L-G-B within the LGBTQ community.
Notes that this idea is being pushed by those sympathetic to the right, suggesting that distancing from the T is better for the LGB.
Warns against falling for this tactic of divide and conquer used by authoritarian regimes throughout history.
Mentions a Supreme Court decision that wasn't just about the T, and urges not to be convinced that the targeting won't extend to others in the LGBTQ community.
Emphasizes that authoritarian groups always move on to target the next group, so division within the community is not a solution.
Stresses the importance of not being misled and manipulated by such tactics, referencing the Supreme Court ruling as evidence.
Concludes by warning against trying to appease authoritarian figures by sacrificing parts of the community.

Actions:

for advocates for lgbtq rights.,
Resist attempts to divide the LGBTQ community (implied).
Stay united and support all members of the LGBTQ community (implied).
</details>
<details>
<summary>
2023-07-04: Let's talk about 4th of July trivia.... (<a href="https://youtube.com/watch?v=tQiKsXCYTx0">watch</a> || <a href="/videos/2023/07/04/Lets_talk_about_4th_of_July_trivia">transcript &amp; editable summary</a>)

Beau presents a Fourth of July special video honoring eight historical figures tied to LGBTQ identities, sparking a reflection on American history and freedom of speech.

</summary>

"They were all confirmed or rumored by their contemporaries to be part of the LGBTQ community."
"How about you just pay homage to the men who gave you the freedom of speech you abuse daily."
"Granted. Happy Fourth of July."

### AI summary (High error rate! Edit errors on video page)

Introducing a Fourth of July special video focusing on eight figures central to the founding of the country.
Each figure is briefly discussed in terms of their historical significance and notable associations.
The figures include Alexander Hamilton, Pierre Charles L'Enfant, Frederick von Steuben, Lafayette, Charles Adams, John Lorenz, James Buchanan, and Thomas Morton.
Reveals a common thread among these figures - their confirmed or rumored ties to the LGBTQ community.
Suggests further research into Jamestown household structures and the origin of the term "log cabin Republicans."
Reads a message requesting a video celebrating American history without LGBTQ references.
Responds to the message by honoring the men who contributed to the freedom of speech, as requested.
Concludes with a wish for a happy Fourth of July.

Actions:

for history enthusiasts, lgbtq+ community.,
Research Jamestown household structures and the origin of "log cabin Republicans" (suggested).
Celebrate and honor LGBTQ figures in American history (exemplified).
</details>
<details>
<summary>
2023-07-03: Let's talk about secrets, planes, and social media.... (<a href="https://youtube.com/watch?v=wvq9x1gOtBY">watch</a> || <a href="/videos/2023/07/03/Lets_talk_about_secrets_planes_and_social_media">transcript &amp; editable summary</a>)

Lockheed Martin's social media posts spark speculation about new secret aircraft, likely the next-generation air dominance program, showcasing significant advancements in military technology.

</summary>

"The technological leap between what we have now and the next generation air dominance platform will probably be as big a leap as it was taken from the planes we had before."
"Lockheed Martin at this point has something that is far beyond the capabilities of anything that we actually know about yet."

### AI summary (High error rate! Edit errors on video page)

Lockheed Martin's social media posts have led to speculation about a new secret aircraft.
The silhouette tweeted by Lockheed Martin is likely the next-generation air dominance program's aircraft.
The next-generation air dominance program includes a sixth-generation manned plane and drones controlled by it.
The top speed of the next-generation air dominance plane is Mach 2.8, slower than the SR-71's Mach 3 cruising speed.
Beau believes Lockheed Martin is not teasing one secret plane but two different ones.
The technological leap to the next-generation air dominance platform will be significant, akin to the leap from previous aircraft like the F-117.
The public often learns about advanced aircraft technology with a significant time lag due to secrecy.
Lockheed Martin may possess technology far beyond current capabilities, potentially related to stealth technology.
The next-generation air dominance program involves drone aircraft controlled by a manned aircraft pilot using artificial intelligence.
The Air Force aims to receive these advanced aircraft by 2030, showcasing significant advancements in military technology.

Actions:

for aviation enthusiasts, defense analysts.,
Research and stay updated on advancements in military aircraft technology (implied).
Monitor official announcements from defense contractors for insights into future developments (implied).
</details>
<details>
<summary>
2023-07-03: Let's talk about legacy admissions and a meme.... (<a href="https://youtube.com/watch?v=E-QDbGvVcHM">watch</a> || <a href="/videos/2023/07/03/Lets_talk_about_legacy_admissions_and_a_meme">transcript &amp; editable summary</a>)

Supreme Court's affirmative action ruling led to scrutiny of legacy admissions, revealing how biases are manipulated to maintain privilege.

</summary>

"They're using your bias, your bigotry, to manipulate you so they can stay up on top."
"People who have less institutional power than you do are never the source of your institutional issues."
"Learn from this."
"You have more in common with the black guy down the road than you are ever going to have with your representative in DC."
"They're playing you. They're using your bias, your bigotry, to manipulate you so they can stay up on top."

### AI summary (High error rate! Edit errors on video page)

Supreme Court struck down affirmative action for college admissions, sparking talk about legacy admissions.
Legacy admissions offer preferential treatment to students whose parents attended the university.
Harvard is currently being challenged on legacy admissions.
Ivy League schools have a high percentage of legacy admissions, ranging from 25-35%.
Legacy admissions make students 45% more likely to be admitted compared to those who are not legacies.
Beau references a meme with a wealthy business owner, a white worker, and a black worker at a table with cookies to explain the concept.
Wealthier individuals, not marginalized groups, benefit from legacy admissions and make admissions less competitive.
Beau criticizes rich white individuals for misleading conservative white workers into believing that marginalized groups are taking opportunities away from them.
He urges conservatives to understand that rich individuals are often the ones manipulating biases and bigotry for their advantage.
Beau encourages people to see beyond these manipulations and recognize commonalities with others rather than being pawns in a cycle of manipulation.

Actions:

for conservative america,
Challenge legacy admissions policies (implied)
Educate others on the manipulation of biases by the privileged (implied)
</details>
<details>
<summary>
2023-07-03: Let's talk about a question of Russia's strength.... (<a href="https://youtube.com/watch?v=cee_0E142X4">watch</a> || <a href="/videos/2023/07/03/Lets_talk_about_a_question_of_Russia_s_strength">transcript &amp; editable summary</a>)

Exploring the questionable theory of Russia's hidden reserves and the lack of strategic rationale behind withholding them.

</summary>

"Let's just pretend that's true, okay?"
"Every time you hear it, ask yourself why? Why would they do this?"
"There is no logical reason for this to occur."
"The losses they have sustained are too great for this to be a faint."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Exploring a theory about Russia's hidden reserves that has resurfaced after recent events.
Hypothetically assuming Russia possesses a secret, strong reserve that has been concealed from US and Western intelligence for over a year.
Considering the theory that Russia is intentionally accepting significant casualties and refraining from using precision-guided munitions.
Speculating on the strategic purpose of Russia holding back this powerful reserve instead of using it to crush Ukraine.
Questioning the lack of clear strategic value in Russia withholding these supposed reserves and not utilizing them when needed.
Contrasting the current Russian military with the Soviets and the level of deception employed by both.
Emphasizing the lack of logical reasoning behind the theory of Russia's hidden reserves in light of the losses sustained.
Urging listeners to critically question and analyze the validity of this theory whenever it resurfaces.
Challenging the notion that there is any strategic benefit in Russia's apparent reluctance to deploy these reserves.
Concluding with a reminder to ponder the motive behind perpetuating this theory and its lack of substantiated reasoning.

Actions:

for analytical viewers,
Question the validity of theories and rumors before accepting them (implied).
</details>
<details>
<summary>
2023-07-03: Let's talk about SCOTUS, analogies, and reactions.... (<a href="https://youtube.com/watch?v=xfdDw9tm6HE">watch</a> || <a href="/videos/2023/07/03/Lets_talk_about_SCOTUS_analogies_and_reactions">transcript &amp; editable summary</a>)

Beau reacts to Supreme Court decision, clarifies misconceptions about Jim Crow analogy, and acknowledges justified fears due to vague ruling implications.

</summary>

"A whole lot of people don't really get it. A whole lot of them have no idea that the term Jim Crow was a slur at the time."
"If you are more familiar with that topic and you understand that it is way more than that, the use of that term might have elicited more of a response from you."
"It's not a good ruling, it's just not Jim Crow."
"The fears that they're expressing, they're justified."
"What people are worried about doesn't happen with this ruling, but this ruling set the tone."

### AI summary (High error rate! Edit errors on video page)

Reacts to a Supreme Court decision and the exaggerated reactions surrounding it on Twitter.
Addresses the analogy made to Jim Crow laws in relation to the ruling on certain businesses' ability to openly discriminate.
Points out the misuse of the Jim Crow analogy and the lack of understanding of its historical context.
Acknowledges the validity of the emotions behind the reactions to the ruling.
Expresses concerns about the vague and potentially broad implications of the Supreme Court decision.
Contrasts the ruling with Jim Crow laws, clarifying that it won't lead to overt discrimination signs like in the past.
Notes that the ruling's real impact lies in setting a tone that could lead to more oppressive laws in the future.
Encourages a deeper understanding of the ruling beyond initial emotional responses.
Concludes by expressing empathy towards those feeling fear and anxiety due to the ruling.

Actions:

for twitter users, trans community,
Educate yourself on the historical context of Jim Crow laws and their impact on society (implied).
Engage in constructive dialogues to deepen understanding of legal decisions and their implications (implied).
Support marginalized communities through advocacy and awareness-raising efforts (implied).
</details>
<details>
<summary>
2023-07-02: The roads to emergency preparedness questions.... (<a href="https://youtube.com/watch?v=O0_K1MT3JxI">watch</a> || <a href="/videos/2023/07/02/The_roads_to_emergency_preparedness_questions">transcript &amp; editable summary</a>)

Beau shares insights on emergency preparedness, from packing essentials based on outdoor experience to persuasive tactics for urban prepping, stressing the importance of knowledge over equipment.

</summary>

"Knowledge weighs nothing. It's much easier to carry."
"When did Noah build the ark? Before the rain."
"The National Guard has to come in so often because people aren't prepared."
"Knowledge is lighter than any equipment you can get."
"Don't take criticism from people you wouldn't take advice from."

### AI summary (High error rate! Edit errors on video page)

Shares insights on the progression of packing for emergencies from excessive equipment to minimal essentials based on experience outdoors.
Advises on keeping survival food and water in Arizona's extreme heat, recommending specific products for sustained high temperatures.
Addresses urban prepping for economic fluctuations, focusing on alternative cooking methods and supply accessibility in dense populations.
Suggests persuasive tactics to encourage friends and family to prioritize emergency kits without sounding alarmist.
Emphasizes the importance of being prepared with basic supplies in case of natural disasters or utility failures.
Explains the significance of a knife in emergency kits as an indispensable tool for various tasks.
Stresses the value of knowledge over equipment in survival situations, advocating for downloading survival manuals and guides.
Recommends incorporating rubber bands, super glue, and WD-40 in emergency kits as versatile and useful items.
Encourages acquiring a US Army Survival Guide and regional edible plant identification book for emergency preparedness.
Talks about the mentality behind leaving some items in packaging for improvisation during survival situations.

Actions:

for emergency preppers,
Contact local emergency preparedness groups for guidance and support (suggested).
Purchase survival food and water suitable for extreme temperatures (exemplified).
Encourage friends and family to prioritize emergency kits using persuasive tactics (implied).
Incorporate rubber bands, super glue, and WD-40 into emergency kits for versatility (suggested).
</details>
<details>
<summary>
2023-07-02: Let's talk about what Republicans can learn from Lindsey Graham.... (<a href="https://youtube.com/watch?v=gzmJuGQKrtk">watch</a> || <a href="/videos/2023/07/02/Lets_talk_about_what_Republicans_can_learn_from_Lindsey_Graham">transcript &amp; editable summary</a>)

Senator Graham's booing incident in South Carolina underscores the danger of the Republican Party's shift towards Trump's personality over policy, urging a separation for those aspiring for more than blind allegiance.

</summary>

"Courting them is a danger to the Republican Party because eventually it will be Trump who is calling all of the shots."
"Trump is a liability to the Republican Party. They keep trying to ignore it."
"Even if these indictments are successful, it's not going to change their opinion of him."
"Supporting Trump will not necessarily get you their vote and supporting Trump and his policies in that far-right agenda will absolutely lose you the moderates."
"Don't confuse social media clicks with votes. They only support one person and it's not you."

### AI summary (High error rate! Edit errors on video page)

Senator Graham, once an off-and-on supporter of former President Trump, is currently supporting him and making campaign appearances.
Despite endorsing Trump and appearing in his support in South Carolina, Graham was booed off the stage.
Beau warns the Republican Party to take note that Trump's faction is no longer about policy and principle but solely about Trump's personality.
Courting Trump's faction poses a danger to the Republican Party as they have shifted from traditional Republican values to following Trump exclusively.
The authoritarian embrace of Trump's faction since 2015 has the potential to take down long-standing Republican members, like Lindsey Graham in South Carolina.
Trump is seen as a liability to the Republican Party, but many refuse to acknowledge this reality.
Those deeply entrenched in supporting Trump are unlikely to change their views, even if indictments are successful against him.
These individuals are now considered more Trumpist than Republican, supporting Trump alone rather than the party's policies.
Supporting Trump may not guarantee votes from his faction, and adopting his far-right agenda could alienate moderates.
Beau warns against mistaking social media support for actual votes and urges for a separation of Trump from the Republican Party.

Actions:

for republican party members,
Recognize the shift towards personality over policy within the Republican Party and advocate for a return to core values (implied).
Take steps to separate Trump from the Republican Party to avoid further alienation of moderates (implied).
</details>
<details>
<summary>
2023-07-02: Let's talk about Trump, AZ, and another call.... (<a href="https://youtube.com/watch?v=Si12A2E2L68">watch</a> || <a href="/videos/2023/07/02/Lets_talk_about_Trump_AZ_and_another_call">transcript &amp; editable summary</a>)

Beau talks about allegations of Trump or his surrogates reaching out to the Arizona governor for votes, raising questions of criminal investigation and potential election interference, speculating on the timing and involvement of investigators.

</summary>

"Hey, find me some, find me some votes."
"Maybe something happened to make them feel comfortable talking about it in public."
"Do we know that? No, no we don't."
"They will be discussing it with them soon."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Talking about Arizona, Trump, and a breaking story similar to the famous phone call in Georgia.
Allegations that Trump or his surrogates reached out to the Arizona governor asking for votes.
Questions arise about criminal investigation and potential election interference.
Speculation on why the story is surfacing now after a long time of silence.
Possibility that media got wind of the story because the feds were already aware and involved.
Uncertainty on whether the individuals involved have already spoken to investigators.
Likelihood of investigators getting involved due to the potential pattern of reaching out to state officials to influence outcomes.
Mention of a recent interview with Raffensperger relating to election interference.
Implication that if not already, individuals will soon be discussing the matter with investigators.
Concluding with a thought on the unfolding situation.

Actions:

for news consumers,
Contact local representatives for updates on the situation (implied)
Stay informed about developments in the story (implied)
Support transparency and accountability in political processes (implied)
</details>
<details>
<summary>
2023-07-02: Let's talk about Kremlin games and generals.... (<a href="https://youtube.com/watch?v=7K9ToWbCdyY">watch</a> || <a href="/videos/2023/07/02/Lets_talk_about_Kremlin_games_and_generals">transcript &amp; editable summary</a>)

Beau provides insights on Kremlin games, a detained general, and split in Russian military loyalty, causing morale issues and headaches for Putin.

</summary>

"Detained doesn't always mean detained, remember?"
"The fallout from his little march towards Moscow, it is not over."
"The morale issues, they're starting."
"Whereas with these two, they just haven't been in the public eye."
"It seems as though they have sided more with Wagner."

### AI summary (High error rate! Edit errors on video page)

Analyzing games in the Kremlin and their implications.
A detained general in Russia is being treated well, not in jail.
Putin's concern about potential coup involvement.
Speculation about two other Russian leaders being detained.
Lack of evidence supporting the detention of the two leaders.
Situation under a bridge in Ukraine involving Russian forces.
Split between military bloggers and the Ministry of Defense.
Criticisms of Russian leadership and morale issues.
Russian troops criticized for lack of adaptability.
Fallout from recent events causing headaches for Putin.

Actions:

for political analysts, kremlin watchers.,
Monitor developments in Russia and Ukraine (suggested).
Stay informed about international relations and their impact (suggested).
</details>
<details>
<summary>
2023-07-02: Let's talk about Chris Christie's campaign.... (<a href="https://youtube.com/watch?v=zTjykgDYspc">watch</a> || <a href="/videos/2023/07/02/Lets_talk_about_Chris_Christie_s_campaign">transcript &amp; editable summary</a>)

Beau unpacks Chris Christie's presidential run, focusing on his goal to crush Trump and steer the Republican Party towards rationality.

</summary>

"I truly believe he is running an entire presidential campaign out of spite and I think that's cool."
"The Republican Party, they have to separate from Trump."
"It's more of a single issue which is Crush Trump."

### AI summary (High error rate! Edit errors on video page)

Explains the rationale behind Chris Christie's run for the presidency.
Believes Christie's main goal is not to win but to crush Trump and salvage the Republican party.
Suggests Christie's campaign is driven by spite and pettiness towards Trump.
Thinks that Christie aims to steer the Republican Party away from authoritarianism and outdated ideas.
Points out that Christie's focus is on starting a separation from Trump rather than winning the presidency.
Speculates that Christie is supported by larger-name Republicans who want to move the party towards a more rational direction.
Views Christie's candidacy as a way to influence the party rather than a serious bid for the presidency.
Notes that Christie's primary objective is to break ties with Trump and lead the party towards a more sensible path.

Actions:

for political enthusiasts,
Support efforts to steer the Republican Party towards more sensible and rational policies (implied).
Advocate for a separation from Trump within the Republican Party (implied).
Engage with larger-name Republicans supportive of moving the party in a more rational direction (implied).
</details>
<details>
<summary>
2023-07-01: Let's talk about a year on Mars.... (<a href="https://youtube.com/watch?v=IOdW1eY1FW8">watch</a> || <a href="/videos/2023/07/01/Lets_talk_about_a_year_on_Mars">transcript &amp; editable summary</a>)

Four NASA volunteers simulate living on Mars for 378 days to test cognitive function and physical performance, paving the way for future missions to the red planet.

</summary>

"They're taking steps to actually get us to Mars and have people on the surface."
"It's pretty exciting stuff."
"It's straight out of science fiction."

### AI summary (High error rate! Edit errors on video page)

Four NASA volunteers will spend 378 days in a Mars-mimicking habitat for monitoring cognitive function and physical performance.
The habitat is designed to provide challenges, with scenarios like equipment failures and communication issues.
The goal is to simulate living on another planet before actually sending people there.
The four volunteers have different backgrounds including emergency medicine, structural engineering, research science, and microbiology.
This mission is the first of three planned, with the next ones scheduled for 2025 and 2026.
NASA is taking steps to eventually have people on Mars, starting with these simulated missions.
Updates on equipment failures, simulations, and responses will likely be shared with the public by NASA.
Beau finds the mission exciting, akin to something from science fiction.
He hints at providing further updates if he finds more information on the mission.
The mission indicates progress towards humanity becoming multi-planetary.

Actions:

for space enthusiasts,
Follow NASA's updates on Mars simulation missions (suggested)
</details>
<details>
<summary>
2023-07-01: Let's talk about SCOTUS, student debt, and second bites.... (<a href="https://youtube.com/watch?v=pUT2r1Z2Wbo">watch</a> || <a href="/videos/2023/07/01/Lets_talk_about_SCOTUS_student_debt_and_second_bites">transcript &amp; editable summary</a>)

The Supreme Court struck down Biden's student debt relief plan, prompting a new plan under a different law and raising questions about strategy and future hurdles.

</summary>

"The short version of this, too late, is we have to see the details."
"I think the Biden administration was counting on an ideological decision from the Supreme Court and they have a contingency plan."
"It seems like the second one [plan] would probably have a better chance because they have a very clear understanding of what this exact court wants."
"If issues arise with the Supreme Court again, the matter might need to go through Congress."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court struck down Biden's student debt relief plan, leading to the emergence of a new plan under a different law.
Beau questions whether the Supreme Court's decision was correct, expressing his belief that Biden was right but acknowledging his opinion doesn't matter in this context.
He notes the lack of sufficient information to form an opinion on the new plan, as it is still a rough sketch with critical details yet to be revealed.
The quick emergence of the new plan indicates prior preparation and effort put into it before the Supreme Court decision was released.
Beau speculates that Biden may have strategically planned for the first shot to miss, allowing for a second attempt with a more favorable law, possibly anticipating the Supreme Court's decision.
The new plan may impact non-discretionary income significantly, potentially relieving individuals of student debt payments.
Beau stresses the importance of waiting for the final presentation of the plan before making judgments, as the early details may not necessarily translate into the final rules.
He criticizes Republicans for supporting breaks for billionaires while being opposed to relief for student debt, considering it counterproductive.
The Biden administration seems to have a contingency plan in response to the Supreme Court decision, aiming to tailor the new student debt relief within the court's boundaries.
If issues arise with the Supreme Court again, the matter might need to go through Congress due to the Democratic Party lacking votes in the House.

Actions:

for policy analysts, political activists,
Stay informed about the developments in Biden's student debt relief plan and its implications (implied)
</details>
<details>
<summary>
2023-07-01: Let's talk about Russia and a general.... (<a href="https://youtube.com/watch?v=Ht4nVZHXi1A">watch</a> || <a href="/videos/2023/07/01/Lets_talk_about_Russia_and_a_general">transcript &amp; editable summary</a>)

Beau examines recent events in Russia, from the detention of a high-ranking official to potential military consequences, suggesting a shift towards authoritarianism and increased paranoia.

</summary>

"It's palace intrigue stuff, not legal stuff."
"No plan survives first contact with the enemy."
"He is going to start behaving more and more like just an authoritarian goon."
"He's losing face, he's walking around saying he's the king. You gotta tell people you're not."
"So the paranoia will probably continue to spiral."

### AI summary (High error rate! Edit errors on video page)

Analyzing recent events in Russia and the potentially misleading coverage.
Speculating on the future implications of recent developments in Russia.
Mentioning the detention of a high-ranking Russian official, General Serevikhin.
Pointing out that in Russian society at this level, actions are often extrajudicial and politically motivated.
Exploring the reasons behind General Serevikhin's detention and the ambiguous nature of such actions.
Hinting at the possibility of a political purge starting in Russia.
Expressing concern over the impact of these actions on military morale and discipline.
Emphasizing the consequences of rigid military command structures in combat situations.
Noting that Putin's actions could worsen the already low morale in the Russian military.
Speculating on Putin's potential shift towards more authoritarian behavior.
Suggesting that recent events may reveal Putin's vulnerabilities and lead to increased paranoia.
Concluding with a reflection on the potential future developments in Russia.

Actions:

for russia watchers,
Monitor developments in Russia and stay informed about the situation (suggested).
Support organizations advocating for human rights and democracy in Russia (suggested).
</details>
<details>
<summary>
2023-07-01: Let's talk about McCarthy getting pushback on Trump.... (<a href="https://youtube.com/watch?v=_IYMKEc1ZJU">watch</a> || <a href="/videos/2023/07/01/Lets_talk_about_McCarthy_getting_pushback_on_Trump">transcript &amp; editable summary</a>)

McCarthy's steps to distance from Trump are vital for the Republican Party's future, as they need to comprehend the damage he could cause if continued placated.

</summary>

"The Republican Party has to get rid of Trump. They have to distance themselves."
"Social media clicks and likes are not votes."
"They created that appetite in Trump, then they spilled a glass of water on him."

### AI summary (High error rate! Edit errors on video page)

McCarthy recently suggested that Trump may not be the strongest candidate to run in 2024.
McCarthy's suggestion was a big step in his campaign to distance himself and the Republican Party from Trump.
McCarthy quickly started to walk back his comments, realizing the significance of his statement.
Despite walking back, McCarthy now claims that Trump is stronger than in 2016.
Trump is a twice-impeached, twice-indicted person who lost his last election, making him not a political powerhouse anymore.
The Republican Party needs to comprehend that they have to distance themselves from Trump to remain viable.
Social media clicks and likes are not equivalent to votes, which led to the party's poor performance in the midterms.
Keeping Trump close has created a hunger for power and will lead to more problematic figures like him.
McCarthy must continue efforts to distance himself from Trump for the Republican Party's and his own benefit.
Allowing Trump's influence to persist will only result in more damage and erosion of faith in the Republican Party.

Actions:

for politically engaged citizens,
Contact McCarthy to urge him to continue distancing himself from Trump (suggested)
Support efforts within the Republican Party to distance themselves from Trump (exemplified)
Stay informed and vocal about the negative impact of continuing to support Trump within the party (exemplified)
</details>
