---
title: Let's talk about the Navy and names....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Xj8TwLVNZ-s) |
| Published | 2023/07/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US Navy is under scrutiny for its historical names following the lead of other military branches.
- Generals who joined the Confederacy are being stripped of their names from military installations.
- Concerns arise as black fathers question ships named after segregationists where their sons serve.
- Unlikely for the Navy to change ship names due to tradition, despite ongoing movement and discourse.
- The renaming of army bases took years of debate before action was taken.
- Possibility that ship names won't be passed on to new ships upon retirement.
- Some sailors serving on these ships may retire before the ship itself.
- Navy's tradition and historical importance in naming ships make change unlikely.
- Naming after historical figures due to their significance in Navy's history might deter name changes.
- Beau suggests starting with senators for any petition or action towards change.

### Quotes

- "I don't think you're wrong."
- "Everything's impossible until it's not."
- "It seems really unlikely that they change the name of a major ship."

### Oneliner

The US Navy faces scrutiny for historical names, unlikely to change despite ongoing movement and discourse.

### Audience

Families of service members

### On-the-ground actions from transcript

- Petition senators for ship name changes (suggested)
- Join the movement to change ship names (implied)
- Advocate within the Navy for renaming ships (exemplified)

### Whats missing in summary

The full transcript provides deeper insights into the historical context and challenges faced in renaming Navy ships named after segregationists.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the US Navy.
And what's in a name?
And when it's time for change?
Because there's an expectation that the Navy may follow the
lead of other branches.
If you've missed it, the US military is
altering a lot of names.
all the way up to the names of major military installations.
They are shedding the names of generals
who decided to join the Confederacy.
And they're being renamed something
a wee bit more appropriate, given
the incredibly diverse nature of the US military.
I've gotten two questions that are remarkably similar.
Both came from fathers who have sons on ships. Both of them are black and their
sons are serving on ships named after ardent segregationists. One is asking,
you know, am I wrong for feeling like kind of slapped in the face over this?
No, I don't think you're wrong. And both are asking if it's likely that the ships
have their names changed. The Navy is different,
probably not. Realistically, I think it's unlikely. Now, one of them asked, you know,
how would I go about starting that conversation, petitioning to make that
happen? The conversation is happening. In fact, the Navy Times actually ran an
article I think about the Stennis and saying it was time to change the name of
that ship. It seems unlikely to happen though. The movement is there, the
discussion is occurring. It's worth remembering that the discussion about
renaming army bases took years, years before it actually happened. It seems
unlikely that the Navy would take that step. I think the more likely outcome is
that when the ship retires the name isn't passed on to a new ship. I think
that's probably what's going to happen with these.
The problem with that
is that
I know with at least two of them
if a
the sailor
first assignment was on that ship
brand new to the Navy
the sailor would retire before the ship will.
I think they've got
almost twenty five years
left in their anticipated service.
It's not impossible. There's a movement
to change the names, but it's the Navy.
They are very tradition-oriented in changing the name of the ship,
especially ships that are, you know,
central major ships, it seems unlikely.
I'm not saying, you know, don't try, you know, don't write your up or whatever route
you want to go, but it just, that one seems like one that probably isn't going to be one.
especially given the fact that the reason these ships were named after these people
wasn't because of their positions on that. It was because they were incredibly important
to the creation of the modern Navy. So the Navy's not going to want to walk away from that.
that. They're still going to want to remember those people even if they don't want to honor
them. If you're going to try, I would start with your senator rather than your representative.
But everything's impossible until it's not, that one kind of seems like a losing fight.
saying that it's right. I'm just saying given the institution of the Navy, it
seems really unlikely that they change the name of a major ship or in this
case at least two major ships. But I don't think there will be a future ship
named after them. I think the name will die. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}