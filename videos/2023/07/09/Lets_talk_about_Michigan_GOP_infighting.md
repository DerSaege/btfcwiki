---
title: Let's talk about Michigan GOP infighting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_yyGP1xIFzo) |
| Published | 2023/07/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of the state of the Republican Party in Michigan and its implications for 2024.
- Discord and struggles within the Republican Party, characterized by infighting.
- Recent literal infighting incident during a closed-door meeting of the State Republican Party.
- Upset Republicans outside the closed-door meeting leading to a physical altercation involving a door handle, a single-fingered salute, and a chair.
- Involvement of law enforcement and hospitalization of an individual as a result of the altercation.
- Historical disputes within the Michigan State Republican Party.
- Comparison with the well-organized Democratic Party in the region.
- Importance of unity for the Republican Party in Michigan in order to compete effectively in 2024.
- Warning that failure to unify could lead to regrets for the Republican Party in the upcoming election.
- Final message encouraging listeners to have a good day.

### Quotes

- "The blue wall kind of seems to be back and for Republicans, Michigan is going to be incredibly
 	important in 2024."
- "If the Republican party does not find some way to kind of come together on this, it seems pretty likely
 	that come 2024, well, they'll be kicking themselves."

### Oneliner

The state of the Republican Party in Michigan, marked by literal infighting, threatens their success in 2024, against a well-organized Democratic Party.

### Audience

Political analysts, Michigan voters.

### On-the-ground actions from transcript

- Reach out to local Republican Party officials to advocate for unity within the party (suggested).
- Get involved in local political events and activities to support a cohesive Republican Party in Michigan (implied).

### Whats missing in summary

Insights into the specific issues causing discord within the Michigan State Republican Party and potential strategies for unification.

### Tags

#RepublicanParty #Michigan #Infighting #2024Election #Unity


## Transcript
Well, howdy there, Internet people. Let's bow again. So today we are going to talk a little
bit about the state of the Republican Party in Michigan and what it means for 2024.
If you have missed it, generally speaking there has been a lot of discord. Some might say that
that the Republican Party is struggling with itself.
There's been some disputes.
I would imagine that many would classify it as Republican infighting.
Now normally when a political commentator says infighting, it is metaphorical.
to some recent developments and a little bit of light reporting. In this case it's
literal. Now I'm going to kind of go through this I'm going to do it vaguely
because it is still very early on in this coverage so keep in mind order of
events may change so on and so forth but from what I have been told the State
Republican Party there was holding a meeting and it was a closed-door meeting
for committee members. There were some Republicans who were outside of those
doors who were kind of upset about having to listen through a door. At some
point a door handle got jiggled which led somebody from inside to kind of come
over and investigate. They might have received a single-fingered salute
through a window that may have led to a door being opened which could have led
to somebody being kicked in the groin. There was also a chair involved. Long
story short, it appears that law enforcement got involved and somebody
wound up going to the hospital. So there's that. The issues within the State
Republican Party in Michigan, this isn't new. I mean this, it being literal in
fighting is new, but there has been a lot of dispute. The real issue for the
Republican Party is that the other, the Democratic Party in this area, in this
geographic region, they kind of have their stuff together. The Republican
party is less than unified. The blue wall kind of seems to be back and for Republicans,
Michigan is going to be incredibly important in 2024. If the Republican party does not find some
way to kind of come together on this, it seems pretty likely that come 2024, well, they'll
be kicking themselves.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}