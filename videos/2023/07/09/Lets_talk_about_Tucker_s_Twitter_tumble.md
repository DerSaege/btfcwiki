---
title: Let's talk about Tucker's Twitter tumble....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7XItmSb07K8) |
| Published | 2023/07/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tucker moved from Fox to Twitter, sparking a lot of speculation.
- Tucker's impact on Twitter is less extreme compared to TV, as Twitter is already inflammatory.
- Musk's moves on Twitter may have incentivized more inflammatory behavior.
- Numbers show a decline in viewership for Tucker's Twitter episodes.
- Views steadily declined: announcement - 137 million, episode one - 120 million, episode two - 60 million, and so on.
- Another set of numbers indicates a decline in people watching Tucker's videos for more than two seconds.
- Concerns about Tucker becoming more extreme without Fox seem unfounded due to the declining viewership.
- The decline could signal a wider issue within the conservative movement.
- Viewers of Fox, considered moderate Republicans, may be moving away from extreme factions.
- People exposed to extreme content on Fox may not actively seek it out, a potentially positive sign.

### Quotes

- "Twitter is Thunderdome."
- "Just because somebody viewed the tweet doesn't mean they watched any of the video."
- "There are a lot of people who really express a lot of concern that without Fox holding him back, that he was going to be even more of a right-wing cultural juggernaut."
- "It definitely appears that there's a decline and it's pretty substantial."
- "This could be a sign that people are moving away from the more extreme factions of the Republican Party."

### Oneliner

Tucker's move to Twitter shows declining viewership, potentially signaling a shift away from extreme conservatism.

### Audience

Political analysts, social media users

### On-the-ground actions from transcript

- Analyze viewership trends to understand political shifts within the conservative movement (implied).
- Encourage critical consumption of media content and its potential impact on political ideologies (suggested).

### Whats missing in summary

Insights on the overall impact of Tucker's declining viewership on conservative media landscape.

### Tags

#ConservativeMovement #TuckerCarlson #TwitterImpact #PoliticalShifts #MediaConsumption


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Tucker on Twitter
and how things are going.
You know, after the host left Fox for whatever reason,
he began putting episodes on Twitter.
There was a lot of discussion about it
and how it was going to play out.
My thing was, hey, he was the most extreme thing on TV.
On Twitter, he's not even close.
Twitter is Thunderdome.
He doesn't have the same impact for being inflammatory,
because all of Twitter is inflammatory.
During this period, Musk made a lot of moves that intentionally or unintentionally incentivized less-than-ideal behavior
and encouraged people to be even more inflammatory.
So, we're going to run through his ratings because there were a lot of people that kind of suggested that him going to
Twitter was just going to make him off the chain and he's going to be even more of an impact  and have even more reach,
and all of that stuff.  Okay, so the first set of numbers
we're gonna go through here,
these are for the number of people that viewed the post,
that viewed the actual tweet.
Now, it's important to remember
that just because somebody viewed the tweet
doesn't mean they watched any of the video,
any of the actual episode.
So we're going to run through those real quick.
All right.
So the announcement got 137 million.
Episode one, 120 million episode two, 60 million episode three.
This was when Trump got indicted numbers mounts back up 104 million, uh, episode
four, 32 million episode five, 17 million episode six, 32 million.
Not sure what happened there.
Episode 7, 15 million. Episode 8, 8 million. I'm obviously rounding these.
So it is an incredibly steady decline. We have another set of numbers that I have
not verified, but there's also the number of people that watched two seconds of
of his video. You have the announcement
28 million almost 29 million, episode 1 26 million,
episode 2 13 million, episode 3 again Trump's indictment
22 million, episode 4 10 million, episode 5 6 million, episode 6
8 million, 7 5 million, and 8
3 million. The numbers are steadily
declining. There are a lot of people who really express a lot of concern that without Fox
holding him back, that he was going to be even more of a right-wing cultural, you know,
just juggernaut. That doesn't seem to be the case. It definitely appears that, I don't want to say
the views are collapsing, which is how it's being framed, but there's definitely a decline
and it's pretty substantial, which could be indicative of a wider issue within the conservative
movement. This could be a sign that people are moving away from the more
extreme factions of the Republican Party, of the conservative movement. You have
to remember that those people who watched Fox, many of them viewed
themselves as moderate Republicans. They were exposed to this and because of that
many of them believed it and it helped energize them in what I would suggest is
a negative way. But they don't care enough about consuming that type of
content to go try to find it. That's a hopeful sign. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}