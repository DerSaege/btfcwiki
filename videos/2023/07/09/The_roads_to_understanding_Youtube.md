---
title: The roads to understanding Youtube....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hQVE6gzmF-0) |
| Published | 2023/07/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the new YouTube studio and the challenges with framing for T-shirt views.
- He shares insights into potential video ideas, like covering historical events day by day or creating a fictional country paralleling real-world events.
- The reasoning behind having two separate channels is explained based on audience preferences for content length.
- Beau explains his process of selecting shirts with multiple meanings for his videos.
- On the topic of tags, Beau admits to not using them due to initial ignorance and reluctance to change.
- He delves into the new algorithm's primary purpose of keeping viewers on YouTube for extended periods.
- The decision to separate long-form content into a different channel is justified based on audience engagement metrics.
- Beau discloses the minimal impact of leaving Twitter on viewership and the importance of utilizing social media for new creators.
- He sheds light on the financial aspects of being a full-time YouTuber and the subscriber count required for sustainability.
- Beau's preferences for Halloween episodes over Christmas ones and his thoughts on AI overtaking creators are shared.

### Quotes

- "I thought the new studio meant we'd get full T-shirt views."
- "Algorithm is always the same."
- "Content is what matters."
- "I don't see that happening with me. I really enjoy this."
- "Y'all have a good day."

### Oneliner

Beau shares insights on YouTube studio challenges, video ideas, channel separation, shirt selection, algorithm insights, Twitter impact, financial sustainability, holiday episodes, and AI concerns.

### Audience

Content creators

### On-the-ground actions from transcript

- Experiment with new video ideas inspired by historical events or fictional concepts (suggested).
- Choose shirts with multiple meanings for metaphors in videos (exemplified).
- Utilize social media platforms to share and grow your content (implied).

### Whats missing in summary

Insights on balancing work-life with YouTube commitments and maintaining audience engagement.

### Tags

#YouTube #ContentCreation #AlgorithmInsights #SocialMedia #FinancialSustainability


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we are going to talk about YouTube.
Questions about YouTube.
I don't know if this means how to start a YouTube channel or
general questions about YouTube.
I don't know.
We'll find out here in a second.
OK.
I thought the new studio meant we'd get full T-shirt views.
Is that going to happen?
you're getting fuller t-shirt views. The framing still doesn't look right. I'm
experimenting with this new thing that YouTube is offering where there's a link
to the shirt and so you can see it. You click on it and you can see it. It was
something that I was actually in the experiment that YouTube ran to see if
they wanted to actually do this for real and people seemed to like that. The
problem is not all the shirts are in there but you'll have that and then again
as we do more of the like actual on the road stuff the hands-on stuff it's shot
from different angles it's framed differently you'll see more then. How
many people in the comment section do you know personally? If by personally you
mean like met in the physical world? I don't know, 20 maybe? Not many. A lot of
you, I've seen your names and screen names pop up down there so often. I feel
like I know you. Whatever the opposite of a parasocial relationship is, that. They
say creative people will have a list of new projects in their head all the time.
I have the drive, but not the idea for an overall channel theme.
Give me ideas.
Well, I don't know what you want to do.
So just some general ideas that I've had.
Covering a major historical event day by day
as it happened from the perspective of somebody
who is experiencing it probably didn't make any sense, imagine doing, I don't know, World War II
as an example, and you're covering the events of that date in order from the perspective of
somebody who was there at that time. So you don't know how it ends. You don't...
You get the story told in real time, and by that I mean on December 7th, you're covering
December 7th.
I think that that would be an incredibly useful tool for people to understand major events
and get full context rather than just the snippets, and it could be made to be entertaining.
Another one, one that I've always wanted to do, was make up a country.
Like completely fictional country.
But the events in that country that you're covering parallel the events in the United
States, and you cover it as if it were a country in Central America.
the same language that US media would use to describe the situation.
You know, security services targeting ethnic minorities.
And put it into that framing, I think it might help people skip past a little bit of American
exceptionalism.
There's a lot of little stuff like that.
of ideas, not a lot of time.
But yeah, I think most people who are on YouTube have a list of projects they want to do but
don't have time to do.
Why do you have two channels?
I don't understand why you wouldn't put them on one.
The algorithm told me to.
So on the main channel, basically anything over 13 minutes, it doesn't gain traction
there because it's a different audience who doesn't want, they want the soundbites and
not the soundbites.
They want the shorter viewpoint that is singular to one topic, very quick, very concise and
that's what they're looking for.
Doing the longer stuff, it's just not what that audience is looking for.
There's overlap, obviously, but it's not the same audience.
And so those longer videos on that channel, they don't do as well, so they end up dragging
the channel down, if that makes sense.
And for those who have just signed up for this channel, I have this whole other channel
where I do daily coverage of political events
and stuff like that in three to eight-minute videos.
Do you buy your shirts for specific meanings,
or do you craft specific videos around shirts?
Yes.
Yes, the answer is yes.
So I go hit thrift stores, or big box clearance tracks,
or stuff like that.
And I look for shirts that normally
have multiple meanings that I can
use for metaphors or symbolism in videos later.
And then I choose the right one for the video.
Why don't you use tags?
What are tags?
No, I know they exist.
Honestly, when I started on YouTube,
I had no idea what I was doing, and now I'm
afraid to change anything.
I never used them before, and I don't use them now.
Yeah, so that's the real reason.
Didn't know they were there initially.
OK.
What do you think of the new algorithm?
Algorithm is always the same.
It always has the same purpose, that is to keep you on YouTube as long as humanly possible.
As long as you understand that all of the minor changes, they don't matter as much.
I know a lot of people put a lot of effort into technical analysis of the algorithm in
and trying to figure out how to get every possible avenue covered, and I know it works.
To me, keeping in mind the overall goal of that algorithm and making your content decisions
based on that is really important, like dividing up the channels.
The reason it was just dragging the channel down is because people didn't watch the full
20 minutes or the 30 minute interview or whatever, so what it told YouTube was this channel normally
gets a 98% view rate, people watch 98% of the video, and then all of a sudden it drops
to 40% or 30%.
So YouTube sees those as bad videos and they don't want to show them to people because
they're afraid they'll leave the platform.
If you view it through the lens of trying to keep people on YouTube, it's easier to
understand.
Now to be fair, YouTube also does things to like try to safeguard people's kind of mental
health a little bit.
Like if you watch and you're doing like autoplay, if you watch a bunch of angry videos, they
will throw something mellow in there, at least that's the way it seems, you know, it's all
a big secret, but it seems like that occurs.
And I think that's intentional.
But again, it goes back to if you get too angry, you may get off the channel, or you
may get off the platform.
Let's see.
Why did you put your long form stuff on a separate channel?
Asked and answered.
Is it still possible to start a successful political YouTube channel today?
Yeah, absolutely, absolutely.
You just have to have your angle.
When you're talking about the YouTube side of it.
It's not necessarily about being like successful channels.
It's about being different but incorporating the same elements, if that makes sense.
You see a lot of channels that will start up and they will be very, very similar to
the channels they like.
a good starting point, but you have to make it your own. That's where I see, that's where
I see most people kind of go off the rails, is they try to stick too closely to the channels
that, the channels they like. And you have to remember, the audience is different.
Are you going to do the YouTube affiliate program? That's the shirt thing I was just
talking about. As far as like using it in like product placement and stuff like
that, the same answer I've had for years. If it was something that actually
benefited the channel, sure. Yeah I think the example I used back then was like
you know getting like an arrow garden or something like that and putting it back
on the shelf so y'all get to see if it really works like it does in the
commercial something like that maybe as far as like going through and doing
product reviews and trying to drive affiliate traffic not not likely it
It would have to match seamlessly with the channel.
Somehow be a benefit to y'all.
Why are your Halloween episodes so much more intense than other holidays?
Shouldn't Christmas be the big one for your demographic?
Yeah, I mean, I get what you're saying and yes, like, realistically, people put a lot
of effort into the Christmas episodes on YouTube because during December ad rates are higher,
so they want those to be really good because it generates more revenue.
I put more effort into the Halloween episodes, I guess, because I like Halloween.
Yeah, I would rather put the effort into something I enjoy.
Like I enjoy Halloween.
That's more my holiday, I guess.
Are you worried about AI overtaking real creators on YouTube?
No, I'm not.
I think that there will be certain genres where
AI is really going to hurt the people who are on YouTube now.
When it comes to news and political coverage, no.
No, because it's really scraping what's out there and using that.
So the content it gathers to form its opinion,
it has to be made by somebody, and that's
going to have to be a person.
I know that there are news outlets now
that are experimenting with AI.
It's going to be a disaster.
I'm calling that now.
The artificial intelligence, it isn't quite as intelligent
as it's being made out to be.
With rapidly changing events, the data
set it uses to create its content
is not going to be up to date.
How far apart are your public and private face?
Not really that far.
You know, again, when the channel started it was a joke and there was a lot of satire
and I played a lot of stuff up.
Now the biggest difference is...
My language is a lot coarser than it is on this channel at times, but that's the big
change right there.
What kind of camera do you use slash do I need to start?
That is a Canon XA something, XA 45, I don't know, it's a Canon XA.
You don't need that to start.
That one was the one that we decided to get because it can do anything and everything
that we might ever want to do.
So we would never need a whole bunch of different cameras.
It's all in one.
The channel started with a broken cell phone.
There's a lot of emphasis on production quality.
Content is what matters.
How much did leaving Twitter hurt you?
People say it's the most important part of being a YouTuber.
I got this number this morning, 0.7%, 0.7% of views.
That was the decline, and that is now that's we actually
didn't actually decline because we have a little bit of growth
each month.
I want to say it's like 3% or something like that.
And so that ate up one of those percent, almost a full percent.
But it's either people from Twitter are coming to the channel like on their own without the
prompts from Twitter, or Twitter is highly overvalued when it comes to actually driving
content or driving traffic to your videos.
I don't know which.
Now, it's worth noting that if you're just starting out,
you've got to get your videos out there.
So you have to use every possible avenue of social
media that's available, because you need that one video to
click, so you can get your base subscribers.
How much money do you make?
Not as much as people think.
I guess I don't really care what you make.
When do you get to the point you can do YouTube full time?
500,000 subscribers?
way less than 500,000 subscribers.
Obviously, it depends on your standard of living,
the cost of living where you're at, how many views you get.
Somebody with 80,000 subscribers that puts out a video a day
and they're capturing a decent percentage
of those subscribers with each video
will make more money than somebody
with a million subscribers who puts out a video a month.
There's a lot to that.
Generally speaking, if you're going to put out a video a day
and you have decent capture of your subscriber base,
80,000 to 100,000 subscribers, and you're full time,
Why don't you do shorts?
I don't have the legs for it.
No, I'm too long-winded.
The shorts format on YouTube, I even played around
with the TikTok, and I played around
with trying to clip videos down.
I talk too slow.
It's really what it boils down to.
Have you ever had a female fan do something
inappropriate? I'm assuming this is talking about like in the physical world. Not really.
I mean, I've gotten inappropriate messages before, but as far as in like out and about,
I don't get inappropriate. I get awkward.
I have had women come up, and awkward is the word, not inappropriate.
Does YouTube allow a good work-life balance?
No.
I mean like anything it is what you make of it.
Simply speaking, you end up throwing yourself into it.
It's hard to maintain that because especially if you're doing news, there's always news
breaking.
There's always another video you can make.
So you have to set your schedule and then stick to it.
There are definitely days when I would want to release like six videos.
But I don't for this reason.
And because eventually the algorithm is just like no more.
Let's see.
Why do you not show your full shirt but put so much energy into picking the right shirt?
It really is about the framing.
about the framing and how it looks on your screen.
Let's see.
When's the next live stream, or are the Q&As replacing those?
The Q&As are not replacing those.
The live streams will be coming when
we changed over to the new facility here.
We didn't take into account what you need to stream, the bandwidth you need to stream.
We're trying to figure out a solution to that at the moment.
Why aren't you really active in Discord?
To let people disagree, to let people disagree, I don't want people to feel like they always
have to agree with me and if I was super active in that group, that discussion, it wouldn't
be as free, I don't think.
That's a big part of it.
Who's the nicest person you've worked with on YouTube?
So I can't say.
It's funny.
person that I have worked with on YouTube, her public face is that she's
not a nice person. Her public face is very very sarcastic and angry and she
is not and you know I don't want to say because I don't want to ruin her image
as being evil.
Do you ever want to quit?
Nah.
I really enjoy what I do and I feel like it makes a difference.
So I don't want to stop.
I have no intentions of stopping.
I know that there's a, for a lot of people, there's like a wall you hit where you just
don't want to do it anymore.
I don't see that happening with me.
I really enjoy this.
So that seems unlikely.
And I guess they put that at the end to wrap it up.
No, I'm not going anywhere.
So that's all of them.
We'll be back with another themed one of these soon, I'm sure.
And then I think that there's a random one with a whole bunch of different questions
about different topics coming soon, too.
All right.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}