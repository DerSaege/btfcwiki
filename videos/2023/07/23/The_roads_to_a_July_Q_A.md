---
title: The roads to a July Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Vqx84OlqG0M) |
| Published | 2023/07/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why politicians in promotional videos walking through Congress halls aren't actually working; compares it to an old filmmaker's trick to create the illusion of action.
- Addresses the question of whether a leftist perspective can work as a harm reduction specialist in a position paid for by law enforcement, touching on the online left's views.
- Talks about the controversies surrounding a country song by Jason Aldean and criticizes it for not accurately reflecting small towns.
- Delves into rumors surrounding the high military budget for Viagra, mentioning off-label use and cash payments to local officials in conflict zones.
- Responds to a question about architecture and authoritarians, hinting at the architectural changes post-1950s in Europe.
- Mentions the possibility of setting up a seed tree cutting exchange parallel to gardening videos but notes the legal implications.
- Clarifies the meaning behind the phrase "quite a rack" in military context and addresses a question regarding his comments about women.
- Explains why NATO wouldn't swiftly destroy Russia's forces in Ukraine, pointing out the complications and risks involved.
- Acknowledges an audio glitch in some videos, clarifying that it's not intentional but a physical issue in the recording process.

### Quotes

- "Sometimes a glitch is just a glitch."
- "It's something that is actually happening as it's being recorded."
- "Even though their job, I want to say they're like a tenant advocate or something like that."

### Oneliner

Beau addresses various questions on politicians in videos, harm reduction specialists, country songs, military rumors, architecture, gardening exchanges, and NATO's actions in Ukraine, concluding with insights on an audio glitch in recordings.

### Audience

Content Creators

### On-the-ground actions from transcript

- Set up a seed tree cutting exchange to parallel gardening videos (suggested)
- Investigate and address any audio glitches in recordings (implied)

### Whats missing in summary

Insights on Beau's perspective on viewer engagement and YouTube analytics.

### Tags

#Q&A #Politics #HarmReduction #MilitarySpending #MusicCritique #NATO #AudioGlitch #ContentCreation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to do a Q&A for July.
And these are your questions.
I don't think these have anything whatsoever,
so these will be all over the place.
I haven't seen these before, so you're getting
kind of an off-the-cuff answer.
If you want to send one in for a future video,
it is question for Beau over on Gmail.
Okay, all right, so why are so many politicians,
promotional videos, them walking through the halls
of Congress, why aren't they working?
It's funny, because that's an old filmmaker's trick.
It's how you move a story along when there's no action.
Think about, what is it?
The doctor that's super house.
or a lot of procedurals. It shows the cast walking and talking in a hallway. It's to
provide the illusion of action while nothing is really happening. So I would imagine it's
for the same reason.
From a leftist perspective, can I work as a harm reduction specialist if it's a position
that is paid for by law enforcement?
A harm reduction specialist.
I mean, okay, so you have to determine what you're really asking.
Are you talking about like the online left and how they're going to see it?
You're a cop.
If you're talking about like leftist principles, if your entire job is engaging in harm reduction,
like you know, administering stuff to stop people who have hurt themselves, stuff like
that, and you're not actually, you know, engaged in law enforcement, I don't necessarily think
that that's really wrong.
I mean, it's something that the left advocates for.
oftentimes those positions get rolled into a police department. I don't think they should be,
but government bureaucracies being what they are, it often does. It's funny because one of my
favorite like actual left, not American left, but actual left people on Twitter recently like tweeted
out something that was like, you know, I love my job or something like that.
And I believe they were fully prepared to take heat for that statement.
Even though their job, I want to say they're like a tenant advocate or something like that.
But there's a lot of the online left that is very concerned and very aware of ideology
and not necessarily the practical implications of what they're saying.
So I hope that helps.
I don't know what kind of answer you're going for,
and I don't know the specifics of the job.
What about this person on Twitter and their predictions?
This is scary.
And there's a screenshot here.
So this is true of anybody who gives predictions on Twitter,
particularly those that may be scary,
may be filled with a lot of hyperbole.
Look at how they make their predictions.
What they say.
Is it in a month, in three months, in 30 days, in a year?
Or do they say within this period?
Look at how they made their prediction,
and then search their profile for those terms.
You will see their previous predictions,
and you'll know whether or not to put
any real stock in it. Talk about the Jason Aldean song. So I've recorded two videos on
this so far and I haven't released either one because I don't think either one fully
captures the issues I have with the song in general and the video itself. You know, there
There are a bunch of obvious issues that have been very well discussed all over the place
as far as the scene where it was filmed and all of that stuff.
But there's other issues with it.
Primarily that song is written and filmed for people from the suburbs.
That is not, that's not.
It is not an accurate reflection of small towns.
The general idea of there's the one line in it, and if you don't know what I'm talking
about, a country singer put out a song that just has a whole bunch of issues, I mean tons,
ranging from stuff that certainly appears to be very dog-whistly to the location where it was filmed and the message it
sends.
And those are probably much bigger issues, but for me, one of the things that I always notice about songs like this is
that they are written by people who do not understand small towns.  I mean, there's actually a line
in this that's like, yeah, cuss out a cop.  Try that in a small town or something like that.
Like, yeah, that's Tuesday.
I mean, tell me you've never been to a small town without telling me.
That's very normal.
In my entire life, I have never heard a good old boy say, oh, good, the cops are here.
Back the blue.
That's not a sentence out here.
It's a song written for people from the suburbs that think they are more rural than they are.
But there's a list of issues with that song.
At some point I'm hoping I can get all of my views into a single video and release it.
Because yeah, it's not good.
I don't know anybody who is actually rural who was like, yeah, this is a great song.
It's not for us.
My friend told me some rumors about why the military budget for Viagra is so high.
Can you talk about some of those rumors?
Just want to know if they're real.
Okay, so if you missed this, there was, I want to say it happened in Congress, anyway,
it came to light how much the U.S. military spends on this particular medication.
Now the real reason is that that's for older people in the service or who have retired
in some cases.
I'm not sure what number they were using.
But there are rumors about off-label use in the military.
Some buds may use it or may have used it in the past to get through a certain training
program.
There was a rumor going around about it being used for people who were, I want to say, going
up into the mountains, a special team that was headed up into the mountains.
And then there's something that is probably far more common or was far more common in
conflicts like Afghanistan.
A lot of times cash payments to local officials help with things.
People all over the world have the same desires.
And a lot of it might have been used that way.
Those are all the rumors that I'm aware of.
There's probably more.
Most times rumors like this, they're at least somewhat grounded in reality.
or not the medical application was accurate or scientifically based, that's one thing.
But I truly believe that these things were tried that way.
But I don't know that that actually impacts the bottom line on any of it.
Not to any major degree.
The only thing I have to hand it to the authoritarian traditionalist on is their take on architecture.
There's a Twitter account that shows architecture from the late 1800s versus what replaced it
in the 50s.
I was wondering if you had any thoughts on this.
First, you never have to hand it to authoritarians.
to throw that out there. Second, the reason, it might be enlightening to try
to figure out what might have happened just prior to the 1950s in Europe that
required the architecture to be replaced.
Something might have happened in the 1940s that altered the skylines of a
lot of cities. I would suggest the architecture being changed is the fault
of authoritarians. Just saying. Howdy there, Beau. It's another internet person. I
wanted to thank you for everything you all are doing with the YouTube channels.
I was curious about the possibility of you setting up a seed tree cutting
exchange parallel to the gardening videos?" I mean, I'm not opposed to it. I hadn't thought
about it. I don't know that we have the time to actually run one, but if there was one
that was set up that was actually functioning, I wouldn't mind highlighting it.
As far as the tree cutting exchange thing, you can get into issues with that if you cross
state lines sometimes.
There's a lot of legal stuff that you'd have to work through here, but I wouldn't be opposed
to helping one that was up and running.
You once said a woman had quite a rack, and it seemed very out of character for you, so
I didn't say anything.
Then you said Trump might have quite a rack.
You once said it about your wife too, am I missing something?
Or is it a sexist slash fat man joke?
Okay that's super funny.
On a soldier's uniform, all of the metals, the ribbons is what they're, anyway.
The colored little rectangles on their uniform.
Altogether those are a rack.
That's what it's called.
This is, I mean, yeah, this is funny.
So he said that his indictments were a badge of honor.
He was going to have quite a rack.
The phrase quite a rack indicates either a large quantity or it may be a small quantity,
But they are ribbons that are associated with heroism.
You have a lot of generals who have large racks.
They have quite a rack.
But that doesn't necessarily mean that those medals were given for valor.
I know one person who basically has the everybody ribbons, the ones that pretty much everybody
gets when they're in, two campaign medals, and then everything else, he only has three
others, but it's a bronze star, a silver star, and a purple heart.
Even though there's not a large quantity, that is something that would also be described
as quite a rack. And yes, my wife does have quite a rack as well. I didn't necessarily
catch the second meaning there, which is really bad because the only time I can think of that
I might have said that about a woman in a video is the woman who highlighted all of
the issues with harassment at hood, and if you were taking that in another way, yeah,
that would be pretty horrible.
But no, that has to do with their ribbons.
We know so much about your life, what do you know about us?
I mean, I guess that would depend on which individual.
Some of y'all, I feel like I know David's daughter, niece, daughter, just became a tattoo
artist and fulfilled her dream because I saw it on social media.
I do kind of scroll through when I can and I pay attention, but I don't really know
what that means.
If you're talking, of course that could mean, like, how much information do we get about
viewers?
And the answer to that is a lot.
YouTube gives us analytics on everything.
So if that's what you're asking about, YouTube analytics are incredibly in-depth.
Like I can tell you cities and states where we have a lot of reach.
I can tell you age brackets.
time different age brackets watch, there's tons of information like that.
What do you think of the Margot Robbie thing?
Oh, that she's mid?
Yeah, I saw that.
There are a whole lot of people, a whole lot of men on social media who convince me often
that the reason they have trouble dating is because they're not interested in women.
Yeah, and they just, rather than admit that, they attack women.
Yeah, I personally think that she's very pretty, if that's the question.
I saw a commentator say NATO could destroy Russia's forces in Ukraine in a day.
Why doesn't it and then just stop at the border?
Okay, in a day, no, that's American exceptionalism or NATO exceptionalism.
It would probably take about a week.
Why doesn't it?
Even if you were to set aside the obvious risk of escalation, there's another thing
at play. The United States, and by extension NATO, if they were to do that, they would
do it mostly with air power. The first things that they would do would be to hit command
and control and air defense. Those locations, those that are in Ukraine, they're in Ukrainian
cities.
The US military is a broadsword, not a scalpel.
One of the reasons that I think Ukraine is handling this properly is that they're doing
it themselves.
They are going to be far more careful than the US or NATO forces would be while fighting
on their own soil, while fighting on Ukrainian soil.
We can talk about how precise US air power is, and it's the most precise on the planet,
but accuracy becomes really relative when you are talking about something that hits
the building next door to you.
Yes, it could be done, but the cost to the civilian populace would probably be more than
most people want.
Is that why they don't do it?
I don't know.
And I would hope that that at least factors into it.
I've noticed that in some videos there is a sound like a pin clicking that will happen
occasionally.
Is that an intentional thing with some kind of meaning like the upside down patch or just
an audio glitch?
Yeah that is, it is an audio glitch.
It is something physical, like it's a mic or a cable or something and I have been trying
to figure it out.
We have replaced all of the components and tried to figure out which piece it is that's
causing the issue, but much like taking a car to a mechanic, when we're actually working
on it and trying to figure it out, it doesn't do it.
But what we do know is that it's a problem, it's a physical issue.
It's not something that's like in the recording or a digital thing.
It's something that is actually happening as it's being recorded.
If anybody has any advice, I'm totally open to it.
But no, it is not an intentional message or anything like that.
Sometimes a glitch is just a glitch.
So that's it.
Those were the questions that we had come in.
And I'm sure that we will have more come next week.
This is going to be a regular thing so we can try to get everybody's questions, especially
if it's something that doesn't prompt a full video.
And there will definitely be more themed videos
coming on this channel.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}