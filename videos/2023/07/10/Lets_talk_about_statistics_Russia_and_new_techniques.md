---
title: Let's talk about statistics, Russia, and new techniques....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QdfzcRRoeWA) |
| Published | 2023/07/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing statistical analysis, Russia, Ukraine, and new techniques in obtaining accurate numbers.
- Questioning the accuracy of reported numbers due to countries having incentives to manipulate data.
- Exploring the origin of the numbers and the methodologies used to determine casualties.
- Contrasting Russia's claim of 6,000 losses with Ukraine's estimate of six figures.
- Describing the analysis of military funerals' photos and excess inheritance cases to ascertain casualty figures.
- Emphasizing that the numbers obtained represent the minimum, not the total count.
- Pointing out that various factors could make the actual casualty count higher than reported.
- Expressing disappointment and disapproval towards unnecessary conflict and resulting casualties.
- Acknowledging viewer support for Ukraine while expressing concern over the human cost.
- Commenting on the effectiveness of the innovative techniques used to estimate casualties.

### Quotes

- "It's not an accurate depiction of the whole picture."
- "None of this was necessary. This was elective."
- "I hope you all have a good day."

### Oneliner

Beau dives into the techniques and challenges of obtaining accurate casualty numbers, revealing the gruesome reality behind statistics in conflicts.

### Audience

Analytical viewers

### On-the-ground actions from transcript

- Advocate for transparency and accountability in reporting casualty numbers (implied).
- Support organizations working towards peace and conflict resolution (implied).
- Raise awareness about the human cost of conflicts through education and activism (implied).

### Whats missing in summary

The emotional impact and detailed analysis provided in Beau's full commentary.

### Tags

#Statistics #Conflict #Casualties #Russia #Ukraine #Transparency


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about
some statistical analysis, Russia and Ukraine,
and some new techniques that were developed
in an attempt to get some numbers.
And they are numbers that are hazy
because pretty much every country involved
has a good reason to be less than accurate
with these numbers.
And we're going to do this.
We're going to talk about what they determined,
how they determined it, and then we're
going to talk about whether or not
those numbers really paint a clear picture.
OK.
So why are we doing this?
Because somebody sent me a message.
And it said, hey, Bo, have you seen this?
How do you feel about the fact that Russia has only lost 50,000?
I don't know that the word only should be in that sentence.
50,000 lost is a whole lot.
For context, that is about seven times what the US lost in Iraq
and Afghanistan the entire time.
That's a huge number.
OK, so where did it come from and why is it surfacing the way it is?
Why are they using these techniques?
According to Russia, I think they're still saying that they have lost 6,000
during the entire conflict.
This is definitely wrong.
It is exceedingly low.
Ukraine is saying that Russia has lost, it's in six figures.
I don't know the exact number at the moment.
That's probably high.
Okay, so how did they get these numbers?
And there's two sets.
The first set was derived using photos from social media
of military funerals from the war.
So that's how they confirm to them. The confirmed number using that method was
right around 27,000, okay? So regardless of what you think of anything else, the
6,000 number is definitely wrong. Okay, the 50,000 number, and realistically I
want to say it's like 48,000 and something, right at 50,000. They did, they
did an analysis of excess inheritance cases. You know, generally speaking these
run pretty consistently throughout the years. It's very predictable. They looked
at the number that was over the predicted amount and this is what they
came up with. It's a good measure. It's a unique way of doing it. Okay, so
here's the thing. These numbers and the way they went about it, yeah, that tracks.
It's smart, definitely inventive. The thing is these numbers represent the
the floor, not the ceiling. It's not an accurate depiction of the whole picture.
This is the minimum, not the maximum, not a clear picture of all of them.
The reason, think about the inmates that were pressed into service. There's no inheritance
claim there. Those who were too young to have anything of value, no claim like
that. Those who were with a private company, their family hasn't been
informed. Those without family, those who are currently missing, this number is low.
But now we do have a clear picture of what the floor is, what the minimum is.
I would remind everybody that it's not a video game, it's not points, they're people.
Based on estimates that have been around and the kind of totaling up and moving through
the various sources, the number is probably closer to 100,000 than it is 50,000.
It's waste.
You know, I know that most people watching this channel are incredibly supportive of
Ukraine.
I think I have made my position pretty clear on this as well.
At the same time, I don't like seeing these numbers.
None of this was necessary.
This was elective.
old man who wanted to have a legacy created a bunch of waste. I can't, I
personally have a hard time seeing it any other way. So the techniques, they're
interesting and I would expect to see them used more and more because it's a
smart way to do it. Particularly if you have a country that is putting out a lot
of propaganda, that is concealing the truth to the degree that Russia is, this
is a very unique way to get your minimums. But this isn't going to be a
truly accurate count it's gonna be the floor. So how do I feel about that? I mean
not good and that's I don't I can't bring myself to celebrate it. Anyway it's
It's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}