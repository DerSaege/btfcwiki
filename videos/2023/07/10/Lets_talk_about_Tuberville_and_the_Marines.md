---
title: Let's talk about Tuberville and the Marines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qfzzB-DgE7A) |
| Published | 2023/07/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Senator Tuberville has been holding up high-level military promotions, including those of the United States Marines.
- The senator's actions have been undermining US military readiness because of his opposition to the branches of the military caring for their personnel's reproductive rights.
- Effective tomorrow, the United States Marines will not have a confirmed commandant due to Senator Tuberville's actions.
- This situation shifts from undermining military readiness to undermining separation of powers and foundational constitutional principles.
- A vacant leadership position in the Marines will result in someone becoming the acting commandant, with significant authority despite lacking confirmation.
- Senator Tuberville's refusal to confirm nominees for high-profile military positions jeopardizes civilian control of the military and undermines checks and balances.
- By not consenting to nominations, Senator Tuberville establishes a precedent where positions are filled by acting officials, diminishing the Senate's role.
- This situation ultimately hands control of the military to President Biden, bypassing the Senate's advice and consent.
- The precedent set by acting appointments weakens the Senate's authority and consolidates power with the President and Secretary of Defense.
- Senator Tuberville's actions may risk U.S. military readiness and the lives of service members and civilians worldwide.

### Quotes

- "His little stunt is jeopardizing civilian control of the military."
- "It's going to show that the Senate has no power here."
- "Effective tomorrow, Senator Tuberville has handed all control of the military to Biden."
- "I'm sure they don't really care about that, but I bet they care about the fact that Tuberville is handing Biden all the power."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Senator Tuberville's obstruction of military promotions jeopardizes civilian control and hands power to the President, undermining constitutional principles and risking military readiness.

### Audience

Congressional constituents

### On-the-ground actions from transcript

- Contact Senator Tuberville's office to express concerns about his obstruction of military promotions and its implications for civilian control (suggested).
- Support organizations advocating for upholding constitutional principles and the importance of Senate oversight in military appointments (exemplified).

### Whats missing in summary

The full transcript provides a detailed breakdown of how Senator Tuberville's actions impact military leadership, civilian control, and constitutional principles, offering a comprehensive understanding of the situation.

### Tags

#SenatorTuberville #MilitaryPromotions #CivilianControl #ConstitutionalPrinciples #USMarines


## Transcript
Well, howdy there, internet people, it's Bo Young.
So today, we're going to talk about Senator Tuberville
and the United States Marines
and how things change tomorrow.
How Senator Tuberville's little political stunt
fundamentally changes tomorrow.
And I'm willing to bet that he doesn't know
that it's gonna change tomorrow.
If you don't know what has been happening, the senator has been holding up high-level
military promotions.
He's been stopping them, stopping their confirmations.
They have been high-level, but not necessarily high-profile.
That changes tomorrow, and with it, all of the dynamics change.
Until now, the senator has been undermining US military readiness, I mean
there's no other way to put it, and the reason he's doing this is because he's
mad that the branches of the military want to take care of their troops. They
want to take care of their personnel so the personnel can take care of the
mission. Having to do with reproductive rights. He's super mad about this and
this is his way of garnering attention. So, holding up the promotions undermines
military readiness. Effective tomorrow, the United States Marines do not have a
commandant. They don't have a confirmed commandant. At that moment, everything
shifts because it's no longer undermining military readiness, it is
undermining separation of powers, checks and balances, foundational constitutional
principles. He's undermining the power of the Senate tomorrow. It all changes. OK, so
a new commandant isn't confirmed. What happens? Well, the Marines just don't have
commandant, right? No, of course not. It's the military. A vacant leadership position is a
vacuum. Somebody is getting sucked up into it. They're going to be the acting commandant. Now,
granted, there will be some things that they can't officially do as commandant, but I am certain
there are Marines down in the comments section that would be happy to explain to you how they
would view a lawful order from an acting commandant. Short version is they would view it as if
it had been chiseled on stone and brought down from a mountain. It's not going to change
anything. These positions, now that we are at the really high profile ones, not just
high level, these positions, they are nominated by the Secretary of Defense, appointed by
the President of the United States with the advice and consent of the Senate.
It's a check on presidential authority.
With Tuberville deciding, well, they're just not going to consent, it doesn't actually
change anything.
People move into these positions as acting.
So what does that actually do?
It shows that the Senate and their advice and consent is irrelevant.
It doesn't matter.
His little stunt is jeopardizing civilian control of the military.
That's what's happening.
And it will undoubtedly become a major news story as it goes on.
people will eventually realize that with this precedent in place that means that
the president can appoint whoever they want and just, well, if the Senate doesn't
confirm them, oh well, they become acting.
is single-handedly upsetting, destroying, eroding the separation of powers in these
checks and balances, okay? Now, since I know that there are going to be a lot of conservatives,
a lot of Republicans who really don't actually care about these principles, let me say it
another way. Effective tomorrow, Senator Tuberville has handed all control of the military to Biden,
foregoing the Senate's obligation to provide advice and consent.
It all reverts to Biden at that point. All that happens is instead of them being called commandant,
They're going to be acting commandant. Once the precedent for this is established
the acting commandant begins to issue their orders.
It's going to show that the Senate has no power here.
So, Biden and the Secretary of Defense have it all.
That happens tomorrow.
I'm fairly certain that those who have been supportive of this little stunt, that they
They may not have cared about risking U.S. military readiness, which risks the lives
of U.S. servicemembers and the lives of civilians all around the world.
I'm sure they don't really care about that, but I bet they care about the fact that Tuberville
is handing Biden all the power.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}