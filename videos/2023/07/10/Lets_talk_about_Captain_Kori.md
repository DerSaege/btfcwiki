---
title: Let's talk about Captain Kori....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fJp_zgNb8uw) |
| Published | 2023/07/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on Captain Cory's journey are being shared.
- Captain Cory, a young man passionate about pirates, received help to fulfill a bucket list item.
- He wanted a YouTube play button and support poured in from various sources to make it happen.
- Captain Cory has passed away, but he lived his dream in the last six months of his life.
- His family is requesting space at the moment to cope with the loss.
- His mother intends to keep the channel running as a memorial to him.
- Messages of support can be left on his channel, "a crack in the box."
- The news might not be what everyone wanted to hear, but it was somewhat expected.
- Despite the sad news, there is solace in knowing that Captain Cory lived his dream.
- The community's support played a significant role in fulfilling Captain Cory's dream.

### Quotes

- "He really did get his dream for six months."
- "I know that this is not the news everybody wanted to hear."
- "Take a little bit of solace in the fact that he really did get his dream for six months."
- "It was due to the people who are going to be pretty upset by this news."

### Oneliner

Updates on Captain Cory's dream fulfillment and passing, with a call for messages of support on his memorial channel.

### Audience

Online community members

### On-the-ground actions from transcript

- Leave a message on Captain Cory's channel, "a crack in the box" (suggested)
- Visit his channel to wish him a good voyage (suggested)

### Whats missing in summary

The full transcript gives a more detailed background on Captain Cory's dream fulfillment and the impact of community support.

### Tags

#CaptainCory #Pirates #Dreams #Support #Community


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about pirates
and dreams and voyages.
We'll provide a little update on Captain Cory.
For those who may not have been around,
don't remember, or missed it, there
will be videos down below for context.
But the short version is, about six months ago,
a young man who was very enthusiastic about pirates
needed a little bit of help fulfilling a bucket list item.
Like all good pirates, he wanted silver.
He wanted a YouTube play button.
A bunch of people from this channel
and a whole bunch of others jumped in and made that happen, made that happen.
So Cory has set sail.
He is no longer with us.
It is important to remember for the last six months of his life, he lived his dream.
He lived his dream.
Now understandably his family seems to want a little bit of space at the moment.
His mother has indicated that she definitely intends on keeping the channel up and running
kind of as a memorial to him.
So I'm sure that while you may not get a response immediately, I'm sure that they would appreciate
any message that you may want to leave over on his channel, which is a crack in the box.
There'll be a link down below.
And I am certain that he would appreciate those who stopped by to wish him a good voyage.
I know that this is not the news everybody wanted to hear, but it was the news that I
think on some level everybody knew was coming and just take take a little bit
of solace in the fact that he really did get his dream for six months and in large
part, it was due to the people who are going to be pretty upset by this news.
Anyway, it's just a thaw, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}