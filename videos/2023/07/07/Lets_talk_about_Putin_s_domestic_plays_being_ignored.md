---
title: Let's talk about Putin's domestic plays being ignored....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=30qOg2WsojQ) |
| Published | 2023/07/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia is strengthening its National Guard, a move commonly made by authoritarian leaders to ensure loyalty.
- Western focus is on the impact of strengthening the National Guard on the Russian war effort.
- Overlooked is the transfer of special units, with skills in undercover operations and dynamic entries, to the National Guard.
- These special units possess skills directly transferable to targeting critics of Putin and undermining opposition.
- Putin may be planning to target individuals less loyal to him using these special units.
- The special units selected are likely chosen for their ability to keep quiet and avoid leaking information.
- The potential use of these units to target critics and opposition should not be ignored by both Russians and the West.

### Quotes

- "Russia is strengthening its National Guard, a move commonly made by authoritarian leaders to ensure loyalty."
- "The potential use of these units to target critics and opposition should not be ignored by both Russians and the West."

### Oneliner

Russia's strengthening of the National Guard and transfer of special units may indicate plans to target critics of Putin, a concerning development for both Russians and the West.

### Audience

Russians, Western observers

### On-the-ground actions from transcript

- Stay informed about developments in Russia's National Guard and special unit transfers (implied)
- Raise awareness about potential targeting of critics and opposition by Putin's regime (implied)

### Whats missing in summary

Insight into the potential implications for critics of Putin and opposition movements in Russia, as well as the importance of vigilance and preparedness in response to these developments.

### Tags

#Russia #Putin #NationalGuard #Authoritarianism #Critics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk a little bit
about something that's happening in Russia.
It's getting some coverage in the United States but it's...
The coverage is pointed at something very specific
and that's true, but it's overlooking something
and what is being overlooked
is something that's pretty important
it tells us a little bit about what Putin may be planning domestically. If you
are a critic of Putin inside Russia, it should concern you. Okay, so what's
happening? The Russian National Guard, I'm just going to call them that, they're
being strengthened. They're getting more. This is a
completely normal move for, you know, some authoritarian goon to do after, you
know, something like that Wagner dust-up, right? You want a force that is more
loyal to you. That's normal. That makes sense. It's pretty predictable. Now Western
focus is on the fact that if they're going to strengthen the National Guard
means they're going to take decent recruits for that. Those recruits won't
be going to Ukraine. So this move is bad for the Russian war effort. That's what
the West is focusing on. That's what the reporting in the West is focusing on.
All that's true. Completely predictable move, get a force more loyal to him, get
it strengthened so it can compete if there's a sequel to those events. Doing
that damages the war effort. Yeah, no problems with that. What's being overlooked is that
part of the conversation includes transferring special units from Russia's, it's basically
Russia's DEA, and putting those units under control of their National Guard. It doesn't
seem like that would be super alarming, but when you really think about their
jobs, it should be. The bureaucracies between the US and Russia, they parallel.
What, when you're talking about special purpose units like that, what do they do?
They've got two real gigs, right? One is going undercover, infiltrating an
organization. That's what they do. Don't think about the mission, thinking about
the skill set that they have. They know how to go undercover. They know how to
infiltrate a group, gain people's confidence, and get them talking. That's
one part. When you are talking about their SWAT teams that were assigned to
this particular agency, again don't think about the mission, think about the
skill set. What do they do? They operate within Russia and by surprise they
They conduct a dynamic entry on a home or a business and they snatch people.
The skill set that the units that are considered, they're being considered for transfer, their
purpose when you were talking about a national defense situation, dealing with
something like that, it's not to be out on the street countering a Wagner type
force. They're not infantry. Even those SWAT teams, they're trained in something
pretty specific. They will not be able to go toe-to-toe with Wagner. They know
this. The Kremlin knows this. This isn't a secret. So why do they
want them? They want them for the skill sets they possess. And the skill sets
that they possess are directly transferable to going after critics of
Putin. In fact, when you're talking about them being placed under the National
Guard, I have a hard time thinking of any other way they could be used.
That seems to indicate that Putin may be planning on going on the offensive, that he may be
looking for people who are less loyal to him and detaining them in some way. And
this is the force that he's kind of picked to do it. They're not political,
they're not military, they don't have any loyalty to the troops, they're law
enforcement and they have the exact skill set necessary to undermine any
criticism of him to infiltrate organizations that might be arising to
oppose him and to snatch their leadership. He's putting it together now.
It's something that people should be aware of, particularly if you're in Russia
and you're a critic of Putin, but the West should be prepared for this as well.
This shouldn't be something that catches everybody by surprise.
Now my guess is the reason he selected them out of all of the different law enforcement entities available
is because they can keep their mouth shut.
The groups that they've been running up against,
they're not weak groups. So
the people involved with these special purpose units, they know how to be quiet.
So as far as news of this is...
news of them being used,
as far as that getting out, probably won't happen for a while.
It will take them grabbing somebody who is high-profile and somebody making some
noise. That's assuming that a chilling effect doesn't occur. This part of the
strengthening of the Russian National Guard is being ignored. It shouldn't be.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}