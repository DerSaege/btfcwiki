---
title: Let's talk about the ruble and where it goes from here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6jzIhxb83qw) |
| Published | 2023/07/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russians view the exchange rates on the ruble as Americans view the stock market, making it an economic indicator.
- The comfort zone for the Kremlin is 80 to 90 rubles per dollar; if it surpasses 90, concerns arise.
- The ruble has lost 21% of its value this year and is currently trading at more than 90 rubles to a dollar.
- The Russian government manipulates the ruble's appearance to counteract sanctions and maintain the illusion of strength.
- Recent events have led to the ruble falling outside its comfort zone, alarming the people and angering Putin.
- Russia can likely sustain these measures until the end of the year before facing significant repercussions.
- Putin may resort to more drastic measures to keep up appearances, depleting reserves faster.
- Maintaining the illusion of a strong ruble is unsustainable in the long run.
- The longer Putin denies his mistakes, the worse the situation will become.
- The current situation is more about appearance than actual economic value.

### Quotes

- "Russians look at the exchange rates on the ruble the way Americans look at the stock market."
- "If it goes above 90 rubles per dollar, they start to get worried."
- "They are cooking the books and they're doing everything that they can to keep the ruble appearing as if it's strong."
- "Putin is not sitting in a good position right now."
- "The longer he refuses to admit his mistakes, the worse that position is going to get."

### Oneliner

Russians closely monitor the ruble's value, which, when outside the comfort zone, sparks unrest and challenges for Putin as he struggles to maintain appearances amidst economic turmoil.

### Audience

Economic analysts, policymakers

### On-the-ground actions from transcript

- Monitor the ruble's value and its impact on Russia's economic stability (suggested)
- Stay informed about the economic situation in Russia (suggested)

### Whats missing in summary

Insights on potential global implications of Russia's economic challenges

### Tags

#Ruble #Russia #Economy #Putin #Sanctions


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the ruble.
We're gonna talk about the ruble and what's happening.
And comfort zones, and where they're at,
and why it's happening, and how much longer they can hold out.
Because the recent unpleasantness in Russia
has weakened their currency.
OK, so to start with, this is what you need to know.
Historically, because of things that have happened in the past,
Russians look at the exchange rates on the ruble the way
Americans look at the stock market.
It's kind of a general economic indicator.
So if the ruble loses too much value, the people start to get nervous.
The comfort zone for the Kremlin is roughly 80 to 90 rubles per dollar.
If it goes above 90 rubles per dollar, they start to get worried.
They would love to have it in some instances below 80.
So the use of the ruble as an economic indicator by a wide portion of the population means
that if it falls out of that comfort zone, there is an increased likelihood for unrest.
The ruble this year, so far through this year, it has been one of the worst performing currencies.
In fact, it may be the worst performing currency.
It has lost 21% of its value.
Right now, it is trading at more than 90 rubles to a dollar, meaning a ruble is worth more
than a penny, but not two cents.
It's not a good place for them to be.
They are at a 15-month low.
Now it's worth noting that if you were to remove the initial drop right after the invasion,
it's still at a five-year low.
It is not performing well at all.
Now the Russian government is doing everything it can.
We have talked on the channel before about how they engage in, I think we termed it currency
controls to be nice.
They're cooking the books, they are cooking the books and they're doing everything that
they can to keep the ruble appearing as if it's strong, to make it seem as though the
sanctions aren't working and therefore hopefully induce the West to drop the sanctions.
This recent dust-up with Wagner, it upset their ability to do that.
It caused some speculation and it dropped their currency outside of their comfort zone.
This is going to be alarming to the Russian people.
It is also going to anger Putin.
Now how long can they keep up with cooking the books?
Probably through the end of the year.
Probably through the end of the year.
that appears to be their limit. Once the reserves that they have have run out,
once their creative accounting measures catch up to them, it's going to go
bad, and it'll go bad pretty quickly. You could expect a lot of openings at the
Ministry of Finance at that point. The problem is those openings are going to be
9 by 18. So what what's the outcome of this? Putin is going to be angry. He's
probably going to push for even more creative measures, which is going to burn
through the reserves they have, it's going to... the more creative they get, the
shorter a period of time they can hold out. Right now this is about appearance.
The actual value of a ruble is... it's even less than a penny, realistically, because
is they are doing everything they can to keep that value artificially inflated.
This development is just, it's throwing out more bread for swans to come to the lake.
Putin is not sitting in a good position right now, and the longer it drags on, the longer
he refuses to admit his mistakes, the worse that position is going to get.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}