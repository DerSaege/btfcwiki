---
title: Let's talk about Threads, Twitter, Pepsi, and Coca-Cola....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IqBTJ_GU7PQ) |
| Published | 2023/07/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparing the current social media landscape to the Cola Wars of the 80s, with Twitter and Threads (owned by Metta) positioned as key players.
- Elon Musk's acquisition of Twitter led to suboptimal management and created an opening for competitors like Threads.
- Musk's mistakes include elevating questionable individuals, altering the creator economy, and proposing a subscription-based approach.
- Social media sees users as the product, with a focus on selling advertising.
- Threads aims to differentiate itself by promoting kindness and warning users about disinformation spreaders.
- Major content creators have issues with Threads due to past problems with Facebook, including algorithm issues and throttling pages.
- The philosophical question arises about concentrating social media power in one company's hands.
- Twitter's unique value lies in its proximity to power, allowing direct interaction with celebrities and politicians.
- Other social media platforms like Mastodon lack Twitter's influence due to technical and ideological limitations.
- The challenge for competitors is replicating Twitter's proximity to power and broad appeal across demographics.
  
### Quotes

- "You're the product."
- "A normal average American could tweet directly to their representative, their senator, the president."
- "The idea of choosing a server for a lot of people is too much."
- "Somebody behind Mastodon have any interest in it. They do not seem to be motivated by pursuit of profit."
- "Even the slightest error gets amplified."

### Oneliner

Beau delves into the emerging social media wars, dissecting Musk's missteps, Threads' differentiation strategies, and the challenge of replicating Twitter's unique influence. 

### Audience

Social Media Users

### On-the-ground actions from transcript

- Support platforms that prioritize user well-being and combat disinformation (implied)
- Engage in social media platforms that value user privacy and community over profit (implied)
- Join or support platforms that strive to maintain a diverse range of voices and perspectives (exemplified)

### Whats missing in summary

Insights on the potential future of social media platforms and the evolving dynamics in the digital landscape.

### Tags

#SocialMedia #Tech #Influence #Competition #DigitalTrends


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about social media
and what we can expect to occur as Twitter and threads,
Facebook and Metta, all kind of vie for position.
And we can learn a lot about this
if we kind of relate it to Coke and Pepsi.
There was a period of time in the 80s
which the cola companies were battling it out. You know they were really going
at each other. It's known as the Cola Wars and we are now entering a period
that is going to be the social media wars. So how did we get here? Musk bought
Twitter. Musk bought Twitter and did not run it in an optimal fashion. Even
Even if you are an Elon Musk fan, you have to acknowledge that.
That created an opening.
Metta, which owns Facebook and now Threads, they want to fill the gap.
They want to take Twitter market share.
Okay, I'm going to assume that most people watching this channel are not fans of Musk.
And that makes sense.
So his mistakes involve elevating less than ideal people.
People whose opinion is not necessarily the best for humanity.
They involve trying to alter the creator economy and how the internet functions in
relationship to money, ads, and the people who produce the most content that drive people
to a website.
Make no mistake about it, most people look at social media as a product.
It's not.
You're the product.
When you engage with social media, you are the product.
eyes are the product. They're selling you advertising. He wanted to switch to a
subscription-based model that if you look at pretty much any site that's
ever tried to go that route with it, it doesn't work. Particularly true if it is
reliant on content creators who are not being compensated from that website.
This has all led to major issues and then there's a whole bunch of minor stuff.
Now along comes threads. Threads is the the Facebook response to it.
Okay, so what have we seen so far? When you go to follow certain people on threads,
threads, you will get a notice that says, hey, this person is known for spreading disinformation.
They are going to try to differentiate themselves from Twitter as much as possible by encouraging
people to be nice.
That's the marketing strategy and it will probably work at least in the short term.
There are two issues when it comes to threads.
large content creators, they have issues with the the company as a whole because
of things that happened on Facebook and when those creators built up large pages
and stuff like that only to have Facebook throttle the pages in an attempt
to get... it seemed like it was them trying to get the creators to buy ads. That was
the perception. I don't know that that was actually what was happening, but
that's the way it seemed. So you have that. You also have major issues with the
way Facebook ran its algorithm. Like a good example, you know, our Facebook, we
were told that we were not me on Facebook. Something that still isn't
resolved. And that leads creators to abandon the platform. So they already
have a bad taste in their mouth when it comes to this company as a whole. Now, you
have the philosophical issue that is likely to come into play at some point.
Social media is incredibly important to the national conversation in this
country, is it a good idea to have all of social media really in the hands of one
person, one company, because if threads takes off that's what's going to happen.
So you're probably going to have people that have a philosophical or
ideological issue with it and then you have to remember what the true value
for the products, y'all, what that was on Twitter. What differentiated Twitter
from other platforms is proximity to power. A normal average American could
tweet directly to their representative, their senator, the president, and maybe
even get a response. There was the the ability to influence everything from
your favorite artists to politicians because you could have direct contact
with them. That's going to be hard to duplicate for another company. Another
company is going to have a very hard time doing that because they have to
convince those people those top tier and we are talking the names you hear on TV
news all the time the people who are covered in celebrity magazines they have
to convince those people to put a lot of effort into their platform or it's not
going to go anywhere because it won't duplicate the one thing that Twitter has
that for the most part Musk hasn't messed up. Okay, so going back to the
Cola Wars, everybody defines it as Coke and Pepsi. The reality is there were
other Cola companies. We just don't think about them much anymore because of the
the effects of the Cola Wars. There are other social media platforms. There's
quite a few. Mastodon is one that comes up a whole lot. I like Mastodon. The
issue with it is, again, it doesn't duplicate Twitter's proximity to power,
that one selling point, and it's technical. And I know computer people are
like, no it's not. It is to the average person. The idea of choosing a server,
and I know this sounds silly, but the idea of choosing a server for a lot of
people is too much. It seems too complicated. And that's something that
Mastodon could easily fix if they were profit-motivated and that's the other
thing with them. They're not. They certainly don't appear to be. That whole
project seems to be very much ideologically driven, which means it's
probably a really good product because they're not in it for the money. At the
same time, because of that, their ability to fix the technical side of things is
limited because the easiest way to do that would be, you know, everybody signs
up and there's a default server. If you want to use the more advanced features
of Mastodon, of the Fediverse, well then you can just use them from there. But
somebody would have to pay for that default server, and that leads to ads, it
leads to commercialization, it leads to a whole bunch of things that from a brief
look, I don't think the people behind Mastodon have any interest in it. They do not
seem to be motivated by pursuit of profit. So you have that as an additional
issue. That's going to limit their ability to fight in the the social media
wars. You have some other ones that seem very geared to to certain demographics,
whether it be right-wing or left-wing, and they're going to face the issue of
not being able to provide that proximity to power, that ability to
to connect with the celebrities and the politicians and stuff like that. And just
having your side on it isn't enough. Truth Social proves that. You, on Truth
Social, you can reach out to Trump himself and maybe even get a retweet or
whatever they call it. That's not enough. It has to be all of it and that's the
hard part that it's the hard part to duplicate. One side isn't enough. It has
to be across the board. Now threads probably has the capital behind them to
incentivize politicians, large celebrities, to come to their platform.
They probably do and they could use a whole bunch of different means to make
that happen. But then they still have to overcome the the reputation associated
with Facebook and then they have to overcome the philosophical thing of it
being one company that pretty much controls everything which is something
that Americans are becoming more aware of partially due to social media. They're
becoming more aware of the risks of that. So I don't know that there is a major, I
don't know that there is a social media platform right now that is going to
replace Twitter. We're gonna have to see how threads really pushes forward and
how to handle it because the slightest mistake is going to drive people away and there's
a lot of room to make an error on social media because even the slightest error gets amplified.
So that's where we're at.
As far as me, I still don't know where I'm, you know, I've left Twitter for a while.
I have no idea where I'm going yet.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}