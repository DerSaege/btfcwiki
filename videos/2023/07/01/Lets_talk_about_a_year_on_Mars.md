---
title: Let's talk about a year on Mars....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IOdW1eY1FW8) |
| Published | 2023/07/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Four NASA volunteers will spend 378 days in a Mars-mimicking habitat for monitoring cognitive function and physical performance.
- The habitat is designed to provide challenges, with scenarios like equipment failures and communication issues.
- The goal is to simulate living on another planet before actually sending people there.
- The four volunteers have different backgrounds including emergency medicine, structural engineering, research science, and microbiology.
- This mission is the first of three planned, with the next ones scheduled for 2025 and 2026.
- NASA is taking steps to eventually have people on Mars, starting with these simulated missions.
- Updates on equipment failures, simulations, and responses will likely be shared with the public by NASA.
- Beau finds the mission exciting, akin to something from science fiction.
- He hints at providing further updates if he finds more information on the mission.
- The mission indicates progress towards humanity becoming multi-planetary.

### Quotes

- "They're taking steps to actually get us to Mars and have people on the surface."
- "It's pretty exciting stuff."
- "It's straight out of science fiction."

### Oneliner

Four NASA volunteers simulate living on Mars for 378 days to test cognitive function and physical performance, paving the way for future missions to the red planet.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Follow NASA's updates on Mars simulation missions (suggested)

### Whats missing in summary

The full transcript provides more details on NASA's Mars simulation mission and the importance of these simulated missions in preparing for human exploration of Mars. Viewing the full transcript can offer a deeper understanding of the challenges and goals of this unique endeavor.

### Tags

#Mars #NASA #SpaceExploration #SimulationMission #HumanityMultiPlanetary


## Transcript
Well, howdy there, Internet people, Lidsbo again.
So today we're gonna talk about a year,
kind of, on Mars, kind of,
and what it is designed to do.
Four volunteers with NASA will be heading into a habitat.
The habitat is designed to mimic Mars.
It's designed to look like Mars,
and it's designed to provide them with those challenges.
It's about 1,700 square feet, so it's not big.
The four people will be in there for 378 days.
During this time, NASA will be monitoring how time in that environment impacts their
cognitive function and their physical performance.
They will test them with scenarios, equipment failures, com issues.
They have to manage the resources, so on and so forth.
The idea is to kind of role play the whole thing
before they actually put people on another planet.
The four people that went, one is an emergency medicine
physician, one is a structural engineer,
one is a research scientist, and one is a microbiologist.
No moisture farmer as of yet.
This will be the first of three missions in this program.
The next one is slated to start in 2025,
and then there will be a follow-up in 2026.
At this point, it's still everything on the ground.
It's still everything here on Earth.
but the reality is they are taking steps
to getting us off this rock as a species.
They're taking steps to actually get us to Mars
and have people on the surface.
I mean, it's pretty exciting stuff.
It's, I think it is.
It's straight out of science fiction.
So we'll probably be following along as different things happen. I'm assuming
that NASA will provide updates when they have an equipment failure and how they
manage it or when they test them with something that is unforeseen and they
run these simulations to see how people respond. I'm assuming that they'll keep
the public updated on that. And if I can find press releases or whatever, any kind of information,
I will pass that along to you. Interesting times. Anyway, it's just a thought. Y'all
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}