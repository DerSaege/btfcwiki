---
title: Let's talk about McCarthy getting pushback on Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_IYMKEc1ZJU) |
| Published | 2023/07/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Bob McGunn says:

- McCarthy recently suggested that Trump may not be the strongest candidate to run in 2024.
- McCarthy's suggestion was a big step in his campaign to distance himself and the Republican Party from Trump.
- McCarthy quickly started to walk back his comments, realizing the significance of his statement.
- Despite walking back, McCarthy now claims that Trump is stronger than in 2016.
- Trump is a twice-impeached, twice-indicted person who lost his last election, making him not a political powerhouse anymore.
- The Republican Party needs to comprehend that they have to distance themselves from Trump to remain viable.
- Social media clicks and likes are not equivalent to votes, which led to the party's poor performance in the midterms.
- Keeping Trump close has created a hunger for power and will lead to more problematic figures like him.
- McCarthy must continue efforts to distance himself from Trump for the Republican Party's and his own benefit.
- Allowing Trump's influence to persist will only result in more damage and erosion of faith in the Republican Party.

### Quotes

- "The Republican Party has to get rid of Trump. They have to distance themselves."
- "Social media clicks and likes are not votes."
- "They created that appetite in Trump, then they spilled a glass of water on him."

### Oneliner

McCarthy's steps to distance from Trump are vital for the Republican Party's future, as they need to comprehend the damage he could cause if continued placated.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact McCarthy to urge him to continue distancing himself from Trump (suggested)
- Support efforts within the Republican Party to distance themselves from Trump (exemplified)
- Stay informed and vocal about the negative impact of continuing to support Trump within the party (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of McCarthy's changing stance on Trump and the potential consequences for the Republican Party if they fail to distance themselves from him.


## Transcript
Well, howdy there, internet people, it's Bob McGunn.
That didn't take long, did it?
Okay, so McCarthy recently suggested
that Trump may not be the strongest candidate
to run in 2024.
We talked about it, said he could expect a lot of pushback,
and said that was a pretty big step in his campaign
to distance himself and the rest of the Republican Party
from Trump.
Almost immediately he started trying to walk it back. He realized how big a step
he had taken. He has since said that Trump is stronger now than he was in 2016.
I mean you can believe that if you want to. It's worth remembering that the
person they're talking about is a twice-impeached, twice-indicted person who
lost their last election. They're not a political powerhouse anymore, and it's
worth remembering that he lost his previous election before the actions
related to the Sixth, before the indictments, before all of that. They are
looking at the same polls that led them to believe there was going to be a red
wave, the thing that did not materialize. If they had a red wave, McCarthy wouldn't
Trump anymore because he wouldn't need the votes from the Trump-aligned far
right Republicans. But social media, they're viewing social media clicks and
likes as votes. Same thing that we've been talking about, same thing that led
to their less than stellar performance in the midterms. They haven't learned
to anything. So he's trying to walk it back. That's fine. At some point, the
Republican Party is going to have to understand. They fed him after midnight.
They let him get that taste of power. They created that appetite in Trump, then
they spilled a glass of water on him, and they created a whole bunch of little
many Trumps that are going to be just as mischievous if not more so, that are
going to be just as problematic if not more so, and are going to continue to
erode the amount of faith that the average American has not just in the
Republican Party but in conservatives in general. The Republican Party has to get
rid of Trump. They have to distance themselves. It has to occur if they expect
to remain viable. Yeah, it's gonna be annoying. It's going to be chaotic.
Probably gonna have a bad outcome, but the longer this is allowed
to continue, the longer they all placate him and do whatever he wants, the more
damage he is going to cause. McCarthy needs to continue the attempt to
distance himself. That's the best thing for the Republican Party and more
importantly to McCarthy, it's the best thing for McCarthy. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}