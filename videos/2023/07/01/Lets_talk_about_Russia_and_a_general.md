---
title: Let's talk about Russia and a general....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ht4nVZHXi1A) |
| Published | 2023/07/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing recent events in Russia and the potentially misleading coverage.
- Speculating on the future implications of recent developments in Russia.
- Mentioning the detention of a high-ranking Russian official, General Serevikhin.
- Pointing out that in Russian society at this level, actions are often extrajudicial and politically motivated.
- Exploring the reasons behind General Serevikhin's detention and the ambiguous nature of such actions.
- Hinting at the possibility of a political purge starting in Russia.
- Expressing concern over the impact of these actions on military morale and discipline.
- Emphasizing the consequences of rigid military command structures in combat situations.
- Noting that Putin's actions could worsen the already low morale in the Russian military.
- Speculating on Putin's potential shift towards more authoritarian behavior.
- Suggesting that recent events may reveal Putin's vulnerabilities and lead to increased paranoia.
- Concluding with a reflection on the potential future developments in Russia.

### Quotes

- "It's palace intrigue stuff, not legal stuff."
- "No plan survives first contact with the enemy."
- "He is going to start behaving more and more like just an authoritarian goon."
- "He's losing face, he's walking around saying he's the king. You gotta tell people you're not."
- "So the paranoia will probably continue to spiral."

### Oneliner

Beau examines recent events in Russia, from the detention of a high-ranking official to potential military consequences, suggesting a shift towards authoritarianism and increased paranoia.

### Audience

Russia watchers

### On-the-ground actions from transcript

- Monitor developments in Russia and stay informed about the situation (suggested).
- Support organizations advocating for human rights and democracy in Russia (suggested).

### Whats missing in summary

Insights on the potential impact of these events on international relations and regional stability.

### Tags

#Russia #Putin #Authoritarianism #MilitaryMorale #PoliticalPurge #Paranoia


## Transcript
Well, howdy there internet people, let's bow again. So today we are going to talk about Russia.
The recent events that occurred there, the coverage of those events and how it might be
unintentionally misleading
and we'll talk about what it might mean for the future because
something happened and generally speaking once it starts happening
it continues. Once that first domino falls, there's a lot of them in the line
down the road, and it's something that I think most people expected to occur, but
now we have reporting that it is occurring. A general, Serevikhin, he was
the number two over Ukraine. Reporting suggests that he has been detained. Now,
Now the obvious question here is why?
Why was he detained?
That doesn't matter yet because we have to talk about what detained means, what it could
mean.
At this level of Russian society, you are not dealing with laws.
All of this is extrajudicial.
It's not about laws at this point.
It is about political posturing, jockeying for position, and whether or not somebody had
a bad day.
It's palace intrigue stuff, not legal stuff.
could mean anything from, general, you need to go chill at your mansion and get
out of the public eye for a little bit. Or it could mean, general, you need to
decide what you would like for your last meal. It could mean either one of those
or anything in between. Why? The conventional wisdom and the public
facing stuff suggests that Putin believes he knew about Wagner's little,
you know, cooing thing ahead of time. Is that true? Maybe. It could mean that or it
could mean that he was actively involved in the planning of it. It could mean that
he said something that Putin doesn't like. It could be that the boss of Wagner
has spoken highly of him and suggested that he should be the number one rather
than the number two. It could be that. We don't know and odds are we probably
never will. We don't know what's going to happen. What we do know is that if he
has been detained, as reporting suggests, the first domino has fallen in what
is likely to be a purge. This is bad for militaries. This is bad for militaries
because what happens is that as soon as something like this starts or even the appearance of one,
which we definitely have, the commanders who are worried about being next, they start employing,
let's just call them, aggressive disciplinary tactics on their subordinates and it rolls
downhill. It is horrible for morale when this starts. People don't know what to do.
The other thing is people are no longer allowed to exercise any initiative. The
commanders want those troops to follow the orders to the letter, which I mean
that sounds great military discipline and all of that stuff, but when you are
talking about combat, you have to be adaptable.
No plan survives first contact with the enemy.
So if you have to follow that plan no matter what,
you and your guys may not come back.
Putin starting something like this at this time
is a horrible move.
Russian military morale is already super low.
They already are already, let's just say,
less than 100% loyal. This is going to exacerbate that issue. It is horrible news
for Russia. It's good news for Ukraine. So that chain of events apparently has
started. Putin is probably going to start acting more and more like the
stereotypical authoritarian leader. See, he's always put on this good face and
walked out there and he's tried to avoid that image but deep down that's always
who he's been. He is now in a position where the mask has fallen. He's not
invincible. He's scared because he's weak and he's been shown to be weak. His
troops think he's weak. Best case scenario based on the the evidence that
we have is that his number two over Ukraine was cool with a coup or has
signaled to the boss of Wagner to such a degree that Wagner's boss would install
him. This is bad news for Putin. He is going to start behaving more and more
like just a authoritarian goon because he doesn't have to keep up the mask
anymore because he can't because he's scared. He was demonstrated to be weak.
He's losing face, he's walking around saying he's the king.
You gotta tell people you're not.
So the paranoia will probably continue to spiral.
We may see other people be detained.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}