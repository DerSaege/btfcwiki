---
title: Let's talk about SCOTUS, student debt, and second bites....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pUT2r1Z2Wbo) |
| Published | 2023/07/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court struck down Biden's student debt relief plan, leading to the emergence of a new plan under a different law.
- Beau questions whether the Supreme Court's decision was correct, expressing his belief that Biden was right but acknowledging his opinion doesn't matter in this context.
- He notes the lack of sufficient information to form an opinion on the new plan, as it is still a rough sketch with critical details yet to be revealed.
- The quick emergence of the new plan indicates prior preparation and effort put into it before the Supreme Court decision was released.
- Beau speculates that Biden may have strategically planned for the first shot to miss, allowing for a second attempt with a more favorable law, possibly anticipating the Supreme Court's decision.
- The new plan may impact non-discretionary income significantly, potentially relieving individuals of student debt payments.
- Beau stresses the importance of waiting for the final presentation of the plan before making judgments, as the early details may not necessarily translate into the final rules.
- He criticizes Republicans for supporting breaks for billionaires while being opposed to relief for student debt, considering it counterproductive.
- The Biden administration seems to have a contingency plan in response to the Supreme Court decision, aiming to tailor the new student debt relief within the court's boundaries.
- If issues arise with the Supreme Court again, the matter might need to go through Congress due to the Democratic Party lacking votes in the House.

### Quotes

- "The short version of this, too late, is we have to see the details."
- "I think the Biden administration was counting on an ideological decision from the Supreme Court and they have a contingency plan."
- "It seems like the second one [plan] would probably have a better chance because they have a very clear understanding of what this exact court wants."
- "If issues arise with the Supreme Court again, the matter might need to go through Congress."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

The Supreme Court struck down Biden's student debt relief plan, prompting a new plan under a different law and raising questions about strategy and future hurdles.

### Audience
Policy analysts, political activists

### On-the-ground actions from transcript
- Stay informed about the developments in Biden's student debt relief plan and its implications (implied)

### Whats missing in summary
The full context and nuances of Beau's analysis and speculation on Biden's student debt relief plan.

### Tags
#Biden #StudentDebtRelief #SupremeCourt #ContingencyPlan #DemocraticParty


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
Biden's student debt relief plan and the new plan
and the Supreme Court and whether or not the new plan
is any good and all of that stuff
and just kind of run through it all.
If you missed what happened yesterday,
the Supreme Court struck down Biden's student debt relief
plan within hours of that decision,
A second plan started to surface, new information started to kind of come out
and it's going to basically be a new plan under a different law.
Now, a whole bunch of questions.
First, do I think the Supreme Court's decision was correct?
No, I don't, but it doesn't matter.
I think Biden's right, I think they misinterpreted.
but realistically my opinion doesn't matter on that. Now the second plan. You
already see people starting to like say it's not any good. Okay I've looked there
is not enough information to have an opinion on it yet. There's really not.
It's a rough sketch. It's not even a plan. The at least what is publicly available.
When it comes to stuff like this the the devil's in the details and the details
aren't out yet. So I don't I don't really have an opinion on that yet. One thing I
do want to point out is that the bits and pieces that came out came out within
And then hours, hours of that decision being released, what does that tell you?
It tells you that it wasn't made up on the fly.
It wasn't made up after the decision.
It means that they had already started working for it.
They'd already started trying to put it together.
What does that mean?
When you look back at the timeline, especially the criticism of it, the plan gets announced
And he has a whole bunch of people basically saying he should have used
this other law that in many ways has more coverage and I got to admit I
didn't understand it now, where yet you have a decision from the Supreme
court striking down one and now he's using that law.
Everybody was talking about to get a second bite at the apple with something
everybody was like, this is kind of a better law to use.
and he has a road map from the Supreme Court I don't think that that was
unintentional because of that I'm willing to give him the benefit of the
doubt it looks to me like he knew the first shot was going to miss and he put
together a plan to catch it on the rebound what it's going to be I don't
know the little bits and pieces that have come out so far it's interesting
As far as raising what non-discretionary income is, that would have a huge impact.
It looks like under some of it, it looks like people may still have student debt, but not
have payments.
But again, all of this is really early on, and we don't know.
None of that may make it to the final rules.
So we have to wait.
It's not ideal.
I see a whole bunch of people, especially Republicans, cheering this.
I feel like that's probably not good politics as they support breaks for billionaires but
not for their voters.
That seems counterproductive, but whatever.
The short version of this, too late, is we have to see the details.
We have to see what the actual plan is once it's fully presented.
And I know somebody, because somebody already asked, like, this is what it looks like.
It looks like they were planning this.
Why isn't the plan ready?
Because they needed to see the decision from the Supreme Court.
now they can tailor the new student debt relief to be within the confines of what the Supreme
Court set out.
I don't think this is over and I think that not, I think the Biden administration was
counting on an ideological decision from the Supreme Court and they have a contingency
plan.
I'm going to wait and see what that plan is before I start to put it down.
This isn't something that, you know, well, I signed the order, they said no, too bad.
It's I signed the order, they said no, well, let's see what they say to this one.
It seems like the second one would probably have a better chance because they have a very
clear understanding of what this exact court wants.
The problem with this and the problem the Biden administration may end up facing is
that this Supreme Court, they're not exactly consistent, so they may run into another issue.
If that happens, then this has to go through Congress.
And right now, the Democratic Party does not have the votes in the House.
So again, once the decision has been issued, they can try to tailor it.
Maybe they can catch the three conservative judges who aren't super far right.
they tailor it to stay within the decision that was just released. But we'll have to
wait and see. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}