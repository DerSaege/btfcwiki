---
title: Let's talk about Trump, AZ, and another call....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Si12A2E2L68) |
| Published | 2023/07/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Arizona, Trump, and a breaking story similar to the famous phone call in Georgia.
- Allegations that Trump or his surrogates reached out to the Arizona governor asking for votes.
- Questions arise about criminal investigation and potential election interference.
- Speculation on why the story is surfacing now after a long time of silence.
- Possibility that media got wind of the story because the feds were already aware and involved.
- Uncertainty on whether the individuals involved have already spoken to investigators.
- Likelihood of investigators getting involved due to the potential pattern of reaching out to state officials to influence outcomes.
- Mention of a recent interview with Raffensperger relating to election interference.
- Implication that if not already, individuals will soon be discussing the matter with investigators.
- Concluding with a thought on the unfolding situation.

### Quotes

- "Hey, find me some, find me some votes."
- "Maybe something happened to make them feel comfortable talking about it in public."
- "Do we know that? No, no we don't."
- "They will be discussing it with them soon."
- "Anyway, it's just a thought."

### Oneliner

Beau talks about allegations of Trump or his surrogates reaching out to the Arizona governor for votes, raising questions of criminal investigation and potential election interference, speculating on the timing and involvement of investigators.

### Audience

News consumers

### On-the-ground actions from transcript

- Contact local representatives for updates on the situation (implied)
- Stay informed about developments in the story (implied)
- Support transparency and accountability in political processes (implied)

### Whats missing in summary

Insights on the possible implications of the emerging story and its impact on electoral integrity.

### Tags

#Arizona #Trump #ElectionInterference #Investigation #MediaCoverage


## Transcript
Well, howdy there Internet people, Lizbo again.
So today we are going to talk about Arizona
and Trump and Ducey and the new story
that's breaking out of Arizona,
which is a pretty familiar story.
We're gonna do this
because questions immediately came in.
So short version of what is being said
is the famous very perfect phone call
that Trump had with the Secretary of State
Georgia, the reporting says that basically the same thing happened in
Arizona as well. That Trump or surrogates reached out to the governor out there,
and basically was like, hey, find me some, find me some votes. Abbreviated version
of the allegation. The questions that immediately arose are, is basically, is
Is this going to lead to a criminal investigation?
Is this something that the feds are going to roll into the election interference thing?
The answer to that question is we don't know and we probably won't until there is an indictment
or some kind of finalized report if it goes that route.
The question that I would be asking is why is this story breaking now all this
time later? People who didn't talk about it for a long time suddenly felt
comfortable talking about it. Maybe something happened to make them feel
comfortable talking about it in public. It's entirely possible that the
reason the media is getting this story now is that the feds already knew about
it and those involved had already discussed it with investigators.
Therefore, they felt more comfortable talking about it with the media. Do we
know that? No, no we don't. But it's relatively safe to assume that if that
isn't the case that they'll be talking to investigators soon. If I had to guess
something occurred that made them feel more comfortable discussing it with the
media. But if that didn't occur, rest assured there are investigators on the
way there like now because that would establish a pattern of reaching out and
trying to put pressure on the state officials in furtherance of
altering the outcome. And that would be something that they could roll into the
election interference allegations. So whether or not they already talked to
the feds about it, they will be soon now. That kind of goes without saying
because this would line up very nicely with what they just interviewed
Raffensperger about. So it seems it seems like it should go without saying
that if they haven't already discussed it with them they will be discussing it
with them soon. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}