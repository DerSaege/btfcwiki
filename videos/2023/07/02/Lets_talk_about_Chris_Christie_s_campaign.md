---
title: Let's talk about Chris Christie's campaign....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zTjykgDYspc) |
| Published | 2023/07/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the rationale behind Chris Christie's run for the presidency.
- Believes Christie's main goal is not to win but to crush Trump and salvage the Republican party.
- Suggests Christie's campaign is driven by spite and pettiness towards Trump.
- Thinks that Christie aims to steer the Republican Party away from authoritarianism and outdated ideas.
- Points out that Christie's focus is on starting a separation from Trump rather than winning the presidency.
- Speculates that Christie is supported by larger-name Republicans who want to move the party towards a more rational direction.
- Views Christie's candidacy as a way to influence the party rather than a serious bid for the presidency.
- Notes that Christie's primary objective is to break ties with Trump and lead the party towards a more sensible path.

### Quotes

- "I truly believe he is running an entire presidential campaign out of spite and I think that's cool."
- "The Republican Party, they have to separate from Trump."
- "It's more of a single issue which is Crush Trump."

### Oneliner

Beau unpacks Chris Christie's presidential run, focusing on his goal to crush Trump and steer the Republican Party towards rationality.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Support efforts to steer the Republican Party towards more sensible and rational policies (implied).
- Advocate for a separation from Trump within the Republican Party (implied).
- Engage with larger-name Republicans supportive of moving the party in a more rational direction (implied).

### Whats missing in summary

Insight into the potential impact of Christie's campaign on the future of the Republican Party.

### Tags

#ChrisChristie #RepublicanParty #PresidentialRun #PoliticalAnalysis #RepublicanPolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Chris Christie
and his run for the presidency.
We're gonna do this because I got a question from somebody
and it made me realize that some people may not understand
why Christie is running and why in many ways he is
unstoppable, he's going to accomplish his goal.
Okay, so the question was basically like, you know, I get you're not covering all of
the candidates, but can you at least explain why Christie thinks he can win?
Bold of you to assume that he's running to win.
I don't think he is.
I don't think he has any intention on actually winning.
I don't even think that, I don't even think he wants to win.
I think he is out to do one thing and one thing only, crush Trump.
That's it.
I don't think his goal is to become president.
I think his goal is to attempt to salvage the Republican party.
I don't think he's trying to win.
There is a reason that he came out swinging immediately because he doesn't care.
He does not think that the MAGA base is worth courting, and he's trying to show the rest
of the Republican party that you can, in fact, mock Trump, that it's okay.
He's trying to be one of those Republicans that comes out of the gate and says, it's
It's time to separate from this man.
I don't think he's trying to win.
I think his goal is two-fold, partially save the Republican Party, save it from itself
before it continues to spiral down that path of authoritarianism and super old ideas that
nobody supports anymore, an aging voting base, all of that stuff. He's trying to
stop that spiral and I don't think he likes Trump. It's that simple. I aspire to
this man's level of pettiness to be honest. I truly believe he is running an
entire presidential campaign out of spite and I think that's cool. I fully
support that. The Republican Party, they have to separate from Trump.
They've got to get away from him, and it has to start with somebody. And I think
Christie is hoping that his legacy can be, he's the person that started it, that
said it was okay to come out and basically be like, hey, you know Trump,
he's a tubby trader and and that's that be his campaign. I really don't think
that he planned on winning at any point in time from the very beginning. I think
there is a concern among a number of Republicans that even if Trump is out of
the race, the Republican Party isn't done flirting with real authoritarianism.
And you have to remember, I know there's so few and far between, but there are a lot of
people who were in the Republican Party who truly believed in a small government.
They may not like you, they may not like your identity, your orientation, your color, anything
about you, but they don't want the government involved. That was at one time
something that rank-and-file Republicans believed. Now the people at the top, they
were always about the money and the government's a good way to get that, but
there are some who truly believe that's where the Republican Party should be and
And I think he's trying to steer the Republican Party that way.
I don't think he's trying to win.
And I don't think that he had any delusions about winning when he entered the race.
He's just trying to kind of chart a course.
I would imagine that there are quite a few larger-name Republicans that are
supportive of Christie's move and maybe even maybe even informally advising
to try to get the Republican Party back towards something sane.
So that's that's what I think it is. I don't really I don't see him as a
serious candidate to be honest. It's more of a single issue which is Crush Trump.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}