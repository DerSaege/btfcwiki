---
title: Let's talk about Kremlin games and generals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7K9ToWbCdyY) |
| Published | 2023/07/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing games in the Kremlin and their implications.
- A detained general in Russia is being treated well, not in jail.
- Putin's concern about potential coup involvement.
- Speculation about two other Russian leaders being detained.
- Lack of evidence supporting the detention of the two leaders.
- Situation under a bridge in Ukraine involving Russian forces.
- Split between military bloggers and the Ministry of Defense.
- Criticisms of Russian leadership and morale issues.
- Russian troops criticized for lack of adaptability.
- Fallout from recent events causing headaches for Putin.

### Quotes

- "Detained doesn't always mean detained, remember?"
- "The fallout from his little march towards Moscow, it is not over."
- "The morale issues, they're starting."
- "Whereas with these two, they just haven't been in the public eye."
- "It seems as though they have sided more with Wagner."

### Oneliner

Beau provides insights on Kremlin games, a detained general, and split in Russian military loyalty, causing morale issues and headaches for Putin.

### Audience

Political analysts, Kremlin watchers.

### On-the-ground actions from transcript

- Monitor developments in Russia and Ukraine (suggested).
- Stay informed about international relations and their impact (suggested).

### Whats missing in summary

Further details on the potential impacts of the Kremlin games and the ongoing situation under the bridge in Ukraine.

### Tags

#Kremlin #Russia #Ukraine #PoliticalAnalysis #MilitaryLoyalty


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some of the games
being played in the Kremlin and what those games
can teach us and what we can learn from them.
Because they provide a little bit of insight
and a little bit of confirmation
and generate a little bit more speculation.
And they give us our first look at whether or not
morale is really being impacted.
Okay, so in a recent video, we talked about a general who appeared like he was being detained.
At time of filming, he is being detained. That is confirmed now. However, he's not in jail.
Detained doesn't always mean detained, remember? He's not in jail, and they made it very clear that he was not in jail.
He's being held elsewhere. Where elsewhere is, we don't know, but he's being treated well.
The investigators are very conscientious in how they are interviewing, questioning, quizzing
him about his possible involvement in the whole little Wagner dust-up thing.
That in and of itself is a pretty clear indication that Putin is weak and he knows it.
Let's be clear, Putin and his investigators are concerned that they might be too mean
to somebody who potentially participated in a coup.
The reason they are concerned, the reason they are being nice, the reason he is being
well cared for is because they're worried that he has the loyalty of the troops, that
his authority is more important than Putin's, that he actually commands them, and it's
a legitimate worry.
So Putin is being very nice to him in an attempt to not upset the soldiers.
Now there is speculation about two other Russian leaders.
Those two, the rumor is that they're detained as well.
The thing is the rumor is entirely based on them not being in public.
That's it.
There's nothing else supporting it.
Yes, that information supports my theory and all of that, but we don't know that.
We do not know about those other two.
It's not like Sarah Vigan.
That is confirmed.
And when it came to him, there were people that were like, yeah, he's been missing and
yeah, they came and snatched him.
Whereas with these two, they just haven't been in the public eye.
One of them, it seems likely that they're also detained, but we don't know that yet.
So don't factor that in.
It's possible.
Even when you look at the headlines that are about it, it's like, where is general so-and-so?
No actual statement that they're being detained, just the question about where they are.
Then we have another thing.
We have a situation under a bridge in Ukraine.
There is a small number of troops that are under a bridge.
Russian forces have been ordered to push them out.
Russian military bloggers, who have traditionally been pretty supportive of the Kremlin and
the Ministry of Defense, they have been incredibly critical of the Ministry of Defense and their
ability to dislodge this small number of Ukrainian troops.
And what it demonstrates is that they aren't rallying around the Ministry of Defense.
has, a split has developed now between the military bloggers who are very essential to
the Russian war effort and the traditional military.
It seems as though they, because they are very nationalistic, because they are very
aggressive, especially, you know, from behind the keyboard, it seems as though they have
sided more with Wagner and that is going to continue to cause issues for the Ministry
of Defense because they're going to be second-guessed.
The morale issues, they're starting.
The criticisms are that the leadership is just throwing Russian troops in a straight
line and it is part of that problem of not allowing them to be adaptable in something
that occurs after, you know, generals start being detained, but it's worth
noting most of these operations actually occurred before anybody, well at least
anybody in the West, knew that that general was being detained. So we don't
know that those are related, but this is not over. That's the important takeaway
from this. The fallout from his little march towards Moscow, it is not over and
it is causing Putin huge headaches. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}