---
title: Let's talk about what Republicans can learn from Lindsey Graham....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gzmJuGQKrtk) |
| Published | 2023/07/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Graham, once an off-and-on supporter of former President Trump, is currently supporting him and making campaign appearances.
- Despite endorsing Trump and appearing in his support in South Carolina, Graham was booed off the stage.
- Beau warns the Republican Party to take note that Trump's faction is no longer about policy and principle but solely about Trump's personality.
- Courting Trump's faction poses a danger to the Republican Party as they have shifted from traditional Republican values to following Trump exclusively.
- The authoritarian embrace of Trump's faction since 2015 has the potential to take down long-standing Republican members, like Lindsey Graham in South Carolina.
- Trump is seen as a liability to the Republican Party, but many refuse to acknowledge this reality.
- Those deeply entrenched in supporting Trump are unlikely to change their views, even if indictments are successful against him.
- These individuals are now considered more Trumpist than Republican, supporting Trump alone rather than the party's policies.
- Supporting Trump may not guarantee votes from his faction, and adopting his far-right agenda could alienate moderates.
- Beau warns against mistaking social media support for actual votes and urges for a separation of Trump from the Republican Party.

### Quotes

- "Courting them is a danger to the Republican Party because eventually it will be Trump who is calling all of the shots."
- "Trump is a liability to the Republican Party. They keep trying to ignore it."
- "Even if these indictments are successful, it's not going to change their opinion of him."
- "Supporting Trump will not necessarily get you their vote and supporting Trump and his policies in that far-right agenda will absolutely lose you the moderates."
- "Don't confuse social media clicks with votes. They only support one person and it's not you."

### Oneliner

Senator Graham's booing incident in South Carolina underscores the danger of the Republican Party's shift towards Trump's personality over policy, urging a separation for those aspiring for more than blind allegiance.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Recognize the shift towards personality over policy within the Republican Party and advocate for a return to core values (implied).
- Take steps to separate Trump from the Republican Party to avoid further alienation of moderates (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Senator Graham's support for Trump, the risks of catering to Trump's faction, and the potential consequences for the Republican Party. Watching the full transcript can offer a deeper understanding of these dynamics. 

### Tags

#RepublicanParty #Trump #ShiftInPolitics #PartyPolicies #Moderates


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to
Should learn from it. Senator Graham has been an off-again, on-again supporter of former President Trump.
But right now he's a supporter. He endorsed
Trump's candidacy early, and he's actually trying to help him on the
campaign trail, making appearances with him. That was something that was supposed
to happen in South Carolina. Graham was there in support of Trump. After
endorsing Trump. He took the stage and got booed off the stage. The Republican
Party needs to take note. They need to pay attention to this. Trump's faction of
the Republican Party, they're not Republicans anymore. They gave up on
policy and principle, they now follow personality. Trump and Trump alone.
Courting them is a danger to the Republican Party because eventually it
will be Trump who is calling all of the shots, even in your district. That
That faction of the Republican Party, that authoritarian embrace that has
just run amok since 2016, 2015, it's something that has the ability to
take down long-standing members of the Republican Party. This happened to Lindsey
Graham in South Carolina. Do you think the people who booed him are gonna vote
for him? Trump is a liability to the Republican Party. They keep trying to
ignore it. They refuse to see horror on horror's face, but that's the reality of
it. If you aspire to be anything other than somebody who takes direct orders
from Trump and does whatever he says no matter the personal or political cost, if
you want to be anything other than that, Trump has to be separated from the
Republican Party. Those people who are that far down that spiral, they're never
going to give him up. Even if these indictments are successful, it's not
going to change their opinion of him. They're no longer Republicans. They are
in fact Republican in name only. They're Trumpist. That's it. Supporting Trump will
not necessarily get you their vote and supporting Trump and his policies in
that far-right agenda will absolutely lose you the moderates. It's math. Don't
confuse social media clicks with votes. They only support one person and it's
not you. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}