---
title: The roads to emergency preparedness questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=O0_K1MT3JxI) |
| Published | 2023/07/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares insights on the progression of packing for emergencies from excessive equipment to minimal essentials based on experience outdoors.
- Advises on keeping survival food and water in Arizona's extreme heat, recommending specific products for sustained high temperatures.
- Addresses urban prepping for economic fluctuations, focusing on alternative cooking methods and supply accessibility in dense populations.
- Suggests persuasive tactics to encourage friends and family to prioritize emergency kits without sounding alarmist.
- Emphasizes the importance of being prepared with basic supplies in case of natural disasters or utility failures.
- Explains the significance of a knife in emergency kits as an indispensable tool for various tasks.
- Stresses the value of knowledge over equipment in survival situations, advocating for downloading survival manuals and guides.
- Recommends incorporating rubber bands, super glue, and WD-40 in emergency kits as versatile and useful items.
- Encourages acquiring a US Army Survival Guide and regional edible plant identification book for emergency preparedness.
- Talks about the mentality behind leaving some items in packaging for improvisation during survival situations.

### Quotes

- "Knowledge weighs nothing. It's much easier to carry."
- "When did Noah build the ark? Before the rain."
- "The National Guard has to come in so often because people aren't prepared."
- "Knowledge is lighter than any equipment you can get."
- "Don't take criticism from people you wouldn't take advice from."

### Oneliner

Beau shares insights on emergency preparedness, from packing essentials based on outdoor experience to persuasive tactics for urban prepping, stressing the importance of knowledge over equipment.

### Audience

Emergency Preppers

### On-the-ground actions from transcript

- Contact local emergency preparedness groups for guidance and support (suggested).
- Purchase survival food and water suitable for extreme temperatures (exemplified).
- Encourage friends and family to prioritize emergency kits using persuasive tactics (implied).
- Incorporate rubber bands, super glue, and WD-40 into emergency kits for versatility (suggested).

### Whats missing in summary

Deep dive into emergency prep strategies and persuasive techniques to encourage preparedness.

### Tags

#EmergencyPreparedness #SurvivalSkills #UrbanPrepping #KnowledgeIsKey #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the roads
to emergency preparedness, Q&A style.
These are your questions.
I haven't seen them yet.
So I'm going to go through them,
give you the best answer that I can with them,
and we'll just kind of answer your questions
as best as I can, a little informal thing.
Okay, so starting off, I watched your previous survival video with you in the Marine.
Why do people like you have so little in your bags?
Is it overconfidence at play?
It's not overconfidence, it's confidence.
There's this weird curve that happens when you start spending a lot of time outdoors.
You initially, you get out there and you want something.
And eventually you start getting all of this different equipment to carry with you.
And then you get to the point where you don't want to lug all of that stuff.
So rather than having a bunch of equipment, you learn how to do as much as possible with
as little as possible.
Knowledge weighs nothing.
It's much easier to carry.
So your outdoor kit, it gets larger and then it shrinks.
Both of us have spent a whole lot of time outdoors and both of us have a lot of instruction
on it.
So that's...
Overconfidence.
Yeah, that's what it is.
Okay.
I live in Arizona.
What kind of food can I keep in my car that won't go bad?
What about water?
Yeah, that's, that's, we run into a similar issue here because of the heat.
Okay, so in the survival video that's mentioned here in the first question, we're going through
like pre-packed emergency bags in one part of the video, and I'm taking stuff out of
them and I'm basically showing that none of those you know total emergency kits
are complete. In it there's quite a few of them where I pull out this orange brick
of food. It's like wrapped in foil. That's what you need. It's not it's not
designed for your car. It's actually designed for like life rafts and stuff
like that it is very it's very resistant to heat now the reason I'm making fun of
it in the videos I go ahead and tell you this it tastes horrible it's just like
this rock hard brick but it will keep you alive so if you have to go out in
the desert a lot maybe throw some under your seat as far as water you have the
issues there, your best bet is probably that canned water company.
It's not cheap.
It's like, I want to say it's like $36 for a 24 pack or something, but it's, I want to
say it's called Blue Can Water, Blue Water, Blue Can, something like that.
Search for canned survival water and look for the one with the blue packaging.
I think that their heat tolerances is up to 140 degrees.
So that's probably going to be your best bet, but double check that.
That's a...
Trying to keep food and water in something that gets really hot is hard, and those are
really your only options, despite the fact that that food is... well, you'll try it.
But the good news is, it's relatively inexpensive and it's good for like five years at a time,
so you don't have to rotate it out.
Okay, for urban-suburban prepping for, say, a possible severe economic fluctuation, how
would you modify standard supplies with an eye to urban environments power grid reliance
for cooking, dense populations, and apartment-dweller safety concerns.
Power grid reliance for cooking.
Yeah, that won't work.
You can't do that.
You have to find an alternative.
I'm assuming you're saying that because you can't have a grill.
Got you.
Okay.
So there are these battery packs.
I have one that came with the solar generator.
In fact, it is the solar generator.
But Duracell, I think Energizer makes one too, they're basically just giant rechargeable
batteries and they pack enough juice to run like small appliances.
So maybe get one of those if you can find one that isn't horribly expensive.
I can tell you right now if I had to pay full price for the one that I have, I wouldn't
have it. Look for one of those and then you can use that to like plug in like a
a flat top grill. It probably wouldn't run a microwave I don't think just
because of the amount of energy of pools but you could use the flat top grill to
boil water, stove, all of that stuff. All of the other stuff, pretty simple. Just
try to gear everything as compact because you probably don't have a lot of
space and as mobile as possible. Make sure you have something that you can
load all of your stuff up in because you may have to change locations. Okay, I've
been gently peer pressuring friends and family to keep a well-stocked emergency
kit around and have been using your good advice of reasoning. It is for a possible
interruption of services, rather than going off on how the sky is falling.
Yeah, that's good.
Any other advice you might have to coax them into making it more of a priority?
Now looking forward to another heat dome here, and it's going to be fire season soon.
Okay, so if you've talked to somebody about it and you've used the logical, you know,
these are things that happen in our areas, you should be prepared for it conversation
and it doesn't go anywhere, you may have to step outside of the norm.
Things that I have seen work make it fun in a bizarre way.
Rather than talking about actual emergencies, talk about the zombies.
I've seen that work.
I have seen people literally trick people into getting what they need.
was just absolutely resistant to doing anything to prepare for some kind of emergency. However,
one of their hobbies included a need to have a good first aid kit. And you start with the
little stuff. Find something that they're interested in that you can help frame it around.
What's your EDC knife? For those that don't know, EDC is everyday carry. A Spyderco. I
I actually have a couple, I don't, I just grab whichever one.
There's personal preference, there's no real specific reason for that other than it's
a good compact folding knife that I can carry just about wherever.
So I'm in an area of the East Coast that has not seen a lot of bad weather yet.
I recently purchased some things that would be good in an emergency.
Wind-up flashlights and an emergency radio with a charger, a large bucket with hazmat
bags for a restroom, water purifier, portable grill that can use coal or wood, and other
basic stuff.
My parents and others acted like I was panicking or doomsday prepping with these basic things.
It's only a matter of time before we have a natural or other disaster due to global
warming or weird weather, just lost utilities or whatever, electrical grids, water purification
sites, etc.
Don't get updated much.
I plan on doing even more when we get to our house.
Right now we're just running.
I'm not digging out a bunker or anything.
How do I convince others that I'm not crazy for thinking this is this way and to get them
to take it more seriously?
There's a whole lot of people you're going to run into that they think it's ridiculous
until they need it.
This is one of those things.
When did Noah build the ark?
Before the rain.
You have to have this stuff together.
If you're up in that area, maybe bring up Sandy, Hurricane Sandy, and talk about how
long people were without because of that.
The stuff you have here, yeah, you're right.
This is super basic.
This is stuff that if your power goes down for three days,
you're going to need.
I'm going to guess that you live in an urban area.
So some of this, maybe it seems a little bit more odd.
But yeah, this is all very normal stuff.
I would try, again, the base reasoning
Why people need it what happens when they don't have it if that doesn't work try something fun. Try something off the
wall
But again when
When you start putting stuff together for emergencies just remember it's for an emergency the reason
The National Guard has to come in so often
Often it's because people aren't prepared and when it comes to this topic, don't take
criticism from people you wouldn't take advice from.
Do you have any suggestions for cabinet latches or other things for preparing your house for
weather or earthquake emergencies?
I've never actually thought about that.
That's not something we really have to worry about here.
If the weather is bad enough where I live, the types of strong weather that we have,
if you're worrying about the cabinets opening, you're going to be getting new cabinets anyway.
That's one of the things about hurricanes, either your house makes it or it doesn't.
I would look not for something for emergencies,
I would look for stuff for child-proofing your home.
I would look for that,
because those are probably built to a better standard
than anything that is purpose-built for emergencies.
That's where I would start trying to find something for that.
I never even thought about that, though, that's...
Yeah that would be annoying in an earthquake to have like everything fall out on you.
Let's see.
I've watched some of your other preparedness videos and the list of five things mostly
make sense, but why the knife?
I assume if I'm ever in that situation I'll figure it out, yeah, but it seems better to
know ahead of time.
I'm guessing that the answer is that it's a useful hand tool, but there's plenty of
other hand tools that it'd be useful and none of them get a spot. Okay, so that list, food, water,
fire, shelter, first aid kit, knife. Okay, first, that's the minimum. If you wanted to add like a
small toolkit, pretty smart. I keep one in my car. That's just your absolute base. You will always
need a cutting instrument of some kind. It doesn't matter. Try to go a day
without one, just in a normal situation. And then think about having to open,
let's say if it's an emergency, you get help from the National Guard even. They
come and take care of the food for you. You have to open that package and they're
not always easy to open. You will, you may need to tie something down and you
may need to cut rope. When you're putting your shelter together, you're
probably going to need to fasten something. It is one of those things
that when you're in the situation, you'll definitely be glad you have it.
Okay. What's the number one thing people should have before being in a survival
situation? Curiosity and an internet connection. Again, knowledge weighs nothing.
Knowledge is lighter than any equipment you can get. Downloading like a survival
manual and assuming that the internet archive is still up, archive.org, you can find most
of them for free and just download them because they're military filled manuals and you paid
for it.
So yeah, it's a matter of having the knowledge more than having the equipment.
If you're talking about the items you need, food, water, fire.
Fire would include flashlights, anything to create a fire to cook, but also to be able
to see food, water, fire, shelter, first aid kit, your meds if you need meds, your pet
meds if they need them, and a knife.
Okay, emergency preparedness.
What's an odd, unusual, unexpected item
that everyone should have?
Rubber bands, super glue, WD-40.
Yeah, all of that is good.
I think the strangest thing that I always keep on hand
is applicator-less tampons.
They are wonderful kindling.
To actually get a fire started,
You can fluff them out, they work very, very well, and because they're compressed, they
don't take up a lot of space in your bag.
And they're also, obviously, they're absorbent.
There's a whole bunch of other things that they can be used for.
What kind of book should I get?
US Army Survival Guide.
Start there.
for free. And then from there I would look for, you know, edible plants that are
in your area for your region, be able to identify those, little stuff like that.
Again, it depends on what you're preparing for, but a survival guide and a
really good first aid manual. Maybe a woofer textbook would be good. Why is
some of your stuff in packaging in the survival video? Ah yeah I leave some
stuff in boxes or like I have a pack of lighters that is still in the package
it came from from the store.
So one of the key things about survival,
especially if you're talking about not just like sheltering
in your home for a normal emergency,
but like you're going out in the woods type of thing,
it's a mindset of being able to use everything for something.
And as an example, the plastic from around that lighter
container, you could use that to put a little bit of peanut
butter in for a snare so it's not sitting on the ground, so
the ants don't come in, or it takes them a little bit longer
to get there.
There's a lot of little stuff that you can use the
packaging for.
The plastic bags, that useless plastic packaging that you
often find in stuff, you can use that to carry water.
you could poke holes in it and maybe even make a net with it,
you have to be able to improvise with stuff like that.
So generally, when it comes to stuff that doesn't have
to come out of the package, I don't take it out
until I absolutely need it.
What should be in an IFAC, an individual first aid kit?
OK, so that's one of those questions I get a lot.
And I answer it and then say that it's the wrong question.
OK, so dressing, tourniquet, chest seals, shears, something
to get an airway, tape.
Now the real question, do you know how to use that stuff?
One of the things that happens, especially
with medical equipment, is people will get a first aid
kit and get a good one, but they won't
know how to use it. My advice when it comes to that is to get the best
training that you can get wherever you're at and then get a kit that is
equal to your skill. Having the stuff and knowing how to use it are two very very
different things. It is much better to know how to use it because if need be
need be, you can improvise the supplies.
You can improvise a chest seal out of plastic packaging, if
need be.
You can't learn how to do it on the fly in a situation if
you don't have access to anything that can teach you.
So the standard kits that you buy that are actually labeled
as IFAC, and they come in the little Mali pouch, most of
those actually have pretty much everything that you need.
It's one of the few emergency preparedness related items
that normally they come well stocked.
You customize it to your liking,
like I will always add, like way more gauze
than can ever humanly be used
because one time I didn't have enough
and I will never be in that situation again.
But most of them come with what you need, but before you really can use it, you have
to have the training.
Not sure if this really fits, I've got a young guy at work who's a lefty, and he's one of
those that thinks things have to get way worse before they get better, and he talks about
war all the time.
He doesn't know I pulled two in Iraq and one in Afghanistan.
How do I tell him that there has to be a better way?
When you are talking about those people who kind of romanticize the idea of conflict,
some of it's just age.
Some of it is never having seen it for real.
They feel like they're desensitized to it because they see it on screens.
The only thing that I have ever actually seen work, and keep in mind I have seen this tried
multiple times and I've only seen it work twice, is actually kind of teaching them,
but not really.
You can pull footage right now, especially with everything going on in Ukraine, there's
a lot of footage that's available.
You show them clips and ask, why did this person get hit?
The first few, yeah, show them clips where the person obviously made a mistake.
they stood up when they shouldn't have, they stepped somewhere they shouldn't have, you
know, things like that.
And then show them some where they didn't do anything wrong.
In fact, recently on Twitter there was footage of some Ukrainian soldiers and they were clearing
a house and they're going up these stairs.
And it's the bad kind of stairs, you know.
the stairs with the curve in it and the open spot, you know, up to your left.
And they're probably halfway up that second portion when a Russian soldier opens up on
them.
Two of them get hit.
If you look in the comments and in the replies under that video, you will see tons of people
trying to figure out what they did wrong, and then do anything wrong.
A lot of people have a view of it that, you know, if you do everything right, if you find
the right set of buttons to push, you'll make it through this level.
A lot of them don't understand that sometimes you can do everything right and still not
make it back.
I think that's one lesson that might be helpful.
But I know it's absolutely frustrating because generally speaking the people who have adopted
that mindset, they're super dedicated and if they would commit to something else, if
they would put the same amount of energy into actively trying to stop, you know, that from
happening in addressing issues or just local organizing or whatever, it would be amazingly
effective.
But it's really hard to get them out of that mindset once they're in it.
Another clip that you might want to show them is the footage of the cops that came out of
the school out there in Texas.
These are people who, they've seen traffic accidents, they've seen all kinds of things
and they've been desensitized, you know, via screens the same way everybody thinks
they have, reality hits different when you slip in it.
And seeing them coming out sobbing and throwing up, it might be useful.
There are a lot of people that definitely romanticize conflict to a level that doesn't
make any sense.
That's the only thing I've ever seen work though, is actually showing them that it's
not a thing where if you make the right move, you're guaranteed to make it.
It's not a video game.
The right combination of moves doesn't get you through.
Sometimes it's just luck.
Let's see.
You once wrote an article, I can't find anymore, where you went into a Walmart and built a
survival kit from scratch.
Dollar General.
But yeah, I remember it.
The website doesn't exist anymore.
Can you do a video like that and a target, if I can be picky?
I was at Target like three days ago.
Yeah, I can try to do that.
I can try to do that again.
The purpose of doing it at the Dollar General
was to show that it can be done very, very inexpensively.
Target, it's definitely more expensive than Dollar General.
But yeah, I can try to put something together like that.
Let's see.
I know it would be hard with your production schedule
eight videos per day but have you ever thought about doing a video over a couple of days where
you actually do everything survival related? So on this channel we have a survival video where
we demonstrate a lot of stuff. One of the things that you run into is the way you would really do
it like let's say fishing okay if you were to fish the way you would fish in
a survival situation using nets and doing a lot of things and you made a
video about it and you put it online fish and wildlife would show up at your
house. This has actually been a topic on how we can do this and remain within the confines of what
you're actually allowed to do because in a survival situation a lot of the rules and regulations that
govern your behavior in normal civilized society do not exist. When we did the one that's on this
channel we were actually going we have brought all this stuff to do the snares
and then we realized if we did that it wouldn't be very useful unless we showed
how to prepare it which is not something I would want to put on my channel you
know there's a lot of there's a lot of kids and there's a lot of people who
don't eat meat, who would not be appreciative of that just
showing up in a video that they were watching.
So there's a lot of limitations to it,
but this is something we've been actively
trying to find a way to do.
So maybe if we can figure out how to do it.
Let's see.
This might sound weird, but I learned a lot
from that video watching you go through the pre-packed bug
out bags.
Watching you quickly rate stuff as garbage or useful
was informative.
Could you do that again at some point
and let us see the items up close and tell us why things
are good or bad?
I'm sure the homeless people would appreciate it.
Yeah, we can do that again.
I'll do that in the same video when we do the target thing.
And that last line for anybody that doesn't know,
We went through a bunch of pre-packed emergency bags.
And then when we were done with them,
we gave them to people who were sleeping rough
and would definitely be able to use it.
But yeah, we can do that.
And yeah, I will explain why, rather than this is cool,
this is garbage, so on and so forth.
OK, so that actually looks like all the questions,
unless I skipped a frame here.
Yeah, that's it.
OK, so there's the Q&A for that.
We have more of these coming, because you all
sent in a whole bunch of questions
over the last 30 days.
I know that I have like three or four sets of questions
for videos.
So I will try to get to them as soon as possible.
There's just a whole lot of news.
If everybody could stop trying to, you know,
coup each other or get arrested or whatever,
it would be super helpful.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}