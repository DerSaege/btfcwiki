---
title: Let's talk about why they're signaling about the planes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jVeTnttfHdA) |
| Published | 2023/07/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the purpose behind the release of information on secret planes by major world powers with defense research programs.
- Major world powers have different approaches to releasing information on defense technology, with the US inducing uncertainty, China revealing products after development, and Russia exaggerating specs.
- The US strategy is to create uncertainty to influence other world powers' spending and production decisions.
- Russia's strategy of overestimating specs worked well during the Cold War but is less effective now.
- Reveals how the US military-industrial complex responds to Russia's announcements by designing products to counter the overestimated specs.
- Mentions a situation where Russia's high-tech equipment was countered effectively by the Patriot missile system because it was designed based on Russia's exaggerated specs.
- Talks about how Russia's strategy now focuses on inducing fear with an overbearing attitude in their information operations.
- Describes how the West sometimes releases false information to make other nations waste resources trying to duplicate impossible technologies.
- Points out that the audience for these information releases is not the general public but other world powers, using the tactic of head games and trolling on an international level.
- Comments on the difference in attitudes between the aviation world and the Navy regarding the release of information, attributing it to institutional culture differences.

### Quotes

- "It is designed to influence spending and production in competitor nations."
- "It's just a thought y'all have a good day."

### Oneliner

Beau explains how major world powers use different strategies to release information on defense technology to influence spending and production in competitor nations.

### Audience

Defense policymakers

### On-the-ground actions from transcript

- Analyze and understand the different strategies major world powers use to release defense information (implied).
- Stay informed about international defense strategies and their implications (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how major world powers use information on secret planes to influence global defense strategies.

### Tags

#DefensePolicy #NationalSecurity #InformationWarfare #MilitaryStrategy #InternationalRelations


## Transcript
Well, howdy there internet people, it's Bo again. So today we are going to talk a little bit more about the secret
planes
But not really
Some questions came in and they're good questions and they help shed light on
foreign policy and how things work
One set of questions was really people like who is this for? Like why would they put this information out?
Is this for potential customers?
why on earth would a private company intentionally put this stuff out? The
other one was from people mostly in the Navy who asking the exact same question,
why would they put this out? You know if you're in the Navy you don't disclose
any specs about subs or anything like that, right? Okay the answer is it's kind
simple. All major world powers that have their own defense research, okay, they
all do something like this. They do it different ways. The West, the US in
particular, likes to induce uncertainty. The target of that messaging, it's not
potential customers. It's not really even the American people. It's not aviation
nerds, it's other world powers. The West likes to hint to things but not actually
disclose specs and we'll come back to why in a minute. China doesn't really
have a pattern that I'm aware of yet. They tend to really kind of build the
product and then say this is what we have. You know there's not a lot of
signaling that I've seen, not on the same level. Russia likes to say, hey, we have
this, here are the specs, and like overestimate them, okay, which really
worked during the Cold War because it made the West spend a bunch of
money trying to compete, to make sure there wasn't a bomber gap or whatever.
doesn't work so well now because in the beginning of the Cold War things were a
little different. That military-industrial complex that the US has
now, when Russia releases specs saying, hey we have this product that can do
this, the American defense industry, they're like, awesome now we get to
build a new product that we're gonna sell to counter it. So they just design
And the American product that is going to be used to counter that piece of equipment
to the specs that are overestimated.
This is kind of what happened with the whole super missile that Russia had.
They had this high-tech piece of equipment that they thought was going to be incredibly
effective and then the Patriot showed up and they got all mad and the people who operate
patriots are kind of like I'm sorry we built our stuff to defeat what you said
you had and so in today's world Russia's strategy doesn't work as well it worked
really well in the 50s and 60s it was incredibly effective so that's what they
do they try to use fear in the sense of this is what we can do and you can't
stop it. That very overbearing attitude with their information operations. The US,
it's uncertainty. It's still to induce fear, make no mistake about it, it's just
less direct. Because now China has to wonder, well how far along is that
silhouette? And keep in mind, we've seen the silhouette, but it is really similar
to that F-19 from the 80s. I mean for all we know that's not even what the plane
looks like and they were just like yeah use that one and throw that up there. It
could be a lie and then the other thing that the West does because there is
uncertainty sometimes the West will be like checking out a new technology
determine it's not feasible, and then release information saying that they're
doing it. Because then it might induce other world powers to waste a bunch of
resources trying to duplicate something that is impossible. So the the audience
for those posts, it's not us. It's other world powers, it's head games, it's
trolling, just on an international level. Now, I'm not sure why there is such a
difference in this attitude when it comes to the way the aviation world
looks at it in the way the Navy looks at it. Because the Navy, like, they don't
release anything ever and they tend to lie and underestimate their capabilities
when stuff does go out. It's probably an institutional culture thing. I don't
really have an answer to that as to why. It definitely seems different though. But
that's the answer. It is designed to influence spending and production in
competitor nations and they do the exact same thing just in different ways. Anyway
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}