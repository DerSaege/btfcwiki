---
title: Let's talk about SCOTUS and electoral politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ibxSfGwcDic) |
| Published | 2023/07/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the connection between the Supreme Court and electoral politics, addressing a message expressing hopelessness towards electoral politics and the Supreme Court's actions.
- Points out that the average retirement age for Supreme Court justices is around 80, with Thomas and Alito being among the oldest.
- Mentions the possibility of Thomas and Alito retiring soon, potentially impacting the court's decisions.
- Emphasizes that the Supreme Court justices are products of electoral politics, even though individuals do not directly vote for them.
- Notes that electoral politics influence the makeup of the court and that future retirements could shift the court's dynamics.

### Quotes

- "But you can't separate what the Supreme Court does from electoral politics."
- "The Supreme Court, the makeup of the court, is a product of electoral politics."
- "The average retirement age for a Supreme Court justice is 80 I think."
- "You can't say that electoral politics has nothing to do with the Supreme Court."
- "It's worth remembering that the two eldest justices are members of the block that are making the decisions most people watching this channel really disagree with."

### Oneliner

Beau explains how electoral politics and the Supreme Court are intertwined, showcasing the potential impact of future retirements on court decisions and underscoring the influence of electoral processes.

### Audience

Voters, Activists, Political enthusiasts

### On-the-ground actions from transcript

- Stay informed about the ages and potential retirements of Supreme Court justices (implied).
- Engage in electoral politics to influence future appointments to the Supreme Court (implied).
- Support candidates who prioritize judicial appointments that resonate with your values (implied).

### Whats missing in summary

The full transcript provides detailed insights into the relationship between electoral politics and the Supreme Court, offering a nuanced perspective on how future retirements could shape the court's decisions and the importance of understanding this connection.

### Tags

#SupremeCourt #ElectoralPolitics #Retirement #Influence #Voting


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the Supreme Court
of the United States and electoral politics
and how they go together.
They're not separate from each other
because I got a message and I feel like
this might be something that is worth mentioning
at this point because there's a lot of,
There's a lot of people who feel like it's hopeless.
Okay, so here's the message.
I really feel for the person who doesn't like electoral politics in that message.
Not to be a doomer, but what does electoral politics matter at this point?
If the Supreme Court is just going to disregard the Constitution and legislate from the bench,
politics means nothing. Got you. I mean, I understand what you're saying. That's
not an invalid feeling right there. But you have to remember what one
of the products of electoral politics is, and that's picking Supreme Court
justices. It's worth remembering that the average retirement age for a Supreme
Court justice is 80 I think. I think it's 80 years old. Thomas, one of the further
right justices. Oh, he's 75. He's 75. And Alito, another right winger, is 73. They're
the two oldest they're probably going to be the next two to retire and given the
fact that Thomas is becoming more and more embattled I mean he might even go
before the average retirement age he might retire early would make sense
right it's what a lot of political figures do when they end up in a
position where they are being scrutinized. They decide it isn't worth it
and they go home and fish. 75, Thomas, I think he'll hit 80 before the end of the
next presidential term. So it's very possible that he retires during that
period, which means that president would, you know, have a little bit to do with
that. So would the Senate. Same thing with Alito. Less likely, but still within the
realm of possibility, because that's the average retirement age, meaning some of
them retire before then. I know for those people who are active with other forms
of civic engagement, electoral politics is horrible and I know that there's a
lot of philosophies where it's literally something you don't do because you're
being, you are giving consent to be governed. I get it. I get it. I understand
philosophies. But you can't separate what the Supreme Court does from electoral
politics. Understand the Supreme Court is chosen by electoral politics. Not
directly, you don't get to vote on them. But your vote, it has something to do
with who ends up on that bench. And if Thomas and Alito were to retire anytime
soon, then it would go to 5-4, right, the other way, which might help a
whole lot of people. A whole lot of people who, those who prioritize other
forms of civic engagement, probably care a whole lot about. I understand the
feeling because there have been some some rulings that I don't agree with
coming off the bench recently. I understand it, believe me, but you can't
separate the two. You can't say that electoral politics has nothing to do
with the Supreme Court. The Supreme Court, the makeup of the court, is a product of
electoral politics. And right now the two eldest justices are members of the block
that are making the decisions most people watching this channel really
disagree with. It's worth remembering that. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}