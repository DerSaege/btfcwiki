---
title: Let's talk about optimism and doom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kOBz0Mq7imo) |
| Published | 2023/07/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau received a message questioning his optimism after discussing the Supreme Court not being as right-wing as perceived.
- The message mentions recent rulings pushing the US closer to discriminatory practices against LGBTQ individuals and expanding Christian rights.
- The message also criticizes Beau's mindset, suggesting his optimism may be due to his identity as a white Cishet man compared to the writer, who is trans and living through difficult times.
- Beau clarifies that he considers himself a realist, not an optimist.
- He dislikes the doomer mindset that accepts a bleak reality without seeking change.
- Beau stresses the importance of not succumbing to doomerism, which he sees as surrendering to the status quo.
- He encourages adopting confidence to make a difference and change the outcome.
- Beau urges against expecting catastrophes constantly and advocates for long-term thinking when aiming for societal change.
- He points out the need to challenge the doomer philosophy that can lead to inaction or mere talk without action.
- Beau reminds the listener not to give up and to believe in their ability to effect change.


### Quotes

- "I'm not an optimist. I'm a realist."
- "Being a doomer is giving up. It's surrender."
- "If you want to change the world, I need you to adopt the absolutely unearned self-confidence of every middle-class white dude on Twitter."
- "Being a doomer to the point that the world and the U.S. should have you at is just surrender to the status quo."
- "Don't give up."

### Oneliner

Beau clarifies he's a realist, not an optimist, rejecting doomerism and urging confidence to make a difference in challenging the status quo.

### Audience

Activists, Change-makers

### On-the-ground actions from transcript

- Challenge the doomer mindset by actively seeking ways to make a difference (implied).
- Refuse to accept the status quo and believe in your ability to effect change (implied).

### Whats missing in summary

The emotional connection and depth of the speaker's perspective can be best understood by watching the full transcript.

### Tags

#Realist #Doomerism #Optimism #Change #Activism


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about being an optimist,
being positive, and being a doomer.
Because I got a message, and we're going to go through it,
and I hope that there are little bits in it that will
useful to people.
Okay.
Hey Bo, I think I finally realized why I like your stuff, but at the same time, really don't.
Correct me if I'm wrong, but you're a pretty big optimist, are you not?
I didn't have this idea until you did that video on the Supreme Court being maybe not
as right-wing as people thought, and to me that was such an absurd notion that I couldn't
figure out why you would even suggest it.
And I felt the need to send this email and ask this among one or two other questions
after the recent 6.3 rulings that among others push us closer and closer to being able to...
This says find, but I think it's supposed to be fine.
LGBTQ people for being that.
And removes yet more protections and yet again expands religious Christian rights.
overall seem maybe not positive, but not nearly as doomer as the state of the
world in the US should have you be. The Trump indictments don't really mean
anything, firmly in the too little too late category, and just opens the way for
a moderate, those-don't-exist Republican or someone as evil as DeSantis, to
easily win the presidency. And to top it all off, as long as SCOTUS is as it is,
elections don't matter. They are ruling from the bench. Tomorrow Biden could
somehow miraculously pass student debt relief through Congress and the Supreme
Court could still strike it down. They 100% would as well. Does this very
different mindset of yours have to do with you being a white Cishet man and me
being trans and currently living through the middle of a genocide? I don't know.
it's just a bunch of thoughts. Okay so we're gonna go through and hit the
questions in this. Correct me if I'm wrong but you're a pretty big optimist
are you not? No I'm definitely not. I'm not an optimist. I'm a realist. I'm a
realist. I am not an optimistic person and I try to look for the wins and try
to look for the things that can be changed. You're not the first person to
make that mistake. In fact, it's a running joke. This shirt was
actually part of a joke that somebody had given me, because I do. I'm
always moving forward and always trying to, you know, find a way to keep
things moving. It says I'm fine. What y'all don't see is that like the entire
half of the shirt on this side is a graphic of like a gaping wound. It's
not optimism, it's a refusal of sorts. And that gets to the next part. Oh, but
before we get into this, I do want to point something out because you're
you're not the first person to mention it.
Y'all need to go back and watch that video again.
Around the three minute mark, I make it super clear
that this is something that is being discussed
in think pieces about the Supreme Court
maybe not being that far right wing.
That's being discussed in think pieces.
There've been questions about it.
I'm not ready to say that yet
and that we have to wait for the decisions
that you're referencing here, just to be clear.
You overall seem maybe not positive,
but not nearly as doomer as the state of the world
and the US should have you be.
That's true, that's true.
I hate doomerism, despise it.
People who adopt that mindset,
become do-mers. What they often say is that it's just them, you know, accepting
the reality. I hate that because it assumes that it can't be changed. It's
It's accepting that the current situation, which is bad, if it wasn't bad it wouldn't
feel like doom, right?
Accepting that situation is how it always has to be.
It's a horrible mindset.
And it goes against history.
On a long enough timeline we win.
And that has a lot to do with my mindset.
I don't think in terms of months, I don't think in terms of the next election.
I think, I try to think longer than that.
And it's, if there was one thing I could get rid of from the left in general, it would
be the habit of always expecting a catastrophe.
And part of the problem is a lot of commentators who are do-mers themselves, they end up turning
winds into catastrophes because it's not everything that they wanted all at once.
Nothing is everything that you want all at once.
When you are talking about societal change, when you are talking about deep systemic change,
it doesn't occur at once.
It takes time.
And that doesn't help people who are in your situation because you are being actively targeted
by various governments, depending on the state that you live in.
So for you, yeah, I would imagine that it does feel pretty hopeless.
Does that have to do with me being a white CISHET man?
Are there varying viewpoints on that?
That probably has something to do with it.
At the same time, I can assure you that becoming a doomer leads to one of two things.
It leads to being so overwhelmed and so believing that nothing is going to change that you literally
can't act.
Or for some people, I don't think this is you, but for some people the Doomer philosophy
puts them in a position where they get to talk about it
but they're not expected to do anything.
Neither one of those is a
a good place to be.
Being a doomer
is giving up.
It's surrender.
It is a lot of what you're saying here.
It's not necessarily wrong, but it's taking the worst possible outcome from each event
And making that be the only outcome.
When you're talking about the Supreme Court, you have the thing of them ruling from the
bench and the elections don't matter.
The Supreme Court had the opportunity to make elections not matter not long ago.
In fact, that was actually the ruling that prompted all of the think-pieces and, at least
I think it was, but they decided to shoot down the independent state legislature theory.
I understand, had they not done that, we would be heading into very open, fasci territory
very, very quickly, probably in the next election.
I'm willing to bet that doesn't get celebrated as a win. That was a huge win.
That was a timeline-altering win. Probably not going to get a lot of play
though in the left. And certainly not among those who are doomer-pilled, you
You know, I would be willing to bet anything that had that decision going the other way,
they would never stop covering it.
You have a much harder world, you have a much harder world, and the thing is if everybody
was facing what you're facing. Just accepted it. It's going to occur no matter what. Look
at the examples. Then it would. It absolutely would. The only thing that stops it is people
who refuse to accept the doom.
If you want to change the world, I need you to adopt the absolutely unearned self-confidence
of every middle-class white dude on Twitter.
Is that self-confidence, that desire, that belief that you can actually make a difference,
that you can alter the outcome, is the only thing that's going to let you do it.
Being a doomer to the point that the world and the U.S. should have you at is just surrender
to the status quo.
And I don't like the status quo.
I don't think you do either.
you think you can or you think you can't. You're right. Don't give up. Anyway, it's
It's just a thought.
I hope you have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}