---
title: Let's talk about the AG of AZ, SCOTUS, and Andrew Jackson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=V6nEaNriePQ) |
| Published | 2023/07/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the attorney general in Arizona, the Supreme Court, and the spirit of Andrew Jackson, which is not typically desirable.
- Andrew Jackson is quoted as saying, "John Marshall has made his decision. Now let him enforce it," after disagreeing with a Supreme Court decision.
- The current attorney general in Arizona, Mays, a former Republican, seems to embody Jackson's statement by encouraging complaints of discrimination in public accommodations.
- Beau suggests that many viewers may not disagree with the attorney general's position on enforcing Arizona's public accommodation law.
- Beau doubts that businesses, especially creative ones, hold bigoted opinions to the extent of discriminating against individuals based on race, gender, or other factors.
- He believes that it is unlikely for this issue to escalate because he doesn't think many businesses share such extreme opinions.
- If future complaints arise, Beau predicts that they may end up back in the Supreme Court due to Attorney General Mays' stance on enforcement.
- The tone suggests that the Supreme Court lacks the ability to enforce decisions beyond bringing matters back to court, requiring broader federal government efforts for enforcement.
- Beau hints at deeper philosophical considerations regarding the rule of law and constitutionality but leaves them for another day.
- In closing, Beau shares his thoughts and wishes everyone a good day.

### Quotes

- "John Marshall has made his decision. Now let him enforce it."
- "If any Arizonan believes that they have been the victim of discrimination, they should file a complaint with my office."
- "It appears that Attorney General Mays has said the Supreme Court has made their decision, now let them enforce it."

### Oneliner

Beau talks about Attorney General Mays embodying Andrew Jackson's spirit against a Supreme Court decision on discrimination complaints in Arizona, doubting businesses' extreme opinions.

### Audience

Legal activists

### On-the-ground actions from transcript

- File complaints with the Attorney General's office in Arizona if you believe you have faced discrimination (suggested).
- Stay informed about developments regarding discrimination complaints and legal actions in Arizona (exemplified).

### Whats missing in summary

Deeper exploration of philosophical aspects related to the rule of law and constitutionality discussed by Beau. 

### Tags

#AttorneyGeneral #Arizona #SupremeCourt #Discrimination #Enforcement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the attorney general
out in Arizona, the Supreme Court and Andrew Jackson,
the spirit of Andrew Jackson,
which is not typically a spirit
that anybody wants to embody.
But in this case, I think most people
probably gonna let it slide. There is a quote that is said to come from Jackson
and that is John Marshall has made his decision. Now let him enforce it. It is
claimed that he said this after the Supreme Court made a decision that he
did not agree with. Now realistically I don't know that he ever said that but
But the attorney general out in Arizona certainly seems to be embodying that statement.
Her name is Mays, she's a former Republican.
And she said, if any Arizonan believes that they have been the victim of discrimination
on the basis of race, color, religion, sex, including sexual orientation or gender identity,
national origin or ancestry in a place of public accommodation, they should file a complaint
with my office.
I will continue to enforce Arizona's public accommodation law to its fullest extent."
So basically that whole website case, it appears that the attorney general in Arizona is less
than enthusiastic about accepting the Supreme Court's interpretation of the
law there, which I don't think there's going to be a whole lot of people
watching this channel that that disagree with her position. Now what happens
next depends on how any future complaints shape up. Now realistically I
I don't think this is going to become an issue because I don't actually believe that...
I don't believe there are a whole lot of businesses, particularly those that are creative in nature,
that are bigoted to this degree.
I really don't believe that it's going to...
I don't think there's a lot of businesses out there that hold these opinions.
So to me, it seems unlikely that this is ever going to actually come up, however, if it
does, rest assured, it will be going straight back to the Supreme Court if it happens in
Arizona, because it certainly appears that Attorney General Mays has said the Supreme
Court has made their decision, now let them enforce it.
The general tone there is that they can't, that they lack the actual ability to enforce
it beyond bringing everything back to court.
And it would take a wider effort within the federal government to actually really compel
a state to to follow the determinations of the Supreme Court. There's a whole
lot of philosophical aspects to this when it comes to rule of law, the
constitutionality, all of that stuff. But I think those can be saved for another
day. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}