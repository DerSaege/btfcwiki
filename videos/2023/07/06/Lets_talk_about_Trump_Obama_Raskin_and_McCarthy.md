---
title: Let's talk about Trump, Obama, Raskin, and McCarthy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=20lmL8YNx_w) |
| Published | 2023/07/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump posted what was said to be the address of former President Obama, leading to a man allegedly attempting to go after the family with weapons in his vehicle.
- The suspect, living in his van, was already being sought by the feds for activities on January 6th and had made ominous social media posts.
- Threats were made towards Representative Raskin and McCarthy, with the suspect entering an elementary school near Raskin's home.
- The danger posed by those more energized and influenced is not limited to Democrats, and there's a lot of anger constantly being fed within certain segments of the Republican base.
- The Republican Party needs to lead and not condone statements from radical members, as it could directly lead to tragedy.
- The severity of the situation demands more media coverage, as it's not just a random incident but something with serious implications.
- The suspect seemed like someone who had already decided to do something and was just looking for a reason, pointing towards the dangerous consequences of inflammatory rhetoric.

### Quotes

- "The danger posed by those more energized and influenced is not limited to Democrats."
- "The Republican Party needs to lead and not condone statements from radical members."
- "The suspect seemed like someone who had already decided to do something and was just looking for a reason."

### Oneliner

Former President Trump's actions led to a dangerous situation, revealing the consequences of inflammatory rhetoric and the need for the Republican Party to take a stand against radical elements.

### Audience

Media consumers, political activists

### On-the-ground actions from transcript

- Contact media outlets to demand more coverage of this serious incident (suggested)
- Start or join community dialogues on the impact of inflammatory rhetoric and political extremism (implied)

### Whats missing in summary

Insights on the potential escalation of the situation as more details emerge and the importance of monitoring it closely.

### Tags

#Trump #Obama #Raskin #McCarthy #RepublicanParty #InflammatoryRhetoric #MediaCoverage


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about some events in DC
and a surprising, but not totally surprising,
connection between Trump, Obama, Raskin, and McCarthy.
And we'll go over the events as we know them
in relation to a story
that has kind of been under-reported given the severity of it.
Okay, so former President Trump posted on social media what was said to be the address
of former President Obama.
A man reposted it, shared it, and then is alleged to have gone to that neighborhood.
He had a couple of weapons in his vehicle, according to the feds, and it appears that he
intended on going through the sewers like a ninja turtle to go after the family.
Okay, so Secret Service caught him after a foot chase I believe. Now the suspect
himself been living in his van. The feds were already looking for him in relation
to activities on January 6th. Over the last week there had been a number of
social media posts that well let's just call them ominous from from the suspect
at least according to the documents filed by the feds. Okay so he certainly
appeared to threaten the National Institute of Standards and Technology.
I have no idea why. There's probably some theory about that that I'm not aware of
of, but rest assured I'll be looking into it. Representative Raskin also got
threats. It appears that during that period the suspect entered an elementary
school near Raskin's home. He also threatened McCarthy, so what
But what can we take away from this?
I mean, obviously the severity of the issue is certainly there.
The allegations are incredibly serious.
There were some statements about his van being self-driving, so he wouldn't be there when
it went off.
Whole bunch of really bad stuff.
But that name McCarthy, one of these is not like the others, right?
Frankenstein has completely lost control of his monster.
The Republican Party absolutely does not have any control over that base.
The danger that is posed by those that are more energized, more heavily influenced, it's
not limited to Democrats.
That seems pretty clear.
Looking at the allegations, it certainly appears like basically anytime in the last week or
so something could have set him off and a tragedy ensued.
There's a lot of anger there and that anger is constantly being fed.
The appetite that has been created in a substantial portion of the Republican base is something
that it's going to be really hard to make them lose their appetite, and the Republican
party has to actually lead.
They can't continue to condone the statements of the more radical members of their party
it is going to directly lead to tragedy. I would imagine that this story is going
to get more and more press as more details come out. Given the severity of
it I feel like a lot of the media was just like oh you know it's one of those
things where this, you know, random person, you know, tried to get into a former
president, happens all the time, not a big deal. Based on the information that was
presented by the feds, it's not a situation like that. That's a little bit
more serious and it probably needs coverage a lot more than it's getting.
and given the wide, very wide, selection of people or entities the person was
angry with, it really seemed like somebody who had decided they were going
to do something and was just looking for a reason, the people who started
this up, the people who created this appetite, they're not, they are not the
people that the Energized Base believes they are. It was just words. It's not going to
be them that goes down for it. To them it was all about tricking people to get a
vote, nothing more. But that rhetoric, the inflammatory nature of it, it can cause
problems. So I would expect to see more about this story as it develops because
I have a feeling it's gonna get weirder. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}