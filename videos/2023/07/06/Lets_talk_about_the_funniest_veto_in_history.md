---
title: Let's talk about the funniest veto in history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kTouNX6_BvM) |
| Published | 2023/07/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor Tony Evers used his super veto power in Wisconsin in a unique and humorous way.
- He slashed a GOP tax cut for the rich from 3.5 billion down to 175 million.
- The wealthy in Wisconsin are not getting the tax cuts that other people are receiving.
- Evers vetoed the cuts for the highest tax brackets, showcasing an interesting use of power.
- He creatively manipulated a part of the legislation that allocated extra revenue for schools.
- By vetoing specific characters in the legislation, Evers extended the funding until the year 2425.
- Some may view Evers' actions as an abuse of executive power, but Beau finds it impressive.
- Republicans in Wisconsin are likely unhappy with Evers' innovative use of the veto.
- Previous rulings have gone against similar veto maneuvers, but Evers' creativity stands out.
- With a new liberal majority in the Supreme Court, there may be a chance for Evers' actions to hold despite expected legal challenges.

### Quotes
- "400 years of increased revenue."
- "I'm not even mad. That's impressive."
- "I'm sure that some would suggest that that is an abuse of executive power."
- "Definitely some interesting news coming out of Wisconsin."
- "It's just a thought."

### Oneliner
Governor Tony Evers creatively manipulates legislation in Wisconsin, cutting tax breaks for the rich and extending school funding to the year 2425, sparking legal battles and Republican discontent.

### Audience
Wisconsin residents

### On-the-ground actions from transcript
- Support legal battles against any abuse of power (implied)

### Whats missing in summary
The full transcript provides detailed insights into Governor Tony Evers' unique use of veto power and the potential legal ramifications, offering a deeper understanding of the political landscape in Wisconsin.


## Transcript
Well, howdy there, internet people. Let's bow again. So today we are going to talk about Wisconsin,
the governor, and the super veto that the governor in Wisconsin has, and how it was recently used
in perhaps one of the funniest ways imaginable. Up there, the governor has the ability to strike
just certain pieces from legislation and move forward with it and sign it.
So Governor Tony Evers up there, he got some legislation. It included a GOP tax cut for the
rich of course. First, he used his super veto to slash the tax cut itself, the total amount. It
It went from like 3.5 billion down to 175 million,
and he vetoed the cuts for like the highest tax brackets.
So the wealthy are not getting the tax cuts
that other people are.
That in and of itself is humorous.
That's a unique use of that power.
The other thing he did was far more imaginative. There was a part of it that included, I want to say it was $325 per
student per year in school's ability to get extra revenue, you know, in school funding.
And it was, according to the legislation, it was set to be used for 2023 to 2024 and 2024 to 2025.
Okay, so he slashed out the 2023 to 2024 and then in the 2024 to 2025 part,
which was written 2024-25, he vetoed the 20 and the hyphen, creating this additional funding
until the year 2425. 400 years of increased revenue.
I'm sure that some would suggest that that is an abuse of executive power.
I gotta be honest, I'm not even mad. That's impressive.
That is fantastic. Obviously, this is going to end up going to court.
I am certain Republicans in Wisconsin are not happy about the governor becoming
Vanna White up there and just, you know, using certain letters. Now the
interesting part about that is that in the past there have been rulings against
this type of use of veto, even though I cannot imagine one being this imaginative.
But in August, the Supreme Court up there, it shifts and it becomes a liberal
majority. So there's a chance that this actually holds, but make no mistake about
it there's going to be a pretty lengthy legal fight over this I'm sure, but
definitely some interesting news coming out of Wisconsin. Anyway, it's just a
thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}