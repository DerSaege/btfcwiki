---
title: Let's talk about guarding against confirmation bias....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uaRcNmnf9aE) |
| Published | 2023/07/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about confirmation bias and how it can lead people into information silos.
- Shares a personal experience related to confirmation bias and hiring practices.
- Mentions how confirmation bias can make people believe objectively false things.
- Explains how the belief system about gender norms can impact hiring practices.
- Mentions a report showing that people with they/them pronouns on their resumes were 8% less likely to get a call back.
- Raises questions about whether this bias is specific to non-binary people or just about listing pronouns.
- Urges deeper investigation beyond headlines to avoid falling for incomplete or misleading information.
- Suggests that putting pronouns on resumes may induce fear in hiring managers leading to potential bias.
- Advises against completely omitting pronouns from resumes but acknowledges the slight impact it may have on call-back rates.
- Encourages putting pronouns on resumes even for those who fit neatly into traditional gender norms to reduce fear and unknown factors in hiring decisions.

### Quotes

- "Everybody wants to confirm what they already believe."
- "It confirmed what I believe, but that's not really what the information shows."
- "The key element to avoiding confirmation bias and falling for bad information is to basically assume it's always going to be wrong and continue to look further into it."
- "Read the source material; it's a very good guard against falling for and consuming bad information."
- "It just links it to the presence of pronouns."

### Oneliner

Beau explains confirmation bias in hiring practices, urging deeper investigation beyond headlines to avoid falling for incomplete information, and encourages putting pronouns on resumes to reduce unknown factors in hiring decisions.

### Audience

Job seekers, HR professionals.

### On-the-ground actions from transcript

- Read source material (guard against bad information) (suggested).
- Put pronouns on resumes (reduce unknown factors in hiring decisions) (suggested).

### Whats missing in summary

The full transcript provides a detailed exploration of confirmation bias in hiring practices and the importance of thorough investigation beyond headlines to avoid incomplete or misleading information.

### Tags

#ConfirmationBias #HiringPractices #Pronouns #Inclusivity #GenderNorms


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about a report
and hiring practices and confirmation bias
and how to avoid it.
And I'm going to take you through
something that happened to me.
Um, confirmation bias is something
that everybody experiences.
Everybody wants to confirm what they already believe.
There are no real exceptions to that.
what you believe to be right and good and true,
you want that proven to be right and good and true.
So when you see something that suggests
what you already believe is accurate,
most times you look a little bit into it.
That process of confirmation bias is something
that can lead people into information silos.
It can lead people to believe things
that are objectively false.
So, I'm gonna take you through something that happened
and you can see how to guard yourself
against falling too far for confirmation bias.
On this channel, a number of times,
I have talked about how I believe that a lot of the bias
that the LGBTQ community experiences
stems from people whose life experience included two boxes, right? It included two boxes. Boys do
this, girls do this. Anything outside of that is a deviation and, you know, it makes them
uncomfortable because it's unknown and the unknown causes fear. The fear can be played on and create
bigotry. It's something that I believe. I see a headline that says non-binary
people experience discrimination when it comes to their resumes and then other
headlines were like those who use they them pronouns are less likely to get a
call back. So this is something that's particularly interesting to me because
part of that belief also stems to the way people's morality interacts with it.
When it comes to non-binary people, there's not a massive, oh that's a
moral thing going on. It's just different. It's something that a lot of people
didn't experience. At least not in the way, not in the way it is recognized
today. You know, it wasn't, it wasn't really classified as an identity. It was,
oh, well she's just a tomboy type of thing. And it didn't go beyond that.
Anything that stepped too far outside of that, well then, again, it becomes a
deviation, people viewed it as strange. So studies and information about non-
binary people, it's something that I look into a little bit more because it's, it
doesn't have that moral component. It's just outside of these people's life
experience. Okay, so basically what the report shows is that when people had
they them on their resume as their pronouns.
They were 8% less likely to get a call back.
That tracks with what I believe.
That's how I expect this bias to play out, right?
But as I start looking into it, what I find is that they didn't test it with
the pronouns of he, him, or she, her. It was just they, them, or no pronouns
listed. So does this report actually show that non-binary people experience
this, specifically non-binary people, or does what it really show is that hiring
managers are less likely to call people back if they have their pronouns listed.
There was no test about he, him, she, her.
Those weren't included in the resumes that were sent out.
It was just they, them, or no pronouns.
You really can't, based on this study alone, you really can't, you can't make that call.
can't say that this is something that is directed at non-binary people. It could
just be directed at people with pronouns. The information isn't there. It confirms
what I believe, but that's not really what the information shows. Now in a
weird way it still kind of confirms what I believe, but to a much lesser degree.
Hiring managers tend to be older. The presence of pronouns that's relatively
new, something outside of the norm, unknown, induces fear. And what would the
fear be? Oh, if I say something wrong, this person will sue us. That type of
stuff. So it's still there, but it doesn't actually show what I thought it was
going to when I first started looking into it. And why did I first start
looking into it? Why did the headline grab my attention? Because it confirmed
what I already believed. So I started looking into it. If you just read the
article, you don't dig deeper, or you just read the headline, you're gonna walk
away believing that this is something specific to non-binary people. That's not
really what it shows. Now you could combine this study with other studies
and I'm pretty sure that's what it would show, but that's not what's
printed. That's not what's in the discussions that are out there right now.
So I mean this obviously leads to the questions. Does that mean you
shouldn't put your pronouns on there because you're 8% less likely to get a
call back? No. I mean, if you're applying for your dream job and you're trying to
get every possible edge, yeah, maybe you forget to include them on that one. But
generally speaking, you're talking about 8%. It's big enough to notice, it's
definitely an issue, but it's not massive. And it's also worth remembering that you
probably don't want to work for them anyway. So unless it's a job that is
incredibly important to you, I would still put them on there. And if you are
somebody who lines up neatly, you might want to start putting your pronouns on
your resume. Even if you are 100% in the box of, this is how it's always been, I
don't need my pronouns. If you start putting them on there, this unknown goes
away, which means the fear goes away. So I hope that helps. Like as I'm explaining
it, it seems a little bit more convoluted than it was in my head when I started
the video. But the key element to avoiding confirmation bias and falling
for bad information is to basically assume it's always going to be wrong and continue
to check, look further into it than just a headline or just the first few paragraphs.
Because you may find something that kind of renders the take of the author of the article
or whatever as wrong, or in this case, not really wrong, but just not complete.
Because without testing he, him, and she, her, it doesn't actually link it to non-binary
people.
It just links it to the presence of pronouns.
Okay, so I hope that helps.
That same practice of just going deeper into it, start with the headline, read the article,
read the source material it it's a very good guard against falling for and
consuming bad information.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}