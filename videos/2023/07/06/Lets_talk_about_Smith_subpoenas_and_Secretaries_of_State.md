---
title: Let's talk about Smith, subpoenas, and Secretaries of State....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=A3YiuNXy4zc) |
| Published | 2023/07/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Arizona is in the spotlight again due to new information emerging.
- Similar to Georgia, there was a phone call in Arizona from Team Trump applying pressure regarding the 2020 election outcome.
- People wondered if this new information might prompt the Department of Justice to intervene.
- Recently, it was reported that the special counsel's office subpoenaed Arizona's Secretary of State's office in May and spoke with lawmakers.
- It's suggested that the Arizona officials may have already talked to federal authorities before this information became public.
- The order in which news breaks may not be the order of events that occurred.
- The subpoenas received in May seem related to various issues, possibly including lawsuits post the 2020 election.
- The scope of the investigation into election interference and January 6 seems broader than what is currently known.
- There are significant developments happening behind the scenes that are not making headlines.
- The surfacing of information in the media may indicate that the authorities already possess the information before it becomes public.

### Quotes

- "That's a wild way to run a witch hunt, I guess."
- "When little elements start to surface in the media, the real reason may be that Smith's office already has the information."
- "A lot is happening that isn't making the news."
- "The order in which news breaks is not necessarily the order in which it occurred."
- "Maybe had it for a month or more before we ever find out about it."

### Oneliner

Arizona faces scrutiny for election interference as new information surfaces, indicating federal involvement and a broader investigation beyond public knowledge.

### Audience

Concerned citizens, activists

### On-the-ground actions from transcript
- Contact local representatives to demand transparency and accountability (implied)
- Stay informed on developments in the Arizona election interference investigation (implied)
- Support efforts to uphold election integrity (implied)

### Whats missing in summary

Insights on the implications of potential election interference and the importance of transparency and accountability in electoral processes.

### Tags

#Arizona #ElectionInterference #FederalInvolvement #Transparency #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Arizona again
because there is some more information coming out of there.
A lot of eyes have been on Arizona
because recently information surfaced
suggesting there was a phone call,
very similar to the phone call in Georgia
from Team Trump trying to apply pressure
regards the 2020 election, trying to alter the outcome, to find votes, so on and so forth.
When that story broke there were a whole bunch of people asking if, you know, this was going to
prompt the Department of Justice to get involved and I said the real question is why do people
feel comfortable talking about it all of a sudden after all of this time and suggested they may have
already talked to the feds. Reporting out of Arizona today says that as recently as
May, remember that phrasing, the special counsel's office had subpoenaed the
Secretary of State's office in Arizona and they talked to lawmakers. They've
already talked to the feds. Smith already has the information. Again, it is important
to remember that the order in which news breaks is not necessarily the order in
which it occurred and that certainly seems to be the the case here. Now, that
phrasing, as recently as May, that would seem to indicate that the Arizona
Secretary of State's office received subpoenas about more than one thing. We
know from reporting that at least part of what the special counsel's office is
looking into deals with some lawsuits that were filed in the aftermath of the
2020 election. It does seem like the election interference slash January 6
probe that it is far more expansive than we currently know. There's
a lot there is a lot happening with that that isn't making the news. So again, they
this subpoena in these conversations occurred in May. We got a little hint
about it a few days ago and now there's confirmation of it. So remember when
When little elements start to surface in the media, that the real reason they may start
to surface and become public is because Smith's office already has the information.
Maybe had it for a month or more before we ever find out about it.
That's a wild way to run a witch hunt, I guess.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}