---
title: Let's talk about Michigan electors and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=h3s9i0PuWO0) |
| Published | 2023/07/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Michigan's Attorney General has charged 16 individuals as "fake electors," marking the first time such charges have been brought anywhere.
- Charges include felonies like conspiracy to commit forgery, forgery, and election law forgery.
- The accused could face a maximum of 14 years, but actual sentencing outcomes are uncertain.
- Expectations for indictments in Georgia and DC are building up, with signs of investigations in other states as well.
- The possibility of more charges and defendants emerging is high due to ongoing investigations across different locations.
- Cooperation with government entities may be a strategy for reducing potential sentences.
- Anticipates a period of consistent news and developments that many have been waiting for.

### Quotes

- "Charges include felonies like conspiracy to commit forgery, forgery, and election law forgery."
- "Cooperation with government entities may be a strategy for reducing potential sentences."
- "I think it's finally going to start kind of snowballing."

### Oneliner

Michigan charges "fake electors," expecting more developments in Georgia and DC, with potential cooperation to reduce sentences.

### Audience

Legal watchers, political analysts.

### On-the-ground actions from transcript

- Contact local authorities to stay informed about developments in similar cases (suggested).
- Support efforts to uphold election integrity through community engagement (exemplified).

### Whats missing in summary

Detailed explanations on the specific charges faced by individuals and the potential implications of cooperation on sentencing could be further explored in the full transcript.

### Tags

#Michigan #Electors #Charges #Cooperation #Developments


## Transcript
Well, howdy there Internet people, it's Beau again.
So today we are going to talk about Michigan
and developments and electors
and how it appears that there are a whole bunch of developments occurring at
one time
and how they may intersect later.
And we'll go through what we know and what we don't because there's a lot of
information that has come out
but there is also a lot of information that is still kind of absent
least at time of filming and we'll try to fill in some of the gaps. Okay, if you
miss the news, the Attorney General in Michigan has announced charges against
16 of the people who were, quote, fake electors. It's worth noting this is the
first time people who filled that role have been charged anywhere. The
The announcement said they were charged with, quote, several felonies.
We'll try to gauge what that means here in a minute.
The attorney general said that it was part of a desperate, quote, attempt to undermine
democracy, quote.
not seem to rule out the possibility of additional charges or additional
defendants, so there still may be more coming in this. Now, what are the charges?
At time of filming, we don't have a complete list for everybody. However, I
was able to find a list for Kent Vanderwood who is charged with conspiracy
to commit forgery, two counts of forgery, conspiracy to commit uttering and
publishing, uttering and publishing, conspiracy to commit election law
forgery and two counts of election law forgery. I mean those are pretty serious
charges. Now there is a statement saying they are facing a maximum of 14 years. A
whole bunch of questions have come in about that. The answer is I don't know. I
am not familiar with Michigan's sentencing habits the way I am the
federal government, I have no idea. What I will say is that, generally speaking, the
odds are that this will be these people's first offense. And normally on a first offense,
you get cut a break. So it is unlikely, just as general, in a general habit across the
United States for it would be unlikely that their sentences would hit that
maximum. That normally doesn't happen, but I don't know their laws up there the
way I do other places. Okay, so we now have charges in Michigan. We are
expecting an indictment in Georgia. There is an expectation of one in DC. It is
worth remembering that the fake elector thing, that's part of Georgia as well, but
we don't know if that's rolled into what's going on currently. We've talked
about the fact that there have been signs that other states are looking into
this scheme. So there may be more coming in different locations with different
electors. These were the first charged. I don't anticipate them being the last.
There will probably be more. As all of this starts to happen around the same
time, because the investigations were opened around the same time, there will
be a, there will be a convergence and there will be a whole lot of people who
are interested in taking whatever sentence they're about to get and
reducing it. One way they may do that is by cooperating with, let's say, other
government entities when talking about people that may be facing federal
charges. And we'll have to wait and see how it plays out, but I have a feeling
we're in for a couple of months of consistent news that everybody's been
waiting for, saying it isn't coming. I think it's finally going to
start kind of snowballing. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}