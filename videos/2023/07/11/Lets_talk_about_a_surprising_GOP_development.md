---
title: Let's talk about a surprising GOP development....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4MwSFOUNx4s) |
| Published | 2023/07/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A man has been charged with a list of serious crimes, including conspiracy and making false statements, relating to various Acts.
- The person charged agreed to recruit and pay a former high-ranking U.S. government official on behalf of principals in China to influence policies with respect to China.
- The allegations suggest that they caught an agent of influence who had connections with the president-elect in 2016, Trump.
- This person, who is the same as the Republican's lost whistleblower, allegedly had contacts within the Republican Party and was willing to exploit them.
- The twist is that the charged individual is believed to be the same person who presented evidence about Biden at a U.S. embassy in Brussels in 2019.
- Despite attempts to use this person as a whistleblower, the gravity of the charges suggests that many within the Republican Party may have been unaware of these events.
- It is likely that the story will continue to surface, and the extent of the Republican Party's knowledge about these events will be a topic of debate.

### Quotes

- "This person is, y'all, you know, y'all, y'all, y'all, y'all, y'all, y'all, you know, you know, you know."
- "How much the Republican Party knew about these events is going to be a matter of debate."
- "Rest assured this story is not going away."
- "This isn't going to be a short-lived scandal."
- "Y'all have a good day."

### Oneliner

A twist in a story reveals serious charges against a person linked to influencing policies and political events, sparking debate within the Republican Party.

### Audience

Political observers, Republican Party members.

### On-the-ground actions from transcript

- Stay informed and follow updates on this unfolding story (implied).
- Engage in respectful and constructive debates about political events and allegations (implied).

### Whats missing in summary

Insight into the potential implications of this unfolding story and its impact on political circles and public perception.

### Tags

#PoliticalEvents #RepublicanParty #Charges #Influence #Debate


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
a winding chain of events
and how it ties together with something else.
And we have a story that definitely has a major twist
at the end.
If you already heard what happened,
don't ruin it for everybody else.
Okay, so a man has been charged with a list of crimes.
They include conspiracy to violate the Foreign Agents Registration Act, conspiracy to violate
the Arms Export Control Act, violation of the Arms Export Control Act relating to Libya,
of the Arms Export Control Act relating to the United Arab Emirates, violation of the
Arms Export Control Act relating to Kenya, making false statements, conspiracy to violate
the International Emergency Economic Powers Act, and another count of making false statements.
These are serious charges.
In the press release that was put out about this, it says that the person agreed to covertly
recruit and pay on behalf of principals based in China, a former high-ranking U.S. government
official, individual one, including in 2016 while the former official was an advisor to the then
president-elect to publicly support certain policies with respect to China.
So it appears based on the allegations, and remember these are allegations,
it appears that they have caught an agent of influence and this agent of
influence had somebody in the orbit of the president-elect in 2016, that's
Trump, as somebody they were hoping to recruit.
So, this person obviously felt they had contacts within the Republican
party going back to 2016, and according to the allegations, was
willing to exploit those contacts.
Here's the twist. This person is, y'all remember the, quote, whistleblower that the Republicans
lost? Yep. This is apparently them. This is apparently that person.
Okay, so according to one of the stories, this individual presented evidence about
Biden at a U S embassy, I want to say in Brussels in 2019, and that is supposed to
be vindication of everything, but according to what the feds are alleging by
that point they were already by allegations, they were already kind of in
the habit of well trading favors. So this story, you are going to see this again,
this is not going to go away. How much the Republican Party knew about these
events is going to be a matter of debate. I would imagine that a whole lot of
people within the Republican Party were completely unaware of this. When you're
talking about the people in Congress, the people who are currently trying to use
this person as a whistleblower, my guess is that many of them did not know. I am
not in the habit of defending the Republican Party, but in this case, given the gravity
of some of the charges here, I'm of the belief that the number of people that were
aware of any of this behavior, any of this alleged behavior, it is probably
pretty small. But rest assured this story is not going away. You will see it again.
It will continue to pop up. This isn't going to be a short-lived scandal. Anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}