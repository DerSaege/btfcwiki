---
title: Let's talk about Trump's requested delay in the documents case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-sYPTtbFaSM) |
| Published | 2023/07/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Team Trump requested an indefinite delay in the documents case to push it past the 2024 election, aiming to use the presidency as a shield against legal entanglements.
- The filing is more than just a delay tactic; it's a test of the judge who has previously ruled in favor of Trump, potentially signaling judicial bias.
- The Department of Justice opposes the delay, viewing it as an attempt to circumvent legal accountability.
- The strategy of delay may not work in all venues, and Trump's confidence in winning could impact the decision to prolong the trial.
- Trump's legal team seems to be testing the judge's boundaries, possibly to gauge her stance and exploit any leniency.

### Quotes

- "There is a citation of a case, USV Hapful, that is an unpublished opinion out of the Eleventh Circuit."
- "The goal here is to create a situation in which Trump can use the presidency as a solution to his legal entanglements."
- "The Department of Justice is opposed to the idea of delaying it until whenever as far as they're concerned they're late they're late they're late for an imporant date."
- "I really see this more as Trump's legal team testing the judge."
- "It may signal to the judge that Team Trump is going to try to exploit her leniency and put her in a position she doesn't want to be in."

### Oneliner

Team Trump's request for a delay in the documents case serves as a strategy to shield Trump from legal consequences and tests the judge's impartiality.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Monitor the developments in the documents case and the judge's ruling (implied).
- Stay informed about legal proceedings and potential implications for the justice system (implied).

### Whats missing in summary

Insights on the potential repercussions of judicial decisions and the importance of upholding legal accountability.

### Tags

#Trump #LegalCase #DelayTactics #JudicialIndependence #JusticeSystem


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump
in the documents case and the requested delay
from Team Trump, what it means and what it's really about.
Because while it is about creating a delay,
there's something else hiding underneath the surface.
OK, so if you don't know what happened,
Team Trump has filed basically asking the judge in the documents case to not set a date
for trial, to delay it indefinitely basically.
The goal is to push it past the 2024 election.
Now, that kind of delay would be extreme, and the real tone of this is, you know, your
honor, I'm kind of too busy to be prosecuted.
There is a citation of a case, USV Hapful, that is an unpublished opinion out of the
Eleventh Circuit.
I'm not a lawyer, but my understanding is that that basically means it's kind of irrelevant.
It is not something that could be used for precedent, by the way I understand it.
So the goal here is to create a situation in which Trump can use the presidency as a
solution to his legal entanglements, delay it for an incredibly long amount of time,
Or if he wins the election, well then he can just self-pardon, is the way this is being
viewed.
And yeah, I mean that's definitely probably the long-term strategy, but this particular
filing is more than that.
It's a test of the judge.
The judge has made numerous rulings in favor of Trump at various points, rulings that
were, let's just say called into question later.
This is a moment where the judge could signal where she's at.
If she was to grant an indefinite delay, that would be a pretty clear signal to Team Trump
that she's kind of on their team.
willing to willing to lean a little bit more their direction in the current
proceedings. Obviously the Department of Justice is opposed to the idea of
delaying it until whenever as far as they're concerned they're late they're
late they're late for an important date. One of the other things to keep in mind
mind is that this is one case in one venue, there are multiple venues being
pursued, they're not all gonna have the same judge. This strategy of delay is not
is not going to work everywhere. The other thing is that I'm not sure it
makes sense. If Trump was confident and his team truly believed that these
charges were the result of a witch hunt and that he would totally be
exonerated, it seems like they would want to get this to trial
as soon as possible because they'd win. And once you win in one case, it creates
additional doubt in the others, right? This strategy, to me, I would read this
as his team doesn't think they can win, so they're trying to delay or get him
into office so he can remedy the situation himself, either through a
self-pardon or pressuring DOJ using less than legitimate means to alter the
outcome. We will have to wait and see what the judge decides, but this is beyond
the delay. I really see this more as Trump's legal team testing the judge. Is
the judge going to be willing to do something that despite the legal
arguments, setting those aside, is something that is so outside the norm
that when I looked I couldn't find anything like this ever happening that
would put her under a whole bunch of scrutiny because if she was to side with
them believe me the media would be all over this you might even have a
situation in which it goes to an appeal it might be a situation in which the
prosecution is just like you have the judge the judge may have a bias there's
a whole lot of there's a whole lot of outcomes from this for the judge and it
may be team Trump trying to test her and see where she's at on this stuff
because they don't know I know that the media and most of the public has a very
clear picture of what they think that judge would do we're gonna have to wait
I don't know that this is going to go that way. It might, but this is pretty
outside the norm and it may be something that signals to the judge that it may
induce the opposite reaction because it may signal to the judge that Team Trump
is going to try to exploit her leniency and put her in a position she doesn't
want to be in. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}