---
title: Let's talk about the Voting Rights Act ruling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=df8stTcAdrg) |
| Published | 2023/11/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent decision regarding section two of the Voting Rights Act by a panel of judges in the Federal Pills Court, limiting the right of citizens and organizations to bring cases under Section 2, leaving it primarily to the Department of Justice (DOJ).
- Notes that this decision contradicts decades of precedent and is expected to be challenged in the Supreme Court, although the outcome is uncertain due to the current makeup of the court.
- Outlines the potential fixes if the Supreme Court upholds the decision, including a legislative fix that seems difficult in the current political climate, or a scenario where DOJ becomes more involved in voting rights cases.
- Warns that the states currently affected by this decision are Arkansas, Iowa, Minnesota, Missouri, Nebraska, North Dakota, and South Dakota, where Section 2 only applies to the attorney general.
- Emphasizes the importance of being prepared for different outcomes and the potential long-term implications of this decision on voting rights and federal interference.

### Quotes

- "Getting Republicans to support voting rights right now is about like asking Russia to provide military aid to Ukraine, it's not going to happen."
- "The Voting Rights Act is kind of one of those. It protects the small amount of voice that Americans have."
- "You need to get one of your betters to bring that case for you."

### Oneliner

A panel decision limits citizen rights under the Voting Rights Act, facing potential Supreme Court challenge and uncertain fixes in a politically charged climate.

### Audience

Advocates, voters, activists

### On-the-ground actions from transcript

- Advocate for voting rights in your community (suggested)
- Stay informed about developments in voting rights legislation (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the implications of the recent decision on the Voting Rights Act and the potential challenges ahead.

### Tags

#VotingRightsAct #SupremeCourt #DOJ #PoliticalClimate #VoterAdvocacy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the Voting Rights Act.
We are going to talk about the decision that was reached,
what happens as it moves forward,
what the expected developments are,
what this decision changes,
and what the fixes are for it if it moves forward
and the experts aren't right on what happens.
happens. Okay, so if you missed the news, a decision was reached regarding section
two of the Voting Rights Act. A panel of judges in the Federal Pills Court
decided that there is no private right of action in section two of the Voting
Rights Act. What does that mean? It means that basically only DOJ can bring a
case under Section 2. Historically, those cases are brought by citizens, they're brought
by individuals, or they are brought by groups of citizens or organizations. It's normally
not DOJ. For those who are familiar with this, yes, this decision does not exactly align
with decades of precedent. Now, most experts will tell you that this is headed to the Supreme
court and the Supreme Court is going to overturn this decision.
Under normal circumstances, I would agree.
If we had a normal Supreme Court, I would like to remind everybody that we don't have
a normal Supreme Court, and I would not stand here today and say, yeah, that's what's going
to happen, because it may not.
So we have to figure out what the fixes are if the Supreme Court goes the other way and
lets this stand.
There are two real options.
The first is the legislative fix.
In a normal political climate, it would probably be pretty easy to get Congress to say, okay,
well let's just amend this poof, there's a private right of action here now.
Everything goes back to the way it was.
That's in a normal political climate.
Getting Republicans to support voting rights right now is about like asking Russia to provide
military aid to Ukraine, it's not going to happen.
So that seems unlikely.
The other option for a fix is one of those moments where the people who brought this
about, well, be careful what you wish for.
Because if DOJ is the only entity that can bring a case like this forward, they have
an obligation to do it, which means there needs to be an office, which means the Department
of Justice will become even more involved when it comes to, I don't know, redistricting
and stuff like that. Those people who wanted less interference and wanted to be able to
kind of do whatever they wanted, you know, when it comes to voting rights, basically
the politicians wanted to pick their voters rather than the voters pick the politicians.
They should probably get ready for a lot more federal interference because that's probably
how this would play out in the long term.
Now regardless of what happens in the future, currently this stands in Arkansas, Iowa, Minnesota,
Missouri, Nebraska, North Dakota, and South Dakota.
If you are in that area, section two of the Voting Rights Act is basically only for the
attorney general.
That's kind of how it works.
There is a decent chance that the Supreme Court overturns this, but we don't know that.
So we have to be ready for the alternative, which is the Supreme Court upholds it.
And from there, there has to be a fix.
There isn't a lot of legislation like this that you look at and you're like, hey, this
is objectively good.
The Voting Rights Act is kind of one of those.
It protects the small amount of voice that Americans have, and they've gone out of their
way to take a section of it and say, well, I mean, yeah, it applies to you, but you can't
actually do anything with it.
You need to get one of your betters to bring that case for you.
It's more of the same typical stuff.
This will probably turn into a big thing.
This will be something that gets talked about.
It'll become maybe a topic of legislation, stuff like that.
This isn't going away.
So it might be something you want to dig into if you want to have a little bit more context
on it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}