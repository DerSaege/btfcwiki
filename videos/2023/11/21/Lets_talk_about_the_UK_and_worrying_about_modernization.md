---
title: Let's talk about the UK and worrying about modernization....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uRYbWEbG2Eg) |
| Published | 2023/11/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the modernization of nuclear arsenals in major powers like Russia, the US, and China.
- Mentions the UK's nuclear deterrent involving Vanguard class submarines with nuclear weapons.
- Describes an incident where a Vanguard class sub faced a malfunction with its depth gauge.
- Speculates that an engineer on board noticed the issue, alerted command, and prevented a potential disaster.
- Emphasizes the importance of maintaining nuclear arsenals safely and orderly to avoid unnecessary tensions.
- Suggests that modernization of nuclear arsenals by countries like Russia and China is not necessarily a bad thing.
- Stresses that nuclear arsenals act as deterrents to prevent wars among major powers.
- Acknowledges the inherent fear associated with nuclear weapons but argues for their safe and functioning maintenance.
- Raises concerns about fear-mongering related to advancements in nuclear arsenals by other countries.
- Concludes by suggesting that modernization can benefit everyone by maintaining a safe and stable nuclear deterrence system.

### Quotes

- "Modernizing the stuff is as good for you as it is for them."
- "These arsenals are about deterrents. They're about deterrents when it comes to other major powers."
- "Advancements, sure you can be a little bit nervous about that but when it comes to just modernizing the stuff it's as good for you as it is for them."
- "There should be better ways to do that, but until they come along, the system that exists, the weapons that are out there, they have to be functioning."
- "Don't let those people who get paid by scaring you make nuclear weapons even scarier than they already are."

### Oneliner

Beau explains the importance of safely modernizing nuclear arsenals to maintain global stability and prevent unnecessary tensions.

### Audience

Policy makers, activists, concerned citizens

### On-the-ground actions from transcript

- Verify and support measures for safe and orderly maintenance of nuclear arsenals (implied).

### Whats missing in summary

The detailed nuances and specific examples discussed by Beau can be best understood by watching the full transcript. 

### Tags

#NuclearArsenals #GlobalStability #Deterrence #Modernization #Safety


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about how I learned
to stop worrying and love modernization.
It's a topic that has come up lately
as different countries start to modernize.
There's a lot of reporting about it
that maybe should be put into context.
There's modernization occurring in a lot of major powers.
And generally speaking, it gets framed in a very scary way and maybe it shouldn't.
Okay, so we are talking about the modernization of nuclear arsenals.
Russia is engaging in it, the US is engaging in it, China is engaging in it.
Each country pretends like the other countries doing it is a really bad thing,
but secretly they're really happy that they are.
And the United Kingdom provided us with an example of why.
Why?
Before we get into this, just to start off with, this occurred like a year ago.
We're just now finding out about it though.
So part of the UK's nuclear deterrent is having, I think the doctrine says one Vanguard
class sub Etsy at all times with nuclear weapons on board.
And that's where our story starts with that piece of information.
Now the official comment on this is basically something happened, it was concerning, the
system caught it, everything's fine now, no more questions.
That's the official line.
Some of the reporting and rumor mill says that one of the Vanguard class subs was out
doing Vanguard class sub stuff, hiding and trying to remain undetected, and well things
didn't go well.
There is a depth gauge on these subs.
On Vanguard class there is at least two.
is where those in command of the sub can see it. That one, what's the word, broke.
It stopped working and the sub began to approach, let's just call it unsafe
depths. Another one of the gauges is in engineering. Now the rumor mill says that
basically an engineer looked over, saw the depth they were at, quickly changed
to their pants and contacted those in command of the sub to alter what was
going on. Some of the reporting says that they went to crush depth. They did not
and we know that because it still exists. But the the reporting suggested it was
getting pretty close to that and if you don't know what that means just think
Titanic sub same thing only this one has 130 140 people on board and nuclear
weapons the Vanguard class subs they've been in use about 30 years so obviously
it is you know bad to lose a sub with nuclear weapons on it and a whole bunch
of people but that's just looking at that immediate event think about the
tensions that would be raised if if part of the UK's nuclear deterrent disappeared
and nobody knew why. A lot of phone calls, right? A lot of phone calls. A lot of
tension. So when you see reporting talking about Russia or China modernizing
their strategic arsenals, keeping their their triad functioning, just understand
it's not necessarily a bad thing. Yes, ideally no country would have nukes.
They're bad. We shouldn't have them as a species agreed. But as long as they're
out there, it would be best if they were maintained in a safe and orderly
fashion. Because if one of theirs disappears, they don't know why, that
would also raise a lot of tension. It creates a scenario where something bad
could happen when there's literally no reason for it to. So when you hear about
about this don't let don't let those people who get paid by scaring you make
nuclear weapons even scarier than they already are you know there there's like
an intrinsic amount of fear that exists with these things the fact that other
countries are modernizing is not necessarily bad it might be something
that keeps you safe as well. Because this is, these arsenals are about deterrents.
They're about deterrents when it comes to other other major powers. That
deterrents stops wars. There should be better ways to do that, but until they
come along, the system that exists, the weapons that are out there, they have to
be functioning. They have to be safe. They have to be where they're supposed to be and
not just randomly disappearing and potentially causing a lot of issues. I hope that that
engineer got a promotion, a medal something for potentially saving 130, 140 lives on the
sub and then you know the whole removal of those tensions. It's just something
to keep in mind because as China seeks parody and Russia modernizes there
there's inevitably going to be a lot of fear-mongering about it. Advancements,
sure you can be a little bit nervous about that but when it comes to just
modernizing the stuff it's it is as good for you as it is for them anyway it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}