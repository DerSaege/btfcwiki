---
title: Let's talk about how people feel about the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GCymOupDsyA) |
| Published | 2023/11/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discussed the importance of how people perceive the economy and its impact on behavior.
- Mentioned polling data showing an increase in Americans feeling that the US economy is strong.
- Noted that people's perception of the economy can influence their spending habits.
- Shared statistics from a Harvard Caps Harris poll, indicating optimism about the economy.
- Pointed out that although presidents don't control the economy, Biden's approval rating on handling the economy has improved.
- Suggested that the positive sentiment towards the economy could lead to significant improvement.
- Emphasized the potential for increased economic activity, particularly during the holiday season.

### Quotes

- "People are starting to fill the rebound."
- "If Americans feel like the economy is doing well, they spend money."
- "It shows that what the economists have been telling us is now starting to be felt."
- "This is a good sign if you have been concerned about the U.S. economic picture."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

People's perception of economic well-being influences spending habits, with recent polls showing growing optimism and potential for economic improvement in the US.

### Audience

Economic observers

### On-the-ground actions from transcript

- Monitor economic trends in your community for potential shifts in consumer behavior (implied)
- Stay informed about polling data reflecting public sentiment on the economy (implied)
- Adjust personal financial decisions based on perceptions of economic conditions (implied)

### Whats missing in summary

Insights on the potential long-term implications of changing public attitudes towards the economy

### Tags

#Economy #Perception #Polling #US #Optimism


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about some economic numbers,
but not the normal kind we talk about.
We're gonna talk about how people feel about the economy
and polling about the economy.
We have been talking on the channel for a while
about how the economists,
the people who really pay attention to the numbers,
they're like, the economy's improving, it's doing better,
but people don't feel it right away.
and there is a lag, that appears to be coming to a close.
People are starting to feel like the economy
is doing better, which is really important in the U.S.
because the United States economic system is...
It is largely based on faith.
How you feel about the economy
actually impacts the economy
because it changes your behavior.
If Americans feel like the economy is doing well, well, they spend money.
If you spend money, the economy does better.
If you feel like the economy is doing poorly, you maybe don't spend money.
The economy does worse.
The way you feel about it matters.
Okay, it's kind of a snowball effect.
So what do the numbers say?
the polling say? 42% of Americans feel that the US economy is strong. That is the highest number
since February. So people are starting to fill the rebound. 47% are optimistic about next year.
30% say that their personal financial situation is improving.
That number is up 5 or 6% from July.
Now this polling was done by Harvard Caps Harris.
And the, I'm going to go ahead and say it, presidents don't actually control the economy.
However, because of the way people are feeling about the economy, Biden's polling on how
he is handling the economy has improved.
It's now 44%.
So overall, this is good news for the U.S. economic picture.
It shows that what the economists have been telling us is now starting to be felt.
Once people start feeling that way, they start acting that way, which actually speeds the
rebound normally.
So unless something unusual happens, we might start to see more significant improvement.
And this is the first set of polling like this that I've seen that shows a consistent
trend of people feeling like the economy is doing better.
The fact that this is occurring right before the holidays, when generally speaking a whole
bunch of money gets spent, is probably going to maybe even kickstart things a little bit.
So, again, there's always outliers, there's always things that can change it, but this
is a good sign if you have been concerned about the U.S. economic picture.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}