---
title: Let's talk about oil, Louisiana, and leaks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rr5_YDqs3WA) |
| Published | 2023/11/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of an oil leak in Louisiana from an underwater pipeline.
- Initial observation of a noticeable sheen on the water at around 9 AM.
- A call about the pipeline leak was made about 10 minutes later.
- By 2 PM, the slick had spread to three to four miles wide.
- Approximately a million gallons of oil have leaked, with efforts to clean up underway.
- The pipeline was shut off quickly following the leak.
- The Coast Guard and three skimming vessels are involved in the cleanup process.
- No impact on the shore or human injuries reported so far.
- Remote controlled vehicles are being used to locate the source of the leak.
- Multiple state and federal agencies are working on containing and cleaning up the spill.
- The response time to the leak appears to have been fast and effective.
- Importance stressed on swift cleanup due to the non-natural elements causing harm.
- Efforts are focused on cleaning up the spill as quickly as possible.
- Anticipated impact on the shore is not currently expected.
- Beau promises to stay updated on the situation and provide further information.

### Quotes

- "A million gallons is not good, but it's way better than a million barrels."
- "Sunlight and wind and stuff like that doesn't leak."
- "They appear to be on the ball on this one."
- "We have to transition. We have to build the infrastructure."
- "Y'all have a good day."

### Oneliner

Beau gives an overview of an oil leak in Louisiana, stressing the importance of quick cleanup efforts and the need for transitioning to alternative technologies.

### Audience

Louisiana residents, environmental activists

### On-the-ground actions from transcript

- Contact local environmental organizations to inquire about volunteer opportunities for cleaning up oil spills (suggested).
- Support initiatives advocating for the transition to cleaner energy sources and infrastructure (implied).

### Whats missing in summary

The emotional impact on marine life and ecosystems due to the oil leak. 

### Tags

#Louisiana #OilLeak #EnvironmentalImpact #CleanupEfforts #TransitionToCleanEnergy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Louisiana
and oil and the leak and how everything is shaping up.
This is early, there's not a whole lot of information
and a lot of it is, it's an estimate more than anything.
But we can go over the information
that's currently out there
and just kind of run through it.
This deals with a pipeline that sits offshore, it's underwater.
So it looks like around, I want to say, 9 AM,
there was a noticeable sheen on the water.
About 10 minutes later, call went out.
And hey, there's a pipeline leak.
By 2 PM, the slick was three to four miles wide.
The estimate says that it is around a million gallons of
oil has come out.
Now, my understanding from talking to somebody about it
was that 1.1 million gallons is the most it could have been
they shut off the pipeline pretty quickly. The Coast Guard arrived pretty
quickly. There are three skimming vessels running right now that I know of trying
to clean it up. So far it has not impacted shore and there are no human
injuries. Some of the questions that came in were asking about barrels. The estimates
that are going out are in gallons, not barrels. Don't get me wrong, a million gallons is
not good, but it's way better than a million barrels. There are kind of like remote controlled
vehicles, looking for any potential source of leak, checking the pipeline.
There's 20-something agencies at the state and federal level that are already working
on it.
I don't want to say this early on, but it looks like they kind of got there quickly,
which is a happy, unexpected thing.
This is not something that is normally quickly dealt with, but it looks like in this case
it was.
All of that being said, it is important to remember that sunlight and wind and stuff
like that doesn't leak.
So they're trying to clean it up as quickly as possible and they appear, at least based
on the information we have so far, to kind of be on the ball on this one.
That could obviously change as more information comes out, but that's where it's at and nowhere
in anything that I looked at did it show an anticipated impact to shore. So they
might be able to do some pretty decent containment with this but we'll have to
wait and see how it plays out. I will definitely keep following this and let
y'all know what's going on. You know, again, we have other technologies. We just
have to transition. We have to build the infrastructure. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}