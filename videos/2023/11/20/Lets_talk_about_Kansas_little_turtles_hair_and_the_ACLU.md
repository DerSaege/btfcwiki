---
title: Let's talk about Kansas, little turtles, hair , and the ACLU....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XgP2aGO1ukU) |
| Published | 2023/11/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The American Civil Liberties Union is involved in a case in Kansas where an eight-year-old boy was allegedly forced to cut his hair.
- The boy, who is Native American, decided to grow his hair after attending a cultural event called "little turtles."
- The school has a policy on hair length that only applies to boys, which the ACLU argues violates religious freedom and involves sex-based discrimination.
- The boy's mother tried to get an exemption from the school, but none was granted.
- The ACLU has given the school until December 1st to address the issue, or it may proceed to a courtroom setting.
- Hair length is a significant issue for those with closely held religious beliefs.
- The ACLU's involvement suggests that the case may escalate beyond a mere request for change.
- Beau anticipates that the ACLU is likely to win on the federal side of this issue.
- The importance of this case lies in the intersection of religious beliefs and discriminatory policies.
- Beau will continue to follow this case closely and provide updates.

### Quotes

- "The school has a policy about hair length and it only applies to boys."
- "You really don't get more closely held religious belief than this."
- "I'm curious to see what the school does and whether or not this proceeds to levels above just hey you need to change this."

### Oneliner

The ACLU challenges a Kansas school's policy on hair length, citing religious freedom and potential discrimination, giving a deadline of December 1st for resolution.

### Audience

School Administrators, Civil Rights Activists

### On-the-ground actions from transcript

- Contact the school to express support for the boy and urge them to address the discriminatory policy (suggested).
- Stay informed about updates on the case and be ready to take action in solidarity with the affected family (exemplified).

### Whats missing in summary

Importance of standing up against discriminatory policies and supporting individuals asserting their religious freedom rights.

### Tags

#ACLU #ReligiousFreedom #Discrimination #KansasSchool #CivilRights


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about little turtles
in Kansas and hairstyles.
Because there's been some developments in Kansas
involving an elementary school and their policy
when it comes to hair length.
And the American Civil Liberties Union is getting involved.
They have contacted the school
behalf of an eight-year-old boy who was allegedly forced to cut his hair. He is
native. He made the decision to grow his hair after attending the gathering of
little turtles, which is an event. I think it occurs in June. It's geared
towards ages 5 to 15 in there and it's it's designed to put them in touch with
their cultural and spiritual beliefs. The school has a policy about hair length
and it only applies to boys. The ACLU is saying that this policy violates not
only, you know, religious freedom, but there's also sex-based discrimination
going on. The reporting says that his mother actually reached out to the
school, tried to get an exemption, they said there weren't any exemptions, and
the ACLU is saying this violates not just federal law but state law too. And
this is one of those things where if there isn't a remedy pretty quickly, it
will probably proceed to a courtroom setting. My understanding is that at this
point the ACLU is just kind of like, hey here you go, you need to fix this, you
have until December 1st is what I believe they were told. You know if you
are not aware the the hair length it's not it is not a it's not something that
is taken lightly. A decision like that is not taken lightly. It is something that
that is for people who talk about, you know, closely held religious beliefs and
all of that stuff and that should be grounds to just do or not do anything,
you really don't get more closely held religious belief than this. This is
important and I'm curious to see what the school does and whether or not this
proceeds to to levels above just hey you need to change this. My guess and my
understanding of at least the the federal side of this is that the ACLU
would win and probably pretty quickly but this is when we'll we'll keep
following it and I will keep you posted. Anyway it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}