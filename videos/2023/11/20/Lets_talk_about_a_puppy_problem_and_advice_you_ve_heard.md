---
title: Let's talk about a puppy problem and advice you've heard....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=igwpTO_PFmo) |
| Published | 2023/11/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A lasting respiratory disease affecting dogs, causing pneumonia, has been noticed in Oregon, Colorado, and New Hampshire.
- The disease, possibly antibiotic-resistant, has shown an uptick in cases over the past year.
- Despite over 200 identified cases in Oregon, experts advise not to panic as there hasn't been a significant increase in dog fatalities.
- The recommended precaution for dog owners is to practice social distancing for their pets by reducing contact with other dogs.
- Researchers are actively working on identifying and combating the disease, but no concrete solutions have been found yet.
- The lack of a centralized system for tracking and addressing the disease presents a challenge.
- It is vital for dog owners in the affected areas to stay informed, especially if they frequent dog parks.
- The disease manifests with symptoms like coughing and sneezing, resembling common respiratory issues.
- Updating vaccines, particularly those targeting respiratory illnesses, is advised as a precautionary measure.
- The disease is currently unnamed, described only as a lasting respiratory illness causing pneumonia in dogs.

### Quotes

- "First, don't panic. They're saying that it's bad, it's rough, but they haven't noticed a large uptick in the number of dogs lost to this."
- "Dogs need to socially distance."
- "It's more of a coalition of the willing than a well-thought-out system so far."

### Oneliner

A respiratory disease affecting dogs in Oregon, Colorado, and New Hampshire prompts caution and social distancing for pets, with ongoing research to identify and combat the illness.

### Audience

Dog Owners

### On-the-ground actions from transcript

- Stay informed about the disease and its developments, especially if you live in areas like Oregon, Colorado, and New Hampshire (suggested).
- Practice social distancing for your dogs by reducing contact with other dogs (suggested).
- Update your dog's vaccines, especially those targeting respiratory issues (suggested).

### Whats missing in summary

The full transcript provides additional details on the ongoing research efforts and the lack of a centralized system to address the disease.

### Tags

#RespiratoryDisease #Dogs #PetHealth #Precautions #Research


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a problem for puppies
and their people, and we will talk about Oregon
and Colorado and New Hampshire specifically,
but this probably applies to a much wider area,
and we just don't know it yet.
It all started, I don't even know if it all started.
There was a noticeable uptick in August,
but it looks like this maybe started a year ago.
And basically, what's occurring is
there is a lasting respiratory disease that often
causes pneumonia in dogs.
It is antibiotic resistant.
It's causing issues, obviously.
want to say in Oregon they have identified more than 200 cases of it. So
what are the experts on this actually saying? First, don't panic. They're saying
that it's it's bad, it's rough, but they haven't noticed a large uptick in the
number of dogs lost to this. So it's not, it isn't like a plague, it's not a puppy
plague, but it's something to be aware of. And generally the advice seems to be
advice you heard for a couple of years. Socially distance. The puppies
need to socially distance. Dogs need to, basically you want to decrease contact
with other dogs. There is a lab that's been working on this pulling samples
from all over the place and trying to come up with something. At time of
filming they don't actually have a real good grasp on it yet but it's something
to be aware of because until they do we don't really know how how bad or how
widespread it is. And unlike, you know, unlike things that deal with humans,
there's not a giant centralized system for this. So there are researchers who
are working on it, but it's more of a coalition of the willing than a
well-thought-out system so far. It's not comprehensive yet. So it's something to
be aware of, particularly if you live in these areas and you take your dogs to
dog parks, stuff like that. And we'll keep you updated on it. So far I haven't even
been able to find a name for this yet. They're just referring to it as a
lasting respiratory disease that often causes pneumonia and it's antibiotic
resistant. The dogs cough, sneeze, all of the normal stuff. And that's the news. And
at the moment, you know, the advice is update on your vaccines, particularly
those that deal with respiratory issues and stuff like that. But it doesn't seem
that they know exactly what this is yet. But it is something to be aware of and
the early advice is to decrease contact with other dogs. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}