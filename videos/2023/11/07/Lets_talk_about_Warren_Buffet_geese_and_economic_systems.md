---
title: Let's talk about Warren Buffet, geese, and economic systems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5DP11T-dOkA) |
| Published | 2023/11/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the economic system in the United States and the concentration of benefits at the very top, exemplified by Warren Buffett.
- Describing Warren Buffett as one of the wealthiest individuals who has greatly profited from the current economic system.
- Criticizing the American myth of the "American Dream" and how it no longer functions as it once did.
- Addressing the defense and support from working-class individuals towards billionaires like Warren Buffett.
- Explaining the exorbitant wealth of billionaires, such as needing to earn a million dollars every year since 1023 to become a billionaire.
- Noting Warren Buffett's shift towards discussing wealth distribution and recognizing the need for adjustments in the system.
- Pointing out that even the billionaire class is starting to acknowledge the inequities in the economic system.
- Anticipating potential reactions from the right wing towards Warren Buffett's views on wealth distribution.
- Comparing Warren Buffett's criticism of the economic system to progressive figures in Congress and the Senate.
- Concluding with a reflection on the evolving perceptions of the economic system.

### Quotes

- "You want to keep a system where the goose lays more golden eggs every year. We've got that. Now the question is, how do those eggs get distributed?"
- "Warren Buffett has a more scathing view of the current economic system than a lot of people that we view as progressives in Congress and in the Senate."

### Oneliner

Beau dives into the skewed economic system, spotlighting Warren Buffett's stance on wealth distribution and the shifting perceptions around it.

### Audience

Economic justice advocates

### On-the-ground actions from transcript

- Question and challenge wealth distribution models (implied)

### Whats missing in summary

Deeper insights into the evolving dynamics of economic inequality and perceptions among different societal classes. 

### Tags

#EconomicJustice #WealthDistribution #WarrenBuffett #AmericanDream #WorkingClass


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about economics.
We're going to talk about the economic system.
We are going to talk about the goose
who laid the gullet in an egg.
We're going to talk about an investment icon, somebody
who is viewed as just a literal genius when
comes to using the system that exists in the United States and making a ton of money.
The Oracle of Omaha.
We are going to talk about comrade Warren Buffett.
Just wait.
So in the United States, the economic system has over time shifted more and more to benefiting
the very top.
like Warren Buffett, if you are not familiar with him, easily one of the top
ten richest, maybe top five, very, very wealthy person, a person who has
benefited immensely from the current system. And it is a system that despite
all American myths, it benefits those on the top the most. And when you say that
you inevitably have people show up to cheerlead, you know, the general idea of
the American dream. At this point they call it that because you got to be
asleep to believe it. It doesn't work the way it used to and it didn't work
great back then, it has shifted more and more. And you often find people who are
in the working class who go out of their way to benefit the interests and defend
the interests of people like Warren Buffett, people who literal billionaires.
and to be clear sometimes I think people have a hard time grasping what a
billionaire really is. If you made a million dollars a year every year and
you saved all of it if you wanted to be a billionaire you would have had to
start making that money earning that money in the year 1023 a billion
dollars is just an absurd figure. So what does all of this have to do with Warren
Buffett? Because the defenders of the current system have apparently lost him,
at least to some degree. This is what he had to say. You want to keep a system
where the goose lays more golden eggs every year. We've got that. Now the
Now the question is, how do those eggs get distributed?
And that is where the system needs some adjusting.
Warren Buffett is now talking about the distribution of wealth and saying it needs some adjustment.
It's noteworthy.
If you are somebody who truly believes that the American economic system is beneficial
and is working as it should to achieve a world where even hardworking people, people who
work 60 hours a week, get a fair shake.
You are now more committed to this system than the billionaire class, because even they
are starting to realize it is way out of whack, and those on the bottom are not getting even
what the billionaire class thinks is a fair shake.
I cannot wait to see the right wing call Warren Buffett a socialist or a communist.
Warren Buffett has a more scathing view of the current economic system than a lot of
people that we view as progressives in Congress and in the Senate.
That should tell you something.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}