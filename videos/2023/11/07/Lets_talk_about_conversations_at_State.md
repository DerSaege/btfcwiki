---
title: Let's talk about conversations at State....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EpiREh2Lukw) |
| Published | 2023/11/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the gap between US diplomats' public statements and the actual actions of US diplomacy conducted by lesser-known diplomats.
- Mentions a leaked memo from the State Department's descent channel criticizing the gap between public statements and actual actions.
- Talks about how the gap in messaging not only misinforms the public but also damages U.S. foreign policy interests.
- Points out that diplomats are unhappy with how their messaging is perceived by the public.
- Indicates that there may be a shift in messaging from the State Department to explain their actions more clearly.
- Suggests that greater transparency in messaging could help cool tensions in certain regions.
- Speculates on potential changes in the Biden administration's communication following the leaked memo.
- Anticipates that the leaked memo will spark a significant public reaction and potentially become a major news story.
- Encourages looking up "descent channel" for more details on the leaked memo.
- Predicts a challenging week ahead for the State Department.

### Quotes

- "We must publicly criticize Israel's violation of international norms."
- "The diplomats at State are super unhappy with how the messaging is coming across to the public."

### Oneliner

Beau explains the gap between diplomats' public statements and actions, noting a leaked memo criticizing the discrepancy, which could lead to a shift in messaging and potential repercussions for the State Department.

### Audience

State Department officials

### On-the-ground actions from transcript

- Research and read the leaked memo from the descent channel to understand the criticisms and discrepancies within the State Department (suggested).
- Stay informed about potential shifts in communication from the State Department regarding their actions and foreign policy messaging (implied).

### Whats missing in summary

Insights on the importance of transparency in diplomatic messaging and the potential impact of leaked memos on public perception and foreign policy decisions.

### Tags

#StateDepartment #Diplomacy #Transparency #ForeignPolicy #Messaging


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about state.
We're gonna talk about State Department
and what goes on behind the scenes.
And we're gonna talk about a news story
that at time of filming is not a news story yet,
but I feel like it's gonna become one.
For the last couple of weeks,
there's probably been on average a video a day
that could best be described as me explaining the gap between US diplomats' public statements,
the diplomats you know the name of, and the actual actions of US diplomacy that are conducted
by the people who you don't know the name of.
people who are foreign policy experts, area experts, country experts, the people who I've
described as those who specialize in the art of the possible.
We are talking about the people that Biden brought on for that amazing foreign policy
team before he even took office, that crew.
You have those two groups inside State Department.
And there has been a huge gap in what one group says and what the other group is doing.
I personally have found this very annoying.
Come to find out, State Department finds it annoying as well.
Inside State, there's a thing called the descent channel.
It's exactly what it sounds like.
It's a place where those mid-level diplomats, those people who you don't know the name
of, where they can write a memo.
And that memo goes up to the people who you do know the name of.
And they can basically come out and be very open and say, this is where you're messing
up.
You're in the mess around stage right now.
You're going to be in the find out phase soon.
And they can speak very freely in this channel.
It's something State Department fosters to allow, you know, two ways of communication.
Memos that go into the dissent channel, they don't normally become public.
One has leaked.
One of the biggest criticisms in what can only be described as a scathing memo is the
gap between what they're actually doing and what gets presented in public.
That's a big part of it.
And that gap, not just is it bad because you, the American people, don't necessarily know
what they're doing. It's also damaging to U.S. foreign policy interests because it
shapes the regional perception of the U.S. and what the U.S. is doing. If you
don't follow foreign policy, if you haven't been watching channels that have
been translating what is being said versus what they're actually doing, you
would be forgiven for thinking that the U.S. has just cosigned everything that's
going on right now. That is not the case. The memo highlights that and then it
highlights what the diplomats who actually do the work, what they think
about certain activities that Israel has engaged in. I'm not gonna go through the
whole thing because for the last couple of weeks anytime I read a quote from
somebody I get hate mail that basically acts like I wrote it, but I'm going to
read the least scathing thing in it. We must publicly criticize Israel's
violation of international norms such as a failure to limit offensive operations to legitimate
military targets.
That's the least scathing thing in it.
It goes on from there.
The short version is the diplomats who do the work, the area experts, the foreign policy
experts at state, they are super unhappy with how the messaging is coming across to the public.
It's also worth noting that my understanding is that this is not the only memo. The one that
leaked is not the only one that is circulating in the dissent channel right now.
So what does this mean?
The standard policy for state is to not comment on anything that comes out of the dissent
channel.
That's their normal thing.
And the reason that's a policy is because they actually, they really do want people
to be able to write memos like this and say you're messing up. They really do try to foster that
free flow of ideas and information. So it is incredibly unlikely that state comment on this
directly. That being said, there might be a shift in their messaging. They may go
to greater lengths to explain like why they're trying to get aid when you know
there's not going to be a pause and stuff like that. They might go
greater lengths to explain what they're doing, which I think would be incredibly helpful,
because a lot of what they're doing would help cool tensions. If more Arab states knew what the
U.S. was doing in regards to trying to get relief and trying to get aid and trying to urge restraint
and all of that stuff, it might cool things off. Now, at the same time, I haven't commented on
what conversations might be happening behind the scenes with those countries, because I don't know.
I can't even guess. For all we know, the state has told them, has told their governments what they're
doing which would help explain why there was a relatively easy time when it came to trying
to make sure that everything didn't escalate.
So that could be happening but that's only the government.
A lot of the people who might act are not members of the government so it needs to be
public. The other thing is you might see a shift in language from Biden or
members of the Biden administration, because a lot of this, it's actually
good politics if it was put out, which is part of the reason I've had a hard
time figuring out why they're not talking about it, so you might start to see that.
The fact that this got leaked means that there's going to be a conversation about it because
some of the stuff that's in this, it's not something that would ever be expected to come
from U.S. government officials. So there's going to be a conversation. My
guess is that this is going to turn into a pretty big story. If not and you want
to read everything, you can Google descent channel and you'll probably
find it pretty quickly. But it's probably gonna be a rough week at State
department. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}