---
title: Let's talk about pauses, the performative, and pursuits....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OMxYO4np8Os) |
| Published | 2023/11/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the US attempt to get Israel to agree to a ceasefire and humanitarian pause, which Israel rejected.
- Describes the US efforts to secure aid for Gaza despite the challenges of getting it in.
- Compares the situation to selling a car to a friend who may or may not have the money.
- Mentions the focus of US efforts on delivering aid to southern Gaza, potentially overlooking northern Gaza.
- Talks about the current US strategy prioritizing certainty over morality in diplomatic efforts.
- Acknowledges the arguments for and against the effectiveness of the current approach.
- Emphasizes that the actions taken by the US are not performative but aimed at achieving specific goals.
- Suggests that while more could be done, there is a methodical approach in place focusing on what can be helped.
- Acknowledges the desire for immediate solutions but stresses the complex nature of the conflict.
- Ends by noting the ongoing fighting and the challenges in finding a quick resolution.

### Quotes

- "There is a method to it and it's focusing, it's kind of like triage."
- "But what's happening, there is a method to it and it's focusing, it's kind of like triage."
- "It helps people, but not those that are most impacted."
- "That's what's going on."
- "There are a lot of people who want immediacy and they want a solution to this quickly."

### Oneliner

Beau sheds light on US efforts to secure aid for Gaza, addressing criticisms and explaining the methodical approach taken despite challenges.

### Audience

Diplomatic observers, Aid advocates

### On-the-ground actions from transcript

- Support organizations delivering aid to Gaza (suggested)
- Advocate for a comprehensive strategy addressing all impacted areas (implied)

### Whats missing in summary

Broader context and detailed analysis can be gained from watching the full transcript.

### Tags

#US #Diplomacy #AidEfforts #Gaza #Conflict


## Transcript
Well, howdy there internet people, it's Bo again.
So today, we are going to talk about the art of the possible.
We're going to talk about aid and pauses and performative
actions.
And we're going to go through a question and hopefully shed
some light, because I feel like this is probably not the only
person wondering what's going on. Okay, so here's the message. I was so mad at
you for the way you covered this in the beginning. I actually stopped watching.
Then I saw things you said that I thought was corporate dim cope actually
happen. I have a question. I saw where the US tried to get Israel to commit to a
ceasefire. I roll emoji and then end quotes humanitarian pause. They said no
even though they said no the US is running around the Middle East trying to
get aid is that just face-saving all caps or is there more to it why try to
get aid if the Israelis won't stop long enough to get it in seems like
performative BS okay so that actually does a really good job of the in case
missed it part. So the US tried to get a humanitarian pause. Israel was like yeah
that's not happening. Immediately the US is going to different countries talking
to different countries to include I believe Turkey and Jordan trying to get
them to deliver aid even though it won't get in. And the question is that
performative. Now, let's put it into different terms. Let's say you have a car, you have a car,
and your friend has always liked this car, and always said, I want to buy that car, and they
never have the money. Now you finally decide you're going to sell the car, and your friend
calls you up and says hey bring it over to me I would like to buy it. How likely
are you to do that because they don't have the money? Or what if your friend
sends you a text message that shows you a stack of hundred dollar bills and it
says bring the car over? Which one is going to be more likely to get a
response from you. It is easy to say no, we're not going to do a humanitarian
pause when there's no humanitarian aid. Not yet. But if it is sitting right
outside, the amount of pressure that can be brought by asking for it at that
point is far greater. I know people have their opinions about some of the top names when it comes
to American diplomatic efforts right now. Just remember that the core staff, the people whose
names you never hear about, we talked about them before Biden ever took office because he had them
picked way back then. They specialize in the art of the possible. Realistically, all
of the aid, anything that's committed, anything that shows up, it's going to
southern Gaza. It's not going to the north. If you want a real no corporate
DEM-COPE assessment, my guess would be that northern Gaza has kind of been written off
because it's already broken off militarily. There's a line there and it's already broken off from the
southern portion. My guess is that right now U.S. efforts are going to be primarily focused on
getting aid into southern Gaza. That's what they're going to be dealing with.
Once that's more under control, they will probably move on. Understand, move on to
trying to get something done about northern Gaza. But understand that's probably not going to be a
quick process. Going around and trying to get countries to say, yeah, we'll put the aid on
planes or ships and get it there, that's making it more tangible.
That's more than just a random dollar amount will provide this much aid.
It makes it real.
When it's real, there's more pressure to say yes.
You have to let it in.
what it is. Again, there are different approaches when it comes to diplomacy for stuff like
this. The current course that the US seems to be on is certainty is more important than
morality. That seems to be where they're at. They're trying to take care of what
they know they can and help where they know they can rather than risk burning
all of their political capital on something that might not work. You want
to criticize that? You want to argue about that? Yeah, that's something to...
There's an argument to be made there because there's an argument that says what's happening
is bad enough, you have to take the risk.
But what's happening is not performative.
They're trying to accomplish certain things.
Now, the argument that more needs to be done and it needs to be more firm, it's an argument
to be made.
That is something that, there's no good counter-argument against it.
I don't know that you could say you couldn't do both at the same time.
There's a lot of criticisms that can be made.
The one that I don't think realistically can be is that what's happening is performative.
It will have a tangible impact, whether or not it's enough for you or whether or not
you think that they could do better, whether or not you think they're going far enough.
All of that, that's free game.
You can argue about that all day.
But what's happening, there is a method to it and it's focusing, it's kind of like triage.
You know this can be helped, you're not certain about this stuff.
So that's what's going on.
My guess is that that push will be successful, but it doesn't actually stop the heavy fighting.
It helps people, but not those that are most impacted.
I guess for a lot of people watching this, you will say that they're working from the
wrong end.
And again, that's a criticism you could make.
It's more about overall strategy than what they're actually doing.
So that's what's going on.
There are a lot of people who want immediacy and they want a solution to this quickly.
There's not going to be one.
It is incredibly unlikely.
It's important to understand that the fighting that you've seen, that's just shaping.
That's not the real fighting yet, as far as up north.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}