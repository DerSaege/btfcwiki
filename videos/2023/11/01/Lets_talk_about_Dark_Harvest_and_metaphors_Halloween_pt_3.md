---
title: Let's talk about Dark Harvest and metaphors (Halloween pt 3)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=A9w_AdRdmks) |
| Published | 2023/11/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau loves symbolism, metaphors, and allegories.
- Talks about the movie "Dark Harvest" set in a small town in the 1960s.
- High school seniors, only boys, are called upon to fight a monster every year.
- The boys are lured with promises of rewards but have no choice but to fight.
- The cycle continues as the hero of one year becomes the monster of the next.
- The adults in the town know about this cycle but push the kids to participate.
- The story delves into themes of duty, sacrifice, and the perpetuation of violence.
- The movie portrays a cycle of violence with no end in sight.
- Violence is shown as not being the solution to the underlying issue.
- Beau shares his reflections on the deeper meanings behind the movie.

### Quotes

- "Violence is not actually the solution to this issue."
- "The right kind of violence or enough violence is the solution but it just creates the next generation robs them of more of their most precious resource."

### Oneliner

Beau loves symbolism and metaphors, discussing the cycle of violence portrayed in the movie "Dark Harvest" where high school seniors are forced to fight a monster every year, revealing deeper themes of duty, sacrifice, and the perpetuation of violence with no end in sight.

### Audience

Movie enthusiasts, symbolism appreciators.

### On-the-ground actions from transcript

- Organize a community movie viewing followed by a thoughtful discussion on symbolism and deeper meanings portrayed in films (suggested).
- Start a book club to analyze literature and movies for hidden allegories and metaphors (suggested).

### Whats missing in summary

Exploration of how societal norms and traditions can perpetuate cycles of violence and sacrifice.

### Tags

#Symbolism #Metaphors #CycleOfViolence #Community #MovieAnalysis


## Transcript
Well, howdy there, internet people, it's Bo again.
So, today, we are going to talk about a movie.
Movie that is very fitting for this evening.
And metaphors, and allegories.
If you have watched this channel for any length of time,
you have probably picked up on the fact
that I love symbolism.
I love metaphors.
things that are allegorical.
I love being able to talk about one thing
when I'm actually talking about something else.
And I love the fact that y'all
indulge me when I decide to do that.
So we will talk about
the
things I see
in a movie called Dark Harvest.
Now, this is a relatively new movie, and it's a scary movie, so if you are somebody who
cares about spoilers and cares about the plot of scary movies, you might want to stream
that first.
I am sure it is on whatever your favorite streaming platform is, because there are definitely
going to be spoilers in this. This is not a movie to watch with your kids, just to be clear.
Okay, so the movie is set in this small, non-descript town. It kind of has a Kansas vibe to it.
It's the 1960s.
And in this town, every year, the high school seniors, just the boys, not the girls, just
the boys, boys becoming men, they're called upon to do their duty.
They have to fight for the town.
They have to fight.
There's the promise of monetary reward, being a hero, all of that stuff, but they don't
have a choice.
They have to fight.
Some of them are eager to do so, at least until it starts, and then they realize that
kind of thing's really scary.
Some of them, they know they don't want to do it, because they know that each year some
of these young men don't make it.
Some of them know they don't want to do it to a degree that they try to get away, get
out of the jurisdiction.
But they get brought back.
And some of them even say, well, they can't make me do this.
They can make me be there, but they can't make me fight.
It sounds like they have some kind of objection to it, I don't know.
And the guy who actually takes down the external threat, which is a monster from the fields,
Sawtooth Jack, the guy that takes him down, he's a hero.
He is a hero.
allured to participate. Monetary reward, a nice car, and being able to see the
world. But they don't have a choice in the 60s. I have no idea if it's the
author's intent, okay, because I watched the movie just a couple days ago. I
I haven't read the book it's based on, but I couldn't help but see the draft in that.
It's the only way I could see it.
But that alone, I mean that would be kind of a cheesy, a cheesy setup, I mean using
a movie to make that point, just that, I don't know that that's enough.
Again, don't know what the author's intent is, but that's not where the story ends though.
It's not just, you know, some kind of wild night where the boys are fighting a monster.
There's a little bit more to it.
See the hero, the guy who actually takes down Sawtooth Jack.
When he comes back to town, nobody wants to be around him because he's a monster.
That could be a metaphor too, all by itself.
But in this case, he is a literal monster.
He is the literal monster.
He comes back.
The boy who takes down Salt Tooth Jack becomes the Salt Tooth Jack of the next year.
The other thing you find out is that the town knows.
The adults, they all know, they have a real clear picture.
But there's that monetary reward and they push their kids to do it, knowing that some
of them won't come back and at least one of them is going to turn into a monster.
And when you hear them talk about it, it's one of the real interesting things in the
movie because most movies in this genre they really don't flesh stuff like this
out but you see the adults particularly those that are you know big in the
community you see them say stuff like doing your duty it's how it's always
been, have to do your responsibility, something you have to take care of, have
to protect the town, so on and so forth. Because if Jack makes it into town and
he gets to the church, well, they have like nine years of bad luck or something.
Things just don't go well. So, they concocted the scheme. The boys, they fight
Jack. The winner, the guy who takes him down, well, the sheriff takes care of him.
Puts him in a hole. He never actually gets to see the world. In this case,
that cyclical pattern of violence or one thing feeds into the next and then it
comes back and then it goes back around. In this case that seed is literally
planted and there's no end. There is no end because in that situation killing
the monster just creates the need for another, it creates that cycle.
Year after year, generation after generation, turning themselves into monsters over dirt.
there's no end because the kind of struggle that sawtooth Jack and the town
are locked in they are committed to believing that the right kind of violence
or enough violence is the solution but it just creates the next generation robs
them of more of their most precious resource, more youth lost year after year,
and nothing changes because violence is not actually the solution to this issue.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}