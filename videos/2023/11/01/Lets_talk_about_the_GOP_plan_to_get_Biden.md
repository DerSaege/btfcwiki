---
title: Let's talk about the GOP plan to get Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wfm3JUddQQQ) |
| Published | 2023/11/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- GOP's new plan to impeach Biden lacks clarity, even after failed hearings described as an "unmitigated disaster."
- GOP shifting from hearings to depositions to depose individuals and manipulate quotes out of context.
- Republican base likely to fall for the manipulative tactics, despite lack of substantial evidence.
- GOP's continued efforts to impeach Biden appear misguided and may alienate independent voters critical for their success.
- Trump's influence may be driving the GOP's relentless pursuit to impeach Biden, despite lack of concrete findings.
- GOP's repetitive tactics of rehashing impeachment attempts may not yield desired results.
- Be prepared for the GOP's forthcoming strategy of using cherry-picked quotes to create a conspiratorial narrative around Biden.

### Quotes

- "It just, everything that they put out was either quickly refuted or it just kind of went off in their face, nothing went according to plan."
- "They will take them, ask them a bunch of questions, they won't release the entire deposition. They'll pull quotes out of context and they'll share them."
- "A lot of the Republican base at this point has been sold on the idea that it's right around the corner, just like everything else that they were promised by the GOP that is still right around the corner."
- "While it might work for their base, it is probably going to have a negative impact on independence and it's worth remembering the Republican Party cannot win without the independence."
- "My guess at this point is that Trump was upset that he was impeached and he is pushing the Republican Party to impeach Biden for something."

### Oneliner

GOP's vague plan to impeach Biden through manipulative depositions may deceive their base but alienate independents, risking their electoral success under Trump's influence.

### Audience

Political Observers

### On-the-ground actions from transcript

- Stay informed on the GOP's tactics and be ready to counter misinformation (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the GOP's impeachment strategies and the potential repercussions on their base and independent voters.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the GOP and Biden and their new plan, because
the old plan didn't work so well.
Um, so the GOP has hatched a new method of trying to get something on Biden and
impeach him for something. We still don't exactly know what, even though they had
hearings, they had impeachment hearings. If you missed that and you didn't know that
they occurred, you would be forgiven because they were that bad. Unmitigated
disaster was the term used to describe them. It just, everything that they put
out was either quickly refuted or it just kind of went off in their face, nothing went
according to plan.
So back to the drawing board like the Wiley Coyote, they have decided that rather than
having more hearings, that's how bad they were, they're not even, it doesn't look like
they're going to have any more.
They're going to do depositions because they think they can do more with depositions.
I mean that's true, if you know what that means, yeah, yeah you can't, because what
they can do, and my guess is what their plan is and what you need to get ready for, is
they will depose people.
They will take them, ask them a bunch of questions, they won't release the entire deposition.
They'll pull quotes out of context and they'll share them.
And they will try once again to create a scenario where something that is kind of mundane seems
conspiratorial.
And odds are large portions of their base will fall for it again.
It will work.
The question is to what degree.
A lot of the Republican base at this point has been sold on the idea that it's right
around the corner, just like everything else that they were promised by the GOP that is
still right around the corner.
I don't think the Republican party is going to have a hard time convincing their base
of it.
you're looking at it from a political standpoint, they might have been better
off just letting it go at that point. Once the hearings went as poorly as
they did, it probably would have been a smarter move just to never mention it
again. Going back and trying to depose people and release, you know, cherry-picked
quotes and all of that kind of stuff. While it might work for their base, it is
probably going to have a negative impact on independence and it's worth
remembering the Republican Party cannot win without the independence. So it
It certainly seems like they are about to make another error in a long string of errors.
My guess at this point is that Trump was upset that he was impeached and he is pushing the
Republican Party to impeach Biden for something. And as of yet, it doesn't appear that they found
anything. And given the fact that they're altering their tactics, my guess is that they're out of
routes to pursue and they're going to try to rehash the same stuff for like
the fourth time, but this time do it through quote sworn testimony, but it
will be out of context. I can almost guarantee it. That appears to be the
plan and I don't see it working but you probably need to be ready for it. Anyway
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}