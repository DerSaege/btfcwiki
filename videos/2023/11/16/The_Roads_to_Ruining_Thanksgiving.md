---
title: The Roads to Ruining Thanksgiving....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OspMIhmbn8Q) |
| Published | 2023/11/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing a tradition of discussing how to navigate difficult family dynamics during the holidays, particularly with relatives who have differing political views.
- Exploring how to handle being the only left-leaning person in a conservative family and navigating political discourse during family gatherings.
- Sharing experiences of being part of two leftist influencer communities with irreconcilable differences on reaching common goals.
- Offering advice on communication styles and overcoming differences in relationships, especially with shared values but conflicting communication methods.
- Addressing challenges in family dynamics surrounding disagreements on social and fiscal topics, particularly regarding policing and risk assessment.
- Providing guidance on engaging in difficult conversations with family members about trans rights and dealing with disagreements while maintaining respect.
- Suggesting inviting someone with differing opinions to participate in activities to foster understanding and bridge ideological gaps.

### Quotes

- "You can't be in a position where you're talking to all of them at once."
- "People are different. They have communication issues."
- "Invite them. Invite them to come out there."
- "You don't have to go, you know."
- "Sometimes you hear something so often you don't think to question it."

### Oneliner

Beau provides insights on navigating challenging family dynamics, ideological differences, and communication styles during the holidays and beyond.

### Audience

Families, activists, individuals in challenging relationships.

### On-the-ground actions from transcript

- Invite family members with differing opinions to participate in activities to foster understanding (suggested).
- Engage in difficult but respectful conversations about social topics and disagreements within the family (implied).
- Seek opportunities to bridge ideological gaps through shared activities and experiences (implied).

### Whats missing in summary

In-depth personal anecdotes and detailed responses from Beau that provide nuanced perspectives on navigating ideological differences, communication challenges, and family dynamics during the holidays.

### Tags

#FamilyDynamics #PoliticalDiscourse #CommunicationStyles #Activism #IdeologicalDifferences


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
So today, we are going to continue a tradition
that started over on the other channel.
We're gonna talk about how to ruin Thanksgiving.
It's not really about ruining Thanksgiving.
It's about learning how to talk with your relatives,
your relatives. Those relatives that are sometimes hard to talk to, who you often
have to be around during the holidays. It's a conversation that comes up pretty
much every year. There's always a bunch of questions about it, so it turned into
a thing. Aside from that, we often get told not to talk about certain things.
You know, certain topics are just not things that, you know, you discuss.
But when you follow the root issues back of most of the problems we have in this world,
they trace back to those things that we're told not to talk about.
You know, money, politics, religion, stuff like that.
So it started as a joke about, you know, nationalizing the mashed potatoes and, you know, making
a guillotine for the little penguin egg, which I found out that isn't a thing everywhere.
They're not actually penguin eggs, they're like hard boiled eggs with olives to make
them look anyway.
And that's where this started.
So we have a bunch of questions and we will go through them.
What do you do when you are the only left leaning person in your family?
This whole thing often gets framed as your one conservative uncle, but it's not always
that in your favor.
On Christmas someone commented, well there's only one person here not for Trump and the
whole room looked at me.
When you are the extreme relative at dinner, no matter how good your arguing skills are,
your options are get piled on or shut up.
I say this not because I need advice, I am firmly in the shut up and don't make a scene
camp but I want to point out that sometimes you can be the odd man out.
Yeah, that's me most times.
Generally speaking, I am the most left person in the room and most times I'm the most anti-authoritarian
as well.
I do have some friends that are more further down than I am, but not many.
I have found myself in this position before, and it's, I don't know that that's relatable.
How do you eat an elephant?
One bite at a time.
You can't be in a position where you're talking to all of them at once.
And it's not just about whether or not you win the argument, it's that they will reinforce
each other even if they're wrong. So it would need to be something where you can talk to
one person at a time and ideally you would talk to those that are youngest because they
have more time to make a difference.
I'm in two leftist influencer communities on the internet.
This is long.
If you examine the figures at the head of both of these communities at the destinations
of where their conclusions lead them, you'd probably find them agreeing on most things.
But the figures have irreconcilable differences in how they think that destination should
be reached.
community isn't one of them. As a result, their communities are not friendly with one
another, to put it very mildly. I don't like seeing them fight, it's very demoralizing
because it feels like this happens on the left all the time. And no one is interested
in collaborating and just says their own thing, which hurts the left. Both communities are
filled with smart, passionate, well-intentioned, and sociable people, but I feel as if I expressed
this to people in either community, or if anyone in said community knew I was in the
other one, they would try to psychoanalyze me, or more likely just try to beat me over
the head with a proverbial stick when it comes to this divide, like they wouldn't think
critically on it.
Being a person who enjoys both communities, the infighting makes me feel disillusioned.
But more than that, I feel as if eventually I'll be forced to pick one side, and it seems
like picking a side means hating the other.
And maybe this is starry-eyed, but I feel as if I don't have it in me to hate most
people, even my political enemies.
I get frustrated, disappointed, and angry with them very often.
But I don't like to hate people.
I turned off all of my notifications from these communities and just kind of want to
tune out from politics, even though that feels like just running away from it, which makes
me feel like a fence rider.
You're a whole bunch of very nice things about me that I really hope aren't true.
If that's true, we're in trouble, okay?
I know you don't have all the answers, but it's my slimmest hope that you can at least
provide me with just a thought that might help me find the answer that works for myself.
So my question is, is it impossible to be really active and make change in politics
without hating the people who you don't agree with, and how do you get people who hate each
other to work together?
Okay.
So what you're describing is incredibly common when you're talking about left online spaces.
It's a thing and I've seen it.
I have seen it with people who the person at, I guess the public figure, where the public
figures literally agree on everything, but they have a personal issue and it causes the
communities to fight.
This is why I will say things like streamer X or another YouTuber, and I am very careful
about who I name because it invariably leads to people who agree on 99.9% of things really
focusing on that tenth of a percent that they don't agree on.
I personally believe in a diversity of tactics.
I have brought up StreamerX, who I think at this point,
everybody knows who it is.
Like, if you're really into those communities,
I brought him up multiple times.
Because his approach is not one that I would use.
Some of the stuff he says, I'm just like, what are you doing?
But I've gotten messages from people who he got off of a bad road.
And then they came to me and I've had a few that like, you know, I watched some of your
videos and didn't really like them, but I watched this person and it resonated.
As far as your questions, is it impossible to be really active and make change in politics
without hitting the people who you don't agree with?
Now, especially when they're riding the bus in the same direction as you, that's a personality
And I will tell you that when you actually step offline and you're out doing stuff in
like the real world, that stuff never comes up.
I mean, it is incredibly rare that any of those personality conflicts that exist online,
they make it in the real world.
Because when you're in the real world, you realize it actually matters what you're doing,
What you are actively doing should trump any of the online stuff.
How do you get people who hate each other to work together?
Normally it takes a unifying cause.
I think I've told this story.
After Michael, after Hurricane Michael, my little circle here.
There's another group, and they've actually come to blows over things before, but in the
aftermath of Michael, none of that mattered.
When you have serious disagreements, let's just call it, it normally takes a serious
event to pull people together.
You don't need to pick a side, you don't need to let the personalities, the public
figures, you don't need to let their personal arguments impact what you do.
And generally speaking, those people who are going to beat you over the head with a proverbial
stick because you like somebody they don't, most times you're never gonna see them out
there actually doing anything in the real world anyway.
It's kind of self-selecting that way.
If they care that much about the online personalities, that's where they're focusing their attention.
And we've talked about it before, that's not even an insult.
The online stuff is important.
But generally, once you step offline, most of those people
aren't even there.
So that had nothing to do with Thanksgiving.
But it did have to do with the communication.
So we'll roll with that.
OK.
Having difficulties with my partner.
Going to skip the part where I share specifics
and pretend I haven't listened to what you already said.
Yes, we share values, but on occasion we have extreme difficulty communicating with each
other and understanding exactly what the other means and where they're coming from.
When we do manage to bridge the gap, we always realize that our values are the same and our
love is reinforced, but during the miscommunication it can often be extremely unpleasant.
Yeah. I'm not writing to ask if I should be with her or not. I've already made that
decision and I'm committed to it. I love her, not because she is always my favorite
person, but because I am mostly my favorite version of me when I have her
radiant presence in my life. Wow. You made me want to take my pills and
I'm not writing to ask you if this is the woman I love, because I already know that
she is.
I'm writing to ask you how two people with shared values and a lot of passion but very
conflicting communication styles can learn how to overcome their innate differences and
learn how to accommodate each other's quirks.
P.S. do you and Miss Bo have communication issues?
them married for 10 years, more than. So yeah, of course. Of course. And it's the same thing.
We have different communication styles, and we are both very passionate and stubborn and
all of that stuff. But if you know it is different communication styles, that's actually a huge
help if you can identify what it is. As an example, I am very concise. Like I say exactly
what I mean. My wife does not. And I've joked about it before. You can ask her what time
it is, she'll tell you how to build a clock. Whereas I am very much who, what, when, where,
in a single sentence. The issue arises for us when, let's say, well, let's start
with her. She is telling me a whole bunch of information and then she gets
interrupted. You know, we have half a dozen kids and it's like Noah's ark
here. So there are interruptions. She gets interrupted and I think she's done. So
because she kind of buries the lead, I have to guess at what was important. What
she was actually trying to say because she does provide so much context. So
there are times when she gets interrupted I think she's finished we go
on about our days it doesn't come up again and she was telling me about all
of this stuff going on on her way to the vet okay and one of them was there's
something wrong with a car okay just example so I go to do something with the
But the point of the story was she never got to go to the vet and the dogs need to go to the vet tomorrow.
That is something that happens with us a lot.
Another one, I know everybody watching this, y'all all know that I'm perfect in every way,
but sometimes I get distracted when she's providing a bunch of information and I miss an important piece.
That leads to a communication error.
For me, it's normally, I have a very, I'm very flat when I'm talking, I'm just like,
you know, this is, this is what's going on.
And sometimes something can be important, but I guess the tone of my voice doesn't
convey that.
And so, not that she misses it, but that she doesn't prioritize it.
Another one could be, she is doing something and I don't realize she's doing something,
and what I'm saying is concise, so it's said, but she didn't hear any of it.
Like she has no idea that it even happened.
If you can identify those issues, you can work to correct them.
It'll be more than a decade and you'll still be working on it.
But at least if you can figure out where the communication errors occur, it's easier.
So that would be my advice here.
But it doesn't go away.
People are different.
They have communication issues.
And yeah, it's extremely unpleasant to use your term at times when it happens.
But if you are, you know, this certainly sounds like the person you're wanting to spend the
rest of your life with, I mean, understand, it's not always going to be roses.
So I mean, I would identify the communication styles and where they conflict, and that will
help a lot but it's not going to alleviate it completely. But I mean just
the way this is phrased, learning how to accommodate each other's quirks, that's
another thing. You're not gonna change each other, not to any significant
Also, not about Thanksgiving.
Thank you for taking these questions.
I've been arguing with my family about trans rights lately.
Every time the topic comes up, I'm not even bringing it up.
And I disagree.
My mother dismisses my arguments by saying that she's of a different generation.
And these are the things that they believe.
She says I should let her have her beliefs and be respectful of her as my mother by not
arguing with her.
Lately my argument has been you taught me morals and ethics and it's difficult for
me to tolerate you not engaging in what Jesus taught us about how to treat people.
With my sister, she just takes whatever I say and says that doesn't sound right to
her, and she doesn't quite believe it. Cognitive dissonance. But she won't Google anything.
They just want to shut me up. I feel like my options are to be quiet and let them be
horrible, or to just not go. Is there another option? My mother raised us Catholic, but
now she's even ignoring Bible verses supporting trans people and treating everyone well. I
I have trans friends and my family intentionally misgenders someone in the media and it boils
my blood.
I would appreciate any advice you have, even if that advice is just to find new people
to have the holidays with."
I mean, that's important.
That's important to recognize.
You don't have to go, you know.
I think most people want to make the effort, but if it is that uncomfortable, you know,
that's always an option.
But save that for the last option, okay.
With my sister, she doesn't quite believe it, but she won't Google anything.
Google it for her first.
Before you have the conversation, have it brought up on your phone in a tab.
that ready. I was just looking at that. I didn't think it sounded right to me
either. Here, why don't you read it yourself? That has worked in the past for me.
My mother raised as Catholic. See, now I'm wondering when this came in. If your mother is
Catholic, you might want to look at some of the stuff the Pope has said recently
and make sure that she sees it because I mean you know infallibility and
everything that goes a long way so there there have been a lot of shifts within
the Catholic Church on on this exact topic it might be worth bringing them up
the other thing to do is you say she's of a different generation but there's
There's nothing in here that lets me know which generation that is.
If you can think of something that that generation really believed was true, and it wasn't,
you might want to keep that in your back pocket as well.
Family intentionally misgenders someone in the media.
Boils my blood, yeah.
And it's stuff like this where I wish I could ask questions like do they do they misgender your friends?
Or is it just this person in the media? Are they you know, is it combative?
I think there's a lot of questions I have about this one.
I think you're on the right track though.
You seem to be using their base beliefs to frame your stuff with.
which, I mean, that's smart. I would, I would definitely have this stuff already
drawn up so she doesn't have to Google anything and I would definitely look
into some of the things the Pope has said recently. Okay, my grandfather was
involved okay so I'm not gonna read the intro to this because it doesn't matter
how well this is disguised that this is super specific and that's it will
definitely give away who you are. Okay, so the question, how do I go to dinner
without addressing this and ruining things for the whole family? So generally
speaking, this is about a piece of family history and how it ties into current
events, and let's just say that the family history was on the wrong side of what is pretty
widely accepted that that was the wrong side of things.
So there's a person in the family that is kind of falling down that same, still looking
at the wrong side of things, and almost idolizing the person who was on the wrong side in their
family.
How do you get to dinner without addressing this?
Yeah, I don't know that you can, man, I'm going to be honest, that's a hard one.
Is it possible to address beforehand?
That might be a conversation to have with this person over the phone prior to anything
occurring.
Just understand the risk of that is that it blows up then.
Yeah, I'll send you an email about this because there's just...
There's a lot there.
Okay.
I have an in-law who's a part of a number of different minority groups, so clearly none
of our straight cis white family can have any opinions on any social topic, and several
of us make more money than them, so clearly disagreements on fiscal topics are us protecting
our privilege.
They are not actually a leftist wanting complete social rewrite, but more in line with Barnier
ASC.
Our family is generally pretty progressive for Americans, and to borrow your metaphor,
we are on the same bus going to the same place.
However, this person has to constantly critique everyone else's opinion if it varies in the
slightest from their own, and has to have the last word on everything.
They fit the conservative meme of an uninformed SJW to a D.
They have an ironclad opinion on every situation, even when they have no information, context,
or experience with the situation, and obviously have a simple solution that doesn't actually
work in the real world.
The most frustrating part is that, as far as any of us know, they don't actually do
anything to further the cause other than purity tests to others.
Much of our family spends our time helping the kind of people they claim to care about
– volunteering, in donations and through our work.
This person is seemingly too high on their horse to see the muddy nuance that comes from
helping people on the ground.
Do you have any advice helping to get them to realize that we aren't the enemy and
are actually on their team, or at the very least shut down some of the self-righteous
purity testing.
Oh yeah, I got an easy one for that.
Invite them.
Invite them to come.
That's the easy one.
Invite them to come out there.
This is a lot like the question about the two online communities.
And if you can get them actually out doing something, that black and white world, it
becomes super gray, super quick.
So if there is volunteering going on, see if they want to go.
That often helps.
This does sound, based on this, it does sound like this may be somebody in one of those
groups.
Aside from groups that are truly ideological in nature and the goal is to spread that ideology,
and there are a few of those out there.
The purity testing stuff, that doesn't really happen, you know?
So I would invite them.
I would invite them, see if they want to come out, and it will probably change their view.
It will probably be something that is, that could be transformative for them, and I mean
good.
If you have somebody that has these views, and the hang up is that they're not active,
and you can trace a lot of their views that are annoying to the fact that they're not
active, the simple answer is get them active.
But at the same time, you may run into somebody who likes the theoretical, because it is neater.
It's a whole lot cleaner when it's not actually in the real world.
And it's just a discussion, and it doesn't have to be implemented.
I've actually got two questions I'd like to ask, one significantly more important than
the other, but I'll allow you to decide which is which.
First off, I've reached a bit of an impasse with my mom.
She claims to be independent and center, and I'd probably put her around libertarian, but
It's a certain state.
So I can see how that would fill center there.
Yeah, that tracks.
I can't keep my mouth shut, so we'll go rounds on politics.
Recently, we had a debate about cops
and over-policing excessive force.
She believes the whole A Few Bad Apples shtick.
And while clearly, at this point, untrue,
I'm sure with a bit of digging,
I can find examples such as Memphis to show this is a far bigger problem than just a handful
of bad cops.
The real sticking point, though, is we seem to have a fundamental disagreement on risk.
She seems to believe that being quicker with force is justified because cops have a risky
job and should be able to end their shift intact.
She says that if people just complied, they would be fine, again, blatantly false at this
point, and a thing I can probably get around after doing some homework, and that any reluctance
towards de-escalation or non-lethal measures is warranted because the cops want to live.
For my part, I feel this twofold.
First, the cops would be in less danger if people didn't feel scared around them.
and to me more importantly if someone's life is to be in danger I feel it should
be the person who signed up for the job and I say this is a vet with a husband
who is still active duty any risk due to uncertainty should lie on the side of
the person with the training. Is there a good way to approach this to get her to
understand? Is there some sort of refraining you might suggest I try? Yes,
Absolutely. Sometimes you hear something so often you don't think to question it
and that this is definitely the case here. I would start by asking her if she
felt that, let's say a logger, if a logger failed to observe best practices and cut
down a tree that had a tree house in it with two kids in it and those kids didn't make
it should they be held accountable.
And then when she answers, explain that that's a riskier job.
We hear it all the time.
Cops have a risky job.
Fishermen, loggers, roofers, those are always in the top 10 when it comes to
actually, you know, the actual most dangerous jobs. Cops almost never are. I
can't think of a year in which they were. Loggers and fishermen are always, always
up at the top. It's the perception of risk, not the reality of it. When you look
at the actual statistics on it, they are not in the top 10. So I would start with
that because most of this argument stems from the fact that they have this super
risky job. And that's just a presumed notion, because it's
been said so often. But the the numbers don't back that up. It
really isn't. And you can get that from the Bureau of Labor
Statistics. I'm sure you could probably just type in top 10
most dangerous jobs 2023. But you could get those from the
Bureau of Labor Statistics or you could go to another way to do it would be to
go to the Officer Down Memorial page which actually tracks everybody who's
lost. One of the other things to note is that many of those who were lost were
not lost like in the way you think. Like no doubt every year there are cops lost
to hostile action or whatever term you want to use but a lot of those who are
lost and who are counted they're not. It is something else that that you
probably wouldn't think of as in the line of duty but they were at work so
it counts. That's where I would start and then you can go to the other stuff
that you have like kind of suggested here but I would start with addressing
that because the risk the perceived risk that the public sees there it is not
backed up by numbers so I mean that's that's where I would go with it. Okay
and for my second question one which may some may argue is more important
perhaps even dire. It is well established now that the amount of tees you have is in
the triple digits. We also know that you always have a relevant tee for the video, plus you
put out four plus a day without fail. How are these shirts organized? Is it just a closet
hanging? Or you just flip through them till one strikes your fancy? Do you have them folded in
drawers and the room looks like a teenager's as you rifle through them? Sorted by color?
Have a folder perhaps with them by theme? You can go to section A5 and grab the metal band shirt
or t-15 if it's a science fiction one that people need to know. I have Z-Racks.
You've seen them in clothing stores. They're like the willy racks, the racks with wheels on them,
and that's what they're hanging on. In fact, they're like right behind where the camera normally
is to absorb sound. So they have two purposes. They hang there and they keep the shirts accessible
and they cut down on the echo. As far as how they are up there, it's pretty haphazard.
Like the ones further back, I have three of those racks. The one that is further back
is like the ones I don't use very often.
But yeah, that's, didn't see that coming,
I'm going to be honest.
But yeah, that's how they're up there.
OK, and that looks like all of the questions.
So I hope that that helps with these.
And maybe you can get some ideas for any issues
that you might have with your family over the holidays
or with conversations that you want to have
or with your significant other or your online space.
OK.
All right, so a little bit more context, a little bit
more information.
And having the right information will
all the difference. Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}