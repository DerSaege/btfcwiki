---
title: Let's talk about Nevada and another entanglement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WDCLu_OeF8k) |
| Published | 2023/11/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Confirmed entanglement involving fake electors now includes Nevada along with Georgia, Michigan, and Arizona.
- The attorney general has not provided details about the investigation, surprising many who believed he wouldn't pursue it.
- The AG mentioned that current state statutes didn't directly address the conduct in question.
- Possible reasons for the investigation's advancement: another state's example or discovering fitting conduct within existing statutes.
- Allegations involve six individuals falsely claiming to be Nevada electors and pledging votes to Trump, including the GOP chair.
- Uncertainty surrounds whether the investigation will result in indictments or lead to a new law.
- The investigation, ongoing for some time, is now being talked about due to information shared by investigators.
- The comparison is drawn between the scenario in Nevada and previous cases in Georgia and Michigan.
- Speculation is rife as limited concrete information is available beyond investigators asking questions.
- Beau advises caution in forming strong beliefs or theories until more details emerge.


### Quotes

- "Whether or not this expands into a Georgia-style case, or it's more like Michigan, or maybe it doesn't go anywhere."
- "It's good to know that the signs actually did mean what everybody thought they meant."
- "I want to believe. Anyway, it's just a..."
- "With that out, y'all have a good day."


### Oneliner

Beau confirms Nevada's involvement in fake electors investigation, with uncertainties looming over potential outcomes and the need for caution amid speculation.


### Audience

Political analysts, activists


### On-the-ground actions from transcript

- Contact local political organizations for updates on the investigation (suggested)
- Stay informed about developments in the case and share accurate information within your community (implied)


### Whats missing in summary

More details about the potential impact of the investigation and how it may influence future electoral processes.


### Tags

#FakeElectors #Investigation #Nevada #AG #PoliticalIntegrity


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
yet another entanglement that is being looked into.
And now there is confirmation of that occurring.
A while back, I said there were signs
that there were more things on the way.
That's now confirmed.
So on top of Georgia, Michigan, and Arizona,
we can now add Nevada as a state
that is looking into the fake electors scheme.
The attorney general has kind of declined to comment
on what's occurring at the moment.
And this whole thing kind of surprised people
because a lot of people believed that he said he wouldn't pursue it.
This is what he actually said.
As you all know, I have been silent on Nevada's fake electors, except to say that the matter
was on our radar.
With it on our radar, we ascertained that current state statutes did not directly address
the conduct in question, to the dismay of some, and I'm sure to the delight of others."
So, what could have changed? A, another state could have provided a road map.
Or B, they discovered other conduct that fit within the statutes that
that this state currently has.
It's worth noting that the AG actually wants like a new law.
So in this case, I want to say there are six people
who are alleged to have falsely claimed
that they were the electors for the state of Nevada
and pledged their votes to Trump.
One of them, if I'm not mistaken,
the GOP chair there. So that's the news that we have. What we don't know is
whether or not this investigation is going to go anywhere. We know that there
is an investigation or a probe as it has been described. We don't know whether or
not that will lead to indictments. We don't even know if they're pursuing it
in that manner. It could be an investigation to better inform them to create a new statute.
There's a lot of things that are unknown at the moment, but what is known is that there
is an active investigation into this whole scenario in the state.
And it's been going on for quite some time and it's just now coming to the surface because
some of the investigators talked to people who talked about it.
So that's what's going on.
Whether or not this expands into a Georgia-style case, or it's more like Michigan, or maybe
it doesn't go anywhere.
We don't know that yet.
And there's a whole lot of people already kind of gearing up and making predictions.
The amount of information that is out there is incredibly sparse.
We know that investigators linked to the Attorney General's office are asking questions.
That's what we actually know.
Everything beyond that at time of filming is coming from speculation.
So it's good to know that the signs actually did mean what everybody thought they meant,
but we don't even really know the purpose of the investigation yet.
I would wait before you start before you start putting too much stock in any
particular theory even if it is one that I want to believe. Anyway it's just a
With that out, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}