---
title: Let's talk about Biden's US-Chinese agreements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xI2SyMGsWpw) |
| Published | 2023/11/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of US-China meeting and agreements reached.
- Hotlines agreement reinstated and improved, addressing Gen Z concerns.
- Agreement to curtail shipment of precursor chemicals to stop Fentanyl flow.
- Previous president's approach to Fentanyl issue criticized.
- Candid open communication between US and China emphasized.
- Chinese perspective on world affairs and economic goals discussed.
- China's focus on economic power over political power stressed.
- Hope for beneficial coexistence between US and China without conflict.
- Importance of hotline to prevent military conflict through open communication.
- Overall, the meeting and agreements seen as a foreign policy win.

### Quotes

- "China is saying, you can have all of that political power. Don't mess with our money."
- "Competition, but not conflict, if that makes sense."
- "It helps avoid conflict during posturing."
- "As long as there's open communication, the likelihood of something spiraling out of control is greatly lessened."
- "Even if nothing else is accomplished, this is a success."

### Oneliner

Beau gives an overview of the US-China meeting, agreements on hotlines and Fentanyl, stressing the importance of open communication for a beneficial coexistence.

### Audience

Foreign policy enthusiasts

### On-the-ground actions from transcript

- Contact local representatives to advocate for peaceful relations with China (implied).
- Support initiatives promoting open communication and diplomacy in foreign relations (implied).

### Whats missing in summary

Insights on the potential long-term impacts and challenges of US-China relations.

### Tags

#US-China relations #Foreign policy #Diplomacy #Open communication #Fentanyl #Hotlines


## Transcript
Well, howdy there internet people, it's Bill again.
So today we are going to talk about US-Chinese relations.
We are going to talk about the meeting that occurred,
the agreements that were reached,
and where things go from here.
Sadly, there's no way to talk about this meeting
without talking about the former president as well.
because both of the agreements tie back to him, one in a funny way and one in a, well, they were cleaning up his mess
kind of way.  Okay, so the meeting was between Biden and Xi Jinping.
The two agreements that were reached, we'll start with the one that I know Gen Z has been concerned about,
The hotlines, the red phone type of thing, those are back. The agreement's in place now.
That was a foregone conclusion ever since the whole float around and find out balloon thing.
But those, the agreement's in place. They will be reinstituted and improved.
So, it's all good news there.
One thing that is worth noting is that normal presidents want their military to talk to other nuclear powers to,
you know, lessen the risk of nuclear war.
It is worth noting that the previous president kind of referred to this as treason because, well, foreign policy wasn't
exactly his strong suit.  Okay, the other thing had to do with Fentanyl. If you remember, back in 2019, Trump said
he was going to stop it. He was going to stop it, and to stop it, he was going to put some
tariffs on China. The tariffs went into place, and, you know, all of us paid more for things
contributing to inflation and all of that stuff, but the flow of the precursor
chemicals didn't stop, and a lot of that had to do with how Mr.
Art of the Deal approached the situation.
Biden has reached an agreement.
The Chinese government will begin curtailing the shipment of
precursor chemicals. That's the one that's probably going to get the most
coverage, but it's probably not the most important. It's very visible in the
United States, so people are going to talk about it. And then there's
the fact that prior, just prior to this meeting, Chinese state media started
making fun of Trump and his foreign policy initiatives. They said that
Trump's actions could be compared to, quote, lifting a stone only to drop
it on one's own foot. We have a saying like that in the U.S. as well. So that
that's been resolved as well. One of the other things that came out of this was
just very candid open conversation, which is important. It's worth
remembering that if you live in a country, if you're an American, you view
the rest of the world through American eyes. And if you're Chinese, you view the
world through Chinese eyes. And the U.S. and China are very, very, very different.
They don't look at the world the same way. They don't look at their place in
the world the same way. Recently I said that, you know, China is very happy with
its place in the world. And there were a lot of questions about that. China
doesn't want to be a country that is involved in every other country on the
planet. They want to make money. That's what they want to do. They want to get
their power through power coupons, not through military force and being a
master of the universe the way the United States does. They just have a
different view of things and that it goes to the type of country that it is.
You know, China is not a nation of immigrants. In the United States when
something happens in another country, there are citizens of the US that have a vested
interest in it.
Not so much the case there.
And the reason I'm bringing this up is because this is something the Chinese president said.
China has no plans to surpass or unseat the United States, and the United States should
not scheme to suppress or contain China.
The short version here is China is saying, you can have all of that political power.
Don't mess with our money.
That's really the conversation that's occurring.
Don't mess with the economic power.
And that's not, that is not an unreasonable situation to develop.
really not in the past with the first Cold War. Both major groups, they wanted
to be masters of the universe. It was ideological more so. This is one that
could eventually, if the cards are played right, this is something that could turn
into a very beneficial coexistence. Competition, but not conflict, if that makes sense.
And hopefully more conversations like this could put us on the track to get there. The
other thing that's important about the hotline is that it allows both countries to posture
militarily without, you know, running the risk of actually starting a war, which is
cool in my book. As an example, let's say the United States, its fleet is going to
sell by Country Y, and Country Y looks to China as their, their protection, their,
they're in China's camp. And country Y calls out Beijing. They're like, hey,
this is making us really nervous. So China picks up the hotline, calls the
United States. It's like, hey, we're gonna do some fly-bys on that fleet of yours.
And this is where the US does the foreign policy equivalent of, dude, come on. And
the Chinese are like, no, we're gonna do that. And the US is finally like, okay,
fine, we'll turn the CRAMs off. And that's, it helps avoid conflict during
posturing. Those types of things happen all the time and they're not supposed to
lead to conflict. It's a, it's shuffling your chips, shifting your cards back and
forth. It's not actually supposed to be something that could lead to a war. And
And as long as there's open communication, the likelihood of something like that spiraling
out of control is greatly lessened.
So overall, just from what we know so far, this is a win.
This is a foreign policy win.
All of this is good news.
Even if nothing else is accomplished, this is a success.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}