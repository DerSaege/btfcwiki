---
title: Let's talk about Trump and revoking release in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=H4SJjlDbWGM) |
| Published | 2023/11/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The DA's office in Georgia filed a motion to revoke the bond of Harrison Floyd for making social media posts tagging potential witnesses, co-defendants, which the prosecution views as witness intimidation and a violation of release conditions.
- Harrison Floyd's actions have led to renewed threats against Ruby Freeman, prompting the prosecution to accuse him of obstructing justice.
- Floyd has been accused of numerous intentional violations of the court-ordered release conditions since his custody release.
- There's a question of why Floyd specifically is being targeted for this behavior, with potential reasons including setting a precedent due to his unprecedented behavior or using confinement as a "taster" to reconsider prosecution offers.
- Prosecutors in Georgia often have bond revoked when they request it, although it's not a certainty.

### Quotes

- "An effort to intimidate co-defendants and witnesses to communicate directly and indirectly with co-defendants and witnesses and to otherwise obstruct the administration of justice."
- "Generally speaking, when the prosecution asks for bond to be revoked, most times in Georgia it tends to be revoked."

### Oneliner

The DA's motion to revoke Harrison Floyd's bond in Georgia sheds light on potential consequences for those involved in Trump's entanglements, hinting at setting a precedent for unprecedented behavior.

### Audience

Legal observers

### On-the-ground actions from transcript

- Monitor the developments in legal cases involving potential witness intimidation and obstruction of justice (implied).

### Whats missing in summary

Insights into the broader legal implications and the significance of setting precedents in high-profile cases. 

### Tags

#Georgia #WitnessIntimidation #ObstructionOfJustice #LegalSystem #Prosecution


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
yet another development out of Georgia.
I feel like this one is going to be surprising
to most people, definitely unexpected.
And it's important and it may be foreshadowing
of things to come for other people
involved in Trump's entanglement there in Georgia.
Okay, so the big question, what happened, right?
The DA's office there, they have filed a motion
that is asking to revoke the bond of Harrison Floyd.
So the general tone of it is that Floyd
Floyd has made social media posts on Twitter to a pretty sizable following, I
want to say like 25,000, and in some of those posts he tagged people that could
be witnesses or co-defendants or something like that. And the prosecution
is basically saying, hey, this is pretty much witness intimidation and it violates
the conditions of his release, therefore we want that release terminated. One of
of the things that was brought up was that Ruby Freeman is facing renewed threats because
of it.
The motion says that Floyd engaged in an effort to, quote, intimidate co-defendants and witnesses
to communicate directly and indirectly with co-defendants and witnesses and to otherwise
obstruct the administration of justice.
Since his release from custody, the defendant has engaged in numerous intentional and flagrant
violations of the conditions of release ordered by the court.
Okay, so there's an obvious question floating around right now, right?
Why this person?
Seems like somebody else has like a much higher profile when it comes to doing things like
this.
There could be two things at play.
is something called a taster, which I don't know that that's common in Georgia, but it's a situation
where prosecutors decide, oh, okay, well, let's let you see what confinement is really like,
and then maybe you can reconsider the offer we have extended to you. That is one thing that might
be at play. I don't know. I mean it is something that happens it's you know if
this was in New York I would be like yeah that's what's going on but it's
not a it's not a widespread practice in the South. The other thing that might be
at play here is setting the precedent for somebody who has often been described
as unprecedented. If there is a pattern and it is demonstrated that the court
will not tolerate this kind of behavior and somebody engages in that behavior
after that point, it might be easier to stop extending certain courtesies that
have been extended because of the person's previous position.
That might also be applied.
Generally speaking, when the prosecution asks for bond to be revoked, most times in Georgia
it tends to be revoked.
That doesn't mean that it's a certainty, but that's generally what happens.
So we will have to wait and see, but I feel like this is a shift in tone.
A shift in tone that may be giving us a hint at what's to come later for other people involved
in the case.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}