---
title: Let's talk about Meadows and Eastman having bad days....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0ZIxmV5H2vA) |
| Published | 2023/11/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Meadows facing a lawsuit from the company that published his book.
- The company claims Meadows breached warranties about the truthfulness of the book.
- The company wants $350,000 back for an advance, $600,000 for expenses, and more.
- Meadows warned Trump against claiming election fraud but allegedly said something else under oath.
- Eastman facing disciplinary action for breaching professional ethics.
- Eastman's license might be jeopardized, impacting his decision on a plea deal.
- The organization involved in the suit is called States United.
- The senior VP of legal at States United believes Eastman had no legal basis for his actions.
- These legal actions stem from fallout due to Trump's attempts to cling to power post-election.

### Quotes

- "Y'all have a good day."
- "That's a pretty hefty sum."
- "It's just a thought."

### Oneliner

Meadows faces a lawsuit over breached book warranties, while Eastman's disciplinary actions may impact his plea deal decision amid fallout from Trump's post-election power struggle.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Stay informed on legal proceedings and ethical breaches (suggested)
- Advocate for accountability in legal and ethical matters (exemplified)

### Whats missing in summary

Insights on the potential ripple effects of these legal cases on future political landscapes.

### Tags

#LegalProceedings #Ethics #Trump #Accountability #Fallout


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Meadows and Eastman
and the day they're having.
Neither one of them is really having a good day.
And one, I mean, we kind of knew was coming.
At least I think most people suspected.
The other was kind of surprising.
So we'll start with that.
We'll start with Meadows and the news
received today. According to reporting, the company that published his book is
going to sue him. So Meadows put out a book and the book very much kept to
the line, you know, as far as the kind of stuff that Trump would want out there.
there. Meadows, according to what the company is saying, made warranties,
guarantees about the truthfulness of the book. Meadows breached those warranties
causing ASP to suffer significant monetary and reputational damage when
the media widely reported, and then it goes on. The important part here is that
he warned Trump against claiming that election fraud corrupted the electoral
votes cast in the 2020 presidential election and that neither he nor former
President Trump actually believed such claims. So basically the company is
saying according to the reporting you told us one thing and told the grand
jury something else. One of those is under oath. Let's see and then as far as
what the company is asking for, the numbers are kind of eye-popping here, they
want 350 grand back for an advance they gave to Meadows. An additional 600,000
in like expenses related to getting the book ready to publish. And then if I'm
reading this right, there's a million dollars for reputational damage suffered
by the company, and a million dollars because of the expected loss of profits, I mean that's
a pretty big bill.
That is a pretty big bill.
That's something that was probably going to weigh pretty heavily on Meadows.
Now I should note, I have seen it reported that it's a million dollars total for the
expected loss of profit and reputational damage.
That's not the way I read it, but we'll wait and see.
Either way, that's a pretty hefty sum with all of Meadow's other legal entanglements.
now needing a lawyer for this, it's got to be weighing, you know?
Now as far as Eastman, Eastman's been going through a disciplinary thing as far as whether
or not he violated the ethics that attorneys are supposed to have.
The preliminary finding indicates that he breached professional ethics.
There's a lot of people reading into this and how it might impact his decision to take
a plea deal.
I'm not certain that I would make that jump.
I get what the analysts are saying.
They're saying that one of the reasons you wouldn't want to take a plea deal or go that
route is because it might jeopardize your license. Okay, but at the same time
there's still the whole like gel aspect. I don't see Eastman as somebody
who... he doesn't seem to have a public record of thinking things through the
way other people would. I don't know that you can make a lot of inferences off of
off of the possibility of him losing his license which I would note has not
actually occurred yet but the assumption that a lot of people are operating under
is that that's going to happen and then it will impact his thought process on
a plea to land Georgia I mean it might but that's a lot of ifs I don't know
I would go that far yet. The organization that brought the suit is called States United,
and the senior VP of legal over there, they had this to say,
As we reach the end of an ethics trial that has spanned from June into November,
it's never been more clear that John Eastman had no legal or factual basis for his plans to help
Donald Trump steal the 2020 presidential election. Today's preliminary finding of culpability
marks a major milestone in the state bar's pursuit of accountability.
I mean, yeah, it does, but I don't know that you can jump from what's going on with his license
to a plea deal. I think most people would say, okay, well, I've already lost my license,
I don't want to lose my freedom as well but he may look at it as I've already
lost my license maybe if I beat the charges in court you know it will change
things or maybe I can get a book deal. So I don't know that I would listen a
whole lot to that analysis unless it's somebody that's actually like really
close to Eastman and really understands how he thinks.
Because I can see that swaying people either way.
But these are more steps that have been taken in what has just become a long national nightmare
when it comes to the fallout from Trump's attempts to cling to power after he lost.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}