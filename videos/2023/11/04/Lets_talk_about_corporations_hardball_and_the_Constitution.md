---
title: Let's talk about corporations, hardball, and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=C3iqLFRIg2Y) |
| Published | 2023/11/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing questions about Senator Hawley's bill and its constitutionality.
- Explaining the need for a constitutional amendment or different Supreme Court judges for change.
- Critiquing the notion of "activist judges" in the context of Citizens United.
- Challenging the idea of basing arguments on what the founders intended regarding corporate personhood.
- Advocating against viewing corporations as people due to historical context.
- Suggesting that supporting Hawley's bill could be a strategic political move for Democrats.
- Emphasizing the importance of continuing the money in politics debate through supporting the bill.
- Speculating on the potential outcomes and political strategies related to the bill.
- Noting the Democratic Party's historical approach to playing politics.
- Acknowledging the potential repercussions of supporting Hawley's bill on his re-election.
- Commenting on the unlikely scenario of Democrats playing hardball politics.
- Sharing a personal perspective on how he might handle the situation differently.

### Quotes

- "I'm fairly certain the founders would not have viewed corporations as people when they didn't view all people as people."
- "You carry that forward. It will almost certainly be defeated. It won't stand when it gets to the courts."
- "Democratic Party does not really do hardball well."
- "I just don't see Schumer doing that."
- "I'm fairly certain the founders wouldn't view corporations as people when they didn't view all people as people."

### Oneliner

Beau breaks down the constitutionality of Senator Hawley's bill, questions activist judges, and strategizes on Democratic Party moves regarding money in politics.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Rally support for strategic political moves within your community (implied)
- Continue advocating for critical political discourse and actions (implied)

### Whats missing in summary

Insight into Beau's unique perspective and analysis

### Tags

#SenatorHawley #Constitutionality #DemocraticParty #MoneyInPolitics #ActivistJudges


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about a couple of questions
that came in about Senator Hawley's bill,
and the constitutionality of it,
as well as whether or not the Democrats
are going to make the right political play.
One of those questions is just gonna get
a real simple answer, and then the other one
it's worth diving into.
Okay, so the first one was, you know,
you said it wouldn't change.
The Supreme Court's view on this wouldn't change.
You would need a constitutional amendment
or to get other judges on the Supreme Court.
That sounds like you're calling for activist judges.
How about we just do what the founders intended?
Okay, well, the assumption there is that it wasn't activist judges that enacted Citizens
United and took corporate personhood to that extreme and all of that stuff to begin with.
That's the assumption you're operating under.
I would also caution against basing your argument in favor of corporate personhood based on
what the founders believed. Because I would like to remind you that I'm fairly
certain the founders would not have viewed corporations as people when they
didn't view all people as people. That doesn't even make sense. The
corporate personhood thing is based on a legal mechanism that was taken to an
extreme to benefit the wealthy. That's how that happened, okay? The other question
was, hey, wouldn't it make more sense for the Democratic Party to support Holly's
bill rather than defeat it simply because it came from a Republican? If I'm
Schumer, okay, and I'm in the Senate and this is me, yeah, I would gather all
all of the Democrats around and be like,
I am the Senate, everybody vote for this.
I don't care if you actually oppose it, vote for this.
Because the Republicans have indicated
they're gonna vote against it.
So what you end up doing is carrying that message
about money in politics.
You carry that forward.
It will almost certainly be defeated.
It won't stand when it gets to the courts.
But you help continue that conversation.
That's the smart move if you actually want to do that,
if that is one of your goals.
Politically, I would suggest that Holly
is one of those people that is kind of a thorn
in the Democratic Party side.
Now, if he put up a bill that all Republicans voted against
and a whole bunch of Democrats voted for
and it passed, the attack ads from other Republicans
his primary write themselves. Holly all of a sudden becomes a rhino. He crossed the aisle.
He worked with the Democrats. I would start calling him woke. Yes, it would make sense
if the Democratic Party was going to play hardball.
Democratic Party does not really do hardball well. It's not something that they're
they're really well known for.
I agree, that might be part of the problem,
but as it stands, the odds are the Democratic Party
will oppose this just based on the fact that currently
it is unconstitutional based on the Supreme Court
and how they look at things.
And because they don't want to give Holley a win,
win. I would suggest that giving him a win on this bill would maybe lead to him
getting a loss when it comes to re-election and I think that would be
more important but that's it's just not how the Democratic Party plays politics
sadly but no I don't think that's a bad idea. If it again if it was me yeah I
I would take it to an extreme.
I would vote for it, pass it, and then talk
about what a great woke bill Holly had written
and how MAGA is coming around and embracing
the woke philosophy.
And I would make a big thing about it.
But I just don't see Schumer doing that.
So anyway, it's just a thought.
God, have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}