---
title: Let's talk about China, the US, and an assurance....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KyAGchfPHDU) |
| Published | 2023/11/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- China made an assurance to the U.S. that they had no plans to invade Taiwan, but elements within the Chinese government are walking it back.
- The commentary suggesting imminent invasion by China doesn't make sense since if they truly planned to invade Taiwan, they wouldn't correct the assurance.
- China's intention might be to wait for a reevaluation of the U.S. position on Taiwan, anticipating a shift in priorities by the U.S.
- Chinese military scenarios show that while the West can defeat China, it's costly, and China can take the island but at a high cost as well.
- China's strategy could involve waiting for the U.S. to reconsider the value of Taiwan and the costs associated with any conflict.
- Mid-level diplomats correcting statements from heads of state is common and not necessarily indicative of imminent action.
- Countries prioritize their interests over friendships, and China may be waiting for a U.S. reevaluation of the situation regarding Taiwan.

### Quotes

- "Countries don't have friends, they have interests."
- "At some point, a long enough timeline, that will occur."
- "It's more likely that they're just maintaining the posture they have had for a very long time."
- "I wouldn't let some mid-level diplomats correcting a statement from the head of state make you think that they're about to invade."
- "And honestly, I do, I think they plan on just waiting the U.S. out until the U.S. just re-evaluates how valuable it is."

### Oneliner

China's strategy regarding Taiwan may involve waiting for a U.S. reevaluation, prioritizing interests over friendship.

### Audience

Policy analysts, diplomats, general public

### On-the-ground actions from transcript

- Monitor diplomatic statements and actions (implied)
- Stay informed about international relations and geopolitical developments (implied)
- Advocate for peaceful resolutions in international conflicts (implied)

### Whats missing in summary

Insights on the potential long-term strategic approach of China towards Taiwan

### Tags

#China #Taiwan #Geopolitics #USForeignPolicy #Diplomacy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about China
and the United States and Taiwan
and an assurance that was reportedly made
that is now being walked back and what it means
because commentary is already starting to come out about it
and it's originating from areas
that like to keep people on edge.
So not too long ago, the Chinese head of state
made an assurance to Biden, and it was reported on.
And the idea was that China had no plans to invade Taiwan.
And by the reporting, this assurance
was believed by the United States.
Now, elements within the Chinese government are walking that back and they're basically
saying we didn't take force off the table type of thing.
The commentary is, of course, jumping to they're planning on invading Taiwan.
No, no, that's not what that means.
That doesn't even make sense.
If the Chinese military wanted to invade Taiwan and the United States believed in assurance
that they weren't going to invade, do you know what they wouldn't do?
They wouldn't correct that.
If they actually wanted to invade Taiwan, they wouldn't come out and be like, hey,
just remember, we didn't take force off the table.
That doesn't even make sense.
would want to maintain that element of surprise because apparently the US believes the assurance.
So the idea that them walking it back means that invasion is right around the corner doesn't
really make a whole lot of sense to be honest.
It is more likely that it's just maintaining the posture that they have had for a very
a long time, which is you have that uncertainty about the force being used, but if you look
at China's posture, they've been pretty clear about the idea that they want the island back,
but they want, I think there's a term for it, like reunification via peace or something
along those lines. They don't want to fight for it. And the reason for that
probably has to do with all these scenarios that get run over and over
again by various militaries. And they work out the same way. If it is, if it's
a country that is quote the West that's running the scenario, almost always the
west is capable of defeating China but it's incredibly costly to do so. If it is
a scenario run by the Chinese military they are more often than not successful
in taking the island but it is incredibly costly to do so. And by
costly to be clear I'm talking about like aircraft carriers at the bottom of
ocean level of costly, something that major powers don't want.
My guess, if I had to guess what China's actual intention here is, it's to wait.
The U.S. position, it's been held for a really long time and it hasn't been reevaluated.
My guess is that at some point, some politician is going to be like, hey, why are we risking
losing carriers over this?
At which point the US position on Taiwan might change.
We've talked about how the US has this strategy for the Middle East that is definitely not
going well at the moment, but the idea is to deprioritize it.
the US out of the Middle East a little bit.
At some point in time, that would probably happen with Taiwan.
My guess is that the Chinese government is waiting for that and moving politically to
alter the situation and set the conditions for when that occurs.
would make the most sense. I mean that doesn't mean that they're not going to,
you know, but them walking back that statement a little bit doesn't indicate
that invasion is imminent. It's just reasserting the the formal position that
existed for a long time. So I would assume that this talking point of it
becoming invasion right around the corner, that's probably going to start
to be pushed a little bit because it's good for clicks for people. I wouldn't
put a lot of stock in that. It doesn't fit with their overall posture,
and they're not... nothing that they have done recently indicates that that's
really what they want to do. They're putting up a lot of pressure because
they're in a near-pure contest with the US and that's a point of
contention. So there's pressure there but I mean it's not a new development. I
I wouldn't let some mid-level diplomats correcting a statement from the head of state make you
think that they're about to invade.
I mean, I want you to think about how often mid-level diplomats have to walk back or correct
statements from a U.S. head of state.
It happens all the time.
It's not indicative of anything.
And honestly, I do, I think they plan on just waiting the U.S. out until the U.S. just re-evaluates
how valuable it is.
Because at some point the U.S. will do that.
Remember countries don't have friends, they have interests.
And eventually there will be a head of state that looks over there and is like, is it really
worth it?
It'll happen at some point, a long enough timeline that will occur.
And my guess is that's what they're waiting for.
Because then there's not a giant cost associated with it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}