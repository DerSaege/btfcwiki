---
title: Let's talk about 10 million miles and lasers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=i_G6kXe7e48) |
| Published | 2023/11/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA used lasers to communicate with a spacecraft headed to an asteroid called Psyche.
- The beams of light traveled 10 million miles, enabling the transmission of a lot more information compared to traditional methods.
- The communication system used is DSOC, which stands for Deep Space Optical Communications.
- This development marks a significant advancement in communication technology in deep space.
- The asteroid Psyche is unique as it is believed to be formed from the core of a planet.
- It is a metal asteroid that has the potential to provide valuable insights into space objects humanity has not closely observed before.
- The DSOC test conducted was part of a larger mission to study the asteroid up close.
- This technology could become more common in future deep space missions, improving information transmission speed.
- NASA's use of lasers for communication showcases a new way to send information back from deep space.
- The success of this communication test is a promising step towards more efficient deep space exploration.

### Quotes

- "NASA used lasers to communicate with a spacecraft headed to an asteroid called Psyche."
- "This development marks a significant advancement in communication technology in deep space."
- "The asteroid Psyche is unique as it is believed to be formed from the core of a planet."

### Oneliner

NASA's use of lasers for deep space communication with the asteroid Psyche marks a significant advancement in technology and exploration, paving the way for faster information transmission.

### Audience

Space enthusiasts, technology innovators.

### On-the-ground actions from transcript

- Learn more about deep space communication technology and its implications (suggested).
- Stay updated on advancements in space exploration and technology (suggested).

### Whats missing in summary

The full transcript provides detailed insights into NASA's innovative use of lasers for deep space communication and the unique characteristics of the asteroid Psyche.

### Tags

#NASA #SpaceExploration #DeepSpaceCommunication #AsteroidPsyche #Technology #Innovation


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about 10 million miles
in lasers in a really weird way to make a phone call
because Psyche phoned home.
OK, so NASA has a spacecraft headed
to an asteroid called Psyche, which
we'll come back to in a minute.
also the name of the spacecraft. On November 14th they communicated, they transmitted little beams
of light back and forth. They traveled 10 million miles which is roughly what 40 something times
the distance from the Earth to the Moon.
This would allow information to be sent, a lot more information, 10 to 100 times
as much information to be sent from deep space
back to Earth. And it's using a system called DSOC, Deep Space
optical communications. It is a laser. It's a laser. And the spacecraft sent one to Earth
and Earth sent one to it. So it's a really unique development as far as communication
It's still incredibly early, but they're calling this first light, you know, it's a big deal to them, it's a big deal to
this program.
Now, the asteroid itself is a unique one because it's basically one of the metal ones that everybody talks about
bringing to Earth and everybody gets rich.  The unique thing about it is that they kind of believe that it formed out of
the core of a planet.
Like a planet was forming and then something happened and it kind of shot out and that's where these come from.
But it's a class of object floating around in space that humanity's never really gotten a good look at.
up close and that's what this is about. The DSOC test was just something they
were doing along the way. This is something you'll probably hear more and
more and more about as that technology improves and eventually it it seems like
something that might become very commonplace when it comes to the the
deep space spacecraft that humanity is sending out and this is how it can get
information back much much faster. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}