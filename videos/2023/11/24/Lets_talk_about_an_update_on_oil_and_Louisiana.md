---
title: Let's talk about an update on oil and Louisiana....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=k7rlBmvSosE) |
| Published | 2023/11/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Spill off the coast of Louisiana, estimated at around a million gallons of oil.
- Likely came from an offshore pipeline, source still unidentified after inspecting 23 miles.
- Sheen being monitored with drones and Coast Guard cutters, no new visible sheen post pipeline shutdown.
- Three skimmers are removing surface oil, spill moving away from the coast to the southwest.
- Lack of coverage due to absence of impact on shore, no oiled animals found yet.
- Concern remains for endangered species like turtles.
- Despite being a significant spill, lack of visuals and impact on shore is why it's not getting much attention.
- Current situation seems positive for the environment, but the spill could have had disastrous consequences.
- Importance of not letting the lack of coverage lead to overlooking the potential severity of the spill.
- Acknowledges the critical role of luck in preventing a worse outcome from the spill.


### Quotes

- "As far as million-gallon oil spills go, this is kind of best case."
- "That's not because there were a whole bunch of procedures in place to make that happen."
- "Could have been much much much worse."


### Oneliner

Beau provides an update on a million-gallon oil spill off Louisiana's coast, underscoring the fortunate lack of severe impacts but cautioning against overlooking potential dangers.


### Audience

Environmental advocates


### On-the-ground actions from transcript

- Monitor local news for updates on the spill (suggested)
- Stay informed on environmental impacts and responses (suggested)


### Whats missing in summary

The detailed analysis and potential long-term consequences of the spill are best understood by watching the full transcript.


### Tags

#OilSpill #LouisianaCoast #EnvironmentalImpact #CommunityAwareness #WildlifeProtection


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk a little bit more
about the spill off the coast of Louisiana.
And we will go over the new information,
just kind of run through where everything is at
and provide a little bit of an update.
It's not getting a lot of coverage in large outlets.
And the reason it's not getting a lot of coverage
actually kind of good news it's because they don't have cool visuals to go with
it really and we'll get to that part but in case you missed it there was a spill
of some kind it's oil estimation puts it around a million gallons so not small
and at the same time not huge. At time of filming the belief is that it came
out of a pipeline that sits offshore. They have inspected 23 miles of it a
little bit more than and they still haven't found the source of it so
there's a little bit of uncertainty and concern there. As far as the sheen, the
pool of it that's out there, it is being monitored via drones and Coast Guard
cutters. There has been no visible sheen, no new visible sheen since they shut
down the pipeline. So it seems like they're looking in the right spot. There
There are three skimmers out there removing surface oil.
It is moving away from the coast, moving out to the southwest.
This is why you're not getting coverage.
This is why it's not being treated like a lot of other spills.
There's been no impact to the shore, so you don't have that footage to go along with
the coverage.
far they haven't found any oiled animals so you don't have that imagery. There is
still a concern when it comes to the animals particularly some endangered
species, turtles in particular. So the reason it's not getting coverage is
is because as far as million-gallon oil spills go, this is kind of best case.
The direction it's moving, everything is working in favor of the environment for once.
That being said, the important thing to remember is that this did occur.
It's not coming on shore, there haven't been the huge impacts, but this is, it very easily
could have gone the other way with this.
The lack of coverage on it is probably not great.
It seems like something that there should be a little bit more coverage on.
Because yeah, right now it seems well in hand.
They are doing everything that you could kind of expect them to do.
They started doing it pretty quickly and it isn't impacting the shore.
But that's through luck.
That's not because there were a whole bunch of procedures in place to make that happen.
A lot of this was just pure luck.
could have been much much much worse. So I don't want to kind of let this one just
slide out of the cycle on us. So you will continue to cover this as much as we can
but given the the lack of interesting visuals to go along with a TV news story
you know, no heart-wrenching images of animals or anything like that, it's
difficult to get information at the moment. So we will, we'll do our best to
keep you updated. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}