---
title: Let's talk about tradition, fundraisers, and a reminder....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=37cVpgKjE-Y) |
| Published | 2023/11/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Heading into the holiday season, it can be a tough time for many who may not want to be alone but have no choice.
- Invites viewers to think about inviting someone who came to mind over during this season.
- Conducts a yearly fundraiser through a live stream to benefit people in domestic violence shelters.
- Focuses on fulfilling immediate needs and providing gifts for older kids in shelters.
- The fundraiser tradition ensures that older kids receive tablets and items to call their own.
- Accidentally did the live stream fundraiser recently and raised close to $10,000.
- Plans to round it up to $10,000 to cover all needs and potentially provide excess funds.
- Explains there won't be an Amazon registry like in the past for miners on strike, but will provide addresses and lists for those willing to send in donations.
- Expresses gratitude for the unexpected success of the fundraiser.
- Assures viewers they will be updated on the fundraiser's progress and ways to contribute.

### Quotes

- "It's become a tradition on the channel."
- "We didn't actually plan on doing it until after Thanksgiving."

### Oneliner

Beau reminds viewers of the tough holiday season ahead, shares about the accidental live stream fundraiser success, and plans for future ways to contribute.

### Audience

Community members, supporters

### On-the-ground actions from transcript

- Send in donations to the shelter addresses provided (suggested)
- Stay updated on the fundraiser progress and ways to contribute (implied)

### Whats missing in summary

The full transcript provides insight into Beau's compassionate efforts during the holiday season, showcasing a successful fundraiser for domestic violence shelters and plans for future contributions.

### Tags

#HolidaySeason #Fundraiser #DomesticViolenceShelters #CommunitySupport #AccidentalSuccess


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about traditions
and answer some questions that came in
and just provide a little bit of a reminder.
No real like news today on this one.
First, remember we are heading into the holiday season
for a lot of people, it's a rough time of year.
And a lot of them may not want to be alone,
but don't have a choice.
If somebody just popped into your head right now,
when I said that, maybe think about inviting them over,
if that's something you can do.
OK, so with that little bit out of the way,
a whole bunch of questions have come in asking
when we are going to do our live stream,
our fundraiser for the shelter.
So if you don't know, every year for the last five years,
we have done a live stream to benefit people
in domestic violence shelters.
And what we focus on is getting any immediate needs
they have, but the real purpose is to get gifts
for the older kids.
There are a lot of organizations
that provide gifts to the younger kids,
but the older kids tend to get left out.
So we get them tablets and stuff like that.
When you're in a shelter like that,
you don't have a lot of control,
and you don't have a lot of space that's your own.
So this gives them something that's theirs,
that they have control over.
And it's become a tradition on the channel.
And a whole lot of people have been asking
when we're going to do it.
Three days ago, by accident, it was a few days ago,
we were testing out the live stream stuff,
because we are very rural, so it's not necessarily
an easy task, and accidentally went live.
And there was a Billa O'Reilly vibe to it.
And we were just like, OK, fine, let's just do it now.
And we already did the live stream.
By the time it was all said and done,
we came in so close to 10,000 that it's just 10,000.
We're going to round it up to that, which
should take care of everything that we normally do
and then the excess that they may get in a check
or something along those lines.
Another question that has come in
is, is there gonna be an Amazon registry, like in the past?
Um, that was for the kids
of the miners who were on strike.
That was a different, that was a different fundraiser
thing that we were doing during the same period
for the last couple of years.
They're no longer on strike.
that's not something that's on our agenda.
But because I know a whole lot of people
wanted to participate in the one for the shelters,
we're going to try to work something out
where we can get like an address
or a couple of different addresses
and a list of what they need.
And y'all can send it in like we did the one year
with the consoles because they needed an Xbox
PlayStation or something like that and they wound up like five of them because
y'all are awesome. So we'll take care of that. I apologize, you know, normally
there's like a big lead-up. We didn't actually plan on doing it until after
Thanksgiving. But when we do our little show-and-tell and go through everything
and talk about it, we'll have a list of different ways that if you wanted to
contribute you can. So yeah we didn't we didn't forget about it it's not that
we're not doing it it's that we did it by accident and it got absolutely no
advertising or lead-up out of it so that's what happened and we will we'll
keep you posted on how everything is going with it and you'll get to see like
in years past. You'll get to see what happened. I can't thank you all enough for it, especially
those who stumbled into a random live stream and opened up their wallets to make it happen.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}