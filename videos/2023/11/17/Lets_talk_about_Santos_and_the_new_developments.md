---
title: Let's talk about Santos and the new developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Wi2JD6OVu4g) |
| Published | 2023/11/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative George Santos is the focus, with a recent ethics committee report revealing more unlawful conduct than previously known.
- Santos allegedly used his House candidacy for personal financial gain, leading to a call for Department of Justice involvement.
- Santos dismissed the report as biased and politicized, announcing he won't seek re-election in 2024.
- Despite Santos's response, Congress may still move towards expelling him, with resistance lessening after the report.
- Expect a renewed push to expel Santos and potential further action from the Department of Justice.
- The situation surrounding Santos continues to evolve, indicating a prolonged saga with no clear end in sight.

### Quotes

- "Santos sought to exploit every aspect of his House candidacy for his own personal financial profit."
- "It is a disgusting, politicized smear that shows the depths of how low our federal government has sunk."
- "Not seeking re-election in 2024, I mean, yeah, that's probably true."

### Oneliner

Representative George Santos faces potential expulsion from Congress as new revelations of misconduct emerge, despite his decision not to seek re-election in 2024.

### Audience

Congressional constituents

### On-the-ground actions from transcript

- Contact your representatives to express support for expelling Santos from Congress (implied)

### Whats missing in summary

Details on the specific allegations against Santos and the potential consequences of the Department of Justice's involvement.

### Tags

#GeorgeSantos #EthicsCommitteeReport #Congress #Expulsion #DepartmentOfJustice


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about George Santos,
Representative George Santos,
and talk about the report that came out,
his statement shortly thereafter,
how that statement and that decision
may not be enough to satisfy Congress
and where it might go from here.
If you are unfamiliar with the storyline, there is a representative in the U.S. Congress
that has had some issues lately about a wide range of conduct that has led to a number
of federal charges.
There has been an attempt to expel him, and it fell basically saying, well, let's wait
and see what the ethics committee report says.
report is now available. And in short, it says that they found more unlawful and
uncharged conduct that they will be referring to the Department of Justice so
more than what we already know about. It also kind of summed it up by saying that
Santos sought to quote exploit every aspect of his House candidacy for his
own personal financial profit. Do you have any idea what it takes for people
in Congress to be like oh no you did way too much with your campaign funds? I mean
And that in and of itself is a statement.
In response, Santos said that the report was biased, of course, and said,
It is a disgusting, politicized smear that shows the depths of how low our federal government
has sunk.
Everyone who participated in this grave miscarriage of justice should all be ashamed of themselves.
He also said, I will, however, not be seeking re-election for a second term in 2024, as
my family deserves better than basically to be harassed by the press.
Not seeking re-election in 2024, I mean, yeah, that's probably true.
may not be enough for Congress because while there was resistance to expelling
Santos before this report it does appear at least at time of filming that that
resistance is well it's lessened a lot and it it appears that there might be
movement to expel Santos from Congress soon. So in addition to the stuff that
grabbed all the headlines, and don't worry there's gonna be a whole bunch of
stuff about what he spent the money on allegedly, and that's yeah I mean some
of it is I don't know that's surprising but anyway in addition to that you you
can probably expect to see a renewed push to go ahead and show Santos the
door and there also may be additional movement from the Department of Justice
as well. This is the story that just will not end. So that's that's where that
stands. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}