---
title: Let's talk about GOP, betters, commoners, and commentary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qQ4BScJ-Nqg) |
| Published | 2023/11/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Bob McKeown says:

- Commentary on Republican response to a disappointing performance, particularly focusing on former Senator Rick Santorum's statement.
- Santorum expressed relief that most states do not allow everything to be put up for a direct vote, implying that pure democracies are not ideal for running a country.
- The underlying belief seems to be that common people are not smart enough to make decisions for themselves and need representatives to decide for them.
- McKeown mentions how the term "our betters" and "commoners" are used sarcastically but actually reveal the true mindset of some individuals.
- There is a notion that citizens are not educated enough to make their own choices, leading to the preference for representative democracy over direct democracy.
- The idea of manipulation and control is brought up, suggesting that certain entities believe they can easily trick and influence the masses.
- The concept of giving the masses someone to be mad at in order to maintain control is discussed.
- The issue of wanting control over their own bodies in Ohio is mentioned, indicating a clash of beliefs between different groups.
- There is a warning to not overlook such revealing statements as they represent a deeper attitude that persists beyond the initial comment.
- The theme of authoritarianism and the need to create a common enemy to justify certain actions is emphasized.
- The distinction between wanting to represent versus wanting to rule is pointed out.
- The concept of citizens being viewed as commoners by certain entities is reiterated.
- The call to keep in mind the revealed mindset that discredits the ability of individuals to govern themselves.
- The idea that certain forces aim to create divisions and distractions to maintain power is discussed.
- The reminder to recall the quoted statement about not allowing people to govern themselves due to perceived lack of intelligence.

### Quotes

- "Thank goodness that most of the states in this country don't allow you to put everything on the ballot because pure democracies are not the way to run a country."
- "They need their betters to decide. They need to vote for somebody to represent their interests because they don't really know what their interests are."
- "They think you're easily manipulated. They think that you can be tricked, you can be fooled."
- "They have to give you somebody to look down at because otherwise you would never accept what's going on."
- "It's a good thing that most states don't allow people to govern themselves. Just not smart enough."

### Oneliner

Bob McKeown reveals the condescending attitude towards citizens in political discourse, exposing the belief that they are not capable of making informed decisions for themselves.

### Audience

Voters, activists, citizens

### On-the-ground actions from transcript

- Challenge condescending attitudes towards citizens and demand respect for individual decision-making abilities (implied)
- Stay informed about political rhetoric and call out manipulative tactics aimed at controlling public opinion (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the condescending attitude towards citizens in political discourse, urging viewers to remain vigilant against manipulation and control tactics.

### Tags

#RepublicanResponse #PoliticalAttitudes #Democracy #Manipulation #Representation


## Transcript
Well, howdy there, internet people, it's Bob McKeown.
So today, we are going to talk about some commentary
coming from the Republican side after,
let's just say yesterday's disappointing performance.
And it's worth noting because it illustrates something.
There's things that I say on the channel a lot
that people normally take to be some kind of hyperbole.
not. It's how they view it. And every once in a while, they say something and it
shows that that's how they view it.
Okay, so former Senator Rick Santorum, a Republican, when talking about Ohio and
the fact that the people of Ohio made their voice heard and decided something
for themselves by a margin of 10 points, mind you. This wasn't close. He had this
to say, thank goodness that most of the states in this country don't allow you
to put everything on the ballot because pure democracies are not the way to run
a country. Right, pure democracies are not the way to run a country. I get it.
because those commoners, they don't know what's best for them. They need their
betters to decide. They need to vote for somebody to represent their interests
because they don't really know what their interests are. They think they do,
but they're not smart enough. That's why you can't have a pure democracy. You
need a representative one. The common thing that I say on this channel that
causes people to cringe if they don't realize I'm being very sarcastic is our
betters, commoners. I say that because it does get a reaction but it's only to
point out the truth because that is how they view it. You don't get to make
decisions. You can't run your life. You are below them. That is how they view it.
You need them to filter out all of those thoughts that you know you think you
want. You think you understand but you're just not smart enough. You don't know
what's best for you. And sure in some ways you can make an argument that it
sometimes maybe that's true, but I don't know that that's true when it comes to
your own body. But here's the wild part about it when they when they acknowledge
it and they often do happens way more than you think. When they acknowledge
that that's how they look at it, that people aren't smart enough, that American citizens
aren't smart enough, they're not educated enough to make their own decisions.
And that's why you can't have direct democracy.
At the same time, they're acknowledging that they are playing you, that they are controlling
that all of those things that we talk about like hey shiny keys look over here
here's an enemy be mad at them vote for me you have to acknowledge that it's true
then because they're telling you with this statement that you're not smart
enough to run your own life then how are you smart enough to delegate that power
to somebody else. They don't think you are. They never did. They think that you're
easily manipulated. They think that you can be tricked, you can be fooled. All
they have to do is play on your emotions because they are your batterers and
you're just the masses. And as long as you can take those masses and give them
smaller group of people to be mad at, you can control them. When they talk like
this, they're admitting that. They are upset that the majority of people in
Ohio want control over their own bodies. That's why they're mad. Because they
don't think you should have that because you're a commoner, you're a serf. You
don't get to determine what you do, they do. Every once in a while they say
something that reveals it and you need to remember it because it doesn't go
away the next week when people forget about the quote. It's an underlying
attitude that exists. They believe that they are your rulers, not your
representatives. They're not there to represent your interest because you're
not smart enough to know what your interests are. You just know red team,
blue team, go. Donkey, elephant. One of those is my team. And as it becomes more
and more authoritarian and as they chip away at more and more of your rights, they have
to give you somebody to hate.
They have to give you somebody to look down at because otherwise you would never accept
what's going on.
They have to villainize somebody.
And you hear me say that from now on.
That they don't want to represent, they want to rule.
Or that they view themselves as your betters, that you're a commoner.
Remember this.
Remember this quote.
It's a good thing that most states don't allow people to govern themselves.
just not smart enough.
Anyway, it's just a thought.
You have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}