---
title: Let's talk about Ohio, Kentucky, and trends....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hjYUA1PuGqc) |
| Published | 2023/11/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides a summary of election results from Ohio and Kentucky, mentioning Mississippi and Virginia.
- Talks about Governor Beshear defeating the Trump-backed opponent in Kentucky by five points.
- Mentions the trend in Kentucky where the party that wins the governor's race tends to win the next presidential race.
- Notes that reproductive rights won in Ohio by ten points, despite opposition from the Republican Party.
- Indicates the passing of recreational use for those over 21 in Ohio by 10 points.
- Advises tempering polling with the actual election results for the next six months.
- Shares his interest in the close race of Susanna Gibson and his desire for her to win due to the attacks against her.
- Suggests that the results from Kentucky and Ohio may send a clearer message than polling.

### Quotes

- "You can't win a primary without Trump, but you can't win a general with him."
- "Reproductive rights won in Ohio by ten points. Landslide."
- "As you are just inundated with polling for the next six months, maybe
  remember this."

### Oneliner

Results from Ohio and Kentucky elections suggest trends and victories in reproductive rights and recreational use, hinting at potential future outcomes.

### Audience

Voters, political enthusiasts.

### On-the-ground actions from transcript

- Keep an eye on election trends and results in your state (implied).
- Support candidates facing attacks and unfair treatment (implied).
- Stay informed about local elections and issues (implied).

### What's missing in summary

The full transcript provides a detailed analysis of recent election results in Ohio and Kentucky, offering insights into potential future political landscapes based on these outcomes.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the results,
the results of today, at least from Ohio and Kentucky.
Mississippi and Virginia, they also voted today,
but their results are not in,
or they're not close enough yet at time of filming.
However, in Ohio and Kentucky, they're either called,
they are close enough,
Or it's such a blowout, nothing is going to change it, to be honest.
Okay.
We will start in Kentucky, the Democratic incumbent, Governor Beshear, defeated the
Trump-backed opponent, Cameron, I think, by five points, not even close, by five
points in Kentucky.
This is something that certainly appears to be another example of you can't win a primary
without Trump, but you can't win a general with him.
It's also worth noting that for the last 20 years, the party that wins the governor's
race in Kentucky wins the next presidential race. If you care about those kinds of trends,
I'm sure that's going to ruin some people's nights. Okay, so you have that five points.
In Ohio, reproductive rights will be enshrined. Reproductive rights will be enshrined. It won.
This was very much opposed by the Republican Party, so much so that they
actually tried to raise the threshold so, you know, the commoners couldn't vote
for what they wanted and they had to defer to their betters, but that failed.
The reproductive rights won in Ohio by ten points. By ten points. Landslide. By
10 points. That one I saw coming so much so that I wasn't really even covering
this in the run up to it. Something I didn't expect to happen, recreational use
for those over the age of 21. Also, that also passed by like 10 points. So if you
are on the more liberal or left-leaning side of things, pretty good night for you.
Pretty good night for you. This occurring in Ohio and Kentucky, these
results. As you are just inundated with polling for the next six months, maybe
remember this. Temper the polling with this. It might be worth keeping
in mind. Now in Mississippi, it's too too early to call, but it does not look good
for the Democratic candidate for governor. In Virginia looks pretty even split so far
about what would be expected. The only race I have been following closely is Susanna Gibson and at
time of filming she's up by two-tenths of a point. I actually really want to see her win not because
of any particular policy stance or anything like that. I just I don't like
the way they attacked her. It's really totally out of spite. I just want to see
her win for that reason. But we'll have to wait until maybe tomorrow before we
could talk about those results. But Kentucky and Ohio I think they I think
Maybe I may be sending a clearer message than any polling.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}