---
title: Let's talk about Missouri, wording, and 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6asVgg3a9cI) |
| Published | 2023/11/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Missouri Supreme Court declined to hear a case about partisan wording on a reproductive rights ballot initiative.
- Secretary of State proposed phrasing the question in a partisan manner.
- The current Secretary of State plans to run for governor in 2024, making reproductive rights a cornerstone of the campaign.
- Seven states have protected access to reproductive rights when the question was put to voters.
- Despite Republican push, reproductive rights isn't a divisive issue when voters have a say.
- Democrats might leverage ballot initiatives to drive voter turnout.
- Republicans risk alienating younger demographics by continuing to push on this losing issue.
- Nonpartisan approach is being maintained in Missouri Supreme Court regarding ballot wording.
- Having reproductive rights on the ballot is likely to influence Democratic voter turnout in Missouri.

### Quotes

- "There have been seven states that have put this question to the voters in one way or another."
- "It's been a position of the Republican Party for a really long time, so they're slow to change on this."
- "Even in Missouri, the Supreme Court is still trying to maintain at least some form of nonpartisan language when it comes to this topic being on the ballot."

### Oneliner

Missouri's Supreme Court declines partisan wording on reproductive rights ballot initiatives, potentially influencing Democratic voter turnout in 2024.

### Audience

Voters, activists, residents

### On-the-ground actions from transcript

- Contact local organizations advocating for reproductive rights to get involved in ballot initiatives (implied)
- Support and volunteer for political campaigns focusing on protecting reproductive rights (implied)

### Whats missing in summary

Context on potential impacts of voter turnout and the significance of nonpartisan approaches in ballot initiatives.

### Tags

#ReproductiveRights #Missouri #SupremeCourt #DemocraticParty #RepublicanParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Missouri
and the Supreme Court there and wording
and the Secretary of State and how things will
apparently not be phrased come 2024
because that was a topic of discussion recently
and we're just gonna kind of run through it
and provide some more information about it.
So, like many states, there is a drive, an initiative to get something on the ballot
related to reproductive rights.
Now the Secretary of State, Jay Ashcroft, proposed phrasing the question and asking
people in Missouri asking those voters if they wanted to allow, quote, dangerous and
unregulated abortions until live birth.
Generally speaking, ballot initiatives and the language on the ballot is supposed to
not be partisan.
And that's what the appeals court said back in October.
State, tried to take it to the Missouri Supreme Court, and the Missouri Supreme Court didn't
even want to hear it. They declined to hear it. So it is worth noting that the current
Secretary of State plans on running for governor in 2024. So it seems clear that this is going
to be a cornerstone of the campaign which is, I mean that's a bold move
given the record when it comes to this particular topic. If you don't know, there
have been seven states that have put this question to the voters in one way
or another. In seven states access to reproductive rights was protected. As much
as social media and a lot of both sides coverage would have you believe, this
actually isn't really a divisive issue in this country. When people get to voice
their opinion on it, it's pretty consistent. Seven out of seven. As far as I know, in addition to
Missouri, when it comes to 2024, there are people in eight states who have either gotten to the
point where it's going to be on the ballot or are attempting to do so. There's probably going to
be more. That's going to be a drive, not just for people who want it protected, but for
the Democratic Party, I mean it's a political move. They'll want it on the ballot so people
will show up for it. Given the results of all of the previous instances, I'm surprised
to see Republicans pushing forward on this topic the way they are.
You would think by now they would realize that overall this is a losing issue.
And even if they win in the short term, the younger demographics are going to turn even
further away from them.
It's been a position of the Republican Party for a really long time, so they're slow to
change on this.
My guess is that no matter how this ends up eventually being worded, it will increase
the turnout for the Democratic Party in Missouri.
now, whether or not it's going to be enough to make a huge difference, I mean that's
kind of up in the air, but having this on the ballot, it's going to have an influence.
Whether or not it alters the course of an election is something else, but it will definitely
have an influence.
So even in Missouri, the Supreme Court is still trying to maintain at least some form
of nonpartisan language when it comes to this topic being on the ballot.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}