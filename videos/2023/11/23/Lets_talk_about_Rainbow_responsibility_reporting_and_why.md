---
title: Let's talk about Rainbow, responsibility, reporting, and why....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_lpYNPDjPsU) |
| Published | 2023/11/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Something was reported all day in a certain light, with a specific word thrown out repeatedly about an incident near the US-Canada crossing.
- An explosive incident occurred involving a car traveling at high speed, swerving, impacting something, and launching through the air.
- The importance of understanding the "why" behind events and the definitions of terms like terrorism, which aim to incite fear beyond the immediate area.
- Various news outlets reported on the incident, some hinting at responsible groups, potentially aiding in fear-mongering.
- Official statements did not indicate terrorism involvement, despite initial fear-mongering media coverage.
- Being accurate in reporting is more critical than being the first to report, especially when attributing blame or shaping narratives.
- Waiting for more information before reporting can lead to more accurate coverage, as opposed to rushing with potentially incorrect information.

### Quotes

- "Being first isn't best. Not if you're going to be wrong."
- "The why is super important because the goal of stuff like that is to get media coverage to influence those beyond the immediate area."
- "All the fear-mongering that went on, it helped whatever cause they were trying to blame it on."

### Oneliner

Understanding the "why" and accurately reporting events is key to preventing fear-mongering and influencing narratives in media coverage.

### Audience

Media Outlets, News Consumers

### On-the-ground actions from transcript

- Verify information before reporting (exemplified)
- Wait for official statements before attributing blame (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of media reporting, responsibility, and the importance of understanding the underlying motivations behind events.

### Tags

#Media #Reporting #Terrorism #Responsibility #Accuracy #Narratives


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about rainbow and reactions,
reporting, responsibility, definitions, and why.
Why?
My eternal question most days, why?
Why did this happen?
Why, why, why?
We're going to do this because all day today something was being reported on
and it was being reported on in a certain light. Over and over and over a
word kept getting thrown out, right? And if you missed the news, something
happened at a crossing between the US and Canada.
well, near there, in that general vicinity.
And a lot of outlets throughout the day
kept throwing out the T-word.
Now, if you're a government entity,
something like this happens.
It looks like that.
You have to treat it that way.
You have to investigate it in that light.
That's the way you have to do it, because if it looks that way
and you don't investigate it that way, and it was,
and then there's another, that's really bad.
But as far as the reporting goes, who?
Don't know, right?
Don't know.
Even at time of filming, what's available
is that it was most likely a New Yorker who maybe possibly
might have originated from the area near that casino up there.
What?
A car exploded.
When?
after it was observed, traveling at an excessive amount of speed, swerving, impacting something,
and then launching through the air like it just came out of a slingshot.
Who, what, when, where?
Near a sensitive area.
I mean, assuming that they didn't have the physics worked out for that.
Near a sensitive area, but not really an area that would make a whole lot of sense to do
something like that at, right? Why? Don't know. Don't know. Even now, don't know. The
why is important. The why is important and it's incredibly important to that
word that everybody kept using all day. Words have definitions and in good
technical definitions of that term. There's a clause, you know, there's no
agreed upon definition for terrorism. Even in, even like in between the US
agencies, they don't have an agreed upon definition. But good technical
definitions, they all have one clause and it's something along the lines of inculcate
fear. Create a general atmosphere of fear. Influence those beyond the immediate
area. Instill terror. Stuff like that. The why. The why is really important because
that's the goal. If you take that clause out of those definitions, what do you
end up with? The unlawful use of violence, your threat of violence to achieve a
a political, religious, ideological, or monetary goal. That's what you end up with with most
definitions. Something similar to that. I would point out that like bank robbery falls
under that. Bumping off your spouse for the insurance money falls under that. The why
is super important. Because the goal of stuff like that is to get media coverage to
influence those beyond the immediate area. That's why that
clause is so important. It explains the why. So a lot of outlets today, they went
through and they reported on this in a certain fashion. Some of them even
Then hinting at who they think was responsible for it.
The people who hinted and tried to attribute it to a certain group or whatever, they worked
for them all day.
Which is funny because odds are if they were going to attribute it to a group, they were
going to do it to a group they didn't like.
But because they helped instill terror, generate an atmosphere or climate of fear, influence
those beyond the immediate area, they gave them a free operation.
They didn't even have to do anything.
And they got the benefit of it.
The who, what, when, where, why thing?
Kind of important.
That's why it's there.
That's why it's been around so long.
I don't know if you still do, but you used to learn this in like grade school.
It was that common, it was that important.
At time of filming, it's worth noting that the official statements have said that there
is no indication of the T-word.
My understanding is that it hasn't completely been rolled out, but it seems incredibly unlikely
at this point. So all of that coverage, all of that fear-mongering that went on,
it helped whatever cause they were trying to blame it on, whatever cause
they were trying to fear-monger about. If they actually are groups that engage in
the T word. You just helped them, like a massive amount, because they got all the publicity,
they got all the influence, and they didn't have to do anything. Being first is less important
in being right. So obviously there's more investigation in this to do, but at time
of filming it does not look like what the quote reporting said most of the day.
All the fear-mongering. I would like to point out that there were some outlets
They kept saying, you know, we're waiting for more information. We're waiting for
more information. And I'm sure that, you know, if you're on a 24-hour news outlet
and you're watching them, you might have switched the channel to see if somebody
else had more information, like ahead of them. I would point out those who waited,
And they were right.
Being first isn't best.
Not if you're going to be wrong.
And especially if your whole goal is to try to manufacture a narrative and blame somebody,
you might want to actually understand how it works.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}