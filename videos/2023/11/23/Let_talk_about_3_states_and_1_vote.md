---
title: Let' talk about 3 states and 1 vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hUnxhpwsAdA) |
| Published | 2023/11/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Three races in New Jersey, Kansas, and Louisiana were determined by a single vote each.
- Despite voting being considered the least effective form of civic engagement, a single vote can still alter outcomes.
- The U.S. is currently highly polarized, with voter turnout being critical for elections.
- The unlikely voter, who doesn't always show up, will play a significant role in deciding future elections.
- It's vital for the Democratic Party to recognize the importance of these unlikely voters for the next election.

### Quotes

- "For these races to be decided by a single vote, it kind of throws that whole thing out that you know, it doesn't matter, right?"
- "The unlikely voter is going to decide this election."
- "They're going to decide the elections."
- "You're going to need them."
- "It's just a thought."

### Oneliner

Three races in different states were decided by a single vote, showcasing the critical role of voter turnout in elections and the power of the unlikely voter.

### Audience

Voters, Democratic Party

### On-the-ground actions from transcript

- Reach out to unlikely voters and encourage them to participate in elections (implied)
- Recognize the impact of every single vote and the importance of voter turnout (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the significance of single votes in elections and the impact of unlikely voters on election outcomes. It also stresses the importance of voter turnout and engagement for future elections.

### Tags

#Voting #Elections #CivicEngagement #UnlikelyVoters #DemocraticParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about one, one illustration,
even though it's three, but it's still just one.
And we will talk about New Jersey, Kansas, and Louisiana,
even though just one.
I'm sure that's clear as mud,
but it'll make sense here in a second.
So, in New Jersey, Bownebrook Borough Council, the Democratic Party won a seat there.
In Sedgwick County, Kansas, they are not going to have a renewed sales tax of 1%, something
like that.
And then in Caddo Parish in Louisiana, the Democratic Party won the sheriff's race there.
Very very different races all over the country, but they are tied together by one.
In New Jersey, the Democratic Party won by a vote of 698 to 697.
Sedgwick County in Kansas, that vote was 473 to 472. In Louisiana, the Democratic
Party won by a vote of 21,621 to 21,620. All three of these
races, one vote. Literally, one vote. Now, sure, odds are, I'm guessing there's gonna
to be some breakouts. But that's where it stands right now. A single vote. Man, the
odds of that, huh? You know, when we talk about voting on this channel, something
that you hear often is that it is the least effective form of civic
engagement because there are things that have a more direct impact in a lot of
ways. There are a whole bunch of different forms of civic engagement.
Voting is generally speaking the least effective as far as actually creating
immediate change. The word is least. It's not not. For for these races to be
decided by a single vote, it kind of throws that whole thing out that you
know, it doesn't matter, right? It is the least effective. You're not going to vote
in like deep systemic change. That's not going to happen. But it can act as a
safeguard, a hedge. Maybe it doesn't get as bad as quick, something like that.
It's just one of those things, because I say that so often about voting, I think it's
important to occasionally remind people that it is least effective.
Never said that it's not effective.
And sometimes it is a vote, a single vote that alters the outcome.
Now, this doesn't happen often.
Normally you can't find three that occur, you know, occurring at the same time like
that.
But that just goes to show how polarized the U.S. is right now.
And how turnout is what is going to decide 2024.
It's going to be the deciding factor.
It's always the deciding factor in a lot of ways, obviously.
But this year, just like in 2022 and the times we've talked about it before, and probably
to an even larger degree.
The unlikely voter is going to decide this election.
It's going to be the voter who doesn't always vote.
It's going to be the voter who doesn't always show up.
It's going to be demographics that are generally viewed as well.
They're not going to show up.
They're going to be too busy on TikTok.
They're going to decide the elections.
And part of this is to remind people within those groups.
And the other part is to remind the Democratic Party, you need them.
You're going to need them.
They will decide the next election.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}