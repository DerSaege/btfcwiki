---
title: The Roads to a Thanksgiving distraction.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nPFSzjomISQ) |
| Published | 2023/11/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hosting a Thanksgiving special Q&A for those seeking a break or distraction during the holidays.
- Answering lighthearted questions from viewers without prior knowledge of them.
- Explaining the importance of entertaining thoughts without accepting them as fact.
- Shedding light on the objective of teaching about foreign policy and advocating for change.
- Addressing personal grooming questions and explaining an accidental live stream success.
- Sharing hobbies, like photography and animals, which intersect with work on the YouTube channel.
- Declining to release certain videos due to emotional climate and strategical content.
- Expressing interest in a Route 66 trip and discussing Stoic principles.
- Responding to inquiries about rural living, property size, and law enforcement in different areas.
- Offering advice on navigating life-changing transitions and seeking viewer input on core video concepts.

### Quotes

- "The mark of an intelligent person is the ability to entertain a thought without accepting it."
- "You can't fix something if you don't know what's broke."
- "I really want you to do the Route 66 trip."
- "If it doesn't matter where you're going, you're already there."
- "Having the right information will make all the difference."

### Oneliner

Beau shares lighthearted Q&A, insights on foreign policy, personal grooming, hobbies, rural living, law enforcement, and life transitions.

### Audience

Viewers

### On-the-ground actions from transcript

- Watch Beau's videos and comment on core concepts that resonate (suggested)
- Support Beau's accidental live stream video on the other channel (implied)
- Learn about entertaining thoughts without accepting them as fact (suggested)

### Whats missing in summary

Insights on Stoic principles and Route 66 trip details can best be experienced by watching the full transcript.

### Tags

#ThanksgivingSpecial #Q&A #ForeignPolicy #RuralLiving #LifeTransitions #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Bo again.
And welcome to The Roads with Bo.
And today we are doing our Thanksgiving special,
which has absolutely nothing to do with Thanksgiving.
It is just a lighthearted Q&A
that the team put together for today.
We try to do stuff like this around the holidays
for people who are with their family and need a break,
or people who can't be with their family,
or they don't have somewhere they want to be
just need something to fill the space. So this is just like normal. There's a
bunch of questions. Haven't seen them. They were picked out from your messages
and we're gonna go through them. These are all supposed to be light-hearted
this time. If y'all remember what happened last time, they were definitely not.
Okay, what's your favorite episode of Futurama? The one where Bender becomes
like God, with the little people on his chest floating through space.
I've heard you say a lot that the mark of an intelligent person is the ability to entertain
a thought without accepting it.
I feel like I know how to do that, but I'm not certain.
And even if I'm right about that, I definitely think I could get better at it.
At the risk of asking for something you've already done, or something that seems obvious
or difficult to explain, could you make a video explaining, showing how to do that?
You do it all the time, everybody does, but they normally do it with information that
is in the future tense.
When you run through scenarios like, well, if I do this, then this could happen, then
this could happen, you're entertaining these different ideas, but you're not accepting
them as what will occur.
The thing is, we don't always do that when we get information.
When information comes in, sometimes it's wise to look at it, play with it, think about
it, entertain it as if it's true, but don't accept it as fact until you've really thought
about it.
And it's also a good way to check your core beliefs.
If you truly believe something and somebody is providing you what they believe is evidence
to the contrary, it's a good idea to entertain it and say, okay, well, let's just say that
that's true and then kind of go through it from there and see if it is, if it holds up
to scrutiny.
If your idea is right, it won't.
If your idea is wrong and it holds up to scrutiny, if you have entertained it, you're less likely
to suffer cognitive dissonance, so just throw it away.
So there we go.
What is your objective for teaching us about foreign policy?
Is it so tactics of bad faith actors aren't as successful?
of. That's part of it. Those people who want a fairer world, a better world, who
also understand American foreign policy, they want American foreign policy to
change. I think most of the people watching this channel want a fairer
a world, they want a better world.
If it becomes clearer how American foreign policy truly functions, you'd probably want
it to change.
And to me, I am of the firm belief that you can't fix something if you don't know what's
broke.
So putting out videos that very bluntly say it's about power.
It's not about all of the window dressing.
It's about power.
And just putting it in those very basic terms, those very real, raw, no mom and apple pie,
I think it helps illustrate what is broken better.
I know there's a lot of people, because I get the messages pretty often, where people
think that because I explain it that way that I think it's a good thing.
No, no, I'm hoping that you hear it and you're not happy with it.
Can you please get rid of this wild, white hair in your beard?"
In parentheses it says, and they sent a picture with it circled.
Yeah, that's not actually a white hair.
It's the way the lighting hits it sometimes.
I can probably make it do it.
It's not actually a white hair, but honestly, even if it was, I probably wouldn't do anything
about it anyway.
the shelter live stream. Yeah, so there's actually a video coming out about this over on the other
channel. A whole bunch of this message has come in. We already did it by accident. We were testing
out the stuff to see if it was working. We accidentally went live and we just started doing
it. It was great success. The video that's coming out on this actually has, it has more information
because I know there's a whole bunch of people who, you know, you've been participating in it for four years, and
this year it got messed up. We are coming up with ways so you can still
participate even though the stream is done.
But yeah, it went well.
Do you watch anime? Um, sometimes. You know, it's not something that I
that I bring up but I don't like cringe when my friends or kids do so yes but
it's not generally speaking it is not something that it really enthralls me
What's your hobby? I have a YouTube channel.
My hobby. I have a lot of hobbies.
Photography. I'm a horrible carpenter.
Animals in general.
I'm one of those lucky people where my hobby intersects with my job.
It's like the coolest thing ever.
Wondering if you would be able to do an episode where you talk about some of your background
objects, or maybe talk about one item at a time in the Rhodes episodes?
Yeah, we can do that.
I think you're probably talking more about the items that come in and out, like the Easter
eggs.
Yeah, I mean, we can, yeah, we'll figure something out.
We can do that.
I was one of those who saw the unreleased video with the Sam Kenison reference.
I don't understand why you don't release it.
Everything in it is factual.
you're a little mad, but it's okay to be mad. So for context here, I have filmed a
number of videos about the current situation in the Middle East that I have
not released. And one of them, for whatever reason, the other day if you were
watching the foreign policy playlist, even though you weren't supposed to be
able to you could see it and like people were commenting on it and it was not
public yeah the reason I haven't released it it's not helpful right now it's not
helpful people are upset people are angry people are emotional and these
videos are, they're about the strategies that are being used by various sides and
how a lot of things that people just hold to be true, they're not. And we're
not talking about like moral questions or historical information or anything
like that, we're talking about the strategies being employed and how many of them, they're
actually hurting the people who most people believe those strategies help.
And the thing is right now, the people who need to hear it, they won't.
emotions there and it would just be it would be just rejected out of hand. I
honestly think that after this cycle finishes I'll probably release them as
is and just once people are not as emotionally charged about it and
And hopefully, it gets through in some ways.
What's the most underrated traditional sci-fi movie?
I don't know what traditional means here.
So let's see.
We have traditional.
I'm guessing just normal sci-fi elements,
like it's not blended with another genre.
The fifth element, I don't think it got the play it should have.
In a video recently you were asked whether you were a Stoic or a Buddhist and you kind
of laughed and said you were neither.
You may not know it, but you're a Stoic.
You exemplify most of the core principles and the best counsel of the texts.
you should really read some of these Stoic texts. I have. I have. When it comes to philosophy
and a lot of spiritual texts or religious texts, I have read a whole lot of them. I
don't... I like to pick and choose. I like to pick and choose. When it comes to the Stoic
stuff. To me, the guidelines that arise governing your own behavior... I like a
lot of those. I like a lot of those and I think they... I think there's a lot of
truth in them but I don't consider myself a stoic. It's, to me, it's more about the
guidelines of personal behavior rather than a philosophy that is adhered to because it
was written down.
I hope that makes sense.
I really want you to do the Route 66 trip.
I'm just wondering how you ever plan to do it if you literally
never take a day off.
Yeah.
I really want to do that one too.
So it's all planned to include how
to deal with video production and all of that stuff.
The thing with that one is that is
going to be spur of the moment.
It's all planned out, but when the window opens,
we have to go right then.
And that's just something we'll be waiting on for that to happen.
Just say, have your pets spayed or neutered?
What?
Like, there's no question here.
Like, that's all it says.
I guess I'm Bob Barker, I wish I could do that voice.
Who is your celebrity crush?
My wife.
I don't have a celebrity crush, I don't have anything witty to say to that either.
You got a bunch of questions about rural things, so kind of a mini-thing going on here.
I like that there's notes in this one.
I want to move to a rural area, but I don't know if I can be surrounded by that many Republicans.
It balances out, though, because there are more conservatives in rural areas, but those
people who are more progressive, they're way to the left.
You don't find a lot of centrist Democrats in rural areas.
The other thing to keep in mind is there are stereotypes, but even in a place like Mississippi,
One out of three people more or less is not a Republican.
It's not, I mean it's pronounced, don't get me wrong, but it's not quite as overwhelming
in most places.
Now like in the area where I live, it's more like one out of ten isn't, but it is what
it is. I want to move to a rural part of Georgia because I want quiet. All the
properties are 40 acres. How do you mow 40 acres? You don't. No, no, you don't. Okay,
if you're moving out to the middle of nowhere, I mean maybe this is your
buying land and gonna put a house in it, please don't, please don't clear it all.
Not just because it's not what you want, but also like it would be better for the environment
if you don't clear at all.
Most times people don't clear the whole property and there's like trees around the outside.
If it's just like a residence out in the middle of nowhere, I would also point out you can
get properties way smaller than 40 acres to put that on and still have privacy.
But if you're talking about a property that has 40 cleared acres, that's probably farmland.
And you don't mow it.
Something gets grown on it.
Yeah, I'm so curious how this came about.
This is one of those times where I wish this was live because I want more information.
You don't have to get 40 acres.
You can get away with a much smaller piece of land if it's wooded, and there's a lot
of places that are like that.
Okay, I was in your part of Florida last month.
Why are the cops more heavily armed than in Boston?
He had an AR shotgun and a sniper rifle in his car.
Yeah, it's funny. I had a friend from New York down, and this exact same question came
up because we were standing outside somewhere and a cop car pulled up, and he looked in
and he was like, what? The answer that I gave him is probably going to be pretty revealing.
That's the only cop for 30 miles.
no backup, like there's nobody for them to call. So yeah, they're, generally
speaking, when you get into very rural areas, they have everything. And it's
weird, though, because while they have everything, you tend to get cops that
are more spirit of the law, rather than letter of the law types. And part of
that is because you know the the departments are small and the sheriff is
elected. You know the populations are small so if a deputy is not a spirit of
the law type the the complaints go to the sheriff and then back to you
know back to the deputy pretty quickly and if there's too many of them he
He didn't work there anymore.
The other reason I think you have more spirit of the law
types is because they're the only cop for 30 miles.
It tends to lead to them being way more
interested in de-escalation because backup
is a long way away.
But yeah, that's why they have that.
And generally, there's probably another pistol in there.
And then some kind of something like an MP5,
but it's probably semi-automatic,
but something very small that they could use
if they needed that type of thing.
There's probably way more than you saw in that car, as well.
How do you get through life-changing transitions
when you don't know where you're supposed to be headed?
Yeah.
So if you reach, if you're going through the end of a stage
phase of your life and you don't know what's next, you have to figure that out first.
I mean, if it doesn't matter where you're going, you're already there.
You have to figure out where you're supposed to be headed, to use your words.
You have to figure that out first.
You have to figure out who you want to be, what you want to be.
And that's the thing about those life-changing transitions, is that you have the ability
to take stock and decide where you want to go from there.
And then it's just a matter of putting it into place.
It is both scary and very freeing at the same time.
OK. I work for this guy on YouTube who asked us to put together a playlist of videos that
covered the core concepts that continually come back up in his videos. We voted and think
it would be better if he just asked the viewers to put them in the comments section of this
video I'm guy on YouTube it's me that's nice okay yeah so for context on this it's I'm
trying to think of one off the top of my head like the bus analogy is something that I bring
up a lot. Those core ideas they constantly get referenced. Let's do this
another way. If there is a video that spoke to you on some level, that
resonated with you in some way, that brought you a new concept or a new idea
that you think is particularly important, the team would like you to put it down
below in the comments section so they don't have to go looking for them.
Because surprisingly, the important ones are not necessarily the most viewed.
That is not a great metric for that.
Okay, and that looks like it.
That's the end of the questions.
I don't know how long this was.
It didn't seem like it took a long time, but yeah, I hope that provided you with the break
or distraction or whatever you needed today.
So a little bit more context, a little bit more information, and having the right information
will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}