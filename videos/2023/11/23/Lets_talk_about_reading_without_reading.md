---
title: Let's talk about reading without reading....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CgasdxMYChA) |
| Published | 2023/11/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges a generational gap in reading habits, with kids preferring movies over books due to easy access to information on their phones.
- Recommends adapting to modern technology by using e-books or the LibriVox app to make reading more appealing to children.
- Shares a personal story about how a chance encounter with an astronaut on a plane inspired his love for reading.
- Emphasizes the importance of reading for increasing knowledge and suggests sharing the astronaut story with kids to encourage reading.
- Advises making reading fun rather than a chore for kids, recognizing the need to adapt to changing times and technologies.

### Quotes

- "No knowledge is wasted."
- "People read what they want to for entertainment, but all books increase knowledge."
- "We shouldn't turn into the people screaming, get off my lawn."

### Oneliner

Beau addresses the generational gap in reading habits, suggests adapting to modern technology to make reading fun, and shares a story to inspire a love for reading. 

### Audience

Parents, caregivers, educators

### On-the-ground actions from transcript

- Share the story of the astronaut encounter to inspire a love for reading among kids (suggested).
- Introduce kids to e-books or the LibriVox app to make reading more engaging (suggested).
- Make reading a fun activity rather than a chore for children (implied).

### Whats missing in summary

The full transcript provides a deeper insight into Beau's personal connection to reading and offers practical tips on how to encourage children to read in a technology-driven world.

### Tags

#Reading #Children #Adaptation #Knowledge #Inspiration


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about reading.
We're gonna talk about reading.
And a generational gap, maybe,
in how people look at reading.
Because I got a message,
and the short version of this is I can't get my kids to read.
I can't get them engaged in reading.
They want to watch movies, they don't want to actually read,
and this is something that's important to me.
I was wondering if you had any ideas,
what got you reading, all of this stuff.
It's different for them.
They grew up with unlimited access to information
in the palm of their hand.
It's not as intriguing for them in the same way.
like talking about paper books, I've run into the same issue.
Sometimes a tablet with e-books will actually do it.
Another thing is LibriVox, which is an app that gives you
basically access to pretty much all of the classics
somebody reads it. That's one that seems to be, it seems, they seem a little bit
more receptive to it because they can multitask and they like to do multiple
things at one time so they can listen to it and do something else. And I mean that
would be my suggestion, you know, rather than trying to force them to do
something that is, I guess to many, it's ancient technology. It's outdated. Try to
adapt because the idea behind it isn't sitting down and staring at pages.
It's experiencing something and having your mind paint the image. So listening
to it is just as good for that. As far as me, what got me reading, it was a story I
heard when I was a kid. I've told it on the channel before, but it was years and
years ago. And people asked at the time, is this like me telling a story and the guy is
really me. No, this is really a story I heard when I was a kid. There's this guy.
He's on an airplane and this guy loved space. Loved everything about it. And I
don't remember who it was, but if we're talking about like one of the astronauts
that like was on the moon was walked by the stewardess to where he was at and
sat right beside him. So this guy is sitting next to his hero and he's trying
to come up with like something to say to him and spark a conversation but
everything he says because he's nervous it just doesn't come out right and in
the story he says that he looks out the window and he looks back at the
astronaut and he's like you know it's amazing we're so high up and like
immediately he felt silly for saying that because you know this guy's been to
the moon but the astronaut was really cool about it and like looked out the
window and was like yeah we're about however far up you know because and I
know that because you can still see structures on the surface of the earth
and the guy telling the story he was like you know that's that's when I knew
I was really out of my league you know I'm sitting here calling it the ground
this guy's calling it the earth that's why he's an astronaut you know and the
the astronaut eventually pulls out a book my guess is maybe to try to get away
from the guy who's telling the story but the book is on Portuguese art and the
guy asks him he's like why are you reading that he's like you're an
astronaut why are you reading about Portuguese art and the guy responded
the astronaut responded by saying because I don't know anything about it
That's why he's an astronaut.
That is, you know, that's why everybody reads books, just increasing knowledge, right?
It's not people read what they want to for entertainment or whatever, but I would suggest
that all books increase knowledge and no knowledge is wasted.
But that answer, because I don't know anything about it, that's what got me reading.
So I don't know, maybe tell them that story, maybe that'll help.
It's tough.
Kids in general are tough, you know, I mean, as long as they can read, you know, it should
be something that you're trying to make fun rather than make it a chore. At least that's
the way I've always looked at it. We have to remember that we shouldn't turn into the
people screaming, get off my lawn. Things are different. They look at things differently.
have different technologies that they're more comfortable with, it really might be
as simple as getting an e-book reader and that might do it.
So something a little less stressful, you know, because it is a period when there might
be some people who aren't with who they want to be with and maybe just a little
bit more relaxing. Check out LibriVox if you're bored today. There's tons of
titles and most of the people reading them it's good like it helps move
the story along but you still get that out of benefit of all of the
imagination that goes with it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}