---
title: Let's talk about Louisiana, clocks, and maps.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5RQV9KeAK-0) |
| Published | 2023/11/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Louisiana facing a Voting Rights Act issue due to underrepresentation of Black population in congressional districts.
- Federal court determined the current maps likely violate the Voting Rights Act.
- Deadline for enacting a new map is January 15th.
- Outgoing governor can call a special session before January 8th when the new governor takes office.
- Potential for delay in enacting new maps due to the holiday season.
- Appeals court provided wiggle room for a small extension, but a prolonged delay could lead to a trial.
- Republican Party may not want a trial as their arguments for current maps may not fare well in public scrutiny.
- Likely scenario: new governor calls for a session, gets a small extension, and enacts a new map.
- There might still be attempts to delay or manipulate the map-drawing process in favor of Republicans.
- Federal courts agree that the current map violates the Voting Rights Act.

### Quotes

- "The question for Republicans is it worth trying to hold off until the next election when they probably won't be able to, in all the ways they have left, put them in the position of openly saying, well, we don't want black people's votes to count."
- "There's an outgoing governor. The incoming governor doesn't come in until January 8th."
- "It seems like something that they [Republican Party] would want to avoid and not just openly say, no, we want a gerrymander."
- "The order from the appeals court is pretty clear and there is a consensus among the federal courts that this map, the current map, likely violates the Voting Rights Act."
- "So we'll have to wait and see though."

### Oneliner

Louisiana faces Voting Rights Act violation due to underrepresentation of Black population in congressional districts; deadline to redraw maps by January 15th with potential political maneuvering.

### Audience

Louisiana residents, activists

### On-the-ground actions from transcript

- Contact local representatives to ensure fair redistricting (implied)
- Stay informed about the map-drawing process and advocate for equitable representation (implied)

### Whats missing in summary

Insights on the potential impact of delayed map redrawing on fair representation and voter rights in Louisiana.

### Tags

#Louisiana #VotingRightsAct #Redistricting #RepublicanParty #FairRepresentation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about clocks and maps
in Louisiana.
We talked about this a little bit about a month ago.
But if you don't remember the coverage, don't worry.
You probably remember the coverage of Alabama's map issue.
It's pretty much the exact same thing.
So in Louisiana, roughly one-third of the population is black, however, when it comes
to congressional districts, only one out of six is majority black, therefore, it probably
violates the Voting Rights Act and new maps need to be drawn up.
That was the determination by a federal court.
It went to appeal.
The appeals court has now said, yes, the maps will likely violate the Voting Rights Act.
You have until January 15th to enact a new map.
That seems pretty simple, right?
Okay, so now the clock's coming to play.
There's an outgoing governor.
So the outgoing governor can call a special session, which I don't know that the governor
is inclined to do that.
Not because they disagree with the maps.
Not because they disagree with the court's ruling.
Just because it's the holidays and stuff like that.
The incoming governor doesn't come in until January 8th.
If the new governor was to call a special session to enact a new map, they have seven
days notice, which takes us to January 15th.
There's an issue there bringing this together.
Now the appeals court did give a little bit of wiggle room as far as if the state requested
delay or something like that.
There can be a small one.
But they also kind of said that if it doesn't happen quick, it goes to trial.
I don't think that the Republican Party really wants that.
The arguments that they've made so far in support of the current maps, I don't think
there are arguments that they would want to make before a wide audience, like the one
that would be watching a trial like that.
It seems like something that they would want to avoid and not just openly say, no, we want
a gerrymander.
It just doesn't seem like a good idea for them.
So my best guess as to what's going to occur if the current governor doesn't just say,
you should have fixed it sooner you're coming back in on the holidays is the
new governor calls for the session they get a small extension and they enact a
new map that's the most likely scenario at the moment but given Republican
desires to keep outdated maps or maps that favor them, there still might be a
little bit of less-than-fair play that occurs here. So we'll end up watching it
still, but the order from the appeals court is pretty clear and there is a
consensus among the federal courts that this map, the current map, likely violates
the Voting Rights Act. So the question for Republicans is it worth trying to
hold off until the next election when they probably won't be able to, in all
the ways they have left, put them in the position of openly saying, well, we don't
want black people's votes to count. It doesn't seem like a winning strategy in
Louisiana. So we'll have to wait and see though. Anyway, it's just a thought. Y'all
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}