---
title: Let's talk about Ukraine, Oil, and bloggers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LqhGtOho8VE) |
| Published | 2023/11/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia holding chemical weapons for winter to target energy infrastructure.
- Ukraine and British intelligence agencies agree on this assessment.
- Ukraine hints at a response involving hitting Russian oil infrastructure.
- Russian economy heavily dependent on oil; disruption could be significant.
- Discrepancies in maps showing Russian control in Ukraine explained by Russian field commanders falsely claiming territory.
- Russian military bloggers influence Western analysts with insights into Russian military culture.
- Western analysts monitor Russian bloggers for unfiltered information.
- Russian bloggers provide valuable information through complaints and indirect statements.
- Monitoring Russian bloggers can provide a deeper understanding of the conflict in Ukraine.
- Conflict in Ukraine ongoing with potential for developments over the winter.

### Quotes

- "Russia didn't really run out of this stuff, they're holding it for the winter."
- "You've got to get this done. And they're just saying, okay, you want a good report, here's one."
- "It's not necessarily for their political takes or for their assessment on what is happening."

### Oneliner

Russia potentially holding chemical weapons for winter to target energy infrastructure, while Russian field commanders falsely claim territory in Ukraine, influencing Western analysts through military bloggers.

### Audience

Analysts, policymakers, activists

### On-the-ground actions from transcript

- Monitor Russian military bloggers for insights into the conflict (implied)

### Whats missing in summary

Analysis on the potential impact of false information provided by Russian field commanders and the role of military bloggers in influencing Western perspectives.

### Tags

#Ukraine #Russia #Intelligence #MilitaryBlogging #Conflict #WesternAnalysts


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to provide a little bit of an
update on Ukraine and some things that are occurring there.
It's really two developments and then an explanation about a
source that a whole lot of Western analysts use.
And I had a question about it, and I was like, yeah, that
really doesn't make any sense if you don't know the whole
story with it.
So we will go over that as well.
Okay, so the first piece of news.
Recently, we talked about how the British services
put out an assessment, and at the time, it was an outlier.
And it was basically, hey, Russia didn't really
run out of this stuff, they're holding it for the winter,
and they're gonna hit energy infrastructure with it.
At the time that assessment was made,
it was the outlier from intelligence agencies.
It has now become the agreed-upon assessment.
Ukraine and their intelligence agency,
they have also said that that's what they think is happening.
Now, my guess is they knew that before the British,
but they're now openly saying
that this is what they think is going to occur.
They have also kind of indicated
that they have a response planned.
And it's basically, okay,
if you hit our energy infrastructure,
we're gonna hit yours.
And they're talking about oil.
That may be a pretty powerful statement.
The Russian economy is not doing well.
It's, there's a lot of cooked books going on right now.
And the one thing that's keeping it afloat, well it's black and comes in giant drums.
That's what the entire Russian economy is floating on.
If they were to disrupt that, that would be a huge blow to the Russian economy.
So it might be something that even the possibility of it, it might be something strong enough
to sway Putin on this.
We'll have to wait and see.
But it's a very powerful statement.
And if they can actually do it, it's even more powerful.
So be ready for that as a response
if that is what happens.
The next bit of news has to do with maps.
And it's something that I think a lot of us
noticed lately but didn't really have an explanation for it and that's that the
maps have started to diverge again. For a long time everybody was basically
looking at ISW's maps, the Institute for the Study of War, looking at their maps
and that was pretty well agreed upon. Now you're starting to see analysts who
lean more heavily into Russian sources, their maps look different and they
believe that Russia controls areas that, well, frankly they don't. And I've seen a
handful of times where a map has indicated a particular area is under
Russian control and then we have footage of Ukrainian soldiers walking
through it like they're out on a picnic because there are no Russian forces near
there. And it was hard to explain. Russian military bloggers have come up
with their explanation. They believe that field commanders are basically, they're
telling their boss, oh yeah, yeah, we took that area when they didn't. And in some
cases, they didn't even try. That tracks. That makes sense. It explains the
anomalies that have shown up lately. Now the Russian military bloggers are under
the impression that this is a widespread practice because Russian brass is telling
the field commanders, we need good reports. You've got to get this done. And
they're just saying, okay, you want a good report, here's one. It's not true, but
Here's a good report for you.
And they think that it's widespread.
I can't say that it's widespread.
I can say, I've seen it a handful of times.
But it makes sense.
This is something that could be very problematic later on.
Because once that starts, the information,
it compounds. Okay, well we took half a kilometer today, tomorrow you say the same thing, the
next day you say the same thing, and you haven't moved. That would mean that Russian brass
is devising strategies based off maps that aren't accurate. Which is, I mean, that's
That's a problem.
There's an old saying, if you're ahead of schedule, your artillery will fall short,
meaning being accurate about where you're at.
There's also a whole slacker thing going on in that statement, but being accurate about
where you're at is really important because if you are ahead of where you're supposed
to be when you call for support, it's going to land on top of you because that's where
the enemy would be if you were fighting them. That could compound and it could
cause a lot of issues for Russia if that is widespread. Okay and that leads us to
the question, why do Western analysts trust Russian military blockers? They
don't. They trust their statements when they're complaining. In the United States,
despite all of the recent news coverage, generally speaking most American troops,
most Western troops understand the idea of operational security and they don't
say things that they shouldn't for the most part. There are obvious very high
profile exceptions lately. That culture really isn't there in the Russian
military. What happens is the Russian military bloggers, they have contacts
with people who are in the field, on the lines. And those people who are on the
lines, they tell the bloggers things. The bloggers do their best to not share
harmful information. But when they start complaining, they tend to let stuff slip.
A good example of this would be, and how you can get real information from it, let's say
you have a blogger complaining that the third platoon over here doesn't have ammo.
They don't actually say the unit, they'll just say we have a platoon that doesn't have
ammo and they're on the line.
But if you pair that piece of information with a report from the Ukrainian side that
says, well, we haven't taken fire from here lately, and you find out that three weeks
ago that particular blogger provided really in-depth information that would have to come
from somebody in that platoon, you can assume that's what it is.
You can pull information out.
The other thing that they tend to do when they're complaining is say stuff like, hey,
our maps aren't right, which now that that information is out, if Ukrainian intelligence
can verify that, there's a lot of different ways that that information can be used.
The other thing that they do a lot is they try to talk around things.
Not in their most public forums, but in their version of their Patreon.
They say things and they don't openly say anything that would be harmful, but they talk
around it.
And they talk around it so much that eventually you know what they're saying.
So that's why you have a lot of Western analysts that monitor those channels.
It's not necessarily for their political takes or for their assessment on what is happening,
although sometimes they understand the Russian military culture to a better degree, and you
can pick stuff up that way.
It's just a good source because it's relatively, I don't want to say unregulated, it is more
free than you're going to get from the official statements.
You're getting better information from them than you are from a press release from the
Russian government because that's all, that is all tailored to cast a certain
narrative. The bloggers, their audience is troops. That's who they're
reaching out to, that's who's listening to them or veterans. If they
start lying too much, they'll get called out and they'll lose their audience so
they're not going to want to do that. It's not an infallible source, but it
can be used the way we just did. We have this weird thing that's going on. This is
their explanation for it, and it kind of tracks. So that's why people are leaning
into them. That's why they're monitored as closely as they are. So at
At this point, this conflict is still occurring.
There's no signs of it stopping any time soon, and there's probably going to be a lot of
developments over the winter that normally in wars past when it got that cold, well nothing
was happening.
That may not be the case right now.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}