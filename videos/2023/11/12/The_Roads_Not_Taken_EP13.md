---
title: The Roads Not Taken EP13
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qp4iYEPzMp4) |
| Published | 2023/11/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces "The Road's Not Taken," a weekly series where he covers under-reported or unreported events from the previous week, providing context for viewers and addressing their questions.
- The US and Chinese presidents are meeting to establish and renew communications between their militaries, addressing the anxiety around nuclear weapons.
- Netanyahu is facing pressure in Israel, with 76% of Israelis wanting him to resign, and Blinken advising against a reoccupation of Gaza.
- Russia is reportedly diverting air defenses from its national borders to areas in Ukraine, potentially impacting national defense.
- Beau comments on the lack of progress in the US budget deal, criticizing the extreme sketch from the Republican side and Trump's request for a televised trial.
- Rudy Giuliani is now advertising vitamins on his show, indicative of the current political landscape.
- The Republican Party is exploring reframing their stance on reproductive rights, influenced by Nikki Haley's approach.
- Democrats are pushing to get reproductive rights on ballots in 2024 to drive voter turnout and support their party platform.
- The Pope dismisses a conservative US bishop, signaling a shift in acceptance within the Catholic Church towards trans-Catholics.
- Beau sheds light on environmental reports indicating record-breaking temperatures and the impact on climate change.
- In a Q&A segment, Beau addresses questions on his closing line references, the importance of community organizing for systemic change, and his favorite Star Trek character, Garak.
- Beau explains the complex dynamics behind the lack of ceasefire in Gaza and the political motivations driving certain decisions.
- Beau shares insights on political decision-making, the need for politicians to adapt to new information, and how representation should prioritize the people's will over personal beliefs.

### Quotes

- "What can American citizens do to make any effect or change to what is going on? Organize. Community networks. That is the building block of systemic change."
- "Should politicians change their mind? Yeah. But they're no more likely to do it than the average person."
- "Having the right information will make all the difference."

### Oneliner

Beau informs viewers about global events, political decisions, and encourages community organizing for systemic change, stressing the importance of staying informed.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Join community networks to drive systemic change (suggested)
- Stay informed and engaged with local politics (exemplified)

### Whats missing in summary

Insights on the significance of staying informed and advocating for community-driven change on pressing issues.

### Tags

#GlobalEvents #PoliticalDecisions #CommunityOrganizing #SystemicChange #StayInformed


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Road's Not Taken, episode 13.
It is November 12th, 2023.
The Road's Not Taken is a weekly series we do on this channel
where we go over the previous week's events,
stuff that was under-reported, unreported,
or stuff that maybe got coverage,
but it's definitely something you don't want to miss
because it's going to be context for later,
stuff that wound up on the cutting room floor, so on and so
forth.
And then at the end, we go over some questions from y'all.
If you want to send in a question,
it is questionforboe, F-O-R, boe at Gmail.
OK, so we will start off with foreign policy.
This will put a lot of Gen Z, give them a little bit of a
reduction in anxiety, because this was a big question.
The US and Chinese presidents will be meeting ahead of the
Asia-Pacific Economic Cooperation, something like
that, meeting ahead of a major summit to work on establishing
and renewing communications between the militaries of the
two countries, those who are experiencing the whole
anxiety that comes along with nuclear weapons for the first time, this is part of it.
We talked about it when the questions came in, they're doing it.
They are establishing those lines of communication.
It's worth noting the US and the Chinese militaries, they've had lines of communication about this
to de-escalate things.
They've kind of been turned off since that whole thing with the balloon, but they'll
be bringing them back and probably establishing one at a higher level, would be my guess.
Okay, a poll on November 3rd found that 76% of Israelis want Netanyahu to resign.
Blinken and the U.S. have been advising Israel against a reoccupation of Gaza.
that advice seems to be being ignored for lack of another word. Netanyahu once
again declined a ceasefire. He also reiterated that vague notion that Israel
will somehow retain security control after the conflict. Talking to people who
are somewhat familiar with his thinking more so than I am that they think that
may mean just just like perimeter control which I don't know that that's
what it is because that was kind of in place before that doesn't seem like
something that he would need to clarify moving on to another conflict Russia is
reportedly having to take air defenses away from protecting Russia's national
borders to shore up air defenses in areas that they have currently occupied
within Ukraine. Obviously that will open the door for other events. If you are
pulling away stuff that realistically should be just reserves, stuff that is integral to the air
defense of your country. To fight an elective war, you are losing. It's really that simple.
Moving on to the U.S. At this point, at time of filming, there's no real progress on a budget
deal. Republicans still can't set aside Twitter likes for the good of their country or even their
party. It's really not going anywhere. There was a rough sketch that came out from the Republican
side. It is, it is extreme to, to not to put too fine a point on it. I don't think the Biden
administration or the Senate's going to go for it. Trump's team is requesting that his federal DC
trial be televised. It is worth noting that the court rules don't actually allow for that,
but he appears to be asking for it and supporting this idea in an attempt to manufacture yet another
grievance for his base to be distracted by.
Rudy is now advertising vitamins on his show.
It's as bad a situation as you think from that.
are still trying to find the right messaging to sell stripping away the rights of women
across the country.
They just can't seem to find the right phrasing to convince women to gracefully accept their
place as second class citizens.
There is a serious conversation in the Republican Party right now about how to reframe their
stance on reproductive rights and people are looking to the way Nikki Haley has been talking
about it to give you an idea of where the conversation is at.
Meanwhile Democrats are doing everything they can to get reproductive rights on ballots
everywhere.
They wanted an issue in 2024 that people are voting on directly and yes it's because it
supports their party platform, but also because it is very obvious that that issue drives voter
turnout. If they show up to vote for reproductive rights as an issue, they will probably vote for
candidates who support reproductive rights. So it's not entirely selfless. In cultural news,
The Pope has dismissed a very conservative U.S. bishop. And then in related events to that,
this is the same bishop that is reported to basically have dared the Pope to fire him back
in 2020. It seems like the Pope might have taken him up on that dare. And then in more
Before POPL news, trans-Catholics can now be baptized in some instances.
The Catholic Church is becoming more and more accepting in major ways when it comes to allowing
certain rites to be performed.
And we've talked about this on the main channel a few times.
For a lot of people, especially in the United States, this isn't huge.
For people where the Catholic Church makes up the social fabric of the country, this
is huge.
Like this is life changing in some ways.
In environmental news, there are two reports that were published recently, and they have
different ways of illustrating the same thing.
One is basically saying, hey, you just lived through the hottest year in the last 125,000
years.
The other is saying this is going to be the hottest year in recorded history.
have lived through the hottest summer of your life so far.
Oddities.
Researchers have apparently determined
that a starfish, their arms are actually part of their head.
And in fact, basically a starfish
is nothing but a giant head, short version.
OK, moving on to the Q&A. And if you haven't seen one of these before, I have not seen
these questions before right now.
OK, settle a debate in our family.
We all watch you and this has become a topic of conversation.
My husband says your closing line on the Road's Not Taken episodes references a cartoon and
my daughter says it references a poem. Who's right? The answer is yes. I like things that
have multiple meanings. You know, having, you know, here's some more information and
having the right information will make all the difference. Sure, now you know and knowing
is half the battle. G.I. Joe. That's the cartoon I'm assuming that's what they're talking
about. There is also a poem by Robert Frost and you know the name of this
channel is you know the roads with bow these are the roads not taken very
similar to the idea of you know a road less traveled and when two paths
diverged he took the road last traveled and it made all the difference so
they're both right. The closing line was definitely influenced by both of those.
So okay, Bo, a serious question. What can American citizens do to make any effect
or change to what is going on? This is not a gotcha, and whoever put this here
has put no context to that whatsoever. But the answer is the same. It doesn't
matter what particular issue you're talking about.
What can American citizens do to make any effect or change?
Organize.
Community networks.
That is the building block of systemic change.
It has to start at the bottom.
I wish we had more time on the main channel
go over that. There's a playlist that goes over community networking in different ways,
everything from fundraising to how to structure it. I mean, we even have a book coming out on that.
That's where it's going to have to start. With the machinery that is in place in this country
to maintain the status quo, if you want
systemic change, you have to build your own machinery.
You have to build your own networks.
So, I don't know
specifically what that question was about, but
the answer is organized. The answer is pretty much always organized.
Okay.
In some of your videos you talk about
Garek from DS9, and I was wondering if you'd expand on why he's your favorite Star Trek character.
Okay.
So for those who are not familiar with the series,
Garek was a bad guy for a lack of a better term in the series. He was on the wrong side.
And
as in he was a spy. As
As time went on, he was a tailor, and just a simple tailor, but he might prefer peace,
but he didn't forget how to be violent.
The reason I like that character is because, A, I like when something good comes from something
bad. And B, he's a very real character. You would know it from looking at him, but there
aren't a whole lot of people who are all good or all bad. And a lot of times, many people
People who are doing good are doing so because they used to do bad.
The character and the ethical questions that get addressed, I find them interesting, especially
in a series that, yeah, it provides a lot of social commentary, but it's normally pretty
black and white, and Garrick is gray in a whole lot of ways, and the real world is gray.
I said that thing, you know, Garrick did nothing wrong talking about that one famous episode
where he definitely stepped outside the lines of what is normally acceptable in that world,
in the Star Trek world.
If you boil down what he said at the end, you saved this many lives and you did this
horrible thing.
It makes a series that often has...the series has conflict in it.
It has wars.
But the wars are always good guys and bad guys.
Very well defined.
In real life they're not, and his character really helped illustrate that.
Why don't Palestinian forces release the captives to get a ceasefire?
They don't seem to be offering them any protection.
Meaning having the captives isn't protecting them.
That has to do with the leadership.
People are going to extremes to differentiate between the forces and the Palestinians, which
is good.
There probably also needs to be a distinction made between the leadership of the forces
everybody else. For the leadership, many of whom are somewhere very safe, what is
happening there is in their plan. The strategy that's being used, the video is
on this channel with the whiteboard. The strategy that is being used requires an overreaction.
It requires all of the bad things that are happening to happen. That doesn't mean that
Palestinians want it. We are talking about the leadership. We are talking about the top
10 people. The strategy that they're deploying requires that because it generates the outrage
which creates recruitment and potentially might bring another state
actor in on their side. The leadership doesn't want a ceasefire any more than
Netanyahu does. They both think victory is right around the corner and they're
both wrong.
I want to be super clear so that this doesn't get taken out of context.
This is about the leadership of these organizations.
They don't want to ceasefire either.
The Palestinian people, the people who are paying the price, the people who aren't in
a palace in another country, they definitely want one.
But the leadership doesn't.
Because the outrage that is being generated around the world helps them.
It's good for them.
So that probably has a whole lot to do with it.
They're not motivated to actually get a ceasefire.
Not the people who, not the people who make the calls. Okay.
Note, this is in context of Gaza.
Are there any other viable options?
If not, tell us why we should vote for Biden aside from the very obvious
This potential 45 disaster.
I know that this is surprising.
I don't tell people how to vote.
I really don't, and I don't endorse politicians.
The only politician I've ever endorsed was Big Bird.
The only actual endorsement I've ever made was Big Bird running against Ted Cruz.
I don't do that.
If you are weighing it between Biden and the alternative from the Republican Party, even
in context of Gaza, Biden's better.
Are there any other viable options?
At this point in time, it would depend on what you mean by viable.
There are a couple of chains of events that could put other people on the democratic ticket,
but I don't know that any of those are likely.
You know, you have a number of people that are kind of waiting in the wings that haven't
really said that they're interested.
You have Marianne Williamson.
You have a number of people who, if the right set of dominoes fell, they could be in that
position.
Now, if you are talking about a third party candidate actually winning, incredibly unlikely,
almost impossible, and I have videos on that, even if they win, they're starting at the
wrong side. That's the top. The president doesn't actually get to set the legislation.
So if you have a third party president, but the Republicans and Democrats still control
the House and the Senate, they can't really do much. This is why when you organize, you
to organize at the bottom and you have to work your way up.
Maybe you can send a message, but with the current risks of Trump being there as the
alternative, I don't know that it's the appropriate time to send a message.
Now that being said, there are a lot of things that might take Trump off the Republican ticket.
And all of that changes.
We're still a year out.
There's a whole lot of variables.
So I would not determine who you are going to vote for a year before you're going to
asked to do it.
Should politicians change their mind?
If they don't, if they don't change their mind despite losing on an issue, they're
labeled as not learning their lesson.
If they change their mind, they're unprincipled.
say politicians take their stances to get and stay in power. I mean, yeah, but I don't
think that fits them all. No, it doesn't. You have some people that are ideologues across
the board. You have some people who are true believers in one particular cause, and most
of those people, they won't change their mind and they won't change the subject. Generally
Generally speaking, the way a representative democracy should function in theory, the politician's
mind doesn't matter.
What they believe doesn't matter.
They should vote in accordance with the people that they're representing and what they want
them to do.
We are so accustomed to having rulers that we don't really acknowledge that the representatives
You're really just supposed to be a proxy vote.
So should politicians change their mind?
Everybody should change their mind.
When you're presented with new evidence, new information, you should change your mind.
If it directly conflicts with what you currently believe.
But that's not just a politician thing.
human thing. People don't like admitting they were wrong. So politically, it depends on the issue.
As an example, the Republican Party, yeah, they can change their stance on a budget item right now.
No, no big deal.
Their stance on reproductive rights that they have stood behind for half a century, they're
going to have to slowly walk away from that.
And that's why they're spending so much time trying to repackage it, because they know
they can't just let it go, which is the smart thing politically.
So they're trying to slowly move away from it now that the dog has caught the car.
But most politicians are people who are interested in power.
And they do take the positions they take to get and stay in power.
There are exceptions to that shirt, there are exceptions to everything.
The way them changing their mind or not changing their mind, the way it gets framed, you're
looking at how their political opposition frames it.
If you are a true believer in something, I mean, my advice would be don't take criticism
from people you wouldn't take advice from.
But the real issue here is thinking that a politician's personal opinion should matter.
When you're talking about a representative, it really shouldn't.
The representative should enact the will of the people in their district.
They really shouldn't have to make a lot of decisions.
The people in their district should make those decisions for them.
Now when you get to the executive branch, it's a little bit different because you need
ideally, somebody in the presidency, they would have certain qualifications.
But generally speaking, in the modern era, they don't.
So I mean, we're not even looking for the right qualifications there.
So I mean, to answer the only real question that's in this, should politicians change
their mind?
Yeah.
But they're no more likely to do it than the average person.
In fact, they're probably less likely to do it.
Because they will be attacked for flip-flopping or whatever.
In fact, when you have a politician who changes their stance, it is normally big news because
it doesn't happen that often.
Okay, that looks like it.
Those are all the questions.
So I hope that helps.
Provide a little bit of context, a little bit of information, and having the right information
and will make all the difference.
God, have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}