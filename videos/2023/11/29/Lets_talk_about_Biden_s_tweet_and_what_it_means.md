---
title: Let's talk about Biden's tweet and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SDtnM8vYn_w) |
| Published | 2023/11/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's tweet about Hamas and a ceasefire sparked reactions from many, with some viewing it as a positive step towards peace.
- A Washington Post article details internal divisions within the White House on the Israel-Gaza issue, shedding light on diplomatic efforts and decision-making processes.
- Biden's visit to Israel aimed at calming tensions and preventing a full-scale conflict, with the US foreign policy team successfully deterring Israel from launching a major operation.
- The US adopted a "bear hug" diplomacy approach, supporting parties closely to prevent escalation into a wider regional conflict.
- Trump's approach to foreign policy, particularly his use of Twitter, is contrasted with Biden's more strategic and less publicized diplomatic efforts.
- Biden's tweet, signaling support for a ceasefire, may indicate a shift in US policy towards Israel, but its public nature could impact diplomatic negotiations negatively.

### Quotes

- "We can't do that."
- "It's not good news."
- "We're in this together."
- "That's bad news, actually."
- "You'll have a good day."

### Oneliner

Beau dives into Biden's tweet on Hamas, Washington Post article on White House divisions, and the US diplomacy approach, hinting at potential shifts in US policy towards Israel and the impact of public statements on diplomatic efforts.

### Audience

Diplomacy observers

### On-the-ground actions from transcript

- Reach out to local representatives or organizations to advocate for peaceful solutions in conflicts (implied).
- Stay informed about international relations and diplomatic efforts to understand the implications of public statements (implied).

### Whats missing in summary

Insights into the potential consequences of publicized diplomatic efforts and the need for nuanced approaches in international relations.

### Tags

#Biden #ForeignPolicy #Diplomacy #Israel #Hamas


## Transcript
Well, howdy there, internet people. Let's bow again.
So today we are going to talk about Biden's tweet
and what it means, and we're going to talk about a Washington Post article
and what that means, and then we're going to kind of circle back around
and talk a little bit more about foreign policy and diplomacy
and how things work at times.
Okay, so if you have no idea what I'm talking about, Joe Biden from his personal, I guess,
account tweeted something out, it says, Hamas unleashed a terrorist attack because they
fear nothing more than Israelis and Palestinians living side by side in peace.
To continue down the path of terror, violence, killing, and war is to give Hamas what they
seek. We can't do that. There's a whole lot of people who are looking at this
and saying this is great. Biden is finally doing something. He's calling for
a ceasefire and that's how they're reading it. You have a lot of people
celebrating that tweet who want a ceasefire. There's also the idea that
this is the first time he's done something. A tweet. There's a Washington
Post article and it came out on November 26th. It's called White House
Grapples with Internal Divisions on Israel-Gaza and it details a lot of
things. Not too long ago I said that when this was all over and the books and
memoirs finally come out at the end of the administration and all of that I
I think there's going to be a lot of surprises. Well, they started now, I think, for a lot of
people in this article. So, when Biden went to Israel, his purpose was to calm, let's quote,
the Israelis and try to stop the ground offensive. That's what the reporting says.
While he was there, he said that the US would not support cutting off fuel, water,
electricity, and aid. The foreign policy team, they were apparently
successful in deterring Israel from launching a full-scale operation against a group in
Lebanon as well.
If that had happened early on when tensions were still incredibly high worldwide, yeah,
that would have caused it to spread.
That would have caused it to spread.
would have probably been the spark for that regional conflict. And on the
channel, that's how I've described it. Spreading, widening, regional conflict.
According to the article, they had, I guess, different terminology in the White
House. More along the lines of, come on, Jack, I'm trying to stop World War III
here. The US adopted what they're calling a bear hug form of diplomacy and that's
basically saying we support you so much and holding them so tight that while they
can't swing, trying to calm things down.
These activities go back to mid-October that are detailed in this.
Okay, so the way people reacted to this tweet made me realize something.
People expect Trump... people expect what Trump did when it comes to interacting with
foreign countries and making statements like that.
Reminder, Trump was absolutely horrible at foreign policy.
One of the largest complaints you will see on this channel from back then is that he
He telegraphed everything he did on Twitter.
It is not normal for foreign policy teams to do that.
This tweet that Biden put out, if you want a ceasefire, this is bad news, actually.
The U.S. diplomatic teams, they have been working trying to keep the pause in effect.
They've been putting a lot of pressure on keeping other countries out of it, trying
to stop it from turning into a regional conflict.
You didn't get tweets about it.
If I had to read this and try to guess what it meant, my guess is that the White House
has been informed that Israel has absolutely no interest in establishing a
peace framework right now. So rather than the bear hug approach that they're
making statements in public now hoping that that will influence people. If you
want to ceasefire the tweets actually bad. It would be better if he wasn't
saying anything. Something changed for this to go out. There's going to be a
shift in US politics and US policy when it comes to Israel, but this isn't, this
This is not part of that.
If you look at it, this is very much something that's still about Israel.
We're in this together.
We can't do that.
It's still that type of diplomacy, but it's had to go public now.
It's not good news.
remember that despite the the habits of the previous president tweeting out
foreign policy decisions and what we're going to do and all of that stuff that's
bad and most most most presidents in other countries don't do it in fact that
That may be the best way to do it.
What foreign leaders that are well-known have social media presences like that?
Katarov?
None of them are good at foreign policy, let's just leave it at that.
They're ego people.
It is worth noting that the Washington Post article said that at some point during a meeting
people said, hey, you know, you're going to end up losing votes if you don't, you know,
change your stance and do something like this.
And that apparently made it get very contentious because the administration was not thinking
about reelection at that point they were thinking about quote stopping World War
III. They didn't put it on Twitter. Anyway, it's just a thought. You'll have a
good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}