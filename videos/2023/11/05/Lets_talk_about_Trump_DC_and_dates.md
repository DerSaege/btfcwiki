---
title: Let's talk about Trump, DC, and dates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bZcrdblnolQ) |
| Published | 2023/11/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on the DC case involving election interference and federal election interference case.
- Jury selection set for February 9th by Chutkin, with the trial scheduled to start on March 4th.
- Trump's strategy appears to be centered on delaying legal processes through appeals.
- The judge in the DC case doesn't seem inclined to let delay tactics hinder proceedings.
- Trump's delay tactics might be rendered ineffective due to the advanced stage of the case.
- Speculation arises about the connection between the jury selection dates and the appeal on the gag order.
- The judge's focus seems to be on maintaining the established trial schedule rather than reacting to external pressures.
- Pressure is mounting on individuals associated with Trump as trial dates draw near.
- The case, despite not having many co-defendants, is expected to have a significant impact on Trump world.
- Efforts to delay the case until after the election are likely to face opposition from the judiciary and the Department of Justice.

### Quotes

- "Trump trying to do everything he can to delay it and the judge trying to get the situation resolved and not allow delay tactics to overrun her courtroom."
- "There are a lot of people who might be called to testify or have already agreed to testify, who have provided testimony to the grand jury."
- "It does not seem like the judiciary or the Department of Justice is willing to allow that."

### Oneliner

Updates on the DC case involving election interference, with Trump's delay tactics facing resistance as trial dates approach.

### Audience

Legal Analysts

### On-the-ground actions from transcript

- Stay informed about the developments in legal cases and their implications (suggested)

### Whats missing in summary

Insight into the potential consequences of the DC case on Trump and his associates. 

### Tags

#DCcase #Trump #LegalStrategy #ElectionInterference #Judiciary


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Trump
and attempted delays and the DC case
and how that is progressing and some dates
that are fast approaching.
OK, so this is about the DC case dealing
with the election interference and all of that stuff.
This is the federal election interference case.
Chutkin has set jury selection for February 9th.
Trump's strategy when it comes to pretty much all legal cases, it seems, is delay, delay,
delay.
Appeal, appeal, appeal.
to get everything stopped and just push the process off as much as possible for
as long as possible. The judge in the DC case seems super unwilling to allow
those tactics to work. The jury selection is that this would be the first
questionnaire. It's a process when you're talking about a federal jury,
especially one like this. It's going to be a process, but the first questionnaire
gets filled out on February 9th, and the trial is set to start on March 4th. So we
are now looking at 90 days to jury selection, more or less, probably closer to
a hundred, and then four months until the trial starts, setting those dates, it kind
of puts, it puts Trump's delay tactics in a weird position because what, what is possible,
that could definitely occur is he continues to file his appeals and because
of how far into the process they are in the actual case, the appeals may be
rendered moot. So it's basically a race. Trump trying to do everything he can to
delay it and the judge trying to get the situation resolved and not allow delay
tactics to overrun her courtroom. So that move, there are a lot of people who are
connecting it to the appeal on the gag order. I don't necessarily think those
are connected because she indicated that, she kind of indicated that if she felt it
was necessary, she would move the trial up.
If she felt that he was going to subvert justice via Twitter, that her move would be to move
the trial up.
I don't think that's what this is.
I think this is her trying to keep to the schedule that was established and I
feel like a lot of people in Trump's circle are going to start feeling more
and more pressure because this isn't a date that's far out now, this is right
around the corner and while this case doesn't have the giant list of
co-defendants attached to it, it is safe to assume that there are a lot of people
who might be called to testify or have already agreed to testify, who have
provided testimony to the grand jury, and it's going to have an impact overall on
Trump world. That's what most of them are trying to avoid. They want to delay it,
get it past the election, in hopes that once Trump is in office, well then he can
just avoid it altogether. It does not seem like the judiciary or the
Department of Justice is willing to allow that. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}