---
title: Let's talk about China, talks, and treaties....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KHf14PbXQzY) |
| Published | 2023/11/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gen Z expressed concerns about China having 500 nuclear warheads.
- China aims to achieve parity with the US in terms of nuclear capabilities.
- Talks between the US and China are set to regulate the strategic and nuclear situation.
- China's stance is focused on security balance and maintaining a deterrent.
- China insists on its no-first-use policy regarding nuclear weapons.
- The talks may address US efforts to enhance nuclear power deployment.
- China's starting position for talks involves expressing concerns about the US-Australia relationship regarding nuclear submarines.
- Some view this as an opening to potentially involve China in the treaty for the prohibition of nuclear weapons.
- The communications lines between higher-level people might be established during these talks.
- The situation between the US and China seems to be starting off on a reasonable and less confrontational note.

### Quotes

- "China aims to achieve parity with the US in terms of nuclear capabilities."
- "China insists on its no-first-use policy regarding nuclear weapons."
- "The talks may address US efforts to enhance nuclear power deployment."

### Oneliner

China and the US begin talks to regulate nuclear situations with a focus on security balance and deterrence, starting off on a reasonable note.

### Audience

International observers

### On-the-ground actions from transcript

- Contact organizations working on nuclear disarmament for updates and ways to support (implied)
- Attend public forums or events discussing nuclear policies and treaties (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current US-China talks on nuclear capabilities, shedding light on China's stance and the potential outcomes of the negotiations.

### Tags

#US #China #NuclearTalks #SecurityBalance #Deterrence #ForeignPolicy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about China treaties and talks.
And I get to give some good news for a change.
It's nice, especially when it comes to this topic.
So if you remember a couple of weeks ago,
Gen Z, they were a little bit stressed, judging by my inbox.
I got a whole bunch of messages from people
who are part of Gen Z, who saw the headlines,
saw the report saying that China now has 500 nuclear warheads
and it was concerning.
And we went over the amount of nuclear warheads
that the US has and how all of this works
and basically said, it's China trying to achieve parity,
this isn't something to worry about,
there's nukes everywhere the whole time you've been alive,
this shouldn't keep you up at night and said that more than likely what would happen would
be there would be tensions.
Something would occur and China would experience something similar to the hopefully not as
bad as the Cuban Missile Crisis and at that point talks would start and those talks would
be about how to regulate the strategic situation, the nuclear situation between the United States
and China. So that was most likely because I'm just totally unaccustomed to anybody being proactive
on the foreign policy scene. It's just not something that we see happen anymore. But next week
those talks are happening without tension, which is cool, and China seems to
have a super reasonable stance, which is also very nice. Chinese state media, the
editor-in-chief of one of the outlets, put out I guess what would count as an
op-ed, but generally speaking when you're talking about the editor-in-chief of a
a of an outlet that is basically an arm of the government, it's it is as good as a press
release from the Chinese government.
And basically, they said that the talk should center on explaining that China is concerned
about rising strategic risks, and they want to achieve, quote, security balance, parity.
That's what they're looking for.
want to make sure they have a deterrent. Realistically, they already have one, as
evidenced by Gen Z's reaction. You do the math, that's 10 per state. They have a
deterrent, but they probably want more. The op-ed says that China has to
maintain its no-first-use policy, which is good. And then it should basically
chastise the United States for constantly attempting to increase its
nuclear power. Not necessarily by building more nukes, but by coming up
with better ways to deploy them. Well, yeah, I'm gonna say that's probably a
talk that should be had and then basically kind of express their
irritation when it comes to the US and Australia's relationship when it comes
to the nuclear subs. That's their starting position for these
conversations. Super reasonable. There are people who are saying that this is the
opportunity to try to get China to come on board with the treaty for the
prohibition of nuclear weapons. No, don't even set that as an expectation. Way too
early in the game for that. Way too early in the game for that. I would be
absolutely shocked if that was even talked about right now. So basically it's
It's the last video, only we didn't have to have the scary part.
China's starting position, I mean, when you look over all of it, the US is not going
to give up the nuclear sub thing with Australia.
else is very, very nice. It's a perfect place to start conversations. My guess is the communications
lines that we talked about in that video, so higher level people could talk to each
other immediately. That might be something that comes up during this. It's just nice
to see a situation start to be diffused before it actually becomes a situation. It's
a nice change. So that's the news. There's no bad part to go along with this. It's
it's all proactive. There's nothing in their starting position that has been
released that even looks like it would really cause a problem. I'm sure the
thing with the subs is it's one of those things that China will constantly say
hey you shouldn't do this and the US will be like yeah okay. Just like the US
is constantly like hey you shouldn't pick on Taiwan and China's like yeah okay
and nobody gives anything on those. Yeah so for those who were concerned about it
particularly those who sent in the emails, it looks like you might be able
to skip out on the scary part completely because the talks are starting without
that happening and they're starting from reasonable places. So this may be, you
know, there's been a lot of comparisons to the Cold War. This may be more of a
just chilly war as far as the tone of the near-peer contest with China.
Starting off, the positions are just way more aligned and way more reasonable.
China is very happy with its place in the world.
So it may not be as confrontational as a lot of people pictured when this first started
because everybody was basically just looking at the Cold War and like, yeah, I've already
seen this one.
So yeah, all good news.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}