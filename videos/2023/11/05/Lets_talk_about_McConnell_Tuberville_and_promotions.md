---
title: Let's talk about McConnell, Tuberville, and promotions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DxNR_zl10Zw) |
| Published | 2023/11/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tuberville's hold on promotions is causing issues, leading McConnell to take action.
- Republican senators are set to have a conference to address the situation.
- McConnell is attempting to reach an agreement to move forward.
- Some Republican senators are open to pushing the promotions through in bulk.
- Nine Republicans need to join Democrats to change rules and push promotions through.
- There seems to be enough support for this based on the current talks.
- The senator from Alabama will likely have to end his political stunt next week.
- This stunt has disrupted the lives of many officers and their families.
- The disruption has had negative impacts on recruitment, retention, and readiness.
- The Department of Defense (DOD) did not give in to Tuberville's demands.
- It is no surprise that the DOD stood firm against the disruptions.
- The chapter of obstruction caused by Tuberville may be coming to an end soon.
- The situation damaged various aspects of officers' lives just for headlines and recognition.
- Other Republicans did not support Tuberville's actions, indicating a lack of unity.
- This period of disruption should hopefully come to a close in the upcoming week.

### Quotes

- "The hold that he has placed on promotions, it seems as though McConnell has finally had enough."
- "The senator from Alabama will have to give up his political show, the stunt that has disrupted the lives of hundreds."
- "The chapter of obstruction is finally coming to a close."

### Oneliner

Tuberville's hold on promotions leads McConnell to take action, with Republican senators convening to address the disruptive stunt likely coming to an end soon.

### Audience

Legislative watchers

### On-the-ground actions from transcript

- Contact relevant senators to express support or opposition to bulk promotions (suggested)
- Monitor updates on the situation and potential resolutions (implied)

### Whats missing in summary

The full transcript provides a detailed account of Tuberville's disruptive actions and the potential resolution through McConnell's intervention.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Tuberville and time,
as in it's running out.
The hold that he has placed on promotions,
it seems as though McConnell has finally had enough.
There is supposed to be a conference of Republican senators next week where they will try to
work out some kind of agreement to move forward.
This comes after a very unpleasant show between Republicans on the Senate floor.
So McConnell seems to be stepping in, pulling the entire conference together, and trying
to come up with some kind of agreement that people can work through.
It is worth noting that those who took him to task on the Senate floor have, they haven't
said that they'll vote to push them all through in bulk and change the Senate rules and all
that stuff, but they have indicated that they are super open to the idea. I feel
as though if this conference does not yield results, that is exactly what they
will do. And it seems to me that they would not make that statement if they
didn't have the other votes. For that to work, there would be, they would need nine
Republicans to cross over and vote with Democrats to change things and be able
to push it through. It certainly seems like they have the votes for it by the
way they are talking. That's not something that you would say, oh well I
would do that if the votes weren't there. So all indications right now are that
And sometime next week, the senator from Alabama will have to give up his political show, the
stunt that has disrupted the lives of hundreds, hundreds of officers waiting for their promotions,
their families put on hold, their kids in schools, just all kinds of stuff.
And apparently, I mean, he got some headlines and name recognition out of it.
Disrupted all of that, damaged recruitment, retention, readiness, so he could be in the
paper.
It is basically what the trade-off looks like at this point.
It is unsurprising that DOD refused to cave to his demands.
I feel like somebody should have told them this was going to happen before it started.
But given the fact that no other Republicans joined in on his little temper tantrum, I
mean that in and of itself should have been a sign.
So it does look like that chapter of obstruction is finally coming to a close, and should end
this coming week.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}