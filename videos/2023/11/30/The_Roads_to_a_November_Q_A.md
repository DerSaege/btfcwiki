---
title: The Roads to a November Q&A
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=L8t1_-YENL0) |
| Published | 2023/11/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the lack of media coverage on Democratic presidential candidates beyond the incumbent.
- Exploring rural Americans' awareness of living by leftist economic principles.
- Sharing thoughts on how Biden might respond to Kissinger's death.
- Clarifying the concept of independents in the U.S. political landscape.
- Responding to questions about the MyPillow guy's intelligence and grooming his beard.
- Touching on fundraising platforms for charity live streams.
- Sharing insights on the Israeli-Palestinian conflict and the importance of preserving life.
- Explaining the significance of utilizing on-platform donation features for fundraising efforts.
- Offering advice on fundraising strategies for community causes through live streaming.
- Wrapping up with a reminder on the power of accurate information and context.

### Quotes

- "The candidates out there, so you've got Williamson, you've got Dean, the other three, I'm not sure who the other one is."
- "At the risk of sounding elitist, do rural Americans generally know they're living by leftist economic principles?"
- "The primary thing at this point should be the preservation of life."
- "You let them take their cut and make it up. It just seems to go smoother and it seems to encourage more people to get involved."
- "A little bit more context, a little bit more information, and having the right information will make all the difference."

### Oneliner

Beau delves into media coverage of Democratic candidates, rural Americans' economic awareness, Biden's response to Kissinger's death, independents in U.S. politics, MyPillow guy's intellect, beard grooming, fundraising platforms, Israeli-Palestinian conflict, and efficient charity fundraising strategies.

### Audience

Community members

### On-the-ground actions from transcript

- Host a fundraiser live stream for a good cause using on-platform donation features (suggested).
- Utilize live streaming to raise funds for community causes (implied).

### Whats missing in summary

Insights on Beau's perspective on the Israeli-Palestinian conflict and the importance of prioritizing human life during conflicts.

### Tags

#DemocraticCandidates #RuralAmericans #Independents #Fundraising #IsraeliPalestinianConflict


## Transcript
Well, howdy there internet people, it's Beau again and welcome to the Roads with Beau.
This is a Thursday episode and we are just doing a Q&A and maintaining the schedule of
putting something out on Thursday.
These are your questions, I haven't seen them yet so I will give you the best answer I can
with absolutely no preparation.
Something that always makes me a little nervous to be honest.
Okay, so getting started.
We've talked about the importance of name recognition when it comes to the other three
Democratic candidates for president.
Why is there no good reporting or comparison charts or whatever?
Oh, so is the media intentionally not reporting on them to avoid giving them name recognition?
No, there's a tradition generally that the incumbent president gets to run for reelection.
It's really that simple.
I don't think there's any kind of great conspiracy.
It's just how it's been done so the media isn't leaning into it.
You didn't have a lot of media looking for people to primary Trump or Obama or any other
former president who was running for reelection.
It's just not something that commonly happens.
The candidates out there, so you've got Williamson, you've got Dean, the other three, I'm not
sure who the other one is.
They get their message out as best they can, but realistically, they are there to push
a message, not necessarily win, and I think most of them know that.
They're more to shape the conversation.
How do you think Biden will respond to Kissinger's death?
How do you think he should respond?
He's already hurting among young progressives.
I fear Biden is living in a world where Kissinger is a well-respected former Secretary of State.
That's a distinct possibility.
How do I think he'll respond?
Probably going to the funeral and having some nice words, observing the traditional standards
that were set when it comes to something like that.
How do I think he should respond?
He is a very, very busy man, and I would totally understand if he was unable to attend given
everything that he's dealing with right now.
At the risk of sounding elitist, do rural Americans generally know that they're living
by leftist economic principles?
Wow, I thought that was going in a completely different direction.
Okay.
At the risk of sounding elitist, do rural Americans generally know they're living by
leftist economic principles?
Are they even aware they're outside what's considered typical for an American economic
experience?
Is this leftover poison from the Red Scare?
What can we do about it?
One question at a time.
Do rural Americans generally know they're living by leftist economic principles?
No.
A lot of them view left and right as a cultural thing, not an economic thing.
And even though they will participate in a co-op and have a union job and engage in mutual
aid and do all of the leftist economic theory, they are socially conservative.
Obviously we're talking about generalities here.
So it's hard to even broach this subject at times.
Are they even aware they're outside what's considered typical for an American economic
experience?
Yes.
That part, yeah.
They totally get it.
But they view it more, oddly enough, it really seems to be associated with like a rugged
individualist rural thing, more than leftist thing, which is funny because it is very much
left? Is this leftover poison from the Red Scare? No, it's a thing in the United States.
Generally speaking, people see the Democratic Party as left. It's not. What can we do
about it? I don't know that you can. I don't know that there's much to do. You know, you
can try to bring those principles to the suburbs, which might be more helpful than trying to
convince rural Americans that what they've done their entire lives is leftist. It might
it be easier to use the example of what they've done to try to reach people in the suburbs
that are slightly more willing to change than rule people.
There's a notorious stubborn streak.
How do you feel about Kissinger's death personally?
Personally?
I did not know him.
I mean, I know what you're asking.
I spend a great deal of my time talking about how foreign policy is in hopes that people
can change what it is.
I do not agree with Kissinger's view of the world.
I am not a fan, and I will leave it at that.
These were too funny not to include together.
You're such a posing woke, I can't say that on YouTube,
you're as white as can be.
Why do you have a black elf?
OK, so first, she's Latina.
OK, second, there's actually a video about this.
We have a Latina elf because my wife met somebody just like
you when she was buying one.
And the person pulled her over to where the white elves were
and said, oh, you want these.
And my wife didn't like that.
Like, the funny thing is, had she not encountered a bigot
at that moment, we would probably
have a white elf because that was the one that was, you know, closest. But she was walking
around and I guess it bothered the person. And yeah, so I find that funny. Yeah, she's
Latina, not black, just to be clear. What's the Elf's name? Libertat.
Did you see the shooter in Vermont has a DV history? Yeah, I've seen a little bit
about that. If you've missed this, and something that is, you know, completely
surprising, the person who has been accused of shooting the three
Palestinians. They have a history with domestic violence in some way. It's not
exactly clear how it all transpired, but we do know that his girlfriend a while
back basically begged the department to take away a firearm from him. And if you
If you don't know, if you haven't seen any of those videos, the overlap between DV histories
and mass incidents is huge.
It is huge.
And it's just one of those things that consistently comes up.
Our independent votes, our independent voters between the Dems and the Republicans, because
logically speaking, if you're leftist center, your closest representation are the Dems,
equally for Republicans on the right. So a far left voter would most likely feel offended if
called a Dem. I don't know if offended is the right word, but they might often point out that
they're not actually a Democrat. But they surely won't vote Republican. So where are the independents
and what do you mean with that strictly speaking? It's not that typical to be in a party here in
Germany and we have like five to seven big ones. Are independents simply not members of a party
or voters that aren't clearly in one or the other's camp.
Okay, so in the U.S., independent means you are not a member of the Republican or Democratic
party, really.
There are other parties in the United States, but most times they get counted as independents.
The majority of independents are not independent in the way you're thinking.
Yes, there are a whole lot of people who are far left that are not Democrats.
They do tend to vote for the Democratic Party.
But when you hear people doing political analysis, those aren't the ones they're talking about.
They count those as progressive Dems.
The independents are really swing voters.
They're centrists between the Republican and Democratic party.
When people are talking about it, that's who they're talking about.
There are independents that are not part of that group, but they're not a huge amount.
bulk are those who swing back and forth between the Republican and Democratic party, either
because they have a set of beliefs that doesn't change, and they know what their principles
are, and they look for the candidate that best aligns overall, or they really don't
have a strong political or ideological framework, and they vote for who they like, obviously
a generalization.
Okay, oh, and this goes on.
Let's say this really far left person is not a Democrat or a Republican, and their views
are so far outside the Overton window that really no party or candidate that has a chance
say getting a majority would align with them.
OK, so this is what happens with the left independents here.
They get lumped in with dems.
But in the end, if this person isn't a pure ideologue
and wants to affect positive change by voting,
they are still always voting democratic.
By that, are they a dem?
No, they're still like they would
counted in that when you're talking about people talking about left and right and the
way things are swinging.
But when you are talking about polling on independence, in that case, they would be
considered an independent.
We need more parties in the United States.
We really do.
There is a large group of people that really, they vote Democratic pretty much every time.
But they're doing so not because they truly support the Democratic Party or the positions
being offered, they view them as less harmful.
choosing their opposition, is what's happening, or they're voting out of self-defense, you
know.
Okay.
Is the MyPillow guy as dense as he seems, or is it all an act?
A person who built a multi-million dollar company can't be dense.
I accept that maybe he inherited it all.
Based on what I've seen, I feel that he is probably
intelligent.
But there may be another issue.
And that would be one of those things
that to me falls outside of what I would cover.
I don't, I feel like there might be another issue that needs to be addressed with him,
but I don't think that he is dense.
Would you consider grooming your beard for those of us who appreciate a well-groomed
beard, maybe for the roads not taken?
What like on video?
Um, yeah, I don't know what this means, I'm going to be honest, other than I do know that
my beard has been extra messy lately.
But on the upside, this is normally around the time of year where I actually trim it.
So maybe you'll get that soon.
Um, yeah, I don't think I'm going to do beard care routines if that's what this is asking
for or it could just be, I don't know.
OK, moving on.
I've been wondering why you haven't made any videos on the Israeli-Palestinian war,
at least not beyond Biden's moves to calm things.
I don't expect you'll voice a strong stance on it, but it would be nice to hear your perspective.
I have recorded a number that I haven't released.
I actually do have a strong sense on it, but right now it's not really...
It is not one that most people want to hear.
Short version, it's all waste.
Nobody's going to be happy.
Neither strategy that is being deployed is going to work.
And the primary thing at this point should be the preservation
of life, and I think that that's what's important more so than anything else.
There are a lot of talking points that are used to support one side or the other that
are not things that can happen.
And because that's the goal they're trying to achieve, it's all for nothing.
version. I've been watching you for a long time. I want to first thank you for all you do. Now,
to my question, I do live streaming on TikTok with a community of 3D printing enthusiasts,
and we would like to start doing a fundraiser live stream to raise money for a good cause every
year. We have several causes that we are thinking about raising money for but we
have no idea how to get started. I was wondering if you could suggest a good
fundraising platform to use. I have looked into give lively and give butter.
I'm not familiar with those a little bit but I have no experience and was
wondering what you use or what you would suggest. Any help would be greatly
appreciated. In my experience, it is best to use the live
streaming feature that allows people to donate.
And I know the immediate reaction from people is, well,
the platform takes a cut. If you're doing this and you're
monetized, you have ad revenue. When trying to direct people off of the
live stream, you lose some of them. You don't, you're not going to make as
much. I don't know if TikTok has the ability like YouTube does, but when
When you're trying to direct people off platform, you're creating one more step.
If it's something simple that they can do from their phone in the app that they're already
in or whatever, it's easier.
If it was me, I would recommend using whatever mechanism the platform you're on has, and
If they take a cut, make it up out of your ad revenue or explain that even with them
taking a cut, I would be willing to bet more money would actually get to the cause.
Like if you don't have enough to make up the ad revenue or make up what they take.
it's one of those things where you can sit there and live stream and raise ten
thousand dollars relatively quickly but if you were to try to direct people off
site to somewhere else you might only make two, three. So even if the platform
takes 30% and you can't make it up you're still talking about seven
thousand instead of four thousand. Now obviously this is me assuming that you
were talking about like a one-shot deal for the live stream. If it's going to be
a campaign where you're constantly directing people there, maybe it works. I
I have, I've never tried it that way.
I'm, I'm of the opinion that you let the, let them take their cut and make it up.
It just seems to go smoother and it seems to, it seems to encourage more people to get
involved. When you see it during the live stream, the money coming in, it helps people
know, hey, look, they're going to make their goal. It encourages people to give.
If you're going to do a fundraiser, I have a video on this channel that is lengthy, that
It goes through some steps.
And we've done them every year, except for the last one,
because that would, you know, if you missed it,
there was a technical error that sent the live stream live
before we were ready.
But what is outlined in there, we
have done year after year after year, and it works.
I think it's like the roads to fundraising
for your community network or something like that.
I would watch that and make sure you apply those principles
if you're going to do some kind of campaign.
I don't know.
Maybe people in the comments will know a little bit more
about these sites.
But generally, you want people to see it happening rather
than going somewhere else.
least that's that's my opinion on it okay and that looks like it that looks
like all of the the questions so okay well a little bit more context a little
bit more information and having the right information will make all the
difference. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}