---
title: Let's talk about Georgia and the 3 who don't get a deal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dvBa4qCSLjY) |
| Published | 2023/11/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fulton County initially had no interest in making any agreement with Trump, Giuliani, and Eastman in the Trump case.
- Recent reporting suggests a change in the people involved in potential agreements, with Eastman now being considered for a deal.
- The state of Georgia has a deadline of June 21st for any negotiated plea agreements in this case.
- Any plea agreements beyond June 21st will not be entertained, and maximum penalties may be recommended.
- One of the co-defendants has requested to move the deadline for plea agreements up to start the trial sooner.
- Fulton County's stance of not wanting to negotiate with certain individuals indicates they believe their case is strong.
- This could be a tactic to induce lower-level individuals to take a deal early by showing the option is available.
- The prosecution's confidence in their case against Trump and Giuliani appears to be strong.
- The situation suggests that the prosecution believes their case against Trump and Giuliani is solid.
- There may have been changes or events that led to the shift in who they are willing to negotiate with in the case.

### Quotes

- "Fulton County believes their case is strong."
- "The prosecution believes their case against Trump and Giuliani in particular is ironclad."

### Oneliner

Fulton County's refusal to negotiate with certain individuals in the Trump case signals a strong prosecution case against Trump and Giuliani.

### Audience

Legal analysts, political commentators.

### On-the-ground actions from transcript

- Move to support legal teams working on cases of public interest (suggested).
- Stay informed about developments in legal cases that impact the political landscape (suggested).

### Whats missing in summary

The full transcript provides detailed insights into the shifting dynamics of plea agreements in the Trump case in Georgia, offering a nuanced look at the prosecution's confidence and strategies.

### Tags

#Georgia #Trump #LegalCase #PleaAgreement #Prosecution


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump and Georgia
and how attitudes are apparently shifting in Georgia,
at least a little bit.
Not too long ago, there was reporting,
pretty reliable reporting that suggested
Fulton County, they had absolutely no interest in working out any kind of
agreement or deal with three people in the Trump case. And that was Trump,
Giuliani, and Eastman. The reporting said those three, they didn't want to deal
with them. They wanted to take them to trial. More recent reporting says it's
It's still three people, but the people have changed.
They don't want to make any agreement with Trump, Giuliani, or Meadows.
Eastman is apparently, apparently it would be okay to make a deal with him now.
That's what the reporting says.
it kind of indicated that literally everybody else has been told you can
work out a deal or negotiations have started or completed or whatever. I mean
that's interesting. That is interesting. You know, one of the obvious questions
is when will we find out? Well, the state has a calendar for something like this
And I want to say it's June 21st, it's June, I know that much.
I'm pretty sure it's June 21st.
And that's the last date that anybody on this case can work out a negotiated plea.
After that, I think the state said that they would recommend the maximum penalty and would
not entertain any negotiated pleas beyond that date.
In what I'm sure is a completely unrelated development, one of the co-defendants has
put in a motion to move that date up.
Not have it in June, but have it sooner.
Make sure that all the plea agreements get in sooner, that that deadline is sooner.
So in theory, the trial could start sooner.
Eastman wanted it moved up.
So what can we take away from all of this?
The first thing to acknowledge is that the initial reporting could have been wrong.
Eastman could have always been somebody that they would have been willing to enter into
some agreement with. Or something changed. Maybe Meadows made them mad or you know
there could be a whole bunch of different little things. But the key part
to take away from this is Fulton County believes their case is strong. Not
just enough to indict, not just enough to convict, but enough to say those people
we're not even going to cut a deal with them. That is a generally a pretty strong
indication that they are very secure in their case. Now the other thing it could
be is them trying to induce those people at lower levels to take a deal early
early, just kind of saying, this option is available for y'all.
They don't get it, but you better take advantage of it.
It could be that, but I feel like it is a very clear indication that the prosecution
believes their case against Trump and Giuliani in particular is ironclad.
That's that's the takeaway from this. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}