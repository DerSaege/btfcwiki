---
title: Let's talk about reactions to the Kissinger news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QqLSAW-1fQ8) |
| Published | 2023/11/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Henry Kissinger's death prompted varied reactions, with some celebrating him as a hero and others condemning him as evil.
- The division in opinions on Henry Kissinger stems from differing views on American dominance and foreign policy.
- Foreign policy, as discussed by Beau, is portrayed as being about power rather than morality or ethics.
- Kissinger's career spanned decades, with notable incidents in Cambodia and Chile showcasing his impact and the divide in opinions on him.
- Kissinger applied foreign policy dynamics in a unique way, which led to either admiration or condemnation based on one's perspective.
- Opinions on Kissinger are polarized based on whether one believes the ends justify the means in foreign policy.
- Kissinger is seen as someone who epitomized the belief that foreign policy is solely about power and influence.
- His actions in Chile and Cambodia serve as clear examples of how he exerted American influence.
- The reactions to Kissinger's passing mirror the ongoing debate surrounding his legacy and the effects of his foreign policy decisions.
- The celebration and criticism of Kissinger's life and achievements stem from fundamental differences in perspectives on foreign policy and power dynamics.

### Quotes

- "Foreign policy is not about morality, it's not about ethics, it's not even about humanity, it's about power."
- "If you are somebody who cringes when I say foreign policy is not about morality, not about ethics, and not about humanity, you are not going to like Henry Kissinger."
- "He was the walking embodiment of foreign policy is about power."
- "The reactions started coming in. The main question, why is he so polarizing?"
- "When you look into Chile and Cambodia, you will get a very clear picture of that."

### Oneliner

Henry Kissinger's death sparks polarized views on foreign policy, power dynamics, and his legacy, reflecting fundamental divides in perspectives.

### Audience

Historical scholars, foreign policy analysts

### On-the-ground actions from transcript

- Analyze Kissinger's impact on foreign policy (suggested)
- Research the historical context of Kissinger's actions (suggested)

### Whats missing in summary

Deeper insights into Kissinger's controversial legacy and the ongoing debate about American foreign policy.

### Tags

#HenryKissinger #ForeignPolicy #PowerDynamics #Legacy #AmericanInfluence


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about some news
and the reaction to that news and the questions prompted
by the incredibly varied reaction because something occurred.
And as soon as it occurred, the commentary started to pour out.
And it's divided.
And it's divided in a very unusual way.
oftentimes you can find a dividing line when it comes to ideology.
You have people of all kinds of ideologies right now
holding somebody up as either a hero or literally the devil.
If you have missed the news, Henry Kissinger is no longer with us.
So, after the commentary went out,
the questions started coming in.
The main question, why is he so polarizing?
Why are all of these people celebrating?
And why are all of these people holding him up in this way?
What is going on here?
The answer's pretty simple.
When we talk about foreign policy on this channel,
I talk about the way it is.
Not necessarily the way I think it should be.
You have often heard me say,
foreign policy is not about morality,
it's not about ethics,
it's not even about humanity,
it's about power.
What seems to be very little consideration.
What's that?
You're objectively a horrible person
with horrible goals, but you're gonna be allied
to the United States, if you take power,
what do you need to take power?
That's how he can be summed up.
If you are somebody who views American dominance
as like the greatest thing, if you are somebody
who believes that the way foreign policy exists today
is the way it has to be, and those people
who play the game that way while they're just doing what has to be done and justifies
the means type of thing, you might look at Henry Kissinger as a hero.
If you do not like the way foreign policy exists, if you are familiar with some of the
effects of it, you very well might look at Henry Kissinger as literally the double.
The career of Kissinger lasted decades, decades.
To go through every single thing, it would be a three hour video.
If you want to isolate a couple of incidents that can showcase it, look at Cambodia and
look at Chile.
Those two locations in particular will give you a clear divide and you'll see why some
people who are very much in favor of spreading democracy as it is, look at
Kissinger as one of the most effective diplomats in the world. You will also see
why there are a number of organizations and entities who wanted to have him
tried. Those two incidents are probably the quickest way to see the divide, and
where it comes from.
Henry Kissinger was not the creator of foreign policy, of the dynamics that
exists today.
But he applied it in a way that, realistically,
Nobody else did. And whether or not you think that makes him a hero or whether
or not you think that makes him evil, that is going to depend largely on
how you look at the world. If you are somebody who cringes when I say foreign
policy is not about morality, not about ethics, and not about humanity, you are
not going to like Henry Kissinger. If you are somebody who hears that and you're
like, well, well, that's the way it is.
Henry Kissinger is going to be somebody that even if you don't agree with the
things that he did, you'll say the ends justifies the means. He was the walking
embodiment of foreign policy is about power.
and to my knowledge never deviated on anything when it came to trying to exert
influence and spread American influence and American power. When you look into
to Chile and Cambodia.
You will get a very clear picture of that.
You will see the effects and you will see how that influence is applied.
So that is why you have some people celebrating and some people talking about what he accomplished.
Anyway, it's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}