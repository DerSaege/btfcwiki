---
title: Let's talk about Georgia and modifications....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eQ0cdXmuFVQ) |
| Published | 2023/11/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Georgia case involving Harrison Floyd and social media posts.
- Prosecution tried to revoke Floyd's bond for social media posts.
- Judge found Floyd committed a technical violation but didn't remand him.
- Modified conditions for bond: no public statements about codefendants or witnesses, limited social media posts.
- Judge's decision shows the court's authority in the case.
- Possibility of similar modifications for others involved in the case.
- Co-defendants likely to become more vocal on social media.
- Violation after modified conditions may lead to remand.
- Future developments in the case to be watched.
- Implications of the judge's ruling on social media use in legal proceedings.

### Quotes

- "Modified conditions for bond: no public statements about codefendants or witnesses, limited social media posts."
- "Judge's decision shows the court's authority in the case."
- "Co-defendants likely to become more vocal on social media."
- "Violation after modified conditions may lead to remand."
- "Implications of the judge's ruling on social media use in legal proceedings."

### Oneliner

Georgia case involving Harrison Floyd and social media posts leads to modified bond conditions limiting public statements and social media posts, showing court authority and potential impact on co-defendants.

### Audience

Legal observers, social media users.

### On-the-ground actions from transcript

- Stay informed on legal proceedings and decisions (implied).
- Monitor social media use in legal cases and its consequences (implied).

### Whats missing in summary

The emotional impact on Floyd and other individuals involved in the legal proceedings. 

### Tags

#Georgia #HarrisonFloyd #legalproceedings #socialmedia #courtauthority #co-defendants


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Georgia
and Harrison Floyd and the proceedings
and how they moved along and what the end result was
and where it goes from here because it has some unique,
some unique downstream effects.
Okay, so if you missed the news
and have absolutely no idea what I'm talking about
the Georgia case dealing with Trump in Fulton County, the district attorney's
office there, they tried to revoke the bond of Harrison Floyd and remand him,
meaning go to jail. And the reason they did this was social media posts. And
basically the end result of this was they didn't remand him, which is rare.
Normally, when the prosecution asks for somebody to be remanded, they normally get their way.
In this instance, the judge found that Floyd had committed a, I believe, technical violation
was the term, and then said that not every violation compels revocation.
So instead, there has been a modified set of conditions for bond.
Short version is to make no public statement about any codefendant or witness, anybody
specifically named in the indictment, and basically can't post on any social media
about the case like it all, almost.
It is incredibly tailored to limit Floyd's ability
to talk about people involved with the case.
Now, the interesting part about this
is that by doing this, the judge is declaring
This is within the court's ability, specifically in this case.
I would not be surprised if we saw other people involved in the case who are active on social
media to see a similar issue.
It seems like the DA's office might ask for that modification for other people.
I don't see why they wouldn't, honestly.
So that's one of those things that we can kind of be waiting for.
Because I feel like as this moves forward, some of the co-defendants are going to become
more and more direct with their statements about people who are actively
involved in the case. But at this point it does not look like the judge is ready
to remand people over this. I would suggest that if a violation occurred
after the conditions were modified that would be a lot more likely. So that's
what has occurred in Georgia and what we can kind of expect as it moves along.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}