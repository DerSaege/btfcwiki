---
title: Let's talk about Colorado, Boebert, and initiatives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IYKO3jKvstI) |
| Published | 2023/11/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Colorado ballot initiatives driving voter turnout through reproductive rights and rank choice voting.
- Democratic Party strategically putting initiatives to boost voter turnout for progressive causes.
- Ballot initiatives likely to impact the re-election chances of vulnerable candidate Boebert in Congress.
- Boebert won her last election by a narrow margin, making her a vulnerable candidate.
- Uncertainty about Boebert's future in Congress depends on voter turnout and Republican Party dynamics.
- Far-right faction of the Republican Party facing challenges and lack of significant accomplishments for their base.
- Boebert is popular among the far right and may still drive turnout despite the ballot initiatives favoring Democrats.
- Democratic Party aiming to leverage similar initiatives in 2024 to mobilize voters based on rights and strategic voting.
- Anticipating voters to prioritize their rights over lukewarm candidate support.
- Political strategy of leveraging ballot initiatives likely to benefit Democrats in future elections.

### Quotes

- "Democratic Party is trying to get things on the ballot for 2024 to drive that turnout for them."
- "They will show up to vote in favor of their own rights."
- "Boebert won her last election by like 546 votes."
- "She is in many ways seen as kind of a star within their movement."
- "It's a sound political strategy, and it'll probably work."

### Oneliner

Colorado ballot initiatives driving progressive voter turnout, impacting Boebert's re-election chances, with Democratic Party strategizing for 2024 based on rights-focused mobilization.

### Audience

Political activists and voters

### On-the-ground actions from transcript

- Mobilize voters in Colorado for upcoming ballot initiatives (suggested)
- Participate in rank choice voting initiatives to drive progressive voter turnout (implied)

### Whats missing in summary

Detailed analysis of the potential impact of ballot initiatives on the political landscape in Colorado and beyond.

### Tags

#Colorado #BallotInitiatives #VoterTurnout #DemocraticParty #ReproductiveRights #RankChoiceVoting


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today we are going to talk about Colorado
and some ballot initiatives and what those mean
for somebody who gets talked about pretty frequently
and just kind of run through the dynamic
because while this is a little different,
you will probably see the same dynamic
show up in other places during the next election.
OK, so we're doing this because I got a message.
And it says, hey, after Ohio, you
said you'd expect the Democratic Party
to put things on the ballot about preserving
reproductive rights and all of that.
You were right.
It happened in Colorado.
First, this was already in the works before Ohio.
So that was already on the way.
I would expect to see more.
And then it went on to say that commentators are saying that this spells the end of Boebert,
that she won't get re-elected because of this.
And I want to know if that's true.
Probably, probably.
My understanding is that there are two different ballot initiatives in Colorado that will
drive, not even democratic on one of them, progressive, drive progressive voter turnout.
One is, of course, the reproductive rights.
Being able to enshrine reproductive rights in the Constitution of Colorado, that's going
to drive democratic voter turnout.
It looks like there's going to also be something about rank choice voting.
is going to drive progressive voter turnout and by that I mean people who
are progressive enough to look at most Democratic candidates and be like
Republican like those people because they're in a situation right now where
they very rarely get to vote for somebody they actually support. They're
voting out of self-defense. If rank choice voting was instituted they could
show up and vote for a candidate that they actually agreed with, rather than one that
they disagree with the least, which is the situation they're in most of the time.
Both of those are likely to drive voter turnout that will trend blue.
As far as it being the end of Boebert's career in Congress, she won her last election by
like 546 votes.
She's definitely a vulnerable candidate.
I would suggest that these two things being on the ballot, that could easily drive 550
that would go the other way. So it depends. It depends on the same thing
that elections always depend on. Who shows up? The Republican Party as a whole,
particularly the far-right faction of it, they're not really in a great space
right now. You know there's a lot of talk and they're keeping their
spirits high but they're not exactly they're not exactly doing well and they
haven't delivered anything for their base. In fact you've had conversations
about that and statements about that in Congress now where Republicans are like
give me one thing that I can go home and campaign on and it's crickets. So there
is that. However, Boebert, and I know this isn't going to... I know a lot of people
watching this channel, like, it's not gonna make a lot of sense, but a lot of
people from the far right like her. Which is, I mean it is what it is. She is in
many ways seen as kind of a star within their movement. So she may still drive
turnout. But the the general take of these ballot initiatives, these things
being on the ballot, that's going to help Democratic candidates? Yeah, yeah, that's
definitely right. So much so that I would expect that, and I haven't looked since I
said that initially, but I would expect that the Democratic Party is trying to get things
on the ballot for 2024 to drive that turnout for them. Because people, they may not show
up to vote for candidates that they are lukewarm on.
They will show up to vote in favor of their own rights.
And they're probably hoping that, well, if they show up to vote for that, they'll vote
for the person they're lukewarm on rather than somebody they actively dislike.
And it's a sound political strategy, and it'll probably work.
So yeah, I definitely think that those initiatives are going to cause an issue for Boebert.
But it's worth remembering, this was underway before I said that.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}