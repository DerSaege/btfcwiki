---
title: Let's talk about changing times and minds....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kSMhIht40YY) |
| Published | 2023/11/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Posing the question of whether people can change their minds, not just on a topic but overall, and shares a personal anecdote about Thanksgiving.
- Describing viewers as being on a bus together, all wanting change in the world, with varying degrees of commitment to the cause.
- Stating the importance of believing in individuals' capacity for change to work towards a better world.
- Emphasizing that change in individuals is gradual and requires patience and persistence.
- Using the example of a family member making a joke about Pilgrims as religious fanatics as a sign of potential change influenced by past interactions.
- Encouraging not to write off people's potential for change based on initial appearances or beliefs.
- Stressing the necessity of continuously striving to help individuals shift their perspectives, even if progress may be slow.

### Quotes

- "You have to believe that individuals can change."
- "If you want the world to change, you have to be willing to accept the idea that individuals can."
- "Just because somebody is wearing a red hat doesn't necessarily mean that they have forsaken all reason."
- "You have to believe that people can change, that individuals can change."
- "It shows a shift and you just have to keep working at it."

### Oneliner

Beau poses the question of whether individuals can change, stressing the importance of belief in human capacity for progress and the need for continuous efforts towards influencing change.

### Audience

Viewers

### On-the-ground actions from transcript

- Engage in constructive dialogues with individuals holding different beliefs (implied).
- Persist in efforts to help shift individuals' perspectives over time (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on the gradual nature of change in individuals and the necessity of maintaining belief in people's capacity for transformation despite initial appearances.

### Tags

#Change #Belief #Individuals #Perspective #Progress


## Transcript
Well, howdy there, internet people.
Let's battle again.
So today, we are going to talk about change,
and buses, whether or not it's possible,
and how all of it kind of comes together.
Because I got a message,
and I think it's an important question
because there's a lot of times that people will,
just instinctually, just due to dealing with it so much,
probably feel this way.
I appreciate all your videos about how to structure arguments with friends and
family.
They come in handy.
My question is kind of simple.
Do you really believe
that people can change their minds?
Not just on a topic, but overall.
It seems so hopeless sometimes.
My family is already here for Thanksgiving.
Last year I talked about the myths of Thanksgiving,
and this year
My uncle jokingly referred to the Pilgrims as religious fanatics.
And I thought that was change.
But I have no doubt he's still going to vote for a Trump clone.
Okay.
I would say that most people watching this channel, you're on that bus.
We've talked about it before.
who want change, all on a bus together. Some people going much much further, some
people only going a little bit, but all headed the same general direction. You
want the world to change, you want a different system, you want a system that
gives everybody a fair shake, that kind of thing. That is the overwhelming
majority of people watching this channel right now. There's arguments now below
all the time in the comments section, but most people want that change and some
kind of it. You want to change the world. So to that question, do you really
believe that people can change their minds not just on a topic but overall? I
mean I kind of have to, right? Don't have a choice. If I believe the world can
change. I have to believe that individuals can because one thing
doesn't happen without the other. It doesn't work any other way. If you do not
believe that people can change then you might as well just give up striving for
better world. You have to believe that it's possible. It's not going to be an
event for most people. They're not just going to suddenly change their mind.
They'll be swayed over time. Their ideas will shift slowly but surely and that's
That's what causes the change.
Sometimes it's imperceptible.
But if you want that better world, you have to believe that individuals can change.
If not, just give it up because there's no way it happens any other way.
bus, that bus analogy, some people are only going to go five or six miles, some
are going five or six thousand, but as long as you can get people headed in the
same direction, it's a win. And him referring to the pilgrims as religious
fanatics. That's probably a pretty good example of that because odds are he got
that line from Wednesday Adams. But when he saw it, the reason he understood it,
the reason he found it funny, the reason he decided it was something that he
could quote might have been your conversation the year before. It's
gradual but make no mistake about it if you want the world to change you have to
be willing to accept the idea that individuals can and it's probably not a
great idea to write them off and just be like well never gonna change and sure
that's true about some people some people won't change but you don't know
who they are so just because somebody is wearing a red hat doesn't necessarily
mean that they have forsaken all reason. I mean I know that that may be hard to
believe at times but if you want if you want that world you have to believe that
people can change that individuals can change and you have to be willing to try
to help them along. It may not work and the best you ever get may be them
recycling a line from a show they saw on TV. But it shows a shift and you just
have to keep working at it.
Anyway, it's just a thought.
You have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}