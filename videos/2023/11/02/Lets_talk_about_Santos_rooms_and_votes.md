---
title: Let's talk about Santos, rooms, and votes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=R0C52oaFnP8) |
| Published | 2023/11/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The vote to expel Santos from the U.S. House of Representatives failed with the help of 31 Democrats.
- The official reason given for not expelling Santos was to wait for the House Ethics Committee to finish their work.
- Questions arose about why 31 Democrats voted to keep Santos in the House.
- Speculation suggests a possible backroom deal to secure Republican votes for the budget and avoid a government shutdown.
- Santos is already off his committees, and Congress is embroiled in a fight over Tlaib's censure.
- It's theorized that a Democrat may have offered a dinner invite to a Republican in exchange for Washington insight to solve these issues.
- The failed vote to expel Santos was juxtaposed with the failed vote to censure Tlaib by Republicans.
- There may have been a quiet agreement made behind the scenes regarding these votes.
- The secrecy of such agreements is emphasized by the rule of not talking about the room where it happens.

### Quotes

- "The first rule of being in the room where it happens is that you don't talk about the room where it happened."
- "Questions arose about why 31 Democrats voted to keep Santos in the House."
- "It's completely possible that maybe a Democrat offered a dinner invite and a Republican responded with Washington insight, and they decided to solve one problem with another."

### Oneliner

The vote to expel Santos failed with the help of 31 Democrats, sparking questions and speculations about backroom deals in Congress.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to express your views on transparency and accountability in congressional decision-making (implied).

### Whats missing in summary

Insights into the potential impacts of backroom deals on political decisions. 

### Tags

#Congress #Transparency #BackroomDeals #PoliticalDecisions #Democracy


## Transcript
Well howdy there internet people, let's bow again. So today we are going to uh we're going to talk
about Santos and the vote and rooms and how that happened and why. If you missed the news
the vote to toss Santos from the U.S. House of Representatives it was taken and it failed it
failed with the help of 31 Democrats. 31 Democrats voted to not expel Santos. Now the official
reason that they have given for this action, those who have commented, it can basically
be summed up as we don't think it's right to expel him until the House Ethics Committee
finishes their work. That's generally the overall talking point. And I mean, sure, maybe
that's what happened. I mean, politicians are known for always telling the truth. But
there were a lot of questions, and they all contained this general theme.
Bo, why did 31 Dems vote to keep George Santos in the House? Do you think they voted that
way to secure enough Republican votes to pass the budget and not shut the
government down. We can dream, huh? Yeah, I mean, that's a dream. That would be a
big ask. That would be a big ask to keep Santos in the House. You got to give us
the budget votes? That doesn't seem likely, but you may not be that far off.
The general idea here is that some, you know, backroom deal was made.
look at the situation, what do you have? You have Santos in trouble, but he's
already off his committees. Meanwhile, Congress is fighting over Tlaib's
censure. It isn't pretty. So it's completely possible that maybe a
Democrat offered a dinner invite and a Republican responded with Washington
insight, and they decided to solve one problem with another. Somebody probably
arranged the meeting, the venue, and the seating. So yeah, Santos looking at
being expelled. That failed with the help of 31 Democrats. Tlaib was going to be
censured, that vote failed with the help of 23, 24, 20-something Republicans. I mean
maybe that's just a coincidence. I mean, it could be. It could just be
happenstance that a bunch of Republicans chose not to censure a Democrat, and I
believe the censure was actually over comments related to Israel at this time
in this political climate. They just decided to do that because they're good
people. I mean, sure it could happen. I think more than likely there was probably some quiet
agreement that was made. If it was, that is what happened, we'll never know. Because
the first rule of being in the room where it happens is that you don't talk about the
the room where it happened.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}