---
title: The Roads to Reaction and Response
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=remNAoBgC5g) |
| Published | 2023/11/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the desire for immediacy in today's world and its potential pitfalls.
- Instant communication and access to global events shape people's reactions.
- The impact of instant information on fostering understanding and breaking down bigotry.
- The downside of instant exposure to global horrors without filters or delays.
- The influence of AI-generated content on shaping opinions and demands for immediate action.
- The difference between reacting instinctually and responding thoughtfully to global events.
- The dangers of acting impulsively based on emotions rather than reasoned responses.
- The importance of learning to respond thoughtfully rather than react emotionally.
- The need to process information, especially sensationalized or distressing content, with care.
- The power dynamics at play in harnessing public desires for action for personal gain.

### Quotes

- "Hot takes, there's a lot of them out there that are, they're well-meaning, they really are and looking at them like I can tell they are well-meaning, but they'll make it worse."
- "We have to train ourselves to think a little bit longer, to process the information just a little bit more before we form an opinion that we are willing to defend."
- "It's totally okay to say, I don't have enough information about this to have a strong opinion."
- "There is no way you understand it all. Nobody can."
- "Having the right information will make all the difference."

### Oneliner

Exploring the dangers of instant reactions in a world driven by immediate information, urging thoughtful responses over impulsive actions.

### Audience

Social media users

### On-the-ground actions from transcript

- Take time to process information before forming opinions (implied)
- Be willing to change opinions based on new evidence (implied)
- Seek out accurate information to make informed decisions (implied)

### Whats missing in summary

The full transcript provides a comprehensive exploration of the impact of instant information on shaping reactions and the importance of thoughtful responses in a fast-paced world.


## Transcript
Well, howdy there, internet people.
It's Beau again.
And welcome to the Roads with Beau.
This is going to be a little bit different for this channel.
But I feel like this is the appropriate place
to do this one, because it doesn't really line up
with the other content.
I guess today we're going to talk about the roads
to immediacy and the desire that people have for that today.
and how that desire is sometimes self-defeating.
And it doesn't lead to good outcomes.
See, we have all grown very accustomed
to living in a world where we have instant communication.
We see things instantly all over the world.
That information can travel, those images, those videos,
They can travel in an instant.
Right now, I am certain that I could pull up and be within a
mile of where troops are currently advancing to cut
Gaza in half.
And I could nail down their position within a mile from
the other side of the world.
information. And that's good because it provides instant communication and in
many ways it breaks down bigotry and hatred in a lot of ways. You can foster it in
others, but generally speaking you fear what you don't know and you hate what
you fear. The more familiar people become with other parts of the world, other ways
of looking at things, the less unknown it is, the less it's feared, the less it's
hated.
It's good.
It's a good thing.
At the same time, we are subjected to the horrors of the world instantly.
No filter, no newsroom saying, I don't know if that's fit for public consumption.
just out there. You stumble across it. And because you see it instantly, you want
something done instantly. You want it fixed. That's the normal response to
seeing bad things is wanting it fixed. And we have a new evolution in this
because we have AI that is generating images and videos that aren't real and
people are demanding solutions to things that didn't happen. They are reacting to
events that never occurred. And it leads to a lot of opinions that are based in
in the heat of the moment. Because it is seen instantaneously and people have the ability to
transmit their idea about it instantaneously, it leads to a lot of opinions that people will
later regret that they expressed with a global audience. Or maybe not a global audience is the
the way they're looking at it right now, but one that will be accessible by a global
audience later.
Hot takes and all of that stuff.
The immediate information, that immediate access to information, good and bad, makes
people want an immediate course of action set to address whatever the issue is.
They want it solved now because what they're witnessing on their screen gives them a moral
injury.
They can't stand to see what they're seeing.
So they want it fixed now.
They want somebody to do something.
And I get it.
That being said, doing something isn't always the right answer.
What happens is it creates this loop where people are saying do something, do something,
do something so much they latch on.
That's something, so it must be done.
Even if it's not the right move, even if it's a reaction, rather than a response.
Reaction is instinctual, it's something that just occurs.
The way you feel when you see images from all over the world, when you see horrible
things, that's a reaction.
The emotions that are generated when you see that, it gives you ideas that are based in
reaction.
And a lot of the horrible things that we see, they're happening because of a cycle of action,
reaction, reaction to the reaction.
Doing something isn't always beneficial.
Doing something to mitigate, doing something to address the issue is, but something could
make it worse.
With humanity's newfound gift of instant communication, being able to receive and send
it, it becomes very important for everybody to learn the skills of response, of making
sure that what you're putting out into the world is well thought out.
can be wrong. There's nothing, there's nothing wrong with being wrong about
something. But you want it based in thought, not emotion. Because most of the
horrible things that have occurred, they're occurring because people are
making decisions either out of self-interest, which that'll always be
there, or emotion, and most times those who are making moves in support of their
own self-interests, they rely heavily on those who will support them based on a
reaction rather than a response.
Hot takes, there's a lot of them out there that are, they're well-meaning, they really
are and looking at them like I can tell they are well-meaning, but they'll make it worse.
make the horror on your screen worse.
You know, my daughter
dressed up for Halloween as Hunter S. Thompson.
And I found out, here's something to note, especially for
younger people watching this, that's a real person, not just a character that
Johnny Depp played.
That is a real person.
He's a journalist, had a very, very long career,
spanned from way back then when fear and loathing was made,
which is apparently how he's known, to the 2000s.
In September of 2001, he wrote something for ESPN.
He started writing it on the 11th.
It's one of the most insightful reactions that I've ever seen because the commentary
is actually kind of based on the reaction.
I'll link it down below, but in it he says we are going to punish somebody for this attack
But just who or what will be blown to smithereens for it is hard to say.
Maybe Afghanistan, maybe Pakistan, or Iraq, or possibly all three at once, who knows?
Not even the generals in what remains of the Pentagon or the New York papers calling for
war seem to know who did it or where to look for them.
If you want to see the ultimate hot take, it's that one.
That's a tiny, tiny section of it.
Because in his reaction, he looked at the response.
looked at what was happening around him and how it would be interpreted around the world.
Because we all have audiences now, it doesn't matter how big your social media presence
is, the algorithm will make sure that you have an audience.
People see it and it influences things.
It's important for all of us to train ourselves to respond rather than react.
So the influence that we are putting out into the world is positive, not one that continues
to feed the cycle of reaction.
We have to take the time to look past the horror.
That horror, it's always been there.
It's always been there.
And I'm not talking about anything specific right now.
We just didn't see it instantaneously.
And oftentimes there were filters in place.
the average person didn't see it. They didn't suffer that moral injury. And I go back and
forth about that. You know, I often will tell people, like, you don't need to watch this.
But there are times when I'm like, everybody should see this. And I know deep down that's
That's not right, not everybody should, but there are some things that I think if people
were more aware of, they wouldn't be happening.
I got a message the other day from somebody that was asking me how I could sleep at night
Because I didn't talk about a paper that had been leaked that was talking about something
horrible happening.
And it was one of those things that kind of hit me and it made me realize how much the
way we interact with information shapes world views.
that paper, yeah, it's about something horrible and that is about what everybody's thinking
about right now.
But that's something that could happen.
The reality is, what's being discussed in that paper, it is happening.
It's happening in Ukraine.
It's happening in Congo, in the Democratic Republic of Congo, Sudan.
I think Somalia, or maybe Ethiopia, Myanmar, it is happening.
And that shows how that instant information alters the way we see things.
Because that was presented, right?
People saw that.
And it's horrible, and so they react to it, unaware that their worst fears about that,
what they're worried about happening is happening, just somewhere else.
We have to learn how to better process all of the information that we have access to.
Most people on their phone have access to the entire base of humanity's knowledge.
And there is a constant influx of more information.
And realistically, the more sensationalized and the more horrible it is, the more it spreads.
It creates a situation where people only see the horrible aspects of it.
They don't get that moment to look for the helpers, so to speak.
Or when there are people helping, they didn't get there fast enough.
That was one that kind of blew me away, watching people drive to get supplies, watching them
drive into a literal war zone.
And most of the hot takes were that they didn't get there fast enough.
That should be a sign.
That probably doesn't need any additional commentary.
Overloading yourself with the images that not everybody is well-equipped to see.
It leads to a reaction, and it can lead to bad things, not because the person reacting
is a bad person, but because they're a good person and they want it stopped.
But believe me when I say, our betters, those that have the power all around the world,
they will take your desire to do something and they'll give you something to do.
may not be in anybody's best interest but theirs.
They will harness that energy and direct it somewhere, somewhere that helps them.
We have to train ourselves to think a little bit longer, to process the information just
a little bit more before we form an opinion that we are willing to defend.
It is totally okay to say, I don't have enough information about this to have a strong opinion.
There's nothing wrong with that statement.
You have access to the entire base of humanity's knowledge.
There is no way you understand it all.
Nobody can.
It's okay to take a moment and think things through.
It's okay to change your opinion when presented with new evidence, and it's important for
the causes that you care about that you do that because there are people who
will harness your your well-meaning activity to make horrible things more
horrible because it benefits them. We need to work on getting the right
information. And having the right information will make all the difference.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}