---
title: Let's talk about what it takes to run for President....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HJP0STOhyqY) |
| Published | 2023/11/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the impact of money on running for president in the United States.
- Mentions a hedge fund billionaire and GOP mega donor, Cooperman, having issues with the current Republican Party and preferring progressives over Trump.
- Points out the significant money problem Trump might face due to lack of support from big GOP donors.
- Talks about Kennedy, a member of the Kennedy family, running on a third-party ticket against political elites.
- Notes that Kennedy is receiving more money from previous Republican donors, possibly aiming to attract both disaffected Democratic and Republican bases.
- Indicates a divide within the Republican Party with fundraising news not looking good for them.

### Quotes

- "He is a divisive human being who belongs in jail."
- "Trump has an issue, a big one, a big mega donor issue."
- "Words don't mean anything anymore in the United States."
- "None of them are good for the Republican party."
- "Republican fundraising is in disarray."

### Oneliner

Beau explains the impact of money on presidential campaigns, from a GOP mega donor's issues with Trump to Kennedy's unique fundraising approach, signaling disarray in Republican fundraising.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact local political organizations to stay informed about fundraising dynamics (suggested).
- Join fundraising efforts for candidates that represent your views (implied).
- Organize community events to raise awareness about campaign finance issues (exemplified).

### Whats missing in summary

Insights on the importance of grassroots support and donor diversity in political campaigns.

### Tags

#PresidentialCampaigns #PoliticalFundraising #GOPDonors #ThirdPartyCandidates #CampaignFinance


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about what it takes to run
for president in the United States, and that's money.
And we're going to talk about how running
can be impacted by money, and where
candidates' money comes from or doesn't come from
can tell us a whole lot.
So we're going to talk about two different pieces of news that
are related under that theme.
The first thing we're going to talk about is a guy named Cooperman.
He is a hedge fund billionaire and a GOP mega donor.
But he is having some issues coming to terms with the current Republican Party.
I think most people watching this channel would agree if the statement was, it would
be a terrible thing for the country if Donald Trump were to be reelected.
That's a quote from him, GOP megadonor.
He also said he's a divisive human being who belongs in jail, and he even said that
he would rather take a chance with progressives than a would-be dictator.
A hedge fund billionaire would rather take a chance with progressives.
who realistically would take half of the money he makes every year, at least, then
he would Trump. Now I need to reiterate something. He is a GOP mega donor. This is
not a liberal guy in any way shape or form. People that he has supported in
the past include Rubio, Bush, DeSantis. He's not a liberal by any definition at
all. In the current presidential primary, he threw a little bit of cash to Chris
Christie. So what you can see from this is Trump has an issue, a big one, a big
mega donor issue. In the past, he didn't need them, not as much as other
candidates. He does now. He definitely does. Now, those small amount donors that
that he's been relying on, that money is going to be eaten into a whole lot by
legal fees. If he wants to run, he needs money and he's going to have to get it
somewhere and this is probably very very telling when it comes to the attitude of
other GOP mega donors. Trump's gonna have a money problem. So many jokes to
make right there. So we can lean into that and look at that and realize that
that the big dollars in the Republican Party, they want somebody else to run.
Okay, the next piece of news has to do with Kennedy.
Kennedy, I think by now he is officially running on a third party ticket.
He is running a campaign against the, quote, elites, the political elites.
A Kennedy who's actually, like, from that family.
Words don't mean anything anymore in the United States.
I don't know how you can run a campaign against the elites when your last name is Kennedy.
If there was a political family that defined political elite in this country, it would
be his.
But that's neither here nor there.
Let's talk about his money.
He is pulling more money from previous Republican donors than Democratic donors.
Now there's two ways you can look at that.
is they believe he will pull more votes from the Democratic candidate than he
would from the Republican candidate so they're donating to him as a spoiler.
And that's possible but I think they've got that read all messed up. Kennedy's
views on certain topics particularly those related to public health are
are probably going to make him very unappealing to most Democratic voters.
Where those views, they would be welcome on the Republican side of the aisle.
And given the rhetoric that he is using, he seems to be aiming for
for the disaffected Democratic base and the Republican base.
He's taking more money from Republicans.
The other thing that could mean is that they actually support him.
That's the other thing it could mean.
You have a couple of pieces of fundraising news coming out.
None of them are good for the Republican party.
It shows a Republican party that is very similar to House Republicans.
They're not united, they're divided, very, very, very much so.
The other thing that Kennedy has that is unique, and I definitely think this is worth mentioning,
he seems to be activating people.
that don't have a history of donations are donating to him. So much like I think
West might be, somebody who could get people who normally aren't engaged in
the electoral process engaged, it seems Kennedy's doing that, at least when it
comes to donations. So that is something else to watch for. And at this
point, as I have repeatedly said, it is way too early to tell what is going to
happen in the next election. But what this tells us, and the one thing we can
take away from this that actually matters, is that right now Republican
fundraising is in disarray. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}