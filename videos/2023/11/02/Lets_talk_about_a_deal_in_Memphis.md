---
title: Let's talk about a deal in Memphis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=v9mavtEADqo) |
| Published | 2023/11/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Memphis Police Department's Scorpion team and the killing of Mr. Nichols are under federal investigation.
- The first former officer involved in the case entered a plea agreement for 15 years in exchange for cooperation.
- Federal authorities seem to believe there is more to uncover in Memphis, hinting at potential future allegations.
- Those opting for trial rather than cooperation could face substantially higher sentences.
- Expect more individuals to cooperate as the investigation progresses, following the first officer's deal.
- The cooperation deal for 15 years sets a precedent for potential future deals and outcomes in the case.
- The situation in Memphis could mirror past cases where multiple allegations emerged.

### Quotes

- "The cooperation deal for 15 years. That's the recommended sentence."
- "The feds are under the impression that there is much more to discover in Memphis."
- "Once you have that first domino that falls, there will be more."
- "If the deal is for 15 years, they're going to understand the message that's being sent."
- "We'll continue to follow it until we get a resolution on all of the individuals involved."

### Oneliner

Memphis Police Department's Scorpion team faces federal investigation after the killing of Mr. Nichols, with the first officer entering a plea deal for 15 years, hinting at more revelations to come and higher sentences for those opting for trial.

### Audience

Legal observers, community members.

### On-the-ground actions from transcript

- Follow updates on the investigation and outcomes (implied).
- Stay informed about developments in the case (implied).

### Whats missing in summary

Details on how the community is reacting to these developments.

### Tags

#Memphis #FederalInvestigation #PoliceMisconduct #PleaDeal #Cooperation


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Memphis and deals
and how things are progressing up in Memphis
and where it goes from here
because there have definitely been some developments
since the last time we checked in on this situation
and we're just gonna kind of go over them.
For context, we are talking about the Scorpion team up there with Memphis Police Department,
what used to be the Scorpion team, and the killing of Mr. Nichols.
So about a month ago, the feds got involved, went through the charging documents.
The number one question everybody had at the time was, what kind of sentence are they looking
Act because the cops were charged by the feds.
And I said then, I didn't have a clue because I have no idea how they would score it out.
The allegations are expansive.
There were a lot of them.
And all I could say was that it wasn't going to be a slap on the wrist because there's
charts and points when it comes to federal sentencing.
But I have no idea how they would have scored it all out.
So the first former officer has entered into a plea agreement.
The plea agreement covers his cooperation with both state and federal investigations
And the sentence is 15 years, sentence recommendation for 15 years.
It's worth remembering.
This is the deal.
The deal with cooperation is 15 years.
The first deal.
There is, based on that, I have a feeling that the feds are under the impression that
there is much more to discover in Memphis.
And this may be a replay of what happened, well, what got turned into the show The Shield.
That's loosely based on a true story.
And I have a feeling that there are going to be a lot more allegations.
So the first deal with cooperation is 15 years.
That's the recommended sentence.
So those who choose to go to trial, their sentence will be substantially higher than
that.
So or I should say should most likely will be substantially higher than that if they
are found guilty.
So that's where that situation sits.
Generally speaking in a case like this, once you have that first domino that falls, there
will be more, especially when they realize the cops know how it works.
And if the deal is for 15 years, they're going to understand the message that's being sent.
So I would expect additional people to decide to cooperate on that one.
But we'll continue to follow it until we get a resolution on all of the individuals involved.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}