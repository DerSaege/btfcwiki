---
title: Let's talk about autoworkers winning and a date....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=swcMb1Qq1W0) |
| Published | 2023/11/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The big three automakers were in a dispute with the United Auto Workers.
- The United Auto Workers employed unique tactics during the strike.
- All three automakers eventually came around to the Union's way of thinking.
- Unions and labor organizing set the standard for fair pay and benefits.
- Toyota has reevaluated how they will be paying their employees.
- Toyota's decision may be related to the success of the UAW strike.
- The UAW invited other unions to synchronize contract expiration dates.
- This alignment of contract dates enhances solidarity among unions.
- The UAW renegotiated their contract end date before extending the invitation.
- The UAW's new contract will end on April 30th, 2028, potentially leading to a strike on May 1st.
- Coincidences like this require deliberate planning and coordination.
- Synchronizing contract dates can empower organized labor significantly.
- This strategic move can strengthen unions and their ability to negotiate.
- The power of solidarity among unions can lead to better outcomes for workers.

### Quotes

- "Unions and labor organizing set the standard for what is considered good."
- "With all contracts expiring around the same time, it puts a lot more power into the hands of organized labor."
- "It takes a lot of work to make coincidences like that happen."

### Oneliner

Beau reveals how unions' strategic organizing and solidarity can shift power dynamics in the workforce, impacting fair pay and benefits for all workers.

### Audience

Labor Activists, Union Members

### On-the-ground actions from transcript

- Organize with your union to synchronize contract expiration dates with other unions for increased solidarity and negotiating power. (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the recent auto workers' union strike, showcasing the power of labor organizing and strategic planning in influencing fair pay and benefits for workers.

### Tags

#LaborRights #UnionOrganizing #FairPay #Solidarity #WorkerEmpowerment


## Transcript
Well, howdy there, internet people.
Let's bowl again.
So today, we are going to talk about some news
for the average person.
We're going to talk a little bit about auto makers
and auto workers and how everything played out,
some side effects, might be side effects,
and then a very interesting request
that might be a sign of things to come.
And we'll just kind of run through it all.
If you missed the news, the big three automakers,
well, they've been in a little, let's just say,
a dispute with the United Auto Workers.
And the United Auto Workers, they
engaged in some really unique tactics
when it comes to not just striking, but how they did it
and how they put pressure on the different automakers.
It didn't take long, really, but all of the big three automakers
have come around in the Union's way of thinking. They won. UAW won. There's no
debate about whether or not it was a win. One of the things that we talk about on
this channel pretty often is how unions and labor organizing in general, when
it happens, it doesn't just help that Union. It doesn't just help those people
who went on strike, it doesn't just help those people at that location because
those activities, they kind of set the standard for what is considered good. And
other companies, sometimes they go on the initiative and they try to align their
pay structure or their benefit structure with those places, with
unions because they want to avoid them. So it helps everybody. In what I am sure is
totally unrelated news, it appears that Toyota has reevaluated the way they will
be paying their employees. I'm sure that has nothing to do with what the UAW just
did. That's just that's just a coincidence, I'm sure. I don't
think that would be a side effect. Now let's get to the
interesting thing. And I'm just going to read this. This is from
the person who's kind of been been the mastermind behind what
the UAW has been doing. We invite unions around the country
to align their contract expiration dates with our own.
I mean, that's interesting in and of itself.
And you can see the power of solidarity
that can be expressed in that way,
because one union can back up another union
because all of their contracts are coming up at the same time.
It becomes even more interesting when
you find out that they renegotiated
when their contract was going to end
before making this invitation to other unions.
Their new contract will end on April 30th, 2028,
which means if they were to be in a situation
where they would have to strike,
they would be doing so on May 1st.
Man, it takes a lot of work
make coincidences like that happen. This is something that might help unions
organize, generally speaking. With all of them expiring at the same time,
particularly around that date, it puts a lot more power into the hands of
organized labor. That's a smart move, and it's a big one. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}