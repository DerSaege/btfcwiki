---
title: Let's talk about the least free states....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6fD2-0m4KUo) |
| Published | 2023/11/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the concept of freedom in the United States and different kinds of freedom.
- Mentioning a think tank, the Cato Institute, that ranks states by freedom through economic and personal perspectives.
- Differentiating American libertarianism from European libertarianism as right-wing free-market advocates.
- Analyzing the rankings of states based on economic and personal freedom criteria.
- Focusing on personal freedom aspects like incarceration rights, gun rights, marriage rights, travel rights, and education.
- Revealing the five least free states in terms of personal freedom: South Carolina, Kentucky, Wyoming, Idaho, and Texas.
- Expressing surprise at states like Idaho not ranking high in personal freedom despite popular perceptions.
- Questioning the discrepancy between rhetoric about freedom and the actual rankings of states.
- Suggesting that states with lower personal freedom rankings often have residents who champion their freedoms.
- Implying a disconnect between the rhetoric of personal freedom and the reality of rights erosion in certain states.
- Encouraging viewers to look at the rankings to understand the nuances between economic and personal freedom.
- Pointing out that certain states, despite claiming personal freedom, may exhibit empty rhetoric when scrutinized.
- Ending with a thought-provoking reflection on the concept of freedom and its implications.

### Quotes

- "But according to the information, according to the rankings, it's not."
- "It is interesting to me that those people who always talk about their freedoms, a lot of them live in states that don't really have a whole lot of personal freedom."
- "They like to pretend that they have the most personal freedom, but when you actually look at the information, it seems like that's just empty rhetoric."

### Oneliner

Beau dives into the rankings of personal freedom in US states, revealing surprises and challenging common rhetoric on freedom.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Analyze the rankings of states by freedom criteria (suggested)
- Scrutinize the disconnect between rhetoric and reality in terms of personal freedom (suggested)

### Whats missing in summary

Exploration of how personal freedoms impact societal well-being.

### Tags

#Freedom #UnitedStates #Rankings #CatoInstitute #PersonalFreedom


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about freedom,
freedom in the United States, different kinds of freedom,
and we're gonna talk about rankings of freedom.
In the US, you always have people, it's about my freedom.
Most times, it's a pretty intangible idea,
and a lot of times, it's just words, it's just rhetoric,
doesn't really mean anything.
But there is a think tank that ranks states by freedom, and it goes through and it takes it in a whole bunch of
different ways.  It's called the Cato Institute. It is a libertarian think tank. I think they would call themselves
libertarian.
They certainly appear to be to me. For Europeans, American libertarian, not your kind of libertarian.
very very different thing. They are like right-wing free-market kind of people.
They put out this this listing every year and they divide it between economic
freedom and personal freedom. Now generally speaking when I'm looking at
the economic freedom you know listing those are freedoms that your boss enjoys
for the most part. There are some of them that that do impact you know the
average person but for the most part that stuff that applies to business. The
personal freedom one though I always find that one really interesting
because they go through and they look at like incarceration rights and they look
at gun rights and marriage rights. They look at travel rights education just all
kinds of things. And they blend it all together, they average it all out, and they come up
with these rankings. And then, of course, they also have one where they average both
the economic and the personal freedom one together. Me, personally, the personal freedom
one is the one that I tend to look at the most. So what we're going to do is we're
We're going to go through the five least free states when it comes to personal freedom in
the U.S. as ranked by an organization that definitely in my mind leans right.
So number one would be the least free state in the United States.
Number five, South Carolina.
Number four, Kentucky.
Number three, Wyoming.
Number two, Idaho.
Number one, Texas.
That's weird, right?
I mean, that's like, that runs counter to, like, all the rhetoric.
You know, when you think of a place like Idaho, you think of personal freedom a lot of times.
You think of that rugged individual list.
But according to the information, according to the rankings, it's not.
Now a lot of these places have good economic freedom, but not personal freedom.
And I think it is, I think it's valuable that they divide the two out like that.
Because in many ways, if somebody was interested, they could go through and they could really
see through a lot of the rhetoric. It is interesting to me that those people who
always talk about their freedoms, a lot of them live in states that don't really
have a whole lot of personal freedom, that rank at the very bottom. And when
that happens you have to kind of wonder if their definition of freedom is the
same as everybody else's or if freedom in these locations really means I will
obey and do what I'm told. I am free to obey my betters and I will keep voting
them in so they can continue to chip away at my rights. When you look at the
listing, it's definitely a conclusion you can draw. I'll put it down below. I'll
try to remember to put it down below so y'all can look at it, especially if
you're interested in the economic freedom stuff because to me, like I said,
And that to me isn't as interesting, it's not quite as telling because the economic
one it lines up with what you would think.
States that are high in taxation and stuff like that, they're at the bottom and we all
know like California, New York, places like that.
But these are states when it comes to personal freedom, these are states that like to pretend
they are free.
They like to pretend that they have the most personal freedom, but when you actually look
at the information, it seems like that's just empty rhetoric.
Anyway, it's just a thought.
Do I have a good day?
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}