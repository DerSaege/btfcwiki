---
title: Let's talk about the Joint Chief's comment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4C7wIxb2QqE) |
| Published | 2023/11/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- General Brown made remarks about Israel’s goal of completely destroying an organization, calling it a tall order and expressing concerns about generating more combatants for Israel's opposition.
- Congress reacted to General Brown’s comments, with one member suggesting that the push for a ceasefire should be dropped if the mission duration is a concern.
- Beau points out the likelihood of resentment and the creation of new combatants in Gaza due to the ongoing conflict.
- The conflict in Gaza is unlikely to end with the current approach, as stronger clampdowns often lead to more resistance and animosity among civilians.
- General Brown's observations are compared to past U.S. mistakes in conflicts, indicating that the strategy employed may not lead to a lasting solution.
- The goal of complete destruction of the opposition organization is deemed almost unattainable, especially given the conditions in Gaza and the potential for future cycles of conflict.

### Quotes

- "The general is 100% correct."
- "These types of conflicts are not solved this way."
- "He's 100% correct. General Brown is 100% correct."

### Oneliner

General Brown's concerns about generating more combatants in conflicts like the one in Gaza are valid, pointing to the unlikelihood of achieving lasting solutions through current approaches.

### Audience

Activists, policymakers, analysts

### On-the-ground actions from transcript

- Advocate for diplomatic solutions and sustainable peace efforts (implied)
- Support organizations working towards conflict resolution and peacebuilding (implied)

### Whats missing in summary

The full transcript provides a deeper analysis of the challenges in conflict resolution and the importance of reevaluating strategies to prevent further escalation and harm.

### Tags

#ConflictResolution #InternationalRelations #GeneralBrown #Israel #Gaza #Peacebuilding


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about something
the chairman of the Joint Chiefs said,
and how Congress responded to it,
and a question that kind of brought all of this up,
because General Brown said something that it definitely
rubbed people the wrong way, but it's important to go over.
So here's the question, I was wondering what you thought about what the Chairman of the
Joint Chiefs said and Congress getting mad.
You've said another cycle is already coming.
He seems to think Israel can't actually accomplish their goal.
Okay so, paraphrasing what General Brown said.
He said that when it came to totally destroying the organization, which is the victory condition
condition that Israel has set.
I believe he said it was a tall order, which is a super polite way of saying almost impossible.
He also said that he was very much concerned about the way the operation has gone and the
possibility that it is generating more combatants for Israel's opposition.
The general is 100% correct.
He's not wrong.
There are people that don't like it, such as the member of Congress who said, if General
Brown is so worried about how long it may take the Israelis to accomplish this mission,
he should advise the President to drop his push for a ceasefire.
So we've talked about how another cycle is coming.
I think anybody watching this, regardless of which side you believe is correct or started
or wherever you are on it, I think that everybody would say that some resentment has been generated
in Gaza and that some of those people will become combatants.
I don't think that that's a stretch to say that everybody can agree on that.
The Israeli goal, the stated goal, is the complete destruction of the organization responsible.
That's their goal.
It's almost impossible to do that, but realistically that's actually good for them.
That's good news because if they were to actually pull it off and they completely destroy that
organization? What about the newly generated combatants? What do they do? Do
they just throw their hands up and say, oh, okay, well, I guess I won't do
anything now? Or do they form new organizations? Organizations that Israeli
intelligence knows nothing about. They don't know their leadership, they don't
One of their tactics, their strategy, their doctrine, their resources, anything.
The general is correct.
Another way of saying this, of what he's saying, would be the strategy and victory
conditions that they have set are unattainable.
Yeah.
He's right.
cycle coming. This will not end this. These types of conflicts are not solved this way.
They are creating a lot of animosity among the civilian populace. A populace that not
all of them were involved, but some of those who weren't involved, they will be now.
This type of conflict is one where the stronger the clampdown, the less it works.
The general knows this, General Brown knows this, because the United States just spent
almost a quarter of a century making this exact mistake.
He is right.
At the same time, the congressperson is kind of right too.
That wasn't something that he should have said in public because of his position.
Not because he's wrong, but because of his position.
It was not great to say that in public.
But at the same time, it's not like the US hasn't sent people to Israel to say this
exact thing to them.
The strategy that was deployed and the way they went about it, it will not end this.
At most, it will buy a few years of relative calm, but it is definitely not an end.
It is important to remember that the goal, in this case, the goal of their opposition
is to provoke a response because it generates outrage and it generates recruitment and it
generates support from from state actors it's the strategy this is all in the
manual and the thing is every country knows this every country knows this but
there aren't many that don't don't respond in the exact same way the US
US knew it before it got itself involved in trying to do it for 20 years that way.
They knew that wasn't the right way.
The manual is older than that.
The US made the exact same mistakes.
So the short version, he's 100% correct.
General Brown is 100% correct.
And I don't think that there's going to be anybody who is well-versed on this type of
conflict that would disagree with his statement.
At the same time, probably was not a statement that should have been made in public because
of his position, but he's definitely not wrong.
And he might be frustrated.
The goal of the complete destruction of the organization in and of itself is almost unattainable
in the best of conditions.
And they're kind of in the worst of conditions because of the way Gaza is.
It's very insulated.
And then even if they were successful, it actually puts them in a worse position.
Because that next cycle is coming.
And if that next cycle is not using the same strategy or doctrine or tactics, they won't
have any idea what to expect. He's not wrong. But he shouldn't have said it in public. Anyway,
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}