---
title: Let's talk about Santos going live....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qxtuJIN00UU) |
| Published | 2023/11/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about George Santos and his recent online rant.
- Santos believes he will be expelled from Congress.
- Santos called out colleagues for engaging in inappropriate activities, such as being not entirely sober in their duties.
- Santos believes his colleagues are hypocritical and gross.
- Allegations of unethical behavior in Congress are not new.
- Santos indicated that he is not running for reelection.
- Santos mentioned that he dislikes being around his colleagues.
- The animosity and mudslinging in Congress seem significant.
- Santos' expulsion from Congress might happen soon.
- Santos' rant lasted for three hours and included profanity.

### Quotes

- "He's super angry and does not like the people he is in Congress with."
- "Yeah, so that's gonna happen."
- "It was something else. It was it was something else."

### Oneliner

Beau talks about George Santos' explosive online rant, revealing allegations and animosity within Congress, leading to Santos possibly being expelled soon.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to address unethical behavior in Congress (implied).

### Whats missing in summary

Context on who George Santos is and further details on the specific allegations against his colleagues.

### Tags

#GeorgeSantos #Congress #Expulsion #PoliticalDrama


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to once again talk about George Santos.
I feel like the amount of time we'll be talking about him
will be starting to decline.
So we're going to talk about Santos
and a discussion he had on the internet.
He decided to go live.
and it was interesting to say the least. Okay, so it should be noted, we'll start
with the real news, he believes that he is going to be expelled. He's not gonna
resign, but he believes that he will be expelled because, quote, I can do math. He
believes that a number of his Republican colleagues are going to vote to expel him and definitely
seems to believe that that was a conclusion that they had already reached. If they had not reached
that conclusion, they definitely would have by the end of his three-hour profanity-laden rant that he
went on. He called out some of his colleagues by name in some cases for
everything from engaging in adult activities with lobbyists, certainly
seemed like the people doing that were married, to being not entirely sober in
their duties, it's just a list. It was a list. So if they hadn't already made the
decision that maybe he shouldn't be in Congress, I am certain that they have
now. He definitely indicated that he believed his colleagues were hypocritical.
Gross was a word that was used. Talked about a lot of various activities that
if you still hold a high opinion of the US Congress you wouldn't expect to find
there but I mean it's not the first time allegations like this have surfaced. He
identified some members of Congress as by different parts of the human anatomy.
It was something else. It was it was something else. So I do feel like when
the break is over and Congress resumes its activities. I feel like Santos being
expelled is probably going to be one of the first things that happens. The
amount of animosity and mudslinging that occurred, I don't really think it can be
overstated. He's super angry and does not like the people he is in Congress with.
It is worth noting that he said he is not running for reelection, but contrary
to what people assumed, it actually apparently doesn't have to do with the
charges. He seemed to indicate that he just didn't like being around all of
those gross people.
Yeah, so that's gonna happen.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}