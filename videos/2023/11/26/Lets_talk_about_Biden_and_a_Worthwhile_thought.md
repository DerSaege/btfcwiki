---
title: Let's talk about Biden and a "Worthwhile thought"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pfCpKC2tJy4) |
| Published | 2023/11/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring Biden's statement on conditioning military aid to Israel and whether it signifies more than typical democratic messaging.
- Biden's response to the press about conditioning military aid to Israel being a "worthwhile thought."
- Mentioning the conditions placed on the transfer of M16s to Israel by the Biden administration.
- Explaining that the condition was that the M16s couldn't be transferred to settlers, with a potential easy workaround.
- Suggesting that Biden's statement hints at considering more stringent conditions than the ones placed on the M16 transfer.
- Comparing the potential progression of conditions to how the US and the West dealt with Russia's red lines.
- Speculating that Biden might be discussing more stringent conditions, although no concrete examples were given.
- Pointing out that the US-Israeli relationship is evolving, particularly in terms of aid, signaling a change from previous policy.
- Emphasizing that the statement is not definitive, indicating ongoing consideration and potential changes in the relationship.
- Concluding with the notion that the statement represents a shift in approach rather than a failure in messaging.

### Quotes

- "It's a worthwhile thought."
- "The US-Israeli relationship is changing, particularly when it comes to aid."
- "I don't see this statement as definitive."
- "I think it's part them easing into whatever the finalized conditions will be."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Exploring Biden's statement on conditioning military aid to Israel and hinting at evolving US-Israeli relations regarding aid, Beau speculates on potential changes while viewing the statement as a shift in approach.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Monitor and advocate for potential changes in US-Israeli relations regarding military aid (implied).
- Stay informed about evolving foreign policy decisions and their implications (implied).
- Engage in dialogues and initiatives related to US foreign aid policies (implied).

### Whats missing in summary

Deeper analysis of potential implications of evolving US-Israeli relations and the need for continued observation of foreign policy shifts.

### Tags

#USForeignPolicy #BidenAdministration #MilitaryAid #USIsraelRelations #PolicyChange


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about Biden
and what he sees as a worthwhile thought and what that means and whether or not that statement was
just typical democratic messaging not being great or there's more to it because the
The full quote, not the one that's in the headlines, but the full quote, there's more
to what he said.
And we're going to kind of run through that because some questions have come in.
If you have no idea what I'm talking about, the press asked the Biden administration,
they asked Biden himself, what do you think about conditioning military aid to Israel?
And the quote that everybody's focused on is him saying, it's a worthwhile thought.
Questions have come in.
Is this just normal, bad, democratic messaging?
Shouldn't the answer have been something to the effect of, it's a worthwhile thought?
Yeah, that's why we did it with the M16 transfer.
Because if you don't know, the Biden administration placed conditions on the transfer of M16s
Israel. And I mean, it's interesting that that was ignored, that he didn't
point that out, but I think there's more to it when you read the whole quote. It's a
worthwhile thought, but I don't think if I had started off with that we would
have gotten where we are today. Okay, so with that M16 transfer, I think people
might have thought I was like summarizing and leaving stuff out when I
said the condition was that it couldn't be transferred to settlers. No, like
that's literally kind of the only condition and there's an easy workaround
to that. They just take the old stuff give it to the settlers and the new
stuff goes to their their active units. So it's not exactly a
stringent condition. Him saying it's a worthwhile thought, and
then leading to the idea that it's progressing. I don't think
we would have gotten where we are today if I had started with
that, something to that effect. It seems to me that he might be considering more
stringent conditions. Whatever he believes is worthwhile is more than that.
So he's viewing that as a taste. Think about how the US and the West in general
dealt with all of Russia's red lines, you know. If you give them tanks, you know, that's going to be
super bad. A little bit, well, we can give them these Bradleys, they're not really tanks, and then
give them tanks. Ease into it. That's kind of my takeaway from what he said there, was that whatever
he is considering, whatever he believes is a worthwhile thought, is more stringent than the
the conditions that were placed on the M16 transfer. And if he believes that
they are the ones coming are more stringent, well there's no reason to
point to something that isn't up to what he wants to do. I would note that he
didn't give any examples of what those conditions might be and it was kind of
off the cuff. So this is probably something that's in discussions. It's not something they're working
out the policy on yet, but the the overriding theme here is that the U.S.-Israeli relationship
is changing, particularly when it comes to aid. Just saying that two years ago, that was not
something that would have been in line with any U.S. policy.
Just kind of putting it out there that, hey, maybe we should put conditions on this.
So I think it's part them easing into whatever the finalized conditions will be, and part
slowly easing the American public into the idea that the relationship is changing.
How it changes, how much it changes, if it changes at all, all that's really still up
in the air.
Because I don't see this statement as definitive.
It seems more like this is something we're looking at, but we haven't made any decisions
yet.
The way state has been pushing this issue, I feel like there will be changes.
How significant? No clue. But I don't think this was just the Democratic Party
failing to say, hey we already did that. We did put conditions on something.
I think it was them kind of leading into something more, but what that is I can't say.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}