---
title: The Road Not Taken EP15
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qqv7aBPJrq4) |
| Published | 2023/11/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces "The Roads with Beau" episode on November 26th, 2023, covering under-reported news and answering viewer questions.
- Tensions between China and the US rise over a US warship in the South China Sea, a situation likely to escalate when their navies come in close proximity again.
- Russia considers building an underwater tunnel to Crimea, possibly with Chinese assistance, sparking speculation and potential future developments.
- A true crime enthusiast in South Korea sentenced to life for killing out of curiosity, while Russia vows retaliation against Moldova for joining EU sanctions.
- North Korea launches a spy satellite, impacting defense experts' perceptions differently based on their government ties.
- In US news, big GOP donors back Nikki Haley despite Trump's polling lead, and egg producers are found guilty of price-fixing from 2004 to 2008.
- California mandates media literacy courses in schools, a federal judge halts the Kansas two-step law enforcement tactic, and a former State Department official faces harassment charges.
- Cultural events include Dolly Parton sparking outrage for her performance outfit and debates about woke culture at Rand Corp.
- The Pope meets with trans people, causing controversy, while Canadian superpigs (feral hogs) invade the US, and caviar authenticity raises concerns of wildlife crime.
- Beau addresses viewer questions on US foreign policy regarding Israel-Palestine, challenges Biden faces, and the emotional responses driving political decisions.
- He shares insights on managing pessimism by acknowledging wins, staying informed, and taking breaks to recharge amidst overwhelming global challenges.

### Quotes

- "There are no simple solutions here, but they see it in that way because they have suffered a moral injury."
- "It won't really change anything. Their money wouldn't be going to it. But that footage will still be there."
- "Acknowledge the wins. Keep up on the news that has good news occurring in it."

### Oneliner

Beau covers under-reported news, foreign policy tensions, US developments, cultural events, and viewer questions on navigating pessimism with a focus on acknowledging wins and staying informed.

### Audience

Viewers

### On-the-ground actions from transcript

- Stay informed about under-reported news and ongoing developments globally (suggested)
- Advocate for media literacy courses in schools to improve critical thinking skills (implied)
- Acknowledge wins in addressing systemic issues to stay motivated for change (exemplified)

### Whats missing in summary

Insights on the importance of acknowledging wins and celebrating progress amidst overwhelming global challenges.

### Tags

#UnderreportedNews #ForeignPolicy #USNews #CulturalEvents #Pessimism #AcknowledgingWins


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today, which is November 26th, 2023,
we are going to do episode 15 of The Road's Not Taken,
which is a weekly series where we go over
the previous week's news that was under-reported,
unreported, or will prove to be important context for later,
or I just found interesting.
And then at the end, we'll go over some questions
from y'all.
OK, so we will start off with foreign policy.
China and the United States are currently
having a moment of tension over a US warship that found its way
to the South China Sea.
China is not happy about that.
This is normal near-peer stuff.
Shouldn't be anything to worry about.
But it will be mentioned later when
There's another situation where the Chinese and US Navy
come in close proximity to each other.
Russia is considering building an underwater tunnel
to connect to Crimea, which is probably a whole lot safer
than a bridge.
You'll end up seeing more information about this.
My understanding is that they're going
Chinese helped to do it. There are some people that are reading into this and
once I get a little bit more information though there might be a video about that
and what it may mean. Okay, in South Korea a true crime fan who killed a
person out of curiosity, quote, because they wanted to try out a murder, quote,
was sentenced to life. Get better idols. Russia promises to get even with Moldova
over the decision to join the EU sanctions. Yeah okay. North Korea launched
a spy satellite. There's gonna be just tons of news about this and we'll talk
about it later when news slows down probably sometime next week. Short
version, the defense experts that are closely linked to governments, oh this is
the worst thing in the world. Those that are a little bit more independent, yeah
it's gonna increase their situational awareness, yes it's you know probably
going to help them in building other kinds of rockets, stuff like that, but
you're definitely going to see a divide between those that are closely linked to
governments and those that are not, because they're going to read this in two
very very different ways. Okay moving on to US News, notable big GOP donors are
are throwing their support behind Nikki Haley.
And they are telling other big GOP donors to do the same.
As much as polling shows Trump having it all wrapped up,
there is still a lot of maneuvering going on
within the GOP to stop that.
Whether or not that will be successful
is anybody's guess at this point.
OK, US egg producers were found guilty
by a federal jury of price fixing from 2004 to 2008.
And that's what the jury found.
So they were found guilty of price fixing on eggs.
OK, despite sensationalism running rampant,
The prevailing theory right now is that the incident Rainbow Bridge was an accident and
the result of mechanical failure.
California has passed a new law mandating media literacy courses in school.
I haven't seen the curriculum yet, but thank you.
A federal judge placed an injunction on the Kansas two-step.
This is a habit of law enforcement officers, particularly in the state of Kansas, where
they extend a traffic stop by completing the stop, you know, writing the ticket and technically
ending the stop, walking away and then coming back to continue to ask questions.
The driver, generally speaking, doesn't know that they aren't still under the cop's authority.
And basically, that's what the judge found.
And they're like, you can't do this anymore.
There are time limits, if you're not familiar with this.
There are time limits and just a general...
The general idea is that a traffic stop should take as long as a traffic stop needs to take.
Not a traffic stop and then hold them there until you bring dogs or something like that
without a whole lot of reason.
Officers often use the Texas two-step to try to find a reason.
So that has been barred by a federal judge.
A former State Department official was charged with harassment, and I actually think the
charges got upgraded, but I don't want to say that because I can't remember now.
But was charged after a video went viral of them harassing a worker, a food service worker,
and talking about their heritage and stuff like that.
It was a pretty bad video and there's more than one if you've only seen the one.
If you missed it, Chauvin, the cop, he was stabbed in prison and the news has now come
out that he is expected to survive.
In cultural news, 77-year-old Dolly Parton performed wearing a cowboy cheerleader's
outfit and it provoked outrage for some weird reason, basically saying she's too old to
be doing that.
I mean, initially when I found out about the outrage, I just assumed they were mad because
she's a teeny bopper who gives away books.
flashback humor.
Rand Corp has been accused of going woke because they have a diversity thing going on.
Rand Corp, if you're not familiar with this entity, this is an organization that can tell
you exactly how many people will be lost if this type of rocket is used in this location
and there are this many civilians around. They are kind of the study and survey, I don't want
to say arm, but the study and survey organization that provides in-depth policy analysis for the
U.S. war machine. Like, it's...Woke and Rancorp don't exactly go together. Somebody that has
actually gone woke, the Pope drew attention by having a luncheon with a group of trans people.
The current pope is definitely upsetting a lot of conservative cardinals in the
United States, but I don't think the pope cares to be honest. In environmental
news, Canadian superpigs are invading the United States. Feral hogs, basically same
kind of thing only they're I guess a little bit more well adapted and they're
they're spreading and the US is trying to figure out how to deal with this. In
odd news the eggs aren't from sturgeon in many commercially available brands
of caviar. And that means they're not caviar, they're just eggs. While that is interesting
and entertaining because there's probably a whole bunch of incredibly wealthy people that have been
definitely overpaying for something, the same analysis determined that what was sturgeon
was wild sturgeon which is not good because it's there are protections in
place and it shouldn't be traded internationally to kind of protect the
species so one thing kind of led to another in this and what what started as
hey this isn't really caviar turned into hey this is kind of an international
wildlife crime. I'm sure there's going to be more developments with that.
Moving on to the Q&A. I made a comment recently about how I've been seeing
lots of people saying that they refuse to vote for Biden until he solves the
situation between Israel and the Palestinians. To be honest, I'm not
entirely shocked by this happening, but the numbers have me really freaked out
because this movement seems to be gaining more volume, if nothing else.
I'm logically aware that a lot of these are likely bots or sock puppets,
but they can't all be.
I guess my main question, if I really have one, is what more could Biden be doing?
Can he just cut off monetary military aid like these people want?
Why is this the hill that so many have chosen to die on
despite the fact that if Trump wins, things will get so much worse.
And why are so many people still insistent that the claim and others about Republican
goals are just Democrat fear-mongering when Republicans are all but openly admitting to
them with their words and actions?
So there's a couple of questions in that.
So first, it's probably not bots.
There are a lot of people that are very upset at how it's being handled.
Some of it is emotion-based.
I think that a lot of people in the United States have forgotten that when new groups
of people come to the U.S. just like previous groups. They maintain ties to the old country.
There are a lot of people in the United States who are wondering whether or not their family
members have made it, and they don't feel like Biden is being forceful enough. It is
is really the real issue.
I think that most understand that he's tried on some level,
but because of the way the federal government works
and foreign policy works, it's like he does something
or says something that would be good to them.
It would be something that would make them happy,
or at least make them a little bit more comfortable.
And then, because of a foreign policy obligation
or something like that, he turns around
and does something else that upsets them.
You can't be mad at them for it.
They are expressing their interests
and they're moving on it in that way
And they're talking about it in that way.
And there's just, this seems to be, by the way this is written, this seems to be more
about Arab Americans.
There are actually, on the other side of the conflict, there are people who are really
upset about how Biden has handled it as well.
It is a politically fraught situation for him.
Okay.
Okay, so I guess my main question is what more could Biden be doing?
Can he just cut off monetary, military aid like these people want?
Can he?
Does he have the ability?
Most of it, yeah.
Some of it, no.
Like right now, there's a whole bunch of people talking about the stuff that's pre-positioned there.
The U.S. has stashes of weapons, like all around the world.
Some of the facilities are small, some of them are massive.
There are regulations that govern these places.
They have a lot of unwritten rules as well, and one of those unwritten rules is that, you know,
the country that has let you store your military hardware in
their country for half a century. If they say they need
it, you give it to them. And he's the administration, I
don't know if this is him exactly, or just DoD moving it
along. But either way, it's the Biden administration. They're
currently reworking those regs to get rid of some of the
paperwork in it and make it easier to give the Israelis what they want.
That is, and that's a good example, that's something that he could say, you know, we
have an obligation to provide this, okay, and that might make some people a little bit
more comfortable because he could frame it as we don't want to, but we kind of have
And it would be kind of true, but when they go through and at the same time
rework the regulations to make the paperwork easier, that's not the
perception. The perception is they're just opening the doors and letting them
get whatever they want, which I mean that's not really perception, that's kind
of what's happening, but that's kind of how those things were set up. So some of
stuff like that, there's an obligation to do it, but there's a whole bunch that he
could, meaning he has the ability to stop it. I don't want to get too far into
this because we already have like a whole whiteboard video recorded about
aid that answers this exact question, but there's a difference between does he
We have the ability to do it and the authority to do it, and can he do it via foreign policy
and staying within the framing that we have?
And then there's the other question of would it actually change anything?
And that's the, to me, that's the important part that people need to understand.
It won't.
If the U.S. stopped, it wouldn't really change anything.
Just as an example of this, because when people are talking about it in the framing of cutting
off everything, they're normally making that from a moral position.
It's not a foreign policy one.
They're like, I see the footage that's coming out, I don't want my money paying for that.
And that's where it comes from.
And in that sense, sure, if it was cut off, their money wouldn't be going to it.
But that footage would still be there.
It wouldn't change anything.
After Hurricane Michael, you'll hear this story again in the whiteboard video, but after
Hurricane Michael, a gas station that was near me, one that I had a relationship with,
I frequented all the time.
It shut down, and it stayed shut down.
I couldn't get gas anymore there.
So of course, I started walking.
No, I found somebody else to supply me with gas.
That's exactly what would happen.
And there would just be massive foreign policy
implications to that.
So that question is, yes, has the ability.
But as far as practicality, real world international affairs,
no, he could.
But it would end up forfeiting the US position
in the Middle East to probably China.
That would be the end result of it.
And that's not something that the US
is going to do, any candidate.
And then, OK, why is this the hill
that so many have chosen to die on despite the fact
that if Trump wins, things will get so much worse?
Um, my guess is people don't like what they're seeing.
I mean, it's a moral call.
The people who are talking about this, they're talking about the aid because they see it
as a simple solution to an issue.
There are no simple solutions here, but they see it in that way because they have suffered
a moral injury.
And it is an emotion based thing, but I can't bring myself to say, you know, hey, that's
bad that you are having such a response to innocence being hurt.
Like I don't, I get it, I get it.
If we're just sticking with the foreign policy side of it, there's not much that can be done there quickly.
There's not much that can be done there quickly.
There's stuff that can be done over time, but that requires a lot of political will, and we'll get more into that in the
White Board video.  As far as why they're saying they won't vote for him, they're mad.
They're mad. It's that simple.
I don't think that they're going to go out and vote for Trump, they may just not vote, or they may vote for a third
party, or if the Biden administration strategy actually pays off, some of that may go away, but I don't think all of it
will.
I think the early response of the Biden administration and the way they came out, the way the U.S. always does, and says
we're behind Israel no matter what, that alienated a growing demographic in the United States because it was so
one-sided in their view.
And then he has what may be a bigger problem for him politically is he has upset the side that supports Israel as well
because they see him as trying to kind of restrain, well I mean he is, he has tried to encourage restraint.
Basically, the path that he has charted is not one that is going to make either side completely happy.
And so he is going to take a hit for this politically.
Do you have a monitor so you can see yourself when recording?
If so, how do you stop yourself from looking at it?
looking at it. I just started YouTube and my videos look weird because I'm always looking
off to one side. Oh, that's easy. Yes, I do. The secret to that is putting the monitor
right next to the camera. I don't really look at it that much anymore, but I did early
on. But you have it right by the camera. So right now I'm looking directly into the camera.
Now I'm looking at the monitor.
You just have it right there.
That's your solution to that.
Eventually you won't even realize it's there anymore.
The only reason I have it is so I can see myself when I do the little jokes like show
the Bailey Sarian shirt when I quote her.
That's the only reason that I still use it.
I appreciate you trying to just cover the foreign policy side of the war, but I need
you to acknowledge that the day-to-day stuff shows that they're just evil.
Maybe if you were covering the day to day, you would have heard about the two people
they killed because they accused them of spying for us, and they cheered in the streets.
Nobody wants death, but it's stuff like this that leaves us with leveling Gaza as
our only option.
They're just evil.
I feel like it's important to point out who the bad guys are.
Okay, so to cover the foreign policy stuff, you actually have to stay up to date on the
day-to-day stuff as well.
I don't cover it, I don't make videos about it, but yes, I am aware of what you're talking
about.
I am aware of the fact that there was actually another one recently as well, it's a total
of three and that out of the two that you're referencing because of what happened, it is
worth noting that one of their families actually said that it was true.
And I think the most important part to point out here is that this happened in the West
Bank, not Gaza.
It's a different location.
It has an entirely different authority system.
It's literally not the same.
I feel like that the details are important when the suggested outcome is leveling a city.
in the wrong spot.
Are you going to start doing live streams every week like you did before?
Every week?
No.
I never did that.
I never did them every week.
They will definitely be done more often now.
They will not be done every week.
Maybe every month.
Now that we know that it works and we have that part worked out, we will definitely do
them more often.
I actually have a list of stuff I have to go back through.
Like people who have asked me, hey, can you come on and do this or whatever, I have to
go through and kind of work through that list because there was a, it was like an eight
month period where we couldn't do anything like that.
Once I get through that, then we'll look at making livestreams happen more often.
Or you may just get lucky and get one by accident, or at a random time.
Okay.
For the most part, I like to call myself a realist, but most of the time I feel I tend
to be pretty cynical and pessimistic.
occurred to me that you also like to look at things from what I would consider a realist
point of view, but tend to shed light on things in a more optimistic way.
I wanted to ask you if you had any suggestions on how to alter my perception, but now I feel
like I'm asking you to be a therapist.
Okay, the key part of this is acknowledging the wins.
There are more than you think.
The problem is the world is so overwhelming.
There is so much that needs to be addressed, so much that needs to be fixed, so many perceptions
that I definitely feel need to be corrected, that the wins get lost sometimes, and you
don't take the moment to celebrate the wins.
Keep up on the news of something that you're interested in that is good, that has good
news occurring in it. Keep up on that news as well as everything else that's going on.
This is why y'all get so many videos that deal with advancements in space. It's kind
of a pallet cleanser, and when there is a win, like as far as police accountability
with the Kansas two-step, just take a moment and say, okay, this is something that's been
a problem for a while, federal judge, there's finally been action on it, acknowledge it.
with the price fixing. There is good news out there. The problem is that when it's
good news, it's a return to the status quo, most times. And most of the people
who watch this channel want more than the status quo. They want improvement on
the status quo, not just fixing something to get it back to the status quo. But
you have to acknowledge the status quo wins to to keep yourself in the fight
and let you know that you're not losing ground and that you can still make some
of those changes and you can still advocate for the world that you want.
That's that's what I would do and it's not that's not what I would do that's
That's what I do.
It helps.
Especially when there are, let's just call them highly emotional issues that oftentimes
lead to comments that really get under my skin.
And the other thing is remember that you can take a break.
I mean, that's funny, me saying it, but remember, you can take a break.
There will be problems there when you come back.
You won't run out of stuff to do.
Okay, and that looks like the end of the Q&A.
Alright, so hopefully you have a little bit more context, a little bit more information,
and having the right information will make all the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}