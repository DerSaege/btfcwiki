---
title: Let's talk about Biden, numbers, and an apology....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=347cOh2hHK8) |
| Published | 2023/11/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden expressed doubt about the numbers put out by the Ministry of Health in Gaza regarding the number of people lost.
- Beau acknowledges his history of doubting official numbers in conflicts and disasters, explaining his skepticism towards numbers that end in zero due to them being estimates.
- He points out that Biden's public perception was affected negatively when he made similar comments about estimates, especially concerning civilian or children lost.
- Beau mentions that Biden privately thinking of numbers as estimates is different from having a public record of doubting official figures.
- Despite Biden apologizing and meeting with Muslim American community leaders to address the issue, Beau believes questioning numbers immediately may not always be appropriate.
- Beau underscores the importance of waiting for more accurate numbers before drawing conclusions, especially in situations involving a large loss of civilians.
- He notes that minimizing civilian losses politically might not be advisable and can lead to unnecessary confusion and misinterpretation.
- Beau mentions that high emotions during times of loss can be exacerbated by careless comments, like the one made by President Biden.
- He praises Biden for acknowledging his mistake, apologizing, and spending extra time with those affected, indicating a positive step towards rectifying the situation.
- Beau concludes by reflecting on the incident and suggesting that acknowledging and rectifying mistakes is more significant than defending them.

### Quotes

- "I doubt any number that ends in zero, not just in conflict, but in natural disaster or mass incident, because they're estimates."
- "The public perception of that, yeah that's not cool."
- "When you have that kind of loss, emotions are high and there's no reason to add any additional confusion to it."
- "But it was a mistake."
- "Anyway, it's just a thought y'all have a good day."

### Oneliner

President Biden's public doubt about official numbers triggers scrutiny from Beau, who stresses the importance of waiting for accurate figures and handling sensitive topics with care and empathy.

### Audience

Media consumers

### On-the-ground actions from transcript

- Wait for more accurate numbers before drawing conclusions (implied)
- Handle sensitive topics with care and empathy (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of President Biden's public doubt regarding official figures and how Beau views such skepticism, stressing the importance of handling sensitive topics with empathy and waiting for accurate information before forming opinions.

### Tags

#PresidentBiden #OfficialNumbers #Skepticism #Empathy #Accuracy


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk about
President Biden and being skeptical of numbers. And a message that I got a couple of weeks ago
that I didn't answer and new reporting has made the message relevant again and now it seems like
a more appropriate time to respond to it. The message says, as somebody who repeatedly doubts
official numbers and has said that he doesn't believe any number that ends in zero, are you
going to defend Biden for doubting the numbers put out by the Ministry of Health? Now if you don't
what happened on October 25th. The president was talking about the numbers put out by the Ministry
of Health in Gaza and they were talking about the number of people lost and he was basically like,
and I have no notion as to whether or not these numbers are accurate, and that's what he said.
Now, to start off with, am I going to defend Biden?
I would like to point out that Biden has a whole press team to defend him when stuff
like this happens.
He doesn't need me.
But more importantly, new reporting says that in completely predictable fashion, the next
He was meeting with leaders from the Muslim American community and the meeting was supposed
to be 30 minutes and it lasted more than an hour and during it he said, I'm sorry, I'm
disappointed in myself, I'll do better and apologized for it.
So here's the thing.
You sent this to me because I have a very long and established record of doubting official
numbers from anywhere, anywhere, in any conflict.
I doubt Ukrainian numbers, especially those that are about the number of opposition that
were lost.
I doubt those, and I think most people watching this channel know that I definitely lean towards
the Ukrainian side there.
But I doubt those.
I doubt any number that ends in zero, not just in conflict, but in natural disaster
or mass incident, because they're estimates.
They'll be cleared up later.
I have a very public record of doing that.
To my knowledge, he doesn't.
So even if he thinks that way privately, and he looks at these numbers and he views them
as estimates or something like that, the very first time that I'm aware of him ever saying
anything like that publicly. It was in relationship to what people are widely
associating as either civilian lost or children lost. Regardless of his meaning
and what he believed, the public perception of that, yeah that's not cool.
that that doesn't come across right even if he even if he did it for the same
reasons that I do his he doesn't have that public record of doing it so it
looks horrible and yeah he should probably apologize for that and he did
you know. So one thing to remember is that there are times when it is best to
just not question that immediately and wait for more accurate numbers to come
out. The reason I don't believe numbers that end in zero, it's not because I believe people
are lying, it's because they're estimates. I believe that they're estimates. And it
applies across the board. And it's very easy to see that it applies across the board. In
situation where there is a large amount of civilian lost, it's probably not a
good idea politically to minimize that. I would point out that as somebody who
does it all the time, I don't think I've done it on the channel because it's
Because when you have that kind of loss, emotions are high and there's no reason to add any
additional confusion to it.
Because a statement like that can then be taken and turned into something that's not
really what was meant.
And again, I have no idea what he meant, but the public way in which that was done and
the way it was said, it was very off the cuff.
It seemed very nonchalant and yeah, it upset people, it bothered people.
The fact that the very next day he met with people in private about it, apologized and
And actually took the time to hear them out, apparently, for twice the allotted time.
And when you were talking about the President, the schedule was pretty tight.
I mean, that's a good sign, but him saying that the way he did, it was a mistake.
Clearing it up after the fact, good.
But it was a mistake.
And it's not really a thing that needs to be defended because he acknowledged it was
a mistake.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}