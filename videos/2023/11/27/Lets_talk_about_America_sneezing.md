---
title: Let's talk about America sneezing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YK4icGRjLPU) |
| Published | 2023/11/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Americans exporting culture leads to cultural shifts in other countries.
- Rise of right-wing figures internationally linked to American politics.
- After Trump's win, right-wing nationalists won in other countries.
- American voter allowing themselves to be tricked by rhetoric and imagery.
- Concern about Trumpism spreading internationally and returning to the US.
- Americans need to prevent the resurgence of far-right ideology.
- Focus on not allowing those who support far-right ideology back into power.
- Need to recognize and prevent the cycle of harmful ideologies.
- States proclaiming personal freedom may not actually have it.
- Americans must ensure not to fall for harmful ideologies again.

### Quotes

- "When America sneezes, Canada gets a cold, or when America sneezes, Europe gets a cold."
- "The far-right ideology, it's bad. It's bad."
- "You have to acknowledge that the fear isn't it's going to spread here. The fear is it's going to come back here."
- "We have to make sure we don't fall for it again as a country."

### Oneliner

American culture export leads to international political shifts, raising concerns about the spread of harmful ideologies back to the US.

### Audience

Americans

### On-the-ground actions from transcript

- Ensure to prevent the resurgence of far-right ideology (implied)
- Recognize and prevent harmful ideologies from gaining power (implied)
- Be vigilant against the spread of harmful rhetoric and imagery (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how American cultural shifts impact global politics and warns against the resurgence of harmful ideologies both domestically and internationally.

### Tags

#AmericanCulture #RightWingIdeology #InternationalPolitics #Trumpism #Prevention


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the one thing America
does really well, and we're going
to talk about what happens when America sneezes.
We're going to do this because there's a whole bunch
of messages talking about things happening in other countries.
And it's Americans talking about the internal politics
other countries. I mean, that's rare to begin with. I mean, to get one or
two is one thing, but to get a whole bunch of them, that's something else.
Because generally speaking, Americans aren't super concerned about the internal
politics of other countries. They all have the same concern, though. It's a
a rise of right-wing
populist figures. That saying,
when America sneezes, Canada gets a cold,
or when America sneezes, Europe gets a cold. It goes to the one thing
the US actually does well. Interestingly enough,
that saying originated during the Napoleonic era
And it was when Paris sneezes, Europe gets a cold.
The U.S.
What do we export?
Yeah, a whole lot of people drawing a blank right now.
We export culture.
It's the one thing that we export.
It's the one thing we do well.
We export culture.
And that's kind of what that saying really means.
I mean, it can apply to a whole bunch of other things, from the economy to, well, everything.
But when there are cultural shifts in the United States, you see them occur in other
places later.
And this is everything from music to fashion.
occurs. It also occurs with politics. After Trump's win, you actually saw a
bunch of right-wing nationalists win in other countries. It happened in two. This
is just carrying that on. Because the US was incapable of stopping itself from
falling for it. The rhetoric, the imagery, the ideas, they spread. We sneezed and it
spread. So you're seeing in other places now. Now keep in mind it's not all the
US. This is like one thing where you can say the US is exceptional but understand
it's not all the U.S. Some of this would have occurred anyway, it always does.
But by and large, a whole lot of this is a direct result of the American voter allowing
themselves to be tricked.
If you look at what people are calling these candidates, they're calling them the Trump
of Country X.
I mean, even in the way they're described, it's a callback to the U.S.
It's concerning for Americans because for a lot of Americans, we feel like that's passed.
We got past Trump.
We haven't yet, just to be clear, and Trumpism is still alive and well.
It's fading, and it could go away.
It needs a decisive defeat at the polls, but we'll see how that works out.
The concern is that because it's happening in other places, well, it might come here.
It was already here.
It was already here.
We sneezed and now it's everywhere.
The thing that Americans need to do is to make sure that we don't sneeze again.
That's the important part.
The far-right ideology, it's bad.
It's bad.
It never really does well for the people who support it, except for those people at the
very, very top.
So the U.S. has to make sure that at bare minimum, you know, we cover our mouth and
nose when we sneeze.
We have to limit it.
The rhetoric, it's starting to fade.
influence of a lot of those people in the US is starting to fade but it could
easily come back. If you're concerned about it, if you're looking at other
countries and you're seeing figures and you're like that's Trump just a European
version, you have to acknowledge that the fear isn't it's going to spread here. The
The fear is it's going to come back here, and if it does, it'll be exported.
It's a cycle.
If it is a concern, because in the American mindset, we can recognize that kind of stuff
in other countries, but it can't happen here, right?
It originated here, it originated here this time.
That's what Americans really need to focus on, is to making sure that they don't get
back in office.
The people who espouse that far-right ideology, that ideology that consists of othering, that
ideology that consists of power for them and you need to do what you're told, all wrapped
up in the cloak of freedom.
We saw recently with the personal freedom thing, those rankings, the states that talk
about it, they don't have it. If your area is free, you don't have to proclaim it. You
don't have to advertise it. It's just known. We have to make sure we don't fall for it
again as a country. That's what you can do. If you're concerned about it, you see it
happening overseas, it's easier to recognize, we have to make sure that we
We don't fall for it again.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}