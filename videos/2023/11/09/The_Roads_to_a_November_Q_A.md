---
title: The Roads to a November Q&A
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7lJr0d0KUL0) |
| Published | 2023/11/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an impromptu Q&A session to maintain the routine on the channel.
- Questions are a mix of light-hearted and serious, reflecting current challenging times.
- Shares recent songs listened to and plans for restarting live streams.
- Addresses a viewer's query about a young Palestinian girl and assures she is alive.
- Talks about the "How to Ruin Thanksgiving Dinner" series and its significance.
- Explains his approach to covering Gaza in a detached manner, focusing on the bigger picture.
- Declines condemning actions, stating it's unnecessary given the existing coverage.
- Mentions receiving requests to condemn specific incidents but refrains from releasing certain videos.
- Advises caution on sharing sensitive content like a song due to current emotional climate.
- Responds to a relationship issue involving differing core values and political views.
- Comments on the alarming statements made by some Republican candidates regarding Mexico.
- Talks about missing presence on Twitter and shares a humorous anecdote involving Halloween decorations.

### Quotes

- "All of my homies know that civilian loss is bad."
- "There may be people around you who are personally impacted by it."
- "That kind of bandwagon mentality and grouping people together like that, it's never good."

### Oneliner

Beau provides impromptu Q&A, addresses tough queries, and advises caution on rhetoric in sensitive times, urging awareness of diverse perspectives.

### Audience

Community members

### On-the-ground actions from transcript

- Be conscientious of your words and their impact on people around you (implied).
- Choose your words carefully, especially when discussing sensitive topics like the situation in Gaza (implied).

### What's missing in summary

Advice on being mindful of the impact of words and the importance of understanding diverse perspectives for effective communication in challenging times.

### Tags

#Beau #Q&A #Gaza #Community #Communication #Values


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we don't really have anything special planned.
We're going to do a quick Q&A. I realized we were coming up
on Thursday, and normally on Thursday morning, something
goes out on this channel that we just have for the week.
That wasn't something that was planned, but it kind of turned
into a schedule accidentally.
So for those people who really like to keep to their
schedules, we have this.
It's a quick Q&A. I haven't seen the questions.
I was told that even though I requested it be light-hearted,
that it's not.
They have some light-hearted ones mixed in.
But most of the questions that came in
have not been light-hearted, which
that stands to reason with everything going on.
OK, so what's the last song you listen to?
Donatella by Lady Gaga, and I'm not even
going to elaborate on that.
Are you ever going to restart your live streams?
Yes, yes.
Some of you may know that we're almost at the point
where we're ready to do a live test
and see if what we have figured out actually works.
If not, don't worry.
They'll restart soon anyway at a different location
or something like that.
We'll figure something out because we have the fundraiser
for the shelter coming up that we do every year.
So that will happen this month.
So you'll definitely see one soon.
And my guess is it'll restart with that.
From that point forward, we'll probably have one
a month or something.
OK.
Note, this is only here because I
I think you can tell her what happened to her.
When you said another generation lost their youth, it hit me hard.
And I thought about this young Palestinian girl from years ago who punched an Israeli
soldier.
I remember her because she had blonde curls like I did when I was a kid.
I guess I don't really have a question.
It's just sad.
I just wonder if she's still alive or if she's OK.
She's still alive.
Her name's ahead.
She is still alive.
That little girl is 22 now.
She was just in the news.
She got detained and has been released.
But yeah, she's still alive.
Surely.
Okay, are you going to do the How to Ruin Thanksgiving Dinner series again this year?
If so, when can we send questions in?
Yes, and now.
For those not familiar with that series, that's one that we put out, I think almost every
or we've done it, where we talk about how to talk about various topics with your
MAGA relatives, basically, is what it has turned into.
We often get told that we shouldn't talk about certain topics, and I get it on some
level, especially if you're trying to have a civil conversation, but the things we get
told not to talk about, you know, money, religion, and politics.
What are most people in the world dying from?
Maybe it is a good idea to talk about it.
Why did you decide to cover Gaza in such a detached way?
normally have a lot of emotion. No shade. Your channel is a nice break from all of the emotion.
I'm just wondering why you chose to do it this way. I can guess where you stand
because of other coverage, but I never heard you condemn anything.
When I say this, this is not a criticism of anybody who went a different route with it.
that this is, again, I believe in a diversity of tactics.
This is just the way I see it.
I think there's enough people that are staring earnestly
into a camera somewhere totally safe with a full belly talking
about how horrible it is.
I think everybody knows how horrible it is.
I think there's plenty of coverage on that.
And I don't think there's a lot of people and there's a lot of coverage about the big
picture stuff and what the major players are doing or trying to do.
And I think that's important.
I'm of the firm opinion that if you want to fix something, you have to know what's broke.
So that has a lot to do with it.
As far as I've never heard you condemn anything, again, not a criticism of those people who
have done this, but if I had to come out and say, you know, hurting civilians is bad, I
would feel like my previous 4,000 videos were a waste.
Like I would feel, I feel like that's not a position I have to clarify.
And I just, I don't, with all of the emotion right now, I don't know that it's necessarily
helpful for me to do that.
Again, that's not true for everybody.
Some people definitely do need to do it.
And if nobody was doing it, I would probably feel more compelled to do so.
But well, I mean, now is as good a time as any.
I've actually been keeping a tally.
And it's basically marking the requests that I get as far as I would like you to publicly
condemn this.
I don't have it in front of me, so these numbers are the last numbers I can remember.
I've had 67 requests to condemn the deaths of Palestinian civilians.
I have had 54, I think, requests to condemn the deaths of Israeli civilians.
I have had two, that number I'm certain of.
I have had two requests to condemn the deaths of civilians without specifying which side.
I think that that says a lot.
I know people will want to talk about scale and all of that stuff, but I don't really
I think that those numbers are commentary in and of themselves, as far as where people
are at.
I actually have a number of videos I've recorded on the topic that I have decided that I'm
not going to release, at least not anytime soon.
And sadly, they'll still be relevant for a while.
It's because where people are at right now, it's a lot of emotion-based thinking and
any resolution to this is, it's not going to come from emotion-based thinking.
He shared a song once that sounds like it's a normal country-go-America-support-the-troops
song, but then there's a big twist that's very relevant right now.
I don't want to say it in case you read this one live.
Wanted to let somebody listen and couldn't remember it.
The name of it is Rich Man's War by Steve Earle.
I know which song you're talking about.
I would caution you, given what I just said about where people are at and how emotional
a lot of people are right now, I'm not sure that that twist is going to be well received
by a lot of people.
The message that's actually being conveyed in the song, it's timeless, but right now
It may not be the appropriate moment to bring that particular message out.
I know you're not a therapist, but you fall into surrogate dad role.
I've been with a guy since COVID, and he's always been a good guy.
Over the last month, he said things about Jews that have been red flags.
I think all religions are stupid, but this is more than that, it's hate.
Is it just because of the war, or is this really who he is?
I think if you sent this message, you know, again, it's core values.
If the core values don't align, they don't align.
And if you're talking about something that is more than something that you find acceptable,
I mean, it seems like it would be OK if it was just like making fun of a religion based
on your statement.
But this is more than that, it's hate.
If you don't want to be around it, then I wouldn't be around it.
When do you take a day off?
After hearing you say four videos per day, seven days per week, I scrolled back more
than a year and never saw you miss a day." You'd have to go back to 2020 for me to
miss a day of publication, but that doesn't mean I record every day. Like
actually the day y'all watch this, unless there's breaking news, I won't record
anything that day. I can record them ahead of time and then they'll go out,
but I at the same time I also don't actually I don't take a day off unless
there's you know a reason to I don't take a day off just because it's a
specific day of the week or something like that it's my work-life balance
That's not a thing for me, I mean it is, but it involves an everyday routine, not a Monday
through Friday or whatever type of routine.
But with that being said, actually just to give everybody a heads up in case it does
happen. I have an old injury that is acting up which may which may require me
to miss it a couple of days so if that occurs don't worry and that should
happen pretty soon if it needs to. Okay, I'm a leftist not a liberal. My boyfriend
My friend is also a leftist, or at least, I thought so.
He said what's happening in Gaza was good, because it would help the Palestinians long-term.
I've heard you talk about how outrage creates recruitment, so I understand what he's saying,
but now I can't look at him.
People are dying and it's good?
That's a question.
WTF am I supposed to do with that?
When I told him I thought that was horrible.
He said I was too weak for the revolution.
I want to leave him.
Am I overreacting?
Again, core values.
Core values.
There are a lot of people who have never actually seen the horrible, horrible things.
who have never seen that kind of loss, who have very strong opinions on how
useful that loss is. It's not uncommon for people to say things like that, and
most times it's younger men who are trying to show how tough they are, for
lack of a better word. That being said, if you told him that it bothered you and he said
you were too weak, yeah, I mean, again, it goes back to core values. And I also think
that you already know the answer when you send this.
All of my homies know that civilian loss is bad.
I mean, I don't know how to respond to that.
It's happening a lot right now.
I have seen it in circles adjacent to mine, not in mine because most of the people in
my immediate circle, they have seen that.
And even if you really don't like the other side, nobody actually thinks it's good.
I've seen it in adjacent circles and it's very common right now.
It is, it's like going through the end of 2001 all over again and watching people say
things that you may not be able to forget what they said.
What's up with all the Republican candidates saying they're going to attack Mexico?
Yeah, I've seen that.
That's a new one.
If you have missed this, if you've missed this, a lot of Republican presidential candidates
to show off their foreign policy skills, I guess, have decided that they're going to
promise to go after the people bringing stuff across the border and all of that, and that's
how they're going to pursue the war on drugs and all of that, is through military force
inside Mexico. I've seen it. I would hope they're not serious. And I would hope that,
you know, they understand that. I mean, there's a word for that. It's called invasion. It
doesn't matter if you're only going after a certain subsection of their citizens. That's
it's an act of war against the country unless they authorize it. If they were authorizing
it, that would be a different story.
But given the way they're talking about it and what they're talking about doing, I think
it's pretty unlikely that the Mexican government would be like, yeah, why don't you do that?
Aside from that, I mean, we've had Tom Clancy movies about this.
It generally doesn't work out well.
For that issue, you have to address demand, not supply.
And certainly, after all of this time losing the war to plants, I'm not sure using the
actual military to do it at that level, the level they're talking about, is a good idea.
In fact, it seems like a really horrible one to me.
Okay.
I miss you on Twitter.
me something Miss Bo did to you recently. So for those who were never on Twitter when
I was on Twitter, my wife was on too. And we would, I guess our dynamic was entertaining.
She is, I mean, she's somebody who would marry me. She's not exactly a timid person
and she has a very unique sense of humor, so there were a lot of funny stories that
got shared.
Something she did to me recently, cleaning up Halloween decorations.
She took a skeleton that had, it has like a hat and a banjo, anyway, it's a skeleton,
and she set him in the passenger seat of my Jeep.
And I didn't notice it until, I don't remember what I was doing, but I was checking on something
at like two o'clock in the morning.
And I walk out, I get in my Jeep, and I look over, and I get into a fight with another
Halloween decoration.
This time I won though, unlike with the werewolf.
That was the most recent thing that I think she did.
There haven't been any new mysterious horses that have shown up that she pretends aren't
really there.
Nothing like that.
Although I do think that there's extra cats, but they may have just wandered up out to
the barn.
But yeah, that's pretty much it, and that is the end of the questions.
So yeah, this was really just to have something out here on Thursday because I know that people
get used to a schedule.
Now that we realize that this has become routine, we'll try to make sure that we have something
a little bit more substantial than a real quick run through some questions.
There's a lot of emotion out there right now, obviously, and it might be wise to be
very conscientious of what you say.
There may be people around you who have an affiliation that you don't know.
There may be people around you who are personally impacted by it.
There are certainly people around you who are gauging future involvement with you based
on what you say, based on your rhetoric, how willing you are to accept horrible things.
It is exactly like the end of 2001 and watching people advocate for things that if they put
any thought into it, they probably wouldn't. But right now, there's still a lot of emotion
clouding judgment. So just be conscientious of what you have to say and remember that
But those demographics that are in the United States, that have a direct connection to this,
a lot of them are scared.
They are scared and they have reason to worry.
There have been incidents targeting innocent people in the United States on both sides.
It's probably very important that people choose their words carefully when they're talking
about this because you will forever alter relationships with people or end them, apparently
at least a couple, and remember that the person who you knew two months ago, they're the
same person you knew today, just because something happened a world away.
That kind of bandwagon mentality and grouping people together like that, it's never good.
It's never good.
That kind of othering, it literally never leads to good things.
There are people who are around you who are probably very sensitive to what is being said,
and you may not know who they are.
And once you say it, they may not be able to look at you again.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}