---
title: Let's talk about Irish sentiment on the situation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=F1HPFUy2wxU) |
| Published | 2023/11/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the vested interest of Irish and Irish-Americans in the Middle East conflict.
- Points out the misconception that the Irish were once supportive of the US but now support Palestinians due to becoming leftist, clarifying that Irish Republicans were always leftists.
- Attributes the change in Irish support to the partition of Ireland mirroring the partition of Israel, leading to a shift in identification from Israelis to Palestinians.
- Mentions the lasting impact on Irish politics, with overt connections drawn during the Troubles in Ireland.
- Describes the strong connection between Irish Americans and the Palestinian cause, influenced by historical dynamics.
- Addresses the coverage of conflicts and how they elicit reactions based on the interests of different groups in the United States.
- Emphasizes that countries don't have friends, only interests, which can change over time.
- Stresses that war and conflict are inherently bad, showcasing the importance of understanding different perspectives and interests.

### Quotes

- "Countries don't have friends. They have interests."
- "War is bad. It's always horrible."
- "It's all based on the perceptions of the time and the interests at the time."

### Oneliner

Beau explains the historical roots behind the strong interest of Irish and Irish-Americans in the Middle East conflict, clarifying misconceptions and shedding light on the shifting dynamics that influence perspectives and reactions.

### Audience

Irish Americans, Middle East conflict observers

### On-the-ground actions from transcript

- Understand the historical perspectives and dynamics that shape different communities' interests in conflicts (implied).
- Educate oneself on the connections between historical events and current sentiments in communities affected by conflicts (implied).

### Whats missing in summary

In-depth exploration of the complex historical and political factors influencing Irish and Irish-American perspectives on the Middle East conflict. 

### Tags

#Irish #IrishAmericans #MiddleEastConflict #HistoricalPerspectives #PoliticalDynamics


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Irish interests
and why it exists.
We're going to do this because over on the second channel
there was a question at the ends of the most recent The Road's
Not Taken episode.
And I answered it because I thought it was common knowledge.
I didn't expand on it.
It became very clear within minutes
that it's not common knowledge, and I did not
do a good job of going over why.
Dozens of questions came in asking
why the Irish or Irish-Americans have a vested interest
in the current conflict in the Middle East
and why their opinions are very strong.
I read all of the questions.
going to read this one because it's broad in scope and I think it will answer everybody else's along
the way. Okay, I watched your recap video with the questions at the end. You talked about how the
Irish and Irish-Americans have an interest in quote the conflict. That was surprising to me I
have to admit I wanted to know which side they were on. I know that sounds horrible. When I
read about it, I began to understand less. It seems like in the beginning they supported us
and were our friends, but somewhere it changed and they supported the Palestinians. What I was
reading said this was because the Irish became leftist. I think I learned from you that they
were always leftists. I'm just wondering if you could tell me what happened. Yeah. Okay, so yes,
Irish Republicans are nothing like American Republicans and this goes back
to look at 1916 the GPO the post office look at the proclamation that's not a
right-wing document so from the beginning when this topic came up the
the Irish movement was already left. So that's definitely not the change. The
source you were reading probably didn't want to acknowledge what caused the
change because it's really really simple. In the beginning the Irish were
incredibly supportive of the idea of a homeland for Jewish people. Why? Because
they saw parallels. They can identify with it. They were a people who wanted
their homeland as well. So they saw this connection and it didn't end there.
There's also the massive migration. There's a whole bunch of things that
drew parallels. Ireland changed over time. Ireland became a country but not all of
it. It was partitioned. It was partitioned. When Israel became Israel, what happened?
A partition showed up in Israel. At that point, the Irish no longer identified
with the Israelis. They started to view the Israelis as more like the British.
because of the partition. At that point, sentiment started to grow for the
Palestinian side and that sentiment still exists today. Throughout the
troubles in Ireland, you will see overt connections being drawn. In fact, one of
the more famous images as far as art and murals that came out of Ireland is an
Irish whatever term you want to use. Somebody who was active in the conflict
holding a rifle and another person's holding their rifle. They're
Palestinian and below it it says one struggle. That connection, it
still impacts Irish politics today in the country. Here in the US, if you were
to walk into an establishment that passes the hat for the calls, you would
You probably see Irish decor everywhere, on the walls, posters, murals, everything would
be about Ireland.
But somewhere in there, there'd be a Palestinian flag.
That connection, it's based on the partition, it has nothing to do with right and left.
something that exists in the Irish consciousness. And to be fair, I think it
may actually be more pronounced among Irish Americans than Irish in
Ireland. It still impacts their politics, but I don't know that it is
as unified. And I could be wrong about that. But that's what it comes from. It comes from
shifting dynamics and a shifting perception. It's really that simple. And again, reading
the message, I wanted to know what side they were on. I know that sounds horrible. When
I read about it, I began to understand less. It seems like in the beginning they supported
us and were our friends. Countries don't have friends. They have interests. It changes.
That's where it comes from.
That's why the coverage elicits such a response.
And that was the basis of the question over on the other channel, was why is there so
much reaction in the United States to this conflict?
And that goes to two things.
And if any conflict was covered the way this one gets covered, showing the footage that
is shown, it would elicit a reaction.
At the same time, news outlets aren't incentivized to provide coverage like that of every conflict
because they don't have built-in groups in the United States who care about it.
don't care about the conflicts, the other conflicts, the same way. In the United
States, obviously Jewish Americans, they have an interest in what happens. Arab
Americans have an interest in what happens. So do Irish Americans. So it
has a built-in audience and so people are, people in news agencies who decide
on coverage, they know that this will be viewed because there's a built-in audience.
When that coverage is provided, other people who don't have a vested interest, they see
the coverage and they also have a reaction.
It's important to remember that what you're seeing, that's war.
That's what conflict looks like, all of it.
It's not always covered that way.
People don't see the footage.
People don't see the inhumanity.
But trust me, that's every conflict all around the war world.
War is bad.
It's always horrible.
But that's the basis of it.
The basis of the shift, the basis of the interest.
It's all based on the perceptions of the time and the interests at the time.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}