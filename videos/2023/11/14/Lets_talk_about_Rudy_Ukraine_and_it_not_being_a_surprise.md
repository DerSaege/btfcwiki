---
title: Let's talk about Rudy, Ukraine, and it not being a surprise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Unby0Cupv3I) |
| Published | 2023/11/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau sets the stage by introducing a story with a long winding road, leading up to an impactful revelation.
- The story focuses on Ukrainian member of parliament Dubinsky and his associates who gained American attention in 2019 for uncovering alleged corruption involving money laundering at Burisma.
- Dubinsky later met with Rudy Giuliani, and Ukrainian intelligence has arrested Dubinsky for treason, suspecting his involvement with a group financed by Russian intelligence.
- Dubinsky denies the allegations, claiming political persecution, and faces a potential 15-year sentence if found guilty.
- The individuals who initially claimed to uncover the corruption at Burisma have been arrested for treason, accused of working for Russian intelligence.
- Ukrainian intelligence suggests that the group's goal was to undermine Ukraine's credibility internationally during a tense situation.
- Despite investigations yielding no results, the initial press conferences shaped the narrative surrounding Burisma, associated with Hunter Biden in Ukraine.
- The individuals involved in the press conferences have now been arrested for treason, leading to questions about their intentions and ties to Russian intelligence.
- The situation raises concerns about potential further developments regarding Burisma and its implications.

### Quotes

- "The people who claimed to have uncovered this that started this whole story thing going on and really gave it a lot of a A lot of credibility, been arrested for treason because they were allegedly working for Russian intelligence."
- "Press conferences who, again, were given by people who have now been arrested for treason because they were working for Russian intelligence."
- "I mean, yeah, we'll probably hear more about this."

### Oneliner

Beau unravels a story involving Ukrainian politics, corruption allegations at Burisma, and ties to Russian intelligence, leading to arrests and potential implications for Ukraine's credibility.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Stay informed on developments in Ukrainian politics and potential corruption scandals (implied)
- Support efforts to uphold transparency and combat corruption in political systems (implied)

### Whats missing in summary

Insights on the potential impact of the ongoing developments on Ukraine's political landscape.

### Tags

#UkrainianPolitics #Corruption #Burisma #RussianIntelligence #Treason


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to,
we're gonna talk about a story
that has a long winding road.
And I'm going to wait to get to the important part
until I tell you the whole story
because it's just better that way.
So you're gonna have to bear with me.
The story centers on a Ukrainian member of parliament,
of Parliament, a person named Dubinsky. Now Dubinsky and some of his associates,
they first came to American attention back in 2019, in November and
December of 2019, because they provided some press conferences in which
they said that they had uncovered something and everybody wanted to know
about it. They had uncovered some corruption, even said that there was
money laundering in this company called Burisma. Maybe y'all have heard of it.
And then later on, Dabinski met with another high-profile person in the
United States, a person named Rudy Giuliani. Dabinski met with Giuliani in
2019 as well. So Ukrainian intelligence has recently taken an interest in
Dubinsky and by taking an interest I mean they arrested Dubinsky for
treason. Some of the other associates, some of the people that were involved
in the stuff dealing with our press conference, yeah they're in hiding in
other countries now. And the allegations center on Dubinsky and others joining
this group, allegedly joining this group, that was financed by Russian intelligence.
millions were provided according to Ukrainian intelligence. Now, of course,
Dubinsky says that it's political persecution, it's a witch hunt, and yeah,
he's saying that he did nothing wrong. He is currently, if found guilty, he will be
looking at 15 years I think. I mean that that's quite the story you know if you
you follow it all the way back. So the people who claimed to have uncovered
this that started this whole story thing going on and really gave it a lot of a
A lot of credibility, been arrested for treason
because they were allegedly working for Russian intelligence
for the tune of millions.
I mean, who could have seen that coming?
Yeah, so I feel like we're going to hear more
about Burisma, and for those who have forgotten that company name that is the company that
Hunter Biden was associated with in Ukraine.
And these press conferences had a lot to do with how it was perceived.
Even though the investigations kept turning up nothing, these press conferences helped
shape the narrative.
Press conferences who, again, were given by people who have now been arrested for treason
because they were working for Russian intelligence.
And interestingly enough, Ukrainian intelligence says that one of the things that they were
supposed to do is take advantage of a tense situation and undermine Ukraine's credibility
on the international stage.
I mean, yeah, we'll probably hear more about this.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}