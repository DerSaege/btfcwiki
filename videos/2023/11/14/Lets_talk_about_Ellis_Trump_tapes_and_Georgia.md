---
title: Let's talk about Ellis, Trump, tapes, and Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nj-K5W18pj4) |
| Published | 2023/11/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing videos of Ellis and Powell in Georgia discussing staying in power.
- Ellis recounts a chilling exchange where they planned to keep Trump in power at all costs.
- Segments from the plea agreement video are circulating, but not the whole context.
- The significance of these videos might not lie in swaying Georgia's case.
- The focus is on a specific disturbing exchange between Ellis and Scavino.
- The intention behind leaking these videos seems unclear and haphazard.
- Despite lacking convincing content, the leaked videos hint at disturbing intentions.
- Conversations planning to stay in power post-election create legal implications.
- Evidence from these videos may impact ongoing cases against Trump and his associates.
- The material from these videos could be pivotal for the federal DC case.
- Every piece of evidence contributes to building a case against Trump.
- Implications of planning to retain power despite election results are concerning.
- The leaked video segments may resurface in similar contexts in the future.
- The videos play a role in bolstering the special counsel's case against Trump.
- The unsettling exchanges hint at a willingness to subvert democracy and legal processes.

### Quotes

- "The boss is not going to leave under any circumstances."
- "We are just going to stay in power."
- "It's worth remembering that when it comes to the DC case, the federal DC case, that yeah, right now it's Trump."
- "This material or material very very similar to it we're gonna see again."
- "Y'all have a good day."

### Oneliner

Videos of chilling exchanges in Georgia reveal intentions to retain power at any cost, impacting legal cases beyond Georgia and potentially resurfacing in future investigations.

### Audience

Investigators, Legal Professionals

### On-the-ground actions from transcript

- Contact legal authorities with any relevant information on the cases against Trump and associates (implied).
- Stay informed about ongoing legal proceedings related to these video revelations (implied).

### Whats missing in summary

Detailed context and analysis of the entire plea agreement video, as well as potential implications for future investigations.

### Tags

#Georgia #Trump #LegalCases #VideoRevelations #Democracy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Ellis and Trump
and Georgia and other places in those videos
and how those videos may not actually,
they may not be as important in Georgia.
They may have another significance here.
So if you have no idea what I'm talking about,
Some segments came out.
They're pieces of the proffer that Ellis and Powell
did in Georgia, part of their plea agreement.
It's a video of them talking about various things.
And it's important to note that these are segments.
This isn't the whole thing, like not even close.
And people are talking about various parts of it.
This is the part that really caught my attention.
It is Ellis, and she's talking about a conversation
that happened right after she kind of apologized
for how the court cases went.
And he said to me in kind of excited tone,
well, we don't care, we're not going to leave.
And I said, what do you mean?
And he said, well, the boss, meaning President Trump,
and everybody understood the boss,
that's what we called him,
he said, the boss is not going to leave
under any circumstances.
We are just going to stay in power.
And I said to him, well, it doesn't quite work that way,
and you realize.
And he said, we don't care.
That was a conversation that Ellis says happened between her and Scavino.
This is not a super huge piece of evidence in the Georgia case.
I can assure you that that's on a piece of paper in Smith's office.
That's going to matter then.
So I don't really understand the purpose of these videos coming out the way they did.
There might be a reason, but it seems to be kind of haphazard and I don't understand
the purpose.
Because there's nothing in here that I'm looking at and I'm like, yeah, this would convince
somebody who was like on the fence to take a deal. So it doesn't seem
intentional in that way. But the idea that during this period after the
election before Jan 6 there were conversations happening where they were
just like, yeah, we're going to stay, we don't care, testimony to that effect, I
mean that's going to be a problem for the former president and those around
him. It's worth remembering that when it comes to the DC case, the federal DC
case, that yeah, right now it's Trump. It doesn't have to stay that way and all of
the evidence that's coming in it's helping the special counsel's office
build their case. This material or material very very similar to it we're
gonna see again. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}