---
title: Let's talk about real life Star Wars or Star Trek....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=f5zHFS2gtOc) |
| Published | 2023/11/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing news overshadowed by other events.
- Rocket allegedly fired from Yemen, intercepted by Israel using Arrow 2 system.
- Intercepted rocket reported to be above 62 miles, potentially in space.
- Possibility of humanity's first space battle.
- Implications of combat taking place in space.
- Speculation about future international treaties or new definitions regarding space.
- Mention of space being reserved for scientific endeavors.
- Likelihood of space capabilities being sought after by major powers.
- Suggestion of a new dividing line between scientific space and the rest of humanity.
- Anticipation of future global attention to this event.
- Choosing between a future resembling Star Trek or Star Wars.
- Beau signing off.

### Quotes

- "Humanity is going to have to decide whether we want Star Trek or Star Wars."
- "Space generally speaking was supposed to be something that was reserved for the good things about humanity."
- "Expect a lot of conversation about this internationally, maybe even treaties."
- "Right now, it's probably not going to get much attention."
- "The capability to intercept these kinds of missiles, it's going to be something that most large powers want."

### Oneliner

Beau addresses an overshadowed event: the potential first space battle with intercepted rockets, sparking future global deliberations between Star Trek and Star Wars.

### Audience

Global citizens

### On-the-ground actions from transcript

- Stay informed about international developments in space technology and treaties (suggested).
- Engage in dialogues about the militarization of space and its implications (implied).

### Whats missing in summary

Further details on the implications and consequences of potential militarization of space. 

### Tags

#Space #RocketInterception #GlobalImplications #StarWars #StarTrek


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about some news
that I feel like might have been overshadowed
and didn't necessarily get the treatment it deserves.
The actual event was reported on,
but I don't know that it was really put into the framing
that it should have because it's material
we will definitely all see again.
a historic event that I'm sure that this generation is tired of living
through them but definitely a first for humanity if the reporting is correct. So
not too long ago there was reporting that a rocket was allegedly fired from
Yemen and that Israel intercepted it with the Arrow system, the Arrow 2 system.
That was reported on. The reporting says that that interception took place above 62
miles, 62 miles above sea level, took place higher than that. That means that
it was above the Karman line, which is a theoretical line that divides things below the Karman line,
can fly using normal aerodynamics, things above it can't. It is a loosely agreed-upon line that
separates the atmosphere from space. It is entirely possible that we just had our first Star Wars,
our first space battle. Actual combat taking place in space. Again the
altitude has not been totally confirmed yet but based on the information that's
currently available it certainly seems like that happened. Space generally
Space speaking was supposed to be something that was reserved for the good things about
humanity, you know, scientific endeavors and stuff like that.
If this reporting is confirmed, expect a lot of conversation about this internationally,
maybe even treaties, or maybe a new definition of where space actually begins because the
capability to intercept these kinds of missiles, it's going to be something that most large
powers want and they want to maintain.
So there may be a new line that above that is supposed to be scientific in nature and
below it is the rest of us.
Right now, it's probably not going to get much attention.
Right now, things occurring more here on Earth are going to take precedence.
But later on, this is going to be a conversation.
And humanity is going to have to decide whether we want Star Trek or Star Wars.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}