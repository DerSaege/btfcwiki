---
title: Let's talk about the Supreme Court and speeding....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QT9kY0m4SuQ) |
| Published | 2023/11/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Growing up in a sparsely populated area divided by cops into three sections, with one cop for a third of the county.
- No speed limit enforcement on the east side when the cop was at the Waffle House on the west side.
- Lack of enforcement on highways due to their isolation from bigger agencies.
- Comparison between the Supreme Court's code of ethics and the cop at the Waffle House scenario.
- The Supreme Court's code of ethics lacks enforcement mechanisms, making it more like suggestions than rules.
- Media scrutiny is the only real oversight, but it doesn't have much power.
- The code of ethics, although well-intentioned, lacks compliance measures.
- Violations of the code may lead to scandals but lack real consequences.
- The Supreme Court's code of ethics needs actual compliance measures to ensure its effectiveness.
- Implication that Congress may need to address the issue of compliance with ethical frameworks.

### Quotes

- "But the cop is always at the Waffle House."
- "It's a nice set of rules, but there's not really anything that happens after that."

### Oneliner

The Supreme Court's code of ethics, like a cop at the Waffle House, lacks enforcement, making it more like suggestions than rules.

### Audience

Lawmakers, Ethicists, Activists

### On-the-ground actions from transcript

- Push for legislative action on enforcement mechanisms for ethical standards (implied).

### Whats missing in summary

Beau's engaging storytelling and unique analogy between speed enforcement by a cop and the enforcement of ethical standards by the Supreme Court. 

### Tags

#SupremeCourt #Ethics #Enforcement #Compliance #Legislation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
the Supreme Court and speeding.
You know, when I was a kid, I grew up in a very,
let's just say, sparsely populated area.
Today, it's like a major destination for people,
but when I was younger, when I was in my late teens,
There was nothing, I mean, literally nothing there.
And the county, it was divided by the cops
into three sections.
There was the north end, central, and the south end.
And at night, on the south end, they
had a cop, a cop for a third of the county.
And what that meant was that if you
were on the west end of the county where the Waffle House was, when you saw that
cop go in to that Waffle House to eat, it meant that if you were going east, there
wasn't a speed limit. I mean there was, there was, it was posted and everything,
but there was nobody to enforce it at all. And you could cover that 40, 50
miles in no time, because there was nothing there.
And the highway patrol, the bigger agencies, they didn't
come down there.
That highway was a two-lane highway with trees right at
the edges.
So even if one did pass you and saw you, he wasn't going
to try to turn around.
He'd flash his lights, and that'd be the end of it.
So the point of this is that the Supreme Court,
they issued their code of ethics.
And it's the same thing.
But the cop is always at the Waffle House.
It's posted.
There are rules.
But there is absolutely no mechanism
for enforcement whatsoever, which means, well,
more like suggestions, I guess.
The only real check on it would be the press looking into it, which is what brought all
of this about.
And even though there were behaviors that a large portion of the United States viewed
as unethical, well, nobody really resigned, right?
So even the check that does exist doesn't really have any power.
It's more like one of those red light cameras, but it can't issue a ticket.
At the end of this, the Supreme Court has put out a code of ethics that if you are trying
to establish a framework by which to make sure things are done in the way they're supposed
to be, you know, some kind of ethical framework, that they left out the part
where it actually has to be complied with. It's a nice set of rules and if
somebody violates it, sure the media will report on it and there will be a scandal,
but there's not really anything that happens after that. I have a feeling that
that there will be people in the U.S. Congress that feel like maybe that's not enough.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}