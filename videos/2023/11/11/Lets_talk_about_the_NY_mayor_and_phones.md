---
title: Let's talk about the NY mayor and phones....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m-Jr9HCAoOg) |
| Published | 2023/11/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mayor of New York, Eric Adams, had his phones and electronic devices taken by the FBI, reportedly for looking into campaign finance and money from overseas.
- Reports differ on whether Mayor Adams willingly gave his phones to the FBI or if they were seized with a warrant.
- The mayor's office insists he is cooperating and has nothing to hide, implying voluntary surrender of the phones.
- If the phones were seized with a warrant, it suggests suspicion of wrongdoing, requiring legal representation.
- Conflicting reports create uncertainty, with no clarification from the FBI.
- Forming a definite opinion before clarity on the situation is cautioned against due to the stark contrast between scenarios.
- Lack of sufficient information at the time of filming to determine the actual sequence of events.
- Regardless of how the situation unfolds, the FBI is currently examining the Mayor of New York's phone.
- The ongoing saga surrounding this incident indicates it will not be resolved swiftly or quietly.
- Further developments are awaited to understand the true nature of the situation, indicating that this issue will persist.

### Quotes

- "There's just a massive amount of difference between those two scenarios."
- "This is not a story that's going to go away anytime soon."

### Oneliner

Mayor of New York's phones seized by FBI; conflicting reports create uncertainty, stressing the importance of awaiting clarification before forming opinions.

### Audience

Concerned Citizens

### On-the-ground actions from transcript

- Contact local officials or organizations for updates on the situation (suggested)
- Stay informed through reliable news sources to understand the evolving narrative (implied)

### Whats missing in summary

The full transcript provides detailed insights into the conflicting reports surrounding the seizure of Mayor Eric Adams' phones by the FBI, urging viewers to withhold judgment until further clarity emerges.

### Tags

#NewYork #FBI #Mayor #EricAdams #Conflict #Investigation


## Transcript
Well, howdy there, internet people. Led Zebo again. So today we are going to talk about New York
and the phones and the mayor of New York and some reporting that probably needs to be expanded on
before anybody starts to form an opinion because you have words being used that mean different
things and they will lead people to draw different conclusions. Okay, so let's go
over what we know. The mayor of New York, Eric Adams, his phones and some other
electronic devices are now in the hands of the feds, the FBI has them now. Now we
We know that the FBI was looking into some campaign finance stuff and whether some money
came from overseas, that kind of stuff.
Some of the reporting says that his phones were seized.
And some of the statements basically paint it more like the FBI showed up after he was
giving a speech or there was some event and was like, hey, can we take a look at your
phones?
he was like, yeah, sure, and then gave them like an iPad or something later.
Okay, those are two very, very different things because the whole statement that is coming
out of the mayor's office is basically, they're not looking at him, he's cooperating.
He doesn't have anything to hide, so he gave up his phones, okay, and that would go along
with them showing up and saying, hey, can we look at your phones?
That's one thing. If they were seized, meaning the fed showed up with a warrant and were like,
give me your phones. If that's the case, the mayor might need to be a little bit more concerned than
he currently appears to be. Generally speaking, by the time the feds have enough to come get your
phone, there is probably at least a suspicion that you have engaged in some
kind of wrongdoing. The problem is, based on the reporting, it's very contradictory
and the FBI office, of course, isn't commenting. They normally don't. So
they're not really providing any clarification. But based on which one of
those stories people are hearing first, they're forming opinions. Until we know
which one it is, I wouldn't form a hard opinion on this yet. There's
just a massive amount of difference between those two scenarios. So when
you're looking at this story, before you really kind of get in there and you know
throw all your weight behind one idea and how it's playing out, I would wait to
have that clarified first because if they got a warrant to physically take
his phones, I mean he should probably get an attorney at that point. If they
just came up and said, hey you know we heard some stuff and we think some
information on your phone might be able to clear it up and it has nothing to do
with you whatsoever, but we'd like to take a look at him and he was like yeah
sure here you go. That's something entirely different, but based on the
reporting it's very conflicting and we don't know which one it is yet. I haven't
been able to to kind of sort through it at time of filming. There's just not
enough information there to to know which one of those scenarios is at play.
Either way, at this point, the feds are looking through the Mayor of New York's phone.
This is not a story that's going to go away anytime soon.
We'll have to wait for further developments and see which direction it heads, but this
isn't something that is just going to be downplayed and forgotten about.
Something is going on here.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}