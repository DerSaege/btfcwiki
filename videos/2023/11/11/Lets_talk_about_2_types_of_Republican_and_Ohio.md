---
title: Let's talk about 2 types of Republican and Ohio....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=apcf2PXPoXY) |
| Published | 2023/11/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the debate between the two types of Republicans: conservative versus authoritarian.
- Describes the pushback he receives for suggesting there are two distinct types of Republicans.
- Talks about recent events in Ohio regarding reproductive rights and the contrasting statements made by different factions.
- Points out the authoritarian tone in one statement regarding the Ohio legislative decision.
- Contrasts this with a more conservative viewpoint expressed by another Republican figure.
- Emphasizes the importance of recognizing the existence of both conservative and authoritarian factions within the Republican Party.
- Urges Republicans to critically analyze the statements made by their party members.
- Warns about the authoritarian mindset seeking to control rather than represent the people.

### Quotes

- "Your voice is irrelevant."
- "There are two types of Republicans now."
- "They're not about being your representative. They are about being your ruler."
- "You can either stand for the conservative values that you profess, or you can get in line and do what you're told."
- "It's going to be you."

### Oneliner

Beau explains the divide between conservative and authoritarian Republicans, using Ohio's reproductive rights vote as a prime example of contrasting viewpoints within the party.

### Audience

Republicans

### On-the-ground actions from transcript

- Analyze the statements made by Republican figures regarding political issues (suggested)
- Stand up for conservative values within the Republican Party (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current divide within the Republican Party, urging individuals to critically think about the statements and actions of their party members.

### Tags

#Republicans #Conservative #Authoritarian #Ohio #PoliticalDivide


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
the two types of Republicans.
And we are going to talk about Ohio.
And we're going to talk about two statements.
You know, the idea that there are two types,
two distinct types of Republican today,
it's something that I kind of get a lot of pushback on.
Because the majority of people who watch this channel,
they support the Democratic Party, some begrudgingly,
because they're not far enough left.
But the majority of people are of that opinion,
and they don't like it.
Because most of them are like, no,
there's one kind of Republican,
and they're all authoritarian goons.
Then from the Republican side of the aisle,
No, no, Republicans aren't authoritarian, they're conservative, and you need to learn the difference.
And it's funny to me because when you look at the statements from people who are obviously
from different factions, you can't help but acknowledge that that split exists.
So that's what we're going to do today, and this might be something that should be part
of the, you know, how to ruin Thanksgiving dinner series.
Because if you have a Republican in your family who doesn't believe that the Republican
Party is shifting away from being just conservative and supporting the Constitution or even the
basic idea of representative democracy, this might be useful.
If you missed it, there was a vote in Ohio.
Reproductive rights was on the ballot, and people voted in a landslide in favor of having
reproductive rights.
Go figure, people like rights.
This is one statement.
To prevent mischief by pro-abortion courts with issue 1, Ohio legislators will consider
removing jurisdiction from the judiciary over this ambiguous ballot initiative.
The Ohio legislature alone will consider what, if any, modifications to make to existing
laws based on public hearings and input from legal experts.
The general tone of that is, even though it was a landslide, well, you commoners better
just do what you're told like the obedient lackeys you're supposed to be.
We don't care about we the people.
You don't get a voice.
That's us.
We're your betters and accept it.
If you vote in a way that we don't like, we're literally going to change the rules.
That's one statement.
Here's another, and to be clear, just to make sure nobody thinks that this is like one of
those rare moderate Republicans or anything, this is somebody who signed a six-week ban,
and this is what they had to say.
We accept the results in regard to issue one.
In a democracy in a country like ours, things are always open to discussion as we move forward.
I think the people of the state will want to take a look at once this goes into effect.
They will have the opportunity to continue to judge how that is in fact working.
So we will see and we'll see how that evolves.
version. I don't like this at all, but this is how the people voted and I am so
certain that my position is right. Well, let's just let them see what they
get now that they voted this way. We'll put it into effect and because I know my
position is right, they won't like it and we'll reevaluate it later. One of these
statements is a conservative statement. The other, I don't think that you could
frame that as normal conservative thought. It's very authoritarian. You have
to acknowledge the two factions exist. They're there. That second
statement, that's from the governor, and it goes with a standard line of thinking
that good ideas generally don't require force. If you think it's a good idea, but
you're outnumbered, well, let them try it. They'll find out they're wrong because
you believe in what you're saying. You think that if they see the alternative
they'll decide that's not really what they want and they'll come around to
your way of thinking. That's a conservative viewpoint. It's not my
viewpoint to be clear, like when it comes to this particular issue, but it's a conservative
viewpoint and it's in line with the Constitution.
It's in line with the idea of a representative democracy.
It's in line with the founding principles, the good ones.
The other, no, it's not in line with any of that because they're flat out saying we
We know what you voted, but you're not smart enough to know what you want.
So we'll just change the rules to make sure that you get what you deserve.
Well, we'll change the rules to make sure that we get what we want.
Your voice is irrelevant.
There are two types of Republicans now.
One of them is a conservative.
The other expresses authoritarian viewpoints that are far outside anything that aligns
with what could even remotely be perceived as American values.
If you are a Republican, look at these statements.
Look at them for yourself.
Read them in context and acknowledge that a large portion of your party, they don't
represent you. They don't represent the way you think. They don't represent
anybody but themselves. They're not about being your representative. They are about
being your ruler. And in some cases they'll come right out and tell you that.
So you can either stand for the conservative values that you profess,
or you can get in line and do what you're told. Just understand that once
that precedent is set, it's not going to be those other people that are being
told what to do.
It's going to be you.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}