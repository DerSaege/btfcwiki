---
title: Let's talk about Biden skipping COP28....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1wfo8tuvJvI) |
| Published | 2023/11/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden is skipping COP28, the climate conference hosted in the UAE, a massive oil producing country.
- The UAE planned to use the conference to work out new oil and gas deals.
- Despite being the 28th conference, there's still no plan to phase out fossil fuels.
- Some believe that COP conferences have become getaways for heads of state rather than focusing on climate.
- Biden's absence may send a message that the conference has deviated from its purpose.
- Climate change is a global issue, and every country has a stake in it.
- Biden's decision to skip the conference may not be solely due to the UAE's hosting or the oil and gas deals.
- The President is currently dealing with multiple pressing issues and a trip to the UAE may not fit in his schedule.
- Regardless of Biden's intention, his absence could still send a strong message.
- Beau supports the idea of sending a message by skipping the conference.

### Quotes

- "Biden is skipping COP28, the climate conference hosted in the UAE, a massive oil producing country."
- "Despite being the 28th conference, there's still no plan to phase out fossil fuels."
- "Some believe that COP conferences have become getaways for heads of state rather than focusing on climate."

### Oneliner

Biden skipping COP28 in the UAE sends a message about the conference's deviation from climate focus amid pressing global issues.

### Audience

Climate activists, policymakers

### On-the-ground actions from transcript

- Advocate for stronger climate commitments at local and national levels (implied)

### Whats missing in summary

More details on Biden's complex schedule and global challenges he is currently facing.

### Tags

#Biden #COP28 #ClimateChange #UAE #FossilFuels


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden skipping COP28,
the climate conference,
because it appears he is not going.
And we're gonna do this because I got a message.
And the message was basically,
hey, I was hanging out with some of my friends
and they are very much environmental people.
And when the subject of Biden skipping COP28 came up,
They were happy about it.
I didn't want to ask them why and seem dumb,
so I'm asking you.
OK, so if you're not familiar with this, it is a meeting.
It's a conference that occurs.
It has occurred numerous times in the past.
And this year, it is going to be hosted in the UAE,
massive oil producing country, you know. The host of the conference is the boss
of an oil company and reporting came out saying that the UAE planned on using the
conference, the climate change conference to work out new oil and gas
deals. On top of that we are on top 28. This is the 28th one of these and we
still don't have a plan, pledge to phase out fossil fuels. There are a lot of
of people who believe that this has turned into a getaway for heads of state and their
closest associates, and it's not really about the climate anymore.
There are some people who believe that him skipping it will send a message.
The US won't be unrepresented.
There's a bunch of other top officials going, but Biden himself is apparently skipping it.
So there are people who are very much into the environment who kind of view this as a
good thing, view it as sending a message saying, this is not what this conference is supposed
to be about.
We're not coming.
And I can get behind that.
I don't have a problem with it being hosted in the UAE.
Climate change is real and it's going to impact every country.
They have a stake in it.
oil and gas deal thing? Yeah, I mean that's an issue. So I definitely understand where they're
coming from. The other thing I want to point out about this though is that may not be why Biden's
not going. People may be assigning a motive to that that isn't really there. It's worth remembering
that the President of the United States is currently dealing with trying to get aid to
Ukraine, the situation in the Middle East, a legislative agenda that has froze because
of the dysfunction in the House, the budget coming up, so on and so forth.
There's a whole lot on his plate right now and a couple weeks in the UAE, long trip,
maybe it just doesn't fit.
But I'll take it.
Whether or not he is intending to send that message, I think that message is going to
be sent.
So okay.
This is one of those moments where I'm not going to argue with results.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}