---
title: Let's talk about Trump and sarcasm....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NRPSK4P292Y) |
| Published | 2023/11/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing media coverage of former President Trump's confusion between Obama and Biden, attributing it to sarcasm and jokes.
- Explaining Trump's statement about Obama when he means Biden, suggesting it's all a sarcastic joke.
- Trump clarified in a social media post that he's not confused but being sarcastic, implying Obama is still running everything.
- Referencing Trump's ability to recall "person, woman, man, camera, TV" as proof that he's not slipping.
- Describing Trump's statement about Orban being a great leader of Turkey as a Thanksgiving joke that people didn't get.
- Asserting that Trump's sarcasm is a coping mechanism for stress, expecting him to make more jokes no one understands.
- Implying that Trump will later explain his jokes on social media because people are not smart enough to get them.
- Suggesting that Trump will continue to use sarcasm to cope with stress and that people should accept his humor even if they don't understand it.

### Quotes

- "Not all of us are stable geniuses."
- "He's probably going to make a lot more jokes that nobody gets."
- "He's using sarcasm to cope with the stress."
- "And we're just not smart enough to get the joke."
- "He'll tell us later what we should believe."

### Oneliner

Former President Trump's confusing statements are all just sarcastic jokes that people don't understand, according to Beau, who believes Trump uses humor as a coping mechanism for stress.

### Audience

People following media coverage.

### On-the-ground actions from transcript

- Accept Trump's humor even if it's not understood (implied).

### Whats missing in summary

The full transcript provides a satirical take on media coverage of Trump's statements, suggesting they are all sarcastic jokes and humor used as a coping mechanism for stress.

### Tags

#FormerPresidentTrump #Sarcasm #MediaCoverage #Humor #CopingMechanism


## Transcript
Well, howdy there, internet people.
Led's Bo again.
So today, we are going to talk about former President Trump
in humor, sarcasm, jokes, things that we don't get,
basically, and we're going to do this
because it's become very, very important and we have
to clear some stuff up.
See, recently, the media has been talking
about the former president and how maybe
the former president is confused, slipping.
That's how they've been describing it.
Because he keeps saying Obama when he means Biden.
Like he would say President Obama
when I beat President Obama or whatever,
but he really means President Biden.
And there were a lot of reports about this,
and some people started making jokes about it.
And because we know Trump has a very thick skin
and wouldn't be bothered by any of that, he clarified.
He put out a social media post explaining that no,
he's not confused, he's not slipping,
he's just being sarcastic.
It's his way of insinuating that Obama is actually
like still running everything.
It's a joke, we're just not smart enough to get.
It's really that simple.
I believe him.
I mean, you have to, right?
I mean, you have to remember that he did say person, woman,
man, camera, TV.
Not everybody can do that.
That's very hard.
That proved that he's not slipping.
So that's obviously he's telling the truth now.
Just like when he said that thing about Orban, he said
he was a great leader of Turkey.
That was also a joke that we didn't get.
He was being sarcastic because, well, it was a Thanksgiving joke is what it was.
It was a Thanksgiving joke.
It was just ahead of its time, like everything with Trump, it was just ahead of its time.
Hungry, turkey, Thanksgiving.
That's how it happened.
It's not that he's slipping.
And it's very important that people understand that he didn't misspeak.
They just don't get the joke.
So everybody knows that.
And see, I think personally that this makes a whole lot of sense.
I definitely believe him because if you really think about it, generally when you're under
a lot of stress, you make sarcastic jokes.
You don't start slipping because there's a lot of things on your mind or anything like
that.
No, you become sarcastic.
that's obviously what's going on here. I don't see why this is such a big deal. I would expect
the former president to become more and more sarcastic as time goes on. He's probably going
to make a lot more jokes that nobody gets and then he'll tell us about them later on social media
when people bring them up. Because, again, he's using sarcasm to cope with the stress.
And we're just not smart enough to get the joke. So he'll tell us later. Not all of us
are sable geniuses. And we just need to accept that and understand that if he says something,
it's funny, even if we didn't get it, and that he'll tell us later what we should believe.
sarcasm.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}