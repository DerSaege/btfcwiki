---
title: Let's talk about Putin, persuade, promise, pay, and pitchforks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vIMs21gWWkQ) |
| Published | 2023/11/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin is perturbed by the possibility of protests in Russia, particularly after a demonstration by soldiers' wives in Moscow.
- The demonstration was about the indefinite deployments with no rotation system in Russia.
- Unlike the US and most countries, Russia lacks a functioning rotation system for combat deployments.
- Requests for additional demonstrations in St. Petersburg and Moscow have been denied.
- Officials have been sent out to persuade, promise, or pay the relatives to prevent further protests.
- The Kremlin believes the main issue behind the protests is late pay for the soldiers' wives.
- Paying them on time might not address the deeper issues causing discontent.
- The Kremlin's concern about the demonstrations indicates a potential erosion of morale among soldiers and their families.
- If soldiers' families are protesting, it suggests a significant problem with morale and support for the military.
- Focusing solely on timely payment is unlikely to resolve the underlying issues causing unrest.
- The lack of rotations for soldiers could be a key factor contributing to the discontent.
- The internal morale in Russia may not match the official narrative presented by the government.
- The situation reveals uncommon insights into the internal dynamics of Russia that are not frequently reported.
- The discontent among soldiers' families signifies a more significant issue than just late pay.
- The lack of rotations appears to be a critical issue contributing to the unrest in Russia.

### Quotes

- "Focusing solely on getting them paid on time, that's probably not going to be effective."
- "If soldiers' families are protesting, there's a real issue."
- "The morale inside Russia, it is not as it is being portrayed by official channels."

### Oneliner

Putin's concern over protests in Russia reveals deeper issues than just late pay for soldiers' wives, indicating potential morale erosion within the military and society.

### Audience
Activists, Military Families

### On-the-ground actions from transcript
- Support soldiers' families in their demands for fair treatment and better conditions (suggested).
- Raise awareness about the lack of rotation system in Russia's military to address underlying issues (implied).

### Whats missing in summary
Further insights into the impact of morale erosion on Russia's military readiness and societal stability.

### Tags
#Putin #Russia #Protests #Military #Morale


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Putin
and persuade, promise, or pay in pitchforks.
We'll talk a little bit about the internal situation in Russia
and how things are developing, how they're progressing along,
because some recent reporting has suggested that,
Well, it seems that Putin is particularly perturbed by the possibility of protests pulling people to the streets.
In Moscow, on November 7th, there was a demonstration, and it was soldiers' wives, their relatives, but mainly their
wives.
And they were upset, primarily, about the indefinite
deployments with the lack of rotation.
So if you don't know, Russia does not have a functioning
rotation system.
So whereas in the United States, a combat deployment
is a set period of time.
And this is most places.
as most countries. Over there, if somebody was lucky enough to have made it, they could
have been there on day one and still be there today with no rotation back home. This, of
course, is being viewed as unsustainable, but the demonstration apparently really got
Putin a little concerned. Requests for additional demonstrations like permits
in St. Petersburg and Moscow have been denied and officials have been sent out
to basically keep the relatives off the streets and they were told to persuade,
promise, or pay to stop them from taking to the streets and expressing their
concerns. So, the Kremlin apparently believes that the real reason the wives
are mad is that the pay is late and they're not getting the pay on time, and
that's the reason they're upset. So, they want to get that fixed. I mean, that
That probably doesn't help, but when it seems clear that there are other issues, that's
probably not the only problem, and paying them on time may not actually be the solution.
This kind of discontent, right now it apparently is more alarming than it should be to the
Kremlin, but the concern is not unwarranted. When you start having the
family members of those that are actually fighting, demonstrating, that's
a clear indication that the overall picture when it comes to what the
citizens actually believe and whether or not they support the military
It's a clear picture that morale is eroding. Generally speaking, soldiers'
families are supportive and they don't want to undermine morale. If it is bad
enough for them to take to the streets, there's a real issue. Their focus on
getting them paid on time, that's probably not going to be effective.
That's probably not going to stop this. It seems clear that it's the lack
of rotations that is really causing an issue. So just a little glimpse of
some stuff that we don't see very often and some reporting on some stuff that we
don't get to hear about much. The morale inside Russia, it is not as it is being
portrayed by official channels. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}