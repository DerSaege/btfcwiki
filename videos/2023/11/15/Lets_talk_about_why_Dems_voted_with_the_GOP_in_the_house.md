---
title: Let's talk about why Dems voted with the GOP in the house....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iCIh8QznURA) |
| Published | 2023/11/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The House of Representatives passed a temporary funding bill due to the Republicans' inability to agree on a budget.
- About half of the Republican Party voted against the bill, influenced by the far-right Freedom Caucus.
- The Freedom Caucus has caused discord within the Republican Party and led to McCarthy's dismissal.
- Beau suggests that engaging in bipartisan efforts and allowing dissent within the party can render the far-right faction irrelevant over time.
- Continuously opposing bills and losing weakens the far-right faction's political power and credibility.
- The Democratic Party's support for bipartisan bills helps diminish the influence of the far-right within the Republican Party.
- Beau stresses that this strategy is necessary to keep the government functioning and reduce the power of those focused more on social media presence than policy.
- The pushback the new speaker receives for this strategy will determine its continuation.
- Beau believes it is the right move for a speaker to address and undermine the influence of the far-right faction within the Republican Party.
- This approach aims to prevent economic issues and government shutdowns while sidelining the faction that focuses on Twitter popularity over policy.

### Quotes

- "Helping them burn their political capital."
- "It is good for the Democratic Party for that faction of the Republicans to lose sway."
- "The Democratic Party sided with the Republicans to stop economic issues from a government shutdown."
- "You're probably going to see comments from other Republicans who aren't part of that far-right faction that are probably going to seem as though they really support them."
- "I'd be prepared to see a whole lot of Republicans say things they don't mean and alter previous positions."

### Oneliner

Beau explains how bipartisan efforts are key to neutralizing the far-right faction's influence within the Republican Party, ensuring government functionality and diminishing political power.

### Audience

Political activists, concerned citizens.

### On-the-ground actions from transcript

- Contact your representatives to support bipartisan efforts in government (suggested).
- Stay informed about the actions and positions of your elected officials (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics within the House of Representatives and offers insight into strategic bipartisan approaches to governance.

### Tags

#HouseOfRepresentatives #Bipartisanship #FarRightFaction #GovernmentFunctionality #PoliticalPower


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about what happened
in the House of Representatives
and whether or not the new speaker
is starting to do something
that a lot of people believe to McCarthy should have.
Okay, so if you have missed the news,
the House of Representatives passed a temporary funding bill.
Again, the Republicans still don't have their act together
enough to get any kind of actual budget going, but there's a temporary bill that funds different
parts of the government either through January 19th or February 2nd.
Okay, so what happened?
Because as soon as the vote was public, questions came in.
Why would Democrats vote for this?
Okay, so the vote itself, I want to say it was 336 to 95, which means about half of a
Republican Party voted against it. The reason half of them did is because of
that far-right caucus, the Freedom Caucus I believe is what they're called. This is
the group that has led to McCarthy's dismissal from his position. This is a
group that in many ways is responsible for a lot of the discord within the
Republican Party. When McCarthy was still Speaker of the House, we talked about it.
The move to make is to basically engage in some bipartisan stuff, get some help
from the Democratic Party, get something through, and allow them to vote against
it. Allow the far-right Republicans to vote against it. Over time they render
themselves irrelevant. They lose power. If they constantly come out against a
bill like they did with this one and they lose, they lose power. They push
themselves into irrelevance. Now realistically the amount of the amount
of people within that group that are truly those making the decisions and
swaying everybody else. It's like a dozen. But the other ones, they follow their lead.
If they continue to lose and they can't actually get their agenda through, their base will
eventually start to see them as people who perform for social media. And it reduces their
political capital.
The fact that they are opposed to a bill, it means less today than it did yesterday
because they don't have the juice to stop it from going forward now.
Helping them burn their political capital.
If this is followed up on with more votes that the majority of Americans want, pushing
forward more bipartisan stuff, simple stuff, and that side of the Republican Party continues
to be against it and they continue to lose, it totally undermines them.
It is good for the Democratic Party for that faction of the Republicans to lose sway.
So that's what's going on.
And realistically, they were pretty much out of time again, because the Republican
Party can't put together an actual budget in large part due to the demands of that far
right contingent.
So this is the Democratic Party keeping the lights on and making that first step with
the new speaker to help render the Republican Party faction that is about Twitter likes
instead of policy, helping kind of make them irrelevant.
Now the amount of pushback that the new speaker gets for this move is going to determine whether
or not he continues doing it.
It is the right move and eventually some speaker is going to have to do it.
So that's what's going on.
That's why the Democratic Party sided with the Republicans, but when you actually look
get it there's more Democratic votes than Republican. It's a move to
stop economic issues from a government shutdown and to further the idea of that
faction of the Republican Party. They really don't matter and that
appears to be this way. Remember that you're probably going to see comments
statements from other Republicans who aren't part of that far-right faction
that are probably going to seem as though they really support them. The
votes are going to say otherwise. I would be prepared to see a whole
lot of Republicans say things they don't mean and alter previous positions while
this process is going on as long as the speaker has the political will to continue, but it is the
right move. It's one that multiple commentators talked about back when McCarthy was still with
that because at some point somebody's gonna have to do it. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}