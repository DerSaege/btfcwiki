---
title: Let's talk about Teamsters, Sanders, Oklahoma, and images....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_hC9FHhRKVE) |
| Published | 2023/11/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a recent incident in the United States Senate involving a Republican Senator from Oklahoma, Bernie Sanders, and a union member named Sean O'Brien.
- During a hearing about economic issues, the senator confronted O'Brien for past social media comments, leading to a tense exchange.
- The senator appeared ready to physically fight O'Brien during the hearing, showcasing a lack of maturity and professionalism.
- Beau warns against picking fights with Teamsters, citing their reputation for toughness and solidarity.
- Beau contrasts the current behavior in the Senate with past experiences where physical confrontations were more common but not celebrated.
- He questions the display of aggression in a legislative setting and suggests that allowing Bernie Sanders to calm the situation didn't convey the intended message.
- O'Brien's response to the senator's challenge with a smile indicated a non-intimidated demeanor, contrasting with the senator's attempt to appear tough.
- Beau criticizes the trend of valuing politicians' tough-guy behavior, equating it to immature displays from high school.
- He expresses disappointment in the lack of maturity and adult behavior in the Senate, suggesting that such conduct doesn't truly represent aggressive individuals.
- Beau likens the incident to a senator trying to portray a cowboy image inadequately, pointing out the transparency of such attempts.

### Quotes

- "First things first, you never fight a teamster."
- "If your intent is to show that you are just a super aggressive man and you're ready to go anywhere, even in the middle of this Senate hearing, maybe it's best that you don't allow Bernie Sanders to strong-arm you back into your chair with words."
- "We're comparing the behavior of today to the behavior of my friends in high school."
- "Acting like my friends from high school? Probably not. Probably not the image you should be trying to cast."
- "It's supposed to be the most deliberative body in the United States."

### Oneliner

Beau delves into a Senate confrontation, warning against aggression, questioning maturity, and critiquing tough-guy politics in a legislative setting.

### Audience

Legislators, political commentators.

### On-the-ground actions from transcript

- Contact your representatives to advocate for professionalism and maturity in legislative conduct. (implied)
- Organize community dialogues on respectful communication and conflict resolution in political settings. (implied)

### Whats missing in summary

The full transcript provides additional context on the incident in the Senate and Beau's reflections on political behavior and maturity.

### Tags

#Senate #PoliticalConduct #Professionalism #Teamsters #BernieSanders


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
the United States Senate is a very, very serious place.
We will talk about the Republican Senator
from Oklahoma, Mullen.
We will talk about Bernie Sanders.
And we will talk about a man named Sean O'Brien,
who is a union person.
I would imagine by this point in time,
a whole bunch of people have already
provided commentary on this.
But I would like to talk about something else.
I feel like I have a little bit of insight into this one.
If you have no idea what I'm talking about
and you missed the news, during a hearing,
and it's important to understand,
this is during a hearing.
This isn't in a hallway outside somewhere.
This occurred during a hearing
about basically about some economic issues.
Sean O'Brien, the Teamster, he's a witness.
And apparently, at some point in the past,
O'Brien had said mean and hurtful things
about the senator from Oklahoma on Twitter or Facebook
or wherever.
The senator asked O'Brien something to the effect of,
you said any time, any place, this is a time,
this is a place.
And there's a quick back and forth.
And then the senator stands up, like he's ready to go.
He's going to fight right there in the hearing.
He's going to fight a teamster.
And that's probably as good a place to start as any.
First things first, you never fight a teamster.
And that's not advice saying they're super tough guys.
You never fight a teamster.
You had better be ready for that third fist
to come in from somewhere because it's going to.
There's a reason they have the reputation they have,
particularly when you're talking about those actively
involved in the Union. That's just a bad move to begin with. The other thing is, and I get
it for a lot of people, this is embarrassing, it shows the childish nature of Republicans
in the Senate. I understand all of that, but maybe it's because I come from a different
time, a different culture. These fights were not uncommon. They weren't even looked down
upon. It was just something that happened. But I can tell you this, if any of my
friends were ever like, this is a time, this is a place, and then stood up like
they were villages super froggy, and Bernie Sanders was able to hold them
back and make them sit down, they would never hear the end of it. That's
worse than losing the fight. That they would be teased. We would still be
teasing that person about this to this day. If your intent is to show that you
are just a super aggressive man and you're ready to go anywhere, even in the
middle of this Senate hearing, maybe it's best that you don't allow Bernie Sanders
to strong-arm you back into your chair with words. It doesn't actually send the
message that I think that the senator might have been trying to send.
Generally speaking, those people who actually are aggressive like that, they
don't they don't care about about somebody just reminding them hey this
isn't the place to do this. The other thing that I think is important to note
about this dynamic is that at the end the senator said something like well
what did you mean by any time any place and O'Brien the teamster was like I just
wanted me to have a cup of coffee, discuss our differences, and then he
smiled. Anybody who has ever been around tough guys, that was not a smile of
acquiescence, that was not a smile of somebody who was intimidated. I
understand that it's embarrassing to even have an incident like this occur in
the Senate. I get that and sadly it wasn't even the only one today or at
least the only one recently. I get that it's embarrassing for that reason but I
think it might be more embarrassing that for a large segment of the American
public, the desire for politicians to be tough guys and act in that way, that's
seen as a positive by some people. We're comparing the behavior of today to the
behavior of my friends in high school. I feel like the United States Senate
should be a little bit more adult than the way me and my
friends behaved. I don't feel like that that's really appropriate, but at the
same time, it didn't reflect what truly aggressive angry people are really like.
It didn't show that. It showed to me this whole thing was a whole lot like a
like a senator who went to Harvard or Yale showing up at some ranch in a pair
cowboy boots he bought the day before in a rented or borrowed truck wearing a
cowboy hat and trying to cast an image. It's easy to see through them most times.
If you're in the US Senate you're supposed to be a smart person. It's
supposed to be the most deliberative body in the United States. Acting like my
friends from high school? Probably not. Probably not the image you should be
trying to cast anyway it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}