---
title: Let's talk about about Trump creeping to the edge....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=p1xf1oR5M2I) |
| Published | 2023/11/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump reposted a message calling for Letitia James and Judge Ngorong to be placed under citizen's arrest for election interference.
- Despite the post's specifics, many might perceive it as a call to action, especially amidst ongoing trials.
- The severe nature of this statement could have significant consequences for Trump, potentially altering how judges view his social media conduct.
- Beau suggests that even for Trump, this post is extreme and may lead to criminal implications if acted upon.
- The downstream effects of this post could be long-lasting and may influence future legal proceedings involving Trump.
- Beau anticipates that this incident will not be easily forgotten and could have repercussions in various cases.
- Trump's attorney might need to closely monitor his social media to avoid legal repercussions.
- The situation is unprecedented and unpredictable, with implications that could extend far beyond the initial post.

### Quotes

- "Despite the post's specifics, many might perceive it as a call to action, especially amidst ongoing trials."
- "The severe nature of this statement could have significant consequences for Trump."
- "Even for Trump, this post is extreme and may lead to criminal implications if acted upon."
- "The downstream effects of this post could be long-lasting and may influence future legal proceedings involving Trump."
- "The situation is unprecedented and unpredictable, with implications that could extend far beyond the initial post."

### Oneliner

Trump's reposted message calling for citizen's arrest could have severe consequences and influence legal proceedings, posing a grave challenge for his social media conduct.

### Audience

Legal professionals, activists.

### On-the-ground actions from transcript

- Monitor and scrutinize Trump's social media posts closely to prevent potential legal repercussions (suggested).
- Stay informed and engaged in legal proceedings related to Trump's social media conduct (implied).

### Whats missing in summary

Legal context and potential ramifications of Trump's social media posts.

### Tags

#Trump #SocialMedia #LegalConsequences #CallToAction #ElectionInterference


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the former president
of the United States, we're gonna talk about Trump
and a social media post that he shared,
that he amplified, that he put out to a whole bunch
of people, we're going to talk a little bit
about the mechanics there, and then we're gonna talk
about where it might go from here.
because his rhetoric is certainly amping up.
Before I read this, I do want to point out the notion,
the legal mechanism of citizen's arrest is so narrow
that in most places, in most cases, it's not even a thing.
incredibly specific thing for a specific situation. If anybody was to attempt to
fulfill the the fantasy that the former president amplified, their best-case
scenario would be a really long time in prison. So this is what Trump reposted.
fantasy. I would like to see Letitia James and Judge Ngorong placed under
citizens arrest for blatant election interference and harassment." And that's
what he put out. That's what Trump, that's the message
Trump amplified. Despite the language in it, I feel like many people are going to
perceive this as a call to action. A call to action that is, it's not a thing, it's
not a thing. This post being shared against the backdrop of the trial
going on dealing with the person who went to Pelosi's home, this is
unbelievable. I mean, it shouldn't be. It's Trump. He has no regard, doesn't
appear to have any regard for those people who still trust him, those people
who are swayed by him, those people who, if they follow this, they will end up in
a world of trouble. So, where does it go from here? Honestly, I don't have a clue. The
people mentioned in this are kind of outside the scope of the gag order. At
the same time, this is such a severe statement and the foreseeable
consequences of it are so dramatic, there might be consequences for the
former president in New York. It will certainly be used in other cases. There
is no doubt in my mind that this statement, this particular social media
posts that Trump shared will surface again and again in regards to other cases trying
to stop Trump from potentially inciting somebody.
We're going to see this over and over again.
The fact that it was made, it is surprising.
might be something that alters the judge, judges, plural judges, in different places,
truly alters the way they look at Trump's social media shenanigans.
The term out of line, I don't know that that covers it.
This is something else, and the fact that a former president of the United States would
put something like this out is...
Even for Trump, this is out there.
If this is acted upon, it will not help Trump.
For those who might be listening, this will not help Trump.
It's hard to say this early on, but the downstream effects from this post being made, something
that, in essence, is something that could easily be perceived as a request to physically
act against these two people.
We're going to hear about it over and over again.
And if the former president isn't very, very careful with their social media, this may
be an instance where somebody turns a civil case into a criminal one.
We haven't heard the end of this.
This isn't like a lot of the other posts the former president has made that were inflammatory,
that were things that weren't in good taste, that were, you know, maybe even defamatory.
There were a lot of posts that he's made that have brushed up against normal lines.
This one's different.
And it is so different, in fact, that I don't think that anybody can realistically predict
where it goes from here and what the different judges do in response and how they view it.
is this is going to be out there in every one of these cases forever now.
This is not one that's going to go away.
This isn't one that's going to be forgotten about.
If I was the former president's attorney, I would be trying to maybe review his social
media before he posts, just in an attempt to protect your
client.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}