---
title: Let's talk about why Russia hasn't quit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OTF8hVm4BEQ) |
| Published | 2023/11/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the reasons why Russia started the war with Ukraine, including stopping Ukraine from moving West, deterring NATO expansion, natural resources, and asserting power.
- Notes that Russia's actions have not achieved their intended goals: Ukraine moved closer to the West, NATO expansion accelerated, and Russia did not gain the desired resources or global power status.
- Russia persists in the conflict to save face and maintain the perception of power, resorting to imperialism by holding onto territory.
- The Russian populace is comfortable with being lied to, and losing the territory taken in the conflict would be a significant blow to national identity.
- Russia's challenge is that they are not winning the war, facing a dedicated Ukrainian opposition that aims to reclaim all lost territory with Western support.

### Quotes

- "Russia has to be seen as powerful."
- "If they lose that, if they lose the territory that they have taken, this was a total loss in every way shape and form."
- "The reason to continue fighting is a face-saving measure."
- "They want to take it all back and they're willing to fight for it."
- "The wider geopolitical war was lost by Russia very, very early on."

### Oneliner

Beau explains why Russia persists in the conflict with Ukraine despite not achieving its objectives, facing a dedicated opposition determined to reclaim lost territory with Western support.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support Ukraine with real, tangible aid and assistance (implied)
- Stay informed on the situation in Ukraine and Russia, share knowledge with others (implied)

### Whats missing in summary

Detailed analysis and historical context; full emotional impact of Russia's motivations and Ukraine's resilience.

### Tags

#Russia #Ukraine #Geopolitics #Conflict #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about a question
and making sure you're not just consuming
one side of a story and we're going to talk about Ukraine
and we're going to talk about Russia
and we're going to talk about why Russia
hasn't just given up because the question that came in
was, I'm going to paraphrase here,
but it was, hey, I don't want to fall into an echo chamber
And everything I see says that Ukraine just keeps winning.
Why does Russia keep sticking its hands into the grinder here?
It's a good question, and it's being asked for a good reason.
OK.
So to start with, why did Russia start this?
Why did Russia start the war?
Why did they elect to invade?
Not the reasons they gave.
Not the weapons of mass destruction reasons.
The real reasons for the war.
Number one, stop Ukraine from moving to the west.
They didn't like that Ukraine was leaning into the west.
Russia had been trying to stop that for a really long time.
That's why they kept interfering in the elections and all of that stuff.
Number two, they wanted to deter NATO expansion against their borders.
Number three, natural resources.
And number four, they wanted to announce that they were a player at the big table again.
They were a major, they were a superpower.
It's very important to the Russian psyche, the collective Russian psyche, for Russia
to be seen as powerful.
and I know that sounds silly, USA, we're number one,
greatest country on the planet and all that stuff.
We're number one in what?
But everybody keeps saying it.
It's part of the national identity.
So those are the real reasons.
Those are the real reasons.
Let's run through them.
Did this stop Ukraine from moving to the West?
No, it drove Ukraine directly into the arms of the West.
Did it deter NATO expansion against their borders?
No, it sped it.
Did they get the natural resources that they wanted?
No.
Were they successful in announcing that they're back in the superpower game,
that they're one of the masters of the universe?
No, this has been utterly humiliating.
They're pushing it out there like they can go toe to toe with NATO.
They can't go toe-to-toe with NATO's surplus.
So with all of this, why don't they just quit?
Russia has to be seen as powerful.
The only thing that they have left that they can say face with is taking dirt.
territory and keeping it. Good old-fashioned imperialism. This is our
country now or it's a country that's a puppet of ours. That's all they have left.
If they lose that, if they lose the territory that they have taken, this was
a total loss in every way shape and form. They didn't win anything and not just is
Is it humiliating on the international stage?
It's humiliating at home because there's no way to spin it.
The Russian populace is very comfortable being lied to.
If Putin walked out and said, yeah, this little section of land here that we have, that's
really all we wanted, I mean, nobody would believe it, but they also wouldn't question
it and it would allow that national identity to remain.
If they lose it, then it's just a total loss.
The problem that Russia is facing is that they're not in a position to be able to frame
it as a win yet.
The territory that was taken initially, Ukraine has taken a majority of it back already.
They continue to advance.
Right now, it's, I don't want to say static, it's slow moving, but Russia isn't winning.
If Russia was to be able to mount a successful offensive and take a large chunk of dirt,
odds are at that point you might start to see Russia finally be like, okay, this is
really all we wanted, let's talk peace, but we're going to keep this.
So the reason to continue fighting is a face-saving measure.
So they can still say, look, we're back in the national game.
Yeah, this didn't go on the international game.
This didn't go the way we planned.
But we didn't lose completely.
You had to give us something.
Don't forget that.
And it plays well at home.
We took this territory.
They have the problem of being on their back foot right now, and they have the problem
of a very dedicated opposition who, as near as I can tell, the overwhelming sentiment
is that they're not going to get an inch.
They want to take it all back and they're willing to fight for it.
As long as Ukraine gets support from the West, real support, they can stay in the fight.
They can stay in the fight and eventually they'll win.
The drag that is occurring in Russia on the economy, on national morale, all of that stuff,
It's there.
It's real.
But it might take time for Russia to totally admit where it's at.
It took a long time in Afghanistan for them.
So that's the reason they haven't just thrown their hands up.
important to them and I am certain that their long-term strategic planners are
also looking at Ukraine as food, as grain. It's needed. Ukraine is a country
that, like I said, if it comes out of this on the other side of this and it does it
and it defeats Russia without needing actual troops from the West, it is going to be a
powerhouse in Europe.
So that's the reason.
It's really that simple.
The war, the wider geopolitical war was lost by Russia very, very early on.
At this point, they are trying to retain some territory so they can pretend that they won.
So they have something to show after squandering a generation, that's what it's about.
And it's very important because of the collective consciousness of the country.
They can't accept that.
And Putin wants his legacy, and it's all reinforcing.
It'll probably take some time before Russia is ready to come to terms with how badly this
has gone.
When you're getting people who are foreign policy people and they're painting a cheery
picture of the conflict.
It's because, realistically, it never should have lasted this long.
It never should have lasted this long.
Ukraine has done the impossible over and over and over again.
Every time they do the impossible, it makes Russia look weak, which in some ways may actually
still their resolve to try to retain territory.
It's the same thing that most long wars are caused by, the egos of old men.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}