---
title: Let's talk about Russia's economy and information consumption....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SeJ0W1mWex0) |
| Published | 2023/11/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a scenario where a meme claims the ruble is outperforming the dollar, leading to confusion.
- Advises on how to be better consumers of information by looking at additional context.
- Illustrates how cherry-picking data can create a misleading narrative about the Russian economy.
- Mentions a deliberate effort by Putin to boost the ruble by ordering companies to sell foreign cash.
- Suggests that the reported surge in the ruble might be an intentional attempt to downplay the impact of economic sanctions.
- Points out that countries under sanctions won't admit they're not working, even if they are, to avoid strengthening their opposition.
- Encourages looking at a broader picture and considering events surrounding a situation for better context.

### Quotes

- "So if you look at this whole chunk of information, you see something that might very well be intentional."
- "You don't know it. It could just be happenstance that it got reported the way that it did, but it could be intentional."
- "It's a good way to kind of train yourself to look for more context, broader picture."
- "If it is true, why did it happen? When did it start? Look for events at that time."
- "Y'all have a good day."

### Oneliner

Beau explains the danger of cherry-picking data and deliberate efforts to spin narratives about the Russian economy, urging for a broader context in information consumption.

### Audience

Consumers of news

### On-the-ground actions from transcript

- Investigate and verify information before drawing conclusions (implied).
- Look for additional context and broader picture when consuming information (implied).

### Whats missing in summary

The detailed breakdown of the specific events surrounding the surge in the ruble and the implications of economic sanctions.

### Tags

#Currency #Russia #InformationConsumption #EconomicSanctions #Putin


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about currency and Russia.
And in the process, we're going to see something
that gets used a lot.
And we're going to figure out how to be better consumers
of information.
Because I got this message, and a similar thing
came in two or three times.
And it's basically, hey, we keep hearing that the Russian economy isn't doing well,
but I just got this meme that says, the ruble is outperforming the dollar.
What?
And that seemed a little odd, but I looked into it.
And you can find that in a number of places.
And it's not actually an inaccurate statement if you add the additional context.
Basically over the last month, the ruble has been doing better.
It has been doing better.
Now the subtext of pretty much everything that I saw was the Russian economy is fine.
It's even outperforming the US.
The sanctions aren't working, so we should totally
stop imposing them, because they're not having any effect.
That's the general tone of these articles.
So to start with, this is what you can do.
Go to Google and type in ruble $2.
Click Enter.
And you'll get a chart.
And it'll come up, normally it comes up right at 30 days,
showing you 30 days.
But it has options for other time periods.
Go ahead and click, I don't know, six months, a year,
five years, doesn't matter.
They're all red.
So what's happened is there was this period
where the ruble did better than it had been,
because it has taken such a dive,
it got a little bit better.
So the charts turned green.
So if you were somebody who, hypothetically speaking,
had a vested interest in showing
that the sanctions weren't working
and that the Russian economy was fine,
you would wanna highlight that.
So you just use that period, cherry picking the data.
Especially if you don't include the additional context of even after this miraculous gain
where the Russian economy is racing ahead, it's still down a massive amount.
It's just over this little period.
So then you have to figure out what happened in that little period.
It's been going on for about a month.
Here's a headline from October 12th.
Russian ruble surges after Putin ordered 43 companies to prop up the slumping currency
by selling some of their foreign cash.
They're cooking the books.
Now given the fact that this was a deliberate thing and something that was so open it got
coverage and then there was a push right at the 30-day mark to put it out there
that the ruble is doing so much better and it's doing great. There might be a
little bit of an organized effort here as an attempt to show that the economic
sanctions on Russia aren't working.
Here's the thing.
If sanctions aren't working, the country that is having them
imposed on them, they're not going
to say they're not working, just so you know,
because they wouldn't want their opposition
to make them stronger.
It's one of those things where countries will only
indicate sanctions aren't working when they are.
So if you look at this whole chunk of information, you see something that might very well be
intentional.
You don't know it.
It could just be happenstance that it got reported the way that it did, but it could
be intentional.
definitely have an overt attempt to make the Russian economy appear stronger
than it is. You have a direct order from the head of state to companies to engage
in something that would prop up the currency. And the way the information
came out it was cherry-picked. It's not showing the actual impacts of the
sanctions. When if you look at a year or five years what you'll see is you'll see
this just just horrific drop right when the war starts and then you see the
initial round of Putin trying to stabilize the economy and you see the
currency shoot back up and then you see an incredibly steady decline from that
point forward. The massive surge in the ruble when you're looking at the
one-year or five-year picture you can't even see it when you're looking at the
It's that small. So I wouldn't worry too much about this, but it is a good way to kind of train yourself to look for
more context, broader picture.  If it is true, why did it happen? When did it start? Look for events at that time.
time. They'll find them. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}