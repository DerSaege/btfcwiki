---
title: Let's talk about a summit, Iran, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=H1OdNUcLTw8) |
| Published | 2023/11/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Organization for Islamic Cooperation (OIC) summit involved Iran suggesting arming Palestinian forces and cutting ties with Israel.
- The OIC rejected Iran's proposals, condemning the situation and calling for more humanitarian efforts.
- Saudi Arabia expressed anger towards the U.S. for not restraining Israel, despite Biden's efforts.
- Beau believes the conflict is less likely to spiral out of control based on recent events.
- Beau explains foreign policy dynamics and the power struggles behind conflicts worldwide.
- Beau plans to create more videos on foreign policy dynamics to help viewers understand current events better.
- Gulf states seem to avoid escalation, while Iran may influence its proxies to take a more active role.
- The Biden administration appears frustrated with Israel's actions and lack of cooperation.

### Quotes

- "It's all about power."
- "History doesn't repeat, but it rhymes."
- "The world has changed."

### Oneliner

Beau explains recent OIC summit dynamics, foreign policy power struggles, and the Biden administration's frustrations with Israel.

### Audience

Foreign policy enthusiasts

### On-the-ground actions from transcript

- Subscribe to Beau's second channel for more educational videos on foreign policy dynamics (suggested)
- Stay informed about current events and foreign policy developments (implied)

### Whats missing in summary

Beau's insightful analysis and explanations on foreign policy dynamics.

### Tags

#ForeignPolicy #MiddleEastConflict #PowerStruggles #Diplomacy #Iran #Israel #SaudiArabia #BidenAdministration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about three things, really.
One is news.
It's foreign policy related and deals with friends and all
of that stuff.
The second thing is how some people could have known this
news weeks ago.
And then the third thing is me asking you all to do something
I never ask y'all to do.
OK, so let's get to the news.
There was a summit, a conference.
The OIC, which is the Organization for Islamic
Cooperation, they met.
And if you're not familiar with it, it's basically
representatives from the Islamic world.
And while they were there, Iran, they made their
pitch. They made their pitch. They were like, hey, we need to help. We need to
help. They suggested arming Palestinian forces and suggested cutting ties,
diplomatic ties, with Israel. There were some other suggestions that were made
but those were the two big ones. That would be a pretty big escalation. The OIC
basically told him to kick rocks. I'm like, no, we're not doing that. The Saudi
Arabian government, they expressed a lot of anger at the U.S. because they feel
the United States has not been able to get Israel to restrain itself. And at the
same time, they did point out that Biden's trying, but Netanyahu is ignoring Biden.
Just as an aside, my understanding is that the Biden administration is, let's
just say, getting frustrated with that dynamic.
So at the end, what happened?
They put out a statement.
They condemned what was going on, said there was a need for more humanitarian efforts,
and they wanted the UN to get involved.
Basically this statement.
Okay, so that's the news.
Now from a foreign policy perspective, two things about this news.
One, this is pretty good news.
again another thing that lessens the risk of the conflict expanding into
something that could spiral out of control. Again I know people are looking
at what's happening and they're like this is out of control. That could be happening
in a dozen cities. So from that side, it's good news. The other thing to note
is that from a foreign policy perspective, kind of predictable. Kind of
predictable. That brings us to our second thing. On October 26th over on the other
channel, The Roads with Bo. I put out that video. It's called The Roads to Foreign
Policy Dynamics. Me standing in front of a whiteboard is the thumbnail. That video
explains the dynamics. It's not specific to any particular conflict, but the
dynamics are similar in a whole bunch of conflicts all over the world. And I got
push back on a couple of things about it. People didn't like what was said. One
of the things that was said was that Red's allies, and if you were to
transfer it to the current Middle Eastern conflict, Red would be the
Palestinian forces. Red's allies don't actually want them to win. They want them
to fight, not win. Sometimes they offer support, but sometimes that's just words.
Got a lot of pushback, a lot of angry messages about that. That's what
just happened. The dynamic is the same all around the world. When it comes to
the Gulf states. The world has changed. It's not 1967 anymore. Foreign policy is
not about the statements and the things that people think it's about. It's about
power and power coupons money. The world has changed. It's even less likely that
they get involved directly now than it was back then. The one country that is
steadfast in its support, there's two unique things about it. One is that it's
It's a country that, when it comes to foreign policy, it's kind of frozen in the past because
it's been isolated, which is why it's important to bring Iran out.
And the other thing is, it's further away.
If it was to escalate, it's probably not their cities, maybe.
When it comes to foreign policy, there are no good guys.
It's all about power.
And the people who think they are the focus, generally speaking, the pond doesn't know
it's a pawn. Okay, so the thing that I never ask y'all to do, I'm going to start
putting more videos that are like that whiteboard one up on the second
channel related to foreign policy, explaining the dynamics. Not about
anything specific, but about how it works. So you can know today's news on October 26th.
Because history doesn't repeat, but it rhymes. And when you get to foreign policy, every country
on the planet, they all have their own individual national anthems, but they're all singing the same
tuned. It's about power. So there's going to be more videos put up, just going
over the dynamics, and we'll probably use more fictitious regions. If you are
interested in foreign policy, go subscribe. Something I don't really ever
ask y'all to do, but if that is something that you're interested in, there's going
to be a bunch of them that go up, that showcase how it happens. Now, in that
video there was one other thing that people were unhappy about it being
verbalized but it's true and just like this it'll be shown to be true because
it's always true. It's actually integral to the strategy that's being deployed. It
has to be true and eventually there will be evidence of it. So the
The important news here is that the Gulf states, they don't seem to want an escalation, and
now they have kind of pushed off Iran, who was asking for one.
That's good news.
The other side to that is that Iran, while it doesn't have control of a lot of the organizations
that they get called proxies in Western media. They don't have control over them, but they
have a whole lot of influence, a whole lot. And if they are upset by the way they were
kind of ignored, they may influence them to take a more active role. So let's all hope
that the that the Saudis were diplomatic that's not anyway um so
that's a rough update on the foreign policy aspects of what's going on over there
um the other thing to note is that it does appear the Biden administration is is getting up
I don't know if upsets the right word frustrated
with Israel's actions and their unwillingness to take advice. How that
translates into US foreign policy moves, that's anybody's guess. But we'll have to
wait and see.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}