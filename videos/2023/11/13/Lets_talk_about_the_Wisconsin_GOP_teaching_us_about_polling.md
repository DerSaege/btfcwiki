---
title: Let's talk about the Wisconsin GOP teaching us about polling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wfbVxdw4kdc) |
| Published | 2023/11/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wisconsin Republican Party targeting Justice Protasewicz for impeachment due to dissatisfaction with the state's voters.
- Speaker of the Assembly, Voss, reached out to judges to gauge opinion on impeaching Protasewicz.
- Voss suggested contacting a conservative group to conduct a poll on whether Protasewicz should recuse herself to influence the narrative.
- The Institute for Reforming Government was considered for the poll, intended to sway public opinion rather than provide accurate information.
- Some polls are used not for information but to manipulate and sway public perception.
- Polls are being used as a tool to alter reality rather than represent it accurately, becoming more about wish casting.
- Consumers of information should be cautious and critical of polls, understanding their potential for manipulation.
- The focus should not be solely on the results of polls but on the intentions behind conducting them and how they are used.
- Polling can be a strategic tool in shaping narratives and perceptions, rather than reflecting reality.
- Beau calls for greater skepticism and critical thinking when consuming poll results and media coverage influenced by polls.

### Quotes

- "Some polls are used not for information but to manipulate and sway public perception."
- "Polls are being used as a tool to alter reality rather than represent it accurately."
- "Consumers of information should be cautious and critical of polls, understanding their potential for manipulation."

### Oneliner

Wisconsin Republicans target Justice Protasewicz, using polls not for information but to manipulate public opinion, urging caution in consuming and interpreting polling data.

### Audience

Information Consumers

### On-the-ground actions from transcript

- Analyze polling data critically to discern potential manipulation (implied).
- Stay vigilant and cautious when interpreting poll results to avoid being swayed by potentially biased information (implied).

### Whats missing in summary

The full transcript provides a detailed insight into the manipulation of polls for political agendas, urging viewers to be discerning and critical consumers of information.

### Tags

#Wisconsin #Polls #PoliticalManipulation #InformationConsumption #MediaLiteracy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Wisconsin.
And once again, you'll get to hear me say,
justice, protest, say wits.
But it's not actually about that story.
It's about something that came out
and it gives us a glimpse of something
and why you might want to be even more cautious
when consuming polls.
This is something that may not get a lot of coverage because it's just a little side note to a much larger story,
being the Republican Party looking at Justice Protasewicz and saying,
well, we're going to impeach her because we don't like the way people in Wisconsin voted,
is kind of what it boils down to, trying to find a way to make sure that she doesn't get to rule
on gerrymandering. That's the big story. You know, once again, Republicans
wanting to rule rather than represent, and that's what most of the coverage is
going to be on. But during this process, the Speaker of the Assembly, I think is
what it's called there, Voss, well, the person reached out, the GOP head reached
out to other judges and try to get their opinion on the idea of impeaching
proto-sawits. There is a suit over this trying to get records and this is
basically a conversation between Voss and his chief of staff and Voss asks if
it would be a good idea to reach out to the Institute for Reforming Government,
which is a it's a conservative group up there, to see if they could do a poll
on whether or not Protasewicz should recuse herself to quote affect the
discussion. Ask them to pay for it and make it public if it helps us. Another
part of this that's interesting, I know they're idiots but let's use their and
then a dollar sign. Now of course the Institute for Reforming Government, they
did not conduct a poll and they issued a statement to that effect and they also
went on to talk about how smart their people were. But the idea that you have
somebody at this level saying, hey see if you can reach out to this group, get them
to do a poll, and if it helps us then make it public to affect the discussion.
It goes to show that in at least some cases the polling isn't there to give
you accurate information. It's there to sway you. It's there to manipulate you, to
make you think that things are one way when they may not be, if it helps us.
That's wild and because of the high nature stakes of this overall story
dealing with the Supreme Court in Wisconsin, this may get completely
overlooked or maybe a footnote in coverage somewhere, but it's a
moment to sit there and look at how information is spread in the United
States and how some polls get championed and pushed out so everybody sees them
and some don't and it doesn't have anything to do with the quality of the
poll. It has to do with whether or not people think it'll help their side, even
if they don't hold a high opinion of the people that will be conducting the poll.
It's just a rare moment where we get to see a very open, plainly stated
thing that should lead us to be better consumers of information and make us
a little bit more cautious when it comes to accepting polling as representative of what's
happening rather than a tool to alter what's happening.
Because more and more the polling is becoming wish casting.
If you put out enough polls that say you're right, eventually people will start to think
Anyway, it's just a thought.
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}