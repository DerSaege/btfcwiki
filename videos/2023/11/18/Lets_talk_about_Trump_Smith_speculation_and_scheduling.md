---
title: Let's talk about Trump, Smith, speculation, and scheduling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Z3FujzNdnY0) |
| Published | 2023/11/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The DA's office in Georgia proposed August 5th for the beginning of the Trump case due to scheduling conflicts.
- The federal DC case is currently scheduled for March and the documents case for May, leading to speculation.
- People are concerned that the documents case might be delayed, affecting the Georgia trial start date.
- Speculation suggests that Smith might try to get the judge in the documents case removed.
- Reports on Smith considering this action are based on expert opinions but lack factual sourcing.
- The Georgia DA might not be able to move up the trial date even if a spot becomes available earlier.
- Trump might oppose any attempt to move up the trial date, potentially leading to delays.

### Quotes

- "The documents case is in May."
- "The Georgia case should take May and start then."
- "He has to be considering his options."
- "Realistically, the documents case very well might get moved."
- "Anyway, it's just a thought."

### Oneliner

The Georgia DA proposes August for the Trump case due to scheduling conflicts, with speculation on potential delays and actions by Smith lacking factual backing.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Monitor updates on the timeline of legal proceedings (implied)
- Stay informed on developments in the Trump case and related legal matters (implied)

### Whats missing in summary

Context on the potential implications of delays in the legal proceedings and the importance of factual reporting in speculation.

### Tags

#Georgia #LegalProceedings #SchedulingConflicts #Speculation #TrumpCase


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about scheduling,
and Trump, and Georgia, and DC,
and the documents one,
and a lot of speculation that is occurring
that is being presented in a way
that leads people to believe that it isn't speculation.
And we're just gonna kind of run through the timeline
that is current, the issues that some people have with it,
and where it goes, okay?
So if you missed the news, the DIA's office in Georgia
has proposed August 5th for the beginning
of the Trump case in Georgia.
Now on the current timeline,
The reason that date is so far out, which is what everybody's asking, is the federal
DC case, the election case, is in March, currently scheduled for March.
The documents case is in May.
So what it appears is that the DA in Georgia was like, okay, so we'll do August.
We'll take the next available slot.
People are unhappy with this because a lot of people believe the documents case is going
to be delayed.
There have been some rulings that make it look like that's going to be pushed back.
So the feeling is that the Georgia case should take May and start then.
And I get it, I understand it, and yeah, if you were asking me, I would say it certainly
looks like the documents case that looks like it's going to be delayed. But the
Georgia DA, they have to go with what's on the table, what's on the calendar
already. So that's why that date was chosen. That's really that simple. The
people who are saying it looks like the documents case is taking up a spot and
and that that time period won't actually be used for the trial.
That's not like unwarranted. That's not horrible logic there. It does look like that might happen.
At the same time, something else that's occurring is a whole lot of people saying that Smith
is getting ready to basically try to get the judge in the documents case removed.
And this is kind of being reported almost as fact. To my knowledge there is no reporting,
no sources that actually say this is happening. What's happened is a whole lot of commentators,
particularly those who really understand the Department of Justice, have been
saying stuff like, he has to be thinking this. He has to be, you know, questioning
whether or not he should try to get her removed. And they've said it so much that
now it has turned into Smith is considering that. And he very well might
be. It's a lot like the people who are looking at the May date for that trial
as just taking up space. It's speculation but it's not unfounded. Smith very well
may be thinking about it but to my knowledge and I've looked there's no
actual reporting that says somebody in his office said that was happening. It's
It's a whole bunch of experts who have informed opinions who are saying he's got to be questioning
it at this point.
He has to be considering his options, stuff like that.
And now it's taken on a life of its own and it's turned into he's definitely questioning
this and some people are even jumping to he is going to.
To my knowledge, there is no sourcing that says that's happening.
It's speculation, it's well-founded speculation, but the reporting isn't there to make that
statement and treat it as factual.
It's an opinion and if you were to ask me, yeah, I think he probably is questioning it,
But there's no evidence to say that, not that I'm aware of.
So that's what's going on.
Realistically, the documents case very well might get moved.
And people have asked, can the Georgia DA move up the case if that spot becomes free?
Probably not.
Trump would probably fight it and probably win.
So that's where it is.
So you're looking at March, then maybe May, and then August for trial dates on the calendar
as they're set, but the May one, there's a wide belief that it will move.
Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}