---
title: Let's talk about Trump and the 14th Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=N0oEUMOZlv8) |
| Published | 2023/11/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump being banned from the ballot is being attempted in multiple states using the 14th Amendment due to his involvement in an insurrection.
- The court in Colorado believes Trump engaged in insurrection but should still be on the ballot because his oath doesn't include supporting the Constitution.
- The 14th Amendment disqualifies individuals who engaged in insurrection, but the presidential oath doesn't explicitly include support.
- There may be other cases where judges rule against Trump being on the ballot, eventually leading to a Supreme Court decision.
- The Supreme Court might find an alternative reason to allow Trump on the ballot despite the automatic disqualification suggested by the 14th Amendment.
- There could be a need for a unified finding for the Supreme Court to make a decision regarding Trump's eligibility based on the 14th Amendment.
- The Supreme Court might stick with the current ruling or interpret it differently to support Trump's candidacy based on the wording of the 14th Amendment.
- Changing the presidential oath to include support might be a suggestion, but it's unlikely to happen easily through Congress.
- Beau doesn't see this legal mechanism as the one that will ultimately prevent Trump from being on the ballot.

### Quotes

- "Trump engaged in an insurrection, he can't be on the ballot because the 14th Amendment prohibits those."
- "There may be other cases where judges rule against Trump being on the ballot."
- "It's probably not going to be what happens."
- "I do not necessarily believe that was the intent of the people who drafted it."
- "I knew this argument was being made. I honestly did not expect it to go anywhere."

### Oneliner

Colorado court believes Trump incited insurrection but should remain on ballot; Supreme Court likely to decide differently.

### Audience

Legal Analysts

### On-the-ground actions from transcript

- Challenge legal decisions through proper channels (implied)

### Whats missing in summary

Context on the potential implications of these legal battles for future elections.

### Tags

#Trump #14thAmendment #LegalChallenge #SupremeCourt #PresidentialOath


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and Colorado and the 14th
amendment and oaths and everything that was decided and where it goes from here.
Because the, the ruling out of Colorado to me was kind of surprising.
Not that, not that Trump should remain on the ballot.
I've said repeatedly, I don't, I don't really think
This is a viable route for that, but the way the court got to that conclusion was unique.
So if you're not familiar with what's going on in a number of states, there are attempts
to get Trump basically banned from being on the ballot, and they're using the 14th Amendment
to the United States to do that.
And to boil it all down, they're saying because Trump engaged in an insurrection, he can't
be on the ballot because the 14th Amendment prohibits those, disqualifies those who engaged
in insurrection.
It's a pretty simple argument.
Surprisingly, it appears that the court in Colorado believes that Trump engaged in insurrection.
However, he should still be on the ballot because he doesn't have to support the Constitution,
because his oath doesn't contain that. In relevant part, the 14th Amendment says,
who, having previously taken an oath as a member of Congress, or as an officer of the
United States or as a member of any state legislature or as an executive or judicial
officer of any state to support the Constitution of the United States who has engaged in insurrection
or rebellion is disqualified. This is the presidential oath. I do solemnly swear or
affirm that I will faithfully execute the office of the President of the United States
and will, to the best of my ability, preserve, protect, and defend the Constitution of the
United States.
Doesn't include to support."
So the reading suggests that it doesn't apply to presidents.
I think that's a unique ruling.
I knew this argument was being made.
I honestly did not expect it to go anywhere.
I'm a little surprised by that.
Now, for those people who are supportive of this route, there are going to be other cases.
My guess is that at some point in one of them, the judge will say, yes, Trump did this, and
this applies to him, he can't be on the ballot, at which point, it will go to the Supreme
Court.
And at that point, my guess is the Supreme Court is going to come up with an entirely
different reason to allow Trump to be on the ballot.
And that's going to be that basically, he, even though the reading of the 14th Amendment
kind of makes it automatic, there still needs to be some kind of unified finding that that
occurred.
That's my guess as to what the Supreme Court is going to say.
Of course they may just stick with this because it is technically correct, which is the best
kind of correct and all that.
I do not necessarily believe that was the intent of the people who drafted it.
I'm sure there are constitutional scholars who studied the 14th Amendment way more than
I have that can provide a little bit more clarity on that.
But given the language in it, the Supreme Court very well may just rely on this to say
that Trump can be on the ballot.
That would be him being on the ballot is my guess as to how this story ends.
We'll have to wait and see how it plays out, because there are a whole bunch of other cases.
And I would imagine that at least a few of them are going to move forward.
And out of those, I'm sure that one is going to say, he can't be on it.
And that's when it'll head to the Supreme Court.
I would suggest that maybe it's time to change the presidential oath to make it include the
phrase to support, but good luck with getting Congress to do something about that.
So that's where it's at.
I would not wait around for this as the mechanism that is going to stop Trump.
It's probably not going to be what happens.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}