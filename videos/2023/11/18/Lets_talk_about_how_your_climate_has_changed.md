---
title: Let's talk about how your climate has changed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=M7r2DtT23DE) |
| Published | 2023/11/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Climate change has already occurred, not just a future event.
- USDA's hardiness zone map has been updated, showing an average increase of two and a half degrees across the country.
- The change in hardiness zones will affect what plants can be grown in different areas.
- The two and a half degrees increase doesn't mean it happened in the last 10 years but suggests a trend towards it.
- Use the updated hardiness zone map to show skeptics the impact of climate change.
- People who garden will have to adjust what they grow due to the changing climate.
- The shift in hardiness zones is something familiar even to conservatives and skeptics.
- In some areas, the change in hardiness zones is dramatic, allowing for the growth of tropical plants in unexpected places.
- Show last year's map of hardiness zones alongside this year's to demonstrate the difference.
- Climate change is no longer a future threat but is already impacting everyone, regardless of their beliefs or feelings.

### Quotes

- "Climate change has already occurred, not just a future event."
- "Use the updated hardiness zone map to show skeptics the impact of climate change."
- "Climate change is no longer a future threat but is already impacting everyone."

### Oneliner

Beau explains how climate change has already occurred, using the updated hardiness zone map to show skeptics its impact, stressing that it's no longer a future threat but a present reality affecting everyone.

### Audience

Gardeners, climate activists, skeptics

### On-the-ground actions from transcript

- Show skeptics the updated hardiness zone map to demonstrate the impact of climate change (suggested).

### Whats missing in summary

The full transcript provides detailed insights into how climate change has already impacted gardening practices through the updated hardiness zone map and serves as a tool to convince skeptics of the reality of climate change.

### Tags

#ClimateChange #HardinessZones #Gardening #Skepticism #Conservatives


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
how your climate has changed, past tense has occurred.
Not some future thing that's going to occur,
not something that you can't see,
not something that you can't feel,
but how it has happened already.
And now we have something that a whole lot of people
over the United States are going to see. So it's no longer just some liberal
talking point. If you garden, you are familiar with something called a
hardiness zone. USDA has a map and it's divided up into zones. It's a map of the
US and each each area is part of a zone. That zone is like a recommendation on
what types of plants and veggies and fruit you can grow in your area. They
just redid it. Odds are your hardiness zone has changed.
Overall, on average, it's about two and a half degrees warmer across the country.
This will alter what kinds of plants you can grow.
Now for those who are really familiar with climate change, understand that two and a
half degrees is not the same two and a half degrees.
numbers used to come up with hardiness zones have to do with like the coldest
night every 30 days or you know there's there's a lot of numbers into how they
average it out. So it doesn't actually mean that the climate has increased two
and a half degrees over the last 10 years or so. So that's good news but the
bad news is that it shows it certainly appears to be moving that way. This is
probably something that you can use to talk to people who are skeptical, to talk
to people who say oh well it's not real, it's not really having an effect.
No, it is. So much so that your friend who gardens, well they're gonna have to
change what they grow. On the upside it does explain why I haven't been able to
grow tomatoes but this is a this is a widely available thing that people are
familiar with. More importantly this is something that a lot of conservatives
are familiar with. A lot of people who would be skeptical this is something you
can show them. You can show them last year's map of the hardiness zones and
this year's map and you can show them the difference and there are some areas
where it is dramatic. In some places it's a shift, you know, but in some
places you can grow tropical plants now in places that you probably wouldn't
wouldn't expect to. That's something that you might want to keep in mind when
you're having these conversations if the person gardens, if the person is even
remotely involved in agriculture, you have something that you can show them and
it might make an impact because sadly while the climate does not care how we
feel about it, climate mitigation, there needs to be more people calling for it
but for that to happen more people have to believe in it. More people have to
understand that it's occurring and that it will impact them. It's no longer
future tense. It is impacting you. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}