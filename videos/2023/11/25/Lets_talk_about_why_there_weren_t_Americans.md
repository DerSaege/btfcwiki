---
title: Let's talk about why there weren't Americans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=q7VLQGG7nks) |
| Published | 2023/11/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why Americans were not in the first group of captives heading home, based on their value to the captors.
- Criticizes the talking points that focus on Trump and his negotiation tactics, pointing out the reality of foreign policy.
- Clarifies that the US does negotiate, mentioning an example involving the Deputy Prime Minister of Economic Affairs in Afghanistan.
- Dismisses the idea of using military force to retrieve captives, particularly in the context of the situation in Afghanistan.
- Warns against simplistic solutions like bombing, as it can lead to further recruitment for militant groups.
- Raises the strategic implications of involving the US militarily and how it might play into the goals of certain groups.
- Emphasizes the importance of avoiding tough-guy rhetoric and bumper sticker slogans in complex situations.
- Suggests that Americans may be among the last to be released due to their perceived value and safety.
- Advocates for a longer process to ensure a more permanent deal and to prevent civilian casualties.
- Encourages a thoughtful approach and wishes everyone a good day.

### Quotes

- "Yes we do. Don't get your understanding of how this type of stuff works from action movies."
- "The tough guy talking points, the bumper sticker slogans, they have no place here."
- "It's almost like this is not a simple situation."
- "The goal here should actually be to extend this a little as long as humanly possible."
- "It may take longer, but there's a much higher likelihood that they return standing up."

### Oneliner

Beau explains why Americans weren't in the first group of captives heading home, criticizes simplistic talking points about foreign policy, and advocates for a thoughtful approach to the situation.

### Audience

Foreign policy observers

### On-the-ground actions from transcript

- Contact organizations involved in foreign policy to better understand negotiation strategies and implications (implied)
- Organize or attend community events discussing the nuances of foreign policy decisions (implied)

### Whats missing in summary

Beau's nuanced insights on the complex dynamics of negotiation and foreign policy are best understood by watching the full transcript.

### Tags

#ForeignPolicy #Negotiation #Complexity #CommunityEngagement


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the recent developments.
And we are going to talk about some of the commentary that
has come out from those developments
and just kind of run through some of the talking points
that have been generated, particularly in regard
to why there weren't Americans.
And we're going to do this not just because the talking
points are not good, but in many cases they're actively, like actively bad. They hurt. They
don't help. If you have no idea what I'm talking about, you missed the news. The pause
is in effect. The pause is in effect and the exchanges have started. Captives are heading
home. In the first group there were no Americans and a lot of American commentators have taken
this and started running with it, okay. I can't believe there were no Americans.
That's surprising to you? Why would there be, just out of curiosity, given the
general dynamics of how this stuff normally works, why would Americans be
in the first batch out? Well, because they're, they're, they're what? Exactly.
Generally speaking, the first people released in a situation like this, they
are those that are least valuable to the people holding them. Those are the
people that get to go home first. Another way to say that would be the most likely
to end up on a video if things went the other way. Those are the people that get
to go home first, normally. I don't know why anybody would expect for there to be
a bunch of Americans in the first batch. That's not how any of this works. The Americans are
more valuable to them. So they wouldn't get rid of them first. But because of this surprising
development for whatever reason, it has led to talking points that center on
Trump but these talking points come through a lot in other in other places
and it's worth just kind of going through them and and making sure people
understand that they're bad. Well Trump would have got them all back the first
day because he doesn't negotiate with people like this. Who told you that?
That's a line from a B-action movie. It's not how like real-world foreign
policy works. The US absolutely negotiates. You know who we should ask?
The current leadership of Afghanistan. We could start by asking the
Deputy Prime Minister of Economic Affairs. Start there. If you don't know who
that is. That's a person that Trump and his team got out of prison so he could
participate in a negotiation with the US that eventually led to a deal that was
so bad that when the details became public everybody was like, hey the
national government's gonna fall. And then it did. Trump actually wasn't good
at this, just so you know. But the idea itself of we don't negotiate. That's the
part that needs to be focused on. Yes we do. Don't get your understanding of how
this type of stuff works from action movies. Yes, the US does. That's just
something that gets said. Going along with that, since we don't negotiate, it's
Well, we would have bombed them until they gave us our people back.
You can honestly look at the situation that's going on over there and think the issue is
a lack of bombing.
That doesn't work either, just so you know.
The Palestinian forces, they're not a military, they're a movement.
It doesn't work the same way.
The stuff from the air actually leads to extra people, right?
Those extra people that are no longer here, they have families.
You're not destroying them, you're recruiting for them.
And it continues the cycle.
More importantly, in this case, in particular, it's actually part of the leadership strategy
get the US involved in that kind of role because they're hoping it could create a
regional conflict and in their mind it might lead to like a coalition of Arab
tanks heading towards Tel Aviv. Now that's not going to happen we've talked
about why but that's what they believe. Why would you play into that? More
importantly if holding the Americans doesn't help what do you think they
would do with them. They don't need them anymore. The tough guy talking points, the
bumper sticker slogans, they have no place here. It's almost like this is not
a simple situation. Yes, because the United States has engaged in a little
bit of encouraging Israel to be restrained and has chosen to help mediate in some ways when
it comes to negotiations and stuff like that. Yeah, the Americans may be some of the last people
released. You might start to see some get mixed in now, now that the pause is truly in effect and
there's a little bit of security going on, but yeah, it is going to take time, but the reason
is because they're viewed as the most valuable, which also means they're the safest.
It may take longer, but there's a much higher likelihood that they return standing up.
I would point out that while people are talking about this and the speed at which Americans
come home, the goal here should actually be to extend this a little as long as humanly
possible to hopefully get some people who are not at a sand table but are at a boardroom
table to work out some kind of permanent deal, so a whole bunch of extra people, civilians,
uninvolved, don't get lost.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}