---
title: Let's talk about Manchin, Justice, and the Senate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5d39Ss_ZcmU) |
| Published | 2023/11/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Manchin is not running for reelection, impacting the balance of power in the Senate.
- Manchin, a Democrat, faced criticism from progressives for his conservative views.
- Jim Justice, a popular figure in West Virginia, is likely to run for Manchin's seat as a Republican.
- The Democratic Party accommodated Manchin due to the importance of his secure Senate seat.
- Progressives now have the chance to field a candidate in West Virginia, but a California-style progressive may not succeed.
- Beau suggests a rural, working-class progressive candidate might have a slim chance in the upcoming election.
- The intersection of political reality and ideological purity is a challenging one, especially in West Virginia.
- Senator Manchin's decision not to run complicates the Democratic Party's efforts to retain Senate control.
- The upcoming Senate race in West Virginia has become significantly more critical due to Manchin's exit and Justice's likely candidacy.
- Beau urges progressives to seize the moment and demonstrate that their preferred candidates can succeed even in challenging states.

### Quotes

- "Be careful what you wish for because it just got a whole lot harder for the Democratic Party to retain control of the Senate."
- "Political reality trumped the ideological purity that people wanted."
- "The intersection of political reality, ideological purity, it's always a bumpy one."
- "If it can be done there it can be done anywhere."
- "Y'all have a good day."

### Oneliner

Senator Manchin's decision not to run for reelection in West Virginia poses a challenge for progressives aiming to maintain Democratic control of the Senate amidst the intersection of political reality and ideological purity.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Field a rural, working-class progressive candidate in West Virginia (suggested)
- Showcase that winning with progressive values is possible in challenging states (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the implications of Senator Manchin's decision on the Senate balance and the challenges faced by progressives in West Virginia.

### Tags

#Senate #Election #Progressives #PoliticalReality #IdeologicalPurity


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Manchin and justice,
but probably not the kind you're thinking about,
and Hills in West Virginia,
and the intersection of ideological thought
and pragmatic political reality,
because we're all about to get a lesson in it.
In case you missed the news,
Senator Manchin has decided
he is not going to run for reelection.
Now, over the years, people who are progressive
have looked at Senator Manchin with,
I mean, for lack of a better word, just absolute disdain
because he's a Democrat who is,
I mean, like super conservative.
Like you can't even call him a conservative Democrat.
He's a liberal Republican who filled the form out wrong.
But even though he might be a Democrat in name only,
It mattered because that seat, it was secure.
And that seat mattered when it came to determining
who had the majority in the Senate
and setting the legislative agenda.
So why did Manchin decide not to run?
Justice.
Not some kind of karmic retribution
for not being a good progressive.
Jim Justice.
Jim Justice has decided to run for that seat.
That's what it looks like.
And Jim Justice is a very beloved figure
in West Virginia for whatever reason.
And odds are the incumbent
who has a whole bunch of goodwill built up
among his constituents cannot beat him.
So the Republican party is likely to pick up that seat.
That means if Biden retains the presidency,
I think they need one more seat
to flip the majority in the Senate.
That's why the Democratic Party was so accommodating to somebody who, basically
on pretty much every policy issue, wasn't a Democrat. The political reality trumped
the ideological purity that people wanted. If you are one of those
progressives who talked about wanting to get rid of Manchin, throw him out of the
Democratic Party or whatever, now is your moment to shine because you have to run
a candidate in West Virginia against somebody who the incumbent didn't even
think they could beat and if you want them to be progressive you got to start
working on that right now because they can't be the kind of progressive that
people wanted. You know they wanted the the California progressive to win in
West Virginia. That won't happen. It's just not in the cards. What could
win would be a progressive who is a union progressive, the kind of progressive that
talks about how bad the coal bosses are, that kind of progressive, the working class progressive.
Now they may hold all of those other progressive ideas but they had better keep them to themselves
during this election because if it comes out they won't win. Political reality versus ideological
thought. So that's what's going on and there are a whole lot of people who have
that opportunity right now to kind of showcase it. You know, the intersection
there, political reality, ideological purity, it's always a bumpy one. The one
in West Virginia it is now right dangerous. There's a lot of accidents
there and we're about to see a huge one. This made every Senate race across the
country that much more important because realistically this is pretty much a
guaranteed Republican win. It is incredibly unlikely that anybody is going
to be able to beat Justice. Now I believe the only type of candidate that has a
chance is a rural working-class progressive that really talks about
those values. Maybe has a chance, but even that's slim.
Nobody else is... anybody else that plans on running, I would save your filing fee.
It's going to be hard and if you are one of those people who was dead set on
on ideological purity and talked about how it was possible
to get that kind of candidate in West Virginia, now is your moment to shine.
Show the country that it can be done because if it can be done there it can be
done anywhere.
So that's where we're at, that's the news
and Senator Manchin deciding that he is not going to run
is one of those cases where, well, be careful what you wish for
because it just got a whole lot harder for the Democratic Party to retain control
of the Senate. Anyway, it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}