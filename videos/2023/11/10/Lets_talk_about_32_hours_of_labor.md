---
title: Let's talk about 32 hours of labor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eC-TVmgY4KA) |
| Published | 2023/11/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Valley Labor Report is starting a live stream supporting union activity in Tennessee and Florida, lasting for 32 hours to draw attention to the idea of a 32-hour work week.
- Before the 40-hour work week, the average work week was 61 hours, showing that change is possible.
- Moving to a 32-hour work week aims to create more leisure time, increase productivity, and improve work-life balance.
- Companies with brand loyalty use a pricing method called "whatever the market will bear" to charge the maximum customers will pay.
- Corporations are profit-driven, and their main goal is to provide value to shareholders, not create social change.
- Organizing is key to making dreams, like a shorter work week, a reality.
- People once thought a 40-hour work week was impossible, but now it's the norm.
- Many dreams become reality when people work towards them.
- The Valley Labor Report's live stream will feature guest appearances from other YouTube channels over the next 32 hours.
- Beau encourages viewers to join the live stream and have a good day.

### Quotes

- "Before it happens, everything was just something in somebody's imagination. It was their dream."
- "Most companies charge the absolute maximum that people will pay before they start losing customers."
- "A whole lot of dreams become reality if people work for it."

### Oneliner

Valley Labor Report's 32-hour live stream supports a shorter work week, challenging norms and advocating for change in labor practices and corporate values.

### Audience

Labor advocates, workers

### On-the-ground actions from transcript

- Join the Valley Labor Report's live stream to support union activity and the idea of a 32-hour work week (suggested).
- Participate in organized efforts to advocate for better work-life balance and increased leisure time (implied).

### Whats missing in summary

The full transcript contains insights on labor practices, corporate behavior, and the power of collective action.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about 32 hours
in a couple of different ways.
And we are going to talk about imagination
and how things are always imagined before they're done.
They're always just somebody's dream.
So here in about 30 minutes, if you are watching this
as soon as it publishes, the Valley Labor Report on YouTube
is going to begin a live stream.
The purpose of it is they are supporting some union activity.
And it's in Tennessee and in Florida.
I won't go into all the details.
They will definitely cover that.
The live stream will start in about 30 minutes.
And it will run for the next 32 hours.
32 hours.
And the reason they're going to do it for 32 hours
is to draw attention to the idea of a 32-hour work week.
Right now in the US, it's 40 hours.
And when you bring up the idea of a 32-hour work week,
you always get people saying, you can't do that.
You can't do that.
Do you know what the average work week
was before the 40-hour work week was instituted?
61.
That was the norm.
That was the average, 61 hours a week.
And then it went down to 40.
But the United States, its economic engine
just kept chugging along.
And now there are people and advocates
trying to move it to 32 to create more leisure time,
to make the work hours more productive,
to provide a better work-life balance.
I know me saying that is funny.
But for people who don't absolutely love what they do,
it's probably a good idea
because it provides them the ability
to do the things that they love.
One of the things that often comes up
when we talk about unions,
particularly those unions that are not seen as union jobs,
like Starbucks, you know?
Well, if they get better wages,
the companies are just going to raise the prices I mean first if you actually
kind of follow that along you're like hey yeah keep that person in poverty so I
don't have to pay an extra nickel for my coffee but aside from that there's
there's something else companies that enjoy a lot of brand loyalty companies
like giant coffee companies or fast food companies.
Do you think they leave money on the table when they set their price?
Do you think they sit there in the boardroom in their $2,000 suits and say, you know what,
I bet our customers would pay more for this product, but let's not charge that.
Let's charge it a little bit less just because we're good people.
not just a profit-driven machine.
Companies that enjoy a lot of brand loyalty, they tend to use a pricing method that's called
whatever the market will bear.
You can Google it.
And that means they charge the absolute maximum that people will pay before they start losing
customers.
it boils down to. Most companies that you know the name of, that you've heard of,
that's what they do. Yeah, they may use labor activity, they may use increased
wages as an excuse to raise prices, but that's only because they can use that.
and they think because of that, that rationale, that reason that they're
providing, they think people will be willing to pay more. So what the market
will bear increases. When you have companies that consistently try to beat
LY last year, they consistently try to earn more than they did the previous
year, and those profits are more than the previous year, but the wages stay the
same. It's a clear indication that those two things are not tied. They don't go
together the way a lot of people instinctually believe they should, and
you believe that because you're probably a pretty fair person. Corporations,
despite legal opinions, aren't people. They are cold, unfeeling monsters and
their entire job is to turn a profit. A corporation's job is to provide a profit,
provide value to its shareholders. That's it. It's not there to be an engine for
social change. It's not there to be a job creator. It's there to make money. So,
So, when you hear that argument, or when you hear the argument that says 32 hours, you
just can't do that.
Yeah, you can.
The same way people in the 1870s were like, you can't have a 40-hour work week.
And now it's the norm.
Before it happens, everything was just something in somebody's imagination.
It was their dream.
whole lot of dreams become reality if people work for it. The easiest way to
work for it is to organize. So the the Valley Labor Report, they'll be there for
the next 32 hours. My understanding is that a lot of your favorite people will
be joining in. The other YouTube channels, a lot of YouTube channels that y'all
watch. I know because YouTube provides a scary amount of information about y'all.
Some of them may be popping into this live stream
over the next 32 hours. Anyway,
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}