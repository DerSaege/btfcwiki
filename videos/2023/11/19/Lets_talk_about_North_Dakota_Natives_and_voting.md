---
title: Let's talk about North Dakota, Natives, and voting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=B7oQ8O-8EkI) |
| Published | 2023/11/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A federal judge in North Dakota found the 2021 redistricting map violated the Voting Rights Act by packing one tribe and cracking another to dilute voting power.
- The judge gave North Dakota until December 22nd to remedy the situation, requiring a special session due to the state's unique legislative schedule.
- Republicans are considering whether to appeal the judge's decision or call a special session.
- There is a pattern of Republicans gerrymandering to protect their power, leading to legal battles and judges striking down maps.
- Despite potential appeals, it seems likely that the natives will come out on top and a new map will be ordered.
- The violation in North Dakota may not be as severe as in some southern states, but it appears intentional.
- The situation is being closely monitored to see how it unfolds, with hopes for a quick resolution.

### Quotes

- "A federal judge in North Dakota found the 2021 redistricting map violated the Voting Rights Act."
- "Republicans are considering whether to appeal the judge's decision or call a special session."
- "It appears likely that the natives will come out on top and a new map will be ordered."

### Oneliner

A federal judge in North Dakota found the 2021 redistricting map violated the Voting Rights Act, leading to a potential shift in favor of natives despite Republican gerrymandering patterns.

### Audience

Advocates, Voters, Activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for fair redistricting (suggested).
- Stay informed about updates on the situation and share with others (implied).

### Whats missing in summary

The full transcript provides more details on the legal battles surrounding gerrymandering and the impact on voting rights in North Dakota.


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today we are going to talk about North Dakota
and natives and voting,
and a story that is going to sound incredibly familiar
to a whole lot of people.
But hey, at least this time it's not in the South.
Okay, so a federal judge has decided
that the 2021 redistricting map in North Dakota violated the Voting Rights Act of 1965.
The basic allegation in the suit, and it definitely appears that the judge agreed with
this on some level was that the legislature basically packed one tribe and cracked another
to dilute the voting power.
So the judge gave them until December 22nd to remedy this situation.
Now, North Dakota has a unique legislative schedule, so they would have to call in a
special session to do that.
The Republicans have indicated that they will be meeting with various people here shortly
to determine whether or not they want to try to appeal or get the special session underway.
There is certainly a pattern that is occurring right now with the maps that were done off
of the last census and Republicans gerrymandering their states to try to protect their power.
And the judges continue to strike the maps down and then there's continued legal battles
over it.
But at this point, it appears that the natives have won and they'll get a new map.
Looking at this, even if there is an appeal, I feel like the natives are still going to
come out on top and a new map is going to be ordered.
quite as bad a violation as we have seen in a lot of southern states
recently, but it appears to be intentional. So we'll continue to follow
this just like we follow all of the other ones and we'll find out what
happens, but hopefully this will get remedied pretty quickly. Anyway, it's just
a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}