---
title: Let's talk about deal or no deal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=59MXb2y2SG4) |
| Published | 2023/11/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculation surrounding a potential deal to pause fighting in the Middle East is fueled by conflicting reports from news sources and governments.
- Three possible scenarios are considered: bad reporting, an agreement in principle with pending details, or a secret deal not yet confirmed by governments.
- The significance of U.S. aid to Israel is explained through domestic politics, where support for Israel has been historically popular among politicians seeking votes.
- Recent shifts in questioning the aid to Israel and conditioning it based on certain criteria indicate a change in approach by the Biden administration.
- Actions taken behind the scenes by the Biden administration, such as imposing conditions on aid related to arms purchases, suggest a strategic plan to influence the peace process in the region.
- Despite ongoing diplomatic efforts and movements, there is a lack of concrete accomplishments or official announcements regarding the situation in the Middle East.

### Quotes

- "It's really more about domestic politics."
- "If you put the pieces together, it kind of looks like the administration's sitting there."
- "That's what the foreign policy situation is like right now."

### Oneliner

Speculation on a potential Middle East deal, U.S. aid to Israel, and Biden administration's strategic moves suggest diplomatic shifts amidst ongoing conflicts.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Monitor updates on the situation in the Middle East for accurate information (implied)
- Advocate for transparent foreign policy decisions and peace initiatives (implied)

### Whats missing in summary

Detailed insights on the potential impact of diplomatic strategies and aid conditions on achieving lasting peace in the region.

### Tags

#MiddleEast #USaid #BidenAdministration #Diplomacy #PeaceProcess


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today, we are going to talk about deal or no deal.
Is it happening or isn't it?
Because there is some mixed reporting.
And we're going to kind of run through it and talk
about the various options.
And since we're on this topic, we'll
also answer two other questions related to it
that have come in repeatedly from different people.
So if you have no idea what I'm talking about, last night some news broke and it said that
a deal had been reached and the deal was basically, there was going to be a five day cease, I'm
sorry, not a cease, anything, a pause.
There would be a five day pause in the fighting and there would be 50 something captives returned
during that period.
That was the reporting.
Shortly after that, both the Israeli and U.S. governments were like, no, that's not real.
So people are asking what it means.
Well, you have three options.
One, it's bad reporting.
It happens.
It could just be bad reporting.
Another option is that there's an agreement in principle, but the details haven't been
finalized yet, so the governments don't want to commit to it. That's another one.
The third option is that there's totally a deal in place, but the
governments aren't ready to say there is, and there could be a list of reasons
for that. The governments could be lying. They do that sometimes. As far as which
of those three it is, don't know, and we won't know until we know, until there's
more information about it. So right now, the only thing that's been confirmed that we know
for certain is that there is pursuit of a deal that appears to be incredibly similar
to this. That's what we know. And the deal is reportedly being brokered by the U.S.
Okay, so moving on to some questions. Most common one that has come in over the last
few hours has been, okay, so if Israel doesn't need the aid and they're not dependent on
that aid from the U.S. Why is the U.S. giving it to them? Makes sense. Why does
a politician stand out in front of the groundbreaking of some infrastructure
project that they voted for? Or sometimes they even voted against, but
they go out there and take credit for it anyway, right? Stand there with a
hard hat and a shovel. Why do they do that? Because they're spending money in a
way that people like and it gets them votes. It's kind of that simple. If you're
younger please understand the default setting for US politicians for decades
was the U.S. supports Israel. An incredibly popular position and providing
aid, well, it played well. It wasn't a politically risky thing. So that's why, I
mean, the aid for Israel is allocated out through a memorandum of understanding
out until I want to say 2028. Our own military isn't funded out until 2028, it's about catching
the votes. A lot of evangelical Christians, there's a whole bunch of demographics that
are just steadfast in their support. That's what it was about. The shift that you're seeing
now, understand it's new.
That is new.
Politicians even remotely questioning it, it's new.
That wasn't common.
So it's really more about domestic politics.
And when you look at the foreign policy side, somebody in the comments said that it was
symbolic.
Yeah, I mean there's some truth to that.
It shows that the U.S. is with Israel and Israel is with the U.S. and all of that stuff
and that's important in that geopolitical card game that's going on over there.
At the same time, I mean that's kind of demonstrated by the joint research projects and military
projects that go on, that's something that can be done with a treaty or just words.
money. It's it's about domestic politics more than anything. So the other question
is people are saying that Biden's administration is doing things behind
the scenes. An example it was the request and I think a good way to do
this and to show you how behind the scenes it kind of is, is right now you
You have a whole bunch of political commentators and even some politicians saying, fine, we'll
give Israel aid, but we need to condition it.
We need to put conditions on the aid that we're going to give Israel and say you can't
do X with it.
As an example, they would say, fine, you want to buy these rifles?
We'll sell you these M16s, but you can't give them to settlers.
like that, putting conditions on the aid. Biden did that two weeks ago. Didn't get
a lot of press, didn't get a lot of fanfare. And probably that had to do with a whole lot
of people who cover foreign policy looking at it and being like, what is this? And they
didn't really know what to make of it. But the Israeli government wanted to purchase
a bunch of M16s and the Biden administration was like, yeah, okay, but you can't do this
with them.
It's stuff like that and given the fact that that and his recent statement about the visa
bans both deal with the same topic, it shows an intent.
It's not haphazard.
There's a plan.
If you put the pieces together, it kind of looks like the administration's sitting there
well, we can't stop this. Let's get the peace process restarted afterward. A real
one. That's what it looks like. How successful that will be? I don't know.
I mean, that's when people talk about an impossible foreign policy challenge, it's
It's this.
This is what is presented as like the unsolvable situation.
But it looks like the Biden administration
is trying to shape the discussion for later,
to be able to say, OK, stuff on this side of this line,
that's yours.
What's over there is theirs.
And y'all stop fighting.
That's what it looks like.
But again, no clue how successful they'd be at that.
But that appears to be what all of the moves are kind of angling towards.
I'll put a link down to the thing about the rifles because I don't know if that's even
easy to find.
But that's what's going on.
So right now there's a whole lot of movement but no accomplishment.
One of those situations where you've been busy all day and haven't done anything, that's
what the foreign policy situation is like right now.
Don't really know anything about the deal, not yet, and we probably won't until there's
There's an official announcement now because my guess is anybody who would have talked
to the press, they're not going to now.
And even if they get one person who would, the reporter is probably going to be, let's
just say more cautious in publishing a story that carries the same line unless they can
get it very well confirmed.
But we do know there is a deal that is attempted at being brokered.
It looks like the U.S. and Qatar are doing a lot of the work, and the general terms are
very similar to what was reported as a reached deal.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}