---
title: Let's talk about applied international law....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mmP_Y2hhSw4) |
| Published | 2023/11/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the application of international norms and how it varies due to power and connections.
- Compares international law to the law in the United States regarding equality in application.
- Mentions how military power plays a significant role in the enforcement of international laws.
- Talks about the influence of connections, like knowing influential people or holding veto power.
- Expresses that power and connections often override international norms and laws.
- Suggests that citizens of a country hold the key to curtail violations of international norms.
- Gives examples where domestic discontent helped shape policy and curtail violations.
- Emphasizes that powerful countries may not view themselves as bound by international standards.
- Stresses that change in policy comes from the people inside a country, not international organizations.
- Concludes by stating that power and connections determine the application of international law.

### Quotes

- "Power and connections overrides the international norms, the international laws, and that's how it works."
- "It's not at the UN. the more powerful a country, the less it views itself as a member of the international community."
- "The only people who can make their voice be heard to the point where it shifts policy."
- "There's no immediate transformation of this because it's a process that takes time."
- "Power and connections."

### Oneliner

Beau explains how power and connections often override international norms and laws, urging citizens to hold their countries accountable for policy shifts.

### Audience

Global citizens

### On-the-ground actions from transcript

- Hold your country's leaders accountable for violating international norms by making your voice heard (implied).

### Whats missing in summary

The full transcript provides a detailed understanding of how power dynamics and connections influence the enforcement of international norms and laws, stressing the importance of citizen action in shaping policy shifts.

### Tags

#InternationalLaw #PowerDynamics #CitizenAction #Accountability #GlobalRelations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the application
of international norms, and how it occurs,
and how that application varies at times,
because we've had a whole lot of questions about this lately.
Wondering why some rules, they don't really get ignored,
But they certainly don't really seem to be enforced.
And I want to remind everybody that this
is one of those videos where I'm talking about how it is,
not how it should be, because those things aren't the same.
So when it comes to international law,
international law works the same way
the law works in the United States, on a practical level, you know.
Everybody in the United States, no matter how well-connected, no matter how powerful,
no matter how much money you have, the law treats everybody the same.
It doesn't matter if you're running for office, you're not going to be extended extra-courtesies,
right?
Okay, everybody's scrunching up their face
and you're like, that's not how it works.
Exactly, exactly.
International law is the same way.
Power and connections have a whole lot to do
with how it gets applied or doesn't.
On the international scene, money, yeah.
I mean, it matters.
Power coupons matter.
But on the international scene there's another kind of power that is more important in this
math and that's military power.
The stronger a nation is militarily, the less international laws are enforced upon that
nation because they don't really have the ability to do it.
Think of it as a country that has a very strong criminal enterprise going on.
One that is stronger than law enforcement in that country.
The laws really don't get applied to that organization because they have more power.
So you have that.
And then you have connections.
meaning maybe in some country, if somebody knows a judge, they get favorable rulings.
If you know somebody that has veto power at the UN, you might get favorable rulings.
It's the same dynamic at play.
Power and connections overrides the international norms, the international laws, and that's
how it works. Again, this isn't good, but it's how it works and answers most of the
questions that come in. They've been coming in since the start of the conflict in Ukraine,
This is the general tone. This is the general answer to it. It's a power
dynamic. Another thing that matters and really matters on a level that is akin
to having a Supreme Court justice in your pocket is nuclear power. Nuclear
powers. I mean, yeah, other nuclear powers are like, hey, don't do that. Wink, wink,
but we're going to do the same thing. And that's how it plays out. Now, does that mean
it's hopeless? No. No. But what it takes is not other countries saying, hey, you're
breaking these international norms. You're violating this international law. You're breaking
this treaty.
That doesn't have much sway if you're talking about a country that has a lot of power and
connections.
What does matter is what happens at home.
If the citizens of the country that are violating those rules, if they have an issue with it,
they can curtail it.
If they make that position known at the family dinner that occurs every two to four years
in the United States, that can have a little bit of impact.
You can point to things where you've seen this play out.
A good example would be Vietnam.
There was a lot of upheaval at home, and it helped curtail the war, helped end the war.
Did it end it completely?
Not right away.
But the discontent at home helped shape that, and it helped bring about that conclusion.
You can look at a notorious base in Cuba that the United States has.
What was happening there once the outcry emerged and then how it was lessened, it didn't end,
but how it was lessened.
You can look at unmanned aerial vehicles and how it was initially used, the outcry, and
again how it was curtailed, not stopped.
There's the ability to mitigate that kind of violation of international law, but it
has to originate with the people inside that country.
That's where the power can actually be checked.
It's not at the UN.
the more powerful a country, the less it views itself as a member of the international community.
They're a world leader and leaders, you know, they get to look the other way.
They get to do things their own way type of thing.
This is also incredibly prominent when the country is also, to complete the analogy,
when the country is a cop.
When the country is a nation that has a position in the world of keeping order in other countries,
gets to bend a lot of rules, kind of like an undercover cop. That's how it
functions. Make no mistake about it, if you are waiting for the UN or an
international organization to say this country can't do that, they have to stop.
I mean they may say it, but what are they really going to do about it? The only
people that have sway in that case, well, it's you, the citizens of that country.
Those are the only people who can make their voice be heard to the point where it shifts
policy.
And when it shifts policy, understand that it may not completely end the practice, but
it can mitigate it.
It can curtail it.
And over time, it might end it.
There's no immediate transformation of this because it's a process that takes time.
You're dealing with institutions.
So why doesn't international law get applied equally to every country?
The same reason laws don't get applied equally to every person in a country.
Power and connections.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}