---
title: Let's talk about Biden, leverage, and signals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nq_Aw5CjyZY) |
| Published | 2023/11/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Explains the concept of leverage in the context of US influence on Israel.
- Points out that US aid to Israel is not leverage due to Israel's massive military expenditure.
- Describes the difficulty in identifying actual leverage the US holds over Israel.
- Mentions Biden's statement on extremist violence against Palestinians as a form of leverage.
- Talks about the significance of the US signaling pressure on Israel publicly.
- Suggests the possibility of a multinational force to enforce borders in the region.
- Comments on the potential responses from Israel to the US statements.
- Anticipates a shift in US foreign policy under Biden's administration.

### Quotes
- "That's leverage."
- "This is a development you're going to see again."
- "It's changing."

### Oneliner
Beau breaks down leverage in US-Israel relations, Biden's impactful statements, and potential multinational involvement in border enforcement, signaling a shift in US foreign policy under his administration.

### Audience
Policy analysts, activists.

### On-the-ground actions from transcript
- Monitor international responses for multinational force developments (implied).
- Stay informed about US signaling to Israel and its outcomes (implied).

### Whats missing in summary
Detailed analysis and context on US foreign policy shifts and implications.

### Tags
#US foreign policy #Israel #Biden #Leverage #MultinationalForce


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about President Biden
and leverage, and what leverage is and what it isn't.
And we're going to talk about some international signaling
that is occurring, and maybe even get into
a little bit of speculation.
Okay, so for quite some time now the topic of leverage has come up and it's in the context
of what leverage does the US have that it could use to influence Israel.
Most people who are in favor of this, they point to the aid the US provides.
That's not leverage.
That's not enough.
I know that they're one of the biggest supporters, like our biggest recipients of US aid.
However, they also have a massive military expenditure of their own.
They're not dependent on that aid.
As much as people like to look at it as if it is leverage, it's not.
have asked what leverage the US actually has. I'll be honest, it's been really hard
to come up with something because most of the leverage the US has is, it's like a
nuclear option. Not actually nuclear, it's something that is so dramatic it
would alter the balance of power in the Middle East. Israel knows the US isn't
going to do that. So it's not really leverage. And it's been hard trying to come up with something
to answer that question. I want to read you something. I have been emphatic with Israel's
leaders that extremist violence against Palestinians in the West Bank must stop and that those committing
the violence must be held accountable.
This is Biden.
Goes on.
The United States is prepared to take our own steps, including issuing visa bans against
extremists attacking civilians in the West Bank.
That's leverage.
It may not sound like a lot, but it is.
mainly because this is something the U.S. can actually do.
A lot of the other things that the U.S. has as leverage, it can't do it because it would
disrupt the balance of power in the Middle East.
That's something that's actionable.
So let's talk about signaling.
This part can't be missed. This is the most pressure the U.S. has put on Israel in a very, very, very long time, and I
don't mean doing it. I mean just saying it. That's very public, and it's sending the message that, A, the U.S. is no
longer going to look the other way.
look the other way when it comes to settlements. And B, it's something that U.S. policy has now
determined is going to be something that they try to enforce, sanctions, you know, type of thing on
individual people, not the country as a whole.
And the other signal that's here is those lines on that map, they're real and they need
to be respected.
That's the signal.
There's also a little bit of, hey, times are changing and you're going to have to change
with it. It's another piece of it. All of those are signals that are easy to see.
I'm going to step away from just the normal stuff for a second, because there's something
kind of interesting here. If you're reading the tea leaves, it seems like Biden
One might be entertaining the idea of a multinational force to enforce those borders.
You know, people talked, there was a lot of conversation about a multinational force afterward.
Seemed odd because, I mean, I can't imagine a lot of countries raising their hand when
And the question is, who wants to go in and, in essence, occupy that area?
That's a big ask.
However, if that multinational force wasn't there for that purpose, there might be a lot
more takers.
I would keep your ears open for the phrases,
multinational force, or internationalizing
the security responsibilities, something like that.
If you start hearing those terms,
I'm going to guess that a number of countries
have decided that the way to stop the fighting is to literally step between them.
Don't know that part.
That part is putting things together that don't necessarily make sense in the current
context and when you put them together that's a conclusion you can draw.
So I don't know that, but that's kind of what it looks like.
Now how Israel responds to this is going to be telling.
I don't know how they're going to respond to that.
There will probably be, they'll either ignore it or have a very strong response at first,
of the two, and then the real response will come out in a few days.
But this is a very, very public statement by the United States head of state that is
definitely trying to influence Israel.
How it's received and how well it works, we don't know yet.
But this is a development you're going to see again.
And Biden kind of signaled this was coming earlier, but I don't think anybody really
believed him.
So we'll have to wait and see how it plays out.
But we have definitely seen a departure from normal US foreign policy, from the status
quo of US foreign policy.
It's changing.
it changes, we'll have to wait and see.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}