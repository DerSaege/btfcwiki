---
title: The Roads Not Taken EP14
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bF8Ss-rWcIE) |
| Published | 2023/11/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Swedish dock workers are refusing to unload Teslas due to a wage agreement issue, showing solidarity among workers.
- Ukrainian resistance is alleged to be tainting food in Russian-occupied areas, causing a perception issue.
- A tentative deal for a pause in the Israeli-Palestinian conflict has been reached.
- Biden reiterates support for a two-state solution in the Israeli-Palestinian conflict.
- Space laser lady threatens to start a new party and accuses the FBI director of censorship.
- Legal developments include a guilty plea related to impersonation and Hunter Biden's team seeking to subpoena Trump officials.
- Nikki Haley calls for increasing the retirement age, while Chip Roy questions Republican accomplishments.
- Allegations in a lawsuit claim a guard encouraged inmate violence in Alabama.
- Social media platform Social is reportedly facing financial challenges.
- Environmental news reveals the significant cost of extreme weather and the need for infrastructure changes.
- The US Army overturns convictions of black soldiers involved in the Houston riots over a century ago.

### Quotes

- "You want to pick battles that are big enough to matter and small enough to win."
- "Governments aren't moral, they're about governing, they're about power."
- "Y'all have a good night."

### Oneliner

Beau covers foreign policy, US news, cultural events, environmental issues, and historical justice, urging action at the local level for meaningful change.

### Audience

Activists, Policy Enthusiasts

### On-the-ground actions from transcript

- Volunteer with organizations like Habitat for Humanity (implied)
- Stay informed about foreign policy impacts and advocate for cooperative approaches (implied)
- Support initiatives addressing climate change and infrastructure improvements (implied)
- Stay engaged with legal developments and advocate for justice (implied)

### Whats missing in summary

The full transcript offers detailed insights into unreported news events, legal developments, cultural controversies, and historical justice issues. Viewing the full content provides a comprehensive understanding of current affairs.

### Tags

#ForeignPolicy #USNews #EnvironmentalIssues #HistoricalJustice #Activism


## Transcript
Well, howdy there, internet people, it's Beau again.
And welcome to The Roads with Beau.
This is episode 14 of The Road's Not Taken.
And it is a weekly series
where we go through the previous week's events,
looking for unreported, under-reported news
or news that'll end up as context later.
And then at the end, we go through some questions
from y'all.
Today is November 19th, 2023.
we will start off with foreign policy.
OK, Swedish dock workers are refusing to unload Teslas
from the docks.
The action is being taken because Tesla reportedly
refused to sign a collective wage agreement.
In Sweden, I want to say like nine out of 10 jobs
are covered by these kinds of agreements.
So what you're seeing is a lot of solidarity
between workers, and it is spreading.
My understanding is that postal carriers
will stop delivering Tesla's mail tomorrow.
Let's see, there are reports that Ukrainian resistance
is tainting food and drink inside Russian-occupied areas
areas for stuff that is gonna end up in Russian hands. I looked into this, I have
found some evidence but it doesn't... not enough to say that it's like super
widespread. However, when it comes to stuff like this, perception is what
matters and this news is spreading like wildfire through Russian channels. So
whether or not it's true, that's one thing, but the Russian troops believe it's
true. There is a tentative deal for a short pause in fighting and the release
of some captives in the Israeli-Palestinian conflict. We'll wait and
see how it plays out. Biden has again repeated his call for a two-state
solution and put out an op-ed in the Washington Post, I think. It's worth
reading, especially if you're interested in foreign policy, because you can look at some of the
statements there and compare them to what the state has done and has hinted at, and you can get
a pretty clear picture of what U.S. policy is going to be like in the future if Biden has his way.
Okay, moving on to U.S. news, the space laser lady threatened to start a new party.
She also told the director of the FBI that she had posted some things on Twitter implying
that he should be familiar with them because of that, and he said that he didn't spend
much time on Twitter, at which point Green said, I'm sure you do, because the DHS organized
with other offices and censored people like me.
It is worth noting that the FBI director works for the FBI, which is part of DOJ, not DHS.
A former Santos fundraiser entered a guilty plea in relation to impersonating a McCarthy
aide, I believe.
Or maybe it was impersonating McCarthy.
Either way, it is another blow to any potential Santos defense.
Hunter Biden's legal team is requesting permission to subpoena Trump and former Trump
officials. The belief in what they're kind of moving for is they want to
demonstrate that Trump pressured officials to engage in what could have
been selective prosecution is what they're angling for. Nikki Haley
called for increasing the country's retirement age for younger people.
Chip Roy asked his Republican colleagues in Congress to name, quote,
one meaningful, significant thing the Republican majority has done.
I don't think he got an answer.
If the Democratic Party doesn't use this during the campaign,
Okay, allegations in a lawsuit against the Alabama Department of Corrections claim a
guard encouraged inmates to kill another inmate who has previously organized strikes within
the prison.
I have a feeling that this is something the Department of Justice may eventually get involved
in.
Moving on to cultural news.
Social has reportedly lost millions and may need to shut down.
The exact amount is being reported differently by different outlets as far as how much has
been lost, but tens of millions.
I haven't been able to discern which one is accurate.
A bunch of people on Twitter got mad about Target having a black Santa, like an item.
Anyway, it turned into a big thing there for a while.
More culture war stuff.
So environmental news.
Extreme weather already cost the United States about $150 billion per year in damages, which
Which means soon the US government is going to have to choose between the cost of changing
our infrastructure, transitioning, or continuing to pay massive amounts of money in damages.
And I put this under oddities just because of the timing on it, but this is something
I think might should be a bigger story because for those who are familiar with it, it's one
of those things where it's kind of nice to see, but when you want to talk about something
that's too little, too late, this definitely applies.
So more than 100 years after the Houston riots, the US Army has overturned the convictions
of more than 100 black soldiers.
The trial that occurred after the riot, it was the largest in U.S. military history.
It resulted in 60 life sentences and 19 hangings.
Thirteen of those were conducted at one time and to this day, it is the most soldiers hanged
at once at any time by the U.S. Army and now those convictions have been overturned.
The story that goes along with it is one that's worth looking into.
It sheds a whole lot of light on how things were back then.
It's nice to see it changed and I guess some of the descendants were present and my understanding
is that some of them may actually be entitled to some benefits through this.
But it's just nice to see some stuff like this addressed because anybody who ever actually
looked into that story, I mean everybody kind of walked away with the same, how did this
happen kind of thing, even though it was a hundred years ago, we know how it happened.
Okay, moving on to the questions and answers.
First, this episode was for Rosina.
Okay, something you often repeat is the idea that the best way to affect change is to begin
at the local level.
Something that's hard for me is choosing a thing that is big enough to start to affect
change.
Gotcha.
Um, you don't want to waste your time, I understand.
You want to pick battles that are big enough to matter and small enough to win.
At the same time, if you are just getting active
and trying to get out there and volunteer
or engage with different organizations,
you wanna start with something you like,
something that you enjoy.
Once you start doing it, it could be anything,
and it doesn't even have to be something
that is totally in line with what you want to accomplish.
The one that I know, that I've seen people do, that has led them to definitely more advanced
forms of activism was Habitat for Humanity, because they enjoyed it.
And when you're doing something like that, you end up meeting other people, you find
out about other causes, and eventually you find something that not only is effective
helps alleviate issues, but it's also where you need to be in the sense of it's where
you can do the most good, it's the most rewarding for you personally, and it doesn't feel like
a drain.
That's the important part, and the only way to find that out is to get out there.
Pick something you like.
Believe me, as you start doing it and start talking to people and finding out about other
causes, you will end up in the position you need to be in.
You just have to be willing to follow that path when it comes.
After the last few weeks, I understand why you've been covering the foreign policy
parts instead of the moral arguments.
It's exhausting, got it.
But my question is, why can't foreign policy be moral?
It should be, right?
I mean, it should be, you would think, because it's about power.
It doesn't have to be, but as long as nation-states, the way we envision them today, as long as
they exist the way they do, where everything is competitive rather than cooperative, foreign
policy is going to only be about power.
As long as we continue to allow our betters to divide us up by lines on a map or whatever,
it's going to be like that.
And if you want to understand it, you have to put the moral stuff aside because governments
aren't moral.
themselves, they're about governing, they're about power, so the foreign
policy is going to reflect that. I know it's not good, but it's the way things
are until we change them. How did the fundraiser do? We don't have the final
numbers yet because we people were donating after the stream, but my guess
is right around 10. Somewhere between 9 and 11. And yeah, we're good. That is
enough. This will be another year I think where we end up having to, you know,
we'll do all of the stuff we want to do. We'll ask them if they need anything
else and then we'll probably end up having to cut them a check too. Which is,
that's that's the position we want to be in so it went well and okay and I'm
supposed to end these or at least mention a ranch story so actually right
after that stream I walk outside and there were a bunch of coyotes that you
could hear throughout the night after the stream and I walk outside and I can
see these eyes you know it's dark and I can see the eyes kind of glowing and I'm
sitting there and I'm like great you know generally speaking this is not a
super dangerous situation but at the same time I got a little nervous and I'm
sitting there looking at them and I reach down real slow and get my
flashlight and I pull it up and shine it at them. I look at all those deer. It wasn't
coyotes. The coyotes, I guess they ran by. It was a bunch of deer and they were
right outside. But yeah, it was a tense couple of seconds there. But yeah, that's
all I've got for you right now. So that looks like it. So hopefully you
have a little more context, a little more information, and having the right
That information will make all the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}