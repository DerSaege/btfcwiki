---
title: Let's talk about Tuberville and the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NIRvCiiLmko) |
| Published | 2023/11/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Tuberville's actions have been impacting military promotions, recruitment, retention, and readiness.
- Tuberville blamed the administration for a problem he caused, prompting senators to test his support for individual candidate approvals.
- Republican senators like Romney, Graham, Ernst, and Young tried to push through military promotions individually but Tuberville blocked them by unanimous consent.
- The Senate floor turned into a Republican-on-Republican fight over military promotions.
- Despite the impasse, there's a parallel track being pursued through a resolution that may allow multiple confirmations at once.
- Nine Republicans crossing over is needed for the resolution to pass, and based on Senate floor dynamics, they might be more than halfway there.
- There's a possibility that McConnell might support the resolution, potentially ending the impasse without Tuberville getting what he wants.

### Quotes

- "It is definitely impacting recruitment and retention, and it is impacting readiness more and more."
- "It is now a Republican-on-Republican fight on the Senate floor."
- "I don't know that it's going to work, but at the same time, there's another parallel track that is being pursued."
- "This impasse may be coming to a close soon."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Senator Tuberville's actions impact military promotions, leading to a Republican-on-Republican fight in the Senate with a potential resolution to end the impasse.

### Audience

Senators and political activists

### On-the-ground actions from transcript

- Contact your senators to express support or opposition to the resolutions and actions described (implied)
- Monitor the progress of the military promotions issue in the Senate and stay informed on the developments (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the ongoing situation in the Senate regarding military promotions and the potential resolution to end the impasse. Watching the full video can provide a comprehensive understanding of the dynamics at play.

### Tags

#Senate #MilitaryPromotions #RepublicanFight #Resolution #Impasse #SenateDynamics


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about the latest installment
in the Tuberville saga and how all of that is playing out
because there was a development last night in the Senate
that it might change some math eventually,
but it definitely demonstrated a different kind of math.
So we'll go over what happened and where
it is likely to go from here. Okay, so if you remember, Senator Tuberville, he's
been holding up military promotions, and it is now to the point where it is
actually, it is definitely impacting recruitment and retention, and it is
impacting readiness more and more as the days go on at this point. He recently got
all upset and said that the administration was was trying to destroy the Senate or something like that,
the administration trying to blame the the issue on Biden, an issue that he caused.
So last night, yesterday, some senators decided to just try and see what he would do because he
said that he would support doing it individually, you know, if you just brought up each candidate
one at a time.
He had made indications that he wouldn't stop that going the long way through doing it.
So some senators tried.
Senators Romney, Graham, Ernst, and Young. Not exactly members of the Biden administration.
I mean, they're Republicans. They tried to push them through, spent hours trying to push them
through. And what they would do is they would go over the name, they would go over the person's
qualifications, and then they would call for a vote by unanimous consent, and
Tuberville would block it. Hours of this. Graham kind of, I don't want to say lost
is cool, but he was definitely not happy and said, you know, I've been trying to
work with you for like nine months to get you to understand, and then he went
into how it has become a retention issue, and I mean that's not the terms that he used, but
noted that Lindsey Graham gets it. So it is now a Republican-on-Republican fight on the Senate
floor, and it appears that Sullivan, who was also part of this if I didn't
mention, kind of indicated they planned on just doing this over and over again.
We'll have to wait and see how it plays out, but that was the takeaway from it.
What I walked away with was that they were going to do this again and again
and again until Tuberville caved. Now is that gonna work? I don't know because at
this point, it's hard to believe that Tuberville actually believes he's
gonna get what he wants through this route. So at this point it kind of seems
like he's just locked into a strategy and a goal and even though it's never
gonna line up he's just gonna follow through with it because he's getting
some headlines. So, I don't know that it's going to work, but at the same time, there's
another parallel track that is being pursued, which is basically through a resolution that
would allow the Senate to move a whole bunch of them at once, and get a whole bunch of
them confirmed at once. There would need to be nine Republicans crossover. That's the
math. If they wanted this to happen they need nine Republicans. Based on what we
saw on the Senate floor, they're more than halfway there. And I can think of
three others off the top of my head that are either prior military or
understand the military well enough to understand how big of a deal this is
be coming, by my count they only need to find one vote and there's a part of me
that thinks McConnell will give it to him. So this impasse may be coming to a
close soon and it does not look like Tuberville is gonna get what he wants.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}