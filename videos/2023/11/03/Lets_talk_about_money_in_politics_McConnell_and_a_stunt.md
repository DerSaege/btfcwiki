---
title: Let's talk about money in politics, McConnell, and a stunt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kkKhFT348-I) |
| Published | 2023/11/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Howley aims to introduce a bill to limit corporate funding in elections, seemingly targeting Citizens United.
- Senate Majority Leader McConnell warned Republicans not to support the bill, indicating it lacks party backing.
- Howley's bill may not have the support to pass, as it challenges a Supreme Court ruling based on constitutional grounds.
- Democrats are unlikely to support the bill, not due to opposing money in politics, but because it may not hold up in court and could provide Howley with a political win.
- Howley's move appears more symbolic for his base rather than a practical solution to money in politics.
- The bill seems more about signaling an attempt rather than achieving tangible results.
- Howley's tactic may enhance his credibility with his supporters, even if the bill is bound to fail.
- The core idea behind the bill isn't necessarily bad, but its feasibility and understanding of the political landscape are questionable.

### Quotes

- "We need to get money out of politics, that type of thing."
- "It's all about signaling. It's not about getting anything done."
- "This senator doesn't actually understand how things work."
- "Look, I tried to do something, but those people, the swamp or whoever, they wouldn't let it happen."
- "Maybe he just doesn't understand it, but that seems unlikely."

### Oneliner

Senator Howley's bill to limit corporate election funding appears more symbolic than effective, lacking support and legal feasibility, ultimately serving as a political gesture rather than a substantial change in money in politics.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Address the need to get money out of politics in real, tangible ways by supporting or engaging with organizations working towards campaign finance reform (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Senator Howley's proposed bill, offering insights into the political dynamics and feasibility of the legislation.

### Tags

#MoneyInPolitics #CampaignFinance #SenatorHowley #CitizensUnited #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
Lutz Bo again.
So today, we are going to talk about money and politics
and Howley and McConnell and something happening
that doesn't make any sense.
And then we'll go through what's actually going on.
OK, so a whole bunch of questions came in.
A whole bunch.
I don't know, maybe five.
And they're all about Senator Howley's new talking point.
He wants to introduce a bill that will limit the amount of money that corporations can dump into the elections.
Sounds good, right? Sounds like he's going after Citizens United.
The obvious question is, why would Holley do that?
Okay, so let's go through the political stuff first.
McConnell has already called the Republicans together in the Senate and been like,
hey I am the Senate do not sign on to this and told him that if they do that
they could expect some red on red fire. I believe he described it as they would
get incoming from the center-right. Then he went on to basically call people out
by name and say you would not have won your election, you would not have won
your election if it wasn't for the Senate leadership fund. Made it real
clear that the Republican Party is not actually behind this. Okay, so if it
doesn't really stand a chance of winning, of getting through, why is
Holley doing it? Does he think the Democrats are gonna sign on to it? They
won't either more than likely because they know that it's a show and they know
that it doesn't stand a chance to stand. So, Citizens United, that case was
decided on a constitutional basis, not a statutory one. Short version, you want to
get rid of that, you want to be able to put the limits in there that people want.
You need one of two things, a constitutional amendment or a Supreme
court that views the Constitution through a different lens. That's it. If corporations
are people, the government cannot restrict the amount of money they spend on speech.
That's the ruling. No amount of Senate grandstanding is going to change that aspect of it. So odds
Democrats are, Democrats won't sign on to it, not because they don't support getting
money out of politics, at least some of them, but because it would give Holley a win and
it wouldn't actually stand in court.
That's probably going to be the read on it.
So what I would do if this comes up, I would talk about the need to address this.
He's doing it more than likely as a show for his base.
We need to get money out of politics, that type of thing.
What he's doing is in a way that certainly appears to me to be very self-defeating and
And maybe he even knows that it's going to be defeated.
But he gives him that talking point, says, look, I tried to do something, but those people,
the swamp or whoever, they wouldn't let it happen.
Gives him good credibility.
But it's not going to go anywhere.
It's all about signaling.
It's not about getting anything done.
So I would take his message as he puts it out there because he's going to he's going
to have to try to sell this.
So as he puts those talking points out there, yes, that's absolutely what we need.
However, this senator doesn't actually understand how things work.
This is what we need because the core idea of what he's proposing, I mean, it's not
a bad idea. Just, I don't believe this is really even intended to stand. I mean
maybe he just doesn't understand it, but that seems unlikely. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}