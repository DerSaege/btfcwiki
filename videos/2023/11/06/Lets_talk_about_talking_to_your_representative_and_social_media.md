---
title: Let's talk about talking to your representative and social media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KzXHjM2jFOE) |
| Published | 2023/11/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of how you phrase your demands when contacting representatives.
- Differentiates between rhetoric on social media and effective communication with representatives.
- Advises on appealing to representatives' self-interest to influence their decisions.
- Emphasizes the impact of threatening to primary politicians as a more effective strategy than merely stating you won't vote for them.
- Advocates for supporting candidates who share your views as a proactive approach.
- Emphasizes that active involvement in politics is more impactful than occasional voting.

### Quotes

- "Voting once every four years, once every two years, it's not enough."
- "Getting involved in running a super-progressive candidate in the primary, that's hard."
- "The representative democracy that exists in the United States, it is advanced citizenship."
- "It might be more effective just to try to support somebody that you do want to vote for."
- "If you do that and they win, and they get up there, when you call them and you say, hey, you need to re-evaluate your position here, or we may have to run another candidate, they're going to know you will do it."

### Oneliner

Be mindful of how you phrase demands to representatives, appeal to their self-interest, and understand that supporting candidates who share your views is more effective than mere threats of not voting.

### Audience

Citizens, Activists, Voters

### On-the-ground actions from transcript

- Support a candidate who shares your views and actively back them (suggested)
- Threaten to primary politicians who do not support your interests (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the nuances of effectively communicating with representatives and influencing political decisions through strategic actions like supporting candidates and threatening to primary politicians.


## Transcript
Well, howdy there, internet people, Ledzebo again.
So today, we are going to talk about reactions and responses.
We're going to talk about calling your representative.
And we're going to talk about the difference between what
you say on social media and what the representative hears
when you say the same thing to them.
Because sometimes the rhetoric that gets used on social media
is not actually something that resonates
with your local politician or your representative up in DC.
We're going to do this because I got a message.
And basically, it was, hey, I called my representative
told them that if they didn't support a particular measure and call for a
certain thing, that I wasn't going to vote for them. And I could tell in their
voice they just did not care what I was saying. Give me a reason to still vote
for this person. First, I don't tell people how to vote. To be clear, I've been
doing this a long time. The only public endorsement I have ever made was for
in support of Big Bird running against Ted Cruz. I do not think that's my place.
So I can't do that, but I can talk to you about the way you phrased something and
why that hardline rhetoric that you see on social media doesn't actually
translate well when you're talking to your representative. So you are the
representative. You're in office. I'm calling you and I say, hey you have to
support cloning dinosaurs or I'm not going to vote for you. You as the
representative, you can look at me one of two ways. Either I'm serious when I say
that or I'm not. If I'm not serious, well, it doesn't matter. I'm irrelevant. You
take the information, your assistant puts it in a little book, helps you
get general information about sentiment. That doesn't really influence your
decision because your party platform and the polling, well they override that, helps
a little bit, but generally speaking I'm pretty irrelevant. You need a whole lot
of people calling to actually even begin to move a representative with those
options right there. Calling and the person doesn't think you're serious. Just
goes to express general sentiment. What if they think you are serious? Okay, you
decide that me demanding dinosaurs be cloned. I am serious about that and I'm
not gonna vote for you unless you do that. One of two things. Either I am a
I'm a part of a single issue voter group that the party has decided, the party in the polling,
has decided it's worth chasing those votes, in which case I probably wouldn't be making
this call because you already would have talked about it, or I'm not.
If you've decided to chase those votes, well then you'll tell me right then.
Why support that?
If I'm not, you go into the book.
General sentiment.
The other option would be that I am somebody who wants a person to align with them 100%
earn the vote. I am irrelevant once again, because if somebody demands a hundred
percent, well, you're never going to get it. So it's not worth chasing that vote, go
into the book about general sentiment. That phrasing, it sounds good on social
media and it motivates people. I'm definitely not saying to not use it on
social media. But when you're actually talking to the representative, it is
self-defeating because you render yourself irrelevant. Does that mean that
there's nothing you can do to shape the opinion of your betters? The person that
is supposed to be representing your interest, how do you get them to do it?
You have to appeal to their self-interests. Their people. Their people who sought out
positions of power, odds are self-interest is a big part of their personality.
How does Trump keep the Republican Party in line?
If somebody speaks out against him, does he say, don't vote Republican?
Don't vote Republican, vote Libertarian?
Does he say that?
No, of course not.
does he keep them in line? Other Republicans do this as well. If they
don't support the right positions, what should happen to them? Well, they are
rhinos and they need to be primaried. That gets a politician's attention way
more than I'm not going to vote for you. They need to be primaried because
they're the incumbent. They're in office. That's why you're calling them, right?
They might be able to skate by without a primary at all, which means they have
more money to use in the general to maintain their position to fulfill their
self-interest. But if you say, I represent this group of people, and if you don't
support the things that we support we may have to launch a candidate that does
support those things in the primary. You get way more way more consideration that
way because you're appealing to their self-interest. The problem with this, and
And this is where it falls apart for a lot of people.
Not voting for somebody, that's easy.
That's easy.
Getting involved in running a super-progressive candidate in the primary, that's hard.
One is way more effective than the other, though.
If you look at the way MAGA controlled the Republican party, you're going to
find out that a whole lot of it was due to their threats of primaring other Republicans.
Look at the commentators, look what they say, but more importantly, look what they did.
They've followed through with it.
Some of them actually won.
They've followed through with it.
The representative democracy that exists in the United States, it is advanced citizenship.
Voting once every four years, once every two years, it's not enough.
if you really want your voice to be heard.
Don't say, I'm not going to vote for you unless you do X, because they don't care.
You're irrelevant.
Your sentiment is recorded, but unless that sentiment far exceeds the polling and the
party platform, you're not moving them.
a primary, putting them in a position where they have to defend their positions, not from
some partisan enemy, but from somebody within their own party, somebody on their side of
the aisle, way more effective.
So I can't tell you how to vote.
It's not something I do.
But it might be more effective just to try to support somebody that you do want to vote
for.
That does align with what you want.
It's more work because you have to find somebody willing to run and then you have to actually
back them.
But if you do that, it is far more effective.
More importantly, if you do that and they win, and they get up there, when you call
them and you say, hey, you need to re-evaluate your position here, or we may have to run
another candidate, they're going to know you will do it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}