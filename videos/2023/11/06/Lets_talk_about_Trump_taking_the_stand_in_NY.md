---
title: Let's talk about Trump taking the stand in NY....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8GidD3Ag7Uk) |
| Published | 2023/11/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's appearance in the New York case was chaotic and a complete show, just as anticipated.
- Despite being found liable for fraud, Team Trump is still pushing the narrative of his innocence, which doesn't seem to make legal sense.
- Trump behaved erratically on the stand, going off on tangents and not answering questions directly.
- The judge had to intervene at one point, urging Trump's attorneys to control him or face consequences.
- The real decision has already been made, focusing now on the extent of the wrongdoing, not its occurrence.
- Trump treated his testimony like a campaign stop, making statements more for the public than the courtroom.
- There seems to be a strategy to provoke the judge into overreacting to create appealable issues and reach an appeals court.
- Beau doubts this strategy will pay off, given the judge's handling of the situation and the courtesy extended.
- The testimony was chaotic and didn't benefit Trump, with no mitigation of his situation.
- The attempt to provoke the judge seems unlikely to succeed based on how things have unfolded.

### Quotes

- "Trump's appearance in the New York case was chaotic and a complete show."
- "The real decision has already been made, focusing now on the extent of the wrongdoing, not its occurrence."
- "Trump treated his testimony like a campaign stop."
- "There seems to be a strategy to provoke the judge into overreacting."
- "The testimony was chaotic and didn't benefit Trump."

### Oneliner

Beau analyzes Trump's chaotic testimony in the New York case, questioning Team Trump's legal strategy and predicting its unlikeliness to succeed.

### Audience

Legal observers, Trump critics

### On-the-ground actions from transcript

- Stay informed about legal proceedings and their implications (implied)

### Whats missing in summary

Insight into the potential long-term consequences of Trump's behavior during the case.

### Tags

#Trump #NewYorkCase #LegalStrategy #Fraud #AppealsCourt


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Trump,
the New York case, him being on the stand,
how everything went, which was about as expected,
and what Trump's team might actually be trying to do.
OK, so if you haven't seen any of the coverage about this,
Short version, Trump being on the stand was every bit as chaotic as people anticipated.
It was a complete show.
Now one of the things that's occurring is it seems that Team Trump is continuing to
try to proclaim his total innocence and try the case.
The problem is that part's over.
Trump and company, the ruling's already there, liable for fraud.
So the extended push to try to just say that's not the case, it doesn't make a whole lot
sense legally speaking if you're using a normal legal tactic. This would be the
point to try to mitigate. That's not... it does not appear that that's what they're
doing. Trump was Trump. Going off the rails, going off on tangents, not
actually answering the question but talking forever, a whole bunch of things.
the judge actually extended the courtesy and told his attorneys to try to keep
him on track. At one point the judge said, I beseech you to control him if
you can. If you can't, I will. I will excuse him and draw every negative
inference I can. Again, the real decision here has already been made. It's
already occurred. Now they're looking at how bad it was, not whether
or not it happened. Trump treated it very much like a campaign stop. The
statements he was making were not for the courtroom, they were for public consumption.
At one point he was asked about a document and he said that you know he was too busy with China
and Russia and trying to keep our country safe and then he was reminded that he wasn't actually
president during the period in 2021 that this it just didn't go well. Now, one
would be forgiven for thinking that this was just a total fumble on Team
Trump's part. I don't think it is. I think what they are hoping for is to provoke
the judge into an overreaction, so they can create
appealable issues and try to get before an appeals court.
That seems to be what they're angling for.
Again, that's just a guess, but it's the only thing that really explains the behavior,
that they are hoping that the judge makes an error and therefore gives them
better grounds to appeal. I don't necessarily see that occurring
based on the way the judge has handled things so far, especially extending every
possible courtesy. I mean, courtesies that you or I would definitely not be extended.
So I think that the strategy that may be being attempted here, I don't think it's
going to pay off. The testimony was just a giant wreck and I don't think that
Trump did himself any favors in this. I don't think that anything presented,
mitigated where he's at at all it did not go well for team Trump and I don't
believe that if the strategy is to provoke the judge and generate that
outrage man that sounds really familiar I don't I don't think it's gonna work
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}