---
title: Let's talk about polling and time travel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tBs2tgfVRpE) |
| Published | 2023/11/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about new polling showing Trump leading for 2024 in battleground states.
- Mentions receiving messages prefaced by "I know you say polling this far out doesn't matter, but..."
- States that polling this far in advance is irrelevant and only useful for primaries and fundraising.
- Demonstrates the irrelevance of early polling by time-traveling to November 6th, 2019, where polling indicated Trump's victory but Biden ultimately won battleground states.
- Goes back to November 2, 2015, when early polling showed Ben Carson leading but proved inaccurate.
- Emphasizes that early polls are not predictive, useful mainly for fundraising and adjusting party strategies.
- Points out the importance of adapting strategies based on demographic responses to polling.
- Criticizes current polling methods as self-selecting and likely to skew towards conservative viewpoints.
- Concludes by reiterating that early polling is insignificant and not predictive of election outcomes.

### Quotes

- "Polling this far out is irrelevant."
- "It's not predictive."
- "Always irrelevant this far out."

### Oneliner

Beau demonstrates through time travel that early polling is irrelevant and not predictive of election outcomes, urging focus on primaries and fundraising strategies.

### Audience

Political analysts, campaigners

### On-the-ground actions from transcript

- Adjust campaign strategies based on demographic responses to polling (implied)
- Focus on fundraising efforts and primary elections (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the inaccuracy and irrelevance of early polling, cautioning against using it as a predictive tool for election outcomes.

### Tags

#Polling #ElectionOutcomes #CampaignStrategies #TimeTravel #PoliticalAnalysis


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk about polling
and messages and time travel because some new polling came out and it shows Trump with a
substantial lead for 2024 in some battleground states and it prompted a whole bunch of messages
And the funny part about it was that most of them actually included the phrase,
I know you say polling this far out doesn't matter, but, or some variation of that.
Because I have said that enough at this point where people know that that's going to be my
response. Polling this far out is irrelevant. It's only useful as a gauge for primaries and
fundraising and stuff like that. It is not predictive of the actual
election. I've said that a lot, but I don't know
that I've ever really demonstrated it. So today we are going to do a little
bit of time travel. Poof! It is now November 6th, 2019, so
roughly the same period out.
The polling that came out today says that the expectation is
that Trump wins the 2020 election and 56 percent of Americans believe that. Now
that's a little bit of a different kind of polling, obviously. That's the
impression that Americans have. During the same period, though, it is a little bit
different because the polling shows Biden winning battleground states, like
in Florida by two points, which he ultimately lost.
But I think we all know polling because of unlikely voters and all of that stuff in this
period it wasn't great.
So let's go back.
We will go back to November 2, 2015 and take a look at it then.
That's a little bit more accurate because it does show that the Republican candidate
seems to have a clear path all the way to the White House.
In fact, the presumptive nominee of the Republican Party doing really well, beating the silly
real estate developer from New York by like six points.
Because at this point in time, Ben Carson was leading in the polls.
They don't mean anything.
They're not predictive.
It's useful for fundraising strategies.
It's useful for the primaries, which are a lot closer.
It's useful for adjusting party platforms and positions and stuff like that.
And it's useful for kind of seeing where different demographics are at, but it's not predictive
of the actual results for the election.
That doesn't happen until just before the election.
They're worth paying attention to because what it does show is that Biden needs to up
his game in these particular states with the demographic of people who still take phone
calls from people they don't know and take surveys.
I don't know how big that demographic is, but he needs to work on it.
It's not predictive.
is going to get further and further off until there is a method of getting something that's
a little bit more representative.
Because current polling methods are very self-selecting and it will tend to skew towards the conservative
side of things.
Again, always irrelevant this far out.
matter. Useful for certain things, useful to keep an eye on, but it is not
predictive of the actual results of the election. Anyway, it's just a thought.
Alright, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}