---
title: Let's talk about missing uranium....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=j3fNMUps71g) |
| Published | 2023/03/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The International Atomic Energy Agency reported 2.5 tons of natural uranium missing in Libya, but it's not weapons-grade or enriched, causing unnecessary panic.
- If the missing uranium reached a group with the capability, it could potentially be enriched into weapons-grade material.
- The difference between natural uranium and weapons-grade uranium going missing is significant in terms of international response and urgency.
- The Eastern Libyan forces claim to have recovered the missing uranium, known as yellowcake, but it's not confirmed by the International Atomic Energy Agency yet.
- The best-case scenario is for the Eastern forces to demonstrate the uranium is properly secured or hand it over to the IAEA.
- Refusal to cooperate and demonstrate the security of the uranium could invite international intervention and lead to negative consequences.
- Beau believes that the situation will likely be resolved swiftly and peacefully, given the stakes involved.
- The missing uranium likely disappeared from a location outside of the government's effective control in eastern Libya, where the recovery was claimed.
- Beau urges against Western involvement in this matter, stating that it should be resolved with international partners or within Libya itself.
- The potential catalyst for escalation could be if the Eastern forces refuse to cooperate or hand over the recovered uranium.
- Beau advises everyone to remain calm and trust the International Atomic Energy Agency to handle the situation.

### Quotes

- "The difference between natural uranium going missing, that's where the international community goes, oh, no."
- "There's no good that can come of that."
- "I don't think they're going to make that mistake."

### Oneliner

Beau clarifies the missing uranium situation in Libya, urging calm and trust in international agencies while cautioning against Western intervention.

### Audience

World leaders, policymakers, diplomats

### On-the-ground actions from transcript

- Trust the International Atomic Energy Agency to handle the situation (suggested)
- Remain calm and avoid unnecessary panic (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the missing uranium situation in Libya, offering insights into the potential risks and necessary caution in handling such sensitive matters.

### Tags

#Uranium #Libya #InternationalRelations #Security #TrustIAEA


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about something
that went missing, and we're going
to provide a little bit of context to it,
because the way it is being reported by a lot of outlets
is unnecessarily vague and can lead people
to be a little bit more concerned about this
than they should be.
OK, so the International Atomic Energy Agency
said that 2 and 1 half tons of uranium went missing in Libya.
Now, first we need to start by clarifying what uranium
means in this case.
This is natural uranium in a concentrate.
This is not the uranium that you hear about in movies.
This is not enriched.
This is not weapons-grade.
If this material, the material that went missing,
was to make it to an entity that had the technological resources
to work with it, over time, it could become enriched.
It could become weapons-grade.
If that were to occur, it would not be two and a half tons.
It would be 25 to 30 pounds.
The way it has been framed by some outlets
is unnecessarily terrifying.
The difference between natural uranium going missing,
that's where the international community goes, oh, no.
We need to find that.
And weapons-grade uranium going missing,
that's when every intelligence agency on the planet stops everything that it's
doing until it's found.
It's, they're two very, very different things.
Okay, but there is still
two and a half tons
of
the natural uranium concentrate
that is missing, and for those who are familiar with this, yes, yellow cake.
The Eastern Libyan forces, keep in mind right now Libya is divided, they have
said that they have recovered it. Now at time of filming that hasn't been
confirmed by the International Atomic Energy Agency but they are working to
confirm it. I, my gut, tells me to believe them. There are not a lot of entities that
would say, oh yeah, we have that if they didn't. The best case scenario here is
that they do have it and they either give it to the IAEA or they demonstrate
that it is properly secured, something like that.
That's the best case scenario here.
An entity, a group, saying that they have that and not being willing to demonstrate
that it's secured and work with the international community on that, that is just begging for
intervention.
And it's not a claim that you can expect somebody to make if they weren't willing
to go down that route and work peacefully to make sure that it's secured.
Saying you have it and then saying that you're not going to cooperate, that would only lead
to bad things.
and those in charge in eastern Libya, that's not a mistake they're going to make.
So I would suggest that the most likely scenario is that this gets resolved relatively quickly.
The most likely place that this went missing, because it wasn't disclosed, is a location
that is outside of the effective control of what is deemed the government there.
It's inside the eastern portion which are the people who said they recovered it.
So it's everything that is coming out makes sense.
I don't think that this is something that should really concern people at this moment.
It's definitely something to watch because it could become concerning in a number of
different ways.
Before it even gets there, I would like to go on record now, there is nothing that the
West can do to make this situation better.
This is not a place for a Western presence.
It won't help.
This is a situation that is going to have to be resolved with other international partners
or within Libya itself.
This is not a location for the West to get involved.
There's no good that can come of that.
But the most likely catalyst for such an event is the Eastern forces now saying that, well,
we have it but we're not going to give it to you.
I don't think they're going to make that mistake.
That would just be ridiculous because all they had to do was say nothing.
They didn't have to say that they had recovered it.
At this moment I would suggest everybody relax and just let the International Atomic Energy
Agency do its thing.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}