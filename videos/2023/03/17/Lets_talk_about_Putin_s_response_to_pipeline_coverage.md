---
title: Let's talk about Putin's response to pipeline coverage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TVP8t7YwRK8) |
| Published | 2023/03/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Putin responded to the idea that Ukraine was behind the pipeline incident, dismissing it as "sheer nonsense."
- Putin's response doesn't make sense as it could motivate his troops and provide justifications if Ukraine was blamed.
- Putin's reputation in the West as a liar influences how people interpret his statements.
- Putin wants Russians to believe they are safe at home and not vulnerable to attacks like the one on the pipeline.
- Putin likely believes that the perpetrators, even if non-state actors, had state assistance in carrying out the attack.
- The possibility of a corporate actor benefiting economically from Russia not having the pipeline is not being widely considered.
- Beau believes Putin is telling the truth in saying it's "sheer nonsense" and that even if civilians were involved, he'd still suggest they had help.
- Putin's messaging domestically and internationally is a key factor in how he responds to allegations regarding the pipeline incident.

### Quotes

- "Putin responded to the idea that Ukraine was behind the pipeline incident, dismissing it as 'sheer nonsense.'"
- "Putin wants Russians to believe they are safe at home and not vulnerable to attacks like the one on the pipeline."
- "Beau believes Putin is telling the truth in saying it's 'sheer nonsense' and that even if civilians were involved, he'd still suggest they had help."

### Oneliner

Putin dismisses Ukraine's involvement in pipeline incident as "sheer nonsense," prioritizing domestic and international messaging over potential truths.

### Audience

Observers, Analysts, Activists

### On-the-ground actions from transcript

- Question the narratives presented by political leaders (implied)

### What's missing in summary

The full transcript provides a detailed analysis of Putin's response to allegations about the pipeline incident, including the potential motives behind his statements and the implications for domestic and international messaging. Viewing the full transcript can offer a deeper understanding of the complex dynamics at play in this situation.

### Tags

#Putin #Ukraine #Pipeline #InternationalRelations #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Putin's response
to the idea that Ukraine was behind
what happened to the pipelines
because he had a pretty short and sweet
response to that storyline.
And that response was its sheer nonsense.
He does not appear to believe the recent reporting saying
that six people, civilians, or non-state people from Ukraine
were behind the hit on the pipeline.
Now, we have a question about that.
Putin just dismissing the idea that Ukraine did the pipeline just makes me believe it.
It doesn't really make sense for him to say Ukraine didn't do it.
It seems like it would motivate his troops and give him justifications.
Why would he say they didn't do it?
Putin now has a reputation in the West as just basically anything he says is a lie.
And I think that's weighing heavily on how people are reading this.
I think the reason he said that he thinks it's sheer nonsense is because he thinks
it's sheer nonsense.
Keep in mind the plot line, the idea that six non-state actors pulled that off with
no assistance, no help from anybody.
It's possible.
I don't know that it's likely.
And I think that the reason Putin responded the way he did is because he doesn't believe
it.
I don't know that this is a situation where you should attribute him being deceitful to
it.
He very well might have just said it's sheer nonsense because he believes it's sheer nonsense.
And then from the other side of it, I honestly think that if he did believe it, he would
still say that it's sheer nonsense.
I don't think that Putin's messaging is helped by the idea that six random Ukrainians pulled
this off.
From Putin's perspective, he wants Russians at home to feel safe.
He doesn't want them to think that the war could come to them.
If six random Ukrainians were able to do that to the pipeline, well, they could hit anywhere.
That's not messaging that Putin would put out.
Even if he did believe it, I think that he would say he didn't.
And it doesn't help him on the international stage either.
it would be better for him if there was evidence that the US helped or the United Kingdom or
something like that.
And I think that his response of its sheer nonsense, and he went on to say that basically
you need skills to do this, was his way of kind of indicating his belief that even if
the people who actually did it, who actually went there and dove down and did it. Even if they were
non-state actors, they had state help. They had state training. They had state resources.
Which, to be honest, that makes a whole lot more sense. It's not impossible for six people to do
this on their own. It isn't. It's something that absolutely could occur, but it's a whole
lot more likely that there was some kind of assistance. I do not have a firm theory on
who was behind it. I have the usual suspects, and then I would also throw in that most people
People aren't looking at it from the possibility of a corporate actor.
It's a pipeline.
There's a lot of money to be made.
Those who stand to benefit economically from Russia not having that pipeline, they have
a huge motive.
That's not really being examined by anybody.
In this case, I think Putin is telling the truth.
I think the reason he's saying it's sheer nonsense is because he believes it's sheer
nonsense.
And even if he believed or there was some intelligence to suggest that it was six civilian
Ukrainians who did it, I think he would still continue to suggest that they had help.
Because that's really bad messaging at home for him.
Because if partisan Ukrainians can do that, well, they can get anywhere.
And that is not messaging he wants.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}