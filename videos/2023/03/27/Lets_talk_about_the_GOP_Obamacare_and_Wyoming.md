---
title: Let's talk about the GOP, Obamacare, and Wyoming....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CKdIyVd89pM) |
| Published | 2023/03/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Party introduces extreme legislation to energize their base.
- Legislation is often a show, creating fictional problems and proposing solutions.
- Wyoming introduced a ban on family planning, but it's not being enforced due to a previous constitutional amendment.
- The amendment in Wyoming, intended to stop effects of Obamacare, is now ironically blocking the ban on family planning.
- Similar instances occurred in Ohio due to a constitutional amendment related to health care.
- Republican Party's strategy of manufacturing problems and passing legislation has serious consequences.
- States are trying to deprive people of rights using this strategy.
- Republican Party's authoritarian march is being stopped by their own legislation.
- The fight to protect rights is ongoing despite these temporary victories.

### Quotes

- "Republican Party creates imaginary issues and then defeats them."
- "Take the win, but this is not a fight that's over."
- "Republican Party continues to enshrine more rights into state constitutions to defeat imaginary issues."

### Oneliner

Republican Party manufactures problems, passes legislation, and unintentionally blocks their own bans in Wyoming and Ohio, showing the serious consequences of their strategy.

### Audience

Advocates for protecting rights

### On-the-ground actions from transcript

- Advocate for the protection of rights in your community (suggested)

### Whats missing in summary

Deeper analysis on the long-term effects of the Republican Party's strategy and the need for continued vigilance in protecting rights.

### Tags

#RepublicanParty #Legislation #RightsProtection #ManufacturedIssues #Wyoming #Ohio


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about how the Republican Party
often gets in its own way in entertaining ways.
And we will talk about a pretty interesting scenario that
has developed in Wyoming most recently,
but it has happened in other places.
So we have talked lately on the channel
about how the Republican party introduces wild legislation for the purposes of just energizing
their base.
It's a show.
We have talked about this in particular in relation to the US House of Representatives
and how it's very likely that they're going to introduce a bunch of very extreme legislation
maybe it makes it through the Senate, but Biden will veto it and the Republicans
do not have the votes to override the veto. The habit, the tendency of the
Republican Party to just manufacture some problem out of whole cloth and then
propose a solution to it, that's not new. It's been around a really long time,
and what they tend to do is they create some mythical scenario. This horrible
thing is going to happen and then they propose legislation to stop this
horrible thing from happening that never would have happened to begin with and
then when it doesn't happen it looks like their legislation was successful.
They have been doing this a very long time. It's a good election strategy. It
it works. The Republican base is not necessarily great at discerning what is a real and what
is a fictional problem. So it works for them when it comes to electoral success. However,
While the problems are imaginary and the legislation is just a show, the legislation is real and
it has real effects.
Sometimes the effects of their show legislation end up being in direct opposition to the Republican
Party on the rare occasions when they actually do have policy.
A hilarious example of this just occurred in Wyoming.
As some of you may know, Wyoming, they put through a pretty comprehensive ban on family
planning and then it went to the courts.
The ban is currently not being enforced because back in the early 2010s, I want to say 2012,
Wyoming introduced a bill, a constitutional amendment, to stop all of the horrible things
that were going to occur because of Obamacare, the Affordable Care Act.
Part of this kind of prohibited a government takeover of health care.
So when it goes before the judge, the judge is like, hey, can you explain to me how this
isn't health care? I mean, it has to be done by a licensed medical professional."
And they were like, huh? And because of the constitutional amendment that they put in
place for an imaginary issue, they have successfully defeated their own legislation. Now, currently,
this is a temporary thing, but reading through it, it's going to be really hard for them
to argue through this.
And even the judge kind of indicated that they believed it was the Republican Party
trying to do an end run around the state constitution.
Which I mean, yeah, that is kind of what they're doing.
But they just didn't expect their constitutional amendment to, I don't know, ever be used.
Now this sounds like a one-off scenario.
It's not.
This happens more often than you might imagine.
In fact, in Ohio, due to a 2011 constitutional amendment about health care because, you know,
they scared everybody about Obamacare, I'm sorry, the Affordable Care Act, as most Republicans
will tell you, those are two very different things, Obamacare is bad and evil, the Affordable
Care Act is what they use to get their health care and that's good.
Anyway, in Ohio they did the same thing.
They introduced the same kind of constitutional amendment, and it was also used to strike
down their attempted ban there.
Now while all of this is humorous and it does go to show how the Republican Party creates
imaginary issues and then defeats them, what we do need to keep in mind is that this is
actually very serious.
You have a number of states trying to deprive people of rights and the only thing that is
standing in their way is their habit of lying to their base and introducing legislation
for made-up problems that just happens to be useful to stop their authoritarian march.
So while, sure, take the win, but remember that this is not a fight that's over.
It's going to continue.
We can only hope that the Republican Party continues to enshrine more rights into state
constitutions to defeat imaginary issues with health insurance.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}