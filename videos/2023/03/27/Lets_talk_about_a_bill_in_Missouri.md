---
title: Let's talk about a bill in Missouri....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zZ_QeFoM4Is) |
| Published | 2023/03/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Missouri's HB 700 introduced by Representative Hardwick, focusing on curtailing government response to public health issues.
- Mentioning a particular prohibition in the legislation on introducing microchips under the skin via vaccines.
- Explaining the potential harmful implications of banning something that doesn't exist, like microchips, leading to conspiracies.
- Addressing how politicians playing into an echo chamber can create real problems later.
- Noting that while some parts of the legislation may cause immediate concerns, the ban on microchips might lead to more vaccine hesitancy.
- Providing a glimmer of hope by mentioning the lack of support for the legislation from lobbying groups.
- Stressing the importance of scientific literacy, education, and critical thinking in the US.
- Expressing concern over the existence and persistence of such legislation despite past failures.
- Concluding with a call for better education and critical thinking skills in the country.

### Quotes

- "Now this is funny on some level. It's humorous."
- "This is one of the real issues when politicians play into an echo chamber."
- "It shows the desperate, desperate need for a lot of scientific literacy in the United States."
- "That vaccine hesitancy, the loss that will be caused by it, it came from show legislation."
- "We need better schools."

### Oneliner

Beau addresses Missouri's HB 700, focusing on the prohibition of microchips through vaccines, illustrating the dangers of show legislation and the urgent need for scientific literacy and critical thinking in the US.

### Audience

Activists, Educators, Citizens

### On-the-ground actions from transcript

- Advocate for improved scientific literacy in educational institutions (implied)
- Promote critical thinking skills in media consumption (implied)
- Support lobbying groups opposing harmful legislation (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the potential implications of legislation like Missouri's HB 700, stressing the importance of education and critical thinking to combat harmful narratives and actions.

### Tags

#Missouri #HB700 #Legislation #Microchips #VaccineHesitancy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about a piece of legislation
out of Missouri.
It is HB 700, introduced by Representative Hardwick.
Before we get into this, everybody, if you can see me,
take a look at my hat.
This isn't a joke.
This isn't a piece of satire.
this is real. Is it just something to point out ahead of time? Apparently, there are at least a
few legislators in Missouri who are going to try to take on Cognito, Inc. and Reagan, I guess.
The piece of legislation itself is more focused on curtailing
the government's ability to respond to a public health issue in the future I'm
sure I'm certain that's not how the people proposing it would would describe
it but that seems to be its main goal and there are a whole lot of things in
the bill that have legitimate public health concerns that hopefully will lead
to it being defeated. However, there is one particular piece of this potential
legislation that I think that we should draw attention to. There is a prohibition
on the introduction of mechanical or electronic devices under your skin via
of vaccine. Microchips. In the United States today, in a state legislature, there
is a bill moving forward that is designed to prohibit microchips. Now this
is funny on some level. It's humorous. For those that don't know, that's not a
thing, but it's also actively harmful in a way that people may not see at first
glance. Undoubtedly, in the future, those of a
conspiratorial mindset will absolutely point to this piece of
legislation and say, look, they, whoever they
is to them obviously have microchips because there in Missouri they tried to ban it.
Why would they try to ban something that didn't exist and it will become self-reinforcing?
So while the obvious first reaction is to laugh, there are downstream implications from
this that are not good, that will continue to cause a problem.
This is one of the real issues when politicians play into an echo chamber and they try to
cater to a small demographic that is highly energized.
They create a situation where their show legislation can actually cause real problems later.
Now keep in mind this is probably not going to be the part of this particular legislation
that gets the most pushback.
I think most people are going to look at it and just kind of giggle, especially when you
consider there are other portions of this that include like prohibiting public school
districts from requiring certain kinds of vaccines and stuff like that.
that has a much more immediate implication when it comes to public health.
But a piece of legislation that includes this is going to cause more vaccine hesitancy.
It's going to cause more problems down the road.
Now there is some good news here.
Bordwick introduced a similar piece of legislation last year, and it didn't go anywhere.
And he has admitted that pretty much every lobbying group in the state is opposed to
this legislation.
Good for them.
At the same time, because lobbying groups are opposed to it from those of a conspiratorial
mindset. It will feed into the idea that big business is out to get them. While
your first reaction may be to laugh at this, it shows the desperate, desperate
need for a lot of scientific literacy in the United States, a lot of education,
and a better understanding of, well, a lot of things, but we definitely need more critical
thinking when we're talking about media consumption, all of this.
This piece of legislation shouldn't exist, but here we are.
And not just does it exist, it's the second time something like this has been proposed.
So apparently the failure of it passing the first time was not enough to dissuade some
Missouri legislators from supporting it, because it is currently moving forward, though it
is expected to ultimately be defeated.
So in the future, when you hear about people talking about how these things, these chips,
they obviously have to exist because some state legislatures are trying to ban them,
this is where it came from.
This is where it came from.
That vaccine hesitancy, the loss that will be caused by it, it came from show legislation.
What I'm assuming is show legislation.
I mean, for all I know, Hardwick actually does believe this.
We need better schools.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}