---
title: Let's talk about Nebraska, allies, and holding the line....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xs2Q9ZsIaho) |
| Published | 2023/03/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nebraska legislature attempting a government takeover of healthcare, starting with banning gender-affirming care for trans minors.
- Senators vowing to filibuster the session to stop this legislation from moving forward.
- Four senators committed to filibuster, possibly joined by a fifth.
- Filibustering the entire session can grind the legislature to a halt but will need support to withstand the pressure.
- Urges people to show support by dropping a line to these senators to encourage and appreciate their efforts.

### Quotes

- "There's a whole lot of people who want to consider themselves allies right up until it's time to do ally stuff."
- "If you're doing it alone, it's hard."
- "People who are trying to take a stand could probably benefit from an email or a social media message."
- "This is definitely a time when some people who are trying to take a stand could probably benefit from an email or a social media message."
- "Y'all have a good day."

### Oneliner

Nebraska senators filibuster against healthcare takeover, needing support to grind legislative halt and hold the line; drop them a line to show appreciation and encouragement.

### Audience

Supporters of trans rights

### On-the-ground actions from transcript

- Contact Nebraska senators to show support (suggested)
- Drop them an email or a social media message (suggested)

### Whats missing in summary

The emotional impact and urgency of showing support for those fighting against the healthcare takeover in Nebraska.

### Tags

#Nebraska #Healthcare #Allyship #Filibuster #SupportTransRights


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Nebraska and health care
and being an ally and what it means to be an ally,
because there is a very unique situation developing
in the Nebraska legislature.
And it's one that pretty much everybody in the country
needs to pay attention to.
You know, there's a whole lot of people who want to consider themselves allies right up
until it's time to do ally stuff.
So in Nebraska, the Republicans there are attempting a government takeover of healthcare.
They want politicians to be able to decide what health care is appropriate for the people
in their state.
Of course, to move this process along, they are starting with a marginalized group that
doesn't have a real voice, a group that's easy to pick on.
They are starting by attempting to ban gender-affirming care for trans minors.
So this legislation, it moved forward.
A couple of senators there in the state legislature have vowed to filibuster the session.
just that. Everything. Everything. It started with two, I think it's up to five
now. I know it's up to four. There's a fifth one that from my understanding has
decided to join, but I haven't seen it reported anywhere. With four people, can
they filibuster the entire session or to use one of the quotes from this
conversation, burn the entire legislative session to the ground? Yeah, I think they
can. I think they can. That is definitely a way to play hardball. That's definitely
going to get attention. They would have to keep this up for three months, 90 days.
It's possible if they stick to it, if they really commit.
And it seems as though at least two of them have a very vested interest in making sure
that this legislation doesn't move forward.
And then you have at least two that have signed on to assist.
So they are going to attempt to filibuster the entire session.
After a while, they're going to get heat.
They are going to get heat.
Because this is going to basically grind the legislature to a halt.
If they can keep it up, that is exactly what it will do.
It will be noticed.
There will be people.
There will be businesses.
There will be a whole lot of interests that aren't getting what they paid for with all
those campaign contributions, and it's going to upset people.
They're going to need support because after a while, all of the pressure that is going
to be exerted on them, it'll take its toll.
It doesn't matter how committed you are to something, how personal the cause is to you.
If you're doing it alone, it's hard.
They need your support.
There is a bunch of reporting on this now, and I imagine there's going to be a lot more.
If this is a cause that matters to you, dropping them a line and saying you appreciate them
trying to hold the line probably go a long way and it may only encourage them
for a day but if enough people do it, if they feel that support, if they understand
that what they're doing is important to people all over the country, it might
be a day here, a day there, maybe ends up only prolonging it a week. But if they
decide to break on day 84, that week matters. I will drop some links down
below so you can get their names. This is definitely a time when some people who
are trying to take a stand could probably benefit from an email or a
social media message because they're in for a very rough three months if they
stick to what they've promised.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}