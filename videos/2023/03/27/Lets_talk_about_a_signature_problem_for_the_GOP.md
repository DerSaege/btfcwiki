---
title: Let's talk about a signature problem for the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5Cvfv_cqL-8) |
| Published | 2023/03/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Los Angeles County District Attorney George Gascon faced a recall petition from conservatives shortly after taking office.
- Over 300 deceased individuals were found to have their names used on the recall petition.
- The LA County Registrar County Clerk referred the matter to the California attorney general for fraud investigation.
- Another petition involving deceased individuals was also identified, suggesting potential fraud.
- The recall campaign denied involvement in using deceased individuals' names and alleged wrongdoing by the county clerk's office.
- While 300 signatures may seem minor, it fits into a trend of potential voter fraud allegations linked to Republican Party affiliates.
- Beau speculates that baseless claims by Trump and his supporters may have influenced constituents to believe voter fraud is easy to get away with.
- The situation in California may lead to significant fallout, potentially impacting the companies involved in circulating such petitions.
- Beau anticipates that the issue will not simply end with the attorney general's investigation.
- Overall, Beau raises concerns about the implications of the recall petition situation and its potential broader effects.

### Quotes

- "Over 300 deceased individuals were found to have their names used on the recall petition."
- "They may have created a situation in which their voters start to engage in it because they think it's easy."
- "I don't think that this is just going to go to the attorney general's office and nothing happen."

### Oneliner

Los Angeles County's recall petition reveals potential fraud with deceased individuals' names, hinting at broader implications for the Republican Party's rhetoric on voter fraud.

### Audience

Voters, Community Members

### On-the-ground actions from transcript

- Contact the California attorney general's office to report voter fraud (suggested)
- Stay informed and vigilant about potential fraud in political processes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the recall petition situation in Los Angeles County and its potential impact on voter perceptions and political processes. Viewing the full transcript offers a comprehensive understanding of the issues at play.

### Tags

#LosAngelesCounty #VoterFraud #RepublicanParty #PoliticalProcesses #CaliforniaAttorneyGeneral


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Los Angeles County
and a unique development that has occurred there
dealing with a recall petition
and how things are shaping up there
and why it might actually start to spell bad news
bad news for the Republican Party in a really bizarre way.
So the LA County District Attorney, George Gascon, almost as soon as he took office,
he became a target of conservatives and a recall effort was launched.
The L.A. County Registrar County Clerk has determined that a number, and by a number
I mean like more than 300, of the people who signed the petition to recall him were no
longer with us at the time of the recall.
They are suggesting that people who were deceased had their names used on this petition.
They have referred this to the attorney general, to the California attorney general, to be
investigated for fraud.
I believe they have also looked into contacting the Secretary of State's investigative division
as well.
This is a second petition.
There was another one that also had a bunch of signatures from people who are no longer
with us involving a completely different matter.
So it appears that at least by the county clerk's assessment, there's been some issues
and some fraud related to these petitions.
Now, the campaign involved with the recall,
they have said they have nothing to do with it,
denied all wrongdoing.
In fact, they seem to have alleged
that perhaps this is the county clerk.
It's the county clerk's office trying to cover up the fact
that they discharged a bunch of signatures and discounted a bunch of
signatures that they shouldn't have or something like that.
Either way, it's gone to the attorney general.
Now, this is a minor in and of itself.
I mean, not for the people involved, but in the grand scheme of things,
300 signatures isn't a big deal.
At the same time, it's part of a trend where you are starting to see more and
conservatives or people affiliated with the Republican Party, causes linked to
the Republican Party, engage in things that they would probably call voter
fraud. Now this is a little different but it goes to that same rhetoric.
I'm starting to think that it might be possible that all of the
rhetoric, all of the baseless claims that Trump and those who echoed his claims
spouted, they might have convinced their own constituents that it's easy to get
away with this. When it's not, you're kind of likely to get caught if you do
something like this. They may have created a situation in which their
voters start to engage in it because they think it's easy, because they have repeatedly
said that these processes aren't secure.
Now that's just a guess.
It's just something I've noticed.
There's a trend that has occurred recently, and that may have something to do with it.
But this is a case out in California that as it progresses, I have a feeling there's
going to be a lot of fallout from it in different ways.
There may be the companies that get involved with circulating these kinds of petitions
are going to have issues.
I don't think that this is just going to go to the attorney general's office and nothing
happen.
feel like you're going to see this material again.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}