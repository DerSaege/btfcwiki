---
title: Let's talk about Trump's petition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gjlVKB1zwO4) |
| Published | 2023/03/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Donald Trump is circulating a petition requesting his supporters to prevent his arrest.
- Those who sign the petition are directed to a page where they are asked to donate to Trump.
- The suggested donation amounts are high, reaching up to $3300, but any amount is accepted.
- Trump claims the petition will be super effective without specifying how.
- Trump's initial fear or panic about being arrested seems to have subsided, and now the focus is on monetizing everything.
- The lack of public support for Trump's in-person assemblies may be why there is no public counter for the petition.
- The petition is unlikely to impact whether or not Trump is indicted in New York.
- Despite this, Trump has the right to organize such a petition as part of his freedom of speech.
- The situation in New York with Trump is still developing, with a grand jury meeting scheduled for Wednesday.
- More information on the developments is expected to come to light soon.

### Quotes

- "Former President Donald Trump is circulating a petition requesting his supporters to prevent his arrest."
- "Trump's initial fear or panic about being arrested seems to have subsided, and now the focus is on monetizing everything."
- "Despite this, Trump has the right to organize such a petition as part of his freedom of speech."

### Oneliner

Former President Trump's circulating petition aims to prevent his arrest, with high donation requests, while developments in New York continue with a grand jury meeting scheduled. 

### Audience

Concerned citizens, political observers

### On-the-ground actions from transcript

- Monitor the developments regarding the grand jury meeting in New York (implied)

### Whats missing in summary

Insights on the potential consequences of the circulating petition and the importance of staying informed.

### Tags

#Trump #Petition #NewYork #Arrest #Supporters


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about some totally surprising news.
The former president of the United States, Donald J.
Trump, is circulating a petition.
This petition is being directed towards his supporters and the general
The purpose of the petition is to request that he doesn't get arrested.
The unsurprising part is that if somebody was to go through the process and fill out
the petition and sign up, they are directed to a page where they are requested to donate
to him.
He suggested donation amounts are high, reportedly up to $3300, but you can donate however much
you would like.
There is a claim saying that he would make it super effective, something to the effect
of 1500% effective or something like that, without really specifying how.
It does appear that if he was scared or panicked by the news, and that is what led him to say
that he was going to be arrested on Tuesday, that fear or panic has subsided a little bit,
and genuine Trump instincts have taken over, which basically monetize everything.
So the petition is circulating, I couldn't find a counter to see how many people had
signed it, so I'm not sure if it is doing well or poorly.
My guess would be due to the lack of support for in-person assemblies, in support of the
former president, he has decided not to include a public counter until the numbers are high
enough to to appear like he has a lot of support. It's worth noting that this
petition is probably not going to have a whole lot of effect on whether or not he
is indicted in New York. That being said, he totally has the right to organize a
petition for this. He has a right to petition for the redress of grievances.
And if there is anything that Trump knows a lot about, it is grievances. So
there is some surprising and totally unsurprising news related to the
developments in New York and the whole Trump situation there. We will find out
a little bit more. We should get a little bit more information today. The
grand jury is scheduled to meet again on Wednesday today. So we'll just have to
wait and see how everything continues to play out. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}