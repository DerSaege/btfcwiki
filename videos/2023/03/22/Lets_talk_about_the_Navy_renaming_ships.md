---
title: Let's talk about the Navy renaming ships....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eKPDXHmBtXQ) |
| Published | 2023/03/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- United States Navy is renaming ships named after Confederacy-related people or events, like the USNS Murray being renamed the Marie Tharp.
- The USS Chancellorsville is being renamed for Robert Smalls, a former slave who became the first black person to command a U.S. naval ship after learning to sail as a child.
- In 1862, Smalls, while being forced to serve on a Confederate steamer, commandeered the ship, guided it past five Confederate forts, and handed it over to the Union Navy.
- Smalls had a remarkable life beyond this act, becoming the first black commander of a U.S. naval vessel and influencing Lincoln's decision to allow black troops.
- Robert Smalls' story is a truly American and uplifting tale that deserves recognition and remembrance.

### Quotes

- "Robert Smalls' story is a truly American and uplifting tale."
- "If you are looking for a story that is just purely American and kind of uplifting, Robert Smalls is definitely one to look into."

### Oneliner

The United States Navy is renaming ships named after Confederacy-related figures, like the USNS Murray becoming the Marie Tharp, and the USS Chancellorsville being renamed for Robert Smalls, a former slave turned hero and influential leader.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Research the remarkable life of Robert Smalls to learn about his contributions and impact on American history (suggested).
- Share Robert Smalls' story with others to spread awareness of this uplifting tale (exemplified).

### Whats missing in summary

Exploring the full transcript can provide a deeper understanding of the significance of renaming ships and recognizing impactful historical figures like Robert Smalls. 

### Tags

#USNavy #ShipRenaming #RobertSmalls #AmericanHistory #CivilWar #InspirationalLeadership


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are gonna talk about some new names
because the United States Navy has decided
to rename a couple of ships.
And this is a process that is becoming
more and more familiar to a lot of people.
And the Navy started this, I want to say,
back in 2020 is when they really started looking into it.
But they are finally cutting ties with a couple of ships
that were named for people or events
dealing with the Confederacy.
One of them is the USNS Murray, and it
is being renamed the Marie Tharp.
This is an oceanographic ship being
renamed for somebody who had a whole lot to do
the development of the Theory of Continental Drift. That's cool. The other is the USS
Chancellorsville, whose ship is being renamed for Robert Smalls. Robert Smalls was a person
who was born a slave in South Carolina, and then went on to become the first black person
command a U.S. naval ship after learning how to sail as a kid. The thing is, that
alone is something that could get a ship named after you, but that's only a piece
of the story. Smalls, in 1862, was being forced to serve on a Confederate steamer
called the planter, and he was the pilot. One night the Confederates, they went for
shore leave. They left him, him and other people being forced to serve on that
ship. They got their families and they set sail. Smalls guided that ship past
not one, not two, not three, not four, but five Confederate forts making it through
the sign and counter sign. The challenge and knowing all of this the the signals
and got past all of them. And once out of range of the last fort, hoisted a white
flag and turned the ship over to the US Navy, to the Union. And as wild as that
story is. That's only a small small part of this guy's life. Other highlights
include him being the first black commander of a naval vessel for the US
and how that happened is just wild. He's also kind of credited for helping to
sway Lincoln when it comes to allowing black troops. The guy's life was amazing
and this is all during like a five-year period and he wasn't somebody who just
faded into obscurity after the Civil War. If you are looking for a story of just
that is just purely American and kind of uplifting. Robert Smalls is definitely
want to look into.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}