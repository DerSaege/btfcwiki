---
title: Let's talk about Trump not getting picked up today....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VBQ2uX9Gp7I) |
| Published | 2023/03/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump claimed he was going to be arrested on Tuesday, but it didn't happen, leading to questions.
- Speculations on why Trump made this claim include fear, panic, or hoping to elicit a response.
- Trump's supporters have remained relatively calm, with some exceptions of false claims of devices in buildings.
- The NYPD investigated these claims, finding no actual threat, but the politically charged nature may lead to further investigation.
- Despite some energized individuals, the overall response from Trump's base has been muted, preventing potential regrettable actions.
- The small group willing to take action in support of Trump may face consequences.
- The lack of significant protests or energetic responses indicates that Trump's play for outrage was not successful.

### Quotes

- "Trump claimed he was going to be arrested on Tuesday, but it didn't happen."
- "The overall response from his base has not been overly energetic, which is good."
- "The muted response from his supporters is good. It's going to keep people from making mistakes."

### Oneliner

Trump's failed arrest claim prompts muted response from supporters with some exceptions, avoiding regrettable actions.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Stay calm and avoid getting caught up in potential provocations (implied)

### Whats missing in summary

Insights into the potential ongoing legal troubles and the impact of false claims on law enforcement and investigations.

### Tags

#Trump #Arrest #Supporters #Response #LegalTroubles


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about something
that was supposed to happen today, on Tuesday,
and it didn't.
And it has prompted a whole lot of questions
as to why it didn't happen.
So we're going to talk about that.
We're going to talk about why Trump may have said that,
run through the various possibilities.
Then we're going to talk about some of the effects
from his statement, and how they may not have been what he really wanted, and just kind of go
through the whole thing. Okay, so it's Tuesday. According to Trump, he was going to be picked
up today. He's going to get arrested today. Big surprise. He lied about that, too. Okay,
people want to know why. There are a number of possibilities. One thing that everybody
needs to keep in mind is that there is a habit of wanting to assign a very devious, nefarious
motive to anything that Trump does. There is the possibility that he's just scared.
He's scared and panic set in and he heard a little bit of a conversation and thought that
that meant that today was the day and all of that stuff. It could be that simple. He was wrong.
he's been wrong before, he will be wrong again. It could just be that. It may not
be some planned event hoping to elicit a response. Another option is that, you know,
his army of lawyers told him that it wasn't likely today, but it was likely in
future. So he might have tweeted that out or whatever it's called over there. He
might have said that in hopes of eliciting a massive response, creating
outrage that would show up before he's actually indicted and hopefully
influenced of the grand jury. You know, he might have been hoping that it
It created protests, and wait, he called for protests, didn't he, in that sense?
That could be it.
If that is what he was hoping for, if he was hoping for a whole bunch of people to take
to the streets and therefore influence the grand jury or influence the DA or whatever,
it did not go according to plan.
There have been some assemblies.
Luckily and thankfully they have been relatively calm thus far. They've also been very small.
The one that was supposed to be the largest was supposed to have 150 people there,
and it looks like about 50 showed up. I mean, I had, when I was in high school,
there were larger protests over the dress code. And I went to a high school that was so small,
it was a high school and a middle school. It wasn't New York City. Those numbers,
they do not bode well for the amount of support the former president has that
are, that's energized enough to lead people to take to the streets. So, you
know, it's not over yet, but based on that initial events, it doesn't look like
like he's going to get what he wants when it comes to that. It does not look as though
there are a whole lot of people who are willing to put themselves on the line the way he expects
them to. So if that's what he was hoping for, he is sorely disappointed at this point. That
may change in the future.
Honestly, while that is a theory, my best guess is that he has so much going on, so
many different legal troubles, that he is scared, that he's just panicked.
Maybe he got a briefing from his lawyers and really wasn't paying attention during briefings
as he is known to do and just heard bits and pieces and assumed that he was going
to be arrested on Tuesday. I don't put that out of the realm of possibility. Now
as far as his supporters, so far pretty calm. It is worth noting that there are
some exceptions to that. The NYPD has had to look into a number of false
claims of devices at buildings and the buildings do appear to be somewhat
related to the court system up there. So there are some people who are energized
enough to take some form of action. Now NYPD has said that there was nothing
that goes boom at any of these places, but the calls were reportedly made. Now
this is one of those things where people may be getting in way over their head.
It's worth remembering the unlawful use of violence or threat of
violence to influence those beyond the immediate area of attack to achieve a
a political, religious, ideological, or monetary goal.
These people may have
bitten off way more than they can chew.
It is unlikely that this is going to be
treated as a simple hoax
because of the political nature of what's going on.
So I would imagine a very large investigation is going to take place
because of that. And there will be more people who end up locked up because of
their support of the former president, assuming they're related. And at this
point we don't necessarily know that. So overall, the response from his base has
not been overly energetic, which is good. It's going to keep people from making mistakes,
doing things they're going to regret. And that's also good for people in the area who
just happen to be around. People won't get caught up in something and maybe get hurt.
So the muted response from his supporters is good. It is good for, well, pretty much
everybody but him, and even though most are muted, there is a small group that is
willing to do something. At least it appears that way. So we'll have to wait
and see how everything plays out, but at this point it does not look like Trump
is going to be arrested on Tuesday, as he said. If he was trying to generate
outrage and create massive protests and people in the streets and all of that, does not look
like that play was successful.
And there are some people who are still apparently willing to go to bat for them and do things
that are going to risk them being confined for quite some time.
But we'll have to wait and see how it all continues to play out.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}