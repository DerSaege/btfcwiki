---
title: Let's talk about Trump, privilege, and documents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TcfPIstVwyQ) |
| Published | 2023/03/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special counsel's office sought to pierce attorney-client privilege with one of Trump's lawyers.
- Judge found prima facie evidence of a criminal scheme allowing DOJ to compel testimony.
- Trump allegedly misled his attorneys, leading them to mislead federal authorities.
- The timeline and evidence could demonstrate Trump's deliberate misleading of attorneys.
- Lawyer will have to testify before the grand jury about previously privileged information.
- DOJ's effort to uncover the truth is genuine, focusing on the phrase "retention."
- Willful retention of documents could be a significant issue if Trump deliberately misled his attorneys.
- Other proceedings apart from New York and Georgia are underway, potentially overshadowed news.

### Quotes

- "Prima facie evidence is a very low standard. Literally, on its face, this is what it looks like."
- "It's worth noting that in the conversations, it does appear that that phrase retention comes up."
- "There are other proceedings happening, and in this case, definitely seems like big news that might get overshadowed."

### Oneliner

Special counsel seeks to pierce attorney-client privilege, implying Trump misled lawyers and potentially faces willful document retention allegations amidst overshadowed proceedings.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Stay informed about the ongoing legal proceedings and developments (implied)
- Support efforts to uphold transparency and accountability in legal cases (implied)

### Whats missing in summary

Insight into the potential implications for Trump and the ongoing legal challenges he may face.

### Tags

#Trump #LegalProceedings #AttorneyClientPrivilege #DOJ #Transparency


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk a little bit about the Trump document case.
While all eyes are on New York, remember there are other proceedings moving forward.
This one involves the special counsel's office and what's happening there.
According to reporting from ABC and what sources reportedly told them, the special counsel's
office sought to pierce attorney-client privilege with at least one of Trump's lawyers and
to compel that lawyer, Corcoran, to testify before the grand jury about things that would
normally be covered by attorney-client privilege.
The judge agreed.
The judge agreed and found prima facie evidence of this criminal scheme that would allow DOJ
to compel that testimony.
It's worth noting, prima facie evidence is a very low standard.
Literally, on its face, this is what it looks like.
It's not a very high bar, but in this case it's enough to get this testimony.
It seems as though what was presented to the judge was the belief that Trump deliberately
misled his own attorneys, who then were put in a position of misleading the feds through
no real fault of their own, is the way it looks at this point.
That seems like it would be something pretty difficult for the feds to prove.
How do you demonstrate that the former president deliberately misled the attorneys?
It's not actually as hard as you might imagine, not in this case, not when there's a clear
timeline of when they were asked for the documents back, when a meeting between the attorneys
and Trump took place, when the feds were informed that there was a diligent search and that
everything had been returned and all of this.
That combined with other pieces of evidence, say hypothetically the security footage from
the club that we know the feds got, that could establish that Trump was aware of the documents
after that point, but continued to tell his attorneys something else.
Or it could show that Trump moved them beforehand.
There's a whole lot of scenarios involving basically a calendar of events and the footage.
And then there's a whole bunch of other ways they could get to that information.
While it sounds hard to prove or to demonstrate, it's really not.
So according to the reporting, the lawyer will end up going back before the grand jury
and have to talk about six separate lines of inquiry that were previously covered by
attorney-client privilege and didn't have to talk about it.
That really does indicate a genuine, a very genuine effort on the part of DOJ to get to
this.
It's also worth noting that in the conversations, it does appear that that phrase retention
comes up.
As we have talked about from the beginning, accidentally ending up with a bunch of documents,
that's one thing.
willfully retaining them, that's something else.
If the feds, and if this reporting is accurate, it certainly seems to be the case.
If the feds believe that he deliberately misled his own attorneys about the existence of those
documents, that goes a long way to showing willful retention.
So while everybody is focused on New York, and awaiting news from there, and then eyes
are likely to turn to Georgia, just remember there are other proceedings happening, and
in this case, definitely seems like big news that might get overshadowed.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}