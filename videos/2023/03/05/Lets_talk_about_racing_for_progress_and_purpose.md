---
title: Let's talk about racing for progress and purpose....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xVSgRbMEQi0) |
| Published | 2023/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message from someone in the car scene who wants to use their hobby and skill to shift people politically.
- The person is left-leaning but has an in with predominantly right-wing individuals through their interest in cars.
- Concerned about being labeled a hypocrite for loving cars and racing while also caring about the environment.
- Advises focusing on the purpose of using the car scene to bring people to a more progressive place rather than worrying about criticism.
- Notes the difference in the political spectrum where the right is better at building a road to move people to their side compared to the left.
- Emphasizes the importance of using available tools, like one's interest in cars, to shift thought and bring about change.
- Acknowledges that taking heat is inevitable, especially from the left, but staying focused on the purpose is key.
- Mentions that helping someone move away from a bigoted ideology is worth any criticism faced.
- Encourages using one's skills and interests, like racing cars, as a means to influence thought and progress.
- Suggests finding like-minded individuals in the left-leaning car scene to create a unified front for positive change.

### Quotes

- "Is it worth a tank of gas to get somebody away from a bigoted ideology? Yeah, absolutely it is."
- "An activist can't be a perfectionist. They have to be a realist."
- "In war, you use whatever tools you have at your disposal."
- "If you think that you can use that to shift thought, you kind of have an obligation to do it."
- "But if that road is built, you can't abandon it."

### Oneliner

Beau advises using one's interests and skills, like racing cars, to shift thought towards progress and purpose, despite facing criticism, especially from the left.

### Audience

Left-leaning activists

### On-the-ground actions from transcript

- Connect with other left-leaning individuals in the car scene to create a unified front (suggested).
- Use your interest in cars as a tool to influence thought and progress (implied).

### Whats missing in summary

The full transcript delves into the importance of utilizing one's skills and interests for activism, even if facing criticism, to drive positive change in political ideologies.

### Tags

#Activism #Progress #PoliticalShift #CommunityBuilding #SkillUtilization


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about racing for progress,
and purpose, and roads, and something
that one side of the political spectrum is much better at.
I do this because I got a message from somebody.
And this person has a hobby.
They have a skill.
And it gives them an in with, well, it gives them an in with people that are predominantly
on the right.
And they're not.
They're on the left.
This skill is cars.
They're car guy.
Race, go to meets, all of that stuff.
And the person wants to use that in to help move people.
to help bring people over, get them out of the right into a more progressive place.
And they say, I love modifying cars, racing, going to meets, et cetera, but as a left-leaning
individual, I feel somewhat alone in the car scene.
And goes through and talks about how they're worried about being labeled a hypocrite.
How can they claim to be for saving the environment when they drive something that gets 12.4 miles
to the gallon. That's really specific. And it talks about how the other ways
they've tried to lower their carbon footprint and all of that stuff, but
their concern is basically being called a hypocrite because of this one thing
that they do, but this one thing that they do also gives them an end. What do I
think? Don't think about it in terms of what other people might say about it.
think about it in terms of purpose. Your goal setting out is to use the car scene
to get people to a better place. That's your goal. You're going to have to
remember that because you're right. You're going to take heat for it, especially if
you get on social media. You're going to take heat for it because the right in the
United States is way better at something than than the left. And that's building
a road. If you are in the center in the US, there is an established road to get
you to the far right. And those on the far right, they generally don't take
swings at those that are moderately right or just a little right.
Happens every once in a while, like rhino type stuff, but generally that doesn't occur.
On the left, the left spends a lot of time critiquing each other because it is more thought
based.
It is more intellectual than fear based, so that happens.
One of the byproducts of this is that road that exists to get from center, or in this
case, to get from the right to a more progressive place, it's got gaps in it.
Because people are very concerned about having the correct take on everything, and being
correct on everything, because we are taught all of these struggles are interrelated.
And they are.
And they are.
My question is, is it worth a tank of gas to get somebody away from a bigoted ideology?
Yeah, absolutely it is. Absolutely.
And that's the way you have to look at it.
You're going to take heat, but it's not going to be from the right, it'll be from the left.
And you're just going to have to remember your purpose when that happens.
purpose when that happens. And eventually, something that will happen will be somebody
who you set on that road, moving to a more progressive place, they are going to outgrow
you. They'll see you as to their right because you race cars. I think it's a great idea.
I think it's a great idea.
If that is your in, if that's your way
that you can help shift thought, use it.
Use it.
Yeah, you will take heat for it.
100%, I'm certain.
But it's probably worth it.
As long as you can remember, your purpose
is to use that in that you have, to shift thought.
Yeah, it's not ideal, but let's be honest.
You're already racing.
You're already burning that gas, right?
It's already happened.
12.4 miles per gallon, right?
That's pretty specific.
I don't think you just pulled that number out of the air.
That's really what your vehicle gets.
You're already doing the part that's harmful.
Do the part that's helpful at the same time.
That seems like it should be an easy thing.
If you have that in, use it.
And I would.
You mentioned in there about finding other left leaning
people who are in the same scene.
I'm sure they exist.
I'm sure they exist.
And if you all got together and you made it known
that that was your unifying thing,
it would get good attention within that community
long as you win. I think it's a great idea. There are a lot of little pockets
of people that are just left. They're just left out. There is no road
established to get them to a more progressive place. If you can help build
that? Yeah, do it. Do it. 100%. It's one of those things that the left has real
problems with. You know, everybody wants to be correct about everything because it
is more intellectual. It is more utopian. It's about finding the right, the right
combination of things so everybody gets a fair shake, we get that deep systemic
change and everything runs as close to perfect as possible. What you're talking
about, it's not the intellectual pursuit. It's the activism side of it. An activist
can't be a perfectionist. They have to be a realist. You have a tool, use it. Use it.
It's that simple. You know, people love to adopt the language of we're at war with this, or we're at war with that.
In war, you use whatever tools you have at your disposal. You have been given one.
If you think that you can use that to shift thought, you kind of have an obligation to do it.
And when you get people who will undoubtedly call you hypocritical, take it.
Okay, you're right. But if that road is built, you can't abandon it. You have to
get as many people down that road as you can. So, I think it's a great idea. I do.
I think that it's a method of reaching people who will not be
reached another way. Do it. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}