---
title: Let's talk about Trump, DC, and filings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ws-QVpmgiqs) |
| Published | 2023/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the lack of coverage on Trump's proceedings in DC and predicts increasing attention.
- Urging support for striking workers at Warrior Met and sharing a link for assistance with final expenses.
- Detailing the civil proceedings in DC regarding Trump's actions on January 6th.
- Explaining Trump's argument of absolute immunity due to his actions being part of his duties as president.
- Summarizing the Department of Justice's filing, stating that public communication doesn't include incitement of violence.
- Not taking a stance on Trump's incitement but asserting that if plausibly alleged, he doesn't have immunity.
- Implying that the courts may send the case back for further proceedings, indicating a negative development for Trump.
- Pointing out the potential financial penalties and damage to Trump's image if the civil case proceeds.
- Mentioning the likelihood of extensive media coverage if the proceedings advance.
- Concluding with a vague statement and well wishes.

### Quotes

- "Speaking to the public on matters of public concern is a traditional function of the presidency."
- "If it does move forward, expect there to be tons of coverage about it."

### Oneliner

Beau addresses Trump's civil proceedings in DC, explains the lack of immunity for incitement, and warns of potential damage to Trump's image if the case proceeds.

### Audience

Activists, supporters, observers.

### On-the-ground actions from transcript

- Support striking workers at Warrior Met with assistance for final expenses (suggested).
- Stay informed about the developments in Trump's civil proceedings and potential implications (implied).

### Whats missing in summary

Full details on Trump's proceedings and potential consequences can be better understood by watching the full video. 

### Tags

#Trump #CivilProceedings #WarriorMet #FinancialPenalties #MediaCoverage


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and filings
in DC and everything that's going on with that.
This is a set of stuff that really
isn't getting much coverage, but it's probably
about to start getting more.
And we'll go through what all of the new developments mean.
But before we get into all of that, in a recent video
said that if there was anything that we could do to continue to help the workers
up at Warrior Met that we would do so. So I would like to draw your attention to a
link that will be down below. After a meeting, one of these striking workers
was lost in an accident because of the strike and everything else that's been
going on. They need some assistance with the final expenses. So that link will be
down below if you are so inclined. Okay, so there are proceedings going on in DC
dealing with Trump's activities on the 6th and these proceedings are civil.
Trump has argued is basically making making the claim that everything he did
the speech and everything that went on was in the course of his duties as
president and therefore he has absolute immunity to any civil litigation or
anything like that. That claim went to a federal appeals court. The
Department of Justice put in a filing on it. This is what it says, speaking to the
public on matters of public concern is a traditional function of the presidency
and the outer perimeter of the president's office includes a vast realm of such speech.
That traditional function is one of public communication.
It does not include incitement of imminent private violence of the sort the district
court found the plaintiff's complaints have plausibly alleged here.
version, the Department of Justice is not taking a position on whether or not Trump
actually incited anything.
What they're saying is that because the courts have found that that was plausibly alleged,
if that is the case, he does not have immunity for that.
That isn't in the scope of his normal duties, therefore the immunity isn't there.
The suggestion is basically made to kick it back down to a lower court and let proceedings
continue.
This is a bad sign for the former president.
Generally speaking, this was kind of the strongest argument he had.
That it shouldn't even take place, shouldn't have to go through any of this because he
has immunity.
If the courts decide that he doesn't, that means all the proceedings go forward.
That means that there's going to be a civil case with possible financial penalties.
There are a number of people who have launched cases in this category.
And if they move forward, the proceedings alone would be incredibly damaging to the
former president and any political aspirations that he might have for the future.
Because that's another set of revelations that will continue to come out in the press.
If it does move forward, expect there to be tons of coverage about it.
Because in civil proceedings, the language tends to be a little bit more blunt.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}