---
title: Let's talk about new Trump document case developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MS9mKeNS1xI) |
| Published | 2023/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New reporting on the Trump documents case confirms his long-held beliefs and dispels false comparisons.
- Many within the FBI believed Trump's attorneys conducted a diligent search and returned everything, leading to a recommendation to close the criminal case.
- Issue arose not from documents being present but from not returning them when asked for.
- Prosecutors had to subpoena surveillance footage showing boxes being moved to change the belief that everything was returned.
- Trump's willful retention of documents escalated the situation.
- Beau believes that if Trump had remained silent after the search, the issue may not have escalated.
- FBI agents initially believed everything was returned and wanted to close the case until video evidence showed otherwise.
- The reporting challenges the narrative of FBI bias against Trump.
- Contrasts between how Biden, Pence, and Trump handled similar situations are evident in the reporting.
- Beau anticipates that this information will influence the special counsel's investigation.

### Quotes

- "The issue was not that somehow they wound up there. The issue is that once they were asked for, they allegedly weren't returned."
- "That was the problem. And that's what moved it to the situation we're in today."
- "It looks like up until they literally had video evidence of boxes being moved, they wanted to let him go."
- "This reporting challenges the narrative of FBI bias against Trump."
- "Y'all have a good day."

### Oneliner

New reporting confirms long-held beliefs about Trump documents case, dispels false comparisons, and challenges FBI bias narrative while contrasting handling of situations by Biden, Pence, and Trump.

### Audience

Interested viewers

### On-the-ground actions from transcript

- Stay updated on developments in the Trump documents case (implied)
- Follow reputable sources for ongoing information on the special counsel's investigation (implied)

### Whats missing in summary

Insightful analysis and commentary on the evolving dynamics of the Trump documents case and its implications on different political figures.

### Tags

#Trump #DocumentsCase #FBI #Biden #Pence #Investigation


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about some new reporting
when it comes to the Trump documents case
because some information has come out
and it has confirmed one of two long time beliefs
that I've had about the case.
Things that I've repeatedly said on the channel.
And it also kind of gets rid of a false comparison
that is often made.
So the reporting suggests that many within the FBI
believed Trump's attorneys.
They believed that the attorneys had conducted
a diligent search and that everything had been returned.
And because of that, their recommendation,
What they were arguing for was to close the criminal case.
No harm, no foul.
Everything was returned.
Something we talked about from the very beginning.
The issue was not that somehow they wound up there.
The issue is that once they were asked for,
they allegedly weren't returned.
That was the problem.
And that's what moved it to the situation we're in today.
The argument that was being made was, you know,
the lawyer said that they went and looked,
they've returned everything, close it up, that's it.
It wasn't until prosecutors subpoenaed the surveillance
footage and saw boxes being moved
that that belief changed.
So it certainly appears that even after all of that time, that if Trump had returned everything,
that that would have been the end of it.
At this point, with this reporting and this knowledge now coming out, it really does show
the difference between Biden and Pence in their cases and what's happening with Trump.
It shows the difference.
And this is something we have talked about a long time.
It was that willful retention of the documents that was really the issue.
And that's what took everything to the next level.
The other thing that I have repeatedly said, and there's no confirmation of this yet,
but I will believe this until I'm shown something otherwise, is that even once the search had
taken place, even once they showed up and looked and found all of that additional stuff,
I believe that if Trump had said nothing, there'd have been nothing.
That that would have been the end of it.
It was the publicity that he brought on it that pushed it to the next level.
That's a personal opinion.
That isn't part of the reporting.
But the reporting does clearly state that the FBI agents on the case believed the attorneys
believed everything had been returned and therefore believed the proper course of action
was to close the criminal case.
The argument about, you know, well the FBI is just out to get him, apparently not, that
that witch hunt doesn't, that isn't backed up by reporting.
So the way that Team Trump has attempted to cast the FBI over this, that doesn't appear
to be true.
It looks like up until they literally had video evidence of boxes being moved, they
wanted to let him go.
And then the other thing, that false comparison between the way Biden and Pence handled it
versus the way Trump appears to have handled it.
This reporting, it's probably not going to get the play that it should because it shows
a number of things and I am still of the belief that at some point reporting will come out,
initially that search was going to be viewed more as a retrieval operation than
something for something to be used later. He was just trying to make sure they got
it all back. But that's just a hunch. Either way, this information is
is probably going to play pretty heavily into the way
the special counsel is looking at all of these developments.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}