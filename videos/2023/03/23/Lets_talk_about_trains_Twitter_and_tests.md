---
title: Let's talk about trains, Twitter, and tests....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cetW3AM2oJs) |
| Published | 2023/03/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reporting on accounts identified on Twitter trying to influence the narrative following a train derailment in Ohio.
- Accounts linked to pro-Moscow voices spreading misinformation like inaccurate maps and false cancer rate charts.
- Some accounts appeared to be Moscow-controlled, while others were American accounts amplifying the message.
- Beau finds it strange that an intelligence service like Moscow's or even Republicans are spreading such misinformation.
- Speculation that these accounts were using Twitter's paid verification service to increase reach and steer the narrative.
- If it was an information operation, the goal might have been to test the effectiveness of using the paid service to influence American politics.
- Potential consequences like congressional hearings if foreign intelligence services use paid services to influence internal politics.
- Beau expects more in-depth analysis on the incident in the future to understand better what happened.
- The time lag between the event and control of the narrative is something to watch for.
- The incident may be a precursor to similar events in the future.

### Quotes

- "Some of the accounts appeared to be Moscow controlled, and some were just American accounts amplifying that message."
- "It's not actionable. There's nothing that really can come of spreading disinformation about this."
- "If it turns out that the paid service is going to be used as a microphone for foreign intelligence services to try to influence internal American politics, it might lead to congressional hearings."
- "Right now the reporting is kind of light, but it's interesting and it's something that is probably one of those things we're going to see again."
- "It may not have been successful but it was just an interesting little tidbit I saw."

### Oneliner

Beau reports on suspicious Twitter accounts linked to pro-Moscow voices spreading misinformation after a train derailment, raising concerns about potential foreign influence on American politics.

### Audience

Internet users

### On-the-ground actions from transcript

- Keep an eye out for misinformation and disinformation spread on social media platforms (implied).

### Whats missing in summary

The full transcript provides more detailed insights into potential foreign influence operations using social media platforms.

### Tags

#Twitter #Misinformation #ForeignInfluence #Politics #SocialMedia


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about trains and Twitter,
and some reporting that is coming out
that isn't really unexpected but a little surprising
in some ways.
Right now, the reporting's pretty light.
I would imagine more will come out as time goes on.
The general tone of it is a non-profit identified a bunch of accounts on Twitter that appeared
to be attempting to steer the conversation in the days and weeks after the derailment in Ohio.
and that some of the accounts were linked to, let's just call them pro-Moscow voices at this point,
and they tried to spread misinformation, disinformation. Things like maps of potential areas that are going to be
impacted that weren't accurate,  rate, or charts about increased cancer rates, which wouldn't happen in a couple of
days.
Just little stuff like this.
And again, some of the accounts appeared to be Moscow controlled, and some were just American
accounts amplifying that message.
The interesting thing to me about it is that I don't get it.
This does not seem like something that an intelligence service would do.
It's not actionable.
There's nothing that really can come of spreading disinformation about this with the possible
exception of tying it to this happened because you're sending money to Ukraine type of thing,
which is that's very much a Moscow talking point right now.
But to be fair, it's also a Republican talking point right now.
An organic Republican talking point, not saying that they're...
Anyway, so just because you see somebody say that doesn't necessarily mean they have been
influenced by an influence operation or an information operation like that.
The other thing that I found interesting is that it seems, according to the reports, that
some of these accounts were using Twitter's paid verification service and using the increased
reach that is supposed to come along with that to try to steer the conversation.
And this is the only part that makes sense to me.
And to be clear, this is total speculation and not in any of the reporting.
But if it turns out that it was an information operation, the goal was probably to test to
see how effective it was. And they just picked a random event and tried to steer
it and manipulate it and see what the results were using the new paid service.
If it turns out that the paid service is going to be used as a microphone for
for foreign intelligence services to try to influence internal American politics, it might
lead to congressional hearings, something along those lines.
There will probably be government interference at that point.
They will get involved, some kind of intervention.
Again, right now the reporting is kind of light, but it's interesting and it's something
that is probably one of those things we're going to see again.
We'll see something along the lines of this occur in the future and there will be references
back to it.
I will keep my eyes out for more in-depth discussion about what exactly happened and
the time lags between the event and and how long it took them to kind of control
the conversation or if they were even able to because we have to keep in mind
it may not have been successful but it was just an interesting little tidbit I
saw and given the fact that we've talked about how some of this might happen I
of course found it very interesting anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}