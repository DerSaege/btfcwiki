---
title: Let's talk about a win at the Supreme Court for Americans with disabilities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VnQLqWNzTpQ) |
| Published | 2023/03/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a significant win at the US Supreme Court for a group of people often overlooked.
- Mentions a heartbreaking story of a deaf student neglected by the school system.
- Describes how the student was given inflated grades and only offered a certificate of completion.
- Explains how the parents used the Individuals with Disabilities Education Act (IDEA) to seek justice.
- Mentions the district agreeing to pay for additional schooling and American Sign Language classes.
- Points out that families with unique needs often settle for what is best for their children.
- Explains how the family also used the American with Disabilities Act (ADA) to pursue monetary damages.
- Emphasizes that the Supreme Court's decision allows families to seek compensation for lost opportunities and education.
- Stresses the significance of this ruling for families without access to legal representation.
- Urges schools to prioritize making things right for students with disabilities.

### Quotes

- "All of that was robbed. They don't get that."
- "School districts better start making it right by students who fall under this."

### Oneliner

A Supreme Court win allows families of neglected students to seek compensation under the ADA, demanding justice for lost opportunities and education.

### Audience

Families, advocates, educators.

### On-the-ground actions from transcript

- Support families navigating special education needs (suggested)
- Advocate for inclusive education practices (suggested)

### Whats missing in summary

The emotional impact of the Supreme Court ruling and the potential widespread implications for families seeking justice for their children with disabilities.

### Tags

#US Supreme Court #Disability Rights #Education #Legal Justice #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a big win at the US Supreme Court.
Good news.
And it's for a group of people who don't often get their wins talked about.
In this case, as in a lot of the other cases, it's certainly worth talking about.
This one, however, needs to be news because there are a lot of people who are going to
have an avenue opened up for them because of this.
Like many big wins at the Supreme Court, it starts off with an absolutely heartbreaking
story.
A deaf student going to school who the Supreme Court basically said was neglected is the
general view of it.
And his parents, during this time that he wasn't learning anything and was basically
being ignored, his parents were told that he was doing fine.
He was getting inflated grades and this went on until right before graduation when his
parents were told that he wasn't going to get a diploma because he wasn't really doing
anything but they'll give him a certificate of completion.
This is the moment when they started kind of looking at legal options and they used
idea, which is the Individuals with Disabilities Education Act.
And they wound up settling, as often happens with families like this.
The district was basically like, you know what, we got it, we'll go ahead and pay for
some additional schooling and pay to teach him, you know, American Sign Language.
Think about it.
All that time in school, and he was using a sign language that he had developed on his
own, couldn't even communicate, could not communicate with other people because his
sign language was unique.
People who have kids who have unique needs at school, they often have unique needs at
home and they're often, you know, the money plays into it.
So when they go to court, they just want their kid put right, you know, they want their kid
to have those opportunities.
So they often settle.
So after that occurs, they decide to use the ADA, the American with Disabilities Act.
Lower courts said, no, you can't do that.
already settled under this, under this other law, the Supreme Court has decided there is
nothing in idea in that law that stops somebody from also using the Americans with Disabilities
Act.
So they can pursue monetary damages for, you know, basically wasting a decade of this kid's
life in addition to getting the education that he should have got to begin with.
This opens up this avenue for a whole lot of people.
There are a lot of people who don't have access to lawyers that can take something
to the Supreme Court.
There are a lot of people who don't really have the means to play the legal system for
as long as it takes to get things right.
This coming down from the Supreme Court means that a family that doesn't have those opportunities,
that settles to do the best they can by their kid, can then use the ADA to get monetary
damages as well.
This is huge, and again, because of everything going on, it's probably not going to get
the coverage that it should, and people need to know about this.
When you hear about some of these cases, they focus on the fact that they have a hard time
communicating.
about everything that you learned in school. All of the basic interests that
you have that were developed during this period, all of that was robbed. They don't
get that. School districts across this country better start making it right by
students who fall under this, or they better start writing checks. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}