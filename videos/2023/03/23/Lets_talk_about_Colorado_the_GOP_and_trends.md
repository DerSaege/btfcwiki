---
title: Let's talk about Colorado, the GOP, and trends....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JY3TqYkhxOE) |
| Published | 2023/03/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on Colorado's political landscape, focusing on the declining GOP and overlooked trends within the Democratic Party.
- The GOP in Colorado is losing ground, with the Democratic Party now leading in registered voters.
- Interestingly, Republicans leaving the party are becoming unaffiliated rather than joining Democrats.
- Speculation arises about why Democratic Party members are also leaving, potentially due to not being progressive enough.
- Suggests that the Democratic Party in Colorado might need to shift towards a more progressive stance to retain and attract voters.
- Calls for those critiquing the party's lack of progressivism to participate in primaries and push for change.
- Notes a decline in registered voters for both parties simultaneously, signaling a deeper issue beyond people choosing independence.
- Encourages the Democratic Party to seize the moment by embracing progressive ideas to motivate voters in Colorado.

### Quotes

- "Republicans leaving the party are becoming unaffiliated rather than joining Democrats."
- "It might be time for the Democratic Party in Colorado to become even more progressive."
- "Those numbers are really interesting, seeing both parties in decline at the same time."
- "The Democratic Party can seize the opportunity, one way or another."
- "To me, it seems like its best chance is to motivate voters by really embracing progressive ideas there."

### Oneliner

Beau updates on Colorado politics, revealing shifts in party allegiance and suggesting a more progressive stance for the Democratic Party to thrive.

### Audience

Colorado voters

### On-the-ground actions from transcript

- Participate in Democratic Party primaries to push for a shift towards a more progressive platform (implied).
- Support and advocate for progressive ideas within the Democratic Party in Colorado (implied).

### Whats missing in summary

Deeper insights into the reasons behind the decline in registered voters for both major parties in Colorado.

### Tags

#Colorado #DemocraticParty #GOP #Progressivism #Voters


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to check in on Colorado again.
About a week ago, we looked at the situation in Colorado
and talked about it a little bit.
Since then, there have been a whole bunch of articles
talking about what's happening to the GOP there
and how they are embracing positions that have led to kind of a long losing streak there.
There is something interesting that's going on though that might be getting overlooked.
We understand that the Republican Party is losing ground there in a big way and their
new leader of the state level GOP is probably not going to help much in that regard.
It's probably going to continue the current trend or even make it worse.
But there's something else.
Okay, so in 2016, the Republican Party had 31.1% of the registered voters.
The Democratic Party had 30.9%.
So the Republican Party had just a little bit of a lead in 2016.
Today the Republican Party has 24.2% and the Democratic Party has 27.4.
So now the Democratic Party is in the lead.
The rest are unaffiliated.
But that leads us to something interesting.
The Republicans that are leaving the Republican Party, they are not becoming Democrats.
They're unaffiliated and that's often chalked up to the Republican Party in Colorado moving
further and further right and that makes sense, that tracks.
But what about the Democratic Party?
They're in the lead now and from their perspective that's good, but they're still declining.
They're declining at half the rate, about half the rate, but they've still dropped.
They have less registered voters today than they did in 2016.
What if it's for the same basic reason?
The Democratic Party members that are leaving, that are becoming unaffiliated, they're not
going to the Republican Party, that's obvious, they're becoming unaffiliated.
If the Republican Party has been viewed as too far right for Colorado, to the point where
Republicans are leaving, what does that say about the Democratic Party losing registered
voters as well. They may be too far right as well. It might be time for the
Democratic Party in Colorado to become even more progressive. The people leaving
the Democratic Party or those who aren't being replaced, it very well might be
occurring because the current positions of the Democratic Party are not far
enough to the left. They aren't progressive enough to capture the loyalty
of people who want a more progressive future. They view the Democratic Party
party as being too far to the right.
It's either that or you have people who believe the Democratic Party is ineffective.
It is one of the two.
It seems unlikely that the trend that is occurring in both the Republican and the Democratic
party. It seems unlikely that it's just because people want to be independents
now. It has to do with policy in some way. We know what it is with the
Republican Party. It very well might be the same reason with the
Democratic Party. It might be time for those who always talk about how the
Democratic Party isn't progressive enough to maybe see what happens in a
primary. You have progressives in Colorado. It might be that moment to kind
of shake it from a very centrist Democratic Party to an even more progressive one.
Those numbers are really interesting, seeing both parties in decline at the same time.
There might be something to that.
It's conventional wisdom at this point now that the Republican Party in Colorado is just
going to continue to decline.
The Democratic Party can seize the opportunity, one way or another.
To me, it seems like its best chance is to motivate voters by really embracing progressive
ideas there.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}