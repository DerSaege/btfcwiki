---
title: The roads to understanding Mr  Beast, charity, and aid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WNYEhip1K48) |
| Published | 2023/03/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Distinguishing between charity and aid is key, with charity addressing symptoms and aid tackling root causes.
- MrBeast's altruistic actions have sparked criticism from a left perspective, but the goal is not to stop him from helping.
- Critics suggest MrBeast should transition from charity to aid to address systemic issues and make a more significant impact.
- While some question MrBeast's capitalistic approach, it's acknowledged that he operates within a capitalist system.
- The desire for MrBeast to provide long-term solutions is prevalent, even though dramatic content may still be necessary for his platform.
- A valid criticism is the potential for MrBeast to enhance his impact by focusing on aid rather than solely charity.
- The left's perspective aims for systemic change and root cause aid, but they may struggle to effectively communicate their ideologies.
- Despite the criticisms, the overarching sentiment is not to halt MrBeast's charitable deeds but to encourage improvement and systemic change.

### Quotes

- "Charity gives a person a fish. Aid teaches a person to fish."
- "Nobody on the left is actually upset or wants him to not do what he's doing."
- "Every heartwarming thing he does is a symptom of a systemic failure."
- "Aid, switching from charity to aid, I think that he'd be pretty open to it."
- "The left does not want people to stop helping people."

### Oneliner

Beau breaks down the distinction between charity and aid, addressing criticism towards MrBeast's philanthropic efforts from a left perspective while advocating for systemic change and a shift towards aid.

### Audience

Creators, activists, supporters

### On-the-ground actions from transcript

- Reach out to MrBeast or his team to suggest transitioning from charity to aid (suggested)
- Encourage MrBeast to focus on providing long-term solutions to address root causes (suggested)

### Whats missing in summary

Full understanding of the nuances and motivations behind the left's criticisms of MrBeast's charitable actions.

### Tags

#Charity #Aid #SystemicChange #Philanthropy #Criticism #RootCauses #Activism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Mr. Beast, and aid,
and charity, and the difference between the two.
And we're going to talk about some of the statements that
have been made about what he's been doing,
and kind of put them into focus.
Because some of the people who have been talking about this,
they haven't done a great job of explaining
position and it has led a lot of people to be pushed away and it's also led to a lot
of confusion.
So I've got this message, I'm going to read it, we're going to go through everything
afterward.
Okay, Beau, I was talking to a friend about the criticism Mr. Beast is receiving for his
charity work and they said they didn't like what he was doing.
They're a fan of yours, and I brought up the tens of thousands of dollars of charity you've
done for women's shelters and asked if they had the same problem with you.
They said no, because what you do is aid, not charity.
What the heck does that even mean?
Then they said you don't profit from the people you're helping.
My brother, please explain to me how paying to help people see is profiting from them.
I don't like capitalism and would consider myself left until I'm around people who call
themselves leftists.
Do they not want him to help people see what the heck is wrong with what he does?
I know you're going to talk about purity testing and diversity of tactics, but I genuinely
want to understand the criticism, so can you please just explain the logic?
I guess I need to read some theory, but no to all that noise.
I don't care about any theory that tells me to oppose helping people.
It's wrong.
Break down these objections into crayons for me.
I'm sure I'm not the only one who's confused.
So if you don't know what's going on, MrBeast, an incredibly popular YouTuber, has
put out videos where they're engaging in objectively good things. They're helping
people. And criticism has come out. The criticism is framed from a left
point of view, but they're not always clear as to what the end goal is. I want
to be very clear on one thing. Unless you're like nut picking and going and
finding the absolute wildest statements you can, nobody on the left is actually upset
or wants him to not do what he's doing.
It's not that they think that him helping people is bad.
It's a yes and thing.
They're trying to make it better, not stop it.
just not always good at explaining that. Now I'm going to go through the
critiques and criticisms that I've seen. I'm sure there are others out there you
are talking about, I mean you're talking about the left. If you want if you want
five opinions put three leftists in a room and ask them to give you one
opinion. But I'll go through the ones that I know. And the first part here that
kind of caught my eye. What you do is aid not charity. Talking about me. We do
both on this channel. We do both. There's a difference between charity and aid and
this is going to become important later because that's a big piece of one of the
critiques. Charity, and we're gonna grossly oversimplify all of this, charity
is short-term. You are relieving the symptoms of an issue, okay? Aid is more
addressing root causes and providing the ability to affect self-help. Those people
in that situation can help themselves with what you're providing. For the
mutual assistance gurus out there, yes, I know, gross oversimplification but not
actually the point right now. The key thing here, charity, give a person a
fish. Aid, teach a person to fish. That's a good way to look at it. We do both. We
do both. Some of what we do with the shelters, which by the way are not just
women shelters. Some of what we do is charity, some of what we do is aid, some
of what we do is a really weird gray area that some people consider aid, some
people consider charity. And this goes to the profiting from them thing. People
aren't talking about him profiting in the sense that I think the way it's being
received by people who are hearing this. When we do the stuff for the shelters,
what do y'all see? On your end, what do you see? You see the supplies, right? You
see the stuff that we're providing. If it's around the holidays, you see the
gifts for the kids. You see shelter employees or organizers, right? Who do
you not see the people being helped. Now with the shelters there's a security
reason for that as well, but generally speaking it doesn't matter what we're
doing, it's very rare that you actually see the people that we're helping. Never
in the context of look at this person that needed help. That's just not how
we do it on this channel. There are people who view the way Beast does his
videos in that light and they view it as though because he's showing their image
like that he's profiting from them. The counter-argument to that is going to be
that if you know he was like hey we're gonna cover all the medical expenses to
let you see again, we're going to film it, that's going to generate revenue which we're
going to use for the next cool nice thing that we're going to do for somebody that the
people would say yes.
And that's where the confusion comes in.
The critique is that that's not how it should be done.
And the reason it's so poorly received by the people who hear this is because they've
view it through that lens of it being very transactional.
And then I think another thing that you see a lot of people want addressed, and this is
a big one, is every heartwarming thing that he does.
Again, nobody is denying that what he's doing is good, okay?
It's not a complaint from any reasonable person that I've seen, but every heartwarming thing
that he does, every good thing that he does is a symptom of a systemic failure.
And a lot of people want him to take that huge platform that he has and say, you know,
this is what we did and we had to do this because of this.
I get it.
It makes sense.
I mean, that's not his channel, though.
That's not what he does.
He is not, I don't look at him and think hardened activist, you know?
I don't look at him and think this is somebody like steeped in political theory.
To me, he's always struck me as kind of a goofy YouTuber who's trying to do some good.
I would suggest that that's a channel for somebody.
That is a whole channel that could exist, is taking not just his stuff, but every one
of those heartwarming stories that you see on the news.
You know the story about the kids who had the lemonade stand to help their teacher who
got cancer.
Yeah, that's a heartwarming story.
It's also a total indictment of our healthcare system.
That could be a channel for somebody.
could take that upon themselves to do that.
But that is one of the other critiques.
They think that while he's doing this, maybe he should mention why it has to be done.
Why countries that are rich in resources have so much poverty.
And that's part of it.
You have some people that are upset because he has a very capitalistic business model.
I mean, that's kind of undeniable, he does, but I would point out that I don't know that
Beast has ever come out and been like, hey, I'm a lefty.
It's like being upset that Bill Gates is using a capitalist model.
We exist in a capitalist system, that's how he's trying to do it.
That's one that I don't, I don't know that that one really makes a whole lot of sense
to me, but I've seen it, I understand what the argument is.
I'm just kind of like, well, yeah, I mean, it is what it is in the current system.
The idea that it's just for a tax break.
There's two things when it comes to that.
First, I don't know that a lot of people understand how that works at that level.
I mean, understand, if Beast owes a million dollars in taxes and he gives a million dollars
to a charity, he still owes like 900 grand in taxes.
It's not a one-for-one ratio.
So that doesn't make a whole lot of sense to me.
Aside from that, it always reminds me, when I hear statements like this, it reminds me
of this story that I was told in a restaurant
and it's basically this guy.
He says he wants to build an orphanage
and he's telling his religious leader that
and the religious leader's like,
great, yes, awesome, do that,
come see me on Tuesday or whatever.
And the guy comes back a couple days later
and he's like, you know, I was thinking about it
and I have the wrong motivation.
The only reason I wanted to build that orphanage
was because I wanted people to think that I was a great man and I would be
remembered as a great man after I'm gone.
And the religious leaders are like,
wait, you uh...
you're not going to do it now?
He's like, no, no, that's the wrong motivation.
And the religious leader asks,
you think the orphans care? If somebody is doing something that is objectively
good, even if they're doing it for the wrong reasons, I tend to just kind of
shrug. So a lot of it to me kind of goes to that. But then we have the one
criticism that, to me, is super valid. And again, it's not really a criticism, it's
a yes and moment. The people I have seen who have been making this particular critique,
they like what he's doing and want him to do better. And that is, they want him to stop
doing just charity and do aid because of the amount of cash, the amount of resources that
he has, the ability, he can actually really help address some root causes.
He can assist in big ways and they'd like to see that change.
And again, getting back to that, rather than talk about the shelters or the hotlines or
any of the other stuff that we do, think about the disaster relief stuff.
When we're doing that, yeah, there's food, there's water in the truck beds, but what's
really there that matters?
What do we focus on?
Generators, chainsaws, hand tools, tarps, roofing nails, the stuff people need to affect
self-help, to get back on their feet themselves.
He has the ability to do that on a massive scale.
And I think people would like to see that.
It goes to that interview.
Person in Africa talking and the people that send us bread and food, that's great, but
we need tractors.
We need pesticide.
The ability to address the problem themselves.
There are a lot of people who look at what he does and look at the amount of money involved
and realize that it could be shifted to something longer, to something that is more impactful.
Now the issue that he would run into is that his model, the way the revenue comes in is
based on those videos, and those videos require something dramatic, you know?
The infrastructure, providing infrastructure, providing aid like that, it's not dramatic.
Nobody wants to watch a video about somebody going in to a bank and setting up an endowment,
you know?
That's not, that doesn't grab attention.
So even if he was doing that, this other stuff, it wouldn't change.
Even if he decided to focus more on providing real aid, to provide long-term change in these
areas.
And most people who are adamant about this, they are particularly talking about the stuff
that he does outside of rich countries, because there, those dollars go a long way, and he
He can make a huge impact.
But nobody wants to watch somebody filling out bank forms.
That is not captivating YouTube.
So he would still end up doing the same stuff that he's doing now to get the revenue.
And so it would be like instead of 1,000 people, in this video he would do like 100, but then
set up an endowment so over the next 10 years, you know, 5,000 get done.
And it just creates a longer period of change.
And I think that that's the one critique that really holds water.
But again, I don't look at him and think hardened activist.
I have no idea if he has ever sat down with somebody and had a conversation like that.
I think that before people really go to criticize to the level that some of them did, maybe
just reach out.
I don't think the guy is opposed to learning.
To me, my read on it is that he actually does want to do good.
That's why I don't want to step all over what he does.
At the end of this, you have critiques that are really focused on wanting what he's
doing to be better.
They don't want him to stop.
They don't want people not to be able to see or kids not to have shoes or any of the
other stuff.
want it done better. Why all of it flared up at one time? I don't know. I don't know.
But just remember, people on the left aren't always good at expressing why they feel a
certain way. Don't let that push you away from the left. And there's always going to
be conflicting opinions, and there's always going to be that argumentation.
Just it, it's part of it.
Nobody, nobody, no reasonable person on the left is opposed to him helping a
thousand people see, okay? That's, that's not really a thing. It's, they, they want
the systemic issues addressed, they want root cause aid, they want... it's all
ideologically motivated stuff and they don't always do a good job of
explaining that ideology to people. And then at times there are moments where
they look at something and they're like, well that's capitalism. Well I mean yeah
we're in a capitalist system, that's how he's doing it. And again to my
knowledge he's never indicated that he's a leftist. So him using a capitalist
model shouldn't really be surprising. I think the reason it's surprising is
because a lot of what he does seems to be in line with what the left wants, but
I have no idea if that's really his position. So as far as the arguments and
the critiques that you're seeing, those are the ones that I'm aware of. Again, I'm
sure there's more, but those are the ones that I'm aware of. They are based in
ideology, and they're based in wanting it to be better, not wanting it to stop. The
left does not want people to stop helping people. That's not what it is.
So if you have these criticisms, particularly the one about aid, about
switching from charity to aid, I think that he would be pretty open to it.
I would reach out to him or to his staff because I don't see him as somebody who is just doing
this for money, who is just doing this for a tax write-off, I'm seeing it more as somebody
who was given a massive platform and is trying to do the best that they can, but may not
have all of the information they need to do it in the best possible way.
But I mean maybe I'm wrong about that, I don't know.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}