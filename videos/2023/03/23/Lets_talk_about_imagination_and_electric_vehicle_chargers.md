---
title: Let's talk about imagination and electric vehicle chargers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IJ9qMc60bzI) |
| Published | 2023/03/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the Overton window, which defines the range of acceptable beliefs in a particular area, often viewed in politics.
- Connects the idea of the Overton window to electric vehicles, showing how people's imaginations and ability to envision something different can impact technological advancements.
- Mentions Jeep's move towards electric vehicles, including the desire for a full electric Jeep that retains the essence of a traditional Jeep.
- Notes the common response to electric vehicles based on current limitations like travel distance and charging times.
- Challenges traditional thinking by proposing alternative locations for electric vehicle chargers, such as fast food restaurants, instead of just gas stations.
- Describes a scenario where charging an electric vehicle can be integrated into daily activities like dining out, shifting the perspective on charging time.
- Foresees a shift in infrastructure with roadside attractions, hotels, and other establishments likely to incorporate electric vehicle chargers.
- Emphasizes the changing landscape of energy sources and the need to question assumptions about how things have always been done.
- Encourages individuals to rethink their approach to solutions and daily routines, considering the potential for significant changes from small shifts in perspective.
- Raises awareness about the evolving infrastructure around electric vehicles and the importance of questioning the status quo.

### Quotes

- "it's one of those moments where you can see how the inability to think of something different prohibits the ability to think of something better."
- "Always ask yourself why are we doing it this way and do we have to."
- "Are you assuming that something has to be done the way it always has been?"
- "It's just a thought."
- "Y'all have a good day."

### Oneliner

Beau explains how the Overton window affects innovation, using electric vehicles as an example to challenge traditional thinking and envision a future with alternative charging infrastructure.

### Audience

Innovators, Electric Vehicle Enthusiasts

### On-the-ground actions from transcript

- Support businesses that install EV chargers (implied)
- Advocate for more diverse locations for EV chargers (implied)
- Question traditional practices in daily life and seek innovative solutions (implied)

### Whats missing in summary

The full transcript provides detailed insights on the impact of imagination and traditional thinking on technological advancements, encouraging individuals to challenge the status quo for a better future. 

### Tags

#Innovation #ElectricVehicles #OvertonWindow #ChargingInfrastructure #QuestionTradition


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about electric vehicles
and the Overton window and how those two things that
don't seem like they have anything to do with each other
kind of do.
In a recent video, we talked about how the Overton window,
which is the range of acceptable beliefs when it comes
to a given area, when it comes to politics,
that's generally how it's viewed.
We talked about how it's limited by people's imaginations,
their ability to imagine something better.
Jeep is, they're moving towards electric vehicles.
They already have a hybrid out.
And I have talked to my friends, and I can't wait,
I really want one, talking about a full electric Jeep that
actually acts like a real Jeep.
And the number one thing that people say in response to it
is, you can't do that.
You travel too much.
And sitting by a gas station for 30 minutes
while your car charges is going to drive you crazy.
And I mean, I have to admit that they're right.
But that's peer pressure, that's tradition,
doing things the way they've always been done.
There are now fast food restaurants
that are looking at putting in EV chargers, the fast chargers.
So rather than what people traditionally envision,
what they imagine, which is a gas station with
electric chargers outside, it's other things.
It can be done at other locations, like a restaurant.
So when you're on a long trip, rather than standing there at
pumps, which is how people imagine it, you pull up, you
plug your car in, you walk inside, you order your food,
you wait for your food, you sit down, you eat.
By the time you're done, it's probably pretty
close to charged, given that time frame.
So it's one of those moments where
you can see how the inability to think of something different
prohibits the ability to think of something better.
And this can be done with a whole lot of other things.
would imagine that roadside attractions, tourist traps, would probably also put
them in. I would imagine you would find them at hotels. It's going to alter the
infrastructure because it's a new type of energy. That's always been one of the
big hang-ups for people is the length of time it takes to charge. Now the length
the time it takes to charge one of these things it's dropping a lot and now you're
looking at 20 to 30 minutes in some cases. That's that that's a fast food
visit. So the infrastructure is going to change but one of the things that people
should keep in mind when they're talking about solutions even in their own lives
is are you are you assuming that something has to be done the way it
always has been and is there a reason to make that assumption because a small
change in how something happens well it can make a big difference you know there
are a lot of gas stations that exist today that have the convenience store
and have a fast food place in it. So I would imagine that that or other like
scenarios are going to take place. I don't imagine a situation where the gas
station as we picture it today is going to just have the pump switched out with
electric chargers the the infrastructure will change and looking at how your life
runs or other other issues that may be dear to your heart always ask
yourself why are we doing it this way and do we have to anyway it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}