---
title: Let's talk about the Ohio River, barges, and getting loose....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=X5bsdxB4Hzc) |
| Published | 2023/03/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A tug moving 11 barges had an incident on the Ohio River, where 10 barges got loose.
- Six of the loose barges have been recovered, while one was pinned against a pier and three against a dam.
- Most of the barges were carrying corn and soybeans, but one had 1,400 tons of methanol, a toxic substance.
- The barge carrying methanol is taking on water, with uncertainty if it's leaking, though early info suggests quick dilution if it does.
- Water intakes downstream have been informed, and notices will be issued about water safety.
- Recovery and normalizing the situation might take time, as seen in a similar 2018 incident that took months to resolve.
- Methanol vapor in a confined space is volatile, so caution is advised.
- Limited hard information is available on water quality, with ongoing testing to ensure safety.
- It's unclear what, if anything, has leaked, making it a wait-and-see situation.
- Beau urges people to stop polluting the river and mentions the Army Corps of Engineers working on a solution but acknowledges it may take time.

### Quotes

- "It'd be great if people could stop putting stuff into the river."
- "So at this point, there's not a lot of hard information about water quality."
- "Methanol in a confined space is, well, let's just say volatile."
- "Y'all have a good day."

### Oneliner

A barge incident on the Ohio River involving methanol leakage prompts caution and uncertainty about water safety, urging vigilance and local updates.

### Audience

Concerned residents near water bodies.

### On-the-ground actions from transcript

- Monitor local sources for updates on water quality (suggested).
- Stay informed about the situation and follow any issued notices regarding water safety (implied).
- Refrain from polluting rivers and water bodies (implied).
  
### What's missing in summary

Beau's engaging delivery and nuanced insights can be fully appreciated in the original transcript.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about news
from the Ohio River, barges and things getting loose,
and talk about what to expect.
OK, so early in the morning, there
was a tug that was moving 11 barges.
10 of them got loose.
Of those 10, six were free.
One wound up pinned against a pier.
And three wound up pinned against a dam.
Most of the barges were carrying things
like corn and soybeans.
One was carrying 1,400 tons of what the media
calling methanol a toxic substance. So at time of filming the one that was pinned
against the pier and the six that were kind of free have been recovered. The
three pinned against the dam are still there. There is one that appears to be
taking on water. Of course it's the one with the methanol. Now the substance is
toxic but at this point nobody knows if it's leaking. If it does leak early
information indicates that it will dilute relatively quickly. Keep in mind
that is early information, so do with it what you will. The water intakes
downstream have been informed and they are putting out notices about whether
or not the water is safe. Now as far as recovery and getting everything back to
normal, it might take some time. In 2018 there was a similar incident and it took
months to sort out. It's also probably going to be complicated by the fact that
the vapor from methanol in a confined space is, well, let's just say volatile.
Don't strike a match around it. So at this point there's not a lot of hard
information about water quality or anything like that. Most places are
saying that they're testing and the water is fine, but we also don't know
what has leaked, if anything. So when it comes to that, it's kind of a wait and see
thing. It would be great, just fantastic, if people could stop putting stuff into
to the river. That'd be great. So the Army Corps of Engineers is working on it and
trying to figure out a solution. It's probably going to take some time unless
they learned a lot from what happened in 2018 because that took a while. So that's
the information. If you're in the area, you're concerned about water quality, look to more local
sources and keep checking because it may change. But they are aware of the situation and according
to reporting they are monitoring. So anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}