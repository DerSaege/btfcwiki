---
title: Let's talk about Pence and the 5th Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JQ-lmkJpsp8) |
| Published | 2023/03/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special counsel wants Pence to talk about what he knows and has been reluctant to do so, fighting the process.
- Judge ruled that Pence must go before the grand jury, except for the time when he was President of the Senate during the events of the 6th.
- Special counsel is likely more interested in information leading up to the events of the 6th rather than on the 6th itself regarding Trump.
- Pence has the option to appeal the ruling but it may just cause delays.
- Pence appealing may not be a smart political move as it could be seen as him being compelled to testify.
- Special counsel's office may ask questions that Pence has already answered publicly to shape the idea that Trump tried to illegally subvert the election.

### Quotes

- "Pence has been reluctant to talk and has fought that whole process."
- "Pence can still appeal this."
- "It's Pence."
- "Special counsel's office is probably going to ask a lot of questions that Pence has already answered in public."
- "That's probably the route they're going to pursue."

### Oneliner

Special counsel wants Pence to talk but he's reluctant, ruling says he must testify except for his time as Senate President; appeal may lead to delays, politically risky for Pence.

### Audience

Legal analysts, political observers

### On-the-ground actions from transcript

- Wait for the legal process to unfold (implied)
- Stay informed about developments in the case (implied)

### Whats missing in summary

Insight into the potential legal implications and consequences of Pence's testimony.

### Tags

#Pence #SpecialCounsel #LegalProcess #Trump #ElectionInterference


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Pence
and the reported ruling that has come down.
So the special counsel wants Pence to talk,
wants to find out what Pence knows.
And Pence has been reluctant to talk
and has fought that whole process.
The judge has basically said you have to go before the grand jury, short version.
There are some exceptions.
The day of the 6th, it looks like the judge accepted the argument that at that point Vice
President Pence was actually President of the Senate and therefore is covered by speech
and debate by that clause in the Constitution.
So he doesn't have to answer questions pertaining to that specific period.
But for everything else, it looks like he's going to have to talk.
And it's really, my guess is the special counsel's office is more interested in stuff leading
up to the 6th, not the events of the 6th when it comes to Trump.
Now Pence can still appeal this.
We don't know if he's going to yet.
There are some who believe that he didn't really expect to win this and that this was
more of a show to kind of set the stage politically for him.
So it doesn't look like he ran to the Justice Department and told on Trump.
Help preserve that Republican base for him.
Okay now, one of the things that has come up, even though I don't really expect this
to come into play with pence a whole lot, it's a topic that has arisen when people
are compelled to talk before the grand jury.
You see it in the comments occasionally, people are like, well they're just going to plead
the fifth.
Yeah and then the next thing that happens, that is likely to happen, is for the prosecutor
to pick up their magic wand and say, cat's tails, newt's eyes, immunize, poof, you now
have immunity. If the person speaking before the grand jury has immunity, it
overcomes a lot of Fifth Amendment claims. So that is something that we're
likely to see. In fact, it's already happened with the DOJ investigation of
the documents, I think. So that the Fifth Amendment thing probably not going to be
as big a deal as a lot of people seem to think it's going to be. Now we'll have to
wait a little bit for Pence's lawyers to decide whether or not him appealing is
good. I don't think an appeal is going to make much difference as far as the
outcome, but it may be yet another delay. I think for Pence politically it might
be better to not appeal it because he's shown that he was being forced to compel, or he
was compelled to testify.
He's done that for his base.
But it would be better politically if anything that was going to happen as far as things
coming about for Trump occurred sooner rather than later.
So I'm not sure if he's going to appeal.
To me, it doesn't seem like a smart political move, but it's Pence.
So the special counsel's office is probably going to ask a lot of questions that Pence
has already answered in public or in a book or whatever, and just get them on the record
to help basically shape the idea that the former president, that Trump, was trying to
get him to illegally subvert the election.
That's probably the route they're going to pursue.
And we'll just have to wait and see how it plays out.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}