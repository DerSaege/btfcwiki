---
title: Let's talk about the Trump NY case this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WhEHzEtKiCw) |
| Published | 2023/03/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump claimed New York dropped the case against him, but there's no evidence to support this.
- The grand jury is still meeting, and it's unlikely that Trump will be indicted this week.
- There are various theories about the delay in the case, including normal grand jury processes, slow-walking by the DA, or uncovering new evidence.
- NYPD is no longer requiring officers to show up in uniform, possibly due to a lack of anticipated crowds.
- The situation regarding Trump's case remains uncertain, with grand jury processes being secret.
- Congressional attention suggests that the case is likely still moving forward.
- Three theories exist: normal proceedings, intentional slow progress by the DA, or the discovery of new information.
- Without a vote from the grand jury, the outcome remains unknown.

### Quotes

- "We don't really know anything."
- "The grand jury is still meeting."
- "We're probably not going to know anything until they vote."

### Oneliner

Trump claims case dropped, but grand jury still meeting and uncertainty looms with NYPD uniform changes.

### Audience

Journalists, concerned citizens

### On-the-ground actions from transcript

- Monitor updates on the case (suggested)
- Stay informed about developments (suggested)
- Engage with local news sources for updates (suggested)

### Whats missing in summary

More details on the potential implications of the case and the importance of staying informed.

### Tags

#Trump #NewYork #NYPD #GrandJury #CaseUncertainty


## Transcript
Well, howdy there, you don't know people, it's Bill again.
So today we are going to talk about Trump in New York and what is going on up there,
what's happening and what is not happening.
And we're going to do this because I got a message that I found pretty entertaining
myself and we'll just kind of go through it all.
Okay.
So did New York really drop the case?
Trump said they had dropped it.
and it's gone complete silence. And NYPD isn't requiring people to show up in uniform anymore.
If you have no idea what that last part is about, NYPD had all of their cops, even like normal plainclothes people,
showing up in uniform in case they had to deal with crowds. People upset that Trump was arrested.
arrested. Okay, so here's the problem with your message. Trump said, and you're
putting some stock in it, remember Trump said he'd be arrested like last Tuesday
or something, his statements on this mean absolutely nothing. The grand jury is
still meeting, okay? That much we know. We're also fairly certain that he will
not be indicted this week based on what the grand jury will be meeting about
and stuff like that. It's just not gonna happen this week. Will it happen later?
Don't know. Don't know. There are a bunch of different theories. One is that they
They are still proceeding with a normal grand jury process, still looking into it, still
hearing more evidence, and the vote hasn't taken place yet.
There is a theory that the DA is slow-walking it for whatever reason.
There is another theory that says the reason they're bringing in more people is because
they uncovered something else.
know, have no idea. Now, as far as NYPD not requiring their people to show up in
uniforms anymore, there could be a number of reasons for that. One, they could have
talked to the DA and the DA said, yeah, it's not happening this week. Two, it could have
been them realizing that the crowds aren't gonna be that big. You know, when
When news broke that this was going to happen and Trump really put it out there and called
for people to show up, there were not crowds that NYPD was going to be concerned about.
So that may have something to do with it there.
Now maybe that changes, but honestly after the turnout from the initial set of events,
I doubt NYPD is going to need their detectives to don crowd control gear.
I don't think that's likely, no matter what happens.
The normal cops will probably be able to handle that.
The short of this is we don't really know anything.
Grand jury processes are secret.
We're not supposed to know anything.
Now as far as the whether or not it's moving forward, I would suggest that if it wasn't
after the attempts at getting involved from Congress, I would guess that the DA probably
would have made an announcement if they weren't going to move forward because otherwise just
taking heat for no reason.
I think that is your strongest evidence, as weird as that is.
That's probably your strongest evidence that it is continuing to move forward.
Because with attention from all over the country, and particularly the stuff from Congress,
if they weren't going to move forward with it, I imagine they would announce it.
But there are the three theories out there.
it's just normal, everything is going the way it should, that the DA is
intentionally moving slow for whatever reason, and that they have uncovered yet
some other thing that they want to look into. So until they vote, we're probably
not going to know anything. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}