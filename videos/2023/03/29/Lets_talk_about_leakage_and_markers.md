---
title: Let's talk about leakage and markers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=v8015ZmWf3s) |
| Published | 2023/03/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing a public service announcement on markers and leakage and their importance in preventing mass incidents.
- Explaining the concept of markers as common occurrences related to the person responsible for such incidents, often involving multiple stressors.
- Emphasizing the prevalence of domestic violence histories and cruelty to animals as common markers.
- Introducing the concept of leakage, where individuals fantasize, plan, and drop hints about harmful actions before carrying them out.
- Mentioning that over half of individuals engaged in leakage, with researchers confirming approximately 56% of cases.
- Describing leakage as a behavior where individuals may intentionally or subconsciously hint at their harmful intentions.
- Urging people to take hints seriously, especially in settings like schools, to prevent potential incidents.
- Stressing the importance of believing individuals when they hint at harmful actions and not dismissing their statements.
- Encouraging understanding and action based on early research to prevent harmful incidents.
- Recognizing the significance of listening and acting on information regarding potential harm, even if it may not make headlines.

### Quotes

- "Believe them. Believe them."
- "That's one of those steps towards prevention."
- "Listen when people say that, you know, they're gonna do it."
- "Leakage, that's the term."
- "Y'all have a good day."

### Oneliner

Beau provides vital information on markers and leakage, stressing the importance of recognizing and acting on hints to prevent potential harm effectively.

### Audience

Preventive individuals or communities.

### On-the-ground actions from transcript

- Believe individuals when they hint at harmful actions (suggested).
- Take hints seriously, especially in sensitive environments like schools (suggested).
- Pay attention to potential markers of harmful behavior and act accordingly (implied).

### Whats missing in summary

The importance of early prevention strategies and the need for increased awareness and action to address potential harm effectively.

### Tags

#Prevention #Markers #Leakage #CommunitySafety #Action


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to do a little public service
announcement about markers and leakage.
We're going to talk about leaky markers.
And hopefully, it's information that, as it spreads,
will do some good in regards to prevention.
Yesterday in that video, I mentioned markers,
and I got a bunch of questions about it.
We'll talk about a few real quickly,
and then we are going to focus on a concept, a theory called
leakage, and go into that.
OK.
So when you are talking about stuff like this,
when you're talking about mass incidents, you have markers.
You have things that are common occurrences when it comes to the person responsible.
And a lot of them are really kind of like, well yeah, of course that would be the case.
Multiple stressors hitting them at one time.
That's one that's been identified.
They could be economic, they could be personal, but multiple things at one time.
It isn't one thing that causes it to happen.
typically a bunch of stressors at once. There is something I've talked about on
the channel repeatedly, an incredibly high prevalence of people that have
domestic violence histories and being mean to animals. All of these are very
common markers and there's more but that's when people use that term that's
That's what they're talking about.
But leakage is one.
Leakage is a concept that people have been talking about.
There's a lot of research behind it.
More than half, more than half engaged in it, and that's just that they were able to
confirm.
The number is actually probably much higher, but I want to say it's 56%.
They were able to confirm, researchers were able to confirm that they engaged in this
behavior.
And basically what it is, these people, they fantasize about it.
They imagine it.
They play it over in their mind over and over and over again before they ever do it.
And they will often tell people that they're going to do it.
They will drop hints.
They will engage in behavior that is very clear.
And there's debate about whether or not it's something they're doing intentionally, like
basically, hey, please stop me, or it's a subconscious thing.
And I think it's probably a blend of both, but I'll leave that to the experts to decide.
These hints, they come along.
And it could be anything from literally telling people, hey, I'm going to do this bad thing
here, in a really frustrated tone, and people blow it off.
Or it could be drawing a map and leaving it where somebody might see it, leakage.
And it's based on what seems like kind of early research into it, there's a whole lot
of connections.
So this is one probably that people should pay attention to, particularly if you work
a school. If somebody says something like that, believe them. Believe them. And there's
already the belief that the person in Nashville engaged in this. And the
messages to the friend basically saying, hey, this is happening today. This is
information that has fallen into the it's a please-stop-me type of behavior,
but again you don't really know that yet. It could just be announcing their
departure, but this is information that needs to get out. This is information
that people need to know about. That way they don't blow it off. That way
they understand that this kind of behavior, particularly if there are other
markers or I don't know they're going around and buying a bunch of stuff, that
this should be a sign. This should be something that people take seriously.
They shouldn't just say, oh that's somebody talking. It needs to be taken
seriously. That's one of those steps towards prevention. There's a lot of
research going on to determine how to stop this before it starts. The thing is
something like that, that's not headline grabbing. Listen when people say that, you
know, they're gonna do it. Nobody's gonna run a story about that. Not many. It's
certainly not gonna be from a page. People aren't gonna know about it. And
that research leads to people understanding, which leads to people
acting when they hear something, which can lead to prevention. So leakage, that's
the term. And I have a feeling we're gonna hear more and more about this term
as the months go on. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}