---
title: Let's talk about things to remember from 2016....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5unv6WZ6HRw) |
| Published | 2023/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reflections on lessons learned and moving forward from 2016, as similar dynamics are playing out in the lead-up to 2024.
- Far-right targeting specific demographics through legislation, rhetoric, and actions, with rhetoric playing a critical role in legitimizing their agenda.
- A technique used by the far-right is saying something extreme, then walking it back to a slightly less extreme position to make critics seem unreasonable.
- Normalization of extreme positions through this technique, making it critical to challenge and question their rhetoric.
- A strategy to deal with racist jokes or extreme rhetoric is to pretend not to understand, forcing the person to explain and reveal the true nature of their statement.
- By questioning and getting details on extreme positions, support can be eroded as people become less interested and supportive.
- Connecting extreme statements to potential violations of the Constitution can be a powerful way to challenge and counter their narrative.
- Emphasizing the impact on constitutional rights, especially the First and Second Amendments, can help appeal to conservatives who value constitutional principles.
- Continuously questioning and dragging out extreme rhetoric is key to exposing the true intentions behind seemingly toned-down statements.
- The importance of pushing back, challenging, and unraveling extreme rhetoric to prevent its normalization and gain broader support against such tactics.

### Quotes

"Pretend like you don't get it."
"If you can't be trusted with the First Amendment, why should we trust you with the Second?"
"People don't want anything to do with it."

### Oneliner

Lessons from 2016: Challenge extreme rhetoric by continuously questioning and exposing the true intentions behind seemingly toned-down statements.

### Audience

Activists, Progressives, Concerned Citizens

### On-the-ground actions from transcript
- Challenge extreme rhetoric by continuously questioning and exposing the true intentions (suggested)
- Connect extreme statements to potential violations of the Constitution to counter the narrative (implied)
- Appeal to conservatives by emphasizing the impact on constitutional rights, especially the First and Second Amendments (implied)

### Whats missing in summary

In-depth analysis and examples from the full transcript can provide a more comprehensive understanding of how to effectively counter extreme rhetoric and tactics.

### Tags

#LessonsLearned #ExtremeRhetoric #ConstitutionalRights #Challenge #FarRight


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about lessons learned
and moving the conversation forward
and things that we should have learned
and should have remembered from 2016.
Because things are cycling back around
and we're seeing a very similar dynamic play out
in the run-up to 2024.
It looks like a lot of the same techniques and tactics
are going to be at play.
And we have to remember how to deal with them before,
how we dealt with them before.
And there are people counting on it.
OK, so what's the situation?
The far right in the country has decided
that there are a couple of demographics
that they're going to go after via legislation,
via rhetoric, and via action.
And that rhetoric is super critical to legitimizing
everything else.
Sounds familiar.
what happened in 2016. So what happened? 2016? I didn't figure it out. We don't
really know how to respond to it as a country. As time went on, we did. It became
more and more obvious because the technique that the far-right was using,
well, it became more transparent. What happens? They say something wild, just
completely out of line, right? And then the outrage ensues. You can't say that.
And then what do they say? Oh, that's not what I meant. I didn't say that. That's
not what I meant. I said this other thing, which is still like way out of line, but
it's a lesser position than what you're saying. Therefore, you're the
unreasonable one. And I'm okay. I'm the good guy here. Because you, you said this
thing about me, and that's not really what I said, right?
And what happens in the process there is that that lesser position, that thing that they
admit to that isn't as bad as what it sounded like, that position gets normalized.
So what do you have to do?
There are one of the more interesting ways to deal with somebody telling a racist joke
around you when you're not really capable of dealing with it the way you might want
to is to pretend like you don't get it.
You might have heard this before.
You pretend like you don't get it.
I'm sorry, what?
I don't understand.
I don't get it.
Explain it to me.
Because what happens is when you have to explain the joke, it's suddenly not funny, right?
You realize how horrible it is.
And those people, they won't talk like that around you anymore.
The same technique applies.
This rhetoric, when it gets explained, it's not palatable to most people to include most
conservatives.
So when they say wild position X, everybody gets mad, and they say, oh no, I really meant
wild position Y, this is the moment where you have to be like, OK, well, I don't understand
wild position Y. How does that work?
How would we go about doing that?
How is this going to play out?
What kind of laws would it take?
And at this point, regardless of the demographic that they're going after, their answer is
going to be an infringement on the First Amendment. So, jump right into that.
I'm sorry, wait, do you not support the Constitution? I've got a question. Do you
not understand what shall make no law means? You know what? If you can't be
trusted with the First Amendment, why should we trust you with the Second? And
I know somebody right now is going like, I don't really care about the Second,
That's not the point.
The point is, the people that are going to hear you, they will.
Pretend like you don't get it.
Get them to explain it because the reality is that their rhetoric is based on sound bites.
It only sounds good in little bits and pieces.
When it gets explained, people don't want anything to do with it.
That's what happened with Trump on a wider scale.
His soundbites, people were like, yeah, let's make America great again.
He gets elected.
And people are like, wow, this is not good.
And he didn't get reelected.
Same premise.
You have to drag it out, though.
You have to drag it out.
You have to be ready to respond to it.
And then when they do, the only way that a lot of this stuff, a lot of the rhetoric and
the sound bites they're using, the only way that they can get it is to violate the Constitution.
Point that out.
A lot of conservatives, they, a decent amount of conservatives still actually care about
Constitution, and you will be able to get through to some of them. And then from
there, once you are talking about the Constitution, your answer that they're
going to give you, almost undoubtedly, is going to be some variation of, well, we
have to protect the children from whatever group it is. Because again, there
are three different dynamics or three different demographics right now that I
can think of that are being hit like this.
But that's going to be their response, at which point the one thing that all conservatives
are going to understand is how that gets applied to the Second Amendment.
If you can violate the First Amendment to help the children, what about the Second Amendment?
Isn't that true?
Is that really what you're getting at?
And you have to keep drawing it back to that.
The key point here is when they make their crazy statement and then they walk it back
to something a little bit more palatable but far more extreme than most people understand,
you can't just stop.
You have to keep questioning it.
You have to keep dragging it out and get them to explain what they actually mean.
Step by step, where does it go from there?
Not your sound bite.
How does this actually work?
Because the people, when they hear how this works, they become less interested in it.
They become less supportive of it.
It worked throughout the Trump presidency.
That tactic of actually bringing it out and getting the details on it, it eroded support.
You're going to have to do the same thing here.
And there are people counting on your ability to do this.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}