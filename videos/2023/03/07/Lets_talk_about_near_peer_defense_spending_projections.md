---
title: Let's talk about near peer defense spending projections....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6JPLiwLsMg4) |
| Published | 2023/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- China's recent defense budget increase by 7.2% may not be substantial enough to trigger a massive increase in U.S. defense spending.
- The increase brings China's defense spending to about $230 billion annually, compared to the U.S. spending of around $800 billion.
- The U.S. doctrine since World War II aims to be able to fight its two largest competitor countries simultaneously, leading to higher defense spending.
- Chinese defense spending, at 7.2% of their GDP, still falls short compared to the U.S. spending, which is at 3.5% of its GDP.
- Beau suggests that U.S. politicians should avoid exaggerating China's defense budget increase to prevent a potential arms race.
- An arms race is detrimental not only from a military perspective but also economically and in terms of wasted resources.
- Beau warns against overcorrecting the U.S. defense budget due to the situation in Ukraine, as it may inadvertently signal to China and reignite the arms race.
- The hope is that both China and the United States are signaling a desire to avoid an arms race by not significantly increasing defense spending.
- The upcoming U.S. defense budget will provide clarity on whether both countries are committed to avoiding an arms race.
- The key is to prevent any actions that might lead to an escalation of defense spending and consequently an arms race.

### Quotes

- "Arms races are bad."
- "Y'all have a good day."

### Oneliner

Beau analyzes China's modest defense budget increase, questioning its impact on triggering a significant rise in U.S. defense spending and advocating for avoiding an arms race.

### Audience

Policy analysts, decision-makers

### On-the-ground actions from transcript

- Monitor and advocate for responsible defense budget decisions (suggested)
- Stay informed about international defense spending trends (suggested)

### Whats missing in summary

Insights on the potential consequences of misinterpreting defense budget increases and the importance of maintaining stability in defense spending to avoid an arms race.

### Tags

#China #UnitedStates #DefenseSpending #ArmsRace #PolicyAnalysis


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about China and the United States and
numbers that came out that were supposed to give us some insight into what was going to happen
that kind of didn't. They didn't really provide a lot of insight
because there's still two ways the
politicians in the United States can play it.
In a recent video, we talked about how the United States
States might be gearing up for a jog. You had people in the Senate, specifically
Mitch McConnell, talk about US defense spending and talking about maybe needing
to increase it and we talked about the term pacing and how that was going to
come up. One of the big concerns was the Chinese defense budget. What
China was going to spend, because US politicians would use that as a
justification to spend more or well not going to spend less but use it as a
justification to spend more. China's numbers came out. 7.2% increase. That
sounds like a lot, it's really not. That keeps it on par with all of the other
numbers that have historically existed. It's enough to cover like
inflation and a little bit of modernization, it's not a huge increase.
Probably shouldn't be enough to trigger a massive U.S. increase, which is our concern
at this point.
As we move into a near-peer contest, we don't want an arms race.
Arms races are bad.
So this increase brings Chinese defense spending up to about $230 billion a year.
Compare that to the United States at about $800 billion a year.
It is worth remembering U.S. doctrine since World War II.
The idea is to be able to fight our two largest competitor countries at the same time.
That's why U.S. defense spending is so much.
7.2% increase by the Chinese does not.
It does not kind of break that rule.
doesn't violate that doctrine. It doesn't put the U.S. in a position where it
should feel like it's not going to be able to do that. Looking at the raw
numbers, 230 billion to 800 billion. Another common metric that gets used to
determine defense spending is a percentage of the GDP, is a share of the
GDP. The increase would put Chinese defense spending at about 1.7% of the
GDP compared to the United States at 3.5% of the GDP. Again, right there where US
doctrine kind of says it should be. So this increase, if politicians in the
United States are honest, it shouldn't trigger a massive defense increase.
It shouldn't trigger massive defense spending increases in the United States.
This isn't a huge increase.
Now, they could come out and they frame it differently and say, you know, China raised
their defense budget almost 10% or something like that, and they could try to scare people.
I would hope they don't do that because these numbers, they're pretty flat.
And if the United States engages in a massive increase in defense spending, China may read
that and the whole situation may be reversed.
Whereas before, we were looking at China's numbers to see what the US would do.
If the US numbers increase a lot, China might increase theirs.
And then we still end up in that arms jog, that pacing event, which is bad.
It's bad for both countries.
It's bad for the entire world, not just from the war aspect, but from the economic aspect
to the wasted resources aspect, all of the problems that could be solved doing other
things aspect.
Arms races are bad.
So the number that we were looking to get some insight, which was the increase in Chinese
defense spending, it wasn't huge.
It wasn't huge.
If it was huge, we could say, yeah, the US is going to have a marked increase in defense
spending.
We could say that.
instance of not, there's not a lot of insight to be gained, especially when there is the
situation in Ukraine going on and the United States sending a bunch of equipment there.
The real hope is that the US doesn't overcorrect on the Ukrainian side of things and raise
the budget so much there that is legitimately designed to produce equipment for Ukraine,
And it signaled to China that that equipment might be for them because that would increase
Chinese defense spending and we're back to the arms race.
So we're going to have to wait and see what the next US defense budget is.
If it's running flat or just a slight increase to keep up with inflation or whatever, we
have a clear signal that at least at the beginning of this, China and the United States are kind
of signaling to each other that they don't want to engage in an arms race, which would
be ideal.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}