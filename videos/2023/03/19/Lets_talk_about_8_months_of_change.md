---
title: Let's talk about 8 months of change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ct_-dsJMfRk) |
| Published | 2023/03/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Billy Young says:

- A person shares their journey of change over eight months, prompted by meeting a trans woman at work.
- They were initially conservative and uncomfortable with the LGBTQ+ community, but their perspectives have evolved.
- The person describes their positive interactions with the trans woman, who they find feminine and kind.
- Despite this progress, they struggle to view the trans woman's friends in the same light.
- The person seeks advice on how to overcome this mental block and continue growing.
- Billy Young reassures them that change is a process and commends their progress.
- He encourages continued learning and self-reflection to combat internal biases.
- The person is reminded that their transformation signifies growth and should be embraced.
- Billy Young explains that passing privilege may influence the ease of accepting the trans woman they know well.
- He acknowledges the person's journey from holding prejudiced beliefs to actively seeking understanding.
- Growth involves realizing that a person's appearance does not solely define their gender.
- Billy Young praises the person's willingness to learn and adapt, showcasing their commitment to change.
- The person is assured that their struggle is normal and indicative of ongoing progress.
- Despite feeling challenged by accepting others, the person is encouraged to keep moving forward in their journey.
- Billy Young suggests that their relationship with the trans woman influences their ability to accept her friends.

### Quotes

- "If you are the same person you were five years ago, you've wasted five years of your life."
- "You're not a horrible person. You are a person mid-change."
- "You went from kind of a bigot to being so concerned about something you thought, not even something you did or said, but something you thought that you sent this message."
- "Change takes time. You're not going to get everything right, right away, but you'll keep moving forward."
- "Just the fact that you want to learn more is a pretty good indication that you're going to be successful."

### Oneliner

A journey of personal growth from prejudice to acceptance, guided by interactions with a trans woman and her friends.

### Audience

Individuals seeking guidance on personal growth and overcoming biases.

### On-the-ground actions from transcript

- Keep learning and allowing yourself to be educated (implied).
- Continue broadening your perspectives and challenging internal biases (implied).
- Embrace change as a gradual process and be patient with yourself (implied).

### Whats missing in summary

The full transcript delves into the nuances of personal growth and acceptance, offering valuable insights into overcoming biases through genuine interactions and self-reflection.

### Tags

#PersonalGrowth #Acceptance #LGBTQ+ #Learning #Bias #Community


## Transcript
Well, howdy there, internet people, it's Billy Young.
So today we are going to talk about eight months of change.
Changing as a person over eight months
and that process and a woman who is, quote, southern sweet.
And we're gonna do this because I got a message.
It's a long one, but I'm gonna read the whole thing
because I think there's probably a lot in this
that a whole lot of people could could stand to hear. And there's two questions
and two answers. Okay, so please answer this because I don't have anybody else I
can ask. I'm 22. I grew up very conservative. Then I moved to a big
city from my small town when I was 19. Eight months ago a trans woman started
where I work. I was always taught anything gay was horrible, and to be honest, trans
people grossed me out. At least the idea of one, because I had never actually met one.
She was nice to me, so I was nice to her. I called her her, and after about a week it
became really easy to do. She's very feminine, southern sweet, and she's pretty. We started
hanging out outside of work.
Recently, I went to a gay club with her and met some of her friends.
With her, it was easy to see her as a woman because everything about her is feminine.
I didn't see her friends that way.
I kept thinking of them as men who were trans rather than women.
Obviously, I didn't say anything like this to them.
I've learned from the woman he works with how hard it is for them and wasn't going
to make it harder, even danced with one of them, but it was in the back of my mind.
I remember you saying I said what I said when somebody told you to say trans woman instead
of woman.
I thought I should ask you because anybody I know who I could answer knows them.
Am I a horrible person?
I don't see why it's so easy with the woman he works with and so hard with them.
I wasn't uncomfortable around them, but just didn't see them the same way I see her.
How do I get past this?
Okay.
So what's happening is you are changing.
You're changing as a person.
You're changing.
You're growing.
You are broadening your horizons.
You're opening your mind.
You are becoming better, kinder, all of that stuff.
It's a process.
It's not an event.
It's not a thing that just occurs all at once and you have everything figured out.
It is a process.
If you are the same person you were five years ago, you've wasted five years of your life.
You'll continue to change.
As far as how you get past it, keep doing what you're doing, keep learning, keep allowing
yourself to be educated. Let's be real clear about something. You went from anything gay
is bad to hanging out in a gay club and dancing with trans people in eight months. I'm going
to suggest the route you're on is pretty effective. What you are probably dealing with is passing.
probably what you're dealing with and this is a it's a whole conversation to
include whether or not that's even a good term to use to describe it but if
you're going to look into it that's probably the term you you want to look
up, passing or passing privilege.
Your friend, the woman who works with you, she's pretty, southern sweet.
So it's easier for a 22-year-old man who is probably very accustomed to objectifying
women to say woman when they kind of meet your form, so to speak.
Eventually as you continue to grow and continue to change, you're going to realize that a
woman isn't a woman simply because you find her attractive and that there's a whole lot
more to this than just meeting a traditional appearance stereotype.
And you will definitely figure that out.
You're on the right path.
You're not a horrible person.
You are a person mid-change.
this message and trying to figure out how to get past it is a clear sign that you're
not trying to hang on to old ideas or anything like that.
You're trying to move forward and change takes time.
I mean, you went from kind of a bigot to being so concerned about something you thought,
not even something you did or said, but something you thought that you sent this message.
You're going to be fine.
You know, you say that she's told you how hard it is and you've obviously learned.
will continue to learn. Don't second-guess yourself too much on this one. First time
you met them, there's a whole lot that you're going to learn along the way. Now, as far
as the other question, why is it so easy with her and so hard with them, you know the answer
to that. You definitely know the answer to that. You're talking about a woman who is
southern sweet and pretty who you hang out with outside of work and want to make sure
you make a good impression on her friends and who seems to be the catalyst for a pretty
big change in your life. Generally speaking, 22-year-old men do not send
messages like this about a buddy. I mean, the only woman who has ever successfully
changed me other than my mother I married. There's probably that lens that you're looking
through and I don't know if you just don't want to say it here or what, but I do not
imagine that I would ever get a message like this from somebody who was really
just friends with the young woman who is southern sweet. That's probably why it's
easier. You know, you like her. Change takes time. You're not going to get
everything right, right away, but you'll keep moving forward. It's a process.
You're going to learn more and just the fact that you want to learn more is a
pretty good indication that you're going to be successful. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}