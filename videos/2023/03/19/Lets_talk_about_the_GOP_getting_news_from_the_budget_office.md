---
title: Let's talk about the GOP getting news from the budget office....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EM6UABFagK8) |
| Published | 2023/03/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- House Republicans face challenges in balancing the budget without touching politically sensitive programs like Social Security and Medicare.
- The Congressional Budget Office reveals that House Republicans must cut 86% of the budget, excluding key programs, to achieve budget balance in the next 10 years.
- If Republicans maintain Trump-era tax cuts, they must slash 100% of the remaining budget to balance it within a decade.
- Beau predicts that Republicans may resort to sabotaging their own proposal and blame it on Biden due to the unfeasibility of their budget plans.
- Despite entertaining political maneuvers, the reality is that serious budget cuts or increased taxes on the wealthy are necessary to address the U.S.'s financial issues.
- Beau suggests that those nostalgic for the 1950s should also revisit the tax rates of that era for insights into potential solutions.

### Quotes

- "House Republicans have put a lot of political capital behind the idea that they are going to balance the budget within the next 10 years."
- "86% means all of it, all of the budget across the board."
- "Their tactics are going to change on this issue when confronted with this information."
- "The U.S. does have an issue. There are two ways to deal with it."
- "We could tax wealthy companies and people a little bit more."

### Oneliner

House Republicans face the daunting task of cutting 86% of the budget to balance it in 10 years, potentially leading to a shift in tactics or blaming Biden, prompting considerations for tax increases on the wealthy.

### Audience

Budget Analysts, Politicians

### On-the-ground actions from transcript

- Advocate for fair tax policies targeting wealthy individuals and corporations to alleviate budgetary strains (implied).

### Whats missing in summary

In-depth analysis of the potential impacts on various sectors and populations from implementing drastic budget cuts or tax increases.

### Tags

#BudgetCuts #TaxPolicy #HouseRepublicans #FinancialIssues #WealthTax #PoliticalStrategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about how the CBO,
the Congressional Budget Office,
has some news for House Republicans.
House Republicans have put a lot of political capital
behind the idea that they are going to
engage in budgetary cuts that will balance the budget
within the next 10 years. Since this has come out I have said in
a number of videos that I don't think that's gonna work out the way they think
it has
and that if they're going to stick to
their idea when it comes to not touching Social Security, not touching
defense and veterans programs and Medicare and things that are just
not politically tenable to cut that they're gonna have a really hard time
finding where the budget can be cut so the Congressional Budget Office has put
out some information for for those Republicans in order to balance the
budget in the next 10 years and not touch things like discretionary defense
spending mandatory vet programs Social Security Medicare so on and so
forth they have to cut 86% of the rest of the budget and that's assuming they
get rid of the Trump-era tax cuts as well. 86%. Now I know most people are
thinking well you know they're Republicans they don't care they'll cut
education they'll they'll cut environmental protections and that's
That's true, but 86% means all of it, all of the budget across the board.
That means money for cops, agriculture, border security, their own budgets, 86%.
Now if they choose to leave the Trump-era tax cuts in place, which they've kind of
have promised to do, at least they're
promising to make the attempt, they
could cut 100% of the rest of the budget
and still not get to a balanced budget in 10 years.
I have a feeling that their tactics
are going to change on this issue when confronted
with this information.
Honestly, they've talked themselves into a corner.
I think the only thing they can really do at this point,
realistically, is sabotage and destroy their own proposal
and blame it on Biden.
That's about all they've got.
This isn't going to move forward.
Even if they go back on taking cuts from programs
that are politically untenable, like discretionary defense,
some mandatory veterans programs, social security, Medicare, stuff like that.
They still have to cut around half the budget.
I don't think this is going to go down the way they think it is.
While all of this is fun, it is entertaining that the numbers don't care about their alternate facts.
It does highlight something.
The U.S. does have an issue.
The U.S. has an issue.
There are two ways to deal with it.
One is over time a bunch of serious cuts could be made.
The other is we could tax wealthy companies and people a little bit more.
That might be a solution, I know nobody really wants to hear that.
I would point out that the people who lean into the whole make America great again thing,
They tend to point to the 50s as the ideal time, you know, disregarding how horrible
it actually was and focusing just on what they learned about it from Leave It to Beaver.
But I would suggest they look at the tax rights during America's golden age from their perspective.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}