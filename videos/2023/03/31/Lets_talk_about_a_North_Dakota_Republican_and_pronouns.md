---
title: Let's talk about a North Dakota Republican and pronouns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=h3FIUgOpfjc) |
| Published | 2023/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican governor of North Dakota vetoed a bill to stop schools from using pronouns.
- Governor's image is that of a small government conservative.
- Governor's veto was based on principle of limiting state overreach.
- Vetoed a bill in 2021 regarding trans students playing sports in school.
- Concerning news: state legislature wants to override the veto.
- Faction of small government conservatives or anti-authoritarian right supporting trans people.
- Split on the right regarding trans people owning guns.
- Some Republicans standing by their principles amidst the authoritarian sweep in the party.
- Expect this principled group to become louder as MAGA and Trump influence wanes.
- Anticipates this group to regain lost power within the Republican Party.

### Quotes

- "It's not inconceivable, it's just not something you see very often."
- "Those who actually stood by their principles, I have a feeling you're going to see them get really loud soon."
- "Expect to see more of this in various ways."

### Oneliner

Republican governor of North Dakota vetoed bill on pronouns, revealing a principled stand against state overreach amidst a split within the conservative movement supporting trans rights.

### Audience

Conservative activists

### On-the-ground actions from transcript

- Contact local representatives to voice support for the governor's veto (suggested).
- Join small government conservative or anti-authoritarian groups advocating for limited state overreach (implied).
- Organize events to raise awareness on the importance of standing by principles within political movements (exemplified).

### Whats missing in summary

The full transcript provides deeper insights into the evolving dynamics of conservative factions and their stances on issues like trans rights and state overreach.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about North Dakota
and a Republican governor and a veto
and a bunch of confusion and pronouns because something
happened in North Dakota that surprised a lot of people.
And there's a lot of people who are just learning
that a certain group actually exists in real life.
And it's an interesting development.
So here's the question.
Can you explain what just happened in North Dakota?
A Republican governor just vetoed a bill
to stop schools from using pronouns.
It's talking about students' pronouns,
and it's actually much wider than just schools.
But the main focus was schools.
Is this the beginning of Republicans waking up?
It's inconceivable to me that they finally come to their senses.
Okay. Is this the beginning of Republicans waking up?
No, definitely not. No. What you need to do is think about the
American left. Most people when they think about what is widely termed as
American left, they understand it's a bunch of factions. It's not as cohesive as the American
right seems, but in reality the American right has factions as well. Oftentimes those factions
they overlap so their differences aren't as pronounced. On this channel I have used the
term small government conservative and normally when I say it there's a bunch
of people in the comments saying stuff like those people don't actually exist.
They do exist there's just like eight of them you know there's not a lot of them
more than eight but there aren't a lot of them and there are even fewer in
politics because they're small government conservatives they tend to
not run. Now, as far as the governor of North Dakota, his image is that of a small government
conservative. I'm saying image, not necessarily because I doubt it, but more because I'm not
actually familiar with his entire record. But the PR that he puts out is very much in
line with being a small government conservative. A small government
conservative may as a matter of course just dislike trans people but this is
not the government's business and that's the way they look at it. I don't know if
that's the case with the governor there but it would be a principled response in
that community to absolutely dislike trans people in every way but still
defend them from anything that would be perceived as government overreach. Here's
a quote from the governor. He's talking about the legislation and he says that
it infringes on local control by unnecessarily injecting the state into
rare instances most appropriately handled at the parent-teacher in-school district level.
Another one is the teaching profession is challenging enough without the heavy hand
of the state government, forcing teachers to take on the role of pronoun police.
It's not necessarily about supporting the LGBTQ community.
It's more about stopping what is perceived to be state overreach.
So that's why the veto occurred.
Now it is worth noting that this governor back in 2021 vetoed a bill dealing with trans
students playing sports in school, and it's basically like you're not going to stop it.
I'm not sure whether or not this governor actually is somewhat supportive or maybe even
a really strong ally, but it seems like the driving force behind the decision is more
about being a small government conservative.
Again, people that most believe only exist in memes.
There are a few of them out there.
Before you get too happy about this though, I do have some concerning news.
The state legislature there wants to override the veto.
In the Senate, I think they actually already have.
In the House, I think they're voting on it today.
today. In fact, by the time this goes out, they may have already voted. When
the bill went up the first time, they needed three additional votes if they
wanted to override it. Whether or not they get those votes, I don't know. So this
faction of the American right of the conservative movement. It exists and it's
actually getting louder. Something that's happening right now from and it's
coming from small government conservatives or people who refer to
themselves as anti-authoritarian right or libertarian right. They are very much
right now coming out in favor of trans people in relation to the Second
Amendment because you had a bunch of prominent right-wing talking heads
basically say, well we need to ban people from owning guns, ban trans people from
owning guns, and you have had a number of anti-authoritarian rights, small
government conservatives, libertarian right, whatever you want to call them,
just come out and say no you're not doing that and it is causing a a split
on the right here but it's not necessarily really about being
supportive of the LGBTQ community it's that that's too much power for the
government that's not the government's business the state can't do that that's
where they're coming from. So it's a rare moment where that faction of the right
shows up and they're very visible, but that is occurring right now. So it's not
inconceivable, it's just not something you see very often. So that's what's
going on and I have a feeling that you're going to see that group, those who
are truly principled, you know, those who didn't get sucked into the authoritarian
sweep that went through the Republican Party or the right in the United States
in general, those who actually stood by their principles, I have a feeling you're going
to see them get really loud soon.
You're going to see them start raising their voices because basically because MAGA and
Trump are kind of falling, I think they may see it as their opportunity to kind of regain
a little bit of the power that they lost.
They used to be far more influential within the Republican Party.
So expect to see more of this in various ways.
Anyway it's just a thought.
Y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}