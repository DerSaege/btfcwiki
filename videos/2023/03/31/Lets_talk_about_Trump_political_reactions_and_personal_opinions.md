---
title: Let's talk about Trump, political reactions, and personal opinions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qhOWOxV3hVE) |
| Published | 2023/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Talks about his personal opinion of the Trump developments and addresses a question from a conservative that prompted the discussion.
- Appreciates the objective reporting on Trump's indictment and the calm demeanor despite being a never-Trump-er.
- States that based on publicly available information, the New York case against Trump seems to be the weakest among all he is facing.
- Mentions a high likelihood of two more indictments, which he believes are stronger cases than the one in New York.
- Expresses opposition to Trump beyond just being a Never Trump-er.
- Urges for objectivity when discussing legal matters and moving away from the theoretical and political commentary.
- Criticizes Republicans rushing to Trump's defense, stating that their words may backfire in the future.
- Points out that the indictment of a former President is a significant event that will have lasting political implications.
- Comments on Democrats potentially overplaying their hand and warns against assuming guilt before legal proceedings conclude.
- Stresses the importance of maintaining professionalism and presumption of innocence in legal matters.

### Quotes

- "I appreciate you immediately changing track and not dancing around celebrating Trump's indictment."
- "A former President of the United States was indicted. That's a big deal."
- "Assuming that he is going to be found guilty is not something that I think they should do."

### Oneliner

Beau Young addresses Trump's developments, criticizes Republicans rushing to his defense, and warns against premature assumptions of guilt by Democrats in a legal matter.

### Audience

Observers and commentators.

### On-the-ground actions from transcript

- Contact your representatives to urge them to prioritize professionalism and presumption of innocence in legal matters (implied).

### Whats missing in summary

Insights into the potential long-term effects of Trump's indictment on political discourse and the justice system.

### Tags

#Trump #Indictment #Politics #PresumptionOfInnocence #LegalSystem


## Transcript
Well, howdy there, internet people, it's Bo Young.
So today we are going to talk about my personal opinion of the Trump
developments and a question from a conservative that has prompted all of this.
And we'll talk about the reactions of politicians, what I think about
the cases, all of that stuff.
I appreciate you immediately changing track and not dancing around
celebrating Trump's indictment.
it was really nice to get just the facts reporting.
As a conservative, it was really nice to see objectivity
and calm from somebody who I know is a never-Trump-er.
Couldn't even get that from any conservative outlet
I follow.
But I'd really like to know what you think
about the case personally and the reactions
on the political side of things.
Fair enough, first, I am opposed to Trump
on a level that is way beyond using the terminology
Never Trump-er.
But point taken.
Yeah, OK.
So my personal opinion, I think based
on what is publicly available, the New York case
is the weakest case out of everything that he is facing.
Now, that being said, the DAF there
has proven an ability to, uh, to keep things from becoming publicly
available, so that case may be stronger than it appears, but based on what
is publicly available, it's not a guaranteed Trump win, but it's not
a slam dunk case either.
Uh, my personal opinion is that there is a high
likelihood that there are going to be two more indictments, I think. Could be
wrong about that, but based on what is publicly available, I think two more
indictments is pretty likely. And I think that both of them are actually stronger
cases than what's in New York. Okay, so I don't have an opinion on how it is going
to turn out. Once things move from the theoretical, commentary, political side of
things and actually enter the legal system, I try to, objectivity is not the
right word, I don't believe objectivity actually exists, but I try to be as fair
as possible. Now, as far as reactions from other politicians and stuff like
that, on the Republican side of the aisle, I think there are a lot of Republicans
who are making a huge mistake right now in rushing to his defense because
everything that they say now is going to be applied to, well, pretty much every
case that comes along. So I think that they're messing up. The good conservative
Republican response right now should be something more along the lines of, you
know, the justice system will sort this out, you know, something like that. Or if
they want to support him, say something like, I'm sure the facts of the case will,
will demonstrate that this was unwarranted.
It should be pretty tame.
You have some Republicans that are engaging in a lot of wild rhetoric, dog whistles, all
kinds of stuff, and I think they'll regret it.
As different proceedings move forward, I think they're going to regret it.
In some cases, it may actually sabotage their own ambitions.
They may end up burning their careers for a sound bite, and I don't think that many
of them understand that yet.
This is not a little thing, and regardless of any posturing about it, a former President
of the United States was indicted.
That's a big deal.
That is a big deal.
We like to pretend that, you know, the justice system is 100% fair and all of that stuff
and everybody's held accountable, and that's not really how it works.
And this is going to get a lot of political attention and all of these comments are going
to, are going to be remembered.
And those soundbites will be played at the most inopportune times, I'm sure.
Now on the other side of the aisle, there are Democrats who I think are overplaying
their hand a little bit.
They're not doing it to the same degree as Republicans, but there's a lot of celebration
that I'm not sure is warranted at this time.
Then you have some that are making statements
that will certainly come up again and again.
A good example of that would be Pelosi.
Pelosi used the phrase, prove his innocence.
There's supposed to be a presumption of innocence.
That's how it is supposed to work.
It doesn't matter what you think, you're supposed to operate under that assumption.
And statements like that will probably feed the rhetoric of Republicans who are trying
to make it seem more politicized, and it will undoubtedly just become a cycle.
The Democratic Party and politicians associated with it, they need to be very careful.
They need to maintain an air of professionalism about it.
I think that's the right move.
Assuming that he is going to be found guilty is not something that I think they should
do.
And this goes back to my feelings about Trump chanting, lock her up, lock her up.
And then when it happened to him and people started chanting, lock him up, it's not how
it's supposed to work.
What we know is that the DA presented evidence and the grand jury found grounds to indict.
what we know. And since it's in the legal system now, it should be treated as a
legal matter and not a political one. But as far as, you know, objectivity and all
of that, I don't think in this climate, like even people who are really good at
maintaining their objectivity are going to be able to. There's a lot, there's a
lot of history here that has to be remembered. So I wouldn't expect a lot of
objective coverage even if you believe it's something that exists. Yeah, as far
as me, once it actually hits the phase where, you know, somebody's been indicted,
And you have to operate under the assumption that they have the presumption of innocence.
And that's the coverage I will try to provide.
But again, yeah, Never Trumper doesn't even remotely describe how much I don't like him.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}