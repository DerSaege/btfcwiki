---
title: Let's talk about Trump, NY, and what happens next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QkfEiKWmdA4) |
| Published | 2023/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the uncertainty surrounding Trump's processing in New York and the need to re-evaluate expectations of knowing things ahead of time.
- Trump is expected to be processed in New York in the next few days under Secret Service protection, but the media's Tuesday expectation may not be accurate.
- Due to security concerns, both inaccurate information from the media and the DA's office may lead to uncertainties about timelines.
- The DA's office may have reasons to withhold accurate information about when Trump's indictment is expected.
- The traditional habit of knowing things ahead of time may no longer be reliable for security reasons.
- There is a possibility that Team Trump was informed about the situation in New York either out of courtesy or through a leak, leading to a lack of courtesy in other areas' prosecutors notifying Trump before indictment.
- The shift from political to legal commentary underscores the changing expectations of knowing things ahead of time regarding Trump's legal matters.
- The Secret Service and investigating jurisdictions are not obligated to inform the public in advance of Trump's legal proceedings, leading to increased speculation rather than certainty about the timeline.

### Quotes

- "The habit of knowing what's going to happen ahead of time - I wouldn't count on knowing that anymore."
- "It may come as a total surprise."
- "The expectations of knowing things ahead of time, those need to change."
- "That needs to start to fall under speculation rather than what we know."
- "Anyway, it's just a thought. Y'all So have a good day."

### Oneliner

Beau explains the uncertainties surrounding Trump's processing in New York and the need to shift expectations from knowing things ahead of time to speculation for security reasons.

### Audience

Legal commentators, political analysts

### On-the-ground actions from transcript

- Stay informed on legal proceedings concerning public figures (exemplified)
- Understand the shifting dynamics of legal commentary and the implications for public knowledge (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the changing landscape regarding public knowledge of legal proceedings involving public figures.

### Tags

#Trump #LegalProcess #PublicKnowledge #SecurityConcerns #Expectations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about what is next for Trump in
regards to the New York case and expectations that people have and
why the American public may need to reevaluate its expectations when
it comes to knowing things ahead of time, because the American public
grown very accustomed to knowing what's about to happen when it comes to timelines.
And that may be changing.
Okay, so what do we know is going to happen with Trump as far as New York goes?
Sometime in the next few days, he's going to be processed, fingerprints, mugshot, all
that stuff.
That's going to happen under the protection of the Secret Service.
Right now the media is creating the expectation that this is going to occur on Tuesday.
Will it?
Maybe.
Or maybe it happens tomorrow, before anybody expects it to, to maintain security.
Regardless of your personal opinion of him, Donald Trump is the former President of the
United States and is being protected by the Secret Service.
The likelihood of less than accurate information being put out about his travel, where he'll
be and when, I mean it's likely that information comes out that isn't true.
Now maybe he is going to be processed on Tuesday, that's totally a real possibility.
But I don't think it's a good idea to accept information like that as fact anymore.
On the other side, the DA's office up there, they also have a reason to be less than forthcoming
with information about timelines due to security concerns.
I think that it's completely within the realm of possibility that the DA's office up there
has put out information that wasn't entirely accurate
about expected timelines, as far as when the indictment was
or is to occur, something like that.
Or at the very least, if somebody
who was, quote, familiar with the matter,
put out information, they may not correct it.
So right now, there's the habit of knowing what's
going to happen ahead of time.
I wouldn't count on knowing that anymore, just out of a sense of security.
There may be a desire to not tell the public ahead of time.
And it's a well-founded reason.
Now something else that may occur because of this, since we're talking about surprises
and expectations, knowing stuff ahead of time.
a high probability that somebody in the DA's office in New York told Team Trump what was
going on when all of this circus started, either as a courtesy or as an actual leak.
If that's the case, other prosecutors in other areas may not extend that courtesy because
of the rhetoric, because of the calls to action, because of the security concerns that were
prompted by the former president's foreknowledge, the prosecutor in Georgia may not let him
know that he's about to be indicted.
The federal prosecutor may not let him know.
That courtesy may not be extended.
So it may come as a total surprise.
Because this moves from political commentary to legal commentary, it's important to understand
that the expectations of knowing things ahead of time, those need to change.
Because neither the Secret Service nor the various jurisdictions that are investigating
the former president really have any requirement to tell the public ahead of time what is going
to happen. So that needs to start to fall under speculation rather than what we know.
What we know is that soon he'll be processed. Do we know the exact day? The speculation
is that it's Tuesday, but that may not be true. Anyway, it's just a thought. Y'all
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}