---
title: The roads to school security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3d1zkx8JGek) |
| Published | 2023/03/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Focuses on prevention and security as top priorities to achieve a safe environment.
- Explains that security isn't about making a place impenetrable but rather about deterring potential threats.
- Emphasizes creating delays in security to allow for appropriate responses in emergency situations.
- Proposes simple and cost-effective physical security measures to enhance safety in schools.
- Suggests using fences, controlled access points, ID checks, and monitoring systems to create layers of security.
- Advocates for the implementation of physical security measures without militarizing or prisonizing school environments.
- Describes the importance of early detection, quick responses, and creating barriers to potential threats.
- Recommends reinforcing ceiling areas for added security in emergency scenarios.
- Stresses the significance of maintaining a school's atmosphere while ensuring student safety.
- Concludes by encouraging action towards improving security measures to prevent potential risks.

### Quotes

- "Security is not about making a location impenetrable; it's about making it so annoying that threats go somewhere else."
- "You're not trying to defeat the multi-million dollar security system; you're trying to defeat the person getting paid $11 an hour to watch it."
- "You're not trying to turn a school into a military installation or a prison; you're trying to maintain the atmosphere of the school."
- "If you employ good physical security, it should be really unlikely that the first sign of a problem is a shot."
- "While you have a bunch of people saying nothing can be done, there's always something that can be done."

### Oneliner

Beau explains the importance of creating delays in security through simple, cost-effective measures to ensure prompt responses and prevent potential risks in schools.

### Audience

School Administrators, Security Professionals

### On-the-ground actions from transcript

- Implement controlled access points with ID checks and monitoring systems (suggested).
- Reinforce ceiling areas for added security in emergency scenarios (implied).
- Maintain exterior doors closed except for approved entry points (exemplified).
- Train staff on responding to security breaches and implementing lockdown procedures (suggested).
- Ensure physical security measures are in place without militarizing school environments (implied).

### Whats missing in summary

The full transcript provides detailed insights on enhancing physical security measures in schools through simple and cost-effective strategies, promoting proactive approaches to prevent potential risks.

### Tags

#SchoolSecurity #Prevention #SafetyMeasures #EmergencyResponse #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about the roads to security,
how to achieve it, what it is, what its purpose is,
and how we can do a little bit better, why we would want to.
I would hope, in this context, it's
easy to understand why we would want to do better.
We're doing this because over on the other channel,
I mentioned a focus on prevention and security
being the priority, should be the priority.
And a lot of people immediately thought
security meant one specific thing.
And that's not really what it takes.
You don't have to introduce a whole bunch more of those
things to a school to achieve security.
And you had people who actually understood security.
Like, the purpose of security is, you know, it's not to actually stop a person.
And that's true.
And this is something that most people don't understand.
When you are talking about corporate or private security, the goal is not actually to stop
the person.
It's not to make that location just impenetrable.
It's to make it so annoying they go somewhere else, sends them down the road, goes to a
different location. If somebody is trying to penetrate your security, you are
trying to make it so annoying, so hard, that they go somewhere else. Now in this
context, in the context of schools, that's not the goal. That's not the goal
because it doesn't matter where it happens, it's bad. So you're trying to
achieve something else. One of the things that people often say is that with enough
If will, resources, and time, any security can be defeated.
And that's true.
That's a true statement.
You will never make a facility so secure that the security can't be defeated.
That's not a thing.
Will, resources, and time.
Time.
Nashville PD just set the standard.
13 to 14 minutes call to fall.
The time it takes to defeat your security should be 15 minutes because that time frame
from Nashville, that also includes assembly, entry, everything, not just the arrival.
And if you do it right, things can start to, the dynamic can change right at arrival.
So that's what you're looking to create.
looking to create a delay in which the call can go out and nothing bad has
happened yet. The lockdown can start, the call can go out, and you're trying to
create a buffer. Physical security can do this. It can. It just has to be
implemented and then carried out, which is the hard part because security is
is annoying. Security is a pain. It doesn't allow for certain normal behaviors and stuff
like that. And it's not stuff you can set it up to where it really doesn't impact the
students, but the staff have to continue to carry it out. So we're going to go through
some really simple basic stuff that can help create that delay from the time the person
is noticed and the call goes out to when they would actually be able to do something bad.
And in a lot of cases, a lot of scenarios, they end up hitting that point where there
would be a rival, where the cops would start to show up before they ever get to hurt anybody.
And that should be the goal.
Okay, so step one.
That's the first thing you think of when you think of securing an installation.
A fence, right?
Yeah, you need one.
A tall one.
And yes, I understand that if they wanted to, they'll climb it.
We'll get to that.
But you have a fence.
Now there is one, one entry and exit point.
One way to drive a vehicle through that gate, to get through that fence.
And this is the only additional armed person that is going to be needed in most places
because most places already have an SRO.
They already have a school resource officer.
They have an armed person there already.
You're adding one at the gate.
But this person never interacts with students.
This person isn't wandering the halls.
This person is at that gate.
So during pickup and drop-off, there's stickers.
You have stickers for each vehicle.
They have the year on it, and they are replaced every year.
The ink is on the sticky side.
So the sticker has to be placed inside the vehicle on the windshield, and it has the
year on it.
Certainly some people are going to say, you know, replacing the stickers every year, that's
just going to be an ongoing expense and it's going to cost less than what we're losing
now.
Okay, so the sticker goes in.
During pick up and drop off, most people on most days are just going to roll right by
the gate if they have the sticker.
every day, there's a set percentage of cars that are going to be stopped and
they're going to get an ID check. And that ID check consists of the guard
saying, hey, let me see your ID, and that person then, the guard calls up to the
school, confirms the information, yes, there's somebody, it's a parent, they're
supposed to be there, all of this stuff. And on most days it's 10%. Some days it's
25%. Some days it's 50%. Some days it's 100%. And yeah, those days are super
annoying. You can talk to anybody that has ever gone on a military installation
and ask them about the feelings they have when they're driving towards
the gate and they see 100% over on a sign. They know they're going to be sitting there
a while. Yep, it's annoying. It's part of it. The fluctuation in the percentages checked,
these are not pre-announced. That fluctuation, it disrupts somebody's planning. They don't
know what's going to happen. So they tend to go to a different entrance, which is
kind of the goal here. But during the ID check, is it really about checking the ID
and getting the information? No, it's about the guard looking into the car. Is
the person wearing boots? Do they have a vest on? Is there anything visible in the
vehicle that might raise suspicion.
Now, during pick up and drop off,
probably not going to be where this happens,
in most cases initially, because that's not
how it has historically occurred.
But once you start implementing the gate throughout the day,
If you leave it open during pick-up and drop-off, that's when the bad person will show up.
They plan.
You have to plan for their planning.
So that's pick-up and drop-off.
Throughout the day, everybody, every vehicle showing up at an abnormal time gets their
ID checked.
And yes, undoubtedly somebody's going to be like, you know, I'm Karen McKarenface with
with a PTA and I'm just driving my friend's car today.
That's great.
All you have to do is show me an ID,
makes me care and face.
Nobody cares, okay?
It's going to be annoying
and it's gonna make people mad.
That's fine, that's fine.
They need to learn how to deal
with disappointment in life.
So, during this part of the day, same thing applies.
It's not actually about checking the ID.
It's not about that.
It's about seeing what is going on in the vehicle
and hopefully deterring people from trying to make it through
and get close to the school this way.
Let's say somebody puts everything in the trunk
and they're totally cool and they make it through the gate.
What happens then?
They get to the parking lot and they have to kit up.
During this period, they'll be noticed.
The call goes out, the timer starts, lockdown starts.
you're creating that delay.
And that's what you're trying to achieve.
OK.
Now, when you're talking about the building itself, there are
cameras on the roof, on the outside of the building.
They are facing the gate, the parking lot, the fence.
Everything is covered 100%.
The monitors to these cameras are inside the office.
If something is noticed, somebody's climbing the fence,
somebody's pulling stuff out of a trunk, the call goes out,
the lockdown starts, the timer starts.
The monitors are off most of the time.
The cameras will turn the monitors
on when they sense motion.
The monitor will come on, it'll make a little noise, and it draws people's attention to
it.
This is technology that exists, it's cheap.
I think it's actually in your like ring camera thing.
The reason you want to do this is because if you are the person trying to penetrate
security, there's kind of a rule.
You're not actually trying to defeat the multi-million dollar security system.
We're trying to defeat the person getting paid $11 an hour to watch it.
If you have the monitors on all the time, people tend to zone them out.
They just don't pay attention to them anymore.
So they're not on all the time.
They flash on when there's motion, and then a button has to be pushed to turn the monitor
back off and put it back in standby.
So this helps with the entire perimeter.
And this is a couple of minutes that can be bought as far as getting that call out early
before they ever get to the school, before they ever actually get to the building.
Now let's say this happens.
Something is noticed.
Somebody comes over their fence.
Somebody opens their trunk.
Things are looking bad.
The call goes out.
occurs other than the lockdown, the guard gate and the SRO converge on that person from
different directions.
They don't meet up and then go.
They converge on that person from different directions.
The first person that is spotted, odds are, yeah, they're going to take fire.
They are going to take fire.
If the person is up to bad things, that's when it'll probably start.
Outside the school away from the students.
When right?
The reason you want them converging and coming from different directions is because if somebody
is looking one way and engaging, it's really easy for them to get hit from another direction.
Hopefully it could be stopped before they ever get to the school.
Maybe they can only pin them down until arrival of emergency responders.
But either way, that delay, that clock, it's started.
Okay, so the school itself, the actual building, one entrance, there's one way that is open
throughout the day, right by the office.
you go through the door, the best way to do this would be to have a buzzer and an
intercom system. So before you ever even enter the building, you hit the buzzer
and you say what you're there to do and whatever, right? You're on camera at this
point. If they have something, the door doesn't open, the timer starts. This is
assuming that whatever happened was missed by the outer rings of security.
It's caught there at the door. If they make it through that, they make it through the first door,
they get buzzed in. Now they're in a room with another set of doors in front of them. These are
real doors, security doors, reinforced doors. To their right, there's a window, bullet-resistant
glass. Almost everything gets handled through there. Parents coming to drop
off little Timmy's bag, they use the box. There will be a box that it's passed
through. It only opens from one side at a time. One side opens up in the four-year
area and you can send in little Jimmy's bag. It closes and the staff can run it
to Little Jimmy. Most people will never get past this area. If it is a bad person, once
they enter that area, assuming they get through the first doors, they still have to get through
the second doors. During this time, the call has gone out. The lockdown has started. You've
set that clock in motion, you're waiting for arrival. If it was missed by the
guard gate and the cameras and everything and they've made it in there,
you still have two people who can respond immediately to somebody in that
little confined space and that's ideal because they can't move without
encountering one of them. The guard gate would be coming from outside, the SRO
from inside. You've got them pinned. All other doors, all other exterior doors in
that school, in every school, they're real doors. Bullet-resistant glass if you have
to have a window. They're security doors. Yes, they can still get through if they
use the right stuff, but it takes time. It's loud. It's noisy. It starts the
the clock starts the lockdown, creates the delay.
Now realistically, in most scenarios, when you were talking about somebody coming from
outside of the school, this is enough.
It really is.
This should be enough to create the delay for responders to get there, but let's say
Something happens, and they make it inside.
What else is going on there?
Back to the exterior doors for a second
before I move on and forget this.
All exterior doors, except for the one approved one,
are shut throughout all time.
They're closed.
You don't use them.
If it can be accessed from the exterior of that building,
we're not necessarily talking about like a courtyard
like that, but they're closed. Inside the office there is a map, lights and noise. If
a door is opened, light goes on where it is and the little alarm clock sound
works, comes on. People inside the office are looking. Now if it stays open or it's
not supposed to be open, it goes and it's secured. If somebody props a door open,
they're gone. They're gone. I don't care if it is that teacher's 364th day of
their 19th year and they're about to retire. They're gone. No exterior door
gets left unsecured. Okay, so let's say the person makes it inside at that point.
With everything, with all the different layers, the lockdown should have
already started. They should already be secured. So hopefully the doors inside
the school itself are decent because that delay it's already running. By this
point people are already trying to get there. Lights and sirens are already on
the way. I have seen a pretty interesting little product recently and
And basically, it's plating that sits in the corner.
There's two sections, meets in the corner of the room,
and in an emergency, it can be pulled out.
And it's still, and it's basically,
it's a collapsible panic room.
Really cool design, really inventive,
gives a good place to hide.
One thing that I want to point out
is that most schools have drop ceilings.
If you do not harden the ceiling area,
a bad person can just raise up the tile,
and it's really bad from there.
So if you're going to employ something like this,
you have to reinforce the ceiling area.
But personally, I like the idea of something like that,
but it's staying out all the time.
It can be decorated.
It can blend in.
That way, again, you're not trying to turn a school
into a military installation.
You're not trying to turn it into a prison.
You're trying to still maintain the atmosphere of the school.
That little room, it could be the computer room,
or it could be something, an art room in that class,
a study room, whatever.
The students don't necessarily have to be constantly reminded
that it's to shield them from rounds.
I really liked that design with the exception of the ceiling thing.
So this helps you create that scenario where the delay gives you time to secure the students and prevent loss.
If you employ good physical security, it should be really unlikely that the first sign of
a problem is a shot.
That's what you want to achieve.
If you can get the lockdown going before the person enters, when the arrival happens from
emergency responders, you have made their job really easy because that person is going
to be walking up and down those hallways trying to find something to do, trying to find a
way to get in, or whatever.
And they're in a funnel.
It should be really, it should be really easy.
And again, you have two people there to respond.
And you don't turn the school into some militarized fortress.
All of this is pretty, it's pretty low signature, but it'll work.
You're creating that delay.
And there are tons of other things that can be done.
I know that there are security professionals that watch this, yes, yeah, there's way more
that can be done.
But this stuff, simple, easy to implement, and cheap, because it's the US and, you know,
We put the dollar back into idolatry.
People care about the money.
This is all pretty inexpensive, and it'll work.
It will pay off.
So it's an overview of what can be done.
While you have a bunch of people saying nothing can be done, there's always something that
can be done.
Even in situations where the assailant is from inside the school, not somebody coming
from outside, a lot of this still helps.
A lot of this still helps.
There's not going to be one set template that is going to solve all problems.
But this is a really good one to deal with one that's sadly pretty common.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}