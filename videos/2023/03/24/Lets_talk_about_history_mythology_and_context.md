---
title: Let's talk about history, mythology, and context....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4NjxBqehQzc) |
| Published | 2023/03/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring American history versus American mythology and context.
- Addressing messages received after linking a video.
- Clarifying that linking a video doesn't mean cosigning everything in it.
- The video in question aimed to explain the Cold War concept of MAD effectively.
- Two main criticisms received regarding historical accuracy.
- Response to the first criticism regarding the development of nuclear programs during World War II.
- Response to the second criticism about the Cuban Missile Crisis and the removal of nukes from Cuba and Turkey.
- Mentioning the common belief about Kennedy secretly pulling U.S. nukes out of Turkey.
- Providing a brief timeline of U.S. nuclear systems placement in Turkey.
- Expressing the presence of U.S. nukes in Turkey to this day and discussing the need for their removal.
- Not delving deep into the topic, hinting at a potential follow-up video.
- Speculating on the motivations behind the narrative surrounding the Cuban Missile Crisis.
- Acknowledging Khrushchev's efforts for peace during the crisis.

### Quotes

- "Exploring American history versus American mythology and context."
- "The video aimed to explain the Cold War concept of MAD effectively."
- "US still has nukes in Turkey to this day."
- "Peace was way more important than political posturing."
- "Khrushchev was definitely somebody who was looking for peace."

### Oneliner

Beau dives into American history, mythology, and context, addressing criticisms on nuclear history accuracy and the Cuban Missile Crisis, revealing the ongoing presence of U.S. nukes in Turkey and hinting at the importance of peace over political posturing.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Research the historical accuracy of events discussed (implied)
- Advocate for the removal of U.S. nukes in Turkey (implied)

### Whats missing in summary

Deeper insights into the historical context and potential follow-up content.

### Tags

#AmericanHistory #AmericanMythology #NuclearWeapons #ColdWar #Peacekeeping


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about American history
and American mythology and context and miscontext.
Because I got some messages, which I normally do
after I link send people to a video,
and one of the critiques of that video
It just runs right into a subject that I love to talk about,
which is the difference between American history
and American mythology.
But because I'm going to talk about one of the critiques,
I have to talk about the other one too.
But before we get into that, just so everybody knows,
when I link a video or something like that,
I'm not co-signing everything on the channel.
I'm not necessarily even co-signing everything
in the video.
It's there for a point.
And the point in that case was for people
who weren't around during the Cold War to understand MAD,
understand nuclear triads, and get all of that information
in a nice, neat little package and explain how long it's
really been going on.
The video does a very good job of that.
The two critiques, the first one, early in the video,
She says something like fearing the Soviets were working on their own bomb,
the US started their program, something like that.
And there are a lot of people that are like, no, it was Germany.
And sent a couple messages about it.
Yeah, yeah, I think given the way the channel is shaping up over there,
I think what she was trying to do was draw attention in additional context
to the fact that, at the time, during World War II,
the Soviets and the US were kind of on the same team.
But the US didn't tell the Soviets what was going on
when it came to the development of the nukes,
like they did, say, the British.
They kind of kept them in the loop on what was going on.
But they didn't do that with the Soviets.
think that's what that was about and it's just a phrasing issue because it's
the first video they put on YouTube. I'm sure they'll get the hang of that that
I don't see that as a huge error mainly because I don't believe that anybody who
made that video is unaware of World War II. I mean just looking at it I don't
think that's what happened I'm willing to bet you know she is aware of World
World War II in general, and probably Einstein's letter as well.
So to me, that's just phrasing and being new to YouTube.
But now to the critique that I actually find really interesting.
I got a couple of messages.
They all contained the exact same language that matters in this.
I really liked that video, but it surprised me that she didn't mention that when the Russians
pulled all of their nukes out of Cuba, Kennedy secretly pulled all of the U.S. nukes out
of Turkey.
But at least she said both sides saw diplomatic solutions.
Just seems weird that she didn't mention it.
And of course, this is about the Cuban Missile Crisis.
And that's American mythology right there.
The standard thing, and if you ask most people, they would probably say it just like that.
The Soviets pulled all of their nukes out of Cuba, and the US pulled all of their nukes
out of Turkey in exchange.
And Kennedy did it in secret, and for whatever reason, the Soviets really didn't talk about
the fact that he did it.
That's what is widely believed, but is it true?
Because it's a new channel over there and the whole gig seems to be providing additional
context, I'm not going to go super deep into this because if it was me, I would want to
do a follow-up video and get into all of the details.
The super short answer is that there are more planets than Jupiter in the solar system.
There are more weapons systems than Jupiter in the U.S. nuclear arsenal.
Quick timeline, in 1959 the U.S. put, to keep it simple, gravity bombs, bombs that come
out of airplanes that were nukes in Turkey.
In 1959 they also put in Honest John systems.
1961 they put in Jupiter systems and then I want to say it was 1965 could be
wrong on that one they put in was an 8-inch howitzer nukes as well. To this
day right now this minute if you were handed an all-access pass to US military
installations in Turkey and you just started walking in and out of the
structures, eventually you would find yourself in a room full of a whole bunch of stuff that
according to American mythology isn't there.
But it is.
In fact, we've talked about it on the channel before.
The US still has nukes in Turkey to this day.
We've talked about how it is my opinion that we should probably remove them.
They don't really need to be there.
But there's a whole story that goes along with this, and I don't want to get too far
into it, but the first critique that people are making, bad phrasing.
The second one, no, that's, to me, it sounds like somebody who knows, as Paul Harvey would
say, the rest of the story.
I mean, let's be real, it's the US.
Have you ever known the US to play fair on the foreign policy scene?
There's a reason that was phrased the way it was, and yes, Khrushchev definitely
sought to avoid war, sought diplomatic means, and the fact that the Soviets
really never hammered the US about removing the Jupiter systems, which, let's be real,
they were pretty much obsolete, but the fact that that never became a huge thing that was
widely talked about for all that time indicates that peace was way more important than political
posturing. So if you are somebody who is wondering what Khrushchev's real position
was, when it came to that topic, he was definitely somebody who was looking for
peace.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}