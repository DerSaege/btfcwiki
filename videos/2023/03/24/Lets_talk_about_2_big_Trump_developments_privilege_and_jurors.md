---
title: Let's talk about 2 big Trump developments, privilege, and jurors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DAfCmFtVn_o) |
| Published | 2023/03/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A judge ruled that executive privilege doesn't apply in the January 6 proceedings, giving the special counsel access to grand jury testimony from individuals like Mark Meadows, potentially revealing insights into Trump's actions.
- The E. Jean Carroll lawsuit involves anonymous jurors to protect their identities due to Trump's history of attacking public officials and individuals, setting a rare precedent for an anonymous jury in cases with high media attention.
- Anonymous juries are increasingly common in the social media age to prevent influence and harassment, something previously seen in cases involving organizations known for targeting individuals.
- The use of an anonymous jury for a former President due to his behavior, as seen in the E. Jean Carroll lawsuit, could have implications for future cases involving Trump.
- The absence of executive privilege for Trump allies and the use of an anonymous jury in a lawsuit are significant developments with potential long-lasting effects on legal proceedings involving Trump.

### Quotes

- "Without executive privilege, the special counsel will obtain grand jury testimony from everybody from Mark Meadows on down to have that ability."
- "The fact that a judge has decided a former President of the United States requires an anonymous jury because of his behavior, that's pretty telling."

### Oneliner

A judge's ruling on executive privilege and the use of an anonymous jury in the E. Jean Carroll lawsuit signal significant shifts in legal proceedings involving Trump.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Stay informed on the developments in the Trump cases (implied)
- Support transparency and accountability in legal proceedings (implied)

### Whats missing in summary

Insights on the potential implications of these legal developments and their impact on future cases involving high-profile individuals.

### Tags

#LegalProceedings #TrumpCases #ExecutivePrivilege #AnonymousJury #Transparency


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about two pieces of information,
two developments in the Trump cases, plural,
that are going to have long-lasting effects.
And we'll kind of go through what those developments are
and what they mean for the long term.
So the first one, the one that is probably
going to get the most attention is the fact
that a judge has ruled that the concept of executive privilege
does not really apply in the January 6 proceedings.
Without executive privilege, the special counsel
will obtain grand jury testimony from everybody
from Mark Meadows on down to have that ability.
Basically, executive privilege isn't held by Trump anymore.
And given the circumstances, the testimony
that his staff, those that might normally
be covered by executive privilege,
the testimony they may have far outweighs any precedent when
it comes to maintaining that separation and that deliberative process. That means
a whole lot of people who were capable of not answering questions before are no
longer going to be able to ignore certain questions and they probably have
a lot of insight into Trump's state of mind and what he did or did not do. The
The other one involves a case that it's on the board, but we haven't really talked about
it much.
In fact, I don't know that we've covered it.
It's the E. Jean Carroll lawsuit, and you can look up the details of this case on your
own.
There's a whole lot to it, but the jurors in that lawsuit will be anonymous.
They will have their identities protected.
judge made that decision because, quote, Mr. Trump repeatedly has attacked courts, judges,
various law enforcement officials and other public officials, and even individual jurors in other
matters. It goes on, if jurors' identities were disclosed, there would be a strong likelihood
of unwanted media attention to the jurors' influence attempts and or of harassment, or worse,
of jurors by supporters of Mr. Trump. Anonymous juries are pretty rare. That's
not a thing that happens very often. They're becoming more common in the
age of social media when jurors are likely to, well, be influenced as the
judge says, be harassed. Until recently, this was something that was really only
used when it came to trials that involved organizations known for going
after people. The last major case that I can think of in which an anonymous jury
was used was the Chauvin Trial.
The fact that a judge has decided a former President of the United States requires an
anonymous jury because of his behavior, that's pretty telling.
That's something that's probably going to come up in other cases later.
So executive privilege is not something Trump allies are really going to be able to hide
behind and an anonymous jury in a lawsuit is setting the stage for a lot of unique conversations
down the road when it comes to juror security and security at any proceeding Trump may be
present at.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}