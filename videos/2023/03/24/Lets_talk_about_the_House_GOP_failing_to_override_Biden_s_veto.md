---
title: Let's talk about the House GOP failing to override Biden's veto....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4GtS5PNlCeI) |
| Published | 2023/03/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the controversial legislation proposed by the House GOP restricting financial advisors from recommending investments based on environmental or social reasons.
- Predicted that Biden's first veto will be on this legislation, which indeed happened.
- House GOP attempted to override Biden's veto but failed spectacularly with only 219 votes.
- House Republicans likely to propose more wild legislation to energize their base, knowing they can't override Biden's veto.
- Anticipates a surge in controversial bills closer to the 2024 election, aiming to energize their base despite inevitable failure.

### Quotes

- "They mounted an effort to override the veto, and it failed in spectacular fashion."
- "And keep in mind, in comparison to some of the stuff that they are likely to propose later, this was not controversial at all."
- "The strategy, if you want to call it that, is to energize the base and say, you know, If we had more seats in the Senate, or we had the presidency, this would now be law or something like that."
- "It is likely to alienate the majority of Americans."
- "Holding votes, like to override a veto, that you know aren't going to go well is not generally seen as good political strategy, but, I mean, here we are."

### Oneliner

House GOP's failed attempt to override Biden's veto reveals a futile strategy of proposing controversial legislation to energize their base, likely alienating the majority of Americans.

### Audience

Voters and political activists.

### On-the-ground actions from transcript

- Contact your representatives to express your opinions on proposed legislation (implied).
- Stay informed about upcoming bills and their potential impacts (implied).

### Whats missing in summary

Insights into the potential long-term consequences of the House GOP's strategy and its effects on American politics.

### Tags

#Legislation #HouseGOP #Biden #Veto #PoliticalStrategy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about that legislation again
for the third time, which wasn't something I saw coming.
But here we are.
So we have talked about this legislation two times before.
This is the rule that the House GOP put forward,
basically saying that financial advisors couldn't really
recommend investments based on environmental reasons
or social or governance reasons, stuff like that.
And it should only be about profit, basically, greatly
oversimplifying that.
The first time we talked about it, I was like, hey,
this is going to be Biden's first veto.
Then he vetoed it and we did another video talking about it and saying,
hey, you know, this is what you can expect from the House GOP when they put
forth wilder and wilder pieces of legislation.
Some of it may be unnerving, but understand Biden can veto it and
they don't have the votes to override a veto.
I had zero expectation of them actually mounting a real effort to attempt to override the veto.
I was wrong. Ever been wrong? Happened to me. They did. They mounted an effort to override the veto,
and it failed in spectacular fashion. They got 219 votes, nowhere near what they needed. They
would have needed a two-thirds majority to get anywhere with it. This was not a nail-biter,
which shouldn't have been a surprise to anybody. It's just math. So now you get to see how
the whole process will play out. Odds are that House Republicans are going to propose
a lot of wild legislation. Stuff to get headlines, stuff to appeal to their base in that social media
echo chamber. And what just happened with this is what is likely to happen to all of it. They'll
propose it, they'll probably get it through the House, maybe it gets through the Senate. If it does,
it goes up to Biden, Biden vetoes it, they don't have the votes to override his veto.
And keep in mind, in comparison to some of the stuff that they are likely to propose later,
this was not controversial at all. I would imagine that as we get closer to the 2024 election,
And you'll see just wild bills be put forth in an attempt to energize their base.
And they'll see them, the base will see them fail in the Senate or get vetoed by Biden.
The strategy, if you want to call it that, is to energize the base and say, you know,
If we had more seats in the Senate, or we had the presidency, this would now be law
or something like that.
And it is likely to work for their base, that base that is contained in that social media
echo chamber.
It is likely to alienate the majority of Americans.
And then, generally speaking, in politics, it is a bad idea to fail.
Holding votes, like to override a veto, that you know aren't going to go well is not generally
seen as good political strategy, but, I mean, here we are.
So that's the process, expect it to play out a few more times before the election.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}