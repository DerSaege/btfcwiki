---
title: Let's talk about what children in other countries learn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wtJnJ-vPhZA) |
| Published | 2023/03/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contrasts critical thinking skills in the U.S. with what children in other countries are learning, particularly in relation to assembling and disassembling weapons.
- Mentions a science kit his kids received to learn about mechanics, but clarifies that it doesn't prepare them for specific careers like mechanics or race car driving.
- Addresses the circulating clips of children in foreign countries learning to field strip weapons in class, contrasting it with what U.S. kids are taught.
- Explains the simplicity of field stripping a weapon and its lack of relevance to becoming a warrior.
- Emphasizes the importance of critical thinking over mechanical skills for warfare and gives examples of critical thinking in combat scenarios.
- Talks about how understanding systemic injustice is vital for creating effective warriors and establishing rapport in foreign countries.
- Advocates for teaching children critical thinking about systems of injustice, technology, and the environment, rather than focusing on weapon-related skills.

### Quotes

- "If you want to create warriors, you have to teach people to critically think."
- "Nationalism is politics for basic people."
- "Sending people who think they're warriors because they know how to field strip a weapon is appalling."

### Oneliner

Beau contrasts critical thinking with mechanical skills, advocating for teaching children to understand systemic injustice over weapon-related training.

### Audience

Parents, educators, policymakers

### On-the-ground actions from transcript

- Teach children critical thinking skills (implied)
- Encourage understanding of systemic injustice, technology, and the environment (implied)
- Advocate for cooperative systems over competitive ones (implied)

### Whats missing in summary

The full transcript provides more context on the importance of critical thinking in preparing individuals for various challenges and the dangers of focusing solely on mechanical skills related to weaponry. Watching the full video can provide a deeper understanding of Beau's perspectives and examples. 

### Tags

#CriticalThinking #Education #SystemicInjustice #Warfare #Cooperation


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we are going to talk about critical thinking
and skills and what children in other countries
are learning versus what they're learning
in the United States.
We're talking about how it's gonna prepare them and we're
going to talk about the impression that it's leaving
with some people who are viewing these clips and we're
going to talk about science kits, toys, and realities. We're going to do this
because I got a message and it provides a good jumping off point for this.
Beau, you talk about how people should be self-reliant. You talk about how people
should have skills. You've said nothing about all the videos circulating of
children in foreign countries learning how to assemble and disassemble weapons
in class. This has to bother you. Our kids are learning about CRT. Their kids are learning
how to be warriors. If we're moving into a quote, an ear-to-ear contest, as you always
say, we should keep up. You have nothing to say about that? Oh, I have plenty to say about
that. Tons. Tons. You know, recently, recently, I got a kit for my kids. It's put out by
discovery I think. It's an engine, it's a plastic engine and if you put it
together and you do it right, you plug it in, it works. Pistons go up and down, the
things that would be the spark plugs light up, like it's really cool. It's like
I don't know 25 bucks or something like that. My kids sat around the dining room
table and put this thing together and they learned a lot about mechanics
while doing it. And they did it right. Plugged it in and it started right up and
they were really excited about it. When they were finished, they were not
mechanics. They certainly weren't race car drivers. Before we get too far into
this and to the real point, I do want to point something out just for the sake of
clarity. If you don't know, there are a whole bunch of clips that are circulating
right now within a certain segment of the American population.
And that is what it shows.
It shows kids basically field tripping a weapon in class.
And it has unnerved some people.
We don't have anything like that in the U.S.
Our kids don't learn that.
I want to point out that a lot, not all, but a lot of those clips are from special
programs that exist in their schools.
I don't know, if we had them in the United States, we'd probably call them
JROTC
or the Scouts.
We have stuff like that in the U.S.
It's a little different, but same premise.
But that's neither here nor there.
What
are they learning
when they're doing that, when they're creating these films
and these students are doing that? What are they actually doing?
They're learning how to field strip a weapon
that is world renowned for being ridiculously simple.
That's why it's famous.
Because it's so simple, it's super reliable.
What does it really take to do it?
What?
Let's see, you remove the magazine
and pull back the bolt, visually inspect the chamber,
let it slide forward, hit the button on the back,
remove the cover, push forward, yank it out.
If you're getting real fancy with it,
you're going to remove the block up front.
it can be done about as fast as I explained it. This is not complex. This
does not make someone a warrior. Our kids in the U.S. end up learning the difference
between cover and concealment in grade school. That's not a good thing. The U.S.
has enough hyper-nationalism and wartime mentality, it doesn't need more. More
importantly, learning how to field strip a weapon does not make you a warrior.
Kids would probably learn more about things that were actually applicable to
warfare, playing with erector sets or Lego bricks. If you want to be a warrior
you need to be able to critically think. And if you can critically think about
any subject, you can critically think about any subject to transferable skill.
Well, if you knew how to critically think, you might realize that running across an open
field towards a hardened target is a really bad idea.
You might realize that turning on your phone when the opposition is using it to locate
you because you want to post a pic to Telegram that makes you look like a warrior is a really
bad idea.
You might realize you need infantry screens.
I want to point out that there are tens of thousands of Russians who went through something
like that and learned how to do that in school, who you can't ask if it made them effective
at being a warrior because they went to Ukraine and they're not talking anymore.
If they had spent time learning how to critically think, they'd probably be a lot better off
because they can learn how to field strip a weapon in 15 minutes.
When you talk about CRT, it's worth noting that some of Russia's biggest failures is
because they didn't understand the mindset in Ukraine.
They didn't understand systems of injustice.
They actually believed that there were going to be Ukrainians on their side.
They believed that they were going to help in large numbers.
If you were dropped at an airport that you believe you're going to be meeting friendlies
at and you meet opposition, it doesn't matter whether or not you can field strip your rifle
because you're not going to be cleaning it.
The person who took it off you is.
If you want to create warriors, you have to teach people to critically think.
And the fact that propaganda, obvious propaganda, is striking fear into the hearts of Americans
shows that there is a lacking of critical thinking.
It's worth remembering that anywhere the United States goes, it has to establish a
rapport with locals, with people in that country.
of injustice all over the world, they're not the same, but they rhyme. Same basic premise,
select a group, keep them down, stack the deck against them. Understanding how it works
in the United States makes it really easy to understand how it works in other countries.
To be clear, this isn't the reason that you should want your children to understand systemic
injustice. That's not the reason you should want it, but if you are that far
into that hyper-nationalism, militancy, then whatever it takes, because if they
learn it, they won't be. They'll understand that nationalism is politics
for basic people. No, I do not think that children should be learning how to
to field strip weapons in class.
I don't think that's necessary.
I think it's far more important
for them to understand how to critically think
about systems of injustice,
about technology,
about the environment.
Because if you do that
and you're effective at it
and we move to a system that is more cooperative rather than competitive,
they're not going to need
to field strip a weapon.
And sending people who in their late teens or early 20s into another country in thinking
they're warriors because they know how to field strip a weapon is appalling.
doesn't make you a warrior. It's a show. It's propaganda and it's working. Some of
Russia's greatest failures was because their troops, their commanders cannot
critically think. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}