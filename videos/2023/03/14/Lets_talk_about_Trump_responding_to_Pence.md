---
title: Let's talk about Trump responding to Pence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aHYOUeH4YCE) |
| Published | 2023/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump is responding to former Vice President Pence's recent statements, indicating that Trump believes Pence is to blame for January 6th.
- Trump claims that if Pence had sent the votes back to legislators, there wouldn't have been a problem with January 6th, despite this not being within Pence's power.
- Trump seems to be trying to shift the blame onto Pence, portraying him as responsible for the events of January 6th.
- The attempt to make Pence accountable for the insurrection demonstrates Trump's mindset of deflecting responsibility and expecting obedience without taking any blame.
- Trump's statement reveals his belief that any negative outcomes are solely due to people not obeying him and that he is absolved of any responsibility.
- Despite not being a fan of Pence, Beau finds Trump's attempt to blame him as concerning and questions whether this is the kind of representation Republicans want.
- The thought process behind Trump's statement raises critical questions for Republicans about the type of leadership they are endorsing.

### Quotes

- "Trying to paint Pence as the person responsible in any way for what happened on the 6th is just wild."
- "And for Republicans, you really need to think about the thought process that has to be behind a statement like this."

### Oneliner

Former President Trump blames former Vice President Pence for January 6th, showcasing his refusal to take responsibility and demanding unwavering obedience.

### Audience

Republicans, political observers

### On-the-ground actions from transcript

- Analyze the thought process behind statements from political figures (implied)
- Question the type of leadership endorsed within political parties (implied)

### Whats missing in summary

Deeper insights into the implications of political leaders deflecting blame and demanding absolute loyalty.

### Tags

#Trump #Pence #Politics #Responsibility #Leadership


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about former President Trump
responding to former Vice President Pence
in his recent statements.
It is the most Trump thing ever.
As you might remember, might know, the former vice president recently, in his strongest
terms yet, said that Trump was wrong and said that, indicated that he believed a lot of
the responsibility for the sixth was Trump's, that Trump kind of made it happen and that
Trump put him and his family at risk, Trump in response said, had he sent the votes back
to the legislators, they wouldn't have had a problem with January 6th.
So in many ways, you can blame him for Jan 6th.
You can blame Pence for Jan 6th.
President Trump seems to be trying to shift the blame to Pence.
If he had just gone along with what I wanted, none of that would have happened.
Which does indicate, at least to me, that Trump's goal was to overturn the election.
the way it seems if he had just done what he was supposed to and send the votes back,
which incidentally is not a power that Pence really has.
The shifting of the blame from himself to Pence, the person who was at risk, I mean
people were chanting pretty direct things about the former Vice President.
the blame to him is just a wild, wild ride. When you really think about how you
have to process things to get to that point where you can honestly say that.
You have to adopt the fashion and the thought process of look at what you
made me do. That's really how it comes across. Trying to paint pence as the
person responsible in any way for what happened on the 6th is just wild and it
shows how Trump's thought process really works and it really is that
anything that negative that occurs is because people didn't obey him. People
didn't do what they were told to do and that he bears no responsibility for
anything. It's a wild statement if you really analyze it, especially given the
that Pence didn't actually have the power to do what Trump wanted. You know,
I'm not somebody who really likes Pence, but this statement is something
else. And for Republicans, you really need to think about the thought process that
that has to be behind a statement like this. And whether or not that really is
somebody that you want representing your party. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}