---
title: Let's talk about Australia and nuclear submarines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5Hq9dw6VJ0c) |
| Published | 2023/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Australia, the UK, and the US have an agreement called AUKUS where Australia will acquire nuclear submarines.
- These submarines are nuclear-powered but not nuclear-armed.
- The propulsion system is nuclear-powered, not for launching nuclear missiles.
- This move is not part of a new nuclear arms race or a significant escalation.
- The submarines will provide Australia with normal, not strategic submarine capabilities.
- China is unlikely to strongly react to this development.
- The US sharing its nuclear propulsion technology with Australia is a unique aspect of this agreement.
- This deal is more about enhancing Australia's defense capability rather than a major strategic shift.
- Australia will receive training and assistance from both the US and the UK for this submarine acquisition.
- The news should be framed as Australia obtaining new submarines, not as a significant escalation.

### Quotes

- "These submarines will not have nuclear weapons on them."
- "Australia is getting new subs."
- "It's not a massive escalation, I don't even know if I'd call this really an escalation."

### Oneliner

Australia's acquisition of nuclear-powered submarines through the AUKUS agreement is not a part of a new arms race or a major escalation, providing enhanced defense capabilities without nuclear weapons.

### Audience

Policy analysts, defense experts

### On-the-ground actions from transcript

- Monitor diplomatic responses to Australia's acquisition of nuclear-powered submarines (implied)
- Stay informed about the implications of nuclear propulsion technology sharing between countries (implied)

### Whats missing in summary

The full transcript provides additional context on the AUKUS agreement and Australia's acquisition of nuclear-powered submarines, offering insights into the strategic implications and potential reactions from China.

### Tags

#AUKUS #Australia #nuclear #submarines #defense #China


## Transcript
Well, howdy there Internet people, it's Beau again. So today we are going to talk about Australia and
submarines and nuclear power and
everything that's going on because some news has
kind of popped and
there are a whole bunch of questions and
And most of them are going to be summed up by a clarification. And it's a
clarification that for once the politicians involved actually tried to
make early on. It just didn't take. So there is an agreement. It is A-U-K-U-S,
however you want to pronounce it. It's an agreement between Australia, the United
Kingdom and the US. In this agreement, Australia will end up with nuclear
submarines. And this is where the question comes in. Almost immediately upon
news breaking, you know, is this part of the arms jog? Is this the start of a new
nuclear arms race? All kinds of stuff. Here's the the most important part that
That is going to kind of stop almost all the questions.
They are nuclear powered, not nuclear armed.
These submarines will not have nuclear weapons on them.
The propulsion system that moves it through the water is nuclear powered.
It's got a little nuclear power plant on the sub, but that's how it moves.
They're not for launching nuclear missiles.
So is it part of a new nuclear arms race?
No.
No, definitely not.
That is not what it is in any way, shape, or form.
Is it part of the arms jog?
I mean, kinda, but not really.
This is something that has been in the works a really long time before tensions really
began to flare, and it's not a massive shift.
It's not something that the Chinese are going to look at and just lose it over.
Nuclear powered submarines, they're quieter, they're stealthier, but as long as they're
not nuclear armed, I don't foresee China really losing it over this.
This is the Australian government obtaining somewhere between three and five nuclear powered
subs. That's kind of it. They'll get training and assistance from both the
U.S. and the British. It is worth noting that I think to my knowledge this is the
first time the U.S. has ever shared any of its nuclear propulsion technology. So
So that is, that's unique.
That's kind of, kind of the bigger news, really.
So at the end of this, Australia is getting new subs.
That's really the way it should be framed, which, yeah, it's more, more defense capability,
more war-making capability, however you want to frame it.
That much is true.
And this isn't a massive escalation, I don't even know if I would call this really an escalation.
This is not a strategic submarine, they're not nuclear armed.
So it provides Australia with normal submarine capabilities, not strategic submarine capabilities.
That's kind of it.
It's not as big of news as it might be, as it might sound.
I mean obviously we'll have to wait and see how China responds, but I don't foresee this
being something that they're really going to care too much about.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}