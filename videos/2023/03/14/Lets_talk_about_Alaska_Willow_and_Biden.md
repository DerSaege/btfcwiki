---
title: Let's talk about Alaska, Willow, and Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=46Qh4NI5N34) |
| Published | 2023/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Willow Project in Alaska, with two main sides: the oil company and environmental groups, both having supporters and opposers.
- Details the approval given by the Biden administration for the project, with the oil company viewing it as a win.
- Notes the compromises made by both sides, with the oil company getting most of what they wanted but having to give up some leases.
- Mentions the environmental groups seeing it as a partial win as they managed to stop some development and gained protections for other areas.
- Analyzes the political and economic realities behind the decision, mentioning the need to keep the economy strong and gas prices down.
- Expresses disappointment in the decision, feeling that it was an missed chance for the administration to lead in transitioning away from fossil fuels.
- Criticizes the compromise made, suggesting that it could have leaned more towards environmental concerns.

### Quotes

- "None of this should have gone forward."
- "This was the opportunity for the Biden administration to make the hard choice and lead."
- "I understand the economics of it. I understand the political realities of it. I get that."
- "I think there could have been, there should have been other options on the table."
- "Y'all have a good day."

### Oneliner

Beau breaks down the Willow Project in Alaska, discussing the political and economic implications and expressing disappointment in the missed leadership moment for transitioning away from fossil fuels.

### Audience

Climate activists, Environmentalists, Policy advocates

### On-the-ground actions from transcript

- Advocate for stronger environmental protections (suggested)
- Support initiatives that lead to a transition away from fossil fuels (implied)

### Whats missing in summary

The emotional weight and detailed rationale behind Beau's disappointment and criticism can be best understood by watching the full video. 

### Tags

#WillowProject #Alaska #BidenAdministration #FossilFuels #EnvironmentalProtection


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about the Willow Project, what
it is, what's happened, the political and economic
realities of it.
We're going to go through kind of all of it real quick.
For frequent viewers of the channel, you already know this.
For newer viewers, this is something
that is important to know.
little bit of a disclosure, to me the climate is a big deal. So that's why we
are breaking this up into coverage and then my personal opinion will be at the
end. Okay, so the Willow Project. It's a massive oil project in Alaska. You had
two sides, main sides, to this. The oil company and a lot of people within the
state were in favor of it. Environmental groups and a lot of people within the
state were opposed to it. The Biden administration gave approval for the
project. The oil company is going to paint this as a win. We're gonna go
through the realities of what happened because both sides in this will paint it
in their own way, and neither are going to be entirely accurate with it.
Oil companies are going to say it's a win, and it is.
They got about three quarters of what they wanted.
It is definitely a win for the oil company.
They did have to give some stuff up, though, to make this through.
First, they're getting three of the four sites, not all of them.
Second, they're giving up almost 70,000 acres of leases
that they've held.
Now, the environmental side is going
to paint it as like a total loss,
because environmental groups, when something like this
happens, they tend to be pessimistic mainly
because they understand the situation we're in.
Is it a total loss for the environmental groups?
No, not really.
They did succeed in stopping about a quarter of it,
getting some of the areas that were going to be leased
or that were leased aren't going to be developed.
And as a consolation prize, the Biden administration
announced protections for a whole bunch of other areas.
I think most environmental groups
going to look at the consolation prize protections and kind of be like yeah
that's great but you know a future president can undo most of this so
they're not going to accept a lot of it but it wasn't a total loss that they
weren't just ignored so definitely a win for the oil company though okay so
So that's what happened, the political and economic realities.
If Biden wants to run again, and this is a clear indication that at this point he definitely
intends on running for re-election, he has to keep the economy humming and gas prices
down.
Because of the global oil market situation, that's in flux.
helps with that. So that was probably a driving factor behind this decision.
Now, the administration has had a decent record on climate up till now. The
pledges about no new drilling. Is there an argument that the administration can
make to say that this really doesn't count because it's been in the works for
I don't know five years or something like that? Can they make that argument?
Yes. Is it going to be accepted by environmental groups? No. Would be
accepted by, I don't know, me? No. But it's an argument that they can make. I don't necessarily
see it that way, but it is an argument that they can make. From the political standpoint,
This was a compromise move from the Biden administration that was good for them politically.
From the environmental standpoint, and this is where we're getting really heavy into my personal opinion,
none of this should have gone forward. None of it should have gone forward.
To me, this was the opportunity for the Biden administration to make the hard choice and
lead and say, we have to break away from fossil fuels.
We have to break away from dirty energy.
And this is a moment to do it, you know, big rousing speech and all of that stuff and try
to get people behind the idea of transitioning.
That is not what occurred.
I don't think this was a great idea.
I understand the economics of it.
I understand the political realities of it.
I get that.
I think there could have been, there
should have been other options on the table.
And I think that the compromise, if a compromise approach
was going to move forward, I think
it could have tilted a little bit more
towards the environmental side, even if it was going to move forward.
I know that the development basically was like, you know, if we fall below this point,
it won't be economically viable.
I find that hard to believe.
And it is worth noting that the Biden administration approved what appears to be the bare minimum
for the company to say that it was economically viable. I don't know that I
agree with the company's assessment on what is economically viable. I think they
could have gone with less, but that is not what happened. So, of the initial plan,
if you have been following this, about three quarters got approved, a quarter of
it got denied. Consolation prizes, some of it are legit wins for the
environmental side, some of it is temporary unless you know it's enacted
in through Congress. So win for the oil company at the same time not a total
loss for the environmental side. At the same time, me personally, I think this was
an opportunity to lead rather than compromise. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}