---
title: Let's talk about David, The Simpsons, and Tallahassee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PZbYah3DZAM) |
| Published | 2023/03/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A school principal in Tallahassee was fired because sixth-grade students saw images of Michelangelo's David and deemed it inappropriate by some parents.
- The connection to a Simpsons episode is made where Marge, an artist, disagrees with censoring Michelangelo, showing how the current world is beyond parody.
- The Simpsons episode aired 30 years ago, showing David from the back and front, something widely watched by children back then but deemed inappropriate now.
- Shielding students from Michelangelo's David in a school promising a classical education is mind-boggling and shows a lack of embracing creativity and art education.
- Beau questions the wildness in schools and the challenges for administrators and teachers in determining what they can teach.
- Art education and embracing creativity are vital for children to understand the world.
- The principal who was fired was given the option to resign but refused.

### Quotes

- "We are shielding our children from the wrong things."
- "A lack of art, a lack of art education, a lack of embracing creativity is going to lead to children growing up who do not understand the world."

### Oneliner

Firing a principal over Michelangelo's David in a school promising a classical education sparks a debate on censorship and the importance of art education.

### Audience

Parents, Educators, School Administrators

### On-the-ground actions from transcript

- Support art education in schools by advocating for comprehensive art programs (implied)
- Encourage creativity and open-mindedness among students by exposing them to various forms of art (implied)

### Whats missing in summary

The full transcript provides a deep dive into the importance of art education and creativity in schools and society. Viewing the entire transcript offers a more detailed analysis and insight into the issue.

### Tags

#ArtEducation #Censorship #Schools #Creativity #Michelangelo #TheSimpsons


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Michelangelo
and the Simpsons in Tallahassee and how all of these things
actually do relate in the most bizarre way.
If you have missed the news, a principal
a school in Tallahassee that promises to provide students with a classical
education that is content-rich, liberal arts and sciences. That principle has
been fired because sixth grade students saw images of David, Michelangelo's David
and this was deemed inappropriate, I guess, and it just kind of went from there,
deemed inappropriate by some parents. I'm not sure how you're supposed to
provide a classical education without the Renaissance, but here we are. There
There are a lot of people who are making the connection to a Simpsons episode.
Because there is an episode of The Simpsons in which Marge, which is the mother if you
don't know, she gets upset about the cartoons her kids are watching.
It's itchy and scratchy, which is basically Tom and Jerry.
And she moves to get the show, let's just say edited.
And the whole move for censorship just spirals out of control and it just goes wild and by
the end of it, people want to censor David.
It's one of those things where the Simpsons predicted this.
Marge, an artist herself, does not agree with censoring Michelangelo.
And yeah, I mean, that's definitely a connection to make and it is funny, especially with all
of the different things that The Simpsons have seemingly predicted over the years.
And it is also funny in the sense that our current world is just beyond parody, even
in The Simpsons.
But I think there's another point that can be made with The Simpsons Tallahassee issue.
episode and it is a real episode it is season 2 episode 9 it aired for the
first time on December 20th 1990 that was so long ago the Soviet Union still
existed was seen by 22 million people on TV do you know what's in that episode
other than the story I just said they drew David they drew David in that
episode from the back and the front and yeah it's there I mean it's the Simpsons
it's not super detailed but it's there. 30 years ago this was something that was
allowed on TV in a cartoon widely watched by children but today it is
apparently too much for sixth graders to handle. When you think about schools
and all of the things that children need to be shielded from at schools, that
there's not a whole lot of action on the fact that at a school that is supposed
to provide a classical education, is shielding their students from David, from
Michelangelo's David, one of the most famous statues in the world, is just
mind-boggling. If you wonder how far this has gone, as far as how wild things have
become in schools and how hard it is for school administrators and teachers to
gauge what they can teach. Think about that Michelangelo's David in a school
that promises a classical education in what I think was an art history class
That's apparently out of bounds.
We are shielding our children from the wrong things.
There is no doubt about that.
And a lack of art, a lack of art education, a lack of embracing creativity is going to
lead to children growing up who do not understand the world.
My understanding is that this principle was given the option to resign and refused.
Good for them.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}