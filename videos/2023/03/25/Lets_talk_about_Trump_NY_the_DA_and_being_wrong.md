---
title: Let's talk about Trump, NY, the DA, and being wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-nFBn6aG970) |
| Published | 2023/03/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's behavior in New York prompts Beau to reconsider his stance on the possibility of Trump going to prison.
- Beau used to be cautious about Trump facing prison time due to his status as a former president, but recent events are making him rethink this.
- The District Attorney in New York received a threatening letter accompanied by white powder after Trump called for protests.
- Trump warned of potential death and destruction if he is indicted, behavior that could be used to justify detention.
- Beau compares Trump's actions to those of a prominent figure from a country the U.S. has biases against, suggesting different treatment.
- Trump's tweets and statements may contribute to a prosecutor's argument for his detention, shifting Beau's perspective on the situation.
- Public opinion differs from legal implications, indicating potential consequences for Trump regardless of public debate.
- Beau suggests that Trump's best interest lies in staying off social media, especially considering the gravity of the threatening letter incident.
- The connection between the threatening letter and Trump could significantly impact any arguments against his potential detention.
- Beau expresses surprise at the unfolding events and expected Trump's lawyers to advise him better on handling the situation.

### Quotes

- "Former President Trump's behavior in New York prompts Beau to reconsider his stance on the possibility of Trump going to prison."
- "Trump warned of potential death and destruction if he is indicted, behavior that could be used to justify detention."
- "The connection between the threatening letter and Trump could significantly impact any arguments against his potential detention."

### Oneliner

Beau reconsiders Trump's potential prison sentence due to recent threatening events in New York, pointing out behavior that could lead to detention.

### Audience

Community members

### On-the-ground actions from transcript

- Contact local authorities to report any threatening or suspicious activities (suggested)
- Support initiatives promoting peaceful interactions and discourse within communities (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of recent events involving former President Trump in New York, prompting a reconsideration of his potential legal consequences.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about former president
Donald J. Trump in New York, and the DA,
and what happened up there and how I'm having
to rethink something because I've had a position
since all of this started and I'm starting to think
I might be wrong about that because
The former president cannot get out of his own way.
Since all of this started, I've been pretty clear about the fact that anytime somebody
asks when is Trump going to prison, I've always been very cautious and saying, I don't know
that he's going to.
He's a former president.
Even if he is found guilty of some of this stuff, I think they'd probably make special
accommodations and maybe put him on home confinement or something like that.
I'm starting to rethink that.
So if you don't know, the DA up there in New York received a letter, a threat, and
it was accompanied by white powder.
Now this is after the former president called for protests.
The former president, even though it was probably said after the letter was mailed, warned
of possible death and destruction if he's indicted.
This is the type of thing that a prosecutor might use in an attempt to justify detention.
Take it out of the Trump scenario for a second and put this same behavior into a different
context.
Pick a country that the U.S. has a long-standing bias against.
You're probably imagining a country in the Middle East.
Imagine a prominent figure in that country calling for action in New York and then a
letter like that showing up.
If there is a connection to be made, the U.S. would probably hold that person who made that
call to action in some way responsible.
That's how it would be viewed.
The latitude the former president receives is in large part due to his expected behavior.
I've talked about it when we were talking about the search.
I was like, you know, I'm pretty sure if he had said nothing, there'd have been nothing.
You know, if he had just let that go, probably wouldn't have turned into a big deal.
And then we found out that there were FBI agents that didn't want to pursue it.
And it certainly seemed to be the case, but when he started tweeting about it and it just
creates a situation where DOJ is kind of forced to act, his tweets may end up being something
that a prosecutor uses as their grounds to request that the former president actually
be detained in a cell.
Now I'm sure a lot of people are going to have opinions about that.
I'm sure there are going to be people who are going to say he should go there anyway,
and some people saying, well no, he didn't really do it, or whatever, and that's fine.
That's an argument that y'all can have, but that doesn't stop the fact that a prosecutor
may attempt to use it for that purpose.
Public opinion is one thing, the legal system is another.
It is probably in the former president's best interest to get off social media.
The letter and powder as reported, that's a huge deal.
That's a huge deal.
If it is somehow tied to the former president, like the person who sent it says, oh I
did it because of this, it's going to be very, very hard to overcome any potential
argument saying, hey, the former president actually needs to be detained.
It's a wild scenario, it's one I did not see coming to be honest.
I honestly expected his lawyers to do a little bit of a better job telling him to be quiet.
statements he's making, calling the DA an animal, there's a whole bunch of them that
may end up having an exhibit and a letter assigned to them.
I may have to rethink the likelihood of him actually ending up in real confinement rather
than just being at home and not being allowed to leave, because a lot of the things that
are currently happening, they're changing the math on that.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}