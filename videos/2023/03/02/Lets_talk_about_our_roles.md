---
title: Let's talk about our roles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aw_7yunWLQg) |
| Published | 2023/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the role of left-wing content creators in bringing forth positive action beyond criticizing the right.
- Talks about the importance of expanding the Overton Window by helping people imagine a better world.
- Emphasizes that everyone has a role to play in advocating for positive change, no matter how subtle.
- Stresses the significance of shifting thought through activities in one's community and online.
- Urges individuals to find their strengths and talents to contribute towards making the world better.
- Encourages making a positive impact beyond just criticizing, by actively participating in activities that bring about change.
- Mentions the discomfort of long-term change but underscores the necessity of shifting thought for progress.
- Acknowledges the role of those who provide commentary on popular culture in shaping perspectives and advocating for a better future.
- Suggests leveraging communities to initiate small actions that can grow over time.
- Emphasizes the value of art, culture, and commentary in influencing societal change.
- Points out that immediate relief work is vital, but long-term change requires shifting thought and advocating for a better world.
- Encourages individuals to recognize their impact, even if it goes unnoticed, in changing the narrative for a better future.
- Advocates for using personal skills and talents to contribute positively to society.
- Stresses the importance of staying motivated and focused on making the world a better place through individual actions.

### Quotes

- "Helping people imagine something outside of that, imagine something better, imagine a world where everybody gets a fair shake, that kind of thing, advocating for that kind of change, even if it's subtle. That's a role, it's a role worth pursuing."
- "Your activities, the things you do in your community, around you, online, all of that, it helps to change the world."
- "You can leverage it to do something small at first and then it grows."
- "But if your goal is to change the conversation and to advocate for something better, to the point where you're sending a message like this, you're putting in good work."
- "Find what you're good at, use that skill, that talent, apply that to making the world better."

### Oneliner

Left-wing content creators and individuals have a vital role in expanding the Overton Window, advocating for positive change, and shifting societal perspectives to envision a better future through their unique talents and actions.

### Audience

Creators and Activists

### On-the-ground actions from transcript

- Advocate for change through content creation and commentary (implied).
- Engage in community activities that bring about positive change (implied).
- Showcase art or cultural events that challenge perspectives (implied).
- Support relief work and funding efforts for immediate assistance (implied).
- Encourage others to imagine a world beyond the current norms (implied).

### Whats missing in summary

Beau's engaging storytelling and personal touch can best be experienced by watching the full transcript.

### Tags

#ContentCreation #Advocacy #PositiveChange #CommunityEngagement #Activism


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about roles.
We're going to talk about roles.
And we'll talk about Windows again.
We're going to talk about the roles we serve.
Over the last couple of weeks, I've
gotten a lot of questions from people
who make content on various platforms.
know, here, a podcast, different places. And the questions are all about content
or about being somebody who makes content, but they apply to everyone. The
questions they're asking, they're kind of universal. They're just framed
specific to making content, and I'll probably work my way through all of the
questions that have come in, but this one in particular I think is worth
addressing now. The person who sent it, their gig, the content that they make, it
It deals with music, but they try to insert, let's just say, more progressive ideas as
they talk about things.
And their question, what role do we serve as a left-wing content creators and what can
we do to bring forth positive action beyond saying the right are awful?
That question though, what role do we serve as left-wing content creators?
What role do we serve as people and what can we do to bring forth positive action beyond
saying the right are awful?
It's a more inclusive phrasing that way.
We talked recently about the Overton Window and how there's an acceptable range of political
thought in any given area.
We didn't talk about why it exists.
In many cases, the edges of the window, they're determined by what people can actually imagine.
They're determined by what people think is possible.
If you make content, particularly with art of any kind, music, literature, sci-fi, it
doesn't matter, video games, it does not matter, if you're making content related to popular
culture in any way shape or form, inserting those messages and helping
people see and imagine what's outside of the window, that things can be better.
that's a pretty important role. You know, for a whole lot of people it's... this is
the way it is and therefore this is the way it always has to be because the
world we inhabit, the system that we live in, it's all around us and it's
self-reinforcing, helping people imagine something outside of that, imagine
something better, imagine a world where everybody gets a fair shake, that kind of
thing, advocating for that kind of change, even if it's subtle. That's a role,
it's a role worth pursuing. Everybody has talents, has skills, things that can be
used to make the world better. Your particular role? Well, you determine that
on your own. And it shouldn't be viewed as competition. Like, I know why I get
questions like this and it doesn't make sense in a lot of ways. Those who can help people
imagine a better world, those who can help people see that it doesn't have to be this
way.
Those people are responsible for the people who show up.
They help shape that frame of mind that things can be better and it encourages people to
get involved.
It's that spear analogy.
And then, you know, making positive change beyond just saying the right was awful, is
that how it's phrased?
Bring forth positive action beyond saying the right is awful.
That's also something that applies to everybody.
Your activities, the things you do in your community, around you, online, all of that,
it helps to change the world.
When you advocate for something, or when you actually get into the real world and start
doing something, beyond just subtly talking about something or bringing
awareness to something. You're making the world better, you're shifting thought.
And on a long enough timeline that change occurs and we win. It shifts the
window, it moves it. That kind of change takes a long time and it's
It's uncomfortable because there are people who need that change right now, today.
But realistically, you have to shift thought.
And if you're one of those people who delves into popular culture and provides commentary
on it and can help explain it, no, this sci-fi show is actually saying that's bad.
you can help people imagine the world that could be, the world that exists
outside of that window, that's a worthwhile role. And if you have a
community of people like that, people who know that something better exists, you
You can leverage it to do something small at first and then it grows.
Doing something in your community and again it's going to be something different every
time if you're into art, you could literally be an art show that showcases things that
shift thought.
You know, the direct stuff, the funding, the relief work, all of that stuff, that's not
the end-all.
That helps.
And many would say it gets the goods, you know, and it helps shift things, and it provides
immediate relief for people who are in a bad way.
But there will be more people in a bad way the next day if it weren't for the people
shifting thought.
Just because your preferred medium of changing the world doesn't get the same kind of
recognition or cred doesn't mean that it's not a worthwhile role.
There are a lot of people who do a lot of good, and they don't always get seen that
way.
It doesn't come off as them shifting thought, it's just content.
But if your goal is to change the conversation and to advocate for something better, to the
point where you're sending a message like this, you're putting in good work.
It's just the kind that doesn't always get noticed.
I would imagine that that applies to a whole lot of people watching this channel.
Shifting thought.
To change society, you have to change the way people think, and it takes time.
But it will take longer if those who are good at that role, who can fill that role, are
of low morale because they feel like they should be doing something more.
Find what you're good at, use that skill, that talent, apply that to making the world
better.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}