---
title: Let's talk about the claims coming out of Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=O2InoUp53j8) |
| Published | 2023/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing claims from Russia about a staged attack by irregular forces crossing from Ukraine.
- Russian government claims a civilian car was hit by irregular forces, calling it a "provocation."
- Ukrainian government denies involvement, with an intelligence officer pointing towards Russian responsibility.
- Mention of the Russian Volunteer Corps potentially crossing the border.
- Beau questions the credibility of Russian state TV and mentions the plausibility of the claims.
- Reference to Russia's distribution of passports in occupied areas as a potential factor.
- Consequences of such incidents being bad for everyone, especially civilians.
- The potential for escalation depending on international reactions and Moscow's stance.
- Ukrainian government's immediate denial of involvement seen as positive diplomatically.
- Uncertainty surrounds the actual events and the credibility of Russian sources.
- Beau expresses concern over the cyclical nature of conflicts once they escalate.
- Emphasis on civilians bearing the brunt of such conflicts.
- Warning about conflicts devolving into prolonged, destructive cycles.

### Quotes

- "It's all bad for everybody."
- "It's always the civilians that get caught up in the middle of it."
- "Russia engaged in a process called passportification."
- "Once something like that starts it tends to become cyclical and it gets bad."
- "Y'all have a good day."

### Oneliner

Analyzing claims from Russia about a staged attack by irregular forces crossing from Ukraine, denials, and the potential consequences for civilians and conflict escalation.

### Audience

International observers

### On-the-ground actions from transcript

- Monitor the situation closely and stay informed about developments (implied).
- Support diplomatic efforts to prevent escalation and protect civilians (implied).

### Whats missing in summary

Analysis of the potential impact on regional stability and international relations.

### Tags

#Russia #Ukraine #Conflict #Propaganda #Civilians


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the claims
that are coming out of Russia.
And these are actually in Russia for once.
We're going to go through what the claims are.
We're going to go through whether or not they are fact,
whether or not they are likely.
We're going to go through what Ukraine says about it
and kind of try to determine
whether or not this is plausible, possible, all of that stuff and what it
means if it is true. Okay, so if you have no idea what I'm talking about, the
Russian government has said that a group of, at this point we will call them
irregulars, crossed from Ukraine into Russia and staged something, some kind of
attack. Now, what that attack was, it's not exactly clear at this point. What is
known at time of filming as far as the Russian position is that something
happened and then a civilian car got hit by irregular forces. They are throwing
out the t-word. Okay, so what's Ukraine saying? It's not us. The Ukrainian
government is saying this is not us. If it happened, it's a setup, it's a
provocation, this has nothing to do with us. A Ukrainian intelligence officer kind
indicated that it was Russians doing it. There is a group like the Russian
Volunteer Corps or something like that that that appears to have at least said
that they're engaging in crossing the border. Okay, so those are the the claims
those are the sides. What's true? I generally don't believe anything just
because Russian state TV says it. I need something more than that. In this case,
it's plausible, especially when you factor in the information coming
from the Ukrainian intelligence officer and him saying that it was a Russian
group. It is actually plausible. About 11 months ago, I'll put the video down
below, we talked about this. We talked about this exact possibility. Russia
engaged in a process called passportification. Passportization
maybe. Either way, they distributed Russian passports to people in occupied
areas, assuming that they were loyal. That may not have been a good idea, and we
talked about in that video how that might be used by people who were engaged
in this kind of behavior. So while I don't necessarily believe the Russian
reports because I don't they have engaged in a lot of propaganda as all
sides do during wartime I don't necessarily believe any of it but does
the story track is it plausible yeah it really is using those earlier scales
moderate confidence, I would say. So if it did happen, what's this mean? It's all
bad. It's all bad for everybody. Generally when groups like this emerge,
they don't take into account civilians, and that's not good.
That's not good. This is the type of thing that can definitely escalate the
situation depending on what it was, how big it was, how the world powers decided
to react to it, whether or not Moscow really wants to apply that T word to it
and try to make that case because that changes things.
The Ukrainian government disavowing it right away, good.
I mean, you know, their statement is that it didn't happen, it's a provocation, it's
a setup.
But the subtext of that is no matter what, it wasn't us.
we don't support it is not really the subtext but diplomatically that's what
it means. So at this point it's incredibly early whether or not things
went down the way anybody is claiming is still up in the air but the story coming
out of Russia which a lot of people are going to want to instinctively
disbelieve simply because it's coming from Russian press and they haven't had a good track record
of honesty lately, it actually is very plausible. It's something that definitely could have happened.
It's something that we talked about on this channel in this exact fashion. People with
Russian passports crossing the border 11 months ago. I really hope that that's not what it is.
I really do because once something like that starts it tends to become cyclical
and it gets bad. It gets bad and it goes back and forth and once that starts it's
always the civilians that get caught up in the middle of it. They're the ones
who pay the price and it signals yet another devolving of the conflict into
something that will last a very, very long time.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}