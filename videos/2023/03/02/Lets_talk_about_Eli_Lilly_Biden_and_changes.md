---
title: Let's talk about Eli Lilly, Biden, and changes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=a-3CE7RGMZ0) |
| Published | 2023/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Eli Lilly and Biden's unexpected good news: capping insulin at $35 per month out-of-pocket expenses.
- Eli Lilly bringing their program in line with the Inflation Reduction Act for all, not just seniors.
- Private insurance automatically applying the $35 cap, while uninsured individuals can register for Eli Lilly's Co-Pay Assistance Program.
- Biden administration likely to take credit for the connection between the Inflation Reduction Act and the insulin cost reduction.
- Eli Lilly's decision likely to pressure other companies in the market to follow suit due to their significant market control.
- Potential for other major companies to announce similar cost reductions, ultimately lowering insulin prices nationwide.
- Lilly's proactive approach to adapt voluntarily before facing heavy regulations amidst shifting demographics and increasing calls for regulation.
- Addressing overpriced medications as a first step towards more affordable healthcare and a shift away from profit-driven patient care.
- Importance of universal healthcare access to move towards a system where everyone is entitled to healthcare, not just the wealthy.
- Encouragement for diabetics to stay informed about the cost reduction and be aware of the company producing their insulin product.

### Quotes

- "Insulin is going to be capped at $35 per month out of pocket expenses."
- "This is a huge win."
- "It's kind of like a peace offering."
- "Once it's recognized as something that everybody should have, rather than, well, that's just something that rich folk get."
- "Y'all have a good day."

### Oneliner

Eli Lilly and Biden team up to cap insulin costs at $35 per month, signaling a potential industry-wide shift towards more affordable healthcare for all.

### Audience

Advocates for affordable healthcare

### On-the-ground actions from transcript

- Stay informed about the cost reduction and updates on insulin pricing (suggested)
- Identify the company producing your insulin product and monitor for cost changes (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the impact of Eli Lilly's decision on insulin pricing and the broader implications for healthcare affordability and regulation.

### Tags

#Healthcare #Affordability #Insulin #Biden #EliLilly


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Eli Lilly and Biden and some good
news that was pretty unexpected.
Um, but this is going to be massive for a whole lot of people.
Insulin is going to be capped at $35 per month out of pocket expenses.
This isn't just for seniors, Eli Lilly has decided to bring the rest of their
program in line with the Inflation Reduction Act. If you have private
insurance, this is going to happen automatically. If you are uninsured, Eli
Lilly has their own program, I actually think it's called the Co-Pay Assistance
Program, Eli Lilly Co-Pay Assistance Program, where you register and you're
going to get that same benefit. $35. Now, the Biden administration is obviously going
to be like, look what we did. Because the connection between the Inflation Reduction
Act and this, I mean, people aren't going to be able to ignore that. And it's not over.
I don't think that this is over yet.
I think there's more to come.
Mainly because, well, Eli Lilly controls
about 30% of the market.
This applies to their products.
They're one company that has decided to do this.
But they're a huge company.
And they control about 30% of the market.
The other companies, they're going
to feel a whole lot of pressure to follow suit.
I don't think that this is going to be the last company to, uh, to make an announcement
like this, this is, this is one of those things that will end up being a, a campaign
piece for Biden, because by the time it's all said and done, I would imagine that the
two other big companies, they're going to, they're going to do the exact same thing and
He will have lowered the cost of insulin for everybody in the country.
That's a pretty big win.
Now, Lilly, honestly, it just seems like they saw what's coming.
They see what's coming and they're trying to adapt to it.
They're trying to be first.
They're trying to do things voluntarily before heavy regulation comes in, which, let's be
honest, there's a lot of people calling for it and demographics are shifting.
As more and more younger voters get to that point, companies are going to want to do things
voluntarily to avoid the regulations.
It's kind of like a peace offering.
cool, let's do happy pins next and keep it going. There are a lot of meds that
are ridiculously overpriced and this is a first step in getting to where, in
in getting to a place where health care isn't about draining
every dime they can out of the patient.
And from there, once the US reaches that point,
everybody having health care, it's a whole lot easier
to get to.
once it's recognized as something that everybody should have, rather than, well, that's just
something that rich folk get.
This is a huge win.
If you are a diabetic, be on the lookout for all the information that's going to be coming
out about this and see which company makes the product you use.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}