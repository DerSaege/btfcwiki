---
title: Let's talk about the highest and lowest paying majors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8rr1_eadhDc) |
| Published | 2023/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing college majors and their corresponding salaries within five years of graduation to understand societal values and trends.
- Top 10 highest paying majors include engineering and business analytics, ranging from $65,000 to $75,000 annually.
- Lowest paying majors consist of education, social services, and performing arts, ranging from $36,000 to $40,000 yearly.
- Signifies a societal issue in undervaluing professions like education, mental health, and social work.
- Implies a lack of critical thinking and information processing skills in society.
- Educators are not adequately compensated for their vital role in society.
- Points out the discrepancy in values between high-paying and low-paying majors.
- Expresses concern about the devaluation of critical professions like education and mental health.
- Criticizes the lack of financial support and respect given to educators.
- Raises awareness about ongoing attacks on the education sector by legislators.

### Quotes

- "We do not value the thing we need the most."
- "Educators don't go into it for money, but there very well might be people who don't go into it because of money."
- "We have an extended problem in the United States with a whole lot of people having issue with processing information, the ability to critically think."
- "Social work? Nah. Mental health? Nah. Education? Nah. None of that stuff's important."
- "This is just identifying a symptom of a disease."

### Oneliner

Analyzing college majors and their salaries reveals societal values, showing undervaluation of critical professions like education and mental health.

### Audience

Students, educators, policymakers

### On-the-ground actions from transcript

- Advocate for fair compensation for educators and professionals in critical fields (implied)
- Support initiatives that prioritize education and mental health funding (implied)

### Whats missing in summary

The full transcript provides a deeper dive into the societal implications of salary discrepancies among college majors. Watching the full video can offer more insight into the speaker's perspective on valuing critical professions.

### Tags

#CollegeMajors #SalaryDiscrepancy #Education #SocietalValues #Undervaluation


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about college majors
and how much they pay within five years of leaving college.
And what it can tell us about the shape of the country
and where the country is going to continue to trend.
Saw this little bit of information
And just, it's pretty stark.
So the research itself was done by the Fed.
Now, the top 10 majors by median salary
within five years of graduation, chemical engineering,
computer engineering, computer science, aerospace engineering,
electrical engineering, industrial engineering,
mechanical engineering, miscellaneous engineering,
business analytics, and civil engineering.
No real surprises there.
These range $65,000 to $75,000.
Not a lot of shocking majors here.
I think most people know these are good jobs right away,
But let's look at the lowest pay.
Theology and religion, social services, family and consumer
sciences, psychology, leisure and hospitality,
performing arts, early childhood education, elementary education,
special education, miscellaneous education.
These range from thirty-six to forty thousand a year.
It's a lot of education jobs.
A lot of education jobs
as the lowest paid
majors
coming out of college.
That's uh...
pretty telling
about our society, right?
Really shows what we value,
what we don't value.
We have
an extended problem
in the United States
with a whole lot of people
having issue
with processing information,
the ability to critically think,
and
when you look at
the resources
that educators are given, how they are compensated for their time.
You can't really be surprised.
Educators don't go into it for money,
but there very well might be people who don't go into it
because of money.
You can look at this
and see
what our society values. Social work? Nah. Mental health? Nah. Education? Nah. None of
that stuff's important. They're all in the top ten lowest. That's a problem and it's
going to make things worse, especially given the fact that we have attacks on education
going on daily from legislators.
Because, let's be honest, they're an easy target.
They're an easy target because they don't have a lot of money.
Can't really compete politically
because money is speech.
I don't really have
a
solution to this.
This is just identifying a symptom of a disease.
We do not value the thing we need the most.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}