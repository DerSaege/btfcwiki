---
title: Let's talk about being snowed in and prepared.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tolRbcL7aUs) |
| Published | 2023/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People in California might be snowed in for 10 days or up to two weeks with no relief in sight.
- It's a reminder to have an emergency kit ready because emergencies can happen anywhere.
- The minimum essentials for the kit include food, water, fire, shelter, a knife, and a first aid kit.
- Food for at least two weeks is necessary, it can be canned goods.
- Water should be either purified, filtered, or bottled based on what's best for the area.
- Shelter preparations are vital, like having a giant blue tarp for the roof during hurricane season.
- Fire means literal fire and light sources like flashlights or lanterns are included.
- Electronic backups like battery backups for phones are recommended.
- A knife is an indispensable part of the emergency kit.
- Having a first aid kit with necessary medications is vital during extended emergencies.
- Keeping all these essentials together in one accessible place is key.
- In case of being snowed in, having essentials ready to move with you is necessary.
- Having these basics covered can make a significant difference during disasters.
- Making the emergency kit more comprehensive is possible with additional resources.
- It's emphasized that having this kit during hurricanes makes a huge difference for preparedness.

### Quotes

- "Food, water, fire, shelter, first aid kit, a knife."
- "If you don't have a kit put together take this as your sign to go ahead and get that done."

### Oneliner

Be prepared for emergencies with essentials like food, water, fire, shelter, a knife, and a first aid kit; having them together can make a huge difference during disasters.

### Audience

Homeowners, residents

### On-the-ground actions from transcript

- Prepare an emergency kit with food, water, fire sources, shelter materials, a knife, and a first aid kit (suggested).
- Ensure all essentials are kept together in an accessible place (suggested).

### Whats missing in summary

The full transcript provides detailed guidance on preparing an emergency kit for various emergencies and stresses the importance of having all essentials together for easy access during disasters.

### Tags

#EmergencyPreparedness #DisasterResponse #CommunitySafety #HomePreparation #California


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about snow
and being snowed in.
There are people in California who look as though
they will be snowed in at least 10 days, maybe two weeks
with no real relief on the way during that period.
So, of course, this serves as the opportunity
for the periodic reminder to get all of your stuff together.
Doesn't matter where you live, there
is some kind of issue that can arise for you,
some kind of emergency that can leave you
cut off for a period of time.
And this is that, I guess at this point,
probably quarterly reminder to have your stuff together.
Have an emergency kit.
Have it prepared.
minimum, you need food, water, fire, shelter, a knife, and a first aid kit. Food. At
least two weeks. At least two weeks. You need to try to get that together. It
doesn't have to be great. You're not talking about... you're not talking about
what you would need to, you know, cook a giant dinner every night, but you need to
have food for everybody in the house for at least two weeks. It could be canned
goods, which are really inexpensive. Water. Either a way to purify water, filter it,
or bottled water. Whatever it takes, whatever it is best for your area.
Shelter. You should probably, depending on your area, have some way to make
shelter or to close up any holes that might exist in your home. That's a big
one here. Where I live, everybody has a giant blue tarp for their roof during
hurricane season. Make sure that you have that together. Fire. Now, fire is
obviously literal fire, but it also means light. It could be flashlights, lanterns,
whatever. I would also tend to include electronic stuff in this. Battery backups
for your phone because maybe the phones actually work. Those are relatively
inexpensive. You can have a bag of charcoal that sits over your shoulder
for years as a backup to cook in case electricity goes down. Then you need a
knife of some kind in your home. You should probably already have one but you
need all of this stuff together and then you need a first aid kit because an
extended period during a disaster or some kind of emergency you don't have
access to emergency services so having a first aid kit on hand really good idea
this also includes meds you need a supply of any meds that you you may need
and you try to get all of it together and keep it all in one place. One of the
big things when you talk about this with most people is, I have all of that in my
home, but is it all together? Is it all somewhere that you can access it in an
emergency? Remember, as let's take being snowed in as an example, if there's
enough snow on that roof it may come in and you may have to move to a neighbor's
house. You want to be able to take your stuff with you. So food, water, fire,
shelter, first aid kit, a knife. Make sure you have those things covered at bare
minimum. It can be more in-depth and there's a bunch of videos about how to
make it more in-depth, but this is one of those reminders that I like to put out
every once in a while because when you go into a hurricane impacted area, the
difference in in the situations between people who had this and people who
didn't is huge. So if you don't have a kit put together take this as your sign
to go ahead and get that done. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}