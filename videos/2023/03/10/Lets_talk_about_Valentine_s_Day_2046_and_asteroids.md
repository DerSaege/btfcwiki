---
title: Let's talk about Valentine's Day 2046 and asteroids....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ijtenqpKpHs) |
| Published | 2023/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An asteroid near Earth in 2046 is causing a buzz due to its size and potential impact.
- The asteroid, measuring 162 feet, has a small chance of impacting Earth according to the Torino scale.
- The current chance of impact is around one quarter of 1%, making it highly unlikely.
- NASA analysts believe the public shouldn't be concerned about this asteroid.
- NASA conducted the Double Asteroid Redirection Test (DART) in 2022 to prepare for scenarios like this.
- With 20 years warning, NASA could potentially develop a plan to redirect the asteroid if the odds increase.
- Despite the low chances of impact, the asteroid's size is large enough to cause major localized issues.
- DART was NASA's first attempt at planetary defense, showing the importance of investing in such technologies.
- Politicians and policymakers should recognize NASA's role as a defense line against potential asteroid impacts.
- NASA's budget should be considered vital for research and development to address planetary defense needs.

### Quotes

- "This is something that could definitely cause major localized issues."
- "Asteroids have hit the Earth in the past. They're going to hit in the future unless they're stopped."
- "NASA gets answers through research, conducts research with funding."

### Oneliner

An asteroid near Earth in 2046 raises concerns, but NASA's preparedness and research alleviate potential impacts and underscore the importance of planetary defense funding.

### Audience

Space enthusiasts, policymakers, concerned citizens

### On-the-ground actions from transcript

- Support funding for NASA's planetary defense research and technologies (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of the potential impact of an asteroid near Earth in 2046, NASA's preparedness through initiatives like the Double Asteroid Redirection Test (DART), and the importance of investing in planetary defense technologies.

### Tags

#Space #NASA #Asteroid #PlanetaryDefense #Research #Funding


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit about space.
And Valentine's Day in 2046, because it's
getting a bunch of headlines, so we're
going to kind of run through it.
There is an asteroid that is going to be near Earth.
It is 162 feet or so, which is big enough to be concerning.
That's pretty large.
And there are a lot of headlines saying
that it runs a chance of impacting Earth.
On the Torino scale, which is what
measures how dangerous it is, it is a 1 out of 10.
There is a chance.
That chance, as currently rated, is about one quarter of 1%.
It is a very small chance that it impacts Earth in 23 years.
Now, the odds of it actually impacting are very, very low.
This is something that, according to NASA's analysts,
this isn't really anything that you get public attention
or the public should be concerned about.
It's just, it is incredibly unlikely.
But given the fact that there are a bunch of headlines
about it and people will definitely lean into it
because this is the first time in recent memory
where NASA was like, yeah, this one actually kind of does
have a chance of hitting us.
We'll go ahead and remind everybody
that in September of 2022,
there was the DART test, Double Asteroid Redirection Test,
where NASA engaged in a proof of concept
to use a space battering ram to knock an asteroid off
of its path in the event that something like this came to be.
And given the fact that they've got 20 years warning,
They probably stand a pretty good chance
of putting something together if it does increase in odds,
as far as hitting the Earth.
The other thing to take away from this
is that when the dark test was going on,
when that was happening, there were a whole lot of people
asking why.
Why are we wasting this money seeing
if we can move an asteroid.
This is why.
In this situation, looking at the odds,
it is very, very small.
And by the way, when I say it's enough to be concerning 162
feet, this is not a planet killer.
This is something that could definitely
cause major localized issues, though.
So there are a lot of times when NASA
engages in something and they spend a whole lot of money doing something.
DART was kind of our first real attempt at planetary defense, first real
proof of concept to deal with this situation.
Asteroids have hit the Earth in the past. They're going to hit in the future
unless they're stopped. So when politicians or when whoever talks
about NASA's budget, just remember that when it comes to stuff like this, they're
kind of the only line of defense. If that percentage was higher, you would have
people demanding that NASA have answers. NASA gets answers through research,
conducts research with funding.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}