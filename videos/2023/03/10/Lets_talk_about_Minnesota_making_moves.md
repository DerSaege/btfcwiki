---
title: Let's talk about Minnesota making moves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IPQ3xJLB7PA) |
| Published | 2023/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Governor Tim Walz's executive order in Minnesota aimed at protecting the LGBTQ community from harmful legislation.
- Mentions how the legislation in various states targets and marginalizes the LGBTQ community to motivate certain groups to vote.
- Describes the impact of the executive order in Minnesota on preventing the state government from assisting in enforcing harmful laws targeting the LGBTQ community.
- Speculates on the reasoning behind the governor's choice to use an executive order instead of waiting for pending legislation.
- Talks about potential long-term impacts of the executive order, including economic development and possible Supreme Court challenges.
- Suggests that other states might follow Minnesota's lead in protecting the LGBTQ community.
- Points out the potential legal challenges to enforcement mechanisms of harmful legislation under the Interstate Commerce Clause.
- Appreciates the governor's proactive stance in implementing the executive order to protect LGBTQ individuals.

### Quotes

- "Minnesota is going to be a place where people from the LGBTQ community can go and be okay."
- "A lot of this legislation is moving forward. This legislation, it's not actually designed to do anything inside the state."
- "So the governor of Minnesota has decided that Minnesota is a free state."
- "This isn't a PR stunt in that sense. This is the state government of Minnesota saying, oh, your extradition request? It's not valid here."
- "The governor of Minnesota stepped up and put this into action when it really needed to be."

### Oneliner

Governor Tim Walz's executive order in Minnesota protects the LGBTQ community from harmful legislation, setting a precedent for other states to follow and potentially leading to economic growth and legal challenges.

### Audience

Advocates, legislators, activists

### On-the-ground actions from transcript

- Contact LGBTQ advocacy organizations to support and amplify their efforts (implied)
- Join local LGBTQ rights groups to stay informed and engaged in similar initiatives (implied)
- Attend community meetings or town halls to voice support for LGBTQ protections (implied)

### Whats missing in summary

Detailed examples and elaboration on how harmful legislation impacts the LGBTQ community directly.

### Tags

#LGBTQ #GovernorWalz #ExecutiveOrder #LegalProtection #CommunitySupport


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about the governor of Minnesota,
Governor Tim Walz, and the executive order that was signed, what it means,
what it actually does, why it was signed now, whether or not
there's pending legislation coming, everything about it, and
what the real impacts of it are going to end up being long term.
Now, if you have absolutely no idea what I'm talking about,
it's kind of not surprising.
So as we have mentioned numerous times on the channel,
there are a bunch of pieces of legislation
throughout the country, about 400 in total.
And they are aimed at marginalizing, targeting,
harassing the LGBTQ community with the goal
of legislating them out of public life in various ways.
A lot of this legislation is moving forward.
This legislation, it's not actually designed
to do anything inside the state.
It doesn't do anything to benefit the state.
It's all about targeting and providing the base
a scapegoat, somebody to be angry at
because anger motivates people to go to the polls.
If you scare them, fill their head with nonsense
about how they're at risk or whatever,
they feel like they have to show up.
So, every election cycle, certain groups of people
create an enemy and create situations
out of whole cloth, things that don't even exist.
And they say that they'll pass legislation to do that,
to curtail this made-up problem.
And that's happening in a lot of places.
This is heavily impacting the LGBTQ community
in various ways.
Because the legislation is designed in a way
where the cruelty is the point, a lot of them
have enforcement mechanisms that go well
beyond anything that a state government should really be doing.
So the governor of Minnesota has decided that Minnesota is a free state.
That Minnesota is going to be a place where people from the LGBTQ community can go and
be okay.
Now it's easy for a state to say something like that.
an easy call to make and say, hey, we're going to be welcoming.
This executive order prohibits the state government there from assisting and cooperating with
those enforcement mechanisms.
This isn't a PR stunt in that sense.
This is the state government of Minnesota saying, oh, your extradition request?
It's not valid here.
That's subpoena?
Yeah, I mean, I guess you could try to serve it if you want to, but we're not making them answer it and it's
degrading the
the ability of this legislation from other states. Now,
why did the governor do it via executive order instead of legislation?
Legislation's on the way. The executive order
was pushed through and done quickly.
I guess their reasoning behind it is because some of the legislation
targeting this community is passing.
They want this in place now.
So there are no issues now.
There is legislation pending in the state legislature in Minnesota
that accomplishes the same thing, that would make it law.
And I haven't seen odds on it passing,
But when you hear people talk about it,
they seem pretty optimistic.
It does look like it's moving forward.
Now, is this going to be the only state to do this?
Probably not.
I would imagine that other states will follow suit.
What are the long-term impacts of this?
Well, you would hope that one thing happens.
If it doesn't, you have to go to the even longer term.
If this situation remains and the longer-term impacts apply,
Minnesota is going to get more business.
Minnesota is going to get more recruitment to its colleges.
Minnesota is going to get more investment.
It's going to be easier for businesses in Minnesota
to recruit a place that, I mean, it's Minnesota.
Let's be honest.
It could probably use some economic development.
This is going to help spur it.
Businesses won't take a hit for investing there.
They won't have problems recruiting
because they put an office there.
So it will help them long term.
Hopefully, in the shorter term, it's
setting up a situation where this stuff goes
to the Supreme Court.
It sets up a situation where the legislation that
is past targeting people, then ends up in court.
And a lot of the enforcement mechanisms,
I don't think that they'll stand up, even under this court.
And in fact, in some ways, especially under this court.
A lot of this has to do with people leaving a state
to engage in business.
There is a very specific part of the Constitution
that deals with that, the Interstate Commerce Clause.
I do not believe that a lot of this legislation
is going to hold up.
And by that, I mean the enforcement part of it.
What they are doing within their state, that, sadly,
And I think a lot of that is going to be upheld.
But the reaching across state lines, not so much.
We're going to have to wait and see.
But this is something that is very much a conservative piece of like a keystone thing
for them is the Interstate Commerce Clause.
So I don't believe that those aspects of it will hold up, but this provides, A, a mechanism
to get it into the court because you're going to have a battle between the states, and B,
it provides people much needed protection right now.
So the governor of Minnesota stepped up and put this into action when it really needed
to be.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}