---
title: Let's talk about an enduring math problem in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LicT6OA64R4) |
| Published | 2023/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of an unsolved mathematical problem in Ukraine and a new piece of information related to it.
- Mentions the High Mars vehicle system provided by the West to Ukraine, which has been effective against Russian forces.
- Explains the discrepancy in the number of High Mars systems sent versus the number destroyed by Russia.
- Reveals that a Czech company started producing inflatable versions of military vehicles, including High Mars.
- Suggests that Russia may have destroyed inflatable decoys, thinking they were actual High Mars systems.
- Points out that the use of inflatable decoys is cost-effective and creates confusion among opposition forces.
- Concludes that the situation clarifies why the numbers reported by both sides seemed exaggerated and implausible.

### Quotes

- "Russia might not have been lying that they very well may have hit these targets that they believed were HIMARS, but they were the inflatables."
- "It is now confirmed that not just are they being used, there are HIMARS versions that are being used in Ukraine right now."
- "Our most likely answer at this point to this, you know, as of yet unsolved math problem that Russia has blown up a whole bunch of bouncy house highmars."

### Oneliner

Beau sheds light on inflatable decoys confusing Russia in Ukraine, unraveling the High Mars mystery.

### Audience

Military analysts, Ukraine supporters.

### On-the-ground actions from transcript

- Verify information on military equipment before making claims (implied).

### Whats missing in summary

The full transcript provides a detailed analysis and explanation of a complex situation involving military equipment in Ukraine, offering insights into potential misinformation and the use of decoys in warfare.

### Tags

#Military #Ukraine #HighMars #Russia #InflatableDecoys


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about one of the great
unsolved mathematical problems of the situation in Ukraine
and how the solution may have finally presented itself.
It's one of those things where it was very hard to determine
but it didn't matter which side you were really on
and how you felt about it,
If your analysis was even remotely honest,
you had to admit that either side could just be lying.
But now we have a new piece of information
that sheds a little bit more light on what
might have been happening.
So today, we are going to talk about how H is for High Mars.
Okay, so, High Mars, it's a vehicle, it's a system.
Say, picture it like a rocket launcher system
on the back of a truck, the West provided Ukraine
with a bunch of these.
Not a whole bunch, but a few, and they have used them
have used them to great effect. They have just wreaked havoc on Russian
forces. Now, Russia has repeatedly claimed that it has destroyed some of these, so
much so that if you were to add them up, they have destroyed more than the West
says that they sent which is I mean that's I mean that that can't have
happened right now at this point you have to acknowledge that either side
could have lied the West could have said that they sent less than they did so
Russia wouldn't be looking for all of them totally in bounds for an information
operation Russia could be lying about how many they hit because it's good for
morale because this is a feared weapon, totally in bounds for an information
operation. Most people believed that Russia was lying because of a briefing
and I will go ahead and tell you this is a mistranslation. There was one instance
in which a briefer was like, hey we destroyed 41 or 44 High Mars or
something like that and it's written behind them. It appears that they were
talking about the number of rockets not the number of launchers but once that
hit you know social media everybody's like well Russia's just making stuff up
now and the whole thing just kind of became a running joke. Any time Russia
said they blew one up they're like well you already blew them all up like more
than twice because they had claimed to have destroyed so many more than were realistically
sent even if the US and the West was lying about how many they sent.
Okay, so there's a new piece of information and we get to understand why these things
have been coming back like Michael.
There is a Czech company that makes inflatables of military vehicles.
If you've never seen one of these things, picture it like a bouncy house, but it looks
like a military vehicle.
There's a fan inside of it with a power source, and it blows it up.
It's the correct size.
The more advanced ones, the power source and fan that sit inside, they actually mimic the
heat signature of the real vehicle.
they're kind of a big deal. You put them out and it confuses the opposition. This
Czech company has acknowledged that sometime in the last year they started
producing HIMARS. So Russia might not have been lying that they very well may
have hit these targets that they believed were HIMARS, but they were the
inflatables. This is one of those moments where everybody could have been
assumed to be lying and it looks like everybody's telling the truth, at least
to their perspective. You know, if Russia targets something that looks like a high
Mars and destroys it, they are going to claim that it's a high Mars even if it
was an inflatable system.
That appears to be what has happened.
And the inflatables is being widely covered in Europe,
so I don't feel like I'm letting anything out of the bag here.
So these systems, these inflatable systems,
they're relatively inexpensive.
They cost way less than the missile we used to destroy them.
It also creates a situation where the opposition force is confident it has destroyed something
that it may not have.
And then you run into a situation like what is happening right now, where once it is discovered,
they become reluctant to actually fire on a perceived target because they don't want
to look silly and blow up a balloon.
These are really inexpensive and really effective tools, and it is now confirmed that not just
are they being used, there are HIMARS versions that are being used in Ukraine right now.
So that explains why Russia has destroyed 172 of the 18 high-mars that were sent or
whatever.
I don't remember the exact numbers.
But it clears that up and it makes a whole lot more sense than anybody trying to run
an information operation and being that off with the numbers.
an information operation to be successful, it has to be at least close to the truth.
And these numbers were getting ridiculous on both sides.
So this is kind of the only thing that explains what has happened and creates a situation
where anything becomes plausible.
Our most likely answer at this point to this, you know, as of yet unsolved math problem
that Russia has blown up a whole bunch of bouncy house highmars. Anyway it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}