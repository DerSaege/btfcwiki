---
title: Let's talk about nuance, context, and candy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=N0rsBxyV-tI) |
| Published | 2023/03/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the lost nuance and context in a serious tone after receiving messages about a previous video.
- Mentions a message questioning the representation of a trans woman on Hershey's package.
- Explains that there were actually five packages featuring different women, but only one was being focused on.
- Connects the issue to a broader push targeting specific groups with legislation in the United States.
- Points out that companies like Hershey's are intentional in their marketing to appeal to a younger LGBTQ demographic.
- Suggests that Hershey's was aware of the possible controversy and embraced it for advertising benefits.
- Emphasizes that the lost context and nuance in the candy bar situation is not real, and it accurately mirrors the US demographics.

### Quotes

- "There is a widespread push in the United States at the state level targeting a specific group of people."
- "These companies are marketing to the younger generation."
- "The lost context, the lost nuance, it's not real."
- "They have a problem with one, and that accurately reflects the demographics in the United States."
- "I just said woman rather than trans woman."

### Oneliner

Beau addresses the lost nuance and context surrounding Hershey's candy bars, revealing the intentional marketing strategy and broader implications of representation.

### Audience

Marketing Professionals

### On-the-ground actions from transcript

- Contact LGBTQ+ advocacy organizations to support inclusive marketing practices (implied).
- Educate others about the importance of diverse representation in marketing campaigns (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the nuances surrounding the representation of a trans woman on Hershey's package, along with the broader implications of intentional marketing strategies targeting specific demographics.

### Tags

#Representation #Nuance #Context #Marketing #LGBTQ #Inclusivity


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about nuance,
maybe loss nuance, context, and candy bars.
This time, it's going to be a little bit more serious
of a conversation.
After that last video about what's going on with Hershey's,
there were quite a few messages, a whole bunch.
One, I actually believe is in good faith.
So we're going to read that message,
and we're going to kind of talk about the perhaps lost nuance.
OK.
I loved your Eminem and Hershey videos,
but I feel like you dropped a bit of nuance
in the Hershey video.
I went to look for their perspective.
Maybe you didn't know, but they put a trans woman
on the package.
you said Hershey simply put a woman on the package and highlighted the she and her.
Maybe some people feel that using a trans woman erases other women on a women's holiday.
Maybe it would be better to specify trans woman instead of leaving it open to interpretation.
The reason I think this is in good faith is that phrase, other women, including trans
women in women, right? As somebody who got a whole bunch of these messages, that is something
that stands out as different from the rest of them. Okay, so here's the thing about this.
You went and you looked for more context, more perspective, all that stuff. Good. This
is what I was talking about in that video when I said some people are going to say there's
There's more to it than that, but there's really not.
This is exactly what I was talking about.
You went and looked for the context.
The problem is you didn't find it.
Not really.
You got their perspective.
You got their talking point, but you didn't get the real context.
What if I told you that there were five packages, different women, five of them, but they're
only talking about one, right?
that particular group of people, they don't want them in public life. It's that
simple. It is that simple. There is a widespread push in the United States at
the state level and it is targeting a specific group of people with
legislation trying to make it impossible for them to just go about their lives.
They want to make sure that they're not in public. It is a common conversation
now within this demographic of people to talk about what state you may have to
move to, where they may have to flee to, just so they can go about their lives
even if they're not hurting anybody. The situation with the candy bars really
reflects that. That they didn't bring up the other four packages, just the one.
And the framing is that it's erasing other women, to use your term.
Interesting thing, as we've talked about this, we always tie back to marketing, because that
really is what it's about.
These companies, they're marketing to the younger generation.
How many of the younger generation do you think identify as LGBTQ?
I don't have the numbers for Canada, but in the United States, one out of five.
Five rappers.
Marketing companies, they've paid real close attention to demographics.
I'm willing to bet that's not an accident.
That is not an accident, it's intentional.
And I'm also willing to bet that Hershey's totally knew that this desktop was coming.
I mentioned that before.
And given the fact that they have stood by this decision the way they have, yeah, I'm
pretty sure they knew it was coming.
They knew it was going to give them a bunch of advertising.
They knew it was going to reach that one out of five and their friends of that younger
generation, which is their future base customers. So the lost context, the lost
nuance, it's not real. There are five wrappers. They have a problem with one.
And that accurately reflects the demographics in the United States. I
assume Canada's is pretty close now as far as this other part maybe it would be
better to specify trans woman instead of leaving it open to interpretation I just
said woman rather than trans woman yeah I said what I said anyway it's just a
With that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}