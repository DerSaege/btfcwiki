---
title: Let's talk about an upcoming consumer protection SCOTUS case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=orEdudK0VL0) |
| Published | 2023/03/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a Supreme Court case regarding the Consumer Financial Protection Bureau's constitutionality.
- The Fifth Circuit ruled the agency unconstitutional based on its funding structure.
- The Biden administration requested the Supreme Court to look into the ruling.
- The agency's funding differs from other agencies as it does not rely on yearly appropriations from Congress.
- The Consumer Financial Protection Bureau obtains funds by requesting from the Federal Reserve System.
- Financial structure was authorized by Congress through the Dodd-Frank Wall Street Reform and Consumer Protection Act.
- If the Supreme Court deems the agency unconstitutional, it could lead to economic turmoil.
- The agency has a significant impact on regulations concerning mortgages.
- Speculation is rife about the Supreme Court's decision, with expectations leaning towards overturning the Fifth Circuit ruling.
- Most courts previously found the agency's structure constitutional.

### Quotes

- "If the Supreme Court decides that this agency is unconstitutional, the economy is probably going to go haywire."
- "It's far off, but everybody's going to be talking about it occasionally until it happens."
- "There's a whole lot at stake when it comes to the financial markets."

### Oneliner

Beau explains the potential economic impact of a Supreme Court ruling on the Consumer Financial Protection Bureau and speculates on the court's decision.

### Audience

Legal enthusiasts, policymakers, activists.

### On-the-ground actions from transcript

- Monitor updates on the Supreme Court case and its potential implications (implied).
- Stay informed about financial regulations and their impact on the economy (implied).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the potential consequences of a Supreme Court ruling on the Consumer Financial Protection Bureau and its impact on the economy and financial markets.

### Tags

#SupremeCourt #ConsumerFinancialProtectionBureau #EconomicImpact #FinancialRegulations #Speculation


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk about a
distant Supreme Court case and
what it means and
why a lot of people are going to be kind of waiting on it, even though it's it's probably a ways off.
And
and how it may end up being used as a gauge to see where the Supreme Court is on certain
issues.
Okay so the Fifth Circuit basically ruled that the Consumer Financial Protection Bureau
is unconstitutional, like all of it, kind of, based on how it is funded, is what it
seems. Now the Fifth Circuit made this ruling the Biden administration asked the
Supreme Court to take a look at it and they're going to. The administration
asked for it to be fast-tracked that part it does not look like is going to
happen. Okay so what is what's at stake here and what is the the concern? This
This agency, and what it does, it's a regulatory body, it oversees marketing and lending practices
and stuff like that.
This agency doesn't get its funding the way most agencies do, which is through yearly
appropriations from Congress.
The Fifth Circuit seemed to basically just say, hey, if you're not getting it that way,
unconstitutional. Generally speaking, for almost a hundred years, the Supreme
Court has held that as long as Congress authorized the funding, then that
qualifies, that counts. The way the Consumer Financial Protection Bureau
gets its money is basically by telling the Federal Reserve System, hey we need
X amount of dollars, and then the Fed gives them the money, as long as it's not more
than 12%, I think, of their operating expenses for the year.
That financial structure was authorized by Congress through the Dodd-Frank Wall Street
Reform and Consumer Protection Act.
after the 2008 financial thing, and this is supposed to safeguard against this, against
that type of stuff.
Now if the Supreme Court decides that this agency is unconstitutional, the economy is
probably going to go haywire.
They're a regulatory body that has a lot of say over a lot of things to include mortgages
and right now that's probably, that's not really a market that can handle a lot
of ups and downs or turmoil or uncertainty. As much as many people might
want to deregulate it or whatever it is they want to do, which is probably the
basis for this, for the original suit and it going to the Fifth Circuit, as much
they might want that, they probably don't want the outcomes of it being done like this. It would
definitely send shockwaves through the mortgage market. Now, the Supreme Court did not fast-track
this, although it seems like it would be something they would have, so it won't be heard until next
year until 2024. In between now and then people are going to wait and they're
going to speculate but one of the the interesting things the things that can
be used as a gauge is that the Fifth Circuit it's pretty far to the right. If
if the Supreme Court aligns with the ruling from the Fifth Circuit that
that basically would rule this agency unconstitutional, it's a sign that the court is even further
right than people currently believe, and I understand how far right people believe it
is.
This would mean it's even further.
The general expectation is that no, the Supreme Court is going to overturn the Fifth Circuit.
And it should be noted that most courts that have looked at the structure of this agency
have found it to be constitutional.
So this is one of those cases.
It's far off, but everybody's going to be talking about it occasionally until it happens
because there's a whole lot at stake when it comes to the financial markets and when
it comes to what we can expect from the Supreme Court in the future.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}