---
title: Let's talk about measles and Kentucky....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dJIFdSGnQNc) |
| Published | 2023/03/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A situation in Kentucky involving a large gathering of around 20,000 people holding hands and in close contact is developing due to one attendee being infected with measles.
- The event, known as the Ashbury revival, took place from February 8th to February 19th in Kentucky, with attendees traveling from various locations, potentially spreading the infection.
- Kentucky state health officials warn that those who attended the revival on February 18th may have been exposed to measles.
- Unvaccinated attendees are advised to quarantine for 21 days and seek immunization with the safe and effective measles vaccine.
- Due to extensive travel and numerous people potentially exposed, cases of measles are likely to appear in different areas.
- Beau urges attendees, especially the under-vaccinated or unvaccinated, to follow health advice to protect themselves and others.
- The risk of diseases like measles spreading is higher now compared to the past due to lower vaccination rates.
- It is vital to be aware of symptoms, take preventive measures, and stay informed about potential outbreaks around the country.

### Quotes

- "Follow the advice. Do what you can to protect yourself and those you care about."
- "There is a high likelihood of things popping up all around the country just because of the number of people involved and the amount of travel involved."

### Oneliner

A situation in Kentucky involving potential measles spread at a large gathering underscores the importance of vaccination and following health advice.

### Audience

Public Health Authorities

### On-the-ground actions from transcript

- Quarantine for 21 days and seek immunization with the measles vaccine (suggested)
- Stay informed about symptoms and potential outbreaks (suggested)

### What's missing in summary

Importance of community education on vaccination and disease prevention.

### Tags

#Kentucky #MeaslesOutbreak #Vaccination #PublicHealth #Prevention


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a situation developing
in Kentucky.
Well, it started in Kentucky.
It's probably going to be developing, well,
pretty much everywhere.
There was a get together in Kentucky
with 20,000 people or so, people who were holding hands
and in close contact for a decent amount of time.
It was the Ashbury revival.
People traveled from all over the place,
crossing state lines to get there.
One of the attendees was infected with measles.
because of the close proximity, because of the hugging and the holding hands and shaking
hands and everything that goes on at a revival, the likelihood of it spreading is pretty
high.
It is pretty high.
The revival occurred from February 8th to February 19th in Kentucky.
The Kentucky state health officials are saying anyone who attended the revival on February
18th may have been exposed to measles.
Attendees who are unvaccinated are encouraged to quarantine for 21 days and to seek immunization
with the measles vaccine, which is safe and effective.
Because of the travel that is involved with this and because of the just staggering number
of people that might have been exposed, there are probably going to be cases popping up
everywhere.
If you attended this, if you were part of this group, particularly if you are under-vaccinated
or un-vaccinated, follow the advice.
Follow the advice.
Do what you can to protect yourself and those you care about.
This is something that can spread.
There was a time in the not too distant past when stuff like this, it didn't have as much
of a risk of spreading because rates were higher when it came to vaccination.
We're not there anymore.
So there is a high likelihood of things popping up all around the country just because of
number of people involved and the amount of travel involved. So this is something
to be aware of and get familiar with the symptoms and try to safeguard and
yourself. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}