---
title: Let's talk about wolves and good news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vvBdRa0PYQA) |
| Published | 2023/03/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Good news after nearly 25 years: Mexican gray wolf population increasing in the US.
- The rarest subspecies of gray wolf in the US, reintroduction efforts started in 1998.
- Over 200 Mexican gray wolves now in the wild in Arizona and New Mexico.
- Program showing success with an increase in numbers over the last seven years.
- Numbers were once in the thousands, now measured in hundreds.
- There are around 240 wolves in the wild currently.
- An additional 300-400 wolves in captivity contribute to genetic diversity efforts.
- Captive bred puppy program aims to introduce wolves to dens in the spring.
- More work needed for genetic diversity despite increasing population.
- Positive progress in reintroducing a predator is rare in environmental issues.
- Often, reintroduction efforts are obstructed by economic interests or trophy hunters.
- This program is successfully moving in the right direction without such obstacles.

### Quotes

- "For once, things are definitely going in the right way."
- "Some fights are going the correct direction."
- "Just a moment to recognize some fights are going the correct direction."

### Oneliner

After nearly 25 years, the Mexican gray wolf population is on the rise in the US, a rare positive outcome in environmental conservation efforts.

### Audience

Conservationists, environmentalists, animal lovers.

### On-the-ground actions from transcript

- Support captive breeding programs for endangered species to increase genetic diversity (implied).
- Advocate for policies that prioritize environmental benefits over economic gains (implied).

### Whats missing in summary

The full transcript provides a detailed account of the successful efforts to increase the Mexican gray wolf population, showcasing the importance of ongoing conservation work and the need for continued support to maintain this positive trend.

### Tags

#Conservation #EnvironmentalNews #MexicanGrayWolf #ReintroductionEfforts #GeneticDiversity


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about some good news, some
good news that has been almost 25 years in the making.
It's not often that we get good news, particularly on an
environmental front.
But here we are.
So we're going to kind of go through it all.
The Mexican gray wolf was listed in the 1970s.
It is the rarest subspecies of gray wolf in the United States.
When reintroduction efforts began in 1998,
almost 25 years ago, there were none in the wild in the US.
Today, there are more than 200.
They are spread out over 59 packs in Arizona and New Mexico.
For the last seven years, there's
been an increase in the number.
Now, more than 200, and I want to say the actual number's
around 240, something like that.
That's good.
That is good.
But it is important to remember that at one point in time,
these numbers were measured not in the hundreds,
but in the thousands.
But the program is ongoing, and it is showing success.
There is also like a survival plan for the species.
Then I want to say that has an additional 300 or 300
to 400 wolves in it that are in captivity, though.
But that leads to the captive bred puppy program.
They breed wolves in captivity.
And then they will introduce them to dens in the spring.
And that is part of where the real work still
needs to be done.
While the population is increasing,
there needs to be a lot more genetic diversity.
And that's what this program is aimed at doing.
And it's getting there.
It's working.
So there's still a lot more to be done.
But for once, things are definitely
going in the right way.
And again, when you're talking about environmental issues,
particularly environmental issues that
deal with reintroducing a predator, the news isn't normally good.
There are a lot of times when local and state governments, they don't see the benefit.
They see the economic benefit rather than the environmental one, and it often leads
to reintroduction efforts, conservation efforts being spoiled or being ruined by
those who want a trophy. That does not appear to be happening here and things
are going in the right way. So just just a moment to recognize some fights are
going the correct direction.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}