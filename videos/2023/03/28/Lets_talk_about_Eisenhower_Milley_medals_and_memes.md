---
title: Let's talk about Eisenhower, Milley, medals, and memes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oN2PiMUwYAw) |
| Published | 2023/03/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a viral image comparing General Eisenhower and General Milley based on their medals and the implication that Milley has undeserved medals.
- Clarifies that the number of ribbons displayed by generals changes over time but doesn't impact the comparison in the image.
- Counters the notion that Eisenhower was "unwoke" by detailing his actions supporting civil rights, desegregation, and other progressive causes.
- Notes that Eisenhower received numerous medals from different countries but was modest and didn't display them all.
- Warns against drawing historical conclusions from inaccurate memes and the dangers of spreading misinformation for specific agendas.
- Urges viewers not to rely on memes for historical accuracy and to question the motives behind such misleading information.

### Quotes

- "Don't get your history information from memes."
- "Your propaganda is bad and you should feel bad."

### Oneliner

Be cautious of historical inaccuracies in memes and understand the motives behind spreading misinformation.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Fact-check historical information before sharing (implied)
- Question the motives behind spreading inaccurate information (implied)

### Whats missing in summary

The detailed examples of Eisenhower's progressive actions and his modesty in receiving and displaying medals.

### Tags

#History #Memes #Propaganda #FactChecking #CivilRights


## Transcript
Well, howdy there, internet people.
Let's bowl again.
So today we are going to talk about generals Eisenhower
and Millie and medals in memes.
This video isn't really about their medals,
but it provides a good jumping off point
because I was sent an image along with a question.
And it can be used to illustrate something.
So it's worth going over.
And I imagine some people are going
get a little bit of a history lesson through this. Okay, so here's the question.
My brother showed me this image and I'm curious if there's any history behind it
or if they just awarded medals differently back then or if Milley has
medals he doesn't deserve. Okay, so what's the image? It has a picture of
Eisenhower and a picture of Milley. Eisenhower is described as an unwoke
Republican general. Milley is described as a woke Democrat military chief. I
would point out that general Milley is in fact a general as well. Both of them
have their racks circled, their display of ribbons, their metals. Eisenhower
only has like three. Milley has a whole bunch. Below that it says won a war under
Eisenhower and lost a war under Milley. The implication being that Milley has
medals he doesn't deserve, or maybe if you're not woke you don't get recognized,
or something like that. The idea is that Eisenhower wasn't appreciated and, you
know, Republican grievance gotcha moment type of thing. Okay, so let's start with
the the basic stuff. Were ribbons awarded differently, you know, at different points
in time? Yes, the way ribbons are awarded did in fact change in the century more
than since Eisenhower joined the military. He signed up in like 1915. Yes,
things have changed. However, that doesn't actually have anything to do with
this really. There have also been changes to the way they are displayed. Also doesn't
really have anything to do with this. But before we go on talking about the metal
side of things, I would like to address the idea that Eisenhower was unwoke. Sure,
by progressive standards of today, not a super woke guy. By the standards of his
contemporaries? Yes, he was. By the standards of the Republican Party today?
Absolutely. We'll just take civil rights to go on because this is one of those
things where he didn't have a whole lot of grand speeches and wasn't really out
on the the front, so to speak, in talking about this. He was a very quiet, modest
guy. So that doesn't really exist, but you can look at his actions.
Eisenhower continued desegregation of the federal government. Eisenhower's
administration, even though Congress kind of gutted it, pushed through the first
civil rights legislation in like 80 years. He appointed an attorney general
that was definitely alert to injustice and made it his business to push
forward with it. It's also worth remembering that Eisenhower didn't just
send the 101st into Germany. He sent the 101st into Little Rock. All of that
footage you see of American soldiers standing outside schools with that I
I wish you would look on their face, Eisenhower put them there to help make sure that integration
went well.
So while he doesn't have a whole lot of speeches or anything like that because he was quiet
and modest, his actions definitely trend towards the woke side of the spectrum when you're
looking at it through the lens of his contemporaries.
Okay, so Eisenhower in that image, three ribbons, and it's true.
When Eisenhower was laid to rest, he had three ribbons on his uniform.
He had the Army Distinguished Service Medal, the Navy Distinguished Service Medal, and
the Legion of Merit.
You know what's weird though?
If you look at images from Eisenhower or of Eisenhower from different points in his career,
you'll see that he had more ribbons.
Generally they don't take them away.
Eisenhower actually had a bunch of medals.
He just didn't wear them.
He was quiet and modest.
He just didn't wear them.
If you were to Google for images of Eisenhower, you will see that throughout his career, his
ribbon layout changed repeatedly.
Now, this next part is something I'm just kind of realizing that I was told and I believed,
but I don't know that I've ever fact-checked it.
But the rumor is that generally speaking, he kind of thought that anything that you
got before D-Day, just didn't matter. You're talking about somebody who you
know would have received a World War I victory medal, but it's not on the
list. He just didn't wear it. I would, I'd be willing to almost bet that Eisenhower
received more medals from different countries, more different countries gave
him medals than Millie has from the US. Eisenhower got medals from the Vatican,
from the Soviets. He got a Danish coat of arms, I think. He was incredibly
decorated. If you were to go to Wikipedia, you could scroll down a little bit, they'll have the
biographical information and little charts up front, and normally they list a few medals that
somebody in the military has. If you were to click on full list, it would take you a really long time
to scroll through them all. He was just quiet and modest. He didn't wear them, didn't display them,
I didn't draw attention to them, to the ones he got from the US, to the ones he got from
other countries.
I doubt most people know that he has an entire list of honorary degrees from all over the
place.
He was incredibly well decorated, just didn't display it.
Okay, so obviously the point is not Eisenhower was modest.
What's the point?
Don't get your history information from memes.
And understand that the person that put this together, they typed in Eisenhower medals
or something like that.
And then they went through and found an image where he had the least amount.
in most cases, if you look at most of the images of him, there's more than three.
So it shows not just that memes like this can be inaccurate out of just being wrong.
People might attempt to play on preconceived notions and preconceived grievances by knowingly
putting out information that was false.
There is nobody who is familiar with Eisenhower's career who believes that he only had three
ribbons.
Seriously, go look at the list he got just from other countries.
with that. So it's it's a form of encouraging a a grievance and a gut
reaction to something and in this case I'm pretty sure the person that put this
together knew knew that they were putting out information that was wrong
And you have to wonder what the purpose of it is, what would be the value in putting
out this information to a group of people who, in theory, would care about the American
military.
Anyway your propaganda is bad and you should feel bad.
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}