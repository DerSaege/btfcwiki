---
title: Let's talk about how Nashville was different....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=V-h3eM3QTQk) |
| Published | 2023/03/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he didn't address a recent incident in Nashville immediately, despite messages from viewers.
- Provides an overview of the incident timeline, from law enforcement receiving the call to engaging, which took 13 to 14 minutes.
- Questions the acceptability of the outcome despite the relatively quick response time.
- Emphasizes that the focus should shift from response to prevention and security measures to stop incidents before they occur.
- Acknowledges criticisms in the footage regarding communication and movement of law enforcement but praises their persistence in pressing forward.
- Urges for a focus on prevention, physical security at schools, and intervention to avoid tragedies.
- Stresses that even with a good response, it's not enough; prevention is key.
- Addresses excuses made in the past about law enforcement's inability to respond effectively and praises the department's actions in this incident.
- Responds to fixation on a scared cop, noting that fear was present but their forward movement was what mattered.
- Rejects the idea of scapegoating a group and calls for attention to statistics and data for a clearer understanding.
- Reinforces the message of prevention and stopping incidents before they escalate to the point of requiring heroism.

### Quotes

- "It doesn't matter how good the response is, by that point it's already a tragedy."
- "The focus has to be on prevention."
- "Prevention, stopping it before it starts. That's what this taught us."
- "As good as can be expected isn't good enough."
- "It's better if there wasn't a need for a hero."

### Oneliner

Beau stresses the importance of prevention over response and praises persistence in pressing forward in an incident, urging a shift in focus towards stopping tragedies before they occur.

### Audience
Community members, advocates

### On-the-ground actions from transcript

- Implement physical security measures and intervention strategies at schools (implied)
- Focus on prevention and stopping incidents before they start (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of a specific incident, urging a shift in focus from response to prevention and security measures. Viewing the full transcript helps in understanding the importance of addressing issues before they escalate.

### Tags
#Prevention #Security #LawEnforcement #CommunityPolicing #SchoolSafety


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about something
I didn't talk about yesterday.
And apparently a whole lot of people expected me to.
Got a number of messages.
Most of them had the same tone.
Stuff like, you talk about all of these.
Is there something different about this one?
Very quiet today.
What's different?
I get it.
Message received.
there is something different about this one, and it is worth talking about because
there is something of value that can be pulled from it. But before we get into
it I would like to point out that no, I do not talk about all of them. On average
in this country there is more than one a day. I do not talk about all of them.
Second is before I talk about them I like to get as much information as
humanly possible. And given the reporting from yesterday, I assumed that the
footage would be available pretty quickly. It was released today, so we will talk about
it. If you have no idea what I'm talking about right now, there was an incident in
Nashville. Okay, so what do we know about it? We're looking at, by all reporting, 13
to 14 minutes from call to fall, from the time law enforcement got the call to the time
they engaged, 13 to 14 minutes.
This includes arrival time, assembly, entry, and engagement, 13 to 14 minutes.
What is that?
As good as can reasonably be expected.
That's as good as it's going to get right there, 13 to 14 minutes.
That being said, is the outcome acceptable?
Are you okay with what happened?
No, right?
people were still lost and that's the thing of value that can be taken away
from it. We can't focus on response because you're seeing one that's
good. It's still not okay which means the focus has to be on prevention, security,
stopping it before it starts. That's what it has to be about because a response
it's always going to be reactive.
Okay, the footage is out.
And there are already critiques in it.
There are already people talking about it.
Yeah, it's not ideal from a training perspective.
Yes, they walk in front of each other.
Almost step on each other.
Their communication is not great.
great. They don't have their a set pace when it comes to their movements but
none of that changes that timeline. For years, years on this channel we have
talked about there's one fact when it comes to this. There's one acceptable
tactic that is fact and that that is you you have to press. You press until you
You don't need to press anymore.
That's what they did.
Everything else is opinion.
They did the one thing that matters.
And as far as their movements and the finer points, yeah, it's off.
These guys are not like high-speed door kickers, they're cops.
They did as good as can reasonably be expected.
The most important part is that they pressed and they kept pressing until they didn't need
to.
The complaint about the movement and not having their pacing right, yeah, the guy in the blue
shirt when he's told to go, he's moving down that hallway so fast he loses half his team.
Yeah, that's not ideal, but it is definitely better than not moving.
They did the best that they could, and keep in mind, this is me.
I don't think anybody has ever described me by saying, oh, Bo, you know, the guy who heaps
praise on law enforcement.
They did as good as you can expect, and it's not enough, right?
So the focus has to be on prevention.
The focus has to be on physical security at the schools.
It has to be on looking for markers.
It has to be on intervention beforehand because even with a good response, it's not enough.
So with all of that in mind, that's your key takeaway is that it doesn't matter how good
the response is, by that point it's already a tragedy.
You have to stop it before it gets to that point.
So that's a basic overview of what occurred and there's your real lesson.
There are certainly statements that can be made that can use this as an example to refute
other points that have been made in the past about why law enforcement couldn't do it.
Yes they can.
Yes they can.
In fact, locked doors, they found a way around them.
Only having a pistol didn't matter.
All of the excuses that had been given over the years as to why law enforcement could
not respond, this department in this incident showed that they are excuses, not reasons.
And yes, the other thing that I want to address is for some reason there's a fixation on
one cop who appeared scared.
They were all scared.
But they did the one thing that matters.
They pressed forward.
There's no complaints.
Sure, there's notes as far as training, but there's no complaints.
Now I know that a lot of people that sent those messages at this point are saying, that's
not actually what I was talking about by it being different.
Yeah I know that, I know that.
You were trying to point out an anomaly, right?
What in anything I have ever put out led you to believe that I would help you scapegoat
a group?
That's not a thing.
If you want to go down that route, you have to look at the statistics and the markers.
If you do that, what you're going to find out is that more than 99% of the time, it's
not them.
That's not going to be a marker for this.
If you're going to go that route, you have to use actual data.
go through and look at four incidents over two and a half years that's it's
it's a fraction of a percent that's that's not even again that's not a
thing and I don't know why anybody would expect me to to play along with that at
At the end of this, prevention, stopping it before it starts.
That's what this taught us.
That's the message that everybody needs to internalize from this.
People like the idea of the hero rushing in to save the day.
It would be better if there wasn't a need for a hero.
It would be better if there was nothing dramatic, because once people have to respond, it's
already horrible.
As good as can be expected isn't good enough.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}