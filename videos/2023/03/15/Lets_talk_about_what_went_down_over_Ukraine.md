---
title: Let's talk about what went down over Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YRr7WKbmYrw) |
| Published | 2023/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Geopolitical tension increased between Russia and the United States due to an incident involving a US MQ-9 Reaper drone and Russian SU-27 aircraft.
- The Russian aircraft played around with the drone, leading to a collision as per the US, while Russia denies a collision occurred.
- The Reaper was in international airspace, but Russia claims it was in a zone designated for special military operations.
- Such interactions between countries' aircraft are not uncommon, often involving interceptors being sent up.
- Despite the incident, it is unlikely to escalate into a major issue or lead to war between the US and Russia.
- These events echo Cold War dynamics, and similar incidents may occur in the future.

### Quotes

- "This isn't something that is likely to cause any major issues."
- "This isn't going to spiral out of control."
- "Be ready for more of this."
- "At the end of it nobody got hurt, nobody was injured except for their pride maybe."
- "This will probably fade from the news pretty quickly."

### Oneliner

Geopolitical tension rises between the US and Russia over a drone collision, unlikely to escalate into war, echoing Cold War dynamics.

### Audience

International Observers

### On-the-ground actions from transcript

- Monitor international relations and diplomatic communications for updates (implied).
- Stay informed about geopolitical tensions and incidents to understand global dynamics (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the recent drone incident between the US and Russia, offering insights into international relations and potential future scenarios.

### Tags

#Geopolitics #US #Russia #DroneIncident #InternationalRelations #ColdWar


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about what went down
and why the geopolitical situation
between Russia and the United States
got a little bit more tense.
We're gonna go over what happened,
the way both countries are framing it,
whether the framing really matters,
and what the long-term impacts
from it are going to be.
OK, so if you have no idea what I'm talking about,
there was an MQ-9 Reaper that was flying off the coast.
Now, this is an unmanned aircraft, US unmanned aircraft.
And it was doing a surveillance flight.
Surveillance flights happen all the time.
Now, it was in international airspace.
The Russians sent up two SU-27s, or SU-27s, or Flankers,
depending on who's telling you what happened.
They're all the same aircraft, just different names for it.
And they started playing with the drone, basically.
One of them dumped fuel on it, pulled it in front of it,
and dumped fuel on it a couple of times.
Then the United States says there was a collision, that one of the Russian aircraft hit the drone
and the drone went into, quote, unguided flight. That is a very Pentagon term for it crashed.
Uh, now the Russians are saying that there was no collision, that something just went wrong with the
drone. The odds are there was a collision. It was not intentional. My guess is the
Russian pilots were playing and one of them messed up. It hit, according to the
US, it hit the propeller and the propeller on the MQ-9 is on the rear of
the aircraft. It's not on the wings like you normally see in movies. It's on
the back. So it this is something that could have happened by accident. Now it
is worth noting that while the Reaper was in international airspace the
Russians are saying it was in a space that was designated for the special
military operation. Is that true? Don't know, but could be, probably. Is the United
States under any obligation to abide by that zone that they created? No. Is the
U.S. allowed to be surprised if they send up interceptors if they violate that
zone. No they're not. Those zones they get created every once in a while by
various countries and it's literally a warning. So the US doesn't really have a
legal obligation to stay out of it but it can't be surprised. Surveillance
flights happen all the time. Now should we believe that it was in
international airspace and it wasn't actually in airspace that it really
shouldn't have been in. Yeah, not because the U.S. wouldn't violate airspace if it
felt it needed to, but because the Reaper doesn't need to be close. It can observe,
it can conduct a surveillance flight from pretty far away, so there's not a
a reason for it to go in, but it probably was in the space designated by Russia as
being part of their operation.
This type of thing happens all the time when you're talking about planes from one country
coming a little too close and the other country sending up interceptors.
The U.S. sent up interceptors and escorted a couple of Russian bombers I think last month.
It's not an infrequent thing.
The playing that occurs when this happens, that's also not rare.
It occurs.
It's normally not dumping fuel or colliding with the other craft, but a little bit of
showboating does occur.
I would guess that the Russian pilots, seeing that it's an unmanned aircraft, were probably
playing with it a little bit more than they would if it was manned.
that led to to an accident. Okay, so what happens from here? The US and Russia go
to war, right? Somebody knocked down a plane. No, no. The diplomats, they talk to
each other, they rant and rave, they lodge their protests, and that's pretty
much the end of it. Nobody was injured, nobody was hurt, a plane went down. This
isn't something that is likely to cause any major issues. So it's just another
string in events that parallels the type of thing that happened during the Cold
War. Be ready for more of this. Be ready for more surveillance flights or just
flights that are designed to go a little too close to the opposition airspace and
then the other country sending up interceptors. It's going to happen again
I would guess within the next two months. Just more than likely you're not
going to have a downed aircraft out of it. So this is unusual but shouldn't be
scary. This isn't going to spiral out of control. This isn't going to turn into a
giant thing. My guess is that Russia probably greenlit the pilots playing
with it to kind of inform the US, hey you're in this zone we told you to stay
out of, and it got a little out of hand. At the end of it nobody got hurt, nobody
was injured except for their pride maybe and this this will probably fade from
the news pretty quickly.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}