---
date: 2023-05-03 03:46:29.371000+00:00
dateCreated: 2023-03-20 11:14:53.154000+00:00
description: Techniques for reaching out to individuals who have a strong belief in unreliable news sources, specifically focusing on conversations with family members.
editor: markdown
published: true
tags: news, fox news, media, change
title: Let's talk about Fox, quotes, and a father-in-law....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fnSfVWT7Ijc) |
| Published | 2023/03/15|
| Theme     | Change |
| Status    | article incomplete |

Beau discusses techniques for reaching out to individuals who have a strong belief in unreliable news sources, specifically focusing on conversations with family members. He emphasizes that change is a process that takes time and suggests starting with small conversations and gradually introducing reliable news sources like the Associated Press (AP).
# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Giving an overview of reaching out techniques and managing expectations when discussing sensitive topics, like politics.
- Describing a situation where someone attempted to talk to their father-in-law about news sources and misinformation.
- Explaining how the father-in-law reacted defensively, pivoting the topic and expressing distrust in all news sources.
- Expressing frustration at the father-in-law's reluctance to accept new information and change opinions.
- Asking for advice on how to break through the defensive rhetoric and make someone acknowledge credible reporting.
- Emphasizing that forcing someone to accept new information is not possible, but multiple gentle, informative interactions can lead to change.
- Suggesting the strategy of slowly introducing alternative news sources like the Associated Press (AP) to encourage critical thinking.
- Recommending showing multiple news outlets' coverage of a story to demonstrate credibility and encourage independent evaluation.
- Mentioning the importance of patience, multiple small dialogues, and allowing time for individuals to process information and change their views gradually.
- Stating that even small movements in changing someone's entrenched beliefs are considered progress in the right direction.

### Quotes

- "The truth is never told, it's realized."
- "It's not going to happen in one conversation."
- "Change is a process. It takes time."
- "If you're serious about it, you have to invest a lot of time in."
- "Just because you can get them to understand the baseless nature of a lot of the claims about the election, doesn't mean that they're going to become progressive."

### Oneliner

Beau shares techniques for gently guiding individuals away from misinformation by introducing diverse news sources gradually, acknowledging that change takes time and patience.

### Audience

Family members, community members

### On-the-ground actions from transcript

- Have multiple gentle and informative dialogues to help individuals transition away from misinformation (suggested).
- Introduce diverse news sources like the Associated Press (AP) to encourage critical thinking (suggested).

### Whats missing in summary

The emotional journey of navigating difficult political and misinformation-related conversations with loved ones is best experienced in full.

### Tags

#Family #Misinformation #Politics #CriticalThinking #Change #Community


## Transcript
Well, howdy there, internet people. It's Beau again.

So today, we're going to talk about reaching out and some techniques to remember when you're trying to do this. Because using the quotes that we talked about last week, somebody attempted to reach out to their father-in-law, and they sent a message in describing how it went.

So we're going to read that and then go through what the next steps are and also manage expectations a little bit. OK.

The beginning is him basically saying, talking to his father-in-law, decent guy,
very much into Fox. Okay?

>I asked what he'd heard from the lawsuit, if anything.

And by lawsuit, we're talking about Fox being sued by the voting machine companies.

> I asked what he'd heard from the lawsuit, if anything, and what he thought.
When I brought it up, he'd never heard of the quote's big surprise. When I explained it and read a few quotes, he was open-minded, but continued to pivot, almost reflexively, trying to talk about Biden or the January 6th footage from McCarthy on Tucker's show.
I held him to stay on topic, and we eventually ended the conversation when he agreed that it's hard to know who to trust, and you can't trust any news anymore. I tried to explain, these quotes come from direct sources, obtained written text direct from Fox anchors and executives, but that didn't seem to be enough.
I hit a brick wall when he's backed into a corner and just blames everybody. He says you can't trust anyone on the news, but I feel like he's really saying I don't trust your sources and refuse to reevaluate my own. "It feels like you can't trust anybody" is an excuse to not have to change opinions or accept new information.
I'm writing you to ask how you would handle this situation. How do you break through? I believe that if your position is that you can't trust any new sources, you're letting yourself off the hook for doing any hard thinking and changing positions. 
How do you break through that rhetorical defense and force them to accept that some reporting is credible, especially when discussing a story about discredited reporting.
Thank you very much, have a good day.

Okay, so first, start with this. The truth is never told, it's realized. You can't force them to do anything. You can't force them to acknowledge something.

But the conversations can lead there, conversations being plural. If you want to try to get somebody out of a bad spot, it's probably not going to happen in one conversation.
You're looking at "you can't trust any news outlet" as a loss. That includes Fox though. So if you moved him from trusting Fox no matter what they say to not trusting Fox, that's a little bit of a win.

When you're having conversations like this, it's important to give each little phase of the information time to digest. If this was me and I was set on continuing the conversation, the next thing that I would probably talk about, and I understand, it's hard, especially when you're dealing with family members or a father-in-law. It's difficult because you don't want to destroy the family dynamic. You don't want to create a confrontational situation. So keep them short.

But I'd ask next time, you know, what outlets he does trust, and if he really says he doesn't trust anybody, it gives you an opening. Send him to the AP, the Associated Press. Very dry, very bare bones reporting fact-based stuff that initially isn't going to give him anything that is going to trigger flight and make him leave. Because it is. It's very simple. A, B, C. Who, what, when, where. They rarely get into the why.

So I would start, if you have to direct him somewhere, go to the AP, the Associated Press, and if he says that there aren't any outlets that he feels like he can trust, I would bring up a story from every outlet on your phone. You know how you have little tabs, have multiple windows open? MSNBC, CBS, ABC, CNN, NPR, AP, Reuters, BBC. And just show him and close one after the other and let him see each one. Every single outlet is covering it except for one. 

Wonder why?

And just kind of leave it at that for the time being. Let that digest because eventually he'll come to the conclusion that maybe all of the people who were telling him that nobody was trustworthy were the ones that he shouldn't trust. That might be a conclusion he reaches on his own. Might be something he realizes. And the interesting thing about this particular case is that more quotes keep coming out. The most recent ones, I mean I can't actually say on the channel, but it certainly looks like, I want to say it a producer, believed that their audience really liked their cousins. I think some of this is really bad. So maybe introduce another set of quotes and see what happens then. Particularly those not just about the coverage and how they had doubts but how they view their audience because he is their audience.

But keep that short because that's going to hurt. If somebody is invested like that, it's difficult. It's difficult to get them to start to change. It's something that if you're serious about it, you have to invest a lot of time in. And it's a bunch of small conversations. It's not going to be one that provides perfect clarity to them.

Change is a process. It takes time, and you have to give them that time.

But if you, after one conversation, they went from, you know, really believing everything that Fox put out to, well, you can't trust any of them, that is movement. It is a defense to not agree with the side that they've been told not to agree with, but it does start the process of them moving in their position.

So I would kind of stick to it, but again, be very gentle in the conversation, and move it slowly, get them to that point where they're realizing it on their own, and move towards AP. That's that it really is a good one. I had a family member that that was the the route that I moved them to was AP. Now this person, they're still conservative, but once you move them out of that echo chamber, they tend to be... This person went from somebody who had a Trump flag to somebody who voted for a Democrat over somebody who was Trump-aligned.

So manage your expectations as to how far they're going to go as well. Just because you can get them to understand the baseless nature of a lot of the claims about the election, doesn't mean that they're going to become progressive, but if you can move them out of that authoritarian space, that dangerous space, that's still a win.

So anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
A Pulp Fiction shirt, with yellow and red shadow font on black.
## Easter Eggs on Shelf
{{EasterEgg}}