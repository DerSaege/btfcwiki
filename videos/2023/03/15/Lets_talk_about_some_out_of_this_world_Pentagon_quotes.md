---
title: Let's talk about some out of this world Pentagon quotes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZeiARpEG760) |
| Published | 2023/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a topic that is gaining attention: the possibility of an artificial interstellar object being a parent craft releasing probes near Earth.
- Sean Kirkpatrick, director of the Pentagon's All Domain Anomaly Resolution Office, suggests the concept of a literal alien mothership in a research paper draft.
- The office's purpose is to explain unexplainable phenomena, like optical errors or objects seemingly defying the laws of physics.
- Despite the intriguing theory, it's emphasized that this is a speculative explanation and not confirmed to be occurring.
- The office's transparency in exploring unconventional ideas aims to remove stigma around investigating such topics post-World War II.
- Beau advises that the public should anticipate more unconventional theories and statements from the Pentagon's anomaly office in the future.
- While the suggested explanation is plausible, it's vital to understand it's one of many possibilities, not a definitive conclusion.
- The office won't make firm statements without thorough evidence and press conferences.
- Expect a surge of bizarre information from the Pentagon's anomaly office over the next few years, prompting a need for open-mindedness and critical thinking.
- Ultimately, the exploration of unconventional ideas is encouraged, but it's vital to differentiate between speculative theories and confirmed facts.

### Quotes

- "Basically, this office, they have found a number of things that could be explained by optical sensors being off, normal run-of-the-mill errors that could explain what was witnessed."
- "It's worth remembering that this office is not going to make a definitive statement on something like that without a giant press conference."
- "Expect a lot of just weird stuff to come out of that office over the next probably three to five years."

### Oneliner

Beau introduces the Pentagon's speculative exploration of an alien mothership concept, urging readiness for unconventional theories without confirmed evidence.

### Audience

Science enthusiasts

### On-the-ground actions from transcript

- Stay informed on future developments from the Pentagon's anomaly office (implied)
- Foster open-mindedness and critical thinking towards unconventional theories (implied)

### Whats missing in summary

Beau's engaging delivery and additional context can be best appreciated by watching the full video.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about something
that is a little bit out there.
But a number of questions have come in about it.
And I have a feeling as this information and these quotes
become a little bit more widespread,
it will become more and more of a topic of conversation.
So it's worth looking at prior to being everywhere.
The quote in question is, an artificial interstellar object
could potentially be a parent craft that
releases many small probes during its close passage
to Earth, an operational construct not too
dissimilar from NASA missions.
with proper design, these tiny probes would reach the Earth or other solar system planets
for exploration as the parent craft passes by within a fraction of the Earth-Sun separation."
Okay, yeah, so that's talking about a literal alien mothership.
It is from Sean Kirkpatrick, who is the director of the Pentagon's All Domain Anomaly Resolution
Office.
It's the Pentagon's X-Files.
Their job is to look into weird stuff and kind of figure out what it is.
So that's the quote that is probably going to show up in a lot of places.
It is worth noting that this is a part of a research paper, a draft, hasn't been peer
reviewed, all of the normal stuff.
The general idea behind it is that this could be happening, not that it is.
Basically, this office, they have found a number of things
that could be explained by optical sensors being off
normal run-of-the-mill errors that could explain
what was witnessed.
If everything else is accounted for, the things that
were seen, the phenomena, were operating outside of the laws
physics. So given that their job in this office is to come up with plausible
scenarios for what this stuff is, this is what they came up with. And the reason
our survey scopes don't see the probes as they head to Earth is that they don't
reflect enough light. The survey scopes just aren't good enough to pick them up.
So, while this is definitely interesting, it's worth noting that they are not saying
this is something that is occurring.
They're trying to explain things that are unexplainable.
This office is relatively new.
The whole idea behind it was to get rid of the stigma of looking into stuff like this.
There are going to be a lot of things coming out of this office in the future that are
just wild because their job is to try to explain the unexplainable.
So they're going to come up with pretty wild theories from time to time.
It's worth remembering that this office is not going to make a definitive statement on
something like that without a giant press conference.
Research papers, positions, theories, little notes scribbled into margins, stuff like that
should not be taken as evidence that the Pentagon believes this is occurring.
This is them looking into incredibly strange things, and contrary to the way it's been
done since World War II, they're doing it pretty transparently.
So the public is going to see a lot of just wild and weird statements, and everybody should
probably be ready for that.
And again, it's not out of the realm of possibility that what they're suggesting here is true.
It's an explanation for some of the things that have been observed.
But it is not the only explanation and they are not saying this definitely happened.
Those are the key things to keep in mind.
So yeah, be ready for a lot of just weird stuff to come out of that office over the
next probably three to five years.
There's going to be a whole lot of it.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}