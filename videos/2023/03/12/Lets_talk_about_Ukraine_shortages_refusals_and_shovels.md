---
title: Let's talk about Ukraine, shortages, refusals, and shovels....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GSXxz2Dikl8) |
| Published | 2023/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Russian military faces shortages of regular ammo like small arms ammo, affecting their offensive in Ukraine.
- Troops are receiving outdated equipment and morale is suffering, with some refusing to follow orders.
- Videos show Russian troops expressing discontent and even surrendering voluntarily to Ukrainian forces.
- Russia's renewed offensive in Ukraine has stalled, leading to widespread discontent among mobilized troops.
- Uncertainty surrounds the situation around Bakhmut, with conflicting reports on Ukraine's defense and Russia's equipment capabilities.

### Quotes

- "The lack of real tactical maneuvering and the lack of equipment is taking its toll on the newer Russian troops."
- "It's waste. It's absolute waste."
- "Russia's renewed offensive has stalled in a way that is creating widespread discontent among the mobilized troops."
- "It has devolved into World War one with drones."

### Oneliner

Russian military's ammo shortages and outdated equipment lead to discontent among troops, stalling their offensive in Ukraine.

### Audience

World leaders, policymakers

### On-the-ground actions from transcript

- Support Ukrainian forces with supplies and resources (implied)
- Stay informed on the situation in Ukraine and advocate for diplomatic solutions (suggested)

### Whats missing in summary

Analysis on the potential diplomatic and humanitarian impacts of the situation in Ukraine.

### Tags

#Ukraine #Russia #MilitaryOffensive #AmmoShortages #TroopDiscontent #WorldWarOne #Drones


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about an update on the situation in Ukraine.
We're going to talk about a lack of supplies and how that is shaping events and how those
events may have incredibly long-term impacts for Russia's attempted offensive at this point.
So what has been going on?
The Russian military is being just absolutely plagued by shortages.
We have talked about it repeatedly on the channel, but normally when we're talking about
We are talking about higher-end stuff, precision-guided munitions, the stuff that is hard to get.
There are now reports of just regular ammo, like small arms ammo, and them not having
enough.
Now, the information that's available, it's not really clear if they just don't have the
ammo or it's a logistical issue of getting it to where it needs to be.
If it's a logistical issue, it is an enduring one.
They have real issues with this.
This has been an ongoing thing for a week or two now.
It is taking its toll.
Aside from that, the tactics in certain areas have shifted, and the equipment that troops
that are just showing up are receiving, it's from a different era.
There are reports now that troops that are being sent on frontal assaults are basically
being given a rifle, a few magazines, and an MPL-50, an E-tool for Americans, for everybody
else.
A little baby shovel, and that's their equipment to move forward across a field and in some
cases to hit a hardened position.
Needless to say, this doesn't work.
This is not, to quote one of the videos I'll reference here in a minute, this is not a
way to fight a war.
The lack of real tactical maneuvering and the lack of equipment is taking its toll on
the newer Russian troops.
The troops themselves are suffering from morale issues.
There are a number of videos now that show Russian troops, troops under Russian command,
sending messages to Putin or in some cases basically telling their commanders they're
not doing it anymore in no uncertain terms.
I'm going to read a translation here.
going to be leaving some parts of this out if you want the whole thing in
colorful detail. This should be relatively easy to find. No one is going
on this assault. You can jail us all. And if someone tries to trick us and say we
aren't going there and then they throw us on the front line, it will be an
an absolute show. It won't be forgiven. We'll just go ahead and attack them, talking about
their commanders. This was one person speaking for an entire unit, and the unit was definitely
in agreement, cheering in the background. They are facing widespread, I don't want
say desertion, but more of, we're not fighting a war like this. We're not going
to just walk into the grinder for no reason. It doesn't actually, from the
videos, this doesn't look like a situation in which these are conscripts
that just don't want to be there. These are people who don't want to be thrown
away is what it looks like. There are a number of surrenders occurring, people
voluntarily finding their way to Ukrainian forces. The Ukrainian military
has set up a hotline with details on how to surrender. I actually think it's
called I Want to Live is the channel. It is, it's not going well. The offensive
that was supposed to reinvigorate the Russian calls here, it's not, it's not
moving and it is not moving and paying a very high cost. It's waste. It's absolute
waste.
There is a back and forth around Bakhmut and whether or not it is going to fall.
A lot of sources are saying that Ukraine is barely holding on and a lot of sources are
saying Russia has the advantage but they don't have the equipment to press
forward anymore. I honestly cannot tell which is accurate, which is an
information operation. I don't really know. It is an absolute mess there. The
one thing that is certain is that Russia's renewed offensive has stalled
and it has stalled in a way that is creating widespread discontent among
the mobilized troops, the new troops, the conscripts. That is a recipe for, it's a
recipe the commanders definitely want to avoid on the Russian side because if it
spreads, if the situation that is depicted in some of these videos becomes
common, the commanders won't be worried about the Ukrainian forces. Now, how
widespread is it? Don't know. I mean, it could just be the, I don't know, maybe 150 people
that are depicted in these videos across the entire line. It could just be that,
or this could be indicative of something wider, but there's no real way to know.
As the videos go out they are obviously amplified because this is this is the
type of propaganda that you can't buy this if you are the Ukrainian side. You
can't manufacture something like this. This is this is pure propaganda gold so
they are amplifying it as much as possible. Because of that it's hard to
tell how widespread it is. But most estimates suggest that we're going to
get a resolution and have a better picture as to how things are going in
that area within the next week or so. But to be honest, we've heard that for
about three weeks now. So those are the major developments that are occurring on
the ground there and it's I think the best description of of it from from
everybody that I've talked to is a person who said that it has devolved
into World War one with drones. Anyway it's just a thought. Y'all have a good
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}