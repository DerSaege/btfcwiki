---
title: Let's talk about what's next for Artemis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_wvUOqlWVJQ) |
| Published | 2023/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Artemis missions by NASA, with Artemis 3 aiming to return humanity to the moon.
- Artemis 2, scheduled to depart around Thanksgiving next year, will be crewed and orbit the moon, not land.
- NASA encountered minor issues during Artemis 1, particularly with the heat shield on the Orion spacecraft.
- Addresses misconceptions about the heat shield needing to look charred and ongoing concerns about its performance.
- Despite concerns, the timeline for Artemis 2 is not expected to be affected.
- Artemis 3 is anticipated a year to a year and a half after Artemis 2, aiming to establish an off-world post for long-term manned projects.
- Emphasizes that the goal is not just to gather rocks but to set up technology for sustained human presence beyond Earth.
- Points out the positive aspect of humanity's exploration drive and the potential for accelerated progress once initiatives like Artemis take off.
- Beau concludes by reflecting on the excitement and profound impact of humanity's venture into space.

### Quotes

- "Humanity, for better or worse, is a species that likes to explore."
- "The goal of this is to set up the technology, set up the equipment, get things rolling."
- "Once it starts in earnest, it's going to happen at a faster and faster rate."

### Oneliner

Beau explains NASA's Artemis missions, from uncrewed tests to returning humanity to the moon, paving the way for off-world projects and accelerated exploration.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Support space exploration initiatives by staying informed and advocating for continued funding and research (implied).
- Encourage interest in science and technology education to inspire future generations of space explorers (implied).

### Whats missing in summary

Details on the specific technical challenges faced during Artemis 1 and how they were addressed.

### Tags

#NASA #Artemis #SpaceExploration #MoonMission #Humanity #OffWorld #FutureExploration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Artemis.
We're going to talk about Artemis 1, what it was, what
happened, how everything turned out.
We're going to talk about Artemis 2, when that can be
expected, and then we're going to talk about the goal of
Artemis 3 and bring it all into focus.
If you have no idea what I'm talking about, Artemis is
NASA's mission to put people back on the moon.
Artemis 1 went up, and it was a test.
Sensors, stuff like that, trying out all of the equipment
was uncrewed.
Nobody was on it.
Overall, NASA's like it went great.
There were some minor hiccups.
They're looking through, fixing stuff.
One of the big things that people are focusing on
is the heat shield on the Orion.
And part of it is people not understanding
that a heat shield is going to get hot.
It's supposed to look a little bit charred.
Part of it is an actual issue that NASA is working on.
It didn't reduce the type of shielding
is supposed to reduce as it goes through.
it didn't reduce in the way and in the pattern that NASA had kind of modeled it to, so there is a
little bit of concern there and they're working on that. But there is no reason to believe that
that is going to throw off the timeline for Artemis 2. Artemis 2 is scheduled to depart
sometime around Thanksgiving of next year.
Artemis II will be crewed. It will be crewed. It will go up and kind of hang
out around the moon. It won't land, but it will go up and be up there. And then
that's that second step. Then it's going to return.
Artemis III is expected a year to a year and a half after that, and that
will once again return humanity to the surface of the moon. It'll land. One of the interesting
things about it is that for the most part, this is kind of the first real adventure that
is leading intentionally towards establishing an off-world post.
The goal of this isn't just to go up there and grab some rocks and come back.
The goal of this is to set up the technology, set up the equipment, get things rolling to
actually be able to start putting long-term manned projects off of this
planet, get off of this rock. You know, right now, those of us on Earth, we could
be forgiven for focusing on all of the negative aspects of everything that is
going on right now. It's kind of important to remember that we do have
some positive things going on. Humanity, for better or worse, is a species that
likes to explore. And pardon the phrase, but it is reaching out into that final
frontier now. Once it starts, it will gather speed. Once it starts in earnest,
it's going to happen at a faster and faster rate, more and more. And you will
start to see things that really do resemble science fiction. And we're
We're right on the doorstep of that right now.
Interesting time to be alive.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}