---
title: Let's talk about TikTok and legislation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2zsOjd24Sds) |
| Published | 2023/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how the U.S. government is addressing concerns about TikTok through the RESTRICT Act.
- Congress and the White House are creating new powers within the Commerce Department to regulate technologies from foreign countries that pose security risks.
- The mechanism is designed to avoid directly targeting TikTok to prevent political backlash.
- The Commerce Department will be granted the authority to make decisions independently to shield elected officials from scrutiny.
- This approach mirrors how unpopular decisions are handled, like military base closures, through committees to avoid upsetting the public.
- Acknowledges that tech experts have raised concerns about TikTok's data security issues, which could potentially lead to the development of user profiles.
- The RESTRICT Act has significant support and is likely to pass, signaling potential trouble for TikTok in the future.
- Once the Commerce Department gains authority, they may impose restrictions on TikTok, affecting its future operations.

### Quotes

- "The idea is to give the Commerce Department the capability to regulate and do anything up to including ban technologies from foreign countries that may pose a security risk."
- "This is a way to implement unpopular decisions."
- "It's probably something that is going to go through so it doesn't look good for TikTok in the long term."
- "Once Commerce, the Department of Commerce actually has the authority, it's no longer in Congress's hands."
- "Odds are Biden will sign it."

### Oneliner

The U.S. government aims to address TikTok concerns through the RESTRICT Act, granting the Commerce Department power to regulate technologies and potentially impacting TikTok's future operations.

### Audience

Tech Users, Policymakers

### On-the-ground actions from transcript

- Monitor updates on the RESTRICT Act and its implications for TikTok (suggested).
- Stay informed about potential regulatory changes affecting technology platforms (implied).

### Whats missing in summary

Insights on potential implications for user data protection and privacy rights.

### Tags

#TikTok #USGovernment #CommerceDepartment #DataSecurity #Regulation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about how the U.S.
government handles things that might be unpopular politically.
And we're going to talk about TikTok
and how the U.S. government has decided
to move forward with the concerns with addressing
the concerns about that particular app.
and what it means for the future.
So the short version is, we will be
talking about the Restricting the Emergence of Security
Threats that Risk Information and Communications Technology
Act, the RESTRICT Act.
So this is the mechanism by which Congress and the White
House, in a bipartisan effort, have decided to kind of go after TikTok, but not TikTok
specifically.
Let's be real, TikTok's really popular, right?
Going after it specifically, it might hurt politicians.
It might upset voters, so they don't want to do that.
What they're doing is creating new powers within the Commerce Department.
And this legislation was drafted with a lot of input from Justice, Treasury, Commerce,
and the National Security Council.
The idea is to give the Commerce Department the capability to regulate and do anything
up to including ban technologies from foreign countries that may pose a security risk.
So what they're doing is giving an agency the power to make the determination on its
own.
That way, the elected officials don't have to take the heat for it.
This is a really common way of defusing the responsibility for something.
When it comes to base closures with the military, they established committees, right?
didn't want to vote on that because people would get upset.
This is a way to implement unpopular decisions.
The downside to it is that this is also how you end up with agencies that have a whole
lot of power that isn't later checked by Congress.
Now in this case, what's the big deal?
people who are really up on the tech side of security that I've talked to, they
acknowledge that there are there are issues with TikTok. How big those issues
are vary from person to person that I've talked to. I haven't met anybody that was
like, oh no it's fine, everybody has has some pretty big complaints. Now the type
of information that they're getting generally it shouldn't be that important
but some of the people have talked about how the information itself that's not
the big deal it's all of it together that can be used to develop profiles and
stuff like that this is what US counterintelligence what the Department
of Justice what commerce and everybody else involved in this is probably
worried about. Now this, the restrict act has a lot of support. It's probably
something that is going to go through so it doesn't look good for TikTok in the
long term. We're gonna have to wait and see because commerce, once they have this
power, they may go to the company and be like, hey you can do X, Y, and Z and you'll
be fine, or they may just come out and be like, yeah, you can't play here anymore.
You need to go somewhere else.
And once it hits that point, once Commerce, the Department of Commerce actually has the
authority, it's no longer in Congress's hands.
So odds are this is going to move forward.
Odds are Biden will sign it.
And then once that happens, it's up to commerce, and we're going to have to wait and see what
they're going to do.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}