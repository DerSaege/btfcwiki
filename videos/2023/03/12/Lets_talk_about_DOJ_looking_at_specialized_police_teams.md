---
title: Let's talk about DOJ looking at specialized police teams....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BhEPtFbN9oE) |
| Published | 2023/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- US Department of Justice announced a upcoming "historical" and "current" **"paper"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"**" **"**"**"**"**"**"**"** **"**"**"**"**"**"** **"**"**"**"**"**" investigation of specialized law enforcement teams, not an investigation. 
- Plainclothes teams with little oversight historically lead to misconduct, prompting a Department of Justice "paper" **"**" investigation on their actions.
- The Department of Justice is expected to compare the actions of these specialized teams to the normal rate of incident among regular law enforcement and determine effectiveness and misconduct rates.
- The purpose of this "paper" investigation is likely to change how funding is allocated, potentially resulting in defunding these teams due to findings of ineffectiveness and high misconduct rates.
- The risk of losing funding is more likely to end these programs than body cam footage or past misconduct.
- This "paper" investigation may be a method for the Civil Rights Division to address the issue without individually investigating multiple departments.
- Beau believes that this investigation is a way to stop future misconduct rather than address past wrongdoings.
- The long-term benefit of this investigation is the potential elimination of historically problematic law enforcement teams.
- Beau suggests that people should not expect immediate arrests of cops in their city due to this investigation; the more likely outcome is a loss of funding for these teams.
- The investigation aims to gauge effectiveness versus misconduct to determine if these specialized teams are truly worth the funding.
- Beau acknowledges that while this is a step towards addressing issues, it may not lead to immediate consequences for cops engaged in misconduct.

### Quotes

- "The risk of losing the funding will do more to end those programs than all of the body cam footage in the world."
- "This is a way to stop the problem rather than engaging with those who have already overstepped."
- "The more likely outcome is that two years from now that team doesn't have any funding."

### Oneliner

The US Department of Justice's upcoming "paper" investigation on specialized law enforcement teams aims to gauge effectiveness, potentially leading to defunding these teams due to high misconduct rates, rather than immediate arrests for cops engaged in misconduct.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Contact policy advocacy organizations for updates and actions related to law enforcement reforms (implied)
- Join community forums or campaigns advocating for police accountability and effective allocation of funding (implied)

### Whats missing in summary

Detailed analysis of specific historical incidents involving plainclothes law enforcement teams and their impact on communities. 

### Tags

#DepartmentOfJustice #LawEnforcementTeams #Misconduct #FundingAllocation #PoliceAccountability


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we are going to talk about a review that is going
to be underway soon.
And we're going to talk about some of the excitement that it
has created, the announcement of this review, and why it
might be a little premature for that.
And I think people may be misreading what this review is
likely to do.
So if you have no idea what I'm talking about,
the US Department of Justice announced
that they're going to be doing a review of specialized law
enforcement teams, like the Scorpion team.
Recently, I did a video talking about this,
how historically plainclothes teams with a whole lot of leeway,
not a lot of oversight or accountability,
lead to misconduct.
The Department of Justice is launching a review.
This is what they're calling it.
People are misinterpreting this and turning it
into an investigation.
Two very different things.
They are going to be reviewing it.
So what you're probably going to get is a historical review,
which is basically that video that I put out,
and then a current review of teams
like this across the country.
They're going to be reviewing their actions, comparing it
to the normal rate of incident among normal law enforcement,
comparing misconduct rates, effectiveness,
all of this stuff.
That's what they're going to do.
This is a paper thing.
Now, does that mean that it's pointless?
Not really, because when they do this, typically what it means
is they are going to change how they hand out money.
That tends to get the attention of the administration
within law enforcement agencies.
Police chiefs like all that federal money.
If the feds conduct this review and find what I'm pretty
certain they're going to find, they will probably not
fund those types of teams.
They will probably say that resources
that they paid for through grants can't be used for them.
That, as messed up as this is, it's a statement of reality.
The risk of losing the funding will
do more to end those programs than all of the body cam
footage in the world.
all of the misconduct that occurs.
It's that money that will get them to shift away from this.
Now, do I think DOJ knows that?
Probably.
This is probably a method for the Civil Rights Division
to get something going to curtail this nationwide
without having to go into a whole bunch
of different departments and actually do
thorough investigation. This is a way to stop the problem rather than engage with
those who have already overstepped to their bounds and engaged in misconduct.
This is a way to just kind of stop it. Some would classify it as sweep it under
the rug and pretend like it never happened, but stop it from happening in
the future. If somebody said that to me, I wouldn't argue. I mean that that's
That's kind of what is likely to occur from this.
So is it good?
I mean, it's a step, but I don't think people should expect the Department of Justice to
be showing up in their particular city looking at the misconduct specifically of that particular
team and expecting them to arrest those cops because that is not a likely outcome from
this. The more likely outcome is that two years from now that team doesn't have
any funding. That's probably how this is going to play out. So it's good
in the sense that long term it gets rid of these types of teams which
historically have caused issues. But as far as what I think most people are
looking for when it comes to really dealing with the cops who have engaged
in misconduct, probably not going to occur through this method. Now that may
change if one of the cities that they choose to go and do a modern review of
stuff that is currently happening, if they happen to stumble on a bunch of
stuff, that's a little bit different. But that's not really what they're going to
setting out to do, not based on what I've read about it.
It's more of a review to gauge effectiveness versus misconduct, see if it's really worth
it, which by all data collected by journalists and everybody else, they're going to find
out it's not worth it, it's not effective, which will curtail the money.
So that's your long-term benefit from it.
But don't think that that means they're going to show up in your town and cops that have
a bad reputation there are going to end up in cuffs.
That's not likely.
Anyway, it's just a thought.
So, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}