---
title: Let's talk about average temperature changes in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CDAKsvEMUJY) |
| Published | 2023/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Average winter temperature in the United States has risen by 3.7 degrees Fahrenheit from 1970 to 2023.
- The average summer temperature has risen by 2.3 degrees Fahrenheit, impacting about 80% of the country.
- This warming trend is noticeably affecting agriculture, as some crops that need chill time to grow may not thrive.
- The impacts of climate change extend beyond temperature changes, affecting water resources as well.
- Warmer winters may lead to reduced snowpack, impacting freshwater sources.
- There is a reluctance to make necessary changes to address climate change.
- Many regions that will be severely affected by climate change are not in the United States and have contributed less to the issue.
- Climate change will have global impacts, including on the US, through factors like water scarcity and food production.
- The transition away from fossil fuels and dirty energy is inevitable and must happen for the well-being of future generations.
- Leaders who prioritize the security and well-being of future citizens are needed to drive these necessary changes.

### Quotes

- "It is occurring, and it's going to have impacts."
- "If you want your kids, your grandkids, to have some semblance of the society that we enjoy, these changes have to occur."
- "The transition away from fossil fuels, away from dirty energy, they're going to have to occur and they will occur one way or another."

### Oneliner

Average temperatures in the United States are rising, impacting agriculture and water resources, requiring a transition away from fossil fuels for a sustainable future.

### Audience

Climate activists, policymakers, citizens

### On-the-ground actions from transcript

- Advocate for leaders who prioritize sustainable energy transitions and climate action (implied)
- Support policies and initiatives that address climate change impacts on agriculture and water resources (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the impacts of rising temperatures in the United States and the necessity of transitioning away from fossil fuels for a sustainable future.

### Tags

#ClimateChange #TemperatureRise #FossilFuels #Sustainability #FutureGeneration


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the climate.
We're going to talk about how things are changing
in the United States.
We're going to talk about what it means for the future
and what it has meant for the past,
because there are new pieces of information,
as far as average temperatures, that are worth going over.
So we're going to do that and then just go from there.
Now from 1970 to 2023, the average winter temperature in the United States has risen
by 3.7 degrees Fahrenheit.
The average summer temperature has risen by 2.3 degrees Fahrenheit.
This is noticeably impacting about 80% of the country.
Winters are warmer.
Summer is warmer.
is warming faster than summer, okay? So what does this mean? A lot. A lot. There are going
to be some areas where things that used to grow don't grow anymore. You know, a lot
of a lot of fruit needs chill time. It needs time where the tree is in a cool period. If
those temperatures are reached for the proper number of hours, well, the fruit doesn't
happen, or it doesn't happen as plentiful in nature. People have talked about climate
change for a long time, and for a long time you've had people saying, oh, it's not real,
it's not that big of a deal.
We have the information, we have the data, it absolutely is real.
It is occurring, and it's going to have impacts.
As far as other things that it's going to impact, think about water.
There are a lot of areas where the snow that falls in winter is what feeds the rivers,
which creates the fresh water.
If things are warmer in the winter, you may not have as much snowpack that then melts
that feeds the water.
It's all interconnected.
There's a huge reluctance to make the changes that are absolutely necessary.
And I think a lot of this is focusing, a lot of it has to do with the way the media focuses
on those who are going to be most impacted, which it's not the United States.
One of the more infuriating things about this is that a lot of the places they're going
be hit hardest by climate change as it grows more and more severe are places that the U.S. doesn't
think about, and places that honestly didn't contribute a whole lot to the problem. Those
are going to be the hardest hit. Because of that, and a lot of the focus being there,
people in the United States don't realize that it is going to impact them as well.
and maybe don't understand the degree to which it's going to impact them. When you
start having less water, less production of food, all of this stuff, it's bad for
the US. When you combine that with the fact that you're going to have people
leaving other areas to come to places that are doing better, you're gonna have
climate refugees, it gets even worse. The resources are going to be stretched thin.
The transition that we have to make away from fossil fuels, away from dirty energy,
they're going to have to occur and they will occur one way or another.
We're going to stop using it. We're going to stop burning stuff for electricity.
It's going to happen. Whether that happens because we have the foresight and we make the changes necessary, or it
happens because we suffer a breakdown and literally can't do it, that's still for us to decide.
but it's gonna happen. If you want your kids, your grandkids, to have some
semblance of the society that we enjoy, some semblance of all of the
creature comforts that exist, these changes have to occur. Yeah, some of them
are going to be expensive, but it's not like there's another option. It has to
happen and I would suggest that the US and other nations probably need to be
looking for leaders who are willing to make those changes, who are willing to
to prioritize the security of future citizens of their countries and humanity.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}