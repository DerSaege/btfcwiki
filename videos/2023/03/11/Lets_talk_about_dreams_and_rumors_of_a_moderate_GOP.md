---
title: Let's talk about dreams and rumors of a moderate GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nYZI1abpoRI) |
| Published | 2023/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Larry Hogan, a preferred moderate GOP candidate, decided not to run for the presidential nomination.
- Rumors suggest other candidates will unite behind an unannounced candidate to counter authoritarian elements in the Republican Party.
- This strategy aims to consolidate support among moderate Republicans and prevent authoritarian votes from being split during primaries.
- Hogan might run as an independent if the strategy fails, specifically to stop a Trump-like candidate.
- Hogan's potential third-party candidacy could make a difference by denying the authoritarian element the presidency with just a few points.
- The organized opposition to authoritarian elements within the Republican Party seems more prepared this time, with plans and contingencies in place.
- The public-facing information indicates a significant number of committed Republicans aiming to steer the party back towards its original ideals.
- Despite being based on rumors, there is substantial support for the idea of returning the Republican party to its traditional conservatism.

### Quotes

- "Those who are opposed to the more authoritarian elements within the Republican Party certainly appear to be a whole lot more organized this time."
- "He just has to get enough to tip it, and with as polarized as things are, there's a pretty good chance that in some races, him catching half a percent to a percent and half, that might decide the outcome."

### Oneliner

Larry Hogan's decision not to run for the GOP nomination may pave the way for an organized effort to counter authoritarian elements within the party, potentially through a third-party candidacy aiming to deny them the presidency with just a few critical votes.

### Audience

Political activists, Republican voters

### On-the-ground actions from transcript

- Unite behind candidates who aim to steer the Republican Party back to its traditional conservatism (implied)
- Support moderate Republicans and independent candidates who oppose authoritarian elements within the party (implied)

### Whats missing in summary

More details on the potential impact of a third-party candidacy by Larry Hogan and how it could influence the outcome of the presidential race. 

### Tags

#LarryHogan #GOP #RepublicanParty #Authoritarianism #PresidentialElection


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about Larry Hogan again.
Larry Hogan, for many people, was the preferred choice for a
moderate GOP candidate to seek the presidential nomination.
A whole lot of people were hoping that Hogan was going to
seek the nomination and be the candidate to try to derail the more authoritarian elements
within the Republican Party.
He was going to be the one to bring it back to Bush-era conservatism.
He decided he's not going to run for the Republican nomination.
Instead, the rumor mill suggests, again rumor, suggests that all of the candidates and all
of the major players who want a return to the normal fiscal conservative, the traditional
Republican party, they're all going to line up behind one candidate who, as of yet, has
not been announced, but it won't be Hogan.
This is an attempt to consolidate all of that support behind one person so as the more authoritarian
elements within the Republican Party battle it out during the primary, the moderate Republicans
stand a chance of gaining more votes than the authoritarian vote all split up.
That's the plan.
Now the question a lot of people have is why didn't they choose Hogan?
Why didn't Hogan become that candidate?
Because well, this way he's not a sore loser.
If that fails, if that fails, if one of the more authoritarian candidates gets the nomination,
Hogan can run as an independent with the express purpose of stopping a Trump-type candidate.
That part is also rumor, but it's now been confirmed by Hogan himself in a way.
While he has ruled out running for the Republican nomination, he has not ruled out running as
a third party candidate.
So it's a rumor, but that's a pretty interesting confirmation of a sort with Hogan himself
saying he's not ruling that out.
Those who are opposed to the more authoritarian elements within the Republican Party certainly
appear to be a whole lot more organized this time.
They don't seem to be underestimating it.
They seem as though they have come up with plans and contingency plans, and while right
Right now, it's all behind closed doors and there's only rumors and little bits and pieces
coming out.
The public-facing stuff that is showing up certainly indicates that there is a decent
amount of Republicans who are very committed to the idea of writing the party, of putting
it back on track to be a Republican party rather than a super far-right party.
Again, it's rumor.
Now the question is, would a third party candidacy from Hogan matter?
It really only needs a couple points.
For it to matter.
it to deny the authoritarian element, the presidency, if that's the route that it goes,
he doesn't have to get a lot of votes, assuming it's Hogan.
He just has to get enough to tip it, and with as polarized as things are, there's a pretty
good chance that in some races, him catching half a percent to a percent and
half, that might decide the outcome. So they have a plan. Let's see them put
into motion. At least it appears they have a plan. There's a rumor of a plan,
and all of the public-facing stuff certainly lines up with the rumor. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}