---
title: Let's talk about North Korea, perspective, and two projections....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4-XS85yUPSk) |
| Published | 2023/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation in North Korea from two different perspectives: American and North Korean.
- Talks about North Korea facing severe food insecurity and the need for international assistance.
- Mentions US intelligence suggesting a possible nuclear test by North Korea.
- Compares the situation to if the US was in a weakened state and facing a potential invasion.
- Points out that North Korea's actions are driven by their own interests and perspective.
- Raises questions about the timing and motivations behind North Korea's potential nuclear test.
- Acknowledges the concerns about North Korea's nuclear weapons program and the international response.
- Emphasizes the importance of understanding foreign policy decisions through the lens of the country making them.
- Suggests various possibilities regarding the motivations behind North Korea's actions.
- Concludes by urging to view North Korea's actions through their own perspective and national interests.

### Quotes

- "Every country pursues their own national interests and for North Korea, they're very much on their own."
- "It's really important to try to view foreign policy decisions through the lens of the country making them."
- "That's the situation they're in."
- "But believe me, if the U.S. was entering a period of food insecurity and China had recently said they wanted to realign the government, people in the U.S. would be worried."
- "Y'all have a good day."

### Oneliner

Beau explains the situation in North Korea from American and North Korean perspectives, shedding light on their motivations and the importance of understanding foreign policy decisions through the lens of the country making them.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Understand foreign policy decisions through the lens of the country making them (suggested)
- Question US intelligence reports and analysis (suggested)

### Whats missing in summary

Deeper analysis on the implications of North Korea's actions and their impact on international relations.

### Tags

#NorthKorea #ForeignPolicy #USIntelligence #NuclearTest #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about North Korea
and two projections.
And we're going to talk about perspective,
because the two projections from an American perspective
don't make sense.
People are looking at it, and they're like,
why would they do this at this point in time?
They need help.
And this is just, it's bad for their interests
and all of that stuff.
And that's looking at it from an American perspective.
From a North Korean perspective, it's a little bit different.
And we're going to kind of go over why.
Now, if you have no idea what I'm talking about,
two projections.
One, North Korea is headed into a period
of severe food insecurity.
They are not going to have enough food
to feed their people.
Now, what would they need to deal with that?
international assistance, more than likely, right?
Okay, so this is one projection. This is one estimate, something that is going to
occur soon.
US intelligence is suggesting that North Korea
may soon test a nuke.
And it has prompted a lot of questions. Why would they do that? It's such a bad
move for them.
And I get it, I understand where that particular idea is coming from,
but that's
looking at it from an American perspective.
So let's just kind of shift things up a little bit.
Think about all of the people you know right now in the US
who truly believe that at some point in the near future,
Russia or China or a combination
is going to invade the United States.
We've talked about this on the channel before,
numerically, logistically, geopolitically,
this really isn't a thing.
But there's a whole bunch of people
are concerned about it, right? That fear, that paranoia, it exists. Okay, now I want
you to picture the United States moving into a period where it will be in a weakened state.
Say mass food insecurity. Those people, what would they be saying? This would be the ideal
moment for China to hit us, we're not at full strength.
What might they want to do?
Engage in a show of strength.
Now let's take that from the American perspective, and let's take it back to North Korea.
North Korea is not a country like the US.
The idea that China is going to invade the US, that's silly.
It's not really a thing.
The countries that are upset and adversarial to North Korea, they do have the power to
politically realign that nation.
And they've said on numerous occasions that they don't like the current government.
So if they're headed into a period of food insecurity where they may not be able to feed
their army they might want to engage in a show of strength and that's how
they're looking at it. Now do I think that's the right move? I mean I don't
like nukes and I think I think it might work better if they would just appeal to
China to to get the food assistance they may need but there may be stuff going on
between the North Korean government and the Chinese government that I'm not aware of.
Now this is all assuming two things though that both of these projections
are correct and we have to remember US intelligence has said that North Korea
is about to test nukes numerous times and they're not always right. Sometimes the
estimates are wrong so you have to keep that in mind. The other thing is the
assumption that the two are connected. Now the situation in which I, the
situation I just described where North Korea is wanting to show strength, that's
them being connected. What if this test was scheduled like a year ago and
they're not connected. This is just the normal advancement of that program.
That's also a possibility. Now you may say that this would be a good
time to delay that test, but then you go back to that position of strength thing.
North Korea does have a culture that is centered around the idea that they might be invaded
at any point in time.
Why?
Because most of the world is very adversarial to North Korea, and a lot of countries have
stated, yeah we really wish there was another government there. And then you
look at it from the perspective of removing the nukes. Let's say North
Korea decides not to have the test, decides to give up its weapons program
unilaterally. Is the rest of the world suddenly going to be friendly to North
Korea just because of that? No, they're still going to be an adversarial nation.
This is why it's really important to try to view foreign policy decisions through the
lens of the country making them.
And sometimes, from the outside, it doesn't make any sense.
But believe me, if the U.S. was entering a period of food insecurity and China had recently
said they wanted to realign the government, people in the U.S. would be
worried. That's the situation they're in. Now don't get me wrong, I am
not defending the development of nuclear weapons. This isn't a, hey this is
something I agree with or support, this is an explanation of what might be
occurring. And you have a number of options. One, U.S. intelligence is wrong.
Two, the test was already scheduled. Three, they are related and they're trying to
show strength, trying to deter what they perceive as a possible invasion because
of their weakened state. It's a good question and it does provide that moment
you get to look at things from from another view but remember every country
pursues their own national interests and for North Korea they're very much on
their own and it their actions have to be viewed through their lens if you want
them to make any sense yeah I mean from a Western perspective the entire world
is mad at you because you have this weapons program. Furthering that
weapons program at a time when you might need international assistance, that seems
like a bad idea. But again, if that weapons program was gone, it's not like
the U.S. would suddenly be super friendly to North Korea. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}