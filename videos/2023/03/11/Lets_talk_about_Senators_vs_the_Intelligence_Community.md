---
title: Let's talk about Senators vs the Intelligence Community....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VwTozeVcxB4) |
| Published | 2023/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contest between legislative and executive branches over access to recovered documents from Biden, Pence, and Trump in various locations.
- Senate intel leaders want to see the documents for oversight, but intelligence community is hesitant to share classified information.
- Senate committee may not need to see actual documents, a readout or summary could suffice.
- Concerns about mishandling of classified material extend beyond the executive branch.
- Intelligence community worried about leaks and limiting access to documents involved in active investigations.
- Beau suggests a compromise can be worked out easily.
- Importance of understanding classification markings to limit access to sensitive information.
- Beau questions if Senate Intelligence Committee actually needs to see the real documents.
- Calls for more oversight on handling classified material by elected officials.
- Tension building around bills reauthorization that benefit intelligence community due to access to documents being used as leverage by politicians.

### Quotes

- "The mishandling of classified material is something that definitely extends beyond the executive branch."
- "There is a major issue when it comes to the elected officials in both the legislative and the executive branches and their handling of classified material."
- "Everybody going into Congress, everybody going into the Senate, into the House, into the executive branch really should probably get a whole lot more training."

### Oneliner

Contest between branches over access to recovered documents prompts calls for more oversight on handling classified material by elected officials.

### Audience

Legislative staff, Oversight advocates

### On-the-ground actions from transcript

- Advocate for increased oversight on handling classified material by elected officials (implied).

### Whats missing in summary

Importance of proper training and briefing for officials on handling sensitive documents. 

### Tags

#LegislativeBranch #ExecutiveBranch #Oversight #ClassifiedMaterial #IntelligenceCommunity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about another contest
shaping up between the legislative
and executive branches.
This one over access to the documents that were recovered
from Biden and Pence and Trump
and all of the various locations
where they shouldn't have been.
The Senate intel leaders are basically saying they want to review the documents.
Legislative branch, the executive branch, the intelligence community is like, yeah,
you don't actually need to see those and it's going back and forth.
The senators are saying they need to see them in order to perform their oversight duties.
The intelligence community is obviously concerned about sharing classified documents with even
more people.
Now, does the Senate committee really need to see them?
Not really.
Do they have an oversight function here?
Yes, but they don't need to see the actual documents in most cases.
They could get a readout of what was on the documents and what led it to be compartmented
or whatever.
They don't need to see the actual documents.
There's a happy compromise that could be worked out here pretty easily.
The senators seem to be trying to tie reauthorization of an intelligence bill to them being able
to see these documents.
intelligence community? Do they have a reason to be concerned? Well, yeah, they
do, because what we're talking about now is the Senate Intelligence Committees,
right? The House Intelligence Committee got a briefing from the National Archives
in which it was disclosed that as many as 80 members of Congress donated
documents with classified markings to libraries and universities.
The mishandling of classified material is something that definitely extends beyond the
executive branch, so there is a legitimate concern from the intelligence community when
it comes to sharing these documents, especially considering that many of them
are involved in an active investigation, a active criminal investigation, and
there's probably still a counterintelligence investigation going on
and they're probably still trying to limit the fallout and when you're doing
that the last thing in the world you want is to have a whole bunch of new eyes
on the documents. So they have a legitimate reason for concern. The Senate
does have a legitimate oversight function here, but it doesn't necessarily
require hands-on the documents. Now some of them, the stuff that's marked secret,
whatever, let them see it. The compartmented stuff, no, they shouldn't.
That's that's the whole thing and it's a
Repeated theme is people not understanding why these classification markings exist and what they mean
The whole goal is to limit the number of people that see them
I'm not sure that
The Senate Intelligence Committee actually needs to see the real
documents. They can get a readout. They can get a brief summary of what the
documents were. I'm sure the intelligence community is also worried about leaks
saying, hey, you know, well this is what was on them and then maybe disclosing
more than they should. Or maybe they're worried about them being donated to a
the library.
The one thing that is certain is that there does need to be more oversight.
The Senate is correct there.
There is a major issue when it comes to the elected officials in both the legislative
and the executive branches and their handling of classified material.
It is way out of line.
talking about a lot of documents being found out in the wild in a whole bunch
of different places, places they never should have been, mingled in with stuff
that certainly should not have found its way to where it found its way.
So this is setting up a contest and it will probably come to a compromise. If
not expect to see a bunch of tension around bills being reauthorized that
would benefit the intelligence community because it looks like there
are some politicians who are willing to kind of use those as leverage to get
what they want and gain access to these documents. The one thing that is very
apparent is that everybody going into Congress, everybody going into the Senate,
into the House, into the executive branch really should probably get a whole lot
more training and they should probably be better briefed on the impacts of this
stuff getting out into the wild the way they repeatedly have. Anyway, it's just a
With that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}