---
title: Let's talk about Louisville and the DOJ....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=50FJ4xKq_ow) |
| Published | 2023/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Louisville agreed to make corrections to their law enforcement based on DOJ Civil Rights Division recommendations.
- The DOJ found a culture within the department violating rights through invalid warrants, excessive force, and discriminatory behavior.
- Recommendations include constant body camera footage reviews, more oversight, and outside assistance for decision-making.
- The hope is for reforms, retraining or termination of individuals, and implementation of all recommendations.
- The Department of Justice can step in if reforms are not made, with extreme measures like taking over the department.
- Similar agreements have taken place in other cities like Ferguson, with Memphis next in line.
- The catalyst for these actions was Breonna Taylor's death, revealing a pattern of rights violations.
- This process aims for serious reform if acted upon in good faith, otherwise risking departmental overhaul.
- It signifies the start of change in law enforcement culture, aiming to address major issues.
- Despite not being a perfect solution, it is a step towards reform and has the potential to save lives.

### Quotes

- "This is a start. And it shouldn't be ignored because it's not the perfect solution."
- "It's a move in the right direction."
- "Those are kind of the two extremes when it comes to the outcomes."
- "This is a case where the bad apples spoiled the bunch."
- "It's good news, but it's not the end of the story."

### Oneliner

Louisville agrees to DOJ recommendations for law enforcement reform after rights violations, signaling the start of serious change with Memphis next in line.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Support and monitor the implementation of recommended reforms by law enforcement in Louisville and upcoming in Memphis (implied)
- Advocate for continued oversight and accountability in law enforcement practices (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the agreement between Louisville and the DOJ, the need for reform in law enforcement practices, and the potential for significant change through good faith action.

### Tags

#Louisville #DOJ #lawenforcement #reform #civilrights #Memphis


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we are going to talk about Louisville
and that agreement that everybody's talking about.
What it means, where it came from,
what they found during this process,
how things could play out from here,
and what city's next,
because that's also gonna be part of this.
So, if you have absolutely no idea what I'm talking about, Louisville has agreed in principle
to some corrections to their law enforcement there based on, let's just call them very
strong recommendations from the Department of Justice Civil Rights Division.
The DOJ's civil rights people, they conducted an investigation, a massive review, and they
found that basically there was a culture within the department that violated rights, lots
of them, in a lot of different ways.
Everything from a pattern of invalid warrants leading to unjust searches, to excessive force,
discriminatory behavior towards black Americans, towards disabled people, towards a lot of
people, a general culture within the department that could be described as lawlessness.
The Department of Justice created some recommendations, a little less than 40 of them, to include
basically constant reviews of reports of body camera footage of everything a lot
more
a lot more oversight.
Now, this
set of
recommendations, it has been agreed to by the city.
I mean it's one of those things, well you're gonna agree to it, or we're gonna
take over your department, is kind of how this normally plays out.
Once the agreement is made, there
is a wide range of things that can occur.
First, the way it should work, the way
people hope this type of thing works,
is that a whole bunch of reforms get made.
People are retrained, people are terminated if they cannot
or will not be retrained.
All of the recommendations go into place.
people actually review
the body camera footage. Not just when there's an incident,
but all the time.
There are outside people available to assist if something is
confusing,
or
perhaps
the local officials don't want to make the call.
There will be a Department of Justice person they can call,
and that person will come in and make that determination for them.
And the hope
is that this process gets used in good faith by the city,
by the department.
That's the carrot.
The stick is that if things don't go that way,
if the department doesn't make the reforms,
if they don't get rid of the people
that they have to get rid of, if things don't move forward,
the Department of Justice is now in a position
to make them do it, and they have a whole lot of tools available to make them do it.
The most extreme is literally them taking over the running of the department.
That is something that is theoretically in bounds.
These types of agreements, they have taken place in a number of cities.
Ferguson is one that had one in place.
Now the obvious catalyst for this was Ms. Taylor's death.
That's what started this.
The review just kind of illustrated that what occurred there wasn't isolated.
It wasn't something that could be safely assumed to never happen again.
So because of the pattern of behavior, because of the pattern of violating people's constitutional
rights, the Department of Justice stepped in.
Now, what's next?
Memphis.
The city of Memphis is about to undergo the exact same kind
of review.
And my guess is that on the other side of it,
they're going to end up in an agreement as well.
So it's good news, but it's not the end of the news.
It's the start of serious reform if people act in good faith.
If they don't, it's the start of the department being
systematically ripped apart.
Those are kind of the two extremes
when it comes to the outcomes.
There's a bunch of stuff in the middle.
So it is good news, but it's not the end of the story.
This is a case where the bad apples spoiled the bunch.
And now there will be a team of people and recommendations
on how to basically get a whole new barrel
and hopefully reform some of the problems,
reform some of the major issues.
There are still going to be problems even once this is done.
There will be because of the overall culture in American law enforcement.
But this is a start.
And it shouldn't be ignored because it's not the perfect solution.
It's a move in the right direction.
And things like this generally have proven to save lives.
So even though it's not the ultimate win, it is a win and it's a move in the right
direction. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}