---
title: Let's talk about the new Nord Stream reporting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=t_8fqb6ClB4) |
| Published | 2023/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring theories about the Nord Stream pipeline incident involving possibly Ukrainian non-state actors.
- Questions the plausibility of the perpetrators being solely non-state actors without any backing.
- Mentions that the unidentified individuals involved are referred to as Destro, Zartan, Baroness, Firefly, Storm Shadow, and Cobra Commander.
- Suggests skepticism about the theory that the perpetrators were completely independent non-state actors.
- Countries' official responses: Germany urges patience for investigations, the US has no official comment, Russia denies involvement, and Ukraine denies knowledge or connection.
- Lack of consensus in the international community on the incident's perpetrators.
- Cautioning about potential spin on concrete evidence by various sides for geopolitical gains.
- Elite officials potentially shaping narratives based on their backgrounds in intelligence.
- Encourages following the investigation for a possible disputed but more concrete answer.
- Not fully taking a stance due to limited evidence but finds this particular theory more intriguing for its reliance on physical evidence.

### Quotes

- "I find it hard to believe that they are non-state actors that just did it on their own without a company backing them."
- "A lot of these elites are coming from intelligence officers."
- "This seems to be following physical evidence, which is nice, rather than creating a theory and then trying to find the evidence to match the theory."

### Oneliner

Beau delves into theories surrounding the Nord Stream pipeline incident, expressing skepticism about the sole involvement of Ukrainian non-state actors and cautioning against potential spin on evidence by various sides.

### Audience

Observers, Analysts, Researchers

### On-the-ground actions from transcript

- Contact relevant authorities for updates on the investigation (suggested)
- Follow reputable sources for reliable information on the incident (suggested)

### Whats missing in summary

Deeper insights and context from Beau's analysis and potential future implications can be better understood by watching the full transcript. 

### Tags

#NordStream #PipelineIncident #InternationalCommunity #Geopolitics #Investigation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit more
about the pipeline, about Nord Stream.
And the new theories that are surfacing about it,
we will run them through the same stuff
that we have run the other ones through.
And just kind of go through it and talk
about where all of the different entities involved,
what they're saying about it.
OK, so if you have no idea what I'm talking about,
there is now reporting that suggests
that the people behind the pipeline getting hit
were maybe Ukrainians that were non-state actors.
That's the current theory that is mostly being floated.
Off top, I will say that it's possible.
Don't get me wrong.
When it gets to the plausible part,
I have questions about them being non-state actors.
That seems a little odd to me.
And I know that in the very first video I did on this,
suggested that non-state actors could be involved. I find it hard to believe that
they are non-state actors that just did it on their own without a company
backing them. That's the part that I have questions about. The
reporting says that six people, five men and one woman, they are unidentified at
this time, so we will call them Destro, Zartan, Baroness, Firefly, Storm Shadow,
and Cobra Commander, they ran a yacht and that they did it. That's possible. The
general framing of it though indicates that they were totally non-state actors.
I don't know about that. I mean they're making this sound like this was just
like six people that got together and were like, hey let's go do this. That
That seems a stretch.
It's possible that they were state actors that nobody can prove that they were state
actors.
That would make a lot more sense to me.
But the general tone right now is that they were people who were supportive of Ukraine
operating independently.
What are the various official governments saying?
Germany, who actually searched the yacht in question.
Their officials are saying, you need to calm down.
We don't really have a whole lot at this moment.
We need to let the investigations play out.
There's a possibility that it was somebody making it look
like Ukrainians that are non-state actors.
The United States really doesn't seem
to have an official comment on it at all.
Russia is saying that it isn't true.
it's misinformation designed to draw attention away from the real culprits
who,
if I'm reading between the lines here correctly,
they think
Hirsch's story
is the right one.
The Ukrainian government has said they have no knowledge of it, they didn't have
anything to do with it,
which tracks.
That's about where it is.
There's a lot
of talk about this,
not a lot of evidence at this point, more so than Hirsch's story, at least when you're
looking at it from German reporting, but still not enough to say, yes, this is what happened.
There are still a whole bunch of other options out there. I'm following this one a little bit
more closely than a lot of the other theories that have shown up, but it's more because you
actually have some form of evidence or at least official agencies pursuing some form of evidence.
Them not being able to identify the people at all leads me to believe that this wasn't just
six people who decided to do it. Being transparent to that degree and nobody being able to know who
you are. That isn't normally a skill set of just random people who decided to do something
like this. So, there is, as far as the international community goes, there's no consensus. Russians
still believe it's the US. The US doesn't really have a say on this. Ukraine says it
wasn't them. Germany, who are apparently people leading the investigation on
this, at least by reporting that's the way it seems, they're the ones that have
this information and they still seem very reluctant to commit to it and are
even suggesting that the idea that it was pro-Ukrainian groups may not be
accurate. It may have been people who wanted it to seem that way. So we really
don't have a lot yet, but this is, it's an interesting development. The whole yacht
and it being searched, that may actually be the part that gets us to who was
actually behind it. Now one thing I do want to caution everybody on, based on
the reaction to this news, even if incredibly concrete evidence comes out, I'm going to
suggest that pretty much every side is going to try to spin that evidence to their favor.
For Russia, to say it's misinformation and trying to distract, shows that they are more
interested in the geopolitical ramifications than they are actually finding out what happened.
For Ukraine to say, you know, it wasn't us, but this definitely looks like a compliment
to our special forces, even though we didn't do this, it would be cool if we did.
of, like, was the joke they were making. I don't necessarily believe their denial, but
at the same time, if they had admitted it, I wouldn't believe that either. Remember
that a lot of these elites are coming from intelligence officers. They're coming from
people within the intelligence community who do have an interest in shaping a narrative,
way or the other. This is one that you should probably follow because I have a
feeling that this train of the investigation will probably eventually
lead to a real answer that is going to be disputed by pretty much the entire
international community, but it does appear to at least be on the
right track for once. A lot of the other theories, plausible and sure, but lacking
a lot of documentation, a lot of evidence. This seems to, at least by the reporting,
be following physical evidence, which is nice, rather than creating a theory and then trying
to find the evidence to match the theory. So, I don't have a hard take on it yet because
I haven't seen enough evidence. But this one is more interesting. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}