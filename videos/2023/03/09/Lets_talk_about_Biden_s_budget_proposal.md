---
title: Let's talk about Biden's budget proposal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=efZt3fW2bs4) |
| Published | 2023/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Biden's budget proposal and its components, including reducing the deficit by 3 trillion over 10 years.
- Increasing taxes on the wealthiest Americans and quadrupling taxes on corporate stock buybacks are key measures.
- Making Medicare solvent through 2050 by raising contributions from top earners from 3.8 percent to 5 percent.
- Not raising taxes on individuals making less than around $400,000 a year is a significant aspect.
- Biden's budget proposal also includes more price negotiation for pharmaceuticals and similar measures.
- The budget proposal is a dare and comparison by the Biden administration to challenge Republicans and let the American people decide.
- Congress controls the budget, not the executive branch, making the proposal more of a statement than a definitive plan.
- Republicans are in a tough spot as they may struggle to match the numbers needed to counter Biden's proposal.
- The debt ceiling debate and related brinksmanship may be rendered pointless if a balanced budget is not achievable.
- Biden's move is strategic, forcing Republicans to reveal their plans and math to the public.

### Quotes

- "It's a show. It's one of these moments where Biden is literally saying, okay, here are my cards, let's see yours."
- "It's a political maneuver to get the Republican Party to show their work."
- "Everything's a show. It's just something to entertain their less informed base."

### Oneliner

Analyzing Biden's budget proposal, daring Republicans to present a better plan, and banking on public reception while revealing strategic political maneuvering.

### Audience

Political analysts, policymakers

### On-the-ground actions from transcript

- Contact your representatives to express your views on budget proposals and fiscal policies (implied).

### Whats missing in summary

Insights on the potential impact of public opinion and Republican response on budget negotiations.

### Tags

#Biden #BudgetProposal #Taxation #Medicare #Congress #Republicans


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Biden's budget.
We're going to talk about what's in it.
And then we're going to talk about what it really is because it isn't a budget
proposal, not really.
Um, and we'll get to why it's not in a second, but here's, here are the, uh, the
key pieces that are going to be blasted all over the media, reducing the deficit
by 3 trillion over the next 10 years. This is going to be done
by increasing the taxes on the wealthiest Americans
and by quadrupling the taxes on corporate
stock buybacks. It will make
Medicare solvent through 2050
by increasing the contribution from the
the top people from 3.8 percent to 5 percent.
And those are the two pieces that I think everybody's going to hit on.
The reduction in the deficit and the Medicare solvency through 2050.
All of it, according to this plan, without raising taxes on people making less than about
400, 400,000 a year.
Okay.
And to be clear, there's other stuff in it.
There's more price negotiation for pharmaceuticals and stuff like that, and we've already seen
how that actually does pay off long term.
But those two pieces are the pieces that are really going to matter because it's not actually
a budget.
Schoolhouse rock stuff here.
The executive branch doesn't control the budget.
It's a proposal.
They put something out, but who controls the budget?
Congress, Congress does.
They have the purse strings.
So what this is, is really a dare.
It's a dare and it's a comparison.
This is the Biden administration saying, hey, this is what we've got for the American people,
Republicans.
I dare you to show them what you want to do.
And then the American people get to decide.
There will be tons of discussion about it once Republicans release whatever they plan
on releasing.
At this point, they have talked themselves into a really difficult position because there's
no way they are going to get to the numbers necessary to make all of this posturing make
any sense.
They're running the risk of ruining the entire economy for talking points that realistically
they're not going to get to because the methods in which they could get to it, they've taken
them off the table, or McConnell over in the Senate has already told them that's not going
to fly.
So they are going to have to settle for something less than a balanced budget.
Because of that, the debt ceiling debate, all of this brinksmanship, it's pointless.
It is pointless.
If they're not going to a balanced budget, it doesn't matter.
Everything's a show.
It's just something to entertain their less informed base.
the less informed members of their base.
That's all it is.
It's talking points for them, and the Republicans in the House know they probably won't see
through it.
But for the rest of America, they're going to have to choose between whatever it is the
Republican Party's going to propose.
And this plan that doesn't raise taxes on average people, raises taxes on the wealthy,
Medicare solvent and reduces the deficit. It's a show. It's one of these moments where
Biden is literally saying, okay, here are my cards, let's see yours. And he is banking
on the fact that Republicans aren't going to be able to put something together that's
better than this. And he might be right. As far as the reception from the average American,
very well might be right, but we're gonna have to wait and see. This is why you
have a lot of people saying that this is symbolic and it doesn't matter and all
of that stuff. It's because the executive branch doesn't control the the Treasury.
It doesn't control the purse strings like that. Congress does. But it's not
necessarily pointless. It's a political maneuver to get the Republican Party to
show their work. Let's see the math, guys. It's really what it's about. Anyway, it's
It's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}