---
title: Let's talk about Biden's billionaire tax plan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YsJPwJkSj2o) |
| Published | 2023/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on additional information following his last video on Biden's budget.
- Mentions a potential proposal by the Biden administration for a minimum tax rate on income for billionaires at 25%.
- Acknowledges the pushback expected from billionaires and their allies in Congress against this proposal.
- Emphasizes the need for Congress to approve such proposals, not just the president.
- Expresses skepticism about the likelihood of the billionaire tax proposal passing due to the influence of wealthy interests in Congress.
- Suggests that bipartisan support from average voters might be necessary to push such a proposal through.
- Notes the symbolic nature of the proposal in setting the tone for economic debates and portraying the Biden administration as progressive.
- Concludes by mentioning this proposal as part of Biden's economic plan that will likely receive significant attention and debate.

### Quotes

- "It's being suggested that the Biden administration is going to suggest a billionaires tax."
- "The idea that a bunch of billionaires are going to allow a billionaire tax to go through seems pretty slim."
- "You're talking about people who are, well, billionaires. They have a lot of money, they have a lot of influence."
- "The only way to overcome that would be sheer numbers."
- "It's definitely setting the tone for an economic debate and it is casting the Biden administration in a more progressive light."

### Oneliner

Beau provides insights on the potential billionaire tax proposal in Biden's budget, facing challenges from wealthy interests and needing bipartisan support for approval.

### Audience

Policy analysts

### On-the-ground actions from transcript

- Contact your representatives to express support for the billionaire tax proposal (suggested).

### Whats missing in summary

Deeper analysis and implications of the billionaire tax proposal within Biden's economic plan.

### Tags

#Biden #TaxProposal #WealthInfluence #Congress #EconomicDebate


## Transcript
Well, howdy there, internet people.
It's Beau again.
So shortly after filming the last video on Biden's budget, got another
piece of information.
And while I'm still not sure if this is going to show up in the budget
proposal, or this is going to be something separate, it is definitely
worth covering because in that first video, I talked about two things
that I thought was going to get a lot of attention, there's also this.
It's being suggested that the Biden administration is going to suggest a billionaires tax,
minimum tax rate on income for billionaires. So if you are part of that 0.01%,
percent, you will have a minimum tax rate of 25 percent. That's the way it's being
explained to me. And this is on new income during that year, no matter what, 25
percent. Obviously there's going to be pushback against that, you know, from
people that have deep pockets. Those are going to be people who care about it. I
I would suggest that if you are lucky enough to fall into this category, maybe it's okay.
Now everything else from that first video still applies to this though.
It's a proposal.
It's really cool to see something like this actually suggested.
At the same time, the president doesn't make these rules.
This is something that we'll have to go through Congress.
The idea that a bunch of billionaires are going to allow a billionaire tax to go through
seems pretty slim.
All they have to do is convince Republican allies in the House to vote against it, and
it's over.
That's gone.
And to be clear on this, if the Democratic Party controlled the House, I would still
I would still wager that billionaires would find the allies to vote
against it. You're talking about people who are, well, billionaires. They have a
lot of money, they have a lot of influence, and that gets them a lot of
people in Congress. The only way to overcome that would be sheer numbers.
You would need Republicans and Democrats, as far as average voters, calling their
representative saying, hey, make this happen. It seems unlikely. You know, there
is that general idea in the United States that everybody will one day be a
billionaire, so have to protect the interest of the billionaire, so one day
when you're on top you'll reap those rewards. So it is interesting to see it
proposed. Biden has actually proposed something like this in the past. This
time it's actually five percentage points up as far as the minimum tax. I
don't I don't expect this to actually go through though. I would be really
surprised. It just seems like all of the very wealthy interests will be working
against this particular item. But it's definitely setting the tone for an
economic debate and it is casting it's casting the Biden administration in a
more progressive light, in a tax the rich light, even though in this case there's
really not much risk to them suggesting it because it is incredibly unlikely to
get through Congress. So it's not like they're going to upset their wealthier
donors. But I would add that to the list of the other items from his economic
proposal that are going to get a lot of press. I would be surprised if by the
time this video comes out it's not already being widely discussed. Anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}