---
title: Let's talk about if NY is handling it wrong with Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YocHYL_LdLA) |
| Published | 2023/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Some suggest New York should have waited to pursue Trump after bigger cases.
- Beau disagrees and believes there's no coordination among prosecutors.
- He thinks starting with New York is appropriate due to the relatively minor charges.
- Trump's calls for support aren't gaining much traction possibly due to the perceived minor nature of the charges.
- Beau sees a slow progression in legal actions as beneficial for supporters to come to terms with reality.
- He believes starting with smaller cases may prevent supporters from making regretful decisions.
- Beau underscores the lack of coordination among prosecutors and the varying legal systems across jurisdictions.
- Legal processes' speed is dependent on filed motions and judicial decisions.
- Beau suggests New York starting with minor charges is advantageous for all parties involved.
- He expresses that Trump might have a better chance at mounting a defense in New York compared to other jurisdictions.

### Quotes

- "I think a slow kind of descent into reality is probably a good thing."
- "Starting with the relatively minor stuff in New York is probably a really good thing for his supporters."
- "I don't think New York is doing it wrong."
- "If they have the evidence, they pursue it."
- "It's just a thought."

### Oneliner

Beau believes starting with minor charges in New York regarding Trump is beneficial for supporters, allowing a gradual acceptance of reality and potentially preventing regretful actions.

### Audience

Legal analysts and interested individuals.

### On-the-ground actions from transcript

- Keep informed about legal proceedings in different jurisdictions (suggested).
- Stay updated on developments regarding the legal cases involving Trump (suggested).

### Whats missing in summary

Insights on the potential impact of legal proceedings on Trump's supporters and the importance of a gradual legal process.

### Tags

#LegalSystem #Prosecution #Trump #NewYork #Coordination


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about whether or not
New York is doing it wrong
when it comes to former President Trump.
There have been a number of people who have suggested
that New York should have waited they should have gone last
because relatively speaking,
what New York is looking at Trump for,
Well, it's kind of minor in comparison
to the other jurisdictions.
So there are those who suggest that they should have waited,
should have started off with a big case,
and then dealt with the little stuff.
I disagree, actually.
I disagree.
First thing I want to say is I don't think there's
that level of coordination.
I don't think that the prosecutors are coordinating
their events with each other.
I wouldn't be surprised to learn that they talk to each other,
but these are different jurisdictions
with different political and legal realities.
I don't think they're coordinating like that.
But if I could pick, like if somebody said,
hey, you determine the order in which they go,
I would have started with New York, I would have started with New York because
it's relatively minor in comparison.
You know, you look at Trump's calls for people to get out there and support him
and say his name and all that stuff.
Those calls, there aren't a lot of people picking up the phone right now.
And I think it's because it is relatively minor.
I think it's because people are looking at it and being like, well, this isn't a big
deal, and this is that little thing, it doesn't matter.  So people aren't getting as energized over it. At
least not yet.
And I think a slow kind of descent into reality is probably a good thing.
thing. It gives people time to get acclimated to it. If it was me, it would be New York,
then Georgia, then any federal cases. But that's simply to assist his supporters, really.
Those people who might be very angry, those people who may not have come to terms with
they might have been misled. If it moves from a smaller thing to a larger thing,
it gives them time to adjust and hopefully might stop them from doing
something they'd regret. But again, I really don't think there's that level of
coordination. I don't think... I don't think it's happening like that. I don't
think that the prosecutors are talking to each other and trying to decide who
goes first or anything like that. I don't think that's happening at all. They may be
keeping each other up to date in the sense of this is what we're doing right now, this
is what we're doing, but I don't even know that that's happening. I would remind everybody
that there are a lot of steps in the legal system and how long each step takes, I mean
that's up to how many motions get filed, what judges say, it's really hard to kind of coordinate
like that.
Even if the prosecutors wanted to, the legal systems in the various jurisdictions really
wouldn't let them.
I think they're all moving as fast as their jurisdiction allows and the evidence and following
where the evidence leads them and in some cases it may take them down a side path.
But I think that starting with the relatively minor stuff in New York is probably a really
good thing for his supporters. I think they're less likely to to be energized to the point
where they make a mistake and do something that they'll regret. And that's good for
everybody. That's good for them. It's good for the people that might be in the area where
they get out of hand. It seems like that's the best route. So, I don't think New York is doing
it wrong. I don't think they should have waited. If they have the evidence, they pursue it.
We're going to have to wait and see on all of this because there are, in New York,
In New York, Trump probably stands a pretty good chance of mounting a decent defense.
I've made my opinion clear on some of the other cases, but in New York, I do think that
there's enough room for him to present a defense that might actually be effective.
It's something we're just going to have to wait and see.
And we're going to have to wait and see how people react to it.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}