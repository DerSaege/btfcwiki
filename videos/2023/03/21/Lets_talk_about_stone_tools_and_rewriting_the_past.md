---
title: Let's talk about stone tools and rewriting the past....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ck_v_PEM2bM) |
| Published | 2023/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Monkeys in Thailand use rocks as tools to smash open nuts, inadvertently creating stone tool-like flakes during the process.
- This observation challenges the long-held belief that early humans exclusively made stone tools intentionally during the Stone Age.
- Scientists are now reconsidering whether early humans accidentally created tools in a similar way to the monkeys, using stones as hammer and anvil.
- There is also a possibility that other primates, not humans, might have been the original toolmakers, with early humans simply utilizing the found stone splinters.
- The implications of this observation are significant, leading to a reevaluation of early human history and potentially rewriting parts of it.
- Toolmaking has been a key factor in understanding the advancement and worldview of early humans.
- If early humans did not intentionally make these tools but found and utilized them, it challenges previous notions about their capabilities and achievements.
- Scientists will need to analyze the locations where these tools were found to determine if they were accidental or not human-made.
- The evolution of human history is constantly evolving, with new observations prompting revisions in our understanding.
- The story of humanity is always in flux, with each new discovery reshaping our perception of the past.

### Quotes

- "Monkeys in Thailand use rocks as tools to smash open nuts."
- "The story of humanity is always being rewritten."
- "Our story, the story of humanity, it's always being rewritten."

### Oneliner

Monkeys inadvertently creating stone tool-like flakes challenge the belief that early humans exclusively made tools intentionally, prompting a reevaluation of human history.

### Audience

History enthusiasts, scientists, curious minds

### On-the-ground actions from transcript

- Analyze stone tool findings to determine accidental or human origin (implied)
- Stay updated on new scientific discoveries challenging historical narratives (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of how a simple observation about monkeys using stones as tools has profound implications for understanding early human history.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about monkeys and stones,
and humans and tools, and how some observations are causing
scientists to rethink a whole lot about early human history.
There are monkeys in Thailand, and these monkeys
use rocks to smash open nuts. Okay? They'll use one rock as kind of like an anvil and
another rock kind of like a hammer. They break open the nut, they eat the nut. Okay? Nothing
too complex there. However, it has now been noticed that during this process, flakes come
off of the stones when they're smashing them together. If they, maybe they missed the nut
door or whatever. The flakes look remarkably like early human tools. Stone
tools that for a very long time scientists have kind of attributed
exclusively to humanity and the implication being that humanity
intentionally made them during the Stone Age. Now because of these observations
and observations that have taken place in other places they're having to kind
of rethink that because it may end up being that they were accidentally
created, that humanity didn't start by making the tools, but either they could
have made them the same way, using hammer and anvil in it, kind of breaking off and
creating that tool on its own accidentally, and then them realizing it
could be used, or there's also the possibility that, well, humanity didn't
make them, that other primates made them, that they were using them in that fashion,
and early humans simply found those little splinters of stone that they used for cutting.
This is one of those bizarre observations that is going to lead to a lot of scientists
reviewing early human history and maybe even rewriting parts of it.
As they try to distinguish what was actually human made, what was intentionally made by
early humans, and what was just a found tool.
Toolmaking is one of those things that they use to distinguish early humans and how advanced
they were and how they viewed the world.
If it turns out that they didn't make these tools, they just found them and put them to
use, they have to reevaluate other things.
So they have to go through and look at where these were found and whether or not they could
be accidental, or maybe they weren't even made by people. It's a unique development,
something that's a little bit different that hit the news that I think people might be
interested in. Our story, the story of humanity, it's always being rewritten. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}