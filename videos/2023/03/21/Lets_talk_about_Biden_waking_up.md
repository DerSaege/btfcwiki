---
title: Let's talk about Biden waking up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Pw424GG_2-A) |
| Published | 2023/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's administration is taking a strong stance against the House GOP budget proposals this week.
- The Biden administration is bringing attention to the potential impacts of the GOP's proposed budget cuts.
- The GOP's proposals include defunding the police, cutting border patrol, train safety, healthcare, energy, manufacturing incentives, Medicare, and defense.
- Beau points out the contradictions in the GOP's proposals, such as advocating for border security while cutting Border Patrol jobs and defunding the police while criticizing lawlessness.
- The Biden administration's focus on revealing the consequences of the GOP's budget cuts aims to disrupt the GOP's talking points and shed light on the potential negative impacts on their base.
- Beau questions the timing of the Biden administration's offensive against the GOP's budget proposals, suggesting it may be strategically timed amidst other news events.
- He expresses concerns about the GOP potentially allowing the US to default if they don't get their way with the budget, despite the potential economic consequences.
- Beau notes the challenge Republicans may face in justifying their positions, especially regarding defunding the police and mischaracterizing Biden as a leftist.
- He hopes for outside pushback to prompt the GOP to reconsider their hardline stance for the benefit of the US economy.

### Quotes

- "You can't sit there and talk about border security while your proposal cuts, I want say 2,000 jobs, out of Border Patrol."
- "It's going to be weird hearing Republicans try to justify defunding the police after the last few years."
- "He is definitely pro-capitalism and they're going to have a hard time responding to this."

### Oneliner

President Biden's administration confronts the House GOP budget proposals, exposing contradictions and potential impacts on Republican priorities, urging reconsideration for the sake of the US economy.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Advocate for transparent budget proposals and policies (exemplified)
- Engage in political discourse and push for accountability from elected officials (exemplified)
- Stay informed about budget decisions and their potential impacts on communities (exemplified)

### Whats missing in summary

The detailed breakdown of the specific budget items and their implications on various sectors within the US.

### Tags

#USPolitics #BudgetProposals #GOP #BidenAdministration #EconomicImpacts


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about President Biden
waking up and deciding this is the week
to really start talking about something
and how the Biden administration is going to be pushing
certain elements all week.
And this is all in reference to
the House GOP budget proposals,
something that we have talked about
on this channel for a while.
We've gone through the numbers.
We have talked about how if the House GOP really plans on trying to achieve what they
are saying in public, the cuts are going to be massive and how they're going to hit Republican
priorities really, really hard.
Now the Biden administration is going on the offensive this week.
They're going to be talking about this all week.
Today, you will hear about how the GOP all of a sudden wants to defund the police.
How they want to cut border patrol.
How they want to cut train safety.
That's today.
Tomorrow, they're going to be focusing on healthcare and energy costs and how the cuts
to healthcare and energy are going to raise prices for, well, everybody, but particularly
those who live in red states.
Then on Wednesday they're going to talk about how getting rid of the manufacturing
incentives
will
encourage
manufacturing jobs to be shipped to overseas.
On Thursday they will talk about the expected Medicare cuts
and how that's going to hit seniors and keep in mind
older people
tend
to vote
more conservative.
They tend to vote for the GOP a little bit more
statistically speaking. And then on Friday, the big one, defense cuts and what is going to do to
national security. Now, here's the thing about this. We've been through most of this. I don't
think I've talked about the manufacturing stuff, but we've been through most of this on the channel.
Everything the Biden administration is going to say this week is true. If the GOP in the House
really plans on trying to make good on all of their public statements, it's
going to take really deep cuts to a whole bunch of Republican
priorities and it is going to disrupt the rest of their talking points. You
can't sit there and talk about border security while your proposal cuts, I want
say 2,000 jobs, out of Border Patrol.
You can't talk about how the left is lawless when you're
advocating to defund the police.
You can't talk about America first and making America great
again and bringing the jobs back when you're getting rid
of the incentives to do so.
This is going to disrupt the GOP a lot, especially
when the impacts on veterans come out.
The reality is, if the House GOP wants to do what it is publicly saying, it is going
to hit their base hard.
And it's all fun and games to rally around talking points until it hits your wallet.
And these cuts are going to hit Republican wallets hard.
The Biden administration has decided, for whatever reason, this is the week to go after
it.
Seems a little odd to me.
I don't know that I would have done it this week, but maybe the Biden administration is
counting on people channel surfing and watching the news a whole lot because of some things
that might be happening in New York.
If that's their logic behind it, okay, I get it.
I don't know that I would have done it when there's another big competing news story.
But I'm not going to complain because for once it looks like the Biden administration
is going to come out swinging.
We'll have to wait and see how committed they are to it, but the Republican Party in the
House has talked itself into a corner.
They have made all of these wild claims in public about what they want to do and how
deep they want to make these cuts and now they're finding out what they're
actually going to be cutting. I have I have a lot of concerns that because a
lot of the House GOP is in its own social media echo chamber and feedback
loop that they may actually allow the US to default if they don't get their way.
Which I mean I think it was the Congressional Budget Office or maybe it
it was Moody's, but one of them, one of the analysis kind of showed that a short-term
default would actually be better for the U.S. economy than the Republican budget.
So I mean, there is that.
Either way, expect a lot of talk about the budget this week because the Biden administration
has made it very clear that this is what they're going after and they're going to talk about
it.
The Republican Party isn't going to have a lot that they can hit back with because the
reality is if they want to do what they have publicly said they are going to do, it's going
to take these cuts.
It's going to take very deep cuts to things that the Republican Party has viewed as core
principles and there's no way around it.
It's going to be weird hearing Republicans try to justify defunding the police after
the last few years.
It's going to be weird seeing them try to square their image of Biden because remember
in those circles he is cast as some leftist.
For those that don't know, he's not.
He's center-right.
He is not a leftist in any meaningful version of the term.
He is definitely pro-capitalism and they're going to have a hard time responding to this.
We can hope that there is enough pushback from outside of their echo chamber that they
start to adjust their views on this and maybe soften their position because if they stick
to a hardline position on this, it
doesn't matter if they get their way or it defaults.
It's really bad for the US economy.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}