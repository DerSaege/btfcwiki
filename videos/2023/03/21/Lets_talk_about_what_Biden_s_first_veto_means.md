---
title: Let's talk about what Biden's first veto means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=s2UQltN2IKY) |
| Published | 2023/03/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's first veto was on legislation that aimed to limit financial managers in considering factors like ESG scores, climate change, and social considerations.
- The legislation, pushed by the Republican Party, wanted to prioritize profit over other factors, but Biden vetoed it.
- This veto serves as a reminder to the House that Biden will use his veto power, setting the stage for future battles with the Republican Party.
- It's unlikely that Republicans in Congress will have the votes to override a veto.
- Typically, more vetoes follow once the first one is used, often as a show for the opposing party's base.
- There might be extreme legislation pushed forward by Republicans in the House, but with Biden's veto power, these measures are unlikely to become laws.

### Quotes

- "As long as Biden has the veto pen, it's unlikely that they'll be able to get anything really wild through."
- "You may see some incredibly extreme legislation move forward, but it really doesn't stand a chance of actually becoming law."

### Oneliner

President Biden's first veto sets the stage for future battles with the Republican Party, signifying a pushback against extreme legislation.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Stay informed about legislative developments and the power dynamics between parties (implied)

### Whats missing in summary

Insights on how understanding veto power dynamics can shape expectations around legislation and political battles.

### Tags

#VetoPower #Legislation #RepublicanParty #PresidentBiden #PoliticalBattles


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about President Biden's first veto recently
in a video, I mentioned a piece of legislation and said it was potentially
his first veto.
It's his first veto.
He vetoed it.
Um, and if you want to know more about what the legislation does exactly, I'll
have the video down below, but it was expected that he would veto.
it. Short version of what it does is that the Republican Party wanted to limit
financial managers in what factors they consider when it comes to ESG scores and
stuff about climate change and social considerations and stuff like that. As
far as the Republican Party concerned, profit was all. That's what they should
be looking at. They shouldn't look at the other stuff. And the legislation would
have prohibited them from looking at other stuff. Biden said no. The veto was
pretty expected. I imagine the Republican Party expected it as well, but it's also
a signal. It's a reminder to those in the House in particular that President Biden
does have a veto pen, and he will use it.
You know, it was unlikely in any event that he would sign this, but him actually using
that veto power kind of sets the stage for future battles with the Republican Party,
particularly those in the House.
And we can kind of start to see the lines form and where they're going to be.
We see who in the Senate on the Democratic side of the aisle might be willing to reach
across the usual suspects, Manchin or other people who may be vulnerable coming up to
re-election.
But we're also reminded that it's unlikely that Republicans in Congress are going to
get the votes to override a veto.
Generally, once the first veto gets used, more follow.
And most times it's a show for the party that is not really in power, the opposing
party to the president, it's a show for their base. They send up legislation they know has
no chance of getting signed. It's going to be harder for them to do that given the situation
in the Senate, but it's not impossible. So we may start to see more legislation move
forward from the Republican party that in many cases might be really unnerving because
may get passed in some cases, but Biden would veto it and they don't have the votes to override it.
So, beyond the news of the legislation being vetoed, which was expected, there's kind of the
warning that you may see some incredibly extreme legislation move forward, but it really doesn't
stand a chance of actually becoming law.
And that's something that people need to keep in mind if Republicans in the House start
pushing for some pretty extreme measures.
Just as it moves, remember, as long as Biden has the veto pen, it's unlikely that they'll
be able to get anything really wild through because they're not going to get the votes
to override his veto.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}