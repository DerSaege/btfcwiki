---
title: Let's talk about the importance of Wisconsin's 2023 election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=w1AK_q2ZMkc) |
| Published | 2023/03/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wisconsin is facing a consequential judicial election in 2023, with one Supreme Court seat up for grabs.
- The current political landscape in Wisconsin includes a Democratic executive branch, a Republican-controlled legislature, and a conservative-leaning Supreme Court.
- The candidates vying for the seat are Janet Protasewicz (liberal) and Daniel Kelly (conservative).
- The outcome of this election is significant due to Wisconsin being a swing state, with potential implications for future elections, especially in 2024.
- Potential issues at stake include family planning laws dating back to 1849.
- The amount of money being spent on this judicial race underscores its importance and the high expectations from both parties.
- The election on April 4th is likely to have national implications, given Wisconsin's swing state status.
- The judicial rulings in this race could impact various issues like redistricting, voting rights, and family planning.
- Despite being an off-year election, this race is expected to be highly consequential and closely watched.
- Beau does not endorse candidates but distinguishes Protasewicz as the liberal and Kelly as the conservative.

### Quotes

- "Wisconsin is a swing state."
- "The candidates are Janet Protasewicz, if you live up there you find that funny because of the ads, and the other is Daniel Kelly."
- "This is a race that the entire nation is going to end up kind of waiting on rulings from, I would imagine, come 2024."

### Oneliner

Wisconsin's pivotal judicial election in 2023 could sway national implications in a swing state, impacting issues from family planning to voting rights.

### Audience

Wisconsin voters

### On-the-ground actions from transcript

- Pay attention to the upcoming judicial election in Wisconsin (suggested)
- Stay informed about the candidates and their stances on key issues (suggested)
- Engage in voter education and outreach efforts within your community (implied)

### Whats missing in summary

Deeper insights into the specific policy differences between the candidates and the potential consequences of the election beyond 2024.

### Tags

#Wisconsin #JudicialElection #SwingState #VotingRights #FamilyPlanning


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today we are going to talk about what might be the most important
election of 2023 and Wisconsin and the Supreme Court.
As odd as it may sound, one of the most consequential elections of this
year is going to occur in Wisconsin and it's for a judge.
So, why? Let me lay the scene. Right now in Wisconsin, you have an executive branch that is Democratic controlled.
You have a legislature that is controlled by the Republican Party.
You have a Supreme Court that leans conservative, four to three margin.
One seat is up for grabs. One seat is up for grabs.
The candidates are Janet Protasewicz, if you live up there you find that funny because
of the ads, and the other is Daniel Kelly.
Protasewicz is the liberal, Kelly is the conservative.
So why does this matter more so than other places?
Why is tens of millions of dollars being spent on a state judicial race?
Wisconsin is a swing state.
Wisconsin is a swing state.
Imagine the claims from 2020 being repeated in 2024.
Imagine those heading towards the Supreme Court in a swing state.
If you have a Supreme Court that embraces Trump's claims or maybe embraces the independent
state legislature theory or something like that, it could lead to real problems.
The other thing that is at the forefront is a law from 1849, I think, about family planning.
That is likely to come before the Supreme Court.
So there is a whole lot on the line right now in Wisconsin.
And what happens here on April 4th is when I think the election is, will probably have
national implications.
It will be very consequential because it's a swing state.
because of the amount of money being pumped into it, there's a lot of expectations from
the parties.
Now again, Janet Protasewicz is the liberal, Daniel Kelly is the conservative.
This is a race that the entire nation is going to end up, is going to end up kind of waiting
on rulings from, I would imagine, come 2024.
And for people in Wisconsin, this obviously matters for everything from redistricting
to whether or not your vote's going to count to family planning to everything.
So if you're up there, this is one to pay attention to.
Off-year elections like that, they often don't get a lot of attention.
This one will be consequential.
I don't endorse candidates, but Janet Protasewicz is the liberal, Daniel Kelly is the conservative.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}