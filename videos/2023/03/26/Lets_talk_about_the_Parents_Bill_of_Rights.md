---
title: Let's talk about the Parents Bill of Rights....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tRfMWTF4014) |
| Published | 2023/03/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Parents Bill of Rights, also known as the Politics Over Parents Act, and addresses a message he received from a parent concerned about the legislation.
- Assures the parent that the legislation faces significant hurdles and may not become law.
- Clarifies that the legislation isn't about parental rights but rather about Republicans in the House targeting trans kids.
- Points out that the legislation passed in the House but faces uncertainty in the Senate, with slim chances of passing and President Biden unlikely to support it.
- Reminds listeners not to panic, as the odds of the legislation moving forward are very slim due to lack of support.
- Notes that similar legislation may continue to be proposed but reiterates that the chances of it becoming law are minimal.
- Acknowledges frustrations with the Democratic Party but underscores the importance of their role in preventing such legislation from advancing.
- Emphasizes the significance of voting strategically, even if the Democratic Party isn't perfect, to prevent harmful legislation from progressing.

### Quotes

- "This is what they're getting for their vote."
- "Understand without even the status quo Democrats right now, without them, this would be on its way to becoming law."
- "To them, picking on your kids is, it's their pathway to electoral victory."

### Oneliner

Beau explains the unlikely path of the Politics Over Parents Act, reassuring listeners about its slim chances of becoming law and stressing the importance of strategic voting.

### Audience

Parents, voters, progressives

### On-the-ground actions from transcript

- Stay informed on legislative developments and advocate against harmful bills (implied).
- Encourage strategic voting to prevent the advancement of detrimental legislation (implied).

### What's missing in summary

Detailed analysis of the potential impacts if the legislation were to pass, and a deeper exploration of the motivations behind such bills.

### Tags

#Parents #Legislation #PoliticalAnalysis #Voting #Democrats


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about what is being called by
some the Parents Bill of Rights, and what is being
called by others the Politics Over Parents Act.
And we're going to kind of go through a message and just
of talk about likelihoods and the reality and how it can help inform some
of us about different things because I got a message and it's it's definitely
worth responding to because I feel like there are probably other people who may
feel the same way. I moved with my daughter from a southern state to a
friendly state. Last year I just read that the parent's bill of rights passed.
Does that apply in my state even though state law says otherwise? What am I
supposed to do? And then it goes on to talk about the bad shape that their
child was in before the move and how much better they are now. Okay, so what
you supposed to do? Take a deep breath. Take a deep breath. All last week we
talked about how Republicans in the House were going to put forth just wild,
extreme legislation in that you probably shouldn't panic about it. That's
this. That is this. If you're not familiar with this, by the way, it has nothing to
do with parental rights, it's the House GOP bullying trans kids. Here's the
reality of where this sits, where this particular piece of legislation sits. It
started in the House and it passed in the House. However, they couldn't even get
all the Republicans in the House to vote for it. That's it. That's all that
has passed. It faces a, let's just call it a rocky future in the Senate, and the
Biden administration doesn't support it. They do not have the votes to override
a veto from him on this. For this to become law, it would it would require a
a chain of very bizarre circumstances. We are talking about a less than 1% chance here.
I'm sure it's unnerving, especially if you went through moving to get your child somewhere
would be welcomed. I'm sure it's very unnerving to see legislation like that at the federal level.
Just to understand the likelihood of it getting through the Senate, it's slim.
It's possible, but it's slim.
The odds of Biden signing it, really unlikely.
Him vetoing it, if he vetoes it, they do not have the votes.
They don't have the votes to override a veto from Biden.
So watch it, be aware of it, definitely remember it, but don't panic over it yet.
So just take a deep breath and understand that the odds of this moving forward are very
slim and there's going to be more like this because that echo chamber base that the Republican
Party has, they are more interested in going after teens and bullying them than they are
about attempting to solve any problem.
So this kind of legislation will continue to get proposed, but the odds of it actually
getting through is very slim.
And then there is something else for everybody else here, particularly people like me, who
often get very frustrated with the Democratic Party because they're not progressive enough
You know, they're not everything that we want them to be.
When you talk to your friends and they talk about voting out of
self-defense, this is what they're getting.
They're getting the ability to take a deep breath
and analyze the situation. The Democratic Party is not
everything that we want it to be in any way shape or form,
but understand without even the status quo Democrats right now, without them,
this would be on its way to becoming law. So when you hear your friends talk about voting out of
self-defense, remember they may have a reason. This is what they're getting for their vote.
So, the short version, the Politics Over Parents Act or the Parents Bill of Rights, whichever
name you want to use for it, it's very unlikely that it actually becomes law.
The downside is, yeah, if something like this gets through, it would supersede state law
in a whole lot of cases.
There would be tons of legal battles over it.
Some of what is contained in it is kind of in direct contradiction with other federal
laws.
Some of it is very much, at least to me, seems to be a violation of the real Bill of Rights.
But make no mistake about it, to them, picking on your kids is, it's their pathway to electoral
victory.
It's gonna keep happening.
Just for the moment, it seems really unlikely that any of it's going to get through.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}