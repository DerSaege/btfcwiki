---
title: Let's talk about LAUSD schools and deals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rUSidi3_CDc) |
| Published | 2023/03/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Los Angeles Unified School District workers, specifically support workers, felt undervalued, leading to negotiations and a strike.
- The average salary for workers in Los Angeles was around $25,000 a year, requiring many to have second jobs.
- The support workers' strike was backed by the teachers' union, with members encouraged to join picket lines.
- A tentative agreement has been reached after a three-day strike, with still pending ratification.
- Details of the agreement include a $1000 bonus for certain employees, a new minimum wage of $22.52, retroactive salary increases, and health benefits for part-time employees.
- There will be a $2 per hour increase for employees effective January of next year.
- The agreement also includes a $3 million investment in education and professional development for SEIU members.
- The negotiations likely involved intense back-and-forth, evident in the specific wage amount of $22.52.
- The union members seem confident that the agreement will be accepted by the membership.
- The teachers' union is also in separate contract negotiations, potentially leading to more developments in the future.

### Quotes

- "Los Angeles support workers felt undervalued, prompting negotiations and a strike."
- "A tentative agreement includes wage increases and benefits for workers."

### Oneliner

Los Angeles support workers strike for fair treatment, leading to a tentative agreement with wage increases and benefits, while teachers' union negotiations continue.

### Audience

Workers, unions, advocates

### On-the-ground actions from transcript

- Support the workers by staying informed about the progress of the agreement and potential future actions (implied).

### Whats missing in summary

Full details of the specific terms and conditions in the tentative agreement.

### Tags

#LosAngeles #WorkersRights #UnionNegotiations #SupportWorkers #TentativeAgreement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
Los Angeles, California, workers, agreements,
and specifically the Los Angeles Unified School District,
because there have been, let's just call them negotiations
going on, and it looks like those have come to a close.
It appears that a tentative agreement has been reached.
There was a three-day period in which there was a strike, and this is the SEIU.
These are support workers at the schools who felt like they were undervalued, probably
had a lot to do with it.
I want to say the average salary there is like 25 grand is what people are making a
a year in Los Angeles, requiring many of them to have second jobs.
They just felt undervalued and their strike was backed up by the teachers' union and
their members were encouraged to join the picket lines and so on and so forth.
A lot of parents, if that's what you're waiting for, that news, it does not appear
that there will be another strike.
A tentative agreement has been reached.
It still has to go through ratification, but it looks like things are going the right way
and they don't anticipate having another strike.
So what is in the agreement?
Okay, so there will be a thousand dollar bonus for current employees who were around back
in 22-21.
The new minimum wage for the school district will be $22.52, which if you are familiar
with negotiations, hearing $22.52, yeah, that was probably a really tough negotiation.
were literally going down to the penny on that one. I'm sure that that started as $22.55
or something like that. There will be salary increases that are retroactive, going back
for different dates and different times. A $2 per hour increase for employees effective
January of next year. Health benefits for part-time employees assigned to work four
or more hours per day. And let's see, there's a three million dollar investment in education
and professional development for members of SEIU, the union. Overall, it looks pretty
good and they certainly appear based on their statements. It certainly seems like they're
pretty confident that the membership and everybody is going to accept this. It's
worth noting that I'm fairly certain that the teachers union is also in
contract negotiations and that's separate from this. So there may be
maybe more to come on this story. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}