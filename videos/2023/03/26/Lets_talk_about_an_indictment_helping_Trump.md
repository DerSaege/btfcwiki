---
title: Let's talk about an indictment helping Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ui3-swP7aKs) |
| Published | 2023/03/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Trump's numbers in relation to a possible indictment and its impact on polls.
- Primary polling numbers among Republican loyalists are being referenced, but they don't matter for the general election.
- Comparing current primary polling numbers to previous election cycles to show how much things can change.
- Mentioning Nikki Haley's rise in polling numbers from single digits to 30% in a head-to-head matchup with Trump.
- Speculating that an indictment might energize Trump's base but won't help him win beyond the primary.
- Noting that moderate and independent voters turned against Trump before the events of January 6th.
- Pointing out that early polling numbers are receiving attention because of Trump's polarizing figure.
- Emphasizing that a possible indictment won't likely have a significant impact on Trump's chances in the general election.
- Mentioning that Republican voters might walk away from Trump if he were to face an indictment.
- Concluding that a sympathy bump from an indictment in the primary wouldn't last long.

### Quotes

- "They do not matter in terms of the general election."
- "He may get a bump in primary polling."
- "The polling numbers are going to change a lot."
- "I don't really see a possible indictment as being something that actually helps him."
- "Most Republican voters would walk away from him as fast as he walked away from the January 6th defendants."

### Oneliner

Analyzing Trump's primary polling numbers and the impact of a possible indictment on his chances beyond the primary, Beau concludes that while an indictment might energize his base, it won't likely help him win in the general election.

### Audience

Political analysts, Voters

### On-the-ground actions from transcript

- Stay informed about political developments and polling data (implied).

### Whats missing in summary

Deeper insights into the potential repercussions of a second Trump presidency and the dynamics within the Republican Party.

### Tags

#Trump #Polling #NikkiHaley #RepublicanParty #Indictment


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump's numbers
and the possibility that his current situation
will actually help him in the polls
because this is something that is being presented
by a lot of people.
And it's specifically related to polling
that deals with the primary.
that's where people are seeing it.
I'm going to read two messages.
One is from just the last couple of days,
and one is from a month ago.
Do you think Trump's possible indictment
will really help him in the polls?
A lot of conservatives seem to think so.
And the next one is, why are you wasting your time
on Nikki Haley?
She's a loser, barely polling in the single digits.
stop wasting your breath on her.
Okay, so first, all of the numbers that are being referenced,
they're polling numbers about the primary.
They're among Republican loyalists. People who, who no matter what,
they're not voting for a Democratic candidate.
Not to put too fine a point on this, but they don't matter.
They do not matter in terms of the general election.
They matter in terms of the primary, sure, but they don't matter in terms of whether
or not he stands a chance of getting us another term.
Okay, the second thing is we're a long way away from that primary, and I talked about
this recently.
The last Republican primary at this point in the game would be February, March of 2015.
Jeb Bush was in the lead, please clap.
He didn't become the nominee.
Now to show how much things can change and how quickly we're going to talk about Nikki
Haley and me wasting my breath because she's only polling in the single digits.
On February 22nd, she was polling at 6%.
Two weeks before, she was polling at 3%.
She doubled her share there.
That's great.
That's great.
I want to say the poll was conducted March 22nd to 23rd.
a head-to-head matchup between Trump and Haley.
Haley got 30%.
She is well out of single digits.
A lot of things are going to change.
Here's the thing.
An indictment will probably energize his base,
and it will get his base to show up.
So maybe they're going to be over counted in polling
because they're going to be more politically engaged.
But as far as it actually helping him win beyond the primary, no, no.
Remember, Trump became president-reject.
He lost before the 6th, before the 6th.
Moderate and independent voters turned against him before the 6th,
before all of the wild claims about the election,
before the apparent attempts to subvert the election,
before the alleged criminal activity
related to trying to overturn the election.
Those voters, they're not going back to Trump.
That's not likely in any way, shape, or form.
He may get a bump in primary polling. As far as it impacting the general, that
seems really, really unlikely. And I would also point out that there are a number
of situations that can occur between now and an election which make his polling
number is irrelevant. So it's just we are very early on in the primary process.
In a normal political climate, you wouldn't even be hearing about these
numbers. The only reason they're getting coverage now is because it's Trump.
Because it's Trump, because he's such a polarizing figure, people continue to
talk about it and bring up the polling numbers which realistically nobody would be paying
attention to at this point in time with any other candidates.
Part of it is there are a lot of people who understand how damaging a second Trump presidency
might be and they are very concerned and part of it is because there is still a loyal piece
the Republican Party that truly supports this guy. So he's getting a lot of press early on,
but a month ago, Nikki Haley was somebody not to waste your breath on, not to waste your time on,
don't even need to worry about talking about her because she's polling at three and six percent,
now in a head-to-head with Trump, she's getting 30%. The polling numbers are going to change a lot.
I don't really see a possible indictment in anything, in any of the many cases that
Trump is dealing with, as being something that actually helps him anywhere outside of
the primary.
And to be honest, I don't think it would really help him in the primary either.
I think there would be a sympathy bump and then it would drop.
I think that most Republican voters would walk away from him as fast as he walked away
from the January 6th defendants.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}