---
title: Let's talk about what happens if Trump gets indicted....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m-z1r45jooU) |
| Published | 2023/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the possibilities of former President Donald J. Trump's run amid legal troubles in Georgia, New York, and DC.
- Speculating on the impact of a potential indictment on Trump's ability to run and his base support.
- Posing questions on what happens if Trump wins the primary and is indicted before or after, and the potential trial's timing.
- Contemplating the unprecedented scenario of Trump being indicted but winning the general election.
- Exploring the uncertainty around historical precedents for dealing with a situation like Trump's legal challenges.
- Noting the challenge for the Republican Party in removing Trump if he wins the primary but is later convicted.
- Mentioning potential scenarios where Trump's legal troubles could become insurmountable post-nomination.

### Quotes

- "There isn't a lot of historical precedent for what could occur because this has never happened."
- "There's going to be a lot of maneuvering that we've never seen before, that the US political system has never witnessed."

### Oneliner

Analyzing the potential impact of legal troubles on Trump's run and the unprecedented scenarios that could unfold, leading to unprecedented political maneuvering.

### Audience

Political analysts, election strategists.

### On-the-ground actions from transcript

- Stay informed on the legal developments surrounding former President Donald J. Trump's potential run. (implied)

### Whats missing in summary

Further insights into the political implications and consequences of Trump's legal challenges, and the potential outcomes for the Republican Party.

### Tags

#DonaldTrump #LegalTroubles #PoliticalAnalysis #ElectionScenarios #RepublicanParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the possibilities.
What could happen concerning
former President Donald J. Trump's run
and the current situation that he finds himself in
in Georgia, in New York, in DC,
and, you know, all over the place, kind of.
And what happens as those things move forward?
Because it has prompted a number of questions.
First, can Trump run if he's been indicted?
Yeah.
Yeah, there's actually nothing stopping that.
He can totally continue to run.
And the impact an indictment might have on his base is fuzzy.
We don't really know.
There are signs because of things that have happened in the past with the impeachments
and the search and all of that stuff.
actually got kind of a bump in the polls among his base. So yeah, he might actually continue to run
and he might even do better during the primary for that. I think a more interesting question is
what happens if he wins the primary and he is indicted before or indicted after
and it goes to trial. I think that is a much more interesting question. Or what
happens if he is able to delay a potential trial until, you know, the
general election and how that plays out. Or, I mean, long shot here, but what if
he's indicted and is scheduled to go to trial and then actually wins.
What happens then?
The answer to these questions, we don't know.
We have no idea.
There isn't a, there's not a lot of historical precedent for what could occur because this
has never happened.
This is definitely something new.
Now it's worth noting that if he was unsuccessful in delaying a trial, if he wins the primary
and then ends up convicted, I'm not sure that the Republican Party can remove him.
In most states there's a cutoff period and if they choose him they're kind of stuck with
him.
There's a whole bunch of interesting scenarios.
The most common question is, can he run under indictment?
Yes, absolutely.
But that's nothing in comparison to some of the other questions that arise from his
current situation.
Because there are scenarios in which, as long shot as they may be, picture a scenario in
in which Trump wins the general election, but is pending trial in Georgia or New York.
There's wild, wild scenarios.
And then you have to look at the more likely possibility of him getting the nomination
and then his legal troubles just becoming insurmountable.
And then what the Republican Party can do then for their candidate.
Because in a lot of states, once that nomination is made, that's kind of set in stone.
They really can't do much.
Now in some of the states, maybe they changed the law to allow a replacement candidate or,
I mean, it's wild.
There are a lot of options because most of the current options would require Trump to
kind of bow out if that situation arose.
That seems really unlikely.
That just doesn't seem like Trump.
So if the witch hunts get him, I'm kind of eager to see how it plays out because there's
going to be a lot of maneuvering that we've never seen before, that the US political system
has never witnessed.
of those things, you know. May you live in interesting times. Anyway, it's just a
With that thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}