---
title: Let's talk about Biden, water, and forever chemicals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nNvc1-HBZS8) |
| Published | 2023/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of water and the EPA's new regulations on PFAS chemicals.
- PFAS chemicals are described as forever chemicals due to their slow degradation and water-repellent properties.
- These chemicals are contaminating the nation's water supply, impacting around 200 million people in the U.S.
- The EPA is focusing on six specific PFAS chemicals, implementing new regulations and treatment procedures.
- Four out of five states took no effective action against these harmful chemicals, prompting EPA intervention.
- Questions arise about funding for addressing the issue, with the Biden infrastructure bill allocating some billions for it.
- While the dedicated funds will help, more resources will be required over time to combat water contamination effectively.
- Emphasizes the daily necessity of water and the importance of immediate action rather than delays.
- Suggests that investing in solving water contamination issues is vital and not just a matter of throwing money at the problem.
- Anticipates ongoing news coverage as different regions decide on implementation and potential legal challenges.
- Encourages staying informed through local and state news outlets for updates on this significant issue.

### Quotes

- "They're called that because they don't degrade quickly, and they repel water. It's just all bad."
- "This may not be something that we want to delay and delay and delay."
- "When the problem is a shortage of funding, throwing money at it is like super helpful."
- "So look to state coverage if this is a topic you're really interested in."
- "Y'all have a good day."

### Oneliner

Beau addresses water contamination by PFAS chemicals, urging immediate action due to their harmful effects on millions of Americans and the importance of ongoing funding for long-term solutions.

### Audience
Environmental activists, concerned citizens

### On-the-ground actions from transcript

- Stay informed through local and state news coverage (suggested)
- Advocate for sufficient funding and resources to combat water contamination (implied)

### Whats missing in summary

The full transcript provides more in-depth information on PFAS chemicals, water contamination, and the EPA's regulations, offering a comprehensive understanding of the issue.


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today we are going to talk about water.
We're going to talk about water and the EPA putting out
some new rules, regulations, and what it means
and what's really happening.
They're looking at what are called PFAS chemicals.
The way they are most often described is forever chemicals.
They're called that because they don't degrade quickly,
and they repel water.
It's just all bad.
These chemicals are finding their way
into the nation's water supply.
This affects around 200 million people in the United States.
Big numbers.
The EPA has decided to look at six in particular,
and they are putting new regulations,
new treatment procedures, stuff like that into place
to deal with it.
Move in the right direction.
The reason they're doing it is because the dangers of this
and the fact that these chemicals can be harmful
have been known for a while.
Four out of five states did, well,
effectively nothing to deal with it.
So the EPA stepped in.
Now, it's the U.S. money, right?
That's what everybody wants to know about.
Who's gonna pay for it?
How much is it gonna cost?
All of that.
Now, the Biden infrastructure bill,
it actually had a few billion for this,
that it's gonna be dedicated for this issue.
That's gonna help, it's not gonna be enough.
There's going to be more that is needed.
And it's one of those things that I'll say more needed over time.
And sure, that's one way to look at it.
But I would suggest that most people use water every day.
This may not be something that we want to delay and delay and delay.
This is probably something that needs pretty immediate action.
And this is something where, quote, throwing money at the problem
might actually help.
People say that when they're looking for an excuse not
to act a lot of times.
When the problem is a shortage of funding,
throwing money at it is like super helpful.
So that's what's happening there.
Imagine that there will probably be
a lot of coverage about this coming out slowly
as different areas decide what they're going to do,
how they're going to implement it.
And I'm sure there's going to be some challenge from somebody
legally.
But this is probably something you're
going to see more news about in your local news
or your state news.
I don't know how much coverage it's going to get
on the national scene.
So look to state coverage if this is a topic
you're really interested in.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}