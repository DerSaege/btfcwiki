# All videos from March, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-03-31: The roads to school security.... (<a href="https://youtube.com/watch?v=3d1zkx8JGek">watch</a> || <a href="/videos/2023/03/31/The_roads_to_school_security">transcript &amp; editable summary</a>)

Beau explains the importance of creating delays in security through simple, cost-effective measures to ensure prompt responses and prevent potential risks in schools.

</summary>

"Security is not about making a location impenetrable; it's about making it so annoying that threats go somewhere else."
"You're not trying to defeat the multi-million dollar security system; you're trying to defeat the person getting paid $11 an hour to watch it."
"You're not trying to turn a school into a military installation or a prison; you're trying to maintain the atmosphere of the school."
"If you employ good physical security, it should be really unlikely that the first sign of a problem is a shot."
"While you have a bunch of people saying nothing can be done, there's always something that can be done."

### AI summary (High error rate! Edit errors on video page)

Focuses on prevention and security as top priorities to achieve a safe environment.
Explains that security isn't about making a place impenetrable but rather about deterring potential threats.
Emphasizes creating delays in security to allow for appropriate responses in emergency situations.
Proposes simple and cost-effective physical security measures to enhance safety in schools.
Suggests using fences, controlled access points, ID checks, and monitoring systems to create layers of security.
Advocates for the implementation of physical security measures without militarizing or prisonizing school environments.
Describes the importance of early detection, quick responses, and creating barriers to potential threats.
Recommends reinforcing ceiling areas for added security in emergency scenarios.
Stresses the significance of maintaining a school's atmosphere while ensuring student safety.
Concludes by encouraging action towards improving security measures to prevent potential risks.

Actions:

for school administrators, security professionals,
Implement controlled access points with ID checks and monitoring systems (suggested).
Reinforce ceiling areas for added security in emergency scenarios (implied).
Maintain exterior doors closed except for approved entry points (exemplified).
Train staff on responding to security breaches and implementing lockdown procedures (suggested).
Ensure physical security measures are in place without militarizing school environments (implied).
</details>
<details>
<summary>
2023-03-31: Let's talk about a North Dakota Republican and pronouns.... (<a href="https://youtube.com/watch?v=h3FIUgOpfjc">watch</a> || <a href="/videos/2023/03/31/Lets_talk_about_a_North_Dakota_Republican_and_pronouns">transcript &amp; editable summary</a>)

Republican governor of North Dakota vetoed bill on pronouns, revealing a principled stand against state overreach amidst a split within the conservative movement supporting trans rights.

</summary>

"It's not inconceivable, it's just not something you see very often."
"Those who actually stood by their principles, I have a feeling you're going to see them get really loud soon."
"Expect to see more of this in various ways."

### AI summary (High error rate! Edit errors on video page)

Republican governor of North Dakota vetoed a bill to stop schools from using pronouns.
Governor's image is that of a small government conservative.
Governor's veto was based on principle of limiting state overreach.
Vetoed a bill in 2021 regarding trans students playing sports in school.
Concerning news: state legislature wants to override the veto.
Faction of small government conservatives or anti-authoritarian right supporting trans people.
Split on the right regarding trans people owning guns.
Some Republicans standing by their principles amidst the authoritarian sweep in the party.
Expect this principled group to become louder as MAGA and Trump influence wanes.
Anticipates this group to regain lost power within the Republican Party.

Actions:

for conservative activists,
Contact local representatives to voice support for the governor's veto (suggested).
Join small government conservative or anti-authoritarian groups advocating for limited state overreach (implied).
Organize events to raise awareness on the importance of standing by principles within political movements (exemplified).
</details>
<details>
<summary>
2023-03-31: Let's talk about Trump, political reactions, and personal opinions.... (<a href="https://youtube.com/watch?v=qhOWOxV3hVE">watch</a> || <a href="/videos/2023/03/31/Lets_talk_about_Trump_political_reactions_and_personal_opinions">transcript &amp; editable summary</a>)

Beau Young addresses Trump's developments, criticizes Republicans rushing to his defense, and warns against premature assumptions of guilt by Democrats in a legal matter.

</summary>

"I appreciate you immediately changing track and not dancing around celebrating Trump's indictment."
"A former President of the United States was indicted. That's a big deal."
"Assuming that he is going to be found guilty is not something that I think they should do."

### AI summary (High error rate! Edit errors on video page)

Talks about his personal opinion of the Trump developments and addresses a question from a conservative that prompted the discussion.
Appreciates the objective reporting on Trump's indictment and the calm demeanor despite being a never-Trump-er.
States that based on publicly available information, the New York case against Trump seems to be the weakest among all he is facing.
Mentions a high likelihood of two more indictments, which he believes are stronger cases than the one in New York.
Expresses opposition to Trump beyond just being a Never Trump-er.
Urges for objectivity when discussing legal matters and moving away from the theoretical and political commentary.
Criticizes Republicans rushing to Trump's defense, stating that their words may backfire in the future.
Points out that the indictment of a former President is a significant event that will have lasting political implications.
Comments on Democrats potentially overplaying their hand and warns against assuming guilt before legal proceedings conclude.
Stresses the importance of maintaining professionalism and presumption of innocence in legal matters.

Actions:

for observers and commentators.,
Contact your representatives to urge them to prioritize professionalism and presumption of innocence in legal matters (implied).
</details>
<details>
<summary>
2023-03-31: Let's talk about Trump, NY, and what happens next.... (<a href="https://youtube.com/watch?v=QkfEiKWmdA4">watch</a> || <a href="/videos/2023/03/31/Lets_talk_about_Trump_NY_and_what_happens_next">transcript &amp; editable summary</a>)

Beau explains the uncertainties surrounding Trump's processing in New York and the need to shift expectations from knowing things ahead of time to speculation for security reasons.

</summary>

"The habit of knowing what's going to happen ahead of time - I wouldn't count on knowing that anymore."
"It may come as a total surprise."
"The expectations of knowing things ahead of time, those need to change."
"That needs to start to fall under speculation rather than what we know."
"Anyway, it's just a thought. Y'all So have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the uncertainty surrounding Trump's processing in New York and the need to re-evaluate expectations of knowing things ahead of time.
Trump is expected to be processed in New York in the next few days under Secret Service protection, but the media's Tuesday expectation may not be accurate.
Due to security concerns, both inaccurate information from the media and the DA's office may lead to uncertainties about timelines.
The DA's office may have reasons to withhold accurate information about when Trump's indictment is expected.
The traditional habit of knowing things ahead of time may no longer be reliable for security reasons.
There is a possibility that Team Trump was informed about the situation in New York either out of courtesy or through a leak, leading to a lack of courtesy in other areas' prosecutors notifying Trump before indictment.
The shift from political to legal commentary underscores the changing expectations of knowing things ahead of time regarding Trump's legal matters.
The Secret Service and investigating jurisdictions are not obligated to inform the public in advance of Trump's legal proceedings, leading to increased speculation rather than certainty about the timeline.

Actions:

for legal commentators, political analysts,
Stay informed on legal proceedings concerning public figures (exemplified)
Understand the shifting dynamics of legal commentary and the implications for public knowledge (exemplified)
</details>
<details>
<summary>
2023-03-30: Let's talk about the FDIC and bank assessments.... (<a href="https://youtube.com/watch?v=HdMFBGcYKKY">watch</a> || <a href="/videos/2023/03/30/Lets_talk_about_the_FDIC_and_bank_assessments">transcript &amp; editable summary</a>)

Beau explains how the FDIC is ensuring larger banks, not Americans, cover the costs of shoring up its fund, protecting smaller community banks from adverse impacts.

</summary>

"The FDIC is insured by banks."
"They're going to stick it to the big banks."
"The FDIC is basically going to really make the banks bail themselves out."

### AI summary (High error rate! Edit errors on video page)

Explains the role of the FDIC in covering failed banks and how it is essentially insurance for bank accounts.
Mentions that recent bank failures have cost the FDIC around $23 billion, prompting the need to shore up their fund.
Notes that the FDIC determines the assessment rates that banks have to pay, with the current plan being to have larger banks contribute more.
Indicates that the goal is to ensure that smaller community banks are not heavily impacted by the fund shoring up efforts.
Suggests that larger banks may face higher assessment rates compared to smaller banks, potentially shifting the burden away from community banks.
Emphasizes that the FDIC seems to be aiming for larger banks to bail themselves out rather than passing the cost to Americans.
Mentions the pressure on the FDIC to ensure that smaller banks do not bear the brunt of the fund restructuring.
Points out that larger banks may see an increase in their insurance payments to cover the fund's stabilization.
Concludes with the idea that industry leaders, i.e., big banks, are likely to be the ones primarily responsible for covering the costs of shoring up the FDIC fund.

Actions:

for financial enthusiasts, bank customers.,
Contact your local community bank to understand how they may be affected by the FDIC's fund stabilization efforts (implied).
Monitor news updates on the FDIC's assessments and fund restructuring to stay informed about potential impacts on different banks (implied).
</details>
<details>
<summary>
2023-03-30: Let's talk about new developments from the Fox case.... (<a href="https://youtube.com/watch?v=vZnmOPdpVkM">watch</a> || <a href="/videos/2023/03/30/Lets_talk_about_new_developments_from_the_Fox_case">transcript &amp; editable summary</a>)

Dominion seeks testimony from key Fox News figures in a case against Fox, potentially damaging their brand and sparking intense legal battles.

</summary>

"Dominion wants people like Rupert Murdoch and the Fox News hosts, Carlson, Hannity, Ingram, all of them, to testify at trial."
"Fox is going to fight this with everything that they have."
"Having a record in a trial of one of their hosts being asked, well, why did you say this in this message is that's going to be damaging not just to the case but to their overall brand."

### AI summary (High error rate! Edit errors on video page)

Dominion wants key figures from Fox News, like Rupert Murdoch and hosts Carlson, Hannity, Ingram, to testify at trial, causing potential issues for Fox.
Fox is likely to vehemently oppose this request, using any means necessary to prevent it.
Dominion believes having these figures testify is critical to their case and will help connect the pieces.
Fox facing their hosts being questioned in court could be damaging to their brand and case.
Fox has been successful in keeping certain information away from their viewers due to their echo chamber.
The potential trial coverage could expose more viewers to the hosts' statements, impacting Fox's brand significantly.
The outcome of whether all key figures will have to testify or just some remains uncertain, but Fox is expected to resist fiercely.
The case is shaping up to be very intriguing, with the possibility of intense legal battles ahead.

Actions:

for legal observers,
Watch the legal proceedings closely to understand the impact on media accountability and transparency (implied).
Stay informed about the developments in the case and the responses from Fox News (implied).
</details>
<details>
<summary>
2023-03-30: Let's talk about how Trump was indicted in New York.... (<a href="https://youtube.com/watch?v=7ezCGzTzE-8">watch</a> || <a href="/videos/2023/03/30/Lets_talk_about_how_Trump_was_indicted_in_New_York">transcript &amp; editable summary</a>)

Former President Trump indicted in New York, potential cooperation hinted, stay grounded amidst speculation and outrage.

</summary>

"Get ready for a bumpy week because there will be a lot of speculation."
"Don't let your emotions get away from you on here."
"Everybody just go off of what is known, not what is speculated."
"There are multiple other criminal investigations going on."
"Stick to what is reported as fact and nothing more."

### AI summary (High error rate! Edit errors on video page)

Former President Donald J. Trump, a leading contender for the Republican nomination in 2024, has been indicted in New York on charges related to a felony, allegedly involving money changing hands and falsification of business records for hush money payments.
A grand jury is reportedly looking into a second payment in relation to Trump's case, possibly uncovering additional criminal violations.
Allen Weisselberg, the former CFO of Trump Organization, has changed lawyers from Trump's legal team, hinting at a potential cooperation with authorities.
Change in legal representation often indicates a shift away from being a potential defendant towards cooperation with authorities.
Beau advises to stay level-headed amidst the upcoming flurry of speculation, fake outrage, and cheering, reminding everyone to stick to verified facts and not get carried away by emotions.
Multiple other criminal investigations are ongoing, indicating that the recent developments might be just the beginning of more to come.
Beau urges everyone to rely solely on verified information and not give in to speculation or emotions.
Expect a turbulent week ahead filled with speculation, outrage, and excitement surrounding these developments.
More information is expected to surface soon, but Beau wanted to share this update promptly.
Beau signs off with a reminder to stay grounded and have a good day.

Actions:

for politically aware individuals,
Stay updated on verified information to avoid being misled by speculation (implied)
Keep a level-headed approach and avoid getting swept away by emotions (implied)
</details>
<details>
<summary>
2023-03-30: Let's talk about a lack of commentary on Ukraine.... (<a href="https://youtube.com/watch?v=Jy1kO_3U-Z0">watch</a> || <a href="/videos/2023/03/30/Lets_talk_about_a_lack_of_commentary_on_Ukraine">transcript &amp; editable summary</a>)

Commentary on Ukraine decreases as commentators become cautious about potential disclosures of military strategies.

</summary>

"The situation there is pretty stable. I wouldn't say it's fine."
"Commentators, they don't know when that's going to happen."
"It's just a desire to not be the reason something went wrong."

### AI summary (High error rate! Edit errors on video page)

Commentary on Ukraine has significantly decreased, causing confusion among followers.
Speculations arose due to the sudden silence on the topic.
The situation in Ukraine is stable, with no major negative events occurring.
Many commentators have discussed equipment coming from the West for a potential counteroffensive in the spring.
With the arrival of equipment and the onset of spring, commentators are becoming more vague and less specific regarding future events.
Commentators may be more cautious about what they say, leading to a decrease in detailed information.
Some commentators might completely stop discussing the topic to avoid incorrect predictions.
The shift in commentary is driven by a desire not to be responsible if something goes wrong.
Commentators refrain from providing detailed insights to prevent premature disclosures of potential military strategies.
The decrease in specific commentary is likely to continue until a major development occurs.

Actions:

for observers of geopolitical commentary.,
Stay informed about the situation in Ukraine and geopolitical developments (implied).
</details>
<details>
<summary>
2023-03-30: Let's talk about Disney vs Florida.... (<a href="https://youtube.com/watch?v=YBT4MHyN_oQ">watch</a> || <a href="/videos/2023/03/30/Lets_talk_about_Disney_vs_Florida">transcript &amp; editable summary</a>)

Disney and Florida clash over control of Disney World's surroundings, with Disney's lawyers striking a surprising deal that may have lasting effects.

</summary>

"Disney needs control to maintain the perfection and brand of Disney World."
"Florida may not have much recourse to challenge the deal."
"The situation between Disney and Florida is set to continue brewing."

### AI summary (High error rate! Edit errors on video page)

Disney and Florida are at odds over the management of the area surrounding Disney World.
For over half a century, Disney had control through a special district, but Florida wanted to change that.
Beau doubts if Florida's decision-makers have ever met Disney's lawyers and describes them as cold and calculating.
Disney needs control to maintain the perfection and brand of Disney World.
Disney's lawyers struck a deal giving Disney control over most things, surprising the new board.
The deal is likely to stand as everything was done in the open, even though it went unnoticed.
Florida may not have much recourse to challenge the deal, which gives control to Disney in perpetuity.
The deal's length and implications are uncertain and may fuel future conflicts.
The situation between Disney and Florida is set to continue brewing.

Actions:

for florida residents, disney enthusiasts,
Contact local representatives to voice concerns about the deal and its implications (suggested)
Stay informed about any updates or changes regarding the management of Disney World's surroundings (implied)
</details>
<details>
<summary>
2023-03-29: Let's talk about the Trump NY case this week.... (<a href="https://youtube.com/watch?v=WhEHzEtKiCw">watch</a> || <a href="/videos/2023/03/29/Lets_talk_about_the_Trump_NY_case_this_week">transcript &amp; editable summary</a>)

Trump claims case dropped, but grand jury still meeting and uncertainty looms with NYPD uniform changes.

</summary>

"We don't really know anything."
"The grand jury is still meeting."
"We're probably not going to know anything until they vote."

### AI summary (High error rate! Edit errors on video page)

Trump claimed New York dropped the case against him, but there's no evidence to support this.
The grand jury is still meeting, and it's unlikely that Trump will be indicted this week.
There are various theories about the delay in the case, including normal grand jury processes, slow-walking by the DA, or uncovering new evidence.
NYPD is no longer requiring officers to show up in uniform, possibly due to a lack of anticipated crowds.
The situation regarding Trump's case remains uncertain, with grand jury processes being secret.
Congressional attention suggests that the case is likely still moving forward.
Three theories exist: normal proceedings, intentional slow progress by the DA, or the discovery of new information.
Without a vote from the grand jury, the outcome remains unknown.

Actions:

for journalists, concerned citizens,
Monitor updates on the case (suggested)
Stay informed about developments (suggested)
Engage with local news sources for updates (suggested)
</details>
<details>
<summary>
2023-03-29: Let's talk about the Ohio River, barges, and getting loose.... (<a href="https://youtube.com/watch?v=X5bsdxB4Hzc">watch</a> || <a href="/videos/2023/03/29/Lets_talk_about_the_Ohio_River_barges_and_getting_loose">transcript &amp; editable summary</a>)

A barge incident on the Ohio River involving methanol leakage prompts caution and uncertainty about water safety, urging vigilance and local updates.

</summary>

"It'd be great if people could stop putting stuff into the river."
"So at this point, there's not a lot of hard information about water quality."
"Methanol in a confined space is, well, let's just say volatile."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A tug moving 11 barges had an incident on the Ohio River, where 10 barges got loose.
Six of the loose barges have been recovered, while one was pinned against a pier and three against a dam.
Most of the barges were carrying corn and soybeans, but one had 1,400 tons of methanol, a toxic substance.
The barge carrying methanol is taking on water, with uncertainty if it's leaking, though early info suggests quick dilution if it does.
Water intakes downstream have been informed, and notices will be issued about water safety.
Recovery and normalizing the situation might take time, as seen in a similar 2018 incident that took months to resolve.
Methanol vapor in a confined space is volatile, so caution is advised.
Limited hard information is available on water quality, with ongoing testing to ensure safety.
It's unclear what, if anything, has leaked, making it a wait-and-see situation.
Beau urges people to stop polluting the river and mentions the Army Corps of Engineers working on a solution but acknowledges it may take time.

Actions:

for concerned residents near water bodies.,
Monitor local sources for updates on water quality (suggested).
Stay informed about the situation and follow any issued notices regarding water safety (implied).
Refrain from polluting rivers and water bodies (implied).
</details>
<details>
<summary>
2023-03-29: Let's talk about leakage and markers.... (<a href="https://youtube.com/watch?v=v8015ZmWf3s">watch</a> || <a href="/videos/2023/03/29/Lets_talk_about_leakage_and_markers">transcript &amp; editable summary</a>)

Beau provides vital information on markers and leakage, stressing the importance of recognizing and acting on hints to prevent potential harm effectively.

</summary>

"Believe them. Believe them."
"That's one of those steps towards prevention."
"Listen when people say that, you know, they're gonna do it."
"Leakage, that's the term."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Providing a public service announcement on markers and leakage and their importance in preventing mass incidents.
Explaining the concept of markers as common occurrences related to the person responsible for such incidents, often involving multiple stressors.
Emphasizing the prevalence of domestic violence histories and cruelty to animals as common markers.
Introducing the concept of leakage, where individuals fantasize, plan, and drop hints about harmful actions before carrying them out.
Mentioning that over half of individuals engaged in leakage, with researchers confirming approximately 56% of cases.
Describing leakage as a behavior where individuals may intentionally or subconsciously hint at their harmful intentions.
Urging people to take hints seriously, especially in settings like schools, to prevent potential incidents.
Stressing the importance of believing individuals when they hint at harmful actions and not dismissing their statements.
Encouraging understanding and action based on early research to prevent harmful incidents.
Recognizing the significance of listening and acting on information regarding potential harm, even if it may not make headlines.

Actions:

for preventive individuals or communities.,
Believe individuals when they hint at harmful actions (suggested).
Take hints seriously, especially in sensitive environments like schools (suggested).
Pay attention to potential markers of harmful behavior and act accordingly (implied).
</details>
<details>
<summary>
2023-03-29: Let's talk about Pence and the 5th Amendment.... (<a href="https://youtube.com/watch?v=JQ-lmkJpsp8">watch</a> || <a href="/videos/2023/03/29/Lets_talk_about_Pence_and_the_5th_Amendment">transcript &amp; editable summary</a>)



</summary>

"Pence has been reluctant to talk and has fought that whole process."
"Pence can still appeal this."
"It's Pence."
"Special counsel's office is probably going to ask a lot of questions that Pence has already answered in public."
"That's probably the route they're going to pursue."

### AI summary (High error rate! Edit errors on video page)

Special counsel wants Pence to talk about what he knows and has been reluctant to do so, fighting the process.
Judge ruled that Pence must go before the grand jury, except for the time when he was President of the Senate during the events of the 6th.
Special counsel is likely more interested in information leading up to the events of the 6th rather than on the 6th itself regarding Trump.
Pence has the option to appeal the ruling but it may just cause delays.
Pence appealing may not be a smart political move as it could be seen as him being compelled to testify.
Special counsel's office may ask questions that Pence has already answered publicly to shape the idea that Trump tried to illegally subvert the election.

Actions:

for legal analysts, political observers,
Wait for the legal process to unfold (implied)
Stay informed about developments in the case (implied)
</details>
<details>
<summary>
2023-03-28: Let's talk about how Nashville was different.... (<a href="https://youtube.com/watch?v=V-h3eM3QTQk">watch</a> || <a href="/videos/2023/03/28/Lets_talk_about_how_Nashville_was_different">transcript &amp; editable summary</a>)

Beau stresses the importance of prevention over response and praises persistence in pressing forward in an incident, urging a shift in focus towards stopping tragedies before they occur.

</summary>

"It doesn't matter how good the response is, by that point it's already a tragedy."
"The focus has to be on prevention."
"Prevention, stopping it before it starts. That's what this taught us."
"As good as can be expected isn't good enough."
"It's better if there wasn't a need for a hero."

### AI summary (High error rate! Edit errors on video page)

Explains why he didn't address a recent incident in Nashville immediately, despite messages from viewers.
Provides an overview of the incident timeline, from law enforcement receiving the call to engaging, which took 13 to 14 minutes.
Questions the acceptability of the outcome despite the relatively quick response time.
Emphasizes that the focus should shift from response to prevention and security measures to stop incidents before they occur.
Acknowledges criticisms in the footage regarding communication and movement of law enforcement but praises their persistence in pressing forward.
Urges for a focus on prevention, physical security at schools, and intervention to avoid tragedies.
Stresses that even with a good response, it's not enough; prevention is key.
Addresses excuses made in the past about law enforcement's inability to respond effectively and praises the department's actions in this incident.
Responds to fixation on a scared cop, noting that fear was present but their forward movement was what mattered.
Rejects the idea of scapegoating a group and calls for attention to statistics and data for a clearer understanding.
Reinforces the message of prevention and stopping incidents before they escalate to the point of requiring heroism.

Actions:

for community members, advocates,
Implement physical security measures and intervention strategies at schools (implied)
Focus on prevention and stopping incidents before they start (implied)
</details>
<details>
<summary>
2023-03-28: Let's talk about Trump's missing painting.... (<a href="https://youtube.com/watch?v=VE89B36XRd0">watch</a> || <a href="/videos/2023/03/28/Lets_talk_about_Trump_s_missing_painting">transcript &amp; editable summary</a>)

Investigation into missing gifts received by Trump from foreign officials could lead to minor repercussions unless his decisions were influenced, potentially triggering erratic behavior.

</summary>

"Former presidents are required to reimburse the federal government for gifts or return them."
"The outcome may depend on whether Trump's decisions were influenced by the gifts."
"Recovery of missing items may lead to erratic behavior from Trump."

### AI summary (High error rate! Edit errors on video page)

Investigation into missing gifts received by Trump while in office from foreign officials.
U.S. officials must report gifts over a certain dollar amount to the State Department.
Over a hundred missing items, including an eight-foot portrait of Trump from an official in El Salvador.
The painting was found hanging in a closet at one of Trump's properties next to yoga mats.
Former presidents are required to reimburse the federal government for gifts or return them.
Unlikely to turn into a serious legal issue for Trump, more of a bookkeeping matter.
The outcome may depend on whether Trump's decisions were influenced by the gifts.
Recovery of missing items may lead to erratic behavior from Trump.
Possibility of Trump returning the painting or reimbursing the government to resolve the issue.
The situation may not escalate given other ongoing matters for Trump.

Actions:

for government officials,
Recover missing gifts from Trump's properties (suggested)
Monitor Trump's response and actions regarding missing gifts (implied)
</details>
<details>
<summary>
2023-03-28: Let's talk about Eisenhower, Milley, medals, and memes.... (<a href="https://youtube.com/watch?v=oN2PiMUwYAw">watch</a> || <a href="/videos/2023/03/28/Lets_talk_about_Eisenhower_Milley_medals_and_memes">transcript &amp; editable summary</a>)

Be cautious of historical inaccuracies in memes and understand the motives behind spreading misinformation.

</summary>

"Don't get your history information from memes."
"Your propaganda is bad and you should feel bad."

### AI summary (High error rate! Edit errors on video page)

Explains a viral image comparing General Eisenhower and General Milley based on their medals and the implication that Milley has undeserved medals.
Clarifies that the number of ribbons displayed by generals changes over time but doesn't impact the comparison in the image.
Counters the notion that Eisenhower was "unwoke" by detailing his actions supporting civil rights, desegregation, and other progressive causes.
Notes that Eisenhower received numerous medals from different countries but was modest and didn't display them all.
Warns against drawing historical conclusions from inaccurate memes and the dangers of spreading misinformation for specific agendas.
Urges viewers not to rely on memes for historical accuracy and to question the motives behind such misleading information.

Actions:

for history enthusiasts,
Fact-check historical information before sharing (implied)
Question the motives behind spreading inaccurate information (implied)
</details>
<details>
<summary>
2023-03-28: Let's talk about Chad.... (<a href="https://youtube.com/watch?v=Kt83PK4RA8c">watch</a> || <a href="/videos/2023/03/28/Lets_talk_about_Chad">transcript &amp; editable summary</a>)

Chad faces internal turmoil and economic challenges as the government considers nationalizing oil assets, risking foreign investment and sparking future uncertainty.

</summary>

"Chad, a country in Africa, is experiencing tension and turmoil."
"Nationalizing oil assets is a risky foreign policy move."
"Chad's oil industry contributes significantly to their GDP."

### AI summary (High error rate! Edit errors on video page)

Chad, a country in Africa, is experiencing tension and turmoil.
The current interim president extended pardons to individuals linked to a rebel group.
The rebel group is believed to be responsible for assassinating the current interim president's father.
The president's pardons are seen as a gesture towards peace talks.
Chad's government is involved in a legal battle over the transfer of oil assets to Western interests.
The government lost the court case and now plans to nationalize the oil assets.
Nationalizing oil assets is a risky foreign policy move.
Chad's oil industry contributes significantly to their GDP.
The nationalization of oil assets is historically a challenging and bold move.
Beau hopes that this information does not become significant news in the future.

Actions:

for foreign policy analysts,
Monitor developments in Chad and stay informed on the situation (implied).
Advocate for peaceful resolutions to conflicts in Chad (implied).
Support sustainable economic practices in African nations (implied).
</details>
<details>
<summary>
2023-03-27: Let's talk about the GOP, Obamacare, and Wyoming.... (<a href="https://youtube.com/watch?v=CKdIyVd89pM">watch</a> || <a href="/videos/2023/03/27/Lets_talk_about_the_GOP_Obamacare_and_Wyoming">transcript &amp; editable summary</a>)

Republican Party manufactures problems, passes legislation, and unintentionally blocks their own bans in Wyoming and Ohio, showing the serious consequences of their strategy.

</summary>

"Republican Party creates imaginary issues and then defeats them."
"Take the win, but this is not a fight that's over."
"Republican Party continues to enshrine more rights into state constitutions to defeat imaginary issues."

### AI summary (High error rate! Edit errors on video page)

Republican Party introduces extreme legislation to energize their base.
Legislation is often a show, creating fictional problems and proposing solutions.
Wyoming introduced a ban on family planning, but it's not being enforced due to a previous constitutional amendment.
The amendment in Wyoming, intended to stop effects of Obamacare, is now ironically blocking the ban on family planning.
Similar instances occurred in Ohio due to a constitutional amendment related to health care.
Republican Party's strategy of manufacturing problems and passing legislation has serious consequences.
States are trying to deprive people of rights using this strategy.
Republican Party's authoritarian march is being stopped by their own legislation.
The fight to protect rights is ongoing despite these temporary victories.

Actions:

for advocates for protecting rights,
Advocate for the protection of rights in your community (suggested)
</details>
<details>
<summary>
2023-03-27: Let's talk about a signature problem for the GOP.... (<a href="https://youtube.com/watch?v=5Cvfv_cqL-8">watch</a> || <a href="/videos/2023/03/27/Lets_talk_about_a_signature_problem_for_the_GOP">transcript &amp; editable summary</a>)

Los Angeles County's recall petition reveals potential fraud with deceased individuals' names, hinting at broader implications for the Republican Party's rhetoric on voter fraud.

</summary>

"Over 300 deceased individuals were found to have their names used on the recall petition."
"They may have created a situation in which their voters start to engage in it because they think it's easy."
"I don't think that this is just going to go to the attorney general's office and nothing happen."

### AI summary (High error rate! Edit errors on video page)

Los Angeles County District Attorney George Gascon faced a recall petition from conservatives shortly after taking office.
Over 300 deceased individuals were found to have their names used on the recall petition.
The LA County Registrar County Clerk referred the matter to the California attorney general for fraud investigation.
Another petition involving deceased individuals was also identified, suggesting potential fraud.
The recall campaign denied involvement in using deceased individuals' names and alleged wrongdoing by the county clerk's office.
While 300 signatures may seem minor, it fits into a trend of potential voter fraud allegations linked to Republican Party affiliates.
Beau speculates that baseless claims by Trump and his supporters may have influenced constituents to believe voter fraud is easy to get away with.
The situation in California may lead to significant fallout, potentially impacting the companies involved in circulating such petitions.
Beau anticipates that the issue will not simply end with the attorney general's investigation.
Overall, Beau raises concerns about the implications of the recall petition situation and its potential broader effects.

Actions:

for voters, community members,
Contact the California attorney general's office to report voter fraud (suggested)
Stay informed and vigilant about potential fraud in political processes (implied)
</details>
<details>
<summary>
2023-03-27: Let's talk about a bill in Missouri.... (<a href="https://youtube.com/watch?v=zZ_QeFoM4Is">watch</a> || <a href="/videos/2023/03/27/Lets_talk_about_a_bill_in_Missouri">transcript &amp; editable summary</a>)

Beau addresses Missouri's HB 700, focusing on the prohibition of microchips through vaccines, illustrating the dangers of show legislation and the urgent need for scientific literacy and critical thinking in the US.

</summary>

"Now this is funny on some level. It's humorous."
"This is one of the real issues when politicians play into an echo chamber."
"It shows the desperate, desperate need for a lot of scientific literacy in the United States."
"That vaccine hesitancy, the loss that will be caused by it, it came from show legislation."
"We need better schools."

### AI summary (High error rate! Edit errors on video page)

Talking about Missouri's HB 700 introduced by Representative Hardwick, focusing on curtailing government response to public health issues.
Mentioning a particular prohibition in the legislation on introducing microchips under the skin via vaccines.
Explaining the potential harmful implications of banning something that doesn't exist, like microchips, leading to conspiracies.
Addressing how politicians playing into an echo chamber can create real problems later.
Noting that while some parts of the legislation may cause immediate concerns, the ban on microchips might lead to more vaccine hesitancy.
Providing a glimmer of hope by mentioning the lack of support for the legislation from lobbying groups.
Stressing the importance of scientific literacy, education, and critical thinking in the US.
Expressing concern over the existence and persistence of such legislation despite past failures.
Concluding with a call for better education and critical thinking skills in the country.

Actions:

for activists, educators, citizens,
Advocate for improved scientific literacy in educational institutions (implied)
Promote critical thinking skills in media consumption (implied)
Support lobbying groups opposing harmful legislation (implied)
</details>
<details>
<summary>
2023-03-27: Let's talk about Nebraska, allies, and holding the line.... (<a href="https://youtube.com/watch?v=xs2Q9ZsIaho">watch</a> || <a href="/videos/2023/03/27/Lets_talk_about_Nebraska_allies_and_holding_the_line">transcript &amp; editable summary</a>)

Nebraska senators filibuster against healthcare takeover, needing support to grind legislative halt and hold the line; drop them a line to show appreciation and encouragement.

</summary>

"There's a whole lot of people who want to consider themselves allies right up until it's time to do ally stuff."
"If you're doing it alone, it's hard."
"People who are trying to take a stand could probably benefit from an email or a social media message."
"This is definitely a time when some people who are trying to take a stand could probably benefit from an email or a social media message."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Nebraska legislature attempting a government takeover of healthcare, starting with banning gender-affirming care for trans minors.
Senators vowing to filibuster the session to stop this legislation from moving forward.
Four senators committed to filibuster, possibly joined by a fifth.
Filibustering the entire session can grind the legislature to a halt but will need support to withstand the pressure.
Urges people to show support by dropping a line to these senators to encourage and appreciate their efforts.

Actions:

for supporters of trans rights,
Contact Nebraska senators to show support (suggested)
Drop them an email or a social media message (suggested)
</details>
<details>
<summary>
2023-03-26: Let's talk about the importance of Wisconsin's 2023 election.... (<a href="https://youtube.com/watch?v=w1AK_q2ZMkc">watch</a> || <a href="/videos/2023/03/26/Lets_talk_about_the_importance_of_Wisconsin_s_2023_election">transcript &amp; editable summary</a>)

Wisconsin's pivotal judicial election in 2023 could sway national implications in a swing state, impacting issues from family planning to voting rights.

</summary>

"Wisconsin is a swing state."
"The candidates are Janet Protasewicz, if you live up there you find that funny because of the ads, and the other is Daniel Kelly."
"This is a race that the entire nation is going to end up kind of waiting on rulings from, I would imagine, come 2024."

### AI summary (High error rate! Edit errors on video page)

Wisconsin is facing a consequential judicial election in 2023, with one Supreme Court seat up for grabs.
The current political landscape in Wisconsin includes a Democratic executive branch, a Republican-controlled legislature, and a conservative-leaning Supreme Court.
The candidates vying for the seat are Janet Protasewicz (liberal) and Daniel Kelly (conservative).
The outcome of this election is significant due to Wisconsin being a swing state, with potential implications for future elections, especially in 2024.
Potential issues at stake include family planning laws dating back to 1849.
The amount of money being spent on this judicial race underscores its importance and the high expectations from both parties.
The election on April 4th is likely to have national implications, given Wisconsin's swing state status.
The judicial rulings in this race could impact various issues like redistricting, voting rights, and family planning.
Despite being an off-year election, this race is expected to be highly consequential and closely watched.
Beau does not endorse candidates but distinguishes Protasewicz as the liberal and Kelly as the conservative.

Actions:

for wisconsin voters,
Pay attention to the upcoming judicial election in Wisconsin (suggested)
Stay informed about the candidates and their stances on key issues (suggested)
Engage in voter education and outreach efforts within your community (implied)
</details>
<details>
<summary>
2023-03-26: Let's talk about the Parents Bill of Rights.... (<a href="https://youtube.com/watch?v=tRfMWTF4014">watch</a> || <a href="/videos/2023/03/26/Lets_talk_about_the_Parents_Bill_of_Rights">transcript &amp; editable summary</a>)

Beau explains the unlikely path of the Politics Over Parents Act, reassuring listeners about its slim chances of becoming law and stressing the importance of strategic voting.

</summary>

"This is what they're getting for their vote."
"Understand without even the status quo Democrats right now, without them, this would be on its way to becoming law."
"To them, picking on your kids is, it's their pathway to electoral victory."

### AI summary (High error rate! Edit errors on video page)

Explains the Parents Bill of Rights, also known as the Politics Over Parents Act, and addresses a message he received from a parent concerned about the legislation.
Assures the parent that the legislation faces significant hurdles and may not become law.
Clarifies that the legislation isn't about parental rights but rather about Republicans in the House targeting trans kids.
Points out that the legislation passed in the House but faces uncertainty in the Senate, with slim chances of passing and President Biden unlikely to support it.
Reminds listeners not to panic, as the odds of the legislation moving forward are very slim due to lack of support.
Notes that similar legislation may continue to be proposed but reiterates that the chances of it becoming law are minimal.
Acknowledges frustrations with the Democratic Party but underscores the importance of their role in preventing such legislation from advancing.
Emphasizes the significance of voting strategically, even if the Democratic Party isn't perfect, to prevent harmful legislation from progressing.

Actions:

for parents, voters, progressives,
Stay informed on legislative developments and advocate against harmful bills (implied).
Encourage strategic voting to prevent the advancement of detrimental legislation (implied).
</details>
<details>
<summary>
2023-03-26: Let's talk about an indictment helping Trump.... (<a href="https://youtube.com/watch?v=ui3-swP7aKs">watch</a> || <a href="/videos/2023/03/26/Lets_talk_about_an_indictment_helping_Trump">transcript &amp; editable summary</a>)

Analyzing Trump's primary polling numbers and the impact of a possible indictment on his chances beyond the primary, Beau concludes that while an indictment might energize his base, it won't likely help him win in the general election.

</summary>

"They do not matter in terms of the general election."
"He may get a bump in primary polling."
"The polling numbers are going to change a lot."
"I don't really see a possible indictment as being something that actually helps him."
"Most Republican voters would walk away from him as fast as he walked away from the January 6th defendants."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's numbers in relation to a possible indictment and its impact on polls.
Primary polling numbers among Republican loyalists are being referenced, but they don't matter for the general election.
Comparing current primary polling numbers to previous election cycles to show how much things can change.
Mentioning Nikki Haley's rise in polling numbers from single digits to 30% in a head-to-head matchup with Trump.
Speculating that an indictment might energize Trump's base but won't help him win beyond the primary.
Noting that moderate and independent voters turned against Trump before the events of January 6th.
Pointing out that early polling numbers are receiving attention because of Trump's polarizing figure.
Emphasizing that a possible indictment won't likely have a significant impact on Trump's chances in the general election.
Mentioning that Republican voters might walk away from Trump if he were to face an indictment.
Concluding that a sympathy bump from an indictment in the primary wouldn't last long.

Actions:

for political analysts, voters,
Stay informed about political developments and polling data (implied).
</details>
<details>
<summary>
2023-03-26: Let's talk about LAUSD schools and deals.... (<a href="https://youtube.com/watch?v=rUSidi3_CDc">watch</a> || <a href="/videos/2023/03/26/Lets_talk_about_LAUSD_schools_and_deals">transcript &amp; editable summary</a>)

Los Angeles support workers strike for fair treatment, leading to a tentative agreement with wage increases and benefits, while teachers' union negotiations continue.

</summary>

"Los Angeles support workers felt undervalued, prompting negotiations and a strike."
"A tentative agreement includes wage increases and benefits for workers."

### AI summary (High error rate! Edit errors on video page)

Los Angeles Unified School District workers, specifically support workers, felt undervalued, leading to negotiations and a strike.
The average salary for workers in Los Angeles was around $25,000 a year, requiring many to have second jobs.
The support workers' strike was backed by the teachers' union, with members encouraged to join picket lines.
A tentative agreement has been reached after a three-day strike, with still pending ratification.
Details of the agreement include a $1000 bonus for certain employees, a new minimum wage of $22.52, retroactive salary increases, and health benefits for part-time employees.
There will be a $2 per hour increase for employees effective January of next year.
The agreement also includes a $3 million investment in education and professional development for SEIU members.
The negotiations likely involved intense back-and-forth, evident in the specific wage amount of $22.52.
The union members seem confident that the agreement will be accepted by the membership.
The teachers' union is also in separate contract negotiations, potentially leading to more developments in the future.

Actions:

for workers, unions, advocates,
Support the workers by staying informed about the progress of the agreement and potential future actions (implied).
</details>
<details>
<summary>
2023-03-25: Let's talk about whether Trump is preparing to lose in NY.... (<a href="https://youtube.com/watch?v=qBpTt55CaME">watch</a> || <a href="/videos/2023/03/25/Lets_talk_about_whether_Trump_is_preparing_to_lose_in_NY">transcript &amp; editable summary</a>)

Report from Rolling Stone suggests Trump may lose in New York, prompting debate on whether his advisors truly believe in lack of impartiality or are managing his behavior post-trial to prevent further legal issues.

</summary>

"His best chance is on appeal."
"Imagine Trump losing a trial and then making the kinds of statements we have come to expect."
"They may be just trying to head that off."

### AI summary (High error rate! Edit errors on video page)

Report from Rolling Stone suggests people in Trump's world are warning him he may lose in New York and should prepare for an appeal due to lack of an impartial jury.
Lawyers in New York may disagree with the notion of Trump not getting a fair trial.
Two interpretations: His advisors believe in the lack of impartiality and are preparing him for appeal, or they are trying to manage his behavior post-conviction.
Managing Trump post-trial is vital to prevent damaging statements that could lead to more legal trouble.
Uncertainty exists in whether Trump's circle genuinely believes he won't get a fair trial or are strategically managing him.
Rolling Stone has a track record of accurate reporting on Trump-related matters, lending credibility to the claims made.

Actions:

for political analysts,
Contact Rolling Stone for more information on their sources (suggested)
Stay informed on legal developments related to Trump (implied)
</details>
<details>
<summary>
2023-03-25: Let's talk about making the right move.... (<a href="https://youtube.com/watch?v=u7nwj0nLs4A">watch</a> || <a href="/videos/2023/03/25/Lets_talk_about_making_the_right_move">transcript &amp; editable summary</a>)

Politicians' domestic messaging can impact foreign policy, urging the US to de-escalate in Syria to avoid civilian casualties and strategic traps.

</summary>

"The right move is to de-escalate."
"If your opposition is trying to provoke a response, you don't give it to them."
"The people calling for escalation, they're not gonna be the ones who pay the price."
"If the feeling is correct, the right move is to de-escalate."
"We should be de-escalating. We should be moving towards ending the mission there."

### AI summary (High error rate! Edit errors on video page)

Politicians creating messaging for a domestic audience can derail good foreign policy by missing the bigger picture and making mistakes.
Talks about how Iran might be manipulating the US due to the US habitually underestimating Iran.
Describes the ongoing conflict in Syria between US forces and Iranian-backed groups, clarifying that they are not proxies but rather backed by Iran.
Expresses frustration at the media's portrayal of the conflict as a recent development when it has been ongoing for years.
States that there is a growing feeling that the attacks by these Iranian-backed groups are becoming more deliberate and intense.
Predicts the emergence of two camps in the US response: hawkish camp advocating for a more aggressive approach and a proportional response camp maintaining the status quo.
Suggests that Iran's backing of groups in Syria could be a strategic move to help Russia and hinder the US without losing anything themselves.
Advocates for the US to de-escalate in Syria, as escalating the situation could lead to civilian casualties.
Warns against falling into the trap of responding to provocations and calls for a de-escalatory approach.
Emphasizes that politicians advocating for escalation won't bear the consequences; it will be civilians paying the price.
Stresses the importance of considering the impact on civilians and the strategic implications of getting involved in Syria.

Actions:

for policy makers, activists,
Advocate for de-escalation in Syria to prevent civilian casualties (implied)
Raise awareness about the consequences of escalation on civilians (implied)
Push for a strategic approach that considers the long-term impact on vulnerable populations (implied)
</details>
<details>
<summary>
2023-03-25: Let's talk about Trump, NY, the DA, and being wrong.... (<a href="https://youtube.com/watch?v=-nFBn6aG970">watch</a> || <a href="/videos/2023/03/25/Lets_talk_about_Trump_NY_the_DA_and_being_wrong">transcript &amp; editable summary</a>)

Beau reconsiders Trump's potential prison sentence due to recent threatening events in New York, pointing out behavior that could lead to detention.

</summary>

"Former President Trump's behavior in New York prompts Beau to reconsider his stance on the possibility of Trump going to prison."
"Trump warned of potential death and destruction if he is indicted, behavior that could be used to justify detention."
"The connection between the threatening letter and Trump could significantly impact any arguments against his potential detention."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's behavior in New York prompts Beau to reconsider his stance on the possibility of Trump going to prison.
Beau used to be cautious about Trump facing prison time due to his status as a former president, but recent events are making him rethink this.
The District Attorney in New York received a threatening letter accompanied by white powder after Trump called for protests.
Trump warned of potential death and destruction if he is indicted, behavior that could be used to justify detention.
Beau compares Trump's actions to those of a prominent figure from a country the U.S. has biases against, suggesting different treatment.
Trump's tweets and statements may contribute to a prosecutor's argument for his detention, shifting Beau's perspective on the situation.
Public opinion differs from legal implications, indicating potential consequences for Trump regardless of public debate.
Beau suggests that Trump's best interest lies in staying off social media, especially considering the gravity of the threatening letter incident.
The connection between the threatening letter and Trump could significantly impact any arguments against his potential detention.
Beau expresses surprise at the unfolding events and expected Trump's lawyers to advise him better on handling the situation.

Actions:

for community members,
Contact local authorities to report any threatening or suspicious activities (suggested)
Support initiatives promoting peaceful interactions and discourse within communities (exemplified)
</details>
<details>
<summary>
2023-03-25: Let's talk about David, The Simpsons, and Tallahassee.... (<a href="https://youtube.com/watch?v=PZbYah3DZAM">watch</a> || <a href="/videos/2023/03/25/Lets_talk_about_David_The_Simpsons_and_Tallahassee">transcript &amp; editable summary</a>)

Firing a principal over Michelangelo's David in a school promising a classical education sparks a debate on censorship and the importance of art education.

</summary>

"We are shielding our children from the wrong things."
"A lack of art, a lack of art education, a lack of embracing creativity is going to lead to children growing up who do not understand the world."

### AI summary (High error rate! Edit errors on video page)

A school principal in Tallahassee was fired because sixth-grade students saw images of Michelangelo's David and deemed it inappropriate by some parents.
The connection to a Simpsons episode is made where Marge, an artist, disagrees with censoring Michelangelo, showing how the current world is beyond parody.
The Simpsons episode aired 30 years ago, showing David from the back and front, something widely watched by children back then but deemed inappropriate now.
Shielding students from Michelangelo's David in a school promising a classical education is mind-boggling and shows a lack of embracing creativity and art education.
Beau questions the wildness in schools and the challenges for administrators and teachers in determining what they can teach.
Art education and embracing creativity are vital for children to understand the world.
The principal who was fired was given the option to resign but refused.

Actions:

for parents, educators, school administrators,
Support art education in schools by advocating for comprehensive art programs (implied)
Encourage creativity and open-mindedness among students by exposing them to various forms of art (implied)
</details>
<details>
<summary>
2023-03-24: Let's talk about what children in other countries learn.... (<a href="https://youtube.com/watch?v=wtJnJ-vPhZA">watch</a> || <a href="/videos/2023/03/24/Lets_talk_about_what_children_in_other_countries_learn">transcript &amp; editable summary</a>)

Beau contrasts critical thinking with mechanical skills, advocating for teaching children to understand systemic injustice over weapon-related training.

</summary>

"If you want to create warriors, you have to teach people to critically think."
"Nationalism is politics for basic people."
"Sending people who think they're warriors because they know how to field strip a weapon is appalling."

### AI summary (High error rate! Edit errors on video page)

Contrasts critical thinking skills in the U.S. with what children in other countries are learning, particularly in relation to assembling and disassembling weapons.
Mentions a science kit his kids received to learn about mechanics, but clarifies that it doesn't prepare them for specific careers like mechanics or race car driving.
Addresses the circulating clips of children in foreign countries learning to field strip weapons in class, contrasting it with what U.S. kids are taught.
Explains the simplicity of field stripping a weapon and its lack of relevance to becoming a warrior.
Emphasizes the importance of critical thinking over mechanical skills for warfare and gives examples of critical thinking in combat scenarios.
Talks about how understanding systemic injustice is vital for creating effective warriors and establishing rapport in foreign countries.
Advocates for teaching children critical thinking about systems of injustice, technology, and the environment, rather than focusing on weapon-related skills.

Actions:

for parents, educators, policymakers,
Teach children critical thinking skills (implied)
Encourage understanding of systemic injustice, technology, and the environment (implied)
Advocate for cooperative systems over competitive ones (implied)
</details>
<details>
<summary>
2023-03-24: Let's talk about the House GOP failing to override Biden's veto.... (<a href="https://youtube.com/watch?v=4GtS5PNlCeI">watch</a> || <a href="/videos/2023/03/24/Lets_talk_about_the_House_GOP_failing_to_override_Biden_s_veto">transcript &amp; editable summary</a>)

House GOP's failed attempt to override Biden's veto reveals a futile strategy of proposing controversial legislation to energize their base, likely alienating the majority of Americans.

</summary>

"They mounted an effort to override the veto, and it failed in spectacular fashion."
"And keep in mind, in comparison to some of the stuff that they are likely to propose later, this was not controversial at all."
"The strategy, if you want to call it that, is to energize the base and say, you know, If we had more seats in the Senate, or we had the presidency, this would now be law or something like that."
"It is likely to alienate the majority of Americans."
"Holding votes, like to override a veto, that you know aren't going to go well is not generally seen as good political strategy, but, I mean, here we are."

### AI summary (High error rate! Edit errors on video page)

Explains the controversial legislation proposed by the House GOP restricting financial advisors from recommending investments based on environmental or social reasons.
Predicted that Biden's first veto will be on this legislation, which indeed happened.
House GOP attempted to override Biden's veto but failed spectacularly with only 219 votes.
House Republicans likely to propose more wild legislation to energize their base, knowing they can't override Biden's veto.
Anticipates a surge in controversial bills closer to the 2024 election, aiming to energize their base despite inevitable failure.

Actions:

for voters and political activists.,
Contact your representatives to express your opinions on proposed legislation (implied).
Stay informed about upcoming bills and their potential impacts (implied).
</details>
<details>
<summary>
2023-03-24: Let's talk about history, mythology, and context.... (<a href="https://youtube.com/watch?v=4NjxBqehQzc">watch</a> || <a href="/videos/2023/03/24/Lets_talk_about_history_mythology_and_context">transcript &amp; editable summary</a>)

Beau dives into American history, mythology, and context, addressing criticisms on nuclear history accuracy and the Cuban Missile Crisis, revealing the ongoing presence of U.S. nukes in Turkey and hinting at the importance of peace over political posturing.

</summary>

"Exploring American history versus American mythology and context."
"The video aimed to explain the Cold War concept of MAD effectively."
"US still has nukes in Turkey to this day."
"Peace was way more important than political posturing."
"Khrushchev was definitely somebody who was looking for peace."

### AI summary (High error rate! Edit errors on video page)

Exploring American history versus American mythology and context.
Addressing messages received after linking a video.
Clarifying that linking a video doesn't mean cosigning everything in it.
The video in question aimed to explain the Cold War concept of MAD effectively.
Two main criticisms received regarding historical accuracy.
Response to the first criticism regarding the development of nuclear programs during World War II.
Response to the second criticism about the Cuban Missile Crisis and the removal of nukes from Cuba and Turkey.
Mentioning the common belief about Kennedy secretly pulling U.S. nukes out of Turkey.
Providing a brief timeline of U.S. nuclear systems placement in Turkey.
Expressing the presence of U.S. nukes in Turkey to this day and discussing the need for their removal.
Not delving deep into the topic, hinting at a potential follow-up video.
Speculating on the motivations behind the narrative surrounding the Cuban Missile Crisis.
Acknowledging Khrushchev's efforts for peace during the crisis.

Actions:

for history enthusiasts,
Research the historical accuracy of events discussed (implied)
Advocate for the removal of U.S. nukes in Turkey (implied)
</details>
<details>
<summary>
2023-03-24: Let's talk about 2 big Trump developments, privilege, and jurors.... (<a href="https://youtube.com/watch?v=DAfCmFtVn_o">watch</a> || <a href="/videos/2023/03/24/Lets_talk_about_2_big_Trump_developments_privilege_and_jurors">transcript &amp; editable summary</a>)

A judge's ruling on executive privilege and the use of an anonymous jury in the E. Jean Carroll lawsuit signal significant shifts in legal proceedings involving Trump.

</summary>

"Without executive privilege, the special counsel will obtain grand jury testimony from everybody from Mark Meadows on down to have that ability."
"The fact that a judge has decided a former President of the United States requires an anonymous jury because of his behavior, that's pretty telling."

### AI summary (High error rate! Edit errors on video page)

A judge ruled that executive privilege doesn't apply in the January 6 proceedings, giving the special counsel access to grand jury testimony from individuals like Mark Meadows, potentially revealing insights into Trump's actions.
The E. Jean Carroll lawsuit involves anonymous jurors to protect their identities due to Trump's history of attacking public officials and individuals, setting a rare precedent for an anonymous jury in cases with high media attention.
Anonymous juries are increasingly common in the social media age to prevent influence and harassment, something previously seen in cases involving organizations known for targeting individuals.
The use of an anonymous jury for a former President due to his behavior, as seen in the E. Jean Carroll lawsuit, could have implications for future cases involving Trump.
The absence of executive privilege for Trump allies and the use of an anonymous jury in a lawsuit are significant developments with potential long-lasting effects on legal proceedings involving Trump.

Actions:

for legal observers, political analysts,
Stay informed on the developments in the Trump cases (implied)
Support transparency and accountability in legal proceedings (implied)
</details>
<details>
<summary>
2023-03-23: The roads to understanding Mr  Beast, charity, and aid.... (<a href="https://youtube.com/watch?v=WNYEhip1K48">watch</a> || <a href="/videos/2023/03/23/The_roads_to_understanding_Mr_Beast_charity_and_aid">transcript &amp; editable summary</a>)

Beau breaks down the distinction between charity and aid, addressing criticism towards MrBeast's philanthropic efforts from a left perspective while advocating for systemic change and a shift towards aid.

</summary>

"Charity gives a person a fish. Aid teaches a person to fish."
"Nobody on the left is actually upset or wants him to not do what he's doing."
"Every heartwarming thing he does is a symptom of a systemic failure."
"Aid, switching from charity to aid, I think that he'd be pretty open to it."
"The left does not want people to stop helping people."

### AI summary (High error rate! Edit errors on video page)

Distinguishing between charity and aid is key, with charity addressing symptoms and aid tackling root causes.
MrBeast's altruistic actions have sparked criticism from a left perspective, but the goal is not to stop him from helping.
Critics suggest MrBeast should transition from charity to aid to address systemic issues and make a more significant impact.
While some question MrBeast's capitalistic approach, it's acknowledged that he operates within a capitalist system.
The desire for MrBeast to provide long-term solutions is prevalent, even though dramatic content may still be necessary for his platform.
A valid criticism is the potential for MrBeast to enhance his impact by focusing on aid rather than solely charity.
The left's perspective aims for systemic change and root cause aid, but they may struggle to effectively communicate their ideologies.
Despite the criticisms, the overarching sentiment is not to halt MrBeast's charitable deeds but to encourage improvement and systemic change.

Actions:

for creators, activists, supporters,
Reach out to MrBeast or his team to suggest transitioning from charity to aid (suggested)
Encourage MrBeast to focus on providing long-term solutions to address root causes (suggested)
</details>
<details>
<summary>
2023-03-23: Let's talk about trains, Twitter, and tests.... (<a href="https://youtube.com/watch?v=cetW3AM2oJs">watch</a> || <a href="/videos/2023/03/23/Lets_talk_about_trains_Twitter_and_tests">transcript &amp; editable summary</a>)

Beau reports on suspicious Twitter accounts linked to pro-Moscow voices spreading misinformation after a train derailment, raising concerns about potential foreign influence on American politics.

</summary>

"Some of the accounts appeared to be Moscow controlled, and some were just American accounts amplifying that message."
"It's not actionable. There's nothing that really can come of spreading disinformation about this."
"If it turns out that the paid service is going to be used as a microphone for foreign intelligence services to try to influence internal American politics, it might lead to congressional hearings."
"Right now the reporting is kind of light, but it's interesting and it's something that is probably one of those things we're going to see again."
"It may not have been successful but it was just an interesting little tidbit I saw."

### AI summary (High error rate! Edit errors on video page)

Reporting on accounts identified on Twitter trying to influence the narrative following a train derailment in Ohio.
Accounts linked to pro-Moscow voices spreading misinformation like inaccurate maps and false cancer rate charts.
Some accounts appeared to be Moscow-controlled, while others were American accounts amplifying the message.
Beau finds it strange that an intelligence service like Moscow's or even Republicans are spreading such misinformation.
Speculation that these accounts were using Twitter's paid verification service to increase reach and steer the narrative.
If it was an information operation, the goal might have been to test the effectiveness of using the paid service to influence American politics.
Potential consequences like congressional hearings if foreign intelligence services use paid services to influence internal politics.
Beau expects more in-depth analysis on the incident in the future to understand better what happened.
The time lag between the event and control of the narrative is something to watch for.
The incident may be a precursor to similar events in the future.

Actions:

for internet users,
Keep an eye out for misinformation and disinformation spread on social media platforms (implied).
</details>
<details>
<summary>
2023-03-23: Let's talk about imagination and electric vehicle chargers.... (<a href="https://youtube.com/watch?v=IJ9qMc60bzI">watch</a> || <a href="/videos/2023/03/23/Lets_talk_about_imagination_and_electric_vehicle_chargers">transcript &amp; editable summary</a>)

Beau explains how the Overton window affects innovation, using electric vehicles as an example to challenge traditional thinking and envision a future with alternative charging infrastructure.

</summary>

"it's one of those moments where you can see how the inability to think of something different prohibits the ability to think of something better."
"Always ask yourself why are we doing it this way and do we have to."
"Are you assuming that something has to be done the way it always has been?"
"It's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the Overton window, which defines the range of acceptable beliefs in a particular area, often viewed in politics.
Connects the idea of the Overton window to electric vehicles, showing how people's imaginations and ability to envision something different can impact technological advancements.
Mentions Jeep's move towards electric vehicles, including the desire for a full electric Jeep that retains the essence of a traditional Jeep.
Notes the common response to electric vehicles based on current limitations like travel distance and charging times.
Challenges traditional thinking by proposing alternative locations for electric vehicle chargers, such as fast food restaurants, instead of just gas stations.
Describes a scenario where charging an electric vehicle can be integrated into daily activities like dining out, shifting the perspective on charging time.
Foresees a shift in infrastructure with roadside attractions, hotels, and other establishments likely to incorporate electric vehicle chargers.
Emphasizes the changing landscape of energy sources and the need to question assumptions about how things have always been done.
Encourages individuals to rethink their approach to solutions and daily routines, considering the potential for significant changes from small shifts in perspective.
Raises awareness about the evolving infrastructure around electric vehicles and the importance of questioning the status quo.

Actions:

for innovators, electric vehicle enthusiasts,
Support businesses that install EV chargers (implied)
Advocate for more diverse locations for EV chargers (implied)
Question traditional practices in daily life and seek innovative solutions (implied)
</details>
<details>
<summary>
2023-03-23: Let's talk about a win at the Supreme Court for Americans with disabilities.... (<a href="https://youtube.com/watch?v=VnQLqWNzTpQ">watch</a> || <a href="/videos/2023/03/23/Lets_talk_about_a_win_at_the_Supreme_Court_for_Americans_with_disabilities">transcript &amp; editable summary</a>)

A Supreme Court win allows families of neglected students to seek compensation under the ADA, demanding justice for lost opportunities and education.

</summary>

"All of that was robbed. They don't get that."
"School districts better start making it right by students who fall under this."

### AI summary (High error rate! Edit errors on video page)

Talks about a significant win at the US Supreme Court for a group of people often overlooked.
Mentions a heartbreaking story of a deaf student neglected by the school system.
Describes how the student was given inflated grades and only offered a certificate of completion.
Explains how the parents used the Individuals with Disabilities Education Act (IDEA) to seek justice.
Mentions the district agreeing to pay for additional schooling and American Sign Language classes.
Points out that families with unique needs often settle for what is best for their children.
Explains how the family also used the American with Disabilities Act (ADA) to pursue monetary damages.
Emphasizes that the Supreme Court's decision allows families to seek compensation for lost opportunities and education.
Stresses the significance of this ruling for families without access to legal representation.
Urges schools to prioritize making things right for students with disabilities.

Actions:

for families, advocates, educators.,
Support families navigating special education needs (suggested)
Advocate for inclusive education practices (suggested)
</details>
<details>
<summary>
2023-03-23: Let's talk about Colorado, the GOP, and trends.... (<a href="https://youtube.com/watch?v=JY3TqYkhxOE">watch</a> || <a href="/videos/2023/03/23/Lets_talk_about_Colorado_the_GOP_and_trends">transcript &amp; editable summary</a>)

Beau updates on Colorado politics, revealing shifts in party allegiance and suggesting a more progressive stance for the Democratic Party to thrive.

</summary>

"Republicans leaving the party are becoming unaffiliated rather than joining Democrats."
"It might be time for the Democratic Party in Colorado to become even more progressive."
"Those numbers are really interesting, seeing both parties in decline at the same time."
"The Democratic Party can seize the opportunity, one way or another."
"To me, it seems like its best chance is to motivate voters by really embracing progressive ideas there."

### AI summary (High error rate! Edit errors on video page)

Updates on Colorado's political landscape, focusing on the declining GOP and overlooked trends within the Democratic Party.
The GOP in Colorado is losing ground, with the Democratic Party now leading in registered voters.
Interestingly, Republicans leaving the party are becoming unaffiliated rather than joining Democrats.
Speculation arises about why Democratic Party members are also leaving, potentially due to not being progressive enough.
Suggests that the Democratic Party in Colorado might need to shift towards a more progressive stance to retain and attract voters.
Calls for those critiquing the party's lack of progressivism to participate in primaries and push for change.
Notes a decline in registered voters for both parties simultaneously, signaling a deeper issue beyond people choosing independence.
Encourages the Democratic Party to seize the moment by embracing progressive ideas to motivate voters in Colorado.

Actions:

for colorado voters,
Participate in Democratic Party primaries to push for a shift towards a more progressive platform (implied).
Support and advocate for progressive ideas within the Democratic Party in Colorado (implied).
</details>
<details>
<summary>
2023-03-22: Let's talk about the Navy renaming ships.... (<a href="https://youtube.com/watch?v=eKPDXHmBtXQ">watch</a> || <a href="/videos/2023/03/22/Lets_talk_about_the_Navy_renaming_ships">transcript &amp; editable summary</a>)

The United States Navy is renaming ships named after Confederacy-related figures, like the USNS Murray becoming the Marie Tharp, and the USS Chancellorsville being renamed for Robert Smalls, a former slave turned hero and influential leader.

</summary>

"Robert Smalls' story is a truly American and uplifting tale."
"If you are looking for a story that is just purely American and kind of uplifting, Robert Smalls is definitely one to look into."

### AI summary (High error rate! Edit errors on video page)

United States Navy is renaming ships named after Confederacy-related people or events, like the USNS Murray being renamed the Marie Tharp.
The USS Chancellorsville is being renamed for Robert Smalls, a former slave who became the first black person to command a U.S. naval ship after learning to sail as a child.
In 1862, Smalls, while being forced to serve on a Confederate steamer, commandeered the ship, guided it past five Confederate forts, and handed it over to the Union Navy.
Smalls had a remarkable life beyond this act, becoming the first black commander of a U.S. naval vessel and influencing Lincoln's decision to allow black troops.
Robert Smalls' story is a truly American and uplifting tale that deserves recognition and remembrance.

Actions:

for history enthusiasts,
Research the remarkable life of Robert Smalls to learn about his contributions and impact on American history (suggested).
Share Robert Smalls' story with others to spread awareness of this uplifting tale (exemplified).
</details>
<details>
<summary>
2023-03-22: Let's talk about Trump, privilege, and documents.... (<a href="https://youtube.com/watch?v=TcfPIstVwyQ">watch</a> || <a href="/videos/2023/03/22/Lets_talk_about_Trump_privilege_and_documents">transcript &amp; editable summary</a>)

Special counsel seeks to pierce attorney-client privilege, implying Trump misled lawyers and potentially faces willful document retention allegations amidst overshadowed proceedings.

</summary>

"Prima facie evidence is a very low standard. Literally, on its face, this is what it looks like."
"It's worth noting that in the conversations, it does appear that that phrase retention comes up."
"There are other proceedings happening, and in this case, definitely seems like big news that might get overshadowed."

### AI summary (High error rate! Edit errors on video page)

Special counsel's office sought to pierce attorney-client privilege with one of Trump's lawyers.
Judge found prima facie evidence of a criminal scheme allowing DOJ to compel testimony.
Trump allegedly misled his attorneys, leading them to mislead federal authorities.
The timeline and evidence could demonstrate Trump's deliberate misleading of attorneys.
Lawyer will have to testify before the grand jury about previously privileged information.
DOJ's effort to uncover the truth is genuine, focusing on the phrase "retention."
Willful retention of documents could be a significant issue if Trump deliberately misled his attorneys.
Other proceedings apart from New York and Georgia are underway, potentially overshadowed news.

Actions:

for legal analysts, political commentators,
Stay informed about the ongoing legal proceedings and developments (implied)
Support efforts to uphold transparency and accountability in legal cases (implied)
</details>
<details>
<summary>
2023-03-22: Let's talk about Trump's petition.... (<a href="https://youtube.com/watch?v=gjlVKB1zwO4">watch</a> || <a href="/videos/2023/03/22/Lets_talk_about_Trump_s_petition">transcript &amp; editable summary</a>)

Former President Trump's circulating petition aims to prevent his arrest, with high donation requests, while developments in New York continue with a grand jury meeting scheduled.

</summary>

"Former President Donald Trump is circulating a petition requesting his supporters to prevent his arrest."
"Trump's initial fear or panic about being arrested seems to have subsided, and now the focus is on monetizing everything."
"Despite this, Trump has the right to organize such a petition as part of his freedom of speech."

### AI summary (High error rate! Edit errors on video page)

Former President Donald Trump is circulating a petition requesting his supporters to prevent his arrest.
Those who sign the petition are directed to a page where they are asked to donate to Trump.
The suggested donation amounts are high, reaching up to $3300, but any amount is accepted.
Trump claims the petition will be super effective without specifying how.
Trump's initial fear or panic about being arrested seems to have subsided, and now the focus is on monetizing everything.
The lack of public support for Trump's in-person assemblies may be why there is no public counter for the petition.
The petition is unlikely to impact whether or not Trump is indicted in New York.
Despite this, Trump has the right to organize such a petition as part of his freedom of speech.
The situation in New York with Trump is still developing, with a grand jury meeting scheduled for Wednesday.
More information on the developments is expected to come to light soon.

Actions:

for concerned citizens, political observers,
Monitor the developments regarding the grand jury meeting in New York (implied)
</details>
<details>
<summary>
2023-03-22: Let's talk about Trump not getting picked up today.... (<a href="https://youtube.com/watch?v=VBQ2uX9Gp7I">watch</a> || <a href="/videos/2023/03/22/Lets_talk_about_Trump_not_getting_picked_up_today">transcript &amp; editable summary</a>)

Trump's failed arrest claim prompts muted response from supporters with some exceptions, avoiding regrettable actions.

</summary>

"Trump claimed he was going to be arrested on Tuesday, but it didn't happen."
"The overall response from his base has not been overly energetic, which is good."
"The muted response from his supporters is good. It's going to keep people from making mistakes."

### AI summary (High error rate! Edit errors on video page)

Trump claimed he was going to be arrested on Tuesday, but it didn't happen, leading to questions.
Speculations on why Trump made this claim include fear, panic, or hoping to elicit a response.
Trump's supporters have remained relatively calm, with some exceptions of false claims of devices in buildings.
The NYPD investigated these claims, finding no actual threat, but the politically charged nature may lead to further investigation.
Despite some energized individuals, the overall response from Trump's base has been muted, preventing potential regrettable actions.
The small group willing to take action in support of Trump may face consequences.
The lack of significant protests or energetic responses indicates that Trump's play for outrage was not successful.

Actions:

for concerned citizens,
Stay calm and avoid getting caught up in potential provocations (implied)
</details>
<details>
<summary>
2023-03-21: Let's talk about what Biden's first veto means.... (<a href="https://youtube.com/watch?v=s2UQltN2IKY">watch</a> || <a href="/videos/2023/03/21/Lets_talk_about_what_Biden_s_first_veto_means">transcript &amp; editable summary</a>)

President Biden's first veto sets the stage for future battles with the Republican Party, signifying a pushback against extreme legislation.

</summary>

"As long as Biden has the veto pen, it's unlikely that they'll be able to get anything really wild through."
"You may see some incredibly extreme legislation move forward, but it really doesn't stand a chance of actually becoming law."

### AI summary (High error rate! Edit errors on video page)

President Biden's first veto was on legislation that aimed to limit financial managers in considering factors like ESG scores, climate change, and social considerations.
The legislation, pushed by the Republican Party, wanted to prioritize profit over other factors, but Biden vetoed it.
This veto serves as a reminder to the House that Biden will use his veto power, setting the stage for future battles with the Republican Party.
It's unlikely that Republicans in Congress will have the votes to override a veto.
Typically, more vetoes follow once the first one is used, often as a show for the opposing party's base.
There might be extreme legislation pushed forward by Republicans in the House, but with Biden's veto power, these measures are unlikely to become laws.

Actions:

for politically aware individuals,
Stay informed about legislative developments and the power dynamics between parties (implied)
</details>
<details>
<summary>
2023-03-21: Let's talk about stone tools and rewriting the past.... (<a href="https://youtube.com/watch?v=Ck_v_PEM2bM">watch</a> || <a href="/videos/2023/03/21/Lets_talk_about_stone_tools_and_rewriting_the_past">transcript &amp; editable summary</a>)

Monkeys inadvertently creating stone tool-like flakes challenge the belief that early humans exclusively made tools intentionally, prompting a reevaluation of human history.

</summary>

"Monkeys in Thailand use rocks as tools to smash open nuts."
"The story of humanity is always being rewritten."
"Our story, the story of humanity, it's always being rewritten."

### AI summary (High error rate! Edit errors on video page)

Monkeys in Thailand use rocks as tools to smash open nuts, inadvertently creating stone tool-like flakes during the process.
This observation challenges the long-held belief that early humans exclusively made stone tools intentionally during the Stone Age.
Scientists are now reconsidering whether early humans accidentally created tools in a similar way to the monkeys, using stones as hammer and anvil.
There is also a possibility that other primates, not humans, might have been the original toolmakers, with early humans simply utilizing the found stone splinters.
The implications of this observation are significant, leading to a reevaluation of early human history and potentially rewriting parts of it.
Toolmaking has been a key factor in understanding the advancement and worldview of early humans.
If early humans did not intentionally make these tools but found and utilized them, it challenges previous notions about their capabilities and achievements.
Scientists will need to analyze the locations where these tools were found to determine if they were accidental or not human-made.
The evolution of human history is constantly evolving, with new observations prompting revisions in our understanding.
The story of humanity is always in flux, with each new discovery reshaping our perception of the past.

Actions:

for history enthusiasts, scientists, curious minds,
Analyze stone tool findings to determine accidental or human origin (implied)
Stay updated on new scientific discoveries challenging historical narratives (implied)
</details>
<details>
<summary>
2023-03-21: Let's talk about if NY is handling it wrong with Trump.... (<a href="https://youtube.com/watch?v=YocHYL_LdLA">watch</a> || <a href="/videos/2023/03/21/Lets_talk_about_if_NY_is_handling_it_wrong_with_Trump">transcript &amp; editable summary</a>)

Beau believes starting with minor charges in New York regarding Trump is beneficial for supporters, allowing a gradual acceptance of reality and potentially preventing regretful actions.

</summary>

"I think a slow kind of descent into reality is probably a good thing."
"Starting with the relatively minor stuff in New York is probably a really good thing for his supporters."
"I don't think New York is doing it wrong."
"If they have the evidence, they pursue it."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Some suggest New York should have waited to pursue Trump after bigger cases.
Beau disagrees and believes there's no coordination among prosecutors.
He thinks starting with New York is appropriate due to the relatively minor charges.
Trump's calls for support aren't gaining much traction possibly due to the perceived minor nature of the charges.
Beau sees a slow progression in legal actions as beneficial for supporters to come to terms with reality.
He believes starting with smaller cases may prevent supporters from making regretful decisions.
Beau underscores the lack of coordination among prosecutors and the varying legal systems across jurisdictions.
Legal processes' speed is dependent on filed motions and judicial decisions.
Beau suggests New York starting with minor charges is advantageous for all parties involved.
He expresses that Trump might have a better chance at mounting a defense in New York compared to other jurisdictions.

Actions:

for legal analysts and interested individuals.,
Keep informed about legal proceedings in different jurisdictions (suggested).
Stay updated on developments regarding the legal cases involving Trump (suggested).
</details>
<details>
<summary>
2023-03-21: Let's talk about Biden waking up.... (<a href="https://youtube.com/watch?v=Pw424GG_2-A">watch</a> || <a href="/videos/2023/03/21/Lets_talk_about_Biden_waking_up">transcript &amp; editable summary</a>)

President Biden's administration confronts the House GOP budget proposals, exposing contradictions and potential impacts on Republican priorities, urging reconsideration for the sake of the US economy.

</summary>

"You can't sit there and talk about border security while your proposal cuts, I want say 2,000 jobs, out of Border Patrol."
"It's going to be weird hearing Republicans try to justify defunding the police after the last few years."
"He is definitely pro-capitalism and they're going to have a hard time responding to this."

### AI summary (High error rate! Edit errors on video page)

President Biden's administration is taking a strong stance against the House GOP budget proposals this week.
The Biden administration is bringing attention to the potential impacts of the GOP's proposed budget cuts.
The GOP's proposals include defunding the police, cutting border patrol, train safety, healthcare, energy, manufacturing incentives, Medicare, and defense.
Beau points out the contradictions in the GOP's proposals, such as advocating for border security while cutting Border Patrol jobs and defunding the police while criticizing lawlessness.
The Biden administration's focus on revealing the consequences of the GOP's budget cuts aims to disrupt the GOP's talking points and shed light on the potential negative impacts on their base.
Beau questions the timing of the Biden administration's offensive against the GOP's budget proposals, suggesting it may be strategically timed amidst other news events.
He expresses concerns about the GOP potentially allowing the US to default if they don't get their way with the budget, despite the potential economic consequences.
Beau notes the challenge Republicans may face in justifying their positions, especially regarding defunding the police and mischaracterizing Biden as a leftist.
He hopes for outside pushback to prompt the GOP to reconsider their hardline stance for the benefit of the US economy.

Actions:

for political observers, voters,
Advocate for transparent budget proposals and policies (exemplified)
Engage in political discourse and push for accountability from elected officials (exemplified)
Stay informed about budget decisions and their potential impacts on communities (exemplified)
</details>
<details>
<summary>
2023-03-20: Let's talk about the permafrost zombie virus.... (<a href="https://youtube.com/watch?v=A_6gdTkIGRU">watch</a> || <a href="/videos/2023/03/20/Lets_talk_about_the_permafrost_zombie_virus">transcript &amp; editable summary</a>)

Exploring the intersection of climate change, ancient viruses, and public health risks as permafrost thaws, necessitating preparedness for potential threats.

</summary>

"Ancient viruses could become a real public health issue for humans as permafrost thaws."
"It's worth remembering that mummified remains frozen in permafrost often carry traces of human disease."
"Transitioning from dirty energy is slow; we must prepare for new and old public health challenges."

### AI summary (High error rate! Edit errors on video page)

Exploring the intersection of climate, public health, science fiction, and the future.
Permafrost is thawing due to climate change, releasing ancient viruses termed "zombie viruses."
A 48,500-year-old virus found in permafrost could still infect amoebas, raising concerns about potential public health threats to humans.
Research on ancient viruses has been ongoing since at least 2014 to understand the viability and potential risks.
The research aims to determine the threat these ancient viruses pose and develop defenses against them.
Mummified remains found in permafrost still show traces of human disease, indicating the potential longevity of viruses.
Transitioning from dirty energy is slow, necessitating preparation for new and old public health issues arising from climate change.

Actions:

for climate researchers, public health professionals.,
Monitor research on ancient viruses in permafrost to stay informed and aware of potential public health threats (implied).
Support initiatives that aim to understand and develop defenses against ancient viruses released from thawing permafrost (implied).
</details>
<details>
<summary>
2023-03-20: Let's talk about people losing faith in Fox.... (<a href="https://youtube.com/watch?v=qNM_XqrzaN0">watch</a> || <a href="/videos/2023/03/20/Lets_talk_about_people_losing_faith_in_Fox">transcript &amp; editable summary</a>)

Beau touches on trust in Fox News, urging to find ways to reach individuals still in the echo chamber, despite internal discrepancies and survey results.

</summary>

"Nobody's a lost cause."
"There will still be a significant portion that, well, you just can't reach."
"The right method is out there, it just has to be found."

### AI summary (High error rate! Edit errors on video page)

Discussed the issue of trust in Fox News following recent revelations.
Mentioned internal discrepancies between what was said in private and public.
Speculated that this could shake some Fox viewers out of their echo chamber.
Shared survey results showing 21% trust the network less, while 35% continue to trust it.
Suggested that some viewers who had no opinion might still trust the network but are hesitant to admit it.
Proposed allowing individuals to maintain their belief as a way of reaching out to them.
Noted the impact of major figures expressing different opinions behind the scenes than on air.
Viewed this situation as an opening to reach out to family members still stuck in the echo chamber.
Encouraged not giving up hope in trying to reach family members with different viewpoints.
Emphasized the importance of finding the right mechanism to reach people.

Actions:

for family members,
Reach out to family members still in the echo chamber (suggested)
Find the right method to communicate with those holding different viewpoints (implied)
</details>
<details>
<summary>
2023-03-20: Let's talk about Trump and his requests.... (<a href="https://youtube.com/watch?v=s6qIsTpvYhc">watch</a> || <a href="/videos/2023/03/20/Lets_talk_about_Trump_and_his_requests">transcript &amp; editable summary</a>)

Former President Trump predicts custody on Tuesday, prompting requests for assembly, while concerns over risks and sensibility prevail among supporters and authorities.

</summary>

"I hope that they wouldn't put themselves at risk for him because he's not going to stand by them."
"I really hope that the combination of members of the Republican Party saying, you know, you don't need to do this..."
"I hope that anybody that comes into Florida understands the laws that have been put on the books when it comes to assembly."
"It's not going to accomplish what they want, and it's just going to put them in a situation where they're paying for believing in somebody who would not stand up for them."
"There are a lot of options and a lot of different scenarios."

### AI summary (High error rate! Edit errors on video page)

Former President Trump believes he will be taken into custody on Tuesday, but Beau doesn't believe him.
New York appears to be moving towards a criminal indictment against Trump, prompting him to request his supporters to assemble.
Republican members like McCarthy and Green are advising against protests, hoping that Trump's base remains calm.
Trump's base seems relatively calm, with some suggesting roadblocks and other disruptive actions.
Beau hopes Trump's supporters will recall that he didn't pardon anyone and may not stand up for them.
Concerns about surveillance in New York and the NYPD's experience in handling assemblies that get out of hand are raised.
Beau stresses the importance of the right to peaceably assemble, even for expressing discontent.
Suggestions are made for supporters to gather in Florida near Trump's place, considering the restrictive laws on assembly in the state.
While most of Trump's base appears calm, there are concerns about potential risks and scenarios unfolding.
Beau hopes for calm and sensibility among all involved parties, acknowledging the uncertainty of the situation.

Actions:

for supporters, authorities, observers.,
Understand the laws on assembly in Florida before gathering near Trump's place (suggested).
Stay calm and sensible amidst uncertainties and potential risks (implied).
</details>
<details>
<summary>
2023-03-20: Let's talk about Putin the emperor.... (<a href="https://youtube.com/watch?v=-9ZKsKL5zD4">watch</a> || <a href="/videos/2023/03/20/Lets_talk_about_Putin_the_emperor">transcript &amp; editable summary</a>)

Beau explains the ICC warrant for Putin, compares him to an emperor, and touches on US legislation protecting personnel from the court.

</summary>

"Is Putin more like an emperor or a pirate?"
"The head of state of any major pole of power is an emperor."
"The United States is an emperor as well."
"Sometimes when it comes to not the morality of it, but the legality and the ability to enforce those legal mechanisms, size matters."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the International Criminal Court issuing a warrant for Putin.
Compares Putin to either an emperor or a pirate, discussing the consequences of each.
Mentions the concept that emperors often behave like criminals but are protected by state power.
Points out that Putin, as the head of a major power, is more like an emperor.
States that Putin facing prosecution by the ICC is unlikely without a significant mistake or Russian people's decision.
Mentions the US legislation authorizing using all means necessary to bring back detained personnel from the ICC.
Notes that the US, like Russia, is also considered an emperor and has legislation to protect its personnel.
Suggests that major powers, not just Russia, have mechanisms to avoid facing consequences for actions.
Talks about the importance of size when it comes to enforcing legal mechanisms.
Concludes by stating that there won't likely be a US President going to the ICC and refers to the concept as just a thought.

Actions:

for world citizens,
Contact your representatives to advocate for accountability for leaders regardless of their power (implied).
</details>
<details>
<summary>
2023-03-19: Let's talk about the GOP getting news from the budget office.... (<a href="https://youtube.com/watch?v=EM6UABFagK8">watch</a> || <a href="/videos/2023/03/19/Lets_talk_about_the_GOP_getting_news_from_the_budget_office">transcript &amp; editable summary</a>)

House Republicans face the daunting task of cutting 86% of the budget to balance it in 10 years, potentially leading to a shift in tactics or blaming Biden, prompting considerations for tax increases on the wealthy.

</summary>

"House Republicans have put a lot of political capital behind the idea that they are going to balance the budget within the next 10 years."
"86% means all of it, all of the budget across the board."
"Their tactics are going to change on this issue when confronted with this information."
"The U.S. does have an issue. There are two ways to deal with it."
"We could tax wealthy companies and people a little bit more."

### AI summary (High error rate! Edit errors on video page)

House Republicans face challenges in balancing the budget without touching politically sensitive programs like Social Security and Medicare.
The Congressional Budget Office reveals that House Republicans must cut 86% of the budget, excluding key programs, to achieve budget balance in the next 10 years.
If Republicans maintain Trump-era tax cuts, they must slash 100% of the remaining budget to balance it within a decade.
Beau predicts that Republicans may resort to sabotaging their own proposal and blame it on Biden due to the unfeasibility of their budget plans.
Despite entertaining political maneuvers, the reality is that serious budget cuts or increased taxes on the wealthy are necessary to address the U.S.'s financial issues.
Beau suggests that those nostalgic for the 1950s should also revisit the tax rates of that era for insights into potential solutions.

Actions:

for budget analysts, politicians,
Advocate for fair tax policies targeting wealthy individuals and corporations to alleviate budgetary strains (implied).
</details>
<details>
<summary>
2023-03-19: Let's talk about a GOP miscalculation.... (<a href="https://youtube.com/watch?v=FhnvvIyQtrg">watch</a> || <a href="/videos/2023/03/19/Lets_talk_about_a_GOP_miscalculation">transcript &amp; editable summary</a>)

The Republican Party's anti-woke strategy for 2024 may backfire as majority of Americans view being woke positively, potentially signaling hope for progress and justice.

</summary>

"Many of their leading contenders have put their entire political presence behind being anti-woke."
"The idea that most Americans view being woke as a positive thing can only be a good thing for society."
"It certainly should encourage the Democratic Party to take even more progressive positions."

### AI summary (High error rate! Edit errors on video page)

The Republican Party's strategy for 2024 may backfire due to their anti-woke stance.
A recent poll shows that 56% of Americans have a positive view of being woke, equating it with being informed about social injustices.
Only 39% of Americans view being woke as overly politically correct or policing others' words.
Republican candidates have heavily invested in being anti-woke, but this may only resonate with 39% of voters.
Majority of Americans see being woke in a positive light, making the Republican strategy potentially problematic.
Pushing different elements of society into the "woke" category may alienate more people.
Republican strategists may regret their current approach in the future.
Some Republican candidates are too entrenched in the culture war to shift their stance.
Being woke is viewed positively by most Americans and signifies progress and justice.
The poll results should encourage the Democratic Party to take more progressive positions.

Actions:

for political strategists, voters,
Reassess political strategies based on public sentiment (suggested)
Encourage political parties to take more progressive positions (implied)
</details>
<details>
<summary>
2023-03-19: Let's talk about Dr. Strangelove as a documentary.... (<a href="https://youtube.com/watch?v=TE0U6XXfZFA">watch</a> || <a href="/videos/2023/03/19/Lets_talk_about_Dr_Strangelove_as_a_documentary">transcript &amp; editable summary</a>)

Beau talks about the absurdity of "Dr. Strangelove," urges for a cooperative system over competition to prevent catastrophic risks, and provides hope for a peaceful world despite nuclear threats.

</summary>

"You have to move to a more cooperative system rather than a competitive one."
"Maybe you can learn to stop worrying, even if you don't learn to love the bomb."
"Every generation can move a little bit further towards peace, towards a world that doesn't rely on systems that are pointed at each other."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of discussing "Dr. Strangelove" as a documentary and mentions how someone believed it was a documentary based on his previous videos.
The viewer shares their surprise upon realizing that "Dr. Strangelove" is not a documentary but a movie.
The viewer expresses their unease at the absurdity of the movie's portrayal of a few people having the power to cause the end of the world.
Beau acknowledges the absurdity of the movie and relates it to the need for a more cooperative system rather than a competitive one to avoid catastrophic risks.
Beau explains the concept of mutually assured destruction (MAD) and how it has kept things in balance despite the risks associated with nuclear weapons.
Beau encourages watching a linked video for more context and history on the topic, suggesting that even if the subject isn't intriguing, the information is valuable.
Beau reassures that there are efforts to limit nuclear threats, even though it might not seem evident, and that much of the posturing is for deterrence.
Beau stresses the importance of not succumbing to overwhelming dread but continuing to push forward for change and peace across generations.
Beau concludes by reminding viewers that despite the existential risks posed by nuclear weapons, humanity has persevered, urging them to keep this perspective in mind.

Actions:

for global citizens, peace advocates,
Watch the linked video for more context and history on nuclear threats (suggested).
</details>
<details>
<summary>
2023-03-19: Let's talk about 8 months of change.... (<a href="https://youtube.com/watch?v=ct_-dsJMfRk">watch</a> || <a href="/videos/2023/03/19/Lets_talk_about_8_months_of_change">transcript &amp; editable summary</a>)

A journey of personal growth from prejudice to acceptance, guided by interactions with a trans woman and her friends.

</summary>

"If you are the same person you were five years ago, you've wasted five years of your life."
"You're not a horrible person. You are a person mid-change."
"You went from kind of a bigot to being so concerned about something you thought, not even something you did or said, but something you thought that you sent this message."
"Change takes time. You're not going to get everything right, right away, but you'll keep moving forward."
"Just the fact that you want to learn more is a pretty good indication that you're going to be successful."

### AI summary (High error rate! Edit errors on video page)

A person shares their journey of change over eight months, prompted by meeting a trans woman at work.
They were initially conservative and uncomfortable with the LGBTQ+ community, but their perspectives have evolved.
The person describes their positive interactions with the trans woman, who they find feminine and kind.
Despite this progress, they struggle to view the trans woman's friends in the same light.
The person seeks advice on how to overcome this mental block and continue growing.
Billy Young reassures them that change is a process and commends their progress.
He encourages continued learning and self-reflection to combat internal biases.
The person is reminded that their transformation signifies growth and should be embraced.
Billy Young explains that passing privilege may influence the ease of accepting the trans woman they know well.
He acknowledges the person's journey from holding prejudiced beliefs to actively seeking understanding.
Growth involves realizing that a person's appearance does not solely define their gender.
Billy Young praises the person's willingness to learn and adapt, showcasing their commitment to change.
The person is assured that their struggle is normal and indicative of ongoing progress.
Despite feeling challenged by accepting others, the person is encouraged to keep moving forward in their journey.
Billy Young suggests that their relationship with the trans woman influences their ability to accept her friends.

Actions:

for individuals seeking guidance on personal growth and overcoming biases.,
Keep learning and allowing yourself to be educated (implied).
Continue broadening your perspectives and challenging internal biases (implied).
Embrace change as a gradual process and be patient with yourself (implied).
</details>
<details>
<summary>
2023-03-18: Let's talk about what happens if Trump gets indicted.... (<a href="https://youtube.com/watch?v=m-z1r45jooU">watch</a> || <a href="/videos/2023/03/18/Lets_talk_about_what_happens_if_Trump_gets_indicted">transcript &amp; editable summary</a>)

Analyzing the potential impact of legal troubles on Trump's run and the unprecedented scenarios that could unfold, leading to unprecedented political maneuvering.

</summary>

"There isn't a lot of historical precedent for what could occur because this has never happened."
"There's going to be a lot of maneuvering that we've never seen before, that the US political system has never witnessed."

### AI summary (High error rate! Edit errors on video page)

Analyzing the possibilities of former President Donald J. Trump's run amid legal troubles in Georgia, New York, and DC.
Speculating on the impact of a potential indictment on Trump's ability to run and his base support.
Posing questions on what happens if Trump wins the primary and is indicted before or after, and the potential trial's timing.
Contemplating the unprecedented scenario of Trump being indicted but winning the general election.
Exploring the uncertainty around historical precedents for dealing with a situation like Trump's legal challenges.
Noting the challenge for the Republican Party in removing Trump if he wins the primary but is later convicted.
Mentioning potential scenarios where Trump's legal troubles could become insurmountable post-nomination.

Actions:

for political analysts, election strategists.,
Stay informed on the legal developments surrounding former President Donald J. Trump's potential run. (implied)
</details>
<details>
<summary>
2023-03-18: Let's talk about the water situation out west.... (<a href="https://youtube.com/watch?v=GQrZWZptMhU">watch</a> || <a href="/videos/2023/03/18/Lets_talk_about_the_water_situation_out_west">transcript &amp; editable summary</a>)

Beau covers the ongoing water shortage out west and the cautious optimism surrounding recent improvements in reservoir levels amidst a long-term drought.

</summary>

"Major reservoirs are trending up."
"Reservoirs and groundwater remain at historic lows."
"We're definitely going in the right direction, but we still have a long way to go."
"The worst may be over, but certainty depends on upcoming forecasts."

### AI summary (High error rate! Edit errors on video page)

Covering the water shortage out west on the channel.
Major reservoirs are trending up.
Reservoirs and groundwater remain at historic lows due to a long-term drought of over 20 years.
Recent rain and snowpack are positive, but the drought could last until 2030.
Forecasts predict warming leading to evaporation.
Hydrologist warns there's still a long way to go despite positive trends.
Concerns about water management areas not continuing precautions after recent improvements.
Hope for ongoing conservation efforts.
Positive news overall, but still a long way to go.
The worst may be over, but certainty depends on upcoming forecasts.

Actions:

for environmental advocates, water conservationists,
Continue conservation efforts to manage water resources effectively (implied)
Stay informed about water management practices in your area (implied)
</details>
<details>
<summary>
2023-03-18: Let's talk about the seaweed blob.... (<a href="https://youtube.com/watch?v=AQKRO_TZ58I">watch</a> || <a href="/videos/2023/03/18/Lets_talk_about_the_seaweed_blob">transcript &amp; editable summary</a>)

Beau explains the challenges posed by the massive sargassum seaweed bloom heading towards Florida and Mexico, impacting coastal areas and tourism, requiring a balance between agricultural needs and beach preservation, and potentially necessitating regulations on nutrient runoff.

</summary>

"Nobody wants to go to a beach that is covered in rotting seaweed and smells like rotten eggs."
"That smell, that's what it is."
"This is gonna become something that they're gonna have to deal with."
"And we've dealt with big ones before, but this is huge."
"It's huge."

### AI summary (High error rate! Edit errors on video page)

Explains about sargassum seaweed bloom in the ocean heading towards Florida and Mexico.
Describes sargassum seaweed as beneficial in the ocean but problematic when it washes ashore.
Mentions that the seaweed can cause respiratory and skin issues for some people.
Attributes the unbalanced sargassum bloom to climate change and nutrients runoff.
Points out the need for coastal areas to address the seaweed issue, especially in conjunction with red tide occurrences.
Emphasizes the importance of balancing agricultural needs with beach preservation.
Raises concerns about the impact on tourism in areas affected by the seaweed bloom.
Suggests that regulations on nutrient runoff may help alleviate the seaweed issue.
Indicates that tourism decline might prompt officials to take action.
Summarizes the seaweed bloom situation as a significant challenge for coastal regions.

Actions:

for travelers, coastal residents,
Monitor local news for updates on seaweed bloom impacts (implied)
Support regulations to reduce nutrient runoff into oceans (implied)
</details>
<details>
<summary>
2023-03-18: Let's talk about Biden, water, and forever chemicals.... (<a href="https://youtube.com/watch?v=nNvc1-HBZS8">watch</a> || <a href="/videos/2023/03/18/Lets_talk_about_Biden_water_and_forever_chemicals">transcript &amp; editable summary</a>)

Beau addresses water contamination by PFAS chemicals, urging immediate action due to their harmful effects on millions of Americans and the importance of ongoing funding for long-term solutions.

</summary>

"They're called that because they don't degrade quickly, and they repel water. It's just all bad."
"This may not be something that we want to delay and delay and delay."
"When the problem is a shortage of funding, throwing money at it is like super helpful."
"So look to state coverage if this is a topic you're really interested in."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of water and the EPA's new regulations on PFAS chemicals.
PFAS chemicals are described as forever chemicals due to their slow degradation and water-repellent properties.
These chemicals are contaminating the nation's water supply, impacting around 200 million people in the U.S.
The EPA is focusing on six specific PFAS chemicals, implementing new regulations and treatment procedures.
Four out of five states took no effective action against these harmful chemicals, prompting EPA intervention.
Questions arise about funding for addressing the issue, with the Biden infrastructure bill allocating some billions for it.
While the dedicated funds will help, more resources will be required over time to combat water contamination effectively.
Emphasizes the daily necessity of water and the importance of immediate action rather than delays.
Suggests that investing in solving water contamination issues is vital and not just a matter of throwing money at the problem.
Anticipates ongoing news coverage as different regions decide on implementation and potential legal challenges.
Encourages staying informed through local and state news outlets for updates on this significant issue.

Actions:

for environmental activists, concerned citizens,
Stay informed through local and state news coverage (suggested)
Advocate for sufficient funding and resources to combat water contamination (implied)
</details>
<details>
<summary>
2023-03-17: Let's talk about what happened in Russia.... (<a href="https://youtube.com/watch?v=3VkWXTy3uiI">watch</a> || <a href="/videos/2023/03/17/Lets_talk_about_what_happened_in_Russia">transcript &amp; editable summary</a>)

Beau examines a fire incident at an FSB building in Russia, with official statements contradicting speculation, urging an electrical systems review for critical facilities.

</summary>

"It is probably time for the Russian government to order a review of the electrical systems in any building or facility that is critical to their war effort."
"Russian media will certainly not deliberately air something that was inaccurate just to keep people calm."

### AI summary (High error rate! Edit errors on video page)

Provides an overview of an incident in Russia involving a fire at an FSB building in Rostov-on-Don.
Russian government suggests the fire was caused by an electrical issue, destroying ammunition.
Ukrainian government implies internal issues at the FSB building rather than external involvement.
Speculation online suggests a military operation by Ukraine or its allies, contrary to official statements.
One person lost and two injured in the incident.
Beau urges the Russian government to conduct a thorough electrical systems review in critical facilities.
Fires seem to be occurring in places vital to Russia's war effort, raising concerns.
Official statements from both governments deny unconventional operation involvement.
Russian media is unlikely to report inaccurately just to maintain calm.

Actions:

for journalists, analysts, concerned citizens,
Contact local authorities to ensure fire safety measures are up to standard (implied)
</details>
<details>
<summary>
2023-03-17: Let's talk about missing uranium.... (<a href="https://youtube.com/watch?v=j3fNMUps71g">watch</a> || <a href="/videos/2023/03/17/Lets_talk_about_missing_uranium">transcript &amp; editable summary</a>)

Beau clarifies the missing uranium situation in Libya, urging calm and trust in international agencies while cautioning against Western intervention.

</summary>

"The difference between natural uranium going missing, that's where the international community goes, oh, no."
"There's no good that can come of that."
"I don't think they're going to make that mistake."

### AI summary (High error rate! Edit errors on video page)

The International Atomic Energy Agency reported 2.5 tons of natural uranium missing in Libya, but it's not weapons-grade or enriched, causing unnecessary panic.
If the missing uranium reached a group with the capability, it could potentially be enriched into weapons-grade material.
The difference between natural uranium and weapons-grade uranium going missing is significant in terms of international response and urgency.
The Eastern Libyan forces claim to have recovered the missing uranium, known as yellowcake, but it's not confirmed by the International Atomic Energy Agency yet.
The best-case scenario is for the Eastern forces to demonstrate the uranium is properly secured or hand it over to the IAEA.
Refusal to cooperate and demonstrate the security of the uranium could invite international intervention and lead to negative consequences.
Beau believes that the situation will likely be resolved swiftly and peacefully, given the stakes involved.
The missing uranium likely disappeared from a location outside of the government's effective control in eastern Libya, where the recovery was claimed.
Beau urges against Western involvement in this matter, stating that it should be resolved with international partners or within Libya itself.
The potential catalyst for escalation could be if the Eastern forces refuse to cooperate or hand over the recovered uranium.
Beau advises everyone to remain calm and trust the International Atomic Energy Agency to handle the situation.

Actions:

for world leaders, policymakers, diplomats,
Trust the International Atomic Energy Agency to handle the situation (suggested)
Remain calm and avoid unnecessary panic (suggested)
</details>
<details>
<summary>
2023-03-17: Let's talk about Russia recovering it.... (<a href="https://youtube.com/watch?v=T2sTm68Vw9s">watch</a> || <a href="/videos/2023/03/17/Lets_talk_about_Russia_recovering_it">transcript &amp; editable summary</a>)

Beau examines the potential repercussions of Russia recovering a reaper drone and the broader implications on international dynamics.

</summary>

"The real concern is not Russia getting it and doing something with it. The real concern is Russia getting it and giving it to the Chinese military who actually do have the capability to exploit it."
"The ability to reverse engineer it, sure they can do it, but by the time they're in a position to be able to exploit it, the US tech is going to be further ahead."
"The Russian government has to understand, on some level, they have to know that if they are successful in this, they're removing one of the key reasons they're not facing this thing on a battlefield."

### AI summary (High error rate! Edit errors on video page)

Exploring the potential scenarios of Russia recovering a reaper drone.
Speculating on Russia's ability to reverse-engineer and exploit the technology.
Concerns about Russia potentially sharing the technology with China.
Impact on US-Ukraine dynamics if Russia successfully recovers the drone.
The significance of Ukraine possessing Reaper drones in the conflict.
Potential implications of Russia's actions on the ongoing situation.
Acknowledging the limitations of Russia's military technology.
Considering the broader geopolitical implications of Russia's actions.
The importance of understanding Russia's intentions and capabilities.
The uncertainty surrounding the situation and US Department of Defense statements.

Actions:

for tech analysts, policymakers, military strategists.,
Monitor developments in Russia's attempts to recover the drone (implied).
Stay informed about the evolving situation and potential impacts on international relations (implied).
</details>
<details>
<summary>
2023-03-17: Let's talk about Putin's response to pipeline coverage.... (<a href="https://youtube.com/watch?v=TVP8t7YwRK8">watch</a> || <a href="/videos/2023/03/17/Lets_talk_about_Putin_s_response_to_pipeline_coverage">transcript &amp; editable summary</a>)

Putin dismisses Ukraine's involvement in pipeline incident as "sheer nonsense," prioritizing domestic and international messaging over potential truths.

</summary>

"Putin responded to the idea that Ukraine was behind the pipeline incident, dismissing it as 'sheer nonsense.'"
"Putin wants Russians to believe they are safe at home and not vulnerable to attacks like the one on the pipeline."
"Beau believes Putin is telling the truth in saying it's 'sheer nonsense' and that even if civilians were involved, he'd still suggest they had help."

### AI summary (High error rate! Edit errors on video page)

Putin responded to the idea that Ukraine was behind the pipeline incident, dismissing it as "sheer nonsense."
Putin's response doesn't make sense as it could motivate his troops and provide justifications if Ukraine was blamed.
Putin's reputation in the West as a liar influences how people interpret his statements.
Putin wants Russians to believe they are safe at home and not vulnerable to attacks like the one on the pipeline.
Putin likely believes that the perpetrators, even if non-state actors, had state assistance in carrying out the attack.
The possibility of a corporate actor benefiting economically from Russia not having the pipeline is not being widely considered.
Beau believes Putin is telling the truth in saying it's "sheer nonsense" and that even if civilians were involved, he'd still suggest they had help.
Putin's messaging domestically and internationally is a key factor in how he responds to allegations regarding the pipeline incident.

Actions:

for observers, analysts, activists,
Question the narratives presented by political leaders (implied)
</details>
<details>
<summary>
2023-03-16: Let's talk about a new Trump phone call in the Georgia case.... (<a href="https://youtube.com/watch?v=qqMDWBYKCpU">watch</a> || <a href="/videos/2023/03/16/Lets_talk_about_a_new_Trump_phone_call_in_the_Georgia_case">transcript &amp; editable summary</a>)

Recent revelations of a third recorded phone call involving Trump and Georgia add layers to the state's case, with potential indictments looming and information trickling out from the grand jury.

</summary>

"It is wild to me the number of people who were recording a phone call with the then President of the United States."
"This indicates that there might be a whole lot more to the state's case."
"Charging decisions were imminent in January."
"Until that process is completed, we're not going to know anything."
"And that may lead to a clearer picture of what's going on."

### AI summary (High error rate! Edit errors on video page)

Recent reporting reveals a third recorded phone call involving former President Donald J. Trump and Georgia.
The special purpose grand jury heard recordings, including a call to Georgia House Speaker David Ralston.
Ralston was reportedly asked by Trump to convene the Georgia Assembly, which he was not enthusiastic about.
Ralston is no longer able to comment, but the recording was played for the grand jury.
Georgia Governor Kemp also mentioned Trump wanting the assembly convened for an audit of mail-in votes.
The content of the third call is unknown, but based on previous statements, it may not be favorable news.
Several individuals were recording calls with the former President, including the one in question.
CNN reported the information, confirming the existence of the recording with five grand jurors acknowledging it.
The revelations from these recordings suggest there could be more to the state's case.
Charging decisions were said to be imminent in January by the District Attorney.
There are conflicting rumors about when the grand jury for potential indictments began, either in February or March.
Until the process is complete, the full details remain unknown, but there might be information trickling out from grand jury members speaking to the press.

Actions:

for legal analysts and concerned citizens.,
Stay informed about updates on the legal proceedings (implied).
Follow reputable news sources for accurate information on the case (implied).
</details>
<details>
<summary>
2023-03-16: Let's talk about Virginia and what's next.... (<a href="https://youtube.com/watch?v=Imp4r34pal0">watch</a> || <a href="/videos/2023/03/16/Lets_talk_about_Virginia_and_what_s_next">transcript &amp; editable summary</a>)

Seven deputies charged with second degree murder in the death of Ervo Otenyo, raising questions about mental health and promising a lengthy and impactful legal process.

</summary>

"The public and experienced mental health professionals alike will be appalled when the facts of this case are fully made known."
"It certainly seems like this is going to be national news."
"There's a whole lot of questions as far as the mental health issues."
"We're probably in for another long haul."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Seven deputies in Henrico County, Virginia have been charged with second degree murder for the death of Ervo Otenyo, a 28-year-old man.
Ervo Otenyo was being admitted to a hospital and allegedly became combative.
The prosecutor stated that Otenyo died of asphyxia and indicated that the seven deputies were involved.
Documentation suggests Otenyo was on an emergency custody order related to mental health, which may be a significant factor.
There are reports of video footage of the incident, described as brutal and inhumane.
Despite the quick arrests of the deputies, there still needs to be a grand jury process before further progress.
The deputies are set to appear in court on the 21st, where more information about the process may emerge.
Statements from the prosecution, family attorney, and local FOP suggest a tense and uncertain situation.
Many questions remain unanswered regarding mental health issues and the sequence of events leading to Otenyo's death.
The unfolding events are likely to attract national attention, with more revelations expected in the future.

Actions:

for community members, activists,
Support mental health advocacy groups (implied)
Stay informed about the case developments and outcomes (implied)
Attend court proceedings if possible to show solidarity and demand justice (implied)
</details>
<details>
<summary>
2023-03-16: Let's talk about Ukraine possibly getting some air power.... (<a href="https://youtube.com/watch?v=VdrgvLK5c0w">watch</a> || <a href="/videos/2023/03/16/Lets_talk_about_Ukraine_possibly_getting_some_air_power">transcript &amp; editable summary</a>)

Beau analyzes the potential impact of Ukraine acquiring new aircraft through trades, questioning whether it will be a significant game-changer in the conflict.

</summary>

"A lot of rumors about this going around about this deal. It might happen but I don't see this as a massive game-changing thing, even if it does."
"The United Kingdom is willing to provide Eurofighter Typhoons to countries that have MiG-29s that they are willing to give to Ukraine."

### AI summary (High error rate! Edit errors on video page)

Exploring the possibility of Ukraine acquiring new aircraft through a series of trades involving Eurofighter Typhoons and MiG-29s.
The United Kingdom is willing to provide Eurofighter Typhoons to countries willing to give their MiG-29s to Ukraine.
Countries currently operating MiG-29s may have to retrain their pilots for the Typhoon, weakening their defenses temporarily.
The MiG-29 is considered outmatched by current Russian aircraft, suggesting the potential aircraft transfer may not be a game-changer.
NATO may hesitate to provide aircraft if the impact on the battlefield isn't significant enough to shift the balance of power decisively.
While the deal might happen, it is unlikely to be a massive game-changer in the conflict.
Concerns exist about the effectiveness of the MiG-29s in altering the balance of power significantly.

Actions:

for military analysts, policymakers,
<!-- Skip this section if there aren't physical actions described or suggested. -->
</details>
<details>
<summary>
2023-03-16: Let's talk about Tennessee and $2 billion.... (<a href="https://youtube.com/watch?v=vhbA2uO3PTI">watch</a> || <a href="/videos/2023/03/16/Lets_talk_about_Tennessee_and_2_billion">transcript &amp; editable summary</a>)

Tennessee's legislation to lock gender markers may cost billions in federal funding, prioritizing money over morality.

</summary>

"This is something that the state can probably legally do and actually get away with."
"Two billion dollars, it's a lot of money and that's really kind of what they're looking at losing."
"But I think in this case, it's the United States. It runs on money."
"The loss of funding may be a stronger argument in Tennessee than morality."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Tennessee is considering legislation that will make it impossible to change gender markers on official documents, essentially locking people into their birth sex.
The bill's goal seems to be to prevent certain groups, like trans people, from claiming discrimination by removing them as a protected class.
If the bill becomes law, Tennessee could lose billions in federal funding due to creating a mechanism for discrimination that goes against federal policies.
While legally Tennessee might get away with this move, it could have significant financial consequences that its supporters might not fully grasp.
Beau questions the motives of politicians in Tennessee and whether they are willing to risk losing substantial federal funding just to create a false enemy for political gain.
The potential loss of two billion dollars in federal money due to this legislation is significant.
Beau points out the moral wrongness of the bill but acknowledges that in the United States, financial considerations often carry more weight than morality.
Beau underscores the importance of monitoring the situation closely to see how it unfolds.

Actions:

for tennessee residents, activists,
Monitor the progress of the legislation in Tennessee and stay informed about its potential impact (implied).
Advocate against discriminatory legislation by raising awareness and mobilizing support (implied).
Support organizations and groups working to protect the rights of transgender individuals in Tennessee (implied).
</details>
<details>
<summary>
2023-03-15: Let's talk about what went down over Ukraine.... (<a href="https://youtube.com/watch?v=YRr7WKbmYrw">watch</a> || <a href="/videos/2023/03/15/Lets_talk_about_what_went_down_over_Ukraine">transcript &amp; editable summary</a>)

Geopolitical tension rises between the US and Russia over a drone collision, unlikely to escalate into war, echoing Cold War dynamics.

</summary>

"This isn't something that is likely to cause any major issues."
"This isn't going to spiral out of control."
"Be ready for more of this."
"At the end of it nobody got hurt, nobody was injured except for their pride maybe."
"This will probably fade from the news pretty quickly."

### AI summary (High error rate! Edit errors on video page)

Geopolitical tension increased between Russia and the United States due to an incident involving a US MQ-9 Reaper drone and Russian SU-27 aircraft.
The Russian aircraft played around with the drone, leading to a collision as per the US, while Russia denies a collision occurred.
The Reaper was in international airspace, but Russia claims it was in a zone designated for special military operations.
Such interactions between countries' aircraft are not uncommon, often involving interceptors being sent up.
Despite the incident, it is unlikely to escalate into a major issue or lead to war between the US and Russia.
These events echo Cold War dynamics, and similar incidents may occur in the future.

Actions:

for international observers,
Monitor international relations and diplomatic communications for updates (implied).
Stay informed about geopolitical tensions and incidents to understand global dynamics (implied).
</details>
<details>
<summary>
2023-03-15: Let's talk about some out of this world Pentagon quotes.... (<a href="https://youtube.com/watch?v=ZeiARpEG760">watch</a> || <a href="/videos/2023/03/15/Lets_talk_about_some_out_of_this_world_Pentagon_quotes">transcript &amp; editable summary</a>)

Beau introduces the Pentagon's speculative exploration of an alien mothership concept, urging readiness for unconventional theories without confirmed evidence.

</summary>

"Basically, this office, they have found a number of things that could be explained by optical sensors being off, normal run-of-the-mill errors that could explain what was witnessed."
"It's worth remembering that this office is not going to make a definitive statement on something like that without a giant press conference."
"Expect a lot of just weird stuff to come out of that office over the next probably three to five years."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a topic that is gaining attention: the possibility of an artificial interstellar object being a parent craft releasing probes near Earth.
Sean Kirkpatrick, director of the Pentagon's All Domain Anomaly Resolution Office, suggests the concept of a literal alien mothership in a research paper draft.
The office's purpose is to explain unexplainable phenomena, like optical errors or objects seemingly defying the laws of physics.
Despite the intriguing theory, it's emphasized that this is a speculative explanation and not confirmed to be occurring.
The office's transparency in exploring unconventional ideas aims to remove stigma around investigating such topics post-World War II.
Beau advises that the public should anticipate more unconventional theories and statements from the Pentagon's anomaly office in the future.
While the suggested explanation is plausible, it's vital to understand it's one of many possibilities, not a definitive conclusion.
The office won't make firm statements without thorough evidence and press conferences.
Expect a surge of bizarre information from the Pentagon's anomaly office over the next few years, prompting a need for open-mindedness and critical thinking.
Ultimately, the exploration of unconventional ideas is encouraged, but it's vital to differentiate between speculative theories and confirmed facts.

Actions:

for science enthusiasts,
Stay informed on future developments from the Pentagon's anomaly office (implied)
Foster open-mindedness and critical thinking towards unconventional theories (implied)
</details>
<details>
<summary>
2023-03-15: Let's talk about Fox, quotes, and a father-in-law.... (<a href="https://youtube.com/watch?v=fnSfVWT7Ijc">watch</a> || <a href="/videos/2023/03/15/Lets_talk_about_Fox_quotes_and_a_father-in-law">transcript &amp; editable summary</a>)

Beau shares techniques for gently guiding individuals away from misinformation by introducing diverse news sources gradually, acknowledging that change takes time and patience.

</summary>

"The truth is never told, it's realized."
"It's not going to happen in one conversation."
"Change is a process. It takes time."
"If you're serious about it, you have to invest a lot of time in."
"Just because you can get them to understand the baseless nature of a lot of the claims about the election, doesn't mean that they're going to become progressive."

### AI summary (High error rate! Edit errors on video page)

Giving an overview of reaching out techniques and managing expectations when discussing sensitive topics, like politics.
Describing a situation where someone attempted to talk to their father-in-law about news sources and misinformation.
Explaining how the father-in-law reacted defensively, pivoting the topic and expressing distrust in all news sources.
Expressing frustration at the father-in-law's reluctance to accept new information and change opinions.
Asking for advice on how to break through the defensive rhetoric and make someone acknowledge credible reporting.
Emphasizing that forcing someone to accept new information is not possible, but multiple gentle, informative interactions can lead to change.
Suggesting the strategy of slowly introducing alternative news sources like the Associated Press (AP) to encourage critical thinking.
Recommending showing multiple news outlets' coverage of a story to demonstrate credibility and encourage independent evaluation.
Mentioning the importance of patience, multiple small dialogues, and allowing time for individuals to process information and change their views gradually.
Stating that even small movements in changing someone's entrenched beliefs are considered progress in the right direction.

Actions:

for family members, community members,
Have multiple gentle and informative dialogues to help individuals transition away from misinformation (suggested).
Introduce diverse news sources like the Associated Press (AP) to encourage critical thinking (suggested).
</details>
<details>
<summary>
2023-03-15: Let's talk about Colorado, the GOP, and a moment.... (<a href="https://youtube.com/watch?v=vqy_kvbfN_I">watch</a> || <a href="/videos/2023/03/15/Lets_talk_about_Colorado_the_GOP_and_a_moment">transcript &amp; editable summary</a>)

Colorado's Republican Party shifts leadership, potentially creating an opening for Democrats to solidify their position in the state amid changing demographics and extreme rhetoric.

</summary>

"This is probably a moment for the Democratic Party to cement itself in Colorado."
"The real problem is that they weren't going far enough."
"Kind of a golden opportunity for the Democratic Party."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Colorado's state-level Republican Party is shifting leadership, with Dave Williams taking the helm.
Williams is a believer in claims about the 2020 election and has criticized Mitch McConnell.
Colorado has been voting for Democratic presidential candidates since 2008, with Biden winning by 13.5 points in 2020.
Williams, as the new leader, will have influence over how the party is organized and candidates are run.
Colorado's demographics are changing, trending more blue over time.
Despite the near defeat of Boebert, the Republican Party in Colorado seems to be doubling down on extreme rhetoric.
This shift presents an opening for the Democratic Party to solidify its position in Colorado.
The Republican Party's strategy of catering to an energized base may alienate moderates, potentially benefiting the Democrats.
Beau suggests that this could be a golden moment for the Democratic Party to make significant gains in Colorado.
Beau signs off with a thought-provoking message for his audience.

Actions:

for colorado voters,
Support Democratic Party efforts in Colorado to solidify and expand their presence (implied).
</details>
<details>
<summary>
2023-03-14: Let's talk about Trump responding to Pence.... (<a href="https://youtube.com/watch?v=aHYOUeH4YCE">watch</a> || <a href="/videos/2023/03/14/Lets_talk_about_Trump_responding_to_Pence">transcript &amp; editable summary</a>)

Former President Trump blames former Vice President Pence for January 6th, showcasing his refusal to take responsibility and demanding unwavering obedience.

</summary>

"Trying to paint Pence as the person responsible in any way for what happened on the 6th is just wild."
"And for Republicans, you really need to think about the thought process that has to be behind a statement like this."

### AI summary (High error rate! Edit errors on video page)

Former President Trump is responding to former Vice President Pence's recent statements, indicating that Trump believes Pence is to blame for January 6th.
Trump claims that if Pence had sent the votes back to legislators, there wouldn't have been a problem with January 6th, despite this not being within Pence's power.
Trump seems to be trying to shift the blame onto Pence, portraying him as responsible for the events of January 6th.
The attempt to make Pence accountable for the insurrection demonstrates Trump's mindset of deflecting responsibility and expecting obedience without taking any blame.
Trump's statement reveals his belief that any negative outcomes are solely due to people not obeying him and that he is absolved of any responsibility.
Despite not being a fan of Pence, Beau finds Trump's attempt to blame him as concerning and questions whether this is the kind of representation Republicans want.
The thought process behind Trump's statement raises critical questions for Republicans about the type of leadership they are endorsing.

Actions:

for republicans, political observers,
Analyze the thought process behind statements from political figures (implied)
Question the type of leadership endorsed within political parties (implied)
</details>
<details>
<summary>
2023-03-14: Let's talk about NY, Cohen, and Trump.... (<a href="https://youtube.com/watch?v=7HaKQ_or89E">watch</a> || <a href="/videos/2023/03/14/Lets_talk_about_NY_Cohen_and_Trump">transcript &amp; editable summary</a>)

Beau cautions against high hopes for a potential criminal case against Trump in New York, citing uncertainties around evidence and cross-examination, urging a reserved approach until trial.

</summary>

"I am not going to get my hopes up about it because we don't know what they have, really."
"Trump's defense is going to go after Cohen pretty heavily, I would think."
"If New York doesn't have some heavy documentation and something other than Cohen to frame it, I think there's a good chance that the case falls apart during cross."
"Manage your own expectations until trial."
"I have reservations about saying that this is the one that's going to move forward on it."

### AI summary (High error rate! Edit errors on video page)

Talks about the excitement surrounding a potential criminal case against Donald Trump in New York.
Expresses skepticism about the case due to lack of clear evidence beyond Michael Cohen.
Points out the difference between testimony before trial and cross-examination during trial.
Anticipates a strong defense from Trump's side, given Cohen's history of adversarial statements.
Raises doubts about the outcome of the case if New York lacks substantial documentation apart from Cohen.
Advises managing expectations regarding the case until it reaches trial phase.
Emphasizes the importance of concrete evidence in ensuring a successful prosecution.
Acknowledges differing opinions among analysts regarding the strength of the case against Trump.
Stresses the need to wait for more information before drawing conclusions about the case's potential outcome.
Encourages caution and reservation in forming expectations about the legal proceedings.

Actions:

for legal observers,
Manage your expectations until trial (suggested)
Wait for more information before drawing conclusions (implied)
</details>
<details>
<summary>
2023-03-14: Let's talk about Australia and nuclear submarines.... (<a href="https://youtube.com/watch?v=5Hq9dw6VJ0c">watch</a> || <a href="/videos/2023/03/14/Lets_talk_about_Australia_and_nuclear_submarines">transcript &amp; editable summary</a>)

Australia's acquisition of nuclear-powered submarines through the AUKUS agreement is not a part of a new arms race or a major escalation, providing enhanced defense capabilities without nuclear weapons.

</summary>

"These submarines will not have nuclear weapons on them."
"Australia is getting new subs."
"It's not a massive escalation, I don't even know if I'd call this really an escalation."

### AI summary (High error rate! Edit errors on video page)

Australia, the UK, and the US have an agreement called AUKUS where Australia will acquire nuclear submarines.
These submarines are nuclear-powered but not nuclear-armed.
The propulsion system is nuclear-powered, not for launching nuclear missiles.
This move is not part of a new nuclear arms race or a significant escalation.
The submarines will provide Australia with normal, not strategic submarine capabilities.
China is unlikely to strongly react to this development.
The US sharing its nuclear propulsion technology with Australia is a unique aspect of this agreement.
This deal is more about enhancing Australia's defense capability rather than a major strategic shift.
Australia will receive training and assistance from both the US and the UK for this submarine acquisition.
The news should be framed as Australia obtaining new submarines, not as a significant escalation.

Actions:

for policy analysts, defense experts,
Monitor diplomatic responses to Australia's acquisition of nuclear-powered submarines (implied)
Stay informed about the implications of nuclear propulsion technology sharing between countries (implied)
</details>
<details>
<summary>
2023-03-14: Let's talk about Alaska, Willow, and Biden.... (<a href="https://youtube.com/watch?v=46Qh4NI5N34">watch</a> || <a href="/videos/2023/03/14/Lets_talk_about_Alaska_Willow_and_Biden">transcript &amp; editable summary</a>)

Beau breaks down the Willow Project in Alaska, discussing the political and economic implications and expressing disappointment in the missed leadership moment for transitioning away from fossil fuels.

</summary>

"None of this should have gone forward."
"This was the opportunity for the Biden administration to make the hard choice and lead."
"I understand the economics of it. I understand the political realities of it. I get that."
"I think there could have been, there should have been other options on the table."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the Willow Project in Alaska, with two main sides: the oil company and environmental groups, both having supporters and opposers.
Details the approval given by the Biden administration for the project, with the oil company viewing it as a win.
Notes the compromises made by both sides, with the oil company getting most of what they wanted but having to give up some leases.
Mentions the environmental groups seeing it as a partial win as they managed to stop some development and gained protections for other areas.
Analyzes the political and economic realities behind the decision, mentioning the need to keep the economy strong and gas prices down.
Expresses disappointment in the decision, feeling that it was an missed chance for the administration to lead in transitioning away from fossil fuels.
Criticizes the compromise made, suggesting that it could have leaned more towards environmental concerns.

Actions:

for climate activists, environmentalists, policy advocates,
Advocate for stronger environmental protections (suggested)
Support initiatives that lead to a transition away from fossil fuels (implied)
</details>
<details>
<summary>
2023-03-13: Let's talk about the new US military plan for the air.... (<a href="https://youtube.com/watch?v=s4gjtvxoDE0">watch</a> || <a href="/videos/2023/03/13/Lets_talk_about_the_new_US_military_plan_for_the_air">transcript &amp; editable summary</a>)

Beau talks about the potential future of the United States Air Force, including the use of pilotless aircraft as wingmen, aiming to enhance mission focus and reduce risks for pilots.

</summary>

"More dog fights, less people."
"The overall idea is to have these aircraft be capable enough to actually function as the wingman, but inexpensive enough to be something that can be lost."
"If the pilot is not at risk, they are less likely to make mistakes."

### AI summary (High error rate! Edit errors on video page)

Introduction to discussing the United States Air Force and its future.
Mention of a new concept that was once seen as science fiction but is now on the brink of implementation.
Possibility of purchasing around 1000 pilotless aircraft in the near future.
Description of these aircraft as wingmen that will fly alongside fighter planes.
Control of the drones either through AI, the fighter pilot, or from a distant observation aircraft.
Purpose of the concept is to assign riskier tasks to drones, allowing human pilots to focus on primary objectives.
Aim of these collaborative combat aircraft is to function effectively as wingmen while being cost-effective enough to be expendable.
Reduced risk for pilots leads to potentially fewer mistakes and better focus on mission objectives.
Plans for purchases to commence in the coming years, with a program named the Loyal Wingman program.
The development could impact military strategies and make engagements with rival nations less costly.

Actions:

for military enthusiasts, policymakers,
Research and stay informed about the developments in the United States Air Force's use of pilotless aircraft (implied).
</details>
<details>
<summary>
2023-03-13: Let's talk about protecting the environment on the high seas.... (<a href="https://youtube.com/watch?v=mUPpp8Wbusk">watch</a> || <a href="/videos/2023/03/13/Lets_talk_about_protecting_the_environment_on_the_high_seas">transcript &amp; editable summary</a>)

Beau introduces the UN's agreement on biodiversity protection in the ocean, aiming to combat overmining and overfishing while ensuring equitable resource distribution, but the key lies in effective implementation by nations.

</summary>

"It's one of those things we'll start hearing more and more about as countries weigh in."
"This is one of those situations where the people causing the problem have put together a group of people to fix the problem in a way that satisfies them."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of biodiversity beyond national jurisdiction focusing on the ocean.
Mentions the Biodiversity Beyond National Jurisdiction Agreement at the UN.
The goal is to protect oceans outside national jurisdiction from seabed to airspace.
Aims to combat overmining and overfishing while protecting unique ecosystems.
Will aid in migration and distribute resources equitably.
The high seas cover about half the planet and are largely unmanaged.
Concerns lie in implementation and whether countries will uphold commitments.
Past environmental agreements have faced challenges in implementation.
Despite skepticism, this agreement is seen as a move in the right direction.
No other agreement or treaty compares to this one.
Emphasizes the role of governments in addressing ocean protection.
Acknowledges that those causing the problem are now involved in solving it.
Optimism exists regarding the potential impact of this agreement.
Encourages staying informed and hopeful about the progress of the agreement.

Actions:

for environmental activists, policymakers,
Keep updated on the progress of the Biodiversity Beyond National Jurisdiction Agreement at the UN (suggested).
</details>
<details>
<summary>
2023-03-13: Let's talk about Pence, history, Trump, and strategy.... (<a href="https://youtube.com/watch?v=VkoCapTA_xc">watch</a> || <a href="/videos/2023/03/13/Lets_talk_about_Pence_history_Trump_and_strategy">transcript &amp; editable summary</a>)

Former Vice President Pence condemns Trump's actions, signaling the Republican Party's slow distancing strategy from the former president, balancing fear of backlash and internal conflicts over authoritarian candidates post-Trump era.

</summary>

"I had no right to overturn the election and his reckless words endangered my family and everyone at the Capitol that day."
"The Republican Party as a whole fears Trump's base and they loathe Trump."
"Seeing Pence start to push that out there in what many see as his first moves towards his own presidential run."

### AI summary (High error rate! Edit errors on video page)

Former Vice President Pence acknowledged he had no right to overturn the election and condemned Trump's actions that endangered lives at the Capitol.
The Republican Party is slowly distancing itself from former President Trump, fearing his base's backlash.
High-level Republicans, including Pence, are gradually making more condemning statements about Trump to shift blame onto him.
Pence's actions are seen as his first moves towards his own presidential run, indicating an alliance with moderates despite his conservative stance.
The Republican establishment is trying to balance distancing from Trump without angering his base before the primary season.
Concerns arise about whether the party will back another authoritarian candidate similar to Trump or move towards traditional conservative values.
The establishment needs to address supporting candidates with authoritarian tendencies within the party post-Trump era.

Actions:

for political observers, republican voters,
Monitor high-level Republican statements and actions regarding former President Trump to understand the party's direction (implied).
Engage in political discourse and activism to push for traditional conservative values within the Republican Party (implied).
</details>
<details>
<summary>
2023-03-13: Let's talk about California's weather.... (<a href="https://youtube.com/watch?v=zbSATMtwBOE">watch</a> || <a href="/videos/2023/03/13/Lets_talk_about_California_s_weather">transcript &amp; editable summary</a>)

Another atmospheric river threatens central and northern California with flooding, urging 15 million under flood watch to stay informed and prepared for potential evacuation.

</summary>

"Your absolute best defense, the thing you need the most, is information."
"If you go into your attic to get away from rising water, please take something to make a hole in your roof so you can get out if you need to."

### AI summary (High error rate! Edit errors on video page)

Another atmospheric river is expected to bring more rain to central and northern California, where flooding, road closures, and damages are already present.
Approximately 15 million people are under flood watch, with some areas facing mandatory evacuation.
Staying informed through local emergency services and news updates is vital during this weather event.
Up to six inches of additional water, on top of existing rainfall and melting snowpack, could exacerbate the situation.
Accessing accurate information is key to making informed decisions and responding effectively.
Isolated individuals must wait for emergency services to reach them and should not worsen the situation by requiring additional rescues.
Being proactive in gathering and processing information can help in making the right choices for safety.
Residents in potentially impacted areas should remain updated with the latest news and alerts.
Those near rivers or unfamiliar with flooding should have a plan in place, including a means of escape if seeking refuge in an attic.
Providing a way to exit from an attic during flooding is emphasized for safety and preparedness.

Actions:

for california residents,
Stay informed through local emergency services and news updates (suggested)
Prepare for potential evacuation if in impacted areas (suggested)
Ensure you have a means of escape if seeking shelter in an attic during flooding (suggested)
</details>
<details>
<summary>
2023-03-12: Let's talk about what's next for Artemis.... (<a href="https://youtube.com/watch?v=_wvUOqlWVJQ">watch</a> || <a href="/videos/2023/03/12/Lets_talk_about_what_s_next_for_Artemis">transcript &amp; editable summary</a>)

Beau explains NASA's Artemis missions, from uncrewed tests to returning humanity to the moon, paving the way for off-world projects and accelerated exploration.

</summary>

"Humanity, for better or worse, is a species that likes to explore."
"The goal of this is to set up the technology, set up the equipment, get things rolling."
"Once it starts in earnest, it's going to happen at a faster and faster rate."

### AI summary (High error rate! Edit errors on video page)

Explains the Artemis missions by NASA, with Artemis 3 aiming to return humanity to the moon.
Artemis 2, scheduled to depart around Thanksgiving next year, will be crewed and orbit the moon, not land.
NASA encountered minor issues during Artemis 1, particularly with the heat shield on the Orion spacecraft.
Addresses misconceptions about the heat shield needing to look charred and ongoing concerns about its performance.
Despite concerns, the timeline for Artemis 2 is not expected to be affected.
Artemis 3 is anticipated a year to a year and a half after Artemis 2, aiming to establish an off-world post for long-term manned projects.
Emphasizes that the goal is not just to gather rocks but to set up technology for sustained human presence beyond Earth.
Points out the positive aspect of humanity's exploration drive and the potential for accelerated progress once initiatives like Artemis take off.
Beau concludes by reflecting on the excitement and profound impact of humanity's venture into space.

Actions:

for space enthusiasts,
Support space exploration initiatives by staying informed and advocating for continued funding and research (implied).
Encourage interest in science and technology education to inspire future generations of space explorers (implied).
</details>
<details>
<summary>
2023-03-12: Let's talk about Ukraine, shortages, refusals, and shovels.... (<a href="https://youtube.com/watch?v=GSXxz2Dikl8">watch</a> || <a href="/videos/2023/03/12/Lets_talk_about_Ukraine_shortages_refusals_and_shovels">transcript &amp; editable summary</a>)

Russian military's ammo shortages and outdated equipment lead to discontent among troops, stalling their offensive in Ukraine.

</summary>

"The lack of real tactical maneuvering and the lack of equipment is taking its toll on the newer Russian troops."
"It's waste. It's absolute waste."
"Russia's renewed offensive has stalled in a way that is creating widespread discontent among the mobilized troops."
"It has devolved into World War one with drones."

### AI summary (High error rate! Edit errors on video page)

Russian military faces shortages of regular ammo like small arms ammo, affecting their offensive in Ukraine.
Troops are receiving outdated equipment and morale is suffering, with some refusing to follow orders.
Videos show Russian troops expressing discontent and even surrendering voluntarily to Ukrainian forces.
Russia's renewed offensive in Ukraine has stalled, leading to widespread discontent among mobilized troops.
Uncertainty surrounds the situation around Bakhmut, with conflicting reports on Ukraine's defense and Russia's equipment capabilities.

Actions:

for world leaders, policymakers,
Support Ukrainian forces with supplies and resources (implied)
Stay informed on the situation in Ukraine and advocate for diplomatic solutions (suggested)
</details>
<details>
<summary>
2023-03-12: Let's talk about TikTok and legislation.... (<a href="https://youtube.com/watch?v=2zsOjd24Sds">watch</a> || <a href="/videos/2023/03/12/Lets_talk_about_TikTok_and_legislation">transcript &amp; editable summary</a>)

The U.S. government aims to address TikTok concerns through the RESTRICT Act, granting the Commerce Department power to regulate technologies and potentially impacting TikTok's future operations.

</summary>

"The idea is to give the Commerce Department the capability to regulate and do anything up to including ban technologies from foreign countries that may pose a security risk."
"This is a way to implement unpopular decisions."
"It's probably something that is going to go through so it doesn't look good for TikTok in the long term."
"Once Commerce, the Department of Commerce actually has the authority, it's no longer in Congress's hands."
"Odds are Biden will sign it."

### AI summary (High error rate! Edit errors on video page)

Explains how the U.S. government is addressing concerns about TikTok through the RESTRICT Act.
Congress and the White House are creating new powers within the Commerce Department to regulate technologies from foreign countries that pose security risks.
The mechanism is designed to avoid directly targeting TikTok to prevent political backlash.
The Commerce Department will be granted the authority to make decisions independently to shield elected officials from scrutiny.
This approach mirrors how unpopular decisions are handled, like military base closures, through committees to avoid upsetting the public.
Acknowledges that tech experts have raised concerns about TikTok's data security issues, which could potentially lead to the development of user profiles.
The RESTRICT Act has significant support and is likely to pass, signaling potential trouble for TikTok in the future.
Once the Commerce Department gains authority, they may impose restrictions on TikTok, affecting its future operations.

Actions:

for tech users, policymakers,
Monitor updates on the RESTRICT Act and its implications for TikTok (suggested).
Stay informed about potential regulatory changes affecting technology platforms (implied).
</details>
<details>
<summary>
2023-03-12: Let's talk about DOJ looking at specialized police teams.... (<a href="https://youtube.com/watch?v=BhEPtFbN9oE">watch</a> || <a href="/videos/2023/03/12/Lets_talk_about_DOJ_looking_at_specialized_police_teams">transcript &amp; editable summary</a>)

The US Department of Justice's upcoming "paper" investigation on specialized law enforcement teams aims to gauge effectiveness, potentially leading to defunding these teams due to high misconduct rates, rather than immediate arrests for cops engaged in misconduct.

</summary>

"The risk of losing the funding will do more to end those programs than all of the body cam footage in the world."
"This is a way to stop the problem rather than engaging with those who have already overstepped."
"The more likely outcome is that two years from now that team doesn't have any funding."

### AI summary (High error rate! Edit errors on video page)

US Department of Justice announced a upcoming "historical" and "current" **"paper"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"** **"**"**"**"**"**"**"**" **"**"**"**"**"**"**"** **"**"**"**"**"**"** **"**"**"**"**"**" investigation of specialized law enforcement teams, not an investigation.
Plainclothes teams with little oversight historically lead to misconduct, prompting a Department of Justice "paper" **"**" investigation on their actions.
The Department of Justice is expected to compare the actions of these specialized teams to the normal rate of incident among regular law enforcement and determine effectiveness and misconduct rates.
The purpose of this "paper" investigation is likely to change how funding is allocated, potentially resulting in defunding these teams due to findings of ineffectiveness and high misconduct rates.
The risk of losing funding is more likely to end these programs than body cam footage or past misconduct.
This "paper" investigation may be a method for the Civil Rights Division to address the issue without individually investigating multiple departments.
Beau believes that this investigation is a way to stop future misconduct rather than address past wrongdoings.
The long-term benefit of this investigation is the potential elimination of historically problematic law enforcement teams.
Beau suggests that people should not expect immediate arrests of cops in their city due to this investigation; the more likely outcome is a loss of funding for these teams.
The investigation aims to gauge effectiveness versus misconduct to determine if these specialized teams are truly worth the funding.
Beau acknowledges that while this is a step towards addressing issues, it may not lead to immediate consequences for cops engaged in misconduct.

Actions:

for policy advocates,
Contact policy advocacy organizations for updates and actions related to law enforcement reforms (implied)
Join community forums or campaigns advocating for police accountability and effective allocation of funding (implied)
</details>
<details>
<summary>
2023-03-11: Let's talk about dreams and rumors of a moderate GOP.... (<a href="https://youtube.com/watch?v=nYZI1abpoRI">watch</a> || <a href="/videos/2023/03/11/Lets_talk_about_dreams_and_rumors_of_a_moderate_GOP">transcript &amp; editable summary</a>)

Larry Hogan's decision not to run for the GOP nomination may pave the way for an organized effort to counter authoritarian elements within the party, potentially through a third-party candidacy aiming to deny them the presidency with just a few critical votes.

</summary>

"Those who are opposed to the more authoritarian elements within the Republican Party certainly appear to be a whole lot more organized this time."
"He just has to get enough to tip it, and with as polarized as things are, there's a pretty good chance that in some races, him catching half a percent to a percent and half, that might decide the outcome."

### AI summary (High error rate! Edit errors on video page)

Larry Hogan, a preferred moderate GOP candidate, decided not to run for the presidential nomination.
Rumors suggest other candidates will unite behind an unannounced candidate to counter authoritarian elements in the Republican Party.
This strategy aims to consolidate support among moderate Republicans and prevent authoritarian votes from being split during primaries.
Hogan might run as an independent if the strategy fails, specifically to stop a Trump-like candidate.
Hogan's potential third-party candidacy could make a difference by denying the authoritarian element the presidency with just a few points.
The organized opposition to authoritarian elements within the Republican Party seems more prepared this time, with plans and contingencies in place.
The public-facing information indicates a significant number of committed Republicans aiming to steer the party back towards its original ideals.
Despite being based on rumors, there is substantial support for the idea of returning the Republican party to its traditional conservatism.

Actions:

for political activists, republican voters,
Unite behind candidates who aim to steer the Republican Party back to its traditional conservatism (implied)
Support moderate Republicans and independent candidates who oppose authoritarian elements within the party (implied)
</details>
<details>
<summary>
2023-03-11: Let's talk about average temperature changes in the US.... (<a href="https://youtube.com/watch?v=CDAKsvEMUJY">watch</a> || <a href="/videos/2023/03/11/Lets_talk_about_average_temperature_changes_in_the_US">transcript &amp; editable summary</a>)

Average temperatures in the United States are rising, impacting agriculture and water resources, requiring a transition away from fossil fuels for a sustainable future.

</summary>

"It is occurring, and it's going to have impacts."
"If you want your kids, your grandkids, to have some semblance of the society that we enjoy, these changes have to occur."
"The transition away from fossil fuels, away from dirty energy, they're going to have to occur and they will occur one way or another."

### AI summary (High error rate! Edit errors on video page)

Average winter temperature in the United States has risen by 3.7 degrees Fahrenheit from 1970 to 2023.
The average summer temperature has risen by 2.3 degrees Fahrenheit, impacting about 80% of the country.
This warming trend is noticeably affecting agriculture, as some crops that need chill time to grow may not thrive.
The impacts of climate change extend beyond temperature changes, affecting water resources as well.
Warmer winters may lead to reduced snowpack, impacting freshwater sources.
There is a reluctance to make necessary changes to address climate change.
Many regions that will be severely affected by climate change are not in the United States and have contributed less to the issue.
Climate change will have global impacts, including on the US, through factors like water scarcity and food production.
The transition away from fossil fuels and dirty energy is inevitable and must happen for the well-being of future generations.
Leaders who prioritize the security and well-being of future citizens are needed to drive these necessary changes.

Actions:

for climate activists, policymakers, citizens,
Advocate for leaders who prioritize sustainable energy transitions and climate action (implied)
Support policies and initiatives that address climate change impacts on agriculture and water resources (implied)
</details>
<details>
<summary>
2023-03-11: Let's talk about Senators vs the Intelligence Community.... (<a href="https://youtube.com/watch?v=VwTozeVcxB4">watch</a> || <a href="/videos/2023/03/11/Lets_talk_about_Senators_vs_the_Intelligence_Community">transcript &amp; editable summary</a>)

Contest between branches over access to recovered documents prompts calls for more oversight on handling classified material by elected officials.

</summary>

"The mishandling of classified material is something that definitely extends beyond the executive branch."
"There is a major issue when it comes to the elected officials in both the legislative and the executive branches and their handling of classified material."
"Everybody going into Congress, everybody going into the Senate, into the House, into the executive branch really should probably get a whole lot more training."

### AI summary (High error rate! Edit errors on video page)

Contest between legislative and executive branches over access to recovered documents from Biden, Pence, and Trump in various locations.
Senate intel leaders want to see the documents for oversight, but intelligence community is hesitant to share classified information.
Senate committee may not need to see actual documents, a readout or summary could suffice.
Concerns about mishandling of classified material extend beyond the executive branch.
Intelligence community worried about leaks and limiting access to documents involved in active investigations.
Beau suggests a compromise can be worked out easily.
Importance of understanding classification markings to limit access to sensitive information.
Beau questions if Senate Intelligence Committee actually needs to see the real documents.
Calls for more oversight on handling classified material by elected officials.
Tension building around bills reauthorization that benefit intelligence community due to access to documents being used as leverage by politicians.

Actions:

for legislative staff, oversight advocates,
Advocate for increased oversight on handling classified material by elected officials (implied).
</details>
<details>
<summary>
2023-03-11: Let's talk about North Korea, perspective, and two projections.... (<a href="https://youtube.com/watch?v=4-XS85yUPSk">watch</a> || <a href="/videos/2023/03/11/Lets_talk_about_North_Korea_perspective_and_two_projections">transcript &amp; editable summary</a>)

Beau explains the situation in North Korea from American and North Korean perspectives, shedding light on their motivations and the importance of understanding foreign policy decisions through the lens of the country making them.

</summary>

"Every country pursues their own national interests and for North Korea, they're very much on their own."
"It's really important to try to view foreign policy decisions through the lens of the country making them."
"That's the situation they're in."
"But believe me, if the U.S. was entering a period of food insecurity and China had recently said they wanted to realign the government, people in the U.S. would be worried."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the situation in North Korea from two different perspectives: American and North Korean.
Talks about North Korea facing severe food insecurity and the need for international assistance.
Mentions US intelligence suggesting a possible nuclear test by North Korea.
Compares the situation to if the US was in a weakened state and facing a potential invasion.
Points out that North Korea's actions are driven by their own interests and perspective.
Raises questions about the timing and motivations behind North Korea's potential nuclear test.
Acknowledges the concerns about North Korea's nuclear weapons program and the international response.
Emphasizes the importance of understanding foreign policy decisions through the lens of the country making them.
Suggests various possibilities regarding the motivations behind North Korea's actions.
Concludes by urging to view North Korea's actions through their own perspective and national interests.

Actions:

for foreign policy analysts,
Understand foreign policy decisions through the lens of the country making them (suggested)
Question US intelligence reports and analysis (suggested)
</details>
<details>
<summary>
2023-03-10: Let's talk about an enduring math problem in Ukraine.... (<a href="https://youtube.com/watch?v=LicT6OA64R4">watch</a> || <a href="/videos/2023/03/10/Lets_talk_about_an_enduring_math_problem_in_Ukraine">transcript &amp; editable summary</a>)

Beau sheds light on inflatable decoys confusing Russia in Ukraine, unraveling the High Mars mystery.

</summary>

"Russia might not have been lying that they very well may have hit these targets that they believed were HIMARS, but they were the inflatables."
"It is now confirmed that not just are they being used, there are HIMARS versions that are being used in Ukraine right now."
"Our most likely answer at this point to this, you know, as of yet unsolved math problem that Russia has blown up a whole bunch of bouncy house highmars."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of an unsolved mathematical problem in Ukraine and a new piece of information related to it.
Mentions the High Mars vehicle system provided by the West to Ukraine, which has been effective against Russian forces.
Explains the discrepancy in the number of High Mars systems sent versus the number destroyed by Russia.
Reveals that a Czech company started producing inflatable versions of military vehicles, including High Mars.
Suggests that Russia may have destroyed inflatable decoys, thinking they were actual High Mars systems.
Points out that the use of inflatable decoys is cost-effective and creates confusion among opposition forces.
Concludes that the situation clarifies why the numbers reported by both sides seemed exaggerated and implausible.

Actions:

for military analysts, ukraine supporters.,
Verify information on military equipment before making claims (implied).
</details>
<details>
<summary>
2023-03-10: Let's talk about Valentine's Day 2046 and asteroids.... (<a href="https://youtube.com/watch?v=ijtenqpKpHs">watch</a> || <a href="/videos/2023/03/10/Lets_talk_about_Valentine_s_Day_2046_and_asteroids">transcript &amp; editable summary</a>)

An asteroid near Earth in 2046 raises concerns, but NASA's preparedness and research alleviate potential impacts and underscore the importance of planetary defense funding.

</summary>

"This is something that could definitely cause major localized issues."
"Asteroids have hit the Earth in the past. They're going to hit in the future unless they're stopped."
"NASA gets answers through research, conducts research with funding."

### AI summary (High error rate! Edit errors on video page)

An asteroid near Earth in 2046 is causing a buzz due to its size and potential impact.
The asteroid, measuring 162 feet, has a small chance of impacting Earth according to the Torino scale.
The current chance of impact is around one quarter of 1%, making it highly unlikely.
NASA analysts believe the public shouldn't be concerned about this asteroid.
NASA conducted the Double Asteroid Redirection Test (DART) in 2022 to prepare for scenarios like this.
With 20 years warning, NASA could potentially develop a plan to redirect the asteroid if the odds increase.
Despite the low chances of impact, the asteroid's size is large enough to cause major localized issues.
DART was NASA's first attempt at planetary defense, showing the importance of investing in such technologies.
Politicians and policymakers should recognize NASA's role as a defense line against potential asteroid impacts.
NASA's budget should be considered vital for research and development to address planetary defense needs.

Actions:

for space enthusiasts, policymakers, concerned citizens,
Support funding for NASA's planetary defense research and technologies (implied).
</details>
<details>
<summary>
2023-03-10: Let's talk about Minnesota making moves.... (<a href="https://youtube.com/watch?v=IPQ3xJLB7PA">watch</a> || <a href="/videos/2023/03/10/Lets_talk_about_Minnesota_making_moves">transcript &amp; editable summary</a>)

Governor Tim Walz's executive order in Minnesota protects the LGBTQ community from harmful legislation, setting a precedent for other states to follow and potentially leading to economic growth and legal challenges.

</summary>

"Minnesota is going to be a place where people from the LGBTQ community can go and be okay."
"A lot of this legislation is moving forward. This legislation, it's not actually designed to do anything inside the state."
"So the governor of Minnesota has decided that Minnesota is a free state."
"This isn't a PR stunt in that sense. This is the state government of Minnesota saying, oh, your extradition request? It's not valid here."
"The governor of Minnesota stepped up and put this into action when it really needed to be."

### AI summary (High error rate! Edit errors on video page)

Explains Governor Tim Walz's executive order in Minnesota aimed at protecting the LGBTQ community from harmful legislation.
Mentions how the legislation in various states targets and marginalizes the LGBTQ community to motivate certain groups to vote.
Describes the impact of the executive order in Minnesota on preventing the state government from assisting in enforcing harmful laws targeting the LGBTQ community.
Speculates on the reasoning behind the governor's choice to use an executive order instead of waiting for pending legislation.
Talks about potential long-term impacts of the executive order, including economic development and possible Supreme Court challenges.
Suggests that other states might follow Minnesota's lead in protecting the LGBTQ community.
Points out the potential legal challenges to enforcement mechanisms of harmful legislation under the Interstate Commerce Clause.
Appreciates the governor's proactive stance in implementing the executive order to protect LGBTQ individuals.

Actions:

for advocates, legislators, activists,
Contact LGBTQ advocacy organizations to support and amplify their efforts (implied)
Join local LGBTQ rights groups to stay informed and engaged in similar initiatives (implied)
Attend community meetings or town halls to voice support for LGBTQ protections (implied)
</details>
<details>
<summary>
2023-03-10: Let's talk about 3 debt ceiling scenarios.... (<a href="https://youtube.com/watch?v=YB3Dg3CGZZg">watch</a> || <a href="/videos/2023/03/10/Lets_talk_about_3_debt_ceiling_scenarios">transcript &amp; editable summary</a>)

Beau explains three scenarios regarding the debt ceiling: default consequences, Republican spending cuts impact, and the detrimental effects on the US economy.

</summary>

"A short default, you're looking at losing a million jobs in a mild recession."
"Republican solution as far as the spending cuts are actually more devastating to the US economy than a default."
"I don't think this is putting America first."

### AI summary (High error rate! Edit errors on video page)

Explains three scenarios examined by Moody's Analytics regarding the debt ceiling.
Scenario 1: If the United States defaults on its loans, a short default will trigger a mild recession leading to a million job losses. A longer default will result in 7 million job losses and a $10 trillion drop in household wealth.
Scenario 2: If Republicans push for spending cuts, it will lead to a recession, 2.6 million job losses, and a 6% unemployment rate by 2024.
Points out that the Republican Party's proposed spending cuts are more harmful to the US economy than a default.
Condemns the Republican Party's actions as detrimental to the American working class and an attempt to blame Biden for a recession in 2024.

Actions:

for voters, concerned citizens,
Contact your representatives to voice opposition to harmful spending cuts proposed by the Republican Party (implied).
Support policies that prioritize economic stability and job growth for the American working class (implied).
</details>
<details>
<summary>
2023-03-09: Let's talk about the new Nord Stream reporting.... (<a href="https://youtube.com/watch?v=t_8fqb6ClB4">watch</a> || <a href="/videos/2023/03/09/Lets_talk_about_the_new_Nord_Stream_reporting">transcript &amp; editable summary</a>)

Beau delves into theories surrounding the Nord Stream pipeline incident, expressing skepticism about the sole involvement of Ukrainian non-state actors and cautioning against potential spin on evidence by various sides.

</summary>

"I find it hard to believe that they are non-state actors that just did it on their own without a company backing them."
"A lot of these elites are coming from intelligence officers."
"This seems to be following physical evidence, which is nice, rather than creating a theory and then trying to find the evidence to match the theory."

### AI summary (High error rate! Edit errors on video page)

Exploring theories about the Nord Stream pipeline incident involving possibly Ukrainian non-state actors.
Questions the plausibility of the perpetrators being solely non-state actors without any backing.
Mentions that the unidentified individuals involved are referred to as Destro, Zartan, Baroness, Firefly, Storm Shadow, and Cobra Commander.
Suggests skepticism about the theory that the perpetrators were completely independent non-state actors.
Countries' official responses: Germany urges patience for investigations, the US has no official comment, Russia denies involvement, and Ukraine denies knowledge or connection.
Lack of consensus in the international community on the incident's perpetrators.
Cautioning about potential spin on concrete evidence by various sides for geopolitical gains.
Elite officials potentially shaping narratives based on their backgrounds in intelligence.
Encourages following the investigation for a possible disputed but more concrete answer.
Not fully taking a stance due to limited evidence but finds this particular theory more intriguing for its reliance on physical evidence.

Actions:

for observers, analysts, researchers,
Contact relevant authorities for updates on the investigation (suggested)
Follow reputable sources for reliable information on the incident (suggested)
</details>
<details>
<summary>
2023-03-09: Let's talk about Louisville and the DOJ.... (<a href="https://youtube.com/watch?v=50FJ4xKq_ow">watch</a> || <a href="/videos/2023/03/09/Lets_talk_about_Louisville_and_the_DOJ">transcript &amp; editable summary</a>)

Louisville agrees to DOJ recommendations for law enforcement reform after rights violations, signaling the start of serious change with Memphis next in line.

</summary>

"This is a start. And it shouldn't be ignored because it's not the perfect solution."
"It's a move in the right direction."
"Those are kind of the two extremes when it comes to the outcomes."
"This is a case where the bad apples spoiled the bunch."
"It's good news, but it's not the end of the story."

### AI summary (High error rate! Edit errors on video page)

Louisville agreed to make corrections to their law enforcement based on DOJ Civil Rights Division recommendations.
The DOJ found a culture within the department violating rights through invalid warrants, excessive force, and discriminatory behavior.
Recommendations include constant body camera footage reviews, more oversight, and outside assistance for decision-making.
The hope is for reforms, retraining or termination of individuals, and implementation of all recommendations.
The Department of Justice can step in if reforms are not made, with extreme measures like taking over the department.
Similar agreements have taken place in other cities like Ferguson, with Memphis next in line.
The catalyst for these actions was Breonna Taylor's death, revealing a pattern of rights violations.
This process aims for serious reform if acted upon in good faith, otherwise risking departmental overhaul.
It signifies the start of change in law enforcement culture, aiming to address major issues.
Despite not being a perfect solution, it is a step towards reform and has the potential to save lives.

Actions:

for community members, activists,
Support and monitor the implementation of recommended reforms by law enforcement in Louisville and upcoming in Memphis (implied)
Advocate for continued oversight and accountability in law enforcement practices (implied)
</details>
<details>
<summary>
2023-03-09: Let's talk about Biden's budget proposal.... (<a href="https://youtube.com/watch?v=efZt3fW2bs4">watch</a> || <a href="/videos/2023/03/09/Lets_talk_about_Biden_s_budget_proposal">transcript &amp; editable summary</a>)

Analyzing Biden's budget proposal, daring Republicans to present a better plan, and banking on public reception while revealing strategic political maneuvering.

</summary>

"It's a show. It's one of these moments where Biden is literally saying, okay, here are my cards, let's see yours."
"It's a political maneuver to get the Republican Party to show their work."
"Everything's a show. It's just something to entertain their less informed base."

### AI summary (High error rate! Edit errors on video page)

Analyzing Biden's budget proposal and its components, including reducing the deficit by 3 trillion over 10 years.
Increasing taxes on the wealthiest Americans and quadrupling taxes on corporate stock buybacks are key measures.
Making Medicare solvent through 2050 by raising contributions from top earners from 3.8 percent to 5 percent.
Not raising taxes on individuals making less than around $400,000 a year is a significant aspect.
Biden's budget proposal also includes more price negotiation for pharmaceuticals and similar measures.
The budget proposal is a dare and comparison by the Biden administration to challenge Republicans and let the American people decide.
Congress controls the budget, not the executive branch, making the proposal more of a statement than a definitive plan.
Republicans are in a tough spot as they may struggle to match the numbers needed to counter Biden's proposal.
The debt ceiling debate and related brinksmanship may be rendered pointless if a balanced budget is not achievable.
Biden's move is strategic, forcing Republicans to reveal their plans and math to the public.

Actions:

for political analysts, policymakers,
Contact your representatives to express your views on budget proposals and fiscal policies (implied).
</details>
<details>
<summary>
2023-03-09: Let's talk about Biden's billionaire tax plan.... (<a href="https://youtube.com/watch?v=YsJPwJkSj2o">watch</a> || <a href="/videos/2023/03/09/Lets_talk_about_Biden_s_billionaire_tax_plan">transcript &amp; editable summary</a>)

Beau provides insights on the potential billionaire tax proposal in Biden's budget, facing challenges from wealthy interests and needing bipartisan support for approval.

</summary>

"It's being suggested that the Biden administration is going to suggest a billionaires tax."
"The idea that a bunch of billionaires are going to allow a billionaire tax to go through seems pretty slim."
"You're talking about people who are, well, billionaires. They have a lot of money, they have a lot of influence."
"The only way to overcome that would be sheer numbers."
"It's definitely setting the tone for an economic debate and it is casting the Biden administration in a more progressive light."

### AI summary (High error rate! Edit errors on video page)

Provides an update on additional information following his last video on Biden's budget.
Mentions a potential proposal by the Biden administration for a minimum tax rate on income for billionaires at 25%.
Acknowledges the pushback expected from billionaires and their allies in Congress against this proposal.
Emphasizes the need for Congress to approve such proposals, not just the president.
Expresses skepticism about the likelihood of the billionaire tax proposal passing due to the influence of wealthy interests in Congress.
Suggests that bipartisan support from average voters might be necessary to push such a proposal through.
Notes the symbolic nature of the proposal in setting the tone for economic debates and portraying the Biden administration as progressive.
Concludes by mentioning this proposal as part of Biden's economic plan that will likely receive significant attention and debate.

Actions:

for policy analysts,
Contact your representatives to express support for the billionaire tax proposal (suggested).
</details>
<details>
<summary>
2023-03-08: Let's talk about sore loser laws and Trump.... (<a href="https://youtube.com/watch?v=CCpQxxBBDgc">watch</a> || <a href="/videos/2023/03/08/Lets_talk_about_sore_loser_laws_and_Trump">transcript &amp; editable summary</a>)

Republicans misunderstand Trump's intentions, believing sore loser laws will prevent him from running independently, but Trump's potential third-party candidacy aims to fundraise and send a message, not necessarily win.

</summary>

"Sore loser laws, keeping them off the ballot through various means, this is not going to work."
"Frankenstein has lost control of his monster. That's what's happened."
"He already doesn't stand a good chance of winning."

### AI summary (High error rate! Edit errors on video page)

Republicans are comforted by sore loser laws, mistakenly believing they will prevent Trump from running as a third-party candidate if he loses the Republican nomination.
Trump may run as a third-party candidate not to win but to fundraise, send a message to the Republican Party, and punish those he considers disloyal.
Keeping Trump off the ballot won't stop him from running independently and portraying the Republican Party as disloyal.
Trump's potential third-party run aims to establish a Trump legacy or set up another run, not necessarily to win the presidency.
Republicans misunderstand Trump's motivations and view him as a normal politician despite his unconventional approach.
Sore loser laws won't deter Trump from running independently, as his decision is not solely based on winning chances.
Trump will likely fundraise off legal challenges if attempts are made to keep him off the ballot.

Actions:

for political analysts,
Recognize the potential impact of Trump's independent run and strategize accordingly (implied)
</details>
<details>
<summary>
2023-03-08: Let's talk about Fox quotes to reach people.... (<a href="https://youtube.com/watch?v=lJMqR1QcIq8">watch</a> || <a href="/videos/2023/03/08/Lets_talk_about_Fox_quotes_to_reach_people">transcript &amp; editable summary</a>)

Beau urges viewers to pay attention to revealing quotes from a defamation case involving Fox News personalities, potentially leading to a shift in perspectives and feelings of betrayal among consumers.

</summary>

"It is worth noting at this point, they're filings. But it would be exceedingly rare for people to just make stuff up like this in a filing."
"This is a case for your Trump-loving uncle, your relative who gets their news from Facebook, who believes the memes."
"I'm willing to bet that most people who consume Fox News they are not going to like what they see and they're going to feel tricked."

### AI summary (High error rate! Edit errors on video page)

Urges viewers to pay attention to quotes related to a defamation case, particularly from Fox News personalities.
Emphasizes the impact of the revealing quotes on potentially changing people's perspectives.
Questions the disparity between public and private opinions of Fox News hosts.
Suggests that the new information might provide a reason for misinformed individuals to re-evaluate their beliefs.
Encourages engaging loved ones who consume Fox News with the information from the case.
Points out the potential manipulation by media outlets like Fox News for ratings and control.
Acknowledges the challenge of getting Fox News viewers interested in this new information.
Indicates that the content of the case might lead to feelings of betrayal among Fox News consumers.
Raises concerns about the education and information spread by certain media platforms.
Conveys the rarity of viewers seeing behind the scenes and potentially feeling deceived.

Actions:

for fox news viewers,
Engage loved ones who consume Fox News with the quotes and information from the defamation case (implied).
Encourage misinformed individuals to re-evaluate their beliefs based on the new information (implied).
Find alternative ways to share the information with Fox News viewers, as mainstream coverage may be lacking (implied).
</details>
<details>
<summary>
2023-03-08: Let's talk about CPAC polling numbers.... (<a href="https://youtube.com/watch?v=e2T66hiAZIM">watch</a> || <a href="/videos/2023/03/08/Lets_talk_about_CPAC_polling_numbers">transcript &amp; editable summary</a>)

Trump's poll numbers at CPAC may not be as strong as they seem, early polling isn't indicative of final outcomes, and the political landscape will shift significantly before the election.

</summary>

"62% for Trump at an event where he was kind of the only big name, those are bad numbers, not good numbers."
"Sure, it looks like a win, but it's not."
"A whole lot of stuff is going to happen between now and then."
"It's very early to start looking at polling."
"There's still a lot of time between now and then."

### AI summary (High error rate! Edit errors on video page)

Trump's 62% poll numbers at CPAC aren't as strong as they seem because he was the main attraction and many other candidates didn't show up.
CPAC's heavy focus on Trump and MAGA has made it less relevant, with many candidates skipping the event.
Early poll numbers are not indicative of the final outcome, citing Jeb Bush's lead in 2015 before the 2016 primary.
The political landscape will shift significantly before the actual election, with new candidates rising and falling based on policy ideas and national appeal.
Trump's repetitive grievances and lackluster efforts may lead to different candidates gaining momentum as the election approaches.

Actions:

for political observers,
Stay informed on the changing political landscape and be prepared for shifts in candidates (implied).
Monitor policy ideas and changes in candidate standings leading up to the election (implied).
Avoid making premature determinations based on early polling numbers (implied).
</details>
<details>
<summary>
2023-03-08: Let's talk about Biden, Bison, and a question.... (<a href="https://youtube.com/watch?v=hgwh_2dE2b0">watch</a> || <a href="/videos/2023/03/08/Lets_talk_about_Biden_Bison_and_a_question">transcript &amp; editable summary</a>)

The Inflation Reduction Act supports tribal-led efforts to restore bison populations, likely leading to positive environmental impacts and a win in the long run.

</summary>

"Tribal governments will do a better job at protecting the bison than the US government overall."
"This is going to be a win."
"It's just getting started, it's too early to call it that."

### AI summary (High error rate! Edit errors on video page)

The Inflation Reduction Act includes funds for the environment, specifically to lift bison populations.
The Secretary of the Interior plans to transfer bison from federal to tribal land for tribal leadership in population restoration.
Bison were once nearly extinct due to hunting, but their numbers are now in the tens of thousands.
Bison are a keystone species and can positively impact ecosystems as they move and multiply.
Tribal governments are expected to play a key role in protecting bison populations better than the US government historically has.
Tribal governments are more invested economically and environmentally in ensuring positive outcomes.
The Inflation Reduction Act's initiative involving bison is predicted to have a positive impact in the long run.
Beau expresses confidence in the program's success and believes it will be a significant win once fully realized.

Actions:

for environmental advocates, tribal communities,
Support tribal-led conservation efforts (implied)
Stay informed about environmental initiatives involving keystone species like bison (implied)
</details>
<details>
<summary>
2023-03-07: Let's talk about things to remember from 2016.... (<a href="https://youtube.com/watch?v=5unv6WZ6HRw">watch</a> || <a href="/videos/2023/03/07/Lets_talk_about_things_to_remember_from_2016">transcript &amp; editable summary</a>)

Lessons from 2016: Challenge extreme rhetoric by continuously questioning and exposing the true intentions behind seemingly toned-down statements.

</summary>

"Pretend like you don't get it."
"If you can't be trusted with the First Amendment, why should we trust you with the Second?"
"People don't want anything to do with it."

### AI summary (High error rate! Edit errors on video page)

Reflections on lessons learned and moving forward from 2016, as similar dynamics are playing out in the lead-up to 2024.
Far-right targeting specific demographics through legislation, rhetoric, and actions, with rhetoric playing a critical role in legitimizing their agenda.
A technique used by the far-right is saying something extreme, then walking it back to a slightly less extreme position to make critics seem unreasonable.
Normalization of extreme positions through this technique, making it critical to challenge and question their rhetoric.
A strategy to deal with racist jokes or extreme rhetoric is to pretend not to understand, forcing the person to explain and reveal the true nature of their statement.
By questioning and getting details on extreme positions, support can be eroded as people become less interested and supportive.
Connecting extreme statements to potential violations of the Constitution can be a powerful way to challenge and counter their narrative.
Emphasizing the impact on constitutional rights, especially the First and Second Amendments, can help appeal to conservatives who value constitutional principles.
Continuously questioning and dragging out extreme rhetoric is key to exposing the true intentions behind seemingly toned-down statements.
The importance of pushing back, challenging, and unraveling extreme rhetoric to prevent its normalization and gain broader support against such tactics.

Actions:

for activists, progressives, concerned citizens,
Challenge extreme rhetoric by continuously questioning and exposing the true intentions (suggested)
Connect extreme statements to potential violations of the Constitution to counter the narrative (implied)
Appeal to conservatives by emphasizing the impact on constitutional rights, especially the First and Second Amendments (implied)
</details>
<details>
<summary>
2023-03-07: Let's talk about the other conservative conference.... (<a href="https://youtube.com/watch?v=htf2QAR-sa4">watch</a> || <a href="/videos/2023/03/07/Lets_talk_about_the_other_conservative_conference">transcript &amp; editable summary</a>)

Beau delves into the overlooked significance of the Never Trump Summit, showcasing a Republican faction against authoritarianism and foreseeing potential strange alliances in future elections.

</summary>

"Well, howdy there, internet people, it's Beau again."
"The Republican Party still hasn't learned its lesson and they're going to have to suffer more electoral loss."
"Most of them were happy about were all of the far-right candidates that lost in the midterms."
"For 2024, the best thing for the Republican Party is for it to lose."
"Get ready to see some strange alliances."

### AI summary (High error rate! Edit errors on video page)

Contrasts the attention on CPAC with the importance of a different summit for the Republican Party.
Notes that CPAC attendance has declined as it has transformed into an authoritarian party event.
Mentions the Never Trump Summit, where Republicans who are against Trump's brand of authoritarianism still gather.
Describes the general tone at the Never Trump Summit as one of disappointment in the Republican Party's failure to learn from electoral losses.
Points out that some Republicans believe it may be necessary for the party to lose again in order to return to normalcy.
Indicates a concern about the prevalent authoritarian streak within the Republican Party, even beyond Trump.
Anticipates strange alliances forming in the lead-up to the election, as anti-Trump Republicans may collaborate with Democrats.
Acknowledges the absence of many individuals at CPAC who may not have been able to attend the other summit but still avoided CPAC.

Actions:

for republicans, democrats,
Attend gatherings or events that foster collaboration between Republicans and Democrats (implied).
Voice opposition to authoritarianism within political parties by engaging in open discourse and activism (implied).
</details>
<details>
<summary>
2023-03-07: Let's talk about near peer defense spending projections.... (<a href="https://youtube.com/watch?v=6JPLiwLsMg4">watch</a> || <a href="/videos/2023/03/07/Lets_talk_about_near_peer_defense_spending_projections">transcript &amp; editable summary</a>)

Beau analyzes China's modest defense budget increase, questioning its impact on triggering a significant rise in U.S. defense spending and advocating for avoiding an arms race.

</summary>

"Arms races are bad."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

China's recent defense budget increase by 7.2% may not be substantial enough to trigger a massive increase in U.S. defense spending.
The increase brings China's defense spending to about $230 billion annually, compared to the U.S. spending of around $800 billion.
The U.S. doctrine since World War II aims to be able to fight its two largest competitor countries simultaneously, leading to higher defense spending.
Chinese defense spending, at 7.2% of their GDP, still falls short compared to the U.S. spending, which is at 3.5% of its GDP.
Beau suggests that U.S. politicians should avoid exaggerating China's defense budget increase to prevent a potential arms race.
An arms race is detrimental not only from a military perspective but also economically and in terms of wasted resources.
Beau warns against overcorrecting the U.S. defense budget due to the situation in Ukraine, as it may inadvertently signal to China and reignite the arms race.
The hope is that both China and the United States are signaling a desire to avoid an arms race by not significantly increasing defense spending.
The upcoming U.S. defense budget will provide clarity on whether both countries are committed to avoiding an arms race.
The key is to prevent any actions that might lead to an escalation of defense spending and consequently an arms race.

Actions:

for policy analysts, decision-makers,
Monitor and advocate for responsible defense budget decisions (suggested)
Stay informed about international defense spending trends (suggested)
</details>
<details>
<summary>
2023-03-07: Let's talk about Fox, a Zoom call, and the New York Times.... (<a href="https://youtube.com/watch?v=GU49dWv-9GM">watch</a> || <a href="/videos/2023/03/07/Lets_talk_about_Fox_a_Zoom_call_and_the_New_York_Times">transcript &amp; editable summary</a>)

Beau dives into a revealing Zoom call obtained by the New York Times, shedding light on Fox's decision-making processes and the struggle between truth and audience satisfaction.

</summary>

"If we hadn't called Arizona those three or four days following election day, our ratings would have been bigger."
"But I think we're living in a new world, in a sense, where half of the voting population doesn't believe in big corporations, big tech, big media."
"It seems like their mission is more to protect the brand."

### AI summary (High error rate! Edit errors on video page)

Explains the difficulty in winning cases like the one against Fox for defamation.
Mentions the New York Times obtaining a Zoom call of Fox higher-ups from November 16, 2020.
Describes the call and communications as "nothing short of amazing" and "very telling."
Shares that the call reveals insights into decision-making processes behind the scenes at Fox.
Quotes a Fox higher-up expressing regret for calling Arizona for Biden, affecting ratings.
Reveals internal debates at Fox about upsetting their audience with the truth during the election.
Talks about the lack of trust in big corporations, big tech, and big media among half of the voting population.
Questions Fox's mission, suggesting it's more about protecting the brand than informing the public.

Actions:

for media consumers,
Read the original article and summaries linked by Beau (suggested).
Dive into the insights about Fox's decision-making process and internal dynamics (suggested).
</details>
<details>
<summary>
2023-03-06: Let's talk about the highest and lowest paying majors.... (<a href="https://youtube.com/watch?v=8rr1_eadhDc">watch</a> || <a href="/videos/2023/03/06/Lets_talk_about_the_highest_and_lowest_paying_majors">transcript &amp; editable summary</a>)

Analyzing college majors and their salaries reveals societal values, showing undervaluation of critical professions like education and mental health.

</summary>

"We do not value the thing we need the most."
"Educators don't go into it for money, but there very well might be people who don't go into it because of money."
"We have an extended problem in the United States with a whole lot of people having issue with processing information, the ability to critically think."
"Social work? Nah. Mental health? Nah. Education? Nah. None of that stuff's important."
"This is just identifying a symptom of a disease."

### AI summary (High error rate! Edit errors on video page)

Analyzing college majors and their corresponding salaries within five years of graduation to understand societal values and trends.
Top 10 highest paying majors include engineering and business analytics, ranging from $65,000 to $75,000 annually.
Lowest paying majors consist of education, social services, and performing arts, ranging from $36,000 to $40,000 yearly.
Signifies a societal issue in undervaluing professions like education, mental health, and social work.
Implies a lack of critical thinking and information processing skills in society.
Educators are not adequately compensated for their vital role in society.
Points out the discrepancy in values between high-paying and low-paying majors.
Expresses concern about the devaluation of critical professions like education and mental health.
Criticizes the lack of financial support and respect given to educators.
Raises awareness about ongoing attacks on the education sector by legislators.

Actions:

for students, educators, policymakers,
Advocate for fair compensation for educators and professionals in critical fields (implied)
Support initiatives that prioritize education and mental health funding (implied)
</details>
<details>
<summary>
2023-03-06: Let's talk about being snowed in and prepared..... (<a href="https://youtube.com/watch?v=tolRbcL7aUs">watch</a> || <a href="/videos/2023/03/06/Lets_talk_about_being_snowed_in_and_prepared">transcript &amp; editable summary</a>)

Be prepared for emergencies with essentials like food, water, fire, shelter, a knife, and a first aid kit; having them together can make a huge difference during disasters.

</summary>

"Food, water, fire, shelter, first aid kit, a knife."
"If you don't have a kit put together take this as your sign to go ahead and get that done."

### AI summary (High error rate! Edit errors on video page)

People in California might be snowed in for 10 days or up to two weeks with no relief in sight.
It's a reminder to have an emergency kit ready because emergencies can happen anywhere.
The minimum essentials for the kit include food, water, fire, shelter, a knife, and a first aid kit.
Food for at least two weeks is necessary, it can be canned goods.
Water should be either purified, filtered, or bottled based on what's best for the area.
Shelter preparations are vital, like having a giant blue tarp for the roof during hurricane season.
Fire means literal fire and light sources like flashlights or lanterns are included.
Electronic backups like battery backups for phones are recommended.
A knife is an indispensable part of the emergency kit.
Having a first aid kit with necessary medications is vital during extended emergencies.
Keeping all these essentials together in one accessible place is key.
In case of being snowed in, having essentials ready to move with you is necessary.
Having these basics covered can make a significant difference during disasters.
Making the emergency kit more comprehensive is possible with additional resources.
It's emphasized that having this kit during hurricanes makes a huge difference for preparedness.

Actions:

for homeowners, residents,
Prepare an emergency kit with food, water, fire sources, shelter materials, a knife, and a first aid kit (suggested).
Ensure all essentials are kept together in an accessible place (suggested).
</details>
<details>
<summary>
2023-03-06: Let's talk about Trump's silent ban from Fox.... (<a href="https://youtube.com/watch?v=nPYwbTTTWxE">watch</a> || <a href="/videos/2023/03/06/Lets_talk_about_Trump_s_silent_ban_from_Fox">transcript &amp; editable summary</a>)

Fox News appears to have put a silent ban on Donald Trump, potentially to limit liability and hinder his return to power, amid decreasing support and influence.

</summary>

"Everybody knows that there's this soft ban or silent ban."
"The odds of him holding a position again seem to be ever decreasing."

### AI summary (High error rate! Edit errors on video page)

Donald Trump has not been on Fox News since September.
There is speculation that Trump has been placed on a do-not-let-him-on-the-air list by Fox News.
This could be due to Fox News trying to limit liability amidst ongoing legal cases.
Fox News might not want Trump on air due to his continued claims about the election.
Murdoch, the owner of Fox News, appears to be looking for another candidate to support.
Trump's absence on Fox News could hinder his ability to return to the White House.
Trump's influence seems to be waning, with decreasing support from high-ranking members and funders.
Trump's rhetoric now influences his base, which then influences policymakers.
The likelihood of Trump holding a position of power again is decreasing.

Actions:

for viewers, political activists,
Monitor media coverage for shifts in narratives and influences (suggested)
Stay informed about political developments and candidate support (suggested)
</details>
<details>
<summary>
2023-03-06: Let's talk about Haley's speech and what it can tell us.... (<a href="https://youtube.com/watch?v=_SUqRuOTBh8">watch</a> || <a href="/videos/2023/03/06/Lets_talk_about_Haley_s_speech_and_what_it_can_tell_us">transcript &amp; editable summary</a>)

Analyzing Nikki Haley's speech gives insights into Republican strategies, focusing on culture wars, leadership change, and differentiation from Trump, with scapegoating of the LGBTQ community likely.

</summary>

"Wokeness was a virus worse than any pandemic."
"The Republican leadership needs change."
"They've lost seven out of the last eight elections."
"A lot of scapegoating of the LGBTQ community."
"That's going to be their big push."

### AI summary (High error rate! Edit errors on video page)

Analyzing Nikki Haley's speech for insights into Republican strategies in the upcoming elections.
Haley's comparison of "wokeness" to a virus worse than a pandemic.
Republicans likely to focus on culture war issues, including being "woke," during primaries.
Transition to a more moderate stance expected if a moderate candidate succeeds.
Emphasis on the need for change within Republican leadership.
Efforts to differentiate from Trump by acknowledging his loss in the 2020 election.
Possibility of Trump or Trump-like candidates succeeding in the primaries.
Some candidates engaging in extreme grandstanding for primary attention.
Haley positioning herself as a moderate compared to Trump.
Anticipated continued emphasis on culture war topics and scapegoating of the LGBTQ community.

Actions:

for politically engaged individuals,
Prepare for the upcoming election cycle by staying informed and actively engaging in political discourse (implied).
</details>
<details>
<summary>
2023-03-05: Let's talk about racing for progress and purpose.... (<a href="https://youtube.com/watch?v=xVSgRbMEQi0">watch</a> || <a href="/videos/2023/03/05/Lets_talk_about_racing_for_progress_and_purpose">transcript &amp; editable summary</a>)

Beau advises using one's interests and skills, like racing cars, to shift thought towards progress and purpose, despite facing criticism, especially from the left.

</summary>

"Is it worth a tank of gas to get somebody away from a bigoted ideology? Yeah, absolutely it is."
"An activist can't be a perfectionist. They have to be a realist."
"In war, you use whatever tools you have at your disposal."
"If you think that you can use that to shift thought, you kind of have an obligation to do it."
"But if that road is built, you can't abandon it."

### AI summary (High error rate! Edit errors on video page)

Received a message from someone in the car scene who wants to use their hobby and skill to shift people politically.
The person is left-leaning but has an in with predominantly right-wing individuals through their interest in cars.
Concerned about being labeled a hypocrite for loving cars and racing while also caring about the environment.
Advises focusing on the purpose of using the car scene to bring people to a more progressive place rather than worrying about criticism.
Notes the difference in the political spectrum where the right is better at building a road to move people to their side compared to the left.
Emphasizes the importance of using available tools, like one's interest in cars, to shift thought and bring about change.
Acknowledges that taking heat is inevitable, especially from the left, but staying focused on the purpose is key.
Mentions that helping someone move away from a bigoted ideology is worth any criticism faced.
Encourages using one's skills and interests, like racing cars, as a means to influence thought and progress.
Suggests finding like-minded individuals in the left-leaning car scene to create a unified front for positive change.

Actions:

for left-leaning activists,
Connect with other left-leaning individuals in the car scene to create a unified front (suggested).
Use your interest in cars as a tool to influence thought and progress (implied).
</details>
<details>
<summary>
2023-03-05: Let's talk about new Trump document case developments.... (<a href="https://youtube.com/watch?v=MS9mKeNS1xI">watch</a> || <a href="/videos/2023/03/05/Lets_talk_about_new_Trump_document_case_developments">transcript &amp; editable summary</a>)

New reporting confirms long-held beliefs about Trump documents case, dispels false comparisons, and challenges FBI bias narrative while contrasting handling of situations by Biden, Pence, and Trump.

</summary>

"The issue was not that somehow they wound up there. The issue is that once they were asked for, they allegedly weren't returned."
"That was the problem. And that's what moved it to the situation we're in today."
"It looks like up until they literally had video evidence of boxes being moved, they wanted to let him go."
"This reporting challenges the narrative of FBI bias against Trump."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

New reporting on the Trump documents case confirms his long-held beliefs and dispels false comparisons.
Many within the FBI believed Trump's attorneys conducted a diligent search and returned everything, leading to a recommendation to close the criminal case.
Issue arose not from documents being present but from not returning them when asked for.
Prosecutors had to subpoena surveillance footage showing boxes being moved to change the belief that everything was returned.
Trump's willful retention of documents escalated the situation.
Beau believes that if Trump had remained silent after the search, the issue may not have escalated.
FBI agents initially believed everything was returned and wanted to close the case until video evidence showed otherwise.
The reporting challenges the narrative of FBI bias against Trump.
Contrasts between how Biden, Pence, and Trump handled similar situations are evident in the reporting.
Beau anticipates that this information will influence the special counsel's investigation.

Actions:

for interested viewers,
Stay updated on developments in the Trump documents case (implied)
Follow reputable sources for ongoing information on the special counsel's investigation (implied)
</details>
<details>
<summary>
2023-03-05: Let's talk about Trump, DC, and filings.... (<a href="https://youtube.com/watch?v=ws-QVpmgiqs">watch</a> || <a href="/videos/2023/03/05/Lets_talk_about_Trump_DC_and_filings">transcript &amp; editable summary</a>)

Beau addresses Trump's civil proceedings in DC, explains the lack of immunity for incitement, and warns of potential damage to Trump's image if the case proceeds.

</summary>

"Speaking to the public on matters of public concern is a traditional function of the presidency."
"If it does move forward, expect there to be tons of coverage about it."

### AI summary (High error rate! Edit errors on video page)

Addressing the lack of coverage on Trump's proceedings in DC and predicts increasing attention.
Urging support for striking workers at Warrior Met and sharing a link for assistance with final expenses.
Detailing the civil proceedings in DC regarding Trump's actions on January 6th.
Explaining Trump's argument of absolute immunity due to his actions being part of his duties as president.
Summarizing the Department of Justice's filing, stating that public communication doesn't include incitement of violence.
Not taking a stance on Trump's incitement but asserting that if plausibly alleged, he doesn't have immunity.
Implying that the courts may send the case back for further proceedings, indicating a negative development for Trump.
Pointing out the potential financial penalties and damage to Trump's image if the civil case proceeds.
Mentioning the likelihood of extensive media coverage if the proceedings advance.
Concluding with a vague statement and well wishes.

Actions:

for activists, supporters, observers.,
Support striking workers at Warrior Met with assistance for final expenses (suggested).
Stay informed about the developments in Trump's civil proceedings and potential implications (implied).
</details>
<details>
<summary>
2023-03-05: Let's talk about North Korea and framing.... (<a href="https://youtube.com/watch?v=6U8YuAKeANM">watch</a> || <a href="/videos/2023/03/05/Lets_talk_about_North_Korea_and_framing">transcript &amp; editable summary</a>)

North Korea faces food scarcity due to a combination of factors, reminding us that people's basic need for food transcends political differences and power dynamics.

</summary>

"People need food."
"They are people without enough to eat."
"They're just average people."
"It's not going to impact the people at the top."
"They're people."

### AI summary (High error rate! Edit errors on video page)

North Korea facing food scarcity, worsened by recent government actions.
Four main factors contributing to the situation: antiquated farming techniques, Western sanctions, weapons program, and public health isolation measures.
People tend to attribute the issue to a single factor, but it's a combination of multiple factors.
The consequences will primarily affect farmers, factory workers, and ordinary people.
Urges to recognize that beyond borders, there are no lesser people, even in adversarial countries.
Emphasizes the importance of people needing food regardless of political differences.
Suggests China stepping up as North Korea's best hope for aid, with Western help as an alternative.
Foreign policy decisions shouldn't overlook the basic need for food.
Calls for remembering that it's the average people who will suffer the consequences, not those in power.

Actions:

for world citizens,
Support organizations providing aid to North Korea (implied)
Advocate for humanitarian aid efforts for North Koreans (implied)
</details>
<details>
<summary>
2023-03-04: Let's talk about wolves and good news.... (<a href="https://youtube.com/watch?v=vvBdRa0PYQA">watch</a> || <a href="/videos/2023/03/04/Lets_talk_about_wolves_and_good_news">transcript &amp; editable summary</a>)

After nearly 25 years, the Mexican gray wolf population is on the rise in the US, a rare positive outcome in environmental conservation efforts.

</summary>

"For once, things are definitely going in the right way."
"Some fights are going the correct direction."
"Just a moment to recognize some fights are going the correct direction."

### AI summary (High error rate! Edit errors on video page)

Good news after nearly 25 years: Mexican gray wolf population increasing in the US.
The rarest subspecies of gray wolf in the US, reintroduction efforts started in 1998.
Over 200 Mexican gray wolves now in the wild in Arizona and New Mexico.
Program showing success with an increase in numbers over the last seven years.
Numbers were once in the thousands, now measured in hundreds.
There are around 240 wolves in the wild currently.
An additional 300-400 wolves in captivity contribute to genetic diversity efforts.
Captive bred puppy program aims to introduce wolves to dens in the spring.
More work needed for genetic diversity despite increasing population.
Positive progress in reintroducing a predator is rare in environmental issues.
Often, reintroduction efforts are obstructed by economic interests or trophy hunters.
This program is successfully moving in the right direction without such obstacles.

Actions:

for conservationists, environmentalists, animal lovers.,
Support captive breeding programs for endangered species to increase genetic diversity (implied).
Advocate for policies that prioritize environmental benefits over economic gains (implied).
</details>
<details>
<summary>
2023-03-04: Let's talk about nuance, context, and candy.... (<a href="https://youtube.com/watch?v=N0rsBxyV-tI">watch</a> || <a href="/videos/2023/03/04/Lets_talk_about_nuance_context_and_candy">transcript &amp; editable summary</a>)

Beau addresses the lost nuance and context surrounding Hershey's candy bars, revealing the intentional marketing strategy and broader implications of representation.

</summary>

"There is a widespread push in the United States at the state level targeting a specific group of people."
"These companies are marketing to the younger generation."
"The lost context, the lost nuance, it's not real."
"They have a problem with one, and that accurately reflects the demographics in the United States."
"I just said woman rather than trans woman."

### AI summary (High error rate! Edit errors on video page)

Addresses the lost nuance and context in a serious tone after receiving messages about a previous video.
Mentions a message questioning the representation of a trans woman on Hershey's package.
Explains that there were actually five packages featuring different women, but only one was being focused on.
Connects the issue to a broader push targeting specific groups with legislation in the United States.
Points out that companies like Hershey's are intentional in their marketing to appeal to a younger LGBTQ demographic.
Suggests that Hershey's was aware of the possible controversy and embraced it for advertising benefits.
Emphasizes that the lost context and nuance in the candy bar situation is not real, and it accurately mirrors the US demographics.

Actions:

for marketing professionals,
Contact LGBTQ+ advocacy organizations to support inclusive marketing practices (implied).
Educate others about the importance of diverse representation in marketing campaigns (implied).
</details>
<details>
<summary>
2023-03-04: Let's talk about measles and Kentucky.... (<a href="https://youtube.com/watch?v=dJIFdSGnQNc">watch</a> || <a href="/videos/2023/03/04/Lets_talk_about_measles_and_Kentucky">transcript &amp; editable summary</a>)

A situation in Kentucky involving potential measles spread at a large gathering underscores the importance of vaccination and following health advice.

</summary>

"Follow the advice. Do what you can to protect yourself and those you care about."
"There is a high likelihood of things popping up all around the country just because of the number of people involved and the amount of travel involved."

### AI summary (High error rate! Edit errors on video page)

A situation in Kentucky involving a large gathering of around 20,000 people holding hands and in close contact is developing due to one attendee being infected with measles.
The event, known as the Ashbury revival, took place from February 8th to February 19th in Kentucky, with attendees traveling from various locations, potentially spreading the infection.
Kentucky state health officials warn that those who attended the revival on February 18th may have been exposed to measles.
Unvaccinated attendees are advised to quarantine for 21 days and seek immunization with the safe and effective measles vaccine.
Due to extensive travel and numerous people potentially exposed, cases of measles are likely to appear in different areas.
Beau urges attendees, especially the under-vaccinated or unvaccinated, to follow health advice to protect themselves and others.
The risk of diseases like measles spreading is higher now compared to the past due to lower vaccination rates.
It is vital to be aware of symptoms, take preventive measures, and stay informed about potential outbreaks around the country.

Actions:

for public health authorities,
Quarantine for 21 days and seek immunization with the measles vaccine (suggested)
Stay informed about symptoms and potential outbreaks (suggested)
</details>
<details>
<summary>
2023-03-04: Let's talk about an upcoming consumer protection SCOTUS case.... (<a href="https://youtube.com/watch?v=orEdudK0VL0">watch</a> || <a href="/videos/2023/03/04/Lets_talk_about_an_upcoming_consumer_protection_SCOTUS_case">transcript &amp; editable summary</a>)

Beau explains the potential economic impact of a Supreme Court ruling on the Consumer Financial Protection Bureau and speculates on the court's decision.

</summary>

"If the Supreme Court decides that this agency is unconstitutional, the economy is probably going to go haywire."
"It's far off, but everybody's going to be talking about it occasionally until it happens."
"There's a whole lot at stake when it comes to the financial markets."

### AI summary (High error rate! Edit errors on video page)

Explains a Supreme Court case regarding the Consumer Financial Protection Bureau's constitutionality.
The Fifth Circuit ruled the agency unconstitutional based on its funding structure.
The Biden administration requested the Supreme Court to look into the ruling.
The agency's funding differs from other agencies as it does not rely on yearly appropriations from Congress.
The Consumer Financial Protection Bureau obtains funds by requesting from the Federal Reserve System.
Financial structure was authorized by Congress through the Dodd-Frank Wall Street Reform and Consumer Protection Act.
If the Supreme Court deems the agency unconstitutional, it could lead to economic turmoil.
The agency has a significant impact on regulations concerning mortgages.
Speculation is rife about the Supreme Court's decision, with expectations leaning towards overturning the Fifth Circuit ruling.
Most courts previously found the agency's structure constitutional.

Actions:

for legal enthusiasts, policymakers, activists.,
Monitor updates on the Supreme Court case and its potential implications (implied).
Stay informed about financial regulations and their impact on the economy (implied).
</details>
<details>
<summary>
2023-03-03: Let's talk about the Railroad Safety Act.... (<a href="https://youtube.com/watch?v=sED4B7_MoTU">watch</a> || <a href="/videos/2023/03/03/Lets_talk_about_the_Railroad_Safety_Act">transcript &amp; editable summary</a>)

Beau explains the Railway Safety Act of 2023, underlining the importance of listening to railway workers for safer operations while discussing new regulations and the need for government responsiveness.

</summary>

"Maybe it might be a good idea to listen to the people who actually do the job."
"Seems like it would be a good idea."
"It's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the Railway Safety Act of 2023 and its historical context.
Mentions a bipartisan group of six senators willing to impose new regulations.
Notes that Schumer has endorsed the Act.
Details the provisions of the Act, including increased fines for railway companies and new safety regulations.
Expresses skepticism about the effectiveness of advanced notice to state emergency agencies.
Questions if the regulations will limit the size and weight of trains.
Points out the importance of listening to rail workers' concerns.
Suggests that governments should heed advice from those who do the job, especially regarding hazardous materials.
Acknowledges the effort of the senators in attempting to improve railway safety.
Encourages considering input from those directly involved in the industry.

Actions:

for legislators, policymakers, railway workers,
Contact your representatives to voice support for regulations enhancing railway safety (suggested).
Stay informed about developments in railway safety regulations and advocate for transparent processes (suggested).
</details>
<details>
<summary>
2023-03-03: Let's talk about McConnell, McCarthy, and defense spending.... (<a href="https://youtube.com/watch?v=mVPNy8jwIy0">watch</a> || <a href="/videos/2023/03/03/Lets_talk_about_McConnell_McCarthy_and_defense_spending">transcript &amp; editable summary</a>)

McConnell's opposition to defense cuts poses a challenge for House Republicans aiming to cut the budget, with the pacing target set on China in an arms jog scenario.

</summary>

"He is somebody who when another politician, even if they're part of his party, you know, kind of takes a swipe at him, he'll just nod his head and quietly work to destroy them."
"It's not an arms race, it's an arms jog."
"Can't allow there to be a mineshaft gap and all that stuff."
"Why doesn't McConnell want defense spending cut?"
"Given the tone and rhetoric that we're hearing, if China starts spending more, the United States will start spending more."

### AI summary (High error rate! Edit errors on video page)

McConnell's position against defense cuts poses a problem for House Republicans.
McConnell's strategic political acumen and history of quiet retaliation against those who oppose him.
Republicans in the House want to cut the budget but need big numbers to show impact.
McConnell's opposition to defense cuts is significant as he holds sway in the Senate.
House Republicans face a challenge in finding budget cuts without upsetting their base.
The pacing target for defense spending is China, signaling a race to maintain dominance.
The possibility of increased spending by China leading to a similar response from the United States.
The implication of an arms race with China and the need for the U.S. to keep pace.
McConnell's stance on defense spending in the context of the near-peer contest with China and Russia.
The potential consequences of increased defense spending and the implications for global dynamics.

Actions:

for politically engaged citizens,
Contact your representatives to express your views on defense spending (suggested).
Stay informed about the implications of defense budget decisions (implied).
</details>
<details>
<summary>
2023-03-03: Let's talk about Hershey's and what's happening.... (<a href="https://youtube.com/watch?v=2oI5JrvZxvA">watch</a> || <a href="/videos/2023/03/03/Lets_talk_about_Hershey_s_and_what_s_happening">transcript &amp; editable summary</a>)

Conservatives upset over pronouns in Hershey's marketing reveal a trend of manufactured outrage losing traction.

</summary>

"There's not. It's at the point now that conservative brands who have built themselves on manufacturing outrage are running out of things to manufacture outrage about."
"It is just mind-boggling to me that this is what conservatives have latched onto in an attempt to manufacture outrage among their base."
"Let's see if we can do this again. I mean, think about the message that that's sending. Two guys, alone together. Obviously, that's the candy that y'all should be mad at next."

### AI summary (High error rate! Edit errors on video page)

Talks about a subset of Americans upset with a candy company due to a marketing move involving pronouns.
Mentions how conservative thought leaders are now upset with Hershey's and asking followers not to buy their products.
Explains that Hershey put a picture of a woman on the wrapper with "her" and "she" for International Women's Day.
Comments on the absurdity of conservatives being upset over pronouns in Hershey's name and marketing.
Suggests that conservative brands, built on manufacturing outrage, are running out of things to be upset about and are now battling candy companies.
Points out that most candy companies have legitimate reasons for criticism, but it generally does not involve their wrappers or marketing strategies.
Speculates that marketing companies working with candy companies intentionally provoke a small group of vocal conservatives for free advertising.
Notes that the demographic appreciating inclusive marketing is expanding while those against it are shrinking.
Expresses disbelief at conservatives focusing on pronouns in candy wrappers to manufacture outrage among their base.
Humorously suggests that conservatives should be mad at Mike and Ike's candy due to the names implying two guys alone together.

Actions:

for social media users,
Support candy companies promoting inclusivity (implied).
</details>
<details>
<summary>
2023-03-03: Let's talk about Biden's first potential veto.... (<a href="https://youtube.com/watch?v=5WsQnV5KwRw">watch</a> || <a href="/videos/2023/03/03/Lets_talk_about_Biden_s_first_potential_veto">transcript &amp; editable summary</a>)

Biden may exercise his veto power for the first time against a resolution overturning a rule related to ESG scores, revealing a shift in the Republican Party's stance on the free market.

</summary>

"Republicans are openly coming out against the free market."
"This shows a glimpse at where the Republican Party is headed."
"Biden has indicated that he is going to override it."

### AI summary (High error rate! Edit errors on video page)

Possibility of Biden exercising veto power for the first time is discussed.
A resolution has passed through both the house and Senate regarding overturning a rule related to retirement investment managers.
The resolution overturns a rule allowing consideration of ESG scores when picking investments.
ESG stands for Environmental, Social, and Governance, reflecting a company's responsible decisions.
Republicans are against this rule, labeling it as "woke capitalism."
Republicans are openly going against the free market, a departure from their traditional stance.
The economic benefit of investing in companies with high ESG scores is debated.
Biden is likely to veto the resolution, with Congress potentially attempting an override.
The Republican Party's shift away from free market principles is observed.
The future actions and decisions regarding the resolution are uncertain.

Actions:

for policy observers, political analysts,
Contact representatives to express opinions on the resolution (suggested).
Stay informed about political developments and decisions (implied).
</details>
<details>
<summary>
2023-03-02: Let's talk about whether Georgia will help Trump.... (<a href="https://youtube.com/watch?v=s64M9ZiC0u8">watch</a> || <a href="/videos/2023/03/02/Lets_talk_about_whether_Georgia_will_help_Trump">transcript &amp; editable summary</a>)

A theory suggests Trump's indictment could rally Republicans in the primary, but general election success remains doubtful due to his divisive nature and past actions.

</summary>

"Trump being indicted will help him."
"Can't win a primary without Trump, can't win a general with him."
"Trump's real danger is his ability to shape what other Republicans do."
"The primary is a different thing."
"I'd be incredibly surprised if we see him in the White House again."

### AI summary (High error rate! Edit errors on video page)

A theory is circulating in political circles regarding the impact of Trump's potential indictment on his chances in the Republican primary.
The theory suggests that Trump being indicted could actually help him by rallying Republicans around him as they view it as a political witch hunt.
While Trump excels at energizing his base and playing the victim, this strategy may primarily appeal to primary Republicans who are more extreme and involved compared to the general electorate.
Winning a primary with Trump's support does not guarantee success in the general election, as seen in previous cases where candidates won primaries but lost in the general election.
Trump's ability to influence and shape the behavior of other Republicans remains a significant threat to the Democratic Party, as he continues to hold power within the GOP.
The theory of Trump's potential indictment helping him in the primary may have limited credibility and may not translate into general election success.
While there is a possibility of Trump winning the primary due to strong support from extreme Republicans, it is doubtful that he will secure the presidency again.

Actions:

for political analysts, voters,
Monitor and actively participate in the Republican primary process to understand the dynamics shaping the party (implied).
Engage in constructive political discourse and challenge narratives that may harm democratic principles (implied).
</details>
<details>
<summary>
2023-03-02: Let's talk about the claims coming out of Russia.... (<a href="https://youtube.com/watch?v=O2InoUp53j8">watch</a> || <a href="/videos/2023/03/02/Lets_talk_about_the_claims_coming_out_of_Russia">transcript &amp; editable summary</a>)

Analyzing claims from Russia about a staged attack by irregular forces crossing from Ukraine, denials, and the potential consequences for civilians and conflict escalation.

</summary>

"It's all bad for everybody."
"It's always the civilians that get caught up in the middle of it."
"Russia engaged in a process called passportification."
"Once something like that starts it tends to become cyclical and it gets bad."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analyzing claims from Russia about a staged attack by irregular forces crossing from Ukraine.
Russian government claims a civilian car was hit by irregular forces, calling it a "provocation."
Ukrainian government denies involvement, with an intelligence officer pointing towards Russian responsibility.
Mention of the Russian Volunteer Corps potentially crossing the border.
Beau questions the credibility of Russian state TV and mentions the plausibility of the claims.
Reference to Russia's distribution of passports in occupied areas as a potential factor.
Consequences of such incidents being bad for everyone, especially civilians.
The potential for escalation depending on international reactions and Moscow's stance.
Ukrainian government's immediate denial of involvement seen as positive diplomatically.
Uncertainty surrounds the actual events and the credibility of Russian sources.
Beau expresses concern over the cyclical nature of conflicts once they escalate.
Emphasis on civilians bearing the brunt of such conflicts.
Warning about conflicts devolving into prolonged, destructive cycles.

Actions:

for international observers,
Monitor the situation closely and stay informed about developments (implied).
Support diplomatic efforts to prevent escalation and protect civilians (implied).
</details>
<details>
<summary>
2023-03-02: Let's talk about our roles.... (<a href="https://youtube.com/watch?v=aw_7yunWLQg">watch</a> || <a href="/videos/2023/03/02/Lets_talk_about_our_roles">transcript &amp; editable summary</a>)

Left-wing content creators and individuals have a vital role in expanding the Overton Window, advocating for positive change, and shifting societal perspectives to envision a better future through their unique talents and actions.

</summary>

"Helping people imagine something outside of that, imagine something better, imagine a world where everybody gets a fair shake, that kind of thing, advocating for that kind of change, even if it's subtle. That's a role, it's a role worth pursuing."
"Your activities, the things you do in your community, around you, online, all of that, it helps to change the world."
"You can leverage it to do something small at first and then it grows."
"But if your goal is to change the conversation and to advocate for something better, to the point where you're sending a message like this, you're putting in good work."
"Find what you're good at, use that skill, that talent, apply that to making the world better."

### AI summary (High error rate! Edit errors on video page)

Addresses the role of left-wing content creators in bringing forth positive action beyond criticizing the right.
Talks about the importance of expanding the Overton Window by helping people imagine a better world.
Emphasizes that everyone has a role to play in advocating for positive change, no matter how subtle.
Stresses the significance of shifting thought through activities in one's community and online.
Urges individuals to find their strengths and talents to contribute towards making the world better.
Encourages making a positive impact beyond just criticizing, by actively participating in activities that bring about change.
Mentions the discomfort of long-term change but underscores the necessity of shifting thought for progress.
Acknowledges the role of those who provide commentary on popular culture in shaping perspectives and advocating for a better future.
Suggests leveraging communities to initiate small actions that can grow over time.
Emphasizes the value of art, culture, and commentary in influencing societal change.
Points out that immediate relief work is vital, but long-term change requires shifting thought and advocating for a better world.
Encourages individuals to recognize their impact, even if it goes unnoticed, in changing the narrative for a better future.
Advocates for using personal skills and talents to contribute positively to society.
Stresses the importance of staying motivated and focused on making the world a better place through individual actions.

Actions:

for creators and activists,
Advocate for change through content creation and commentary (implied).
Engage in community activities that bring about positive change (implied).
Showcase art or cultural events that challenge perspectives (implied).
Support relief work and funding efforts for immediate assistance (implied).
Encourage others to imagine a world beyond the current norms (implied).
</details>
<details>
<summary>
2023-03-02: Let's talk about Eli Lilly, Biden, and changes.... (<a href="https://youtube.com/watch?v=a-3CE7RGMZ0">watch</a> || <a href="/videos/2023/03/02/Lets_talk_about_Eli_Lilly_Biden_and_changes">transcript &amp; editable summary</a>)

Eli Lilly and Biden team up to cap insulin costs at $35 per month, signaling a potential industry-wide shift towards more affordable healthcare for all.

</summary>

"Insulin is going to be capped at $35 per month out of pocket expenses."
"This is a huge win."
"It's kind of like a peace offering."
"Once it's recognized as something that everybody should have, rather than, well, that's just something that rich folk get."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Eli Lilly and Biden's unexpected good news: capping insulin at $35 per month out-of-pocket expenses.
Eli Lilly bringing their program in line with the Inflation Reduction Act for all, not just seniors.
Private insurance automatically applying the $35 cap, while uninsured individuals can register for Eli Lilly's Co-Pay Assistance Program.
Biden administration likely to take credit for the connection between the Inflation Reduction Act and the insulin cost reduction.
Eli Lilly's decision likely to pressure other companies in the market to follow suit due to their significant market control.
Potential for other major companies to announce similar cost reductions, ultimately lowering insulin prices nationwide.
Lilly's proactive approach to adapt voluntarily before facing heavy regulations amidst shifting demographics and increasing calls for regulation.
Addressing overpriced medications as a first step towards more affordable healthcare and a shift away from profit-driven patient care.
Importance of universal healthcare access to move towards a system where everyone is entitled to healthcare, not just the wealthy.
Encouragement for diabetics to stay informed about the cost reduction and be aware of the company producing their insulin product.

Actions:

for advocates for affordable healthcare,
Stay informed about the cost reduction and updates on insulin pricing (suggested)
Identify the company producing your insulin product and monitor for cost changes (suggested)
</details>
<details>
<summary>
2023-03-01: Let's talk about whether Russia's nukes work.... (<a href="https://youtube.com/watch?v=GEWDJkGRpvs">watch</a> || <a href="/videos/2023/03/01/Lets_talk_about_whether_Russia_s_nukes_work">transcript &amp; editable summary</a>)

Beau addresses Russia's nuclear arsenal maintenance, revealing concerns about functionality while stressing the catastrophic impact even a fraction can have.

</summary>

"A small fraction of it functioning as intended is enough to cause untold amounts of devastation."
"It is all bad. Please go watch War Games."

### AI summary (High error rate! Edit errors on video page)

Addresses the question of whether Russia's nukes still work and provides context on the maintenance process.
Mentions the substance "fog bank" used in nuclear weapons, its importance, and the challenges faced in obtaining it.
Shares insights on the difficulties of maintaining and modernizing nuclear arsenals, including the issue of part replacement.
Raises concerns about the maintenance standards of Russia's nuclear weapons compared to Western countries.
Explains the significance of even a fraction of the arsenal functioning for maintaining deterrence.
Acknowledges the likelihood of some Russian nukes not working due to maintenance issues and corruption.
Emphasizes that the concept of deterrence remains unchanged despite potential failures in some weapons.
Comments on the misconception that a nuclear exchange can be won, stressing the catastrophic nature of such an event.

Actions:

for global citizens, policymakers,
Monitor and advocate for transparency in nuclear arsenal maintenance and modernization (implied)
Support anti-corruption measures in military spending and equipment upkeep (implied)
</details>
<details>
<summary>
2023-03-01: Let's talk about student debt, SCOTUS, and what's next.... (<a href="https://youtube.com/watch?v=HOXiBny6gBw">watch</a> || <a href="/videos/2023/03/01/Lets_talk_about_student_debt_SCOTUS_and_what_s_next">transcript &amp; editable summary</a>)

Exploring student debt relief, forgiveness, and the Supreme Court's involvement, with a focus on potential outcomes and political ramifications.

</summary>

"Debt forgiveness becoming a campaign issue for 2024."
"Republican Party facing consequences of stopping debt forgiveness."
"Democratic Party needing significant control of the Senate for debt forgiveness legislation."

### AI summary (High error rate! Edit errors on video page)

Exploring student debt relief, forgiveness, and the Supreme Court's involvement.
Biden administration's push for debt forgiveness reaching the Supreme Court.
Majority opinion predicts the conservative Supreme Court will strike it down.
Possibility of justices ruling that the plaintiffs lack standing to sue.
Potential outcomes if the court strikes down the debt forgiveness.
Biden administration signaling that payments will resume if the forgiveness is struck down.
Limited options for the Biden administration if the forgiveness is struck down.
Debt forgiveness becoming a campaign issue for 2024.
Democratic Party likely framing the issue in their favor if forgiveness is stopped.
Republican Party facing consequences of stopping debt forgiveness.
Impact on the Republican Party and their supporters if forgiveness is halted.
Democratic Party needing significant control of the Senate to pass legislation for debt forgiveness.
Speculation around the Supreme Court decision and its financial impact on millions.
Uncertainty about the future financial stability of many due to the pending decision.

Actions:

for voters, policymakers, activists,
Contact your representatives to advocate for student debt relief legislation (implied).
Stay informed about the Supreme Court decision and its potential impact (implied).
</details>
<details>
<summary>
2023-03-01: Let's talk about cookies, college, and cons.... (<a href="https://youtube.com/watch?v=NGX-Lqp6TaQ">watch</a> || <a href="/videos/2023/03/01/Lets_talk_about_cookies_college_and_cons">transcript &amp; editable summary</a>)

Support Troop 6000 and Project Rebound by buying Girl Scout Cookies online and donating to support formerly incarcerated individuals' college scholarships.

</summary>

"Support Troop 6000 by buying Girl Scout Cookies online."
"Project Rebound boasts a 0% recidivism rate, a remarkable achievement."

### AI summary (High error rate! Edit errors on video page)

Girl Scouts started Troop 6000 in 2017 to cater to children in New York City shelters, providing activities and support during their stay.
Troop 6000 helps children transition to permanent housing, with the average stay in NYC temporary shelters being about 18 months.
Support Troop 6000 by buying Girl Scout Cookies online; a link will be provided in the description to order at least four boxes.
Project Rebound at California State University, Northridge, supports formerly incarcerated individuals in pursuing higher education.
From 2016 to 2020, participants in Project Rebound had an impressive 3.0 GPA and a 0% recidivism rate.
Donate to Project Rebound through a link provided below to contribute to college scholarships for program participants.
The scholarship fund at Project Rebound requires 50 donors to unlock around $5,000 for college scholarships.
Both programs, Troop 6000 and Project Rebound, are doing commendable work despite being geographically apart.
Project Rebound's 0% recidivism rate is particularly noteworthy and speaks to the program's success in supporting formerly incarcerated individuals.
Despite any reservations one may have about the Girl Scouts organization, supporting Troop 6000 through cookie purchases can make a significant difference to the children in shelters.

Actions:

for supporters and donors,
Buy Girl Scout Cookies online to support Troop 6000 (suggested)
Donate to Project Rebound for college scholarships (suggested)
</details>
<details>
<summary>
2023-03-01: Let's talk about Biden's speech, Virginia, and the debt ceiling.... (<a href="https://youtube.com/watch?v=f62ehTf_yo4">watch</a> || <a href="/videos/2023/03/01/Lets_talk_about_Biden_s_speech_Virginia_and_the_debt_ceiling">transcript &amp; editable summary</a>)

Beau analyzes Biden's speech in Virginia, exposing Republican healthcare threats and the political game around the debt ceiling issue.

</summary>

"Republicans are coming for your health care."
"Raising the debt ceiling is not about new spending."
"It's a political game trying to frame a false narrative."
"They don't really care about the debt ceiling. It's a show."
"Republicans may target healthcare or raise taxes on the rich."

### AI summary (High error rate! Edit errors on video page)

Biden's speech in Virginia focused on Republicans targeting healthcare, specifically the Affordable Care Act and Medicaid.
Virginia's importance stems from imminent retirements, making it a key state for the Democratic Party.
Roughly a quarter of Virginians rely on the Affordable Care Act or Medicaid.
The debt ceiling issue arises with McCarthy advocating for cuts tied to raising it, a move Biden opposes.
Biden argues that raising the debt ceiling isn't about new spending but about paying bills from past spending, much of which was approved by Republicans.
Republicans' reluctance to specify cuts and lack of interest in a balanced budget reveal their political game around the debt ceiling issue.
Beau suggests that Republicans may target healthcare or raise taxes on the wealthy to achieve a balanced budget.
The Republican actions regarding the debt ceiling are viewed as a political show for their less-informed base.

Actions:

for political analysts, democratic supporters.,
Contact local representatives to advocate for protecting healthcare and opposing cuts tied to the debt ceiling (suggested).
Organize community meetings to raise awareness about the implications of the debt ceiling issue on healthcare and national finances (implied).
</details>
