---
title: Let's talk about Putin the emperor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-9ZKsKL5zD4) |
| Published | 2023/03/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the International Criminal Court issuing a warrant for Putin.
- Compares Putin to either an emperor or a pirate, discussing the consequences of each.
- Mentions the concept that emperors often behave like criminals but are protected by state power.
- Points out that Putin, as the head of a major power, is more like an emperor.
- States that Putin facing prosecution by the ICC is unlikely without a significant mistake or Russian people's decision.
- Mentions the US legislation authorizing using all means necessary to bring back detained personnel from the ICC.
- Notes that the US, like Russia, is also considered an emperor and has legislation to protect its personnel.
- Suggests that major powers, not just Russia, have mechanisms to avoid facing consequences for actions.
- Talks about the importance of size when it comes to enforcing legal mechanisms.
- Concludes by stating that there won't likely be a US President going to the ICC and refers to the concept as just a thought.

### Quotes

- "Is Putin more like an emperor or a pirate?"
- "The head of state of any major pole of power is an emperor."
- "The United States is an emperor as well."
- "Sometimes when it comes to not the morality of it, but the legality and the ability to enforce those legal mechanisms, size matters."
- "It's just a thought."

### Oneliner

Beau explains the ICC warrant for Putin, compares him to an emperor, and touches on US legislation protecting personnel from the court.

### Audience

World citizens

### On-the-ground actions from transcript

- Contact your representatives to advocate for accountability for leaders regardless of their power (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of the implications of international law and power dynamics.

### Tags

#Putin #InternationalCriminalCourt #EmperorVsPirate #USLegislation #PowerDynamics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Putin
and what's happening with him internationally.
Because over the weekend, a whole bunch of people
asking why I hadn't talked about it.
Because it seems important.
This is not something that I'll be waiting around
for to happen.
It seems incredibly unlikely.
So if you have no idea what I'm talking about,
the ICC, the International Criminal Court,
issued a warrant for Putin.
Now, here's the thing.
The real question you have to ask yourself
deals with the concept that we have talked about on the channel
number of times. In fact, the first time we talked about it was almost four years
ago to the day. Is Putin more like an emperor or a pirate? For those who know
where this is going, if I can find the song I'll put it down below. More like an
emperor, right? There's a story, a very old story, and I'm gonna sum it up real
quick. A pirate is captured and brought before an emperor. And the emperor says,
what are you doing? You're disturbing the piece of the seas. And the pirate's like,
what are you doing? You're disturbing the piece of the entire planet. But I only
have one ship, so I guess I'm a pirate. You do the exact same thing with a fleet,
So you're an emperor? Pretty smart answer. The idea is that many emperors behave
in a way that if it wasn't for the cloak of state power, well, they'd be criminals.
and that often the size and the power of the state protects them. There are actions, when
you think about an emperor empire building back in the day, what are they doing? The
same thing a pirate does. The idea is that there's a moral equivalency but the
Emperor normally doesn't face consequences for those actions, just the
pirate. This is a theme that has been explored for centuries and Chomsky has
talked about it, there is a very unique video that I will try to find and put
down below, but at the end of it Putin is an emperor. The head of state of any
major pole of power is an emperor. It's unlikely that he'll end up facing
prosecution by the ICC, it would take him making a huge mistake going somewhere he
knows he shouldn't go or the Russian people deciding that's where he needs to
be. Short of him being deposed, I don't see it happening. The warrant is gonna
bother him. There's going to be a whole bunch of things he can't do, a whole bunch of places he can't go.
But without the Russian people deciding that he needs to be there, probably not going to happen.
I'm not saying it's right. I'm saying it's the way it is, and it's the way it's been pretty much as long
as there's been nation-states. Now people in the United States in particular. Law
and order and all of that. They probably feel some kind of way about me just
saying this isn't going to happen. So I want to read something. The president of
United States is authorized to use all means necessary and appropriate to bring about the
release of any U.S. or allied personnel being detained or imprisoned by on behalf of or at
the request of the International Criminal Court. I think the official name of this is the
Servicemembers Protection Act. It is widely known as the Invade the Hague Act.
It was signed, I want to say back in 2002. The United States is an emperor as well.
The U.S. actually has legislation authorizing getting its troops back.
What do you think Russia would do for their head of state?
It seems unlikely, it seems very unlikely, and it isn't unique to Russia.
There won't be a President of the United States going to the ICC.
There is legislation pre-authorizing the ability to use any and all appropriate means.
What is deemed appropriate would be up to whoever is in power at the time.
But historically, any and all appropriate means means anything.
It's not just a Russia thing.
It's a major power thing.
Sometimes when it comes to not the morality of it, but the legality and the ability to
enforce those legal mechanisms, size matters.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}