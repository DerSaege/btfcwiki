---
title: Let's talk about Trump and his requests....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=s6qIsTpvYhc) |
| Published | 2023/03/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump believes he will be taken into custody on Tuesday, but Beau doesn't believe him.
- New York appears to be moving towards a criminal indictment against Trump, prompting him to request his supporters to assemble.
- Republican members like McCarthy and Green are advising against protests, hoping that Trump's base remains calm.
- Trump's base seems relatively calm, with some suggesting roadblocks and other disruptive actions.
- Beau hopes Trump's supporters will recall that he didn't pardon anyone and may not stand up for them.
- Concerns about surveillance in New York and the NYPD's experience in handling assemblies that get out of hand are raised.
- Beau stresses the importance of the right to peaceably assemble, even for expressing discontent.
- Suggestions are made for supporters to gather in Florida near Trump's place, considering the restrictive laws on assembly in the state.
- While most of Trump's base appears calm, there are concerns about potential risks and scenarios unfolding.
- Beau hopes for calm and sensibility among all involved parties, acknowledging the uncertainty of the situation.

### Quotes

- "I hope that they wouldn't put themselves at risk for him because he's not going to stand by them."
- "I really hope that the combination of members of the Republican Party saying, you know, you don't need to do this..."
- "I hope that anybody that comes into Florida understands the laws that have been put on the books when it comes to assembly."
- "It's not going to accomplish what they want, and it's just going to put them in a situation where they're paying for believing in somebody who would not stand up for them."
- "There are a lot of options and a lot of different scenarios."

### Oneliner

Former President Trump predicts custody on Tuesday, prompting requests for assembly, while concerns over risks and sensibility prevail among supporters and authorities.

### Audience

Supporters, authorities, observers.

### On-the-ground actions from transcript

- Understand the laws on assembly in Florida before gathering near Trump's place (suggested).
- Stay calm and sensible amidst uncertainties and potential risks (implied).

### Whats missing in summary

The full transcript provides a detailed analysis and speculation on the potential reactions and actions surrounding former President Trump's situation, offering insights into the dynamics between him, his supporters, and legal authorities.

### Tags

#Trump #Supporters #Assembly #RepublicanParty #NewYork #Florida


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and his requests from his supporters and
the reaction to them from some pretty prominent members of the Republican party
and what we can hope happens with all of this.
So if you have no idea what I'm talking about, the former president of the
United States has indicated that he believes he will be taken into custody
on Tuesday. Now, me personally, I don't really believe anything the man says, so
I don't really put any stock in the idea that he's going to be taken into
custody Tuesday. That being said, it does appear that the the New York system is
is moving in the direction of pursuing a criminal indictment of the former president.
Because of this news, he has made requests for his supporters to assemble and there's
a lot of talk about it.
You have McCarthy and Green saying, you know, there's no need to protest or people hoping
that they don't protest or whatever, the reaction from Trump's base, for the most
part has been pretty muted.
You do have a number of people suggesting roadblocks and just all kinds of stuff.
I would hope that they remember the 6th.
I would hope that they remember that he didn't pardon anybody when he had the ability.
He didn't stand up for them.
And I hope that they wouldn't put themselves at risk for him because he's not going to
stand by them.
That seems super unlikely.
And when it comes to New York, anything they do there, it's going to be a lot like the
They may not know it, but going into New York, they're going into one of the most
surveilled places on the planet.
There's not a lot of transparent travel into New York City.
Going over a bridge, through a tunnel, flying in, there's going to be a record.
And if things get out of hand, it will be like the 6th, you know, a week or two later
somebody's going to knock on their door.
I would really hope that the combination of members of the Republican Party saying,
you know, you don't need to do this, the acknowledgement that he is not going to stand up for them
the way he wants them to stand up for him, and the reality of the situation in New York City
and the fact that they would be dealing with NYPD that has a lot of experience
dealing with assemblies that get out of hand, I hope that it keeps them calm. We
do need to remember that they have the right to peaceably assemble, even if,
even for this reason. I mean, if they want to go out there and wave their
flags and express their discontent, that's okay. I really hope that they stay
calm. You know, the alternative would be to do something in Florida near his
place, a show of support there. I hope that anybody that comes into Florida
understands the laws that have been put on the books when it comes to assembly.
They're very restrictive, and there's probably a lot of things that they might want to do
that in Florida is a big deal.
Whether or not it's going to happen, it's hard to say at this point.
It is hard to say.
The overwhelming majority of his base seems relatively calm about the whole thing.
You do have some that are particularly upset and suggesting all kinds of wild things.
I would hope they don't do it.
It's not going to accomplish what they want, and it's just going to put them in a situation
where they're paying for believing in somebody who would not stand up for them.
I hope that that is realized by his most ardent supporters.
But we're going to have to wait and see.
There is a possibility that it gets bumpy.
There are people who are suggesting there's risks with the system up there pursuing a
criminal indictment.
And there are people that acknowledge there are risks if they refuse to follow evidence
and pursue criminal indictments.
There are a lot of options and a lot of different scenarios.
I am hopeful that people come to their senses and stay calm.
I'm not certain that's what's going to happen, but I am hopeful.
We'll have to wait and see what happens.
I would not necessarily anticipate something happening on Tuesday, with him going into
custody on Tuesday.
I know that's the claim, but it's Trump, so you have to have some other source to believe
anything from that man.
So anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}