---
title: Let's talk about people losing faith in Fox....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qNM_XqrzaN0) |
| Published | 2023/03/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discussed the issue of trust in Fox News following recent revelations.
- Mentioned internal discrepancies between what was said in private and public.
- Speculated that this could shake some Fox viewers out of their echo chamber.
- Shared survey results showing 21% trust the network less, while 35% continue to trust it.
- Suggested that some viewers who had no opinion might still trust the network but are hesitant to admit it.
- Proposed allowing individuals to maintain their belief as a way of reaching out to them.
- Noted the impact of major figures expressing different opinions behind the scenes than on air.
- Viewed this situation as an opening to reach out to family members still stuck in the echo chamber.
- Encouraged not giving up hope in trying to reach family members with different viewpoints.
- Emphasized the importance of finding the right mechanism to reach people.

### Quotes

- "Nobody's a lost cause."
- "There will still be a significant portion that, well, you just can't reach."
- "The right method is out there, it just has to be found."

### Oneliner

Beau touches on trust in Fox News, urging to find ways to reach individuals still in the echo chamber, despite internal discrepancies and survey results. 

### Audience

Family members

### On-the-ground actions from transcript

- Reach out to family members still in the echo chamber (suggested)
- Find the right method to communicate with those holding different viewpoints (implied)

### Whats missing in summary

Insights on how to effectively communicate with individuals holding onto their beliefs despite conflicting information.

### Tags

#Trust #FoxNews #EchoChamber #Family #Communication


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about trust in Fox News.
After the revelations contained in the releases,
as far as the internal discussions,
versus what they were saying in public,
we've talked on the channel about how this might actually
be the thing that causes some Fox viewers to lose faith in the network.
And I have expressed the opinion that, you know, that this is going to be one of those
things that might truly shake some people out of the echo chamber.
You know, I am of the belief that everybody can be reached.
Nobody's a lost cause.
just have to find the right mechanism to do it. There was a survey of Fox viewers
over the age of 18 in which they were asked about how their faith in the
network had shifted or whether it had since the revelations related to the
lawsuit and all of those quotes coming out. 21% trust the network less.
35% continue to trust it, 11% say they didn't trust it before and the remainder had no opinion.
So what you're looking at here is a clear indication that this is something that is
kind of getting through to some viewers.
Now my personal read on this error is that those who had no opinion, which I want to
say is 23% I think, that they continue to trust the network, they just don't want to
say that.
So that would bump that up to 68% continuing to trust the network.
I would also suggest those who say they didn't trust it before probably did and they just
don't want to admit that they were tricked. Now, if you encounter somebody like that,
it might be a good idea to allow them to maintain that fiction. If you have people in your life
who are suddenly like, well, you know, I only watched Fox News for entertainment. I didn't
really believe it. Okay, Uncle Bob, I absolutely, I believe you. You know, I would just let
Let them have that, to be honest.
The quotes are unique.
They're a situation in which you have major figures at Fox reportedly expressing the exact
opposite opinion of what they expressed on the air or what they leaned into on the air
or what they gave credibility to on the air.
And what is on the air is what their viewers saw.
And that's what they believed.
And the behind the scenes conversations are, they're jarring.
If you were somebody who truly believed that, to see the people who led you to believe that
say that they didn't believe it themselves? That's going to be jarring.
21% said that they trust the network less and 11% said they didn't trust it
to begin with. That's 32% of Fox viewers with either no or less trust than before.
for. This is an opportunity. It's a moment to reach out to those family members who are
still in that echo chamber. According to the survey, there will still be a significant
portion that, well, you just can't reach. But at some point there will be another mechanism.
So don't give up hope when it comes to reaching one of your family members.
The right method is out there, it just has to be found.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}