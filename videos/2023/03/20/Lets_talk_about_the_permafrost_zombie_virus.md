---
date: 2023-03-21 04:13:16.518000+00:00
dateCreated: 2023-03-20 11:17:25.612000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about the permafrost zombie virus....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A_6gdTkIGRU) |
| Published | 2023/03/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the intersection of climate, public health, science fiction, and the future.
- Permafrost is thawing due to climate change, releasing ancient viruses termed "zombie viruses."
- A 48,500-year-old virus found in permafrost could still infect amoebas, raising concerns about potential public health threats to humans.
- Research on ancient viruses has been ongoing since at least 2014 to understand the viability and potential risks.
- The research aims to determine the threat these ancient viruses pose and develop defenses against them.
- Mummified remains found in permafrost still show traces of human disease, indicating the potential longevity of viruses.
- Transitioning from dirty energy is slow, necessitating preparation for new and old public health issues arising from climate change.

### Quotes

- "Ancient viruses could become a real public health issue for humans as permafrost thaws."
- "It's worth remembering that mummified remains frozen in permafrost often carry traces of human disease."
- "Transitioning from dirty energy is slow; we must prepare for new and old public health challenges."

### Oneliner

Exploring the intersection of climate change, ancient viruses, and public health risks as permafrost thaws, necessitating preparedness for potential threats.

### Audience

Climate researchers, public health professionals.

### On-the-ground actions from transcript

- Monitor research on ancient viruses in permafrost to stay informed and aware of potential public health threats (implied).
- Support initiatives that aim to understand and develop defenses against ancient viruses released from thawing permafrost (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of the implications of climate change on ancient viruses, public health risks, and the importance of proactive measures in the face of evolving environmental challenges.

### Tags

#ClimateChange #PublicHealth #AncientViruses #PermafrostThaw #ScienceFiction


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the intersection
of climate and public health and science fiction
and the future.
So scientists have been looking at things contained
in the permafrost.
permafrost is exactly what it sounds like.
It's something that remains frozen permanently, year round,
at least it used to.
But as the climate has changed, that's also changing.
And areas are thawing.
Some of the things being found are viruses.
They are being called zombie viruses because they are being revived.
News has hit the cycle about one that was 48,500 years old, and it was found that it
could still infect amoebas.
This leads to a lot of questions as far as as the climate continues to change and as
these areas melt, will ancient viruses then become a real public health issue for humans?
Because if they can still infect amoebas, the odds are that they may still be able to
infect humans, other viruses.
And then there are also concerns about whether or not this type of research should be taking
place.
As is often the case with the media picking up on a story, doesn't always provide context.
This has been going on, this type of research has been going on, since at least 2014, pulling
out viruses that were frozen and seeing if they are still viable.
A lot of them are.
is, not just is it cold, so things tend to keep, it's also, I mean for the most part,
kind of oxygen free.
So there's a lot that can be learned.
Now the research is hopefully going to kind of gauge the threat and also, maybe, develops
some kind of defense.
It's worth remembering that oftentimes when mummified remains are found frozen in the
permafrost after hundreds of years, there is still strong traces of human disease in
the mummies.
So while the idea of an ancient virus striking back after 40,000 years sounds like a plot
to a really bad science fiction movie, it's not out of the realm of possibility.
And given the fact that our transition away from dirty energy is less than speedy, it's
It's probably a good idea to look into the other things we're going to have to deal
with along with the impacts of climate change.
And new, old public health issues, they're going to be one of them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}