---
title: Let's talk about protecting the environment on the high seas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mUPpp8Wbusk) |
| Published | 2023/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Introduces the topic of biodiversity beyond national jurisdiction focusing on the ocean.
- Mentions the Biodiversity Beyond National Jurisdiction Agreement at the UN.
- The goal is to protect oceans outside national jurisdiction from seabed to airspace.
- Aims to combat overmining and overfishing while protecting unique ecosystems.
- Will aid in migration and distribute resources equitably.
- The high seas cover about half the planet and are largely unmanaged.
- Concerns lie in implementation and whether countries will uphold commitments.
- Past environmental agreements have faced challenges in implementation.
- Despite skepticism, this agreement is seen as a move in the right direction.
- No other agreement or treaty compares to this one.
- Emphasizes the role of governments in addressing ocean protection.
- Acknowledges that those causing the problem are now involved in solving it.
- Optimism exists regarding the potential impact of this agreement.
- Encourages staying informed and hopeful about the progress of the agreement.

### Quotes
- "It's one of those things we'll start hearing more and more about as countries weigh in."
- "This is one of those situations where the people causing the problem have put together a group of people to fix the problem in a way that satisfies them."

### Oneliner
Beau introduces the UN's agreement on biodiversity protection in the ocean, aiming to combat overmining and overfishing while ensuring equitable resource distribution, but the key lies in effective implementation by nations.

### Audience
Environmental activists, policymakers

### On-the-ground actions from transcript
- Keep updated on the progress of the Biodiversity Beyond National Jurisdiction Agreement at the UN (suggested).

### Whats missing in summary
The full transcript provides a comprehensive overview of the challenges and hopes associated with the Biodiversity Beyond National Jurisdiction Agreement, offering valuable insights into the need for international cooperation in protecting ocean biodiversity.

### Tags
#Biodiversity #OceanProtection #UNAgreement #ResourceDistribution #EnvironmentalActivism


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about biodiversity
beyond national jurisdiction.
We're talking about the ocean.
It's about the high seas.
There is an agreement that is kind of making its way
through the process after about 15 years or so of trying
to get it put together.
It is the Biodiversity Beyond National Jurisdiction Agreement at the UN, and the goal of this
thing is to protect oceans off the coastlines, so outside of national jurisdiction, from
the seabed to the airspace above it.
It's supposed to protect against overmining, overfishing, all of this stuff.
It will protect unique ecosystems.
It will help with migration.
There's a ton of stuff that is covered in this, and that's why it's taken so long to
get it together.
But it finally looks like it's ready to really start moving forward.
This space, the high seas, this portion of the Earth, is about half the planet and for
the most part it is completely unmanaged.
This aims to kind of equitably distribute the resources in a way and make sure it's
It's not harvested in a fashion that is not sustainable or greatly impacts a specific area.
That's the overall goal.
The agreement itself, it's taken a long time to get there.
There's a lot to it and overall, it looks pretty good, but like anything of this scale,
The real question is how it gets implemented and whether or not countries that even may
agree to it and sign on to it actually live up to their commitments under it.
We have seen agreements and ideas kind of fall short when it comes to other environmental
causes that have come through the UN.
So while we don't really want to get our hopes up for this yet, it's a move in the
right direction and it's something that is desperately needed to the point that there's
not really anything to compare this to.
There is no other agreement or treaty that really even comes close to this.
But again, we have to see what the actual governments, those national jurisdictions,
actually do with it.
Because if it wasn't for those governments, for those national jurisdictions, this probably
wouldn't be much of a problem anyway.
This is one of those situations where the people causing the problem have put together
a group of people to fix the problem in a way that satisfies them.
So we're going to have to see the implementation.
But overall, it's pretty hopeful.
There are a lot of people who are normal do-mers who really don't think any of this stuff will
work that are pretty excited about this one.
So it's on the horizon.
It's one of those things we'll start hearing more and more and more about as countries
weigh in as it moves through the process. So it's something to just keep your eye
on and kind of hope for the best at this point. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}