---
title: Let's talk about Pence, history, Trump, and strategy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VkoCapTA_xc) |
| Published | 2023/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former Vice President Pence acknowledged he had no right to overturn the election and condemned Trump's actions that endangered lives at the Capitol.
- The Republican Party is slowly distancing itself from former President Trump, fearing his base's backlash.
- High-level Republicans, including Pence, are gradually making more condemning statements about Trump to shift blame onto him.
- Pence's actions are seen as his first moves towards his own presidential run, indicating an alliance with moderates despite his conservative stance.
- The Republican establishment is trying to balance distancing from Trump without angering his base before the primary season.
- Concerns arise about whether the party will back another authoritarian candidate similar to Trump or move towards traditional conservative values.
- The establishment needs to address supporting candidates with authoritarian tendencies within the party post-Trump era.

### Quotes

- "I had no right to overturn the election and his reckless words endangered my family and everyone at the Capitol that day."
- "The Republican Party as a whole fears Trump's base and they loathe Trump."
- "Seeing Pence start to push that out there in what many see as his first moves towards his own presidential run."

### Oneliner

Former Vice President Pence condemns Trump's actions, signaling the Republican Party's slow distancing strategy from the former president, balancing fear of backlash and internal conflicts over authoritarian candidates post-Trump era.

### Audience

Political observers, Republican voters

### On-the-ground actions from transcript

- Monitor high-level Republican statements and actions regarding former President Trump to understand the party's direction (implied).
- Engage in political discourse and activism to push for traditional conservative values within the Republican Party (implied).

### Whats missing in summary

Insights into potential future Republican Party dynamics and the importance of monitoring the party's direction post-Trump era.

### Tags

#RepublicanParty #DonaldTrump #MikePence #PresidentialRun #AuthoritarianCandidates


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about former Vice President Pence, former President Trump,
and some words that were spoken.
And what might be taken as a sign of the Republican Party's strategy to continue to push former
President Trump into irrelevancy, slowly but surely, with a lot of caution.
So Pence is at this thing and he's giving his remarks, he's giving his speech, and
he says, I had no right to overturn the election and his reckless words endangered my family
and everyone at the Capitol that day.
And I know history will hold Donald Trump accountable.
And in another place, he says President Trump was wrong.
There's a lot of comments about it.
So what you're seeing when you combine this with some of the other statements that were
made by high-level Republicans concerning Trump is you're starting to see this slow
push to make ever more condemning claims and statements about the former
president, about former President Trump. And you have to wonder why they don't
just walk out and say, he did everything that they said he did, all right, fear.
There's a lot of fear. The Republican Party as a whole fears Trump's base. They
fear the backlash. So they fear Trump's base and they loathe Trump. So they're
trying to work out a balance where they slowly put out more and more
Controversial statements, statements that are designed to push the blame onto Trump
and do so slowly so his base isn't angered.
Slowly getting his base acclimated to the idea that the Republican Party is not going
to support Trump again, that he's going to get a lot of pushback this time.
they're trying to walk that fine line between putting that information out
there before the primary season really starts in earnest and angering his base
so much that they call for him to run it on a third-party ticket. They're trying
to walk that fine line. One of the interesting things about it is seeing
Pence do it. Pence is Pence, but he is a player within the Republican
establishment. Seeing him start to push that out there in what many see as his
first moves towards his own presidential run kind of indicates that he's throwing
his lot in with the moderates. Now, make no mistake about it, this does not mean
that Pence is a moderate. Pence is not like Hogan as an example. Pence is a very
conservative, very right-wing, very set in his ways. He's anti-Trump. So there is
is an alliance that is probably very uneasy between those candidates who are on the far
right but really loathe Trump and the normal Republican Party from back in the day.
I would expect to see more high-level Republicans say things that are less than flattering about
former President Trump and those who continue to support him. There were some
veiled shots at Tucker Carlson's attempt to reframe what happened on the 6th as
well coming from Pence. You're probably going to see a lot more of that as well.
The one question that I still have that I haven't seen any real indication of
is whether or not the loathing that the Republican Party establishment, the
moderates, that original Republican Party pre-MAGA, I haven't seen any
indication as to whether or not they would back another authoritarian like
Trump, a Trump-lite candidate. Right now they seem to be all aiming at Trump, but
there are other candidates who are just like him, that are still part of that
MAGA faction. We haven't seen anything suggesting that they're going to push
back on those candidates as much as they are against Trump, which honestly is a
mistake. If they want to retake the Republican Party, they have to reintroduce
those traditional conservative values and move away from the more authoritarian
aspects of the Republican Party, which would include those other candidates. But
so far we haven't seen that, which leaves me with a little bit of concern that
they may get rid of Trump using this method and end up throwing their weight
behind somebody who is just as bad or maybe even worse from that same
ideological makeup. And that's the one piece of information we all kind of
need and we need to know what the Republican establishment is planning
in that regard, and whether or not all of those contingency plans they've
developed would apply to candidates of the same ideological makeup. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}