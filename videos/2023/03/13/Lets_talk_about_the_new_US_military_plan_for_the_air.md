---
title: Let's talk about the new US military plan for the air....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=s4gjtvxoDE0) |
| Published | 2023/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to discussing the United States Air Force and its future.
- Mention of a new concept that was once seen as science fiction but is now on the brink of implementation.
- Possibility of purchasing around 1000 pilotless aircraft in the near future.
- Description of these aircraft as wingmen that will fly alongside fighter planes.
- Control of the drones either through AI, the fighter pilot, or from a distant observation aircraft.
- Purpose of the concept is to assign riskier tasks to drones, allowing human pilots to focus on primary objectives.
- Aim of these collaborative combat aircraft is to function effectively as wingmen while being cost-effective enough to be expendable.
- Reduced risk for pilots leads to potentially fewer mistakes and better focus on mission objectives.
- Plans for purchases to commence in the coming years, with a program named the Loyal Wingman program.
- The development could impact military strategies and make engagements with rival nations less costly.

### Quotes

- "More dog fights, less people."
- "The overall idea is to have these aircraft be capable enough to actually function as the wingman, but inexpensive enough to be something that can be lost."
- "If the pilot is not at risk, they are less likely to make mistakes."

### Oneliner

Beau talks about the potential future of the United States Air Force, including the use of pilotless aircraft as wingmen, aiming to enhance mission focus and reduce risks for pilots.

### Audience

Military enthusiasts, policymakers

### On-the-ground actions from transcript

- Research and stay informed about the developments in the United States Air Force's use of pilotless aircraft (implied).

### Whats missing in summary

Detailed insights on the potential ethical and strategic implications of shifting towards pilotless combat aircraft. 

### Tags

#UnitedStatesAirForce #FutureOfMilitary #PilotlessAircraft #CombatTechnology


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about the United States Air
Force and the future of it.
And a new concept that they are entertaining,
look like they will be enacting, that not too long ago would
have been written off as science fiction,
would have been written off as something that that'll
be happening 20 or 30, 40 years from now.
But now it looks like it's right around the corner.
And it may lead to the purchase of about 1,000 aircraft
in the near term.
And these aircraft will not have pilots,
but they will be wingmen.
They will fly alongside fighter planes,
and they will be the wingman.
more dog fights, less people.
The craft themselves, the drones,
will be controlled either via AI by the fighter pilot
themselves or from an observation aircraft further
off.
The concept here is to allow the drone
to take on the riskier assignments.
and the secondary stuff that comes into play when aircraft go into combat and allow the
pilot to focus on the main mission unless the main mission becomes incredibly risky
and then the drone goes and does it.
The overall idea behind what they're calling collaborative combat aircraft, I think is
the buzzword for it.
The overall idea is to have these aircraft be capable enough to actually function as
the wingman, but inexpensive enough to be something that can be lost.
You lose some of these planes that the Air Force uses today, you're out a whole lot of
money and you're out of a whole lot of training with that pilot.
If you can get the same effect with a drone controlled by that pilot who is now in a safe
position, it's just overall better as far as risk management.
The other good thing about this is when you're talking about something like this, if the
pilot is not at risk, they are less likely to make mistakes.
So because there's less pressure, they're
going to be more focused on achieving the goal, which
could help in a lot of other ways.
That remains to be seen.
It's a theory, but it hasn't ever been tested yet.
So these purchases may start occurring
within the next year or two.
And getting this program up and running,
one of the terms they're using for it
is the Loyal Wingman program, if you really
want to start digging into it and look it up.
It's definitely something that would tip the scales.
It would also make any potential arms jog
with a near-peer nation a lot less expensive.
So it's a development.
I don't know if I would call it good news,
but it is certainly unique news, and it's definitely
something to watch. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}