---
title: Let's talk about Virginia and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Imp4r34pal0) |
| Published | 2023/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Seven deputies in Henrico County, Virginia have been charged with second degree murder for the death of Ervo Otenyo, a 28-year-old man.
- Ervo Otenyo was being admitted to a hospital and allegedly became combative.
- The prosecutor stated that Otenyo died of asphyxia and indicated that the seven deputies were involved.
- Documentation suggests Otenyo was on an emergency custody order related to mental health, which may be a significant factor.
- There are reports of video footage of the incident, described as brutal and inhumane.
- Despite the quick arrests of the deputies, there still needs to be a grand jury process before further progress.
- The deputies are set to appear in court on the 21st, where more information about the process may emerge.
- Statements from the prosecution, family attorney, and local FOP suggest a tense and uncertain situation.
- Many questions remain unanswered regarding mental health issues and the sequence of events leading to Otenyo's death.
- The unfolding events are likely to attract national attention, with more revelations expected in the future.

### Quotes

- "The public and experienced mental health professionals alike will be appalled when the facts of this case are fully made known."
- "It certainly seems like this is going to be national news."
- "There's a whole lot of questions as far as the mental health issues."
- "We're probably in for another long haul."
- "It's just a thought, y'all have a good day."

### Oneliner

Seven deputies charged with second degree murder in the death of Ervo Otenyo, raising questions about mental health and promising a lengthy and impactful legal process.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Support mental health advocacy groups (implied)
- Stay informed about the case developments and outcomes (implied)
- Attend court proceedings if possible to show solidarity and demand justice (implied)

### Whats missing in summary

Details on the potential impact of community activism and advocacy efforts in seeking accountability and justice.

### Tags

#PoliceViolence #JusticeForErvoOtenyo #MentalHealthAwareness #Accountability #CommunityAdvocacy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about something
that occurred in Virginia and how things
will progress from here as best as I can tell you.
At time of filming, this is not something
that has really hit national news yet.
But I imagine it will become national news pretty quickly.
Frequent viewers of the channel can imagine
what that means based on this shirt and that statement.
You already know what happened.
Seven deputies in Henrico County, Virginia
have been charged via criminal information or criminal
complaint, one of the two, with second degree murder for their alleged role in
the death of a 28-year-old man, Ervo Otenyo. According to statements by the
prosecutor, he was being admitted to a hospital and during the process became
combative. According to the prosecution, he died of asphyxia and the prosecutor
said that it had something to do with the seven people, presumably the seven
charged, who were on top of it. A attorney for the family said something that I
think is definitely worth noticing. The public and experienced mental health
professionals alike will be appalled when the facts of this case are fully
made known. Mental health professionals. It's worth noting that some of the
documentation indicates that he was there on an emergency custody order,
which indicates mental health. That is probably going to play into this a lot
because this is a situation where it certainly appears that law enforcement
would have understood that it was a mental health issue. There is reportedly
video. I have not seen it. I don't know that it has been released at time of
filming. People have described it and the situation as brutal and inhumane.
Disturbing is another word that has been put out. Okay, so what happens from here?
Because of the way they were arrested, there still has to be a grand jury
process. So, they were arrested very quickly. This is all within about a week as far as the
investigation goes. But there has to be a grand jury process and indictments before anything else
moves forward. Some of them appear to be due back in court on the 21st, which is when we'll start to
to find out a little bit more about that process based on the statements from the prosecution
and the attorney for the family and the local FOP basically saying, we're waiting for more
information and hope that it clears the names of the deputies, but not coming out and just
claiming the innocence of the deputies the way the FOP often does, I have a feeling that
this is going to be, it's going to be bad.
And we're probably in for another long haul.
There's a whole lot of questions as far as the mental health issues.
how he wound up in a jail and then headed back to a hospital, and there's a whole lot
of unanswered questions at this point.
But this is one of those things that as it unfolds, it certainly seems like this is going
to be national news.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}