---
title: Let's talk about Tennessee and $2 billion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vhbA2uO3PTI) |
| Published | 2023/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tennessee is considering legislation that will make it impossible to change gender markers on official documents, essentially locking people into their birth sex.
- The bill's goal seems to be to prevent certain groups, like trans people, from claiming discrimination by removing them as a protected class.
- If the bill becomes law, Tennessee could lose billions in federal funding due to creating a mechanism for discrimination that goes against federal policies.
- While legally Tennessee might get away with this move, it could have significant financial consequences that its supporters might not fully grasp.
- Beau questions the motives of politicians in Tennessee and whether they are willing to risk losing substantial federal funding just to create a false enemy for political gain.
- The potential loss of two billion dollars in federal money due to this legislation is significant.
- Beau points out the moral wrongness of the bill but acknowledges that in the United States, financial considerations often carry more weight than morality.
- Beau underscores the importance of monitoring the situation closely to see how it unfolds.

### Quotes

- "This is something that the state can probably legally do and actually get away with."
- "Two billion dollars, it's a lot of money and that's really kind of what they're looking at losing."
- "But I think in this case, it's the United States. It runs on money."
- "The loss of funding may be a stronger argument in Tennessee than morality."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Tennessee's legislation to lock gender markers may cost billions in federal funding, prioritizing money over morality.

### Audience

Tennessee residents, Activists

### On-the-ground actions from transcript

- Monitor the progress of the legislation in Tennessee and stay informed about its potential impact (implied).
- Advocate against discriminatory legislation by raising awareness and mobilizing support (implied).
- Support organizations and groups working to protect the rights of transgender individuals in Tennessee (implied).

### Whats missing in summary

The full transcript provides more context on the financial and moral implications of Tennessee's proposed legislation, offering a comprehensive view of the potential consequences beyond just federal funding.

### Tags

#Tennessee #Legislation #TransgenderRights #FederalFunding #Discrimination


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Tennessee
and another piece of legislation
that appears to be moving forward there.
And what is going to happen if it does move forward?
This one is a little different than some of the other ones
that have been put forward recently there,
although it does have the same general goal,
which is to other people, to scapegoat people, to create tension among their own citizens,
um this particular bill in Tennessee it will basically make it impossible to change your
gender marker on official Tennessee documents. So you're going to have whatever sex you had at
birth and there won't be any changing it. Right now in Tennessee you're allowed
to do this but they're going to change it. The most likely goal behind this is
to make it impossible for certain groups, trans people, to claim that they are
being discriminated against. It kind of removes them as a class by doing this.
Now, the interesting thing about this is that the Fed saw this coming and if this
happens, if this moves forward, and again it's already made it through
one house, if it moves forward, if it becomes law, Tennessee stands to lose
billions in federal money. The federal government made it pretty clear that
you can't discriminate in this fashion and by creating a mechanism to
discriminate in that way they are very likely to lose a lot of money which will
impact Tennessee. In
in ways I think that
those who support this may not understand yet.
We are not talking about a small amount of money that they're going to be giving
up.
They're probably looking at it through a lens
of, well, we'll be able to do without it. I'm not so sure about that.
So
This is something that the state can probably legally do and actually get away with, whereas
the more recent ban on drag, I have real questions about what's going to happen as that moves
through the courts.
This one they could probably get away with from the court's point of view, but they're
going to lose a whole lot of money in the process. We're going to have to see
how committed to creating a fictitious enemy to get re-elected the politicians
in Tennessee really are. Two billion dollars, it's a lot of money and that's
that's really kind of what they're looking at losing. Now I don't think I
I need to say that from a moral standpoint, this is just all wrong.
But I think in this case, it's the United States.
It runs on money.
And the loss of funding may be a stronger argument in Tennessee
than morality.
So we're gonna have to wait and see what happens, but this is definitely one to keep your eye on.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}