---
title: Let's talk about a new Trump phone call in the Georgia case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qqMDWBYKCpU) |
| Published | 2023/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent reporting reveals a third recorded phone call involving former President Donald J. Trump and Georgia.
- The special purpose grand jury heard recordings, including a call to Georgia House Speaker David Ralston.
- Ralston was reportedly asked by Trump to convene the Georgia Assembly, which he was not enthusiastic about.
- Ralston is no longer able to comment, but the recording was played for the grand jury.
- Georgia Governor Kemp also mentioned Trump wanting the assembly convened for an audit of mail-in votes.
- The content of the third call is unknown, but based on previous statements, it may not be favorable news.
- Several individuals were recording calls with the former President, including the one in question.
- CNN reported the information, confirming the existence of the recording with five grand jurors acknowledging it.
- The revelations from these recordings suggest there could be more to the state's case.
- Charging decisions were said to be imminent in January by the District Attorney.
- There are conflicting rumors about when the grand jury for potential indictments began, either in February or March.
- Until the process is complete, the full details remain unknown, but there might be information trickling out from grand jury members speaking to the press.

### Quotes

- "It is wild to me the number of people who were recording a phone call with the then President of the United States."
- "This indicates that there might be a whole lot more to the state's case."
- "Charging decisions were imminent in January."
- "Until that process is completed, we're not going to know anything."
- "And that may lead to a clearer picture of what's going on."

### Oneliner

Recent revelations of a third recorded phone call involving Trump and Georgia add layers to the state's case, with potential indictments looming and information trickling out from the grand jury.

### Audience

Legal analysts and concerned citizens.

### On-the-ground actions from transcript

- Stay informed about updates on the legal proceedings (implied).
- Follow reputable news sources for accurate information on the case (implied).

### Whats missing in summary

Context on the potential implications for the legal case and political ramifications.

### Tags

#Georgia #DonaldTrump #GrandJury #LegalProceedings #ChargingDecisions


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Georgia
and former president Donald J. Trump and phone calls.
Because according to recent reporting there's a third one.
There is a third call that was recorded
that up until now, we didn't know about, um, and we're going to kind of go through
that and what it may mean and what we know and what we don't to recap, this
is all recordings that the special purpose grand jury reportedly heard.
The first is the one that we knew about dealing with team Trump, calling
an investigator, I believe with the Secretary of State's office there, and suggesting that
they would be praised if things went their way.
The next is the famous call to Raffensperger in which Trump wanted the votes found to alter
the outcome.
The third is reportedly a call to the Georgia House Speaker, David Ralston.
Now according to statements given at the time, Ralston said that Trump wanted him to convene
the Georgia Assembly and move from there and try to do something there.
And according to the statements at the time, Raulston made it clear that he wasn't really
enthusiastic about that idea.
He can't comment today because he is no longer with us, but the recording was apparently
played for the special purpose grand jury.
It's worth remembering that Kemp, the governor there in Georgia, said something very similar
about Trump wanting the assembly convened and perhaps in order to audit the mail-in
stuff.
And Kemp was like, yeah, I don't have the authority to do that.
So this adds another layer.
We don't actually know the content of this call, but given the other ones and the statements
at the time and the other statements at the time that kind of match.
We can assume that this is probably not good news.
It is wild to me the number of people who were recording a phone call with the then
President of the United States.
This information, it was reported by CNN, and the existence of the recording was confirmed
by them, and I guess five of the grand jurors had said, yeah, they heard it.
That seems pretty real.
How much it's going to have to do with the decisions and possible indictment, we're going
to have to wait to see.
But this is a surprise.
of what the special purpose grand jury heard, at least what we know that they heard, was
stuff that was pretty much out in the open already.
This indicates that there might be a whole lot more to the state's case.
We're going to have to wait and see how it all plays out.
It's worth remembering the DA said charging decisions were imminent in January.
There is the two competing rumors.
One is that a grand jury to actually get indictments began back in February, I think.
And then there's another one that said it couldn't even start until March.
I have no opinion one way or the other.
I'm just aware of the two rumors.
But until that process is completed, we're not going to know anything.
But it does seem like there might start to be a little information trickle when it comes
to people who were on the special purpose grand jury talking to the press.
And that may lead to a clearer picture of what's going on.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}