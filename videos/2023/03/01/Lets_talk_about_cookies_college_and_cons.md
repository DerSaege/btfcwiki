---
title: Let's talk about cookies, college, and cons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NGX-Lqp6TaQ) |
| Published | 2023/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Girl Scouts started Troop 6000 in 2017 to cater to children in New York City shelters, providing activities and support during their stay.
- Troop 6000 helps children transition to permanent housing, with the average stay in NYC temporary shelters being about 18 months.
- Support Troop 6000 by buying Girl Scout Cookies online; a link will be provided in the description to order at least four boxes.
- Project Rebound at California State University, Northridge, supports formerly incarcerated individuals in pursuing higher education.
- From 2016 to 2020, participants in Project Rebound had an impressive 3.0 GPA and a 0% recidivism rate.
- Donate to Project Rebound through a link provided below to contribute to college scholarships for program participants.
- The scholarship fund at Project Rebound requires 50 donors to unlock around $5,000 for college scholarships.
- Both programs, Troop 6000 and Project Rebound, are doing commendable work despite being geographically apart.
- Project Rebound's 0% recidivism rate is particularly noteworthy and speaks to the program's success in supporting formerly incarcerated individuals.
- Despite any reservations one may have about the Girl Scouts organization, supporting Troop 6000 through cookie purchases can make a significant difference to the children in shelters.

### Quotes

- "Support Troop 6000 by buying Girl Scout Cookies online."
- "Project Rebound boasts a 0% recidivism rate, a remarkable achievement."

### Oneliner

Support Troop 6000 and Project Rebound by buying Girl Scout Cookies online and donating to support formerly incarcerated individuals' college scholarships.

### Audience

Supporters and donors

### On-the-ground actions from transcript

- Buy Girl Scout Cookies online to support Troop 6000 (suggested)
- Donate to Project Rebound for college scholarships (suggested)

### Whats missing in summary

The full transcript provides additional details on the impactful work of Troop 6000 and Project Rebound, encouraging support through specific actions like purchasing cookies and making donations.

### Tags

#GirlScouts #Troop6000 #ProjectRebound #Support #Donations


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about cookies
and college and cons.
And we're going to help out two causes.
This is something that we would normally do via a live stream
and help out in that way.
However, both of the causes we're going to be helping,
it can't really be done that way.
And I'll explain why as we go through them.
So the first one, and the thing you
have to know to kind of start this off,
is that back in 2017, the Girl Scouts started Troop 6000.
Troop 6000 is a little bit different than most Girl Scout
Troops.
It caters to children in New York City shelters.
So this troop allows them to go to meetings each week,
give some activities, introduces them to kids
that have similar experiences.
They also have a transition thing
helps them as they move to permanent housing. Keep in mind the average stay in
New York City temporary shelters is about 18 months. So there's a support
network that is developed and it helps them move as they move forward. So how
can you help with this? Girl Scout Cookies. There will be a link down below.
You can buy them from anywhere. They will mail them to you. I think you have to
order four boxes, but they'll mail them to you. The reason we can't do this via
a live stream as we normally would is because if I wound up with, you know, a
thousand boxes of cookies, I just don't want to put that much weight on. The
other program is on the other side of the country. It's out in California. It's
It's called Project Rebound, and it is at California State University, Northridge.
And this is a program that provides a support structure for formerly incarcerated people
and gets them through college.
The interesting thing about this is that for the period of time from 2016 to 2020, which
was the period I could find the numbers for.
I have not seen numbers for 2021 or 2022 yet, but I don't think they'll be that much different.
the people in the program had on average a 3.0 GPA, which is great, which is great.
They also had a 0% recidivism rate. If you are familiar with what those rates
normally look like, you understand that that is nothing short of a miracle. There
will be a link down below if you would like to donate to that. The reason we
can't do that the normal way is because there is like $5,000 in scholarships or
something like that that is up for grabs if they get 50 donors. It's not about the
dollar amount, it's about the amount of donors and that goes to the college
scholarships there at the university. So that also can't be done the normal way
we do this stuff. So you have two programs on opposite ends of the country,
both that are, they're putting in good work. A 0% recidivism rate is just, I'm
almost skeptical of it. It's so low. But if you start looking into that project,
you'll find out that there's it's been around a while and it has done a whole
lot of good and then of course everybody likes Girl Scout cookies even if you are
one of the people who has an issue with the Girl Scouts or something that they
have done or some political stance that they may have taken certainly you can
kind of put that aside when you understand what this particular troop
does, and how much those meetings might mean to the kids to get to go to.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}