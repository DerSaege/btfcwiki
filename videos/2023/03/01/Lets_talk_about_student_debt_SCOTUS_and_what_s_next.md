---
title: Let's talk about student debt, SCOTUS, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HOXiBny6gBw) |
| Published | 2023/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring student debt relief, forgiveness, and the Supreme Court's involvement.
- Biden administration's push for debt forgiveness reaching the Supreme Court.
- Majority opinion predicts the conservative Supreme Court will strike it down.
- Possibility of justices ruling that the plaintiffs lack standing to sue.
- Potential outcomes if the court strikes down the debt forgiveness.
- Biden administration signaling that payments will resume if the forgiveness is struck down.
- Limited options for the Biden administration if the forgiveness is struck down.
- Debt forgiveness becoming a campaign issue for 2024.
- Democratic Party likely framing the issue in their favor if forgiveness is stopped.
- Republican Party facing consequences of stopping debt forgiveness.
- Impact on the Republican Party and their supporters if forgiveness is halted.
- Democratic Party needing significant control of the Senate to pass legislation for debt forgiveness.
- Speculation around the Supreme Court decision and its financial impact on millions.
- Uncertainty about the future financial stability of many due to the pending decision.

### Quotes

- "Debt forgiveness becoming a campaign issue for 2024."
- "Republican Party facing consequences of stopping debt forgiveness."
- "Democratic Party needing significant control of the Senate for debt forgiveness legislation."

### Oneliner

Exploring student debt relief, forgiveness, and the Supreme Court's involvement, with a focus on potential outcomes and political ramifications.

### Audience

Voters, policymakers, activists

### On-the-ground actions from transcript

- Contact your representatives to advocate for student debt relief legislation (implied).
- Stay informed about the Supreme Court decision and its potential impact (implied).

### Whats missing in summary

The full transcript provides detailed insights on the potential consequences of the Supreme Court decision on student debt relief and the political implications.

### Tags

#StudentDebt #Forgiveness #SupremeCourt #BidenAdministration #PoliticalImpact


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about student debt relief,
student debt forgiveness, and the Supreme Court.
Because lots of questions.
Most of the questions are, what's going to happen if?
And then fill in the blank.
And there's a lot of variations of that.
So we're going to kind of run through the different
possibilities.
If you have no idea what I'm talking about, the student debt forgiveness that the Biden
administration kind of pushed through, it went to the Supreme Court and arguments were
heard.
Okay, so the majority opinion of commentators and legal analysts, they think that the Supreme
Court's going to strike it down.
conservative Supreme Court, and they think they're going to strike it down and say that it was,
um, that while Biden had authorization under the Heroes Act, this is just such a big thing,
it needs to go to Congress first, violate separation of powers. Okay? There is a
not insignificant number of people who actually think something weird is going to happen,
and that is that a bizarre collection of justices are going to decide that the
people who brought it to the Supreme Court don't have standing, that they
they shouldn't have been able to sue, for lack of a better term. Conservatives
historically take a very narrow view of standing, and there were questions about it.
So that is a possibility.
If the court finds that they don't have standing, well, that's the end of it, and things move
forward at least for the time being.
If they strike it down, what happens next?
The Biden administration has made it pretty clear.
The signals have gone out that if it is struck down, not even that, if once the decision
comes down, that 60 days after that, payments resume.
The pause will end and people will have to start paying again.
So if it gets struck down, payments resume.
What options does the Biden administration have?
Not much.
Not really.
They can try to tweak it, but it'll just go back to court again.
If it is struck down, it becomes a campaign thing.
It immediately converts into a campaign issue for 2024.
And once again, the Republican Party becomes the dog who caught the car and doesn't know
what to do. In this case, you know, the debt forgiveness, this is 10 or 20
grand that would be coming out of the pockets of, I don't know, I think like 40
million Americans, something like that. And it's not going to be difficult for
the Democratic Party to say, we pushed this through, we did this, this is what
Biden did, the Republicans stopped it, they're going to be able to frame it that way pretty
successfully and pretty easily.
And that's a whole lot of people that are going to be upset by that.
And that's probably something that would be remembered around election time.
Then you have to wonder, is it going to be enough?
is if the Supreme Court strikes this down, basically this is only happening through Congress,
which means the Democratic Party has to get real control of the Senate.
Not just by one seat.
They have to have real control.
They have to be able to push through legislation.
I don't know if it's going to be enough for that.
But this will be like another recent Supreme Court decision for the Republican Party.
They might get what they think they want and then understand that their loudest supporters
are not indicative of the entire country because there's a whole lot of people who are going
to be financially impacted by this to a huge degree and it is it's this is going
to be the Republicans did this they stopped this and that's how it'll be
framed so the decision it's going to come down a while from now until then
there's going to be a lot of speculation. I mean, of course, I guess there could be
another leak, but that seems unlikely. So we have to wait, and we have to just sit
here and wait to see what happens to the financial future of tens of millions of
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}