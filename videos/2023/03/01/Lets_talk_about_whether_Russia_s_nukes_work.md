---
title: Let's talk about whether Russia's nukes work....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GEWDJkGRpvs) |
| Published | 2023/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the question of whether Russia's nukes still work and provides context on the maintenance process.
- Mentions the substance "fog bank" used in nuclear weapons, its importance, and the challenges faced in obtaining it.
- Shares insights on the difficulties of maintaining and modernizing nuclear arsenals, including the issue of part replacement.
- Raises concerns about the maintenance standards of Russia's nuclear weapons compared to Western countries.
- Explains the significance of even a fraction of the arsenal functioning for maintaining deterrence.
- Acknowledges the likelihood of some Russian nukes not working due to maintenance issues and corruption.
- Emphasizes that the concept of deterrence remains unchanged despite potential failures in some weapons.
- Comments on the misconception that a nuclear exchange can be won, stressing the catastrophic nature of such an event.

### Quotes

- "A small fraction of it functioning as intended is enough to cause untold amounts of devastation."
- "It is all bad. Please go watch War Games."

### Oneliner

Beau addresses Russia's nuclear arsenal maintenance, revealing concerns about functionality while stressing the catastrophic impact even a fraction can have.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Monitor and advocate for transparency in nuclear arsenal maintenance and modernization (implied)
- Support anti-corruption measures in military spending and equipment upkeep (implied)

### Whats missing in summary

Beau's detailed analysis and warnings about the potential risks and consequences of assuming all Russian nukes are functional.

### Tags

#Russia #NuclearWeapons #Maintenance #Deterrence #Corruption


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about fog and banks
and a question, whether or not things still work
after all of this time.
It's a question that has come up a few times.
My guess is that it has something to do
with Russia's new submarine.
So we're gonna answer the question,
but once I answer it, stick with me,
because wait, there's more.
And it's important context.
It's definitely something that you
need to keep in mind when you're using the answer
to this question for anything.
OK, and that question is, do Russia's nukes still work?
Eh, maybe, maybe.
Some of them undoubtedly do not.
some of them will fail. Nuclear weapons have to be maintained, they have to be
updated, they have to be... it's maintenance. Maintenance and modernization. And it's
more difficult than you might imagine. There is a substance called fog bank.
What is fog bank? Well that's classified. Like what's it exactly made of, its
composition, also classified. What does it do? Kind of also classified, but the
world learned about it back in 2007-ish when the United States was going through
its maintenance and modernization of its nuclear weapons because, well, they ran
into a problem. They didn't really remember how to make it. A lot of the
people who were involved in the creation of this material, they weren't around anymore.
So it caused delays in modernizing and maintaining America's nuclear arsenal.
I don't really have a good understanding of the internal mechanics of a nuke, not something
ever really studied. But how this was explained to me is that fog bank is an
interstage material and it's where fission turns to fusion. Super important
part, right? But they had real issues getting their hands on it. And this was
in the US. A country that does maintain its equipment. So did Russia really
maintain all of this stuff after all of this time? Maybe. Probably not. Not to the
same standards. If you were to use the gauge of the rest of Russia's equipment
Yeah, probably half of it is is going to have issues not just with the warhead
itself, but with everything else that goes along with it that was supposed to
be maintained over the years. And even with maintenance comes its own issues. If
you go to maintain something that was made 30 years ago, manufacturing
processes are different. You have to replace a part. Odds are that part's not
just going to be sitting around anymore. And when it is the newer production part
is put in, well, I mean everything from there kind of becomes theoretical. Is it
really going to work? I don't know. And this applies to everything that is
involved in strategic arms from the from the guidance to the rockets to I mean
everything literally everything. These arsenals are aging and Western
countries have engaged in what they call life extension just maintaining them
replacing parts that they can, stuff like that. We don't really know how well
Russia has done this, so let's just assume half of them don't work. Here's the
context that you have to keep in mind. The other half, that's totally enough.
That's enough. Even with, even assuming a massively degraded arsenal, it's enough.
It's enough to maintain deterrence. So while, yeah, those people who are talking
about this and there's a lot of discussion about it, those people who are
saying, yeah, this stuff probably doesn't work, understand it doesn't all have to
work. A small fraction of it functioning as it was intended is enough to cause
just untold amounts of devastation. So while technically correct, which is the
best kind of correct, it's worth remembering that the concept of
deterrence isn't altered by this just because of the sheer number of weapons
that are in the arsenals. So yeah, some of it will probably fail. Some of it will
probably fell. If the United States had issues getting material to engage in
its life extension program and maintain this stuff, it's a safe bet that
Russia did. And whether or not the money that might have been allocated to
maintain this equipment actually went to that is a whole other question because
there's a lot of corruption. I mean at this point you're talking about rockets
that probably have dry rot and hoses and stuff. So the people who are making this
point are correct. There are probably a lot of Russian nukes that do not
function. At the same time they don't need them all to function and that's the
important part. This argument is being made in large part, not all, but most people that
I've seen make this argument are those who think that somehow a nuclear exchange is winnable.
It is not. It is not. It's all bad. It is all bad. Please go watch War Games. Anyway,
That's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}