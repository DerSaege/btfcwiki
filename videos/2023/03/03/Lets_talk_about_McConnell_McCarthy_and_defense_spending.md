---
title: Let's talk about McConnell, McCarthy, and defense spending....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mVPNy8jwIy0) |
| Published | 2023/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- McConnell's position against defense cuts poses a problem for House Republicans.
- McConnell's strategic political acumen and history of quiet retaliation against those who oppose him.
- Republicans in the House want to cut the budget but need big numbers to show impact.
- McConnell's opposition to defense cuts is significant as he holds sway in the Senate.
- House Republicans face a challenge in finding budget cuts without upsetting their base.
- The pacing target for defense spending is China, signaling a race to maintain dominance.
- The possibility of increased spending by China leading to a similar response from the United States.
- The implication of an arms race with China and the need for the U.S. to keep pace.
- McConnell's stance on defense spending in the context of the near-peer contest with China and Russia.
- The potential consequences of increased defense spending and the implications for global dynamics.

### Quotes

- "He is somebody who when another politician, even if they're part of his party, you know, kind of takes a swipe at him, he'll just nod his head and quietly work to destroy them."
- "It's not an arms race, it's an arms jog."
- "Can't allow there to be a mineshaft gap and all that stuff."
- "Why doesn't McConnell want defense spending cut?"
- "Given the tone and rhetoric that we're hearing, if China starts spending more, the United States will start spending more."

### Oneliner

McConnell's opposition to defense cuts poses a challenge for House Republicans aiming to cut the budget, with the pacing target set on China in an arms jog scenario.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to express your views on defense spending (suggested).
- Stay informed about the implications of defense budget decisions (implied).

### Whats missing in summary

Insights on the broader implications of global defense spending dynamics.

### Tags

#McConnell #DefenseSpending #China #USMilitary #PoliticalStrategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about McConnell and McCarthy and the debt
ceiling and defense spending because, uh, McConnell indicated something
that might pose a problem for Republicans over in the house.
We we've talked a lot about McConnell over the years, and we have talked
how he is politically savvy. He is somebody who when another politician,
even if they're part of his party, you know, kind of takes a swipe at him, he'll
just nod his head and quietly work to destroy them. So, interesting thing
happened. McConnell has decided he is against defense cuts. Doesn't want any
cuts to defense spending. Actually kind of sounded like he wanted more defense
spending. Republicans in the House want to cut the budget, at least somewhat, and
they need big numbers to make it seem like they did anything. Biden goaded them
them into booing the idea of cutting Social Security, Medicare, stuff like that.
The only other place that they can realistically get big numbers easily is defense spending.
McConnell has basically just said that he doesn't want that.
He doesn't want defense cuts.
in the Senate. Let's be honest, he is the Senate, at least the Republican side of the aisle.
If he's not in favor of defense cuts, they're probably not going to happen,
because anything the House does has to go through the Senate.
Maybe McCarthy shouldn't have taken a shot at McConnell, like right as soon as he had to go
go through all those votes. Probably wasn't a good idea. Now, that leaves House Republicans
in a very unique situation where if they actually want to get any real cuts as far as the debt
ceiling thing goes and try to tie it together, they're going to have to work for it. They're
going to have to actually go into the budget and try to find programs that can realistically
be cut without upsetting their base.
It might be harder than they're imagining.
So all of that is the funny stuff.
Now the not so funny stuff.
Why doesn't McConnell want defense spending cut?
He's talking about accepting the realities as they are.
Get ready to start hearing a new term a lot, pacing.
That is our pacing target.
We need to keep pace with.
This is the country we're pacing.
It's not an arms race, it's an arms jog.
The pacing target will be China.
McConnell is obviously also talking about Russia and Ukraine when it comes to defense
spending, but the tone is more of an acknowledgement of the near-peer contest and signaling at
least the Senate's willingness to up defense spending to maintain a high degree of dominance
over the Chinese military. So they will keep pace with them.
Can't allow there to be a mineshaft gap and all that stuff.
That's how quickly it happened.
So that part's not
so
not so funny.
That's not good news to be honest.
China may or may not
start spending more.
Given the tone and rhetoric that we're hearing,
if China starts spending more, the United States
will start spending more.
And we will be in an arms jog.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}