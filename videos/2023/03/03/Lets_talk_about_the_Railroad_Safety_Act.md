---
title: Let's talk about the Railroad Safety Act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sED4B7_MoTU) |
| Published | 2023/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Railway Safety Act of 2023 and its historical context.
- Mentions a bipartisan group of six senators willing to impose new regulations.
- Notes that Schumer has endorsed the Act.
- Details the provisions of the Act, including increased fines for railway companies and new safety regulations.
- Expresses skepticism about the effectiveness of advanced notice to state emergency agencies.
- Questions if the regulations will limit the size and weight of trains.
- Points out the importance of listening to rail workers' concerns.
- Suggests that governments should heed advice from those who do the job, especially regarding hazardous materials.
- Acknowledges the effort of the senators in attempting to improve railway safety.
- Encourages considering input from those directly involved in the industry.

### Quotes

- "Maybe it might be a good idea to listen to the people who actually do the job."
- "Seems like it would be a good idea."
- "It's just a thought."
- "Y'all have a good day."

### Oneliner

Beau explains the Railway Safety Act of 2023, underlining the importance of listening to railway workers for safer operations while discussing new regulations and the need for government responsiveness.

### Audience

Legislators, policymakers, railway workers

### On-the-ground actions from transcript

- Contact your representatives to voice support for regulations enhancing railway safety (suggested).
- Stay informed about developments in railway safety regulations and advocate for transparent processes (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the Railway Safety Act of 2023, the involvement of senators in improving railway safety, and the significance of considering frontline workers' perspectives for effective regulation.

### Tags

#RailwaySafety #Regulations #GovernmentResponsiveness #WorkerInput #Legislation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
the Railway Safety Act of 2023 and what's in it
and how there's a little link to history in it,
a very recent history, and how sometimes
it might just be better to listen to people.
Okay, so the good news, it does appear
that at least some in Congress are willing to impose some new regulations and try to
make things safer when it comes to the railways.
There is a group of six senators, it's bipartisan, and in addition to those six, it looks like
Schumer just like immediately endorsed it.
Haven't heard anything from the House yet, but this looks like something that will probably
go through, we hope, at least in some fashion.
So what's in it?
It raises the fines for railway companies that don't follow the rules, basically.
It adds a whole bunch of new safety regulations, dealing with everything from blocked crossings
to detection equipment, it requires railways to provide advanced notice to state emergency
agencies as they move through, which to be honest, I mean, that's cool and everything,
but with the amount of rail traffic that exists, the odds are that those are going to be pretty
much ignored.
I like the idea, but I don't know that that's going to do a
whole lot of good.
There's also going to be new rules about the size and weight
of the cars, of the trains.
I wonder if that means they're going to limit the amount of
cars on individual trains.
There's also some regulations about a minimum time
that inspectors are going to spend performing safety
inspections on cars.
And all of this combined should make things better.
If a lot of that sounds real familiar,
Because a whole lot of this was mentioned by the rail workers, the unions, that were
wanting to go on strike recently and the government didn't let them, kind of forced the labor
dough on them.
Yeah, a whole lot of this was mentioned by them and they were concerned about it in addition
to the scheduling issues.
So maybe it might be a good idea to listen to the people who actually do the job and
kind of take their advice when you're talking about, I don't know, hazardous material.
Seems like it would be a good idea.
It would be better if the government would take that kind of advice before something
that happened. But this particular group of senators, at least they're
making an attempt to kind of shift things here, which is going to be a welcome
development.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}