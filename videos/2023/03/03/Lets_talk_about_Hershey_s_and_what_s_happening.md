---
title: Let's talk about Hershey's and what's happening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2oI5JrvZxvA) |
| Published | 2023/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a subset of Americans upset with a candy company due to a marketing move involving pronouns.
- Mentions how conservative thought leaders are now upset with Hershey's and asking followers not to buy their products.
- Explains that Hershey put a picture of a woman on the wrapper with "her" and "she" for International Women's Day.
- Comments on the absurdity of conservatives being upset over pronouns in Hershey's name and marketing.
- Suggests that conservative brands, built on manufacturing outrage, are running out of things to be upset about and are now battling candy companies.
- Points out that most candy companies have legitimate reasons for criticism, but it generally does not involve their wrappers or marketing strategies.
- Speculates that marketing companies working with candy companies intentionally provoke a small group of vocal conservatives for free advertising.
- Notes that the demographic appreciating inclusive marketing is expanding while those against it are shrinking.
- Expresses disbelief at conservatives focusing on pronouns in candy wrappers to manufacture outrage among their base.
- Humorously suggests that conservatives should be mad at Mike and Ike's candy due to the names implying two guys alone together.

### Quotes

- "There's not. It's at the point now that conservative brands who have built themselves on manufacturing outrage are running out of things to manufacture outrage about."
- "It is just mind-boggling to me that this is what conservatives have latched onto in an attempt to manufacture outrage among their base."
- "Let's see if we can do this again. I mean, think about the message that that's sending. Two guys, alone together. Obviously, that's the candy that y'all should be mad at next."

### Oneliner

Conservatives upset over pronouns in Hershey's marketing reveal a trend of manufactured outrage losing traction.

### Audience

Social media users

### On-the-ground actions from transcript

- Support candy companies promoting inclusivity (implied).

### Whats missing in summary

Cultural commentary on manufactured outrage and shifting demographics influencing marketing strategies.

### Tags

#Candy #Conservatives #Marketing #Inclusivity #ManufacturedOutrage


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to, once again, talk about candy.
We're going to talk about candy in the United States
and how a certain subset of Americans
is once again upset with a candy company.
You might remember not too long ago,
there was a large subset of conservatives
who were really mad at M&Ms due to branding changes, and then M&Ms put out a satirical
press release that a lot of conservative thought leaders did not understand was satire, and
then they declared victory over the M&Ms only to be really surprised when the M&Ms came
back during the Super Bowl.
I made a video about it at the time, talking about how it was satire and so on and so forth.
And at the end of it, I made a joke.
I held up a Hershey bar and said that if conservatives really wanted to stop, you know, the woke
agenda, they would go after Hershey's because it's in their name, her and she, and pronouns
are bad and all of that stuff, and it was a joke.
At least I thought it was.
So conservative thought leaders are now upset and asking their followers not to buy Hershey's
because there are pronouns in Hershey.
To celebrate International Women's Day, Hershey put a picture of a woman on the wrapper with
her and she kind of highlighted and that has just sent them over the edge.
I don't even know what to say to that.
I understand that there are going to be people that say there's more to it, but there's
really not.
There's not.
It's at the point now that conservative brands who have built themselves on manufacturing
outrage are running out of things to manufacture outrage about. So they are
going to battle with candy companies and losing generally. And the
weird part about it is most candy companies, they have like legitimate
reasons for somebody to actually be upset with them. They really do. A
a whole lot of them, doesn't generally include their wrappers or their marketing strategies.
Because at this point, you have to acknowledge that the marketing companies, the ad companies
that work with these candy companies, they're not unaware of what happens on social media.
They're counting on it.
At this point, it's probably just a strategy to anger this small group of conservatives
that happens to be very vocal.
So when they get out there and start screaming about, you know, don't buy their product,
all it does is provide the candy company with millions of dollars worth of advertising.
So it's almost now like it's intentional because the marketing companies know that
one demographic, the people who would like a rapper that shows pronouns, that demographic
of people is expanding while those who would be angry about it is shrinking.
So they intentionally anger those who would be angry about it to capitalize on their social
media accounts and get millions upon millions of dollars in free advertising.
It is just mind-boggling to me that this is what conservatives have latched onto in an
attempt to manufacture outrage among their base. I mean, I say it's mind-boggling, but
I guess I should have seen it coming, since I literally said. Mike and Ike's. Those candies,
Ike and Mike's, whatever they're called. Obviously, that's the one that conservatives
should really be mad about. Let's see if we can do this again. I mean, think about the
message that that's sending. Two guys, alone together. Obviously, that's the
candy that y'all should be mad at next. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}