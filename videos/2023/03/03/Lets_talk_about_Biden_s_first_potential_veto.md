---
title: Let's talk about Biden's first potential veto....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5WsQnV5KwRw) |
| Published | 2023/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Possibility of Biden exercising veto power for the first time is discussed.
- A resolution has passed through both the house and Senate regarding overturning a rule related to retirement investment managers.
- The resolution overturns a rule allowing consideration of ESG scores when picking investments.
- ESG stands for Environmental, Social, and Governance, reflecting a company's responsible decisions.
- Republicans are against this rule, labeling it as "woke capitalism."
- Republicans are openly going against the free market, a departure from their traditional stance.
- The economic benefit of investing in companies with high ESG scores is debated.
- Biden is likely to veto the resolution, with Congress potentially attempting an override.
- The Republican Party's shift away from free market principles is observed.
- The future actions and decisions regarding the resolution are uncertain.

### Quotes

- "Republicans are openly coming out against the free market."
- "This shows a glimpse at where the Republican Party is headed."
- "Biden has indicated that he is going to override it."

### Oneliner

Biden may exercise his veto power for the first time against a resolution overturning a rule related to ESG scores, revealing a shift in the Republican Party's stance on the free market.

### Audience

Policy Observers, Political Analysts

### On-the-ground actions from transcript

- Contact representatives to express opinions on the resolution (suggested).
- Stay informed about political developments and decisions (implied).

### Whats missing in summary

Full context and depth of analysis on the implications of the resolution and the potential impacts on investment practices.

### Tags

#Biden #VetoPower #ESG #RepublicanParty #Investments


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the possibility of Biden exercising
his veto power for the first time.
Something he hasn't done so far in the administration, but it looks like it's coming.
Congress has sent up a resolution.
This is something that has already made it through both the house and the Senate.
It made it through the Senate because of, you know, a Democratic Senator being out and
mansion and I think the Senator, the Democratic Senator from Montana crossed and voted with
Republicans.
What this resolution does is it overturns a rule that allows retirement investment managers
to consider an ESG score when picking investments
for their clients.
What's an ESG score?
Environmental, Social, and Governance.
Basically, the score is supposed to reflect a company making
conscious decisions when it comes to societal change
and governance and the environment, as the name suggests.
It means that the company is at least somewhat a responsible corporate entity, that or they
have a really good PR department.
Now it's worth noting that the rule they're overturning, that Republicans are trying to
overturn. It doesn't require that an investment fund manager or whoever takes
this into consideration. It just allows it. And I find that odd. Republicans are
labeling this as woke capitalism, which is, I mean, that's a term. That is
definitely a term there, but at its root it's the Republican Party openly coming
out against the free market, which is weird. It's becoming less weird, less odd
for the Republican Party to take a stance like that, but traditionally the
Republican Party has kind of, at least in theory and at least in rhetoric, wanted
the free market to do its thing. Now one thing that should, in my opinion, play
into this is whether or not companies that have high ESG scores are
better investments. Do they outperform the market? And in the short term, I
I couldn't find anything. I mean I found
conflicting information. Some sources say that they do,
some sources say that they don't. I'm certain that there's going to be
you know a stock person that will show up down below and let us know
but I could not tell. Now
theoretically it would seem like
companies that pay better attention to the environment to
societal change, stuff like that, might be companies that over the long term have
a better grasp on the market. But again, that's the long term and we don't know
that yet. So as far as the economic benefit, I don't know that there is any
hard information on it. I'm hoping somebody will correct me on this or at
least provide it. But this is what the Republican Party is pushing up to Biden,
a battle against quote, woke capitalism. Now Biden has indicated that he is going
to override it. And that would make sense. The possibility does exist of
Congress then overriding his veto. It's something they can attempt. I do not
think that they have the votes to do that. It doesn't seem likely that there's
going to be that many Democrats cross the aisle in either house, so if he does
decide to veto it, that'll be the end of the conversation, at least for the time
being, even if Republicans do attempt to override that veto. But this shows a
glimpse at where the Republican Party is headed. No longer about the free market,
no longer really catering to people being able to invest their money where
they want to, which is weird because it's kind of been a long-standing thing that
you know the individual should be able to invest their money better than the
government. That's like their whole thing behind their opposition to Social
security but I guess that has changed so we'll we'll wait and see what the
administration does I'm fairly certain it's going to be vetoed and I don't
believe that Congress can override it anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}