---
title: Let's talk about the FDIC and bank assessments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HdMFBGcYKKY) |
| Published | 2023/03/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the role of the FDIC in covering failed banks and how it is essentially insurance for bank accounts.
- Mentions that recent bank failures have cost the FDIC around $23 billion, prompting the need to shore up their fund.
- Notes that the FDIC determines the assessment rates that banks have to pay, with the current plan being to have larger banks contribute more.
- Indicates that the goal is to ensure that smaller community banks are not heavily impacted by the fund shoring up efforts.
- Suggests that larger banks may face higher assessment rates compared to smaller banks, potentially shifting the burden away from community banks.
- Emphasizes that the FDIC seems to be aiming for larger banks to bail themselves out rather than passing the cost to Americans.
- Mentions the pressure on the FDIC to ensure that smaller banks do not bear the brunt of the fund restructuring.
- Points out that larger banks may see an increase in their insurance payments to cover the fund's stabilization.
- Concludes with the idea that industry leaders, i.e., big banks, are likely to be the ones primarily responsible for covering the costs of shoring up the FDIC fund.

### Quotes

- "The FDIC is insured by banks."
- "They're going to stick it to the big banks."
- "The FDIC is basically going to really make the banks bail themselves out."

### Oneliner

Beau explains how the FDIC is ensuring larger banks, not Americans, cover the costs of shoring up its fund, protecting smaller community banks from adverse impacts.

### Audience

Financial enthusiasts, bank customers.

### On-the-ground actions from transcript

- Contact your local community bank to understand how they may be affected by the FDIC's fund stabilization efforts (implied).
- Monitor news updates on the FDIC's assessments and fund restructuring to stay informed about potential impacts on different banks (implied).

### Whats missing in summary

Details on the potential consequences for larger banks and the broader banking industry from the FDIC's plan to have them bear the costs of fund stabilization. 

### Tags

#FDIC #Banks #Insurance #FinancialSecurity #CommunityBanks


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the FDIC and banks
and assessments and poetic justice.
So as most people know, recently there
were a couple of banks that did not do so well.
They ran into major issues, and the accounts
had to be covered by the FDIC, which is pictured as insurance
for your bank account.
The cost to the FDIC for those bank failures, right now,
I want to say it's at like $23 billion.
Now, the fund that the FDIC has, they have to shore that up.
They have to get money to put back into that.
And this was something when all of the insurance stuff was going on.
This was something that a lot of Americans were worried that they were going to have
to pay for.
That's not how it works.
The FDIC is insured by banks.
they get a kind of an insurance rate that they have to pay.
Interesting little tidbit is that the FDIC
has a whole lot of leeway in determining which banks get
assessed, how much they pay, their rates, so on and so
forth.
They have indicated, the reporting
indicates that the way they are currently
looking at shoring up this fund is
to steer a large portion towards what they
are calling industry leaders.
They're going to stick it to the big banks.
That's who's going to pay for it.
Oh, no.
The goal here is to make sure that community banks
and stuff like that that don't have as much money
are not adversely impacted.
So the current line of thinking with this
is that the assessments for larger banks
are going to be at a higher rate than smaller banks.
And we're not just talking about total dollar amount.
That part's obvious.
But also, let's say it's a percentage.
Small bank may get 5%.
Big bank may get 15%.
That's not actually how it's done.
That's for illustrative purposes.
But it does appear that the FDIC is basically
going to really make the banks bail themselves out
this one which is I mean that's a nice change right that's that's that's
interesting this isn't finalized yet but there's a lot of political pressure to
make sure that smaller banks don't have to carry the burden here and the FDIC
line of thought at this point in time is to make the industry leaders come up
with cash and the way this works is I want to say it's every quarter they have
to make what amounts to an insurance payment and that those larger banks the
well their premium is going to go up. Anyway it's just a thought y'all have a
a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}