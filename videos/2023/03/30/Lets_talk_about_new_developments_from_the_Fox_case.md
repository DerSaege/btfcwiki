---
title: Let's talk about new developments from the Fox case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vZnmOPdpVkM) |
| Published | 2023/03/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dominion wants key figures from Fox News, like Rupert Murdoch and hosts Carlson, Hannity, Ingram, to testify at trial, causing potential issues for Fox.
- Fox is likely to vehemently oppose this request, using any means necessary to prevent it.
- Dominion believes having these figures testify is critical to their case and will help connect the pieces.
- Fox facing their hosts being questioned in court could be damaging to their brand and case.
- Fox has been successful in keeping certain information away from their viewers due to their echo chamber.
- The potential trial coverage could expose more viewers to the hosts' statements, impacting Fox's brand significantly.
- The outcome of whether all key figures will have to testify or just some remains uncertain, but Fox is expected to resist fiercely.
- The case is shaping up to be very intriguing, with the possibility of intense legal battles ahead.

### Quotes

- "Dominion wants people like Rupert Murdoch and the Fox News hosts, Carlson, Hannity, Ingram, all of them, to testify at trial."
- "Fox is going to fight this with everything that they have."
- "Having a record in a trial of one of their hosts being asked, well, why did you say this in this message is that's going to be damaging not just to the case but to their overall brand."

### Oneliner

Dominion seeks testimony from key Fox News figures in a case against Fox, potentially damaging their brand and sparking intense legal battles.

### Audience

Legal observers

### On-the-ground actions from transcript

- Watch the legal proceedings closely to understand the impact on media accountability and transparency (implied).
- Stay informed about the developments in the case and the responses from Fox News (implied).

### Whats missing in summary

Full context and nuances of the legal battle between Dominion and Fox News.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about a little update on the Fox
News case, because there has been an interesting
development, a request, from Dominion.
And it appears like the judge, at least at this point,
may be agreeing to it, we don't know yet, but early indications kind of say the judge
is at least entertaining it.
Dominion wants people like Rupert Murdoch and the Fox News hosts, Carlson, Hannity,
Ingram, all of them, to testify at trial.
would certainly cause issues for Fox. I would imagine that Fox is going to fight
this with everything that they have. They will certainly use any and all tricks
that they have at their disposal to stop this from occurring. From Dominion's
point of view, bringing them and asking them about the messages, the quotes, it's
important to their case and it helps kind of put the pieces together and it
allows that information to be seen right there, not in some deposition, but right
than asking the hosts about the information that was on their shows.
For Fox, this is horrible, horrible.
Having a record in a trial of one of their hosts being asked, well, why did you say this
in this message is that's going to be damaging not just to the case but to
their overall brand. I mean right now they're doing a decent job of kind of
keeping it away from their viewers and a lot of their viewers because they're in
an echo chamber they don't even know some of the things that were said about
about the coverage, some of the things that were said about them.
But if it's done at trial
it's going to get a lot more coverage and
when it's the host, that brand, that personality
that's talking about it, I mean that's gonna be something else.
So we don't actually know what is going to happen, whether or not the judge is
going to say, yes, all of them are going to have to come in, or maybe just some of them.
But we can pretty much guarantee that Fox is going to fight it tooth and nail.
They're going to do everything they can to stop that from happening.
But that case is starting to look like it's going to be very interesting.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}