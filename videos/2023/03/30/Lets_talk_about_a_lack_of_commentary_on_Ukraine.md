---
title: Let's talk about a lack of commentary on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Jy1kO_3U-Z0) |
| Published | 2023/03/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commentary on Ukraine has significantly decreased, causing confusion among followers.
- Speculations arose due to the sudden silence on the topic.
- The situation in Ukraine is stable, with no major negative events occurring.
- Many commentators have discussed equipment coming from the West for a potential counteroffensive in the spring.
- With the arrival of equipment and the onset of spring, commentators are becoming more vague and less specific regarding future events.
- Commentators may be more cautious about what they say, leading to a decrease in detailed information.
- Some commentators might completely stop discussing the topic to avoid incorrect predictions.
- The shift in commentary is driven by a desire not to be responsible if something goes wrong.
- Commentators refrain from providing detailed insights to prevent premature disclosures of potential military strategies.
- The decrease in specific commentary is likely to continue until a major development occurs.

### Quotes

- "The situation there is pretty stable. I wouldn't say it's fine."
- "Commentators, they don't know when that's going to happen."
- "It's just a desire to not be the reason something went wrong."

### Oneliner

Commentary on Ukraine decreases as commentators become cautious about potential disclosures of military strategies.

### Audience

Observers of geopolitical commentary.

### On-the-ground actions from transcript

- Stay informed about the situation in Ukraine and geopolitical developments (implied).

### Whats missing in summary

Context on the impact of commentator caution and the anticipation of future developments in Ukraine.

### Tags

#Ukraine #Commentary #Geopolitics #MilitaryStrategy #Caution


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about commentary about Ukraine,
or I should say, a lack of commentary about it.
Because I got an interesting question from somebody,
and I would imagine there are other people who
are in a similar situation and are wondering the same thing.
And I'll just kind of shed a little bit of light on it
on what it might be and what else you can expect.
Hey, what's up?
Did something bad happen?
Why did all of you go quiet?
I follow five people who have provided pretty frequent
commentary on Ukraine, and all of you
just stopped talking about it all of a sudden.
I looked at other people, and nobody is getting
into detail about anything.
My only thought is that something finally
went wrong in Bakhmut.
What's going on?
And then there's actually some names of the people
that they follow.
OK, no.
The situation there is pretty stable.
I wouldn't say it's fine.
It's as fine as a situation like that can be.
It is pretty stable.
Nothing horrible happened.
Nothing went the wrong way.
Nothing like that.
Over the last few months, most commentators that you probably watch, they've talked about
a couple of things.
One is certain equipment coming from the West, and how that equipment is needed for a counteroffensive.
And most people assumed that counteroffensive would take place in the spring.
A whole lot of that equipment has arrived, or is arriving, like literally as I film this.
And it's spring.
There are probably a number of people who are going to become more vague and less specific
information, particularly those who provide estimates about what is going to
happen in the future. They may become way less detailed about it. There's a... the
The likelihood is that people are being more conscientious about what they say.
If somebody has something that they notice about movements or anything like that that
is particularly insightful, they'll probably keep it to themselves right now.
And that's what's going on.
Out of the names that I recognize, I'm almost positive that's the case with two of the
people. So there's that. You will see commentators, particularly those who say
this could happen or this could happen or this could happen, that stop doing
that or stop doing it as well become less detailed. You may have some that
just completely stop talking about the topic altogether so they don't mess up.
you might have some, well they're just wrong. They thought something was gonna
happen and that's not what occurred. I would expect that there might be some
commentators that do that and don't let it hurt your feelings. If that's the
case, if one of them does that, and I'm fairly certain that one of them will,
It's an acknowledgment that YouTube can be accessed from different places.
So that is probably what you're noticing.
It's just a desire to not be the reason something went wrong.
So people will be less specific, they will be less detailed, they won't provide as much
insight into where they may go, stuff like that.
And it all has to do with what people have talked about for the last few months as far
as a possible Ukrainian counteroffensive.
And understand that commentators, they don't know when that's going to happen.
So when they start to see pieces in place, they tend to get quiet because they don't
want to say, oh, there's an opening here.
They could do this and that be the plan and then broadcast it on YouTube like right before
it happens or something like that.
So that's probably what you're noticing.
It will probably continue for some time until there is a major development.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}