---
title: Let's talk about how Trump was indicted in New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7ezCGzTzE-8) |
| Published | 2023/03/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Donald J. Trump, a leading contender for the Republican nomination in 2024, has been indicted in New York on charges related to a felony, allegedly involving money changing hands and falsification of business records for hush money payments.
- A grand jury is reportedly looking into a second payment in relation to Trump's case, possibly uncovering additional criminal violations.
- Allen Weisselberg, the former CFO of Trump Organization, has changed lawyers from Trump's legal team, hinting at a potential cooperation with authorities.
- Change in legal representation often indicates a shift away from being a potential defendant towards cooperation with authorities.
- Beau advises to stay level-headed amidst the upcoming flurry of speculation, fake outrage, and cheering, reminding everyone to stick to verified facts and not get carried away by emotions.
- Multiple other criminal investigations are ongoing, indicating that the recent developments might be just the beginning of more to come.
- Beau urges everyone to rely solely on verified information and not give in to speculation or emotions.
- Expect a turbulent week ahead filled with speculation, outrage, and excitement surrounding these developments.
- More information is expected to surface soon, but Beau wanted to share this update promptly.
- Beau signs off with a reminder to stay grounded and have a good day.

### Quotes

- "Get ready for a bumpy week because there will be a lot of speculation."
- "Don't let your emotions get away from you on here."
- "Everybody just go off of what is known, not what is speculated."
- "There are multiple other criminal investigations going on."
- "Stick to what is reported as fact and nothing more."

### Oneliner

Former President Trump indicted in New York, potential cooperation hinted, stay grounded amidst speculation and outrage.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Stay updated on verified information to avoid being misled by speculation (implied)
- Keep a level-headed approach and avoid getting swept away by emotions (implied)

### Whats missing in summary

The detailed nuances and potential implications of the indictments and ongoing investigations.

### Tags

#Trump #Indictment #LegalSystem #Speculation #StayInformed


## Transcript
Well, howdy there, internet people.
Let's bow again.
Former president Donald J.
Trump, leading contender for the Republican nomination for president in 2024, has been
indicted, has been indicted, um, reporting at this point in time, information is, uh,
sparse, but the reporting suggests he was indicted in New York and, uh,
While the exact charges are not known, early information suggests one of them is a felony.
The charges are reported to be related to the whole money that changed hands and the
falsification of business records dealing with it, as far as the hush money payments.
In related news, the grand jury up there is reportedly looking into a second payment.
We talked about it the other day and talked about how there were a number of options on
why things were moving the way they were, moving as slowly as they were, and one of
them was that they had uncovered another potential criminal violation and that they were looking
into that.
That does appear, according to reporting, to be true at this point.
In more related news, Allen Weisselberg, the former CFO of Trump Organization, has changed
lawyers.
Up until very recently, Weisselberg was being represented by Trump lawyers, lawyers from
Trump World.
That is no longer the case.
Generally speaking, while there's no evidence of this at this point, but generally speaking,
when this occurs and the person changes lawyers and it's moving away from a potential defendant,
it's an indication that that person may have decided to cooperate.
So those are the developments as we know them right now.
There is undoubtedly going to be a whole lot more information coming out, but I figured
I would put this out as soon as possible.
Get ready for a bumpy week because there will be a lot of speculation.
There will be a lot of fake and real outrage.
will be a lot of people cheering. But most importantly, there's going to be a
lot of speculation. Temperate. Go off of what is reported as fact and nothing
more. Don't let your emotions get away from you on here. Keep in mind, there are
multiple other criminal investigations going on and this might just be the
first of many future developments when it comes to this. So everybody just go
off of what is known, not what is speculated. So anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}