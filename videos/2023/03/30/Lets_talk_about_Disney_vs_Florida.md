---
title: Let's talk about Disney vs Florida....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YBT4MHyN_oQ) |
| Published | 2023/03/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Disney and Florida are at odds over the management of the area surrounding Disney World.
- For over half a century, Disney had control through a special district, but Florida wanted to change that.
- Beau doubts if Florida's decision-makers have ever met Disney's lawyers and describes them as cold and calculating.
- Disney needs control to maintain the perfection and brand of Disney World.
- Disney's lawyers struck a deal giving Disney control over most things, surprising the new board.
- The deal is likely to stand as everything was done in the open, even though it went unnoticed.
- Florida may not have much recourse to challenge the deal, which gives control to Disney in perpetuity.
- The deal's length and implications are uncertain and may fuel future conflicts.
- The situation between Disney and Florida is set to continue brewing.

### Quotes

- "Disney needs control to maintain the perfection and brand of Disney World."
- "Florida may not have much recourse to challenge the deal."
- "The situation between Disney and Florida is set to continue brewing."

### Oneliner

Disney and Florida clash over control of Disney World's surroundings, with Disney's lawyers striking a surprising deal that may have lasting effects.

### Audience

Florida residents, Disney enthusiasts

### On-the-ground actions from transcript

- Contact local representatives to voice concerns about the deal and its implications (suggested)
- Stay informed about any updates or changes regarding the management of Disney World's surroundings (implied)

### Whats missing in summary

Additional context on the potential long-term impacts of the deal and how it may affect the overall Disney World experience.

### Tags

#Disney #Florida #Control #Management #Deal #LegalIssues


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the situation between
Disney and the state of Florida, and what is going on
there, the recent developments, and the news there.
If you have no idea what I'm talking about, there has been
a situation that has developed between the state of Florida
Disney over how the area surrounding Disney World is managed. For a very long
time, like more than half a century, Disney had a whole lot of control
through a special district. The state of Florida decided that that wasn't the way
they wanted it done. Now they I guess they did create a new board to manage
that. About a year ago I put out a video talking about this and when the news
broke I had a whole bunch of people send me messages about it. There were two main
points in that video. One was me doubting the fact that many of the
people behind Florida's decisions here had actually ever met lawyers that work
for Disney. I have. I have. And I kind of walked away with the impression that the
the lawyers that work for Disney are the inspiration for the villains in the
cartoons. They are cold, they are calculating, and they think, they think
long-term. And they do. They just seem very without compassion. And I met them
vacation, when they were on vacation, they still managed to cast that vibe. The
other thing, the second point in that video that matters, is the fact that
Disney can't manage Disney World without running the infrastructure, without
having the control that it had. Disney World can't be Disney World. It's... if
If you've never been there, everything is perfect.
The surrounding areas, everything is perfect.
Everything is managed.
In fact, there are certain times of year
when there's not a lot of people,
and if you're there driving through the area
early in the morning, it's kind of creepy.
It seems like a set from a movie.
But it's because everything is so particular.
And that's their brand.
That's why it's the most magical place on earth.
That's why people travel from all over the world
to go there.
Disney has to have that control for that place
to exist in the way that it does.
And if they were to let it fall,
it would damage their brand.
So what's the news?
Disney's lawyers, in fact, did not let it go.
They, they got a new deal with the old board before the old board lost its power and that
deal basically gave Disney control over not everything but most things and enough for
them to maintain Disney World as Disney World. This was a surprise to the new
board who believes that now they really just don't have any power and it
definitely caused a lot of conversation. I don't know that much about this type
of law. So I called a friend and I asked, I was like, I mean, can they really just kind
of sneak this by like this? And the person was like, no, not in Florida. You can't do
something like that in secret and then just spring it on people at the last moment. You
have to, and the whole time I can hear keys tapping. You have to do it in public. There
There has to be a public notice, dah, dah, dah, dah, dah, dah.
And all of a sudden, they're like, oh, no, they did it.
And they were checking to see if it had been done in accordance with Florida law.
And apparently, everything was done out in the open, just nobody noticed.
So my friend's opinion is that, yeah, this will likely stand.
There isn't a whole lot that the state of Florida can do to avoid the whole thing.
They may argue pieces of it or whatever, but it's going to be really hard.
One of the more interesting parts about the deal is that it gives it to Disney in perpetuity.
And then there's a bit about the descendants of King Charles and just all kinds of stuff
as far as the length of how long this thing is going to last.
So definitely an interesting development and it is something that is absolutely certain
to fuel the future culture war stuff.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}