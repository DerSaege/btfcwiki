---
date: 2023-05-03 03:43:47.695000+00:00
dateCreated: 2023-03-20 11:11:07.909000+00:00
description: Significance of recent quotes that have surfaced in a defamation case and their potential impact on Fox News viewers.
editor: markdown
published: true
tags: fox news, misinformation
title: Let's talk about Fox quotes to reach people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lJMqR1QcIq8) |
| Published | 2023/03/08|
| Theme     |  Media, Misinformation, Deception |
| Status    | article incomplete |

# Human written summary
Beau discusses the significance of recent quotes that have surfaced in a defamation case and their potential impact on Fox News viewers. He urges people to share these quotes with loved ones who are regular Fox News viewers, as they may prompt them to question the network's credibility and motives. Beau emphasizes the need to reach out to misinformed individuals and provide them with reasons to reevaluate their beliefs.

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges viewers to pay attention to quotes related to a defamation case, particularly from Fox News personalities.
- Emphasizes the impact of the revealing quotes on potentially changing people's perspectives.
- Questions the disparity between public and private opinions of Fox News hosts.
- Suggests that the new information might provide a reason for misinformed individuals to re-evaluate their beliefs.
- Encourages engaging loved ones who consume Fox News with the information from the case.
- Points out the potential manipulation by media outlets like Fox News for ratings and control.
- Acknowledges the challenge of getting Fox News viewers interested in this new information.
- Indicates that the content of the case might lead to feelings of betrayal among Fox News consumers.
- Raises concerns about the education and information spread by certain media platforms.
- Conveys the rarity of viewers seeing behind the scenes and potentially feeling deceived.

### Quotes

- "It is worth noting at this point, they're filings. But it would be exceedingly rare for people to just make stuff up like this in a filing."
- "This is a case for your Trump-loving uncle, your relative who gets their news from Facebook, who believes the memes."
- "I'm willing to bet that most people who consume Fox News they are not going to like what they see and they're going to feel tricked."

### Oneliner

Beau urges viewers to pay attention to revealing quotes from a defamation case involving Fox News personalities, potentially leading to a shift in perspectives and feelings of betrayal among consumers.

### Audience

Fox News viewers

### On-the-ground actions from transcript

- Engage loved ones who consume Fox News with the quotes and information from the defamation case (implied).
- Encourage misinformed individuals to re-evaluate their beliefs based on the new information (implied).
- Find alternative ways to share the information with Fox News viewers, as mainstream coverage may be lacking (implied).

### Whats missing in summary

The full transcript provides more context on the potential impact of revealing quotes from a defamation case involving Fox News personalities, urging viewers to reconsider their beliefs and question media manipulation.

### Tags

#FoxNews #DefamationCase #Quotes #Perspectives #MediaManipulation


## Transcript
Well, howdy there, internet people. It's Beau again.

So today, we're going to talk about quotes some more. We're going to talk about some more quotes, because they keep coming out and they keep getting more and more interesting, to be honest. More and more telling.

So we're going to talk about the quotes and some of the effects these quotes might have if the right people see them and how it answers a common question that I get on this channel that I normally
don't have an answer to.

If you have loved ones, family members, that are frequent viewers of Fox News, you have got to get them interested in this story. It needs to be a very high priority for you, because the filings that are coming out when it comes to this suit, when it comes to this defamation case, the quotes that are in them, they are wild. And they are very telling. And they are the type of quotes that might actually make people have a second thought.

It is worth noting at this point, they're filings. But it would be exceedingly rare for people to just make stuff up like this in a filing.

I have some new favorite quotes:
>"We are very, very close to being able to ignore Trump most nights. I truly can't wait. I hate him passionately." - Tucker Carlson

According to the filings, that's Tucker Carlson. There's more.
>"That's the last four years. We're all pretending we've got a lot to show for it because admitting what a disaster it's been is too tough to digest. But come on, there isn't really an upside to Trump.

That's the one that I think might actually be able to hit home with a lot of people. Four years. Not an upside. Is that what the coverage on Fox indicated?

And keep in mind, this is one person, but there's quotes from Pick Your Host basically at this point.

Is that what the coverage showed? Did they suggest it was a disaster while it was going on? Or did they cheerlead? Did they indicate that what was happening was bad?

Any way, I don't think that most people would say that that was the impression they got from Fox, which should lead them to the question. And if it doesn't occur naturally, it should be pretty easy for you to point them to the question of, "Well, why Why'd they say it then?" If they didn't believe it, if behind the scenes basically everybody was like, hey, this guy's a failure, why did they cheerlead the way they did? And if they're willing to do that for somebody that they hate passionately, when else might they do it?

Do they often put out coverage that is counter to what they believe, simply because they think that's what the commoners need to hear? And if that's the case, I wonder if they really think it's a good idea that your kids are uneducated. "You shouldn't read that. Don't learn that. You don't really need to know anything about history. Don't read those books. You don't need an art education. You don't need to know anything except how to be a good worker bee."

Think that's the education their kids are getting? Probably not. Seems like it's just a way to keep the commoners common. Think they're really angry at a candy bar? Or is it just something to give to the commoners to keep them kicking down, keep them scapegoating somebody so they don't look up at their betters.

This is one of those moments where there's probably a whole lot of people who could be reached if they actually saw these quotes, if they actually saw these filings. It would probably make a difference for a whole lot of people.

You know, it's very easy to try to paint the political opposition as just evil, and sure, I mean, I get it, particularly lately, but I don't know that that's true of everybody. I think a lot of them are misinformed. A lot of them really don't get it because they have no reason to question it.

This gives them that reason.

If you have somebody that you've been trying to reach, you've been trying to get them to be like, hey, this isn't actually true. What they're telling you is false. These filings, this case, you've got to get them interested in it, and honestly, like this is one of those moments where if you had to pretend like, you know, you were just shocked because there was something in there about voting machines to get them to read it, I mean, I wouldn't criticize you. This is one of those moments because it doesn't matter who their favorite host is, as near as I can tell, it's pretty much everybody at Fox had, let's just say, a very different public and private face.

Honestly, it looks like there was only one person at Fox that actually believed any of the baseless claims about the election. Judging by the emails and texts and everything that we've seen so far, one person actually believed it themselves.

Everybody else was just for ratings or to not upset people for money, to stay on top to keep the commoners in their place, to keep partisanship going. Whatever motive you want to assign to it, but they certainly don't appear as though they put this information out or echoed the claims or allowed other people to say this stuff because they believed it.

It looks more and more like control system. This is a case for your your Trump-loving uncle, your relative who gets their news from Facebook, who believes the memes. This is a case you've got to get them involved in. You have to get them interested in it. The hard part is I seriously doubt Fox is going to cover this adequately. So you're gonna have to find another way to do it. But these quotes they are just wild and it really is. Pick your host.

I'm sure by the time it's all said and done once everything goes through the system somebody will put all of this in a database and you can type in their name and just pull it up.

This is one of those rare moments where the people who are being swayed get to see behind the scenes and I'm willing to bet that most people who consume Fox News they are not going to like what they see and they're going to feel tricked because it it certainly appears that they were.

Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
A black shirt with red text in a blocky sarif font:
```
Most of you traded
your country for a
red hat.
```
## Easter Eggs on Shelf
{{EasterEgg}}