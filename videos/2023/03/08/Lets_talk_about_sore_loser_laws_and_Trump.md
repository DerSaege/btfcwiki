---
title: Let's talk about sore loser laws and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CCpQxxBBDgc) |
| Published | 2023/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Republicans are comforted by sore loser laws, mistakenly believing they will prevent Trump from running as a third-party candidate if he loses the Republican nomination.
- Trump may run as a third-party candidate not to win but to fundraise, send a message to the Republican Party, and punish those he considers disloyal.
- Keeping Trump off the ballot won't stop him from running independently and portraying the Republican Party as disloyal.
- Trump's potential third-party run aims to establish a Trump legacy or set up another run, not necessarily to win the presidency.
- Republicans misunderstand Trump's motivations and view him as a normal politician despite his unconventional approach.
- Sore loser laws won't deter Trump from running independently, as his decision is not solely based on winning chances.
- Trump will likely fundraise off legal challenges if attempts are made to keep him off the ballot.

### Quotes

- "Sore loser laws, keeping them off the ballot through various means, this is not going to work."
- "Frankenstein has lost control of his monster. That's what's happened."
- "He already doesn't stand a good chance of winning."

### Oneliner

Republicans misunderstand Trump's intentions, believing sore loser laws will prevent him from running independently, but Trump's potential third-party candidacy aims to fundraise and send a message, not necessarily win.

### Audience

Political analysts

### On-the-ground actions from transcript

- Recognize the potential impact of Trump's independent run and strategize accordingly (implied)

### Whats missing in summary

Insights on the implications of Republican misinterpretations of Trump's actions and the need for strategic planning in response.

### Tags

#Trump #RepublicanParty #Election2024 #ThirdParty #SoreLoserLaws


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a thing that seems to be comforting many Republicans
that probably shouldn't be, because it's based on a misreading of a person that they
have a long history of misreading.
A lot of Republicans seem to be taking solace and taking a lot of comfort in the fact that
laws known as sore loser laws exist.
And the purpose of these laws is to stop somebody who runs in a primary, let's say hypothetically
speaking, the Republican presidential primary, and loses, will these laws stop them from
being on the ballot as an independent, as a third party.
They believe that this will stop Trump from running as a third party candidate if he loses
the Republican nomination.
This is something that Trump has repeatedly kind of teased or threatened.
Okay, it's funny to me that after all this time they still don't understand the man.
First, if he loses as the Republican candidate, there is a pretty good chance, it is definitely
not zero, that he runs as a third party.
And he would probably do so not with any intention of winning.
See the belief that Republicans have, he won't do it because of these, I want to say they're
six states or so, that just he won't be able to win.
He won't be able to mount a successful campaign, therefore he'll be behind when it comes to
Electoral College and all of that stuff.
It's cute that they think he cares.
That's not what it would be about.
It would be about fundraising and sending a message to the Republican Party that he
would view as disloyal.
That's how it would play out.
I would also point out that keeping him off the ballot, there's no way he would
spin that into the Republican Party being taken over by rhinos who just
aren't loyal to the people, and all of that stuff and everything that he did to
all of those other electoral workers throughout the years.
Frankenstein has lost control
of his monster. That's what's happened.
And now they're looking to the rule book and
hoping that the monster follows it. It seems unlikely
if he doesn't win
that there's a decent chance that he's gonna run his third party.
And it's not gonna be about winning. It's not gonna be about winning the presidency.
It's going to be about sending a message and punishing those who he views as
disloyal and setting up for maybe another run or maybe a run by one of his
kids or maybe just establishing a party, a Trump party, as his legacy.
They are viewing him as a normal politician which is just wild to me
after all of this time, the sore loser laws that many people are pointing to
and saying, hey, this right here, this shows that he won't run for president
because he can't really win. He doesn't stand a good chance of winning. He
doesn't stand a good chance of winning now. It has nothing to do with it. If he
stood a good chance of winning, you wouldn't be having this conversation
about fallback plans and taking comfort in the sore loser laws.
He already doesn't stand a good chance of winning.
Those laws aren't going to matter.
Whether or not Trump runs as a third party candidate is going to have nothing to do with
those laws.
And believe me, he will fundraise off of every one of them as he fights against it in court.
It's probably time for the Republican Party to recognize that, yeah, we all know that
Frankenstein wasn't the monster.
He was the doctor, but we also all know that Frankenstein really was the monster.
The Republican Party is going to have to take responsibility for what they have created
here.
Sore loser laws, keeping them off the ballot through various means, this is not going to
work.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}