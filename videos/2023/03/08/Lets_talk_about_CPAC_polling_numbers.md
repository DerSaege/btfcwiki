---
title: Let's talk about CPAC polling numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=e2T66hiAZIM) |
| Published | 2023/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's 62% poll numbers at CPAC aren't as strong as they seem because he was the main attraction and many other candidates didn't show up.
- CPAC's heavy focus on Trump and MAGA has made it less relevant, with many candidates skipping the event.
- Early poll numbers are not indicative of the final outcome, citing Jeb Bush's lead in 2015 before the 2016 primary.
- The political landscape will shift significantly before the actual election, with new candidates rising and falling based on policy ideas and national appeal.
- Trump's repetitive grievances and lackluster efforts may lead to different candidates gaining momentum as the election approaches.

### Quotes

- "62% for Trump at an event where he was kind of the only big name, those are bad numbers, not good numbers."
- "Sure, it looks like a win, but it's not."
- "A whole lot of stuff is going to happen between now and then."
- "It's very early to start looking at polling."
- "There's still a lot of time between now and then."

### Oneliner

Trump's poll numbers at CPAC may not be as strong as they seem, early polling isn't indicative of final outcomes, and the political landscape will shift significantly before the election.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed on the changing political landscape and be prepared for shifts in candidates (implied).
- Monitor policy ideas and changes in candidate standings leading up to the election (implied).
- Avoid making premature determinations based on early polling numbers (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's poll numbers at CPAC, cautioning against premature conclusions and stressing the need to monitor the evolving political landscape.

### Tags

#Trump #CPAC #PollNumbers #PoliticalLandscape #ElectionPrediction


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump's poll numbers at CPAC and what they mean and what they don't mean and
why it hasn't changed some people's opinions.
We're gonna do this because we got a message and I think if we follow this all the way through, it'll be pretty
enlightening.
So
Beau, while you haven't said anything since CPAC ended, you've made it clear that you don't think Trump is really a
contender this year. Trump got 62% of the vote at CPAC. Can you explain why you and
others don't think he'll win? 62% seems like a strong lead." Yeah. I mean, that's
okay. Actually, no. Those are horrible numbers. Okay, who was at CPAC? Trump. He's
the big name. He was the big draw. His people showed up to see him. A lot of
the other candidates, they didn't even bother showing up because CPAC is no longer the big
draw that it once was.
Why?
Because it leaned in really heavily on Trump, on MAGA, and it's kind of put itself into
a little bit of irrelevance.
A whole lot of candidates did not show up.
Since they didn't show up, their people, their fans, those people who are in the Republican
group, those people that show up to stuff like this, if their candidate isn't going
to show up, they're probably not going to show up.
62% for Trump at an event where he was kind of the only big name, those are bad numbers,
not good numbers.
Those are jests of Republicans, jests of Republicans who are primary voters, and jests of Republicans
who are primary voters who would go to CPAC.
And he couldn't really even, I mean 62% isn't great, it is not great.
So I don't know about other people, I'm not even sure who you're talking about.
But for me, it's a combination of watching his plays and stuff like this.
Sure, it looks like a win, but it's not.
I mean, the fact that the other candidates didn't even bother to show up kind of shows
it's not that important.
The other thing to keep in mind is that we're a long ways away from getting anywhere.
It's very early to start looking at polling.
Let's do it a different way to illustrate the point.
Let's go back to the last Republican primary, 2016.
If you wanted to get comparable numbers to where we're at right now, you would have to
go back to the polls for 2015 in February and March.
Who's winning at that point?
When I tell you, please clap.
Jeb Bush.
And if I'm not mistaken, after that it went to Ben Carson and then to Chris Christie.
Jeb Bush was like the shoe-in winner at this point in the lead up to 2016.
That's how far away we are from any of this mattering.
A whole lot of stuff is going to happen between now and then.
Not just with things that Trump may decide to do or things that may occur, but also other
candidates, their policy ideas are going to get pushed out there. Some of them,
because of their policies, they're going to get knocked out. They're going to get
knocked out of the race. Some of them will be elevated because of it. You have
a lot of candidates who played to their constituencies in very red
states. When they get up on the national scene, that stuff isn't gonna
to make it and that'll shift all of the numbers and as Trump moves through and
his rhetoric continues to be the same that CPAC speech that he gave not his
best work I I mean even for even for the situation he's in where he really didn't
have to put in much effort. I mean there's a reason he's not packing the
house anymore. It's basically the same grievances over and over again and they
are losing grievances. So as all of the numbers shift you're gonna see
entirely different candidates start to rise, at least I think so. You know
Everybody right now is kind of watching Trump and just assuming that if he falls, it'll
go to somebody else.
I don't know that any of that's a foregone conclusion yet.
There's still a lot of time between now and then.
I don't think that you're really going to be able to make any real determinations until
maybe a week before the first rural vote.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}