---
title: Let's talk about New Orleans, water, and engineers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ycJaQig7gWo) |
| Published | 2023/09/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Officials are concerned about salt water intrusion into New Orleans from the Mississippi River.
- Salt water intrusion is due to the Mississippi River's weak flow, causing ocean water to move north.
- This intrusion impacts drinking water systems in the area.
- The Army Corps of Engineers is planning to mitigate the issue by adding height to levees and bringing in 36 million gallons of fresh water daily.
- The cause of this intrusion is linked to a drought in the Central United States, which weakens the flow of the Mississippi River.
- Climate change is identified as the root cause of the drought and subsequent issues.
- Despite past issues, the Army Corps of Engineers is expected to handle the situation effectively.
- Residents in the area need to stay informed and vigilant as the situation progresses.
- The plan in place should work, but it's only a relief measure, not addressing the underlying causes like climate change.
- Continuous occurrences of such issues are expected and may worsen over time.

### Quotes

- "Climate change is real. It doesn't matter if you want to ignore it or not. Eventually you're not going to be able to."
- "This type of stuff will continue to happen and it will get worse."

### Oneliner

Officials are concerned about salt water intrusion in New Orleans due to weak Mississippi flow caused by climate change, requiring ongoing vigilance and relief efforts.

### Audience

Residents in the area

### On-the-ground actions from transcript

- Stay informed and vigilant about the salt water intrusion issue in New Orleans (implied)

### Whats missing in summary

The full transcript provides detailed insights into the impact of climate change on salt water intrusion in New Orleans, stressing the need for ongoing attention and action to address the issue effectively.

### Tags

#NewOrleans #MississippiRiver #saltwaterintrusion #ArmyCorpsOfEngineers #climatechange


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about New Orleans
and the Mississippi and salt water and fresh water
and what's going on.
Okay, so if you have missed the news,
officials are concerned that in October
and that timeframe is subject to change,
but currently they think it's gonna happen in October,
There will be salt water intrusion.
What this basically means is that the flow of the Mississippi, well, it's not strong
enough.
It's weak.
So the ocean kind of creeps north.
It starts moving up and the salt water that's in the ocean goes with it.
This impacts drinking water systems.
It's kind of a big deal.
So the Army Corps of Engineers is on it.
This is their plan.
This is what they're going to do.
As far as mitigation, they're going to be adding to some levees.
I want to say one of them they're planning on adding 25 feet in height to.
That's an undertaking.
That's the mitigation side.
As far as the relief side, they will be barging in about 36 million gallons of water, fresh
water every day. Every day. It's a big deal. So, obvious question. Why is this
occurring? The drought. The drought that occurred in the Central United States,
increased temperatures, all of that stuff. If there's drought in the Central
U.S., this kind of stuff, it doesn't exist in a vacuum. That means there's less
water to flow eventually into the Mississippi, which decreases the strength
of the flow, which leads to the ocean creeping north. Short answer, climate
change. You're seeing it, it's happening, climate change is real. It doesn't matter
if you want to ignore it or not. Eventually you're not going to be able
to. So there is a real issue on the horizon at this point in time. It looks
like the Army Corps of Engineers has a plan, but given the less than great
relationship between the Army Corps of Engineers and New Orleans, the people in
New Orleans don't always hold a high opinion of them, that may not be a lot
of comfort, but at this point in time, that's the best plan they have.
And realistically, it should work.
I don't see why they wouldn't be able to up the amount of water being barged in if necessary.
But this is definitely something you need to pay attention to if you are living in that
area.
If you are around there, you need to pay attention and you need to be listening because it may
not be October.
an estimate, and it is an estimate that is openly subject to change, so pay
attention. And despite past events, the Army Corps of Engineers normally does a
pretty good job. I know that there's going to be people from New Orleans who
are not entirely enthusiastic about that, but they normally do a pretty good job,
and they seem to have a plan, but all of this is relief. It's not a response, it's
not mitigation, it's not addressing the the real causes. This type of stuff will
continue to happen and it will get worse. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}