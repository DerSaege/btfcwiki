---
title: Let's talk about why Trump loves his voters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m5i8xjsPsyI) |
| Published | 2023/09/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump professes his love for his base, and it's true; he loves them deeply.
- Trump understands that a government shutdown will hurt Americans but reassures his base that they won't be blamed; he'll pin it on Biden.
- He openly commands a government shutdown while assuring his base that they won't face the blame.
- Trump counts on the ignorance of his base to manipulate them into believing his narrative.
- Trump loves his base because they are easy to manipulate and trick into supporting him, even when he's ordering actions that harm Americans.

### Quotes

- "Did you catch what he actually said there?"
- "He just doesn't think they're smart enough to figure it out."
- "That's why Trump loves the uneducated."

### Oneliner

Trump openly commands a government shutdown, reassuring his base they won't face blame, banking on their ignorance, and love for manipulation.

### Audience

Voters, activists, citizens

### On-the-ground actions from transcript

- Call out manipulation and misinformation in political leaders (exemplified)
- Educate and inform others about political tactics (exemplified)
- Stay informed and critically analyze political statements (exemplified)

### Whats missing in summary

The full video provides in-depth analysis on Trump's manipulation tactics and the dangers of blind loyalty to political figures.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about why Trump loves Americans.
Why he loves his base in particular.
A lot of times he will profess his love for his base.
And a lot of commentators are like, that's just not true.
No, it is true.
It is true.
He does love his base on a deep level.
it's important to understand why. So, what we're going to do today is go through a statement from
him and it's going to illustrate exactly why he loves his supporters as much as he does.
The Republicans lost big on debt ceiling, got nothing, and now are worried that they
will be blamed for the budget shutdown.
Wrong.
Whoever is president will be blamed.
In this case, crooked Joe Biden.
Our country is being systematically destroyed by radical left Marxists, fascists, and thugs.
The Democrats, unless you get everything, shut it down.
Did you catch that?
Did you catch what he actually said there?
This wasn't said at some donor retreat in private.
This was said in public.
This was a message to his base, the people that he loves.
They are worried that they will be blamed.
Blamed for the government shutdown that's coming, right?
Why would they be blamed?
Because it's going to hurt Americans.
It is absolutely going to hurt Americans.
And he's saying, no, no, no, no, don't worry.
won't be blamed for it. They'll blame it on Biden. And it says, unless you get
everything shut it down. So it's very clear that he understands that a
government shutdown is something the American people are going to blame on
somebody because it's bad for them. It's gonna hurt them. And then he says to shut
it down anyway so he doesn't care about actually hurting Americans as long as
the blame goes where it's supposed to. To Biden, that's who he thinks the American
people will blame for it, particularly his base. Even though in the same thing
he is saying he is commanding to shut it down. He's ordering it. He is ordering
the government shutdown and at the same time saying don't worry they're gonna
to blame it on Biden. Why do you think he can say this? Because he loves the
uneducated. He is banking that his base is too ignorant to read this and
understand it. He is openly commanding a government shutdown and saying don't
Don't worry, even though I'm saying this and I'm telling you to do it, they're going to
blame Biden.
It's Biden's fault for what I'm ordering you to do.
And don't worry, the base is going to eat that up because they're not that smart.
That's what he's saying.
This wasn't said in private.
This was open.
This was public.
His base will read this.
He just doesn't think they're smart enough to figure it out.
And that's why he loves them.
Because they are easy to manipulate.
They are easy to trick.
Because he knows all he has to do is walk out there, hug that American flag, and they'll
think he's a patriot.
They'll think he's out there fighting for their interests.
Even though he knows that what he's ordering is so bad, it's going to be blamed on somebody.
He just doesn't think that his base is smart enough to figure out who to blame.
They'll believe whatever he says because they're easy to manipulate.
That's why Trump loves the uneducated.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}