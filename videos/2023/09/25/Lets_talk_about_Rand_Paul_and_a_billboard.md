---
title: Let's talk about Rand Paul and a billboard....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LFA_7LZdwus) |
| Published | 2023/09/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two viral images with a surprising connection to a room and a house caught news outlets' attention this weekend.
- One image featured Rand Paul in a bathrobe at work, supposedly protesting the Senate's dress code changes.
- The other image showed a billboard with a typo involving the Ukrainian flag, attracting media coverage as well.
- Both images, although attention-grabbing, turned out to be fake; the Rand Paul one was likely AI-generated.
- The tendency for surprising images to go viral is due to the element of surprise that appeals to specific demographics.
- Beau notes that humor and viral content often rely on surprises and subverting expectations.
- The incident with the fake images underscores the need for better fact-checking by news outlets.
- Beau suggests a simple method to avoid falling for fake images: wait 24 hours for verification by reliable sources.
- With fake audio and video becoming more sophisticated, taking time before reacting is key to distinguishing truth from misinformation.
- As the election approaches, the risk of encountering fake content increases, making it even more vital to verify information before sharing.
- Beau advises against relying solely on instant information and urges a more cautious approach to consuming and sharing news.
- The lack of user-friendly tools for verifying images underscores the importance of waiting for credible journalism to confirm or debunk stories.
- The upcoming election is expected to see a rise in misinformation and influence operations, making it imperative to stay vigilant and skeptical of what is shared online.
- Beau warns about the potential for misinformation to spread through social media and encourages readiness to counter false narratives with verified information.

### Quotes

- "The secret to humor is surprise."
- "Your best defense is time."
- "Just wait."
- "Be on the lookout for it, especially when it comes to the election."
- "Be aware and be ready for it."

### Oneliner

Beau warns about the prevalence of fake images going viral and advises waiting for verification to combat misinformation, especially in the context of upcoming elections.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Wait 24 hours before sharing surprising or emotional images online to allow for verification by reliable sources (suggested).
- Stay vigilant and skeptical of potentially fake content, especially as the election approaches (implied).

### Whats missing in summary

Importance of critical thinking and caution in consuming and sharing information online to combat the spread of fake news.

### Tags

#FakeNews #ViralImages #Misinformation #FactChecking #Election


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about a tale of two images,
two images that at first glance
have nothing to do with each other,
aside from a connection to a room and a house.
But generally speaking, when you first look at them,
you wouldn't put the two together,
but both of them became viral blockbusters this weekend.
And they were picked up on by some news outlets.
And they have something in common.
OK, so the first one is an image of Rand Paul.
An image of Rand Paul.
Now, if you missed the news, the Senate
did away with its dress code.
So according to the story, this weekend, Rand Paul,
He showed up for work wearing a bathrobe
to kind of express his displeasure
with the new dress code.
The other image was one of a billboard in a city.
And it had a Ukrainian flag on it.
And rather than glory to Ukraine, there was a typo.
And it said, glory to something else.
And also, it got picked up by news outlets.
People ran with it.
I thought the typo was a little weird.
It was odd.
It didn't seem like a normal typo.
It seemed like almost somebody did that on purpose.
And then the thing about Rand Paul showing up to work on
the weekend, not wearing shoes in DC, walking around
barefoot, yeah, that seemed odd to me as well.
Neither one of the images are real.
The Rand Paul one is almost certainly AI generated.
The other one, it might have just been done with like old
school Photoshop, not sure.
But neither one of them are real.
Both of them were picked up by news outlets and treated as
real, and some news outlets got burned by both of them.
So what does this tell us?
It tells us that if there is an image that is surprising,
because that's really what it boils down to there,
is there's an element of surprise in the image,
and it can appeal to a certain demographic.
It might go viral.
A lot of things that go viral have a surprise in them,
subverting of expectations type of thing.
the secret to humor is surprise.
So we can expect them to go viral.
We can expect AI images to be treated as real
by some news outlets.
We talked about this a few months ago.
And I, probably having way too much faith in humanity,
said that I thought eventually it would
lead to better fact checking.
Given the fact that some outlets got burned by both images,
I may need to re-evaluate that stance.
But, just because outlets aren't up to it yet,
that doesn't mean that you have to fall for it.
So, what can you do to protect yourself
from falling for fake images?
You know, you don't want to be somebody
who posts an image of a well-known director
next to a Triceratops and actually believe
that he shot the Triceratops on safari.
The simplest method, give it 24 hours. If it is something designed to elicit
emotion, create a surprise, it'll be checked out. Obviously not all outlets
will, but most, some maybe, at least a few, will seek verification of the story.
just wait. We have become very addicted to instant information and a 24-hour
news cycle. We're gonna have to slow down because it's not just fake images, it's
fake audio, it's fake video, it can all be done now. This is important leading
into the election because there's gonna be a bunch of it and there will be some
outlets who get caught up in it accidentally. There will be some outlets
who don't care because it suits their agenda. Your best defense is time. It'll
be verified or it'll be debunked. So let Rand Paul in a bathrobe be a learning
experience. Right now there aren't a lot of tools for you to download and check.
There are some that exist but they're they're kind of higher end. You need a
little bit more experience to use them I guess. I don't know how to use them. If
If you are aware of one that is good and is user-friendly, please recommend it, but right
now your best defense is time and waiting for actual journalism to occur and people
to check it out.
Other than that, it's just a waiting game.
So be on the lookout for it, especially when it comes to the election.
This is obviously, I think the last time we talked about it, we talked about it in the
context of influence operations.
And believe me, it's already being used there.
So just be aware and be ready for it.
Be ready for your uncle who spends way too much time on Facebook to talk to you about
something that didn't happen and truly believe that it did because he had a picture of it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}