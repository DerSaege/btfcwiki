---
title: The Roads Not Taken EP 5
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=iAQ7BBgzE04) |
| Published | 2023/09/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces "The Road's Not Taken" weekly series focusing on under-reported events and stories that will provide context later on.
- Reports on a powerful storm in Libya that flooded the city of Derma, causing unknown casualties but estimated at more than 10,000.
- Points out how economic issues in China might lessen the likelihood of Chinese intervention in Taiwan according to foreign policy experts.
- Mentions the trial of alleged leaders of the Ottawa convoy demonstrations in Canada, showing the influence of US events on Canadian politics.
- Notes a shift in Russia's artillery tactics towards accuracy rather than saturation, potentially impacting their relationship with North Korea.
- Unveils the UK's new jet propelled drone, the Hydra 400, armed with laser-guided brimstone weapons, specifically designed as a tank killer.
- Reveals the discovery of the world's largest known lithium deposit in the United States, with significant foreign policy implications due to its necessity for electric vehicles.
- Comments on the Texas Attorney General's acquittal in the Senate trial and suggests Texas Republicans may face consequences for it.
- Mentions Trump's accusations against President Biden and his son's indictment, posing a test for gun rights activists.
- Criticizes candidate Vivek's promise to deport US-born children of undocumented parents, a violation of the US Constitution.

### Quotes

- "Foreign policy is about power, nothing else. Don't get it twisted."
- "They don't want to represent, they want to rule."
- "The world may be better if morality and fair play mattered in foreign policy, but they don't."
- "It's not the content of the document that matters. It's why he had them."
- "Don't cross the picket line."

### Oneliner

Beau analyzes under-reported global events, from Libyan floods to evolving Russian tactics, questioning foreign policy and US influence on Canada.

### Audience

Global citizens

### On-the-ground actions from transcript
- Watch "The Roads to Ukraine" video for insights on Armenia and foreign policy (suggested).
- Support strike funds and showcase demands of strikers to show solidarity (exemplified).

### Whats missing in summary

The full transcript provides in-depth analysis of global events often overlooked by mainstream media, urging viewers to stay informed about under-reported stories. 

### Tags

#GlobalEvents #UnderreportedNews #ForeignPolicy #USInfluence #Solidarity


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, I'd like to welcome you to episode five
of The Road's Not Taken, a weekly series
where we go over the previous week's events
and we look at stories that were uncovered,
under-reported, or stories that later on
are going to provide a whole bunch of context
to events that are just bound to happen.
A lot of times, news will break,
it won't really be covered,
And then later on, it becomes incredibly important.
And that's what we try to focus on here.
We will start off with foreign policy.
And we will start off in Libya.
OK, in Libya, a powerful storm dumped a whole bunch of rain
from the Med.
That was followed by two dams busting.
This flooded the city of Derma.
This is a city with 100,000, more or less, people in it.
The toll at time of filming is unknown, but it is estimated to be more than 10,000.
There will probably be a pretty lengthy video about this over on the main channel and how
Americans, particularly those who live in red states, need to learn from what happened
here.
In China, economic issues are being read by foreign policy experts, people who provide
commentary as lessening the likelihood of Chinese intervention in Taiwan.
Basically they feel that China's current economic issues will distract them from any
military foreign policy aims that they have.
In Canada, the trial of some of the alleged leaders of the Ottawa convoy demonstrations
are underway.
The ideology and politics involved with these demonstrations is a pretty clear example of
when the US sneezes, Canada catching a cold.
On behalf of the United States, I would like to apologize.
Russia seems to be evolving when it comes to their artillery tactics.
Russia's tactics have basically been the same since World War II.
They now seem to be trying to adjust, pun intended, and focus more on accuracy than
saturation.
This might alleviate some of the issues they have and the debt that they're going to incur
with North Korea.
It's worth noting that even today, their doctrine really calls for set numbers.
If they're going to hit this area, it is this big, it has this many people in it, they need
to fire 720 shells.
That's how it works, rather than having five really well-placed ones.
We're starting to see that shift.
We'll see how sustained it is and how successful they are at it.
In news that is definitely going to matter later, the United Kingdom has unveiled the
Hydra 400. This is a jet propelled drone that fires laser-guided brimstone weapons. They
are air-to-ground missiles. For the uninitiated, this is a tank killer. It is a tank killer.
I would imagine that it's going to see use relatively soon. As soon as they can get this
thing filled it, they're going to want to try it out for real.
The world's largest known lithium deposit was found in the United States.
This is going to have big foreign policy implications for the future.
This is an ingredient that is necessary for electric vehicles.
in the United States.
It looks like the Texas Attorney General was acquitted in the Senate trial.
There will be tons of commentary on this.
I'm going to suggest that Texas Republicans set themselves up for failure here.
My guess is they are hoping that this provides them with cover and says, you know, well,
we looked into this and it wasn't a big deal.
see how the voters feel about that. Trump once again accused the current president of the United
States with being cognitively impaired, and then he went on to suggest that Biden was going to lead
us into World War II. Moving on, President Biden's son was indicted. However, he was indicted on a
charge and in a way that gun rights activists have been trying to whittle away at.
It's likely that the Bruin decision will come into play when determining whether or not
these charges are going to stand.
And we will get to see whether the Republican Party and gun rights activists, whether they
believe in principle or just loyalty to their party.
candidate Vivek says he would deport children born in the United States if their parents
are undocumented.
Of course, this campaign promise runs contrary to that little document called the U.S. Constitution.
This is not something he can do while abiding by the U.S. Constitution.
Mitt Romney is retiring.
This is a huge, huge devastating blow to the moderate Republican faction.
They needed him.
And there's going to be a whole bunch of liberals and leftists who probably never liked Romney,
but now they're going to have to wonder about the actions of whoever is going to replace
the senator from Utah.
A federal judge has again declared DACA illegal,
reigniting a long-standing fight,
and once again putting Republicans in a position
where they have to produce some kind of legislation
to solve the problem.
That legislation would need to originate in the House.
That is not legislation they will be able to create,
partly for a lack of will,
because they don't actually want to solve any problems,
they want to complain about them,
and partly because they lack the ability and coordination
to put anything together.
So that is going to end up becoming a campaign issue in 2024.
A Michigan Republican charged in the fake elector scheme
claimed in a court filing that she and 15 others acted,
quote, at the direction of Trump.
That's gonna matter later.
Okay, Blake Masters and Kerry Lake, they had a phone call that has been described as intense.
The two unique candidates plan on vying for the same Senate seat at least at time of filming.
There have been some signs that suggest Masters maybe having second thoughts about all this.
Either way, it's going to be entertaining.
Both of them have a lot of support within the GOP.
They probably don't have a lot of support statewide.
We'll have to wait and see how it plays out.
When we're looking at the Republicans catching the car,
after realizing that curtailing reproductive rights
is not actually a popular position
outside of their own little echo chamber,
The GOP wants to rebrand and they want to get away from the term pro-life.
They want a new term, not a new policy.
Again, they are assuming that their voters and the voters that they can reach aren't
really that bright.
That is the basis behind this rebranding effort, that it's not the policy that's the problem.
It's the terminology.
It will be okay if they take away half the country's rights as long as they call it
something else.
The end result, once again, they don't want to represent, they want to rule.
Crenshaw seems to be gearing up for a fight with Tuberville over that catastrophic promotions
old. Let them fight. Let them fight. That ought to be entertaining. Let's see what
else we have going on here. Republicans in Wisconsin have launched a
multi-pronged attack against any attempt to try to remedy the just widespread
systemic gerrymandering that goes on in the state. They're trying to go a
number of different routes to make sure that they can hold on to those
gerrymandered districts. Once again, rule not represent. JM Smucker looks like they're buying
Hostess Twinkies for $4.6 billion. No real importance there, I just like Twinkies. Okay,
moving on to cultural news. Fox is being sued by pension funds in New York and Oregon.
The complaint says that Fox failed its responsibility to the shareholders and promoted quote political
narratives without regard for whether the underlying factual assertions were true or
based on sources worthy of credit.
And it sought quote to profit from defamation.
These are pension funds that are invested in FOX.
Twitter or the site formerly known as Twitter, they verified the account of President John
F. Kennedy, yeah, not candidate Kennedy, President John F. Kennedy verified that account who
Quote it says that he has quote come back from the dead
Remember how the new verification process was supposed to get rid of yeah, things are going great over Twitter, okay
In environmental news the feds have signaled that they are likely to approve an
Exemption that would allow
ranchers, and others to kill wolves being reintroduced to Colorado under certain circumstances.
That's right folks, the wolves aren't even there yet, and there are already plans in
place to kill them.
I have questions about how successful that reintroduction program is going to be.
This year is on track to become the hottest year in Earth's history.
After July and August saw temperatures reach the Paris Agreement target of 1.5 degrees
Celsius above pre-industrial levels.
This is the first time the 1.5 threshold has been passed for more than a month.
It's the hottest summer of your life so far.
In some odd news, we have K2-18b.
It's an exoplanet, and it has been identified as a location that may support life.
It's an exoplanet of K2-18, and that's a cool dwarf star.
It's inside the Goldilocks zone,
and it's drawn a whole lot of attention
due to the fact that it has methane and carbon dioxide
in its atmosphere.
So they believe that it supports life.
Obviously, there's gonna be more research.
OK, moving on to the questions and answers.
OK, I saw you comment about naming conventions
and why Satan II was called that.
Can you explain it?
So this is how NATO reporting names work.
Understand that a lot of the weapon systems
that we have names for, those aren't the producing country's
names.
Those are NATO names for that particular vehicle or weapon.
And the coding actually isn't that hard.
A, if it starts with an A, it's an air-to-air weapon,
like the Apex.
B is a bomber, like the backfire.
C is commercial or cargo, like the camel.
Let's see, F is fighter, like fulcrum,
which Fulcrum is one of those where the Russian pilots that
actually heard that name, they kind of leaned into it.
They started using it because they view it as pivotal to the
Russian air program, which is cool.
But that's where we also get fishbed.
There's a whole bunch of planes that have names that are not
quite as cool as Fulcrum.
And that's where it comes from.
H is a helicopter, the most famous Russian helicopter
being the Hind, that's how it works.
So S is surface to surface missiles.
So Satan II, Scud, Stix, things like that.
That's just to kind of unify how NATO reports that stuff.
So if one of these is spotted in a combat zone,
that's how it comes back.
it's all it is all standardized this way you don't have a bunch of different
translations for one particular device what's up with the changes to YouTube's
ads lots of creators are complaining okay so that is in my opinion not a big
deal. If you don't know what's going on, up until very recently creators were
allowed to choose what types of ads would appear before or after their
content. Like you know how sometimes they'll be an ad that you can watch a
few seconds and then skip and sometimes you can't skip and all of that stuff
right? It used to be that creators could select specifically what type of ad
they want it. Now they can't. I'll go ahead and tell you on the main channel, I
never picked. So the ad experience over on the main channel is what you'll get
everywhere now. If it was something that that really bothered you, just expect it
to to be YouTube wide now. I never had any complaints about it. I don't want to
say, I don't want to say that you know creators are blowing it out of proportion
Because by YouTube standards, a lot of videos
over on the main channel, they should have
an ad in the middle of it.
And I don't like the way it breaks it up.
So I select so there's not one over there.
So I can understand people wanting
to have control over that.
But from what I can tell, I don't think
this is going to be a big deal.
Okay, let's talk about inconsistency.
My tax dollars are going to defend Ukraine.
Why aren't they going to defend Armenia
from non-defensive invasion?
Okay, well first they are.
Just maybe not the way you think.
But more importantly, you're looking at this
the lens of good guys and bad guys and morality and fair play and all of that stuff. None of that
has anything to do with foreign policy. Foreign policy is about power, nothing else. Don't get it
twisted. But the United States has committed funds to run an ad campaign in another country
that Armenia is having issues with. If you don't get that reference, there's a
video on this channel, on the Roads with Bo Channel, titled The Roads to Ukraine.
Watch that, it might give you a little bit of insight, but there there's
definitely funding committed to promote certain things. It's just not the same
way, but it wouldn't be because foreign policy-wise, Ukraine is more important.
I know people want morality and fair play to matter when it comes to these decisions.
They don't.
That's not how it works.
The world would be better if it was how it worked, but it's not.
Over on a new social media platform, leftists and moderates have been arguing about whether
Biden is an authoritarian.
Is Biden truly an authoritarian?
Okay, well, it depends on what kind of leftist you're talking about.
You have to understand that when you're talking about the various political ideologies,
a lot of them are...
they're based on...
these are the core beliefs, these beliefs are not things that can change.
If you are talking about an anti-authoritarian leftist,
so we're talking, if you're using the grid,
all the way over, or at least mostly over to the left
and all the way down, yeah, Biden's an authoritarian.
Is he an authoritarian to the same degree that Trump is?
No, and I think most anti-authoritarian leftists
would agree with that.
However, the difference that you see,
I'm assuming as a moderate,
between Biden and Trump,
oh, it's nowhere near as great to them.
They don't see it as as big of a difference
because they're still using
the state's monopoly on violence.
And that is really the defining line.
So for leftists to say that Biden is an authoritarian,
So for leftists to say that Biden is an authoritarian,
particularly if they're like anti-authoritarian leftists,
which you're the only people I can imagine
making this argument.
Yeah, I mean that's a very principled stance for them to take.
Again it goes to the foreign policy thing though.
That's ideology and how things should work.
In the pragmatic world, I mean things are a little different.
Now you have some authoritarian leftists who really might say that Biden doesn't go far
enough in being an authoritarian.
But I don't think that those are the people making this argument.
You have probably run into a group of anti-authoritarian leftists, and that's who's making this claim.
I will go ahead and tell you, don't dismiss them.
They may be very concerned with purity of belief, but philosophically, if you're going
to listen to a group of people that are far outside the Overton window and you want to
inform your political opinions based on a philosophy, I wouldn't shut them out.
I would definitely listen to what they have to say, even if some of the rhetoric they
use isn't something you want to hear.
Are you a Stoic or a Buddhist?
I'm Bo.
I think that most philosophies, most religions have things that are valuable in them.
And I think that if you can understand a large number of them and you can kind of search
out the universal elements, you will find humanity's baseline and you will find a
a good place to kind of start to build your own philosophy and figure out where you want
to be in the world.
As far as my religious beliefs, we don't talk about them on the channel.
I will talk about religion every once in a while if there's a situation that arises
that I feel like it has to be addressed, but generally speaking, it's not something that
I think is my place to critique really.
Why does Trump need to see the documents?
He's had them for over two years.
This is obviously about the documents case and how Trump is saying that he wants access
and so on and so forth.
People want to know why he needs to see them.
He's given access to them because they're evidence, it's discovery, the same way somebody
would be given access to a note that was written about them, or they would see photos of stuff
that it was alleged that they stole or something like that.
Why does he need to see them?
He doesn't.
Even for a defense, I don't understand how having access is going to help.
I would imagine that his attorneys are not going to spend a whole lot of time pouring
over those documents.
They're really not that important.
It's not the content of the document that matters.
It's why he had them.
The content of the document isn't something they're going to be able to dispute, so realistically
he doesn't have a need, however, he is entitled to them because of discovery.
Any advice for Drew Barrymore?
Oh, yeah, I saw that.
That hurt.
Don't scab?
I honestly feel like she thought her show was incredibly important to people
and she thought that people would be okay with her show going back even during
this strike. She found out they're not and it has caused quite the backlash. Now
Now she also said something that said it's more complicated than that and there are other
people's jobs and all of this stuff.
If there is a contract thing involved where other people are going to lose their jobs
or there's legal consequences because it's quote complicated, just remember that if you
honestly feel like you have to do it, there's a strike fund.
If you wanted to, and this goes to anybody who is, quote, not having riders right now,
but going back on the air, if you're really that upset and you're in a position where
you feel like you have to do it even though you want to show solidarity, there are ways
to show solidarity.
There's a strike fund.
You could make part of your show showcasing the demands of those who are on strike.
There's always something that can be done rather than just breaking the strike.
I am from that generation, so Drew Barrymore and all of that stuff, but I cannot imagine
a scenario in which the show has to go on the air, but if that scenario existed, there
are a number of ways available for her to show solidarity with the unions.
It's not a thing where there's nothing that she can do.
If the show's going to go on, it's a talk show.
You can have a segment about it every episode.
There are ways that you could do it if there's some kind of contract or there's some situation
that absolutely requires it.
And to be honest, I can't think of any.
And if that situation does exist, more can be done than just owning the decision and
going forward.
I would assume that the best thing to do would be what's always the best thing to do in this
situation.
Don't cross the picket line.
But if there is some bizarre extenuating circumstance, there are alternatives that are available.
And it looks like that's it.
That is it.
Okay.
So that's the information from last week that didn't get the coverage it should, either
on my channel or in general, that you will probably see again.
And better information will make all the difference.
Have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}