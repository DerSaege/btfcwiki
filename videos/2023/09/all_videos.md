# All videos from September, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-09-30: Let's talk about the first flip in the Trump Georgia case.... (<a href="https://youtube.com/watch?v=q1BnmbYpQTA">watch</a> || <a href="/videos/2023/09/30/Lets_talk_about_the_first_flip_in_the_Trump_Georgia_case">transcript &amp; editable summary</a>)

Scott Hall pleads guilty in Georgia case, signaling potential domino effect on higher-ups with mounting pressure and unfavorable implications for the Trump team.

</summary>

"This is not good news for the Trump team, but it is what has transpired so far."
"The terms of this deal becoming public at the same time as two people who are likely to have much more information, that coming out at the same time as it becoming public that their big deal is being prepared for them to be offered, that's going to cause some ketchup bottles to be thrown, I think."

### AI summary (High error rate! Edit errors on video page)

Scott Hall pleaded guilty to five counts in the Georgia case, including conspiracy to commit intentional interference with the performance of election duties.
He agreed to five years of probation, a $5,000 fine, 200 hours of community service, a ban on activities related to polling and election administration, and has to testify.
The deal requires Hall to testify truthfully at any further court proceeding and provide a pre-recorded statement to the District Attorney.
There are at least two other plea offers being prepared by the government in the same case.
Hall is the first person to flip in this case, suggesting he may have received a good deal due to his lower rank in the hierarchy.
Clark's attempt to move his case to federal court has been denied.
The public disclosure of Hall's deal coincides with the preparation of significant offers for other individuals, likely causing tension.
This development is unfavorable for the Trump team, hinting at more deals to come.
The situation suggests mounting pressure on higher-ranked individuals as deals progress.
Overall, the unfolding events indicate potential trouble ahead for those involved.

Actions:

for legal observers, political analysts,
Stay informed about the developments in legal cases and understand their potential implications (implied).
Support efforts to ensure accountability and transparency in legal proceedings (implied).
</details>
<details>
<summary>
2023-09-30: Let's talk about the GOP tanking the GOP budget.... (<a href="https://youtube.com/watch?v=LD5AiiAAWqA">watch</a> || <a href="/videos/2023/09/30/Lets_talk_about_the_GOP_tanking_the_GOP_budget">transcript &amp; editable summary</a>)

The House GOP's far-right bill failed, setting the stage for a likely government shutdown as McCarthy could have prevented it by crossing the aisle.

</summary>

"They killed the most conservative position we could take and then called themselves the real conservatives."
"It was just too far outside the window that the Senate would even consider."
"At this point, it is a relatively safe assumption that there will be a government shutdown."
"It's just a thought. Y'all Have a good day."

### AI summary (High error rate! Edit errors on video page)

The House GOP put up a very right-wing bill that didn't pass because of the far-right wing of the Republican Party.
The bill was rejected 232 to 198, leading to a high likelihood of a government shutdown.
Dan Crenshaw pointed out the contradiction in the GOP's actions, calling their position conservative while rejecting it.
The leverage to avoid a shutdown was lost when the far-right Republicans shot down their own bill.
A government shutdown seems inevitable, with unknown duration, as it may take a miracle to avoid.
McCarthy could have prevented the shutdown by reaching across the aisle, but didn't.

Actions:

for politically engaged individuals,
Prepare for a government shutdown by ensuring you have necessary supplies and resources (implied).
</details>
<details>
<summary>
2023-09-30: Let's talk about McCarthy, Boebert, and how I'm not that smart.... (<a href="https://youtube.com/watch?v=DC9JKQb5eeA">watch</a> || <a href="/videos/2023/09/30/Lets_talk_about_McCarthy_Boebert_and_how_I_m_not_that_smart">transcript &amp; editable summary</a>)

Beau explains the manipulation and split within the Republican Party, showcasing the misleading tactics used by different factions.

</summary>

"Vote with Dems, never get elected again."
"This is how the Republican Party dupes its rank and file."
"They will give you a slogan and violate it while they're giving it to you."

### AI summary (High error rate! Edit errors on video page)

Explains a message criticizing his political analysis, suggesting he stick to discussing wars rather than politics.
Describes the message's assertion that Republicans can't team up with Democrats and still get elected, using catchy slogans like "vote with Dems, never get elected again."
Questions the logic behind the Republican bill being stopped by 21 Republicans who voted with Democrats.
Points out the hypocrisy of Republicans using slogans like "vote with Dems, never get elected again" while not following it themselves.
Notes how this situation reveals the manipulation within the Republican Party and the split between different factions.
Talks about information silos within the Republican Party and how Trump's influence has caused division.
Mentions the infighting within the Republican Party due to conflicting ideologies and misleading tactics used by different factions.

Actions:

for political analysts, voters,
Educate others on the manipulation tactics used by political parties (implied)
Stay informed about political developments and question catchy slogans and talking points (implied)
</details>
<details>
<summary>
2023-09-30: Let's talk about Feinstein's vacancy and Newsom's political test.... (<a href="https://youtube.com/watch?v=tCTel-6KZeU">watch</a> || <a href="/videos/2023/09/30/Lets_talk_about_Feinstein_s_vacancy_and_Newsom_s_political_test">transcript &amp; editable summary</a>)

Governor Newsom faces a political dilemma with Senator Feinstein's vacancy, testing his national political acumen and strategic decision-making amid conflicting promises and ambitions.

</summary>

"He has put himself in a very difficult political position."
"This is probably his first real political test on the national scene."
"He does seem like somebody that's going to be destined for national prominence."

### AI summary (High error rate! Edit errors on video page)

Senator Feinstein's vacancy requires Governor Newsom to make an appointment.
Newsom previously stated he'd appoint a black woman and not someone running for the position.
Newsom faces a dilemma as influential candidates are now running for the position.
Barbara Lee is a popular choice for the vacant Senate seat.
Appointing one candidate may alienate others within the Democratic Party.
Avoiding the appointment may lead to an incumbent in the next election.
Newsom is in a tough political position due to his national ambitions.
Several influential figures, like Porter and Schiff, are eyeing the Senate seat.
Newsom's handling of this situation is his first national political test.
This decision will show how Newsom navigates politically sensitive situations.

Actions:

for politically aware individuals,
Analyze and stay informed about Governor Newsom's decision-making process (implied)
</details>
<details>
<summary>
2023-09-29: Let's talk about the GOP and $1 for SECDEF.... (<a href="https://youtube.com/watch?v=vgF8IG-chus">watch</a> || <a href="/videos/2023/09/29/Lets_talk_about_the_GOP_and_1_for_SECDEF">transcript &amp; editable summary</a>)

Republicans use social media antics like reducing the Defense Secretary's salary to $1 to provoke outrage and gain attention, while real issues like Americans facing financial difficulties are sidelined.

</summary>

"They're confusing social media engagement with votes."
"What's the message? Marjorie Taylor Greene thinks she knows more about the military than the 40-year-plus veteran."
"Legislation like this has zero chance of ever actually going anywhere."
"This is what they're doing for social media clicks."
"Rather than getting a budget together, this is what they're playing with."

### AI summary (High error rate! Edit errors on video page)

Republicans are using social media to provoke outrage and elevate their profile.
Marjorie Taylor Greene introduced a provision to reduce the Defense Secretary's salary to $1 a year.
Greene believes the current Defense Secretary is destroying the military despite his 40+ years of service.
Legislation like Greene's provision has zero chance of becoming law but is used for social media engagement.
High-ranking military officials are paid well to make them harder to buy and prevent financial motives.
While Americans face financial difficulties, politicians are focused on social media antics instead of real issues.

Actions:

for politically engaged individuals,
Contact local representatives to prioritize real issues over social media antics (implied)
</details>
<details>
<summary>
2023-09-29: Let's talk about deals, Chesebro, Powell, and Trump in Georgia.... (<a href="https://youtube.com/watch?v=GKDfq-uwWl0">watch</a> || <a href="/videos/2023/09/29/Lets_talk_about_deals_Chesebro_Powell_and_Trump_in_Georgia">transcript &amp; editable summary</a>)

Georgia prosecution considers offering plea deals in the Trump case, potentially triggering a chain reaction of defendants accepting deals, with downstream effects causing concern for the Trump team.

</summary>

"Georgia prosecution may offer plea deals in the Trump case to Cheesebro and Powell."
"A plea deal is not a guarantee for defendants to walk away without consequences."
"The downstream effects of these potential plea deals could lead to more cooperation from other individuals."

### AI summary (High error rate! Edit errors on video page)

Georgia prosecution may offer plea deals in the Trump case to Cheesebro and Powell.
No offer has been made yet, but the state is considering making one soon.
The prosecution will reach out to the defense individually to extend an offer.
A plea deal is not a guarantee for defendants to walk away without consequences.
If Cheesebro accepts a deal, it could set off a chain reaction of other defendants accepting deals.
Cheesebro might be offered the best deal out of the two defendants.
The development is expected to be resolved within the next 20-something days.
The downstream effects of these potential plea deals could lead to more cooperation from other individuals.
This situation could cause concern for the Trump team as it signals potential cooperation from others with valuable information.
The process may lead to a snowball effect as more people potentially accept plea deals.

Actions:

for legal observers,
Stay informed about the developments in the legal proceedings surrounding the Trump case (implied)
Monitor updates on plea deals offered by the prosecution (implied)
Be prepared for potential downstream effects on related cases (implied)
</details>
<details>
<summary>
2023-09-29: Let's talk about Trump, New York, and Georgia.... (<a href="https://youtube.com/watch?v=aXtcP62nQpU">watch</a> || <a href="/videos/2023/09/29/Lets_talk_about_Trump_New_York_and_Georgia">transcript &amp; editable summary</a>)

Beau provides updates on Trump-related developments in New York and Georgia, hinting at potential reasons behind Trump's legal decisions and urging viewers to brace for rapid developments.

</summary>

"The New York case will be moving forward. On Monday, the Georgia case is staying where it's at."
"Get ready for a whole lot of developments to start occurring very, very quickly."

### AI summary (High error rate! Edit errors on video page)

Updates on Trump-related developments in New York and Georgia are discussed.
The civil case in New York seeking $250 million will begin trial on Monday.
The trial was initially supposed to run until December 22nd but may move faster now.
The witness list for the trial has about 100 names, indicating it may be tedious.
In Georgia, Trump has decided not to seek removal of his case to federal court.
Trump's decision in Georgia is based on confidence in a fair trial and due process.
Speculations arise on why Trump made this decision, possibly to avoid testifying.
Trump may believe his own propaganda, influencing his legal decisions.
Beau hints at potential reasons behind Trump's choice regarding his case jurisdiction.
The New York case will progress, while the Georgia case remains where it is.
Expect rapid developments and information influx in the near future.
Viewers are urged to prepare for an increase in the pace of events regarding these cases.

Actions:

for legal observers, political analysts.,
Stay informed about the developments in the Trump-related cases in New York and Georgia (suggested).
Follow reliable news sources for updates on these legal proceedings (suggested).
</details>
<details>
<summary>
2023-09-29: Let's talk about Russia recognizing there's no end in sight.... (<a href="https://youtube.com/watch?v=RRcWKwxYbJ0">watch</a> || <a href="/videos/2023/09/29/Lets_talk_about_Russia_recognizing_there_s_no_end_in_sight">transcript &amp; editable summary</a>)

Russia acknowledges a protracted conflict, focusing on force generation due to degraded combat power, while Ukraine anticipates economic issues causing a termination by 2025.

</summary>

"Russia has finally acknowledged being in a protracted conflict, expecting military goals to be achieved by 2025."
"The Russian economy may not hold out for the duration necessary to achieve military objectives."
"Both sides agree that the conflict will be long-term, with Russia having the clocks but Ukraine having the time."
"As conflicts drag on, Russian resolve is likely to weaken faster than the resolve of those fighting for their homes."
"Beau questions the understanding and planning of the Russian Defense Minister amid the ongoing conflict."

### AI summary (High error rate! Edit errors on video page)

Russia has finally acknowledged being in a protracted conflict, expecting military goals to be achieved by 2025.
Their military efforts are focused on force generation due to degraded combat power.
Russian tactics led to the loss of experienced personnel needed for training new troops.
The Russian economy may not hold out for the duration necessary to achieve military objectives.
Western commentators should stop setting unrealistic expectations on the conflict.
Ukraine believes the war may terminate due to Russian economic issues in 2025.
Both sides agree that the conflict will be long-term, with Russia having the clocks but Ukraine having the time.
The only potential threat to a Ukrainian victory could be the West giving up and not providing support.
As conflicts drag on, Russian resolve is likely to weaken faster than the resolve of those fighting for their homes.
Beau questions the understanding and planning of the Russian Defense Minister amid the ongoing conflict.

Actions:

for conflict analysts,
Monitor and advocate for realistic expectations in Western commentary on the conflict (implied).
Support Ukrainian efforts by ensuring continued assistance from the West (implied).
Stay informed and educated on the ongoing conflict dynamics (implied).
</details>
<details>
<summary>
2023-09-28: The Roads Not Taken Special Trump Exhibit C (<a href="https://youtube.com/watch?v=7ouiamoJh0o">watch</a> || <a href="/videos/2023/09/28/The_Roads_Not_Taken_Special_Trump_Exhibit_C">transcript &amp; editable summary</a>)

Beau recaps recent legal troubles for Trump, including liability ruling in New York and attempts to suppress incriminating phone call in Georgia, warning of potential financial devastation.

</summary>

"The potential liability is huge. It's much bigger than I think the Trump world understands."
"Having the right information will make all the difference."

### AI summary (High error rate! Edit errors on video page)

Recapping recent legal developments involving Trump and his businesses, focusing on New York and Georgia.
New York judge ruled Trump Organization liable for fraud, which could lead to canceling its ability to operate in the state.
Delay in ruling on whether houses owned by Trump companies will be affected by the decision.
Trial expected to be shorter due to key decision made in summary judgment.
Trump's team reportedly trying to suppress infamous phone call about finding extra votes in Georgia.
Georgia jurors' identities to remain secret.
DA in Georgia dismisses threats against her, particularly from House Republicans.
Mention of Mark Meadows reportedly burning documents in his office fireplace.
Challenges to Trump's Presidential Records Act defense and denial of recusal request in D.C. election interference case.
Potential probe into Trump's political operations and payments to lawyers for witnesses.
Fake electors in Michigan seeking case dismissal based on brainwashing claims.
Trump's comments on General Milley potentially causing issues for him.
Warning of potential financial devastation for Trump from legal proceedings.

Actions:

for legal analysts, political commentators,
Contact local news outlets to stay informed about ongoing legal proceedings and developments (suggested)
Stay engaged with political and legal news to understand the implications of these cases (implied)
Support organizations advocating for accountability and transparency in political activities (implied)
</details>
<details>
<summary>
2023-09-28: Let's talk about the Biden inquiry's memorable part.... (<a href="https://youtube.com/watch?v=jAVNO4SC1lU">watch</a> || <a href="/videos/2023/09/28/Lets_talk_about_the_Biden_inquiry_s_memorable_part">transcript &amp; editable summary</a>)

House Republicans prioritize impeachment over preventing a government shutdown that could harm millions of working-class Americans financially.

</summary>

"Playing impeachment is the priority as the clock winds down and millions of working-class Americans, many of whom are their constituents, get hurt."
"Keeping the lights on, stopping millions of working-class Americans from suffering financial hardship, that's not important."
"This was the priority."
"Doing something to prevent this, you're right, had everything gone according to the way things should work, nobody would have noticed."
"They will notice when their checks stop coming, and there will be anger generated."

### AI summary (High error rate! Edit errors on video page)

House Republicans initiated an inquiry to determine whether to impeach the current president.
Their witness stated that the current evidence does not support articles of impeachment.
Despite this, House Republicans are pursuing further investigation with 12,000 pages of bank records.
The inquiry into impeachment has taken precedence over preventing a government shutdown.
In roughly 61 hours, a government shutdown may occur, impacting millions of Americans who could stop receiving paychecks.
Some individuals may not even be aware of how they will be affected by the government shutdown.
House Republicans have prioritized the impeachment inquiry over helping working-class Americans.
Beau criticizes the Republican Party for losing sight of the real issues and focusing on impeachment for social media engagement.
He points out that preventing financial hardship for Americans should take precedence over impeachment proceedings.
Beau urges people to pay attention to the fact that millions of Americans may suffer due to the government shutdown while Congress prioritizes impeachment.

Actions:

for american citizens,
Contact your representatives to urge them to prioritize preventing a government shutdown and supporting working-class Americans (implied).
Stay informed about the government shutdown situation and how it may impact you and your community (implied).
</details>
<details>
<summary>
2023-09-28: Let's talk about the 2nd GOP debate.... (<a href="https://youtube.com/watch?v=tQakZ3LpJEg">watch</a> || <a href="/videos/2023/09/28/Lets_talk_about_the_2nd_GOP_debate">transcript &amp; editable summary</a>)

Beau provides a scathing overview of the lackluster Republican primary debates, pointing out the absence of substantial policy debate and missed opportunities to challenge Trump, ultimately suggesting that the Democratic Party may be the real winner.

</summary>

"If you skipped it, wise decision. If you watched it, I apologize."
"The real winner is Democrats if they're paying attention."
"It was just bad. That's the only word for it."
"None of them got that breakout moment for them."
"No substantial policy discussion, nothing."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of the recent Republican presidential primary debates.
Describing the lackluster and disappointing nature of the debates.
Mentioning the lack of substantial policy debate and the absence of engaging moments.
Noting how Republican candidates avoided discussing reproductive rights, a departure from traditional strategies.
Speculating on the impact the debates may have had on Republican primary voters.
Expressing a view that none of the candidates effectively challenged Trump's position.
Suggesting that Democratic strategists could capitalize on the weaknesses shown by the Republican candidates.
Emphasizing the missed opportunities by all candidates to make a significant impact and challenge Trump.
Concluding that the Democratic Party might be the real winner from these debates.

Actions:

for political analysts, democratic strategists,
Analyze the weaknesses shown by Republican candidates on reproductive rights and strategize accordingly (implied).
Monitor the reactions and responses of Republican primary voters to the debates (implied).
</details>
<details>
<summary>
2023-09-28: Let's talk about Biden, Bernie, and UAW.... (<a href="https://youtube.com/watch?v=qRboj1lrb-g">watch</a> || <a href="/videos/2023/09/28/Lets_talk_about_Biden_Bernie_and_UAW">transcript &amp; editable summary</a>)

Beau explains why Bernie's support for Biden and Biden's appearance at a picket line are significant moments often misunderstood and misrepresented, shedding light on the importance of advocating for workers' rights.

</summary>

"A sitting president of the United States showed up at a picket line of an active strike and threw the weight of the Oval Office behind the workers."
"I think Bernie was just trying to acknowledge that and use it. Use it. Use the power of the president showing up to get that message out there."
"Biden showing up was for show. Absolutely. Absolutely. That part is true."

### AI summary (High error rate! Edit errors on video page)

Bernie showing support for Biden by making a video is being misconstrued by some as him selling out to corporate Democrats.
Biden's appearance at a UAW picket line with workers is seen as historic by Bernie, not a sign of selling out.
Beau believes Bernie's video was an attempt to draw attention to the importance of Biden's support at the picket line.
The President showing up at a picket line was a significant move that could be used to advocate for striking workers.
Beau argues that Biden supporting some progressive ideas does not mean he is entirely progressive and rejects the narrative of good guys versus bad guys.
Biden's appearance at the picket line was seen as a show, which is a common practice to garner attention and media coverage.
Bernie's video used the President's appearance to shed light on CEO pay packages versus worker wages and the struggles faced by workers.
The decline in union strength has impacted starting wages and job status in sectors like automotive manufacturing.
Beau defends Bernie's actions as a way to bring attention to workers' issues and believes Biden's support was a strategic move for gaining media coverage.
The situation is viewed as a blend of social media narratives and pessimism, but Beau encourages considering the positive impact of the President's appearance at the picket line.

Actions:

for activists, political observers, voters,
Support workers' rights and unions by actively participating in strikes and picket lines (exemplified)
Advocate for fair wages and better working conditions in your community (implied)
Educate others about the importance of unions and the struggles faced by workers (implied)
</details>
<details>
<summary>
2023-09-28: Let's talk about Alabama, black voters, and the Supreme Court.... (<a href="https://youtube.com/watch?v=6Yp-IGwVTXQ">watch</a> || <a href="/videos/2023/09/28/Lets_talk_about_Alabama_black_voters_and_the_Supreme_Court">transcript &amp; editable summary</a>)

The Supreme Court intervened twice to prevent Alabama Republicans from diluting black voters' power through redistricting, reinforcing the need for fair representation.

</summary>

"Alabama Republicans were being told for the second time that they can't dilute the power of black voters because."
"It seems odd that these kinds of court cases are still happening, but on the other hand, I guess it doesn't seem that odd."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court denied Alabama Republicans the ability to diminish the power of black voters through redistricting.
Alabama produced new maps with only one majority black district, prompting federal judges to intervene.
Despite federal judges' orders to create a second majority black district, Alabama Republicans failed to comply.
A special master was appointed to address the issue when Alabama Republicans disregarded federal judges' instructions.
The Supreme Court upheld their decision, reinforcing the need for a second majority black district.
Alabama Republicans' defiance and attempts to dilute black voters' power were strongly condemned by the courts.
The special master submitted three proposals to address the redistricting issue in Alabama.
The proposed maps aim to respect communities of interest while ensuring fair representation.
Selecting one of the special master's proposals will determine the new maps for Alabama.
Overall, the court rulings and interventions aimed to prevent the dilution of black voters' power in Alabama.

Actions:

for voters, activists, legal advocates,
Advocate for fair redistricting in your community (implied)
Stay informed about voting rights issues and legal proceedings (implied)
</details>
<details>
<summary>
2023-09-27: Let's talk about the Senate taking the reins from McCarthy.... (<a href="https://youtube.com/watch?v=Z7iXlilmz_M">watch</a> || <a href="/videos/2023/09/27/Lets_talk_about_the_Senate_taking_the_reins_from_McCarthy">transcript &amp; editable summary</a>)

The Senate steps in to tackle the budget impasse among House Republicans, showcasing the importance of understanding governance over social media engagement.

</summary>

"Republicans in the House are more interested in social media clicks than governing."
"McConnell understands that there's, probably 80, 90,000 people in Kentucky that get a direct federal government."
"The ones that are on there trying to make you angry, those are the bad ones."
"McCarthy has the choice between choosing a government shutdown or his speakership."
"House Republicans causing issues are more interested in social media engagement than passing legislation."

### AI summary (High error rate! Edit errors on video page)

The Senate is stepping in to deal with the budget impasse among Republicans in the House.
Republicans in the House are more focused on social media clicks than governing, leading to a potential government shutdown.
A bipartisan budget is being pushed through by the Senate with no controversial elements like continued funding for Ukraine and disaster relief.
Senators are able to work across the aisle and come to compromises because they understand the importance of keeping the government running.
McConnell is willing to work together to avoid a government shutdown, showcasing a stark difference from the Republicans in the House.
The House Republicans causing issues are the ones more interested in social media engagement than passing legislation.
McCarthy has the choice between avoiding a government shutdown and losing his speakership, with the blame likely falling on House Republicans if a shutdown occurs.

Actions:

for politically engaged individuals,
Contact your representatives to urge them to prioritize governance over social media engagement (implied)
</details>
<details>
<summary>
2023-09-27: Let's talk about Trump being found liable in New York.... (<a href="https://youtube.com/watch?v=b3e0Jwk5C_I">watch</a> || <a href="/videos/2023/09/27/Lets_talk_about_Trump_being_found_liable_in_New_York">transcript &amp; editable summary</a>)

Major developments in Trump's New York civil case ruling him liable for fraud reshape upcoming events, potentially devastating for his business and political aspirations.

</summary>

"In defendant's world, rent-regulated apartments are worth the same as unregulated apartments."
"Trump found liable for fraud, right? You're going to see that everywhere."
"This may be something that really starts to cast a shadow on any political aspirations he may have."

### AI summary (High error rate! Edit errors on video page)

Major developments in one of Trump's entanglements are completely reshaping events for next week.
The focus is on the New York civil case for $250 million where the judge ruled Trump liable for fraud.
The judgment narrows down the trial set to start on October 2nd to focus on potential damages.
Trump faces severe issues as business certificates were yanked, possibly leading to loss of his flagship company.
Attorneys on Trump's side were sanctioned for frivolous arguments, with potential fallout affecting his business empire and political aspirations.
The events of the day are devastating for Trump, making a positive resolution unlikely.
The stress from these developments may lead to more erratic behavior and statements from Trump.

Actions:

for legal analysts, political commentators,
Monitor updates on the case and its impacts on Trump's future (implied).
</details>
<details>
<summary>
2023-09-27: Let's talk about Russians selling info to Ukraine.... (<a href="https://youtube.com/watch?v=HMUZM9vVHCc">watch</a> || <a href="/videos/2023/09/27/Lets_talk_about_Russians_selling_info_to_Ukraine">transcript &amp; editable summary</a>)

Beau delves into a story about Russian officers potentially selling information to Ukrainian Special Operations, stressing that the truth holds little significance compared to anticipating responses in a climate of paranoia and distrust.

</summary>

"The truth doesn't matter in this situation at all. It's the first casualty."
"The real value is trying to figure out what the responses are going to be."
"When that investigation occurs in a military that is just wracked with paranoia and distrust, they're going to find the traitor."

### AI summary (High error rate! Edit errors on video page)

Introduction to discussing a story about Russian officers potentially selling information to Ukrainian Special Operations.
Emphasis on the debate surrounding the truth of the story and why its veracity may not ultimately matter.
Mention of a similar previous occurrence where the believability of a story outweighed its truth.
Detailing the story about Russian officers in Crimea selling information to a local partisan organization.
Acknowledgment that whether the story is true or fabricated, it still warrants investigation due to its believability.
Analysis of the political climate and military dynamics in Russia influencing the need to act on the story.
Mention of potential distrust among Russian officers towards their command and the possibility of information leaks.
Speculation on the existence of a partisan organization and the potential consequences of investigating a fabricated story.
Assertion that investigations in a paranoid military environment are likely to result in finding a traitor, regardless of the truth.
Stating that the truth of the story is not as critical as anticipating reactions and responses to it.
Concluding with the idea that the truth may only be known after the war and that the focus should be on predicting responses.

Actions:

for analysts, strategists, commentators,
Contact Ukrainian Special Operations for support in investigating potential information leaks by Russian officers (implied)
Coordinate with local organizations to monitor and respond to any developments related to the story (implied)
</details>
<details>
<summary>
2023-09-27: Let's talk about 2 questions about Ukraine and talking points.... (<a href="https://youtube.com/watch?v=zKaKdzTIIfg">watch</a> || <a href="/videos/2023/09/27/Lets_talk_about_2_questions_about_Ukraine_and_talking_points">transcript &amp; editable summary</a>)

Beau debunks common misconceptions about the situation in Ukraine, clarifying Russia's losses and the true impact of casualties.

</summary>

"Russia already lost the war."
"A lot of times information can be presented in a way that disregards the reality."
"Russia is not doing well, and you don't have to take a Western commentator's word for it."
"There is no way that Russia exits this elective war in a better position than when it started."
"Everything that is being determined now is just waiting for Russia to realize it."

### AI summary (High error rate! Edit errors on video page)

Addressing two common talking points regarding the situation in Ukraine.
Russia does not have the initiative and is losing territory in Ukraine.
The casualties and amputees in Ukraine do not accurately represent the situation.
Stories of military helmet complaints and aircraft armor study to illustrate misleading information.
Ukraine having more amputees does not mean they are losing; it shows their ability to provide advanced care.
Russia is not faring well in the conflict, as indicated by Putin's directive and Lavrov's statements.
Wish-casting and misinformation are prevalent in analyzing the conflict.
Russia is not in a winning position and will not come out of the war in a better position.

Actions:

for ukrainian supporters, conflict analysts,
Support organizations providing advanced care for wounded soldiers (implied)
Stay informed and combat misinformation in your circles (implied)
</details>
<details>
<summary>
2023-09-26: Let's talk about the Governors of Florida and California.... (<a href="https://youtube.com/watch?v=qWN-TOoZd2I">watch</a> || <a href="/videos/2023/09/26/Lets_talk_about_the_Governors_of_Florida_and_California">transcript &amp; editable summary</a>)

California and Florida governors set for a debate on Fox with Hannity, seeking political gains and national recognition.

</summary>

"The grudge match of the governors."
"He might be one of those people who believes that eventually Trump will not be in the running."
"Why do you rob banks? That's where the money is."
"For Newsome, all he has to do is not lose."
"If you don't want to watch it, don't worry, I will so you don't have to."

### AI summary (High error rate! Edit errors on video page)

California and Florida governors agreed to a debate on Fox with Hannity as the moderator.
Governor of Florida seeks Republican nomination, campaign not going well, hoping for an alternative outcome.
Governor of California, Newsom, likely planning to run for president in the future, building name recognition.
Both governors have political gains from the debate, reinvigorating campaigns and building national profiles.
Newsom may have agreed to Fox debate to showcase his debating skills and reach Fox viewers.
Governor of Florida needs fireworks to boost his campaign, while Newsom just needs to avoid losing.
The debate may produce interesting moments due to the dynamics at play.

Actions:

for political observers,
Watch the debate to understand the dynamics and potential impact. (suggested)
Stay informed about political events and debates. (implied)
</details>
<details>
<summary>
2023-09-26: Let's talk about how the Eyes had it in Canada.... (<a href="https://youtube.com/watch?v=ixAHoywZFZI">watch</a> || <a href="/videos/2023/09/26/Lets_talk_about_how_the_Eyes_had_it_in_Canada">transcript &amp; editable summary</a>)

Beau explains the nature of the Five Eyes intelligence alliance, likening it to a collaborative library, and addresses privacy concerns and conspiracy theories surrounding international intelligence sharing.

</summary>

"Five Eyes is not an intelligence agency. It's more like a library."
"It's not an international spy agency. It's more like a library."
"There's way more than the Five Eyes."
"Privacy concerns exist and they're real."
"It's a library."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the Five Eyes intelligence alliance, consisting of the United States, the United Kingdom, Australia, New Zealand, and Canada.
Clarifies that the Five Eyes is not an intelligence agency but rather a collaborative effort among these countries to share information.
Traces the origins of the Five Eyes back to 1941 when US and UK code breakers began sharing information during WWII.
Compares the Five Eyes to a library where each "book" (piece of intelligence) was donated by a spy from one of the member countries.
Mentions the existence of other intelligence-sharing agreements like the Nine Eyes and the 14 Eyes, involving different countries.
Notes that privacy concerns arise due to the nature of intelligence sharing among these allied nations.
Addresses conspiracy theories that have emerged around the Five Eyes concept despite it being a longstanding arrangement.
Emphasizes that while the Five Eyes alliance was initially focused on signals intelligence, it has expanded to include various types of intelligence over the years.
Points out controversies like Echelon that have arisen regarding international intelligence sharing.
Concludes by reiterating that the Five Eyes is more akin to a collaborative structure than a centralized spy agency, with agencies from member countries sharing information.

Actions:

for global citizens, policymakers,
Contact local representatives to advocate for transparency and accountability in intelligence-sharing agreements (suggested)
Join advocacy groups promoting privacy rights and oversight of government surveillance activities (suggested)
</details>
<details>
<summary>
2023-09-26: Let's talk about Virginia, luck, and leakage.... (<a href="https://youtube.com/watch?v=1wkU_qPUVo0">watch</a> || <a href="/videos/2023/09/26/Lets_talk_about_Virginia_luck_and_leakage">transcript &amp; editable summary</a>)

A person's keen observation on Instagram led to the prevention of a potential mass shooting by recognizing and acting on leakage markers.

</summary>

"Believe them. If somebody says or leaves something around indicating they're gonna do something like this, believe them."
"Understanding leakage markers and being vigilant can be vital in preventing harmful incidents."
"A person identified concerning content on Instagram related to a church in Virginia and alerted the authorities."
"The concept of 'leakage' involves visible signs or markers that individuals planning harmful acts often display before carrying them out."
"Recognizing leakage markers and taking them seriously can save lives by preventing tragedies from occurring."

### AI summary (High error rate! Edit errors on video page)

A person identified concerning content on Instagram related to a church in Virginia and alerted the authorities.
The person's tip led to the prevention of a potential mass shooting at the church.
The concept of "leakage" involves visible signs or markers that individuals planning harmful acts often display before carrying them out.
Many people tend to overlook these signs as jokes or harmless, leading to missed prevention opportunities.
Recognizing leakage markers and taking them seriously can save lives by preventing tragedies from occurring.
It is debated whether leakage is subconscious or a cry for help, but its presence can help stop dangerous situations.
Understanding leakage markers and being vigilant can be vital in preventing harmful incidents.
The person's awareness of leakage and quick action on Instagram potentially saved many lives.
While the cause behind leakage behavior remains uncertain, its patterns have been observed and can be acted upon to avert disasters.
Beau urges viewers to familiarize themselves with the concept of leakage and its markers for early intervention in potentially harmful situations.

Actions:

for community members,
Familiarize yourself with the concept of leakage and its markers (suggested)
Stay vigilant and observant for potential leakage markers in your surroundings (implied)
Take prompt action if you notice concerning behavior or signs of leakage (implied)
</details>
<details>
<summary>
2023-09-26: Let's talk about Ukraine, the Black Sea, and numbers.... (<a href="https://youtube.com/watch?v=kkm47MBmc6w">watch</a> || <a href="/videos/2023/09/26/Lets_talk_about_Ukraine_the_Black_Sea_and_numbers">transcript &amp; editable summary</a>)

Beau talks about recent events in Ukraine, speculating on casualties, the impact on Russian leadership, and the development of Ukraine's special operations community.

</summary>

"The precision with which it occurred, and the timing, it is definitely showing that Ukraine's special operations community is coming into its own."
"That's pretty disruptive to command and control."
"They may be a little bit more potent than people who can run on logs and, you know, do a backflip throwing a hatchet."

### AI summary (High error rate! Edit errors on video page)

Talks about recent events in Ukraine involving speculation and confusion.
Mentions a Ukrainian operation leading to a strike against a Russian naval headquarters.
Raises questions about the actual numbers of casualties in the strike.
Speculates that different sets of numbers provided may be accurate, describing different groups of people.
Mentions the critical condition of a Russian general overseeing a key portion of the lines.
Addresses the commander of the Black Sea Fleet possibly not attending future trials at The Hague.
Points out the disruptive nature of the strike on command and control.
Notes the development and precision of Ukraine's special operations community.
Comments on Russia's response with strikes targeting civilian infrastructure in Odessa.
Concludes by leaving the audience with a thought to ponder.

Actions:

for global citizens,
Stay informed on international news and developments (suggested).
</details>
<details>
<summary>
2023-09-25: Let's talk about why Trump loves his voters.... (<a href="https://youtube.com/watch?v=m5i8xjsPsyI">watch</a> || <a href="/videos/2023/09/25/Lets_talk_about_why_Trump_loves_his_voters">transcript &amp; editable summary</a>)

Trump openly commands a government shutdown, reassuring his base they won't face blame, banking on their ignorance, and love for manipulation.

</summary>

"Did you catch what he actually said there?"
"He just doesn't think they're smart enough to figure it out."
"That's why Trump loves the uneducated."

### AI summary (High error rate! Edit errors on video page)

Trump professes his love for his base, and it's true; he loves them deeply.
Trump understands that a government shutdown will hurt Americans but reassures his base that they won't be blamed; he'll pin it on Biden.
He openly commands a government shutdown while assuring his base that they won't face the blame.
Trump counts on the ignorance of his base to manipulate them into believing his narrative.
Trump loves his base because they are easy to manipulate and trick into supporting him, even when he's ordering actions that harm Americans.

Actions:

for voters, activists, citizens,
Call out manipulation and misinformation in political leaders (exemplified)
Educate and inform others about political tactics (exemplified)
Stay informed and critically analyze political statements (exemplified)
</details>
<details>
<summary>
2023-09-25: Let's talk about Rand Paul and a billboard.... (<a href="https://youtube.com/watch?v=LFA_7LZdwus">watch</a> || <a href="/videos/2023/09/25/Lets_talk_about_Rand_Paul_and_a_billboard">transcript &amp; editable summary</a>)

Beau warns about the prevalence of fake images going viral and advises waiting for verification to combat misinformation, especially in the context of upcoming elections.

</summary>

"The secret to humor is surprise."
"Your best defense is time."
"Just wait."
"Be on the lookout for it, especially when it comes to the election."
"Be aware and be ready for it."

### AI summary (High error rate! Edit errors on video page)

Two viral images with a surprising connection to a room and a house caught news outlets' attention this weekend.
One image featured Rand Paul in a bathrobe at work, supposedly protesting the Senate's dress code changes.
The other image showed a billboard with a typo involving the Ukrainian flag, attracting media coverage as well.
Both images, although attention-grabbing, turned out to be fake; the Rand Paul one was likely AI-generated.
The tendency for surprising images to go viral is due to the element of surprise that appeals to specific demographics.
Beau notes that humor and viral content often rely on surprises and subverting expectations.
The incident with the fake images underscores the need for better fact-checking by news outlets.
Beau suggests a simple method to avoid falling for fake images: wait 24 hours for verification by reliable sources.
With fake audio and video becoming more sophisticated, taking time before reacting is key to distinguishing truth from misinformation.
As the election approaches, the risk of encountering fake content increases, making it even more vital to verify information before sharing.
Beau advises against relying solely on instant information and urges a more cautious approach to consuming and sharing news.
The lack of user-friendly tools for verifying images underscores the importance of waiting for credible journalism to confirm or debunk stories.
The upcoming election is expected to see a rise in misinformation and influence operations, making it imperative to stay vigilant and skeptical of what is shared online.
Beau warns about the potential for misinformation to spread through social media and encourages readiness to counter false narratives with verified information.

Actions:

for media consumers,
Wait 24 hours before sharing surprising or emotional images online to allow for verification by reliable sources (suggested).
Stay vigilant and skeptical of potentially fake content, especially as the election approaches (implied).
</details>
<details>
<summary>
2023-09-25: Let's talk about New Orleans, water, and engineers.... (<a href="https://youtube.com/watch?v=ycJaQig7gWo">watch</a> || <a href="/videos/2023/09/25/Lets_talk_about_New_Orleans_water_and_engineers">transcript &amp; editable summary</a>)

Officials are concerned about salt water intrusion in New Orleans due to weak Mississippi flow caused by climate change, requiring ongoing vigilance and relief efforts.

</summary>

"Climate change is real. It doesn't matter if you want to ignore it or not. Eventually you're not going to be able to."
"This type of stuff will continue to happen and it will get worse."

### AI summary (High error rate! Edit errors on video page)

Officials are concerned about salt water intrusion into New Orleans from the Mississippi River.
Salt water intrusion is due to the Mississippi River's weak flow, causing ocean water to move north.
This intrusion impacts drinking water systems in the area.
The Army Corps of Engineers is planning to mitigate the issue by adding height to levees and bringing in 36 million gallons of fresh water daily.
The cause of this intrusion is linked to a drought in the Central United States, which weakens the flow of the Mississippi River.
Climate change is identified as the root cause of the drought and subsequent issues.
Despite past issues, the Army Corps of Engineers is expected to handle the situation effectively.
Residents in the area need to stay informed and vigilant as the situation progresses.
The plan in place should work, but it's only a relief measure, not addressing the underlying causes like climate change.
Continuous occurrences of such issues are expected and may worsen over time.

Actions:

for residents in the area,
Stay informed and vigilant about the salt water intrusion issue in New Orleans (implied)
</details>
<details>
<summary>
2023-09-25: Let's talk about McCarthy, Ukraine, and confusion.... (<a href="https://youtube.com/watch?v=6wGI0WDHz18">watch</a> || <a href="/videos/2023/09/25/Lets_talk_about_McCarthy_Ukraine_and_confusion">transcript &amp; editable summary</a>)

Beau breaks down the chaotic U.S. budget situation, advising consistency in news consumption amid conflicting reports and stressing the global impact of American political dysfunction.

</summary>

"Basically, it's a clown show up there."
"It's a mess."
"The politicians up there [...] there is not a unified position."
"If this is something that is stressing you out, limit your consumption."
"There are countries all over the world [...] catch a cold when the US gets sick."

### AI summary (High error rate! Edit errors on video page)

Explains the confusion surrounding Ukraine's aid in the U.S. budget, with contradictory news reports.
Mentions a representative from Georgia, dubbed the "space laser lady," initially opposing aid to Ukraine.
Indicates that the aid issue has seen many changes, with Republicans eventually supporting aid to Ukraine.
Emphasizes the importance of continued aid to Ukraine for both Ukrainian and U.S. interests.
States that experts in foreign policy generally agree on the necessity of aid to Ukraine.
Describes the chaotic situation in the U.S. House of Representatives regarding the budget debates.
Notes the uncertainty around funding for programs like food delivery to low-income senior citizens.
Advises limiting daily consumption of news on the budget debates due to rapidly changing information.
Points out the lack of unified positions within the U.S. House of Representatives, contrasting with the Senate's support for aid.
Encourages consistency in checking news updates to avoid conflicting reports amid the ongoing changes.

Actions:

for global citizens concerned about u.s. political dysfunction.,
Limit your news consumption to once a day for clarity on the evolving situation (suggested).
Stay updated on the budget debates at the same time daily to avoid conflicting reports (implied).
</details>
<details>
<summary>
2023-09-24: The Roads Not Taken EP6 (<a href="https://youtube.com/watch?v=e6XCHOJ1eQ8">watch</a> || <a href="/videos/2023/09/24/The_Roads_Not_Taken_EP6">transcript &amp; editable summary</a>)

Beau covers foreign policy, missed opportunities in US politics, indigenous land rights, Russian conscript confusion, and South Korean investigations.

</summary>

"He lost the opportunity to have Republicans standing beside Zelensky as he talked about how vital that relationship with the United States is."
"Nobody who has less institutional power than you is the source of your problem."
"Shovels can be incredibly significant and useful, especially in situations you might not think about it."

### AI summary (High error rate! Edit errors on video page)

Beau introduces episode six of "The Road's Not Taken," a series where he covers under-reported stories and provides context.
He starts off discussing foreign policy, mentioning the F-35 aircraft and NATO's tactics to decentralize aircraft for survivability.
Beau criticizes the missed opportunities of the current Speaker of the House, McCarthy, regarding Zelensky's visit to the US.
He talks about indigenous land rights in Brazil being enshrined by the high court, anticipating future conflicts due to resource interests.
Beau analyzes confusion surrounding open-source data on Russian conscript loss rates, noting Russia's use of less trained personnel in combat.
He mentions a situation in South Korea where US military installations were raided, leading to investigations of US service members for involvement in a drug ring.
Beau reports on Bulgarians accused of spying for Russia in the UK, hinting at significant espionage activities.
He comments on Russia deploying elite paratroopers as regular infantry, showcasing a lack of appropriate personnel.
Beau shifts focus to the United States, mentioning an investigation into misconduct allegations against Baton Rouge cops by the Civil Rights Division.
He notes the expansion of the United Auto Workers Strike and Biden's potential visit to support the union workers.
Beau touches on Trump's promise of mass deportations if re-elected and the importance of not blaming those with less power for problems.

Actions:

for policy influencers, activists,
Contact local representatives to advocate for indigenous land rights protections (suggested)
Support unions and workers' rights by joining local labor movements (implied)
</details>
<details>
<summary>
2023-09-24: Let's talk about unanswered calls, Menendez, and traditions.... (<a href="https://youtube.com/watch?v=xwGUU_MbH6I">watch</a> || <a href="/videos/2023/09/24/Lets_talk_about_unanswered_calls_Menendez_and_traditions">transcript &amp; editable summary</a>)

Senator Menendez indicted on bribery charges sparks calls for resignation, unveiling a new trend in Congress where indicted politicians relinquish committees instead of resigning, raising concerns about accountability and a dangerous precedent for the nation.

</summary>

"They're almost not expected to be. They're not expected to be held to the same standards."
"Those countries that openly accept the idea that their betters truly are better and they can go by a different set of laws, it doesn't go well for that country."

### AI summary (High error rate! Edit errors on video page)

Senator Bob Menendez from New Jersey has been indicted on bribery charges, leading to calls for his resignation from the Democratic Party.
Menendez has decided to step down from the Senate Foreign Relations Committee but not resign from the Senate itself.
This situation echoes a new tradition in Congress where politicians facing indictments give up committee assignments rather than resigning.
Previously, the tradition demanded resignation upon indictment, but now it seems to have shifted towards relinquishing committee roles.
The change in this tradition may have occurred in the last six years during an administration that openly disregarded laws and norms.
The current leniency towards indicted politicians suggests they are not held to the same standards as the general public.
The Democratic Party is likely to intensify calls for Menendez's resignation as legal proceedings progress.
Politicians being held to lower standards than the average citizen historically leads to negative outcomes for countries.
Accepting that elites are above the law can result in rapid deterioration of a nation.
The lack of accountability for politicians sets a dangerous precedent for the country's future.

Actions:

for politically engaged individuals,
Advocate for stricter accountability measures for politicians (implied)
Stay informed about politicians' actions and hold them accountable (implied)
</details>
<details>
<summary>
2023-09-24: Let's talk about Trump leading Biden in a poll.... (<a href="https://youtube.com/watch?v=NCAR4ORUY24">watch</a> || <a href="/videos/2023/09/24/Lets_talk_about_Trump_leading_Biden_in_a_poll">transcript &amp; editable summary</a>)

Beau explains the limitations of early polling and advises caution in interpreting results due to inaccuracies and demographic biases.

</summary>

"Polling this far out when it comes to how people are going to vote, it means nothing, absolutely nothing."
"I wouldn't worry about it too much, other than this is going to give the Trump campaign huge amount of ammunition."
"There's a whole lot of things that will happen between now and the election that might alter the results of this poll."
"I cannot imagine a world in which Trump beats Biden by ten points in the general."
"I'd be very cautious putting too much stock in polling at this point until pollsters can figure out how to reach the people who are not answering their phones."

### AI summary (High error rate! Edit errors on video page)

Explains a recent poll showing Trump with a 10-point lead and the potential impact.
Acknowledges that the polling may not be accurate, citing discrepancies with other polls.
Points out flaws in current polling methods, such as reaching only a specific demographic.
Emphasizes the uncertainty of predicting election outcomes based on early polling.
Raises scenarios like economic changes or legal issues that could drastically alter election results.
Suggests caution in taking polling results too seriously due to their limitations and potential inaccuracies.
Notes the potential for polling to become even less representative in the future.
Raises questions about the timing and implications of the poll results within the political landscape.
Considers the possibility of the poll being an outlier rather than a true reflection of voter sentiment.
Encourages skepticism towards polling until methods improve to reach a more diverse sample.

Actions:

for political analysts, voters,
Wait for more accurate polling methods to be developed (implied)
</details>
<details>
<summary>
2023-09-24: Let's talk about Taylor Swift's fans showing up.... (<a href="https://youtube.com/watch?v=V-YOYe_XM_U">watch</a> || <a href="/videos/2023/09/24/Lets_talk_about_Taylor_Swift_s_fans_showing_up">transcript &amp; editable summary</a>)

Taylor Swift's social media post led to a significant increase in voter registration, especially among 18-year-olds, stressing the importance of showing up to vote for actual change.

</summary>

"It led to tens of thousands of people registering to vote."
"Not showing up when it actually matters, when it's time to vote."
"You have to show up. You have to do it."
"In some places her activity alone will swing races."
"I think that they understand that shade never made anybody."

### AI summary (High error rate! Edit errors on video page)

Taylor Swift's social media post directing people to vote.org led to tens of thousands registering to vote, averaging 13,000 users every 30 minutes.
This resulted in a 23% increase for National Voter Registration Day, all from an Instagram post.
The post also caused a 115% increase in 18-year-olds registering to vote, signifying a substantial impact.
Many younger people seem unhappy with the current state of things and are motivated to make a change.
Beau stresses that registering to vote is just the first step, and it's vital to show up and vote when it really matters.
He warns against letting the momentum fade and not following through with voting after registering.
Beau urges people to understand the importance of showing up to vote to bring about the desired change.
He mentions that sustained engagement and messaging from Taylor Swift could have a significant impact on certain races.
Younger people, particularly those affected by current legislation, might have a higher turnout due to their friends being targeted.
Beau expresses hope for increased youth participation in voting, given the circumstances and motivations.

Actions:

for young voters,
Register to vote and encourage others to do so (exemplified)
Show up and vote in elections (implied)
</details>
<details>
<summary>
2023-09-24: Let's talk about Dallas, politics, and a change.... (<a href="https://youtube.com/watch?v=Kwf6Q6v3jxY">watch</a> || <a href="/videos/2023/09/24/Lets_talk_about_Dallas_politics_and_a_change">transcript &amp; editable summary</a>)

The mayor of Dallas switching parties sheds light on political opportunism and the prioritization of party over policy in American politics, reflecting broader issues in Texas and the US.

</summary>

"If you will vote for somebody simply based off of party affiliation, nothing to do with policies, nothing to do with track record, that's an issue."
"The focus on party over policy is an indictment of the American voter."
"It's more beneficial to have an R after your name than a successful track record."
"Voters are more interested in party than policy."
"That loyalty mattering that much is an indictment of the American voter."

### AI summary (High error rate! Edit errors on video page)

The mayor of Dallas switched to the Republican party, making Dallas the largest city in the US with a Republican mayor.
This move in Dallas sheds light on broader political issues in Texas and the US, reflecting a deep-seated problem in American politics.
The switch may signify the mayor's intention to run for a different office, possibly statewide, where having an "R" after his name could be more beneficial than a successful track record.
The focus on party affiliation over policies and track record is criticized as a flaw in the American voter mentality.
Beau questions the voters' tendency to prioritize party loyalty over other factors when electing officials.
The importance of party affiliation over policy positions is viewed as an indictment of American voters.
The mayor's party switch raises concerns about political opportunism and ambition rather than genuine alignment with new policies.
Beau suggests that voters basing their decisions solely on party affiliation contributes to the current state of the House of Representatives.

Actions:

for american voters,
Question candidates on their policies and track record before casting your vote (suggested).
Research candidates beyond their party affiliation to make an informed decision when voting (implied).
</details>
<details>
<summary>
2023-09-23: Let's talk about the potential shutdown and proposed CR.... (<a href="https://youtube.com/watch?v=djUSHghItYg">watch</a> || <a href="/videos/2023/09/23/Lets_talk_about_the_potential_shutdown_and_proposed_CR">transcript &amp; editable summary</a>)

Beau explains the potential impacts of a government shutdown and the proposed Republican budget cuts, urging people to seek detailed information rather than relying on labels like "extreme budget."

</summary>

"each thing impacts the next."
"you're going to lose roughly 800 Border Patrol people."
"there's something on that list they don't want to give up."
"maybe there's some money there that could be used just saying."
"Just because somebody says, well, it's an extreme budget, yeah, good call. Don't take their word for it."

### AI summary (High error rate! Edit errors on video page)

Explains the potential impact of a government shutdown on federal workers and services.
Points out the ripple effects of federal workers not getting paid, affecting the local economy.
Breaks down the proposed Republican continuing resolution and its potential consequences, like an 8% cut leading to severe impacts on various services and programs.
Raises concerns about vulnerable populations bearing the brunt of budget cuts.
Suggests prioritizing cuts from sources like companies not paying taxes rather than affecting the most vulnerable.
Encourages seeking detailed information and context rather than relying solely on labels like "extreme budget."

Actions:

for budget-conscious citizens,
Contact local representatives to express concerns about the potential impact of budget cuts on vulnerable populations (implied).
Join advocacy groups that provide detailed information on proposed budget cuts and their consequences (implied).
</details>
<details>
<summary>
2023-09-23: Let's talk about Trump wanting a shutdown.... (<a href="https://youtube.com/watch?v=Bcf4xCBRmXY">watch</a> || <a href="/videos/2023/09/23/Lets_talk_about_Trump_wanting_a_shutdown">transcript &amp; editable summary</a>)

Former President Trump is willing to cause economic disturbance by shutting down the government to stop prosecutions against him, showing a lack of understanding of government processes and potential economic damage.

</summary>

"Your paycheck, you can do without that."
"He's telling you right now, your paycheck, you can do without that."
"He doesn't know how the government works."
"That's what he wants. It's what it's always about for him."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Former President Trump is willing to shut down the government to stop prosecutions against him.
Republicans control the House, but defunding prosecutions won't go anywhere without Senate approval.
Trump's plan to defund prosecutions won't work and lacks majority support in the House.
Even if a government shutdown occurs, it won't stop the prosecutions due to permanent appropriations.
Trump's actions show a lack of understanding of government processes and potential economic damage.

Actions:

for politically aware citizens,
Share information on the implications of a government shutdown and the reasons behind it (suggested)
Educate others on the concept of permanent indefinite appropriations (suggested)
</details>
<details>
<summary>
2023-09-23: Let's talk about Bennu and you.... (<a href="https://youtube.com/watch?v=AWaZqonlS8o">watch</a> || <a href="/videos/2023/09/23/Lets_talk_about_Bennu_and_you">transcript &amp; editable summary</a>)

Beau explains the low chances of the asteroid Bennu hitting Earth, despite sensational headlines, due to its scare factor and potential catastrophic impact.

</summary>

"This should not be high on your anxiety list."
"The odds are 0.037%, so I mean, I don't think any of us have to worry about that."
"Fear is good for clicks."
"It's going to be a problem."
"This is a big deal."

### AI summary (High error rate! Edit errors on video page)

Explaining the recent headlines about the asteroid Bennu potentially hitting Earth in the future.
Providing context about the odds of impact and the most likely date.
Emphasizing that the chances of Bennu hitting Earth are extremely low, at 0.037%.
Pointing out that the asteroid generating headlines is due to the fear factor and sensationalism.
Describing the estimated force of impact if Bennu were to hit, equating it to 1200 megatons, far exceeding the power of current nuclear weapons.
Comparing the potential impact of Bennu to historical events like World War II.

Actions:

for space enthusiasts,
Monitor updates on Bennu's trajectory and potential impact (implied)
Support scientific research and funding for asteroid detection and deflection (implied)
</details>
<details>
<summary>
2023-09-21: The Roads to 100K and a Q&A.... (<a href="https://youtube.com/watch?v=LJ8fmVCDOkQ">watch</a> || <a href="/videos/2023/09/21/The_Roads_to_100K_and_a_Q_A">transcript &amp; editable summary</a>)

Beau shares insights, answers questions, and offers advice on various topics in a celebratory Q&A session for reaching 100,000 subscribers.

</summary>

"Do what you're best at. Focus on where you can be most effective."
"The only thing that could convince independents to vote for Trump in 2024 is the economy."
"The answer is it's in the protracted stage. It's going to be a while."
"Celebrate reaching 100,000 subscribers with a lighthearted Q&A session."
"If everybody got where they were most effective, it would be incredibly helpful."

### AI summary (High error rate! Edit errors on video page)

Celebrating reaching 100,000 subscribers with a lighthearted Q&A session.
Reveals a thumb injury from a werewolf Halloween decoration mishap.
Answers questions about marriage advice, brand deals, and personal life.
Shares insights on the left's fascination with certain regimes and conspiracy theories.
Talks about family dynamics, future plans, and community building.
Explains the importance of education in understanding geopolitics.
Touches on personal interests, hobbies, and favorite TV shows.
Addresses audience questions about various topics, including historical books and US alliances.
Gives advice on focusing efforts to make a meaningful impact in the world.
Mentions future plans for content creation and engagement with the audience.

Actions:

for viewers seeking insights, advice, and a lighthearted q&a session celebrating 100,000 subscribers.,
Reach out to organizations or groups that focus on community networking and support their initiatives (implied).
Focus on what you are best at to make a meaningful impact in the world (implied).
Stay informed about current events and geopolitical issues to understand the world better (implied).
</details>
<details>
<summary>
2023-09-17: The Roads Not Taken EP 5 (<a href="https://youtube.com/watch?v=iAQ7BBgzE04">watch</a> || <a href="/videos/2023/09/17/The_Roads_Not_Taken_EP_5">transcript &amp; editable summary</a>)

Beau analyzes under-reported global events, from Libyan floods to evolving Russian tactics, questioning foreign policy and US influence on Canada.

</summary>

"Foreign policy is about power, nothing else. Don't get it twisted."
"They don't want to represent, they want to rule."
"The world may be better if morality and fair play mattered in foreign policy, but they don't."
"It's not the content of the document that matters. It's why he had them."
"Don't cross the picket line."

### AI summary (High error rate! Edit errors on video page)

Introduces "The Road's Not Taken" weekly series focusing on under-reported events and stories that will provide context later on.
Reports on a powerful storm in Libya that flooded the city of Derma, causing unknown casualties but estimated at more than 10,000.
Points out how economic issues in China might lessen the likelihood of Chinese intervention in Taiwan according to foreign policy experts.
Mentions the trial of alleged leaders of the Ottawa convoy demonstrations in Canada, showing the influence of US events on Canadian politics.
Notes a shift in Russia's artillery tactics towards accuracy rather than saturation, potentially impacting their relationship with North Korea.
Unveils the UK's new jet propelled drone, the Hydra 400, armed with laser-guided brimstone weapons, specifically designed as a tank killer.
Reveals the discovery of the world's largest known lithium deposit in the United States, with significant foreign policy implications due to its necessity for electric vehicles.
Comments on the Texas Attorney General's acquittal in the Senate trial and suggests Texas Republicans may face consequences for it.
Mentions Trump's accusations against President Biden and his son's indictment, posing a test for gun rights activists.
Criticizes candidate Vivek's promise to deport US-born children of undocumented parents, a violation of the US Constitution.

Actions:

for global citizens,
Watch "The Roads to Ukraine" video for insights on Armenia and foreign policy (suggested).
Support strike funds and showcase demands of strikers to show solidarity (exemplified).
</details>
<details>
<summary>
2023-09-14: The Roads Not Taken Special Trump Exhibit B (<a href="https://youtube.com/watch?v=R7Mpz974Zwk">watch</a> || <a href="/videos/2023/09/14/The_Roads_Not_Taken_Special_Trump_Exhibit_B">transcript &amp; editable summary</a>)

Beau runs through Trump's legal entanglements, from classified documents to immunity defenses, providing key insights often overlooked.

</summary>

"After all of this time, after they were exposed and out in the wild, given that they're still being treated as TS documents, it's going to be really hard for him to claim that storing them in a bathroom or on a stage was within any kind of rag at all."
"There's always maneuvering that is occurring behind the scenes."
"The penalty for this censure is not much."
"Let's be honest, there's not a lot of those."
"Having the right information will make all of the difference."

### AI summary (High error rate! Edit errors on video page)

Introducing another episode of "The Road's Not Taken" focused on Trump's legal entanglements.
Explaining developments in Trump's legal cases that may be overlooked.
Analyzing the classified documents case and the implications of the judge's order.
Detailing the definitions of top secret and secret classifications in relation to the case.
Clarifying that despite Trump's claims, the documents are still classified.
Summarizing Trump's legal challenges in various cases, including Georgia and E. Jean Carroll.
Mentioning Trump's attempt to invoke an immunity defense in the defamation suit.
Touching on Colorado and New Hampshire's positions on Trump's 14th Amendment challenges.
Reporting on Jenna Ellis being censured by a Colorado judge for violating rules on attorney conduct.
Updating on Navarro's trial and the potential mistrial claim related to protesters.
Concluding with a rare win for Trump regarding access to Scott Perry's calls with other Republicans.

Actions:

for legal observers, political analysts,
Stay informed on updates regarding Trump's legal cases (implied).
Monitor the developments in the classified documents case and implications for national security (implied).
Follow the legal proceedings in different states concerning Trump's challenges (implied).
</details>
<details>
<summary>
2023-09-10: The Roads Not Taken EP4 (<a href="https://youtube.com/watch?v=3VHD-if23Ds">watch</a> || <a href="/videos/2023/09/10/The_Roads_Not_Taken_EP4">transcript &amp; editable summary</a>)

Beau sheds light on under-reported events, from G20's significant move welcoming the African Union to ongoing issues like cyber attacks and international tensions.

</summary>

"G20 welcomed the African Union at their summit. This is gonna be really big news."
"Elon Musk is taking incredibly heavy criticism for allegations that his company disrupted."
"A Ukrainian attack on Russian ships. Those ships later went on to launch attacks against civilians."
"The NYPD agreement requires new policies and bans kettling, low-flying helos."
"Australia and China have resumed high-level talks after a three-year break."

### AI summary (High error rate! Edit errors on video page)

The episode focuses on under-reported events from the previous week, providing context that is often overlooked.
G20 welcoming the African Union at their summit is significant news that may go under-reported.
Putin's promotion of a general who advocates invading Eastern Europe and sees Ukraine as a stepping stone is concerning.
Elon Musk faces criticism over disruptions caused by his company.
A Ukrainian attack on Russian ships that later launched attacks against civilians, including children, is a continuing story.
Issues with Starlink deployment and its relationship with the Department of Defense will persist until resolved.
The UK-France route closure due to a security incident may have future implications.
Australia and China resuming high-level talks after a three-year break signals positive news.
Russian nationals charged in cyber attacks in the US are currently in Russia, not in custody.
The NYPD reaching an agreement on handling demonstrations, imposing new policies to ensure de-escalation, is noteworthy.

Actions:

for news enthusiasts,
Contact local media outlets to urge better coverage of significant events (suggested).
Join organizations advocating for peaceful conflict resolution (implied).
Monitor developments in international relations and share information with your community (exemplified).
</details>
<details>
<summary>
2023-09-07: The Roads Not Taken Special Trump Edition A (<a href="https://youtube.com/watch?v=PPcF4zssqPM">watch</a> || <a href="/videos/2023/09/07/The_Roads_Not_Taken_Special_Trump_Edition_A">transcript &amp; editable summary</a>)

Beau covers updates on legal entanglements involving the former president, from trial proceedings to defamation cases, hinting at ongoing challenges ahead.

</summary>

"The state's position is that whether we have one trial or 19 trials, the evidence is exactly the same."
"You're going to see that material again."
"Decline to sign, defendants' arguments are completely without merit."

### AI summary (High error rate! Edit errors on video page)

Introducing a special Trump edition of The Road's Not Taken, focusing on under-reported news.
The former president expresses an interest in testifying, although it seems unlikely.
Updates on the Georgia State case, where Chiefs Breaux and Powell may be tried together or separately.
The trial for lack of a better term is set to start on October 23rd.
The DA's office aims to try all defendants on October 23rd, with a trial possibly lasting four months.
Expectations of lots of finger-pointing during trials regarding blame and responsibility.
Even if trials are severed, juries will hear the entirety of the alleged conspiracy.
Employee number four in the documents case has struck a cooperation agreement with the feds.
In the federal DC case, DOJ files a complaint about Trump's social media posts prejudicing the jury pool.
Trump's request to delay the trial in the civil fraud case in New York was denied.

Actions:

for legal observers,
Follow legal updates and analyses to stay informed about ongoing proceedings (implied).
</details>
<details>
<summary>
2023-09-03: The Roads Not Taken EP3 (<a href="https://youtube.com/watch?v=ogqBy6_uiRA">watch</a> || <a href="/videos/2023/09/03/The_Roads_Not_Taken_EP3">transcript &amp; editable summary</a>)

Beau gives an overview of under-reported foreign policy, environmental, and cultural news events from the past week, shedding light on significant developments and potential implications.

</summary>

"AI-generated books about foraging: accuracy is life and death."
"Exxon report: energy transition not happening at the needed scale."

### AI summary (High error rate! Edit errors on video page)

Overview of under-reported news events from the past week.
Icebreaker ship heading to Antarctica to rescue someone at KC Research Station.
Nobel Foundation reverses invitations to Russia, Belarus, and Iran.
Putin arranging high-level meetings with various countries to rehab Russia's image.
Ukrainian drone strikes into Russian territory are becoming more effective and embarrassing for Russia.
Speculation about China's stumbling economy and G7 closely monitoring.
Ukraine's counteroffensive advancing slowly with Western critics urging realistic expectations.
Kenneth Cheesebrough severs case from Sidney Powell.
Lawmakers in Texas call for investigation into Texas Guard's intelligence gathering.
McCarthy may order a full House vote on impeachment inquiry into Biden.
Panel in Georgia may convene to suspend Georgia State Senator Steele.
Pope criticizes US Conservatives for prioritizing ideology over faith.
Billionaires planning an environmentally friendly community in California face resistance.
Truth Social faces shareholder approval test by September 8th.
Burning Man attendees stranded after rapid rains in the desert.
Assistance offers for those impacted by Adelia in Florida.
Concerns about AI-generated books on foraging lacking accuracy.
AI experiment covering high school sports suspended for inaccuracy.
Russian film "The Witness" flops due to propaganda content.
New term "thousand ton rule" estimates premature deaths due to emissions and climate issues.
Exxon report states slow progress towards achieving net zero goals.
Radioactive boars in Bavaria linked to contaminated truffles from atmospheric testing.

Actions:

for media consumers,
Contact individuals impacted by Adelia in Florida and offer assistance (implied).
</details>
