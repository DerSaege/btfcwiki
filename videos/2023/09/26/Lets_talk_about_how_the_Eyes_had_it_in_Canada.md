---
title: Let's talk about how the Eyes had it in Canada....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ixAHoywZFZI) |
| Published | 2023/09/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the Five Eyes intelligence alliance, consisting of the United States, the United Kingdom, Australia, New Zealand, and Canada.
- Clarifies that the Five Eyes is not an intelligence agency but rather a collaborative effort among these countries to share information.
- Traces the origins of the Five Eyes back to 1941 when US and UK code breakers began sharing information during WWII.
- Compares the Five Eyes to a library where each "book" (piece of intelligence) was donated by a spy from one of the member countries.
- Mentions the existence of other intelligence-sharing agreements like the Nine Eyes and the 14 Eyes, involving different countries.
- Notes that privacy concerns arise due to the nature of intelligence sharing among these allied nations.
- Addresses conspiracy theories that have emerged around the Five Eyes concept despite it being a longstanding arrangement.
- Emphasizes that while the Five Eyes alliance was initially focused on signals intelligence, it has expanded to include various types of intelligence over the years.
- Points out controversies like Echelon that have arisen regarding international intelligence sharing.
- Concludes by reiterating that the Five Eyes is more akin to a collaborative structure than a centralized spy agency, with agencies from member countries sharing information.

### Quotes

- "Five Eyes is not an intelligence agency. It's more like a library."
- "It's not an international spy agency. It's more like a library."
- "There's way more than the Five Eyes."
- "Privacy concerns exist and they're real."
- "It's a library."

### Oneliner

Beau explains the nature of the Five Eyes intelligence alliance, likening it to a collaborative library, and addresses privacy concerns and conspiracy theories surrounding international intelligence sharing.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Contact local representatives to advocate for transparency and accountability in intelligence-sharing agreements (suggested)
- Join advocacy groups promoting privacy rights and oversight of government surveillance activities (suggested)

### Whats missing in summary

The full transcript provides a comprehensive explanation of the Five Eyes intelligence alliance, its history, functions, and implications for privacy and international relations.

### Tags

#FiveEyes #IntelligenceSharing #PrivacyConcerns #InternationalRelations #Collaboration


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are gonna talk about how the eyes have it.
And Canada, that thing that happened in Canada,
and a term that apparently a whole lot of people
just got exposed to for the first time.
And we're gonna go through,
a whole bunch of questions came in,
we're not gonna read all the questions,
but we're gonna answer them all in this video.
We're going to lead off with one question
may actually be the most important piece of information to really understand if
you want to know what the Five Eyes really is. Okay, so the question is how
do we know that it wasn't a Five Eyes agent that did that thing in Canada and
they're just trying to blame India? Well the easiest way to know that is that the
Five Eyes don't have agents.
Five Eyes is not an intelligence agency, despite
how it is apparently coming across.
That's not what it is.
It's also not new, and it's also not incredibly secret.
It has been around since 1941.
It can trace its origins back to 1941, when US and UK
Code breakers were sharing information.
Yes, 1941, before the United States officially joined the war,
because again, giant poker game, everybody's cheating.
This is a massive assistance in cheating.
Don't view Five Eyes as an intelligence agency.
It's not what it is.
It's more like a library.
But every book in the library was donated by a spy.
spies that work for the Five Eyes.
The Five Eyes being the United States,
the United Kingdom, Australia, New Zealand, and Canada.
Those are the Five Eyes.
This also answers another question.
Why didn't Canada have to make a public declaration
and show the intelligence to get some nations
to know that it was real?
Because they already had it.
It was in the library.
they read it. That's what it is. Now, there are some privacy concerns that go
along with this, obviously, because a lot of countries have laws about intelligence
agencies spying on their own people. They generally don't have laws about another
country spying on their people and then sharing the information with them. So
there's kind of a, there's a workaround there. Now, this term has come out into
the public because it has been revealed, I guess by a US diplomat, that Canada
didn't have to disclose where they got the information because apparently they
got it from a different Five Eyes member. They got it through the library.
Now, all of this is fuel for conspiracy theories, which is weird because the truth is, I mean,
that seems like enough, like you wouldn't need to add anything more to that.
But it has become, and this term has become linked to a whole lot of theories.
The other thing to know is that there's way more than the Five Eyes.
The Five Eyes is the best known agreement like this, but there is also something called
the Nine Eyes, which includes Denmark, France, the Netherlands, and either Norway or Sweden.
I can't remember which, but it doesn't matter because whichever one is not a member of Nine
Eyes is a member of 14 Eyes, which includes Belgium, Germany, Italy, Spain, and whichever
one I messed up.
These agreements, there's a whole lot of them.
It's worth noting that 14 Eyes, that's not actually called 14 Eyes on any documentation.
That's just kind of the nickname for it.
There's also Five Eyes Plus, which I want to say that is more focused on the Pacific.
There are a number of these agreements around, and it is countries that have been long-time
allies, countries that have shared national interests for a very long time, also sharing
information.
That's what it is.
a library. That's your best analogy that you're going to find. Now initially it was all about
signals intelligence, but as you know it progressed over the last 80 years, there's also geospatial,
basically every kind of intelligence can be shared in this library now.
but it is a structure that exists and I think most people understood that this
occurred particularly with the Five Eyes because of how close those countries
coordinate when it comes to the military and intelligence but I guess it's
surprising that there's a formal agreement but I mean it's it's been
around a really really long time. There's been a number of controversies that have
arisen that are kind of worth looking into, one being Echelon. That was at the
time when news broke, you know the US people in the US were kind of like,
nah, okay. Europe on the other hand, they like lost it. They they were really
upset. But the crux of this is that it's not an international spy agency. It's
more like a library. All of the intelligence agencies from, when you're
talking about the Five Eyes, the US, UK, Canada, Australia, and New Zealand, the spy
agencies from those countries share information with other agencies from
Five Eyes Nations. That's what it is. Learning about some international spy
library for lack of a better term, I guess it's one of those things that could
be unnerving especially if you were a you know like ardent nationalist and
very, very of the opinion that your spy agency only works for your country or something like
that. But, I mean, I think deep down everybody knew this was happening. And, again, it's not
like this news just broke. This has been public for a really long time. It's just that a new group
of people is being exposed to this for the first time. Again, privacy concerns, they
exist and they're real. And other than that, I mean, it's a library. Anyway, it's just
a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}