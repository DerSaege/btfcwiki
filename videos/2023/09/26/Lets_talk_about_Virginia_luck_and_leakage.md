---
title: Let's talk about Virginia, luck, and leakage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1wkU_qPUVo0) |
| Published | 2023/09/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A person identified concerning content on Instagram related to a church in Virginia and alerted the authorities.
- The person's tip led to the prevention of a potential mass shooting at the church.
- The concept of "leakage" involves visible signs or markers that individuals planning harmful acts often display before carrying them out.
- Many people tend to overlook these signs as jokes or harmless, leading to missed prevention opportunities.
- Recognizing leakage markers and taking them seriously can save lives by preventing tragedies from occurring.
- It is debated whether leakage is subconscious or a cry for help, but its presence can help stop dangerous situations.
- Understanding leakage markers and being vigilant can be vital in preventing harmful incidents.
- The person's awareness of leakage and quick action on Instagram potentially saved many lives.
- While the cause behind leakage behavior remains uncertain, its patterns have been observed and can be acted upon to avert disasters.
- Beau urges viewers to familiarize themselves with the concept of leakage and its markers for early intervention in potentially harmful situations.

### Quotes
- "Believe them. If somebody says or leaves something around indicating they're gonna do something like this, believe them."
- "Understanding leakage markers and being vigilant can be vital in preventing harmful incidents."
- "A person identified concerning content on Instagram related to a church in Virginia and alerted the authorities."
- "The concept of 'leakage' involves visible signs or markers that individuals planning harmful acts often display before carrying them out."
- "Recognizing leakage markers and taking them seriously can save lives by preventing tragedies from occurring."

### Oneliner
A person's keen observation on Instagram led to the prevention of a potential mass shooting by recognizing and acting on leakage markers.

### Audience
Community members

### On-the-ground actions from transcript
- Familiarize yourself with the concept of leakage and its markers (suggested)
- Stay vigilant and observant for potential leakage markers in your surroundings (implied)
- Take prompt action if you notice concerning behavior or signs of leakage (implied)

### Whats missing in summary
The emotional impact and urgency conveyed by Beau's message in recognizing and acting upon leakage markers promptly to prevent tragedies.

### Tags
#Prevention #LeakageMarkers #Vigilance #CommunitySafety #Awareness


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about something
that happened in Virginia,
and how hopefully it can happen
in a whole bunch of other places as well.
Somebody recognize something and they have a pretty big issue.
Okay, so if you haven't heard the news,
is what went down. A person was on Instagram. Somebody they knew had posted
some images, some stuff related to a church and the person who's viewing it
knew something was wrong. They identified the leakage, a concept we have talked
about on the channel before. And they called the cops. They called the cops and said,
hey I think this guy's about to do something bad at this church. Cops show
up. The guy's at the church. He is armed, he's got a bunch of ammo, and a
manifesto about all the people that he is about to end. Prevented. Prevented it
happening. Okay, so we're going to go over leakage again. There are a number of
things and I will have another video down below that goes over the markers but
leakage is something that is coming to the forefront. Basically what they have
found out is that a whole lot of people who engage in this type of behavior, who
engage in mass incidents that prior to doing so there was super-evident stuff
that they said or did or left laying around that if anybody had actually seen
it and been paying attention they should have been able to identify it for
what it was. But most times people blew it off. Oh, he's not really gonna do that.
doesn't really mean it, or thought it was a joke. If somebody says or leaves
something around indicating they're gonna do something like this, believe
them. Believe them. Sometimes it's a list of names. Sometimes it's a map. Sometimes
it's really weird photos. If you observe something like that, just remember what
it is. Somebody who was aware of this concept, at least in some way, and knew
enough to be unnerved by what was posted, saved lives. Absolutely saved lives. Was
there armed ammo manifesto? Right about to go in. Now there is debate over whether
or not leakage is subconscious or it is quite literally a cry for help. Please
stop me from doing this type of thing. There's debate, nobody really knows. Not
yet. I haven't seen anything settle that debate. But while the cause of the
behavior is not known, the behavior is. It's been observed a lot and if it's
acted upon, it can stop really bad things before they start. Please get
familiar with the concept of leakage and the markers. I will have a video down
below about leaky markers. So, they got lucky. A whole bunch of people got lucky
because somebody was paying attention on Instagram. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}