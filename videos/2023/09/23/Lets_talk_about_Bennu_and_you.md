---
title: Let's talk about Bennu and you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AWaZqonlS8o) |
| Published | 2023/09/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the recent headlines about the asteroid Bennu potentially hitting Earth in the future.
- Providing context about the odds of impact and the most likely date.
- Emphasizing that the chances of Bennu hitting Earth are extremely low, at 0.037%.
- Pointing out that the asteroid generating headlines is due to the fear factor and sensationalism.
- Describing the estimated force of impact if Bennu were to hit, equating it to 1200 megatons, far exceeding the power of current nuclear weapons.
- Comparing the potential impact of Bennu to historical events like World War II.

### Quotes

- "This should not be high on your anxiety list."
- "The odds are 0.037%, so I mean, I don't think any of us have to worry about that."
- "Fear is good for clicks."
- "It's going to be a problem."
- "This is a big deal."

### Oneliner

Beau explains the low chances of the asteroid Bennu hitting Earth, despite sensational headlines, due to its scare factor and potential catastrophic impact.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Monitor updates on Bennu's trajectory and potential impact (implied)
- Support scientific research and funding for asteroid detection and deflection (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the asteroid Bennu's potential impact on Earth, offering context on the odds and implications, which may interest those curious about celestial events.

### Tags

#Asteroid #Bennu #Impact #Space #Science #Context #LowProbability #FearMongering


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Bennu, it's an asteroid.
There are a lot of headlines coming out right now and we're
going to run through them because the headlines are, I
mean, they are what they are.
But we're going to add a little bit of context to it.
We're going to talk about the odds of that occurring, when,
all of that stuff.
If you don't want to watch the whole video, short version is
This should not be high on your anxiety list.
So Bennu is an asteroid, and some new information
came out saying that it might hit Earth in the future.
And those are the headlines.
It's going to hit Earth, might hit Earth in the future.
The most likely date of impact would be September 24, 2182.
I mean, I don't think any of us have to worry about that.
The odds are 1 in 2,700, so 0.037%.
OK, so with that in mind, why is it generating headlines?
One, because it's scary and fear is good for clicks.
And two, because if it does hit, well,
it's going to be a problem.
The estimate, which I would like to stress,
is an estimate because I don't think anybody can really gauge this very well, but they're
saying it would hit with roughly the force equivalent to 1,200 megatons.
For context, the Tsar, which is kind of the biggest nuke we have right now, humanity has
right now, it's 57 megatons.
So I mean, this is a big deal.
more context the weapons used during World War II.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}