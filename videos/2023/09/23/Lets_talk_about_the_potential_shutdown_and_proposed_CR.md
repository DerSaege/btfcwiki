---
title: Let's talk about the potential shutdown and proposed CR....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=djUSHghItYg) |
| Published | 2023/09/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the potential impact of a government shutdown on federal workers and services.
- Points out the ripple effects of federal workers not getting paid, affecting the local economy.
- Breaks down the proposed Republican continuing resolution and its potential consequences, like an 8% cut leading to severe impacts on various services and programs.
- Raises concerns about vulnerable populations bearing the brunt of budget cuts.
- Suggests prioritizing cuts from sources like companies not paying taxes rather than affecting the most vulnerable.
- Encourages seeking detailed information and context rather than relying solely on labels like "extreme budget."

### Quotes

- "each thing impacts the next."
- "you're going to lose roughly 800 Border Patrol people."
- "there's something on that list they don't want to give up."
- "maybe there's some money there that could be used just saying."
- "Just because somebody says, well, it's an extreme budget, yeah, good call. Don't take their word for it."

### Oneliner

Beau explains the potential impacts of a government shutdown and the proposed Republican budget cuts, urging people to seek detailed information rather than relying on labels like "extreme budget."

### Audience

Budget-conscious citizens

### On-the-ground actions from transcript

- Contact local representatives to express concerns about the potential impact of budget cuts on vulnerable populations (implied).
- Join advocacy groups that provide detailed information on proposed budget cuts and their consequences (implied).

### Whats missing in summary

The full transcript provides a comprehensive breakdown of the potential consequences of a government shutdown and proposed budget cuts, urging individuals to seek detailed information to understand the real impact.

### Tags

#GovernmentShutdown #RepublicanBudgetCuts #BudgetPrioritization #CommunityAction


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about two questions that have come in about the potential for
a government shutdown, what it actually means, and the proposed Republican continuing resolution,
which really doesn't seem like it matters at this point because it doesn't look like
it's going through.
But the question, it came in, and it's one of those that I think it's important because
is it's the kind of questions people should be asking
when they hear certain kind of rhetoric.
And I went back and looked, I never clarified it,
so I'm assuming other people didn't either.
Okay, so the first one is what's really gonna happen?
You said that the VA, Social Security, stuff like that,
it was gonna go ahead, so who's not getting their check?
The people currently working for the federal government,
a lot of them are gonna have issues,
particularly military service members.
You'll have food safety inspection issues,
probably have travel delays
because of stuff related to that.
There's a lot of federal jobs
that will have people furloughed, which will cause issues.
The lack of a check will cause wider issues
as things move along.
If you, let's look at it this way.
Let's say you live in an area near any federal installation, any military base.
Just as an example, those service people not getting paid means they're not spending money
in the local economy, which means less hours, which means more economic impact further down
the line with people that don't have anything to do with the federal government.
economy, sections of the economy, it doesn't exist in a vacuum like that. Each
thing impacts the next. Moving into the holiday season, that's gonna be bad. It's
gonna have a lot of a lot of downstream effects. Now the other
question is basically I've heard commentators say that you know the
proposed Republican continuing resolution, their short-term budget, that
it's just extreme but nobody says what that means. Okay, short version, I want to
say it's 8%. I think it's 8% cut like top line. So here are some numbers that
reflect that 8%. 60,000 seniors, no mills on wheels. 2.1 million women, infants, and
and kids would be waitlisted for WIC.
I think it goes without saying that when it comes to the case of infants, I mean, they
don't have a lot of time to sit on a wait list.
Forty thousand fewer educators or people in school staffs that would be serving tens of
millions of students, and I want to say the number was 7.5 million, I think,
disabled students. The feds pay for a lot of that. If they're not there, it strains
other resources as well. Looks like 300,000 people would lose housing
vouchers, okay, and the numbers were almost a hundred thousand of those are
seniors, twenty thousand are vets. This puts them closer to homelessness. More
than six million people would be impacted by lower maximum payouts for
Pell Grants. While there is one party that's trying to make higher education
more affordable, the other party is trying to lower the amount of money that
people who can qualify for Pell Grants can get. That's a pretty stark
difference between the two parties right there. You're looking at, this is one that
And I would mention, if somebody asks, you're going to lose roughly 800 Border Patrol people.
That might be something worth bringing up.
Few hundred rail inspectors, looks like 35,000 slots for registered apprentices go away.
And more than 100,000 kids would lose access to the Head Start program.
That's pretty significant, I would suggest.
I would suggest that most of the United States, when it comes to this list, there's something
on that list they don't want to give up.
This is one of the issues.
I actually agree on some level.
The federal government spends too much money.
through and doing a top-line approach and just saying we're going to cut this
percentage, that's not really how to go about this. There probably needs to
be some kind of prioritization as to where that money is coming from. I don't
think it should come from the most vulnerable among us. I think it might be
a better idea to go the other route with it and maybe you know those people who
have companies that make hundred million billion dollars a year that
don't pay any taxes maybe maybe there's some money there that could be that could
be used just saying rather than you know stuff leading to seniors not eating and
people being thrown out in the street just seems like that might be a better
idea. But the question itself, and there were a couple of them, apparently there
were a whole lot of people talking about how how extreme the cuts were, but nobody
did any detail as far as what it did. And the weird thing is there's actually a
bunch of nonprofits, there's a bunch of different groups that actually went
through and did a real quick this is what it would do, and the information is
out there, but you have to look for it. And that's the kind of context that, I
mean, you need. Just because somebody says, well, it's an extreme budget, yeah,
good call. Don't take their word for it. Get the actual information. So that's an
overview of what they were looking for. Now, as it stands at time of filming, this
doesn't actually look like it's going anywhere, but that's what they were
looking at. That's what they were hoping to get. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}