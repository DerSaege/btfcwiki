---
title: Let's talk about Trump wanting a shutdown....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Bcf4xCBRmXY) |
| Published | 2023/09/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump is willing to shut down the government to stop prosecutions against him.
- Republicans control the House, but defunding prosecutions won't go anywhere without Senate approval.
- Trump's plan to defund prosecutions won't work and lacks majority support in the House.
- Even if a government shutdown occurs, it won't stop the prosecutions due to permanent appropriations.
- Trump's actions show a lack of understanding of government processes and potential economic damage.

### Quotes

- "Your paycheck, you can do without that."
- "He's telling you right now, your paycheck, you can do without that."
- "He doesn't know how the government works."
- "That's what he wants. It's what it's always about for him."
- "Y'all have a good day."

### Oneliner

Former President Trump is willing to cause economic disturbance by shutting down the government to stop prosecutions against him, showing a lack of understanding of government processes and potential economic damage.

### Audience

Politically aware citizens

### On-the-ground actions from transcript

- Share information on the implications of a government shutdown and the reasons behind it (suggested)
- Educate others on the concept of permanent indefinite appropriations (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's willingness to cause economic disruption by shutting down the government to stop prosecutions against him, despite lacking support and understanding of government processes.

### Tags

#Trump #GovernmentShutdown #Prosecutions #EconomicDamage #RepublicanControl


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump and what Trump wants, what he considers a fair deal,
and what all of this has to do with a potential government shutdown and you.
Because there's definitely something that you should note in this.
Okay.
So, recently, during an interview, the former president was asked if he was in support of
shutting down the government to get their way.
He said, I'd shut down the government if they can't make an appropriate deal.
Absolutely.
Okay, but what does he think an appropriate deal is?
Has he called on members of the House to do anything in particular?
What's fair to him? It'd be important to know that. Luckily, there's a post.
Republicans in Congress can and must defund all aspects of crooked Joe Biden's weaponized
government that refuses to close the border and treats half the country as enemies of
the state.
This is also the last chance to defund these political prosecutions against me.
Wow.
The former president, the man who said that he would put America first and all of that
stuff, certainly appears willing to shut the government down, causing a massive amount
of economic disturbance and damage.
if they don't stop the prosecutions against him.
If you are an American citizen, you should pay note.
Make no mistake about it, that is him putting himself above your economic livelihood.
That's what's happening right there.
Now all of that's fine and good.
It's a good look at how he views things.
But I have a couple of basic civics lessons that I would like the former president to
understand.
First and foremost, the Republicans, those people who are part of his party, they control
the House.
The budget also has to go through the Senate.
They don't control that.
Then it has to be signed.
There is no way that defunding prosecutions against him goes anywhere.
That's not happening.
It will not occur.
More importantly, the Republicans who would be willing to do that, even in the House,
that's not a majority.
That's not enough to make that happen.
He doesn't have the support that he thinks he does.
It's just not real.
It is very out of touch with reality.
That will not occur, but there's something else.
It certainly appears that he and some other Republicans, they seem to think that if there's
a government shutdown, well, that would also stop the prosecutions against him.
No, no, it wouldn't.
You would think that the former president of the United States would be familiar with
the concept of permanent indefinite appropriations.
That's what funds those prosecutions.
That's not even being discussed right now.
That's not part of this.
That will continue even through a government shutdown.
So not just is the former president apparently willing to call on Republicans to shut down
the government, cause a bunch of economic disturbance and damage for his own gain, he
would do it without even researching whether or not it would work.
This is the man that had such a hold on this country.
His actions are, they're self-evident.
He's telling you right now, your paycheck, you can do without that.
economic damage that's going to be caused by the uncertainty, you already, it
hasn't even started yet and you already have business leaders really concerned
so much that they're voicing their concerns because during the government
shutdown there's a whole lot of things that disrupt economic activity and he's
willing to put the entire country through that because he he doesn't know
how the government works.
He doesn't know what indefinite appropriations are.
There really isn't anything that Republicans in the House can do to stop the prosecutions
that he is facing.
And just in case he doesn't know this, the Georgia one and the New York one, those aren't
even federal.
Those have nothing to do with that either.
It's just not real, but that's what he wants.
That's what it's about for him.
It's what it's always about for him.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}