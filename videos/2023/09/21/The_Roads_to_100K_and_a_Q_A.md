---
title: The Roads to 100K and a Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LJ8fmVCDOkQ) |
| Published | 2023/09/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Celebrating reaching 100,000 subscribers with a lighthearted Q&A session.
- Reveals a thumb injury from a werewolf Halloween decoration mishap.
- Answers questions about marriage advice, brand deals, and personal life.
- Shares insights on the left's fascination with certain regimes and conspiracy theories.
- Talks about family dynamics, future plans, and community building.
- Explains the importance of education in understanding geopolitics.
- Touches on personal interests, hobbies, and favorite TV shows.
- Addresses audience questions about various topics, including historical books and US alliances.
- Gives advice on focusing efforts to make a meaningful impact in the world.
- Mentions future plans for content creation and engagement with the audience.

### Quotes

- "Do what you're best at. Focus on where you can be most effective."
- "The only thing that could convince independents to vote for Trump in 2024 is the economy."
- "The answer is it's in the protracted stage. It's going to be a while."
- "Celebrate reaching 100,000 subscribers with a lighthearted Q&A session."
- "If everybody got where they were most effective, it would be incredibly helpful."

### Oneliner

Beau shares insights, answers questions, and offers advice on various topics in a celebratory Q&A session for reaching 100,000 subscribers.

### Audience

Viewers seeking insights, advice, and a lighthearted Q&A session celebrating 100,000 subscribers.

### On-the-ground actions from transcript

- Reach out to organizations or groups that focus on community networking and support their initiatives (implied).
- Focus on what you are best at to make a meaningful impact in the world (implied).
- Stay informed about current events and geopolitical issues to understand the world better (implied).

### Whats missing in summary

The full transcript provides a deep dive into Beau's personal life, thoughts on various topics, and engagement with the audience during a celebratory Q&A session. Viewing the full transcript can provide more detailed insights and advice on different subjects. 

### Tags

#Q&A #CommunityBuilding #Celebration #Insights #Advice #Engagement #Education #Geopolitics #Impact #Support


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to do the roads
to 100,000 subscribers, because that happened,
and we got this thing.
So we're going to do a lighthearted Q&A,
is what it's supposed to be.
They told me that the questions were mostly lighthearted,
but there were some other ones in there.
It looks like there's 20 pages of questions,
which seems a bit excessive, if that really is what this is,
we will definitely be doing parts to this.
And speaking of parts, y'all also
get to know something else.
Other than the Play button, I had a boo-boo.
So next week, when I am having a really hard time reading quotes
off my phone and scrolling, it's because I can't use my phone.
And people will ask, it was a werewolf.
A werewolf hurt my thumb, ripped it open.
And by that, it's not a joke, it's actually a werewolf.
It was a Halloween decoration I was getting out.
Yeah, so there's that.
Okay, so jumping right into the questions.
Starting off with the very important ones,
who would win a fight between Darth Helmet
Zap Brannigan? Well, I mean, I think the obvious answer is Zap because he has
wave after wave of his own men at his disposal. Okay, you've done dating advice.
What are the top three things you'd say are key to a happy, long-lasting marriage?
What things are absolutely detrimental? And what would you say you need to work
more on yourself when it comes to marriage. Wow, okay. Top three things. Well,
I mean first would obviously be the having the core values, sharing those
same core values. If you don't have those it's gonna be hard and there will be
more less than happy times. Another one I think would be not not setting
unrealistic expectations about what you're going to be able to change about
your spouse. You know there's a whole bunch of little things that people do
and I have seen people fight extensively over little things and things that I
have known that that person has done for 25 years and we're not talking about
like anything horrible here we're just talking about little behaviors that are
just off and a lot of times people think blah you know that'll change it probably
won't. So there's that. I would say sharing the same long-term goals so
you're working towards the same thing, understanding that it's not always happy.
I think that's a big part of it because there are people today who think that it
should always be roses and it's not. And trust. I mean it's way more than three
things, what things are absolutely detrimental, not doing the other stuff.
Probably the most detrimental would be a loss of trust, because at that point it's just
you become like roommates more than like a couple.
And what would you say you need to work more on yourself when it comes to marriage?
I'm perfect, obviously.
Realistically, I don't know that we have that much time.
I mean, this is already going to be a long episode.
Well, I don't think that it's anything in particular.
I don't think it's like stopping a behavior.
I think it's more of good behaviors.
Generally speaking, I think that's how people should
probably look at it.
I don't know what would be the most important.
You would have to ask my wife that.
I have so many questions.
I will limit them.
Does your family ever get frustrated with the time
you devote to your channel?
Shared goals, shared core values.
I'm sure the answer to this is yes.
But it's also, you know, that story with the frog and the scorpion, she knew I was
a workaholic when she picked me up.
Are there still plans to take your show on the road?
Yes, yeah, I think that's actually the whole purpose of this channel.
And we are slowly working in that direction.
We're getting there.
Why don't you do brand deals?
We won't get mad.
Doesn't fit with the channel.
So the main channel, it just doesn't fit.
The idea of putting in 30 to 90 seconds into a three to eight minute video, it doesn't
sit right with me.
On this channel, I mean, I guess it would be feasible, but also I just don't want to
do it. I like the little feature that YouTube now has set up, the YouTube
shopping feature where when I'm using something or I'm putting something in
the background I can tag it. That's probably as close as I would ever get to
doing something like that. It would have to be something that fits seamlessly
with the channel that I actually liked and used and that I thought y'all would
to get some value out of, with the exception of Jeep.
Jeep can still buy me for one of those new hybrids.
Let's see.
Does it get confusing?
Disorienting with a set that duplicates another place.
Like, I swore I put my coffee.
OK, so let me finish reading that.
I swore I put the coffee cup here.
way it's back at the shop. That used to be a problem, yes. That actually did become an
issue initially, but now it's so different that it is easier to keep that straight. Because
while the back looks the same, this way definitely it does. Like I would not have a place to
sit in my actual shop.
Not comfortably, anyway.
Okay.
That's a big one.
Okay.
I recently got into a massive blowout argument with a bunch of people in a leftist space
I frequent about Ukraine, asking allies on the side of Russia, being an expansionist,
kleptocratic dictatorship that has no legitimacy to its claims of Ukrainian land this time
or in 2012 with Crimea.
They fired back with some of the Russian talking points about it belonging to Russia, Ukraine
being an illegitimate state invested, and arguments that basically boiled down to, I'm
a Leninist, so it belonged to the USSR, it belongs to Russia now, never mind what Putin
and the Kremlin are actively doing is standard far-right dictatorship stuff.
They called me an Enrico Bidenist.
Nice.
My question boils down to this, here we go.
Why do you think our comrades on the left can be as drawn into root for these regimes
as some of the very people we are supposed to be fighting on the right?
And how do we de-conspiracy our spaces?
So that we have honest discussions about what is going on politically without being accused
of being the CIA Western propaganda line.
Okay why do I think that happens?
So for some, nostalgia.
It's just nostalgia.
It's associating Russia with the Soviet Union and more importantly it's associating Russia
with like the ideal of the Soviet Union, not necessarily what it was in practice, and then
there is a huge lack of understanding when it comes to foreign policy in general with
most people, and it goes back to, you know, looking for the good guys, looking for good
guys and bad guys in foreign policy, and that doesn't exist.
There are no good guys.
There's just less bad guys.
A lot of them are willing to overlook what Russia does because they see the US as, quote,
the bad guy because of US history when it comes to interventions.
But acknowledging the US checkered past when it comes to that, they take it the next step
and they turn it from the US did these bad things to those opposing the US must automatically
be the good guys because they view it in a binary and that's just not how it works.
The answer to this is, like many things, when it comes to how do you achieve utopia, education.
That's where it has to go.
How does one have a t-shirt for everything?
There must be hundreds, if not thousands.
Building them up slowly over the years, there's hundreds, there's not thousands.
I don't think I've hit 1,000 yet.
That's definitely something to look at when you
think about it in those terms.
What is the question that you wished people would ask just
so you could talk about the answer?
Questions where the answer is community networking.
because I really feel like that is the thing that's
going to pull everything else together.
I do.
I think that's really important.
So any question where the answer results in community networking
relates to community networking, anything like that.
I'm curious about your farm.
What animals do you raise?
we have horses, rabbits, dogs, cats. It seems like this year what we raised
mostly were rabbits that were feeding on my vegetable garden, mostly. How many kids
do you and your wife have on the ranch? What are the youngest and oldest? Do you
take in foster kids as well as horses. There are five currently on the ranch, 17 and 5
as far as oldest and youngest. Foster kids. Depends on how you define that. If you define
that the way my wife defines foster horses in the sense of they come here and they're
not really expected to ever leave. Yeah, we do that. But we don't foster for them to
stay here a couple of months and then go somewhere else. But yeah, we have taken in kids.
What in your opinion is the best sandwich, sausage and egg?
What's one thing you own that you really should throw out?
My old hats.
Like this one.
It's funny because like now that I'm doing this and I see them more often, I realize
when they start to like get well worn.
And I get a new one.
But the old ones are just sitting over there, I don't know why.
What's the scariest animal?
Man.
Man is the scariest animal.
People aren't talking much about the Biden administration's move towards decriminalization.
Even if it's only going Schedule 3, it seems a seismic shift.
am I missing something? No. Yeah, it would be a big deal if it happens. I think in this
case, I think it's one of those things where people have heard that it's going to happen
for so long that now that there are concrete steps that are being taken, I don't think
people are actually acknowledging that it really is moving that way this time. Because
this is something that's been talked about since, I mean, I was a teen. I'm not sure
that people understand that it really does look like that, that might happen.
Not just it being talked about, but it might actually occur.
So I think that's why it's not getting a whole lot of coverage.
What is your favorite smell?
Your least favorite smell.
Fall is favorite, disinfectant is least favorite.
get one song to listen to for the rest of your life, what is it? I do not have an answer
to that. I can't answer that question. What happened with the new technology automatic
weapon that you told us was coming available okay it's just happily I
haven't heard anything about it but I wonder why not right now it's still
super expensive it is out it's not automatic to be clear it's some
automatic but it is available to civilians but it is expensive probably
Maybe seven to eight times the cost of the more popular rifles right now.
What's the dumbest, goofiest, most ridiculous way you got injured?
Or in trouble as a young person?
It was like, I mean, getting attacked by a werewolf Halloween decorations right on up there.
Um, that's a ridiculous way I got injured when I was younger.
Um, I don't know. I mean, I grew up in the 80s when we did a lot of stuff that was just not smart.
smart. I tore my chin open like stitches could could see bone on a skateboard
when I was younger. Where did you go to school? All over the South. All over the
South. Moved around a whole lot as a kid. Favorite episode of Star Trek? Star Trek
any one original, TNG, Voyager, and so on. The one where Garrick did nothing wrong.
That's my favorite one. And for those who don't watch the shows, don't get the
reference. There is an episode, Garak is a character and he's an ex-spy basically
but now he's just a tailor. During an episode he makes some choices that in
the Star Trek world are very bad but at the end of the day for a very small
number of people that were sacrificed, he literally saved billions.
And there's a lot of ethical questions that are posed in that.
I think that makes it a very standout episode.
How many people are on your team to keep the channels running and what are their roles?
Okay, so I guess you could say five, plus me, and to sum it up simply, we have one person
that transfers everything.
Like when I'm done recording this, I will pull the card out of that camera, I will put
it on YouTube.
Anywhere else it shows up, I didn't have anything to do with it.
If you're listening to this on a podcast, don't know how it got there.
And that is one person's gig.
Now they do other stuff as well, but that's their main thing.
Then we have somebody who does set work, which I know that doesn't sound like something
that would happen a lot here, but that's just because we have videos in production
that you all haven't seen yet.
But a good example of their work is this place.
I talked about how we converted a steel building into a workspace.
By we, I mean, you know, really it was kind of like two people.
I might have done like three percent of this.
You know, they did it.
We have a person whose main gig is editing and stuff like that, a person who handles
all the paperwork, which is more than you might imagine, especially given, like when
we do the charity stuff.
You know, somebody has to keep track of all of this and make sure that the stuff that
we order gets to the place it's supposed to go and all of that stuff.
And then we have a person who just kind of makes sure that everybody has what they need
and is doing what they should to keep everything rolling.
Again, if you are, if you're getting on YouTube and you reach that point where you're going
to start bringing more people on.
Remember, if you got to the point where you're bringing people on to help, whatever it is
you're doing is working.
Don't change that.
Don't bring people in to help with content or research or anything like that.
Bring them in to do the other stuff and bring them in to do it and then let them do it.
Don't micromanage.
tell them what you want done, not how to do it.
Let them surprise you. It always works better.
And while we're on that topic though, since I mentioned the paperwork,
there's something that has come up a few times.
People have talked about ads that are showing up on progressive or left-leaning channels
that don't necessarily fit with the theme of the channel. In some cases they
are in direct opposition to it. Just remember that if you were to go to, I
don't know, Trevor Project is a good example. If you were to go to their
annual report for last year and you scroll down to their donors, I think they
They call them beacon donors there.
You will see an entry that says, friends of Bo and the internet people.
If you're getting ad revenue that you don't feel comfortable taking because you don't
have control over the ads to that degree, the reason you may not feel comfortable taking
it is because it's going after people that you support. It's relatively easy to estimate
how much came in from those ads and find a place to put it.
We can't let bigots win because of a lack of creativity. Which is your favorite horse,
Marilyn, she's the one that did the cameo when I was filming in the barn on this channel.
She's the one that came up and said hi.
She is my horse and we have very complimentary personalities.
Have you ever visited the Seattle-Washington metro area?
If so, what did you enjoy about it?
Yes, very shortly.
And I wasn't there long enough to enjoy anything, just saying.
Let's see.
Do you think the current MAGA movement is more of a personality cult around Trump or
serious shift towards authoritarianism in America? Yes. It's definitely about
personality for a lot of them, but it's representative of people embracing
authoritarianism, people wanting to be ruled, people wanting to be told what to
do. The world has become confusing to a lot of people and they need somebody else
to be arbiter of their own thoughts and that leads to authoritarianism. So I
think it does have a lot to do with personality but you can't discount it.
That shift, that drift towards that, it's real. It's not just him. What are your
with thoughts on using anaerobic digesters
to harvest the methane from livestock manure
to produce biogas.
So I'm assuming that this is to actually use it for fuel.
So I think that we need to get away from burning anything.
I think that should be the goal.
That being said, in interest of emergencies,
I wouldn't mind having a reactor here.
A reactor, if you're not familiar with this stuff.
It's a sealed cage, a sealed box, basically.
It sounds way more high tech than it is.
So I mean, it's a unique use for the waste,
but I think the overall goal should be
to get away from burning stuff.
Now, at the same time, I have heard,
have not looked into this,
but have heard people talking about how,
once everything's kind of yanked out of it,
the stuff that is still inside of it,
I guess the digestibles,
the remaining material inside of it
is apparently like super durable
and can be used to make basically like
kind of pressed two-by-fours and stuff like that.
Which, I mean, I think that's cool.
But I think overall, it will probably
come up with a better solution for large-scale use.
OK.
Any idea or best guess how the current political instability
is going to impact the financial sectors if the government fails
to reach a budget agreement, depends on how long,
how long a shutdown occurs.
If it's short, I mean, you're gonna take a hit,
but it probably won't be huge.
If it's prolonged, it could get pretty bad.
I mean, the money the government spends,
The money the government spends, it is the livelihood of a lot of communities.
It's a lot of small towns, interestingly enough it's a lot of small towns in red areas.
It's going to depend on how long.
And then you also have to factor in not just how it hits the financial sectors and how
they feel about things, you know, the economy is based on faith, but the practical implications
when you have, you know, based on the most recent proposal the Republicans put forward,
you know, it includes stuff like getting rid of mills on wheels for seniors and stuff like
that.
to alter the economy more than people think. I want to say it throws millions,
two million, I think, off a wick. These are big deals. So there will be the
faith-based shifts when it comes to the financial sectors, but also like the
real ones. Let's see, what is your religion? Or if you can't answer that for
reasons, what are those reasons? I think spiritual religious beliefs, I think
they're yours. As long as it's not an act of harm, as long as it is something
that isn't being used to try to steer government
or an act of harm to you or yourself or something like that
or you or those around you, it's none of my business.
And I think that there are a lot of topics that we discuss
that are, you know, you shouldn't talk
about those kind of topics.
this is one that I think that we can do without.
If we address the other stuff and we address it
from a philosophical standpoint,
the religious viewpoints on it,
they should fall along.
You know, I also think that it is,
It is wise to not pin yourself down to a specific set of beliefs, especially if you're going
to be providing commentary.
And then the next question is Ms. Beaureligious and on any other social media, what's something
she thinks would be helpful to put on the channel?
In reverse order, a roast of me.
She thinks it would be funny to take the insulting messages and have her read them and me respond
to them.
I guess like mean tweets type of thing.
I think that she just started getting on blue sky as far as another social media.
As far as is Miss Bo religious?
She is very fond of the saying that I like spiritual fruit, not religious nuts.
Are both yours and her parents still alive and kicking?
Does her family give you or her heat for the work you do on the channel?
So yes, our parents are still alive.
Does her family give you or her heat for the work you do?
I mean, yeah.
So in 2016, my dad had a Trump flag.
That led to some interesting conversations.
Her dad did not have a Trump flag, but that's only because his HOA wouldn't allow it, I'm
I'm sure, we definitely have some differing opinions on things.
And most times the conversations are relatively civil.
Sometimes it gets heated.
The interesting part about it is that it's not me.
It's not me that actually gets like really passionate about it.
her. And I find that amusing because she's normally the mellow one. How much
beard is too much beard? I don't know. I mean, I don't think I have an answer to
that. That's that is very much a personal preference.
uh...
How did you team up with Lucifer?
How did you team up with Lucifer
or at least the actor who plays him?
Oh, that was a joke.
That was a joke about the license plate. That was a joke.
The...
Okay, so I have a fallen one license plate.
It's the license plate from the show, Lucifer.
I made a joke about doing a favor for a guy out in LA.
That was literally a joke, I've never met that guy.
That was a joke.
With her oodles of extra time between the six kids,
the one husband, the two dogs, the two kittens,
the question mark horses, right?
Cause that number always changes.
The ranch, the life-saving nursing, the community,
the hurricane seasons and whatever sundries she hath added.
Could you ask her who she looks up to besides herself?
She's gonna say her dad because, you know,
it's the right thing to say, and he got her into medicine and all of that stuff.
There was a colonel when she was in the military that she definitely looked up to.
They were going through a lot of the same things, but they were very different in age
and everything.
this colonel, she really bonded with my wife and helped mold her career in a way.
Do you guys do audiobooks?
I'm not sure if that means are we going to put our book on audiobook or do we listen
to them?
So the answer is kind of, as far as whether or not our stuff is going to go out in an
audiobook.
The answer is yes, but in a unique way.
Do we listen to them?
I'm a huge fan of LibriVox, so if I'm driving on a long trip or something like that, sometimes.
But it's not like a constant thing.
What do you teach your daughters about self-defense?
Same thing I teach my sons.
I don't know how to answer that.
I'm not sure if this is asking about specific techniques or in general, but I teach my kids
to be very security conscious and to be prepared and know how to respond if they have to.
Do we get a title for your book or an ETA?
I think it's going to be just called Community Networking and an ETA is I am hoping that
y'all will be able to buy this for people for Christmas.
So soon, hopefully.
OK.
Let's see.
Since you're asking four questions,
I'll throw my biggest dilemma at you,
something I've been wondering about for years,
at least since Trump shattered any illusion I might have had.
Maybe you'll have some sort of answers.
How do we get to the point where we simultaneously have both freedom of expression, including
the freedom to lie and a well-informed public, education, media literacy, critical thinking.
Most of the people who are falling for the lies, they would also fall for lies that aren't
politically motivated. They are not good consumers of information. Education.
Education. That's when you are talking about utopia, when you're talking about
reaching a progressive, more fair, more equitable society, it has to start
with education. People have to be better informed, otherwise it'll always fall
apart. So stopping the demonization of being educated is critical to that. If it
keeps going the way it has been with a lot of people now embracing, you know, I
love the uneducated. Of course they do. That's how billionaires create a
a permanent underclass.
Any foreign countries you'd like to travel to?
All of them.
I want to go to Egypt at some point.
Although I've come to believe that you would support your wife in any endeavor, I suspect
that you have a very traditional marriage where she is the one who cooks, cleans, raises
the kids and the horses for the most part, and does all the shopping errands, etc.
True or false?
And please elaborate, because I'm nosy.
I mean, so it's weird, because right now the answer to this is kind of yes, but when
we first got married, I was at home with the kids and did this.
It is traditional in, I don't know if that actually fits.
We very much fit neatly into like the standard gender roles of tradition.
But we also will step outside of that.
I read that thing recently about the percentage of men that had never changed a diaper and
burst out laughing.
But currently, she very much runs the ranch.
This is true, but I wouldn't read too much into it.
What really tickles your funny bone?
Dry satire.
Deadpan satire.
Okay.
I can't imagine that you have time for this, but do you enjoy movies?
Do you have a favorite?
I mean, yeah, I like movies, and I like well-done series.
Like binge-watching series that are well-done.
Like right now I am slowly making my way through for all mankind and I find that really interesting.
If you haven't heard of it, it's kind of an alternate history if the Soviets had reached
the moon first.
It's a cool premise.
Do you think Kristen Wilker did a good interview?
I don't think a good interview can be done with that man.
What or one or two things that made you really happy this year?
I mean, I got a new daughter, that was cool.
I'd like to hear about a hobby, if you've got time for hobbies.
I seem to recall you love wild creatures.
Yeah, yeah, I do.
My hobby, I mean this is my hobby, it's just turned into a full-time gig.
Yeah I mean I am very lucky in that regard.
I enjoy my work to the degree that my hobbies at this point are being with kids, playing
with the animals, stuff like that. What do you do or watch to decompress and take a break
from the news, politics, lawsuits, etc.? What makes you think I decompress? Now, I have
a number of things I do. Most of them are the normal things. You know, I enjoy walking
around the ranch. I have an anipi. That's it. What are three adjectives those closest
to you would use to describe your off-air personality? Things that are different than
and the on-air personality I'm assuming, obsessive, sarcastic, and quiet.
That's probably a big surprise.
Yeah, I'm not actually a huge talker.
So I guess that would, I think that's what they'd say.
What's the update with George Santos?
Once I heard, he is not currently trying to work out a plea deal, but that's all I've
got.
When, where do we get the cussing bloopers?
They'll come eventually.
I know that your horses are more of the Mrs. thing, but have you ever ridden horses?
I know that your horses are more of the Mrs. Thing, but have you ever ridden horses?
Oh yeah, no, I ride.
When I say that, I mean that I don't enjoy taking care of the horses, you know, dealing
with their hooves, stuff like that.
That's not fun for me.
My wife actually enjoys that.
That's what I mean by that.
Yes, I ride, not as often as I would like, but yeah, I do.
If you could be what you wanted to when you were a kid, what would it be?
I would have been a medic in fifth group.
That's what I wanted to be.
Was there any video you had trouble making because someone in the news did something
so outrageous or ridiculous that you kept breaking up and couldn't stop laughing multiple
times multiple times.
Have you experienced high rent by rent?
Yes.
Yes, I have seen that.
That's the thing where the guy gets willed in in the wheelchair by the pig, right?
Yeah, okay, let me just finish reading it.
It is a nine minute art music video live performance.
He's an independent UK artist.
He has touched so many lives.
He has a long backstory.
Video is about mental health and doubt.
It's really a roller coaster, but truly watch high rent.
Yeah, I mean, I would agree with this.
It's definitely unique.
Okay.
I saw a news report the other day about all the container ships that are stuck on both
sides of the Panama Canal because dry weather has left too little water for the canal to
operate.
I'm wondering if you have any thoughts on the inverse relationship between economic
efficiency and economic resilience.
I don't think I have heard you mention this before, but it seems like it would be something
in your oil house.
So, almost irrelevant of the topic, those two things don't go together.
Efficiency and resilience, they don't go together.
You know, redundant networks is a term that I used to use because to me redundancy is
good because it is more resilient.
When you try to strip something down to its bare parts, it's never as strong as if it
would be if it was designed with it being shook in mind.
And when it comes to the economy, when it comes to critical infrastructure, all of it,
The idea of resilience needs to be more important than economic efficiency.
What can be done about the left's failure to message?
As much as I hate three-word chants, lock her up is much more effective than an in-depth
discussion about the finer points of constitutional law and how it applies in the context of history
to Trump's alleged guilt.
I get what you're saying, and you're right, from a motivational standpoint, from a sloganeering
standpoint, I get what you're saying.
But here's the thing, if you want a leftist society, if you want that utopian Star Trek
kind of world, you have to have the conversation, you have to have the discussions.
Because education is what's going to get us there.
You have to have an educated populace.
educated populace is harder to make emotional please to the way the right does.
And that's part of it.
Those discussions at education, it's absolutely necessary.
And the other thing is, when you are talking about progressives, when you're talking about
the American left and their messaging, they don't take the gloves off.
Because a lot of times taking off the gloves requires them to reinforce things that they
would like not to have around, reinforce a bad idea to get rid of a worse one.
But they don't see it that way because they know they'll catch heat for it.
A good example of this is a lot of political candidates that are on the right, that are
men, calling them weak.
It would be devastating to them when it comes to their voters, their base.
It would really hurt them, but the American left is never going to do that because it
reinforces toxic masculinity.
That's another reason the left has a huge issue with messaging because a lot of things
are out of bounds for them.
If you could only choose from the following three items to have for breakfast for the
rest of your life, what would you choose?
French toast, pancakes, or waffles?
Probably French toast.
How about a list of the new artificial reefs created in the neighborhood of Russia?
Yeah, you got me.
Okay, I thought that was going to be an environmental question.
For those who don't get it, that's about all of the ships that they've lost to a country
without a navy with major ships.
Okay, of the current Republican candidates, which in your opinion, or should I say based
on their record, would be the most likely to reach out across the aisle and not completely
shut out Dems?
I'm assuming this is for President.
They're all pretty right-wing.
It's one of those things where even those who are opposed to the authoritarian drift,
they're still super right-wing.
I don't know that you're going to get a lot of bipartisanship out of any of them.
I think your best bets would probably be, well, people that wouldn't get elected.
Your best bets are probably Hutchinson, maybe Christie.
You don't have a lot of good options for that.
Any advice for my 19-year-old daughter who is struggling with deciding on a major?
She is only a sophomore, she's got plenty of time.
And taking different types of classes at the U of Utah, but worry she is wasting her time
and money.
I tried to tell her that she has plenty of time to declare her major, and whatever courses
she takes is never wasteful.
Education learning is never wasted, yeah.
But I'm apparently not convincing, okay.
She makes excellent grades, but her stress about this is through the roof at times.
How easily did you decide on your major path?
I still haven't decided completely.
I don't know what I want to be when I grow up.
So wasting her time and money.
If she is incurring a lot of debt and that's starting to weigh on her, if that's the concern
and she feels like she's wasting the money and it's building up this debt, people like
Like us saying all education is good, no education is wasted, that's not going to help.
The thing is, at 19, she doesn't know what she wants, nobody did.
She's a sophomore, all of this is core stuff anyway.
She has time, you're right, everything you're saying is right.
But you're not convincing.
Her stress about this is through the roof at times.
Maybe suggest a little bit of travel or, you know, a little bit of work experience.
Try that.
I know that may not be the answer you're looking for.
But if the stress is there, she's not really going to be focused on finding what she wants
yet.
She's going to be focused on finding something that fits within the idea of not wasting time
and money and all of that.
And based on what you have here, that's not what you want her to look for.
You want her to look for something she's going to be happy doing.
And sometimes the place to find that out isn't necessarily in class.
When do you sleep?
Who says I sleep?
Where does Canada fall into the spreadsheet of the USA's top allies?
Will the USA support Canada regarding the India-Canada situation currently ongoing?
Oh, I don't think it's going to get that serious.
It's not going to get that serious.
Canada is definitely one of our stronger allies.
I don't know, probably top five.
But the current situation between Canada and India,
I truly believe that's going to be all diplomatic.
That's not getting hot, I promise.
It's not going to get that serious.
Earlier today I overheard a snippet from one side of a phone conversation between two family
members who both voted for the tangerine dream.
My deepest apologies to the actual band.
You had no part in this.
The conversation mentioned a video that explains every event from like 2015 up to January 20th.
Oh no.
The person speaking during my accidental eavesdropping had also a few months ago spouted a talking
point that Zelensky was outlawing Christianity, okay?
And I countered with actual information that Zelensky was taking action against the Russian
Orthodox Church, which is an arm of the former KGB and priests from Robes, and that Christianity
in Ukraine is now safer because of it.
Anyway, back to the video.
I did try to find this video so I could prepare myself with the actual information to counter
the regurgitated talking points that I am expecting, but I could not find anything recent
that matched.
Do you have any idea what they're on about?
Uh, a video that explains every event?
No.
Something like that, it's probably something out of the
Q world, that it could be a video that's not even on
YouTube.
It could be on one of the other platforms.
And it probably has like 5,000 views.
And it's going to be really hard to find.
Get the name, though, because I'd love to hear it.
I'd like to watch it, for real.
And that goes to what I was talking about earlier though.
When you are talking about that utopian society, that better world, and needing an educated
populace. Nobody, nobody should hear the phrase, it explains every event from 2015 to now and
and think that that's a logical, like nobody should fall for that. That's media consumption.
That's where those issues are.
Just to preface, I feel you did the best job covering the scandal with Susanna Gibson.
Many other YouTubers focused on exactly what the GOP wanted and weren't as steady on the
cultural analytical will as you were. What I wanted to ask you about was the differences
between it and the recent developments with Boebert. While I acknowledge the difference
was the environment in which the acts took place, the plastering of it over the internet
by leftist content creators makes me feel uneasy. Do you think that it is a similar
situation to Gibson, in that there would be better takeaways by voters.
Where if we focused on Boebert's policy positions, lack thereof, or do you feel that
the innate nature of it being in a public space should be grounds for leniency to creators
who show the video of Boebert?
I'm not trying to bait drama, and please cast this aside if you think it would do so.
No, I mean, you're asking a pretty legit question.
When it comes to stuff like this, everybody's different.
People are different, right?
What appeals to you, what motivates you, what you like to hear, and how you like your information
packaged, it's different than other people.
Yeah, as far as the Boebert thing, this is the first time it's mentioned on my stuff.
But there are a lot of people who, and I'm aware of a couple of channels on the left
that do this, they will use this, bring people in talking about this, and then do a bait
and switch to bring out her horrible record.
And I know that that happens.
I know that there's also some people who are just going to show it and be like, haha.
I like the bait and switch approach, but I also believe in a diversity of tactics and
Knowing that I don't know how to reach every single person, I'm far less critical of how
other people try to go about it.
So okay, MTG has been spouting at the mouth again as she is like, as she likes to do about
states succeeding from the U.S. My question is why are liberals and anyone basically not
MAGA opposed to this? When you think of the states where this lunacy actually seems like
a good idea the rest of the U.S. would be better off without them. Okay, yeah, and this
This person is from Europe.
The problem is, in the United States, you take a state like Mississippi, widely seen
is like a super red state and you know they're they're all about we're gonna leave the union
and all that stuff and it is one of the redder states three out of ten three out of ten
are not that um the support that that seems to exist for this it's not real
The support that actually exists is far less than it seems, based on social media.
Even in super red states, you're still talking 30% are Democrats, vote Democrat.
And even more would be opposed to it.
In a video a few months back, you mentioned that you listen to music that matches the
mood you want to be in.
I followed you for a while, and it seems like all the music you recommend has the same mood,
that being...
Whew.
Man, I'm glad I didn't keep reading that.
Okay, I think the list of music that I put out at that one point in time, I think that
that was tailored at that time to hit a bunch of different styles that contained that message.
You know, a song that I listen to on a pretty regular basis that is not this theme that
I can't say.
Let's just say that it is Rage Against the Machinie is what it says.
Work by Britney Spears, I will listen to that.
That is something that I actually find motivating in a very ironic sense.
But either way, it works.
The music there, the beat is there.
I was wondering how to answer the question, what is a woman or define woman?
I'm an ally but don't know how to articulate this and I hate it being a gotcha question.
You could go into the difference between sex and gender and all of that stuff.
You could.
But I actually saw a response recently and it was the funniest thing that I have witnessed
in a long time. And the general tone of it was, I'm a straight guy. Anything that gets
me excited. It is a gotcha question. And turning it into that and treating it in that manner
and being that dismissive of it, it definitely seemed to work.
So I mean, you can try to educate them.
But generally speaking, those people who are asking this question,
they're beyond it.
You're not going to reach them.
They are happy in their own ignorance.
If you were a tree, what kind of tree would you be and why?
Some kind of fruit tree.
I don't know.
Ha ha ha ha!
Which show do you think you would do best on?
Jeopardy! The Voice?
Tell me you've never heard me sing.
Or Dancing with the Stars.
I actually can dance, but I'm going to go with what is Jeopardy!
Okay, do any of your kids have an interest
in discussing the news with you or each other?
I'd like to think they may share your journalistic talents
and possibly follow in your footsteps.
They have my tendency to focus on little bits of information
tendency to focus on little bits of information and just expand out from
there and learn as much as they can. That obsessive trait that you know I
describe as dedicated and everybody else describes as obsessive. Yeah they
have that which might turn into that one day. If all the ordnance and missiles
that Russia aimed at civilian targets had been used exclusively against
military targets granted that they could hit them how much better would their war
be going much much the use of the precision guided stuff early on and using
that against civilian infrastructure horrible idea the horrible idea and it
has cost them a lot.
U.S. History books.
Can you recommend any books on U.S. history or world history for that matter that you
have found to be exceptionally well written and informative?
People's History, 1776, 1619 Project, Buried My Heart at Wounded Knee.
I personally, when you are looking at history books, I think it's best to not look for
something that is textbook that covers a whole bunch of stuff. I like to look for books on
specific periods in history. I think those are generally better, more informative.
What style of music do you prefer? What style of music do you prefer? Or like the rest of you,
are your choices an eclectic mix? Yeah, they are. Very much so. Do you think rock music will
make a resurgence on the popular music charts because of the political frustrations of Gen Z?
I hope so. I hope so. I'm not sure, though. It's possible, but I don't think it's going to come
back in probably not in the the 80s rock sense it's gonna be different it's
probably going to be more akin to rage so okay been following you since the
beginning of the pandemic introduced to your channel from a co-worker. Since then
I've been following you. Your followers have grown greatly in numbers as
have the frequency of the post you make a day per day. How many people do you
have working with you doing research filming and editing? The sheer number of
sources and reading that must be done in what seems like real time would appear
to be impossible for a one-man operation.
OK, so research and content, like research
and what goes out, that's all me.
Everything else, and I can't stress this enough,
everything else with a YouTube channel is somebody else.
Somebody else takes it.
The list of questions I am reading right now
was put together by pulling them off
the thread on Patreon and pulling them out of emails and put together for me
like this so I could read them. Like everything else with the exception of
the research and the actual production of the content is done by somebody else.
And that frees up a lot of time for me to research on my own. So you're right, if I
If I had to do everything else that normally comes with a YouTube channel, I would never be able to put out the number
of videos I do per day as in-depth as I do.  It just wouldn't happen.
We have a very good division of labor here that works really well.
How do you keep your sanity and calm demeanor when dealing with people who are, shall I say, based outside of reality?
It's funny. I have an easier time dealing with them than I do people who are, quote, principled conservatives.
conservatives, those people who I know have been led down a road because they're easily
manipulated, I actually, I feel sorry for them on some level.
Those who can process information well, those people make me mad.
Because they're choosing to be that way.
This is a question about how the US news covers or fails to cover conflicts.
I don't want to go too far into it because this is actually the subject of the video.
That's like already been recorded just waiting to go out, but news keeps breaking.
I'm gonna skip this one.
Question exactly how many t-shirts do you own?
I have no idea.
No clue.
I mean hundreds.
Hundreds is the right answer.
Your assessment of geopolitics is it's a big poker game where everyone's cheating.
I agree with the premise completely.
You also argue that there are no friends in geopolitics, only interests. I agree,
but I have a question. What if having friends is a country's permanent interest?
Asking vis-a-vis the U.S. building closer relationships with Europe, Japan, South
Korea, Philippines, Vietnam, etc. None of these alliances have to be permanent, but
But is it not in the U.S.'s long-term interest to keep our word and build lasting trust in
defense of global peace?
Or since our foreign policy can change from president to president, is it more accurate
that alliances are our interest until they aren't?
Oh, nice.
I mean, yeah, that's true.
But here's the thing about a country's permanent interest.
There isn't in your list Europe, Japan, South Korea, Philippines, Vietnam.
That's your list.
Let's do this a different way.
Let's say Germany, Japan, Korea, Philippines, Vietnam.
Wasn't that long ago for any of them.
It's interests.
And they normally aren't long lived.
I mean, all of this, the U.S. has been at war in these locations in the last 150 years.
I mean, yeah, it's not permanent.
Is it in U.S. interest to have, you know, long-term peace?
Yes, it should be.
But what changes is the political makeup inside the country, and it makes it hostile to other
nations.
And that's how friends become enemies or how interests change.
Okay.
That's also something that we're doing in a video.
Okay, skipping past that one too.
Are you still planning on visiting Route 66?
Yes, that's still on my list of things to do for this channel.
That's actually the first actual on-the-road thing that we want to do.
If so, which states?
Oh, Chicago to L.A. Do the whole thing.
And who will be with you?
I don't know.
When it comes to the team, some of them like to maintain their privacy.
So they probably won't go.
Some might.
I'm not sure.
And will you interview people along the way?
Yes.
Looking forward to you visiting my museum in Stroud, Oklahoma if you make the trip.
Route 66 Spirit of America Museum.
I will check it out.
Can you tell us either a movie, a band, a TV show, something that you thought you weren't
going to like, but then it ended up that you did?
The Americans.
The TV show The Americans.
Somebody actually just kind of, I'm going to say gently reminded me over and over again
to watch that.
I was very reluctant because normally shows like that are really bad and they get a whole
lot of stuff wrong.
They actually did a really good job with it.
I really liked that one and I did not think I was going to.
Okay, thank you so much for the work you do.
My question may be a tad personal so I totally get it if you don't take it up.
The first video of yours I ever saw was you talking about what it's like to be black
in America.
And I've never heard someone describe it so accurately and succinctly before, like lots
of other black people don't even have an emotionally impressed understanding.
And I was an adult before I did.
So my question is, how did you come to such a grasp of the subject?
your wife and kids black, is who's the person framed over your right shoulder.
In my experience it takes some kind of significant personal relationship.
If not virtually walking in their shoes for a time to get to that level.
Okay, so the painting back there, that is something that one of y'all sent.
something that one of y'all said and I put up there there was actually a number
of them but I really liked that one I was gonna rotate through them but that
one just kind of stuck up there I think that I think one of the things that was
very beneficial for me when it came to understanding the black experience in
the United States was a guy named Old Man Jack. As the name would suggest, he was an
older guy. And he had done a whole lot of time. And he started doing it because of some
activities he did back in the 70s with some some nationalist movements and
conversations with him over coffee it was enlightening and that that's that
was one of the things that really helped frame a lot of stuff for me so can you
explain the use relevance of the Curious George patch. Okay, so there's actually a
video on this that's, I think it's called, Let's Talk About Curious
George. Curious George was a mascot of a group that I was part of and basically
my kid really liked Curious George. Years and years and years later, after that, my
kid liked Curious George. It didn't really have a significance at first. And then as
time went on, and if you watch that video, you'll get to hear the origin of Curious
George and it is way different than you might imagine. I actually had questions
about the appropriateness of it in today's age because there are certain
themes in Curious George that might make you think that maybe it shouldn't be
something that is still in use. If you actually find out the origin of Curious
George, like where it came from, you'll understand why it hasn't been cancelled,
because the assumptions that get made, they're they're not right. It just, it's
stuck. I don't, I don't know why. I've been wondering if you have any favorite
shirts that you don't get to wear as often as you'd like, or that you haven't
haven't had a chance to wear at all yet.
Yeah, so I have a couple that have been sent by y'all
that I haven't been able to wear.
Like I have a Yosemite Sam one.
Haven't worn it yet.
One of y'all sent that.
I have one that is a tie-dye curious George.
Okay, the whole thing is tie-dye,
but like, and even the George is done with tie-dye.
I think it's really cool,
but I haven't found it an appropriate video
to tie it to yet. I started watching your channel with your Ukraine war coverage and
you rapidly became my favorite person on YouTube. Thank you for helping to open my eyes to things.
I wasn't properly understanding or appreciating.
Unfortunately, that journey hasn't been the best
for my mental health, yeah.
With all of the very important problems we are facing,
social, political, economic, and environmental,
across multiple fronts, local, national, and globally,
it feels like too much,
and I don't know where to start helping.
I have some, though limited means,
but if I try to solve all of the problems,
I can't do enough to make any kind of difference.
But if I try to focus on one, I am ignoring other very important issues.
These challenges far exceed my ability to combat them.
As a father, I am hating the world I am leaving for my children.
How do you decide what the most important thing to focus on is?
How do you stay sane?
Do what you're best at.
Do what you are best at.
on that where you can be most effective. Don't don't turn it into a thing where
you want to do something for all of these causes because you're right you
won't be effective and just because you are most effective at one thing and
that's what you're putting the work in on it doesn't mean that you don't care
about the other stuff. If everybody got where they were most effective it
would be incredibly helpful but a lot of times people either they don't know
where they're most effective or they don't they don't think that that's
where they should be. Diversity of tactics is really important. Understand
And let's say that you are a social issue near you, pick a social issue near you, and
you are very good and you are very effective in a role of fundraising for them, for an
organization that does good work there.
You're fantastic at it, but you don't like doing it really because you don't get that
rush of actually helping people.
You don't feel it anyway.
You don't see it.
That's still what you need to be doing because that's what you're most effective at.
That's the part that's hard.
You have to figure out what you can do better than most other people.
That's where you belong because that's how you are most effective.
That is the most effective way to affect change.
How do you stay sane?
Do that.
Start there.
Well I promise, do you have an idea of what the Biden administration's stance on Saudi
Arabia is?
They don't seem to be playing ball except for occasionally tossing the ball back.
I think it certainly appears to me that the Biden administration is trying to enact the
foreign policy plan that has been talked about for a long time where the USD prioritizes
the Middle East.
What that means is that Saudi Arabia, Iran, and Israel, they become key players.
The U.S. doesn't intervene anymore.
The U.S. doesn't play those games anymore.
Those countries, it's their game now, and the U.S. is stepping away from it.
I think that's actually his plan.
So he should be disengaging from Saudi Arabia.
And that does appear to be what he's doing.
You said you play with the horses, do you ride them as well?
Yes.
I just, I guess a lot of people mistook that.
Yeah, I can ride, I do ride, I have been thrown.
I know how to take care of the horses too, I just don't, I don't enjoy it.
To me it's work.
The flip side to this, I like taking care of the dogs.
That to me is fun.
That's what I mean.
So when I say I play with them, what I mean is yeah, I'll ride them, I'll hang out with
them, you know, do ground work with them or whatever, pet them across the fence and talk
to them.
That kind of thing.
I play with them.
the work. I'll play a no-work on that one. Who is Bo's meme consultant? What's the pay like?
We do not have one of those. Okay, what is your beverage? Coffee, something more robust? Do you
own any regular coffee cups or mugs?
Uh, yeah, I drink way more coffee than I should, I'm sure.
Way more than the recommended beverage allowance.
I hope Biden doesn't come get me over that.
Do you own any regular coffee mugs or cups?
Yeah.
some back there up on the shelves. Or do you mean not logo? Well you can't answer
me. But yes, I have a bunch of coffee mugs. Maybe I should start cycling through
those as well. With so many still in delusion land, which is your favorite Q
delusion? That Kennedy's coming back. That's my favorite.
Do you think the mummified aliens on display in Mexico
are the real thing or something else?
I don't have an opinion on that.
I know nothing about what it is.
Anything that I would say would be more about what
I would want it to be, not what I think it
is based on any information.
Okay, this is a question about how long Russia can sustain due to economic issues.
There's a video recorded on this that is waiting to go out, so I'm not going to get into this,
but the answer is it's in the protracted stage.
It's going to be a while.
Could this conflict continue for years?
Yeah.
It most likely will.
Bo, what could be the factors that convince independent and moderate voters, i.e. non-Trumpers,
to vote for Trump in the 2024 election?
The only thing that would do that is the economy, that's it, nothing else.
Basically Americans would have to get to the point where they are ill-informed enough to
think that Trump would help the economy.
That's the only way independents, moderates would go that way.
I know you use a lot of euphemisms to avoid terms that set people off, but why do you
avoid using the name Progosian and instead use the boss of Wagner. It's
funny because it's the exact opposite reason. I had somebody tell me that those
associated with Wagner do not like it when Americans mispronounce Wagner. If
you go back to the very first videos where Wagner came up, I pronounced it
correctly and then I was told this and I've used every opportunity I could to insert the
word Wagner and mispronounce it.
Just to understand, I wasn't informed of this in like a nice way and it was just me being
Trump claimed he won the 2020 election. Doesn't that mean even if he was denied
it that he can't run again because he was elected twice because of the 22nd
of November? Yeah, nice. Yeah, I don't know that that's an argument we should make. That's
That's funny.
So either he's lying or he's ineligible and try to put it into that.
I mean that might be fun.
That would be a fun argument to make online.
Not one that you would expect to actually stick.
What are Bo and his family harvesting from the garden?
Any funny garden stories?
I think our biggest thing this year is going to be sweet potatoes.
Oh, a bunch of sweet potatoes, it looks like.
Uh, any funny garden stories?
I'm sure I told y'all about the deer.
If not, short version, I, uh, I got to pet a deer that neither
one of us knew the other one was there, and we wound up
really close to each other.
And then the other deer came out of the treeline, spooked
the one that was by me, and Bambi beat me up.
That's the last funny garden story that we've had.
When you sometimes switch patches,
do you swap the hat too, or just the patch?
Oh, that's Velcro.
Yeah, just the patch.
Oh, that's it.
OK.
So those are all of the questions.
Man, that might have been 100 questions
for 100,000 subscribers or something.
I don't know what the plan was, but this was a lot of them.
And I'm actually surprised we made it through all of them
with one sitting.
But that's it.
So there's your special.
We will be back to our normally scheduled content
on this channel on Sunday morning.
Anyway, it's just a thought.
And thank you all for hanging in there with me
all these years. You have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}