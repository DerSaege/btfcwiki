---
title: Let's talk about the GOP tanking the GOP budget....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LD5AiiAAWqA) |
| Published | 2023/09/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The House GOP put up a very right-wing bill that didn't pass because of the far-right wing of the Republican Party.
- The bill was rejected 232 to 198, leading to a high likelihood of a government shutdown.
- Dan Crenshaw pointed out the contradiction in the GOP's actions, calling their position conservative while rejecting it.
- The leverage to avoid a shutdown was lost when the far-right Republicans shot down their own bill.
- A government shutdown seems inevitable, with unknown duration, as it may take a miracle to avoid.
- McCarthy could have prevented the shutdown by reaching across the aisle, but didn't.

### Quotes

- "They killed the most conservative position we could take and then called themselves the real conservatives."
- "It was just too far outside the window that the Senate would even consider."
- "At this point, it is a relatively safe assumption that there will be a government shutdown."
- "It's just a thought. Y'all Have a good day."

### Oneliner

The House GOP's far-right bill failed, setting the stage for a likely government shutdown as McCarthy could have prevented it by crossing the aisle.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Prepare for a government shutdown by ensuring you have necessary supplies and resources (implied).

### Whats missing in summary

Details on specific impacts of the impending government shutdown. 

### Tags

#GovernmentShutdown #HouseGOP #FarRight #DanCrenshaw #PoliticalLeverage


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the House GOP
and the fact that we are very likely now headed
into a government shutdown for at least some period of time.
We'll talk about the bill,
and then we will talk about Dan Crenshaw
being right about something,
and then we will talk a little bit
about some of the impacts and go from there.
Okay, so if you missed the news,
the House GOP put up an incredibly right-wing bill.
So right-wing that realistically,
it wasn't gonna get through the Senate.
It was as right-wing as you could even hope
to make believable and have treated as a real thing
and maybe have some negotiation
between the House and the Senate and all of that stuff.
It was very, very, very right-wing.
It funded the government for like 30 days.
It cut spending.
It increased border restrictions, all kinds of stuff.
It didn't pass.
And it didn't pass because of the far right wing
of the Republican Party.
They basically said that it didn't go far enough, I guess.
They weren't having it.
So the bill was rejected, I want to say 232 to 198.
Republicans could not pass their own bill.
OK, so let's talk about Dan Crenshaw and something he said
in a moment of political clarity.
They killed the most conservative position
we could take and then called themselves
the real conservatives, which is like,
Make that make sense.
You can't make it make sense.
And so now you're going to get a more liberal spending bill.
He's probably right.
He is probably right.
He is definitely right about this
being the most conservative, most right-wing bill
that they could ask for.
Anything beyond this, it would have
dropped for being too much. It was just too far outside the window that the
Senate would even consider. So they they shot down that bill which means the US
government is likely headed into a shutdown. The leverage that exists is
avoiding the shutdown. There would have been Democrats who would have been more
open to negotiating about very conservative, quote, incredibly far
right-wing positions, if it meant avoiding a shutdown. But once the
shutdown happens, well, it's the Republicans fault, especially when it
happens like this. So the Democratic Party isn't going to be willing to come
that far along, because it was the Republicans who tanked their own bill.
The public's going to know that. The Republican Party couldn't put forth
anything. So in essence, the far right removed their leverage, because now there
is a very very high likelihood of going into a shutdown. Okay so what does a
shutdown mean? That's the questions and there are tons of them coming in. I
already have gotten questions about what it means for HUD, what it means for
national parks. I'm going to put together a video explaining what's gonna happen.
If you have questions about any specific government programs, agencies, anything
like that, put them in the comments down below. We'll go through them and put
together something so people understand what's happening. And I will go ahead and
tell you now for those people that have plans headed to the national parks,
that they're probably not going to be open. There's going to be a lot of
different impacts from this. So if you have questions, get them down below.
we'll try to answer them. At this point though, it is a relatively safe
assumption that there will be a government shutdown of some period of
time, how long we don't know yet. The reality here is at this point, it would
take a miracle to avoid a government shutdown. It would take McCarthy
basically swallowing his pride and doing some things that I feel like if he was
gonna do them, he would have done them before now. And again, that shows the
other side to this. Yes, far-right Republicans tanked this, but McCarthy had
the ability to avoid all of this by simply reaching across the aisle. So
that's where we're at. There will be more updates on this soon, I am sure, and we
will try to keep you informed as best we can. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}