---
title: Let's talk about McCarthy, Boebert, and how I'm not that smart....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DC9JKQb5eeA) |
| Published | 2023/09/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a message criticizing his political analysis, suggesting he stick to discussing wars rather than politics.
- Describes the message's assertion that Republicans can't team up with Democrats and still get elected, using catchy slogans like "vote with Dems, never get elected again."
- Questions the logic behind the Republican bill being stopped by 21 Republicans who voted with Democrats.
- Points out the hypocrisy of Republicans using slogans like "vote with Dems, never get elected again" while not following it themselves.
- Notes how this situation reveals the manipulation within the Republican Party and the split between different factions.
- Talks about information silos within the Republican Party and how Trump's influence has caused division.
- Mentions the infighting within the Republican Party due to conflicting ideologies and misleading tactics used by different factions.

### Quotes

- "Vote with Dems, never get elected again."
- "This is how the Republican Party dupes its rank and file."
- "They will give you a slogan and violate it while they're giving it to you."

### Oneliner

Beau explains the manipulation and split within the Republican Party, showcasing the misleading tactics used by different factions.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Educate others on the manipulation tactics used by political parties (implied)
- Stay informed about political developments and question catchy slogans and talking points (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the internal dynamics and manipulation within the Republican Party, which can be better understood by watching the entire video.

### Tags

#RepublicanParty #PoliticalManipulation #InformationSilos #Factionalism #TrumpAdministration


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about talking points.
We're going to talk about McCarthy
reaching across the aisle.
We're going to talk about a message
that I received that explains how that could never happen.
And we're going to explain how even
Boebert is smarter than I am.
Okay, so here's the message.
Bo, I get that you're a dim, but stop, just stop.
Just stay out of politics coverage and tell us about wars.
You keep saying McCarthy could teach,
I'm assuming that means reach across the aisle,
and team up with Democrats.
That will never and could never happen.
We, the Republican voters of this country,
would never vote for him again.
No Republican gets to team up with Democrats
and get elected again.
Dems won't take their own side in an argument, and that's their problem.
We do.
We don't let people cross like Manchin does to you.
Vote with Dems, never get elected again.
I like that it rhymes.
It's that simple.
The fact that you don't know this shows you don't understand politics.
Even Boebert understands this.
That's why she didn't break ranks.
Even Boebert is smarter than you.
just to cover war.
That's pretty impassioned, or maybe it's just the way I read it.
I guess you're right.
Maybe I'm just not that smart.
Maybe I don't get it.
Maybe I don't understand politics at all.
But I do have one question about this.
That bill that got tanked, that bill that got tanked, the GOP
bill, the Republican bill for the budget, it got stopped by 21 Republicans.
Wait a second, wait, I know I'm not real smart, but I don't think 21 is a majority
in the House.
could go look. But I'm pretty sure that that's not enough. Who'd they vote with?
How did they stop the Republican bill? They voted with the Democrats, just to be
clear on this. Do you know who one of those people was that didn't break
ranks? Boebert. This is how the Republican Party dupes its rank and file.
They give you a catchy talking point, a slogan, vote with Dems, never get elected again.
And they get you saying it while they vote with Dems. And then you use that slogan to
defend them. 21 Republicans broke ranks and voted against the Republican bill.
That's what happened. I know, again, I'm not real smart, but I feel like that runs
counter to your message here unless you're telling me that all of the far
right Republicans are gonna lose their next election. Which, I mean, if that's
the case. I mean okay, I mean I guess you got me, but I feel like the person who
sent this message, I feel like they don't understand what happened. I feel like
they don't understand that Boebert crossed and voted with the Democratic
Party who was opposed to the steep cuts in the bill. Worked across the aisle just
saying. This is how they do it. They sit there and they tell you something and
they say it with such confidence and you believe them and you believe that they
believe whatever slogan they're giving you. They don't. They will give you a
slogan and violate it while they're giving it to you. Vote with Dems, never
get elected again.
They voted with Dems.
That's how they stopped the bill.
This not just does it show the way Republicans manipulate their base.
It shows the split in the Republican Party.
Because the overwhelming majority of Republicans supported that bill.
It was the far-right faction, it was the MAGA-aligned faction that broke ranks, voted with the Democratic
Party, and stopped it.
But by reading this, you would know that.
It shows the information silos at work.
There are silos even within the Republican Party.
It shows that factionalism.
When I talk about how the Democratic Party isn't really a singular entity, it's a coalition
party, the Republicans, generally, they've been one party up until very recently, until
Trump came along and brought in far more authoritarian rhetoric and style of government.
It split the Republican Party.
Now the Republican Party is at odds with itself because you have some that are just conservatives.
Not my people, but they're just conservatives.
And then you have some people who are overtly authoritarian.
The information silos that exist, there's no overlap between those.
That's why you have the infighting in the Republican Party today.
Because while most of the Republican Party consistently misleads their base, they do
it in different ways now.
You have a Republican saying, hey, McCarthy can't cross the aisle.
He can't go around the far-right Republicans.
We'll vote him out.
You're not allowed to do that because we're good Republicans.
Meanwhile, the far-right Republicans went across the aisle.
This is how they dupe them.
This is how they trick them.
They give them a rhyming talking point, a three-word slogan, get them repeating it,
and they never check to see if the people who gave them that slogan are violating it.
I know I'm not that smart, but I know that.
Anyway, it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}