---
title: The Roads Not Taken EP3
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ogqBy6_uiRA) |
| Published | 2023/09/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of under-reported news events from the past week.
- Icebreaker ship heading to Antarctica to rescue someone at KC Research Station.
- Nobel Foundation reverses invitations to Russia, Belarus, and Iran.
- Putin arranging high-level meetings with various countries to rehab Russia's image.
- Ukrainian drone strikes into Russian territory are becoming more effective and embarrassing for Russia.
- Speculation about China's stumbling economy and G7 closely monitoring.
- Ukraine's counteroffensive advancing slowly with Western critics urging realistic expectations.
- Kenneth Cheesebrough severs case from Sidney Powell.
- Lawmakers in Texas call for investigation into Texas Guard's intelligence gathering.
- McCarthy may order a full House vote on impeachment inquiry into Biden.
- Panel in Georgia may convene to suspend Georgia State Senator Steele.
- Pope criticizes US Conservatives for prioritizing ideology over faith.
- Billionaires planning an environmentally friendly community in California face resistance.
- Truth Social faces shareholder approval test by September 8th.
- Burning Man attendees stranded after rapid rains in the desert.
- Assistance offers for those impacted by Adelia in Florida.
- Concerns about AI-generated books on foraging lacking accuracy.
- AI experiment covering high school sports suspended for inaccuracy.
- Russian film "The Witness" flops due to propaganda content.
- New term "thousand ton rule" estimates premature deaths due to emissions and climate issues.
- Exxon report states slow progress towards achieving net zero goals.
- Radioactive boars in Bavaria linked to contaminated truffles from atmospheric testing.

### Quotes

- "AI-generated books about foraging: accuracy is life and death."
- "Exxon report: energy transition not happening at the needed scale."

### Oneliner

Beau gives an overview of under-reported foreign policy, environmental, and cultural news events from the past week, shedding light on significant developments and potential implications.

### Audience

Media consumers

### On-the-ground actions from transcript

- Contact individuals impacted by Adelia in Florida and offer assistance (implied).

### Whats missing in summary

Insight into the importance of staying informed on under-reported news events for better information consumption and understanding of global events.

### Tags

#UnderreportedNews #ForeignPolicy #Environment #AI #ClimateChange


## Transcript
Well, howdy there, internet people.
Let's bow again.
And welcome to episode three of The Road's Not Taken,
a series where we go over the previous week's events
and focus on news that was under-reported
or news that wasn't covered at all on my channel
that is probably information you'll see again.
It will pop up later.
And this is just to provide a little bit of a heads up
as to what may be coming and provide some context
to events in the future.
And we will start off with foreign policy stuff.
And we'll start off with an icebreaker, not a figurative one.
There is an icebreaker.
A ship is heading towards Antarctica
right now to rescue a person who is
in medical distress of some kind.
What the issue is and who the person is hasn't been released,
but they are at KC Research Station.
This is the type of event that tends
to get fixated on by large outlets,
because there's a lot of cool visuals and high stakes.
So I would expect to see that come up again.
Let's see, the Nobel Foundation has reversed invitations
to Russia, Belarus, and Iran.
This is standard Cold War-style politics. Expect to see a whole bunch of this in the
future over the next couple of years. Putin is arranging a high-level meeting with Turkey
to talk about the grain deal again. This seems to be part of a wider diplomatic push from Putin.
I would expect to see him meet with high-ranking leaders in China, Iran, anywhere that has
some power, even if it's only regional, that is somewhat accommodating.
It seems as though he's trying to rehabilitate the image of Russia on the international stage.
I don't know how successful that's going to be while they are still prosecuting the
war that led to the decline of their reputation on the international stage.
I don't know how successful this push is going to be.
Let's see.
In other news, Ukrainian drone strikes into Russian territory are becoming increasingly
more effective and more embarrassing for the Russian government. They're becoming more
accurate as well. Eventually, my guess is the Russian government is going to engage
in some kind of propaganda stunt about this. They're going to have to do something to
make Russian citizens who are within the range of Ukrainian drones feel safer and feel like
The Russian government is doing something because right now, honestly, it appears that
civilians that are in that range inside of Russia, it seems as though the reason they're
safe is because Ukraine allows them to be, not because the Russian government is doing
a good job of protecting them.
That perception is something that Putin is going to have to change.
That's not a tenable position for him to be in.
More news about China.
The economy is stumbling, but it's not falling, at least not according to the people that
I have talked to.
There's a lot of speculation about the economy in China and whether or not it's about to
tank. The people that I've talked to have said, no, this is more of a stumble that shows some
deep-seated systemic issues, but it's not something that's just going to collapse their economy.
It's worth noting that it does appear that members of the G7 are paying very close attention to this
as this little stumble is showcasing a lot of weaknesses in the Chinese system.
And as China, I don't want to say steps because it seems like it's already happened,
stepped into the number two spot as far as world influence,
There are going to be more and more major and regional powers that are going to be looking
for weak points in the Chinese system as a whole, and some of those are definitely in
the economic system.
Back to Ukraine.
counteroffensive has been advancing slowly, but it has been advancing. There have been some Western
observers and commentators that have become critical, and there are criticisms that have
come from allies. Ukraine is urging its allies to maintain realistic expectations. And that's a
logical and reasonable request, it is worth remembering this conflict, the best
description I have heard is that it's World War I with drones. There's a lot
of tech, but the type of fighting is, it's not static, but it's close to it. A lot
of the commentators are encouraging combined arms maneuvers and they
want results similar to the first Gulf War.
Yeah, combined arms can get you there, but it's worth remembering, especially for Western
commentators, that a key piece of the West being able to do that is the massive amount
of air superiority that the West maintains, that Ukraine does not have.
It's also worth noting that when Ukraine was first invaded by Russia in the most recent
part, Russia wanted it to be like the first Gulf War too.
It didn't work for them either.
It's almost like that operational template is decades old and completely different terrain.
not be advisable to keep trying to win this conflict by doing the same things
that worked years and years and years ago. It's a different war. Russia has
deployed and by deployed, they put it on quote combat duty, an ICBM that is
scary. It's strategic signaling. It's messaging. We'll end up doing something
in-depth about this next week on the other channel but it's kind of on the
back burner so I wanted to talk about it now because a bunch of questions came in.
The fact that it's on the back burner should answer most of the questions.
This isn't something to... this shouldn't keep you up at night. It's messaging.
It's propaganda, they're not getting ready to launch, at least not at this time, and
most of the people who sent in questions were more focused on them launching against the
United States.
It is incredibly unlikely.
Okay, moving to the U.S. Kenneth Cheesebrough has filed to sever his case from Sidney Powell's.
Can't imagine why.
The filing definitely seems to indicate that Cheesebrough's team views Powell as a liability
because it went way out of its way to talk about how different they were.
I think that there is still a little bit of misunderstanding about how some of these cases
work and I'm not sure that a lot of the legal maneuvering that is occurring right now is
actually the smart move.
In Texas, lawmakers are calling for an investigation into the Texas Guard, specifically some of
their intelligence gathering efforts.
The allegations include them exceeding the scope of what they're allowed to do, particularly
when it comes to gathering intelligence within the United States, and mishandling classified
documents.
I'm going to go out on a limb here and suggest that those people calling for an investigation
going to get it. The allegations alone are enough to to cause closer scrutiny.
It seems like McCarthy will order a full House vote to determine whether or not the House of
Representatives wants to initiate an impeachment inquiry into Biden. Why? It's a political move.
My guess is that they don't have the votes, so this sets up a situation where those people
who want to be distanced from the, let's be honest, at this point the reason they don't
have the votes, even though they hold the majority, is because they don't have the
evidence to warrant one.
But it allows those people to have some distance, while at the same time giving those people
who have constituents who are demanding an impeachment for something, it allows those
representatives to vote in favor of impeaching Biden.
And that's how they can frame it in their ads.
As far as suspensions, removals, that kind of thing, we are also likely to see a panel
convened in Georgia to review the possibility of suspending a Georgia State Senator Steele,
S-T-I-L-L, I think. He was indicted in the Fulton case. And the way that works is the Governor can
convene the panel. I want to say the Attorney General is on it, and then one representative from
the House and one representative from the Senate in the state, I think is how it works.
And we'll probably hear more about that this week.
The Pope chastised the US Conservatives for replacing faith with ideology and maintaining
a quote backwards and reactionary attitude.
In seemingly related news, Trump is still leading in the GOP primary.
Now out in California, billionaires are starting to open up about some plans they have to build
a community that they say is going to be incredibly environmentally friendly, it's going to have
open spaces. It's going to be advanced in a whole bunch of ways. There is probably
going to be some resistance to this in California because a lot of people are
already starting to see it as like a community development for the elite.
Let's see. Trump social media platform, Truth Social, it is facing a big big big
test with its shareholders. If shareholders fail to approve an extension by September
8th, then the whole deal, the whole thing may go sideways. And it's going to leave
people on the hook for some money. Now, it's worth noting that at one point in time, Digital
world that company, their shares were going for more than $170 a share.
On Friday, they were going for less than $17 per share.
Burning Man attendees were stranded after rapid rains in the desert.
being urged to conserve food, water, fuel, everything.
Basically the rains came in so quickly, it made a mess of the area around it, making
it incredibly hard to get out or to bring stuff in.
I don't know how long that situation is going to last.
There seem to be two different camps developing among people, those who just want to get out
and those who are saying, we're self-reliant and we can handle this.
We'll see how it plays out.
Now as far as providing assistance, we're getting a bunch of messages coming in from
people offering assistance to those that were impacted by I'dalia near us.
We are not getting a lot of requests for assistance, like at all.
So either they're in a situation where they're not able to access things yet, or we just
don't have a strong presence in that audience.
So if you know people who are in that area, in the area impacted by Adela here in Florida,
get them our information.
If they need help, just make sure you get them in touch with us.
OK, let's see.
Now moving on to cultural news.
AI generation tech, all of that stuff,
it's having a bad week in the news and publishing world.
There are concerns being expressed
about the possibility of AI-generated books being
available about foraging.
And the concern is that some of the books
are less than accurate.
When you were talking about foraging,
particularly with things like mushrooms
or something like that, accuracy is life and death.
It's a real big deal.
So that is something to definitely be aware of.
As people flood the market with stuff that might not
have the same level of research, it
It may be a good idea, if you are getting books about things that could be dangerous,
to make sure you are getting it from a source that isn't just scraped data from somewhere.
At the same time, on the news front, a news outlet was experimenting with AI to cover
high school sports.
having an issue with the product not being up to standards as far as accuracy or style,
the verbiage was unique.
The program has reportedly already been suspended.
The Russian film, The Witness, has flopped just completely.
This is a film that cost about 200 million rubles to make.
It has grossed about 14 million and it's been out a bit.
The film follows a violinist who happened to be in Ukraine when the Russian, I'm sorry,
liberation began.
The film is just, it's full of ridiculous way over the top propaganda and nobody wants
to watch it.
It seems the general attitude of a lot of Russians right now is weak enough of that
on the news, we're not going to pay to watch it.
So it is not going well.
Let's see.
The move over to environmental news, there is a new term that is being thrown out.
It's called the thousand ton rule.
And basically what it means is that for every thousand tons of emissions, well, that's one
person gone.
By that math, we'll lose roughly about a billion people over the next hundred or so years due
to premature death due to climate issues.
In related news, an Exxon report states that an energy transition is underway, but it is
not happening at the scale that would allow us to achieve net zero goals on time.
An Exxon report is saying that.
And undoubtedly somewhere in there, there is, you know, we're all looking to try to
figure out who's responsible for this.
Yeah.
So the interesting part about this is that you are now having entities like Exxon point
out that even by Exxon standards, we're not moving fast enough.
Now some odd news.
Radioactive bores in Bavaria are not entirely due to Chernobyl, as previously thought, according
to a new report.
The report suggests that truffles are contaminated in part due to atmospheric testing.
concern is that the wild boar, which is seen as a delicacy in certain areas, that people
are going to stop eating it.
And if people don't eat it, well, then, you know, they could overpopulate and, let's
be honest, who wants to eat a radioactive spider pig?
So the concern is that the population is going to continue to grow.
And I mean, it just seems not good to have a bunch of radioactive wild pigs running around.
So that is the rough overview of the news that we kind of skipped over this week.
We're going to go on to a couple of questions here.
Let's see.
You've talked about your dog increased disease due to climate change and vaccines.
you maybe do a piece on the intersection of all three? Oh, yeah, yeah, we can do that.
We can actually talk about it right now. So, when we have talked about climate change,
we have talked about how insects are likely to have increased range. They'll be able to
go beyond the areas that they normally are.
That means that the vaccines you need for your animals,
not just dogs, but animals in general,
they're going to change and you need to stay updated on it.
Because as the climate shifts,
some insects that carry diseases
that might impact dogs or horses or whatever,
they may now be able to survive in your area
where they weren't able to before. So you didn't need to protect them against these diseases before,
but now you do. That's actually a really good point, and we will probably turn that into a
full video over on the other channel at some point.
Okay, let's see. Building off that question about work-life balance, I have a question.
How do you keep so many things going at once without losing them?
I work doing media production and public relations for a non-profit with a shoestring budget.
How do you maintain your production schedule?"
So first, I just want to point out that I have a video on community gardening, on community
gardens that has been in production I don't know a year maybe more and it's
not even like it's because it's gonna be an amazing video it's just something
that isn't a high priority we wanted to get it out before spring and then when
we realized that that wasn't going to happen because that's when people would
need it well it got pushed back it became a lower priority because we didn't
the deadline. So as far as maintaining the production schedule, you have to keep realistic
deadlines and be willing to amend them. That video, I want to say it was supposed to be the third,
maybe fourth video that went out on this channel. So it's not about forcing everything through,
it's about making sure that you keep up with what the priorities are. And then if you do
that most people won't even notice that a video has been in production for a year.
I mean I'm sure a lot of people know that one's that one is still back there
because it's been an annoyance to me that it's not done. But yeah that would
be my advice is to focus on the deadlines and develop a very smooth
the workflow for stuff that happens constantly. The content that has to go
out all the time, simplify that workflow as much as humanly possible. And then the
special projects, just make sure that you keep the deadlines intact and make
sure that you're willing to shift them when the priorities change. That would
be that would be my advice and don't think that you're not going to lose one
every once in a while okay Bo a very dear and very right-wing friend of mine
has what he believes to be an antique National Rifle Association sign on the
wall of his garage what he actually has there is the logo of the National
Recovery Administration? Is it wrong slash evil of me to withhold this
information from him? No, no, it's not. I mean, see, I wouldn't be able to not tell
him for an extended period, you know? I mean, if this is something that he's
pointing out he has this antique NRA sign. Probably not somebody that would...
You know what you should do? You should get him a book on the National Recovery
Administration. Just get him a book on it. See if he would give it to him
for Christmas or something like that.
And maybe he'll realize the logo.
And that would be, I personally think
that would be very entertaining.
OK, so it looks like that's it for the Q&A.
Weren't many this time.
So they're probably putting together a whole Q&A episode
with questions for me to go through.
Because normally there's more than this
at the end of one of these.
So that's it.
So hopefully, going over the under-reported content like
this, it will help provide better information consumption
and people will have better access to information.
And honestly, that will make all of the difference.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}