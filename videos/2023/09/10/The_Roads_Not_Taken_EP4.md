---
title: The Roads Not Taken EP4
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3VHD-if23Ds) |
| Published | 2023/09/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The episode focuses on under-reported events from the previous week, providing context that is often overlooked.
- G20 welcoming the African Union at their summit is significant news that may go under-reported.
- Putin's promotion of a general who advocates invading Eastern Europe and sees Ukraine as a stepping stone is concerning.
- Elon Musk faces criticism over disruptions caused by his company.
- A Ukrainian attack on Russian ships that later launched attacks against civilians, including children, is a continuing story.
- Issues with Starlink deployment and its relationship with the Department of Defense will persist until resolved.
- The UK-France route closure due to a security incident may have future implications.
- Australia and China resuming high-level talks after a three-year break signals positive news.
- Russian nationals charged in cyber attacks in the US are currently in Russia, not in custody.
- The NYPD reaching an agreement on handling demonstrations, imposing new policies to ensure de-escalation, is noteworthy.

### Quotes

- "G20 welcomed the African Union at their summit. This is gonna be really big news."
- "Elon Musk is taking incredibly heavy criticism for allegations that his company disrupted."
- "A Ukrainian attack on Russian ships. Those ships later went on to launch attacks against civilians."
- "The NYPD agreement requires new policies and bans kettling, low-flying helos."
- "Australia and China have resumed high-level talks after a three-year break."

### Oneliner

Beau sheds light on under-reported events, from G20's significant move welcoming the African Union to ongoing issues like cyber attacks and international tensions.

### Audience

News enthusiasts

### On-the-ground actions from transcript

- Contact local media outlets to urge better coverage of significant events (suggested).
- Join organizations advocating for peaceful conflict resolution (implied).
- Monitor developments in international relations and share information with your community (exemplified).

### Whats missing in summary

Insights into under-reported global events and the importance of staying informed for a comprehensive understanding of global dynamics.

### Tags

#UnderreportedEvents #InternationalRelations #CommunityAction #MediaCoverage #GlobalAwareness


## Transcript
Well, howdy there, internet people, Lidsbo again,
and welcome to episode four of The Road's Not Taken,
a series where we go over the previous week's events
that were under-reported,
news that should have made headlines,
should have got real coverage,
but didn't for one reason or another.
Most of this is material that is context.
you'll see it again, it will come up in the future.
So hearing a little bit more about it ahead of time
can help you piece the story together as it moves forward.
All right, we're gonna start off with foreign policy.
G20 welcomed the African Union at their summit.
This is gonna be really big news
that goes wildly under-reported.
And it shows a disconnect between news reporting
reality, especially when it comes to sensationalism and how sensationalism
really drives a lot of reporting and the doom and gloom that goes along with the
news. I want you to consider all of the coverage that you saw when BRICS started
talking about expanding and inviting countries from Africa to attend. The G20
is welcoming the African Union. It's going to be a regional bloc treated the
way the EU is. It's huge news. It's not going to get the coverage it should. This
is going to have a lot of impacts down the road and we will probably do a whole
episode on this over on the other channel. Let's see. Putin promoted a
general who has advocated for invading Eastern Europe said that he sees Ukraine
as a stepping stone, talking about committing to a years-long campaign. This
will probably get a little bit of attention later. It may not mean much in
the grand scheme. But the messaging that is being sent by promoting somebody who
has openly stated stuff like this, it goes along with their deployment of
the ICBM and all of that stuff. It's saber-rattling. Elon Musk is taking
incredibly heavy criticism for allegations that his company or in some of the allegations
him specifically disrupted. Let's leave it at that. A Ukrainian attack on Russian ships. Those
ships would later go on to launch attacks against civilians including children. That story is not
going to go away. There are a lot of issues with how Starlink has been deployed and the relationship
with DoD. We've talked about it before on the other channel. This is going to keep coming up
until there is a resolution. Let's see, a main route between England and France was shut down
for quite a while, while the British Army, their EOD, their bomb squad, was examining a vehicle
the entrance to the tunnel. So, how that plays out and what it actually was, why they were on the
level of alert they were, all of that, it's probably something that's going to come up again.
Australia and China have resumed high-level talks after, I don't know, a three-year break.
They had a high-level meeting. That's good news. That is good news. Large countries, powerful countries,
going on a break, it's normally not a good thing, a lot like a relationship.
9. Russian nationals were charged in a series of cyber attacks in the United States.
My understanding is that they are currently in Russia.
They have been charged, but they are not in custody.
Russia held some quote elections in the occupied area.
It's really to provide a manufactured talking point for their propagandists.
The elections have absolutely no legitimacy.
Romania is upgrading port infrastructure.
This is to help Ukraine and the grain issue.
That's what's going on there.
A federal judge in the United States has ordered Texas to remove that floating barrier that's
down in the Rio Grande.
Germany has charged two men with treason for spying on behalf of Russia and allegedly passing
some documents.
OK, on to the US.
The FAA has told SpaceX it should keep its Starship grounded.
That giant rocket?
It needs to stay on the ground.
He recommended 63 corrective actions in a report that was looking into the mid-air rapid
disassembly that occurred back in April.
Trump shared an article on his social media site that was criticizing his unchristian
behavior.
He apparently only read the headline and didn't really understand that because he shared it
from his profile. It was deleted shortly thereafter. 13 presidential centers going all the way
back to Hoover have put out a statement basically in favor of democracy and suggesting that
the United States needs to take care of its own house and protect democracy in the United
States because they're the foreign policy initiatives that the US wants to
engage in. It's incredibly hard for anybody to take that seriously while you
actively have members of a party's upper echelons basically calling for the end
of democracy in this country. Looking at you Republicans. Flesh-eating bacteria
was may still be present in post hurricane waters where Idalia hit. If
you're ever dealing with this don't enter water, water like that that if you have
cuts, if you have open cuts, it's a big deal. You need to be on the lookout for
lesions that kind of look like spider bites and if you see them you need to
get to the dock as quickly as possible. The New York Police Department reached an
agreement in response to a whole bunch of lawsuits concerning their handling of
demonstrations. The agreement requires a bunch of new policies. It bans
kettling, low-flying helos that are used to kind of intimidate those attending
demonstrations. They are supposed to engage in de-escalation. We'll see how
all that turns out. Maui has sent out a message to tourists. The message is don't
stay away. Come to the island. Officials are basically asking people to still come
and visit, but stay away from the impacted areas and be respectful. We'll
We'll see how that plays out.
There's a lot of tension between those who are native to Hawaii and tourists, and a whole
lot of it is because of the behavior of tourists when they come to the island.
From an economic standpoint, I totally get why officials are saying this.
They need the money.
the same time. I am skeptical that that issues are going to be avoided.
Tennessee's Gloria Johnson has announced her bid for Senate. She was one of the
Tennessee three. The three people that Republicans in Tennessee tried to
expel because they didn't like them I guess. She was not expelled and it
definitely highlighted the the underlying race issues in that little
political stunt. Three people engaged in the activity, the two young black men
then were expelled they are now back which is just the ultimate funny right
there but she was not however it gave her a much higher profile and she is
trying to capitalize on that you use that for a Senate bid. The special
counsel that is looking into Hunter Biden might be bringing him up on a gun
charge this month. We'll have to wait and see. And let's see, McConnell's doctor, the
house doctor up there at the Capitol, is basically saying it wasn't a stroke, it
wasn't a seizure, and that he's okay. I don't know that the majority of Americans
going to believe that.
The request, what they are asking people to believe are that the two times that that happened
to him and he just kind of froze up, that both of them occurred on camera.
That seems unlikely unless it has something to do with the lights or something like that.
Now I will say that when you're looking at other footage from this period, he's fine.
It does seem to be a pretty limited freeze-up, but to say that there's kind of nothing wrong,
We see what we see.
Okay, in cultural news, Tucker Carlson brought a person on his Twitter show who claimed that
former President Obama was gay and had a substance issue.
Generally speaking, nobody really cared.
The claims are doubted by many, but it's also worth remembering at this point, Obama's
been out of office a long time.
This is the type of story that is specifically for older people who are very much in conservative
movements who don't want to let go of the past and maybe don't realize how much time
has passed since Obama's activities mattered politically speaking. He's been
out of office a long time. The actions were even longer ago than that. And for
the most part, most Americans wouldn't care even if this was true. It's one of
those things where a lot of the comments I saw were, you're trying to make me like
Obama. There is a huge disconnect between conservative talking heads and
the way the average American feels in regards to anything dealing with the
LGBTQ community or substance issues, anything like that. I'm not sure why
they're going after Obama after all this time other than to cater to a very
specific demographic of people who I mean as far as I know they're not
exactly the type of people who are going to be on Twitter but there we are the
hurricane that hit the southwestern United States has reshaped and altered
the landscape there in Death Valley. The park may be closed for months while they
try to get everything kind of rehabilitated there. In odd news, Florida
man was arrested by the Coast Guard for trying to cross the Atlantic in what is
basically a hamster wheel style watercraft. The interesting part about
this story is that it's Florida man was arrested by the Coast Guard for trying
to cross the Atlantic in a hamster wheel type thing. Again, this isn't the first
time the sky's been busted for this because Florida. Let's see. Okay, there
are a whole bunch of examples coming out of Ukraine right now when it comes to
failed generic tourniquets. We have talked about them on the channel before.
Cat Generation 7 tourniquets are the ones that I have personally done like a
little bit of research on and those are the ones that I use. There are a bunch
of companies that make things that look like that. They are not all the same and
them being used in Ukraine, when they fail it's it's a big deal. And there are
a lot of examples of those of that happening. So when you are purchasing one
make sure you're getting you're not getting one that's like Bob's Tarnicate.
You're getting one that's that's coming from a reputable company. Okay now
on to the Q&A's at the end of this. Let's see, what do we have? What's your go-to
comfort food for camping or disasters? Cheesy rice. Cheesy rice. I don't really
know why. There's no interesting story behind that. It's just easy to prepare
and it sits well. Tell us a ranch story. You should do this every week on the
roads. Okay, you're actually going to get one over on the main channel. You're going
to get a ranch story about Zonya and a fence soon. I recorded that video a couple of days
ago, but news keeps breaking. So something else? Okay. I have a, we've got a friend and
When he married a woman from Mexico, they had a kid.
One of the funniest things that happened was their kid, who speaks fluent Spanish, failed
Spanish one year.
To me, that was just the funniest thing in the world, mainly because of the way his mom
is.
She was incredibly unhappy.
I tried to explain to her, I was like it probably didn't have anything to do with him not
speaking Spanish.
We know he speaks Spanish, it's him not doing the work because he knows how to do it.
One of my kids got an F in agriculture.
It's not as funny now.
I'm like dude, you live on a ranch.
That's the weekly sitcom rant story, I guess.
Could you explain why you don't believe in long prison sentences when you talk about
January 6th?
No, no, no, no, no.
Okay, don't get that wrong.
It's not that I don't believe in long prison sentences in regards to January 6th.
I don't believe in long prison sentences, period.
most crimes. I personally believe that prison should be rehabilitative and
generally speaking long prison sentences aren't really conducive to that. Don't
don't don't mistake that for something that just applies to them. I am I am
somebody who believes that the overwhelming majority of people who are
currently in custody don't need to be. They need to be more on like a probation type thing.
That's not something that is specific to January 6th. In fact, for the leadership, that might
be one of the few times when I actually understand it, or those who are true ideologues and who
aren't going to alter their opinion.
That might be one of the few times I actually get why the government uses those methods.
I still think there are probably better ones, but at least with that it's something I can
understand.
Do you think it would help deradicalize older right-wing Americans if there was more centrist
or a leftist talk radio?
In parenthesis, like actually on the radio.
So not podcasts.
Yes, however, like, okay, so there's two questions here.
Do I think it would help deradicalize older right-wing Americans?
No, not really.
Mainly because at that point, if you were to take one of them from right now, by the
time they work themselves out of where they have worked themselves into as far as their
fervent beliefs, they will have aged out of any efforts.
But I believe it's worth doing to stop or slow that radicalization process.
So it's not to impact those that are older right now.
I don't know that there's much that can be done there, especially if they're still
actively consuming that kind of rhetoric. But having that as an alternative or a competing
a competing show on the same kind of platform, that's probably incredibly useful.
Are you going to talk about Project 2025? Yes, it seems like something you'd be covering a lot.
I will just not right now but yes and if you're not if you haven't heard of it
yet there is a conservative plan called project 2025 there are some people who
have put out some stuff about it already and it's good it's worth it's worth
getting up to speed on this is gonna be something that is going to become a big
issue closer to the election and right now would be the time to kind of brush
up on it but yes I will definitely be covering that you're right it is
something it's one of those things that's in the background that I'm paying
very close attention to why do you why do you wear hoodies in Florida this time
of year on the roads. Okay, so yeah, it's still hot here in the daytime. In the
daytime, it is still 90 to 100 degrees. At night it cools off, sometimes can get
to like 70, and I understand that it's 70 is still warm, but it feels cooler. So
So I film these at night most times, so that's why I'm
wearing hoodies in these.
It's not a style thing to differentiate the channels.
This is normally done at night, and it's cooler.
And that's it.
Those are all the questions.
OK.
So now you have a little bit more context, a little bit
more information, and better information will make all of
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}