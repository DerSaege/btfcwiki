---
title: Let's talk about Trump, New York, and Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aXtcP62nQpU) |
| Published | 2023/09/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on Trump-related developments in New York and Georgia are discussed.
- The civil case in New York seeking $250 million will begin trial on Monday.
- The trial was initially supposed to run until December 22nd but may move faster now.
- The witness list for the trial has about 100 names, indicating it may be tedious.
- In Georgia, Trump has decided not to seek removal of his case to federal court.
- Trump's decision in Georgia is based on confidence in a fair trial and due process.
- Speculations arise on why Trump made this decision, possibly to avoid testifying.
- Trump may believe his own propaganda, influencing his legal decisions.
- Beau hints at potential reasons behind Trump's choice regarding his case jurisdiction.
- The New York case will progress, while the Georgia case remains where it is.
- Expect rapid developments and information influx in the near future.
- Viewers are urged to prepare for an increase in the pace of events regarding these cases.

### Quotes

- "The New York case will be moving forward. On Monday, the Georgia case is staying where it's at."
- "Get ready for a whole lot of developments to start occurring very, very quickly."

### Oneliner

Beau provides updates on Trump-related developments in New York and Georgia, hinting at potential reasons behind Trump's legal decisions and urging viewers to brace for rapid developments.

### Audience

Legal observers, political analysts.

### On-the-ground actions from transcript

- Stay informed about the developments in the Trump-related cases in New York and Georgia (suggested).
- Follow reliable news sources for updates on these legal proceedings (suggested).

### Whats missing in summary

Insights on the potential implications of these legal developments for Trump and the broader political landscape.

### Tags

#Trump #LegalDevelopments #NewYork #Georgia #FairTrial


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are calling to talk about some developments
coming out of New York and Georgia concerning Trump.
We have been trying to keep most of the procedural Trump news
over on the Roads with Beau, over on the other channel.
In fact, we put out a video this morning
covering the important events of late.
However, there have been two developments
that will not hold until the next update.
So we're gonna run through those real quick.
Starting in New York, this is the civil case
where they are seeking a minimum of $250 million.
That case was kind of on hold but a five-judge panel
has decided, nope, it's not on hold anymore.
Trial starts on Monday, on Monday.
Now, initially that trial was supposed to run from Monday
until right before Christmas.
I wanna say December 22nd.
But given the fact that some of the major questions
have been resolved in that case,
it will move a little bit faster.
People have been asking what shorter means.
Not short, just shorter.
The witness list, I wanna say has like 100 names on it.
It still looks like it's most likely going to be,
let's just say tedious.
So, it won't last until Christmas, but it's probably not going to be over in a day or
two.
Okay, moving to Georgia.
Trump has decided, well, let me just read it.
Trump now notifies the court that he will not be seeking to remove his case to federal
court.
It also said this, this decision is based on his well-founded confidence that this honorable
Court intends to fully and completely protect his constitutional right to a fair trial and guarantee
him due process of law throughout the prosecution of his case in the Superior Court of Fulton County,
Georgia." I mean, that's cool. That's what they said, but I think people want to know what are
the other possible reasons he might have made a decision because a whole lot of commentators
were saying he'll get a better jury in federal court. Ever since this came up, I've been saying
stuff like, yeah, I mean, he could, but I don't know that it's a good idea.
As soon as his news broke, people started asking what I meant by that because I wouldn't
say while the decision was being made because I don't like to interrupt people when they're
making a mistake.
If you want to remove your case to federal court, you have to explain why.
You have to give them a story.
You have to testify.
Trump or maybe his attorneys didn't want that.
That would be my guess.
Another thing at play probably has to do with Trump believing his own propaganda.
He has hit that phase of putting out information where he's starting to buy into some of the
stuff that he says that he probably didn't believe initially.
And the federal judge is, I want to say, appointed by Obama, either way, appointed by a Democrat.
So of course, they're not on the same team or something like that.
Those would be my guesses, and the having to testify is the reason I was like, yeah,
that may not be a great idea this whole time.
So at the moment, the New York case will be moving forward.
On Monday, the Georgia case is staying where it's at.
Get ready for a whole lot of developments to start occurring very, very quickly.
We've been talking about how that's going to happen.
It's definitely happening now.
All those people that have been saying, what's taking so long?
You're about to be flooded with information and events, and it is just going to continue
to pick up speed. So be be ready for that. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}