---
title: Let's talk about the GOP and $1 for SECDEF....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vgF8IG-chus) |
| Published | 2023/09/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are using social media to provoke outrage and elevate their profile.
- Marjorie Taylor Greene introduced a provision to reduce the Defense Secretary's salary to $1 a year.
- Greene believes the current Defense Secretary is destroying the military despite his 40+ years of service.
- Legislation like Greene's provision has zero chance of becoming law but is used for social media engagement.
- High-ranking military officials are paid well to make them harder to buy and prevent financial motives.
- While Americans face financial difficulties, politicians are focused on social media antics instead of real issues.

### Quotes

- "They're confusing social media engagement with votes."
- "What's the message? Marjorie Taylor Greene thinks she knows more about the military than the 40-year-plus veteran."
- "Legislation like this has zero chance of ever actually going anywhere."
- "This is what they're doing for social media clicks."
- "Rather than getting a budget together, this is what they're playing with."

### Oneliner

Republicans use social media antics like reducing the Defense Secretary's salary to $1 to provoke outrage and gain attention, while real issues like Americans facing financial difficulties are sidelined.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact local representatives to prioritize real issues over social media antics (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how Republicans are leveraging social media for attention while neglecting pressing issues.

### Tags

#Republicans #SocialMedia #MarjorieTaylorGreene #DefenseSecretary #PoliticalAntics


## Transcript
Well, howdy there internet people, it's Bo again.
So today, we are going to talk about Republicans,
the internet, social media and
what a lot of Republicans are really concerned with
right now, particularly some of those in the house.
Because for a while, I've been saying that Republicans
are leaning into social media,
they're confusing social media engagement with votes,
They're really just out there to provoke outrage in hopes
that it elevates their profile.
And I had somebody send me a message saying, hey, you
always say that, but you don't give any evidence.
I'm willing to bet that any time I've said that, the topic
kind of made itself evident.
But since the question was asked, let's go through it.
Marjorie Taylor Greene introduced this little provision.
The purpose of it is to make sure
that the Secretary of Defense gets paid $1, $1 a year
to reduce the salary to that.
Not a joke.
I'm going to read it.
None of the funds made available by this act
may be used to pay Defense Secretary Lloyd James
Austin III, a salary that exceeds a dollar.
Okay, first why? Why would this be introduced? Because according to Marjorie Taylor Greene,
the defense secretary, well, he's destroying the military.
Austin has more than 40 years in the military.
Marjorie Taylor Greene just hopefully figured out that space lasers weren't a thing.
So I don't know that that legislation really matters and her opinion on this is really one
that should be considered that heavily, but here's the other thing.
This was put out there. This is the provision. Do you think that it's going to
become law? Contrary to what a lot of people pretend, just because something
makes it through in the House doesn't mean that it becomes law. For a bunch of
people who like to swear they love the Constitution, they have a real hard time
following basic civics. If it makes it out of the House, it's got to pass the
Senate, a Senate that confirmed Austin. I want to say 93 voted in favor. I feel like they don't
want him paid a dollar just saying. And then even if something wild was to happen there, then
Biden would have to sign off on it. So what is this? It has zero chance of becoming law,
has zero chance of being implemented. So why do it? To send a message? What's the message?
Marjorie Taylor Greene thinks she knows more about the military than the
40-year-plus veteran who's the current Secretary of Defense. Is that the message?
or is it something that is just so out there that it's going to provoke social
media engagement? It's going to generate outrage. Outrage that they believe
transfers into votes. It's not simple. There have been a number of pieces of
legislation that originated in the House that are like this zero chance of ever
actually going anywhere but they can use that on social media to walk out there
and say hey I introduced this today because we're gonna stop them from
making the military woke you should be mad about that that's what it's about
There's no way this is going to occur. It's also worth remembering for those
who may not understand this, the reason that a lot of high-ranking people in the
military are paid very well or people who are in sensitive positions are paid
very well is because it makes them harder to buy.
This is somebody that would have access to a whole lot of sensitive information.
You don't want them in financial trouble because they may try to solve that problem, right?
You don't want to give them a motive.
That would be ridiculous.
But speaking of financial trouble, I would like to point out once again that this is
what's going on while the country is watching the clock as millions of Americans are about
to have financial difficulty because rather than getting a budget together, this is what
they're playing with.
This is what they're doing for social media clicks.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}