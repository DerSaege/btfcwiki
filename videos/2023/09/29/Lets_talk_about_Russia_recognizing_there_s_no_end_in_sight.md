---
title: Let's talk about Russia recognizing there's no end in sight....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RRcWKwxYbJ0) |
| Published | 2023/09/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia has finally acknowledged being in a protracted conflict, expecting military goals to be achieved by 2025.
- Their military efforts are focused on force generation due to degraded combat power.
- Russian tactics led to the loss of experienced personnel needed for training new troops.
- The Russian economy may not hold out for the duration necessary to achieve military objectives.
- Western commentators should stop setting unrealistic expectations on the conflict.
- Ukraine believes the war may terminate due to Russian economic issues in 2025.
- Both sides agree that the conflict will be long-term, with Russia having the clocks but Ukraine having the time.
- The only potential threat to a Ukrainian victory could be the West giving up and not providing support.
- As conflicts drag on, Russian resolve is likely to weaken faster than the resolve of those fighting for their homes.
- Beau questions the understanding and planning of the Russian Defense Minister amid the ongoing conflict.

### Quotes

- "Russia has finally acknowledged being in a protracted conflict, expecting military goals to be achieved by 2025."
- "The Russian economy may not hold out for the duration necessary to achieve military objectives."
- "Both sides agree that the conflict will be long-term, with Russia having the clocks but Ukraine having the time."
- "As conflicts drag on, Russian resolve is likely to weaken faster than the resolve of those fighting for their homes."
- "Beau questions the understanding and planning of the Russian Defense Minister amid the ongoing conflict."

### Oneliner

Russia acknowledges a protracted conflict, focusing on force generation due to degraded combat power, while Ukraine anticipates economic issues causing a termination by 2025.

### Audience

Conflict analysts

### On-the-ground actions from transcript

- Monitor and advocate for realistic expectations in Western commentary on the conflict (implied).
- Support Ukrainian efforts by ensuring continued assistance from the West (implied).
- Stay informed and educated on the ongoing conflict dynamics (implied).

### Whats missing in summary

Analysis of potential humanitarian impacts and civilian resilience in the conflict zone.

### Tags

#Russia #Ukraine #ConflictAnalysis #MilitaryGoals #EconomicImpact #WesternSupport


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Russia
finally acknowledging it.
Something we've been talking about on this channel
since literally before it started.
And Russia has finally come to that conclusion,
and the defense minister has said as much.
From the West, from Western commentators,
There has been a push of how long it's going to take.
This is taking too long.
It should be done faster than that.
And they are expecting unrealistic results.
And Ukraine recently said that they expect it
to go until 2025 or 2026.
Russia now says that their military goals could be achieved with the consistent implementation
of the measures in the action until 2025.
Russia is acknowledging now that the troops won't be home by Christmas, that they have
entered a protracted conflict that will take years.
Here's the thing.
This is a piecemeal dissemination of information.
They can achieve their military goals.
What are their goals again?
They don't know.
Not really.
This is the opposite of mission creep.
where they had a mission, it did not go according to plan, and now they're trying
to find a new one rather than acknowledge the obvious that they lost
the war. Normally mission creep goes the other way. Go over there with a specific
goal, something happens, and the goal expands. This is the opposite. They have a
force there that is now searching for a mission. I do not believe that Russia
thinks it can take the capital by 2025. It's worth noting that right now Russian
efforts are really kind of focused on force generation, building up combat
power, because they don't have any. I don't want to say they don't have any. What
What they had has been greatly degraded and they're trying to rebuild their force.
The problem is their tactics early on led to the loss of those people who might actually
be able to provide the training that the new troops would need.
Russia is in a very precarious position.
It is worth noting that this is the information that was provided to Putin.
So sure, 2025, as long as you just temper that with the fact that these are the same
people who told them they could take the capital in a couple of weeks.
Russia at some point is going to have to acknowledge that they've lost.
They lost.
And come to terms with that.
The Russian economy really can't hold out for as long as it would take to, to complete
any tangible military objective.
It's not there.
It isn't there.
That's one of the interesting things.
The Ukrainian intelligence assessment, one of the things that they believe will cause
a termination of the war, or at least additional Russian effort, is the economy not being able
to hold up, and it will falter sometime in 2025 by their assessments.
But by then they'll have arms to continue until 2026.
That's the Ukrainian assessment.
I doubt that the Minister of Defense that doesn't really seem to have a great handle
on their own job really understands the economic issues that are on the horizon.
You have both sides now saying that this is a years-long conflict.
It would be great if Western commentators stopped setting unrealistic expectations because
this has evolved into one of those situations where Russia has the clocks but Ukraine has
the time.
The only thing at this point that could jeopardize a Ukrainian victory that is within the normal
realm of assessing stuff would be the West giving up and not helping.
That's about it.
When you are talking about normal contingency planning and you are looking through this,
that's really the only thing that can cause their loss.
sure you have wild card stuff but when you're actually looking at what is
likely to happen that's the only thing and then it's time. It's time. Nobody
wants a war to last a long time. All wars are popular for the first 30 days, right?
We saw that in Russia. That plays out every single time. It really doesn't
matter what country it is. As this drags on, Russian resolve will probably weaken.
It will probably weaken at a much faster rate than the resolve of people
fighting for their homes. I feel like this should be a lesson that
Americans, the West in general, don't need.
But for those who are cheerleading the Russian efforts and continuing to talk about when
they're going to send in their good troops or whatever the cope of the week is, the defense
Minister of Russia is now saying, there's no end in sight. 2025, if everything goes
according to plan. And it won't. I mean, I would point out that thus far, nothing has
gone according to plan. They are hoping for a dramatic shift in their command, control,
all of this stuff in the middle of a conflict. And as near as I can tell, the
effort they are willing to put into achieving that change is ordering it.
That's not a plan, that's a wish.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}