---
title: Let's talk about Biden, Bernie, and UAW....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qRboj1lrb-g) |
| Published | 2023/09/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Bernie showing support for Biden by making a video is being misconstrued by some as him selling out to corporate Democrats.
- Biden's appearance at a UAW picket line with workers is seen as historic by Bernie, not a sign of selling out.
- Beau believes Bernie's video was an attempt to draw attention to the importance of Biden's support at the picket line.
- The President showing up at a picket line was a significant move that could be used to advocate for striking workers.
- Beau argues that Biden supporting some progressive ideas does not mean he is entirely progressive and rejects the narrative of good guys versus bad guys.
- Biden's appearance at the picket line was seen as a show, which is a common practice to garner attention and media coverage.
- Bernie's video used the President's appearance to shed light on CEO pay packages versus worker wages and the struggles faced by workers.
- The decline in union strength has impacted starting wages and job status in sectors like automotive manufacturing.
- Beau defends Bernie's actions as a way to bring attention to workers' issues and believes Biden's support was a strategic move for gaining media coverage.
- The situation is viewed as a blend of social media narratives and pessimism, but Beau encourages considering the positive impact of the President's appearance at the picket line.

### Quotes
- "A sitting president of the United States showed up at a picket line of an active strike and threw the weight of the Oval Office behind the workers."
- "I think Bernie was just trying to acknowledge that and use it. Use it. Use the power of the president showing up to get that message out there."
- "Biden showing up was for show. Absolutely. Absolutely. That part is true."

### Oneliner
Beau explains why Bernie's support for Biden and Biden's appearance at a picket line are significant moments often misunderstood and misrepresented, shedding light on the importance of advocating for workers' rights.

### Audience
Activists, Political Observers, Voters

### On-the-ground actions from transcript
- Support workers' rights and unions by actively participating in strikes and picket lines (exemplified)
- Advocate for fair wages and better working conditions in your community (implied)
- Educate others about the importance of unions and the struggles faced by workers (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of the significance of Bernie's support for Biden and Biden's appearance at a picket line, shedding light on the importance of advocating for workers' rights and the impact of declining union strength on starting wages and job status in various sectors.

### Tags
#BernieSanders #JoeBiden #WorkersRights #UnionSupport #PoliticalAnalysis #SocialJustice


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Biden and Bernie
and jobs and a development
and how some people are reading that development
and a message I got,
because it definitely kind of gave me pause a little bit.
But I think it's something that might be worth going over
because while this occurred
and it definitely got some coverage,
I'm not sure people understand what the coverage really shows.
OK, so here's the message.
I'm confused about something, and I've
got some friends telling me something
that I find hard to believe.
In case you missed the news, end quotes,
Bernie put out a video strongly supporting Biden.
Biden showed up at a UAW picket line.
Now my friends are saying that was all for show
that Bernie falling for it is proof that Bernie has either lost it or has sold out to the corporate
Dems. Just wondering what you think. I want to quote a Bronx tale, but I won't. Okay, so
So Bernie did put out a video, and I've seen it.
It's not incredibly long.
He put out a video basically just highlighting kind of why this was important.
And I'll try to find it and put it down below so you can watch it.
Bernie sold out to the corporate Dems.
Sure.
all this time. It was Joe Biden that got Bernie to give up on his beliefs. I mean, that doesn't
really track. Or that he's lost it. By supporting something he's always supported? Unions, right?
Okay, the reason he made that video, and he says it, in it, he says that, you know, it's
historic.
It is.
A sitting president of the United States showed up at a picket line of an active strike and
threw the weight of the Oval Office behind the workers.
Yeah, I mean, that's kind of a big deal.
It's not something that has happened in modern American history.
It is worth noting, it is worth acknowledging.
I don't think that Bernie acknowledging this is a sign that he's lost it or that he's sold
out.
I think he's trying to draw attention to the fact that the President of the United States
showed up at a picket line, and then he used that appearance in his video, he used that appearance
to help make the case for the striking workers, which, I mean, that's totally on brand for Bernie.
That's like his whole thing. So I don't think that this is any violation of his beliefs.
I think it's more a whole lot of people being told over and over again that Biden isn't,
you know, super progressive and all of this stuff, and now they don't know what to think.
Biden is not super progressive, but he does embrace some progressive ideas.
It's not an all or nothing situation.
are, again, trying to put things into good guys and bad guys. There's way more to it than that.
About the only thing your friends have right here is that it was for show.
Biden showing up was for show. Absolutely. Absolutely. That that part is true.
I mean, what did you expect? I mean, do you expect him to show up as Dark Brandon with
an axe handle. I mean that kind of union activity doesn't happen very much anymore.
Picket lines are for the most part, there are exceptions obviously, but picket
lines for the most part are demonstrations. They are for show, to
get attention, to get media coverage, to get support. Having the president show up
at your line and vocalize that support that can then be used by other people
say I don't know a senator historically in support of unions to help make the
case that's the point that's that's that's the thing and this is one of the
issues that a lot of that side of the aisle has.
There are not many people who are so far left that supporting unions is bad.
That's kind of not a thing.
But because of the nature of a lot of the discussion, saying anything that would be
supportive of Biden, well, that would be bad.
Even though objectively, if they were to list a qualification for president, I would imagine
that most people who have an opinion on whether or not Bernie has sold out to the corporate
Dems, I would imagine that saying a president showing up to support a union
on strike and being at the picket line, that that would go in the good column. I
think Bernie was just trying to acknowledge that and use it. Use it. Use
the power of the president showing up to get that message out there. To talk about
the pay packages of the CEOs, which I don't remember, I didn't take notes
during the video, but I want to say they're around 20 million versus the
worker. All of the profits, the stock buybacks, this is what he was
actually talking about in that video. And he was able to do that because he had a
good launching point because the president showed up. Good people who are
good at getting a message out, they do that a lot. Sometimes they may take a
message that was asking about whether or not somebody sold out and use that to
talk about the workers and how they have lost and how they're really making
less now than they used to because of inflation over the years.
That the starting wages, they're not what they should be.
That an automotive manufacturing job, which at one point in time was seen as like the
pinnacle of manufacturing jobs in the US, it's not anymore because the unions
haven't been as strong, because they haven't had the support, because they
haven't had people show up at the picket line and say, hey look at these guys,
listen to what they're saying. Now I don't think Bernie sold out. I don't
think Bernie sold out because he supports unions and I don't think he's
lost it. I think he put out a message in a video that could be consumed by
people who maybe don't understand the importance of unions and he used the
appearance of the President of the United States at a picket line to help
make the case. I mean when you are talking about somebody who's kind of
always talking about inequality, and talking about what the workers need,
those people who are listening to you after a while, they already know.
You need something new.
You need some new jumping off point to get people to hear it.
And Biden gave them that.
I tend to believe that Biden totally understood that it was, quote, a show, it was politics.
Him showing up there got more coverage on it, it helped get the workers' demands out.
I don't know what more you want in that case, and I certainly don't think Bernie has sold
out by continuing to support things that he has supported his entire career.
It's a combination of the way social media discussions occur and being a doomer.
It's not good.
Anyway, it's just a thought.
It's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}