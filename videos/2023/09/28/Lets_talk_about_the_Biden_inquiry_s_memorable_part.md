---
title: Let's talk about the Biden inquiry's memorable part....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=jAVNO4SC1lU) |
| Published | 2023/09/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- House Republicans initiated an inquiry to determine whether to impeach the current president.
- Their witness stated that the current evidence does not support articles of impeachment.
- Despite this, House Republicans are pursuing further investigation with 12,000 pages of bank records.
- The inquiry into impeachment has taken precedence over preventing a government shutdown.
- In roughly 61 hours, a government shutdown may occur, impacting millions of Americans who could stop receiving paychecks.
- Some individuals may not even be aware of how they will be affected by the government shutdown.
- House Republicans have prioritized the impeachment inquiry over helping working-class Americans.
- Beau criticizes the Republican Party for losing sight of the real issues and focusing on impeachment for social media engagement.
- He points out that preventing financial hardship for Americans should take precedence over impeachment proceedings.
- Beau urges people to pay attention to the fact that millions of Americans may suffer due to the government shutdown while Congress prioritizes impeachment.

### Quotes

- "Playing impeachment is the priority as the clock winds down and millions of working-class Americans, many of whom are their constituents, get hurt."
- "Keeping the lights on, stopping millions of working-class Americans from suffering financial hardship, that's not important."
- "This was the priority."
- "Doing something to prevent this, you're right, had everything gone according to the way things should work, nobody would have noticed."
- "They will notice when their checks stop coming, and there will be anger generated."

### Oneliner

House Republicans prioritize impeachment over preventing a government shutdown that could harm millions of working-class Americans financially.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact your representatives to urge them to prioritize preventing a government shutdown and supporting working-class Americans (implied).
- Stay informed about the government shutdown situation and how it may impact you and your community (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how House Republicans are prioritizing impeachment over addressing critical issues like preventing a government shutdown and supporting working-class Americans. Viewing the full transcript can offer a deeper understanding of Beau's perspective on these political priorities.

### Tags

#HouseRepublicans #ImpeachmentInquiry #GovernmentShutdown #WorkingClassAmericans #Priorities


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden, House Republicans, the inquiry.
What's important and what's not.
We're going to run through everything real quick because, you know, it, it
started and we'll, uh, we'll go through it.
So if you have missed it, Republicans in the House have decided that they want
an inquiry into whether or not they should impeach the current president.
There are questions about the amount of evidence that House Republicans have.
I do not believe that the current evidence would support articles of impeachment.
That's not a disclosure notice, that's a quote from their witness.
That's not me saying that.
That's their witness.
But they want to kind of look into it a little bit more.
They've got 12,000 pages of bank records and no money going to the president.
But they want to look into it a little bit more.
It's a case of show me the person and I'll find the crime.
what's going on because there's no evidence to support the allegations at
this point but they want to investigate it a little bit more I would suggest you
should probably do that prior to all of this but whatever that's it is what it
is but here's the important part this is the the part that I think Americans
really need to pay attention to, is that we're roughly 61 hours away from a
government shutdown.
Playing impeachment is what Republicans in the House decided was a priority.
Not keeping the lights on.
61 hours and there are going to be a whole bunch of people who are going to stop getting
a paycheck.
There are going to be millions of Americans impacted and some of them, I realized today,
some of them don't know it.
There's a facility near me that makes stuff for the government.
guy who drives a truck. He doesn't work for the government. He doesn't work for the federal
government. But when that facility stops producing, he loses out. He's not going to have as much
his pay will go down. Rather than work to help millions of working-class
Americans, they're doing that. That's what they're doing as the seconds, minutes,
and hours tick by. I think that that's really what the American people should
remember about this. There was no evidence presented, so there's nothing to remember there.
What should be important is that Congress, House Republicans, decided that this is the priority.
playing impeachment is the priority as the clock winds down and millions of
working-class Americans, many of whom are their constituents, get hurt. That's what
people need to remember about this. That's the most memorable thing about
this inquiry. It shows you that the Republican Party has just kind of lost
the plot. Keeping the lights on, stopping millions of working-class Americans from
suffering financial hardship, that's not important. Why? Because it doesn't get
social media clicks because it doesn't drive engagement because that's what
they're about. It doesn't generate that anchor. I'll tell you this, doing something
to prevent this, you're right, had everything gone according to the way
things should work, nobody would have noticed. That's true. I promise you they
will notice when their checks stop coming, and there will be anger generated.
And this is what people should remember.
This was the priority.
Holding an inquiry, talking about something when their own witness, I do not believe that
the current evidence would support articles of impeachment.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}