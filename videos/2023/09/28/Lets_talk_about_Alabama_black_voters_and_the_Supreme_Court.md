---
title: Let's talk about Alabama, black voters, and the Supreme Court....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6Yp-IGwVTXQ) |
| Published | 2023/09/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court denied Alabama Republicans the ability to diminish the power of black voters through redistricting.
- Alabama produced new maps with only one majority black district, prompting federal judges to intervene.
- Despite federal judges' orders to create a second majority black district, Alabama Republicans failed to comply.
- A special master was appointed to address the issue when Alabama Republicans disregarded federal judges' instructions.
- The Supreme Court upheld their decision, reinforcing the need for a second majority black district.
- Alabama Republicans' defiance and attempts to dilute black voters' power were strongly condemned by the courts.
- The special master submitted three proposals to address the redistricting issue in Alabama.
- The proposed maps aim to respect communities of interest while ensuring fair representation.
- Selecting one of the special master's proposals will determine the new maps for Alabama.
- Overall, the court rulings and interventions aimed to prevent the dilution of black voters' power in Alabama.

### Quotes

- "Alabama Republicans were being told for the second time that they can't dilute the power of black voters because."
- "It seems odd that these kinds of court cases are still happening, but on the other hand, I guess it doesn't seem that odd."

### Oneliner

The Supreme Court intervened twice to prevent Alabama Republicans from diluting black voters' power through redistricting, reinforcing the need for fair representation.

### Audience

Voters, Activists, Legal Advocates

### On-the-ground actions from transcript

- Advocate for fair redistricting in your community (implied)
- Stay informed about voting rights issues and legal proceedings (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the complex legal battles surrounding redistricting in Alabama and the importance of fair representation.

### Tags

#Alabama #Redistricting #VotingRights #SupremeCourt #BlackVoters


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Alabama and voting in the
Supreme court twice and federal courts twice, and then a special master.
Okay.
So if you have missed the news, the Supreme court told Republicans in
Alabama, no, we're actually not going to allow you to dilute the power of black
voters, basically would have boiled down to.
A whole chain of events.
Alabama produces some new maps.
Maps have one district that is majority black.
It should have more than that, period.
And it goes to federal court.
Federal judges are like, hey, you need more than one district that's majority black.
You're intentionally diluting the power of black voters.
And Alabama was like, no, we don't want to do that because the 60s.
And we're going to take it to the Supreme Court.
And the federal judges were like, fine, take it to the Supreme Court, goes to the Supreme
Court.
And the Supreme Court says, hey, yeah, you actually can't do that.
So you need to create a second district.
So Republicans in Alabama, they create new maps, but they don't create a second district
that is majority black or close to it.
And they go back to the federal judges.
The federal judges are like, seriously?
Fine, you don't even get to say anymore,
we're appointing a special master.
Alabama Republicans are like,
well, we're gonna take it to the Supreme Court again.
They take it to the Supreme Court.
Supreme Court's like, did I stutter?
And that's pretty much the end of it.
Now it goes back down.
It is worth noting the special master,
I believe their proposals were ready today,
about the same time Alabama Republicans were being told
for the second time that they can't dilute
the power of black voters because.
It seems odd that these kinds of court cases
are still happening, but on the other hand,
I guess it doesn't seem that odd.
But I want it kind of just acknowledged for a moment
that what Alabama was trying to do with these maps
was so out of line that a Supreme Court that
is just notorious for undermining voting rights,
it was out of line by their rulings.
And the open defiance of the court probably did not help.
So one of the special master proposals,
I think they submitted three, that will be reviewed.
And one of those will go into effect.
From what I understand, it wasn't actually super hard.
It was difficult to not break up other areas of interest,
other communities of interest.
But it wasn't impossible.
And there are three different variations.
And I'll select one of those, and those
will be the new maps in Alabama.
Anyway, it's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}