---
title: The Roads Not Taken Special Trump Exhibit C
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7ouiamoJh0o) |
| Published | 2023/09/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recapping recent legal developments involving Trump and his businesses, focusing on New York and Georgia.
- New York judge ruled Trump Organization liable for fraud, which could lead to canceling its ability to operate in the state.
- Delay in ruling on whether houses owned by Trump companies will be affected by the decision.
- Trial expected to be shorter due to key decision made in summary judgment.
- Trump's team reportedly trying to suppress infamous phone call about finding extra votes in Georgia.
- Georgia jurors' identities to remain secret.
- DA in Georgia dismisses threats against her, particularly from House Republicans.
- Mention of Mark Meadows reportedly burning documents in his office fireplace.
- Challenges to Trump's Presidential Records Act defense and denial of recusal request in D.C. election interference case.
- Potential probe into Trump's political operations and payments to lawyers for witnesses.
- Fake electors in Michigan seeking case dismissal based on brainwashing claims.
- Trump's comments on General Milley potentially causing issues for him.
- Warning of potential financial devastation for Trump from legal proceedings.

### Quotes

- "The potential liability is huge. It's much bigger than I think the Trump world understands."
- "Having the right information will make all the difference."

### Oneliner

Beau recaps recent legal troubles for Trump, including liability ruling in New York and attempts to suppress incriminating phone call in Georgia, warning of potential financial devastation.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact local news outlets to stay informed about ongoing legal proceedings and developments (suggested)
- Stay engaged with political and legal news to understand the implications of these cases (implied)
- Support organizations advocating for accountability and transparency in political activities (implied)

### Whats missing in summary

Insights into the potential long-term consequences of the legal challenges on Trump's financial and political future.

### Tags

#Trump #LegalTroubles #NewYork #Georgia #FinancialConsequences #PoliticalOperations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to do another special Trump exhibit
on The Road's Not Taken.
I believe this is the third one, so exhibit C.
We will be going through the news related to Trump
and all of his various legal entanglements.
There is a lot of news to go through, as is typically the case.
We will start in New York, which definitely has the biggest news, and the big news is
the ruling.
If you missed it, the judge ruled that basically, yeah, they're liable.
They are liable for the fraud.
So the revoking of the certificates, that has been called the corporate death penalty.
What he is looking at when it comes to canceling the ability to do business in New York, it's
huge.
It is huge.
And this would cancel the certificates that allowed the Trump Organization and all of
its trusts, its LLCs, everything.
It stops them from being able to function in the state.
It's huge.
When it comes to the houses owned by the companies and whether they would also be subject to
the ruling, the judge said, I'm not prepared to make a ruling at this time on that topic.
So at the moment, it has been left on the table.
But the judge has extended the deadline to suggest receivers to 30 days.
It was initially 10.
Now, all of this news means that the trial, which is slated to start October 2nd, is likely
to be much shorter than originally anticipated because the key element was decided in the
summary judgment.
So I think the original plan was for it to start on October 2nd and it might last until
just before Christmas.
It will probably wrap up much before then.
Now, in a very bizarre turn of events, Trump World is super upset that the judge is using
the assessor's evaluation of Mar-a-Lago and seems to be casting a false narrative surrounding
it.
Trump World is kind of putting out a bunch of information that doesn't really add up,
it doesn't really make sense.
The judge did not make a valuation of the club.
That was done by the tax people down in Florida, from my understanding.
It might do Trump world some good to remember what that valuation was in 2020, I think.
And if I'm not mistaken, I believe that the Trump team appealed that evaluation, saying
that it was too high.
That might be something that they want to remember as they start making statements about it.
The crux of it is that they feel it was valued way too low.
Which with everything going on in this ruling and what is happening in the New York case,
the fact that they're even remotely concerned about the valuation leads me to believe that
they may not actually understand the situation.
This is a quarter of a billion dollar suit.
They might be focused on the wrong things here.
Moving on to Georgia, Trump has reportedly been asking his team to come up with proposals
for different ways to suppress the now infamous phone call in which he was asking about finding
some extra votes or whatever.
This claim has, of course, been denied by his legal team.
This is the phone call that Trump described as perfect, I believe.
That move, trying to get that suppressed, I understand the humor in it, given the fact
that Trump repeatedly said that it was perfect and there was nothing wrong with it and all
of that stuff. At the same time, from a legal perspective, I would expect them to start
doing a whole bunch of things that make their client not look great and maybe undercut some
of the things that he has said in public because they're trying to defend him. That phone call,
Yeah, it would be great for the Trump team if it was somehow suppressed and not admissible.
How they are going to attempt to manage that, that's anybody's guess.
Georgia jurors will have their identities kept secret.
Some media organizations are petitioning to be able to report demographic information
about the jury, but I haven't seen any, I haven't seen any outlets actually fighting
this aspect of it because I think they, I think they understand the situation.
The DA in Georgia says threats against her are a waste of time.
And she went on to say, in this job and as a woman in general standing in this position,
threaten me is a waste of time. The interesting thing about this is that when you just see that
quote you would think that it would be directed at Trump's base and people who might be calling
her and stuff like that. It certainly appears to be directed at House Republicans who have expressed
interest in looking into how she does things. I don't think that that's gonna
go over very well. In some kind of outside news that is probably going to
come up later, Cassidy Hutchinson, her book says the wife of Mark Meadows
complained about the cost of dry cleaning and the dry cleaning was to
remove the smell of smoke from his clothes. It's reported that Meadows would
burn documents in the fireplace in his office. We'll probably see that
material again. Now on to the documents case. Trump's Presidential Records Act
defense is going up and smoke faster than anything in any fireplace.
Experts are beginning to talk about how using the act would require disregarding major portions
of it and allowing Trump to just pick and choose what parts of the act would apply at
any given time.
That's not how things work.
That's not something that would be feasible in a courtroom.
And it has to be remembered, or it should be at least acknowledged, that under the Act,
reports that were generated by, I don't know, hypothetically speaking, an intelligence
agency, those would be considered agency records, not personal records, which would really cause
issue for the former president. It doesn't help. Okay, moving on to the D.C. election
case, the interference case. The judge has denied Trump's recusal request. Who didn't
see that coming? If you missed it, Trump basically said that the judge needed to recuse herself
because something about her parents
being communists or something,
there was a whole bunch of stuff.
And it was thin.
It was thin.
Nothing that I saw would indicate an inability
to preside over the trial.
Apparently the judge feels the same way.
Trump's team is fighting the proposed gag order.
They filed arguing that the prosecution can't show how Trump's statements would provoke lawlessness.
I'm sorry, I summed out there. I was thinking about this documentary.
I watched about this thing that happened on January 6th.
Anyway, that was one of the arguments.
The other was that the real reason for this proposed rule was because Biden wants to silence
Trump. And another one was that being talked about by Trump really wasn't a bad thing because it
helps with their book sales. I'm paraphrasing. To me, the arguments were not great. They weren't
very convincing to me. I am not sure how the the judge will look at them.
Now some wider ranging elements to Trump world and everything that's going on
right now is that there might be a probe occurring
looking into Trump's political operations and the payment of lawyers
for potential witnesses or co-defendants and how that might create issues.
I would imagine that is something we will see come up again probably pretty quickly.
In Michigan, some of the fake electors are trying to get their cases dropped after the
The attorney general said that the defendants were brainwashed in that, quote, they legit
believe that when talking about like the election being taken and all of that stuff.
Now the AG said this outside of a courtroom setting and all of that, however, it's the
attorney general and the attorney general in these comments kind of saying that the
intent may not be there.
We're going to see that again guaranteed.
That's going to come up probably a lot actually.
This is something that's probably going to help the defendant's case.
How much it's going to help them is yet to be seen.
But that comment, when you are looking at a lot of crimes, the intent matters.
And if they were brainwashed, if they did legit believe that, it puts them in a situation
where they may not be criminally responsible under the law.
Now whether or not the Attorney General's statements actually amount to that being a
professional opinion or legal opinion or something like that that would matter in a courtroom,
that's yet to be seen.
But it coming out and it being said, you're probably going to see most, if not all, of
the defendants try to use this statement in some way.
In another element to what's going on, and especially when it comes to Trump talking
about potential witnesses, uh, Trump appears to have suggested General Milley may have
committed a capital offense and said some things that will probably come back to, uh,
to cause problems for the former president.
Um, I've been asked a couple of times since it happened why I didn't comment on it because
it's normally something that I would talk about, especially the way Trump
phrased it. There's two reasons. One, at this point we can expect the former
president to become more and more and more erratic. His statements are going to
get even wilder than that one. And so this is just kind of the beginning of it. I
I don't necessarily see this as huge, aside from that, I don't think Milley needs me to defend him.
General Milley has been around quite some time and it is worth remembering that the former president
already went up against a bunch of guys with long tabs back in January of some
year.
I can't remember.
He didn't win.
We see how that has turned out for him.
I don't believe that Trump initiating any kind of PR battle with General
Milley, is a good idea.
I understand that for a lot of people on the right, Milley has
become a person they feel that they can go after and talk bad
about, and it'll get them good social media engagement and get
them those clicks that they think eventually convert to
votes despite all evidence to the contrary, I don't believe that that's
necessarily true. I think that when they go after Milley, people like Milley, I
think it hurts them far more than they understand. And again, Milley's got one of
those long tabs on his shoulder.
I don't think that the general needs, you know, help from people on YouTube.
I think he's got this.
Um, so that is going through, uh, the, the recent developments in Trump world and
all of the entanglements to the former president has found himself in there is
probably going to be a whole lot of news in the next week or two when it comes to Trump's
legal proceedings.
And the larger news we'll talk about over on the main channel and then the procedural
stuff we'll get to here.
The New York case, it has the potential to be absolutely devastating to the former president
and those around him.
Based on what they seem to be tweeting about and what they appear to be most concerned
with, I don't think they understand that yet.
I do not think that they understand the gravity of the situation that came through with that
summary judgment.
It can go really, really bad for Trump from here.
The financial aspects of this are huge.
potential liability is huge. It's much bigger than I think, I think the Trump
world understands. Now maybe they are still convinced that somehow they're
gonna get out of it or maybe they'll win on a pill or something like that, but
Realistically, the president is going to have some severe financial issues coming up.
The business side of Trump world is about to be shook pretty hard.
Based on the social media commentary, I don't think they get that yet.
But we'll have to wait and see what happens over the next couple of weeks, but the potential
liability from that ruling, it's definitely bigger than either they know or they want
their base to know.
I can't tell which.
So that's everything.
That's the information you might have missed over the last week or so when it comes to
Trump and how everything is progressing.
And having the right information will make all the difference.
Y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}