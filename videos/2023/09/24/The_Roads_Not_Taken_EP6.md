---
title: The Roads Not Taken EP6
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=e6XCHOJ1eQ8) |
| Published | 2023/09/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces episode six of "The Road's Not Taken," a series where he covers under-reported stories and provides context.
- He starts off discussing foreign policy, mentioning the F-35 aircraft and NATO's tactics to decentralize aircraft for survivability.
- Beau criticizes the missed opportunities of the current Speaker of the House, McCarthy, regarding Zelensky's visit to the US.
- He talks about indigenous land rights in Brazil being enshrined by the high court, anticipating future conflicts due to resource interests.
- Beau analyzes confusion surrounding open-source data on Russian conscript loss rates, noting Russia's use of less trained personnel in combat.
- He mentions a situation in South Korea where US military installations were raided, leading to investigations of US service members for involvement in a drug ring.
- Beau reports on Bulgarians accused of spying for Russia in the UK, hinting at significant espionage activities.
- He comments on Russia deploying elite paratroopers as regular infantry, showcasing a lack of appropriate personnel.
- Beau shifts focus to the United States, mentioning an investigation into misconduct allegations against Baton Rouge cops by the Civil Rights Division.
- He notes the expansion of the United Auto Workers Strike and Biden's potential visit to support the union workers.
- Beau touches on Trump's promise of mass deportations if re-elected and the importance of not blaming those with less power for problems.
  
### Quotes

- "He lost the opportunity to have Republicans standing beside Zelensky as he talked about how vital that relationship with the United States is."
- "Nobody who has less institutional power than you is the source of your problem."
- "Shovels can be incredibly significant and useful, especially in situations you might not think about it."

### Oneliner

Beau covers foreign policy, missed opportunities in US politics, indigenous land rights, Russian conscript confusion, and South Korean investigations.

### Audience

Policy influencers, activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for indigenous land rights protections (suggested)
- Support unions and workers' rights by joining local labor movements (implied)

### Whats missing in summary

The full transcript provides in-depth insights into global events, geopolitical strategies, and domestic political dynamics, offering a comprehensive analysis of underreported stories and missed opportunities in foreign policy and US politics.

### Tags

#ForeignPolicy #UnderreportedStories #PoliticalAnalysis #LaborRights #Geopolitics


## Transcript
Well, howdy there, internet people, it's Beau again,
and this is episode six of The Road's Not Taken.
This is a weekly series where we go through
the previous week's events, and we go over stories
that were either under-reported or maybe they got coverage,
but the emphasis wasn't there to explain,
hey, this is context and you're gonna see it again,
or it got coverage but it didn't get coverage on my channel
want to make sure that the information is out there. And then after that we go
through a few questions from y'all. Okay so we are going to start off with foreign
policy as we always do and we are starting off with some news about the
F-35. The F-35 is NATO's new generation of aircraft. In a world's first two
Norwegian F-35s landed on a highway in Finland. This is demonstrating a tactic
used to decentralize aircraft in the event of a conflict and increase
survivability. And as I say that, I realize I should probably rephrase it. If a
conflict was to emerge with a near peer or a country that believes they're a
near a pier, you don't want all of your aircraft pinned at airfields. You want to
be able to decentralize them to provide, to make it harder for the opposition to
find and hit them all. This tactic, it's been used a lot. It's never been
demonstrated with an F-35 though, and that has now been accomplished in part
with, you know, newer member to NATO. Meanwhile, here in the United States, well,
we lost one. So there's that. In what is becoming a trademark move for the
current Speaker of the House in the United States, McCarthy missed another
golden opportunity. Zelensky came to the United States and instead of taking
a foreign policy victory lap, not just for the United States, but also for the
Republican Party, McCarthy turned him away. He wanted to talk to the House and
McCarthy was like, no. He lost the opportunity to have Republicans standing
beside Zelensky as he talked about how important that relationship with the
United States is. Historically, the Republican Party was actually instrumental
in helping to foster the relationship that exists between Ukraine and the
United States. The current posture that the Republican Party has, at least some
of the Republican Party, that's due to Trump.
Historically speaking, the Republican Party was very involved in it.
And this could have been an opportunity to kind of bring it back around and get the
Republican Party realigned with what the majority of Americans want.
He could have had Republicans standing there, as Zelensky said, that U.S. support, quote,
saved millions of Ukrainian lives, which is something that Zelensky later said.
McCarthy dropped the ball here.
The current posture of the Republican Party concerning this, it's going to come back
to haunt them, especially as they continue to push narratives about Biden and all of
this stuff and they're using that footage and, you know, I said fire him and all of,
you know, all of that stuff.
If you are not aware, at that time there was a thing called the Senate-Ukraine Caucus,
whole bunch of Republicans in it.
They were instrumental in making that happen.
was kind of doing what they wanted. And this was a moment for McCarthy to readjust and
realign the Republican Party and missed it. But I mean, I guess he has a lot on his mind
right now. Okay, so moving on, indigenous people in Brazil had their land rights enshrined
by the high court. This is a huge win for them and it's also something you're probably going to see
again because just like in the United States, well that land, there's stuff under it, there's stuff on
it that other people want. So this is going to keep coming up. Okay, there was some analyzation
of some open source data concerning the loss rate of Russian conscripts and it has led
to some confusion.
It is widely being reported right now that draftees last about four and a half months.
That's, I think it's a legitimate mistranslation, I don't think that this is people intentionally
putting out bad information, I think this is them not understanding what the data actually
those.
That data was not designed to show an expectancy for conscripts.
What it was showing was it was taking a look at those who were known to be lost and seeing
how long it took for that to happen.
It wasn't comparing it to conscripts who weren't lost.
What a lot of people think this shows, it wasn't even designed to show.
They were working with a very limited data set and they were doing the best they could
with that data set.
The only thing that you can really pull from this is that Russia is doing what everybody
suspected as far as putting less than adequately trained people on the lines.
That's really the only conclusion you can pull from this.
The expectancies, that's not what this data would show.
And if it was, if that data was there and they were trying to show that, it would probably
actually be less than this.
where Russia isn't very forthcoming concerning its losses.
Okay, so in South Korea, South Korean law enforcement officials rated some U.S. military installations.
And they are investigating more than a dozen U.S. service members, has to do with a drug
ring.
Now, generally speaking, the United States, they would kind of look at this as, well,
we're guests in that country, and that's how they should look at it.
However, with everything going on right now about people being overseas and all of that
stuff, the U.S. might get weird.
If the US tries to intervene, it's probably going to stress things a little bit.
It's not going to lead to a real deterioration of the relationship or anything like that,
but there will be some frosty moments.
But we don't know that that's going to happen yet.
Okay, so in the United Kingdom, five Bulgarians were accused of spying for Russia.
They are set to be charged with conspiracy to conduct espionage.
Now the information that has come out, there's only so much.
Their laws work a little bit different.
probably won't get as much information. I know there are some people in the
United States who are going to want to look into this and they're going to be
upset about that. I would point out that, you know, they don't have a prime minister
with a bathtub full of classified documents, so I don't know that we have a
lot of room to criticize how they handle it. The information that has come out,
what it has kind of led me to believe that this was a real intel cell that
they were trained but that's at first glance we'll have to wait and see what
occurs as we get more information okay so also news related to Ukraine Russia
Russia has put some elite paratroopers on the line as regular infantry, deployed them
in that way.
That indicates a lack of the appropriate personnel to fill the role.
They're having personnel shortages.
It's a waste of those resources to use them in that manner.
It also might indicate that they don't plan on going on the advance anytime soon.
But it shows a lack of core professional infantry and a lack of faith in the conscripted infantry.
So they have that lack of faith with the conscripts.
They don't have the professional core infantry to reinforce them.
So they use what they see is the next best thing.
Now moving to the United States.
In completely unsurprising news, the Civil Rights Division, the Feds, they have opened
an investigation into Baton Rouge's Brave Cave, is apparently what that warehouse was
being called.
If you missed it, there are some misconduct allegations involving Baton Rouge cops.
And they are significant, and I have a feeling that we don't have all of that story yet.
There's going to be more.
The United Auto Workers Strike is expanding in different ways, and there is reporting
to suggest that Biden is going to go and visit the workers on the picket line.
That's a pretty big step for a sitting president to take.
It will definitely signal his support to the union.
It's probably going to make the bosses mad.
I cannot remember a situation in which a sitting president joined a union on the picket lines.
Now on the other side, Trump is promising mass deportations if elected back to office,
you know, because it worked so well the first time.
Just as a general reminder, please remember that nobody who has less institutional power
than you have is the source of your problem.
It's not going to be people that politicians are telling you to kick down at.
It's going to be the politicians who are telling you to kick down.
In more news about Trump, there is reporting that suggests he is concerned and he has been
asking his advisors about prison, wondering if he will go to a nice one or a bad one or
if they'll make him wear a jumpsuit type of thing.
That reporting is one of those things that might indicate that he's finally starting
to understand the gravity of the situation.
3M reached a settlement.
It is a $6 billion one about hearing loss.
If you were in Iraq or Afghanistan and you used that air protection, check it out.
Generally speaking, when you are talking about giant suits
like this, the payouts, they're like five bucks.
From what I've seen, the typical payout
is supposed to be about $25,000.
So if you are covered, you might want to look into this.
There are indications that Tesla will
be a huge target to be unionized. Tesla has been pretty successful at resisting
unions. It's worth noting that there are some new NLRB, which is the the
government entity that oversees unions and stuff like that. There are some new
rules that might make unionization more likely.
The U.S. Senate has abandoned its dress code, and this is super troubling to a whole lot of people.
As you listen to people complain about this, listen to the latent classism and elitism in it.
It's definitely there.
You know, right now Congress, I mean, they don't even have a budget.
know why they should have to dress for work when it's pretty clear they're not working.
There is a teen in Texas who is reported to have been suspended for having locks.
It happened the same week as the Crown Act took effect. The school is arguing that he isn't being
punished for having locks or being black or anything like that, but instead it's over the
length of his hair." I would point out that Texas education and schools
consistently rank pretty poorly in national rankings. Maybe a preoccupation
with hair length has something to do with that. There might be more important
things that the administration should be worried about.
More Trump news, and this is in yet another case.
So Trump filed a suit against Michael Cohen.
Many people have said that it was frivolous, that type of suit, a nuisance one is how it's
being characterized in a lot of ways.
In a poetic turnaround, it appears that Trump has been ordered to sit for a deposition,
not to exceed nine hours.
I have a feeling that something in that courtroom did not go the way he planned.
Rudy Giuliani's lawyers, they have filed suit for more than a million dollars in unpaid
legal bills. We also found out some more about Rudy via Ms. Hutchinson. Just Google it.
Trump reportedly did not want to wear a mask during the pandemic because it messed up his makeup.
Yeah, and then continuing something from last week, Drew Barrymore has rethought
her position when it comes to restarting her show during the writer strike. She
has decided that that is not something she is going to do. Okay, now on to some
cultural news, Bob Ross, the famous TV painter and sometimes plush toy that
appears on the shelves behind me, he began the joy of painting back in 1983.
You can now own the first painting he ever did on that show if you have about
10 million dollars because it's for sale and the going price is a little less
than 10 mil. I would like to remind everybody that that was a painting that I believe he did
in about 30 minutes. Okay, so environmental news. The Biden administration announced the
American Climate Corps and it is an organization that is designed to train young people in high
demand skills for jobs in a clean energy economy. There's going to be a whole
video on this next week over on the other channel. If you're interested in
getting involved, the sign up is whitehouse.gov slash climate corps.
There's an S at the end of that. Okay, the Antarctic sea ice reached another
record low for the winter season. It is the third time in seven years the record
has been broken. That's not good. California is suing the oil companies BP,
Exxon Mobil, Chevron, Shell, and ConocoPhillips along with the American
Petroleum Institute over allegations that they deceived the public concerning
the damage that comes from fossil fuels and damaging natural resources. There are
other jurisdictions that have filed suits like this, but California is
definitely the biggest. Okay, now we're going to go to some odd news. There have
been some rumors about a sub that the Chinese military might have lost in the
waters near Taiwan. They are rumors. They haven't been confirmed. Adding fuel to
that speculation is the absence of China's defense minister, disappeared for
a while. And there is also some reporting to suggest that he is or was under investigation.
A little bit more odd news was Donald Trump Jr.'s Twitter account was hacked. The tweets
were unusual. One of them claimed his father had died and that he was running for precedent
instead. Okay, so moving on to the Q&A portion. Can you explain why sometimes
YouTubers don't talk about things they should? I guess I mean there are topics
that I know you or another YouTuber care deeply about, but you don't talk about
it. It seems like you should. I'm going to guess what you are witnessing is ethics.
There's no explanation in this about what the topics are, but there is a topic
that most channels that are like mine have been talking about lately and I
haven't been covering it. If you know people involved in a story, you shouldn't
cover it. There's, if you can't talk about something objectively and set that
aside, it's not something you should cover. So this is actually important
because I know a number of, I know a number of people on YouTube who do this
and they don't announce it. They just don't cover something. It's not that they
don't care in most cases, it's that they care about that a lot to the point where
they can't set their bias aside. And it's not something that even if they were to
disclose their bias. A good example, on the main channel when I am talking about
about the Kurds, I will often have Kurdish apparel on
or in the background, or I will flat out say,
I'm incredibly biased.
But there are times when even when you do something
like that, it's not enough to safeguard your coverage.
And I can see the other person that's here doing that.
that's the only thing that I can kind of make out of this.
Okay.
I think two of the most important emergency tools
are the pickaxe and shovel.
In terms of homesteading projects,
I feel like root sellers are underrated.
Do you agree or disagree?
I don't say pickaxe and shovel,
But if you go to the survival video that's on this channel,
you'll see an e-tool, which is the same thing.
Shovels can be incredibly important and very useful,
especially in situations you might not think about it,
and like fires.
Now, as far as root cellars, I don't
have a lot of adult experience with those.
We can't have them here.
If you dig something like that here, you just get mud.
The water's right below the ground.
So that's not something that's really feasible here.
But from what I remember of Tennessee and Kentucky, yeah.
That was something that was pretty well used.
What is that weird blue drink you're
drinking in some video. It looks like an olive oil bottle. That's water. That's
water. I had a friend come into town and the friend likes alkaline water. I ordered
to, because that's not something that you can just go pick up at the store here, I
ordered some and had it delivered but it took so long to get here. They were
already gone. So that's that's what that is. It comes in a weird blue bottle that
does it's a square. Yeah I know what you're talking about. Okay this one has a
note from the team and it says this came in after we built the question lists for
the 100k roadshow but we want to know what you would say and the question is
I'm what you would call a tech bro I need a new project give me some
inspiration. A project for a tech bro. I think a just criminally underrated movie is a movie
called Things to Do in Denver When You're Dead. This is not a movie you can watch with
your kids. In it one of the characters is trying to like get away from being a
criminal and the idea what they were trying to build was a thing where people
who were terminally ill or just aging could record VHS tapes, I think, and they were prompted
by a series of questions and they would record the answer to those questions and then their
family members could come in and get the advice from their loved one later on.
It didn't work out well in the movie, but I'm convinced the theory is sound.
I actually think that that might be something that people would do, especially if it was
something where they knew that information would be accessible for
years and years and years. There you go. I think that's probably a whole lot
better than trying to, you know, take clips of grandpa and use AI to guess
what they would say or something like that. Just have the the prompts and go
from there. I don't know that's the first time I've ever been asked to pitch a
tech product at all much less in like with like 30 seconds lead time. Okay so
that's it that's that's what we're ending on I guess. So there's some context
information and a glimpse of what some of the news cycle is
going to look like next week.
And it will provide that background information.
And having the right information will
make all the difference.
Y'all have a good night. Thanks for watching.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}