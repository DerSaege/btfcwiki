---
title: Let's talk about Trump leading Biden in a poll....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NCAR4ORUY24) |
| Published | 2023/09/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a recent poll showing Trump with a 10-point lead and the potential impact.
- Acknowledges that the polling may not be accurate, citing discrepancies with other polls.
- Points out flaws in current polling methods, such as reaching only a specific demographic.
- Emphasizes the uncertainty of predicting election outcomes based on early polling.
- Raises scenarios like economic changes or legal issues that could drastically alter election results.
- Suggests caution in taking polling results too seriously due to their limitations and potential inaccuracies.
- Notes the potential for polling to become even less representative in the future.
- Raises questions about the timing and implications of the poll results within the political landscape.
- Considers the possibility of the poll being an outlier rather than a true reflection of voter sentiment.
- Encourages skepticism towards polling until methods improve to reach a more diverse sample.

### Quotes

- "Polling this far out when it comes to how people are going to vote, it means nothing, absolutely nothing."
- "I wouldn't worry about it too much, other than this is going to give the Trump campaign huge amount of ammunition."
- "There's a whole lot of things that will happen between now and the election that might alter the results of this poll."
- "I cannot imagine a world in which Trump beats Biden by ten points in the general."
- "I'd be very cautious putting too much stock in polling at this point until pollsters can figure out how to reach the people who are not answering their phones."

### Oneliner

Beau explains the limitations of early polling and advises caution in interpreting results due to inaccuracies and demographic biases.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Wait for more accurate polling methods to be developed (implied)

### Whats missing in summary

The full transcript provides detailed insights into the unreliability of early polling and the potential impact of inaccurate data on political narratives.

### Tags

#Polling #Trump #Biden #Election #Accuracy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about polling
and Biden and Trump and a huge lead for Trump
and what it all means because a whole bunch of questions
have come in because a whole bunch of outlets
are pointing to a poll.
So if you missed it, what does the poll say?
It says that Trump has like a 10 point lead.
That's substantial, that's big.
So we're gonna talk about the impacts
that first before we actually talk about the poll because what I'm gonna say
about the poll doesn't actually change the effects for this part. The Trump
team, oh they are gonna use this. One of the big things right now when you're
talking about Republicans is there are a lot of them who believe Trump can't win
a general. This shows that he can. Look at this poll, and Team Trump is going to
use it for that. They're going to try to cement their already very strong
position in the Republican primary. Now, is this a case of the Trump team
embracing that post-truth world? Kind of, but every politician would use it for
this. So this isn't something you can actually slam the Trump team for. Any
politician in this position would be using this poll for the exact same
thing. But, as you might have just picked up on, the polling is probably not
accurate. The poll, part of it was done by the Washington Post, who says that the
quote, sizable margin of Trump's lead in this survey is significantly at odds with other public
polls that show the general election contest in a virtual dead heat. The difference between this
poll and others, as well as the unusual makeup of Trump's and Biden's coalitions in this survey,
suggest it is probably an outlier. They're saying that it's it's probably a bad poll.
Now, when you have that kind of notification, that kind of basically disclosure, you should
probably take it seriously, but we need to point to some other things about polling lately.
they haven't really been great. It was polling like this that led a whole bunch
of people to believe there was a red wave coming, one that never reached
shore. Why? Because polling right now is basically done of people who will pick
up the phone to an unknown number. That's not a behavior that is incredibly
common among, not even young people, I mean people my age and down.
Don't do it.
So you're not getting an accurate representation when the poll was taken.
I also want to say that this was done of just a little bit more than a thousand people.
Not great representation there.
There's also questions about the order of the questions and how they were asked and
all of that stuff, which to me, yes, it matters, but the whole phone thing is a much bigger issue.
And then you come to the final and most important part, polling this far out when it comes to
how people are going to vote, it means nothing, absolutely nothing.
There's a whole lot of things that can happen between now and then.
What happens if the economy crashes?
Well, Biden loses.
Biden loses, unless it crashes because of a government shutdown that people blame on
the Republicans.
If it rebounds, becomes incredibly strong and everybody's feeling it, well, then Biden
wins.
What if Trump is in custody?
There's a whole lot of things that will happen between now and the election that might alter
the results of this poll.
I wouldn't worry about it too much, other than this is going to give the Trump campaign
huge amount of ammunition to placate people who are concerned about the
electability in a general election so he can get through the primary. Given the
fact that people, Republicans, are kind of turning against him when it comes to
not showing up to the debates and it's entering that phase where people are
start dropping out really quickly. If I was somebody who was conspiratorial, I might think
that this was somebody who maybe wanted Trump to get through the primary because he would be easier
to beat in a general. But again, there's no evidence of that. I just, it's one of those timing
things. Like, yeah, that's odd. Especially given everything else that's going on.
Now, realistically, is that the likely scenario? No. The likely scenario is that it's a bad poll.
It's an outlier. As they said, I cannot imagine a world in which Trump beats Biden by ten points
in the general. I can't imagine a chain of events that would lead to that. Your
most likely answer is what they said. It's an outlier poll. But I would be very
cautious putting too much stock in polling at this point until pollsters
can figure out how to reach the people who are not answering their phones. They
They are going to become less and less accurate because the number of people who don't answer
unknown numbers is going to continue to increase.
So the polling will become less and less representative.
Anyway it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}