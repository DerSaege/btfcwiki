---
title: Let's talk about unanswered calls, Menendez, and traditions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xwGUU_MbH6I) |
| Published | 2023/09/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Bob Menendez from New Jersey has been indicted on bribery charges, leading to calls for his resignation from the Democratic Party.
- Menendez has decided to step down from the Senate Foreign Relations Committee but not resign from the Senate itself.
- This situation echoes a new tradition in Congress where politicians facing indictments give up committee assignments rather than resigning.
- Previously, the tradition demanded resignation upon indictment, but now it seems to have shifted towards relinquishing committee roles.
- The change in this tradition may have occurred in the last six years during an administration that openly disregarded laws and norms.
- The current leniency towards indicted politicians suggests they are not held to the same standards as the general public.
- The Democratic Party is likely to intensify calls for Menendez's resignation as legal proceedings progress.
- Politicians being held to lower standards than the average citizen historically leads to negative outcomes for countries.
- Accepting that elites are above the law can result in rapid deterioration of a nation.
- The lack of accountability for politicians sets a dangerous precedent for the country's future.

### Quotes

- "They're almost not expected to be. They're not expected to be held to the same standards."
- "Those countries that openly accept the idea that their betters truly are better and they can go by a different set of laws, it doesn't go well for that country."

### Oneliner

Senator Menendez indicted on bribery charges sparks calls for resignation, unveiling a new trend in Congress where indicted politicians relinquish committees instead of resigning, raising concerns about accountability and a dangerous precedent for the nation.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Advocate for stricter accountability measures for politicians (implied)
- Stay informed about politicians' actions and hold them accountable (implied)

### Whats missing in summary

Further insights on the potential consequences of allowing politicians to operate under different standards than the general public.

### Tags

#SenatorMenendez #BriberyCharges #PoliticalAccountability #Congress #Indictments


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the Senator
from New Jersey, Robert Menendez.
We are going to talk about calls
and calls going unanswered.
We're going to talk about what appears
to be a new tradition in Congress.
And we'll talk briefly about where that tradition started.
Okay, so if you have missed the news,
The senator from New Jersey, Bob Menendez, he has been indicted on charges, allegations
related to bribery, and they are expansive allegations.
Almost immediately, the Democratic Party called for his resignation, Menendez is a Democrat.
They called for him to resign.
And he's made it pretty clear that he's going to step down from the Senate Foreign Relations
Committee, but he's not resigning from the Senate.
Just giving up his committee assignments.
Undoubtedly that's going to bother some people on both sides of the aisle.
At the same time, doesn't that sound familiar?
I feel like this just happened.
I feel like there was another person in Congress recently who was indicted, faced calls to
resign, and then really just kind of gave up on their committee assignments.
That appears to be the new tradition.
The older tradition, for the most part, when it came to Congress and indictments, was basically,
it's cool if you're getting investigated, you know, stuff happens, but if you get indicted,
need to resign. Now it looks like the tradition is, well, you give up your
committee assignments. I would like it noted that there's protocol and
tradition for Congress people being indicted. That in and of itself is
probably commentary, but you have to wonder how that change occurred. I'm
going to suggest it happened within the last six years or so. It happened during
a period in which there was an administration that openly flaunted the law.
That when they were told they were breaking the law, that doesn't matter, that doesn't
apply to us, we're the elite.
And if DOJ says something about it, well, we just go after them, politicize the matter.
like that's what happened. It seems like that that is what happened. Now my guess
is that the Democratic Party is going to be a little bit more, a little bit more
zealous in calling for the senator's resignation. I don't think that those
calls are over. I don't think the first few that went out are going to be the
end of it especially as the proceedings start to move forward. But at the same
time we have now entered a period where politicians aren't just in a second tier
of a criminal justice system where they aren't really held to the same standards
as everybody else. But they're almost not expected to be. They're not expected to
be held to the same standards. And generally speaking, that's a really bad
thing. When you look back through history, those countries that openly accept the
idea that their betters truly are better and they can go by a different set of
laws, it doesn't go well for that country. Things tend to deteriorate
rate pretty quickly.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}