---
title: Let's talk about Taylor Swift's fans showing up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=V-YOYe_XM_U) |
| Published | 2023/09/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Taylor Swift's social media post directing people to vote.org led to tens of thousands registering to vote, averaging 13,000 users every 30 minutes.
- This resulted in a 23% increase for National Voter Registration Day, all from an Instagram post.
- The post also caused a 115% increase in 18-year-olds registering to vote, signifying a substantial impact.
- Many younger people seem unhappy with the current state of things and are motivated to make a change.
- Beau stresses that registering to vote is just the first step, and it's vital to show up and vote when it really matters.
- He warns against letting the momentum fade and not following through with voting after registering.
- Beau urges people to understand the importance of showing up to vote to bring about the desired change.
- He mentions that sustained engagement and messaging from Taylor Swift could have a significant impact on certain races.
- Younger people, particularly those affected by current legislation, might have a higher turnout due to their friends being targeted.
- Beau expresses hope for increased youth participation in voting, given the circumstances and motivations.

### Quotes

- "It led to tens of thousands of people registering to vote."
- "Not showing up when it actually matters, when it's time to vote."
- "You have to show up. You have to do it."
- "In some places her activity alone will swing races."
- "I think that they understand that shade never made anybody."

### Oneliner

Taylor Swift's social media post led to a significant increase in voter registration, especially among 18-year-olds, stressing the importance of showing up to vote for actual change.

### Audience

Young voters

### On-the-ground actions from transcript

- Register to vote and encourage others to do so (exemplified)
- Show up and vote in elections (implied)

### Whats missing in summary

The full transcript provides additional context on the impact of social media posts by influential figures like Taylor Swift in driving voter registration and youth engagement in the political process.

### Tags

#VoterRegistration #YouthEngagement #TaylorSwift #Influence #Voting


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about voter registration
and how I found out that a whole bunch of y'all
are Taylor Swift fans.
Because as soon as the news broke,
I started getting messages.
My inbox just overflowing.
And I'm just like, man, it's 7 AM.
So what's the news?
Taylor Swift, social media post, she made a social media post.
It led to tens of thousands of people registering to vote.
They directed them to vote.org.
And it looks like at some points,
they were averaging 13,000 users every 30 minutes.
And the end result was a 23% increase
for National Voter Registration Day,
all from an Instagram post.
Maybe she should say it in a tweet.
Now, one of the interesting parts about this
is that it led to a 115% increase in 18-year-olds
registering to vote.
That's pretty substantial.
That's pretty substantial.
And I know the concern is that 18-year-olds, young people,
they won't show up, you know, because that's what people say.
I don't know that that's true anymore.
I think that there are a lot of people who are younger, who are unhappy with the current
state of things.
They are unhappy with their friends being targeted by those people in power.
But it is important to say, if you are one of the tens of thousands of people who signed
up and registered to vote, understand that's not the end of the journey.
It's just begun.
Don't let that be your next mistake.
Not showing up when it actually matters, when it's time to vote.
they're not going to help us. They're too busy helping themselves. If you want
change, you have to show up. You have to do it. You made the first step,
but that is just the first step. So an increase like that in certain areas,
particularly if it's sustained, if the messaging from Taylor Swift continues
and her audience continues to answer that call. I mean I would imagine that
in some places her activity alone will swing races. Again assuming not that they
just register but that they also show up. I think that there's going to be a
higher percentage of younger people showing up this time because again it is
their friends that are being directly targeted by a lot of this legislation so
I'm hopeful and I think that they understand that shade never made anybody
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}