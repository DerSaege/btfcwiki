---
title: Let's talk about Dallas, politics, and a change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Kwf6Q6v3jxY) |
| Published | 2023/09/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The mayor of Dallas switched to the Republican party, making Dallas the largest city in the US with a Republican mayor.
- This move in Dallas sheds light on broader political issues in Texas and the US, reflecting a deep-seated problem in American politics.
- The switch may signify the mayor's intention to run for a different office, possibly statewide, where having an "R" after his name could be more beneficial than a successful track record.
- The focus on party affiliation over policies and track record is criticized as a flaw in the American voter mentality.
- Beau questions the voters' tendency to prioritize party loyalty over other factors when electing officials.
- The importance of party affiliation over policy positions is viewed as an indictment of American voters.
- The mayor's party switch raises concerns about political opportunism and ambition rather than genuine alignment with new policies.
- Beau suggests that voters basing their decisions solely on party affiliation contributes to the current state of the House of Representatives.

### Quotes

- "If you will vote for somebody simply based off of party affiliation, nothing to do with policies, nothing to do with track record, that's an issue."
- "The focus on party over policy is an indictment of the American voter."
- "It's more beneficial to have an R after your name than a successful track record."
- "Voters are more interested in party than policy."
- "That loyalty mattering that much is an indictment of the American voter."

### Oneliner

The mayor of Dallas switching parties sheds light on political opportunism and the prioritization of party over policy in American politics, reflecting broader issues in Texas and the US.

### Audience

American voters

### On-the-ground actions from transcript

- Question candidates on their policies and track record before casting your vote (suggested).
- Research candidates beyond their party affiliation to make an informed decision when voting (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of the impact of party loyalty on political decisions and the need for voters to prioritize policies over party affiliation.

### Tags

#Dallas #Texas #AmericanPolitics #PartyAffiliation #Voters


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Dallas, Texas.
The mayor, a little bit of a change up,
what it means for the future of politics in Texas,
what it means for the future of politics in Texas,
what it says about one party,
and how it reflects a pretty deep-seated issue
in American politics nationwide.
So, the city of Dallas, if I'm not mistaken, it is now the largest city in the United States
with a Republican mayor. Was a Republican elected mayor of Dallas? No, no, no, no.
The mayor of Dallas switched to the Republican party. And you might be asking why, right?
I mean, that seems weird, but it's not.
Not if you take an incredibly satirical, cynical view of American politics.
Then it makes complete sense, and it doesn't speak well of the voters.
I'm sure there is some stated reason for the change.
I'm sure that there is a reason that Johnson, the mayor, gave for his change of heart.
But he's been in the Democratic Party for more than a decade.
I think it's unique that he's changing parties at a point in time where just a couple of
weeks ago, I said that the Republican Party in Texas might be going through
some changes, that it was going to get bumpy because of all of the infighting.
If somebody is politically ambitious, it's a golden opportunity. In fact, I
think I said if you wanted to get into Texas politics, now's the time. So it
It might be a signal that the mayor plans
on running for a different office.
Not as mayor of Dallas, but as something else.
Statewide, maybe.
And it is more beneficial to have an R after your name
than a successful track record.
That doesn't say much for the American voter. If somebody's been in politics for
as long as this person has been and generally well received by the people
that he's supposed to be representing.
It seems weird that a party change should matter that much, but it's Texas.
You need that R. You need that R after your name if you want to win a statewide election.
It's that simple.
All that says is that voters are more interested in party than policy.
If you want to know why there are issues, that's why.
That's why.
Again, I don't know that this is the reason, but I mean that's a whole lot of coincidences.
It normally takes a lot of planning to have all of those coincidences line up like that.
So it seems like that would be the reason.
won't know until the next election cycle and the mayor announces what they plan
on running for. But I feel like it's probably not going to be mayor. I could
be wrong, but all signs point to an attempt to use the entrenched nature of
of the Republican Party in Texas, and just get a position because you have an R after your name.
That's what it seems like. It would be weird to have a successful career and then switch to the
opposing party if you didn't plan on going further. It wouldn't make any good political sense.
He won as a Democrat. So to change his party and then continue with the same
policies, change the policies that got him elected, none of that makes sense. The
The only thing that does is that there are higher ambitions for the mayor.
Party affiliation in the United States, that loyalty, mattering that much, that is an indictment
of the American voter.
You know, a lot of the time on this channel, we talk about politicians and all of the stuff
they've done wrong, this is something that the average American voter needs to
own. If you will vote for somebody simply based off of party affiliation, nothing
to do with policies, nothing to do with track record, that's an issue.
That's why the House of Representatives looks the way it does right now.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}