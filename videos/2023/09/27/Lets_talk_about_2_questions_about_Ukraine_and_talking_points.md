---
title: Let's talk about 2 questions about Ukraine and talking points....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zKaKdzTIIfg) |
| Published | 2023/09/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing two common talking points regarding the situation in Ukraine.
- Russia does not have the initiative and is losing territory in Ukraine.
- The casualties and amputees in Ukraine do not accurately represent the situation.
- Stories of military helmet complaints and aircraft armor study to illustrate misleading information.
- Ukraine having more amputees does not mean they are losing; it shows their ability to provide advanced care.
- Russia is not faring well in the conflict, as indicated by Putin's directive and Lavrov's statements.
- Wish-casting and misinformation are prevalent in analyzing the conflict.
- Russia is not in a winning position and will not come out of the war in a better position.

### Quotes

- "Russia already lost the war."
- "A lot of times information can be presented in a way that disregards the reality."
- "Russia is not doing well, and you don't have to take a Western commentator's word for it."
- "There is no way that Russia exits this elective war in a better position than when it started."
- "Everything that is being determined now is just waiting for Russia to realize it."

### Oneliner

Beau debunks common misconceptions about the situation in Ukraine, clarifying Russia's losses and the true impact of casualties.

### Audience

Ukrainian supporters, Conflict analysts

### On-the-ground actions from transcript

- Support organizations providing advanced care for wounded soldiers (implied)
- Stay informed and combat misinformation in your circles (implied)

### Whats missing in summary

Importance of staying vigilant against misinformation in conflict analysis.

### Tags

#Ukraine #Russia #Conflict #Misinformation #Casualties #Analysis


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about two talking points
that are making the rounds,
and they are getting way more traction than they should,
because both of them are pretty easily kind of discounted.
And one happens to be something
that just really gets under my skin,
because it's the type of talking point
that gets thrown out by people
who present themselves as experts on foreign policy
or conflict that quickly shows they are not.
Okay, so two of them, and this to the person
who sent this, I am not picking on you in any way.
These are two talking points that are very common
and your message just happens to have them both.
I notice Bo never mentions how much land Russia is holding.
20%. Also, casualties are never mentioned and how many Ukrainian amputees? Okay, yeah, 20%. Sure,
we'll just roll with that. I don't know if that's accurate, but it doesn't matter. You know another
way to say it, Russia has half what they started with. Russia does not have the initiative. They
are losing territory, not gaining it. They're not holding anything either that they're losing it.
And once again, in the greater context of a war, Russia already lost the war.
Russia already lost the war.
The fighting is something else.
But now let's go to the casualties amputees thing, because this is one of those things
that really bothers me.
two quick stories. A military, they adopted a new helmet. They adopted a new helmet and
the docs hated it. The doctors hated the helmet. Lodging complaints about it because
it obviously wasn't doing any good because they are treating so many more head wounds
now. The other one is a meme. It's all over social media every once in a while. It goes
through cycles, but it's a picture of an aircraft, it has a bunch of dots on it.
What happened was, during the war, there was a study commissioned to find out where they
should armor planes, and that chart, or chart like the one that's being passed around,
was created.
And they created it by like counting the holes in planes when they came back.
Now at first glance, the idea would be, well, you armor the places with the most holes.
That's where they're getting hit the most.
No, you armor the places where there aren't any holes, because those planes never made
it back to be counted.
Yes, Ukraine, they have substantially more amputees, but that does not say what you think
it says.
It says that Ukraine has a better ability to get their wounded back to advanced care.
That's what it says.
I would be willing to bet that at least 20% of Russian amputees came from Ukrainian resources.
It doesn't mean what you think it means.
The reason the docs were treating more head wounds after the introduction of that helmet
was because before then those people never would have made it back to the doctors.
A lot of times information can be presented in a way that disregards the reality and that's
a really good example of it.
You're right, there aren't a whole lot of Russian amputees but that's because they never
made it back to advanced care.
never got somewhere where that could be performed. The idea behind these talking
points is to showcase it as well Russia's really not doing as bad as it
seems. Yeah, I mean this is a lot like that article that came out saying that
there was no way Ukraine could get through those lines and it was published
on the day Ukraine was breaking through those lines. Russia is not doing well, and you don't
have to take a Western commentator's word for it. Take Putin's, because he has told the defense
minister that they have until the end of October to regain the initiative. I don't know what the
or is, maybe they get replaced, maybe, I don't know. It's also worth noting that
Lavrov made a very interesting statement, basically kind of throwing
out an off-ramp saying, you know, if Ukraine, if they wouldn't join a
a military alliance, meaning NATO, then, well, we'd leave. We'd recognize their pre-invasion borders.
That's not the move of somebody who is winning, somebody who is safe in their position,
especially since they've already established, quote, republics with
elections, quote. There's a whole lot of people who are wish-casting. They're
trying to present information that supports what they wanted to happen.
The information isn't there to back it up.
Once Russia couldn't take the country, it was going to go bad for them.
There is no way that Russia exits this elective war in a better position than when it started.
lost. Everything that is being determined now is just waiting for Russia to
realize it. A lot of wars end up this way. A whole lot. Anyway, it's just a
With that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}