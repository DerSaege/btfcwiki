---
title: Let's talk about Trump being found liable in New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=b3e0Jwk5C_I) |
| Published | 2023/09/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Major developments in one of Trump's entanglements are completely reshaping events for next week.
- The focus is on the New York civil case for $250 million where the judge ruled Trump liable for fraud.
- The judgment narrows down the trial set to start on October 2nd to focus on potential damages.
- Trump faces severe issues as business certificates were yanked, possibly leading to loss of his flagship company.
- Attorneys on Trump's side were sanctioned for frivolous arguments, with potential fallout affecting his business empire and political aspirations.
- The events of the day are devastating for Trump, making a positive resolution unlikely.
- The stress from these developments may lead to more erratic behavior and statements from Trump.

### Quotes

- "In defendant's world, rent-regulated apartments are worth the same as unregulated apartments."
- "Trump found liable for fraud, right? You're going to see that everywhere."
- "This may be something that really starts to cast a shadow on any political aspirations he may have."

### Oneliner

Major developments in Trump's New York civil case ruling him liable for fraud reshape upcoming events, potentially devastating for his business and political aspirations.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Monitor updates on the case and its impacts on Trump's future (implied).

### Whats missing in summary

Analysis of the potential long-term consequences beyond business and political aspects.

### Tags

#DonaldTrump #NewYorkCase #FraudRuling #LegalImplications #PoliticalImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the former president
of the United States, Donald J. Trump,
and one of his many entanglements.
There were some major developments today in one of them,
and we're going to talk about those
and how they will impact the events of next week
because it's completely reshaping them.
If you are trying to keep track,
this is regarding the New York situation.
So not the documents case, not the case in Georgia,
not the case in D.C., has nothing to do with Smith
or the special counsel.
This is the New York case, the civil case
for $250 million, that one.
To give you a brief taste of how it went,
I'm just gonna read something the judge wrote.
In defendant's world, rent-regulated apartments
are worth the same as unregulated apartments.
Restricted land is worth the same as unrestricted land.
Restrictions can evaporate into thin air.
A disclaimer by one party casting responsibility
on another party exonerates the other party's lies.
The judge went on to call this world a fantasy world.
The judge ruled that Trump was liable for fraud.
That's the headline, but there's way more to it.
Okay, so this is regarding statements that might have exaggerated the former president's net worth.
There were some alleged exaggerations on financial records.
That's what all of this is about.
The judge making this judgment now, making this ruling now, what does it do for the trial
that is supposed to start next week?
I want to say on October 2nd.
It narrows it a lot, what they're going to really be determining at this point.
It certainly seems like the main arguments will be about, well, any potential damages.
So this is all headline stuff.
Trump found liable for fraud, right?
You're going to see that everywhere.
But wait, there's more.
Unless I am greatly misunderstanding something, it looks like there were some certificates,
some business certificates.
They got yanked along the way.
It certainly appears that this is going to cause severe issues for the former president.
They might, the proceedings dealing with that, it might end with the former president losing
the flagship company.
Beyond that, the judge determined that some of the arguments made by attorneys on the
Trump side of things were frivolous and sanctioned to them, I want to say for $7,500.
Okay, so what does this mean?
that next week the real question is going to be about how bad this hurts Trump.
It's worth remembering that Trump has been trying to get this case dismissed repeatedly
and stating that he did nothing wrong.
The events of today make it incredibly unlikely that he pulls out any situation, any resolution
to this that could be deemed a win.
In fact, it very well may be devastating.
The damage done, it may exceed the business side of things.
Right now, most people are talking about how it's going to affect his business empire and
how badly it may or may not hurt that.
It may go even further than that.
This may be something that really starts to cast a shadow on any political aspirations
he may have.
At the end of this, the former president had a really, really bad day.
We can assume that they will try something, and there will certainly be appeals about
this, and they'll try to pull it out and mitigate somehow next week.
How they would do that, I don't have a clue.
Couldn't even guess.
I don't think they're just going to give up.
The potential fallout from this might lead the former president to become even more erratic
in his behavior and statements.
It's worth remembering that lately the former president has made a lot of errors.
He has misspoken quite a few times, and it's led some people to question his abilities
and how long he will maintain those abilities.
This amount of stress added on top of everything else that is going on, I would imagine that
that there will be a flood of social media posts, that his behavior may become more erratic.
I think that's the best way to put that.
So that's what's occurred.
That was the big news of the day.
It will probably be days before we get a real clear picture of how it's going to impact
Trump World's strategy for next week and beyond, because this is a devastating blow to the
former president.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}