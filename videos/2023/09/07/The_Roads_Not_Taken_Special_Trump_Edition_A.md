---
title: The Roads Not Taken Special Trump Edition A
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PPcF4zssqPM) |
| Published | 2023/09/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introducing a special Trump edition of The Road's Not Taken, focusing on under-reported news.
- The former president expresses an interest in testifying, although it seems unlikely.
- Updates on the Georgia State case, where Chiefs Breaux and Powell may be tried together or separately.
- The trial for lack of a better term is set to start on October 23rd.
- The DA's office aims to try all defendants on October 23rd, with a trial possibly lasting four months.
- Expectations of lots of finger-pointing during trials regarding blame and responsibility.
- Even if trials are severed, juries will hear the entirety of the alleged conspiracy.
- Employee number four in the documents case has struck a cooperation agreement with the feds.
- In the federal DC case, DOJ files a complaint about Trump's social media posts prejudicing the jury pool.
- Trump's request to delay the trial in the civil fraud case in New York was denied.

### Quotes
- "The state's position is that whether we have one trial or 19 trials, the evidence is exactly the same."
- "You're going to see that material again."
- "Decline to sign, defendants' arguments are completely without merit."

### Oneliner
Beau covers updates on legal entanglements involving the former president, from trial proceedings to defamation cases, hinting at ongoing challenges ahead.

### Audience
Legal observers

### On-the-ground actions from transcript
- Follow legal updates and analyses to stay informed about ongoing proceedings (implied).

### Whats missing in summary
Insights into the potential impact of the legal entanglements on the former president's future endeavors.

### Tags
#Trump #LegalEntanglements #TrialUpdates #DefamationCase #LegalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we have a special Trump edition
of The Road's Not Taken.
This is your first time showing up over here.
This series is dedicated to covering under-reported news
or news that didn't get covered on the main channel.
In this case, there were a whole lot of developments
when it comes to the former president,
so many that if I broke them up the way I normally do and put them on the main channel,
y'all would have nothing but Trump news for like two and a half days.
And given the fact that there is other news happening, this seemed like a nice way to
do this.
Okay so starting off, the former president said that he's looking forward to quote
absolutely testifying.
No he's not.
That seems incredibly unlikely.
I have a whole video on this coming out over on the main channel, if it's not out already
by the time you watch it.
That just seems really, really unlikely, a bad idea.
Okay, so the first hearing in the Georgia State case, now understand there's going to
be a lot of cases, keep them straight.
This is the Georgia State case.
The first hearing occurred and the judge did not allow Chiefs Breaux and Powell to sever
their cases.
So they, at this point in time, it looks like they will be tried together, but they might
be tried separately.
The judge left that open from those who didn't request a speedy trial.
I think the goal of all of this was to try to have everybody severed off into their own
trials. That way they might, if necessary as a legal tactic, be able to point the finger at other
people who weren't on trial in that courtroom in front of that jury. Doesn't look like that's going
to happen at time of filming. The speedy trial, for lack of a better term, is scheduled to start
on October 23rd. The judge said, we're certainly here, ready and willing to provide both defendants
that right, meaning the right to a speedy trial.
And we're planning to make that October 23rd trial date stick.
Okay.
So in this same proceeding, the DA's office suggested they would be ready to try all of
the defendants, all of them, on October 23rd.
The judge didn't rule the idea out but seemed skeptical of the DA's ability to do that
because it's a lot of people.
The DA's office said that the trial would take about four months and have about 150
witnesses.
The judge seemed to believe that it could take twice that long.
So that hasn't been ruled out, moving everybody up, but it kind of seems unlikely at this
point.
I had questions a bunch come in asking me if I thought the DA was really ready for that.
Yes, I do.
I believe the DA was ready as soon as the indictments came out.
I don't.
All of the lead up time where everybody was like, they're wasting time, they're wasting
time what are they doing, I think that's what they were doing.
I absolutely believe that they are ready to move forward with this case like tomorrow.
So I don't think from a DA's point of view, I don't think the October 23rd date would
be an issue, but it might be as far as the defendants preparing for the trial.
I think that would be the real issue there.
It became pretty clear during the hearing that we are likely to see a lot of finger
pointing during trials as they move forward about who's to blame and who's really responsible
for any particular act.
And at the end of it, I think that the strategy for severing the cases, it might not matter
to any way because the DA's office indicated, quote, the state's position is that whether
Whether we have one trial or 19 trials, the evidence is exactly the same.
The number of witnesses is the same.
So what that means is that even if the trials are severed, the juries are going to be presented
with the entirety of the alleged conspiracy.
My guess is that part of this was designed to break the cases up in hopes that the prosecution
would only present evidence directly related to that particular defendant, giving them
just a piece of the overall conspiracy as the DA is framing it.
That might make it easier to defend against.
That doesn't appear to be how the DA's office plans on going forward with this.
Okay, so if there was a series of trials, there would probably be a small advantage
when it comes to those who went first, because there would be sworn testimony in each trial,
And it narrows things down for the defendants who come later.
In this same proceeding, the Georgia case, the DA's office is also requesting that the
jury's identities be kept completely secret to avoid any possible intimidation.
Let's see.
Okay, a federal judge is expected to rule on Meadows and the request for removal to
federal court and that's supposed to happen soon.
That particular decision is likely to set the tone for all of the other ones to follow
and my understanding I think at this point there's already three or four more people
who have filed to attempt to get their case removed to federal court.
If the Meadows case is removed, others might have a shot.
If his case isn't removed, it seems unlikely that anybody else is going to win using this
particular strategy.
Meadows certainly is the best candidate when you look at things as a whole.
Meadows is the best candidate to have his case removed.
Don't necessarily know that it's going to work, but out of the kind of requirements,
he's best positioned to make the case.
So if he fails, realistically, the other people should just hang it up.
It's not going to work.
Meadows also has asked a state judge in Georgia to pause his case at the state level until
the federal courts make a final determination on removal.
Okay, moving on to the documents case, there were developments there as well.
A former Trump attorney made voice recordings and then transcribed them when it comes to
his time with Trump.
This is Corcoran.
There is a really interesting passage that I am certain is going to get a whole bunch
of coverage if it hasn't already at time of filming.
Corcoran, according to his notes and his recordings and everything, he says, we've got a grand
jury subpoena and the alternative is if you don't comply with the grand jury subpoena,
you could be held in contempt.
Trump.
What happens if we just don't respond at all or don't play ball with them?
Corcoran.
Well, there's a prospect that they could go to a judge and get a search warrant and
that they could arrive here.
Okay, so, I mean, obviously this is going to get a lot of coverage because of, I mean,
you know, what happened and how all of this played out.
There's definitely a comedic tone to some of this.
At the same time, it's worth noting that what happens if we just don't respond at all or
don't play ball with them, that might go to show that somebody was willfully retaining
something or attempting to obstruct.
You're going to see that material again.
Okay, this is still in the documents case.
Employee number four in the documents case is reported to have struck a cooperation agreement
with the feds.
According to what's known, agreed to testify in the classified documents case and in exchange
isn't going to be prosecuted.
It's worth noting that I don't think this person was ever charged.
I think this might have been a case of, well, look, you're either on this team or that team.
Those people, they have their names on an indictment.
Is that what you want?
Or we won't charge you at all.
Tell us what you know.
That might be how that played out.
Okay, moving to the federal DC case.
This is the election interference case in D.C., not the state one in Georgia.
DOJ filed a complaint stating that Trump's just constant barrage of social media posts
threatens to prejudice the jury pool.
This is the first step that we've seen that is really kind of Smith's team being like
you're gonna have to shut the former president up. And we'll see how it
plays out. The filing's been made, but we'll we have to wait and see how
everything progresses on that as to whether or not there's gonna be any more
restrictions on what Trump is allowed to say. Okay, in the New York case, Trump's
request to delay the trial in the civil fraud case was denied by a one-sentence, handwritten
order.
It read, quote, decline to sign, defendants' arguments are completely without merit.
I mean, I've never actually seen a judge give somebody a single-fingered salute in writing,
But I'm pretty sure that's what it looks like.
Just completely disregarding it, like, we're done with this.
We are going to trial.
We're moving on.
Deal with it.
It's a unique position.
Okay.
So, in still yet another legal entanglement, Trump is liable in the second E. Jean Carroll
case.
happens now in the defamation case. The trial that will occur in January will
now determine damages. Definitely not a good couple of days for the former
president. So in addition to all of this, there are now so many 14th Amendment
disqualification cases being pursued by different people. I can't keep track of
of them all, and I'm not even going to try, they are popping up everywhere.
So whether or not this is a tactic that's going to succeed in keeping Trump or other
candidates off the ballot or making them ineligible, that remains to be seen.
But there are a whole lot of people who seem to think it's the route to go.
I have a bunch of questions about it but I'm not an attorney and there's a whole bunch
of attorneys who seem to think that this is a definite move that might work or they're
just doing it for the fun of it.
So with all of this bad news for former President Trump, did anything go his way so far this
week?
yes. Trump did score a win. Shareholders at Truth Social agreed to give the company more time to
complete its marcher. It was very close to running out of time, which would have presented a
significant issue for the former president. So now there's more time to do that. I mean,
It's a win, but the decision was made by those people who were very supportive of the
former president and may not be aware of all of the other news that has come out that might
have impacted their decision a little bit.
So that is all of the news for like the first three days of this week when it comes to the
former president and the legal entanglements that he has found himself in.
It's worth noting and worth remembering the speed of all of this stuff, it is going to
happen more and more and more often.
It's just going to be a constant barrage because he is involved in so many cases that
are moving forward in so many jurisdictions, in so many types, that there is going to be
a constant stream of Trump-related news.
We will probably do these relatively often on this channel, On the Roads with Bo.
Mainly because I just, I don't want the main channel to turn into, you know, Trump TV covering
tire fire. So that's it. Most of this information you are going to see again.
Right now it's not, none of this is huge at the moment, but it's going to be
context for later developments that are probably going to be pretty significant.
And so it's just getting that information together. And again, having
good information will make all the difference. Anyway, you all have a good
day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}