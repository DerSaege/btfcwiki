---
title: The Roads Not Taken Special Trump Exhibit B
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=R7Mpz974Zwk) |
| Published | 2023/09/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing another episode of "The Road's Not Taken" focused on Trump's legal entanglements.
- Explaining developments in Trump's legal cases that may be overlooked.
- Analyzing the classified documents case and the implications of the judge's order.
- Detailing the definitions of top secret and secret classifications in relation to the case.
- Clarifying that despite Trump's claims, the documents are still classified.
- Summarizing Trump's legal challenges in various cases, including Georgia and E. Jean Carroll.
- Mentioning Trump's attempt to invoke an immunity defense in the defamation suit.
- Touching on Colorado and New Hampshire's positions on Trump's 14th Amendment challenges.
- Reporting on Jenna Ellis being censured by a Colorado judge for violating rules on attorney conduct.
- Updating on Navarro's trial and the potential mistrial claim related to protesters.
- Concluding with a rare win for Trump regarding access to Scott Perry's calls with other Republicans.

### Quotes

- "After all of this time, after they were exposed and out in the wild, given that they're still being treated as TS documents, it's going to be really hard for him to claim that storing them in a bathroom or on a stage was within any kind of rag at all."
- "There's always maneuvering that is occurring behind the scenes."
- "The penalty for this censure is not much."
- "Let's be honest, there's not a lot of those."
- "Having the right information will make all of the difference."

### Oneliner

Beau runs through Trump's legal entanglements, from classified documents to immunity defenses, providing key insights often overlooked.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Stay informed on updates regarding Trump's legal cases (implied).
- Monitor the developments in the classified documents case and implications for national security (implied).
- Follow the legal proceedings in different states concerning Trump's challenges (implied).

### Whats missing in summary

Insights into the potential long-term repercussions of Trump's legal battles and the broader implications for national security.

### Tags

#Trump #LegalEntanglements #ClassifiedDocuments #ImmunityDefense #14thAmendment #Censure


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna be doing another episode
of The Road's Not Taken.
This time it will be another special Trump exhibit.
I believe this is exhibit B.
We'll be running through some of the developments
in Trump's many legal entanglements
and just kinda keeping everybody up to date
on stuff that might get glossed over.
We are going to start with the classified documents case,
because there is something in that that is incredibly
interesting to me, just some language that
appeared in an order that certainly appears to suggest
that Judge Eileen Cannon has decided
that some of those documents, well,
they may still be something.
So let's run through it.
The order itself reads,
person subject to this order are advised
the direct or indirect unauthorized disclosure,
retention or handling of classified documents
or information could cause serious damage
and in some cases, grave damage
to the national security of the United States.
Well, that's interesting.
Those documents are still classified.
All of the talk, all of Trump's claims in the media, all of that, they're still classified.
The phrase grave and serious damage, those are your signs.
Let's jump to the definitions and we will go to top secret first.
Top secret refers to national security information or material which requires the highest degree
of protection.
The test for assigning top-secret classification shall be whether its unauthorized disclosure
could reasonably be expected to cause exceptionally grave damage to the national security.
Secret Secret refers to that national security information
or material which requires a substantial degree of protection.
The test for assigning secret classification
shall be whether its unauthorized disclosure could
reasonably be expected to cause serious damage
to the national security.
Going back up to that order, persons
subject to this order are advised
that direct or indirect unauthorized disclosure,
retention, or handling of classified documents
information could cause serious damage or in some cases grave damage to the
national security of the United States. Almost seems like those definitions were
sitting out when this order was written. So this was all about Trump wanting more
access to the documents. The judge also warned that unauthorized disclosure or
mishandling of this kind of stuff could quote constitute violations of federal
criminal law. Yeah, I mean that's kind of how they wound up there. Short version,
Trump can view or discuss this material in a security area approved by the CI
person the I think they have a different term for it classified information
security officer okay so even though this seems minor it certainly appears
that the judge has already determined that Trump's arguments about this stuff
being declassified by winking a nod or whatever all not true okay let's go to
Georgia. Trump and Meadows are offering to wave speedy trial if they can have
their cases severed. I don't... that's new. I don't know about that. The DA is still
saying they're ready to go. They want to go at the end of October. Try them all
together. The election interference case. Trump asked for the judge's
recusal in that case and they said that basically believe she has prejudged both
the facts pertinent to this case and President Trump's alleged culpability
and they went through and cited them some things that she had said in other
cases and stuff like that. In one part in particular they went through and
referenced this whole thing that she had said about how the people really
responsible weren't held accountable yet. They're not in the courtroom and they
use that as justification to say that she she had already prejudged Trump.
Interestingly enough she never actually names Trump. She just talks about how the
people responsible are in the courtroom. This was in another January 6th case I
I think.
The attorneys certainly believed that that's who she was referencing.
That's interesting.
I don't think that they believe she'll be unfair.
I don't think that's what this is about.
I think there's something else at play.
judge, when you look at the other cases, those involving January 6th, my guess is
that Team Trump does not want to lose in court, see him convicted, and then have
him sentenced by this judge. She does not seem to be the forgiving sort. I think
that probably has more to do with this than anything else.
Okay, moving on to the E. Jean Carroll case.
So Trump tried to get a stay through a federal appeals court
in relation to that 2019 defamation suit.
It is still scheduled to go to trial on January 15th.
Trump wants time to try to invoke an immunity defense,
like a presidential immunity defense or something like that.
Though the court denied the stay, it gave both sides 15 days to submit written briefs on this
matter and whether or not he should be allowed to invoke an immunity defense like that.
In New York, Trump appears to be trying to assert kind of a statute of limitations
claim over some of the real estate deals in question, and this is in the case that involves
him inflating numbers, his net worth, all of that stuff.
These arguments at the end of the day, they're going to hinge on whether or not new filings,
new documents about the real estate cases reset the clock, because some of the deals
are too old but it appears like some of those deals were used later and the
numbers were inflated at least that's the allegation and to be clear this is
the 250 million dollar suit that was brought by the New York Attorney General.
Let's see, and 14th Amendment news because there's a lot of that.
So a judge in Colorado rejected Trump's attempt to get a 14th Amendment challenge removed
to federal court.
It seemed almost like it was rejected due to like a procedural thing, but it's worth
noting that the Colorado Secretary of State said, quote, Trump is a liar with no respect
for the Constitution.
Doesn't seem like Colorado is going to be a friendly area for Trump to try to maneuver
in when it comes to the 14th Amendment claims.
Now in New Hampshire, the Secretary of State desperately wants the 14th Amendment issue
to be heard and reviewed by the Supreme Court before it goes too far.
Seems to believe that if Trump is left off the ballot in some states and is on it in
others, it's just going to cause chaos and anger.
I have a lot of questions about this tactic.
Even if it works, there's a whole lot of unintended consequences from this.
People being upset and angry about the ballots not matching in the different states, that's
not one of them.
I would remind everybody that ballots are always different in different states, so it
is what it is when it comes to that.
Okay.
Jenna Ellis news.
So Ellis was censured by a judge in Colorado.
During these proceedings, Ellis stipulated that some of the comments she made about the
election, violated rules prohibiting reckless, knowing, or intentional misrepresentations
by attorneys.
Now Jessica Yates, who is an attorney who helps the Supreme Court regulate attorneys,
there's a quote there that says, the public censure in this matter reinforces that even
if engaged in political speech, there is a line attorneys cannot cross, particularly
when they are speaking in a representative capacity.
The penalty for this censure is not much.
I want to say it's like a $200 fine or something like that.
It's not a big deal.
The reason you may see this again is because Ellis stipulated that, hey, yeah, some of
that was out of line.
So that will probably come up later.
Then you have Navarro.
So Navarro was recently found guilty at trial.
Navarro is claiming that some jurors seeing protesters during a break while they were
deliberating is grounds for a mistrial.
The arguments were heard, the judge didn't immediately rule after this post-trial hearing.
To be clear, post-trial hearings are rare, they don't happen very often.
And basically the judge is going to decide the issue later, whether or not it really
grounds for a mistrial.
There were some signs that said, I want to say they said defend democracy or something
like that.
It doesn't appear that any of the jurors actually talked to any of the protestors,
But apparently Navarro and his team believe that just the presence of the signs might
be enough to get a mistrial.
We'll see how that goes.
We will end with a rare Trump win.
An appeals court ruled that the special counsel's office can't have access to Scott Perry's
calls with other Republicans who are in Congress.
Basically speech and debate.
In the U.S. Constitution, there's a thing called the speech and debate clause which
protects people in Congress.
The court decided that that applied here.
Does that mean that all of Scott Perry's information is no longer accessible and none of that's
going to come into play?
No.
It's pretty narrow in scope, but this is a Trump win.
And given everything else that we just covered, I mean, let's be honest, there's not a lot
of those.
So there you go.
So that appears to be the news related to Trump and the legal entanglements.
I'm sure there's more.
There's always maneuvering that is occurring behind the scenes.
There's always little bits of legal news that comes out, but these were the ones that seem
like we'll end up seeing them again.
And to me, the classified documents, that order, using that language, that's going to
be the one that, that's going to be important later, because they are still treating these
documents as TS, as top secret documents, and even the judge is acknowledging that
at this point. So it seems incredibly unlikely given that after all of this
time, after they were exposed and out in the wild, given that they're still being
treated as TS documents, it's going to be really hard for him to claim that
storing them in a bathroom or on a stage was within any kind of rag at all. So
that's where we're at. I'm sure there will be more Trump news to come
undoubtedly. I don't think we'll end up doing these every week but it'll
probably be pretty often, just to provide a little bit of context and a little bit
more information. And having the right information will make all of the
Anyway, y'all have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}