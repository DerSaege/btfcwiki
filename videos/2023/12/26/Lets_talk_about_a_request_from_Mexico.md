---
title: Let's talk about a request from Mexico....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6c1Z3S0rub8) |
| Published | 2023/12/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculation arises about public statements designed for foreign policy consumption after Mexico offers to help with the U.S. southern border in exchange for assistance in migrants' home countries and re-examining the relationship with Cuba.
- The majority of issues in migrants' home countries stem from bad US foreign policy, necessitating the US to clean up its own mess there.
- The US policy towards Cuba is outdated, reflecting Cold War dynamics and warrants reevaluation.
- The Mexican president's public request for assistance and policy changes raises questions about how to garner Republican support for such initiatives.
- Tying developmental assistance and talks with Cuba to border security could potentially generate bipartisan support but lacks evidence of coordination.
- While the idea of orchestrating public requests for political gain is intriguing, there is no proof of such manipulation.
- Despite the lack of evidence, the scenario sheds light on how public statements can be tailored for specific outcomes without certainty of the truth coming to light.
- The need to reassess the US-Cuba relationship and address past foreign policy mistakes is emphasized.
- Generating Republican support for policy changes through border-related incentives is suggested as a strategic approach.
- The concept of orchestrating public requests for political advantage remains speculative and unlikely to be confirmed.

### Quotes

- "It is time to re-evaluate the U.S. relationship with Cuba."
- "The overwhelming majority of them, the situation was caused by bad US foreign policy."
- "Tie it to the border."
- "It is time for the U.S. to clean up its mess."
- "That is how things at times are created for public consumption."

### Oneliner

Speculation surrounds public statements for foreign policy consumption after Mexico's offer to assist the U.S. southern border prompts questions about ties to developmental aid and talks with Cuba, aiming to garner Republican support through border-related incentives, despite lacking evidence of coordination.

### Audience

Policymakers, Advocates

### On-the-ground actions from transcript

- Advocate for reevaluating US foreign policy towards Cuba and addressing past mistakes (suggested)
- Support initiatives that aim to provide developmental assistance to migrants' home countries (suggested)
- Engage in bipartisan efforts to generate support for policy changes through strategic approaches (suggested)

### Whats missing in summary

Further insights on the potential impact of reevaluating US foreign policy towards Cuba and addressing past mistakes.

### Tags

#ForeignPolicy #USCubaRelations #RepublicanSupport #BorderSecurity #MigrantsAid


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we are going to talk about information
for public consumption and talks, maybe,
that might be happening, and how things happen at times.
Because I got a message, and it's interesting.
because there's a bunch of speculation in it.
But I mean, it's not wrong.
Like, it's not off base.
The message was basically, hey, I
saw that bit about how a lot of times statements
are designed for public consumption
when you're talking about foreign policy.
And I was wondering if that had anything to do
with the weird requests from Mexico.
Mexico has offered to help with the U.S. southern border.
In exchange for that, they want two things.
They want the U.S. to help with development in the migrants' home countries, and they
They want the US to re-examine its relationship with Cuba in open talks.
We've talked about it before on the channel, both of these things, they're pretty good
ideas.
Let's be real about the situation in the migrants' home countries.
The overwhelming majority of them, the situation was caused by bad US foreign policy.
And if it wasn't directly caused by it, it was certainly not improved by it.
So it would make sense for the US to kind of clean up its own mess there.
That tracks.
When it comes to Cuba, the U.S. policy towards Cuba is dated.
It is dated.
It is out of date.
It is based on the dynamics of the Cold War.
So the real crux of the question is why would the Mexican president ask for this in such
a public way?
I mean, I would ask how you would get Republicans on board with opening talks with Cuba or providing
developmental assistance to the migrants' home countries.
If you wanted to do that as part of a sweeping change to U.S. foreign policy, I mean, how
might you get Republican support for it?
Tie it to the border.
I mean, it makes sense.
There's zero evidence that this is what occurred.
If you're not catching the implication here, the idea is that perhaps Biden or
Biden's foreign policy team more likely was like, hey, why don't you ask for this
in public?
You get a win, you look great because we'll try to get it done and everybody
gets what they want and it helps the U.S., you know, in domestic politics, it helps generate
support for the idea because Mexico will help with the border.
Yeah, I mean, it makes sense, but there's no proof that that occurred.
But I mean, it would just be a phone call.
It's interesting to think about, but it's important to understand that there's no evidence
and there won't be if it occurred.
But yeah, that is how things at times are created for public consumption.
Because it is time to reevaluate the U.S. relationship with Cuba.
It is time for the U.S. to clean up its mess.
This is a good way to get Republican support for it, but there's no way anybody would
ever find out that was true.
It would fall under those, it would fall into the category of a conspiracy theory that's
It's kind of believable, but it is super unlikely there would ever be any evidence of that.
But that is how it might work.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}