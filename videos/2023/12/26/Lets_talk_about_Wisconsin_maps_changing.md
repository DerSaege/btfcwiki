---
title: Let's talk about Wisconsin maps changing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LXzWl7moRnY) |
| Published | 2023/12/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wisconsin is considered the most gerrymandered state in the country.
- Justice Protasewicz correctly identified this fact during her campaign, causing discomfort among Republicans who created the contentious maps.
- Republicans threatened to impeach Justice Protasewicz if she took a case on the gerrymandered maps, but she proceeded with it.
- The court ruled that the maps in Wisconsin are indeed gerrymandered.
- New maps are expected to be ready for use by 2024, with the legislature having the first chance to submit them by August.
- If the submitted maps are deemed partisan, the court will step in and select a map.
- Due to widespread gerrymandering, the Wisconsin legislature heavily leans Republican, raising concerns about their map submission.
- The redistricting will likely impact the makeup of the legislature, with predictions that Republicans may retain control of the assembly but lose the Senate.
- This redistricting issue in Wisconsin is seen as highly consequential and is nearing conclusion.
- The resolution and new maps are expected before the next election, providing fair representation after a decade of partisan maps suppressing voter voices.

### Quotes

- "Wisconsin is widely viewed as the most gerrymandered state in the country."
- "They threatened to impeach her if she took a case. And she was like, yeah, no, I'm taking the case."
- "The odds are it's going to go to the court."
- "It is likely going to alter the makeup of the legislature in Wisconsin."
- "Having their voice and their vote diluted by people who wanted to rule rather than represent."

### Oneliner

Wisconsin faces consequential redistricting issues, with new maps expected before the next election after a decade of partisan suppression of voter voices.

### Audience

Wisconsin residents, political activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for fair and non-partisan redistricting (suggested)
- Stay informed about the redistricting process and attend relevant community meetings (implied)

### Whats missing in summary

Insights on the potential impacts of the new maps and the importance of fair representation in elections.

### Tags

#Wisconsin #Redistricting #Gerrymandering #JusticeProtasewicz #Legislature


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Wisconsin
and the Supreme Court there, and MAPS,
and yes, Justice Protasewicz.
And we're going to kind of run through what
should be the final chapter in this ongoing saga.
OK, so a quick recap of what has got us to this point.
Wisconsin is widely viewed as the most gerrymandered state
in the country.
If you go to Google and type in most gerrymandered state,
you will get a blurb about Wisconsin,
unless the results have changed because
of recent developments.
During her campaign, Justice Protozsiewicz
correctly identified this objective fact. Republicans who created the maps that are
so contentious like 10 years ago, they were really uncomfortable with her stating this is fact.
They threatened to impeach her if she took a case. And she was like, yeah, no, I'm taking the case.
They took the case, court decided for three that surprise the maps are gerrymandered.
Okay, so this is what happens from here.
The new maps are supposed to be ready for use by 2024.
The legislature has first crack at it.
I think they're supposed to have them ready for use by August.
If they're not going to do it, if they don't submit maps that are not partisan, the court
will step in and will select a map.
The legislature there, because of widespread gerrymandering, heavily leans Republican.
There is a decent chance that they just see what they can get away with.
I feel like the court will give them one shot and then they will select a map.
So if they don't come up with one that isn't gerrymandered, the odds are it's going to
go to the court.
Consider this, basically the entire assembly and like half the Senate will be running in
2024 under a new map.
It is likely going to alter the makeup of the legislature in Wisconsin.
I don't see how it couldn't.
Most people that I have talked to have said odds are Republicans will still retain control
of the assembly, but they'll probably lose the Senate.
I don't know if that's true.
Those are people that know a lot about Wisconsin, so we'll have to wait and see.
But this is probably one of the more consequential redistricting issues that was going on in
U.S. and it has at this point it looks like it's coming to a conclusion and we
should get a total resolution and new maps before the next election which
would be nice because the people in that state have been voting under maps that
were clearly partisan for a decade having their voice and their vote
deluded by people who wanted to rule rather than represent. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}