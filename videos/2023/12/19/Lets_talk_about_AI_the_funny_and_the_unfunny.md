---
title: Let's talk about AI, the funny, and the unfunny....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vRTa1FjMQIU) |
| Published | 2023/12/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk wanted his own AI, a large chatbot not "woke" as per user expectations.
- The AI turned out to be "woke," providing unexpected answers to users' questions.
- The situation is funny because the product didn't meet user expectations and suggested a left bias in reality.
- The goal was to create a politically incorrect AI with less bias towards liberal ideas, which it didn't achieve.
- The unexpected outcomes show that AI may not be under the control of its creators.
- It's a humorous moment when right-wing users were upset by the AI's responses.
- The wider implications of AI's unpredictability should give people pause.
- Comparing AI development to a "Jurassic Park" moment where consequences are overlooked.
- The situation is humorous but also raises concerns about AI technology.

### Quotes

- "It's one of those Jurassic Park moments where, you know, so concerned about whether or not we could, we didn't stop to think if we should."
- "It kind of indicates that AI as we are talking about it here, it's not really under the control of those people creating it."
- "You had a bunch of people on the right wing asking it questions like you know are trans women male and it's like no of course not."

### Oneliner

Elon Musk's attempt at a "not woke" AI turns out unexpectedly "woke," revealing AI's unpredictability and implications for bias in technology development.

### Audience

Tech enthusiasts, AI developers

### On-the-ground actions from transcript

- Question AI biases (implied)
- Stay informed on AI development (implied)
- Engage in ethical considerations in technology (implied)

### Whats missing in summary

The full transcript provides a humorous take on the unpredictability of AI, urging caution and reflection on the wider implications of technology development.

### Tags

#AI #ElonMusk #Technology #Bias #Ethics


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about AI.
We're going to talk a little bit about AI.
We're going to talk about something
that is incredibly funny.
We're going to talk about Elon Musk.
And then we're going to talk about something
that isn't quite so funny.
Um, okay.
So if you missed it, Elon Musk wanted his own AI type thing.
And again, just remember that term,
I'm not really sure it should apply here,
but it is a large language model chat bot.
And Musk's apparent goal with it was
to create one that wasn't, quote, woke, OK?
And that's what the user base was expecting.
So the product was pretty woke.
People were asking it questions and definitely not getting
the answers they wanted.
Now, that's funny for a number of reasons, mainly just
because any product that is designed to do one thing and
does something else, at least in the user's eyes, whether
or not it's true, that's funny.
It's also funny because this certainly does add a little
bit more credibility to the idea that if you have a bunch
of information and it's all being processed, well, facts
reality do have a well-known left bias and all of this is worth you know
mentioning and laughing about and all of that stuff but the part that isn't
quite as funny about this is that by all indications the goal of this was to be a
politically incorrect AI. It was to be an AI that wasn't woke. It was supposed to
have quote less bias towards the left, towards liberal ideas and it doesn't.
It kind of indicates that AI as we are talking about it here, it's not really
under the control of those people creating it. There's your black mirror
twilight zone moment. I mean this whole thing is humorous in the fact that
you had a bunch of people on the right wing asking it questions like you know
are trans women male and it's like no of course not and yeah I mean it's funny
that that upset them, and it's funny that the product did not perform as expected.
It's not so funny when you think about the wider implications of it because AI is a technology
that we are just diving into.
It's one of those Jurassic Park moments where, you know, so concerned about whether or not
we could, we didn't stop to think if we should.
This is a humorous moment that should also kind of give people pause.
Because it is not performing as expected.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}