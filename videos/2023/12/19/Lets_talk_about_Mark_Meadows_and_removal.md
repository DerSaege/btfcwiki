---
title: Let's talk about Mark Meadows and removal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=I1hcr35KJh4) |
| Published | 2023/12/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mark Meadows, involved in a case in Georgia with Trump, tried to move his case to federal court but was ruled against and appealed.
- The appeals court decided that Meadows' case must stay in Georgia because the events leading to the action were not related to his official duties.
- The judges determined that Meadows, as a former federal officer, did not have the authority to conspire to overturn election results.
- If Meadows decides to continue fighting and take it to the Supreme Court, the outcome remains uncertain.
- Meadows' case staying in Georgia may indicate potential rejection of Trump's claims as well.
- Meadows faces a decision on whether to pursue moving his case to federal court, considering the potential lack of immunity there.

### Quotes

- "The determination over whether or not he [Meadows] count as a federal official even though he was no longer part of the government, that was kind of up in the air."
- "If Meadows continues to fight at the next op Supreme Court, I'm not sure how likely that is."
- "The fact that they shot Meadows down, it leads to the idea that Trump's claims will also be rejected."
- "Even if it goes to federal court and he gets it removed, the immunity that he thinks is there, these judges certainly don't think it's there."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Mark Meadows' legal battle in Georgia impacts Trump's claims and the decision on moving the case to federal court, raising questions about immunity.

### Audience

Legal analysts, activists.

### On-the-ground actions from transcript

- Stay updated on legal proceedings and analyses (implied).

### Whats missing in summary

Insights on the potential implications of Meadows' case outcome on future legal actions and political decisions.

### Tags

#MarkMeadows #LegalBattle #Georgia #FederalCourt #TrumpElectionClaims


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today we are going to talk about Mark Meadows, Georgia,
and all of that process and what's going on,
and the decision that was reached today
and how it impacts a few other things along the way.
Okay, so if you don't know what's going on,
Mark Meadows, one of those who was brought up
with Trump in Georgia in that giant case. He has been trying to get his case removed from Georgia
to the federal court system. He was ruled against, and he appealed it. The appeals court
has reached their decision. A three-judge panel basically decided, now it needs to stay in Georgia.
relevant quotes that matter here when talking about the law was said that it
does not apply to former federal officers and even if it did the events
giving rise to this criminal action were not related to Meadow's official duties.
That's a big part of this. The determination over whether or not he
would count as a federal official even though he was no longer part of the
government, that was kind of up in the air. The decision that these actions were
not part of his official duties, that was... it seemed to me to be obvious, but now
the judges say that, so that's important. It goes on, simply put, whatever
the precise contours of Meadow's official authority, that authority did not extend
to an alleged conspiracy to overturn valid election results. Okay, so where
does it go from here, right? That's the obvious question. If Meadows continues to
fight at the next op Supreme Court, I'm not sure how likely that is. I don't
know that he would or should pursue it, but they have the Trump legal teams, and
when I say that I mean all of them, not those just employed by Trump, they've
made a lot of decisions and made a lot of appeals and stuff like that that maybe
weren't the best strategically. I'm still actually of the opinion that if Meadows
had been successful here, he would regret it. But at the moment, his case will stay
in Georgia. If it goes to the Supreme Court, we'll see what they say. One thing
that's worth noting is that the statement, what Meadows is saying is, what
Meadows is saying protects him, is a smaller ask than what Trump is saying.
You know, they're different, but when you compare them, the fact that they shot
meadows down, it leads to the idea that Trump's claims will also be rejected.
That's the way I read it. I'm sure legal channels are going to have a much more
in-depth take on that, but that's the way it seems to me. From here, he has to make
the decision as to whether or not he wants to continue to try to fight to
get it removed to federal court. I would imagine at this point he's starting to
understand that even if it goes to federal court and he gets it removed, the
immunity that he thinks is there, these judges certainly don't think it's there.
And it may not be a wise idea to remove it to federal court. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}