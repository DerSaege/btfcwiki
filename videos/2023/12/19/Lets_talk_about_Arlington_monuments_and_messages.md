---
title: Let's talk about Arlington, monuments, and messages....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=E2pJ_EZXVbI) |
| Published | 2023/12/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the questions around the Reconciliation Monument at Arlington and the history behind Confederate monuments.
- Points out that the monument isn't actually called the Reconciliation Monument and that it's referred to as the Confederate Memorial on the Arlington Cemetery website.
- Talks about the historical context of Confederate monuments being built during periods of civil rights activity with messages of racial oppression.
- Notes that the Confederate Memorial at Arlington was built in the early 1900s with a tone of reconciliation, different from many other Confederate monuments.
- Mentions Congress's involvement in the decision to potentially remove the Confederate Memorial through the NDAA of 2021.
- Addresses the impossibility of completely removing the connection to the Confederacy from Arlington Cemetery due to Confederate soldiers buried there.
- Provides historical context on how Arlington was initially taken during the Civil War for strategic military reasons, not due to Union losses.
- Mentions Mary Custis, who owned Arlington before it was taken by the US Army, and her connection to Robert E. Lee.
- Comments on the current situation where the Department of Defense is trying to remove the Confederate monument, authorized by Congress, with some members now expressing outrage.
- Talks about a temporary restraining order in place while the issue is being sorted out and criticizes Congress for potentially not addressing the situation they initiated.

### Quotes

- "You won't find that name. Click on where it says monuments and look for something called the Reconciliation Monument. You won't find that there."
- "The one at Arlington, it really did have a different purpose. It was built in the early 1900s and it really did have a tone of reconciliation."
- "Congress actually made this happen, to be clear."
- "That is impossible, and more importantly, I think you know it's impossible."
- "I don't think that there's ever going to be a point in time where people do not understand the connection between Arlington National cemetery, the Civil War, or the Confederacy."

### Oneliner

Beau breaks down misconceptions around Confederate monuments at Arlington Cemetery, pointing out the historical context and Congress's role in potential removal.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Contact local historical societies to learn more about the history of Arlington Cemetery (suggested)
- Support community efforts to contextualize historical monuments accurately (exemplified)

### Whats missing in summary

Deeper insights into the historical significance of Arlington Cemetery and the ongoing debate around Confederate monuments.

### Tags

#Arlington #ConfederateMonuments #Congress #History #Community #Removal #ReconciliationMonument


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Arlington
and a statue that's there and history
and a question that comes up and congressional concern
and all of that stuff.
And then at the end we'll hit the news.
And we're gonna do this
because I got a couple of questions about it,
but one in particular I found interesting.
Okay, so here's the message. I have a question about the Reconciliation Monument at Arlington.
I know you oppose most Confederate monuments because of when they were built and the message
attached to them, but I think you would agree this is different. Congress sure seems to think so.
So as a history buff, I have to ask you, as somebody who seems interested in history,
how can we allow them to remove all connection to the Confederacy from
Arlington Cemetery? Without something identifying the war, how will we know the
history of the cemetery? The Union losses made seizing the land a necessity." Okay,
so we're gonna go through this like line by line now. I have a question about the
Reconciliation Monument at Arlington. Yeah, see, I know that that's what
everybody's calling it on social media, this is what I want you to do. Go to
ArlingtonCemetery.mil. You won't find that name. Click on where it says
monuments and look for something called the Reconciliation Monument. You won't
find that there. You will find something that says the Confederate Memorial,
though, just as a point of fact. Let's see. I know you oppose most Confederate
monuments because of when they were built and the message attached to them,
but I think you would agree this is different. That's true. That is true. If you don't know what
this section is about, most Confederate monuments, those you find in the deep south, they were not
built like right after the Civil War or anything. They were built during periods of civil rights
activity, and the message attached to them was pretty clear, you uppity folk better stay in your
place. That's why they were built. The one at Arlington, it really did have a
different purpose. It was built in the early 1900s and it really did have a
tone of reconciliation. That much is actually true. So there is that.
Congress sure seems to think so. See, that's weird. Like, I know that there are
congressional people who are upset about this possibly being removed, but you know
why it's being removed, right? Because Congress said to the NDAA of 2021
established a commission. They did a commission because they didn't want to
take responsibility or heat for actually taking the votes, so they created a
commission to make the decisions that would then have to be implemented within three years.
Congress actually made this happen, to be clear. Okay, so as a history buff, I have to ask you,
as somebody who seems interested in history, how can we allow them to remove all connection
to the Confederacy from Arlington Cemetery.
That is impossible, and more importantly,
I think you know it's impossible.
I mean, the obvious reason it's impossible
is because there's Confederate soldiers there.
I mean, that makes it really hard
to remove any connection.
But I think you know the other reason too,
because you go on to say,
without something identifying the war,
How will we know the history of the cemetery?
The Union losses made seizing the land a necessity.
Seizing, that tells me you know where this land came from.
Incidentally, it wasn't over Union losses,
but it was seized.
So for those who don't know, Arlington,
it was taken early in the war,
not because of union losses, but because of its heights and artillery and being able to
defend DC.
That's why the US Army initially took it.
And they took it from Mary Custis.
That's who owned it.
I know some history people right now are coming away.
I thought, no, no, Mary Custis actually owned it.
She was married to somebody though.
married into this family, the Lees. She married a man named Robert E. Lee. I feel like it's
going to be really hard to remove all connection to the Confederacy from that place. Okay,
so if you haven't figured it out, there is a monument in the middle of, it's not in
the middle in Arlington, in the National Cemetery there, and it is the Confederate
monument. It's a Confederate monument. There are some pretty objectionable things
about it, and DOD, the Department of Defense, is trying to remove it, and some
people in Congress who need an outrage of the week decided to get mad about
this even though it was Congress that authorized the Commission that told DOD
to remove it. The current development at time of filming, what has happened, is
there is a temporary restraining order while all of this gets sorted out. It's
weird though because if Congress actually had an issue with it, Congress
can undo what Congress did, but maybe they don't have the votes for it. Or maybe
you know, they don't really want to win, they just want to get people mad. Get
those social media clicks because that is what passes for governing in the
Republican Party, I guess. I don't think that there's ever going to be a point in
time where people do not understand the connection between Arlington National
cemetery, the Civil War, or the Confederacy.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}