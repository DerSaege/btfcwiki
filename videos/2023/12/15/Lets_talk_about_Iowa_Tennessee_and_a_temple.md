---
title: Let's talk about Iowa, Tennessee, and a temple....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=FG5vpP5rnBU) |
| Published | 2023/12/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Satanic Temple set up a religious display in the Iowa State Capitol, sparking controversy among politicians and religious leaders.
- The display was vandalized by someone from Mississippi, charged with criminal mischief.
- An after-school Satan Club linked to the Satanic Temple is forming in Tennessee.
- The Temple clarified they don't advocate for introducing religion into public schools unless other religious groups are present.
- Right-wing politicians often push for religious freedom but mainly mean their religion.
- Separation between church and state exists to allow all religions, not just one, access to state facilities.
- Changes in rules may occur where religious groups are no longer allowed to use state or county facilities or funds.
- There's a possibility that this situation is a form of protest, and changes may or may not happen.
- The desire to display religious beliefs on state property can lead to issues and undermine religious freedom.
- Beau reminds that the US was founded as a non-theocratic country, and that principle has served well.

### Quotes

- "If you open the door, well, all religions get to walk through."
- "The desire to try to convert people on state property or to signal your allegiance to a particular voting bloc tends to just lead to issues."
- "It's just a thought."

### Oneliner

The Satanic Temple's display in Iowa sparks controversy, while an after-school Satan Club forms in Tennessee, raising questions about religious freedom and separation of church and state.

### Audience

Activists, educators, policymakers

### On-the-ground actions from transcript

- Support inclusive policies in schools by advocating for the presence of diverse religious groups on campus (implied)
- Advocate for the separation of church and state to ensure religious freedom for all (implied)

### What's missing in summary

The full transcript provides more context on the potential outcomes of the situation and the historical significance of the separation between church and state in the US.

### Tags

#ReligiousFreedom #SeparationOfChurchAndState #Protest #SatanicTemple #Education


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about a display
in the Iowa State Capitol and what occurred there
and what it has to do with an after-school club
that is forming in Tennessee
and what we can expect the likely outcome
to be from all of this.
because it all does tie together.
So we will start in Iowa.
The devil went down to Iowa, basically.
So the Satanic temple put up a permitted religious display
in the capital there.
Obviously, it drew a lot of attention.
lot of politicians got upset, religious leaders, so on and so forth. The reason
the temple was able to do this is because they allow other religious
groups to do the same thing. So I believe the the display was set to be taken down
I want to say on Friday, but it was vandalized by somebody from Mississippi
who was charged with criminal mischief in the fourth degree. That person is
probably very lucky, very lucky, because this is something that could easily be
construed as attempting to deprive someone of their rights. So you have that
going on and at the same time the after-school Satan Club, which is a
national nonprofit that I believe is linked to the Satanic Temple, they are
forming one of those clubs at an elementary school in the Memphis
Shelby area in Tennessee. It is worth noting that the interim superintendent
there said, I do not support the beliefs of this organization at the center of
recent headlines, I do however support the law. And it's worth noting that the
The temple has said that, quote, does not believe in introducing religion into public
schools and will only open a club if other religious groups are operating on campus.
The fact that this organization doesn't actually like worship the devil, that hasn't, it hasn't
It really calmed the rhetoric and fear coming from religious leaders in the area.
Now this type of thing has happened in the past.
What generally occurs is right-wing politicians start screaming about religious freedom.
They open up state institutions or county institutions to religious groups or for religious
displays, but when they're talking about it, they really only mean their religion.
That's not how it works in this country.
If you open the door, well, all religions get to walk through.
That's why that hedge between the church and state and all of that.
That's why that's been around so long.
What generally happens from here, I don't know if it's generally, oftentimes the rules
are changed and organizations, religious organizations, are no longer allowed to
use state or county facilities or funds. In this case, I mean, it seems
like there might be people at both of these locations who understand that
this is a form of protest. And that may not occur here. It may be one of those
things where these organizations exist. You know, the club gets set up and it
runs for a while and it proves its point but nothing really changes because it is
Tennessee. The alternative to that is the right wing just loses its mind because
it can't take a joke, and they end up curtailing the very laws that they put
into place in the first place. There's a reason that that separation exists. It's
not to curtail somebody's religious beliefs. If you actually believe
something you believe that it's internal. You don't have to display it. The desire
to try to convert people on state property or whatever or to signal your
allegiance to a particular voting bloc, which is oftentimes what it really is,
It tends to just lead to issues and what often happens is it undermines the idea of religious
freedom because what occurs is this group, they fight and they say, well, we want to
have our Bible study at this school and eventually the law gets passed and then other groups
come in and they get mad about that or they start screaming about yoga or something like
that and then it gets repealed and when it gets repealed it goes through this
period where you can't even talk about it at school. For people who love to talk
about how much they support the founding principles of this country it's important
to remember that this country was set up to be a country that was not theocratic
and that principle it served the country pretty well for a long time. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}