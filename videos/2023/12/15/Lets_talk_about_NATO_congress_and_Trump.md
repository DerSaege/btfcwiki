---
title: Let's talk about NATO, congress, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IR7ePHJNihM) |
| Published | 2023/12/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NATO is a critical component of American influence and dominance around the world.
- Regardless of political ideology, NATO is universally recognized as key to American power.
- The preservation of NATO is viewed as paramount by both the far right and the far left for different reasons.
- Congress recently passed a measure making it impossible for a president to unilaterally withdraw from NATO.
- This move by Congress signals concerns about a potential future president, particularly alluding to Trump's actions.
- Republicans in Congress supported this measure without objection, suggesting concerns about Trump's impact on American power.
- This action implies that even those endorsing Trump believe he may be inept at foreign policy.
- The measure was passed as a safeguard against potential actions by a re-elected Trump that could harm American interests globally.

### Quotes

- "NATO is the building block of American influence and dominance."
- "Republicans in Congress believe Trump might do something ruinous to American power that they had to act."
- "They actually believe he is somebody who is so inept at foreign policy."

### Oneliner

NATO is pivotal for American influence globally, with recent congressional action showing concerns about Trump's impact on US power.

### Audience

Congress members, policymakers, voters

### On-the-ground actions from transcript

- Contact Congress members to express support for measures safeguarding American interests (implied)
- Stay informed and engaged in foreign policy decisions affecting American influence (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of NATO's significance for American power and the recent congressional action as a response to concerns about potential actions by President Trump.

### Tags

#NATO #AmericanInfluence #CongressionalAction #ForeignPolicy #Trump #AmericanPower


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about NATO.
We're going to talk about NATO
and what it means for American influence around the world.
We're going to talk about something Congress just did
and why it's really important to kind of take note of it.
We're going to talk about Trump,
And we are going to talk about some universal facts when it comes to foreign policy.
When people talk about foreign policy, oftentimes they will have very conflicting views as to
how things should work.
And a lot of it is based on various ideologies.
But the one thing that is universally agreed upon, and there aren't a lot of things that
are universally agreed upon, but one thing that is, is that NATO is the building block
of American influence and dominance.
You may think that's a good thing, you may think that's a bad thing, but it doesn't
matter where you set on the ideological spectrum, you know that's a statement of
fact. NATO is that important to the United States. If you are somebody who
is on the far left side of things, you probably acknowledge that NATO is a
tool of the establishment. You view it in that way. It's a tool of the status quo
and it is what the United States uses to influence events around the world.
And that it is a major component of American power. If you're on the far
right and you're like a true nationalist, even though you may look at NATO and be
like, you know, those aren't our problems. You know that it is the building block
that occurred after World War II that allowed the United States to rise to the
prominence it currently has. If you are somebody who just read some books about
all of the misdeeds of the United States in foreign policy adventurism and halfway
through the third book you were just like you know what I've seen enough I'm
not gonna say that whoever is opposed to the United States is the good guy but
maybe they got a point if that is the extent of your take on foreign policy
you know that NATO is a major component of American power. It doesn't matter
where you're at on the spectrum. You know this to be true. So much so that if you
are somebody who is on the far right, you view the preservation of NATO, if you
have any understanding of foreign policy, you view the preservation of NATO as
just of paramount importance and if you're on the far left you view it as
one of the major goals when it comes to disrupting the status quo to have the
United States pull out of NATO. It doesn't matter where you're at you know
this to be true because NATO is a force to be reckoned with. There is no military
power even remotely close, anywhere in the world, that has the capabilities that NATO
has.
Okay, so now that we're done with this, guess what Congress just did?
Congress made it impossible for a president to unilaterally withdraw from NATO, which
Which makes sense, given everything that we just said, and the idea that people in Congress
should be pursuing American interests, that makes sense that they did that.
It's weird that they had to though, right?
I mean, given the fact that anybody with a basic understanding of foreign policy understands
that NATO is a building block of American power, it seems weird that they would need
to do that because no president would want to disrupt American influence,
American power, American economic stability, and then along the way disrupt
the security of Europe, disrupt the balance of power in the Middle East, in
the Pacific. I mean realistically it's easier to do the things it wouldn't
hurt if NATO, if the US pulled out of NATO. And that would be Central and South
America, they might actually do better. Africa probably wouldn't see much of a
change, but that's it. Everywhere else in the world it would have an impact. Now
some people would say that's a growing pain that you would have to go through
before you get to something better. Some people would say that, you know, it will
lead to a bunch of wars, and realistically they're probably both right.
But Congress felt the need to prohibit a future president from unilaterally withdrawing.
Why?
this is the important part. There's only been one president that was like, hey, I
want to do that. That's a good idea because I'm a stable genius. So this
passed without Republican objection. The Republican Party, they didn't oppose
this going through. What Republicans in Congress are telling you by pushing this
through, by allowing this to go through, by voting for it, in fact by helping it to get set up
and get into the bill, is that they believe Trump, if re-elected, might do something
so ruinous to American power and American economic stability that they had to act.
That's what they're saying. There's no other way to read it. So I think it's
important to acknowledge that, especially as Republicans endorse Trump, it's
important to remember that they're endorsing Trump to energize Trump's
base for them, but they actually believe he is somebody who is so inept at
foreign policy that they have to preemptively pass laws to stop him from
totally undermining the the United States' place in the world. That's how
how bad he is.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}