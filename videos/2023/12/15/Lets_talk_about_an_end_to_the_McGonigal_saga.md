---
title: Let's talk about an end to the McGonigal saga....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=a-gz8pcREDc) |
| Published | 2023/12/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the final chapter in the story of McGonagall, a former FBI counterintelligence boss accused of working for a Russian oligarch to lift sanctions.
- McGonagall pleaded guilty to the accusations, although no actual espionage or trading of secrets was proven.
- The prosecution requested a 60-month sentence, while McGonagall's attorneys asked for probation.
- The judge sentenced McGonagall to 50 months, considering the seriousness of the offense and his previous position.
- The high sentence was likely influenced by McGonagall's role as the boss of the counterintelligence division and the trust placed in individuals in such positions.
- This case is significant because it tarnishes the reputation of US counterintelligence.
- McGonagall's attorneys hoped to leverage his remorse and past work to secure a lenient sentence, but the judge did not overlook his position.
- The sentence of 50 months, although high, is not excessive and signifies the end of this prominent case.
- It's unlikely that there will be any appeals following this sentencing.

### Quotes

- "Counterintelligence people are supposed to be the government's most trusted people."
- "The judge sentenced McGonagall to 50 months."
- "This was a big black eye to US counterintelligence."

### Oneliner

Former FBI counterintelligence boss faces 50-month sentence for working with a Russian oligarch, tarnishing US counterintelligence's reputation.

### Audience

Law enforcement officials

### On-the-ground actions from transcript

- Keep a keen eye on individuals in positions of trust and power (implied).

### Whats missing in summary

The impact of the case on national security and the importance of upholding integrity within law enforcement organizations.

### Tags

#FBI #Counterintelligence #Russia #Sentencing #USSecurity


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the final chapter
in the story of McGonagall and how that has all played out
because the sentence came down today.
OK, so if you don't remember this case,
you have no idea what I'm talking about.
He was the boss of the CI division
of the New York field office of the FBI.
He was the counterintelligence boss.
So his job was to catch spies.
He was accused and pleaded guilty
to basically working at the behest of a Russian oligarch,
trying to get him off the sanctions list
and stuff like that.
This is a big deal for somebody
that held his position, there wasn't actual espionage as in trading of secrets that anybody's
been able to demonstrate. However, the use of his skills and former position, all of that stuff
weighed into this. The prosecution asked for 60 months for five years. His attorneys asked for
non-custodial, basically, probation. The judge went through and kind of weighed
this and came back with a sentence of 50 months. That is high for this type of offense,
but not ridiculously high and obviously not as much as the prosecution wanted.
I think the prosecution probably wanted a higher sentence because of his
position, because he was the boss over the counterintelligence division.
Counterintelligence people are supposed to be the government's most trusted people.
They are the people who get to look for spies so they have access to all kinds
of stuff and they are viewed as almost above suspicion especially once you get
to a position like this. Keep in mind the FBI has a massive role when it comes to
counterintelligence and being the boss of the counterintelligence division in
New York that's that's huge. I mean the I think the only higher as far as the
level of sensitive stuff would be the one in DC. So this was a big black eye to
US counterintelligence. The sentence is high, but not ridiculously so. Obviously,
I don't think he's going to be happy with it given that his attorneys were
hoping for basically a very lengthy probation and saying that you know he
learned his lesson and he was remorseful and I think they were hoping on
balancing his previous work but it was the position that he held they weren't
going to let that slide. So this case, which was a big one, it looks like it's
finally wrapped up. I doubt there's going to be any appeals over this or anything
like that. So we can cross this one off the board. Anyway, it's just a
With that, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}