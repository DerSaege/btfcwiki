---
title: Let's talk about Republicans being right about Trump's immunity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=E60SKpkuKF4) |
| Published | 2023/12/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans, including former officials from five different administrations, filed an amicus brief opposing Trump's claim of wide-ranging presidential immunity from criminal prosecution.
- They argue that granting such immunity could embolden former presidents to commit criminal acts to prevent their successors from taking office.
- Republicans are using a constitutional basis to refute Trump's claim, stating that nothing in the Constitution supports his argument for criminal immunity.
- The court accepted the brief, indicating a significant challenge from a group of Republicans against the former president.
- This opposition from Republicans showcases a divide within the party between actual conservatives and authoritarians on the right.
- The likelihood of the former president prevailing on this appeal seems low.
- Beau reminds viewers that supporting Trump's aims may go against the U.S. Constitution, as pointed out by the Republicans who believe he is trying to subvert the Constitution through this appeal.

### Quotes

- "Nothing in our Constitution or any case supports former President Trump's dangerous argument for criminal immunity."
- "You have to decide whether you want to support Trump and his aims, or you're going to support the U.S. Constitution."

### Oneliner

Republicans, including former officials, challenge Trump's claim of presidential immunity, warning of constitutional subversion.

### Audience

Political observers, Republican voters

### On-the-ground actions from transcript

- Support efforts challenging unconstitutional claims by former officials (exemplified)
- Educate others on the importance of upholding constitutional principles (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how Republicans are opposing Trump's claim of presidential immunity and the potential implications for the U.S. Constitution. Viewing the full transcript can provide a comprehensive understanding of this legal and political battle.

### Tags

#Republicans #Trump #Constitution #LegalBattle #USPolitics


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today, we are going to talk about Republicans being right,
because they are.
A whole bunch of Republicans are absolutely correct
when it comes to their interpretation
of the U.S. Constitution in relation to Trump and his claims.
And they filed to state their opinion.
The court accepted the brief.
So, quick recap, in Trump's federal election case, okay, this is the DC case, he's claiming
this wide-ranging criminal immunity, like he's immune from prosecution because he's
president or was president, whatever.
Smith obviously opposes this, went to Judge Chutkin, Judge Chutkin was like, yeah, no,
that's not a thing.
the appellate court now. A group of Republicans, former officials from, I believe, five different
Republican administrations, filed an amicus brief, a friend of the court brief, meaning they are
not directly involved in the case, but they have insight that they feel is important, and the court
agreed to allow them to file it. The Republicans filed and basically said that agreeing with
Trump's claim of this wide-ranging presidential immunity from criminal prosecution would quote,
embolden presidents who lose reelection to engage in criminal conduct through official
acts or otherwise, as part of efforts to prevent the vesting of executive power required by Article
2 in their lawfully elected successors." Article 2, meaning Article 2 of the U.S. Constitution.
They also said, "'Nothing in our Constitution or any case supports former President Trump's dangerous
argument for criminal immunity.'" Short version here. You have Republicans now arguing what we've
been talking about on the channel. They're citing a different part of the Constitution,
but they are using a constitutional basis to say, absolutely not. You cannot give him this level
of immunity from criminal prosecution because, I mean, if you do, well, basically Biden never
has to leave office, kind of what they're saying, in much more legal language. The court
accepted the brief, which means they wanted to hear it because they didn't have to accept it.
It is a clear indication that you have a a whole lot of Republicans who are former officials
are going to be throwing up some roadblocks for the former president. Republicans who were
probably like actual conservatives, not just authoritarians who are on the right.
I don't think this is the last time we're going to see this and it's certainly, it
seems unlikely that the former president is going to prevail on this appeal.
So it's important to remember, I know a lot of people watching this channel support the
former president and you watch this just to see, quote, the other side.
You have to remember at this point you now have a whole bunch of Republicans coming out
who span a very long time with the Republican Party and they're coming out saying the former
president is trying to subvert the Constitution via this appeal.
This is not real, he's made it up, is basically their argument.
There are going to be a lot of people who are going to have to decide whether they want
to support Trump and his aims, or they're going to support the U.S. Constitution.
Because a lot of Trump's aims are in direct contradiction to the Constitution.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}