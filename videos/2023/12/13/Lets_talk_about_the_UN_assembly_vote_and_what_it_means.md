---
title: Let's talk about the UN assembly vote and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=T275QwTD09M) |
| Published | 2023/12/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the purpose of the General Assembly vote at the United Nations, which was to demand a ceasefire.
- Outlines the voting results: 153 in favor, 23 abstained, and 10 opposed.
- Clarifies that the vote cannot force a ceasefire and is not binding.
- Notes that the vote may not have a significant impact on pressuring Israel to announce a ceasefire immediately.
- Emphasizes that the US and Israel have differing opinions on post-offensive plans, with the US having international backing for its plan.
- Suggests that the vote signals Israel may need to reconsider its stance based on the international community's position.
- Indicates that the UN's goal is to promote peace, even though Cold War politics, with the US vetoing, influenced the Security Council's decision.

### Quotes

- "The weight of this vote as far as its ability to force a ceasefire, it's about on par with a Twitter poll."
- "The fact that they couldn't get it through the Security Council, that's Cold War politics that's still left over."
- "The signaling that occurred here should be a blinking red light in Tel Aviv saying, hey, maybe you need to change course on some things."

### Oneliner

Beau explains the limited impact of the UN General Assembly vote on enforcing a ceasefire, signaling potential shifts in Israel's stance but unlikely to immediately pressure for a ceasefire.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor international developments and diplomatic efforts for updates on the Israel-Palestine conflict (implied).

### Whats missing in summary

Insights into the potential economic consequences and decision-making effects following the UN General Assembly vote. 

### Tags

#UN #GeneralAssembly #Ceasefire #US #Israel #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the vote.
We're gonna talk about the vote,
the General Assembly vote at the United Nations.
We're going to talk about what it means,
where it goes from here,
and what it can tell us about the future a little bit.
Because as soon as the vote occurred,
that was the question, what happens next?
Okay, so if you have no idea what I'm talking about, the General Assembly of the United
Nations took a vote, and the purpose of the vote was to demand, it's a quote, demand
a ceasefire.
153 voted in favor, 23 abstained, and 10 were opposed.
The question is, what's going to happen now?
Nothing, nothing.
The weight of this vote as far as its ability to force a ceasefire, it's about on par with
a Twitter poll.
Doesn't really mean much.
It can't force a ceasefire this way.
It's not binding.
That being said, it also wasn't pointless because the U.S. and Israel, as we talked
about yesterday, they have differing opinions on what should happen the day after, assuming
Israel is successful in their offensive.
The U.S. has a plan and it has some international backing.
Israel does not seem to be on board with that plan.
This vote kind of signals that they may need to change their mind about that.
It's not just that only 10 voted with Israel, you know, voted to oppose the ceasefire.
It's not just that, it's that the only foreign policy heavyweight was the US.
The other countries are not exactly, they are not exactly countries that are known for
being deciding factors in foreign policy decisions of this scale.
It's not like the world is waiting on whether or not Micronesia supports something.
The only foreign policy heavyweight that voted with Israel is the US, and the US has this
plan that Israel isn't on board with.
vote may do something to change that. So it wasn't pointless. And honestly, I think
it's probably good to get this on record. The United Nations, their goal is to
promote peace. That's what they're supposed to do. The UN should absolutely
be calling for that, it's literally their mission, it's supposed to be anyway.
The fact that they couldn't get it through the Security Council, that's Cold War politics
that's still left over, and to be clear, it's the US vetoing it.
This vote may sway Israel on what happens after the offensive.
That's what it might do.
As far as it putting any real pressure on Israel to announce a ceasefire, seems unlikely.
is committed to their course and realistically even if they have even if
they were to decide they bit off more than they could chew it's still going to
be at least a week or two like if they made the decision today that they wanted
to leave they can't just pull out immediately there's more to it than that
that. This might sway further decision-making, but it's not going to
alter the decisions that have already been made. It wasn't pointless. I know
it's going to seem pointless because it can't force a ceasefire, but the
signaling that occurred here, it should be a blinking red light in Tel Aviv saying,
hey, maybe you need to change course on some things. Because first you see stuff
like this, and then you start suffering economic consequences if you're a
country. But that's really it. I know that's not the answer people want to hear, but this
isn't really going to change the ceasefire dynamic much. If the US was to now take this
vote and push, maybe, but that seems pretty unlikely because the current US plan now hinges
on Israel being able to degrade the Palestinian forces.
So the US probably isn't going to push for that,
even though they might be able to use this to try to get it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}