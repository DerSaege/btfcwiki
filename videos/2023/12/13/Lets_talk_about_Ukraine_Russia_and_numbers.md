---
title: Let's talk about Ukraine, Russia, and numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=JNmwv19LiB4) |
| Published | 2023/12/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The U.S. released declassified numbers on Russia's losses in Ukraine, revealing high casualty rates.
- Russia lost a significant amount of tanks, IFVs, and APCs during the invasion.
- Russia aims to boost its active duty troops to 1.5 million by recruiting individuals deemed undesirable by Putin.
- The U.S. releasing these numbers is a strategic move to showcase their intelligence accuracy and influence.
- Some U.S. politicians question where the money is going, which is towards supporting Ukraine.
- There is reluctance from certain politicians to support aid due to their far-right ideology alignment with Putin.
- Despite the release of these numbers, it may not sway those opposed to aiding Ukraine.
- The Democratic Party may need to compromise to ensure aid is approved, potentially impacting future elections.
- Those against aid post-number release may be driven by ideology rather than corruption concerns.
- Civilian casualties in conflicts often surpass combatant losses.

### Quotes

- "There are certainly people watching this video who are like I want US foreign policy to fail."
- "They have chosen a far right-wing ideology over US national interest."
- "They have to make the U.S. lose so they can feel better about their Twitter talking points."

### Oneliner

The U.S. released revealing numbers on Russia's losses in Ukraine, but some politicians' reluctance to support aid stems from a far-right ideology alignment with Putin.

### Audience

Politically aware viewers.

### On-the-ground actions from transcript

- Support aid efforts for Ukraine (suggested).
- Advocate for policies that prioritize humanitarian support (implied).

### Whats missing in summary

The full transcript provides a nuanced understanding of the political motivations behind aid support and opposition.

### Tags

#Russia #Ukraine #US #Politics #AidSupport #ForeignPolicy


## Transcript
Well, howdy there, Internet People, it's Beau again.
So today we are going to talk about Russia and Ukraine
and numbers and why the U.S. released those numbers,
what the thought process is behind releasing them
and why I actually don't think it's going to matter,
not to a lot of people.
Okay, so if you missed the news,
The U.S. released declassified numbers
when it comes to its estimates on what Russia
has lost in its invasion of Ukraine.
At the start of the conflict,
Russia had around 900,000 active duty troops.
They committed 360,000 of those to the invasion.
Of those, 315,000 have been rendered KIA or WIA.
For comparison and context,
in the entirety of Operation Iraqi Freedom,
the U.S. KIA, WIA rates about 10% of that
over the entire period of OIF.
These numbers are incredibly high. They released some other numbers, stuff people
watching this channel might care about. Russia entered with roughly 3,500 tanks
of which they have lost 2,200 when it comes to IFVs and APCs, infantry
fighting vehicles, or armored personnel carriers. Things that most people would
call tanks, but they're not tanks. They entered with 13,600, they lost 4,400 of
those. Now Russia is in the process, their hope is to bump their numbers back
up and have active duty of 1.5 million. They are relying heavily on recruiting
people that Putin views as undesirable basically. The thing about that is they
generally do not actually make good soldiers. So there's a distinct
possibility that the rates may actually end up going up as wild as that is. So the
The question is, why is the US releasing this right now?
Something like this doesn't get released without a reason.
Because by putting this out, it's the US saying, this is what we believe.
How accurate it is gives Russian intelligence a view of how accurate US estimates are.
So you can assume that this is about as close to accurate as the U.S. can get.
They will render it a little bit inaccurate to kind of skew the opposition, but not by
much.
So this is what the U.S. actually believes.
Why release it?
there are a bunch of politicians in the United States who keep asking, where is the money going?
That's where it's going. That's where it's going. I mean, we talked about it in another video. It's
actually going to a whole bunch of places in the United States where the money is going. But the
product of that money being shipped overseas, this is what it's doing. The Biden administration,
I think is making an error in assuming that the people asking that question don't actually
know already.
Their reluctance to support the aid doesn't come from them not understanding what's happening
for the scale of what's happening.
Their reluctance comes from the fact that they share a far right-wing ideology with
Putin.
don't want Russia to lose. I understand why the administration released this but
I don't think it's going to matter. I think that those people who hold that
opinion, I don't think it's going to sway them. There is probably enough support
to get the aid pushed through. It's going to take some wheeling and dealing like
we talked about earlier. The Democratic Party is going to have to give up some
stuff that they don't want to give up, which will probably hurt them in the
election, but they really don't have a choice if they want to get the aid
through. It's important to acknowledge that if you hear people, if you hear
politicians ask what it's going to after these numbers were released or say that
it's being wasted in the sense of some kind of corruption or whatever. You have
to acknowledge now that these numbers have been released, if they're making
those cases it's because they want US foreign policy to fail. They have
chosen a far right-wing ideology over US national interest. Now understand there
are certainly people watching this video who are like I want US foreign policy to
And that's fine. You're not a senator. You're not somebody who, theoretically, is supposed to be advancing U.S. foreign
policy.
After these numbers have come out, those who are opposed to the aid on the idea that it is corruption or being wasted or
they don't know what's going on, it's all a front.  They can't actually believe that now that they have seen these
numbers.
Those who oppose it because they're interested in peace or whatever, that's an entirely
different thing.
I understand why the administration put these out when it did.
you don't know, there is a lot of high-level stuff going on right now when
it comes to trying to push the aid through. I understand why they were
released, I just don't think it's going to matter to a lot of the people who are
willing to literally cause the US to fail so they can support an
ideology that has always failed. They have to make the U.S. lose so they can
feel better about their Twitter talking points. Keep in mind, like with any
conflict there are more civilian lost than combatants.
Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}