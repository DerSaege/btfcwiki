---
title: Let's talk about Biden's off-camera comments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hnaIzaOeINc) |
| Published | 2023/12/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden made off-camera comments about Netanyahu's government issues and the two-state solution, implying a disagreement with the current Israeli government.
- Biden expressed concern that Israel is running out of time and facing global backlash due to indiscriminate bombing.
- Israeli officials mentioned a disagreement with the US regarding the "day after Hamas" strategy.
- The US plans to exert diplomatic pressure but not take significant action during the Israeli offensive against Hamas.
- For the US plan to work, Hamas must be degraded, Palestinian Authority needs to govern Gaza, and an international coalition must act as peacekeepers.
- Israel seems hesitant about the US plan, creating diplomatic tension between US and Israeli officials.
- The US is banking on diplomatic pressure to achieve its goals in the conflict, but doubts remain about the feasibility of the plan.
- If Hamas remains combat effective, the Palestinian Authority won't be able to govern Gaza effectively.
- Economic consequences may influence Israel's offensive actions as international pressure mounts.
- US foreign policy appears to rely heavily on the success of a challenging plan that experts have deemed difficult.

### Quotes

- "The US is going to exert diplomatic pressure only, period, full stop."
- "This is not me saying, yay, this is good. This is me saying what's happening."
- "US foreign policy is hinging on the successful completion of something US advisors said was really ill-advised."

### Oneliner

Biden's off-camera comments reveal US-Israeli tensions, with the US banking on a challenging plan for Gaza's future amid diplomatic pressure.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor diplomatic developments closely and stay informed about US-Israeli relations (implied)
- Advocate for peaceful resolutions and humanitarian aid for Palestinian civilians affected by the conflict (implied)

### Whats missing in summary

Insights on potential humanitarian impacts and grassroots efforts to support affected communities.

### Tags

#US #ForeignPolicy #Israel #Palestine #Diplomacy


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about
Biden's off-camera comments,
what they mean for US foreign policy,
and what it means for the future of the situation.
Okay.
There were also some comments
that were made by Israeli officials,
and they all factor into a certain outcome.
Okay.
So if you miss the news, Biden was at a, I believe fundraiser, either way
he was at an event, it was off camera.
When he is off camera, he speaks a little bit more freely in this, he said that
Netanyahu had some issues within his government, basically that the right wing
parties were exerting too much influence and that Netanyahu needed to change his
opinion when it came to a two-state solution, because Biden and US foreign
policy supports that, the inferences that the current government in Israel
does not, that Netanyahu does not.
Biden also said that Israel was kind of running out of time, that world
opinion was turning against them, and it was due to, quote, indiscriminate
bombing. That's pretty sharp language. Okay, now Israeli officials have said
that there is a disagreement with the US when it comes to what they are
apparently in conversation calling the day after Hamas. Alright, so what does
this mean? If you watched the video about the UN vote, went through and we're
talking about the three things that it would take for that not to be an
absolutely horrible decision on the part of the US foreign policy-wise, that's
what's happening. The US position is that they're basically going to stomp their
feet and say bad, bad, don't do that, but not really do anything significant while
the Israeli offensive is going on in hopes that the Israelis can degrade
Hamas to the point where their combat ineffective. As I said in that video I am
skeptical that that is going to occur to the degree that it needs to for the rest
of U.S. foreign policy to play out. The trouble with debating, quote, the day
after Hamas, you have to actually get to that point. And people familiar with
these sorts of conflicts have seen multiple the day after IS or the day
after AQ, both of which are still active organizations. Okay, so that's the first
thing that has to occur. The second thing is that the Palestinian Authority has to
be able to assert governance over Gaza. That can happen. If step one actually
occurred, step two could occur very easily because the international
community would just route all of the aid through the Palestinian Authority.
therefore they have de facto control pretty quickly. And then the third thing
is the international coalition made up of Arab nations willing to act as
peacekeepers and stand between the Palestinians and the Israelis. That's the
US plan. The Israeli official commenting that there are some disagreements when
it comes to the day after.
That indicates that Israel is not necessarily on board with this.
Those comments were made before Biden's comments.
So what you can safely assume from that is that the U.S., when it comes to the actual
conflict, what you are seeing on your screens, the U.S. is going to exert diplomatic pressure
only, period, full stop.
That's the plan.
After that, you will probably see US and Israeli officials go head to head, again, diplomatically.
When it comes to the US trying to make this plan happen and Israel not really wanting
it but at the same time not actually having an alternative, at least not one that they
have publicly put out or even hinted to.
So that's where the situation is.
I have had a number of messages about this type of video.
Please understand that this is not me saying, yay, this is good.
This is me saying what's happening.
And I would like it noted that those three steps, they weren't anywhere in public conversation,
but they were all confirmed by statements made within the last 24 hours.
So this is the rough sketch for what the U.S. wants.
I have a lot of doubts about step one occurring to the level that it would need to.
Understand that if the Israeli offensive cannot degrade Palestinian forces to the point where
they are combat ineffective, the Palestinian authority doesn't have any chance of being
able to assert governance because those forces will operate against the Palestinian Authority.
That's step one and it's not impossible, it can occur, but I'm skeptical that it's
going to especially given the fact that Israel is getting a lot of pushback from
the international community and while Israel has a habit of just being like
we're doing what we have to take it up later that only goes so far and they
can start to see economic consequences because of it at some point those
economic consequences might influence the government there and reduce the
length of time that offensive is going to run. And they were under the
impression, it certainly appears, that they were under the impression that this
was going to be a lot easier than it is. And realistically, it only gets harder
from here.
That first step, that's a big ask.
Everybody, all of the US advisors that went there, all of the public statements from US
defense people, they all said this.
They all said this was incredibly difficult.
Now it looks like US foreign policy is hinging on the successful completion of some
that US advisors said was really ill-advised. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}