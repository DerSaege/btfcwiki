---
title: Let's talk about policy, Biden, and student debt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3Xe-Q8ORfWo) |
| Published | 2023/12/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Policy shifts take time, and it's vital to mark small wins even in setbacks of big policy changes.
- Explains the importance of nuance in discussing big policy changes in the current political climate.
- Illustrates an example of legislation in the Senate concerning hedge funds not owning single-family housing.
- Emphasizes the need for a 10-year period for hedge funds to sell off their inventory to prevent a housing crash.
- Talks about the challenges of understanding large financial numbers like $400 billion for student loan forgiveness.
- Reveals that the Biden administration has forgiven over $100 billion in student debt, impacting millions.
- Notes the lack of recognition for these small wins and the disconnect between commentary and actual events.
- Stresses the significance of acknowledging small wins to understand and appreciate ongoing progress.
- Mentions the motivation and fight sustainability gained from recognizing and celebrating incremental changes.
- Encourages taking a moment to appreciate progress, combat cynicism, and stay engaged in pushing for change.

### Quotes

- "Policy shifts take time, and it's vital to mark small wins."
- "You have to take the time to acknowledge those small wins."
- "Those little changes add up real quick."
- "More than $100 billion has been forgiven."
- "Acknowledging the wins helps motivate you and keep you in the fight."

### Oneliner

Policy shifts take time, marking small wins is vital; embracing nuance in discussing big changes, especially in forgiving student debt over $100 billion.

### Audience

Advocates, Activists, Students

### On-the-ground actions from transcript

- Acknowledge and celebrate small wins in your community's progress (implied).
- Stay informed about ongoing policy changes and their impacts (implied).
- Support initiatives that address financial issues and student debt forgiveness (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the significance of recognizing incremental progress and the impact of policy changes on various sectors. Viewing the full transcript can offer a deeper understanding of the nuanced approach required in discussing and advocating for social change.

### Tags

#PolicyShifts #SmallWins #StudentDebt #Progress #Nuance


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
how policy shifts take time.
How it takes time for policy to actually change.
And we're going to talk about the current political climate
and why maybe we should have a little bit more nuance when
we're talking about big policy changes
and why it is incredibly important
to mark the small wins when there is a setback,
when it comes to a big policy change.
And we're going to do this because a whole bunch of y'all
asked the same question.
And interestingly enough, it was a question
I had before I made the video.
And I actually got the answer before making the video,
but well, we'll get there.
So in a recent video, we talked about how there is a piece of legislation in the Senate.
And if it goes through, hedge funds won't be able to own single family housing.
And hedge fund means more than hedge fund.
It's really just big investment entities.
And they have 10 years to sell off the inventory they already have.
If it was to go through, that's what would happen.
That 10 years.
When I heard that, I'm sitting there, I'm like, why would you let them profit off this
for 10 years?
But I didn't want to be like the person who sent me the message about how aid works and
just not understand something and get super angry because of it.
So I called a friend who understands a lot about economics.
And as soon as he asked the first rhetorical question, I realized why they're giving him
10 years.
I didn't include it in the video, because honestly I thought that was just a moment
of me being like super dense and not picking something up.
But a whole bunch of y'all had the same question, so here's your answer.
I'm going to do it the same way he did it to me.
If you have a whole bunch of something and you put it all on the market at once, what
happens to the price? It drops. Supply and demand thing. It just drops. So if they
were to say give them a year, all of those houses that they have in their
inventory would come on the market at once. It would lower property values
which if you're trying to buy a house right now you're probably like yeah do
that. The problem is it would probably drop it below what it really should be
and more importantly below what people owe because it's going to drop other
property values as well. So if somebody had already bought a house and now they
have to move when they go to sell it they're underwater. They owe more than
they'll be able to get from it. So it ends up basically bankruptcies and
foreclosures, it would create a housing crash. That's why they give them 10 years.
It's that simple. And yeah, I did. Like once it was explained to me, I was
like, wow, that's really obvious. I feel dense for not having picked that
up right away. But yeah, that's the reason. Okay, so big policy changes, they take
In today's political climate, having nuance about anything, well, it doesn't get clicks, you know,
it doesn't drive traffic. You want to have that hard opinion. And because of that, people don't
track the small wins. But sometimes those small wins add up real quick. Give you a good example.
example, one of Biden's big promises, student loan forgiveness, right, going to get rid
of that student debt, $400 billion for 10 million borrowers.
That was the plan.
That was the big sell right there.
And yeah, he did his part, pushed it through, went to the Supreme Court, got shot down.
From that point forward, he failed on student loan forgiveness.
He didn't do anything.
Another broken promise.
Something like that.
The thing is, there have been a bunch of small wins along the way.
How much do you think has been forgiven?
What if I was to tell you the Biden administration has forgiven more student debt than any other
administration in history?
Like by a lot, like it's not even close.
When you're talking about big numbers, they become imaginary.
Like nobody knows what they would do with $400 billion, right?
It's hard to wrap your mind around that much money.
So let's do it a different way.
I think most people can understand $100 million because most people understand $1 million.
So $100 million hundred million dollars.
To get to a billion, that's a ten.
To get to ten billion.
That's times 10.
Again.
To get to a hundred billion that's times ten.
Again.
We're still not to the level that has been forgiven.
It was all done in little chunks, $9 billion here, $3 billion here, $5 billion this week.
And I want to say the grand total right now is $127 billion plus the five from this week,
132, impacting three and a half million borrowers.
The failure is, I mean, that's a pretty big chunk right there.
But because we don't track the small wins, nobody talks about this.
Nobody acknowledges that we are talking about more than $100 billion.
it doesn't the little numbers they don't get clicks nobody really talks
about it nobody cares unless you're you know in the 800,000 that that particular
group what was impacted by that that particular section that got forgiven you
care about it then but when it's done little by little nobody really notices
which is why he's been able to do a hundred billion dollars worth because
you're not getting a lot of pushback at three billion here and five billion
there. So what you have is a disconnect between the commentary and what's
actually occurring. If Biden had came out and said that he was going to
forgive a hundred billion dollars in student loan debt, the Republican Party
would have lost their mind. There would have been no difference between the
four hundred billion dollar and the one hundred billion dollar number. But saying
Getting that $400 billion number, the little bits and pieces didn't matter so much.
They kind of just let it slide by.
Policy shifts take time, and it doesn't matter what policy you're talking about, what form
of social change you are looking for.
You have to take the time to acknowledge those small wins.
If you don't, you will miss something major occurring and you may even be opposed to somebody
who did it because you didn't see it happen the way you wanted it to.
More than $100 billion is huge, but because of the way commentators are, I would imagine
that most people, A, don't know that that much has been forgiven, and B, probably feel
like Biden, after it hit the Supreme Court, he didn't do anything else.
That's not true.
I mean, he's doing something this week.
There's another $5 billion that's
being added to that total.
And there will be more, because that's how they're doing it,
doing it little pieces at a time.
It doesn't matter the calls that you care about.
There are wins.
Taking a moment to acknowledge them, not just
Does it help you have a more accurate picture of what's going on?
It also helps motivate you and keep you in the fight.
You don't become quite as cynical and feel like nothing will ever change because you
get to see the little changes.
And those little changes add up real quick.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}