---
title: Let's talk about Trump, recordings, and Michigan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3IDdYAfU2m8) |
| Published | 2023/12/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- News broke about a reported recording of a phone call on November 17th, 2020 involving Trump, McDaniel, and Michigan officials.
- Trump and McDaniel were allegedly pressuring officials in Michigan not to certify the election results.
- Key phrases from the call include Trump saying officials "They would look terrible if they signed" and McDaniel urging not to sign, saying they will provide attorneys.
- The call aimed to prevent the certification of Wayne County votes, potentially affecting around 880,000 people.
- This was another attempt by Trump to alter the election outcome in Michigan, where Biden won by 154,000 votes.
- The recording may play a significant role in legal cases and could come up in federal and Georgia cases.
- While Beau hasn't heard the recording personally, its contents are widely reported.
- This recording adds to the evidence that may strengthen legal cases against Trump's interference in the election.

### Quotes

- "They will look terrible if they signed."
- "We'll take care of that."

### Oneliner

News broke about a recording of Trump pressuring Michigan officials not to certify election results, potentially affecting 880,000 voters, adding to legal evidence.

### Audience

Legal activists, concerned citizens

### On-the-ground actions from transcript

- Contact legal advocacy organizations to stay informed and support efforts against election interference (suggested)

### Whats missing in summary

Full context and emotions conveyed in the actual recording.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump and McDaniel and recordings and Michigan,
because some more news broke.
It is not great news for the former president.
Okay, so there is a reported recording of a phone call that took place on November 17th,
2020. It is reportedly Trump and McDaniel talking to officials in Michigan and attempting
to get them to not certify the results. Key phrases include Trump telling them that they
They would look terrible if they signed.
Reportedly RNC Chair McDaniel said, if you can go home tonight, do not sign it.
We will get you attorneys.
Trump adding to that, we'll take care of that.
Yeah, so this is another recording.
This would have, if this drive had been successful and those votes did not count from Wayne County,
I think you're talking about 880,000, 878,000 people, something in there, and they would
have been deprived of their vote in the 2020 election.
It's another phone call with Trump reportedly pressuring officials to not certify the results.
Keep in mind, Biden won this state by 154,000 votes.
This was apparently another attempt to alter the outcome of the election and, well, I mean,
I guess the term that Trump likes to use for this is election interference.
Now I'm sure to the Trump supporters this is just yet another witch hunt.
I feel like we're going to see this material again.
It will almost certainly come up in the federal case.
we're not 100% certain about what Michigan itself is going to do. And, I mean, there's a slim
possibility that we might hear about it in Georgia as well. It is worth noting that I have not
personally heard this recording yet, but the contents of it are widely being reported on.
So it's another example, it's another block. This is something that will
certainly bolster Smith's case if he didn't already have this. It's important
to remember as information like this starts to kind of come out, that there's
probably a whole lot of evidence that Smith's team has that we are not aware
of yet. And we will hear more about it as time goes on. But this one's probably pretty significant.
But we'll definitely be hearing about this recording again.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}