---
title: Let's talk about Rudy's next chapter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8Hrhd8qWBKs) |
| Published | 2023/12/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Rudy Giuliani has filed for Chapter 11 bankruptcy, buying time for a potential appeal.
- Giuliani lists his liabilities between $100 million and $500 million, with assets of $10 million.
- Plaintiffs in the Georgia case are actively seeking to put liens on Giuliani's properties and discover his income sources.
- Giuliani's team must be cautious in disclosing information during the bankruptcy proceedings to avoid negative consequences.
- The current situation seems like a delay tactic amid existing financial issues.
- The Rudy Giuliani saga continues, indicating that it's not over yet.

### Quotes

- "Rudy Giuliani has filed for Chapter 11. He's filed for bankruptcy."
- "This is one of those moments where Rudy's team has to be incredibly careful."
- "The financial situation was not great, but I feel like this occurred at this moment to buy time."

### Oneliner

Rudy Giuliani files for bankruptcy as a delay tactic amid financial troubles and ongoing legal battles, requiring caution in disclosures.

### Audience

Financial advisors

### On-the-ground actions from transcript

- Analyze the financial implications of filing for bankruptcy (implied)
- Seek legal advice on disclosing financial information during bankruptcy proceedings (implied)

### Whats missing in summary

Details on the specific legal issues leading to Giuliani's bankruptcy filing. 

### Tags

#RudyGiuliani #Bankruptcy #LegalIssues #FinancialTroubles #DelayTactic


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit more
about Rudy Giuliani and how things have progressed
since yesterday, because there have been some developments
already that are worth noting.
The most important, and definitely the thing
that's getting the headlines, is Rudy Giuliani
has filed for Chapter 11.
He's filed for bankruptcy.
A lot of questions about that.
Realistically, this is probably something
to buy time for an appeal.
That might really be the main driving factor here.
He theoretically could ask the judge
to say that he doesn't have to pay that judgment.
the $146 million to the plaintiffs in the Georgia case,
but given the nature of it and the findings in it,
that seems incredibly unlikely.
So it seems as though this is more about buying time
for an appeal and maybe just pushing things off
a little bit.
All that being said, according to the paperwork
that has been reported, he lists his liabilities
as somewhere between $100 million and $500 million
to include a million in taxes.
His assets are apparently $10 million.
So it seems relatively clear that even without the Georgia
judgment, there were financial issues already. Okay, so from the other side, from
the plaintiffs, they are already actively looking to put liens on his properties
and discover his sources of income and try to identify all of those. Going
Going through that process at the same time as him putting in paperwork that is supposed
to disclose a whole lot of stuff, this is one of those moments where Rudy's team has
to be incredibly careful.
They have to be very careful about what they say and what they show in the bankruptcy proceedings
because it could go really bad if they forgot a house, property, stream of income, and the
plaintiffs in Georgia then discovered it.
That could be really bad.
So at this point, I would view this mostly as a delay tactic that has some actual grounding
in reality, assuming that all of the numbers about the hundreds of millions are accurate.
The financial situation was not great, but I feel like this occurred at this moment to
by time. Could be wrong about that, but that's the appearance anyway. So unsurprisingly,
The Rudy Saga has not quite yet ended.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}