---
title: Let's talk about Wisconsin and impeachment being super unlikely....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Zm3PPJqQbLA) |
| Published | 2023/12/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Justice Protozeewicz identified Wisconsin as gerrymandered while seeking office on the Wisconsin Supreme Court.
- Republicans were nervous about her involvement in a redistricting case and wanted her to step down, but she refused.
- Republicans threatened impeachment over her refusal to step down.
- The Republican Party, specifically the House Assembly, heavily pursued the idea of impeaching her for a while.
- The leader of the GOP in Wisconsin stated that impeachment is now "super unlikely."
- It seems that the Republicans understand the potential backlash of impeaching Justice Protozeewicz.
- Justice Protozeewicz is currently not in danger of being impeached, and the long-running story may be coming to a close.

### Quotes

- "Impeachment was, quote, super unlikely."
- "If Republicans hold that position and then try to stop her from correcting it, they're creating a situation where you have even more upset people about it."

### Oneliner

Republicans back down from threatening impeachment against Justice Protozeewicz over redistricting concerns in Wisconsin.

### Audience

Wisconsin voters

### On-the-ground actions from transcript

- Contact local representatives to express support for fair redistricting (implied)

### Whats missing in summary

The full transcript provides a detailed account of the political saga surrounding Justice Protozeewicz and the Republican Party's stance on redistricting in Wisconsin.


## Transcript
Well, howdy there, internet people, let's vote again.
So today we are going to talk about Wisconsin
and the Wisconsin Supreme Court and redistricting
and justice protest site widths,
as well as, well, a Republican change of heart.
They've decided that, you know what,
they're okay with something now.
Okay, so to catch everybody up on this saga as quickly as possible, Justice Protozeewicz,
while seeking office, while trying to become Justice Protozeewicz on the Wisconsin Supreme
Court, she correctly identified the fact that Wisconsin is gerrymandered.
Because of this, Republicans were super nervous about her possible involvement in a redistricting
case, which would look at things that were, you know, widely viewed as gerrymandered.
They wanted her to step down and not be involved in that case.
She was like, no, not doing that.
They threatened impeachment over this, they said they were going to impeach her.
We talked about it as all of this was going on, and it seemed like a really horrible idea
for Republicans to try to do that because it's just one more thing that the voters
there would recognize as them wanting to rule rather than represent.
One of the reasons Justice Protozei was won was because she correctly identified the gerrymandering.
That seems to be the position.
If Republicans hold that position and then try to stop her from correcting it, they're
creating a situation where you have even more upset people about it.
And we talk about this at the time.
The Republican Party, specifically the House Assembly, they really went all in on this
idea of impeaching her for quite some time, and there was a lot of rhetoric about it.
Something has occurred because now the leader of the GOP over there on this, well, he said
that impeachment was, quote, super unlikely.
That's a real quote.
I'm not making that up.
That's actually what he said.
So it does appear that they understand that the backlash that they would receive for engaging
in that might actually be enough to overcome the gerrymandering, which would present problems
for the Republican Party as a whole in Wisconsin.
So as of right now, it appears that Justice Protesiewicz is not in danger of being impeached.
This long-running story may finally be coming to a close.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}