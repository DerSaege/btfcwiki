---
title: Let's talk about non-profit admin expenses....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3ddA7XuaKNE) |
| Published | 2023/12/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of looking beyond administrative salaries when supporting non-profits and charities.
- Shares a personal story of a marketing fundraising professional who significantly increased donations for a struggling organization.
- Emphasizes the value of hiring skilled individuals, even if it means higher administrative salaries.
- Compares the skills of those running large logistics for nonprofits to Fortune 500 company executives.
- Suggests evaluating administrative salaries as a percentage of revenue and program expenses to determine effectiveness.
- Advises talking to individuals who have used a charity's services to gauge its impact.
- Acknowledges that high administrative salaries may seem concerning but could be justified if they contribute to the organization's goals.
- Acknowledges the necessity of resources in addressing societal issues and supporting non-profits.

### Quotes

- "Fights like that, they take resources."
- "High administrative salaries may seem concerning but could be justified if they contribute to the organization's goals."
- "Look at it in the context of the overall amount of funding."
- "You're going to see what they do firsthand."
- "It is a fight and fights like that they take resources."

### Oneliner

Beau explains the nuances of evaluating administrative salaries in non-profits and charities, stressing the importance of impact over numbers and resources in addressing societal issues.

### Audience

Supporters of non-profits

### On-the-ground actions from transcript

- Talk to individuals who have used a charity's services to understand its impact (suggested).
- Analyze administrative salaries as a percentage of revenue and program expenses to gauge effectiveness (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on evaluating administrative salaries in non-profits and charities, focusing on impact assessment beyond numbers and acknowledging the resource requirements in addressing societal challenges.

### Tags

#Nonprofits #Charities #AdministrativeSalaries #ImpactAssessment #ResourceAllocation


## Transcript
Well, howdy there, internet people. Let's bow again. So today we are going to talk a little bit about non-profits
and charities and administrative salaries. Because in a recent video I said that the
financials weren't necessarily the most important thing when we're looking at organizations to support.
and I had a couple of people send in messages asking about administrative
salaries and they've basically all said that you know a big cue for them was
that if the administrative salaries were really high then you know that they
didn't think that was one that they should support. I used to believe the
exact same thing but over the years I have met some of the people who have
those salaries and it one person in particular made me start paying more
attention to it and I'll tell you a story but it applies to a lot of other
stuff as well. So I met her twice in two different places working for two
different charities and we became friends. She was, is basically a marketing
fundraising event coordinating goddess and she kind of works like a hired gun
for nonprofits. The most recent job that I'm aware of, I know she's moved on from
here but she left here and then went up north somewhere then I think she went to
Tennessee and it was that one where I actually got the numbers because I was
curious when we were talking and she gets paid a quarter million dollars a
year 250 thousand dollars a year when she showed up at this place the
previous five years their donations the amount of money they had coming in had
been flat. They were making their operating expenses but they they didn't
have enough to like fix anything when it broke. Like they were literally at the
point of like we need to do a bake sale. And they hired her. The first year after
she was there the revenue, the donations that came in, went up three million
dollars. The second year they went up an additional five million dollars. Over
those two years, yeah, they paid her half a million dollars, but they netted 7.5.
It seems like a good investment.
And it's important to remember that when you are hiring people that have specialty skills
like that, who are really good at what they do, you don't have to be competitive.
is, oddly enough, a quarter million dollars isn't competitive. She could go to work at
a resort and make way more. But she enjoys what she's doing, and she likes helping,
and she likes moving around every couple of years. And then when you look at organizations
that have a lot of logistics, there are some massive organizations that have logistics
similar to Walmart, a lot of these nonprofits, and obviously here we are talking about not
mutual aid, we are talking about legal entities that have like bookkeeping and stuff like
that they have to do.
The people who can successfully run those, they could also successfully run a Fortune
500 company.
So they don't have to be competitive as in paying the same, but if they're not close,
you're not going to get those people.
Everybody likes to frame it as a war on whatever, whatever their cause is.
It's a war on poverty, a war on homelessness.
a war on whatever.
The one thing I think that everybody watching this channel knows is that wars cost money.
So what I normally do is look at the administrative salaries as a percentage of the revenue, like
of how much they have coming in and what their program expenses are and do a comparison.
Now if they're spending more on administrative salaries than they are like actually doing
what the charity's supposed to, yeah that's a super bad sign.
But just because it hits, you know, seven figures total, that's not necessarily a bad
thing because it can, it might be a good use of the funds to bring in more or to
set up the logistics to keep stuff rolling. It's, if you have, if you run
across a charity and you're looking at trying to decide whether or not to
support it and you notice those high administrative salaries, talk to
somebody who's used their services. That is always going to be a better test
of how good they are than just about anything else because you're going to
see what they do firsthand and then you scale it based on the amount of people
they help per year it's on their website or you know the amount of money they
have towards a certain goal or whatever and look at it in context of the overall
amount of funding. Because I can imagine if you're looking at a charity and you
see that they're paying a quarter million dollars a year for somebody to
plan parties, you might not look at that as a good use of funds. But if those
parties and those events bring in the money that actually allows the
non-profit to do its job. I mean it's not great none of this should exist but in a
perfect world the non-profits wouldn't exist because the problems would be
solved. It is a fight and fights like that they take resources. Anyway it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}