---
title: Let's talk about the UN and abstaining....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gxLt691aroo) |
| Published | 2023/12/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of a recent United Nations vote, particularly in the Security Council versus the General Assembly.
- Describes the content and implications of the resolution passed at the UN, focusing on humanitarian aid and access in Gaza.
- Notes the absence of a direct call for a ceasefire in the resolution.
- Addresses the reasons behind countries like the US and Russia abstaining from the vote, citing their desire for specific condemning languages that were not included.
- Emphasizes the importance of quick and increased aid delivery, despite the lack of certain elements in the resolution.
- Urges universal support for the aid efforts and expresses uncertainty about an imminent ceasefire call.
- Concludes by stressing the critical nature of aid delivery and the ongoing negotiations at the UN.

### Quotes

- "Everybody needs to hope that aid starts getting in there and getting in there quickly."
- "What people were hoping for did not occur."
- "It is a hopeful sign on the humanitarian front."
- "Those people, if you were waiting for the UN to take action, what you were waiting for, it didn't happen."
- "By the time all the negotiations were done, it really turned into the Security Council calling for more aid to get in quickly."

### Oneliner

Beau breaks down a recent UN vote, noting the emphasis on humanitarian aid in Gaza and the absence of a direct ceasefire call, urging universal support for quick aid delivery.

### Audience

United Nations observers

### On-the-ground actions from transcript

- Support and advocate for quick and increased humanitarian aid delivery to Gaza (implied).

### Whats missing in summary

Deeper insights on the nuances of international negotiations and the dynamics that impact UN resolutions.

### Tags

#UnitedNations #UNVote #HumanitarianAid #Ceasefire #InternationalRelations


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the United Nations.
We're gonna talk about the vote up at the UN.
We're going to talk about what's in it, what's not in it.
We're gonna talk about why countries voted the way they did
because there's some questions
about the countries that abstained.
And just kind of run through what this really means
because there was a lot of negotiation
prior to this moving through and there is at least some confusion
about what actually made it into the final resolution.
Okay, the first thing to know and probably the most important
is that this didn't make it through the General Assembly.
This made it through the Security Council.
Getting something through the Security Council is much harder
then getting something through the General Assembly.
At the Security Council, there are multiple nations
that have veto power.
They don't like it, no.
And it doesn't matter if every other country
is in favor of it.
They can veto it by themselves.
In this case, with this resolution,
every country with veto power,
they either voted in support of it
Or they abstained.
So it went through.
OK.
Most people who are waiting for the UN to act, what you want
is a call from the Security Council for a ceasefire.
That is not in this.
That is not part of this.
calls for the creation of the conditions for a cessation of hostilities.
It doesn't actually call for a ceasefire.
What is in it?
This is the important part.
It wants urgent and extended humanitarian pauses and corridors throughout the Gaza Strip
a sufficient number of days to enable full, rapid, safe, and unhindered humanitarian access.
That's what's in it. That's the important part. By the time all the negotiations were done it
really turned into the Security Council calling for more aid to get in quickly. For those who are
waiting for a call for a ceasefire, that doesn't sound like much. The people that
I have talked to suggest that you're looking at days, not weeks, before the
humanitarian issues spiral way out of control. This may not have been the win
you are looking for, but this is important to everybody. It doesn't matter
where you're at as far as which side you're on when it comes to the conflict.
This is important for everybody. Now, what's in it? It is supposed to provide
more aid faster. And in theory that's what's supposed to happen. I say in
theory because there are still things that can disrupt that. But that's
what the call is really about now. The negotiations as far as a cessation or
suspension when it comes to the hostilities, it just didn't go anywhere.
So all of that kind of got left out. Okay, so why did countries vote the way they
did? If it's really just a call for aid, why did both the United States and
Russia abstain? Why wouldn't they just support it, right? Both countries wanted
wanted language that condemned an action. The U.S. wanted language that condemned the
October attack. Russia wanted language that condemned Israeli air operations. That's
that was what led to that. Neither got what they wanted, so they both abstained, which
Which realistically, probably the best scenario because if they both had got what they wanted,
they probably both would have vetoed it.
So that's where it's at.
What people were hoping for did not occur.
Those people, if you were waiting for the UN to take action, what you were waiting for,
it didn't happen.
what did happen is important and it may it may be more important than people
realize. So we all, no matter where you're at, everybody needs to hope that
that aid starts getting in there and getting in there quickly. Okay so that
pretty much wraps up all the questions that came in about this. The other ones
are, you know, when are they actually going to call for a ceasefire? I don't
know that they're going to. All of the questions were phrased, when is that
going to happen? I don't know that that they will. They were, they were talking
and negotiating about terminology related to that and it didn't make it.
Now they may still be talking about that, but the AIDS situation, it might have
just taken precedence and they were like, okay we need to get this through now.
We'll see how it plays out because again there are multiple ways for
this to still not work.
But it is a hopeful sign on the humanitarian front.
Anyway, it's just a thought.
I hope you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}