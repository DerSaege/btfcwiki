---
title: Let's talk about the misery index telling us to be less miserable....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Oc8T0oyvh_4) |
| Published | 2023/12/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of the "Misery Index" as a key economic indicator often overlooked.
- Defines the Misery Index as the sum of the inflation rate and the unemployment rate.
- Points out that the Misery Index is a more accurate reflection of economic conditions for everyday people than traditional economic metrics.
- Shares historical context: In June 1980, the Misery Index reached its highest point at 21.98.
- Mentions that since the 2000s, the average Misery Index has been 8.3, currently standing at 6.8, the lowest since the pandemic started.
- Notes that economic indicators favored by economists may not immediately impact average people, but current conditions suggest improvements are reaching them.
- Reports a shift in outlook from Bank of America regarding the likelihood of a recession, now leaning towards a more positive outcome.
- Emphasizes that presidents do not control the economy, attributing economic improvements to factors like consumer confidence and effective spending of injected money.
- Concludes that people should start feeling the positive economic changes that have been discussed for the past six months.
- Wraps up by leaving viewers with a thought to ponder on.

### Quotes

- "The misery index is basically adding the rate of inflation with the seasonally adjusted unemployment rate."
- "Presidents don't actually control the economy."
- "It does appear that people should finally start actually feeling the improvements in the economy that everybody's been talking about for half a year."

### Oneliner

Beau introduces the "Misery Index," an economic indicator reflecting everyday conditions, noting recent improvements and cautioning against crediting presidents with economic changes.

### Audience

Economic enthusiasts

### On-the-ground actions from transcript

- Monitor economic indicators and understand their impact on average people (implied).

### Whats missing in summary

Historical context and detailed explanation of the Misery Index calculation.

### Tags

#Economy #MiseryIndex #BankOfAmerica #ConsumerConfidence #EconomicIndicators


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the economy,
but we're going to talk about the economy using an index
that isn't often discussed, but for most people,
it might be the more important one.
It was developed back in the 1970s,
and it was originally called something
like the Economic Discomfort Index,
or something along those lines.
As time went on, it became known as the misery index.
The misery index is basically adding the rate of inflation
with the seasonally adjusted unemployment rate.
It is a useful, if imprecise, metric
for looking at the economy and how it impacts normal people.
When a lot of people talk about the economy,
they are focused on the overall economic health
of the nation.
Understand, a whole lot of money can be changing hands,
but none of it could be reaching the people on the bottom.
So those metrics aren't really good for understanding
where people should be at and where the the real economic conditions are for the average person.
The Misery Index is one of the closest things you get to it.
Okay, so let's establish a baseline. What's the highest the Misery Index has
ever been? That was in June of 1980, and it was at 21.98.
Okay, since the beginning of the 2000s, it has been at 8.3. That's the average.
8.3. So definitely better than the 1980. It is currently at 6.8. It has dropped to 6.8.
That is the lowest it has been since the pandemic started. So we have been talking for quite
some time about how those economic indicators that economists look at that
really impact people that have a whole lot of money, like those are saying, hey
things are getting better, but it takes a while for the average person to fill
that. The conditions are now there for the average person to start filling it.
that's what this says. Now it is also worth noting in like related economic
news, Bank of America has repeatedly said that recession was on the way,
recession was on the way, and this has been a running theme. They have now
shifted their outlook and they are saying that it does appear more likely
that the soft landing sticks. So I know, given the nature of this channel, most
people are going to want to immediately credit Biden with this. Just remember the
constant thing I say, you know, presidents don't actually control the
economy. Consumer confidence had a whole lot to do with this. I think that that
may have been a very integral part of it. I mean, don't get me wrong, pumping the
money in helped, but pumping the money in without it being spent wouldn't have
done any good.
But it does appear that people should finally start actually
feeling the improvements in the economy that everybody's been
talking about for half a year.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}