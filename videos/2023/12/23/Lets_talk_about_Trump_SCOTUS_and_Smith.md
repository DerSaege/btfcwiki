---
title: Let's talk about Trump, SCOTUS, and Smith....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zEIrl4i2mXA) |
| Published | 2023/12/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of Trump's legal situation, Beau mentions his claim of presidential immunity in a DC case.
- Trump's claim was rejected by Judge Chuckin, leading him to appeal to the appeals court.
- Smith attempted to expedite the case to the Supreme Court, but they declined to hear it out of order.
- The Supreme Court's decision not to hear the case early is not necessarily indicative of favoritism towards Trump.
- Beau cautions against jumping to conclusions about the Supreme Court's stance on Trump's cases.

### Quotes

- "I don't know that you can interpret that as, well, they're going to let him go."
- "I can't say that the Supreme Court isn't going to hear the case or is going to decide against him."
- "I can't look at recent events and say that that's the conclusion I would draw."

### Oneliner

Beau analyzes Trump's legal situation, cautioning against assumptions about the Supreme Court's stance.

### Audience

Legal analysts, concerned citizens

### On-the-ground actions from transcript

- Stay informed on legal developments and rulings (implied)
- Avoid jumping to conclusions based on limited information (implied)

### Whats missing in summary

Contextual details and nuances from Beau's analysis can be best understood by watching the full video.

### Tags

#Trump #SupremeCourt #LegalAnalysis #PresidentialImmunity #Smith


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump,
and Smith, and the Supreme Court.
And how people are viewing what occurred,
and whether or not all of the downfeelings
are really warranted.
Because there's an interpretation out there
of the chain of events that, I mean,
I don't know if it really sits with me right.
Okay.
So if you don't know what happened, and we kind of have to run through a lot here.
Trump in the DC case, is making the claim that he has presidential immunity, which is
a thing, but it's, it's very limited and I don't think anybody really believes it
extends as far as he is claiming and to be honest I don't think he believes it
but the claim is that he is immune to these charges because of well this idea
of presidential immunity okay so he made this motion this idea put it before Judge
Chuckin. Judge Chuckin was like, yeah, no, that's not a thing. He appealed it, going
to the appeals court with it. Smith basically tried to get the Supreme Court
to hear it out of order before the appeals court ruled on it. The Supreme
Court said no, they weren't going to do that. People are viewing this as this is
Is the Supreme Court helping Trump?
Yeah I don't know about that, I don't know about that.
If the Supreme Court wanted to help Trump, it seems like they would have said yeah we'll
hear it and then say yes he has that immunity, then his cases go away.
In point of fact they didn't do that.
They let the appeals court hear it.
That's what's going to happen now.
It goes to the appeals court.
appeals court is most likely going to uphold the idea that he is not immune, at which point
Trump could try to take it to the Supreme Court, but what if there's nothing procedurally
wrong?
What if there's nothing for the Supreme Court to rule on?
They don't have to take the case then either.
What we know, and what we know for fact, is that the Supreme Court elected not to hear
the case out of order.
That's it.
That's all we know.
I don't know that you can interpret that as, well, they're going to let him go.
That doesn't even square.
If that was their plan, they could have just done it now.
And then given their candidate, who apparently they're still behind, like that's how the
theory would have to play out that they truly support Trump and they want him to win the
presidency again, it seems like clearing up all of these cases for him early would have
been a good move, but that isn't what they did.
I can't say that the Supreme Court isn't going to hear the case or is going to decide
against him.
What I can say is that them not hearing it out of order, I don't know that I would take
that as a sign that they're super on his side.
Because if he had heard it, if the Supreme Court had heard it, they could have ruled
in his favor, and then given him a year in the clear to run his campaign.
But they didn't do that.
I would be leery about forming any doom and gloom opinions about this.
At the same time, the idea that they're just not going to hear it after the appeals court
It's possible, but we don't know that either.
This is kind of early in this.
I would wait to see a little bit more play out before you assume that the Supreme Court
is just going to throw everything for Trump.
I can't look at recent events and say
that that's the conclusion I would draw.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}