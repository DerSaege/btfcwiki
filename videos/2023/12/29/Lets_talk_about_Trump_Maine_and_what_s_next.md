---
title: Let's talk about Trump, Maine, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YB9tM-nqs0Q) |
| Published | 2023/12/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Secretary of State in Maine decided that Trump will not be on the ballot due to Section 3 of the 14th Amendment after a hearing.
- This decision has led to the situation going to the Supreme Court for a final resolution.
- Multiple states, including Arizona, Alaska, Nevada, New Jersey, and others, are also involved in similar proceedings.
- Despite Trump's circle's rhetoric, it is primarily Republicans and conservatives pushing these actions.
- The core issue at hand is whether Trump's actions on January 6th qualify as engaging in insurrection or rebellion under the 14th Amendment.
- The Supreme Court will have to determine if Trump's actions fit the criteria outlined in the 14th Amendment.
- Until the Supreme Court makes a decision, there will be ongoing developments and no resolution in sight.
- Beau expresses skepticism about the outcome, believing that the Court may find a way to argue that the Amendment does not apply to Trump.
- The final decision rests with the Supreme Court, and until then, all other actions are secondary.
- Updates will be provided as the situation progresses towards the Supreme Court.

### Quotes

- "Democracy is sacred, so on and so forth."
- "It's worth noting that despite the rhetoric being used by those in Trump's circle, most of this is actually being pushed by Republicans, by conservatives."
- "There's going to be a lot of back and forth."
- "But until it gets to the Supreme Court and the Supreme Court has interpreted the Constitution, then we're really not going to know anything."
- "The other proceedings along the way, they don't matter as much, because it's already headed there."

### Oneliner

The Secretary of State in Maine's decision to exclude Trump from the ballot under the 14th Amendment sets the stage for a Supreme Court showdown on whether Trump's actions constitute insurrection.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Stay informed about the developments in the case and the Supreme Court proceedings (implied)
- Support organizations advocating for the protection of democratic processes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the legal implications and potential outcomes of Trump's exclusion from the ballot in Maine under the 14th Amendment.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Trump and Maine
and the 14th Amendment.
What has happened and where it goes from here
and what eventually the Supreme Court
is going to have to actually decide.
If you have no idea why we're talking about this,
the Secretary of State in Maine after a hearing
has decided that Trump will not be on the ballot.
They said they didn't make that decision lightly, democracy is sacred, so on and so
forth, and then went on to say, I am mindful that no Secretary of State has ever deprived
a presidential candidate of ballot access based on Section 3 of the 14th Amendment.
I am also mindful, however, that no presidential candidate has ever before engaged in insurrection.
So what happens now?
This is going to the Supreme Court.
Keep in mind, this isn't just about the proceedings that we've talked about.
On top of the ones we have mentioned on the channel already, we have Arizona, Alaska,
Nevada, New Jersey, New Mexico, New York, Oregon, South Carolina, Texas, Vermont, Virginia,
Wisconsin, and Wyoming.
There's probably more.
Those are just the ones I know about.
So this is going to the Supreme Court.
Now obviously there's a lot of apprehension about that because it is a conservative Supreme
Court.
It's worth noting that despite the rhetoric being used by those in Trump's circle, most
of this is actually being pushed by Republicans, by conservatives.
So we're going to run through the relevant part of the 14th Amendment real quick because
that's going to tell us what the Supreme Court is really going to have to decide at
the end of the day.
Okay so it says, No person shall be a Senator, Representative in Congress, or Elector of
President and Vice President, or hold any office, civil or military, under the United
States or under any State, who, having previously taken an oath as a member of Congress, or
as an Officer of the United States, or as a member of any State Legislature, or as an
Executive or Judicial Officer of any State, to support the Constitution of the United
States, shall have engaged in insurrection or rebellion against the same, or given aid
or comfort to the enemies thereof."
So what does this mean?
The Supreme Court is going to have to decide, is Trump a covered person by this?
Did he take the appropriate oath?
Was what happened on January 6th an insurrection or rebellion?
Did Trump engage in it or give aid and comfort?
That's what they're going to have to decide.
And they're going to have to decide all of those as yes.
If any of them are no, this may not apply.
There are going to be a lot of developments in this saga when it comes to the 14th Amendment.
There's going to be a lot of steps along the way.
There will not be a resolution until that happens.
Until then there's going to be a lot of back and forth.
But until it gets to the Supreme Court and the Supreme Court has interpreted the Constitution,
then we're really not going to know anything.
I've made no secret about the fact that I think this is a long shot.
Because I feel like they will find a way to argue that this doesn't actually fit him.
They will get one of those questions to be answered as no.
But we'll have to wait and see what happens.
And until this goes to the Supreme Court, it's a wait and see attitude, because that's
what's going to decide it.
The other actions, sure, they matter, but at this point, there's enough in motion that
it's going to the Supreme Court.
So the other proceedings along the way, they don't matter as much, because it's already
headed there. But obviously we will provide updates from time to time on how
of this is going. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}