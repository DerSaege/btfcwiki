---
title: Let's talk about a new list in Florida starting New Year's....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hYFBQG08j44) |
| Published | 2023/12/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New registry in Florida starting January 1st created by the Protect Our Loved Ones Act - the Special Persons Registry.
- Voluntary registry for individuals with specific conditions like nonverbal, Alzheimer's, or deaf.
- Aimed at providing law enforcement with vital information to prevent tragic outcomes.
- Information on the registry can be critical in cases of wandering or emergencies.
- Requires documentation of the condition, like a birth certificate.
- Registry setup by sheriff's departments, cross-referenced between counties.
- Aims to prevent misunderstandings and potential tragedies during encounters with law enforcement.
- Acknowledges the need for systemic change within law enforcement culture.
- Despite mixed feelings, recognizes the potential to save lives through this registry.
- Implementation of the database starts on January 1st, likely to happen swiftly.

### Quotes

- "This registry is voluntary."
- "The fact that it needs to is a total indictment of law enforcement culture."
- "Until that occurs, this will save lives."

### Oneliner

New voluntary registry in Florida aims to provide vital information to law enforcement starting January 1st, potentially saving lives despite exposing systemic issues.

### Audience

Floridians, caregivers, advocates.

### On-the-ground actions from transcript

- Register yourself or a loved one on the Special Persons Registry (exemplified).
- Ensure documentation of the condition is available for registration (exemplified).
- Support initiatives for systemic changes within law enforcement culture (implied).

### Whats missing in summary

The emotional impact of the registry on individuals and families. 

### Tags

#Florida #SpecialPersonsRegistry #LawEnforcement #SystemicChange #CommunitySafety


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about something
going into effect on January 1st.
And it's one of those things
that it kind of bothers me that it exists,
but at the same time, I know it's going to save lives.
So, come January 1st, there will be a new registry in Florida, it's authorized by the
Protect Our Loved Ones Act, and it creates the Special Persons Registry.
This registry is voluntary.
You can put yourself on it, or if you are the caregiver of somebody who needs to be
on it, you can put them on it. Things that would be on this registry. The person living
at this address is named Bob and Bob is nonverbal. Jim has Alzheimer's, likes to wander.
Susie is deaf, cannot hear your commands.
We all know the reason this exists, one of the reasons it exists.
The fact that it needs to is a total indictment of law enforcement culture.
At the same time, having that information might prevent really bad outcomes.
Now, other than the obvious reason for this registry existing, it's worth noting that
this information could really come in handy if somebody does wander or if they're trying
to respond and they need to know maybe the kids are drawn to water. That's one
of the examples that was cited putting this together. So it is a registry, it's
voluntary and from what I understand to put yourself or a loved one on it that
you have the ability to do that for. You're making decisions for them. I think
you need a birth certificate, documentation of whatever the condition
is, and they have specific conditions like being on the spectrum, Alzheimer's,
things like that that are listed out, but it also seems to be that's not an all
encompassing list and it seems like there is special consideration for
something that may not be on the list that's important for them to know. The
if I understand this correctly and I might be wrong because it isn't
implemented yet but it sounds like it's going to be kind of set up by the
sheriff's departments and that's where that information would go but then the
registry for each county is cross-referenced so if if somebody winds
up crossing county lines the information is still available. We have seen a lot of
tragedies because law enforcement assumed that the person was ignoring them
or the person was non-compliant because they just had a problem with authority
or whatever. This information is something that, in the best case, can
diffuse any situation that happens before it starts because they're going
to be aware of it as they approach. It also kind of removes the, well we didn't
No. So it is one of those things that I have some mixed feelings about. The fact that this needs to
exist, that should be a sign that other things need to happen to totally revamp how things are
done. At the same time, until that occurs, this will save lives. Anyway, it goes into
affect January 1st the authorization to start creating this database it goes
into effect then. My guess is the the sheriff's departments that they seem
pretty eager to get this so I would imagine this is going to happen pretty
quickly.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}