---
title: Let's talk about a New Year's resolution, a message, and change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kKx6yhN13fU) |
| Published | 2023/12/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Someone seeking advice is reflecting on making a big change in their life after turning their life around following a rough past, including jail time and criminal activity.
- They have secured a good paying job that involves traveling and building storefronts for a chain business, which will keep them away from negative influences.
- Beau encourages the person not to return to their old life and suggests starting fresh in a new location without looking back.
- He advises against reconnecting with old acquaintances or revisiting familiar places to avoid falling back into destructive patterns.
- Beau recommends packing up only necessary belongings and leaving everything else behind to start a new life unburdened by the past.
- Emphasizes the importance of making a clean break and not discussing the old life with others to prevent temptation.
- Urges the person to focus on the new opportunities ahead and fully commit to the chance to start fresh.
- Beau suggests preparing thoroughly to ensure they never have to return to their previous environment once they leave.
- He acknowledges that making significant changes may involve uncomfortable adjustments but assures that the outcome is worthwhile.
- Encourages the individual to embrace the unknown, make new connections, and allow positive change to follow their decision to leave the past behind.

### Quotes

- "Leave and don't look back."
- "Don't go back. My advice is starting today, you start going through your trailer, looking for anything you can't buy again later."
- "You got a once-in-a-lifetime shot. Take it."
- "Don't come back. The change will follow."
- "If you were, you wouldn't want to be a better person."

### Oneliner

Beau advises someone with a troubled past to leave behind familiar environments, start fresh elsewhere, and embrace the chance for positive change.

### Audience

Individuals seeking a fresh start

### On-the-ground actions from transcript

- Pack up necessary belongings and leave everything else behind (suggested)
- Prepare thoroughly to ensure not having to return to the previous environment (suggested)
- Refrain from reconnecting with old acquaintances or revisiting familiar places (suggested)

### Whats missing in summary

The full transcript provides in-depth guidance on leaving behind a troubled past, embracing new opportunities, and making a clean break for positive change.

### Tags

#FreshStart #PositiveChange #LeaveThePastBehind #NewBeginnings #Opportunities


## Transcript
Well, howdy there, internet people.
Let's bail again.
So today, we're going to answer a question and talk about a
New Year's resolution.
Don't worry.
There will be something about Maine and all of that stuff
with the next video.
I just want to get this one out, because it is Friday
before New Year's, and I feel like it might be important for
person who sent this message. I have a question for Bo. I've been wanting to
make a big change in my life for a long time. So long that I was thinking about
it when he did that video about the guy dealing with the college students in his
hood. I don't live in the hood. I'm 26. I live in a trailer park and I live in the
kind of trailer park that gives trailer parks a bad name. To keep it short in
in case he reads this.
I'm not a good guy, but I wanna be.
I've been in and out of jail.
I'm clean and have been for six months.
And I started a really good paying job
the day after New Year's.
I'll be doing traveling and building
storefronts for a chain business.
Not like chains, but like chain restaurants,
but not restaurants.
I'll be gone six months.
When I come back, as long as I do a good job,
I'll leave again two months later for another six months.
This is good for me because I won't be around the people
that tend to put me in the same situations.
I'm almost 30 years old,
and I'm still falling for peer pressure.
Pretty pathetic, right?
I'm single, probably, because I'm a not nice person.
My mom's dead, never knew my dad.
I have nobody in my life except the people here,
so I go with the flow.
I went to jail for stealing converters off cars.
That's where I'm at in my life.
I'm worried that I'm going to come back after six months
and mess up the only good shot I've ever had.
He had really good advice for old dude.
Maybe he's got some for me.
I don't even know why I'm writing,
but I don't have anybody else to ask.
The one thing I've learned since getting clean
is that I don't have anybody.
You may not have anybody, but you've got an opportunity.
My advice on this would probably be the same no matter what,
but it's a whole lot easier for you.
Those jobs, I know those jobs.
I've got friends that do those jobs.
They leave, they're gone.
Got one buddy that does it with elevators,
installing elevators.
He's gone months at a time.
And the thing about the guys that do those jobs, as long as
they don't drink their paycheck or put a
dancer through college, they tend to come back with a lot
of money, like tens of thousands of dollars.
So this is what you do when you are coming back,
and you are heading back home.
Don't stop, don't go back, period.
The one recurring thing here is you don't have anybody.
Yeah, that's sad, and it's probably pretty lonely.
It can be depressing, but it also
means you don't have anybody tying you down. Don't go back. Go a couple towns
over, rent a little place in some working-class neighborhood. I'm sure
there's a reason you haven't thought of this, like, or you you decided against it.
Maybe when mom passed, you know, insurance paid off the trailer or
something. It's not worth it. Whatever it is, it's not worth it. You want to build
your life somewhere else, you have to get somewhere else. If you want a different
life, it's not gonna happen there. So just don't go back. And I mean, ever. Change
your phone. Don't go back to see how they're doing. Don't go back to show off
how well you're doing. You get nostalgic, thinking about the good old days, you
want to see them, go to the Sheriff's Department website. They'll have their
picture. If you go back, they'll have yours too. You got a once-in-a-lifetime
shot. Take it. Do not go back. My advice is starting today, you start going
through your trailer, looking for anything you can't buy again later.
Anything that you can't replace somewhere else, and you pack it up.
Everything else, leave it, and don't go back.
It's probably not going to be much.
When I was in my 20s, it could fit in a duffel bag.
Just don't go back.
the money and start your life somewhere else. Don't think about your old life.
Don't talk about it with people because if you do, what's gonna happen is they're
gonna hear that this is the type of person you could be. So some of them that
maybe enjoy the same substances, well, they'll invite you out.
Just clean break, and then move on from there.
This is a chance, and I think you know that, that the key part based on that message, and
I have known, not to say it's cliche, but I have known you before.
The only ones I still know are the ones that left and never looked back.
That would be my New Year's resolution if I was you.
To get set up, get everything ready, so you do not set foot in that place once you leave.
Once you go to work, you just never go back.
If you do have stuff that you want to keep for whatever reason, put it into storage.
don't go back. When it comes to a new year, people like to make changes. Most
people don't have everything kind of lined up for you. One of the things about
making big changes is that you have to change things you may not want to along
the way, but I promise you it's worth it. Yeah, moving into some place where you
don't know anybody, it's not ideal. It takes a while to make friends,
especially if it's a small town. I'm guessing it is. It may not be, but you
You don't know anybody as well.
That works both ways.
You don't have anybody to hang out with at first, but you don't want to hang out with
the people you know now.
Leave.
Don't come back.
The change will follow.
I don't think you're a not nice person.
If you were, you wouldn't want to be a better person.
You wouldn't care.
Leave and don't look back.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}