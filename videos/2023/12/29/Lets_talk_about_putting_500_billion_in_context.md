---
title: Let's talk about putting $500 billion in context....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MqvmGN2lzUE) |
| Published | 2023/12/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the context of putting $500 billion into perspective in international trade.
- Mentions BRICS, an organization of countries challenging the West and the dollar.
- Responds to a message criticizing his coverage of BRICS and $500 billion in trade.
- Points out the significance of $500 billion in international trade within the context of BRICS.
- Compares the $500 billion in trade to Walmart's revenue of $611 billion in 2023.
- Emphasizes that BRICS is not yet on par with major Western economic entities.
- Provides historical context on the terms "first world" and "third world."
- Dismisses doomsday scenarios about the dollar's fate due to BRICS.
- Suggests potential expansions of BRICS involving Egypt, Ethiopia, and Saudi Arabia.
- Notes the geopolitical influence and significance of these potential expansions.
- Stresses the importance of placing large economic numbers in context.

### Quotes

- "For you or I, if somebody walks up and they're like, hey, we're going to give you $500 billion. That's a huge chunk of money, right? That's a massive amount of money. For international trade, it isn't."
- "It's almost Wal-Mart."
- "But it's because of context."
- "The doomsday scenarios about what would happen to the dollar, I don't think that they're incredibly accurate, but that doesn't mean that BRICS can't become a world player."
- "When you were talking about the global economy, $500 billion, it's almost Wal-Mart."

### Oneliner

Beau explains the context of $500 billion in international trade within BRICS, debunking hype and stressing the need for perspective.

### Audience

Economic analysts, policymakers, activists

### On-the-ground actions from transcript

- Research and understand the implications of BRICS and $500 billion in international trade (suggested)
- Stay informed about geopolitical and economic developments (suggested)

### Whats missing in summary

Deeper insights on the potential impact of BRICS and the $500 billion in international trade beyond economic aspects.

### Tags

#BRICS #InternationalTrade #Economics #Geopolitics #Context


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today we are going to talk about putting $500 billion
into context because I got a message
and there's an idea out there
and it's just worth going over.
So we will talk about $500 billion.
We will talk about context.
We will talk about international trade.
we will talk about BRICS, which if you're not familiar with that, it is an organization
of countries that is attempting to become a new economic bloc that would challenge the
West and specifically the dollar.
And I got this message.
You're one of the few that will actually talk about BRICS, but anytime you do, you forecast
it as something that someday might matter, but that it's irrelevant today.
I'd be willing to bet anything you don't cover the World Bank saying they're on track
to have $500 billion in trade between them in 2024.
I like you, but your crystal ball doesn't work for economics.
This is a huge development, and you'll refuse to cover it because it doesn't fit the narrative
that they're just not there yet.
We're challenging the entire West now, not in 10 years."
Yeah I mean I saw it.
I saw the press release and the couple of articles that went out about BRICS maybe next
year having $500 billion in trade.
And you're right, I probably wouldn't have covered it without this message.
have mentioned it. But it's because of context. For you or I, if somebody walks up and they're
like, hey, we're going to give you $500 billion. That's a huge chunk of money, right? That's
a massive amount of money. For international trade, it isn't. It's really not. Four Bricks.
The idea that next year, if everything goes well, they'll have $500 billion in trade
between these nations.
That is press release, breaking news, it's a huge deal.
Do you know what wasn't press release, breaking news, huge deal?
Walmart having a revenue of $611 billion in 2023.
entity within the West. It's not there. It's not there. That's not to say that it
won't ever be there, but it's not there yet. It can chip away at the dollar's
dominance, but it's a long way away from it having a major impact. And it's also
worth remembering that there are a lot of people who who only view the terms
first world and third world in the context of rich country and poor
country. It's not where those terms came from. First world was the West. The second
world was the the communist-aligned countries. This would not be the first
time that there was another economic system at play when it comes to
international trade. The doomsday scenarios about what would happen to
the dollar, I don't think that they're incredibly accurate, but that doesn't
mean that BRICS can't become a world player. It just won't live up to the hype.
the expansions? Yeah, I mean that could matter, particularly the fact that they
might get Egypt, Ethiopia, and Saudi Arabia to come into their economic block.
That's a big deal. Why would Ethiopia matter? Egypt, Ethiopia, and Saudi Arabia
would give them a massive amount of influence over the Suez. That's why it
would matter. Bricks becoming a major player, it's not just going to be about
the economy. The same way the West isn't just about the economy. It's not about
dollars changing hands, it's also about geopolitical influence. They're not
there yet. Could happen, but it's it's not going to be next year. So it's
important, especially when you get to those numbers that are so huge they seem
imaginary and you really can't wrap your mind around them, it's important to put
them in context. $500 billion in international trade. It's not... when you
were talking about the global economy, $500 billion... it's almost Wal-Mart.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}