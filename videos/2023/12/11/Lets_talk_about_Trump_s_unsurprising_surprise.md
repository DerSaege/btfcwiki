---
title: Let's talk about Trump's unsurprising surprise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mBAo3lhgnJQ) |
| Published | 2023/12/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump made a surprise announcement about not testifying in the New York civil case, which was expected.
- Speculation on what happens next in the case.
- The current schedule indicates deadlines for filing briefs by January 5th and closing arguments by January 11th.
- Legal experts believe the case won't go well for Trump, with varying estimates on the potential financial impact.
- Final determination might come in February after closing arguments in January.

### Quotes

- "Trump saying that he's gonna do one thing and doing something else."
- "The question isn't whether or not it's going to go his way. The question is how bad it's going to be for him."

### Oneliner

Trump's surprise announcement about not testifying in the New York case leads to speculation on the case's future, with legal experts predicting a tough outcome for him and potential financial implications, expected to conclude around February.

### Audience

Legal analysts, observers

### On-the-ground actions from transcript

- Stay informed about updates on the New York case and its implications (suggested)
- Follow legal commentary and expert opinions on the case (suggested)

### Whats missing in summary

Insights into potential repercussions and broader implications of the case

### Tags

#LegalAnalysis #Trump #NewYorkCase #CivilCase #FinancialImplications #LegalExperts


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about New York
and Trump's surprise announcement
that was totally unsurprising.
And where everything goes from here.
Okay, so if you missed it for quite some time,
the former president was saying that he absolutely,
100% definitely was going to get up on Monday today and take the stand in the New York case,
in that civil case.
And last night he said, well, never mind, I'm not going to do that, paraphrasing of
course, but he said he would not be taking the stand today.
That's not really a surprise.
I never thought he was going to, and realistically it would be a bad move for him to.
There's nothing he can do to help in this situation, he can only hurt.
So it's not really surprising that he's not taking the stand, unless you are surprised
Trump saying that he's gonna do one thing and doing something else. So the
question is what happens from here? Odds are today nothing. My guess is that they
won't do anything today because it was supposed to be him testifying. My
understanding, and this is all subject to change, but my understanding is that
tomorrow there will be a cross-examination of a previous witness that was I believe in favor of
Trump and then there will be some rebuttal witnesses called by the prosecution by the state there.
So we are nearing the end of this. As far as the schedule goes,
The current schedule says that they have until January 5th to file any of their briefs.
And then January 11th, that's closing arguments.
This one is almost over.
That's one of the big questions that keeps coming in.
The proceedings keep going on and on and on and on.
We're finally at the end of one.
Currently, it is pretty widely believed that this is not going to go well for Trump.
The question isn't whether or not it's going to go his way.
The question is how bad it's going to be for him.
That seems to be the general consensus from legal experts on this one.
I have seen estimates as far as how bad it's going to go.
They are so all over the place that I can't put any stock in them at this point.
They range from something that's relatively minor in the lower millions to hundreds of
millions of dollars.
So I can't even guess on that.
But we will find out.
My guess is February.
We'll have a final determination because closing arguments will happen in January.
But the judge doesn't have to decide like right that moment.
So there could be a lag.
But it won't be too long.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}