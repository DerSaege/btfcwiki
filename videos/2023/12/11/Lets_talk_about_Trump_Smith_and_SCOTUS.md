---
title: Let's talk about Trump, Smith, and SCOTUS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IfWxfvGuaQc) |
| Published | 2023/12/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of presidential immunity being claimed by Trump in the context of the January 6th case in DC.
- Smith has asked the Supreme Court to decide immediately on the question of whether a former president is immune from federal prosecution for crimes committed while in office.
- Reads a section from Article 2 of the Constitution to show that presidential immunity doesn't exist.
- Notes that the Constitution allows for criminal trial even if a president has been impeached and removed from office.
- States that the idea of presidential immunity as described by Trump is fabricated.
- Criticizes the Republican Party for choosing to support Trump over the country and Constitution.
- Predicts that the Supreme Court may not find the need to hear Trump's appeal and might leave it to the appeals court.
- Doubts that even the current Supreme Court might side with Trump due to the clear constitutional provisions.
- Concludes by expressing uncertainty about the Supreme Court's decision.

### Quotes

- "The idea of presidential immunity and the way Trump is describing it, it's made up. That's not a thing."
- "You can support Trump or you can support the country. You can support the Constitution or you can support the real estate developer."

### Oneliner

Beau explains why presidential immunity does not exist and questions whether the Supreme Court will side with Trump over the Constitution.

### Audience

Legal scholars, political activists

### On-the-ground actions from transcript

- Contact legal experts to understand the implications of presidential immunity (suggested)
- Share this information to raise awareness about the Constitution's stance on presidential immunity (implied)

### Whats missing in summary

Full context and depth of analysis on presidential immunity and its implications in ongoing legal proceedings.

### Tags

#PresidentialImmunity #Trump #Constitution #SupremeCourt #LegalIssues


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Smith and Trump
and the US Constitution and the United States Supreme Court
and the idea that the president has complete, total,
and utter immunity from everything ever that occurred
when he was in office, okay?
So if you don't know what's going on,
Trump is claiming that he has something called
presidential immunity. And appealing on these grounds, asking to stop the proceedings when
it comes to the January 6th case in DC, the federal one. Smith has asked the Supreme Court
to decide on this immediately, to basically put it on an expedited list and move it along
right away because it's incredibly important. He says, this case presents a fundamental question
at the heart of our democracy whether a former president is absolutely immune from federal
prosecution for crimes committed while in office.
That's part of the filing.
Let me just go ahead and tell you, he's not.
He's not.
There are a lot of people with We the People profile pictures saying that presidential
immunity is a thing.
If you had read the document that you have as your profile picture, you might have picked
up on something.
I'm going to go ahead and read something here.
This is from Article 1, and it is Section 3, last part.
in cases of impeachment shall not extend further than to removal from office and disqualification
to hold and enjoy any office of honor, trust, or profit under the United States.
But the party convicted shall nevertheless be liable and subject to indictment, trial,
judgment and punishment according to law. So, it is incredibly clear from the Constitution
that even in an instance where the president was already punished for that activity, setting up
some weird double jeopardy claim or whatever, that even if they're impeached and removed from office,
can still be tried criminally. The idea that there's some kind of blanket immunity,
that does not exist in the Constitution. It is super clear about this. The idea
of presidential immunity and the way Trump is describing it, it's made up.
That's not a thing. Again, this is one of those moments that has occurred repeatedly
for the Republican Party. You can support Trump or you can support the country. You
You can't do both.
You can support the Constitution or you can support the real estate developer.
I don't know what the Supreme Court is going to do.
My guess, and it's just a hunch, is to just be like, yeah, we don't need to hear this
and let the appeals court deal with it.
would be my guess because it is so far outside what is real it seems like they
wouldn't hear it. If they do hear it I do not anticipate them siding with Trump
even with this court. Just a plain reading of the Constitution makes it
pretty clear that the idea that a president is somehow immune, it just
doesn't hold up.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}