---
title: Let's talk about Biden marching in to a pricing debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OrNpW3zm78Y) |
| Published | 2023/12/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration is considering utilizing march-in rights to address high prices on pharmaceutical products developed with government funding.
- This move could potentially impact pricing in the United States significantly.
- Pharmaceutical companies are not happy about this potential action and are threatening to stop accepting government funding.
- Supporters of the Biden administration's move believe that not naming specific pharmaceutical products is a strategic decision to pressure companies into reducing prices across the board.
- The administration's strategy of withholding specific product names is seen as a smart move to encourage companies to lower prices overall rather than focusing on specific products for public support.
- Despite potential legal challenges from pharmaceutical companies, the administration seems determined to proceed with this course of action.
- The lack of media coverage on this issue is concerning, considering the significant impact it could have on people's medical bills.
- The administration's approach aims to reduce prices overall rather than mobilizing support for specific products.
- The outcome of this decision will likely affect many individuals who are unaware of the ongoing efforts to lower medical costs.
- This initiative could lead to a substantial change in the pharmaceutical industry's pricing practices.

### Quotes

- "If the goal is not to mobilize support for certain products, but to get the overall price down, this is the smart way to do it."
- "Undoubtedly, this is going to end up in court."
- "If they are successful with this, it's going to matter to a whole lot of people."
- "There is a fight going on right now to reduce their medical bills."
- "If it's just a thought, y'all have a good day."

### Oneliner

The Biden administration's consideration of march-in rights to address high pharmaceutical prices stirs controversy among companies and supporters, opting for a strategic approach to lower prices overall.

### Audience

Advocates, pharmaceutical companies

### On-the-ground actions from transcript

- Support initiatives that aim to lower pharmaceutical prices (suggested)
- Stay informed about government actions impacting pharmaceutical pricing (suggested)
- Advocate for transparent and fair pricing practices in the pharmaceutical industry (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the Biden administration's potential use of march-in rights to address high pharmaceutical prices, exploring industry reactions and public support strategies.

### Tags

#BidenAdministration #PharmaceuticalPrices #MarchInRights #MedicalCosts #PublicSupport


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about
the Biden administration marching into a discussion
in a very pronounced way.
And the reason it's occurring, what might change,
And the reason it's occurring, what might change,
we will talk about march-in rights,
we will talk about the two issues
that people have with the way this is being done
and where it goes from here.
So if you've missed the news, and you
would be forgiven if you missed this,
because this is not getting the coverage that I
think it should, the Biden administration
has put pharmaceutical companies on notice
and basically said, hey, if we feel like your prices are
too high on products that you used government
funds to help develop, we very well might exercise our march in rights, which basically
allow the government to, it's not really canceling the patent, it's amending it, altering the
deal so to speak, and allowing other companies to make that product in hopes of driving the
price of the product down would probably work. It tends to anyway. There are a
number of pharmaceutical products that this would apply to and the government
certainly my understanding is that the government has the the rights to do this.
Now if this occurs it will probably have a pretty marked impact on pricing in the
United States. There are two complaints with how it's being done. One is coming
from the pharmaceutical industry. Big surprise, right? They're obviously not
happy about this. And their angle on it is saying, well, if that's the way you're going
to play it, maybe we won't take government funding anymore, and it will jeopardize the
public-private partnerships. I'm sure another company will. I don't really see that as quite
the winning argument that apparently some people think it is. So that's one set of complaint about
this is basically the pharmaceutical companies don't like it because they have to be conscientious
of what they charge. The other is from people who actually support it and it's that the Biden
administration isn't naming these specific pharmaceutical products that they're talking
about. I understand why people want the administration to do that. I get it. Don't get me wrong.
I understand what you're saying. That it would be better for the American people and
help energize, support for it and all of that if people knew specifically what products
would be impacted. The Biden administration is right. Not telegraphing is actually the
smart move. If you don't tell the pharmaceutical companies which
products you're talking about, they have to evaluate all of them. If you have a
list of 10 and you name them, they are going to look at those 10. If you don't
name them, they're going to look at all of them. And if there is a percentage of
the profit that can be cut to safeguard their patent, they probably will. There
will probably be a more widespread reduction if the administration is vague
here. I get what you're saying. If they would name them, it would get a whole
bunch of people who are reliant on that product to come out and support this. I get it from the
mobilizing of support aspect, but if the goal is not to mobilize support for certain products,
but to get the overall price down, this is the smart way to do it.
So, the Biden administration appears to be moving forward with this.
Undoubtedly, this is going to end up in court.
This would be a pretty big change.
My understanding of how this works is that the administration is well within the bounds.
But regardless of that, the pharmaceutical companies will probably attempt to find a
legal reason for any rule produced by the Biden administration to not take
effect. Again, this isn't getting the coverage that it should. If this
takes hold, if they are successful with this, it's going to matter to a whole lot
of people. To a whole lot of people who realistically probably have no idea that
there is a fight going on right now to reduce their medical bills. Anyway, it's
If it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}