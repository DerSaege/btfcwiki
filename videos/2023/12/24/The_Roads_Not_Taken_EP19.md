---
title: The Roads Not Taken EP19
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WNC2Z-zQ-YQ) |
| Published | 2023/12/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The episode covers unreported or underreported news from the past week on December 24th, 2023.
- Germany is deploying troops to Lithuania as a deterrent against Russia, pressuring other NATO members to increase their presence.
- Putin authorized the seizure of energy assets from "unfriendly countries," likely impacting Germany and Austria.
- Top Chinese and U.S. military officials are reestablishing communication channels for potential de-escalation.
- U.S. news includes the release of Glenn Simmons after being wrongfully convicted and Congress being on vacation with pressing matters awaiting their return.
- GOP candidates threaten to withdraw from the Colorado primary if Trump is not on the ballot.
- Steve Bannon claims the Navy SEALs have become "woke" and "Hollywood."
- Scientists simulated a runaway greenhouse gas scenario turning Earth into Venus, reaching temperatures over 900 degrees.
- Social media debates whether Trump has a particular odor, sparking conspiracy theories.
- Beau addresses various questions, including the use of UN forces, changes in the Republican Party, NATO's actions in Crimea, and the fate of Mount Rushmore.

### Quotes

- "It really is that simple."
- "I mean, it's a Southern tradition for New Year's. You eat black-eyed peas, you get good luck. It's really that simple."
- "That's what those are. Why? For New Year's."

### Oneliner

Beau covers unreported news from December 24th, 2023, including international tensions, U.S. developments, and societal debates, ending with a festive Southern tradition.

### Audience

News enthusiasts

### On-the-ground actions from transcript

- Contact local representatives to advocate for diplomatic solutions to international tensions (implied)
- Support organizations working towards criminal justice reform to prevent wrongful convictions (implied)
- Engage in critical discourse and fact-checking to combat misinformation on social media (implied)

### Whats missing in summary

Deeper insights into masculinity, the potential for multinational peacekeeping forces, and the significance of traditions in different cultures.

### Tags

#UnreportedNews #InternationalTensions #USPolitics #SocialDebates #SouthernTraditions


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is December 24th, 2023,
and this is episode 19 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and talk about news that was unreported, underreported,
just didn't get the coverage I think it should.
We'll end up with context later,
or sometimes I just find it interesting.
Today, we will be covering stuff a little bit lighter than normal.
Merry Christmas.
Okay, so starting off with foreign policy.
Germany will be permanently deploying troops to another country for the first time since,
you know, the troops will be stationed in Lithuania as part of a deterrent against Russia.
move puts a whole lot of pressure on other members of NATO to step up their presence.
Putin signed decrees authorizing the seizure of energy assets from quote, unfriendly countries.
This is likely to hit Germany and Austria the hardest.
Meanwhile, the Kremlin is super mad that the US is signaling support for seizing Russia's
central bank's assets and using those to support Ukraine.
It is worth noting that those assets could total more than $300 billion.
Top Chinese and U.S. military officials are back on speaking terms.
The break is over.
That's good news.
It provides a channel for de-escalation.
Moving on to U.S. news, Glenn Simmons was released after being demonstrated to have
been wrongfully convicted.
He was in prison for more than 48 years.
Congress is currently on vacation, while a whole bunch of pressing matters remain leaving
a whole lot for the new year.
Expect a whole lot of, let's just say, confrontation when the session resumes.
GOP candidates threaten to withdraw from the Colorado primary unless Trump is on the ballot.
I mean, okay, it is worth remembering everybody that Colorado hasn't gone red in
the presidential election of like 20 years, and that Trump lost ground in
Colorado from 2016 to 2020, Senator Tuberville dropped the last holds on
military promotions. I'm sorry, Senator Tuberville dropped the last holds on
military promotions. So those are all moving through now. In cultural news,
Steve Bannon has claimed the Navy Sills have become woke. They have gone
Hollywood. It's a quote. Hollywood. I am certain that that is going to keep them
up at night. Moving on to environmental news, scientists simulated what would
happen if a runaway greenhouse gas scenario actually occurred and it
basically turns Earth into Venus. It creates a situation in which the Earth's
ground temperature reaches more than 900 degrees. That's not good. In oddities, there is a widespread
debate on social media to include members of the Republican Party related to whether or not former
President Trump stinks like actual odor. The topic took the number one spot
trending on social media for a brief period of time. Yeah the country's doing
great. That's what matters right now. Rudy apparently owes Hunter money. Rudy Giuliani
is reported to owe Hunter Biden money. According to his bankruptcy filings, an unspecified amount
was declared. This has launched scores of conspiracy theories. I can't even get
through them all, but I have a feeling we're in for a wild ride on that one.
Okay, moving on to the Q&A section. And this is questions sent in by you, picked
by the team, and I haven't seen them until now. And we're starting off with a
a note that says, say the email address.
It is question for Bo, question for F-O-R, Bo, B-E-A-U, at Gmail.
Then tell everybody, you don't use Telegram.
The scammer is back.
So in the comment sections of the channels on the videos,
you may get a reply from an account that vaguely resembles
Mine, it's not me.
I do not use Telegram.
I do not ask people to go to Telegram.
For those listening on the podcast,
this is specific to YouTube, obviously.
Okay, starting off with the questions.
Is there any reason UN forces couldn't be used
instead of forces from Middle Eastern countries?
Is it just that the UN isn't open to doing it again
because of their experience in the Balkans?
Realistically, the United Nations does not have the teeth for this.
It really is that simple.
I have videos somewhere about the world's EMT, and it talks about a force that is there
to engage in actually helping countries.
A component of that is actually having the teeth to deal with a situation like this.
The United Nations doesn't really have the force to do it.
The UN's forces are made up of other countries.
There's not a cohesive element to it that can deal with something this complex, really.
The bad side, or the worst side, I guess, is that the alternatives are like NATO forces.
That's the closest thing that exists that could do it, and that's just a bad idea all
around too, for political reasons, for optics, for how it would be received, for how NATO
might respond if those forces came under attack by a group that was not happy with the situation.
It's just all bad.
This is the reason that force has to be built.
Eventually it is going to.
It's going to have to be built.
That Department of Something Else, as Thomas Barnett calls it, it's been called the Department
Peace by Marianne Williamson, World CMT, they are very, very similar ideas and they run
the spectrum from political ideology and it's one of those things that eventually will have
to occur.
The problem with it is it has to be built probably within the US military and then internationalized.
That creates its own set of issues.
But when this is all said and done, odds are it will be a multinational force that is maybe
operating at the request of the UN, but is pretty independent of it, and allowing the
UN to do the humanitarian side of it.
It's a mess.
Okay, I have questioned what has made the Republican Party turn so far from what they
were just a decade or so ago.
Rhetoric and groupthink has obviously played a large part.
I recently heard of another possibility, or at least one that may play a part, have you
heard of the blue dot effect?
Yeah okay, so this doesn't have to do, the blue dot doesn't necessarily have to do anything
with like red and blue, Democrat, Republican.
Summing this up quickly, it's the idea
that no matter how safe and how comfortable you are,
you're always going to be looking for threats,
for risks, for problems, because that's how we survive.
That's that's how we evolved. I mean, that's interesting. It's an interesting idea that
this is why the Republican Party has taken the turn and kind of become a
little dramatic. It might explain why the base is falling for it. It doesn't
explain why the politicians are leaning into it as hard as they are.
The answer there is the embrace of authoritarianism.
But it might explain the basis behavior because they see something, they're told it's a risk,
they're told it's a threat, and that that fear center gets active.
I mean, yeah, I like it.
worth looking into. I would imagine in a week or two you'll hear me talk about it
after I look into it a little more. What is keeping NATO from doing the same
thing it did in Yugoslavia against the Serbs? Why can't it take over the air
role in Crimea? Okay, simple answer here. It would lead to direct confrontation
with Russia, direct conflict with Russia, which is something that NATO actually does
want to avoid.
I know it doesn't always seem that way.
But the idea of a hot conflict, NATO versus Russia, is very dangerous.
It can spiral out of control, and it's really that simple.
Okay, these do not seem like light-hearted at all.
Objectionable statues and monuments happen a lot, but let's be real.
How do you handle Mount Rushmore?
Tear it down or give it a pass, which is the right thing to do.
If it's me, I get to make the decision?
It goes to Lakota.
Because it is right now, it becomes their property and they decide what to do with it.
It's theirs.
For those that don't know, that area, the Black Hills should be theirs.
To me, the right move is to transfer ownership.
Now I personally would like to think that they would not tear it down, but they would
incorporate it into, basically leave the monument, but in order to get to it you have to learn
a whole lot along the way.
I think that that would be what I would do with it, if I was them, but my simple answer
would be it goes to them and they get to decide, even if they decide to get rid of it.
It's theirs.
I'm a trans man.
I'm five months into my transition.
I'm slowly learning what manhood means to me.
Some very flattering things.
In your view, what does it take to be a good man?
It's subtle, understated.
At one point in time, I actually have a whole bunch of videos on masculinity, and at one
point in time, like a clip show was put together, I'll try to find the link and put it down
below.
It's different for every person, it is different for every person.
There is a concept in Japanese art that really explains it well, and at least to me it's
transferable because it is perfection without effort.
You should be trying to do the right thing in the way that best suits you.
masculinity is going to be unique. It's not a team sport. Everybody's is going to
be different and the more you try to force a role the harder it's going to
be and I don't mean you know you can't have people that you look at and you're
like this person right here, this is somebody
that I would like to take these qualities from.
I don't mean that.
I mean, your general disposition will
shape a lot of what your outwardly projecting
masculinity is.
fit very neatly into that stereotypical form of it. I'll find it. Watch the clip
show and forgive my mispronunciation of Japanese terms. Okay. Settle a debate.
What is the greatest, what is the greatest sniper shot ever taken? Doug
Conley. That was the person, I can't remember the actual, the marksman's name.
I want to say Palm, Palmer, something like that, but Doug Conley. Was in the early
90s and he was holding himself. He had a revolver and was threatening himself and
he's involved in a standoff with law enforcement and the SWAT team showed up
and the I want to say the shooter was a Vietnam vet and lined up a shot when the
guy lowered it and stopped pointing it at his head and sent the round right
through the cylinder of that revolver. To me that's the best shot. For a while
it got taken to show how like what SWAT teams were supposed to be. A, special not
used all the time, and B, they were supposed to be a life-saving tool.
And yeah, so the round went through the cylinder of the revolver.
He couldn't hurt himself, and to me, that's the best shot.
During your impromptu livestream, a lot of people requested the upside-down bow patch
and Deep Goat. I'm not sure if personality is the proper term but I'm
sure you know what I mean. Is there any other personality you thought of and if
so why haven't you tried them out? For those that have no idea what that means,
flipping the patch on this hat upside down, it normally means there's a lot of
satire, there's a bait and switch type of thing going on. Deep Goat is a play on
the the Watergate informant I guess and they've been absent from the channel
for a while then they make a return is there any other personality yeah two
actually one I put on the channel which is a it's the same
type of content, but it's delivered in a very tent revival preacher kind of way.
And I did a video about that because the topic was about a preacher who said
something that really got under my skin. But I felt like, I felt like if I did it
too often it might be offensive to some people.
But then again, recently I was listening to Drew Morgan
and just the inflection in his voice.
He's like, and that is the difference in the way
he did it.
Very, very southern preacher, and it got
me wanting to do it again.
And then another one is a fed up high school or civics
teacher or history or civics teacher that's another one that I have
entertained. A lot of ideas not a lot of time you know and we have a a pretty a
pretty full production schedule. Note this is kind of related link at the end
Okay, you did a video once where you for all intents and purposes gave a
pre-deployment briefing. I was looking for it the other day because while it
was obviously satirical, the advice on getting your stuff together with sound,
do you happen to remember the name of it? I do not, but apparently the link will be
down below. I do remember that. It was going through how to, how to prepare and
have all of your stuff together for that final trip. I do remember that video.
That was a while ago. Okay. In the video showing the stuff for the shelters,
there looks to be bags of black-eyed peas on top of the car seats. If there is,
is why. Yes, that's what those are. Why? For New Year's. How southern are you? Yeah.
So that's a tradition. Eat black eyed peas. It's supposed to be good luck. And that was
something that I don't know that they asked for that. I think we threw that in. I can't
I don't remember, no, to be honest.
But yeah, it's a Southern tradition for New Year's.
You eat black eyed peas, you get good luck.
It's really that simple.
I don't know the origin of it, but I mean,
it's pretty common.
And that looks like it.
So those are all the questions.
OK, so a little bit more information,
a little bit more context and having the right information
will make all the difference.
Y'all have happy holidays.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}