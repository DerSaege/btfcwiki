---
title: Let's talk about Japan, Ukraine, the US, and the UK....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=yEv12soju58) |
| Published | 2023/12/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Japan plans to export Patriots missiles to the United States, which they make under license from Mitsubishi.
- The United States will likely send their own US-made Patriots to Ukraine in return.
- Japan has a policy of not exporting weapons to countries at war, hence the workaround through the United States.
- Japan's reluctance to directly involve itself in wars is due to its history and sensitivity.
- This strategy allows support to flow without Japan violating its core tenets.
- Similar situations might occur with other countries with export policies like Japan's.
- Japan's move strengthens the U.S.-Japanese relationship significantly.
- Japan's support enhances the U.S.'s stockpiles without the risk of dropping below required levels.
- The dynamic is like an international poker game, where Japan provides support to its allies.
- The United Kingdom might also receive support from Japan in a similar fashion.

### Quotes

- "Japan is going to export to the United States, Patriots, the missiles."
- "In the international poker game where everybody's cheating, Japan just slid the US some chips."
- "This move for the Japanese government, it's a smart move."

### Oneliner

Japan plans to export missiles to the U.S., who will then send their own to Ukraine, navigating around Japan's policy of not exporting weapons to countries at war, strengthening alliances without direct involvement in conflicts.

### Audience

Foreign Policy Analysts

### On-the-ground actions from transcript

- Contact organizations advocating for peaceful resolutions to conflicts (implied)
- Support diplomatic efforts to prevent escalation of conflicts (implied)

### Whats missing in summary

Details on the potential impacts of this export strategy on the ongoing conflicts and diplomatic relations between countries involved. 

### Tags

#Japan #UnitedStates #Ukraine #Export #Alliances


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Japan
and the United States and Ukraine
and maybe even the United Kingdom
when it comes to something else.
And we're going to talk about a workaround
and we are going to talk about Japanese patriots.
I feel like by the time this video goes out, the reporting will be cleared up, but right
now there's a bunch of questions, so we will go ahead and answer them.
If you have no idea what I'm talking about, the short version is that Japan is going to
export to the United States, Patriots, the missiles.
Japan makes their own under license.
I think Mitsubishi actually makes them.
Japan is going to send some to the United States.
Now, the next step, more than likely, is the United States
is going to send some US-made ones to Ukraine.
The United States has levels when
it comes to certain kinds of weapons and armaments.
and they don't allow the U.S. inventory to fall below those levels.
Japan has a rule, basically they don't export weapons to a country at war.
So they will export their patriots to the United States.
United States will then in turn export US-made patriots to Ukraine. That's
what's happening. How does the United Kingdom fit in? I feel like the same
thing's probably going to happen with 155 millimeter shells as well. That's
probably on the horizon. Japan takes its position of not getting involved in
wars very seriously. Even this is, it's not really surprising, but it's unusual.
Because of their history, they are very sensitive to getting directly involved in something.
So this allows the support to flow without violating that core tenet that they have.
And we'll probably see more of similar things with other countries.
probably see things like this occur in other places where you have countries
that have policies about exports and they won't export to Ukraine but they're
going to export to another country who will then export to Ukraine. Not the same
equipment, different equipment, the exact same type just manufactured in different
locations.
This move for the Japanese government, it's a smart move.
It strengthens the U.S.-Japanese relationship on a pretty deep level.
keep in mind, they have been an incredibly reliable partner for a long time now.
But this kind of bumps them up a little bit.
In the international poker game where everybody's cheating, Japan just slid the US some chips.
And I do, I believe they're going to slide the United Kingdom some as well.
And the same thing is going to happen there.
So it's really not that complicated.
It's not that the U.S. is out.
It's that the U.S. doesn't want to drop below the levels that it fills it needs.
So they're going to fill up the stocks with the Japanese produced missile and ship the
U.S. produced ones out.
they have the inventory that they feel they need.
OK, anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}