---
title: Let's talk about Iowa, food, and spending....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m1AZfOfBHps) |
| Published | 2023/12/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises the issue of Iowa's decision to not participate in a summer program for EBT, impacting low-income families.
- Explains the program provides $40 per month per child to help with food costs during school breaks.
- Points out the state's cost for this program is only 50% of administrative costs, around $2 million.
- Criticizes the governor's choice to prioritize a publicity stunt over funding for children's meals.
- Calls out politicians for using vulnerable groups as scapegoats and distracting from real issues.
- Dismisses the argument that tax dollars are being wasted on providing food for kids who may not be deemed "needy."
- Argues against denying food assistance to those in need based on the worst-case scenario of a few who may not qualify benefiting.
- Expresses concern over the politicization of children's basic needs like food.

### Quotes

- "When you have a country that is trying to politicize and debate whether or not kids should eat, I suggest that's a problem."
- "For the nine billionth time on this channel, a group of people who have less institutional power than you do are never the source of your problems."
- "Those in power just get you looking down at them, so you're not paying attention to what those in power are doing."
- "Assuming that's occurring, the worst-case scenario here is that somebody using this framing paid their taxes and that money went back to their kid."
- "Anyway, y'all enjoy your holiday meals. It's just a thought. Have a good night."

### Oneliner

Iowa's refusal to participate in a food assistance program for low-income families raises concerns about prioritizing publicity stunts over children's meals, prompting a critical look at politicizing basic needs.

### Audience

Advocates for child welfare

### On-the-ground actions from transcript

- Contact local representatives to express support for programs providing food assistance to low-income families (suggested).
- Donate to local food banks or organizations supporting children's nutrition (exemplified).

### Whats missing in summary

The emotional impact of prioritizing politics over children's well-being and the importance of community support for vulnerable families. 

### Tags

#ChildWelfare #FoodAssistance #Politicization #CommunitySupport #LowIncomeFamilies


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Iowa
and food and spending.
And we're gonna do this because I got a question
and it prompted me to see something.
So we're just gonna kinda run through the question
real quick and then we'll talk about the news
and then I'll answer the question.
Okay, I know you're gonna do something about Iowa
the kids welfare for food. Before you do, just know that this is happening because our tax dollars
were being used to pay for food for kids who aren't actually needy. It's a waste in taking
money out of the pocket of working-class Americans. Now, I'm going to be honest,
before I got this message, I had no idea what was going on in Iowa. Okay, so this is what's
What's going on?
The governor there has decided that the state is not going to participate in a summer program
for EBT.
What this program does is it gives $40 per month per child to low income families.
to help offset the food costs because the kids aren't in school, not getting lunch there.
And remember, low-income families will get lunch provided.
The cost of this to the state is 50% of the administrative costs in Iowa.
That's about $2 million, $2 million.
Now the governor had this to say, if the Biden administration and Congress want to make a
real commitment to family well-being, they should invest in already existing programs
and infrastructure at the state level and give us the flexibility to tailor them to
our state's needs.
let's be real clear. This is about two million dollars, which is about the same as the governor
spent to send National Guard troops from Iowa down to the border for a publicity stunt.
Maybe there's a concern about what some people view as the state's needs.
Maybe that has something to do with it.
Low-income kids won't get to eat, but yeah, publicity stunt, that's okay.
This is another example of politicians using a group of people that don't have any institutional
power and using them as the scapegoat.
For the nine billionth time on this channel, a group of people who have less institutional
power than you do are never the source of your problems.
That's not how it works.
Those in power just get you looking down at them, so you're not paying attention to what
those in power are doing. I mean we're talking about a dollar a day, a little
more than a dollar a day, for food and I'm not even joking in the the statement
about it there's something about how the EBT card doesn't fix childhood obesity
or something. It is what it is. Okay so let's go back to the question that
actually prompted me to look into this. I know you're gonna do something about
Iowa and kids welfare for food. Before you do, just know that this is happening
because our tax dollars were being used to pay for food for kids who aren't
actually needy. It's a waste and taking money out of the pocket of working-class
Americans. I have no idea if that's true, okay? I don't. I don't like the framing
of it in a whole bunch of ways, but you know what? Let's just say that it is for
second. Let's just say that that happens, that people who aren't really needy, that
those kids also get food. Okay, assuming that's occurring, the worst-case scenario
here is that somebody using this framing paid their taxes and that money went
back to their kid. That's your worst case scenario. And on the basis of that, you
want to deny everybody else. Those who are needy, right? Because, I mean, that
stands to reason. Those who are needy, they're not paying a lot in
taxes. That's why the tax dollar term is in there. Those who aren't needy,
will they pay taxes? Right? So your worst case scenario is that somebody who paid
taxes, their kid got money via that method for food. I personally I can live
with that. I mean that that doesn't really seem like that big of a problem
to me. When you have a country that is trying to politicize and debate whether
or not kids should eat, I would suggest that's a problem. Anyway, y'all enjoy
your holiday meals. It's just a thought. Have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}