---
title: Let's talk about 2 unwise Trumpworld statements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KqO1H9bcnGY) |
| Published | 2023/12/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining two separate incidents in Trump World that happened recently, involving Rudy Giuliani and the former president.
- Giuliani is in the middle of dealing with a defamation case related to election workers and is determining how much money he will have to pay for his comments.
- Despite being ruled out of bounds by the judge, Giuliani insists that his comments were true and does not regret them.
- Giuliani repeats his controversial statement during the proceedings, potentially leading to increased monetary penalties.
- Moving on to Trump, who claims presidential immunity and asked the Supreme Court to decide on it quickly, which the Court agreed to expedite consideration of.
- Trump's team released a statement criticizing the actions as an attempt to prevent him from retaking the Oval Office in 2024.
- Beau points out that if Trump truly believed in his presidential immunity claim, taking it to the Supreme Court quickly should benefit him.
- The statements from Giuliani and Trump's team indicate their strategies to drag out legal proceedings and cater to their base rather than a strong legal basis.
- Beau observes that both instances show Trump World trying to influence public opinion through statements, not realizing that the judicial system is impartial.
- Concluding with a reflection on the ill-advised nature of both statements and how they reveal Trump World's tactics.

### Quotes

- "I told the truth. They were engaged in changing votes."
- "If Giuliani is engaged in that behavior while the dollar amount is being figured out, it probably means the dollar amount goes up."
- "If there was any basis to the president's claims, it seems like they would want it to go to the Supreme Court."
- "You have two instances last night that were Trump world acting like Trump world."
- "The judicial system literally does not care about the court of public opinion."

### Oneliner

Beau explains recent incidents involving Giuliani and Trump in Trump World, showing their tactics to influence public opinion and drag out legal proceedings.

### Audience

Political observers, activists

### On-the-ground actions from transcript

- Contact legal aid organizations to support efforts combating misinformation and defamation cases (suggested)
- Stay informed on legal proceedings related to political figures and hold them accountable (implied)

### Whats missing in summary

Analysis of the potential implications of these incidents on future legal battles and public trust in political figures.

### Tags

#TrumpWorld #Giuliani #PresidentialImmunity #SupremeCourt #LegalProceedings


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump World
and how Trump World had a bad night.
We're gonna talk about two separate instances,
two incidents, and kind of just run through them
and it will explain some things that will happen later.
One deals with Rudy Giuliani and the other, of course,
deals with the former president.
We will start with Rudy.
Okay, if you don't know what's going on with Giuliani right now, he is in the middle of
dealing with the whole defamation thing, dealing with the election workers.
And they're in the portion of the proceedings in which they're determining how much money
Giuliani is going to have to pay.
Because the judge has already ruled that some of the comments that Giuliani made were out
of bounds.
So, Giuliani said, everything I said about them is true.
Of course I don't regret it.
I told the truth.
They were engaged in changing votes.
A reporter at this point is like, there's no proof of that.
Giuliani says that there is and says quote, stay tuned. So while the proceedings
are going on to determine how much he has to pay for this statement, this
guy, he makes it again. I mean, I feel like that's gonna come up in court. Part
Part of the monetary penalties that are involved in this, the reason they occur is to make
sure that the same behavior isn't repeated.
If Giuliani is engaged in that behavior while the dollar amount is being figured out, it
Probably means the dollar amount goes up.
Not a wise statement from Giuliani.
And I feel like he probably realized that right afterward.
Okay, moving on to Trump.
If you missed yesterday, Trump is making this claim saying that he has presidential immunity
Smith basically asked the Supreme Court to go ahead and decide on that, and the
Supreme Court said that they would expedite consideration of it. Doesn't
mean they're gonna take the case, but they'll think about it quickly. This
bothers the former president and his team, who released a statement. Crooked Joe
So Biden's henchman, deranged Jack Smith, is so obsessed with interfering in the 2024
presidential election with the goal of preventing President Trump from retaking the Oval Office,
as the President is poised to do, that Smith is willing to try for a Hail Mary by racing
to the Supreme Court and attempting to bypass the appellate process.
Let's be clear on something.
Smith taking this to the Supreme Court and trying to get it there quickly, if Trump actually
believed he had presidential immunity, that would be Smith doing him a favor.
If there was any basis to the president's claims, it seems like they would want it to
go to the Supreme Court because once the Supreme Court decides that he does have this presidential
immunity, then the whole case goes away. If they're upset about him doing this, it
seems like maybe they think that the presidential immunity claim won't stand
up and their whole purpose behind it is just trying to drag out the proceedings
in hopes of getting beyond the election and using that time to continue to
massage their base. Putting this statement out, I think for a lot of people, kind of
gave the game away. You have two instances last night that were Trump
world acting like Trump world and trying to win things in the court of public
opinion in the way that they've tried in the past, apparently not realizing that
the judicial system literally does not care about the court of public opinion.
Both of these statements were pretty ill-advised. Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}