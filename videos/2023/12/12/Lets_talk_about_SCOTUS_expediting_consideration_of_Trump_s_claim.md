---
title: Let's talk about SCOTUS expediting consideration of Trump's claim....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VlxzRVy8_DU) |
| Published | 2023/12/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Supreme Court considering Trump's claim of presidential immunity on an expedited schedule.
- Appeals court reviewing the case quickly as well.
- Trump given until the 20th to respond, and until Wednesday by the appeals court.
- Beau previously argued that presidential immunity as claimed by Trump is not supported by the Constitution.
- Beau received a message asserting that presidential immunity is established in case law and referenced previous investigations.
- Beau refutes the argument by clarifying that there has never been a ruling on criminal immunity for presidents.
- The Office of Legal Counsel memo cited by the message does not carry the force of law and can be changed.
- Beau mentions a precedent in Nixon v. Fitzgerald where the president was immune in civil liability cases within their official duties.
- However, this immunity does not extend to pre-presidency matters or criminal cases.
- Beau questions the idea of full immunity for presidents and how it could potentially lead to abuse of power.
- He points out the absurdity of suggesting that presidents have total immunity for any action.
- Beau underscores the importance of not granting unlimited power based on partisan support.
- He expresses skepticism about the Supreme Court taking up the case due to the weak argument presented.
- Beau concludes by discussing the ongoing legal proceedings and speculates on the Supreme Court's decision.

### Quotes

- "Presidents are not kings."
- "There's no case law, there's no precedent, there's nothing like that."
- "If you're saying they can literally do anything they want because they sold you a hat and a bunch of rhetoric you might want to revaluate."
- "Presidents don't have full immunity for anything that they do."
- "Y'all have a good day."

### Oneliner

Supreme Court fast-tracking consideration of Trump's presidential immunity claim, prompting critical analysis from Beau on the limits of presidential power.

### Audience

Legal scholars, political analysts, concerned citizens.

### On-the-ground actions from transcript

- Contact legal advocacy organizations to stay informed on updates regarding the Supreme Court and appeals court decisions (implied).

### Whats missing in summary

Deeper insights into the potential implications of granting presidents full immunity for their actions.

### Tags

#Trump #PresidentialImmunity #SupremeCourt #LegalAnalysis #ChecksAndBalances


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump,
the Supreme Court, and the appeals court,
and everything being expedited,
and where all of that sits,
and then we are going to go over a message that I got that,
I feel like we should go over.
Okay, so if you missed the news,
Smith basically asked the Supreme Court
to decide on Trump's claim of presidential immunity."
Almost immediately, the Supreme Court agreed to put it on an expedited schedule for consideration.
To be clear, the Supreme Court has not determined it's going to take up the case, just that
it will consider it quickly.
I want to say Trump has until the 20th to put in his response.
The appeals court also said that they will review it on an expedited basis, and I think
they gave him until Wednesday to respond with that.
Okay, so it is moving along very, very quickly to settle this question that Trump has brought
In an earlier video, when all of this first started, I said that presidential immunity, the way it was being framed by
Trump, wasn't really a thing and read a little bit of the Constitution to demonstrate that.
I have a message. You are wrong, wrong, wrong, wrong. Four wrongs, last one, all caps.
presidential immunity is clearly demonstrable in case law it came up in
the mullet investigation mullet with a T the OLC said it clearly making
precedent presidents have full immunity for everything they do in office you're
wrong okay case law I don't think that word
means what you think it means. Okay, there's never been a ruling about whether or not presidents
have criminal immunity. That's actually not something that's ever been ruled on. The OLC
did not make precedent. It wrote a memo, a memo that has exactly zero force of law behind it,
None. It's basically policy within DOJ, not law. And it can be changed by another memo.
That's all it is. And incidentally, it doesn't say that they have immunity forever, just while
sitting as president, to be to be clear. The closest you get to anything like that is Nixon v.
Fitzgerald in 1982, which determined that when it came to civil liability, not criminal,
the president was immune for everything to the outer perimeter of their actual duties.
So, even in civil law, there's not total immunity there.
There's another case dealing with Clinton later that basically said they have no immunity
for pre-presidency stuff.
But even in the only real ruling we have on this, it's only in civil cases and it only
extends to what they're doing within the scope of their duties.
I feel like Smith would argue that what occurred is not within the scope of the president's
duties.
Just saying.
Presidents have full immunity for everything they do in office.
Okay let's say that's true.
I mean, if that's the case, then explain to me why Biden just doesn't block Trump up.
Not have elections at all.
Declare himself king.
He has immunity for it, right?
That's not a crime.
his total immunity for everything he does in office. When you apply it to
anybody but the person that you bought a red hat to support, all of a sudden it
doesn't make any sense, right? It sounds like a really bad idea. It is a really
bad idea. Presidents don't have full immunity for anything that they do.
that's silly. When you can excuse literally any behavior which is what's
being argued here, you might want to consider that you have given that
person too much power over your own thought process. If you're saying they
can literally do anything they want because they sold you a hat and a bunch
of rhetoric you might want to reevaluate. There's no case law, there's no precedent,
there's nothing like that. Presidents are not kings. I feel like somebody said that
recently. But either way, you know, despite messages saying that it is
settled law, the Supreme Court has decided they will consider taking the
case up on an expedited schedule and the appeals court is reviewing it on an
expedited thing as well. You would think if it was settled law they would have to
do that. I'm still not convinced the Supreme Court is going to take it up
because the argument, it seems so weak that they will probably allow the
appeals court ruling to just stand. That would be my guess. They may take it up
simply to make it apply everywhere, but I don't know that that's a foregone
conclusion yet. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}