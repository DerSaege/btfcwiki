---
title: Let's talk about the military review of the Teixeira case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=F5XyJvvF3wo) |
| Published | 2023/12/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on Teixeira and the internal report revealing what happened.
- Teixeira, with an IT job, gained access to classified material and shared it on Discord to appear cool.
- The bizarre scenario led to theories of a foreign intelligence operation or an information leak operation.
- The report confirmed the events and criticized the lax security procedures, resulting in 15 military personnel disciplined up to Colonel rank.
- Some coworkers knew Teixeira accessed unauthorized material, raising questions on why no action was taken.
- Despite recent news on leaks and compromised information, security procedures were neglected, indicating a need for a security culture revival.
- Teixeira has been charged, pleaded not guilty, and plans to go to trial.
- More actions may follow due to the lax security culture within the military.
- Anticipates changes in how the U.S. handles classified material given the frequent breaches.
- Concludes with a prediction of upcoming changes in handling classified information.

### Quotes

- "The scenario was so bizarre that there were a lot of people who believed either A, it was a foreign intelligence operation, or B, it didn't actually happen..."
- "The way the information was handled was so bad."
- "There's probably more to it."
- "I expect a lot of changes when it comes to how the U.S. handles classified material."
- "Y'all have a good day."

### Oneliner

Beau gives an update on Teixeira's case, revealing security breaches and the need for a security culture revival within the military.

### Audience
Military personnel, security professionals

### On-the-ground actions from transcript

- Monitor and report unauthorized access to classified material within your organization (implied)
- Advocate for stronger security procedures and culture within your workplace (implied)
- Stay informed about updates and changes in handling classified information (implied)

### Whats missing in summary

Insights on the potential consequences of lax security procedures and the importance of accountability in handling classified material.

### Tags

#Security #ClassifiedMaterial #Teixeira #Military #IntelligenceCommunity


## Transcript
Well, howdy there, I don't know people, let's bow again.
So today we are going to talk about Teixeira and documents
and provide a little update on what's going on there
because the internal report is now out.
And it gives us a pretty clear picture of what happened.
And it also undercuts a lot of theories
that we're developing.
And when I say theories there,
I mean like actual theories, not like, you know.
Okay, so if you don't remember who this is, this is the person who basically kind of had
an IT job, mainly, and gained access to a bunch of classified material.
That material was then shared via Discord in what appears to be an attempt to make himself
look cool.
The scenario was so bizarre that there were a lot of people who believed either A, it
was a foreign intelligence operation, or B, it didn't actually happen and the whole
thing was an information operation designed to leak the information that he put out.
The reason people believed theories like this, and I have to admit that last one I entertained,
The reason people believed theories like this is because it was just...it was bad.
The way the information was handled was so bad.
It was one of those things where they couldn't believe that would happen the way it was described.
So what does the report say?
No it happened the way it was described.
And the security procedures there were incredibly, incredibly lax.
So lax in fact that 15 people have been disciplined for it within the military all the way up
to Colonel.
So there were allegations within the report that some of Teixeira's coworkers were actually
aware that he was accessing stuff that he shouldn't.
And somehow this didn't go anywhere.
I would expect that given all of the news lately that has been about leaks and information
being compromised, people working for a foreign intelligence service for decades, stuff like
that, that the US is probably about to have a security culture revival inside the military
in the intelligence community. They've dropped the ball a lot, and this one was, it really looks
like they had everything they needed to know to stop it, and were just like, yeah, kind of whatever.
Um, the information was shared with a small group of people on Discord, but then it circulated more
widely. Now, at time of filming, Teixeira has been charged. He's entered a plea of not guilty,
and I believe it looks like he plans on taking it to trial. We'll see how that plays out as
things move forward, but that's where it sits right now. The 15 or so people within the military
that we're disciplined over this. That may not be the end of it. You know, getting
up to kernel is getting pretty high up the chain, but there may be more to it
because somebody should have noticed the lax security culture there.
There's probably more to it. It will be handled more in-house, but I would
expect a lot of changes when it comes to how the U.S. handles classified material
because there's just been an endless stream of breaches. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}