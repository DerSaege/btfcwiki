---
title: Let's talk about SCOTUS, Washington, and conversion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-Kj1nJ0zsdA) |
| Published | 2023/12/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court of the United States did not take up a case regarding Washington State's ban on conversion therapy.
- Washington State banned conversion therapy, a discredited practice aiming to change someone's orientation or gender.
- Lower courts are divided on whether such bans can exist.
- The Supreme Court's decision to not hear the case means the ban stands, but it could have been a nationwide decision.
- The core issue isn't about free speech but whether states can regulate conduct under professional licenses.
- Allowing such speech under professional licenses could weaken state licensing systems.
- Beau believes the Supreme Court should have taken up the case to affirm state regulations.
- The conservative justices likely avoided the case due to the inevitable ruling in favor of state regulation.
- The Supreme Court's decision maintained the ban on conversion therapy but missed an chance for a broader impact.
- Allowing such speech could lead to harmful practices without legal repercussions.

### Quotes

- "The core of this case is whether or not the state has the ability to regulate conduct that occurs under a state issued professional license."
- "It's a win but it was an opportunity for it to be an even bigger win."
- "The Supreme Court's decision to not hear the case means the ban stands, but it could have been a nationwide decision."

### Oneliner

Beau explains why the Supreme Court's decision on Washington's conversion therapy ban is more significant than just a win, revealing the core issue of state regulation over professional conduct.

### Audience

Activists, policymakers

### On-the-ground actions from transcript

- Advocate for policies that protect vulnerable communities from harmful practices (implied)
- Support organizations working to uphold state regulations on professional conduct (implied)

### Whats missing in summary

More context on the potential impact of allowing harmful practices under professional licenses.

### Tags

#SupremeCourt #ConversionTherapy #StateRegulation #LegalJustice #ProfessionalEthics


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
the Supreme Court of the United States,
and we're going to talk about Washington State.
And the Supreme Court deciding not to take up a case,
and there's a whole bunch of people celebrating
that as a win, and it is in a way.
But we're also gonna talk about why I agree
with the dissents in part.
I really think the Supreme Court
have taken up this case. Okay, if you have no idea what I'm talking about, the
state of Washington has a ban on something called conversion therapy. You
can probably assume, by the way that I said that, that let's just say that it
is widely viewed as discredited. The purpose of it is to change
somebody's orientation or gender, and that's the whole goal behind it.
The widely accepted view is that it's not real. It's scientifically discredited, okay?
The state of Washington, acting on this, going with best practices, they have a
ban on this kind of therapy. Somebody took it to the Supreme Court trying to
get that ban overturned. The Supreme Court decided not to hear the case. So
the ban stands and a whole lot of people are cheering and I get it because it's a
win. The thing is lower courts are divided on this. In some places a ban
like that can't exist. I believe the Supreme Court should have taken it up
because they can only decide one way and that's in favor of the state. They can't
decide any other way. In the media this is being framed as a quote free speech
case. No it is not! Who told you that? The core of this case is whether or not
the state has the ability to regulate conduct that occurs under a state
issued professional license. That ban, it applies to licensed therapists. That's
the actual question. The court, the Supreme Court, has to rule in favor of
the state, otherwise it undermines the entire state licensing system throughout
the country. So I wish they had taken it up. To understand the impacts of this,
let's do it this way. Let's say Memaw, she has an ailment and she has a nurse
that takes care of her and she trusted this nurse and this nurse tells her one
day you know that element you have if you proved that you had faith it would
go away there's a rattlesnake in the backyard that's just speech but it's
under a professional license that's why they don't have a choice in how they're
going to rule on this. And then if they were to rule and say that that's okay,
please understand that when grandma's faith is less than adequate in a
moment of doubt, and that venom is stronger than her faith, you don't even
get to sue. And that's not like a slippery slope argument. That's literally
what's being argued is that that kind of speech would be permissible. So yeah, it's
a win in the sense that the ban gets to stay in effect, but it's also a loss
because this could have been an opportunity for this to go nationwide.
Even this court, as conservative as it is, they would have had to have ruled in
favor of the state because no matter how people talk about it being a free speech case, at
the end of the day the real question being settled is whether or not the state has the
ability to regulate conduct that occurs under a state issued professional license.
That's what would be decided and the Supreme Court is not going to disrupt that.
They're not going to say that the state can't regulate the conduct under a state-issued
professional license.
If they did, there's no reason to have those licenses.
They go away, which I am fairly certain that that is not something that the Supreme Court
would want because it would very quickly apply to, I don't know, investment
brokers, accountants, all kinds of things and whatever they say, wow that's just
free speech. It's a win but it was an opportunity for it to be an even bigger
win. That's the reason the conservative justices chose not to take it up because
there is no way they can decide that the state doesn't have that power.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}