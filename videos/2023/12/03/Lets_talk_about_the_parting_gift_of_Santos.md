---
title: Let's talk about the parting gift of Santos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xAA-5tIZNrc) |
| Published | 2023/12/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Santos and his impact on the Republican Party.
- Santos has been expelled but plans to file official complaints with the Ethics Committee.
- Allegations against colleagues from New York and New Jersey, including specific accusations.
- Santos' history makes Beau hesitant to immediately look into the allegations.
- The allegations range from improper stock trading to money laundering.
- It's unclear whether the complaints will be filed and if they hold substance.
- Santos is not fading away, indicating a significant media profile.
- Republicans are now stuck with addressing Santos due to their delayed response.
- Beau hints at a lesson not learned from a past politician's situation.
- The potential impact of the allegations on those inclined to believe conspiracy theories.
- Regardless of the truth, the statement could harm those involved politically.
- The saga of Santos is far from over, with more to unfold.
- Beau refrains from detailing the allegations tied to specific names.
- The influence of Santos' statement on political perceptions.
- The uncertainty on how the situation will unfold.

### Quotes

- "Santos is not just going to go away, which is what I think most Republicans were hoping for."
- "Those people who are primed to believe in conspiracy theories by the Republican Party, they were kind of encouraged to do so."
- "I don't think we have seen the final chapter yet in the Santos saga."

### Oneliner

Beau talks about Santos's impact on the Republican Party, his specific allegations against colleagues, and the potential political fallout, hinting that the saga is far from over.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed about updates regarding Santos and his allegations (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Santos's allegations and their potential consequences, offering insights into the political dynamics at play.

### Tags

#Santos #RepublicanParty #EthicsCommittee #Allegations #PoliticalImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to once again talk about Santos.
We're going to talk about his parting gift
to the Republican Party.
If you have missed the news,
the representative has been expelled.
However, he decided to make a statement,
a rant I guess might be a better word and in it he promised that on Monday he
would be filing official complaints with the with the Ethics Committee at
Congress. The allegations are about his colleagues. They are pretty specific. If
this was anybody but Santos, I would have already started looking into them. At
the same time, it is Santos who does have a has a history that leads me to not be
super interested in being the first one to dig into them. However, the allegations
are incredibly specific and they deal with three Republicans from New York and
one Democrat from New Jersey that were by name. It seemed as though there were
more and they range from everything from maybe a little bit of improper trading
of stocks to like money laundering. So we'll have to wait and see a if the
complaints are actually filed and b if there's any substance to them regardless
of regardless of the validity of these claims or whether or not Congress even
and pursues them. It became pretty clear that Santos is not just going to go away,
which is what I think most Republicans were hoping for. That doesn't appear to
be likely at the moment. It seems as though Santos understands that he got a
pretty big profile. The media will listen to what he says because the Republican
Party waited so long to address the issue and now they're kind of beholden
to a man that sounds familiar. It certainly sounds like a lesson they
should have learned with another politician who is no longer in office.
But the claims were made I'm not going to repeat them and tie them to anybody's
names because they are from Santos and we'll wait and see what an investigation
turns up if the complaints are even filed. But regardless of whether or not
there's any validity to them. Those people who are primed to believe in
conspiracy theories by the Republican Party, they were kind of encouraged to do
so, they'll probably believe these. Regardless of whether or not it's true,
just the statement is going to to harm them politically. So we'll have to wait
and see how this plays out, but I don't think we have seen the final chapter yet
in the Santos saga. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}