---
title: Let's talk about the evolving US position and changing relationships....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aRLMiKavY9s) |
| Published | 2023/12/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about how relationships change over time in international affairs, especially in diplomacy.
- Describes how relationships between countries shift behind the scenes first before going public.
- Mentions the subtle changes in U.S. diplomacy towards Israel over the last month or so.
- Indicates a significant shift in the U.S. position regarding the forced relocation of Palestinians from Gaza or the West Bank.
- Mentions the public statement from the vice president's office regarding the U.S.'s stance on Palestinian relocation and Gaza borders.
- Notes that the relationship shift between the United States and Israel is becoming more public due to Netanyahu's response to U.S. advice.
- Explains the escalation of public statements in international affairs to generate more pressure.
- States that cutting all aid to Israel is unlikely due to foreign policy implications.
- Attributes the shifting relationship dynamics to changing demographics within the United States.
- Emphasizes the importance of focusing on government actions to influence foreign policy outcomes.

### Quotes

- "Under no circumstances will the United States permit the forced relocation of Palestinians from Gaza or the West Bank."
- "It's not foreign policy, it's not diplomacy, it's not how it works."
- "This is probably where it's going to end."

### Oneliner

Beau explains the shifting U.S. stance on Palestinian relocation and Gaza borders, reflecting changing relationship dynamics with Israel in international affairs.

### Audience

International policy observers

### On-the-ground actions from transcript

- Contact your representatives to express your views on U.S. foreign policy regarding Israel and Palestine (suggested).
- Join or support organizations advocating for peaceful resolutions in international conflicts (implied).

### Whats missing in summary

Insights on the potential implications of the U.S.'s evolving position on Israel-Palestine relations for future peace processes.

### Tags

#InternationalAffairs #Diplomacy #USForeignPolicy #Israel #Palestine


## Transcript
Well, howdy there Internet people, it's Bo again.
So today we are going to talk about how relationships change over time
in international affairs
and how most times
when you're talking about diplomacy
relationships between countries
they shift behind the scenes first.
That way the various countries involved
they know the new position before that new position goes public.
Over the last month or so
we have talked about how U.S. diplomacy
has been going on behind the scenes
and just creeping along
little changes, little changes, little changes
and how the relationship
between the United States and Israel is shifting.
During this period, I've gotten a lot of pushback,
and people have basically said, OK, well, if it's changing,
what is it going to change to?
And I haven't really stated an answer because I didn't know.
I could see the signs that it was changing,
but I didn't know how far we'd go.
What if I was to tell you that the United States was going
to soon adopt the position that under no circumstance would it accept the forced
relocation of Palestinians from either Gaza or the West Bank and that it would
not accept their redrawing of borders concerning Gaza. So the idea that Israel
is just going to keep the north end of Gaza which is something that is very
prominent right now, it's not going to happen. That the US would view that as
something that might actually change a vote at the UN. I think if I said that
most people would say that I've lost the plot. I'd like to read you a statement.
Under no circumstances will the United States permit the forced relocation of
Palestinians from Gaza or the West Bank, the besiegement of Gaza or the redrawing
of the borders of Gaza. Right now the vice president of the United States is
at COP 28. While she was there she met with the Egyptian president. This
statement is from her office. It's from the White House. The shift that is
occurring in the relationship is becoming more public. Partially, in my
opinion, it's because Netanyahu is less than a receptive when it comes to US
advice or U.S. concerns. So rather than it all being done behind the scenes, how
it was earlier, it's becoming more public in hopes of generating more
international pressure. And this is generally how it works. It kind of
escalates. Offhand statement, then a tweet, now you're getting a statement from
the vice president. Eventually it'll be from State Department or the president.
That shift is occurring. The relationship is changing. Now of course there are
going to be people who say well they need to do X, Y, or Z. A big one right now
is to cut all aid. That's not going to happen. That isn't something that's going
to occur whether you believe it's the right thing or not, there's a whole
bunch of foreign policy implications to that way down the road, but the
relationship itself is definitely shifting and a lot of it has to do with
shifting demographics within the United States. It wasn't that long ago when
absolutely unconditional support for Israel was just a given because it was
an incredibly safe bet politically at home. It's not anymore and since it's
not it's being reflected in the foreign policy. This is what we were talking
about when we're talking about how if people really wanted to affect the
outcomes when it comes to foreign policy they have to focus on what their
government does. This statement is straight from the vice president's
office and it did occur after the meeting with the Egyptian president. It
is worth noting that that is also balanced with the idea that the United
States supports, I believe they were called, legitimate military objectives.
Israel's legitimate military objectives within Gaza. And then that statement was
kind of balanced with, but the civilian cost has been too high and it can't keep
going like that.
People, generally speaking, when you're talking about any issue, especially one
like this. People like things that are black and white, all or nothing, you're
pregnant or you're not type of thing.
That's not foreign policy, it's not diplomacy, it's not how it works.
It is a subtle shift but this has taken place over the last 45 days.
this is probably where it's going to end. There might be a little bit more but
generally speaking that this is going to be probably the new position that the
US has and then where it goes from here
well that's a that's an entirely that's an entirely open-ended question as far
as how any future peace process moves forward.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}