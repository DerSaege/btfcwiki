---
title: Let's talk about Trump, SCOTUS, and Biden staying in office....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=p-JtdfWF79k) |
| Published | 2023/12/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent ruling on presidential immunity in a civil case involving Trump.
- Mentions a subsequent ruling on Trump's immunity in criminal cases while president.
- Quotes Judge Chutkin's decision that former presidents are not immune to federal criminal liability.
- Speculates that Trump will likely appeal the decision.
- Expresses doubts about the current Supreme Court siding with Trump on this issue.
- Argues against the concept of immunity for former presidents based on actions taken while in office.
- Points out the potential dangerous consequences if presidential immunity was extended indefinitely.
- Believes the Supreme Court is unlikely to support Trump's argument on presidential immunity.
- Cites previous instances where the court ruled against former presidents.
- Concludes that current court decisions indicate no immunity for Trump in civil or criminal cases.

### Quotes

- "The United States has only one chief executive at a time, and that position does not confer a lifelong get-out-of-jail free pass."
- "There's no reason for any president to ever willingly leave office."
- "The current decisions by the court suggest that presidential immunity does not apply to Trump."
- "They're basically just daring Biden to try to hold on to power."
- "It's just a thought."

### Oneliner

Beau explains recent rulings denying Trump presidential immunity in civil and criminal cases, casting doubt on potential Supreme Court support and warning against extending immunity indefinitely.

### Audience

Legal scholars, political analysts.

### On-the-ground actions from transcript

- Monitor updates on Trump's legal battles and potential appeals (suggested).
- Stay informed about Supreme Court decisions and implications for presidential immunity (suggested).

### Whats missing in summary

Insights into the potential impact of ongoing legal battles on future presidential accountability.

### Tags

#Trump #PresidentialImmunity #LegalRulings #SupremeCourt #Accountability


## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about Trump
and presidential immunity again.
This is a different video, just so you know.
Yesterday, we talked about a decision that said
Trump did not have presidential immunity
when it came to a civil case.
And in that video, I was like, you know,
just remember the supplies to civil cases.
You know, there's this whole other thing that has to go on when it goes to cases that
are more in the criminal purview.
Like an hour later, there was a ruling on that.
So what happened?
Basically Trump made the same kind of claim.
I was president at the time.
I'm totally, absolutely immune.
I can do whatever I want.
I'm immune.
Doesn't matter.
Judge Chutkin disagreed and said,
whatever immunities a sitting president may enjoy,
the United States has only one chief executive at a time,
and that position does not confer a lifelong
get-out-of-jail free pass.
Former presidents enjoy no special conditions
on their federal criminal liability.
Defendant may be subject to federal investigation,
indictment, prosecution, conviction,
punishment for any criminal acts undertaken while in office,"
Chutkin wrote. Okay, so that's the decision. What does that mean? More than
likely means that Trump is going to appeal this and there are people who are
concerned about how the current Supreme Court might rule in this situation and I
I get it, given the current temperament of the Supreme Court.
That being said, I don't even think this Supreme Court is going to go with Trump on this one.
The idea that a former president cannot be held responsible for the actions that they
undertake while president is, I mean, that's quite the ruling.
If that ruling was made, I mean, let's just be clear.
There's no reason for Biden to ever leave office regardless of the election.
If that was true, if that kind of immunity really did apply, then there would be no reason
for any president to ever willingly leave office.
They could engage in whatever methods they wanted to to try to retain power because there's
no penalty.
It's not something that is going to hold up.
I don't believe that even this court would side with Trump's argument on this.
It seems incredibly unlikely because the long-term effects of that decision, they far outweigh
any potential loyalty that the justices might have to Trump.
And I would remind everybody that there have been instances where this court decided in
a way that was not necessarily favorable to the former president in matters that
were much lower stakes. So the the current decisions by the court suggest
that presidential immunity does not apply to Trump in either the civil or the
criminal aspects of what's going on right now in these cases. And the current
framing of it, if argued that way before the Supreme Court, would lead to a
situation where the Supreme Court, if they sided with Trump, they're basically
just daring Biden to try to hold on to power. Doesn't seem likely. Anyway, it's
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}