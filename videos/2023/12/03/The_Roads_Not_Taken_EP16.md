---
title: The Roads Not Taken EP16
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eMrhJegs7r8) |
| Published | 2023/12/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of world events, unreported news, and interesting tidbits from the previous week.
- EU pressuring China over support of Russia to evade sanctions.
- Russia using "active defense" instead of "offensive" terminology.
- Tensions between India and the United States due to a hit-for-hire plot.
- Trump making controversial statements about American democracy.
- Various news snippets: pro-Palestinian demonstrator, Olympic gold medalist avoiding prison, inmate stabbing Chauvin, Democrats launching super PAC in New Hampshire, Sandra Day O'Connor's passing, Texas judge releasing law enforcement records.
- MAGA potentially boycotting Walmart over pride support.
- Elon Musk's response to advertisers blackmailing him.
- COP28 agreement on methane criticized as greenwashing.
- Nicaragua accuses Miss Nicaragua pageant director of rigging contest to overthrow government.
- Addressing viewer questions on voting, Trump testifying, G.I. Joe shirt, and Project 2025 concerns.
- Handling panic in emergency situations and dealing with vehicle accidents.
- Managing disagreements between strong-willed individuals.
- Utility worker's perspective on lead service line rule and inventory creation.

### Quotes

- "If every presidential race is a vote to save democracy, doesn't that mean democracy is already toast?"
- "Democracy is advanced citizenship."
- "Closing his mind to the right will open it to the left."

### Oneliner

Beau covers a range of global events, from EU-China tensions to domestic news like Trump's comments, addressing viewer questions on voting and emergency responses.

### Audience

Viewers interested in global events and political analysis.

### On-the-ground actions from transcript

- Take a wilderness first responder course to be better prepared for emergency situations (implied).
- Research and enroll in online first aid courses or Stop the Bleed courses for basic medical training (implied).
- Stay informed about Project 2025 and potential implications for the future (suggested).
- Start difficult but necessary conversations with individuals holding opposing political views (implied).
- Proactively address potential safety concerns in your community, such as lead service line issues (implied).

### Whats missing in summary

Insights on the importance of staying informed about global events, engaging in political discourse, and being prepared for emergencies by acquiring relevant skills and knowledge.

### Tags

#GlobalEvents #PoliticalAnalysis #EmergencyPreparedness #Voting #LeadServiceLines


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to The Roads with Bo.
It is December 3rd, this is episode 16
of The Road's Not Taken, which is a weekly series
where we go through the previous week's events
and take a look at news that was unreported,
under-reported, or it will serve as context later.
In some cases, it's just something I found interesting.
After that, we will go through some questions from y'all.
Okay, so starting off with foreign policy.
Reporting suggests the EU will press China
about its support of Russia.
And the real concern is that some Chinese companies
are helping Russia sidestep the sanctions.
are helping Russia sidestep the sanctions.
The reporting suggests that if the European Union
cannot get some kind of commitment from China on this,
cannot get some kind of commitment from China on this,
that some of these Chinese companies
may end up on the next sanctions list.
We're probably going to find out how far that friendship
with no limits thing is really going to go here.
OK, the term active defense is being
used instead of the term offensive by Russian officials.
The terminology change is believed to be designed to lower expectations of felled Russian offensives.
That's active defense.
That's new.
Okay, India and the United States are experiencing some increased tensions right now after the
United States charged an Indian national in a hit-for-hire plot.
Further complicating things is the accusation that the whole thing was directed by an Indian
intelligence officer and the plot was to take out at least four people into different countries.
This does seem to echo Canadian allegations from earlier.
Now while long-term damage to the relationship between the United States and India or Canada
and India is unlikely from this, the trial will create a situation that generates public
interests and it may create a political situation that is hard to control.
The real relationship isn't going to change much, though.
That seems just incredibly unlikely.
Okay, so moving on to U.S. news, Trump, in what I'm sure he will call another sarcastic
moment, said, quote, from that day on our opponents, and we have a lot of opponents,
but we've been waging an all-out war on American democracy.
We know.
We know you have.
We're aware of that.
It's just weird to hear it said.
I'm sure that he misspoke, but I would imagine that that little soundbite is going to be
used over and over again as as time moves on. Okay, a pro-Palestinian
demonstrator set themselves on fire in Atlanta outside the Israeli consulate.
Keller, who is an Olympic gold medalist, wound up avoiding prison and was given a
combination of home detention, probation, and community service for his role in January 6th.
The inmate who stabbed Chauvin, the cop in the George Floyd case,
The cop was stabbed 22 times and that inmate has been charged with attempted murder.
Democrats are launching a super PAC in New Hampshire to encourage residents to write
in Joe Biden's name.
Biden's name will not be on the ballot because New Hampshire is not following the DNC rules.
This is probably the only chance for candidates like Marianne Williamson or Dean Phillips
or anybody else who wants to actually primary Biden.
This is their chance to make a splash and get some momentum.
So it puts them on a more even playing field.
If they're going to make a move, they need to do it in New Hampshire, and they've got
a limited amount of time to get the things rolling there.
Sandra Day O'Connor, the first woman on the Supreme Court, has passed away.
In Texas, a Texas judge has ordered the release of records related to the law enforcement
response at Uvalde.
I would expect appeals on that.
It seems that the authorities have a vested interest
in keeping that information quiet.
Let's see.
Oh, just in case y'all can hear it, that's rain.
For those who miss the rain on the tin roof,
it's actually something you can hear now
Cultural news.
MAGA seems to be gearing up for another one of its super successful boycotts.
This time, they're apparently looking at Walmart.
I guess it's because Walmart might be pulling their advertising from Twitter.
but it supports pride and this has caused an issue.
I feel like getting MAGA to boycott Walmart
is gonna be a big task.
Certainly seems like it.
In related news, Elon Musk said that advertisers
should go have fun with themselves
if they were going to try to blackmail him
over content concerns.
And if you've missed that whole thing,
there appears to be a number of major companies
that are pulling advertising from Twitter or X.
So that's the development there.
In environmental news, so at COP28, an agreement on methane
is just being trashed, and it's being called greenwashing.
in an open letter, a whole bunch of environmentalist groups,
they referred to it as, quote,
another set of hollow voluntary pledges.
Basically, there was a feeling that this was the chance
to do something, and it didn't happen.
In oddities, the government of Nicaragua
has seemingly accused the director of the Miss Nicaragua
pageant of rigging the pageant so anti-government contestants
would win as part of a plot to overthrow the government.
I didn't really read too much beyond that.
I don't know if that's as silly as it sounds or not.
That's something I'll have to look into tonight or tomorrow
morning.
But just the framing of that, I mean,
I can't wait to see the headlines,
the beauty queen coup or something like that.
All right, moving on to Q&A.
Quick question.
I hear a lot that Trump will never be asked
by his lawyers to testify or be on the stand.
Can Jack Smith call Trumps to the stand?
That's not gonna happen.
Um, so, Trump has made a lot of promises saying that he's going to testify.
I don't believe he will.
I really don't.
Not if he has a, uh, not if he has any alternative.
Bo, while watching your videos, it's possible for the viewer to cover your face with their
hand and lip read your beard.
What?
I'm going to be honest, I have no idea what that means.
The most important and difficult question you'll ever be asked.
Where is Springfield in The Simpsons?
Can't answer that, can you Mr. Smarty Pants know it all?
According to the show's creator, it's based on a town in Oregon.
However, if you're looking at the show and you're just going off stuff in the show,
it's in North Dakota.
In one of the episodes, Moe, the character Moe, he said that he moved there because the
town's zip code spelled something out on a calculator, 80085 is not a zip code.
However, 58008 is in North Dakota.
Okay, Gen Z viewer here.
Most of my fellow young leftists are in strong debate over the efficiency of presidential
voting as risk reduction.
When your two choices would do nothing to stop the slaughter of civilians, where does
the power of choice really lie for those of our mindset and objectives.
I personally am hesitant to commit to that idea, but I don't have the heart to argue
against it like I did in 2020.
I'm a black American, so voting between the lesser of two evils isn't something people
like me haven't had to do for decades.
But if every presidential race is a vote to save democracy, doesn't that mean democracy
is already toast.
OK, so I'd like to point out, I don't tell people how to vote.
OK, so I mean, this appears to be
a question between voting or not voting is what it looks like.
It doesn't seem like this is one talking about a protest
vote or something for a third party or something like that
when your two choices would do nothing.
Think about if it was me and this was the dilemma
I was faced with, I would look at it like, OK, you're a leftist.
Both candidates support capitalism, right?
So that's not something you expect to see a difference with.
And when it becomes something that's accepted like that,
it just cancels each other out, and you look to harm reduction
in that fashion.
When it is this issue, and you don't
feel that they're doing enough, I mean, I can see how that's a little bit different.
So I mean, if your view is that there is no difference between the two candidates on that
issue and that issue is a priority for you, and this is for any issue obviously, then
you can either look at it through the lens of, well, it cancels each other out, so move
to the next issue that's important to you, or you could say it's a deal breaker.
I mean, those are your options.
But if every presidential race is a vote to save democracy, doesn't that mean democracy
is already toast?
No, democracy is advanced citizenship.
The problem is that people believe democracy will sustain itself based solely on voting.
It will not.
Voting is the least effective form of civic engagement.
It's the one that is most commonly used, but it's the least effective.
It's not enough to keep democracy functioning.
I have said, I've said since before Biden won, that if people want an end to Trumpism,
it has to be defeated soundly at the polls.
Nothing else is going to do it.
It's not going to be, you know, eking out a victory.
It's not going to be something that can be framed as it being close.
They have to be rejected and rejected again.
And either it has to be a continuous losing strategy, which so far it has been, or it
has to be something that is just so lopsided.
It's such a landslide for the candidate who isn't MAGA that they give up on it.
Please understand, many of the people who support this, they're not actually the people
who are ideologically the far-right authoritarians.
They're opportunists.
They don't actually believe in it, they just view it as a means to get power.
If they can't get power that way, they'll change their rhetoric.
Okay.
Wow, this is a long one.
I was driving down the highway just out of a more rural area.
Coming into the highway of a more urban area, there was a van stop slightly diagonal in
the road.
of my flashers in park, it had just got dark.
So I approached the driver and the front is smashed in
with no other vehicles around.
I can see the outline of the driver kind of bobbing
his head.
Okay, so this person came up on a vehicle accident.
And they got out the call.
It looks like law enforcement had been informed
of the vehicle, but they didn't know anybody
was still inside of it.
They thought it was an abandoned vehicle.
Help showed up, but the person did not
feel like they handled it well.
What do I need to do to not feel panicked
in that or similar situations?
What should I have in the car?
OK, so if you're looking for training,
I'm a huge advocate of something called the woofer,
the wilderness first responder, those courses.
They're a little bit more expensive,
but it's kind of the MacGyver's first aid course.
That's the one that I would suggest taking.
And then once you've taken something like that,
then you can build your kit from there.
You'll know what you'll need.
It doesn't do a whole lot of good
to have the medical equipment if you don't know how to use it.
So the first part is training.
And if you don't want to do the woofer or you see that price
tag and you're like, wow, there are online courses,
there are stop the bleed courses,
there's a whole bunch of stuff that is an introduction.
And for what you're describing here, it would have helped.
And it would have answered your big question about whether or not
you can move that person.
Is it uncomfortable when two people
who have been on your channel and who you obviously like
are publicly fighting?
They're both strong-willed women.
I feel like that would be uncomfortable.
Well, it wasn't until I was asked to comment on it.
OK, so if you don't know what's going on,
the Democratic Party in Florida,
who is led by somebody who has been on the channel,
has basically kind of said, well,
we're not really going to do a presidential primary.
We don't need to.
Obviously, Marianne Williamson is not happy about this.
I mean, realistically, both of them are doing what they think is right for the position
that they are in.
I have, I understand the position of the Democratic Party here.
When you look at the polls, people talk about how it's inevitable that Trump becomes the
nominee because of how he is doing in the polls.
When it comes to the Democratic primary, Biden's actually doing even better than Trump.
It's a pretty big lead.
So I get it, you don't want to have a bunch of mudslinging and negative energy out there
about your candidate.
tradition is the incumbent already won the primary, the one that led to them being elected.
All that being said, I'm also kind of a fan of the option being there.
To me, whether or not the decision to do this and basically just say, you know, Biden won
without any voting, it's really going to depend on what happens in New Hampshire, which we've
talked about in this video, which is nice.
Biden's not going to be on the ballot.
He's going to be a write-in candidate.
If Biden still wins as a write-in candidate, I mean, that's a pretty clear indication
that there's a lot of support for him.
If a different candidate wins or even comes close,
it would probably make sense to re-examine this decision.
So I would wait and see what happens in New Hampshire
and go off of that before any firm decisions are made.
But I would like to point out I'm not
the leader of the Democratic Party, and I'm not running for president.
They probably have much stronger opinions about this than I do.
I'm starting to feel bad for your G.I. Joe shirt.
It seems to be taking a beating lately.
Might need a secondary war news shirt.
starting to show its age. Yeah, the one with Baroness on it certainly is. Fun fact, there's
actually multiple G.I. Joe shirts that have different characters on them. The one that is
all faded is the one with Baroness on it, the character Baroness, which is not just used for
for war news, but also used for spy news.
Project 2025 I know you don't talk about partisan stuff
and I totally understand why.
I don't tell people how to vote either.
I was raised that it's a personal thing and no one else's business.
I wasn't even allowed to ask my parents who they voted for.
But I mentioned to a leftist on Twitter that I was afraid.
My intention was to vote third party this time, but for reasons I'm pretty sure you
already know, or for reasons I'm pretty sure you already know, but this project has me
really scared.
Like really scared for people I love.
I consider myself a leftist too, or maybe a leftist in training, but I'm not sure I'm
willing to cast what amounts to a protest vote with this hovering like a nuke.
Um, when it comes to project 2025, yeah, it's something to be concerned about it
is something to be concerned about.
So, so concerning in fact, that the reason I haven't covered it is because I am
holding it until we know who the candidates are.
I'm worried that people are doing what they did in 2016 and they're normalizing it.
If you're not familiar with this, it's a rough sketch and it is something that I am
concerned about.
You will get a video on this once we know who the candidates are, because I think it
would be important to have those candidates specifically respond to what's in that plan.
And if you have no idea what I'm talking about, you can Google it.
You can Google Project 2025, it will come up.
There are a whole bunch of, there's a whole bunch of commentary on it.
A lot of it is going to seem very alarmist, and some of it is, but understand it's concerning.
It's not something that should be blown off.
I have a friend who is independent, right-leaning, libertarian in most cases, who is stubborn
against anything pro-left.
He has many hangups and issues with the left
that keep him from accepting them
as a good option in his mind.
Therefore, I found it was an easier strategy
to, instead of making him like the left,
make him dislike the right.
I've brought to his attention the many barbarous actions
of the right to at least make him less comfortable
about entrusting a vote to them instead
singing the left's praises. Is this a good strategy? Is there any way this can backfire?
Is this even effective since I'm not opening his mind to the left, but instead closing
his mind to the right? If you close his mind to the right, it will open to the left. Can
this backfire, right-leaning libertarian. If free markets and capitalism are
something that is just ingrained, your backfire is that you're going to create
a corporate dumb. I mean that's that's what you're gonna end up doing. So if
you're talking about somebody who is on their way to the far right, I mean that's
an improvement. Just remember, everybody has a path and when you're talking about somebody
who is on the right, getting them to move left is a process and it takes time. And sometimes
it's just one little thing that opens it up. But if they're already a libertarian and you
You start showcasing how the right, and I'm assuming by this you mean the Republican
Party, not just the right in general, if you end up showing them that they are very authoritarian,
What you may end up doing is creating somebody who is incredibly anti-authoritarian, who's
kind of a centrist, with the exception of just being very extreme when it comes to being
anti-authoritarian.
I hope that makes sense.
I'm trying to stick with a long-running joke on the channel of not saying a word.
Think about people who are as anti-authoritarian
as humanly possible.
Most of them are also on the left.
I feel like you did a disservice in your coverage
of the new rule on lead service lines in water systems.
I work for a utility, though I am writing personally,
not representing the utility.
For the past several months, I have
been working on creating the inventory required by this rule.
It requires us to create an inventory of something
we do not own and provides no legal mechanism for us
to comply.
We will be using machine learning and random sampling
to establish the inventory.
We still haven't worked out the private property access.
Yeah, that's going to be a thing.
That's going to be an issue.
In our system, there are no known lead pipes,
and any lead found is removed.
There are around 2,500 lead goosenecks
that are not classified as lead under the rule.
We already target pipeline replacement projects
to eliminate these.
Corrosion inhibitors are in use, and we
have no positive lead samples.
The problem in Flint occurred after the water source
was changed without understanding the impact
on the water chemistry.
We will spend around a million dollars creating this inventory,
and I honestly see no public health benefit from this.
And then from here, it goes on to say some other things.
But given the fact that the person went out of their way
to say, I am writing personally, I
feel like that means that the utility they represent
would not be happy about them writing this.
So it gives a lot of specifications
and points out some other issues.
OK, so when it comes to this, you're
It's a matter of you are working in a utility that
is apparently doing everything it can
and doing everything right.
When it comes to Flint, before everything went bad,
they were doing the same thing.
It's a risk that doesn't need to be there.
Yeah, getting at the private property access,
That's going to be an issue.
And it'll be one of those things that ends up
being fine-tuned later.
And there are probably going to be other issues with this.
Now, as far as the money, it sounds
like this is something y'all are already doing.
And the rule isn't in effect yet.
So this is something that is being done because you're
being proactive at the place where you work, which is good.
I would point out that the money is,
there should be a package from the feds
coming if the new rule does go into effect.
I have to be honest.
This is the first time I've ever gotten
a message that started with, I feel
like you did a disservice in your coverage that
just wasn't garbage.
You're bringing up points about the practicality
of enacting the rule.
Yeah, it's not a small project.
It's huge.
It is huge.
But from what you have here, especially some of the stuff
that I didn't read, as far as some of the things
y'all do to keep it safe, remember
that maybe not every utility is like that.
And when it hits a point where the feds have to get involved,
they don't really carve out a whole lot of exceptions.
So I mean, I understand what you're
saying as far as the difficulty, the cost.
And in some places, it won't be needed,
because they're already doing what they can.
But there are issues that have to be addressed, especially
since when it comes to Flint, the people who took it from,
OK, it was working, to no, it's not,
there wasn't a whole lot of accountability there,
which might lead to other people doing the same thing,
being less than safe with it.
OK, so that looks like it.
And yeah, that's all of it.
And that's all of the questions.
So there's a little bit more context, a little bit more
information.
And having the right information will make all the
difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}