---
title: Let's talk about NY, DC, and 3 Trump losses....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qsnuotsyPlc) |
| Published | 2023/12/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining two cases against Trump in New York and DC, with significant losses for Trump.
- Mentioning a DC case brought by cops impacted by January 6th against Trump where his immunity claims failed.
- Noting two losses in an E. Jean Carroll case in New York where Trump's attempt to delay the case based on immunity was unsuccessful.
- Detailing Trump's team's attempt to remove an expert witness in the E. Jean Carroll case who calculates damages, which the judge denied due to being too late in the game.
- Speculating that damages in the E. Jean Carroll case are expected to be significant, possibly leading to a large payout.
- Anticipating more movement in both cases soon, especially the E. Jean Carroll case starting on January 16th.

### Quotes

- "He tried to use his immunity claims there, And the judges said that the argument, quote, fails."
- "The expectation is that it is going to be a pretty big number."
- "You didn't turn in your homework on time, basically."

### Oneliner

Trump faces significant losses in two cases in DC and New York, with immunity claims failing and the expectation of a substantial payout looming.

### Audience

Legal observers

### On-the-ground actions from transcript

- Follow the developments in the legal cases against Trump, especially the E. Jean Carroll case starting on January 16th (suggested).
- Stay informed about legal proceedings and outcomes related to accountability for public figures (suggested).

### Whats missing in summary

Insights on the potential implications of these legal battles for future accountability and implications for other similar cases.

### Tags

#Trump #LegalCases #Accountability #EJeanCarroll #DC #NewYork


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
in New York and DC.
We will talk about two cases and three pretty big losses
for Trump and where everything goes from here.
These are two cases we haven't really been following
very closely and it's good to catch everybody up
because some of these are, they're coming up pretty soon.
Okay, so the first one that we're gonna talk about is a DC,
a DC case brought by cops who were impacted
by January 6th against Trump, the suits against Trump.
He tried to use his immunity claims there,
And the judges said that the argument, quote, fails,
and that this case is indistinguishable from other
cases where the rulings have already been made.
So his attempt to delay that, not happening.
The other two losses both occurred in an E. Jean Carroll
case up in New York.
The first is one where he attempted
to delay the case based on his magical immunity.
That is not happening.
And it looks like the case will move forward on January 16,
is when the trial starts.
And this is another one where the real purpose of it
is to determine what the damages are.
In the same case, Trump and his team
were kind of frantically attempting
to get rid of a witness, an expert witness
from the other side.
The witness is somebody who calculates damages.
And Trump's team really, really wanted this person
to not be involved in the calculations.
You might remember them from another case
they were recently involved with involving Rudy Giuliani in Georgia, you know, that led
to the $148 million thing.
I can understand why Trump would be nervous about that particular expert witness being
used, but the short version is the judge was like, yeah, you've had a really long time
to deal with this, you're way, way too late
to try to strike a witness, introduce your own witness,
all of this stuff when it comes to this.
It's just too late in the game to do that.
You had plenty of time.
You didn't turn in your homework on time, basically.
There is a pretty big assumption that the damages in the E.
Carol case are going to be huge. The expectation is that it is going to be a
pretty big number and it doesn't really have much to do with the expert witness
in this case. It's just a culmination of events and the general consensus is that
he's looking at a pretty big payout. So we will start to see more movement on
both of these cases pretty soon and from there we'll just follow them and see
how how they progress as time moves on. But the E. Jean Carroll case is the more
pressing one beginning January 16th. Anyway it's just a thought y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}