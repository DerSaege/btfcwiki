---
title: Let's talk about Nukes, NEST, and Vegas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KAolMq6W57s) |
| Published | 2023/12/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a theory circulating about the National Nuclear Security Administration looking for scary things on New Year's.
- Confirms the rumor that they will be looking for a dirty bomb, which they do annually.
- Describes how the Nuclear Emergency Support Team (NEST) will be involved in scanning for radiation.
- Mentions the use of a high-tech helicopter (Huey) with pods hanging off the side for the scanning.
- Assures that this activity is routine and not due to any specific threat, happening at major events with large crowds.
- Talks about the presence of NEST in places like Vegas and New York for New Year's celebrations.
- Emphasizes not to panic if you see the scanning helicopter labeled as NEST.

### Quotes

- "You will see a Huey."
- "If you happen to see those, don't panic, it's pretty normal as weird as that is to say."
- "If you see anything that's labeled NEST, don't panic."

### Oneliner

Beau explains the routine scanning for dirty bombs by NEST on New Year's, reassuring people not to panic if they see the high-tech helicopter flying low.

### Audience

Event Attendees

### On-the-ground actions from transcript

- Be aware and informed about the routine scanning by NEST for dirty bombs during major events like New Year's (implied).
- Stay calm and don't panic if you witness the scanning helicopter labeled as NEST (implied).

### Whats missing in summary

The full transcript provides detailed information about the routine scanning for dirty bombs by NEST during major events like New Year's celebrations.

### Tags

#Security #NEST #NewYear #RoutineScanning #DirtyBomb


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a theory
that is floating around,
about something that's going to happen on New Year's.
And this is different than a lot of the ones
that I've done in the past.
Normally when I start off with it's a theory,
I'm about to debunk the whole thing.
Not so much in this case.
Okay, so a couple of messages came in about it.
there is a rumor that the National Nuclear Security
Administration is going to be looking for very scary things
over the next couple of days.
If you have never heard of that agency, it exists,
and they have something called NEST, Nuclear Emergency Support
Team.
OK, so yes, they will be looking for a dirty bomb.
They do it every year, OK?
So the theory that is going out, it is 100% true.
They are absolutely going to do this.
I know they will do it in Vegas.
They will do it in New York, maybe in LA,
anywhere where there are large New Year's celebrations planned.
They will show up.
Okay, it's not because of any particular threat.
They always do it.
They also do it at the Super Bowl and stuff like that.
To just alleviate concerns,
I'll tell you what you're gonna see.
You will see a Huey.
It will look like a Huey anyway.
If you don't know what a Huey is,
in every movie about the Vietnam War,
the green helicopter that lands
and everybody gets out the sides
while Fortunate Son is playing,
one of those, only it's probably going to be silver and blue, and it will have these
weird looking pods hanging off the side of it.
And yeah, they fly over relatively low, and they're scanning for radiation, and if they
pick it up, it, you know, sets other things in motion.
It's a scan, it's nothing to actually worry about, they're not doing it because they've
had some credible threat or anything like that it's it's I mean looking for
something like that I don't know that that's something you would call routine
but I mean it it is it's routine they do it pretty often with major events where
there's large crowds that people would know about ahead of time so if you
happen to see those don't panic it's it's pretty normal as weird as that is
to say and what I found as far as the theory that's circulating I actually
didn't see any that were like set up to be panic inducing it's just a statement
of fact, Nest will be looking in this location for a dirty bomb.
I mean yeah, that's a true statement.
I mean some of them are from sources that are always doomsday, you know, scaring people
type of thing, but I didn't see any exaggeration about them, you know, having a threat or anything
like that.
But yeah, so it's a high-tech Huey, it'll fly over, and the only thing about it is that
it's relatively low.
If you see anything that's labeled Nest, don't panic.
If you ever see something that is labeled Nest and it's not around an event like that
where you would expect to see them checking,
then it's okay to panic.
Because they're, I mean, that's just all scary,
to be honest, but this use of it, it's very common.
And again, I know they'll do it in Vegas and New York
because they always do.
So, anyway, it's just a thought.
Y'all have a good day..
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}