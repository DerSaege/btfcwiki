---
title: Let's talk about Georgia's new map and how it isn't over....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lUK4nnBVaRU) |
| Published | 2023/12/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Georgia's congressional districts needed to be redrawn due to violations of the Voting Rights Act.
- A judge ordered the creation of another majority black district, which was done while preserving partisan balance.
- The new map has nine seats for Republicans and five seats for the Democratic Party.
- Despite compliance with the court order, concerns were raised about potential new violations of the Voting Rights Act in a different way.
- The judge approved the new map but did not rule on the new violation, suggesting it may be best suited for another case.
- Affiliates of the Democratic Party might appeal or initiate a new case regarding the map.
- The map approved by the judge is likely to be used in 2024, with little chance of further changes.
- The coalition district and related issues may be decided after 2024, too late to impact the new map's implementation.
- The outcome is viewed as an incremental step in the right direction, not a clear win or loss.
- Judges cannot mandate partisan changes, as it is not within the Voting Rights Act's scope.

### Quotes

- "It's not exactly a win, but it's not a loss either."
- "They complied with the order. They just did it in a way that allowed them to keep their edge."

### Oneliner

Georgia's redistricting saga unfolds as a judge orders a new majority black district while maintaining partisan balance, setting the stage for potential future challenges and decisions post-2024.

### Audience

Georgia residents, Voting Rights advocates

### On-the-ground actions from transcript

- File an appeal or initiate a new case regarding the approved map (suggested)
- Stay informed and engaged in the redistricting process (implied)

### Whats missing in summary

The detailed legal nuances and potential implications of the redistricting decision, as well as the importance of ongoing vigilance in upholding voting rights.

### Tags

#Georgia #Redistricting #VotingRightsAct #PartisanBalance #LegalProcess


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Georgia and maps
and districts and lines and being very creative
and how this story is probably not over,
even though the most likely outcome for 2024's maps
has already been reached.
Okay, so quick recap.
The maps in Georgia, the congressional districts, they needed to be redone.
There was a suit that said, hey, these violate the Voting Rights Act.
And a judge was like, hey, those violate the Voting Rights Act.
Said they needed to create another majority black district.
That was the order.
That's what the judge said to do.
exactly what they did. And they did it in a very creative way, which preserved the partisan balance.
Nine seats for Republicans, five seats for the Democratic Party. Most likely outcome, obviously.
Now, that was exactly what was ordered.
If you looked at the new map and compared it to the old,
You would be forgiven for saying, hey, it looks like they're violating the Voting Rights Act
in a whole new way over here. Yeah, I mean, you might walk away with that conclusion.
In fact, that was brought up in court. And the judge approved the new map,
did not rule on whether or not a new violation in a different way was
occurring and said that that was best suited for another case. So my guess is
that those affiliated with the Democratic Party will appeal and if that
doesn't work, there will be a new case. However, all of that being as it is, the map that the judge
approved, that's most likely going to be the map used in 2024. So, this story isn't over and there
is admittedly a very slim chance that the map that just got approved gets changed again, but
that's really unlikely. So it's not over, but as far as 2024 goes, you're probably looking at
the new map. And the coalition district and all of that stuff that got broke up and how everything
shook out, that will probably end up being decided after 2024. Or it may be decided before then,
but it will be too late to institute a new map with those changes. So that's where that sits.
It's not exactly a win, but it's not a loss either.
It's an incremental step in the right direction.
Remember that judges can't force partisan changes.
That's not part of the Voting Rights Act.
They complied with the order.
They just did it in a way that allowed them to keep their edge.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}