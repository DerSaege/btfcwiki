---
title: Let's talk about Michigan and cleanish energy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sqIi9z13ZPw) |
| Published | 2023/12/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the controversy around Michigan's move to 100% clean energy by 2040.
- Friends who are environmentalists are upset about the redefinition of clean energy in the bill.
- The bill mandates that 100% of electricity comes from clean energy, with 60% from renewables.
- The remaining 40% can be from hydrogen, nuclear, or natural gas with carbon capture.
- People are particularly upset about natural gas with carbon capture being included.
- Beau sees the bill as a positive step, but acknowledges it's not perfect.
- Carbon capture is seen as expensive, inefficient, and still promoting fossil fuel use.
- Beau questions whether advancements in carbon capture technology will make it more efficient.
- Believes new electricity plants won't be built due to costs, existing ones may add carbon capture.
- Acknowledges that despite the flaws, Michigan's move is a significant step in the right direction.

### Quotes

- "Everybody I know that had an issue with this, it had to do with the natural gas."
- "That's what they're mad about, not the overall bill."
- "It's a big step."

### Oneliner

Beau explains the controversy around Michigan's 100% clean energy bill, particularly focusing on the redefinition of clean energy and the inclusion of natural gas with carbon capture.

### Audience

Michigan residents, environmental activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for stricter definitions of clean energy (implied)
- Join local environmental groups to stay informed and involved in energy policy (implied)

### Whats missing in summary

In-depth analysis of the potential impact of including natural gas with carbon capture in Michigan's clean energy transition.

### Tags

#Michigan #CleanEnergy #EnvironmentalPolicy #CarbonCapture #Renewables


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Michigan and energy
and 100% clean energy and what counts as clean energy
and a question, the message that I got was basically,
hey, you know, after that Biden video,
I have an environmental question of my own.
My state, Michigan, just passed a law
that said that they were gonna move to 100% clean energy
by 2040.
I was incredibly excited about this.
My friends who are hardcore environmentalists,
they were really mad and got very animated
and I didn't even understand what they were mad about.
Okay, your friends are not mad that a bill,
guess a law. I think the governor signed it. Moving to 100% clean energy by 2040
got passed. They're not mad about that. They're mad that what counts as clean
energy is kind of redefined in the bill. So I'll go over what I know about it. I
will say overall I think this is a good bill. I think it's a step in the right
direction. It is definitely not perfect though. Okay, so what it does is, as far as
electricity cells, a hundred percent have to be from quote clean energy. When you
and I say clean energy, we probably mean renewables, right? Stands to reason, that's
the clean stuff. 60% has to be from renewables. 40% can be from hydrogen,
nuclear, or natural gas with carbon capture. Everybody who I know that is
upset about this, it's the last one. That's the one they're mad about.
They don't think that that should be counted. And I get it. I get it. Their
argument is that carbon capture is incredibly expensive, it's not really
efficient, and it still promotes the use of fossil fuels. I mean, that's
really the argument. And it's, yeah, it's expensive. That's kind of the reason it
doesn't bother me. When it comes to carbon capture, I don't know enough about
about it to to know whether or not those people who support it actually believe that there are
going to be breakthroughs that make it more efficient and make it cheaper. I don't know
that, but I would assume there are some. To me, when you are talking about electricity plants,
things to generate energy for the state. 2040, I mean, that's not really that far
off. I think because of the cost and the trend when it comes to the legislation,
I don't think any new ones will be built. I mean, I know it's in there, but I don't
expect new construction because it is expensive. I feel like they would
probably go with other alternatives. Now that does mean that natural gas can be
used and they'll have carbon capture stuff maybe added to it or something
like that as far as existing stuff. But this is definitely a step in the right
direction and it's a big step. Michigan, I want to say they're tenth when it comes
to emissions. So the people I know who are irritated with this legislation, it
isn't that it's bad, it's that they redefined this part of it and everybody
I know is upset about the natural gas with carbon capture being included in
that 40 percent. That's what they're mad about, not the overall bill. I would
imagine if that wasn't there, there wouldn't be much opposition to this at
all. I mean, I'm sure there would still be people who don't want nuclear, but I
don't know any of them. Everybody that, everybody I know that had an issue
with this. It had to do with the natural gas. And again, when it comes to carbon
capture, it's expensive and it seems pretty inefficient. At the same time, I
can remember when solar power would barely power a light bulb, and now it's
the generator I use for disaster relief. Maybe it advances. I don't know if that's
there though. I don't know if that's the expectation. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}