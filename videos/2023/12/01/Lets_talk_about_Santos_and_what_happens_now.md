---
title: Let's talk about Santos and what happens now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-SRtfDxFYwk) |
| Published | 2023/12/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative George Santos was expelled from Congress by a relatively decisive vote, shrinking the Republican majority.
- Santos was not convicted of any crime, sparking concerns about setting a new precedent for expelling members without a conviction.
- Despite lacking defenders, Santos faced opposition for his alleged activities, particularly upsetting Republicans by mishandling donors.
- The fear now is that more members could face expulsion for lesser offenses without the need for a conviction.
- The concern about potential future expulsions is shared by many people.
- The allegations against Santos and the knowledge of other Congress members played a significant role in his expulsion.
- This event in Congressional history may end up being overshadowed by other behaviors that have yet to face formal action.
- While currently significant, the expulsion of Santos may diminish in importance over the next few years.
- The situation serves as a reminder of the dynamics and consequences within Congress.

### Quotes

- "This event in Congressional history may end up being overshadowed by other behaviors."
- "The concern now is that there are going to be more attempts to expel people based on less serious behavior."
- "He messed with the money. When he engaged with donors the way he did, that upset a whole lot of Republicans."
- "The concern about potential future expulsions is shared by many people."
- "I think this little portion of Congressional history is going to end up being a footnote."

### Oneliner

Representative George Santos's expulsion from Congress sparks concerns about setting new expulsion precedents without convictions, potentially leading to future expulsions for lesser offenses.

### Audience

Congressional observers

### On-the-ground actions from transcript

- Contact your representatives to voice concerns about expelling members without convictions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the expulsion of Representative George Santos and the potential implications of setting new precedents for expelling members from Congress without a conviction. Viewing the full transcript will give a comprehensive understanding of the situation and its broader impacts in the political landscape.

### Tags

#Congress #Expulsion #GeorgeSantos #Precedent #PoliticalEthics


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
Representative George Santos
and how the saga of Representative Santos
has come to a close along with his time in Congress.
We are also going to talk about
some of the downstream effects from this
that I'm not sure a lot of people want to acknowledge.
Okay, so if you have missed the news, Santos was expelled from Congress today by a vote
of 311 to 314, so a relatively decisive vote.
This is only the sixth time in U.S. history this has occurred.
It shrinks the Republican majority from 221 down to 213.
So that's the basic coverage.
The Democratic Party feels that his former seat is going to be an easy pickup.
Now Santos didn't have many defenders, but there were a few people who drew issue when
it came to the precedent being set here.
Because generally speaking, to be expelled, well, you have to have been convicted.
Santos wasn't. The concern, whether or not that really was their concern or they
were just looking for a way to keep that seat remains to be seen, but the concern
is now that the precedent of having to have to be convicted is gone, there's a
higher likelihood that there are going to be more attempts to expel people
based on less serious behavior. Now in Santos's case it's important to
remember that there are a lot of members of Congress who were familiar with the
details, some of which might even be considered victims of his activities,
alleged activities, and he did break the the big rule when it comes to the
Republican Party. He messed with the money. When he engaged with donors the
way he did, that upset a whole lot of Republicans. So the precedent being
set aside here, not really surprising. At the same time, it has been set aside. So
you can foresee a scenario in which a a member of Congress says
something that is inappropriate and that leading to them being expelled now. It's
It's a concern that a number of people have had and shared.
I would like to think that the nature of the allegations against Santos is what brought
this about and the fact that there were other members in Congress who were aware of the
details.
We'll see how that plays out.
But this little portion of Congressional history is going to end up being a footnote, I think,
to a lot of other behavior that as of yet either hasn't really come to light or it's
come to light but there hasn't been any formal action.
So while right now this seems like it's a big deal, over the course of the next couple
of years, it may seem kind of small potatoes.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}