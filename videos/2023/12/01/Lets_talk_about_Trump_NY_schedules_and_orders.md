---
title: Let's talk about Trump, NY, schedules, and orders....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BOWJrEA_GgQ) |
| Published | 2023/12/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is facing a civil entanglement in New York, resulting in a gag order due to social media posts.
- The judge quickly found Trump in violation of the gag order, leading to a $15,000 fine.
- Despite Trump's appeal, the appeals court upheld the gag order, which will be rigorously enforced.
- Trump's legal team inadvertently confirmed the validity of the gag order, potentially leading to stricter enforcement.
- Transcripts of voicemails causing issues for the court were 275 single-spaced pages long.
- Trump is scheduled to testify on December 11th, with arguments set for early January and a decision expected by the end of January.
- The New York civil entanglement should be resolved by the end of January, with a possible appeal from Trump.

### Quotes

- "The judge very quickly said that it will now be enforced rigorously and vigorously."
- "This is probably one of those moments where Trump's legal team was like, oh yeah, we'll fight this."
- "The New York civil entanglement should be wrapped up by the end of January."
- "Now undoubtedly Trump will probably try to appeal the decision, whatever it is."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Trump's New York civil entanglement leads to a upheld gag order, transcripts causing court issues, and upcoming scheduling with a resolution expected by January end.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed about the developments in the New York civil entanglement (suggested)
- Monitor Trump's legal actions and potential appeals (implied)

### Whats missing in summary

Insights into the potential implications of the New York civil entanglement and how it could affect Trump's legal situation overall.

### Tags

#Trump #NewYork #legal #gagorder #civilentanglement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, New York,
and the scheduling, the order,
how all of it is shaping up and where it goes from here
because we have a bunch of new information
about the New York entanglement.
So for those who need a brief recap,
This is Trump's New York civil entanglement, and during this, because of things that were
posted on social media, Trump was hit with a gag order.
And the judge found him to be in violation of it relatively quickly.
Over that period, he was fined $15,000 or so.
Trump and team appealed the gag order and the appeals court put a hold on it waiting
for the final determination.
The final determination is out and the appeals court decided that the gag order was valid
and it is back on.
Now what this means is that the gag order the judge put into place has already been
through an appeal process.
The judge very quickly said that it will now be enforced rigorously and vigorously.
Judge doesn't have to be worried about being overruled on a penalty now because the order
was found to be valid.
This is probably one of those moments where Trump's legal team was like, oh yeah, we'll
fight this.
We'll fight this.
And what they wound up doing was confirming that it was valid, which makes the judge more
likely to apply it in a more stringent fashion.
Okay, so along the way, we did find a little bit about how much the posts were causing
a problem for the court.
The transcripts of voicemails that were single-spaced were 275 pages long.
Okay so that pretty much takes care of that.
Now let's talk about scheduling because we have found out a little bit more there.
It looks like on December 11th Trump will testify again for whatever reason.
arguments are set to occur in early January, and the judge has indicated
that believes the decision will be reached by the end of January. So the
New York civil entanglement should be wrapped up by the end of January. Now
undoubtedly Trump will probably try to appeal the decision, whatever it is. It
will probably work out a lot like the appeal with this gag order. Probably not
a good idea, but I feel like it's kind of a foregone conclusion that he's going to
try to appeal it. So that's where we're at. We are looking at the end of January
for this one to be resolved for the most part, aside from any potential appeal.
Alright, anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}