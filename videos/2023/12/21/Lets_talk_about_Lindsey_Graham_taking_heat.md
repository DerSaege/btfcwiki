---
title: Let's talk about Lindsey Graham taking heat....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZO-MeSItqKA) |
| Published | 2023/12/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party is experiencing another division between old school GOP and a newer faction.
- Lindsey Graham and other senators publicly expressed doubt on the evidence for the Biden impeachment inquiry.
- House Republicans heavily engaged on social media had promised their base an impeachment, but senators dismissing the evidence is undermining this narrative.
- The faction of the Republican Party upset with senators not convinced by the evidence is illustrated by a scenario involving the "space laser lady."
- Beau questions why those claiming to have evidence don't present it when given a platform.
- The MAGA faction's credibility is at stake as they have repeatedly promised evidence that never materialized.
- Lindsey Graham, despite his tenure, refuses to play along with this narrative, recognizing the potential backlash.
- The divide within the Republican Party is becoming more pronounced and will impact future elections.
- Beau points out the necessity of differentiating between the factions within the Republican Party.

### Quotes

- "That whole chain of events, that's strange, right?"
- "They've heard it since 2020, and that evidence never came."
- "It's worth remembering that Lindsey Graham's been up there a long time."
- "That division is something that is going to matter in the elections."
- "They have similar goals, but they're looking at it through very, very different eyes now."

### Oneliner

The Republican Party faces a growing division over the lack of evidence in the Biden impeachment inquiry, risking credibility and future elections.

### Audience

Political analysts, Republican voters

### On-the-ground actions from transcript

- Differentiate between the factions within the Republican Party (implied)

### Whats missing in summary

Insight into the potential long-term implications of this internal divide within the Republican Party.

### Tags

#RepublicanParty #LindseyGraham #MAGA #PoliticalDivision #FutureElections


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk about yet another divide in the Republican Party that is starting
to surface.
Yet another little disagreement between old school GOP and that newer faction of the Republican
Party.
This particular division centers on Lindsey Graham, of all people.
If you've missed it, Graham and some other senators have spoken publicly and basically
said, yeah, you know, we don't really think they have it.
That whole impeachment inquiry into Biden, yeah, we don't think they have the evidence
there.
Narratives kind of falling apart type of thing.
This is really bad for the House Republicans who are big on social media engagement because
they have promised their base an impeachment and the reality is to have
Republican senators up on Capitol Hill being like, yeah, this isn't going
anywhere. It's doomed from the start. That indicates that they don't actually
have the evidence that they said they had. If you can't convince other
Republicans on Capitol Hill, odds are you're not gonna be able to convince the
American people, right? So that faction of the Republican Party, very upset with
senators basically saying, yeah, we haven't seen the evidence. The space
laser lady, she was speaking, and how can Lindsey Graham in Washington DC on Meet
the Press say that he hasn't seen a smoking gun of evidence, that he doesn't
I think we've produced enough evidence to impeach Joe Biden."
And then she asked the crowd, you know, do you think we have enough evidence or something
like that?
And they all cheered.
And then she says, well, I think somebody else better run for Senator in South Carolina.
See, that's weird.
That whole chain of events, that's strange, right?
Because if you're in a situation where you can't even convince other Republicans that
you have the evidence. What would you want to do if you had it? If you had that
evidence and you had a platform like this at that moment and you were able to
speak to a whole bunch of people at once, you could address the idea that you
don't have any evidence by listing it, right? By being like, our evidence is
is this, and this, and this, but that's not what happened, what happened was we're
gonna primary you. Yeah, I mean, okay, I'm sure he's really gonna care about that in
2026. The problem that the MAGA faction of the Republican Party has is that most
Republicans, those that didn't really just fall off and completely sign over
their thought process, they've heard this before. Oh, we have the
evidence, you'll see it the Thursday after next. They've heard that.
They've heard it since 2020, and that evidence never came. I don't know that
they're gonna fall for it again. I mean, if they do, I will have
underestimated them. It seems unlikely that they will allow the same group of
people to trick them yet again and play them yet again because they've heard this same
thing when it came to evidence about the election that never showed up and they're asking for
them to fall for the same gimmick while it's being discussed in court that that evidence
never showed up, and it's being illustrated over and over again. Seems
like a bad strategy. It's worth remembering that Lindsey Graham's been up
there a long time. He's no McConnell, like when it comes to political strategy, but
he has made it up on Capitol Hill for quite some time, and he does not see the
value in playing along with this little stunt.
I would imagine it's because he's been up there long enough to know that generally speaking,
those stunts, when they fail, they benefit the person they were trying to impeach.
He doesn't want to suffer the backlash from it, because he has a primary he has to worry
about in 2026. So this is yet another example of that divide surfacing. It is
going to become more and more pronounced. When people talk about the Republican
Party, if you want to really have that analysis make sense, you're going to have
to differentiate between the two factions because they're not on the same
team. They have similar goals, but they're looking at it through very, very different eyes now.
That division is something that is going to matter in the elections over the next,
I mean maybe even out to 2026. Maybe. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}