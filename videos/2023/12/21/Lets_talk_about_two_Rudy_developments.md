---
title: Let's talk about two Rudy developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-K5_Om5TWs0) |
| Published | 2023/12/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Giuliani facing enforcement of a $146 million judgment, seeking to dissipate assets.
- Judge rules Giuliani must pay immediately due to failure to satisfy earlier awards.
- Giuliani's claim of not having money contradicted by judge referencing his sizable staff.
- Plaintiffs suing Giuliani again, seeking to permanently bar defamatory statements.
- Statements suggesting evidence to back up defamatory claims despite losing the case.
- Plaintiffs likely to succeed in both enforcement and lawsuit against Giuliani.
- Giuliani expected to have to pay up soon.

### Quotes

- "Giuliani facing enforcement of a $146 million judgment, seeking to dissipate assets."
- "Plaintiffs likely to succeed in both enforcement and lawsuit against Giuliani."

### Oneliner

Giuliani faces immediate payment due to prior failures, plaintiffs likely to succeed in legal actions.

### Audience

Legal observers, political analysts.

### On-the-ground actions from transcript

- Support organizations fighting defamation (suggested).
- Stay informed on legal proceedings (suggested).

### Whats missing in summary

Details on specific defamatory statements and evidence.

### Tags

#Giuliani #LegalProceedings #Defamation #Enforcement #Plaintiffs


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Rudy Giuliani
and Georgia again,
and then how we may be talking about it again, again.
Okay, so the first major development occurred yesterday
when the plaintiffs sought to get enforcement
of the judgment against Giuliani
that $146 million judgment.
I want to say it was $148, and then the judge reduced it
by two because of another case.
But a sizable sum, they wanted to be
able to seek enforcement of that and start
getting paid immediately.
And Giuliani, of course, was like,
I'm supposed to have 30 days.
The judge ruled that Giuliani's failure to satisfy even the more modest monetary awards
entered earlier in this case provides good cause to believe that he will seek to dissipate
or conceal his assets during the 30-day period.
So the judge said, yeah, they can seek enforcement immediately.
We'll see how that plays out.
of Giuliani's arguments was, you know, I just don't have the money. And the judge,
I'm gonna paraphrase this, but was basically like, don't tell me you're
broke, I've met your spokesperson. Kind of indicating that you can't really
square the idea of I have no money while having a sizable staff. So you have
that going on, that development, and it seems as though it is going to move
forward pretty quickly with the enforcement, meaning the trying to
actually get the cash and put it into the hands of the plaintiffs. In
Incredibly related news, these same two plaintiffs are suing Giuliani again.
This time they are asking a judge to permanently bar Giuliani from making defamatory statements about them.
And some of the statements that he is alleged to have said seem to suggest that no, even
though he lost the case, that those statements, they're really true and that he has evidence
to back it up.
You can see it Thursday after next or something like that, again, paraphrasing.
I feel like that's probably going to move forward pretty quickly as well.
It's not an unreasonable request, especially given the outcome of the first case.
So everything right now seems to be leaning very heavily in favor of the plaintiffs.
And I realistically don't see that changing anytime soon.
It does look like Giuliani's going to have to pay up on this one.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}