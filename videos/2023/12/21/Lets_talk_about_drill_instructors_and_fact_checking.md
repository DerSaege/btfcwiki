---
title: Let's talk about drill instructors and fact checking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=56Y8ZAQNRHU) |
| Published | 2023/12/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the reasons behind drill instructors yelling, including to establish authority, create urgency, and provide stress inoculation.
- Reveals a lesser-known reason for yelling, which is to turn a teachable moment for one person into a lesson for everyone.
- Shares the importance of repeating information in videos to combat misinformation and reach a wider audience.
- Emphasizes the effectiveness of multiple exposures to debunk false beliefs and encourage critical thinking.
- Advocates for using teaching techniques to help people learn how to fact-check and be less susceptible to misinformation.

### Quotes

- "If you suffer enough shark attacks, which is where a whole bunch of instructors come up and yell at you all at once and normally yell conflicting things, and anyway, you can become very unbothered by a lot of things that might happen later if you're in the military."
- "It turns a teachable moment for one person into a teachable moment for everybody."
- "Battling that kind of bad information is something that I think is really important."
- "It's a teaching technique, and it's one that works."
- "Y'all have a good day."

### Oneliner

Beau explains the reasons behind drill instructors yelling and the importance of repeating information to combat misinformation effectively.

### Audience

Educators, Critical Thinkers

### On-the-ground actions from transcript

- Share educational videos multiple times to combat misinformation effectively (suggested).
- Encourage critical thinking through repeated exposure to debunk false beliefs (suggested).
- Use teaching techniques to help others learn how to fact-check and be less susceptible to misinformation (suggested).

### Whats missing in summary

The detailed examples and explanations provided by Beau in the full transcript are missing from this summary.

### Tags

#DrillInstructors #Yelling #CombatMisinformation #TeachingTechniques #CriticalThinking


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to do things a little bit
differently.
We're going to do things backwards.
I'm going to read the question at the end.
But we are going to talk about drill instructors, drill
sergeants, and their most iconic behavior, why they do
what they do.
We're going to talk about why instructors yell, why
instructors yell.
There are three very well-known reasons for this behavior.
The first is to establish authority.
When this practice first started, the recruits,
they were young and aggressive.
The establishment of that authority,
it played into things later when it
came to the chain of command and stuff like that.
But it also helped keep order in the beginning
phases of training. The second reason is urgency. It's urgency. When people are
yelling, people move faster. Most training programs are, they're full. They
don't have a lot of time and they have a whole lot of stuff that they're trying
to teach during that period. So they want people to move quickly. The third
is stress inoculation. Meaning, if you get yelled at enough, something that would
normally stress people out, well, you become inoculated to it. If you suffer
enough shark attacks, which is where a whole bunch of instructors come up and
yell at you all at once and normally yell conflicting things, and anyway, you
can become very unbothered by a lot of things that might happen later if you're
in the military. And then there's the fourth reason, and it is lesser-known, but
it is probably the most important. When a recruit is getting yelled at, when that
new soldier is getting yelled at, what are all the other soldiers doing looking
straight ahead, right? They can't look over and see what he did. They can't look
over and find out what got that person in trouble, why they're getting yelled at.
If you listen to good instructors, they will say exactly what the person did
wrong and they'll say it loud enough for everybody to hear. Think of the best
known example when it comes to the stereotype of drill sergeant behavior,
right? Why is your footlocker unlocked? From the very first sentence, private pile
and everybody else in there knew what he did wrong. It turns a teachable
moment for one person into a teachable moment for everybody. And that's why it's
done. It's a teaching technique. You don't have to employ this teaching
technique with that much hostility, but it works. So here's the question. Why do
make fun of people in videos explaining how to fact check. You have vids on this
and you could just send them a link in response to their email. Your
audience is mostly really smart. Most of them don't need these videos to begin
with and I don't see why you remind people of the same stuff periodically.
Because if I send them an email, not everybody else hears it. If you look at
those videos where I do that, the topic that is, that needs to be debunked is in
the title. So when people are searching for that, looking to confirm their bias,
there's a high probability they will run across that video. Those people who are
searching out something to make them believe that Zelensky bought a mansion
in Florida, they are people who do not know how to do that.
Or they don't apply it if they do know how.
So putting it out there like that, it provides more people with the possibility of stumbling
across that information.
Yeah, I actually have long videos explaining how to fact check and go through stuff.
But the way YouTube works, those older videos, unless somebody sends it to you, you're probably
not going to see it.
The other thing is, if you watch every video on this channel, yeah, you're going to hear
about how to use 10i a dozen times.
Maybe not a dozen, but probably half a dozen by now.
But not everybody watches every video.
Battling that kind of bad information is something that I think is really important.
So yeah, I use a technique that puts that information out there multiple times so a
whole bunch of people can see it.
And it's a technique that maybe should be used in other ways as well.
from somebody else's mistake, it helps get around that block that people
have when it comes to admitting they've been fooled. Because if you send a
message in and you get a response of, hey you're wrong and this is why. Okay. But if
you're looking for that information and you run across somebody else who
believed the same thing you want to believe and it gets debunked and you
learn how to check it yourself in the process, I would like to believe that
they would be less susceptible to it in the future. It's a teaching
technique, and it's one that works.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}