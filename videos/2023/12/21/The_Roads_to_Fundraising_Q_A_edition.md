---
title: The Roads to Fundraising Q&A edition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CGuTpd44F78) |
| Published | 2023/12/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Annual fundraiser for domestic violence shelters providing gifts for teens
- Tablets, cases, earbuds, Google Play, Hulu cards given to teens in shelters
- Excess funds used for other shelter needs like holiday meals
- Tablets for teens provide a sense of control and ownership in a chaotic situation
- Importance of communication to understand shelter needs for effective donations
- Need to call shelters to determine current needs as they vary each year
- Example of past needs like cleaning supplies and bras at shelters
- Main channel fundraiser may set up an endowment for long-term impact
- Ways for individuals with limited resources to contribute to activism locally
- Importance of acknowledging wins and taking breaks when overwhelmed
- Tips on decentralizing activist groups and avoiding centralization pitfalls
- Beau's interest in Jeep Wranglers and desire to try the environmentally friendly 4XE

### Quotes

"Tablets for teens provide a sense of control and ownership in a chaotic situation."
"Call shelters to determine current needs as they vary each year."
"Importance of acknowledging wins and taking breaks when overwhelmed."

### Oneliner

Beau shares insights on fundraising for domestic violence shelters, the importance of communication with shelters, and tips for effective activism locally.

### Audience

Community members

### On-the-ground actions from transcript

- Contact local shelters to inquire about their current needs (suggested)
- Volunteer to run errands or help with maintenance at organizations in need (suggested)
- Support causes by offering specific skills or assistance as per their requirements (exemplified)

### Whats missing in summary

The full transcript provides detailed guidance on fundraising for domestic violence shelters, effective communication with shelters, and practical tips for local activism.

### Tags

#Fundraising #DomesticViolence #CommunityNetworking #Activism #ShelterSupport #LocalChange #Decentralization #JeepWrangler #Environmentalism


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
So today, we're gonna be talking about
all the stuff behind me.
So we will be talking about the livestream,
how all of that is progressing,
and all of the developments with it.
And then we also have some Q&A stuff
that will go along with this.
And from what I understand,
I guess we're doing the Q&A
because there were a bunch of, quote,
messages that had questions about community networking
or loosely activism.
So we'll be going through those.
OK, if you have no idea what any of this is about,
over on the main channel, every year
we do a fundraiser for domestic violence shelters.
We focus on getting gifts for teens
who are in the shelters over the holidays.
And we get them tablets, a case for the tablets, earbuds,
and then like a Google Play card and a Hulu card.
That's what's in the bags behind me.
And we've been doing this for five years or so.
And the excess funds, we get other stuff
that the shelters need.
This year, it went really well for a number of reasons.
We did well with the fundraiser.
The cost of the tablets has actually dropped.
And there weren't as many teens in the shelters,
which is always good.
That's always good.
So we had more excess funds.
So, this year we're helping provide holiday meals as well, which is, that's new.
We've never been asked for that before.
And then the other shelter that we help out, they asked for kid tablets.
They have like a rubbery case, so they're bouncy, I guess they're really hard to break.
And those will be used while the parent is filling out paperwork, putting together their
resume, doing the workshop stuff, all of that, so there's something there.
The reason we focus on tablets for the teens is because when they enter a shelter like
this, they don't normally have all of their possessions, their entire world has been turned
upside down, and they don't have a lot of control.
The tablets give them something that's theirs, and even if it's only an 8 inch screen, they
have control.
It gives them a window into something else, and it has been well received, so we're sticking
with that.
The reason we normally don't do stuff for the younger kids is because there are other
organizations that focus on that.
Sometimes the teens kind of get left out.
Okay, so we have all the stuff that you see behind us and there's actually more food
and stuff off screen.
hands and stuff like that that just didn't seem like it'd be good I didn't
put back there. And then on top of all of that we we will be giving a check to
each shelter for three or four thousand dollars each. So very successful year.
Everything went well. Now all we have to do is deliver it. Okay so moving on to
the Q&A. If you're gonna do one of those videos where you show all the stuff you
bought for the shelter this year, can you be more clear on why you show it? I work
in a shelter and think it's to show the variety of need, but you only actually
say that and stress the communication aspect in one of the videos you've done.
Fair enough. Yes, that's why we show it. There are five of these videos now. If
you look the excess stuff, it's never the same. Yeah, we've never done holiday
mills before. We've never done car seats or high chairs. I don't know if
you can see those. The needs change every year and that's true in every
shelter, there are some things they always need, like detergent.
And with a DV shelter, it's normally scentless detergent.
They normally need socks, stuff like that.
There are some things that are pretty much always in need, but
they may not be in dire need.
And so if you have funds, when you're looking and trying to figure out what to provide for
the shelter, don't guess, call, which is what we do.
We go through and we get what we focus on, the tablet bags, the bags you see back there.
We put those together.
The excess, we call them and ask them what they need and then we get it.
We don't try to guess because it is never the same.
One year, every shelter that we were doing something with, they all needed cleaning supplies.
It was just nothing but cleaning supplies.
Another year, they had us out buying bras because that's not something that gets donated.
It changes, and most times shelters don't have a lot of storage space, so you want to
focus on their immediate needs.
If you have that kind of funding, or if you're trying to get it and you're putting stuff
together, make the call and find out exactly what they need.
We've been doing this for years now.
We would not have guessed that they needed car seats, high chairs, I don't know if
there's any back there, but they also needed candy for stockings.
We wouldn't have guessed that.
There's no way we would have guessed that.
We wouldn't have guessed about the tablets for the younger kids.
So yeah, the communication part is very important.
To my way of thinking, it's smarter to find out how much excess funds you have and then
make the call because it gives you a clearer picture.
You recently did a video on the main channel talking about medical debt and how St. Paul
bought a bunch of medical debt and then forgave it.
Would you be open to the idea of having a live stream where the internet people can
raise funds to buy up medical debt and then forgive it.
I'm open to it, but I honestly don't know anything about it.
I really like what they did.
I thought that was an amazing use of funds and the way it got leveraged, but I wouldn't
even know where to go buy medical debt.
I have no idea how that works.
We can have somebody look into it maybe and see what we can do.
As far as the live streams go, we are looking into something else because our live streams
are now hitting the amount necessary to set up an endowment, which if you don't know
how they work, the short version is the organization that's being helped, they get a smaller amount
of money up front, but they get money every year, like forever.
And it definitely, it creates a longer impact, I mean they would be getting funding after
I'm gone, which I mean that seems really cool.
So that's something we're looking into.
But yeah, we can see what we'll find out about medical debt.
I want to convert my beliefs into action, as you say, but feel like none of the usual
forms of activism are accessible to me or fit my strengths.
As someone who lives in a rural part of New England and has not that much time outside
of work, hardly any money, and minimal social skill.
How can I affect change locally?
Are there any roles that are often unfulfilled that might be well suited for someone like
me?"
It depends on what you mean by minimal social skill.
Can you phone bank?
There's that.
It depends on what your skills are.
If you're like a white collar worker, most places that are like, quote, legit non-profits
like 5013C's, they generally need help there.
If you're a blue collar worker, every place that has a building needs maintenance and
needs stuff done, and that's stuff that doesn't require social skills, and it's generally
speaking stuff like that is stuff you can do when you have time.
Every skill is needed by a cause that you support in your area.
It's not always easy to find it, to find your place, but once you get there, it's fulfilling.
And sometimes it's as simple as they need somebody to run errands.
One of the reasons that we don't just hand them all of the excess in a check is because
then they would have to run around and try to go get all the stuff.
And we can do that for them and just relieve one more burden that they have.
Generally speaking, any organization that's doing good,
they're normally not well-funded, and they don't have a lot of time.
There's always something.
If the situation you're describing here,
I would look for the calls you support and then contact them and ask what they need and
just ask them to go through a list and be like, hey, I am willing to help out.
Let me know what you have.
The good thing is, generally speaking, organizations that are trying to better the world, they're
pretty understanding of different people's abilities and what they are
comfortable doing. And they're normally pretty willing to work around it. Okay.
Can you give a shout out to the town of Merrillville, Missouri? Today the new
The New Nottoway Humane Society, the local no-kill shelter, caught fire.
Because of the nature of the fire, animals had to be evacuated and could not return until
sometime after Christmas when the building can be approved safe again.
The call went out on Facebook, 40 animals needed foster homes for the holidays.
Within hours, all the animals were placed in foster care.
cool. That is definitely a community coming together. And it's also going to be cool because
I have a feeling that there's going to be a lot of fosterfells, you know, like the foster horses.
So, I mean, the animal's going to be hanging out with the family over the holidays.
I don't feel like they're all going to be coming back in January, which is that's cool, too.
Okay. Hey, Bo. I'm a non-binary 22-year-old and I have a trans friend who's very much the archivist
type. Considering you and her have a very similar archivist drive, if you have any general advice
for her in terms of how to best use that drive and manage the mental health
struggles that come with it. I'm sure she'd appreciate it. Note, and this is
from the person who put the questions together, says note there's another
question in this message and since you would answer with I don't know and then
ask me tomorrow just tell her to check out good good good and it's all one word
and I have no idea what that means so okay then all right as far as being
somebody who is methodical and seeks a bunch of information and intends to kind
to file it all away the key part is acknowledging the wins you have to
acknowledge the wins and you you have to you have to keep track of something else
if you are as an example you know a lot of the content on the main channel is
about foreign policy. Right now, I am super unhappy with how things are going, and that's
why you are getting videos about a win with the Catholic Church progressing. You're getting
more videos answering questions about information consumption, because those are things that
are they still like keep me in the game so to speak I'm still archiving the
information and all of that but it's it's a palate cleanser and it's I mean
with what I do it's good for y'all as well but it's also a necessity if you're
covering something and it's just all bad news. And if she is... I mean I'm having
to fill in some blanks here because I feel like part of this message got
deleted. But if she is the person who's like keeping track of all of the
developments for your community and letting all of your friends know, which
Which is the role that somebody with this personality type is going to kind of naturally
fall into.
There's a, yeah, she's going to need the same thing.
There's going to need to be a palate cleanser.
Something that can not just be a win when it comes to the topic that she's focusing
on but also a break from that topic when there aren't wins to be had.
So that would be my advice and I mean it's funny me saying this again but remember you
can always take a break.
You can always take a break.
Whatever it is that you are focused on and you are studying, you are archiving, you are
gathering the information so it can be provided and people can act on it.
Whatever that issue is, it will be there when you get back from your break.
Okay.
I'm on the organizing committee for an activist group that is on paper, decentralized.
As the name organizing committee suggests, no it isn't.
In practice, I could draw concentric circles with one person at the center lashing out aggressively at any disagreement.
For reasons both ideological and practical, I highly value decentralization.
How do I work towards making that a reality?
Okay, generally speaking, the person who ends up at the center is somebody who's good at planning and logistics.
They're not always great at implementing, though.
implementing though.
It is wise, in my opinion, to have people who take the lead
on various projects.
And again, when you're talking about decentralization, you
don't necessarily want the lead on that project to always be
the lead on similar projects.
But having somebody who is kind of the person that
is expected to be making those moves, it's important.
And it helps alleviate a lot of the personality conflicts.
OK, this says, a fun one to end on.
I have been watching your stuff for over a year now.
And I know you're a Jeep person like me.
I have a Wrangler 4XE and I love it
because I only get gas four times a year.
I think you would love it too
since you love the environment.
My question is, what kind of Wrangler do you have now
and do you plan on getting a 4XE in the future?
Yeah, I have an ancient TJ.
And yes, I would love a 4XE,
which for those who don't know,
It's like a hybrid Jeep.
I just want to see Jeep people jeep it up for a little bit
and see how well they hold up.
OK, so that looks like it, and I hope
that answered your questions.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}