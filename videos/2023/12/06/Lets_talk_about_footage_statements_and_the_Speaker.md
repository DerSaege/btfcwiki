---
title: Let's talk about footage, statements, and the Speaker....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bvE_vNd6OVg) |
| Published | 2023/12/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican party aimed to release all footage from January 6th to downplay the events as a mere tourist visit, but faced backlash for blurring faces to prevent retaliation.
- The Speaker of the House expressed concern about people being retaliated against if their faces were not blurred in the footage from January 6th.
- The Department of Justice (DOJ) already has access to the raw footage from January 6th and has released sections to help identify individuals.
- Blurring faces in public viewing room footage aims to prevent retaliation against private citizens from non-governmental actors.
- The promise to release the footage was part of an effort to amplify a conspiracy theory, suggesting that the events of January 6th were fabricated.
- Some individuals present at January 6th later ran for office under which party? The theory that the footage backs up will be contradicted by it.
- Blurring faces is intended to prevent the public from identifying individuals in the footage, not to cover up certain groups' involvement.
- Blurring faces may lead conspiracy theorists to claim that those supporting face blurring are complicit in hiding identities.
- Acknowledging the truth about who participated in the events of January 6th could mitigate confusion and manipulation among the Republican Party's base.

### Quotes

- "It's probably a really good idea for the Republican Party to just acknowledge that yes, the people carrying the Trump banners were in fact Trump's people."
- "The promise to release the footage was an effort to amp up a conspiracy theory."
- "Blurring out the faces will help stop the public from identifying people."
- "Acknowledging the truth about who participated in the events of January 6th could mitigate confusion and manipulation among the Republican Party's base."
- "Let's be clear, there have been a number of people who were at January 6th who then ran for office. What party did they run under?"

### Oneliner

The Republican Party's attempt to control the narrative of January 6th through blurred faces exposes deeper issues of manipulation and confusion among its base.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Hold accountable political figures who perpetuate misinformation (implied)

### Whats missing in summary

Insight into the potential consequences of perpetuating misinformation and manipulating narratives.

### Tags

#RepublicanParty #January6th #ConspiracyTheory #Misinformation #Manipulation #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a statement, two statements actually, by the United States
Speaker of the House.
And I just want to say before we even get started, this isn't a joke.
This chain of events actually occurred and we're just going to go through the two statements
and y'all can think about it however you would like. Okay, so if you've missed the news the
Republican party for quite some time has been saying we're going to release all the footage
from January 6th to show that it's, you know, it's all just not true. It's being over-hyped.
It was a tourist visit and all of this stuff, right? This is the Speaker of the House.
We have to blur some of the faces of persons who participated in the events of that day
because we don't want them to be retaliated against or to be charged by the Department of Justice.
He said DOJ.
And to have other, you know, concerns and problems.
I mean, if that is a concern, I'm going to suggest just off top
that maybe it wasn't just a tourist visit, maybe there was more to it, just saying.
As soon as that statement went out, there was a lot of predictable backlash from people
saying, you're hiding evidence from DOJ, I mean that seems bad.
So a statement then came out that said faces are to be blurred from public viewing room
footage to prevent all forms of retaliation against private citizens from any non-governmental
actors.
The Department of Justice already has access to raw footage from January 6th.
That's true.
We saw a lot of it, right?
DOJ released sections of it, to put it out there, to help identify people because they
couldn't do it. I feel like part of the problem the Republican Party is facing
here is that the whole promise to release the footage was an effort to
amp up a conspiracy theory. The whole thing was about playing into this idea
that what those of us who watched it live saw didn't actually happen and that
there was more to it than that and that it was all a setup or whatever. They fed
into this because it would tell people well hey it wasn't really our people who
did it it was other people you know it wasn't supporters of the Republican
Party, it wasn't Trump's people, it was these other people. It was never actually
supposed to be released. It was like all of the other stuff that the Republican
Party promises its base, but those who really believe in the conspiracy
theories have put so much pressure on them that they felt like they had to.
Let's be clear, there have been a number of people who were at January 6th who
then ran for office. What party did they run under? The theory that this footage
is being released to quote back up is going to be undermined by the footage.
Blurring out the faces will help stop the public from identifying people.
That's really what it's about. And the funny part about this is that it is
going to lead to a follow-on theory because those people who believe the
conspiracy theory are then going to come out and say, you know what, those people,
those people who were okay with blurring the faces, they're in on it. They're
They're trying to hide and conceal the identities of those people who weren't our people.
That's where it's going to go, mark my words.
It's probably a really good idea for the Republican Party to just acknowledge that yes, the people
carrying the Trump banners were in fact Trump's people.
The people who were there and then ran for public office as Republicans are in fact Republicans.
It would cut down on a whole lot of issues if they would just do that.
But it is better to continue to feed misconceptions held among their base, because that confusion
makes them easier to manipulate.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}