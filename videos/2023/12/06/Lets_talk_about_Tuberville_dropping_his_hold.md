---
title: Let's talk about Tuberville dropping his hold....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vq-VshH1Ah4) |
| Published | 2023/12/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Tuberville released the hold on promotions for military officers, except for four-star generals.
- Tuberville perceived four-star generals to be of higher value, hence retaining leverage on them.
- There's a disconnect between Tuberville's actions and his understanding of the military.
- Pressure will mount on Congress to override Tuberville regarding the promotions of four-star generals.
- Acting positions are currently filled due to the delay impacting officers' careers.
- Other members of Congress may advocate for national security to override Tuberville's hold on promotions.
- Tuberville might be trying to maintain his hold for longer to appear favorable before releasing it.
- Beau doubts that most people will fall for Tuberville's tactics.

### Quotes

- "He perceives them to be of higher value and therefore maintains leverage."
- "There's a disconnect between Tuberville's actions and his understanding of the military."
- "It's certainly annoying for the people who are being directly impacted and how it impacts their career."
- "He might have worked out some kind of a deal so he can maintain his hold for an extra month or whatever."
- "I do not think that the majority of people are going to fall for this stunt though."

### Oneliner

Senator Tuberville's release of military promotions, except for four-star generals, reveals a disconnect between his actions and military understanding, sparking pressure for Congress to intervene.

### Audience

Military members, Congress

### On-the-ground actions from transcript

- Contact your representatives to advocate for the timely promotions of military officers (implied)
- Monitor the situation closely and be ready to support actions that prioritize national security and military careers (implied)

### Whats missing in summary

Insights into potential long-term impacts and the broader implications of Senator Tuberville's actions. 

### Tags

#SenatorTuberville #MilitaryPromotions #Congress #NationalSecurity #MilitaryCareers


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Senator Tuberville
and his decision and what it means
and how he probably misreading the situation.
And we will talk about that as we answer a question
that came in about it.
So if you miss the news and don't know what happened,
Senator Tuberville has decided in his graciousness to release the hold on promotions for hundreds
of military officers that he has basically just delayed their careers to get some headlines.
He is releasing the hold on all officers except for four stars, for four-star generals.
So that has occurred as we knew it would.
We've talked about it before.
The Senator actually said that, you know, we didn't get the win we wanted, but we think
that we opened some eyes or something to that effect.
I mean, that's true, just not the way he thinks.
I would imagine that there's a whole bunch of people who are now following organizations
like vote vets, who understand that a whole lot of politicians have a rhetoric about supporting
the individual soldier that they don't actually, you know, believe. I would imagine that some eyes
were opened in that regard. I would be willing to bet that if you were to ask the average American
if they knew what the hold was over, most would not be able to answer. I don't think that he got
the win he thinks he did. The question that came in was why would he retain the
hold on the four stars? The answer to that is simple. The same reason that there
weren't any Americans in the first batch during the exchanges. He perceives them
to be of higher value and therefore maintains leverage. That's that's why.
It's the same dynamic. The thing is Senator Tuberville doesn't actually
understand much about the military. These are higher ranking. They are also
incredibly high-profile positions which means there's going to be enormous
pressure on members of Congress to override him. I would imagine we will see
that pretty quickly. The other thing that I don't really think that he
understands is, and if you were in the military, feel free to answer the
question down below, if you are given an order by a three-star, do you move
slower than if it had been given to you by a four-star? I mean at that point does
it really matter? There will still be positions that are filled by acting
right now and it is certainly annoying for the people who are being directly
impacted and how it impacts their career. But it's the amount of pressure
and leverage that he has was greatly reduced. And I do believe, especially
with some of the positions that these officers were moving into, I would
imagine that it's going to be much easier for other members of Congress to
scream about national security and get his hold overridden and just do them one
at a time. But we'll have to wait and see how it plays out. He might have worked
out some kind of a deal so he can maintain his hold for an extra month or whatever and
then release it on his terms so he seems like the good guy.
I do not feel like the majority of people are going to fall for this stunt though.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}