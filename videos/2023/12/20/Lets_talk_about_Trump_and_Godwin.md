---
title: Let's talk about Trump and Godwin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=u1S0REyJing) |
| Published | 2023/12/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Biden administration compared Trump's rhetoric to Hitler, sparking debate.
- Reference to Godwin's Law: likelihood of Nazi comparisons in online debates.
- Godwin, creator of the law, was reached out to for his opinion.
- Godwin believes Trump intentionally uses inflammatory rhetoric with similarities to Hitler.
- Godwin implies Trump's rhetoric is not accidental, citing specific remarks.
- Emphasizes the importance of not equating Biden and Trump as the same.
- Trump may continue using such rhetoric to garner attention.
- Stresses the non-coincidental nature of Trump's rhetoric.

### Quotes
- "I think it [comparing Trump's rhetoric to Hitler] would be fair to say that Trump knows what he's doing."
- "One of them is your basic status quo centrist American president, which is exactly what everybody said he was going to be. The other is a far-right extreme authoritarian."
- "That rhetoric, it's not an accident. It is not an accident."

### Oneliner
Biden administration compares Trump's rhetoric to Hitler, with Godwin affirming intentional parallels, stressing the importance of not equating them.

### Audience
Online Political Observers

### On-the-ground actions from transcript
- Read the full interview with Godwin for deeper insight (suggested)
- Stay informed about political rhetoric and its implications (implied)

### Whats missing in summary
Importance of remaining vigilant against dangerous rhetoric to safeguard against authoritarian tendencies.

### Tags
#Trump #Biden #Rhetoric #GodwinsLaw #Authoritarianism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about a comparison
that Biden made.
And then an interesting interview
that somebody decided maybe it was time to have
and basically run through some different views
on some of Trump's rhetoric.
If you've missed it, it's actually not something we've been talking about a lot on the channel,
Trump has been using more and more inflammatory rhetoric. I mean, more than even he was using
before. And the Biden administration made a comparison, and they said that he parodied
Hitler. This immediately brought up the idea of Godwin's Law. If you're not
familiar with Godwin's Law, it is an old internet adage that says,
as an online discussion grows longer, the probability of a comparison involving
Nazis or Hitler approaches. Basically, the longer debate goes on, the more likely
somebody is to say, oh well the other side, this is who they are. It's called
Godwin's Law because a guy named Godwin like came up with it. Politico reached
out to Godwin, the guy who actually came up with this, because when Biden said
that there was a lot of pushback, but not as much as you might imagine. It was
political math on their part and I think it paid off, but here's an interesting
piece of this interview and I'll have it down below because it's actually much
longer than we'll be able to go over. So to be clear, do you think comparing
Trump's rhetoric to Hitler or Nazi ideology is fair. Godwin said, I would go
further than that. I think it would be fair to say that Trump knows what he's
doing. I think he chose that rhetoric on purpose. But yeah, there are some real
similarities. He goes on later to say, you could say the vermin remark or the
poisoning the blood remark, maybe one of them
would be a coincidence.
But both of them pretty much makes it clear
that there's something thematic going on,
and I can't believe it's accidental.
So the person who formulated this adage that
is at its heart saying, hey, don't make this comparison
unless it's real.
saying it's real. Right now you have a lot of people who are falling prey to
the idea of, well they're both the same, they're not. They aren't. And it is
important to remember that. One of them is your basic status quo centrist
American president, which is exactly what everybody said he was going to be. The
other is a far-right extreme authoritarian. They aren't the same and
it is very important to remember that. There's obviously going to be more and
more conversation about this as it goes on, especially as Trump realizes he can
get headlines from paraphrasing this guy. He'll probably do it more often. It's
important to remember that that rhetoric, it's not an accident. It is not an
accident.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}