---
title: Let's talk about Trump being off the ballot in Colorado....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BWUJyR83TsE) |
| Published | 2023/12/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Colorado Supreme Court ruled that President Trump is disqualified from holding office under the 14th Amendment of the US Constitution.
- The ruling states listing Trump as a candidate on the presidential primary ballot in Colorado is a wrongful act.
- A Colorado judge previously ruled that Trump engaged in insurrection.
- The current ruling will be held on appeal until January 4th.
- If the ruling stands, Trump will not be on the ballot for the presidential election.
- Similar proceedings are happening in other states, with some rulings being struck down.
- The argument is that the ban on holding office due to insurrection is automatic and doesn't require a guilty finding.
- The US Supreme Court's decision on Colorado's ruling will have a wide impact.
- Beau expresses doubt that the Supreme Court will side with Colorado's ruling.
- The situation has been unexpected and surprising.

### Quotes

- "Colorado Supreme Court ruled Trump is disqualified from office under the US Constitution."
- "US Supreme Court's decision will impact many places."
- "The situation has been unexpected and surprising."

### Oneliner

Colorado Supreme Court disqualifies Trump from office under the US Constitution, setting the stage for a potential US Supreme Court decision with wide-reaching implications.

### Audience

Political analysts, legal experts

### On-the-ground actions from transcript

- Monitor updates on the appeal deadline for the ruling (suggested)
- Stay informed about similar proceedings in other states (suggested)
- Await the US Supreme Court's decision and its implications (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the legal implications of the Colorado Supreme Court ruling on Trump's eligibility to hold office under the US Constitution.


## Transcript
Well, howdy there, internet people, let's bow again.
So today, we are going to talk about Colorado,
the Colorado Supreme Court, Trump, the US Constitution,
and the United States Supreme Court,
because this is certainly headed that direction.
The Supreme Court of Colorado put out quite the ruling.
A majority of the court holds that President Trump is disqualified from holding the Office
of President under Section 3 of the 14th Amendment of the United States Constitution.
It goes on.
Because he is disqualified, it would be a wrongful act under the election code for the
Colorado Secretary of State to list him as a candidate on the presidential primary ballot.
Okay, so the short version here is that the Colorado Supreme Court has determined
that the former president of the United States, Donald Trump, is ineligible to run again,
can't hold office because of the insurrection ban that is in the U.S.
Constitution. It's worth remembering that back in November, I think, a Colorado
judge ruled that Trump had engaged in insurrection. So it was a 4-3
decision. This is certainly going to the US Supreme Court. The current ruling is
held on appeal until January 4th. If this stands, Trump will not be on the
ballot. There are other states where similar
proceedings are occurring. Some have been struck down and they've said no. We
knew eventually that one state was going to agree and then it goes to the Supreme
Court to decide. The general argument here is that the ban, it is automatic. It
doesn't require any finding because it's in the Constitution the way it is. It
doesn't require that he be found guilty of insurrection or something like that.
That it's just an automatic thing that occurs and that's the end of it. If you
engage in this you you you can't you can't run for office again. So we'll have
to wait and see how it plays out. The decision that will ultimately come from
the United States Supreme Court will impact a whole bunch of different
places because if they decide that Colorado's ruling stands you will see
the same ruling in other places and yeah I'm gonna be honest I have a lot of
doubt that the Supreme Court is going to side with the ruling in Colorado I had
a lot of doubt that it would ever get this far though this this is this is a
a surprise. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}