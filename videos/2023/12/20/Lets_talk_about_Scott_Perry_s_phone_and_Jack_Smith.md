---
title: Let's talk about Scott Perry's phone and Jack Smith....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RYjcqKxvD4k) |
| Published | 2023/12/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Scott Perry's phone was seized by the federal government in connection with the January 6th probe in 2022.
- Perry claimed protection under the speech and debate clause in the Constitution to prevent access to his text messages and emails.
- A legal battle ensued, with a judge ruling against Perry's claim of protection.
- An appeals court has ordered Perry to disclose 1,659 out of 2,055 documents, withholding access to 396.
- The court found that most of the information was not protected by speech debate, but some were.
- The documents are believed to contain information on the vice president's role during certain events in Congress and election claims.
- Perry's attorney mentioned his undecided stance on appealing the decision.
- Investigators are now poised to access a significant amount of text messages and emails for the investigation.
- Despite some documents being protected, the majority will be available for scrutiny.
- The outcome may provide valuable context to the investigation.

### Quotes
- "Representative Scott Perry's phone was seized by the federal government in connection with the January 6th probe."
- "The court found that most of the information was not protected by speech debate, but some were."

### Oneliner
Representative Scott Perry's phone seized in January 6 probe, court orders disclosure of majority of documents, potentially revealing significant information.

### Audience
Legal observers, political analysts

### On-the-ground actions from transcript
- Stay informed on the updates regarding the investigation (implied).

### Whats missing in summary
Legal nuances and detailed courtroom proceedings

### Tags
#ScottPerry #LegalBattle #SpeechAndDebate #Investigation #Transparency


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
Representative Scott Perry's phone.
We're going to run over the timeline of this
because it goes back a ways, talk about the events,
and then talk about the decision
and where it goes from here.
Okay, so if you have missed this,
Scott Perry, Representative Scott Perry.
Back in 2022, I think in August,
the feds took his phone in connection
with the January 6th probe.
The phone was seized, but the federal government
needed a second warrant to access data on the phone,
meaning text messages and emails.
Perry asserted speech and debate.
clause in the Constitution that is there to make sure that people in Congress can speak
freely and not be punished for it.
Okay, so because of that, there's been a legal battle.
Judge ruled that they weren't really protected.
Perry appealed.
The appeals court has made their decision.
Having now analyzed each of the 2,055 documents still at issue, the court will order Perry
to disclose 1,659 of them, but not the 396 others."
So what the court found was that overall, the information was not protected by speech
debate. By overall I mean the majority, but some of them were. That's a lot
of documents that will be shortly in the hands of Smith and that team. The
documents are alleged to have information about the role of the vice
president during certain things that occur in Congress as well as
information about all the election claims. This is probably going to be a
pretty significant development when it comes to the investigation and providing
little bit more context to things. Now Perry, at time of filming, the attorney
said that, well, he's undecided about whether or not he's going to appeal this
decision. At this point, them all being reviewed, I don't think an appeal
would matter. I don't know that it would change the outcome. But yeah this has been a long time
in development and now it does appear that the investigators are finally going to really get to
look at and use a whole lot of text messages and emails that have kind of already been screened for
them because you know 396 of them were protected by speech and debate. That's
what the court found. But those other 1,659 that those might be pretty
interesting. It's just a must-see. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}