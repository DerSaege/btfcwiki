---
title: Let's talk about Biden's pardons and effectiveness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YV9gGuvV9f8) |
| Published | 2023/12/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration recently expanded pardons for simple possession of marijuana.
- The impact is more about finding housing or employment rather than widespread.
- To receive benefits from the pardon, individuals must apply for a certificate from the DOJ.
- Most marijuana charges occur at the state level, not federal, limiting Biden's impact.
- Biden's goal is to set an example for governors to follow suit in pardoning similar offenses.
- This move signifies a potential shift in how federal charges are treated.
- The main aim is to encourage Democratic governors to take similar actions.
- The focus is on setting a precedent rather than directly impacting a large number of individuals.
- The majority of marijuana charges are state-level, limiting federal impact.
- The Biden administration is aiming to lead by example and encourage other governors to act similarly.

### Quotes

- "No one should be in a federal prison solely due to the use or possession of marijuana."
- "The real goal probably wasn't to just help the few thousand that are going to be impacted directly."
- "The overwhelming majority of those charges occur at the state level."
- "It's a thought. Y'all have a good day."

### Oneliner

The Biden administration's pardons for marijuana possession set an example for governors, aiming to lead by example rather than create a widespread impact.

### Audience

Policy advocates, activists, governors.

### On-the-ground actions from transcript

- Reach out to local governors to advocate for similar pardoning actions (implied).
- Support policies that prioritize pardons for simple possession of marijuana (implied).

### Whats missing in summary

Further analysis on the potential implications of the Biden administration's actions and the broader impact on criminal justice reform efforts.

### Tags

#Biden #Pardons #Marijuana #Governors #CriminalJustice #Reform


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden and pardons and its impact and why the
people who are saying it isn't going to have a wide impact are right, but also
they might be missing something.
So, the Biden administration has expanded on something they did earlier.
Recently, in the last two years or so, the Biden administration pardoned a whole bunch
of people for simple possession of marijuana.
have expanded the statutes covered by that. Now I've talked to people and asked
like how many people are going to be impacted by this. The number I get is
quote thousands. How many? No clue. But they generally agree that it isn't going
to be a widespread impact and it's more about being able to find housing or get
a job, something along those lines.
The pardon itself is automatic, however, to get those kind of benefits from being pardoned,
if you are one of the people who is impacted, DOJ is going to have like a certificate that
you have to apply for.
Now the reason people are saying it's not going to have a huge impact is because Biden
has the ability to pardon federal offenses, most of this type of charge occurs at the state level.
It isn't widespread. That's true. That's a true statement.
But it also might be missing the point because when the pardons went out,
Biden said, just as no one should be in a federal prison solely due to the use or possession of
marijuana, no one should be in a local jail or state prison for that reason, either.
The goal here is like a lead by example type of thing. The push that the Biden administration
is going for is trying to get governors to do something similar.
So that would have a wider impact because governors can deal with state charges or the
president can't.
That's the push.
Now there are other things that this might be a signal of as far as how things are treated
at the federal level. There's a whole lot to that. We may end up doing an old video on what that
would actually take. But this is a sign that he might be willing to pursue that. The real goal
of this though, it probably wasn't to just help the few thousand that are going to be impacted
directly. It's to set the tone, particularly for Democratic governors,
that it's okay to do this. And I think that's, I think that's what they're
angling for. We'll have to see how well that's received. But those people who are
saying this isn't going to impact a large percentage, yeah, they're right.
they're right but it's because the the overwhelming and I mean like just it's
not even close those charges occur at the state level so again maybe it sets
the tone maybe some other governors take him up on you know following that lead
but we'll have to wait and see and we probably won't see anything until after
the new year. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}