---
title: Let's talk about a question about progress....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-kV6g4MtRdU) |
| Published | 2023/12/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the issue of groups left out in the United States who get a raw deal and how they are perceived by others.
- Reads a message from a conservative individual questioning why minority groups seem to dislike America and American values.
- Responds by pointing out that these groups have been marginalized and mistreated by society, making it hard for them to support the country.
- Advises the conservative individual to support these groups on the issues where they acknowledge they are mistreated, even if they don't agree on everything.
- Emphasizes the need for the system to change to be inclusive of those who have been historically marginalized.
- Uses the analogy of a child not embraced by the village burning it down for warmth to explain the anger and desire for change among marginalized groups.
- Encourages advocating for change in the system to earn respect rather than expecting support for an unjust system.
- Suggests that actively supporting marginalized groups will lead to a deeper understanding of their struggles and potentially reveal more injustices.

### Quotes

- "A child that is not embraced by the village will burn it down to fill its warmth."
- "If you want them to respect a system, it has to be a system worthy of respect."
- "You have identified things that you will say is them getting a raw deal."

### Oneliner

Beau addresses the disconnect between marginalized groups and societal perceptions, urging support for change in a system that currently excludes and mistreats them.

### Audience

Conservative individuals, Allies, Advocates

### On-the-ground actions from transcript

- Support marginalized groups on the issues where they are mistreated (implied)
- Advocate for systemic change to be inclusive of marginalized communities (implied)
- Listen to and understand the struggles of marginalized groups by actively engaging with them (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on understanding and supporting marginalized groups in the United States, urging a shift in societal values and systems towards inclusivity and respect.

### Tags

#MarginalizedGroups #SystemicChange #Support #Inclusivity #Respect


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about groups that are left out
in the United States, those that get a raw deal,
and how other people perceive them at times.
Because I got a message, it's long,
And I think it's in good faith, which is a nice change.
So I'm going to read the whole thing,
and then we're going to go through it.
Can you explain something to me?
This isn't a gotcha question.
I'm a your side of the fence conservative.
I'm religious, so I don't approve of a lot of things
that have become common, but it's not my business.
I have a lot of sympathy for minority groups,
racial minorities, religious minorities, even alphabet minorities.
They get a raw dill in a lot of ways, I admit that.
But a lot of them, especially younger ones, really seem to hate this country.
Not in some racist, they're all the same kind of way, but they genuinely dislike America
and American values.
So here's my question.
Do they not understand how hard it makes it for people to support them when they make
it clear they want to completely change the country?
I know they want change and can even agree there needs to be some, but those that want
to tear it all down make it really hard to support the rest.
There's a lot in that.
I do.
is in good faith. I mean, if it wasn't, I don't think you'd be sending it. Here's
the thing, you acknowledge that these are groups that are left outside of American
society. They get a raw deal and you admit that. How can you expect them to
want to uphold that society? I think that's a better question. I mean I
understand what you're saying. You see things that you will acknowledge. Hey
that's wrong. That needs to be fixed. And you want to stand up for them, but they
say these other things and that's just too far for you. I get it. I mean, first
acknowledge you don't have to co-sign everything that every single person in
one of these groups says to support them in the things that you acknowledge are
them getting a raw deal. You don't have to sign off on the whole thing. I would
suggest that those issues where you admit they're getting a raw deal, even
if you don't support them on the rest, you kind of have an obligation, right, to
support them on those things and I think that's why you're sending this. It's one
of those things where you are encountering people who have been left
outside the system, given a raw deal by the system, by the establishment, by the
status quo for so long with people looking at them saying, hey well they're
getting a raw deal, oh well, that they've become more radical. Why would you want
to protect a system that didn't protect you? Why would you want to uphold a
system that left you outside of it? Those places where you admit they're
getting a raw dill. That's kind of the source of it, right? And it's interesting
to me that you can see the raw dill, acknowledge that it's impacting them
more than I'm assuming by this you were like a white heterosexual guy like
you're you're you are just 100% within the the the norm box right you see them
getting a raw deal, and you equate them wanting that changed with wanting to, uh, I guess
they're rejecting America and American values.
If you were getting a raw deal, would you like those values?
Like everything that you're saying in here, it all lines up with you seeing it, you acknowledging
it's wrong, but you just want it changed a little bit.
From their perspective, they see way more that's wrong because it's impacting them.
And as far as why they're younger, they get a raw dill, they're younger, they want to
tear it all down, I don't know why that's surprising.
A child that is not embraced by the village will burn it down to fill its warmth.
If you want to alter their opinions, you have to shift American values to something that
includes them.
You have to change it.
If you want them to respect a system, it has to be a system worthy of respect.
And if it's one that is leaving them out, giving them a raw deal, and even you will
admit that.
You really can't expect them to want to uphold it.
They want the warmth.
If you want to avoid that, you have to advocate for the change that you don't want to because
... well, it's hard, I guess.
Because they're not happy.
It all flows together.
Of course they're not happy.
They're getting a raw deal.
You have identified things that you will say is them getting a raw deal.
I promise you that if you actively support them in those things, you're going to find
out that they may be getting even more of a raw deal than you think because once you
start supporting them, you'll be around more, you'll hear more, you'll understand
more. This message, I don't think it's a gotcha question. I think it's in good
faith and I think realistically this is you at the beginning of really starting
to get it. One of those things where it's like you're so close but you're
actually trying to get there.
You will.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}