---
title: Let's talk about belief, Trump, and SCOTUS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nolVhvRblFs) |
| Published | 2023/12/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into Trump, the United States Supreme Court, and beliefs regarding presidential immunity.
- Beau addresses a message questioning his beliefs and points out that Trump may share the same belief.
- Trump believes that the Supreme Court will rule against him, as indicated by his actions.
- Trump's statement about presidential immunity and his reaction to the Supreme Court's decision indicate his belief that the Court will rule against him.
- Beau questions the extent of presidential immunity claimed by the former president.
- Beau believes that Trump's goal may be to prolong the legal process to maintain support rather than truly believing he will win.
- The former president has made inaccurate claims in the past, casting doubt on his current statements.

### Quotes

- "Trump has presidential immunity, period. End of story."
- "The idea that presidential immunity exists to the level the former president is kind of saying is silly."
- "I think his goal is to try to drag it out as long as possible and stop it from going to the Supreme Court in hopes that he can once again kind of trick a whole bunch of people into supporting him."

### Oneliner

Beau dissects Trump's belief in presidential immunity and his strategy to maintain support through legal processes, questioning the former president's true intentions.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact legal experts to understand the implications of presidential immunity (implied)

### Whats missing in summary

Insight into the broader implications of Trump's legal strategies and beliefs.

### Tags

#Trump #SupremeCourt #PresidentialImmunity #LegalProcess


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Trump
and the United States Supreme Court
and belief in who believes what.
Because I got a message talking about what I believe,
and I felt like it's important to point out somebody else
I think actually believes the same thing.
I don't understand how you believe the Supreme Court would rule against our president.
Trump has presidential immunity, period.
End of story.
You will see.
Okay, maybe.
You don't see how I can believe that, huh?
I think Trump believes that.
I think Trump believes that the Supreme Court is going to rule against him on this issue.
Allow me to read you something.
The Supreme Court has unanimously rejected deranged Jack Smith's desperate attempt to
short circuit our great Constitution.
Crooked Joe Biden and his henchmen waited three years to bring this sham case and now
they have tried and failed to rush this witch hunt through the courts.
Of course I am entitled to presidential immunity, I was president, and it goes on from here,
but the point is he's happy.
He's happy the Supreme Court did not elect to hear that case early, to hear that appeal
early, and establish that he has presidential immunity.
You would think that if you believed the Supreme Court was going to rule in your favor, you
would want to get that case to them as soon as possible, because if the Supreme
Court decides he has this all-encompassing presidential immunity,
that means the cases go away. Right? It seems like he wouldn't have said, oh, you
know, Smith asked the wrong question, you shouldn't hear it. It seems like he would
be in favor of getting it up there, because he believes the Supreme Court's
to rule in his favor and therefore all the cases go away. It seems odd, right, that he seems happy.
It seems strange to me.
The idea that presidential immunity exists to the level the former president is kind of saying
is silly. It really is, just on its face. I mean, understand that if the Supreme
Court was to rule in his favor, Biden can do anything he wants. It's just not how
this country was set up. I don't believe that Trump actually thinks he's going to
win this argument. I think his goal is to try to drag it out as long as possible
possible and stop it from going to the Supreme Court in hopes that he can once
again kind of trick a whole bunch of people into supporting him. If you were
in a position where you were certain that a court was going to rule in your
favor and if that happened, like almost all of your legal troubles disappeared
overnight it I mean you would be sad if they decided not to hear it right I mean
maybe that's just me maybe maybe he enjoys all of this it's fun for him or
something. I think there's probably another answer. Please remember that the
former president has made a lot of claims about what, in fact sometimes he's
actually like said the opposite of what was actually ruled upon. Forget like what
he thinks is going to happen.
He has said a lot of things that were less than accurate
about his proceedings.
This might be one of them.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}