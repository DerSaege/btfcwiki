---
title: Let's talk about a totally surprising blurred footage development....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4Ar4csH11mU) |
| Published | 2023/12/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speaker of the House Johnson promised to release all footage, but faces were blurred to protect individuals from retribution.
- Conservatives are now turning against Johnson for blurring the faces of federal agents.
- Releasing the footage was an attempt to placate conspiracy theorists, but pressure forced them to fulfill the promise.
- Blurring faces has fueled another conspiracy theory about protecting undercover individuals responsible for the events on the 6th.
- Republicans supporting the blurring of faces are now seen as part of the conspiracy in this theory.
- The longer these conspiracy theories persist, the more individuals become isolated and susceptible to negative actions.
- The Republican Party needs to acknowledge the truth about what happened on the 6th and the identities of those involved.
- The danger lies in the party's reluctance to tell its base the truth, as they have fueled these conspiracy theories to maintain energy.
- Failure to set the record straight could lead to the conversion of that energy into something truly negative.
- Leadership of the Republican Party must eventually come clean about the events.

### Quotes

- "At some point, the Republican Party is going to have to acknowledge what happened."
- "The longer these theories take hold, the further people fall down these echo chambers."
- "They're not deep state operatives."
- "The Republican Party seems very unwilling to tell its base the truth."
- "But that energy may be converted to something truly negative if the record isn't set straight."

### Oneliner

Speaker of the House blurs faces in footage to protect from retribution, leading to a backlash from conservatives and fueling conspiracy theories, urging the Republican Party to confront the truth to prevent negative outcomes.

### Audience

Republicans, Political Observers

### On-the-ground actions from transcript

- Hold accountable leadership in the Republican Party to acknowledge and address the truth (implied).

### Whats missing in summary

Analysis of the potential consequences of perpetuating conspiracy theories and the importance of truth in political discourse.

### Tags

#GOP #ConspiracyTheories #PoliticalAccountability #Transparency #Leadership


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about some completely
predictable news dealing with the blurring of the footage.
If you have missed it, the Speaker of the House, Johnson,
he's trying to make good on his promise
to release all of the footage.
he said that they would be blurring faces of some of the people in it and
that this was to protect them from retribution from first it was from the
Department of Justice and then later it changed to non-government actors. In a
completely foreseeable chain of events. Newsweek now has a report out saying
that conservatives are turning against Johnson because he is blurring the faces
of the feds. I feel like somebody said that was gonna happen. So the whole
thing behind releasing the footage was to placate people who believe a
conspiracy theory. That was the whole reason for it, and they leaned into it
and it was supposed to be a promise that they were never going to actually
fulfill, but there was so much pressure to do so they felt like they had to. Now
because they're going to be blurring faces, it has fed into yet another
conspiracy theory suggesting that they're trying to protect the identities
of like undercover people who were actually responsible for what happened
on the 6th. And that's what it's all about. Incidentally, that makes those Republicans
who are on board with blurring the faces, that makes them part of the bad guys in this
conspiracy theory now. At some point, the Republican party is going to have to acknowledge
what happened. To me, yes, on some level this is humorous because, I mean, how could you
not see this coming. At the same time, it's actually not funny, because as time goes on,
the longer these theories take hold, the further people fall down these echo chambers,
the more they become isolated, the more likely they are to do something bad.
The Republican Party is going to have to come out and say, yes, this is what happened. Yes,
the people who were there who then ran for office as Republicans are in fact Republicans.
They're not deep state operatives.
On one hand, I get it. It's funny, but there's an element of danger here
because the Republican Party seems very unwilling to tell its base the truth.
And the reason for that is because they have leaned into these conspiracy
theory is to keep their base motivated, and they don't want to lose that energy.
But that energy may be converted to something truly negative if the record isn't set straight.
At some point, the leadership of the Republican Party is going to have to admit what occurred.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}