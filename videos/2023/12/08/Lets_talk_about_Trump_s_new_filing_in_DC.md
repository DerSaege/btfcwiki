---
title: Let's talk about Trump's new filing in DC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lafmIvVreTg) |
| Published | 2023/12/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump filed a notice to appeal regarding presidential immunity in a D.C. federal case on election interference.
- Trump's team filed a motion to stay to halt all proceedings in the case until further notice from the court.
- Trump intends to act as if the case has paused until he hears from the court, which is an unusual approach.
- Typically, when a motion is filed, one waits for a response from the judge before proceeding based on that understanding.
- The court is likely to respond to Trump's preemptive actions, as it deviates from standard legal procedures.
- The judge may have direct words about this unusual course of action by Trump.
- It will be interesting to see how the court handles Trump's attempt to stop the proceedings unilaterally.
- The judge is unlikely to allow much time to pass while Trump assumes everything has halted based on his say-so.
- Expect an update on this situation soon.
- Stay tuned for further developments on this legal maneuver by Trump.

### Quotes

- "Trump will proceed based on that understanding and the authorities set forth herein absent further order from the court."
- "The judge may have some very direct words about how things work in her courtroom."
- "I'm sure we'll have an update on this relatively soon."

### Oneliner

Trump filed a notice to appeal on presidential immunity, attempting to stop proceedings unilaterally, prompting likely swift court response.

### Audience

Court Watchers

### On-the-ground actions from transcript

- Watch for updates on the legal proceedings involving Trump's attempt to halt the case (suggested)
- Stay informed about the outcome of Trump's motion to stay (suggested)

### Whats missing in summary

Insights into the potential consequences of Trump's unconventional legal strategy.

### Tags

#Trump #LegalProceedings #PresidentialImmunity #DCFederalCase #ElectionInterference


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump
and a motion that was filed,
and how I imagine, we'll probably hear pretty quickly
back about this particular motion,
because of something that was included in it
that I have to admit I've never seen before.
It is definitely a novel approach.
I'm not sure how that strategy is going to work out.
Okay, so if you missed the news yesterday, Trump filed a notice to appeal when it comes
to the whole idea of presidential immunity in the D.C. federal case, so the election
interference case there.
The appeal is about the idea that the former president is immune to all of this.
The judge there was like, no, that's not how this works.
So Trump's team is filing a notice to appeal.
Along with that, they filed a motion to stay, to basically stop all of the proceedings in
regards to this case.
And that in and of itself is bold, but it also includes this.
Trump will proceed based on that understanding and the authorities set forth herein absent
further order from the court.
Trump is basically saying that until he hears from the court, he's going to act like the
case has stopped, that the motion to stay is in effect, and that everything has just
kind of paused for the moment, and that unless he hears back from the court, that's what's
happening.
Yeah, I feel like the court's going to respond to that.
That's not generally how this works.
Normally, when you file a motion, you wait for a response from the judge, proceed based
on that understanding.
I don't know that an understanding has been reached with the court.
I feel like the judge might have something to say about this, and I imagine we might
even hear about that today.
If not, I feel like there will be a response that occurs pretty quickly.
I feel like in this case, the judge may have some very direct words about how things work
in her courtroom.
It will be interesting to watch, I'm fairly certain.
I don't see this just being ignored, and I don't feel like the judge is going to allow
a whole lot of time to pass while the former president is under the understanding that
everything stopped because he said so.
It just doesn't seem like something that's going to occur.
I'm sure we'll have an update on this relatively soon.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}