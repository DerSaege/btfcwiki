---
title: Let's talk about a question on Ukrainian aid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nz0ynXlWwMk) |
| Published | 2023/12/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how the United States provides aid to other countries, particularly focusing on military aid.
- Addresses misconceptions about foreign aid, specifically the idea of physically shipping cash overseas.
- Clarifies that the majority of the aid money is spent in the United States, supporting jobs and economic activity domestically.
- Encourages people to understand the process of foreign aid before forming strong opinions.
- Provides examples, like the case of a brother working on military equipment for another country, to illustrate how aid money circulates back into the US economy.
- Points out that those who believe aid money is not benefiting the United States economically may be misinformed.
- Emphasizes the importance of gathering all necessary information before developing strong opinions on complex topics like foreign aid.

### Quotes

- "Do you actually picture people loading pallets of money onto C-130s and flying it over?"
- "If your news outlet has led you to believe physical currency is being shipped to Ukraine, you need a different outlet."
- "You may confuse a yacht for a truck."
- "Morality doesn't have anything to do with foreign policy."
- "You do not have the information to have an opinion as strong as the one you're holding."

### Oneliner

Beau clarifies misconceptions about US foreign aid by explaining how the majority of military aid money is spent domestically, supporting jobs and economic activity.

### Audience

US Citizens

### On-the-ground actions from transcript

- Understand the process of foreign aid and how it benefits both the recipient country and the US (implied).
- Educate yourself on where aid money is spent and how it contributes to the domestic economy (implied).
- Fact-check information about foreign aid to ensure accurate understanding (implied).

### Whats missing in summary

Full understanding of how US foreign aid works and its economic impact.

### Tags

#USForeignAid #Misconceptions #EconomicImpact #InformationConsumption #FactChecking


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about the United States
and aid and how it works, because there are definitely
some questions about how it actually functions.
And I feel like maybe this isn't the only person who
doesn't actually understand how the finances work
the U.S. provides aid to another country. And if you're going to have a strong
opinion about it, you should probably understand how it works. Okay, so here's
the question. I'm just wondering how much Zelensky is paying you to encourage
Americans to ship money over to him. 68 billion dollars is money that could be
better used here. I don't even understand what the money is for. We're shipping him
weapons and equipment too. My brother has been working enough overtime building stuff for their
war to buy a new truck. So we pay for their weapons and give them money? So what is the money for,
huh? So Zelensky can buy a house or a yacht? I'm sick of shipping cash overseas."
Okay, so let's start with this. The 68 billion dollars. That is specific. That's a specific number.
That deals with military aid. Had you chosen one of the other numbers, this wouldn't be quite as
lopsided as it's about to be, but this is specific to military aid. Okay, so I'm sick of
shipping cash overseas. What do you actually think is
happening? Like this is the part about this that I don't
understand. Do you actually picture people like loading
pallets of money on to the in the back of C 130s and flying
it over? What do you think occurs? Here's, I'm gonna give
some advice here. I don't even understand maybe work on figuring that part out before
you develop a strong opinion. My brother's been working enough overtime building stuff
for their war to buy a new truck, so we pay for their weapons and give them money. So
what is the money for? So Zelensky can buy a house or yacht? No, so your brother can
buy a new truck. Out of that 68 billion dollar number that you're using because
it is specific to military aid, almost all of it, like 90% of it, is spent in
the United States. I am willing to bet almost anything. Your brother lives in
Arizona, New Mexico, Pennsylvania, or Arkansas because that's where the money's
going. It's not being like literally shipped over. It's being used to pay
people to make stuff here that is then shipped over. Your brother's new truck
came out of this 68 billion to be clear. There are other numbers. I think all in
it's probably closer to the hundred billion. But even using the larger
number, most of it is spent in the United States. The idea that it would be better
spent here, it is being spent here. You have a direct, you have direct
evidence in your own life of where it is being spent. If your news outlet, if the
news that you consume has led you to believe that like physical currency is
actually being shipped to Ukraine, you need a different outlet. This is
pretty basic. This is something that definitely should have been explained
during coverage especially if they're taking the position that it isn't
benefiting the United States economically and to be clear just using
your brother's truck as an example the money is spent in the United States okay
and it goes to a worker somebody who's apparently working overtime okay so then
And they take that money, and they buy a truck with it, and that truck is, that's somebody
else's job.
It generates a lot more economic activity than just the money that is spent.
If you want to have an opinion about the aid, and it's one of those things where you're
like, I don't believe in supporting wars, that's one thing.
If you want to take a moral stance, yes, I'll tell you, you know, morality doesn't have
anything to do with foreign policy, but at least I can understand it.
If you are going to take the stance of, I don't really care that the money's being spent,
but it would be better spent here, and it is being spent here, that's not a moral thing.
That's an information consumption thing.
You do not have the information to have an opinion as strong as the one that you're holding.
I mean, you're writing some random dude on YouTube suggesting that the president of another
country is paying him off.
Seth Zylinski can buy a house or yacht.
No, no, it's for the, I'm going to guess Dodge Ram, in your brother's driveway.
where it ends up. And not really just there, because the people who made that truck, they
spent money from it too. If that isn't being explained by the pundits who have you mad
about this, they're doing it on purpose. This is a basic thing when it comes to the
United States providing military aid. The money spent here. This is something you
definitely need to understand. There are 117 more than 117 production lines right
now in the United States and 31 states producing stuff for Ukraine. All of those
people are getting paid. That's where it's going. Most of it is Arizona, New
Mexico, Pennsylvania, and Arkansas. So it is important to gather all of the
information you need. If you're saying, I don't even understand this, start by
filling in that blank before you develop a very strong opinion about something.
Because you may, you may confuse a yacht for a truck. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}