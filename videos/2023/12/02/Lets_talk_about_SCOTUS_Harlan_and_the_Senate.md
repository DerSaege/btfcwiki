---
title: Let's talk about SCOTUS, Harlan, and the Senate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8xVENApmluw) |
| Published | 2023/12/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent actions taken by the Senate Judiciary Committee regarding subpoenas for Harlan Crowe and Leonard Leo.
- Mentions the partisan nature of the vote on the committee, with Democrats wanting to hear from them.
- Notes that if Crowe and Leo refuse to comply with the subpoenas, it will have to go to the full Senate and require 60 votes for enforcement.
- Questions the Senate Judiciary Committee's motivation for looking into the issue of ethics surrounding the Supreme Court.
- Emphasizes the importance of the Supreme Court's legitimacy and the need for public faith in its ethical operations.
- Suggests that if Congress wants to implement an ethics code for the Supreme Court, they should simply do so without creating a spectacle that undermines the institution.
- Advocates for a straightforward approach of putting forth legislation for an ethics code instead of engaging in lengthy hearings that may further damage the Supreme Court's legitimacy.

### Quotes

- "The legitimacy of the Supreme Court is paramount. If the American people do not believe that the Supreme Court is operating in an ethical and good faith manner, eventually the rulings will be ignored."
- "If Congress wants to put forth ethics, an ethics code that the Supreme Court is supposed to follow, they need to just do it."
- "The show, it doesn't matter. Put forth the code, make sure it is applied from here on out, and go from there."

### Oneliner

Beau delves into the Senate Judiciary Committee's subpoenas for individuals linked to Supreme Court justice gifts, stressing the importance of public faith in the Court's ethics and advocating for a direct approach to implementing an ethics code.

### Audience

Congress Members

### On-the-ground actions from transcript

- Put forth legislation for an ethics code for the Supreme Court (suggested)
- Ensure the ethics code is applied consistently from now on (suggested)

### Whats missing in summary

Insights on the potential repercussions of undermining the legitimacy of the Supreme Court and the importance of public trust in its ethical operations.

### Tags

#SupremeCourt #Ethics #SenateJudiciaryCommittee #Legislation #PublicTrust


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk about the Supreme Court
of the United States and the Senate Judiciary Committee.
And getting to the heart of the matter
when it comes to the current view of the Supreme Court,
particularly in relationship to the ethical questions
that have been raised lately.
Okay, so if you've missed the news,
The Senate Judiciary Committee voted to issue subpoenas,
and those subpoenas are for Harlan Crowe and Leonard Leo.
Now, I don't think Harlan Crowe needs any backstory.
That's the person who is involved
with the gifts for a Supreme Court justice.
Leonard Leo is a large figure in the Federalist Society.
The Senate Judiciary Committee is asking for records from them.
The vote was very partisan.
It was a party-line vote on the committee, and Democrats want to hear from them.
This is a subpoena from the committee, which means if Leo and Crowe are just like, yeah,
we're not complying, it has to go to the full Senate.
And they need 60 votes to make it happen, to make that enforceable.
Don't know if they're going to get it.
Don't know if they're going to get it.
Now, the real question here is why Senate Judiciary wants to look into it.
And the obvious answer is, oh, well, there's this thing that is perceived to be unethical.
I mean, sure, sure.
And the Supreme Court put out their own guidelines for ethics, which have no enforcement mechanism,
didn't make the American people feel warm and fuzzy, right?
The thing is, when you are talking about the Supreme Court, and this is something that
was brought up, the Supreme Court is an entity that is a lot like currency.
It's faith-based.
The Supreme Court can't cut funding.
They can order it, but they can't do it.
can't call up an army to enforce their decisions. It is faith-based. The
legitimacy of the Supreme Court is is paramount. If the American people do
not believe that the Supreme Court is operating in an ethical and good
faith manner, eventually the rulings will be ignored. This is what Senate
judiciary is getting at and they want to try to do some fact-finding on it, I guess.
Now realistically, if it was me, if there are people in the Senate that want to try
to say the Supreme Court needs an ethical code, they need to just do it. Just put it
forward. Don't turn it into a giant thing trying to undermine the Supreme Court.
Just put forth the legislation. That needs to be the route that that goes.
I think a giant set of hearings that undermines the institution even
further at a time when it is definitely losing its legitimacy in the eyes of
Americans, it's probably not a great idea. If Congress wants to put forth ethics, an
ethics code that the Supreme Court is supposed to follow, they need to just do
it. The show, it doesn't matter. Put forth the code, make sure it is applied from
here on out, and go from there. If you are interested in correcting the system,
that's what needs to happen. The hearings are political in nature. I mean there
there there is a legislative purpose but the goal I think everybody knows what
the problem is. So just it might be better just to put forth the ethics
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}