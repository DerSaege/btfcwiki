---
title: Let's talk about Trump and Presidential immunity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wDKKCZWQaeU) |
| Published | 2023/12/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An appeals court decision impacts Trump's immunity in lawsuits filed by law enforcement and congressional staffers regarding January 6th events.
- Trump's claim of presidential immunity was rejected because his speech inciting actions did not fall under official responsibilities.
- However, his failure to act during the Capitol incident falls within his official duties and grants him immunity from that claim.
- The decision allows pending lawsuits against Trump to move forward, requiring him to go to trial and contest facts.
- The ruling may influence how courts view Trump's actions in other cases, but this specific case is civil, not criminal.
- The decision could lead to numerous legal challenges for Trump in the DC area and potentially beyond.
- Trump's legal team likely views this decision unfavorably, as it opens the door to more legal entanglements.
- The only immunity granted to Trump was regarding his failure to stop the Capitol events, deemed part of his official capacity.
- Trump can now be sued for his actions leading up to the Capitol incident, but he has the chance to contest the claims in court.
- The decision creates uncertainties for Trump's legal future, prompting potential creativity from attorneys in pursuing legal actions.

### Quotes

- "The judges had to weigh whether or not what was happening was part of his official responsibilities."
- "This isn't a situation where Trump lost these cases, it's a situation where those cases can now proceed."
- "All in all, not a good decision."
- "If you are on Trump's legal team, you're not happy about this."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

An appeals court decision on Trump's immunity opens the door to legal challenges following January 6th events, impacting civil lawsuits and potential future legal entanglements.

### Audience

Legal observers, activists, concerned citizens

### On-the-ground actions from transcript

- Monitor the progress of the lawsuits against Trump and advocate for accountability (suggested).
- Stay informed about the legal implications of the court decision and its potential ramifications (implied).

### What's missing in summary

Insights on the broader implications of the court decision and its potential impact on accountability and legal proceedings against political figures. 

### Tags

#Trump #Immunity #LegalChallenges #Accountability #CourtDecision


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Trump and immunity
when he has it, when he doesn't,
because an appeals court made a decision about this.
And it's gonna have some pretty far reaching impacts
down the road to a whole bunch of different cases.
Okay, so short version, what happened?
some law enforcement and congressional staffers, they filed suit against the
former president for what happened on the 6th.
Trump said, no, I have presidential immunity.
I can't be held responsible for that.
The judges had to weigh whether or not what was happening was part of
his official responsibilities.
Short version is the speech that some claim kicked all of this off where the former president
directed people to go where, you know, everything happened.
That did not fall under the category of official responsibilities, therefore he does not have
immunity.
That was more of a person running for office rather than a person who was in office.
The claim that he should have stopped and done more to stop what was going on at the
Capitol once it started, that is within his official responsibilities.
So he is immune from that claim.
And that's kind of how the whole thing shapes up.
So what does this mean?
First, keep in mind, there's going to be more bills, almost certainly.
But for the meantime, the lawsuits brought by members of Congress, brought by cops, brought
staffers, there's, I don't know, six, met more probably, that have been on hold
waiting for a determination like this. They get to move forward now. Now that
doesn't mean that Trump lost them. What it means is now he actually has to go to
trial. He can contest the facts. He can say something to the effect of, no, what I
said during that speech actually didn't cause it, and he can try to argue it that
way. So this isn't a situation where Trump lost these cases, it's a situation
where those cases can now proceed. It may have a little bit more reach, this
decision, because it may impact how other courts look at his activities. The thing
to keep in mind is that this case is civil, not criminal.
So there will probably be different standards applied in cases that might be criminal.
Overall, though, this kind of just opens the door to a whole bunch of legal entanglements
for the former president in the DC area, and maybe even more depending on how creative
attorneys get with this.
So all in all, not a good decision.
If you are on Trump's legal team, you're not happy about this.
About the only thing that they got was, hey, you know, you can't say that he should have
stopped it and you're going to sue him for that because that's a decision that he made
in his official capacity and therefore he's immune to a lawsuit.
That's really all I got.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}