---
title: Let's talk about lead, the EPA, Biden, and moving forward....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6h5Ao-7hQ9s) |
| Published | 2023/12/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The EPA has proposed a new rule that will require most cities in the United States to replace all lead pipes within 10 years, regardless of lead levels in the water.
- The rule also aims to lower the amount of allowable lead in water systems, with a requirement to provide filters if levels exceed the limit.
- Nearly a hundred thousand people in Flint were affected by lead contamination, underscoring the urgency of addressing this issue.
- The ban on installing lead pipes began in the 1980s, revealing decades of neglect in fixing the problem despite knowing that no level of lead is safe.
- Funding for this pipe replacement initiative is available through the bipartisan infrastructure law passed in 2021.
- Public comments will be accepted for 60 days, followed by a hearing in January before the rule takes effect.
- While some cities may resist the rule, the EPA and Biden administration are pushing for it, backed by available funding.
- The key challenge may revolve around the financial responsibility for replacing lead pipes.
- Beau speculates that unless influential politicians advocate against it, the rule is likely to move forward smoothly.
- The focus should be on prioritizing public health and preventing lead poisoning, rather than financial considerations.

### Quotes

- "No acceptable safe level of lead."
- "It's better for me politically if your kids get lead poisoning."
- "Y'all have a good day."

### Oneliner

The EPA proposes a rule mandating lead pipe replacement in cities, prioritizing public health over financial concerns, with funding available from the bipartisan infrastructure law.

### Audience

Citizens, legislators, activists

### On-the-ground actions from transcript

- Contact local officials to advocate for prioritizing public health over financial interests (implied)
- Stay informed about the progress of the rule and actively participate in public comments and hearings (implied)

### Whats missing in summary

The full transcript provides additional context on the history of lead pipe issues, the urgency of addressing them, and the potential resistance from certain cities.

### Tags

#LeadPipes #EPA #Biden #PublicHealth #Infrastructure #Advocacy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about water.
We're going to talk about water and lead and the EPA and Biden and where it goes
from here, because the EPA has suggested a new rule and that rule is going to lead
to some pretty big changes if it goes into effect.
Before it goes into effect there will be 60 days of public comments and then like
a hearing I think in January. So what's the rule? Basically most cities in the
United States will have 10 years to replace all the lead pipes. It's kind of
what it boils down to. Short version. And this is regardless of any lead levels
that show up in the water. There's also a part of in it that is designed to lower
the amount of allowable lead and if a system goes above that well they have to
provide everybody with filters. They have to provide all the consumers with
filters. You know there is undoubtedly going to be some pushback on this from
various cities. It's worth remembering that almost a hundred thousand people
in Flint were impacted by that. It's also worth remembering that the process
of banning installation of the lead pipes, that started in the 80s. They've
had decades to fix this issue and they have chosen not to when it is widely
accepted that there is no acceptable safe level of lead. They just let it
ride and now they're going to have to fix it. One of the big questions of
Of course, because it's the United States and the first thing we think of is not, you
know, the kids drinking the lead water, it's going to be who's going to pay for this.
That bipartisan infrastructure law, Biden's law back in 2021, that has a whole lot of
money for it.
So there will be public comment, there will be a hearing in January, and then this should
go into effect. There will probably be pushback from some cities, but I don't think it's going
to be enough to overcome this rule. This is something that the EPA wants, this is something
that the Biden administration wants, the money's there, the timing, it all fits. So unless there
There are some heavyweight politicians who want to come out and basically make the case
to the American people that, no, actually, it's better for me politically if your kids
get lead poisoning.
I feel like this one's going to move forward.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}