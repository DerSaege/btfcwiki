---
title: Let's talk about intelligence failures in other countries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ui3SWzr7QJw) |
| Published | 2023/12/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring failures through a different country's lens.
- Prompted by a question on intelligence failure causes.
- Israel's intelligence failure as a case study.
- Factors contributing to intelligence failures.
- Groupthink, political pressure, opposition misjudgment, and doctrine misunderstanding.
- Examples of historical lessons shaping intelligence assessments.
- Importance of understanding applied doctrine over written doctrine.
- Political pressure as a common factor in intelligence failures.
- Lack of institutional memory affecting intelligence assessments.
- Failure to challenge consensus leading to intelligence failures.
- Propaganda's role in shaping perceptions of opposition capabilities.
- Groupthink, underestimation of opposition, and political pressure as key factors in Israel's case.
- Importance of safeguarding against failure in intelligence assessments.

### Quotes

- "Groupthink, underestimation of opposition, and political pressure as key factors in Israel's case."
- "Israel's intelligence failure as a case study."
- "Exploring failures through a different country's lens."

### Oneliner

Beau delves into Israel's intelligence failure, citing groupthink, underestimation of opposition, and political pressure as key factors in this insightful exploration of intelligence failures.

### Audience

Analysts, policymakers, researchers.

### On-the-ground actions from transcript

- Challenge consensus in intelligence assessments (implied).
- Understand applied military doctrines of opposition (implied).

### Whats missing in summary

Beau's detailed analysis and examples provide a comprehensive understanding of the dynamics behind intelligence failures.

### Tags

#IntelligenceFailures #Israel #Groupthink #PoliticalPressure #Propaganda


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about failures
and how they occur.
It is something we have talked about on the channel before.
However, when we talked about it before,
it was through the lens of a specific country.
That is not the country in question now.
So we're going to go back through it
and talk a little bit about how it happens in other places.
It's kind of the same,
and I will have the original video down below
people who want to look a little bit more into it, but I'll do a quick recap
in this one. And this is the message that prompted all of this. I was wondering if
you had any thoughts on what caused the intelligence failure. I watched your
video about intelligence failures and I can see how groupthink and political
pressure played into it, but that doesn't seem like enough. What am I missing? Okay
so if you've missed the news, there's a whole lot of reporting that suggests
Israel had had a lot of information, some of it pretty detailed, and some of it
stuff that definitely should have been taken seriously given the source, and it
wasn't. So people are asking how did that happen? Okay, in the United States it's
normally four things or a combination of these four things. Okay, first is group
think. Group think is where you have a whole bunch of people conduct and create
these assessments and estimates. Then they talk to each other and when they
talk to each other they start to adjust their estimate, their assessment, to be a
little bit more closely in line with everybody else's because they don't want
to be an outlier. What generally happens is the person who is most persuasive
in their belief, all of the other estimates and assessments move in that direction.
The problem with this is that the person who is most persuasive is not necessarily the
most accurate.
So what ends up happening is a giant stack of estimates that says X is going to happen
and two that say Y is going to happen.
But if you're looking at the information objectively, it's pretty clear that Y is
going to be what occurs, but because of the sheer volume of estimates saying X,
that's what the policymaker believes. That's one. Another, assuming your
opposition won't make a mistake overestimating them. The US does this a
lot, and this is due to this is due to historical lessons learned by the US
military. Vietnam and then the first time in Afghanistan against the Soviets. Those
two, those two conflicts right there shaped this belief. In the United States
there isn't the presumption that that low-tech group, they're not going to be
any good, they're primitive or whatever. The US doesn't, they don't fall prey to
to that. They're on the other end. They overestimate their opposition and assume
they won't make mistakes that they might. A good example of this you can see on
the channel is prior to the Russian invasion of Ukraine. I did this video
where I was talking about the different ways they might try to go about it.
And offhandedly I was like, you know, and somebody mentioned they might try to do
some air drops and take some stuff from the air. I just don't see that as likely.
That would be a horrible mistake. It was offhanded to the point I had dismissed
it to the point it almost didn't even get mentioned. Well they did try to do
that. It was a horrible mistake but they tried. As a commentator that's fine. If
you're actually producing information that is going to be relied upon in
combat, saying that it's not going to happen or dismissing it to that degree,
that may lead to like an airfield being undefended. So that's how intelligence
failures occur there. Another is not understanding your opposition's applied
doctrine. Applied being the key word. Every country in the world has a military
doctrine, how they are supposed to behave in combat. The thing is, very few countries
follow what's written down. The United States is famous for not following their own doctrine.
So if you don't understand their applied doctrine, what they historically do, and you base it
off of what they have written down, you'll make mistakes. This also occurred during the Russian
invasion of Ukraine. You had a whole bunch of people waiting for that second wave of good troops
or good equipment or whatever because there's a doctrine that says they would do that but
they've never actually used it ever. So you can't actually rely on that being what they're going to
do. The fourth is of course political pressure and that isn't always something super nefarious.
Sometimes it's just the normal functioning of governments. A good example, in the U.S. it's
it's a fair a fair assessment to say the U.S. can go toe to toe with Russia and China at the same
time. However, nobody up in D.C. is going to want to hear that because then they have to
explain to the American voter why they keep spending all bunch of money. Okay, so
groupthink definitely appears like it it played into what happened in Israel. It
looks like the information went up and it got a couple levels up the chain and
everybody came to the same conclusion. My guess is somebody was really persuasive.
The political pressure probably had a lot to do with it because it does seem as though the Israeli government was more
interested and more concerned with what was going on in the bank.  Israel does not have the institutional memory of
something like Vietnam.  like Vietnam. They don't have that. The IDF, they are a military organization that
has won consistently. So they don't view it that way. They do tend to underestimate
low-tech groups. This definitely played into this. If you look at the reporting,
it kind of looks like the information went up a couple of levels and then it
hit this point where everybody was just like they're not gonna be able to pull
that off and it got disregarded and that's where the failure occurred. The
interesting thing is that Israel is actually supposed to have a safeguard
against this where if everybody is saying one thing they're supposed to be
person who, even if they believe what everybody else is saying, they argue
against it just to make sure this kind of failure doesn't occur. It doesn't look
like that was applied here. So here it's not a matter of assuming your opposition
won't make a mistake overestimating them, it looks like it is a case of underestimating
them because they're low tech, because they're not a real military.
This is something that militaries all over the world do.
In fact, the US is kind of an exception on this one.
definitely made the same mistake going into Ukraine. There is something
that occurs with the propaganda in various countries. Like in the US, the
propaganda about the quote rice farmer in Vietnam, okay, again taking on mythical
status, like to where people will even forget the NVA, the North Vietnamese Army,
even existed. It's just the VC, right? That took hold in the U.S. very, very easily, and it helped
create the idea of always overestimate your opposition because of the history in the U.S.
Because there was a time when the U.S. was the low-tech army, and it's part of the American myth.
So that propaganda took hold. In Russia there's a lot of propaganda that
basically says Ukrainians, well they're lower. So how would they be able to
stand up to it? A lot of it is believing your own propaganda. The US does it in a
way that for once in this particular case it's kind of useful because the US
generally speaking overestimates their opposition and they have to guard
against they have to guard against assuming the other side won't make a
mistake rather than being completely caught off guard by a group that did
train, that did plan, the US kind of always assumes that they did. So that
that appears to be the situation. Groupthink a couple of levels up the
chain, underestimating the capabilities of the opposition and then political
pressure diverting resources. That's what looks like costed. Now it's still early
on. More information will certainly come out, but based on what's available, those
three things definitely apply. I know there's a lot of people that want to
turn this into some conspiracy. Probably not. This is a pretty standard failure
in how intelligence agencies fail to produce the results that they're
supposed to. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}