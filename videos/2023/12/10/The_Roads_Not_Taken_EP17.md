---
title: The Roads Not Taken EP17
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RbaiId41wZs) |
| Published | 2023/12/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces "The Roads with Beau" episode 17, covering under-reported events and taking questions.
- Updates on foreign policy: Putin ally's ambition for the world, potential Russian aggression towards the Baltics, and Moody's warning on China's credit rating.
- Houthis threatening ships bound for Israel, a federal judge barring migrant family separation, and U.S. aid politics hindering Ukraine's success.
- Special operations targeting high-level figures, fluctuating gas prices amid global tensions, and budget games by Republicans.
- Legal updates include a ruling against delaying trials for political purposes and growing support for Nikki Haley over Trump.
- Ohio utility regulator involved in a bribery scheme, news in culture like a movie about George Santos and Taylor Swift as Time's Person of the Year.
- Elon Musk's platform changes and environmental news on COP28's failure to meet climate goals.
- Senator John Kennedy's price comparison from "Home Alone," showing detachment among elected officials.
- Updates on shelter fundraiser successes, future plans with the channel, and handling disagreements among commentators like Nance and Kelt.
- Explanation on captives being called hostages or prisoners, focusing on non-state actors' distinctions.

### Quotes

- "We want the world, preferably all of it."
- "A pretty clear indication that the appeals court sees Trump's DC trial staying on schedule."
- "I see this as a very big wasted argument."
- "Having the right information will make all the difference."
- "Y'all have a good day."

### Oneliner

Beau covers under-reported events from foreign policy to cultural news, legal updates, and Q&A, offering context and insights to help navigate the information overload.

### Audience

News consumers

### On-the-ground actions from transcript
- Contact organizations aiding Ukraine to counteract hindrance from U.S. aid politics (suggested).
- Support shelters with holiday meals and specialized items for teens (exemplified).
- Stay informed on foreign policy developments and geopolitical tensions (implied).
- Stay engaged with legal proceedings and understand implications of judicial decisions (implied).
- Support environmental initiatives addressing climate change goals (exemplified).

### Whats missing in summary

Insights on how the under-reported news can inform and empower viewers to navigate today's complex global landscape.

### Tags

#ForeignPolicy #LegalUpdates #CulturalNews #ClimateChange #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is December 10th, 2023,
and this is episode 17 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
covering developments that were under-reported,
unreported, or will serve as context later,
or things that I just happen to find interesting.
And then at the end, we take some questions from y'all.
Okay, so starting off in the world of foreign policy, a Putin ally said on Russian state
TV, quote, we want the world, preferably all of it.
And that's where we're going.
Right now, the Russian empire is growing back.
We're coming back.
The Baltics will be next.
They say they're ready for a war with Russia.
How long will that take?
About 15 minutes.
Checking my notes here to remind everybody that we're way into their last two week operation.
This is undoubtedly going to be used a lot to show that there are people very close to
Putin who want more than Ukraine and this will probably be used to showcase how important
it is for NATO to continue to support Ukraine in its endeavors.
Moody's is warning that it may downgrade China's credit rating outlook.
That is not really surprising.
We've talked about how the Chinese economy is not falling like a lot of the more sensationalist
outlets are putting it, but it's stumbling.
It's having issues right now.
They will probably be pretty short-lived.
Okay, let's see.
The Houthis are signaling that they will target all ships bound for Israel.
It's a pretty big escalation and that might be something that actually starts to prompt
a wider response.
A federal judge has barred the separation of migrant families for eight years as part
of a settlement.
It preemptively bars the practice if Trump was to return to office.
The U.S. is playing politics when it comes to aid to Ukraine and toying with just massive
defeat on the foreign policy scene and of course is hindering Ukraine's ability to
win.
Republicans are playing budget games with something that should theoretically be off
limits to play games with.
special operations seem to have successfully taken out a high-level
collaborator and former member of parliament who was outside Moscow.
Currently gas prices are continuing to fall but it is important to remember
with the global tensions you might see them start to go back up a little bit.
OK, moving on to US News.
OK, so this is a quote from the ruling recently.
And it's something that's kind of being overlooked.
It's being talked about in the legal channels
and in legal coverage, but it's not really
getting widespread play.
Delaying the trial date until after the election, as Mr. Trump proposes, would be counterproductive,
create perverse incentives, and unreasonably burden the judicial process.
In addition, postponing trial would incentivize criminal defendants to engage in harmful speech
as a means of delaying their prosecution."
I mean, all of that sounds like pretty common sense stuff.
This coming from the court is a pretty clear indication that the appeals court sees Trump's
DC trial staying on schedule.
It does not seem like they want to allow his delay tactics to work.
Republican insiders are starting to coalesce around Nikki Haley as basically they've determined
that the former president is a losing candidate long-term.
We're probably going to see more and more of this as it goes on.
There's going to be more and more high-profile people who throw their support behind Haley.
Ohio's top utility regulator turned himself in last week in connection with a $60 million
bribery scheme dealing with energy concerns.
The allegations are pretty serious.
It's worth noting that this is connected to the same bailout that sent Ohio's former
House Speaker to federal president for, I think, 20 years.
Moving on to cultural news.
There is reportedly a movie in the works about George Santos.
Yeah, the country's doing great.
The special election to replace him, I believe, is set for February 13th.
Taylor Swift is Time's Person of the Year.
This has upset people on the right who feel like somehow the liberals made her or something.
getting weird, expected to get weirder. Advertisers appear to be taking Elon Musk
up on his offer and leaving the platform. If you remember, he had some colorful
advice for some of them. In totally unrelated news, it appears that Musk is
is considering bringing back Alex Jones to the platform.
I am certain that that will help with the issues
he's having with advertisers.
In environmental news, in the background of COP28,
there was an unspoken acknowledgment from most people
that the world would not meet its 1.5 degree goal.
It does not look like that's going to happen, and sadly, because of a lot of the other stuff
that was going on there, it didn't... acknowledging that that wasn't going to be met should have
been a push to get more done.
That didn't really... it doesn't seem like that occurred.
Please comment is hitting its furthest point and is starting to head back.
It should be back around 2061.
In oddities, Senator John Kennedy tweeted out that Kevin in Home Alone spent $19.80
during his shopping trip and that today it would cost $72.28.
He blamed this on Biden, apparently unaware that that movie was released in 1990.
We've officially hit the point where we've had people up there so long that we have senators
who are talking about, you know, back in my day, a gallon of milk was a nickel until the
Kaiser came.
Stuff like this shows how out of touch some of our elected officials really are with the
rest of the world.
Okay, moving on to the Q&A.
Before we start, we will do a little update on the shelter.
We got the numbers from the shelter fundraiser.
We got the number from the shelters.
have more than enough. In fact, we will definitely be able to do all the tablets that they need,
those gift bags. Then for one, we will also be doing basically getting all of this stuff
for holiday meals for people. And then for the other, they are going to send us a little
bit more of a specialized list of things that that some of the the kids may want
that may not show up if they weren't specifically asked for and we'll be
taking care of that and odds are based on the numbers we have seen so far once
we're done with all of that we will end up cutting cutting them checks as well
So, this is definitely a successful year.
And part of that is because there aren't as many teens in the shelters this year, which
is always, it's always good to have overshot for that reason.
This is a service you don't actually want people to need, so the lower the numbers the
better. Okay. Can you comment on the situation in Venezuela? I'm
going to wait. There is a high profile meeting, a very high
level meeting between the two countries that is supposed to
occur this week. We will know a whole lot more about it after
that and we'll be able to see the real tones of people okay the new assault
weapons ban seems to cover all the ways the previous one was ineffective I know
it has no chance of surviving the Supreme Court but is it a bill that
finally creates an assault weapons ban that would be comprehensive you know
You know what, I'm going to be honest here.
That middle part, I know it has no chance of surviving the Supreme Court, I haven't
read it.
You're 100% right.
It's not going to make it through this court.
I have not even read the bill.
I could not tell you whether or not it was comprehensive or not.
It's on my list of stuff to read, but it keeps getting moved down because realistically,
Even if it's passed, signed, and everything goes well, the Supreme Court is going to knock
it down.
What's the joke about Dodge Rams?
Okay, so just so everybody knows, that's a rule joke.
I don't get into the whole what truck is best thing.
I know that's a cardinal offense among rednecks, but I don't have a hard opinion about that.
The joke is that not that those trucks aren't used by rural people, it's that generally
speaking when somebody from the suburbs buys a giant truck and then takes it to a rural
area and gets it stuck, it's almost always a dodge rail.
It's a self-selecting thing.
And that was the joke.
As far as all of the jokes about trucks and all of that
stuff that you hear me talk about, the only one that I'm
actually serious about is you probably don't need a dually.
To me, a dually is very much a piece of equipment, not a personal vehicle.
And when I see people in the suburbs who buy duallys who obviously do not use it for pulling
or hauling or anything like that.
I do tend to laugh a little bit at that
because those things are,
it leads to a chain of events that I always find amusing.
It's a piece of equipment that to me,
it's more akin to like a tractor than a personal vehicle.
It's not, it's designed to work,
not just passenger transport.
They are very expensive,
and the chain of events is normally,
I want, you know, big bad truck,
I buy big bad truck and spend at times six figures,
and then I spend the next two years
until I trade it in complaining about the fuel bill,
because I bought a vehicle
that if you're using it in the suburbs or in the city,
you're getting single-digit mile to the gallon.
That's the only thing I'm actually serious about
when I talk about that stuff.
How many people outside your immediate family
have seen you without a ball cap?
A lot.
Yeah, I'm not actually just like stuck with it on.
In fact, I have done videos without one.
The fact that you don't know that is why I always wear it.
On YouTube, people recognize patterns,
and like when you're looking at the thumbnail.
And if I don't have the hat on, the video
to catch first. I'll put one of them down below and it is satire. In fact, both the ones that I
think both of the ones that I did without wearing a hat are satire. So just, you know,
as I start singing the praises of Kavanaugh, understand it's a joke.
Can you explain to me what a fake elector is?
I'm confused as to how this could happen.
Don't we know who should show up to cast an electoral vote?
Okay, so this is generally, this is not specific to any person's allegations or anything like
that because they are different all over the place but it see and that also gets
into something else it is incredibly likely to my way of thinking there will
be more information about this later as the the federal case in DC progresses to
other people. This is a lot like somebody making a driver's license for
themselves that isn't... it doesn't look like a real driver's license. They cast
them as alternate to be used if those other ones were thrown out and that was
how a lot of the electors frame it and some of them I actually believe that
they believed that's what was going on but the idea was to have this
documentation so something else could happen. Those allegations have not
really been made yet but I believe they they probably will be. I hope that clears
that up. Note I told you some people would be able to tell and the message is
the note is from the team who puts the questions together. The message is not to
be a downer but lately in some of your videos it looks like talking about the
news is painful for you, don't forget to take a break. No, that's actual pain. That's physical
pain. I have an injury that is acting up. I have been scanned and poked and prodded a whole lot
lately. Sometimes if I am grimacing, that may not necessarily be from the news. It may be literal
pain. We are hoping to get that taken care of soon. What are your future plans
with the channel? Would you entertain a buyout from a larger network? The plans
for the channel, the main channel, pretty much stay the way it is. Eventually I
would like to get back down to only doing three videos a day at some point.
that would be nice. The second channel, eventually I would like to be on the
road with that, which I'm hoping will happen a little bit this summer. We do
have some news that's coming, but lately I have taken to the idea of
not really announcing anything ahead of time, because any time I do something
happens and throws it off schedule. Would you entertain a buyout from a larger
not work now. I am very happy with what I do and I wouldn't want it to change. We
have other stuff that we would like to do as things move on but we're... my key
thing right now is I want to put out the content that people have
have come to rely on. I want to stick with that as much as I can. I feel like there's
a responsibility there. And then the other stuff, the expansions and all of that, that
takes a backseat to putting out what built the channel.
Do you have a comment on the spat between Nance and Kelt?
If you don't know Big Red Kelt and Malcolm Nance, there was a disagreement that occurred.
No, I don't have a comment on it.
They are both grown men.
That's between them.
And I think people are misreading that, just remember that people in professions like that,
they have opinions, they're not afraid to express them even if they disagree.
And I would imagine, like I don't actually know what their relationship was before that,
so if they were friends, they'll be friends again afterward.
If they weren't, they may be conference because of it.
I wouldn't read too much into it.
I mean, keep in mind, I've only seen the one tweet though.
If something more happened, I don't know what I'm talking about then.
And for those that have no idea what I'm talking about, two people who provide commentary on
military intelligence and foreign policy stuff.
They have a disagreement as far as the handling of captives.
I think that sums it up nicely.
Why do some captives get called hostages and some get called prisoners?
Is it a civilian versus military distinction?
No, it's pirates and emperors.
A non-state actor does not get prisoners.
They don't take prisoners.
That's where that distinction is.
And people can,
there's a lot of commentary about this right now.
And I know that it's for PR purposes and I get that.
And it's coming from both sides on this.
I think it's important to acknowledge that the people who are outside of those trying to influence the PR, the
terminology isn't going to matter to them.  I see this as a very big wasted argument. People are going to make an
equivalency or they're not.  not. I don't think the language here it matters that much. That's why I don't
actually use either term. I don't think that it's it's going to shape people's
opinions very much on this. Okay and that looks that looks like it.
Okay, so that's everything.
I hope it got a little bit more context, a little bit more information, and having the
right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}