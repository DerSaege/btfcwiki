---
title: Let's talk about Wisconsin, electors, and a settlement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=11rTjY-sol8) |
| Published | 2023/12/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wisconsin is in the spotlight due to a civil suit resolution involving fake electors.
- The settlement requires the fake electors to admit that Biden won and their actions aimed to overturn his victory.
- This admission is significant as it's the first acknowledgment in the elector controversy.
- Wisconsin does not have an official criminal investigation, relying on the federal government for this aspect.
- Chief Broward's involvement with state investigators raises questions about potential criminal proceedings.
- The conclusion of the civil suit adds to the growing evidence against attempts to deny Biden's win.
- This situation complicates any defense the former president may try to mount.
- The civil side of the issue has concluded, leaving room for potential further actions, whether probes or undisclosed criminal investigations.

### Quotes

- "This is going to make it very hard for the former president to mount a defense."
- "Wisconsin does not have an official criminal investigation."
- "It's just a thought, y'all have a good day."

### Oneliner

Wisconsin faces a significant resolution with fake electors admitting Biden's win, complicating potential criminal investigations and impacting future dynamics.

### Audience

Wisconsin residents, legal observers.

### On-the-ground actions from transcript

- Stay informed about the developments regarding the resolution and potential investigations (implied).

### Whats missing in summary

Insights on the potential implications of these developments on future political dynamics in Wisconsin.

### Tags

#Wisconsin #Electors #CivilSuit #Biden #Trump


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we are going to talk about Wisconsin
and the resolution to a proceeding,
what it means, how it is unique,
and what it means for Wisconsin in general
as time kind of moves forward
because it alters the dynamics a little bit in the state.
in the state. Okay, so if you have missed the news, 10 of the fake electors in
Wisconsin have settled a civil suit. That suit, it, the settlement to it requires
that they admit that Biden won and that their actions were part of an effort to
overturn Biden's victory. They also have to withdraw their filings. They agreed
not to serve as electors in 2024 or in any election in which Trump was a
candidate. Okay, so how is this significant? To my knowledge, this is the
first time that anybody involved in the elector thing has acknowledged that
Biden won and that their actions were a part of trying to deny him his victory.
That's kind of a big deal. Now one of the things that is certainly going to come
especially with some of the news about Kenneth Cheesebrough, is that this is
civil. Wisconsin officially does not have a criminal investigation proceeding.
They don't have one officially. The Attorney General in the state has
indicated that they plan on relying on the federal government to sort out the
criminal aspects to it. At the same time, there is the reporting that says Chief
Broward is talking to state investigators there, which is odd. If there's not a
criminal proceeding, what's going on? It could just be a probe. Remember, there are
investigations that occur that aren't geared toward ending with criminal
prosecution and that may be what's going on there or the AG could
absolutely have a criminal investigation going on and just not want to tell
anybody. That's also a possibility but at this point we don't know. What we do
know is that the slate of alternate electors there, the civil suit has come
the conclusion, they have agreed that they were wrong, basically, and that Biden won.
All of this is, like a lot of the other little building blocks we've been seeing lately,
where things are coming to conclusions, this is going to make it very hard for the former
president to mount a defense when there are so many people that are saying, yeah, I was
actually part of this and, no, I mean, we were kind of trying to stop Biden's victory.
It's going to be incredibly difficult to overcome all of that.
So that's where that sits.
The civil side of this has closed out now, so anything else that occurs is going to be
part of something else, which maybe it's just a probe, maybe it's a criminal investigation
they don't want to tell us about, but we'll have to wait and see.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}