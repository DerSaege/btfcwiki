---
title: Let's talk about Dems going after Wall Street on housing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IPgbA8WJXok) |
| Published | 2023/12/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the push by the Democratic Party to increase housing affordability by restricting certain entities from purchasing single-family homes.
- Details two bills in Congress, one in the House and one in the Senate, which have different approaches to addressing the issue.
- Describes the Senate bill as more direct, prohibiting hedge funds and similar entities with over 50 million in assets from buying single-family homes and giving them 10 years to sell off existing ones.
- Points out the issue of limited housing supply for normal people and how investors buying single-family homes can drive up prices, leading to a decrease in affordable housing.
- Expresses hope for the Senate bill's potential impact on housing affordability but acknowledges the expected pushback, especially from entities vested in maintaining the current situation.
- Mentions challenges in getting the bill past the Republican Party due to powerful entities opposing it.
- Notes that while some Republicans have shown concern for affordable housing, it might not be enough to ensure the bill's passage.
- Indicates that the bill might have a better chance in the House but suggests waiting to see how the situation unfolds.
- Concludes by mentioning that the issue is actively being addressed and encourages consideration of the proposed solutions.

### Quotes

- "They can't buy any single-family homes. Period. Full stop."
- "There is a lot of demand, so the value, the price is going to go up."
- "There are people trying to alleviate the issue."

### Oneliner

Beau explains Democratic efforts to increase housing affordability by restricting certain entities from purchasing single-family homes, with two bills in Congress proposing different approaches and facing potential challenges from powerful interests.

### Audience

Legislative watchers

### On-the-ground actions from transcript

- Advocate for affordable housing policies by contacting your representatives (implied).
- Stay informed about housing legislation and its potential impact on your community (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the housing affordability issue, including the impact of investor purchases on housing prices and the potential challenges faced by proposed legislation.

### Tags

#HousingAffordability #DemocraticParty #Legislation #SingleFamilyHomes #Investors


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today we are going to talk about single-family homes,
who can buy them, and how the Democratic Party
is engaging in a push to increase housing affordability
by kind of restricting certain people or entities
from purchasing single-family homes.
Okay, so there are two, two bills in Congress, one in the House, one in the Senate.
That's not abnormal, normally that's how this kind of thing works, except these aren't
the same bill.
They're not paired bills like normally happen, they do two different things.
The Senate bill basically says that hedge funds and hedge fund, the definition of hedge
fund includes far more than hedge funds. It seems like any investment entity that
has more than 50 million in assets is considered a hedge fund for these
purposes. Yeah, they can't buy any single-family homes. Period. Full stop.
And the ones that they have, well they have 10 years to sell them off. Out of
Of the two, that is definitely the most direct and the one that will impact housing affordability
the quickest.
The other involves allowing them to have X number and then at this point, there's a fee
assessed, it's a lot more convoluted.
The Senate version, I don't want to say the Senate version, the Senate bill is definitely
more direct.
So right now, one of the issues is obviously there isn't a large amount of supply when
it comes to housing that normal people can afford.
To make that worse, a lot of investors have looked at this and said, hey, there's not
a lot of supply here.
There is a lot of demand, so the value, the price is going to go up.
should buy them. And when they do that, well, it drives the price up, which kind
of creates a self-reinforcing cycle where the price keeps going up, so the
the supply keeps shrinking. The amount of affordable housing keeps going
down. This looks good to me as far as a plan, the Senate version, the
Senate bill. I'm sure that this is going to get a lot of pushback, so much so that
I am hopeful that something like this would make it through, but I don't
know. They're gonna have a hard time getting this past the Republican Party.
There are a lot of entities with a whole lot of money that have a vested
interest in maintaining the situation as it is. When you have that rare moment
when politicians are actively going after entities with more than 50 million
dollars in assets. There's going to be a lot of money thrown at stopping because
they have a lot of money. It's definitely something to watch, but I wouldn't get
your hopes up just yet. There are a few Republicans that have
expressed concern when it comes to affordable housing, but I don't know
that it's enough. I don't know that it's gonna get the numbers needed to get this
through. In the house it stands a better chance something like this getting
through, but we'll have to wait and see how it plays out. Either way, it's on the
table. It's something that's actively in play for once now. There are people
trying to alleviate the issue.
Anyway, it's just a thought.
And you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}