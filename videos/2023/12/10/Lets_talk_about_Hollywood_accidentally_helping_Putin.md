---
title: Let's talk about Hollywood accidentally helping Putin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=B1kM80RWfhM) |
| Published | 2023/12/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Actors on a celebrity messaging platform were unknowingly used by Russian intelligence for international intrigue.
- The platform allows fans to receive personalized messages from their favorite celebrities.
- Messages are typically used for birthday wishes or encouragement, even for seeking help.
- Russian intelligence operatives manipulated actors to send messages asking Vladimir to seek help, disguised as messages to Zelensky.
- The actors, including some well-known names, were unaware of the true purpose of their messages.
- The incident sheds light on the need to be cautious about the information consumed, given the rise of influence operations on various platforms.
- A video portraying celebrities asking Zelensky for help received millions of views, showcasing the effectiveness of such operations.
- Another tactic involves attributing fake quotes about sensitive topics to celebrities like Taylor Swift.
- Beau warns about the potential spread of these tactics from international to domestic politics, especially during elections.
- The sophistication of these influence operations may escalate in domestic political scenarios, potentially impacting future elections.

### Quotes

- "You have to be very careful what you're viewing and how much stock you put in it."
- "You can't believe everything you read on the internet."
- "Surely eventually you will see this level of sophistication applied to domestic politics."
- "It's just something to be aware of."
- "Y'all have a good day."

### Oneliner

Actors unwittingly manipulated by Russian intel on a celebrity messaging platform raise awareness of influence operations' dangers, extending from international to domestic politics.

### Audience

Online consumers

### On-the-ground actions from transcript

- Be cautious about the information consumed (implied)
- Stay vigilant against manipulated content on various platforms (implied)
- Spread awareness about influence operations and misinformation tactics (implied)

### Whats missing in summary

Exploration of the potential long-term impacts and countermeasures against escalating influence operations in politics.

### Tags

#InfluenceOperations #Celebrities #RussianIntelligence #DomesticPolitics #Misinformation


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Russia and Hollywood
and how a group of actors unknowingly got swept up
in a little bit of international intrigue
when they were using a very common platform,
a platform that a lot of celebrities use.
The purpose of the platform is to allow people
to get personalized messages from celebrities.
Normally, these are used to wish people happy birthday
or something along those lines.
So the person receiving it gets the message
from their favorite character on their favorite show or whatever.
Sometimes, they can be used to encourage a person to seek help.
if they, I don't know, hypothetically speaking,
had a substance issue.
So it appears that people linked to Russian intelligence
used Cameo, used that platform, to reach out to a bunch of actors
and get them to record messages asking Vladimir to get help.
Of course, these were packaged and made to appear as though they were asking Zelensky
to get help with a substance issue.
The actors, of course, had no knowledge that that's what they were participating in.
So much so that I'm not actually going to name them, but I mean, yeah, some of them
are some pretty big names.
They're definitely from shows and movies that you would recognize.
Some people might even find those movies precious, but the undertone here, and something that
we all need to be aware of when we're consuming information, is that because of the availability
platforms like this and the different ways in which influence operations can be
ran, you have to be very careful what you're viewing and how much stock you
put in it. This video that was created to make it look like all of
these celebrities were asking Zelensky to get help, it was viewed millions of
times. Another common one that they appear to be using is taking an image of
a well-known celebrity, I think they did this with Taylor Swift, and then putting
a quote next to it about the war in Ukraine. Of course the celebrity never
said anything like that. It's like my favorite quote by Abraham Lincoln, you
can't believe everything you read on the internet. But we have to be aware of it
because you're going to see stuff like this start showing up in domestic
politics as well. Not just in international influence operations but
surely eventually you will see this level of sophistication applied to
domestic politics, and dirty tricks they get used during elections. I would be
surprised if we don't see some of this this cycle in 2024. So it's just something
to be aware of. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}