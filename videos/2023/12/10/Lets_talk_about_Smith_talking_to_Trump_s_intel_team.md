---
title: Let's talk about Smith talking to Trump's intel team....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NpIstHRZMIM) |
| Published | 2023/12/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump suggested a foreign government changed votes during the 2020 election, leading to the possibility of classified material entering the case.
- Trump's team talked to various intelligence and security officials to find evidence of vote flipping, but they all denied it, undercutting Trump's claim.
- Smith's team anticipated and investigated Trump's claims thoroughly, even talking to the National Guard about timelines.
- The real story is Smith's team being prepared for Trump's claims, showing readiness and strategic planning.
- The idea of a foreign power altering votes to make Trump lose seems illogical, especially considering his foreign policy failures.
- Smith's team being proactive and ready for challenges is more intriguing than Trump's claim being refuted by his own team.

### Quotes

- "Trump suggested a foreign government changed votes during the 2020 election."
- "Smith's team investigated Trump's claims thoroughly."
- "The real story is Smith's team being prepared for Trump's claims."
- "A foreign power altering votes to make Trump lose seems illogical."
- "Smith's team being proactive is more intriguing than Trump's claim being refuted."

### Oneliner

Trump suggested a foreign government changed votes during the 2020 election, but Smith's team was prepared and investigated thoroughly, showing readiness and strategic planning.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Contact political representatives to advocate for transparency and accountability in election challenges (suggested)
- Join organizations working on election integrity and security (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's claims, Smith's team's preparedness, and the illogicality of a foreign power altering votes.

### Tags

#Trump #Election2020 #Security #Integrity #Preparedness


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and Smith and DC
and a list of people and a surprising development
that occurred on Saturday.
Okay, so to sum all of this up
and kind of set the scene for this information
because the actual storyline here isn't really important.
getting to something else, but the short version is Trump was kind of suggesting that perhaps
a foreign government had changed votes during the 2020 election. Now, if this suggestion was
believed by the court, or he could demonstrate that he needed access to more information,
a bunch of classified material might enter the case.
Now obviously some people have assigned like a nefarious motive to this.
Like he wants this classified material for some reason.
And I get it, you have to consider that given, you know, the other charges.
But I think it's just about creating a delay.
You know, classified material moves slower.
Okay, so that's what's going on.
And obviously Smith's team is opposed to this.
On Saturday, they put in a filing.
And in it, they revealed a couple of things.
First, they talked to Trump's people,
Trump's intelligence team.
They talked to the former DNI,
that's the Director of National Intelligence,
Former Acting Secretary of DHS
Former Acting Deputy Secretary of DHS
former CISA Director
former Acting CISA Director
former CISA – senior Cyber Council
former national security advisor
former Deputy National Security Advisor
former Chief of Staff to the national security coun formed
ex. Chairman of the Election Assistance Commission
the Presidential Intelligence Briefer
ex. Secretary of Defence
former DOJ leadership and they asked them for any evidence that like a vote
was flipped in a machine and basically Trump's entire intelligence team was
like yeah no that didn't happen and gave them no evidence that they just flat
out this didn't occur. I mean yeah that undercuts Trump's claim because if none
of these people suggested that that occurred you have to wonder where he got
it. Maybe he just made it up you know. And while all of this is interesting
there's something that might be more important. Smith's team had talked to
these people. This is kind of an off-the-wall claim, not something you
would actually expect to show up in this proceeding, but they were ready for it.
That's interesting. It shows exactly how far they investigated Trump's rhetoric
and his claims and how many people they talked to to verify what he was telling
the public. In the same filing it certainly seems as though they also have
already talked to the National Guard about the timeline when it comes to the
And I feel like there's going to be a surprise there.
The real story here, I mean, sure, yes, it is noteworthy that Smith's team used Trump's
people to undercut Trump's claim, but to me the real story is that Smith's team had
anticipated this or were at least ready for it. They had prepared in some way.
They knew where that information was at and apparently already had it. I feel
like somebody who people like to say was always playing 4D chess is a few moves
behind. It's also just offhand, it is definitely worth remembering that the
idea that a foreign intelligence service would try to alter votes to make Trump
lose? I mean that's that's a stretch in and of itself. If they were going to to
alter votes somehow through some kind of magic they would probably want him to
win given the fact that he was an abject failure when it came to foreign policy
which is what they would be concerned about. Like the whole basis for this
doesn't make any sense. The fact that it's undercut by his own people, that's
interesting. But to me, it is far more interesting that Smith's team was ready
for this. During that period where everybody was saying, you know, what are
they doing? What are they doing? What's taking so long? It certainly looks now
like they were going through every possible response and getting ready for
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}