---
title: Let's talk about Kenneth Chesebro nationwide....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2HhW2D5jh7c) |
| Published | 2023/12/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a question about Kenneth Chesbro and his cooperation in different states.
- Disputing between Georgia and Michigan versus Georgia and Wisconsin cooperation.
- Chesbro reportedly cooperating in Georgia, Wisconsin, Michigan, Nevada, and Arizona.
- Speculation about Chesbro being an unindicted co-conspirator in a federal case.
- Mentioning the separate evidence and witnesses being gathered for each state case.
- Suggesting the possibility of these state cases contributing to a federal case in the future.
- Implying a potential follow-on federal case involving others besides Trump.
- Predicting the eventual acknowledgment by the Republican Party of these events.
- Stressing the inevitability of closure on these cases regardless of delay.
- Concluding that Chesbro is cooperating based on reported evidence.

### Quotes

- "It seems incredibly unlikely that none of these cases move to the point of closure with a whole bunch of national attention."
- "This is pretty devastating to Trump's case overall."
- "It's worth remembering that there may be other people doing the same thing, just not as high-profile."
- "At some point, the Republican Party is going to have to deal with it."
- "There will be information that shows up that is damaging to the former president."

### Oneliner

Beau delves into Kenneth Chesbro's cooperation in various states and hints at potential repercussions for Trump's case and the Republican Party's future.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Contact state investigators for updates on the cases (implied)
- Monitor media coverage for developments in the investigations (implied)
- Stay informed about the legal proceedings and potential implications (implied)

### Whats missing in summary

Detailed analysis and context on Kenneth Chesbro's role and potential impact on ongoing investigations and future legal proceedings.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are gonna talk about Kenneth Chesbro.
And we're gonna answer a question that has come in
and we're gonna talk about how I realized
I need a where in the USA is Carmen San Diego shirt.
Okay, so the message that came in was basically,
hey, settle an argument.
My friend and I are going back and forth
talking about where Chesbro is cooperating. I read, and there's a link
to the article, that he's cooperating in Georgia and Michigan. My friend says that
he's cooperating in Georgia and Wisconsin. Yes, based on the reporting,
Based on the reporting, he has decided that his best route is to talk to
everybody. The reporting suggests that Chief Sparrow is cooperating in Georgia.
That's very well known. In Wisconsin, reportedly, there is an investigation
occurring that has not been publicly acknowledged yet, and there is some light
reporting saying he's cooperating with that. In Michigan, reportedly met with state investigators.
In Nevada, met with grand jury. In Arizona, I believe, plans on sitting for an interview
with state investigators there. That's the reporting. It is also worth noting that on top
of all of this, it is widely believed that he is one of the co-conspirators, the
unindicted co-conspirators in the federal case. It would stand to reason if you are
doing a multi-state tour that you would probably cooperate in the federal case
as well, even though that has not been, hasn't been reported on and we
haven't seen any indication of that. One thing to keep in mind about all of this
is that each one of these state cases is being built with its own evidence, its
own witnesses, and its own system of gathering information. And that all of
it may end up being used in a federal case down the road. I don't necessarily
think it'll be used in Trump's case, but there are strong indications that there
may be a follow-on federal case for other people who are not in the the
Trump DC case. It's worth remembering and keeping in the back of your mind that
I feel like there are probably other people who are doing the same thing
that just aren't as high-profile or getting the media attention. Again, it
comes back to the idea that at some point the Republican Party is going to
to acknowledge what occurred. It seems incredibly unlikely that none of these
cases move to the point of closure with a whole bunch of national attention.
It's something that the Republican Party is going to have to deal
with. It doesn't matter how long they put it off, at some point it's going to occur.
So, the answer to the question is yes, based on the reporting in all three of those states,
and then two more that aren't in your conversation.
This is pretty devastating to Trump's case overall, because as this conversation occurs,
multiple conversations involving this potential witness. There will be
information that shows up that is damaging to the former president. Anyway,
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}