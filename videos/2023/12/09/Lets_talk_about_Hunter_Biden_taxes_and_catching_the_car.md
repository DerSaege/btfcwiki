---
title: Let's talk about Hunter Biden, taxes, and catching the car....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AhfqhLD42NQ) |
| Published | 2023/12/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hunter Biden has been charged with tax-related allegations.
- The Republican Party is celebrating this, but there is no link to President Biden in the allegations.
- Over the years, the Republican Party has embraced wild talking points that their base bought into.
- The base often doesn't understand that the value lies in the talking point, not necessarily in taking action on it.
- Hunter Biden was more valuable to the Republican Party when he wasn't charged.
- The idea of a two-tier system falls apart with Hunter Biden being charged.
- This development might have cost the Republican Party one of their major talking points for the 2024 election.
- The tax charges against Hunter Biden are serious and could have political implications.
- There might be an attempt for a plea deal in this case.
- The Republican Party's political considerations regarding this situation may not have been thoroughly thought through.

### Quotes

- "The base demands you act on your wild talking points."
- "The big claim of the former president, is that, oh, it's all political, it's a two-tier system."
- "Tax charges, like they're a big deal."
- "It seems problematic to suggest that the Biden DOJ is there to protect Biden."
- "Y'all have a good day."

### Oneliner

Hunter Biden's tax charges shake up the Republican Party's narrative, revealing the value of wild talking points and potential political consequences.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Analyze the political implications of Hunter Biden's tax charges (implied).

### Whats missing in summary

Deeper analysis of the potential impact of Hunter Biden's tax charges on the political landscape. 

### Tags

#HunterBiden #RepublicanParty #TaxCharges #PoliticalImplications #TwoTierSystem


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about Hunter Biden and the Republican
party may be catching the car again.
The current mood in the Republican party is one thing.
The long-term impacts for their most prominent member may not actually be
good. But, okay, so let's run through it. If you missed the news, Hunter Biden has been charged.
It's tax stuff. It is tax stuff. It looks like there is a series of allegations about
lying on tax forms and not paying. Quick glance at the allegations and some of the evidence that
the prosecution has, yeah, he's probably in trouble. Some of those
deductions are, I mean, even the most creative accountant would have
questions about some of that. So as far as that goes, this case will probably
proceed. The Republican Party is currently celebrating this for whatever
reason. It's worth noting that nowhere in any of the allegations is there a
link to President Biden. Surprise. But they're celebrating it. One of the things
I've realized recently about the Republican Party is that for years and
years and years they embrace incredibly, incredibly outlandish talking points.
just wild talking points and politicians said it over and over again and the the
base bought into it. The problem with this is that over time the base demands
you act on your wild talking points. A lot of times the base doesn't understand
that the value is the talking point, not actually doing something about it. For the Republican
Party, Hunter Biden was far more valuable when he wasn't charged. The big claim of the former
president, is that, oh, it's all political, it's a two-tier system.
Look at this shady stuff that Hunter Biden did, and he didn't get charged.
They're only charging me because I'm poor, pitiful Trump and, you know, I never
did anything wrong.
That kind of falls apart once Hunter Biden is charged.
The claim of the two-tier system disappears when that happens.
In securing a victory, I guess, in the tax charges, it seems as though the Republican
Party might have lost one of their most important talking points moving into the 2024 election.
This occurred under the Biden DOJ.
It seems problematic to suggest that the Biden DOJ is there to protect Biden when they hit
him with some pretty serious charges.
And if you don't know, sometimes tax charges, like they're a big deal.
And a few of these are.
So we'll have to see how this plays out politically, looking at what the allegations are as far
as the case itself.
Yeah I'm going to guess that there might be an attempt for a plea in this one.
But the political considerations, they may not have been thought all the way through
by the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}