---
title: Let's talk about LA, Vegas, Biden, and trains....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=t9t2h7rRyUg) |
| Published | 2023/12/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration announced a $8.2 billion investment in 10 passenger rail projects in the United States, the largest since Amtrak's creation in 1971.
- The goal of this investment is not just job creation and infrastructure update but also to make Americans more comfortable with passenger rail travel.
- The flagship project among these is a high-speed rail from Vegas to LA, reaching speeds of 220 miles per hour and reducing travel time to about two hours by 2028.
- This initiative is part of Biden's efforts to boost the economy, create jobs, and modernize American infrastructure.
- The focus on the LA to Vegas route is strategic because it's a frequently taken trip, allowing people to become more accustomed to using passenger rail.
- Biden's visit to Vegas aimed to promote these infrastructure projects and the economic benefits they bring, although overshadowed by local events.
- Passenger rail could be a sustainable way to transport people in the US, especially as technology advances and interest in rail travel grows.
- Increasing familiarity with passenger rail may lead to more sustainable projects that utilize technologies common in other countries.

### Quotes

- "The goal is to get the American people more comfortable with rail moving passengers."
- "Passenger rail is something the US probably needs to become super enthusiastic about."
- "This is an attempt to alter that dynamic."

### Oneliner

Biden's $8.2 billion investment in passenger rail aims to boost the economy, create jobs, and familiarize Americans with rail travel for a sustainable future.

### Audience

Americans

### On-the-ground actions from transcript

- Support and advocate for the development of passenger rail projects in your local area (implied).
- Use passenger rail services whenever possible to familiarize yourself with this mode of transportation and support its growth (implied).

### Whats missing in summary

The full transcript provides more insight into the importance of passenger rail for sustainable transportation and economic growth.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Biden and trains
and infrastructure and Vegas, baby Vegas,
and an announcement that is probably being overshadowed
with all of the other news.
Okay, so if you missed it,
the Biden administration has announced
that the federal government will be investing
massive amount of money into 10 passenger rail projects within the
United States. By a massive amount of money we are talking about 8.2 billion
dollars. This is the largest federal investment in Amtrak since Amtrak
became a thing back in 1971. The goal here is not just job creation, not just
updating infrastructure, the goal is, I would imagine, to get the American people
more comfortable with rail moving passengers. We use it all the time for
everything else, but there's still a reluctance when it comes to passenger
rail. It is something that is used all over the world, but in the United States
we are definitely falling behind. One of the big highlights, probably the
flagship project out of all of these is high speed rail from Vegas to LA. By high
speed, I want to say it's going to reach 220 miles an hour. There's three billion
dollars for the line from Vegas to LA. That trip should take around two hours.
Sadly, you will no longer go through that country though. It's projected to be
done by 2028. So this is more of the same when it comes to the Biden administration and how they
are trying to stimulate the economy and improve American infrastructure. This creates jobs and it
provides a taste. I think the reason that this was chosen is that this is a trip that is taken
often. The LA to Vegas trip, that is something that occurs frequently and
this provides people a way to do it and allows them to become more familiar with
passenger rail. When people become more familiar with something, they're more
likely to use it. When they're more likely to use it, they are more likely to
who want it in other locations.
So that's my guess.
And I feel like the reason he went to Vegas
was probably to promote this and talk
about the infrastructure jobs that are going to be created
and how it's going to help the economy
and how it's going to help the United States catch up
when it comes to rail and all of this stuff.
But local events in Vegas probably kind of took over.
So it would seem in bad taste to show up and be
incredibly enthusiastic about a rail project with everything
that's going on.
So it altered his tone, would be my guess.
passenger rail is something the US probably needs to become you know super
enthusiastic about. We are a huge country and this is a way that is much more
sustainable to move people and as the technology improves and people become
interested in it and start to use it it's more likely that these projects
become even more sustainable and they use technologies that are in use in
other countries but we don't use them here because well realistically not that
many people ride the rails. I feel like this is an attempt to alter that
dynamic. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}