---
title: Let's talk about Hunter Biden running circles around the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qnCPdYokZ78) |
| Published | 2023/12/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hunter Biden successfully outplayed the Republican Party politically by requesting a public hearing.
- Republicans are investigating Hunter Biden without evidence linking his activities to his father, the President.
- Hunter Biden wanted the hearing public to avoid cherry-picked information being released.
- Hunter Biden stated that his father was not involved in any of his business dealings.
- Hunter Biden's public statement pushed the Republican Party into potentially needing to make everything public.
- The House formalized an impeachment inquiry against the President on the same day, overshadowed by Hunter Biden's actions.
- Coverage of the impeachment inquiry included phrases like "debunked claims" and "lack of evidence."
- Hunter Biden's team played the situation well politically, making the Republican Party look poorly thought out.
- The focus on political stunts by all involved overshadows the real news and developments.
- The Republican Party may inadvertently make Hunter Biden a more prominent figure by pursuing their inquiries.

### Quotes

- "Let me state as clearly as I can, my father was not involved in any of my business."
- "He certainly seems to have ran circles around him today."
- "This is all a political stunt from everybody involved."

### Oneliner

Hunter Biden outplays Republicans in a political maneuver, pushing for a public hearing and diverting attention from the formalized impeachment inquiry.

### Audience

Political observers

### On-the-ground actions from transcript

- Follow political developments and stay informed (exemplified)
- Engage in political discourse and critical thinking (exemplified)

### Whats missing in summary

Insight into the potential long-term implications of Hunter Biden's political maneuver and the overshadowing of significant political events by strategic actions.

### Tags

#HunterBiden #RepublicanParty #ImpeachmentInquiry #PoliticalStrategy #PublicHearing


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Hunter Biden and how Hunter Biden successfully
outplayed the Republican party politically today.
And it occurred in such a fashion that I'm not sure people are actually going to
acknowledge it because the second part of the formula, well, it didn't really
a lot of headlines, at least not ones that the Republican Party would have wanted. Okay, so if
you missed it, the Republican Party is investigating the President's son, Hunter Biden. There were a
whole bunch of claims about his behavior and how it might be linked to the President.
Hunter Biden is definitely not an angel. That being said, so far there has been no evidence presented
that any of his activities can be linked to his dad. They, the Republican Party, issued a subpoena
for Hunter Biden. Hunter Biden wanted it public. Why would he want it public? Because the Republican
Party has been cherry-picking information and releasing it to get Twitter likes.
So he wanted the hearing to be public, okay, fair enough.
He showed up today, which is when he was supposed to go into this closed-door, you know, backroom
deposition, and he said, I am here to testify at a public hearing today to answer any of
the committee's legitimate questions. Republicans do not want an open process
where Americans can see their tactics, expose their baseless inquiry, or hear
what I have to say. What are they afraid of? I am here. He went on to say, let me
state as clearly as I can, my father was not involved in any of my business, not
as a practicing lawyer, not as a board member of Barisma, not in my
partnership with a Chinese private businessman, not in my investments at home nor abroad, and
certainly not as an artist." Okay, so he showed up when he was supposed to testify and basically was
like, let's do it in public, and then left.
The Republican party was obviously upset by this.
Representative Jim Jordan suggested charging Hunter Biden with contempt of Congress.
If you're not familiar with, with Representative Jordan,
And he might be best known for defying a congressional subpoena related to January 6th.
So at the end of this, this little public statement by Hunter Biden, it certainly appears to have
successfully pushed the Republican Party into a position where they will
probably need to make everything public.
Or they run the risk of turning Hunter Biden into a political force because it
will be pretty easy if they were to go down the contempt road to make him out
to be somebody who's being politically persecuted or whatever.
And we've seen this effect a lot with Trump, where people rally behind him.
It is primarily a right-wing thing, but don't think that the Democratic Party is immune
to that.
It would probably happen if they went that route.
But all of this is the sideshow.
The real news is what it kind of overshadowed, which is today the House formalized an impeachment
inquiry against the president.
But because of this public statement by Hunter Biden, it's not really getting much coverage.
And the coverage it is, even in pretty centrist outlets, the headlines include phrases like
debunked claims, lack of evidence when talking about the impeachment inquiry.
For somebody who the Republican Party likes to cast as a whole bunch of disparaging
terms for people that have substance issues and stuff like that. He certainly seems to have ran
circles around him today. At the end of this, you have a stage set where if the Republican Party
pursues it, they may actually make Hunter Biden a more prominent figure, and it undermines
their impeachment inquiry.
This was not well thought out on the part of the Republican party, and Hunter Biden's
team played this really well politically.
Now we're all talking about the political stuff because let's be honest, this is all
a political stunt.
Like across the board, this is a political stunt from everybody involved.
So it's an interesting development.
We'll see how it plays out over the next week now that the impeachment inquiry is, quote,
formalized.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}