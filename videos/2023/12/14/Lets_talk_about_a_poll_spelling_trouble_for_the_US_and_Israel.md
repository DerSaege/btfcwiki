---
title: Let's talk about a poll spelling trouble for the US and Israel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ggj-Gf60akA) |
| Published | 2023/12/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains polling conducted in the West Bank and central and southern Gaza, revealing surprising sentiments.
- Describes the goal behind provocative operations like the one on October 7th: to provoke an overreaction and security clampdown.
- Notes that such tactics are used worldwide to generate outrage and international sympathy.
- Points out that overreactions help smaller forces, like Hamas, gain new supporters.
- Mentions that 60% of respondents want the Palestinian Authority dissolved, and support for Hamas has tripled in the West Bank.
- Suggests that Israeli and U.S. plans may face challenges due to the increased support for Hamas.
- Indicates that the U.S. plan relied on degrading Hamas to make them combat ineffective for the Palestinian authority to govern Gaza.
- Expresses skepticism about the success of current plans due to the polling results.
- Urges for consideration of alternate strategies as current approaches seem unlikely to succeed.
- Advocates for a negotiated resolution to conflicts like these and warns against relying solely on overwhelming force.

### Quotes

- "It's just a thought. Y'all have a good day."
- "Even if this polling is off, these types of conflicts are nothing more than PR campaigns with violence."
- "It is the responsibility of everybody to acknowledge that this ends at a negotiation table."

### Oneliner

Beau explains polling results from the West Bank and Gaza, revealing challenges for U.S. and Israeli plans against Hamas, advocating for negotiated resolutions in conflicts.

### Audience

Activists, policymakers, diplomats

### On-the-ground actions from transcript

- Advocate for peaceful negotiations to end conflicts (implied)
- Support aid efforts in Gaza (implied)
- Work towards de-escalation and conflict resolution in affected regions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of polling results in conflict-affected regions, outlining the challenges faced by current political plans and advocating for peaceful solutions through negotiation.

### Tags

#ConflictResolution #PollingResults #USPolicy #IsraeliPolicy #Hamas


## Transcript
Well, howdy there, internet people.
It's Bobo again.
So today, we are going to talk about some polling.
We're going to talk about some polling that was conducted
and how, even though that polling isn't really surprising,
it's counterintuitive.
Most people think that it's odd, even though it's just kind
of how things normally work.
And we're going to talk about how that polling sets
stage for some major difficulties on the foreign policy scene when it comes to
the future plans of both the United States and of Israel. Right after this
started, and I mean right after my inbox started filling up with the question of
why would they do this? What were they hoping to accomplish? And I said the goal
The goal was to provoke an overreaction when an organization engages in an operation like
the one that occurred on October 7th.
The goal is to provoke an overreaction, to provoke a security clampdown.
The people behind the strategy of the Palestinian forces wanted what you're seeing on your
screens because they know it generates outrage and it turns the bystander into
somebody who is sympathetic, the person who is sympathetic into somebody who is
active. It grows their force and it provokes outrage in the international
community. That's why they want it. That's why operations like this are conducted
And this isn't just a thing dealing with Palestinians, this is a strategy that has
been deployed all over the world.
And that's generally what happens when the overreaction occurs.
It helps the smaller force.
Okay so what's the polling say?
The polling was conducted in the West Bank and in central and southern Gaza.
I'm assuming they didn't conduct polling in northern Gaza for obvious reasons.
The big parts of this that are going to matter, 60% want the Palestinian Authority dissolved.
for Hamas has increased across the board. In the West Bank it has tripled. So what
does this mean? It means that the bystander became sympathetic and the
person who is sympathetic became active. So as far as Israeli plans, it is
incredibly unlikely that they will be able to defeat Hamas because there are
now new supporters. It is incredibly unlikely that they will be able to
degrade Hamas because there are new supporters. For the U.S., the U.S. plan at
this point hinged on a couple of things. First, it hinged on Israel being able to degrade Hamas
to the point where they would be combat ineffective. The U.S. plan didn't actually need for them to be
defeated but needed them combat ineffective for a while because the U.S. plan was for the Palestinian
authority to assert governance over Gaza.
If, as it seems, the current forces within Gaza are not going to be degraded and made
combat ineffective, and the majority of people want the Palestinian authority dissolved,
well that presents some real issues.
And I went over the three things that the U.S. was kind of angling for after that Security
Council vote.
People asked what the Plan B was.
I don't think there is one.
But based on this polling, they had better start coming up with one because it does not
appear that the US plan or the Israeli plan is going to be successful.
This is pretty predictable.
This is how this works.
When you are talking about this kind of irregular conflict, this is what happens.
I will put the video from, I think it published on the 9th and was recorded on the 8th.
I'll put it down below.
At this point, I do not see a way that this ends without another cycle.
I would hope that people file all of this information away.
So the next time the politicians come out and talk about using this overwhelming force,
it's acknowledged that it doesn't work.
It plays well for the cameras, but it doesn't actually solve the problem.
I am not aware of anything other than a full mobilization of the UN and putting, literally
putting thousands of troops in between the two sides for an extended period,
while simultaneously pumping a massive amount of aid into Gaza
that even stands a chance of stopping another cycle now.
This was, I mean you can go back and watch that video,
this was pretty predictable, but I don't know, consider it my optimistic nature, I
was hopeful that something would change. It does not look like it's going to. It is
important to remember that the next time, because rest assured at this point, that next
time seems pretty inevitable, that everybody remembers the mistakes of this
time. These mistakes, this cycle, it has been going on for decades. It doesn't
work. Even if this polling is off, and I know somebody is going to try to suggest
that. Even if it is off by, cut it in half, cut it in half, and support still
doubled. These types of conflicts are nothing more than PR campaigns with
violence. If you cannot grasp that, there is no way to win. In conflicts like this,
it is the responsibility of everybody to acknowledge that this ends at a
negotiation table. And the goal should be to get there as quickly as possible,
possible because it doesn't end any other way it just keeps going anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}