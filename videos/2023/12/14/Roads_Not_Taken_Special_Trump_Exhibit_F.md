---
title: Roads Not Taken Special Trump Exhibit F
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=zRvC4-A_Tak) |
| Published | 2023/12/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides a recap of recent developments in the legal cases involving Trump, including a briefing described by Kenneth Cheesebrough to Michigan officials about fake electors and a ruling in the DC case.
- Trump won a ruling in the DC case putting federal election interference proceedings on hold, but an appeals court agreed to expedite the case with briefs due between December 23rd and January 2nd.
- A federal appeals court ruled that Trump cannot use presidential immunity to dismiss a civil defamation case brought by E. Jean Carroll, set to go to trial in January.
- Smith's team accessed Trump's phone records for the post-election period, potentially revealing damaging information about his phone calls, Twitter use, and locations.
- The New York case involving Trump has ended the testimony phase after 10 weeks, with briefs due by January 5th and a verdict expected by the end of January, likely resulting in Trump paying a sizable fee.
- Legal observers expect Trump to face consequences in the New York case, with the possibility of a significant financial penalty or even the "corporate death penalty."
- In other news, Trump claims Biden will lead the country into a depression, but the Dow Jones has hit an all-time high, with Trump leaving office with fewer jobs than when he started.
- Beau mentions that Chief Spro's testimony and recordings provided may lead to criminal charges against Trump in various jurisdictions.

### Quotes

- "Everything that is going to happen with that, all of the dramatic stuff, it's occurred."
- "The general consensus among legal observers is that Trump is going to end up paying a sizable fee here."
- "Most of Biden's term has been fixing Trump's mistakes."
- "Having the right information will make all the difference."
- "It's worth remembering that we know now that Chief Spro is basically on tour talking to various jurisdictions about what occurred."

### Oneliner

Beau provides updates on legal cases involving Trump, including potential consequences, with Chief Spro's testimony possibly leading to criminal charges, as the legal drama unfolds.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Follow updates on the legal cases involving Trump (suggested)
- Stay informed about the developments in the legal proceedings (suggested)
- Support transparency and accountability in legal matters (implied)

### What's missing in summary

Insights on the potential implications of these legal cases for the political landscape and future accountability measures.

### Tags

#LegalCases #Trump #LegalDrama #Accountability #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to the Roads with Beau.
Today we are doing the Roads Not Taken
Special Trump Exhibit F, I think.
I think we're on F.
Figured we'd do this before I got too close to the holidays
because odds are a whole lot of these proceedings
are just going to slow down or stop,
at least in the public's view, until after the new year.
So I'll give you a recap of where the big developments have
occurred lately, and then kind of run on from there
before we get our little break in.
OK.
So starting off with the big news,
we'll probably end up with a video on this
over on the other channel.
Kenneth Cheesebrough apparently told Michigan prosecutors about a briefing that Trump had
related to the whole fake electors thing.
So he described a briefing in which he took part to Michigan officials.
CNN has reportedly obtained the recordings.
I've listened to bits and pieces of it so far.
The recordings describe how Cheese Bro basically made Trump aware of the electors and what
they were doing and how it was working, and the interesting part was that the real deadline,
to speak, as far as changing the outcome of the elections, was not the date that some
believed at the time, but it was, you guessed it, January 6th.
Yeah.
There's probably going to be a lot more to come out of that.
Okay, speaking of January 6th, we'll take a look at the DC case real quick.
So Judge Chutkin ruled that the federal election interference case that's going on there in
DC would be put on hold pending the outcome of Trump's presidential immunity claims.
This gave the former president a much sought after win, something he desperately needed
in his quest to delay the proceedings until after the election.
So early on that day, it looked pretty good for Trump, I imagine he was pretty happy.
That win was pretty short-lived because just a little while after Chutkin made that ruling,
the appeals court agreed to hear the case on an expedited basis.
Briefs will be filed between December 23rd and January 2nd.
So over the holidays is when they're going to have to have their briefs and the first
date I think is for Trump's first brief.
Then there's one from Smith's team and then Trump gets to reply.
The arguments have not been scheduled yet, but given this timeline, it seems incredibly
unlikely that this is going to grant Trump the delay that he needs to push
it beyond the election if he gets a delay at all. Currently the case is
scheduled to go to trial on March 4th. In an even worse turn of events for the
former president, a federal appeals court ruled that he cannot use presidential
immunity to dismiss a civil defamation case brought by E. Jean Carroll. If this
immunity is so weak it can't protect him from a defamation claim in a civil case,
it's probably not going to help much in a criminal case. The E. Jean Carroll case
I believe is set to go to trial in January.
Now as far as the investigation itself into the election interference stuff, we now know
that Smith's team accessed Trump's phone records for the post-election period.
There's a lot of speculation about an expert witness that was disclosed.
The witness deals with technical phone data.
So the expert will be able to tell the court who was called and when, but more importantly
combined with other records, they would be able to tell who Trump called and then who
they called, which might cause an issue for the former president if the way it looks is
the way it is.
That could be incredibly damaging.
else the witness might be able to show is not just the pattern of calls, but it could
show when Trump was using Twitter and what he was aware of at what times and how he responded,
which would also probably be pretty damaging to the former president.
It could also be used to show location information, which I don't know that that would be damaging,
but it might be enlightening.
It might have a lot to do with some of the other testimony that we have heard.
Now, the other big news is coming out of the New York case right now.
So the Trump case in New York has ended the testimony portion after 10 weeks or so of
testimony.
So now both sides are going to get to write their briefs.
The briefs have to be in by January 5th.
So mations occur on January 11th.
A verdict is expected by the end of January.
So that case is basically done.
Everything that is going to happen with that, all of the dramatic stuff, it's occurred.
We are now at the point where the attorneys are handing in their final briefs and then
the judge is going to make the final determination.
The general consensus among legal observers is that Trump is going to end up paying a
sizable fee here. How much is up for debate, whether or not the, quote,
corporate death penalty gets enacted is also up for debate, but the general
tone is that it did not go well for the former president overall. That's not
incredibly surprising if you were keeping up with the day-to-day on
that. Now in random Trump news, it's apparently very important for people to
know that Trump aced his recent cognitive test. Apparently that's still
something that he thinks is really hard to do. I would like to point out that I
I once passed one after a head injury.
But, okay, other than that, we have Trump claiming
that Biden is going to lead the country
into a 1929 style depression while here in the real world,
the Dow Jones hit an all time high.
It is worth remembering that Trump was the first president
since Hoovervilles were a thing
to leave the office with less jobs than when he started.
I don't know that running on the economy is really going to help Trump on this one.
Most of Biden's term has been fixing Trump's mistakes.
So, that's it for right now, that's the news in the Trump legal world.
It's worth remembering that we know now that Chief Spro is basically on tour talking to
various jurisdictions about what occurred.
is it's probably pretty likely that there are other witnesses doing the same thing.
This is something that might end up bringing Trump into yet another criminal case in yet
another jurisdiction.
The recordings and the testimony that he's reportedly provided is what would be needed
if the prosecution was trying to tie the local events to Trump, which may lead to charges
against Trump.
But that's it.
Everything else right now is already kind of on hold.
There may be more developments before the new year, but it seems unlikely.
I think our biggest surprise would be maybe somebody in the Atlanta case deciding that
it's time for them to cooperate or something along those lines.
I don't think we're going to have any huge surprises until the new year now or any major
developments.
I mean it is Trump world so there could be one tomorrow but that's that's where
we're at so you have a little bit more information a little more context and
and having the right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}