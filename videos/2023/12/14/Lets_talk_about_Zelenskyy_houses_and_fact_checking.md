---
title: Let's talk about Zelenskyy, houses, and fact checking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5gwbcDqTyGQ) |
| Published | 2023/12/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of fact-checking information yourself, especially in light of current stories circulating about Zelensky.
- Responds to a viewer's claim about Zelensky using aid money to buy a yacht, which led to the viewer sending a link to a tweet stating Zelensky bought a mansion in Florida.
- Advises on the initial step of fact-checking by searching for the claim on Google and checking for news outlets reporting on it.
- Suggests waiting for a major outlet to pick up the news if it initially doesn't show up in search results.
- Encourages looking at non-news results and websites dedicated to debunking social media rumors to verify information.
- Guides viewers on using a reverse image search tool like TinEye to verify the authenticity of images being shared.
- Analyzes the edited image from the tweet and proves its falsity through comparison with original images.
- Dissects the fake naturalization certificate image and clarifies that the house in question is not in Vero Beach and not worth $20 million.
- Emphasizes the importance of being a critical consumer of information to avoid being misled by sensationalized claims.
- Commends the viewer for questioning and seeking accurate information, stressing the need to develop skills to discern misinformation.

### Quotes

- "Now download a couple images of the house. Doesn't matter which ones."
- "It's just made up for engagement and clicks."
- "Our biggest sign should have been that it wasn't property records being shown."
- "There is nothing in that tweet that's true."
- "Y'all have a good day."

### Oneliner

Beau explains how to fact-check claims like Zelensky buying a mansion, revealing the importance of independent verification and critical information consumption to combat misinformation.

### Audience

Information consumers

### On-the-ground actions from transcript

- Use reverse image search tools like TinEye to verify images shared online (implied).
- Verify sensational claims by checking multiple sources and using critical thinking skills (implied).
- Develop the ability to discern misinformation and avoid spreading false information for engagement (implied).

### Whats missing in summary

The full transcript provides a detailed guide on fact-checking and verifying information, promoting critical thinking skills and independent verification to combat misinformation effectively.

### Tags

#FactChecking #Misinformation #CriticalThinking #InformationConsumers #Verification


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about information consumption
and you are going to learn
how to fact check something yourself
using a technique I've talked about on the channel before,
but it's been a while, everybody can use a refresher
and it definitely is useful
given the current story about Zelensky buying a house.
And we're also gonna check in
on somebody who sent a message that I read on the channel recently, talking about Zelinski
using money to buy a yacht, and you know, his brother was working at the manufacturing
plant and working overtime, bought the new truck, and I explained that that's really
where the aid money goes.
Okay, well that person definitely saw that video, and they continue to watch, because
they sent this.
I bet you feel pretty dumb.
made a whole video making fun of me for saying Zelensky used aid money to buy a yacht. Well,
he bought a house in Florida. And there's a link. And the link is to a tweet. It's
a tweet. And the tweet says, Zelensky reportedly buys $20 million mansion in Vero Beach, Florida.
And there are some images. There are some images of the home that he reportedly bought.
And for whatever reason, there's an image of a naturalization certificate that has his
information on it including a picture of it. Okay, so if you wanted to know whether
or not this was true, what's the first thing you would do? Go to Google, right? Or
whatever your preferred search engine is. Go to Google, type in Zelensky buys
mansion. If you do that, you're not going to find any news outlets reporting on
this, which would be weird because this would be a huge story. Okay, so that's
That's odd, but maybe the person on Twitter, they're breaking it.
So wait an hour, see if it shows up, see if a major outlet picks it up.
While you're doing that, you could look at results that aren't news and maybe see if
anything shows up there, like on websites dedicated to debunking social media rumors.
That's where you'll find it.
you don't want to have to wait for some website to debunk it for you. You want to
be a good consumer of information, you want to be able to discern this stuff for
yourself. So we're going to go through some techniques so next time you won't
be taken advantage of, you won't be tricked, manipulated, then get angry and
send me another email. Okay, so the first thing I want you to do, that image of the
naturalization certificate, take a screenshot of it, but you don't need the
whole thing. Just crop Zelinsky's photo, okay, save just his photo, cut everything
else out and just get his photo. Then you're gonna go to a website called
10i, T-I-N-E-Y-E. It is a reverse image search. Upload the image. You get a bunch of results,
right? But it looks different. The results from 10i, they have a blue background. Everything else
is the same, but they have a blue background. But it's the same image because the one on the
the certificate? Well it's been edited. They changed the background to make it
look like a passport photo and they made it super grainy to make it look like I
guess a passport photo from the 80s. So if you are looking at that website you
can click compare and it will compare the image that it found online to the
image you uploaded. And even though the backgrounds are different, you can tell
it's the same image. If you want something to compare to make it super
obvious, look at his neckline. He's got a jacket and a shirt on, and the jacket's
kind of off-center, and it hits in the same spot on his neck in both images.
It's the same image. It's just been edited. Okay, so now we know that's fake.
Like that's not real. The only piece of evidence, or one of the pieces of
evidence, I don't know how that certificate is supposed to be evidence
of them buying a house, but whatever, that is not real. It's been edited, it's
manufactured, there's no way that they would use an edited image from online
on a document like that. It's fake. Okay, now download a couple images of the house.
Doesn't matter which ones. Upload them up into the same website, into TenEye. What
do you get? You get a bunch of results from real estate websites. Okay, if you
follow the links, because it shows you where the images are from and it provides
you a link, if you follow that link you will end up on one of these sites and
And the first thing you should notice is that the house isn't in Vero Beach, is it?
Weird.
That's not true either.
The other thing that you would notice is that it's not a $20 million home.
The highest I found was 10.9.
And if you wanted to get a little further into it, you can use property records to find
out who actually owns it.
I'm not going to put the person out there because they didn't ask for their house to
be used in this way.
But the person lives in Atlanta and has owned the house for quite some time.
So what do you have?
You have the text of this image.
Zelensky reportedly buys $20 million mansion in Vero Beach.
The house in the image, not in Vero Beach, not $20 million.
So now you have Zelensky reportedly buys.
And there's an image of Zelensky, but it's been edited and it's like on a fake document.
It's not real.
They made it up.
And hopefully, now that you know how to do this, you won't fall for it anymore.
You'll be harder to trick.
There are people who will spread bad information for clicks, something that is just sensational.
It's worth noting that the websites that debunked this did it a while ago.
It's not a new social media claim that's false.
It's been around.
It just got resurfaced on Twitter because there's currently aid requests going on.
If something is this sensational, check into it, and I mean you did in a way.
You sent me this message, although by the tone of this, I feel like you believed it.
Just saying.
But at the same time, good for you.
I did.
I made a video that straight up, it was kind of poke in front of you.
But I feel like you came back because you knew the information was accurate.
You want accurate information, most people do.
Sometimes you just need the skills to determine whether or not somebody's lying to you.
And that's what's happening here.
There is nothing in that tweet that's true.
The images don't support it.
It's just made up for engagement and clicks.
But now you have those skills, TenEye.
Really useful.
There are other reverse image searches out there.
I like Ten Eye because it will oftentimes pick up an edited photo like that.
Not all of them do that.
It's a pretty good one.
But there are other ones.
Just remember that if something sounds sensational, there might be a reason.
Check into the information.
Our biggest sign should have been that it wasn't property records being shown.
It was just some images of a house and a document, a reported document, that has nothing to do
with buying a house.
They made it up.
Or they were also tripped.
Either way, probably not a place that I
would accept information from without checking.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}