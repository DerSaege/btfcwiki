---
title: Let's talk about the GOP vs MAGA in the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=B2Bdx-7qF3Y) |
| Published | 2023/12/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzes the passing of the defense budget in the House.
- Focuses on how the bill made it through, not its contents.
- Speaker Johnson's contrast with Speaker McCarthy, attributing McCarthy's downfall to his unwillingness to take certain actions.
- The compromise bill passed in the House with a vote of 310 to 118, with majority Republican support.
- Opposition from some Republicans stemmed from what wasn't included in the bill, particularly far-right culture war elements.
- Speaker Johnson employed procedural tactics and Democratic votes to push the bill through, sidelining the far-right faction.
- Johnson's repeated actions suggest a deliberate effort to reduce the influence of the far-right MAGA faction within the Republican Party.
- Removing certain elements from the bill weakened the far-right faction's standing and political capital.
- Johnson's strategy aims to consolidate power and marginalize the performative far-right faction.
- The inclusion of aid for Ukraine in the bill indicates a commitment to support, albeit not the full aid package required.
- Overall, the transcript delves into political maneuvering within the Republican Party and the implications of strategic decision-making.

### Quotes

- "It shows that they don't have any power."
- "It is good for the Democratic Party to get rid of them because it's also where all of the weird ideas come from."
- "Removing that kind of behavior from the military, it's not gonna happen."
- "It consolidates power under Johnson and makes the MAGA faction well weaker."
- "The reduction in support that that faction has now, it's going to make it harder and harder for them to mount a challenge to the Speaker."

### Oneliner

Beau analyzes the strategic political maneuvers behind the defense budget passing in the House, showcasing the sidelining of the far-right faction and Speaker Johnson's consolidation of power.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to express your views on defense budget priorities (implied)
- Support organizations working towards aiding Ukraine (implied)

### Whats missing in summary

Insights into the broader implications of political tactics and factional dynamics within the Republican Party.

### Tags

#DefenseBudget #RepublicanParty #SpeakerJohnson #MAGA #PoliticalStrategy


## Transcript
Well, howdy there internet people, let's vote again.
So today, we are going to talk about the defense budget
being passed in the House specifically.
We're not gonna talk about what's in it.
A whole bunch of people are gonna cover that.
We're gonna cover how it actually made it through
because I think that's far more interesting.
It certainly appears that Speaker Johnson
is willing to do something
that Speaker McCarthy wasn't willing to do.
Just couldn't bring himself to do it.
And in my opinion, that's part of the reason
Speaker McCarthy is no longer Speaker.
Okay, so let's go over the news real quick.
If you missed it, the compromise bill.
So this is the bill that was worked out
between the two versions when it came to
the House passed one, the Senate passed another,
they work out the differences and then it goes back.
The compromise version made it through the House.
It passed in the House by a vote of 310 to 118.
It's $886 billion.
That vote, though, 310 to 118,
it's a Republican-controlled House,
so obviously, the votes against,
those are all Democrats, right?
No, no, the majority of them are Republicans.
I think it's 73, 75 of them, somewhere in there.
And it didn't have to do with what was in it.
It had to do with what wasn't in it.
Most of the far right-wing culture war nonsense stuff
that got added, well, it got taken out.
They pushed through, I want to say,
they didn't want DOD helping women
who were trying to exercise their reproductive rights.
Now that got yanked out.
I think they wanted a ban on gender-affirming care.
It's not in there anymore, weird.
There will still be drag shows on military bases.
All that stuff got taken out.
Stuff that the far right side of the Republican Party
really wanted, priorities for them.
and they were throwing up roadblocks to stop this.
So what did Speaker Johnson do?
Used a procedural trick to get it to the floor, required a higher threshold, but got it to
the floor and then used democratic votes to get it through.
We talked about this when McCarthy was still speaker, that that was the move he needed
to make, because it renders that far-right, Twitter-like faction of the Republican Party
irrelevant, because they push all of this stuff and then they can't get it through.
It shows that they don't have any power.
And when your entire political platform is about being performative on social media,
you have to rack up some wins, otherwise, well, your performance isn't very good.
Every time this happens, it weakens that faction of the Republican Party.
Johnson has done this more than once now.
I don't think it's an accident.
It certainly appears that he is trying to reduce the influence of the far right wing
of the Republican Party, the MAGA faction.
And I know some people are going to say, but Johnson is far right-wing.
He is.
But he's not performative.
And that performative faction, that is the one that is causing a lot of discord within
the Republican Party, a lot of division.
It is good for the Democratic Party to get rid of them because it's also where all of
the weird ideas come from.
So there is a mutual benefit to non-MAGA Republicans working with Democrats to render that far-right
faction irrelevant, and that is certainly what it appears is happening.
It's not the first time he's done it.
They made all of these promises, and they did a whole lot of grandstanding talking about,
know drag shows on military bases and to be clear understand there are a lot of
traditions in the military that involve like guys going into drag and it is it's
kind of a thing on certain military installations to help nonprofits in that
way. There's one of the shelters that we actually help here they have an event
that is it's called walk her way or something like that where people are
encouraged to show up and walk a mile in her shoes wearing high heels the guys
from seventh group they show up most years in the most elegant dresses you
have ever seen in your life then afterward from what I understand they
actually have a party on base. This is all to raise money for DV shelters.
Removing that kind of behavior from the military, it's not gonna happen. It isn't
going to occur. So the grandstanding on stuff like this and then losing on
something that they present as, well it's just necessary and this is what we have
to do, and they can't get it done, it doesn't make them look good, and it reduces their
political capital.
It consolidates it more to a far-right faction that isn't MAGA, far-right that still roughly
believes in the Republic, not far right that would rather have, you know, one
person in charge that has a whole lot of furor behind them. So I think that we're
gonna see more of this. We're gonna see more of the speaker using procedural
tricks to bring votes to the floor that the far right side doesn't want and then
using Democratic votes to get it through. Partially it's stuff like this that is
considered must pass but every time it happens it consolidates power under
Johnson and makes the MAGA faction well weaker. If McCarthy had done this as a
a whole bunch of people suggested he'd probably still be speaker because they wouldn't have
the support to really engage in ousting him.
And that's kind of what happened here.
The reduction in support that that faction has now, it's going to make it harder and
harder for them to mount a challenge to the Speaker, which I would keep in mind
that the Speaker is supposed to be the leader in the House, not their errand boy.
It definitely looks like the MAGA faction in the House is being outplayed
by other Republicans, and I don't know that there's much they can do about it
at this point. They have gone too far in on the performative stuff to start
introducing any policy, because that policy is going to seem boring to their energized base.
Definitely worth noting. One of the big questions that did come in as far as what's in it is aid
for Ukraine. The initiative to help Ukraine was extended. I think until 2027, I could be wrong
about that fact check it and there's two maybe three hundred million dollars in
there for that this is not the aid package that is necessary for Ukraine to
continue this is more of the consider it like the overhead to continue helping
but the fact that that's in there indicates that they're they're
They're operating on good faith to get the aid to Ukraine.
It's a good sign, but it's not the end of that battle.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}