---
title: Let's talk about COP28, science, statements, and walkbacks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2IOcshVryPA) |
| Published | 2023/12/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- COP28, a climate summit of world leaders, faced criticism due to its CEO host being from an oil company.
- Criticism arose when the host stated that a phase-out of fossil fuels may not achieve the 1.5-degree goal.
- Doubts increased among skeptics of the summit's credibility.
- Concerns were raised about the lack of a roadmap for sustainable socio-economic development post-fossil fuel phase-out.
- Biden's absence from the summit was interpreted differently, with varying messages sent.
- The CEO later attempted to clarify his statements, mentioning the inevitability and importance of phasing out fossil fuels.
- The overall reputation of COP28 was significantly damaged, especially amidst existing scrutiny and lack of tangible results.
- The conference's outcomes have been questioned, with voluntary pledges and unmet goals being prominent.
- The lack of concrete results may lead to a shift in approach towards future climate summits.
- The incident underscores the reluctance of oil company CEOs towards phasing out fossil fuels.

### Quotes

- "There is obviously going to be reluctance to the phase out of fossil fuels from people who, well, are CEOs of oil companies."
- "This may be the turning point for whether or not people put any faith in the ability of the UN climate summits to produce anything worthwhile."

### Oneliner

Criticism over COP28's host, doubts on fossil fuel phase-out, and lack of tangible results question the summit's credibility and impact on climate goals.

### Audience

Climate activists, policymakers

### On-the-ground actions from transcript

- Contact climate organizations for updates and ways to support climate goals (implied)
- Join local climate action groups to push for tangible outcomes from climate summits (implied)

### Whats missing in summary

The full transcript provides detailed insights into the controversies and challenges faced by COP28, offering a nuanced view of the implications of the summit's proceedings and statements.

### Tags

#COP28 #ClimateSummit #FossilFuelPhaseOut #Criticism #UNClimateSummits


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk a little bit more about COP28
and some statements that were made there,
and science, and walking things back eventually,
and all of that stuff.
So recently, we talked about how there
were a lot of people who were looking at this year's COP28,
which, by the way, this is a, for those who don't know,
This is a climate summit of world leaders, big deal.
This is where progress is supposed to be made.
There was a lot of criticism this year
because the person selected to host it, Al Jabeer,
is also the CEO of an oil company,
and they figured that maybe that wouldn't go so well.
So, there was an exchange where he was being asked questions.
And he said, there is no science out there or no scenario out there that says the phase
out of fossil fuel is what's going to achieve 1.5, 1.5 being 1.5 degrees higher than now,
is a goal that as time passes seems unlikely that it's going to be met. So
this obviously led those people who were skeptical of this arrangement to be even
more skeptical. Another thing that was said during this was show me the
roadmap for a phase-out of fossil fuel that will allow for sustainable
socio-economic development unless you want to take the world back into caves."
It seems like there might have been some validity to some of the concerns.
Again, this is one of those things when Biden didn't go, when he made the decision not
to go, it was read differently by different people and the overall message that was sent
was, well, you're not really making any progress and it in some ways strengthened the position
of those who truly care about climate change.
So these statements were made and then later there was an attempt to change the tone and
bring the temperature down, pun intended. Al Jabeer went on to say, I have said over
and over the phase down and the phase out of fossil fuel is inevitable. In fact, it
is essential. He is saying that his comments were misinterpreted, that they were kind of
taken out of context. It is, I mean, the comments are what they are. There is
obviously going to be reluctance to the phase out of fossil fuels from people
who, well, are CEOs of oil companies. I mean, that seems like it should go
without saying, but apparently not. If the end result of this conference doesn't
have something incredibly tangible in it, this may be the turning point for
whether or not people put any faith in the ability of the UN climate summits
to produce anything worthwhile. This was incredibly damaging to the overall
reputation of this, especially when it was already under scrutiny. You know, the
results, they just haven't been there. It's been a lot of, we're going to do
something, a lot of voluntary pledges, a lot of goals set and not met, and this
may be the thing that prompts a different approach to occur. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}