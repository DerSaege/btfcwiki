---
title: Let's talk about the DOD IG report....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gUDLvCLJXdM) |
| Published | 2023/12/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Defense instituted a program to keep extremists out of the military, leading to 183 extremists identified in the past year.
- In addition to extremists, 58 service members were identified with gang affiliations.
- Out of the 183 identified extremists, 68 cases were cleared or unsubstantiated.
- Chain of command acted in every substantiated case, ranging from discharge to court-martial, demonstrating seriousness in addressing extremism.
- Weak link found in recruitment screening process, where steps were not always followed, potentially due to laxity from struggling to meet recruitment goals.
- Despite recruitment screening issues, the program appears to be functioning well overall.
- Programs initiated by the Secretariat of Defense, which some deemed unnecessary, have resulted in high numbers of extremists being identified.
- Army had the highest numbers of identified extremists, while Space Force had the lowest.
- Screening process may be improved by rejecting candidates without proper screening in place, potentially increasing recruiter diligence.
- Overwhelming majority of substantiated cases resulted in significant punishments, with only three receiving counseling.

### Quotes

- "Department of Defense instituted a program to keep extremists out of the military."
- "Every single instance where it was substantiated, it was acted upon."
- "The Army had the most numbers, raw numbers, but Space Force had the lowest."

### Oneliner

Department of Defense identifies extremists and gang affiliations in the military, with chain of command taking serious actions on substantiated cases while recruitment screening remains a weak link.

### Audience

Military personnel, policymakers

### On-the-ground actions from transcript

- Reject candidates without proper screening (implied)
- Increase recruiter diligence in screening processes (implied)

### Whats missing in summary

The potential long-term impacts and effectiveness of the program in keeping extremists out of the military.

### Tags

#DepartmentofDefense #MilitaryExtremism #RecruitmentScreening #ChainOfCommand #ProgramEffectiveness


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
an inspector general report for DOD,
what they learned, how things are going
with a certain program, how successful it's been,
where any lapses are, and how widespread it is.
because this is something that when it was enacted there were a whole lot of
people who are like well this is unnecessary. Turns out maybe it wasn't. So
the Department of Defense instituted a program to keep extremists out of the
military. Over the last year there were 78 service members that were suspected
of advocating the overthrow of the government. 44 that were suspected of
engaging in or supporting terrorism. The grand total for extremists was 183. The
remainder of those were made up of people who advocated for widespread
discrimination or they advocated for using violence to achieve their political
goals. So, 183. That number is up from last year, but there there is some good
news that we'll get to here in a minute. On top of the 183 there were 58 that
were identified or suspected of having gang affiliations, which is something
else the military has been working to deal with. It is worth noting that
political extremism outnumbers gang affiliations. So when it was all said and
Don, you're looking at a pretty large number here.
Out of these, 68 of these cases were deemed to be cleared
or unsubstantiated.
So you're talking about 173 that weren't.
Now, not all of these cases are closed and finalized yet.
As I go through this, I'm sure everybody's like, so what's the good news?
This is the good news.
Out of the cases that were closed, that were determined to actually be substantiated, the
chain of command acted in every single one of them, which is talk to your friends who
were in the military.
I mean, that's kind of rare in and of itself.
The actions taken by command ranged from involuntary discharge to an NJP, non-judicial punishment,
and I believe two of them actually went to court-martial.
So they are taking it very seriously.
One of the other things that is not going well is the screening that occurs at recruitment.
Basically what the IG found out was that not all of the steps were being taken.
During the recruitment process, there's supposed to be screening for a lot of this.
Sometimes that paperwork wasn't there.
There was no evidence that the screening occurred.
People who are familiar with recruiters, I think everybody can guess what happened here.
This was one of those things where the recruiter determined, well, it's not that bad and lost
the paperwork.
I'm sure that people have heard stories about, you know, have you ever had this kind of injury?
Well, yeah.
No, you haven't.
Probably along the same lines.
remember, they're having a hard time with recruitment now.
So the years and years of missing recruitment goals
has led recruiters to be maybe a little bit more
lax than they should.
That appears to be a weak link.
Everything else in this program, while the numbers are high,
it actually appears to be functioning,
which again nice change. So it is important to note that when this went
about and you know when the Secretariat of Defense you know did the stand down and all
of that which was let's be honest it was a symbolic a symbolic thing but the
programs that came along with it that people didn't think were necessary I
I mean these are pretty high numbers when you're talking about people who are advocating
overthrow or suspected of engaging or supporting.
They have it broken down by branch.
It did look like the Army had the most numbers, raw numbers, but remember the Army is a large
organization and Space Force of course had the lowest.
So it's a brief overview.
My guess is that the screening process will be fixed by basically rejecting anybody that
doesn't have it done, which will make recruiters do it.
That's what they're going to have to end up doing with this.
But it does appear that it's working, and again, the interesting thing is that every
single instance where it was substantiated, it was acted upon.
And most of them with pretty high-end punishments for the military.
I want to say only three were given just counseling.
So it's a brief look at that.
I'm sure we'll come back to it next year
and see what the numbers look like then.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}