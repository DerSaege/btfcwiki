---
title: Let's talk about the US, moral responsibility, and strategic defeat....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZpzWhyyzou4) |
| Published | 2023/12/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Austin's speech and the widely circulated quote about protecting Palestinian civilians in Gaza.
- Emphasizing the importance of context in understanding the key points of the speech.
- Explaining the difference between tactical victory and strategic defeat in warfare scenarios.
- Connecting the strategy of protecting civilians to winning in urban warfare.
- Stating that protecting civilians is vital to preventing civilians from sympathizing with the opposition.
- Addressing the pressure the US has put on Israel regarding civilian casualties.
- Suggesting that the US believes Israel has not effectively protected civilians in the conflict.
- Pointing out the diplomatic challenges the US faces as Israel is an ally.
- Mentioning the predictability of another organization rising even if the current one is rooted out.
- Recognizing Austin's expertise and experience in defense matters.

### Quotes

- "The lesson is not that you can win in urban warfare by protecting civilians. The lesson is that you can only win in urban warfare by protecting civilians."
- "Austin knows what he's talking about."
- "It's not unknown."
- "The US believes the conflict is going."
- "So even if Israel was successful in rooting out the current organization, another one's going to spring up."

### Oneliner

Analyzing Austin's speech on protecting Palestinian civilians reveals the importance of civilian safety in urban warfare and the strategic implications for Israel and the US.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Advocate for policies that prioritize civilian protection in conflict zones (implied)
- Support humanitarian aid efforts in conflict-affected areas (implied)
- Stay informed and engaged with diplomatic efforts to address civilian casualties in conflicts (implied)

### Whats missing in summary

Analysis of the potential long-term impacts of failing to protect civilians in conflicts. 

### Tags

#Austin'sSpeech #CivilianProtection #UrbanWarfare #USPosition #Israel #ConflictMitigation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Austin's speech
and that quote that is everywhere right now.
And we're going to talk about what it means
and what it means for the US position.
The major outlets, they pulled two paragraphs
from that speech.
And they're important paragraphs.
They are the most important in the speech.
However, context is key.
They probably should have pulled four paragraphs, the
paragraph immediately before and immediately after.
So we're going to go through all of it, those four
paragraphs, because there is something pretty important in
it that you would miss if you didn't get
first paragraph. If you have no idea what I'm talking about, the United States
Secretary of Defense gave a speech. There were a couple of paragraphs in it that
definitely caught a lot of news organizations and caught their attention.
And so we're going to to go through those. Okay, so the two that are
circulating everywhere to catch everybody up. You see, in this kind of a fight, the center of
gravity is the civilian population, and if you drive them into the arms of the enemy,
you replace a tactical victory with a strategic defeat. So, I have repeatedly made clear to
Israel's leaders that protecting Palestinian civilians in Gaza is both a moral responsibility
and a strategic imperative. That's what went out and I mean you can see why that went out everywhere.
To get rid of some of the jargon real quick before we add the context. Replace a tactical
victory with a strategic defeat. If you watch this channel a lot you have heard me say that
even if Russia wins the fighting in Ukraine, they lost the war. Winning the
fighting in Ukraine would be a tactical victory, losing the war strategic defeat.
The goal of this was to assert themselves, show that they were a
superpower again, and push back on NATO. When the invasion caused countries to
ask to join NATO, it was a strategic defeat, regardless of what happens with the fighting.
Okay, so here's the four paragraphs together. So the lesson is not that you can win in urban warfare
by protecting civilians. The lesson is that you can only win in urban warfare by protecting
You see, in this kind of fight, the center of gravity is the civilian population, and if you drive them into the arms of
the enemy, you replace a tactical victory with a strategic defeat.
So I have repeatedly made clear to Israel's leaders that protecting Palestinian civilians in Gaza is both a moral
responsibility and a strategic imperative.
And so I have personally pushed Israeli leaders to avoid civilian casualties, and to shun irresponsible rhetoric, and to
prevent violence by settlers in the West Bank,  dramatically expand access to humanitarian aid. So, the fourth
paragraph.
That one. It shows that it's not just about avoiding civilians in strikes.
That it's more than that. Hearts and Mind stuff. Making sure that the civilian
population doesn't begin to sympathize with their opposition, with Israel's
opposition. So that's important overall, but it's that first paragraph that
really matters. So the lesson is not that you can win in urban warfare by
protecting civilians. The lesson is that you can only win in urban warfare by
protecting civilians. Then goes on to say that if you if you don't do that it
drives them into the arms of the opposition. You can look to the amount
of pressure the US has put on Israel in regards to civilian casualties. You can
at statements that US officials have made. It is the position of the US that
this first part, protecting civilians, wasn't done. The lesson is that you can
only win in urban warfare by protecting civilians. The US does not believe that's
happened. This is as close as you are going to get to a U.S. official saying
that Israel already lost when it comes to the strategic objectives, even if they
win the fighting.
An easier way to say this is that the strategy deployed with the victory
condition they have makes that victory condition unattainable and there's
going to be another cycle. That's what's being said there. When this quote came
out I had a whole bunch of people ask me what I thought about it.
Austin knows what he's talking about.
He's not some random Secretary of Defense.
He has experience with this, and this, it's in the manuals.
He knows what he's talking about.
He understands this subject.
It's not unknown.
On October 9th, less than 48 hours after the initial attack, there's a video on this channel
going over this exact thing, talking about overreaction and why the small force wants
to provoke it because it turns the bystander into somebody who's sympathetic, somebody
who's sympathetic and to somebody who's active. It's not unknown. This is as
close as you're going to get to a U.S. official saying that in public. The U.S.
is in an awkward diplomatic position because Israel is an ally and that they
can't just come out and say that everything that has happened is not
going to achieve the objective because the the amount of loss is going to
create people who are sympathetic or active for their opposition. So even if
Israel was successful in rooting out the current organization, another one's going
spring up. That's what's there. So I wouldn't expect this to be something that the US repeatedly
says but it gives a pretty clear indication of how the US believes the conflict is going.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}