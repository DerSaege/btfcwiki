---
title: Let's talk about Justice Thomas, pay, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mV0KzGZEzj8) |
| Published | 2023/12/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of Justice Thomas, the Supreme Court, pay, finances, timelines, and ProPublica.
- Mentions ProPublica as a reputable outlet known for breaking major stories.
- Talks about a report from ProPublica regarding Justice Thomas facing financial issues in January 2000.
- Justice Thomas discussed financial problems with a GOP lawmaker on a flight back from a conservative conference.
- Lawmaker attempted to get justices a raise but it didn't happen.
- Panic in the GOP was triggered by Thomas mentioning the possibility of resigning over pay, which could lead to Clinton nominating replacements.
- Justice Thomas started receiving gifts shortly after, connecting to previous reporting by ProPublica.
- ProPublica's reporting doesn't imply anything nefarious but raises questions about the timeline of events.
- Urges the audience to read the reporting to understand the situation better.
- Emphasizes that the issue will come up and be politically significant, regardless of evidence surfacing.

### Quotes

- "This is going to be big."
- "Politically, it is going to be used whether or not evidence ever surfaces."
- "Y'all have a good day."

### Oneliner

Beau explains a report from ProPublica on Justice Thomas facing financial issues and a timeline of events that could have political implications.

### Audience

Advocates and activists.

### On-the-ground actions from transcript

- Read the report from ProPublica to understand the situation better (suggested).
- Stay informed and prepared for this issue to be a significant talking point (implied).

### What's missing in summary

Context on Justice Thomas's financial issues and potential implications for the Supreme Court.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Justice Thomas,
the Supreme Court, pay, finances, timelines,
and ProPublica and some information
that is probably going to become really important.
If you are not familiar with ProPublica, it's an outlet.
I highly recommend them.
They are old-school, digging-through-documents
kind of journalists.
They break a lot of major stories that then later become
things that are being discussed on major outlets, outlets
that you have heard of.
OK, so we are going to go over some information
that they put out, a report they put out,
about Justice Thomas and a timeline.
This is what the reporting says in January of 2000.
Justice Thomas, at a conservative conference,
he is, according to the reporting,
facing some financial issues.
On a flight back, he winds up sitting next to a GOP lawmaker,
and he talks about it pretty openly.
And he says that Congress needs to give him a raise,
them do speaking fees, that kind of thing. Now, that lawmaker definitely tried with the raises.
I'm not sure about the speaking fees, but definitely made an attempt to get justices a raise.
It didn't happen, though. That raise didn't occur. They couldn't accept speaking fees and understand
there are a lot of law schools like that doesn't necessarily have to be like a lot of speaking fees
are really payments for something else or people believe they're payments for something else.
A lot of law schools would provide speaking fees to the justices so it would be something
that could actually be legitimate. But it never happened. Okay, so from there, the
lawmaker is a little panicked about the idea of Justice Thomas resigning or
other justices resigning over pay because that was something that Thomas
said on the plane. If that had occurred, Clinton would have nominated their
replacements. So panic is set off in the GOP. Shortly thereafter, in the years
that followed, well, Justice Thomas starts getting a bunch of gifts, the ones that
had been discussed in previous reporting. Incidentally, I think broken by ProPublica.
That's quite a chain of events. Now there's nothing in it, there's nothing in the reporting
because ProPublica, they're journalism. They don't imply things they can't prove. So
There's nothing in the reporting suggesting anything nefarious, but that question is now
out there.
This is going to come up.
This is going to be big.
I will put the links to the reporting down below, strongly suggest you read it so you
can understand the situations as far as what they were paid at the time, what that means
in today's money because all of that's in there.
And this is going to become an issue.
This is going to come up because whether or not there is any evidence of a connection,
the timeline alone is, well, it's sus.
And politically, it is going to be used whether or not evidence ever surfaces.
This is going to be a big deal.
It's something you need to understand and be familiar with as talking points start to
be developed.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}