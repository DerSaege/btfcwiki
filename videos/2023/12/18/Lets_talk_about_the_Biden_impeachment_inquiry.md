---
title: Let's talk about the Biden impeachment inquiry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OeaGCD0J8nE) |
| Published | 2023/12/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of the impeachment inquiry into President Biden, authorized by the U.S. House of Representatives in a party-line vote.
- Speculating on the possible political ramifications and outcomes of the inquiry, including the potential for impeachment.
- Noting the lack of substantial evidence presented for impeachment and characterizing the inquiry as a political stunt or fishing expedition.
- Mentioning the skepticism and lack of support within the Republican Party for impeachment proceedings.
- Expressing doubt about the likelihood of the impeachment process progressing beyond the House or Senate due to insufficient evidence.
- Concluding that it is improbable for President Biden to be impeached and removed based on the current circumstances and evidence presented.

### Quotes

- "If there was a smoking gun, I think we'd be talking about it."
- "For an impeachment to move forward, not just they need all Republicans in the Senate, they need a bunch of Democrats."
- "It's just a thought. Y'all have a good day."

### Oneliner

Beau analyzes the Biden impeachment inquiry, suggesting it lacks evidence and might be a political stunt, expressing doubts about its progression beyond the House or Senate.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Monitor political developments surrounding the Biden impeachment inquiry (implied).

### Whats missing in summary

Analysis of the potential impact of the inquiry on public perception and political dynamics.

### Tags

#Biden #Impeachment #PoliticalStunt #RepublicanParty #Evidence


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and the inquiry and where things sit.
A lot of questions have come in about it, and I think it's important for everybody
to acknowledge the realities of this.
If you missed the news and don't know what I'm talking about, because a lot of stuff
happened that day and it got overshadowed, the U.S.
House of Representatives authorized an impeachment inquiry into President Biden.
It was a 221 to 212 party line vote.
So the inquiry was authorized.
This might lead to an impeachment.
Again, we'll have to see how it plays out.
I feel like the political backlash may stop the vote or those Republicans that are in areas that
aren't deep red may realize that that vote will not suit them politically. But if it was to make it
to that stage and Biden gets impeached. It would then go to the Senate and the Senate would have
to convict. Talking about the impeachment, everybody is talking about the House because
that's where all the action is right now. But it might be worth taking a look at what the jurors
are saying when it comes to this. The narrative is, quote, falling apart, the Republican narrative
of wrongdoing by Biden. If there was a smoking gun, I think we'd be talking about it,
it." That's a quote. Another senator said that he had seen no evidence. Now, this is a series of
statements that when you say they're coming from the Senate, well, I mean that
mean anything. This is Lindsey Graham and Grassley Republican senators. The
Republican Party doesn't even have the Republican Party on its side. For an
impeachment to move forward, not just would they need all Republicans in the
Senate, they would need a bunch of Democrats. They don't even have all
Republicans. You're already starting to see stuff about him being impeached and
removed. Yeah, I mean that's not that's not going to happen. Just saying. Based
Based on what we have seen from the story being presented by House Republicans, there's
no there there.
It's a story with a lot of allegations and accusations, but it is super light on evidence
as in really haven't seen any.
The general consensus is that the inquiry is a political stunt.
A more generous interpretation is that it's a fishing expedition.
They don't actually have anything to impeach him on yet, but if they look hard enough,
maybe they'll find something.
I mean, maybe that's true.
I mean, it doesn't matter what you think about Biden, you have to acknowledge he is in fact
politician and you need to keep that in mind. But realistically it seems unlikely
that it gets out of the House. In fact it may be something that the Republican
Party regrets doing. Unlikely to get out of the House as far as it going anywhere
in the Senate, it's just not going to. Not based on the evidence that has been
presented or even evidence that they claim they have. It still doesn't rise to
that level. So just to set everybody's mind at ease, that doesn't seem really
like it's in the cards. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}