---
title: Let's talk about what low intensity means and a question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ebFcAiNFfu4) |
| Published | 2023/12/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the concept of "low intensity" conflict compared to major wars like World War II or conflicts like Ukraine.
- Clarifying that "low intensity" does not mean relative peace but rather a different kind of military operation.
- Acknowledging the terminology concern and openness to feedback about outdated terms.
- Addressing a hypothetical question about whether the US could stop a conflict by force, pointing out the limitations and consequences.
- Emphasizing that while the US has the military power to stop conflicts, deploying that power often leads to more harm than good.
- Advocating for the US to move away from being the world's policeman and cautioning against intervention in conflicts where neither party is an ally.
- Noting the inadequacy of US peace enforcement strategies, which often result in taking out leadership but failing to maintain peace afterwards.

### Quotes

- "Low intensity does not mean like relative peace. It means a different kind of operation."
- "The US has the military power to stop the fighting, yes. Deploying that in the way it would have to be done, never gonna happen."
- "The U.S. wins the war, destroys the opposition's military, and then loses the peace."
- "The question is yes, the power exists but it won't be used."
- "The US needs to be moving away from being the world's policeman."

### Oneliner

Beau explains "low intensity" conflict and why the US won't use its military power to stop certain conflicts, advocating for a shift away from interventionism.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Advocate for a shift in US foreign policy towards non-interventionism (implied).

### Whats missing in summary

Beau's insights on the limitations of military intervention and the importance of reevaluating US foreign policy.

### Tags

#USForeignPolicy #ConflictResolution #MilitaryPower #Interventionism #Peacekeeping


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we are going to talk about two of your questions.
Well, I guess a question and a comment
that I definitely want to clear up.
And they both deal with the same topic.
One is a terminology thing,
and one is a hypothetical what if.
We'll start with a terminology thing
because I realize this is probably important.
Okay, so the message says, love your channel, but I think you should know low intensity
is a term that has fallen out of usage.
It might be a good idea to explain what that is because people aren't going to view what
comes next as low intensity.
Okay, so a few times I have said low intensity.
That is a term to describe a certain kind of conflict.
Understand what it means is low intensity in comparison to World War II.
It is low intensity in comparison to Ukraine.
Low intensity conflict, the low end of that term includes the troubles.
The high end of that term includes large phases, large periods of the war in Iraq.
Low intensity does not mean like relative peace.
It means a different kind of operation.
Don't confuse it.
It's still war.
If you ever hear me using a term that has fallen out of usage, please let me know.
I do want to clarify that.
So just to be clear, when people are talking about Israel and the end of this phase of
operations, the end of major combat operations, and the term low intensity comes up, that
doesn't mean like peaceful patrolling.
It is still combat.
The next question is a hypothetical one that I find interesting because it shows how limited
US options are in certain cases, and it's not just here.
Could we stop this fight?
I'm not asking if we should, I'm asking if we could.
And by that, I'm kind of reading into this to mean, can we stop it by force?
And we meaning the US.
Diplomatic efforts, they're underway.
So I don't think that's the question.
Could the US stop this by force?
Yeah.
Yes.
With a whole lot of casualties and declaring war on an ally.
it is one of those things, sure, it's possible, it's not going to happen though.
That would be, that's just outside the realm of anything that would really even be considered.
Like it wouldn't even remotely be considered, it wouldn't, it would not be something even
brought up in a conversation.
The US has the military power to stop the fighting, yes.
Deploying that in the way it would have to be done, never gonna happen.
And it's not just about one of the parties being an ally.
There are times when the U.S. has the power to stop two parties from fighting who aren't
allied, neither one of them are an ally.
But doing it might actually be worse than allowing the fighting to continue.
And it's one of those things where realistically the United States needs to be moving away
from being the world's policeman.
Pick a conflict where neither party is an ally to the U.S. and they're fighting.
something like that, it's an internal thing. If the U.S. was to come in and do
what the U.S. does when it comes to conflict, the population in that
area, everything that happens is then the U.S. They did it. There are times when it
It won't be done for foreign policy reasons.
And then there are times when it won't be done because it would literally be worse.
The U.S., when it comes to enforcing peace, the U.S. isn't really that good.
The way it stops conflicts is by taking out command and control, taking out the leadership.
not a scalpel, it's a sledgehammer. And the U.S. in particular when you look at
recent conflicts, the U.S. wins the war, destroys the opposition's military, and
then loses the peace. And when you're talking about stopping a war, it is using
force to do that, oftentimes you just fracture the various sides and create an
even more violent situation. And that's in a situation where there
aren't foreign policy constraints stopping it because there's no way the
US is going to use force against Israel. That's just not going to happen. So the
question is yes, the power exists but it won't be used. So anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}