---
title: Let's talk about eagles and industry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mk8Y-s4O3fE) |
| Published | 2023/12/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two men are in trouble for allegedly killing 3,600 birds, including bald and golden eagles, from January 2019 to March 2021.
- They face charges for violating the Bald and Golden Eagle Protection Act, with text messages allegedly admitting to their felonies.
- The birds were killed and likely sold for their feathers, but it's unclear how many were protected.
- Despite not being arrested, the men have been ordered to appear, indicating a strong case against them.
- The illegal wildlife trade for feathers continues due to its appeal as a status symbol.
- This case may draw attention to the ongoing issue of wildlife trafficking, potentially becoming a significant trial.

### Quotes

- "Only elephants need ivory, only rhinos need horns, and only birds need feathers."
- "There's probably going to be a lot more said and if this goes to trial it will probably turn into a really big thing."

### Oneliner

Two men face charges for illegally killing thousands of birds, shedding light on the dark world of wildlife trafficking.

### Audience

Wildlife enthusiasts, conservationists

### On-the-ground actions from transcript

- Join wildlife protection organizations to support efforts against wildlife trafficking (implied)
- Stay informed about wildlife protection laws and report any suspicious activities involving protected species (implied)

### Whats missing in summary

The emotional impact of the environmental harm caused by the illegal killing of thousands of birds and the importance of raising awareness about wildlife conservation efforts.

### Tags

#WildlifeTrafficking #BirdConservation #IllegalKilling #EnvironmentalProtection #ConservationEfforts


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about eagles, bald and golden.
And we're going to talk about Montana and two men who
engaged in something over the course of years
and now find themselves in trouble over it.
OK, so what's going on?
So the federal government is alleging that two men from January of 2019 to March of 2021
engaged in a spree.
And during this, they killed 3,600 birds, some of which are bald eagles and golden eagles.
The prosecution alleges that they have text messages in which basically they're admitting
that they're committing felonies and they know they're committing felonies.
And to tell their buyers that they're very active.
Not their wording in the alleged text messages.
The two men have been charged with one count of conspiracy and 13 counts of violating the
Bald and Golden Eagle Protection Act.
It appears that they were killed, the birds were killed, and then sold, probably for their
feathers.
There is no comment from the government on how many of the 3,600 were protected.
There are 13 counts that I guess the government believes that they can prove.
And the reporting says, you know, they would meet up in this location, go do it.
There's a lot of information that has been released early on that suggests this is a
pretty strong case.
My understanding is that the two men have not been arrested, but they have been ordered
to appear.
It's worth remembering that only elephants need ivory, only rhinos need horns, and only
birds need feathers.
industry, as weird as it sounds, it's not going away. It's something
that continues and in some ways flourishes. As people view it as the
forbidden fruit, a status symbol. A market has developed for it. So we'll probably
hear more about this because of the scale of this. There's probably going to
be a lot more said and if this goes to trial it will probably turn into a
really big thing. Obviously, wildlife protection organizations are going to be talking about this
and using it to highlight how this industry is still out there. There are still people out there
doing this on a pretty large scale anyway it's just a thought you all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}