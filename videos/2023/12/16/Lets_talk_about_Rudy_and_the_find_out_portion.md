---
title: Let's talk about Rudy and the find out portion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WhBAOAcTXCY) |
| Published | 2023/12/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A federal jury decided that Rudy Giuliani will have to pay $75 million in punitive damages, plus additional amounts for emotional distress and defamation.
- The total amount Giuliani has to pay is about $148 million.
- The news related to the election is expected to trend due to these developments.
- The tide seems to have turned against those who pushed certain theories about the election.
- Legal maneuvering may occur, but Giuliani will have to pay the amount.
- There might be a chance to appeal some parts of the decision, but the payments are inevitable.
- Advice is given to the plaintiffs, Ms. Freeman and Ms. Moss, to get a good accountant and enjoy their retirement.

### Quotes

- "Giuliani will have to pay $148 million."
- "The tide seems to have turned on that."
- "He's going to have to pay."
- "Get a really good accountant, tax person, stuff like that."
- "Y'all have a good day."

### Oneliner

A federal jury decided Rudy Giuliani must pay $148 million, signaling a shift in news trends and legal outcomes related to the election.

### Audience
Legal observers

### On-the-ground actions from transcript
- Get a good accountant and tax person (suggested)
- Enjoy retirement (suggested)

### Whats missing in summary
Context on the defamation case and its impact.

### Tags
#RudyGiuliani #DefamationCase #LegalOutcome #ElectionTheories #PunitiveDamages


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Rudy Giuliani and a
decision that was reached today, and what it means,
where it goes from here, all of that stuff, because a
determination was reached on the amount that he was going
to have to pay.
and at this point I'm really just building up the suspense for the people
who don't know yet.
Okay, so
in the defamation case
a federal jury has unanimously decided
that Giuliani
will have to pay seventy five million dollars
in punitive damages
plus an additional
twenty million dollars
each
the two plaintiffs for emotional distress, plus an additional $16 million each to the two plaintiffs
for the actual defamation. Bringing the total to right at $148 million,
dollars. I believe we have officially reached the find out portion of the show.
I would expect news to start trending this direction. The developments related
to everything having to do with the election. Overall, I feel like the news
is going to be more like this than delays or things going towards those who pushed certain
theories about the election.
It seems like the tide has turned on that.
So obviously the questions are going to be, you know, is he really going to have to pay?
According to the attorneys that I've talked to, yeah.
There will be some legal maneuvering, trying to structure this and that or whatever.
But the end result is that yes, he's going to have to pay.
One of the people that I talked to did say that there was a chance that maybe he would
be able to appeal on some parts of it and get it slightly reduced but not much.
The people I've talked to seem incredibly incredibly positive that the
payments will be will be heading out. Now to Ms. Freeman, Ms. Moss, some free
advice, although y'all never need anything for free again, get a really
good accountant, tax person, stuff like that, and enjoy your retirement. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}