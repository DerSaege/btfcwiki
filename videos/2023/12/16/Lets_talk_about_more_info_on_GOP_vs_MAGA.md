---
title: Let's talk about more info on GOP vs MAGA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nxD6aZFoqLU) |
| Published | 2023/12/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The current situation within the Republican Party in the House is being discussed, particularly decisions made for the budget indicating an attempt to lessen the influence of the far-right faction.
- A memo was circulated before a vote, seen as a move to in-run conservative objections and pass liberal military policy with House Democrats' help.
- The relationship between the current Speaker and MAGA Republicans seems to be over, as indicated by the memo.
- There may be objections similar to those faced by McCarthy, comparing Speaker Johnson to Speaker Boehner.
- The MAGA base might realize that a hard-line approach doesn't benefit them, unlike an incremental one.
- Despite Democratic control in the Senate and presidency, promises made to appeal to the far-right may not be fulfilled.
- The far-right within the Republican Party is heavily invested in social media echo chambers, potentially missing pushback and confusing vocal supporters with general sentiment.
- Discord within the party seems to have accomplished nothing, and they may not be in a good position moving towards the election, especially with difficulties in explaining the impeachment inquiry.
- The lack of clarity regarding messaging may lead to issues in presentation, potentially impacting independent voters.
- If the far right pushes too hard, it could make the Republican Party appear more dysfunctional, affecting independent voters.

### Quotes

- "It's safe to say that the honeymoon period between the current speaker and the MAGA Republicans, oh it's over."
- "They may be confusing their most vocal supporters for the sentiment of everybody that they are supposed to represent."
- "The lack of clarity regarding messaging may lead to issues in presentation."

### Oneliner

The current Speaker of the House's attempts to lessen far-right influence within the Republican Party may lead to discord and potential dysfunction, impacting independent voters.

### Audience

Political analysts, Republican Party members

### On-the-ground actions from transcript

- Pay attention to social media echo chambers and try to break through them (implied)
- Stay informed about political developments and positions of different factions within the Republican Party (implied)

### Whats missing in summary

Insights on potential future developments and strategies within the Republican Party 

### Tags

#RepublicanParty #SpeakerJohnson #MAGARepublicans #HouseDemocrats #SocialMediaEchoChambers


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we are going to talk a little bit more
about the current situation
within the Republican Party in the House.
We talked a little bit about how some decisions made
for the budget kinda indicated
that the current Speaker of the House
is attempting to lessen the influence of the far-right faction of the Republican Party.
It turns out that they're kind of aware of that.
Prior to the vote, they started circulating a little memo, a talking point thing here.
This is an obvious play to in-run conservative objections and pass liberal woke military
policy with the help of House Democrats, a page ripped from the Boehner playbook, a reference
to the former speaker of the House.
So it is safe to say that the honeymoon period between the current speaker, Speaker Johnson,
And the MAGA Republicans, oh it's over.
It's over.
And based on this memo, it seems likely that you are going to start hearing the same type
of objection that you heard when it came to McCarthy.
And they're going to start comparing them to Speaker Boehner a lot.
It's not really a surprising development but it's certainly going to be interesting to
watch it play out because we honestly, we don't know how that's going to end up.
It seems as though the MAGA bass might start to realize that the hard line approach actually
doesn't get them anything.
it does is stop everything and that an incremental approach actually gets them a little bit.
I don't think that the far right is going to be super successful with this, but I have
underestimated the gullibility of their base before.
seems like those people who are paying attention would realize that with the
Senate under a Democratic control and the presidency under Democratic control
making a bunch of promises to appeal to the far-right, well I mean they're just
going to be a whole bunch of unfulfilled promises come re-election time. It seems
like people would realize that but this part of the Republican Party is very
invested in social media and they have created social media echo chambers so
they may not be hearing the pushback they may not they may be confusing their
most vocal supporters for the sentiment of everybody that they are supposed to
represent. So it doesn't look like the the discord that led to all of that mess
with the speaker actually accomplished anything, and it doesn't look like the
Republicans in the House are going to be in a good position as they move toward
the election, especially with the impeachment inquiry, that even now they have a hard time
explaining exactly what it's about, which is going to be one of those things that makes
it really hard to convince the American people, as we talked about with the January 6 committee.
what you're going to tell them, tell them, tell them what you told them. They can't get to the
part where they even know what they're going to tell them. That's going to lead to issues in the
presentation. It should be interesting, and if it gets too far out of hand, if the far right
If the government pushes too hard, it will make the Republican Party appear even more dysfunctional.
And that will probably have an impact on independent voters.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}