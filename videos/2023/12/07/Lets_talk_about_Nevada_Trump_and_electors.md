---
title: Let's talk about Nevada, Trump, and electors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BPbEoLjHr9A) |
| Published | 2023/12/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nevada charges Republican leaders in relation to fake electors, similar to Michigan and Georgia.
- Charges include felonies like offering false/forged instruments for filing.
- Prosecution gained steam with cooperation of witness Kenneth Cheesebrough from Georgia.
- Multiple jurisdictions, prosecutors, and grand juries all reaching the same conclusion on conduct.
- Presumption of innocence for all, but evidence is consistent across cases.
- Narrative of politically motivated actions starting to weaken.
- Acknowledgment needed that election interference occurred to alter outcomes.
- Real movement in the case expected to take time.

### Quotes

- "At some point, it's going to have to be acknowledged."
- "It's the same type of evidence in each jurisdiction."
- "The framing though of this being politically motivated, it's starting to fall apart."

### Oneliner

Nevada joins Michigan and Georgia in charging Republican leaders for fake electors, exposing consistent evidence across jurisdictions and weakening politically motivated narratives.

### Audience

Political observers

### On-the-ground actions from transcript

- Follow updates on legal proceedings and outcomes (suggested)
- Stay informed and engaged with developments in the case (suggested)

### Whats missing in summary

The full transcript provides more context on the legal process and the implications of the charges for the individuals involved.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about some news coming out
of the great state of Nevada.
And we are going to talk about, well,
how that state now joins two other states with probably more
on the way.
And following a certain course of action,
we're going to talk about how it got to this point
and what kind of pushed it along.
we're going to talk about a realization, an acknowledgement that at some point is
going to have to occur. Okay, so if you have missed the news, Nevada has
announced that six people, many of whom are within the Republican leadership in
the state, have been charged. And they have been charged in relationship to the
whole fake elector thing. So it is like Michigan, like Georgia, and odds are more
states will follow, at least one more. It looks like the charges range from
offering a false instrument for filing, offering a forged instrument, it's stuff
like that, they are felonies. So my understanding is that the prosecution was
looking into this. They were investigating it and things were moving
along. However, it picked up and the investigation gained a lot of
steam when the prosecution was able to secure the cooperation of one witness in
particular, a witness that is reportedly Kenneth Cheesebrough. Most people will
recognize that name from the proceedings in Georgia. So from here what happens?
It's gonna look like Georgia and Michigan. It's gonna follow that same
path. There's a lot of procedural stuff that has to be gone through, but the
process is now underway there. You know, for quite some time, a lot of Republicans
have leaned into the idea that, well, it's politically motivated to get Trump.
That's what it's about. And that they've pushed that narrative and a lot of
high-ranking Republicans have followed suit. At this point, you are talking about
multiple jurisdictions, multiple prosecutors, multiple avenues of evidence,
multiple grand juries, all reaching the same conclusion. At some point it's going
to have to be acknowledged that the conduct is being viewed in multiple
jurisdictions the same way. Every one of these people has the presumption of
innocence, all of them. The evidence that is being leveled, it is pretty much the
same. It's the same type of evidence in each jurisdiction. It makes the line that
it's politically motivated, a little bit harder to swallow, or it should, especially
now that you have people from one case cooperating and reportedly providing evidence in a case
in another state.
Everybody has the presumption of innocence.
is entitled to a defense. The framing though of this being politically
motivated, it's starting to fall apart. Obviously there were a lot of people
who did not believe that that's what was going on to begin with, but even amongst
those who are ardent supporters of the Republican Party, at some point you have
to look at all of this and acknowledge that multiple jurisdictions have reached
the conclusion that something occurred, it dealt with the election, they believe
it to be criminal, and that the purpose at its heart was to alter the outcome
of the election and take away your voice, at some point that's going to have to be acknowledged.
With this particular case, you're probably looking at quite some time before you start
to see real movement on it, because it's going to be just like the lead up to the other cases.
It's going to take time.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}