---
title: Let's talk about Russia, clubs, and the future of the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pF8do4irdEo) |
| Published | 2023/12/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia's Supreme Court labeled the LGBTQ+ community as an extremist movement, leading to security services raiding LGBTQ+ establishments in Moscow.
- Putin has long promoted "traditional family values" and passed anti-LGBTQ laws like the gay propaganda law in 2013.
- The quick progression from anti-LGBTQ laws to security crackdowns in Moscow serves as a warning for Americans, as we are about 10 years behind Russia in this regard.
- The targeting of the LGBTQ+ community is often a testing ground for authoritarian regimes that may eventually impact other groups.
- Americans cheering on authoritarian actions are unknowingly setting themselves up to be the next target of oppressive measures.

### Quotes

- "When masked, security services are photographing your documents, generally speaking, that's a bad sign for your future."
- "It's not gonna stop with our community. They're just the test group."
- "If you're cheering this on because you are not part of this group, you have to understand that once a government gets power like this, it's really hard to get it back."

### Oneliner

Russia's anti-LGBTQ progression warns Americans of authoritarian measures and the dangerous implications of supporting such actions.

### Audience

Aware citizens

### On-the-ground actions from transcript

- Reach out to LGBTQ+ organizations for support and guidance (suggested)
- Advocate for LGBTQ+ rights and protections in your community (implied)
- Stand in solidarity with marginalized groups facing oppression (implied)

### Whats missing in summary

The emotional impact and urgency of standing against oppressive actions and protecting marginalized communities.

### Tags

#Russia #LGBTQ+ #Authoritarianism #Warning #HumanRights


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Russia.
We're gonna talk about Moscow in particular,
some things that happened there.
And we're gonna talk about a timeline and we're gonna see
if any of it really kind of sounds familiar to anybody
because there were some developments
that are probably going to lead to a lot of decisions
decisions being made on the international scene, as well as how companies interact with Russia in the
future, and a whole bunch of other developments from this. Okay, so, the Supreme Court in Russia
basically said that the LGBTQ plus community was, quote, an extremist movement.
Less than 48 hours later, security services in Moscow began raiding bars, clubs, saunas,
and a lot of those locations have already said they're going to have to shut down.
While security services were there, they took people's documents and photographed them,
which is a clear indication of them going on a list. Putin, throughout his time as leader,
he has pushed the idea of, quote, traditional family values, pushed it over and over again.
About 10 years ago, it was 2013, they passed something there called the gay
propaganda law. And basically what it boiled down to was, hey, if a kid is
going to read that book, there can't be anything. You can't have anything like
that in it. You can't have gay characters. You can't have anything even
remotely suggesting that that's okay. You got to get that stuff out. It's not just
you not get to have it in the library, it's illegal. Sounds familiar, right? It
took ten years, one decade, to go from there to security services, rating bars,
and creating lists. It's worth noting that earlier this year they passed a law
prohibiting gender transition. Does any of this sound familiar? Make no mistake
about it, it can happen here.
There's a template with the way the people who push this stuff look to Putin as some
strong man leader, they need somebody to tell them what to do, they will follow it.
The timeline and the progress and how quickly it happened, it's something Americans should
take note of.
We are basically 10 years behind them.
When masked, security services are photographing your documents,
generally speaking, that's a bad sign for your future.
When security services believe that something like this,
an immutable characteristic, is something
that they need to spend their time on,
that they need to schedule within 48 hours,
schedule coordinated raids to multiple places
to send a message.
Understand, it's not gonna stop with them.
It's not gonna stop with our community.
They're just the test group.
There's a lot of people in this country
who are cheering on authoritarianism
will eventually impact them. It will impact them. But right now, they've been
given permission to be their worst. Of those people, they're different. Kick
down at them. It doesn't take long and once the precedent is set, it'll be
applied to other groups. Make no mistake about it. If you're cheering this on
because you are not part of this group, you have to understand that once a
government gets power like this, it's really hard to get it back. It tends to
expand and it goes after other groups.
The U.S. has a decent segment of its population who thinks that going down
this road would benefit them. In real life, it just sets them up to be next, but they
don't know that because the person who manipulated them did it while they were hugging a flag.
This is something that Americans should see as a warning.
way. It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}