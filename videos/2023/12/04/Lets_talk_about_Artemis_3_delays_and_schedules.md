---
title: Let's talk about Artemis 3, delays, and schedules....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LzjZ8AIxqGw) |
| Published | 2023/12/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA's Artemis project aims to put somebody back on the moon, with Artemis III being the first crewed craft scheduled for late 2025.
- A recent GAO report indicates a potential delay, with Artemis III likely to occur in early 2027 instead of the initially planned late 2025.
- The main delay is attributed to the Human Landing System (HLS) by SpaceX, moving slower than expected in its development.
- The report suggests that if HLS development follows the average timeline of NASA's major projects, Artemis III will be pushed back to 2027.
- The HLS system didn't meet several key milestones, leading to delays in the Artemis mission.
- Issues also arise with the development of suits by a company called Axiom, with NASA requesting additional emergency life support integration.
- The delay in the Artemis mission means it will take longer before another person steps foot on the moon.

### Quotes

- "Artemis III will be the first crewed craft going where, well, actually somebody has gone before."
- "It looks like it's going to be a wee bit longer before we actually see another person on the moon."

### Oneliner

NASA's Artemis project faces delays due to setbacks in the Human Landing System (HLS) development, potentially pushing the mission to early 2027, prolonging the wait for a return to the moon.

### Audience
Space enthusiasts

### On-the-ground actions from transcript

- Stay updated on NASA's Artemis project progress and advocacy efforts to support timely developments (implied).
- Engage with space-related communities to stay informed and potentially contribute to advancements in space exploration (implied).

### Whats missing in summary

Details on specific HLS milestones and potential solutions to expedite development.

### Tags

#NASA #Artemis #SpaceExploration #MoonMission #HLS


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Artemis and NASA
and delays and what we can actually expect as far as
Artemis moving forward, because a report came out.
And well, it's not good.
So for those who don't know, Artemis is NASA's new project
put somebody back on the moon.
And Artemis III will be the first crewed craft going
where, well, actually somebody has gone before.
It's currently scheduled for late 2025.
And that is the current schedule.
However, a GAO report that came out on November 30th
says that, well, you should really expect 2027, is basically what it boils down to.
It seems that the main hiccup is with the HLS, that is the Human Landing System. It's
a something by SpaceX, and it's moving at a slower pace than expected. So the report says,
We found that if the HLS development takes as many months as NASA major projects do, on average,
the Artemis 3 mission would likely occur in early 2027. The complexity of human spaceflight suggests
it is unrealistic to expect the HLS program to complete development more than a year faster than
than the average NASA major projects, the majority of which are not human space flight projects.
So what's happened is the HLS system, it didn't multipass.
It didn't pass a number of key milestones.
It's not that it felled them, it just didn't reach them yet, so they're delaying.
On top of this, there's an issue with the suits as well, because the suits, they were
developed and NASA was like, hey, we have some suggestions, and every engineer knows
what happens from there.
Those are being made by a company called Axiom, I think.
NASA basically wanted like an hour, an hour's worth of emergency life support built into them.
Kind of a big deal, you know. So what I think a whole lot of people watching these videos on
this channel were waiting for and were hoping was going to happen pretty quickly. It looks like it's
It's going to be a wee bit longer before we actually see another person on the moon.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}