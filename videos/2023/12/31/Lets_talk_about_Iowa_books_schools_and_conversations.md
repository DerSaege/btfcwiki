---
title: Let's talk about Iowa, books, schools, and conversations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bttF1gtlb6E) |
| Published | 2023/12/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Iowa legislature passed a law curbing school activities, but a federal judge issued a temporary injunction against it.
- The three main components of the law are banning books, banning LGBTQ+ content, and requiring parental notification for pronoun changes.
- The ban on books, including history books and those aiding students, has been halted temporarily.
- The ban on LGBTQ+ content was described as overly broad and is also temporarily halted.
- The parental notification requirement stands because the plaintiffs lacked standing to challenge it.
- The law is seen as overly broad and unlikely to fully stand in its current form.
- Educators have a temporary reprieve, but the situation may change based on the progress of the case.

### Quotes

- "The ban on books, including history books and those aiding students, has been halted temporarily."
- "The law is seen as overly broad and unlikely to fully stand in its current form."
- "Educators have a temporary reprieve, but the situation may change based on the progress of the case."

### Oneliner

Iowa faces a controversial law curbing school activities, with bans on books and LGBTQ+ content temporarily halted, while a parental notification requirement remains standing.

### Audience

Educators, Activists, Community Members

### On-the-ground actions from transcript

- Monitor the progress of the case and advocate for inclusive policies in schools (implied).
- Support educators in navigating the legal challenges and standing up for diverse educational materials (implied).

### Whats missing in summary

Details on the potential impacts of the law on students and the educational environment.

### Tags

#Iowa #Schools #BanningBooks #LGBTQ+ #InclusiveEducation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Iowa
and books and discussions and something
that was supposed to go into effect on January 1st
that doesn't look like it's going to now.
And we will run through what occurred,
what parts are standing,
what parts have been temporarily halted,
and where everything goes from here.
Okay, if you have no idea what I'm talking about,
In Iowa, the legislature there put through a law
that it curtailed a whole lot of stuff in schools.
And it went to a federal court.
And the judge was like, yeah, no.
And put a temporary injunction in place.
Temporary being the key word.
So right now, it's not going to be enforced.
But there's this whole case that's
going to have to play out.
It really has to do with three main components.
The first is the banning of books,
because that's a thing.
The judge said that it was incredibly broad,
and it resulted in having history books removed
from schools, classics, award-winning novels,
And then, quote, even books designed to help students avoid being victims, to protect the
kids and all.
That part is not going into effect.
The whole book ban part of it is not going into effect right now.
The other part was a ban on identity, orientation, basically anything having to do with the
LGBTQ plus community. That was struck down. It's not going into effect
temporarily. It was described as wildly over-broad. The third component is the
requirement to notify parents if the student requests a pronoun change or
something like that. That will go into effect. The judge didn't actually make a
determination on that. The plaintiffs didn't have standing to challenge it.
If you're going to take something to court, it has to be something that
impacts you, and it doesn't. So that part stands. The other two, they are
are temporarily halted. This is a law that when you read through it, it is
wildly over broad. It's a really good description of it. I don't feel like it's
going to stand with the exception of the pronoun changes depending on whether or
not somebody can get a case up there that actually has standing. That may
be challenged, but even if it is, that one might stand.
That might stand.
Because the way the laws work on that, it's notifying the parent of information.
It is unlikely that that one gets struck down in Iowa.
The ban on discussions and the book ban, they're probably not going to hold up.
this case moves through, it's gonna be one of those things where this is way
too broad, it's a clear violation, you're not gonna be able to do this. But right
now, educators have a reprieve for the moment, but this is something they have
to watch because if that injunction gets lifted or something like that, they have
to comply with it until it is struck down or changed, but for the moment it's
it's going in the right direction.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}