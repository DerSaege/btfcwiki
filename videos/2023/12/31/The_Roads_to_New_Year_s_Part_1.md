---
title: The Roads to New Year's (Part 1)
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2TrXXkfaoxs) |
| Published | 2023/12/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special New Year's Eve edition of The Road's Not Taken with a giant list of Q&A questions.
- US military resilience in the face of political divide, safeguards in place to prevent collapse.
- Avoiding question about leftist folk artists due to controversial nature of their work.
- Plans to revamp charity efforts for longer-term impact by setting up endowments.
- Speculation on Trump's potential delay tactics regarding federal cases.
- Predictions on potential nominees for Republican and Democratic parties after a significant event.
- Addressing concerns about the national debt and wealth disparity in the US over the last 40 years.
- Plans and themes for future books, including a history book.
- Personal insights and traditions shared, like New Year's resolutions and holiday video traditions.
- Thoughts on polyamory, beard maintenance routine, and surprises in 2023.
- Community engagement, charity work, and grassroots movements to address political issues.
- Personal anecdotes about family, pets, and encounters with fans in public spaces.

### Quotes

- "The US military is very resilient, and there are a lot of safeguards in place for that."
- "Sometimes it's somebody else."
- "It does not bother me. I don't see it as really that abnormal."
- "Early on, I did one by happenstance and I got a bunch of messages from people saying, Hey, you know, nobody puts out content today."
- "If you travel enough, you start to realize that you have more in common with somebody on the other side of the planet than you think you do."

### Oneliner

Beau answers viewer questions in a special New Year's Eve Q&A, discussing military resilience, charity revamps, book plans, personal traditions, and more.

### Audience

Viewers and community members

### On-the-ground actions from transcript

- Contact local colleges to find connections to leftist organizations in rural areas (suggested)
- Initiate grassroots movements or join political parties to counter issues like Citizens United (implied)
- Share insights and start honest dialogues about conspiracy theories step by step (implied)

### Whats missing in summary

Insightful Q&A session covering diverse topics from military resilience to personal anecdotes, offering a glimpse into Beau's thoughts and experiences. 

### Tags

#MilitaryResilience #CharityRevamp #BookPlans #CommunityEngagement #GrassrootsMovements


## Transcript
Well, howdy there, internet people, it's Bo again,
and welcome to the Roads with Bo.
Today is New Year's Eve, 2023,
and this is a special New Year's Eve edition
of The Road's Not Taken.
We didn't have a whole lot of news that didn't get covered,
so this is a Q&A,
and it's your questions,
and it is a giant list of them.
It looks like the document is like 20-something pages long.
We'll see if this gets done all in one sitting, and we'll just kind of run through and see
what y'all want to talk about today.
Okay, if Trump is elected, will the US military fall apart over the divide?
No.
The US military is very resilient, and there are a lot of safeguards in place for that.
worth remembering that most people in the military do not have the view that
people think they do. There's a reason he needed a mob of people to do what
happened last time. It didn't happen with troops the way it would in a
whole lot of countries because there are safeguards in place and there are
always there will always be people in key positions that would thwart something like
that.
What leftist folk artists do you listen to other than David Rowlick's?
Yeah, I'm going to pass on that because most of those answers are they are of their
leftist that are more active type of leftist stuff.
I'm just going to skip over that one.
How are you feeling?
You mentioned pain.
Yeah, that's still there.
Haven't quite got that sorted out yet.
If you celebrate Xmas, what types of gifts this year for
the fam?
Do your animals get treats?
The dogs get presents.
The dogs get toys.
As far as everybody else, it is so cliche.
It's not even funny.
We're talking like, you know, Legos for the littler kids,
tech stuff for the oldie.
Like, it is what it is.
Do you make resolutions?
Care to share.
Resolutions, not so much, but normally on New Year's
I make a plan for the next year.
Like, on this, so this year, one of the things
that we want to do is kind of revamp a little bit
the way we do our charity stuff because we've realized
we can make a much longer term impact.
The streams, like the ones for the shelters,
the streams raise more than is needed to satisfy the goal.
by the goal.
And if we were to use an endowment,
we could basically do the stream once for a shelter.
And the payout from the endowment each year
would take care of it forever, like literally forever.
And we're definitely looking into that.
That is something I would like to have set up before next year.
because what we can do is basically it would be harder for the next two years because one
year the first year we would do one shelter and then have to get the actual presence for
the other one and then the following year we would basically end up doing the same thing.
But it would permanently deal with that.
So it's stuff like that.
I don't know if resolution is the answer, it's more of a plan.
The word is Trump may not even think he should have immunity, he's just trying to delay
long enough to become president and order an end to federal cases.
What are the chances he can delay that long with his appeal?
He can try.
I believe that, I believe that personally.
He can try, but I actually don't think that's...
I feel like as things start to speed up with a lot of the different proceedings, his behavior
is going to become more erratic and more honest in public, and I feel like that is actually
going to preclude him from being elected again.
Upon the death of either the dear leader or President Brandon, who will be the nominee
for either respective party?
If I had to guess, Nikki Haley for the Republican Party and maybe Gavin Newsom for the Democratic
Party.
What could you watch Leave the World Behind?
And what could you add to your survival videos to combat this especially acoustic warfare?
Sorry, this is my idea of fun and lighthearted.
I have not seen that, but as far as acoustic warfare, the secret to life is mobility.
If you ever decided to take your family and move to Ireland, where in Ireland would you
like to settle. Quirk. Quirk. For no other reason than that's where my family's from.
If Bo ever becomes president, what would your priorities be? Climate, income, inequality,
and creating the Department of the World's EMT, the Department of Peace, the Department
of something else, depending on where you heard that idea first. To be super clear,
I have no interest in that.
I follow you faithfully every day because you are some very nice things about me, thank
you.
But one thing bugs me, I can't read your t-shirts.
The top half appears on screen, but the bottom half lies below the line of sight.
Since you obviously work hard to match subject with T, is there any way to show the entire
message?
started using YouTube shopping to do this.
If you click on the little shopping bag icon that shows up in the lower left hand corner,
you'll actually get to see the full t-shirt most times.
Now it is worth noting that when I bought some of the shirts, I was very aware that
only the top half was going to be showing.
So just keep in mind, like the ones that I have that say breaking news, that I wear for
breaking news, normally there's some like super sarcastic comment at the bottom.
Those don't actually have anything to do with why I wore it.
So just base the interpretation off of what's visible.
What is your take about the fact that during the last 40 years, the national debt has increased
by 20 trillion, while at the same time, the cumulative wealth of the 1% has increased
by about 20 trillion?
I'm going to guess that's correlation.
And the reason I say that is because the 1% makes money off of government spending that
isn't debt.
So I'm going to suggest correlation, not causation.
Have you decided in what way you will publish your book?
When will it be published?
Can we expect more books from you in the future and what will their themes or genres be?
Yes, I know that's four questions, but I only used one question mark.
Fair enough.
Okay.
Yes, we have decided what way we're going to do it and I'm happy because somebody pointed
out a way that could actually avoid the issues I was concerned with.
When will it be published?
Very soon.
As in, I would have liked to have it out tomorrow.
What was the plan?
It's not going to make that, but it will be very soon.
I have seen the cover art soon.
Can we expect more books?
Yes.
What will their themes or genres be?
I know that we have mentioned we're working on a history book.
There are others, but the way nothing goes according to schedule, I'm just going to keep
that a secret until they start to come out.
And they'll just come out when they finish, whichever one.
order or plan anymore on that one.
Is there anything about the new setup that you don't like?
Not really.
I don't like this chair because it makes noises.
And the mics, it's funny because they're the same mics, but because of the way this
room is done, they're way more sensitive.
Like, because of the way I work and like large blocks of time, you know, at once, you know,
sometimes I neglect myself.
I don't eat or whatever and y'all will hear my stomach growl in a video, or at least
I can hear it.
I don't like that, but I mean the only... that would be a change in my behavior, which
probably isn't going to happen.
So there's nothing that really can be done about that.
Everything else I'm pretty happy with.
Hi, but what do you think are the skills of the future in the workplace?
I've reached out via email before, but the question doesn't seem to have ever made
the cut.
Perhaps it's apt for a New Year's Q&A.
Apparently it is.
Don't read this if you want to give a spontaneous answer.
Well, too late for that.
But if you want to comment on my thoughts so far, the skills I was thinking include
how to embrace still with the world to get slightly more progressive on a long enough
timeline, how to keep up with digitization, how to work with AI, and being more empathetic.
Yeah, I mean all of that.
I am super skeptical of how AI is going to play out, at least in my world.
The future of the workplace, because of the way it's blending, I don't think it's going
to be about specialization anymore, and not as much anyway.
You'll still need a specialty, but I think you will need to be cross-trained in other
things.
Like, I know welders that have better social media practices than I do.
So I think that's going to become more common.
Now that Christmas has passed, will the little elf go away and who decides where it moves
in the videos?
We are loyal fans and hope you and your family have a wonderful new year.
Thank you.
Yes, the elf is gone.
She has flown back to the North Pole.
Who decides where it moves in the videos?
Normally me.
I normally move her right before I make a video.
Sometimes it's somebody else.
And like, and it's funny you say that because she may be back there right now, because for
whatever reason there's a bunch of the Easter eggs back there right now, I'm assuming that
there's something coming up that will explain to me why they're all back there.
How do you break up with a friend?
Is that even something you need to do as an adult?
My friendship with this guy has evaporated over the course of the past few months.
he looks like you, aged down 20 years.
Ouch, ouch.
So, and by that, you mean he looks five, right?
And he thinks he's the authority
on all foreign affairs matters in existence
because he's a Reddit mod.
It's honestly completely insufferable.
I'm just tired, I don't know how to explain
that I center civilians and that when elephants fight,
the grass gets trampled.
Yeah, I mean, you explain it.
just like that. It's not something I can hide. I'm sorry, I'm Latino. It's not something I can hide.
So his abrasive condescending explanation of why things have to happen the way they do is disgusting, dehumanizing,
and demented.
If any of the text convos we've been having about this had taken place in person, we would have come to blows.
Is it okay to ghost people as an adult? Yes.
Unless you have set out and created some special relationship where you have agreed to provide
somebody access to you, you don't owe anybody your time.
And I can get, I get this because, and this is for me, it's torn because half the time
when I'm making videos, I'm like, this is why it's happening this way.
But I would hope to think that the people watching understand that I'm not happy that
it's happening that way.
But I have a lot of friends that foreign policy is their thing.
And there are a whole lot of them who view the way it is as the way it has to be.
So I feel you on this.
And yes, sometimes it can get super heated.
But yeah, you don't have to have people who annoy you in your life.
What is your preferred way to deal with topics that have been poisoned by conspiracy theories?
I have been making great efforts to deconspiracy my life,
but unfortunately, I seem to have reflexively become
very hostile to certain subjects.
For example, my fiancee didn't know
what bohemian growth was
and ran into a conspiracy TikTok about it.
And I immediately went on high alert
when she started talking about it.
Thankfully, she looked more into it
and found more debunking than conspiracy,
but it just as easily could have gone the other way.
And I feel my initial reaction was the kind
that would not have made that conversation
as constructive as the one we did have.
Thanks for everything you do.
I hope the tour gets going this year
so me and her can come see Garage Man,
her nickname for you.
I tend, when you're talking about conspiracy theories, especially those that are widely
held or are based on a misunderstanding, or even more so those that have some grounding
in truth, slowly, step by step, tends to be the best way to go through it.
It takes remaining calm and taking the time to go step by step through each piece and
explain which parts are real, which parts aren't, what they really mean, and does it
mean that lizard people rule the world?
What is your beard maintenance routine?
Also hedge or string trimmer for the springtime trim.
my beard maintenance routine, Dove soap.
And then, like now, because of some messages
and comments from people who need to read lips,
I try to notice when the mustache starts to grow
over my lips and make sure I take care of it then.
But it is very, it is very haphazard.
What are your plans for your beard in the new year?
I'm actually right at the point
it's probably time for a trim.
As a former educator, one of the best parts of the job was hearing surprising insights
from young people.
Can you share any examples of insights you've learned from young people you know?
That's a whole video.
I think one of the, also depends on what you mean by young.
I think one of the things that I have taken away the most is the desire for consistency
in everything across the board
and that is it's a
my uh... my generation didn't have that we weren't looking for that
and this generation is and i think that it's uh...
i think it's useful
what's your favorite board game
card game and why
i like all board games
and card games, especially trivia games.
I like board games because you can learn a lot about a person
while playing board games.
And I don't just mean games you would think of like chess.
You can learn a lot about somebody playing Monopoly.
So is there a musician you admire
for non-musical reasons?
who is the musician, and what are the reasons.
I like any of them that put their money
where their mouth is, that's big with me.
What is the best way to seek out mutual aid
and other groups to give my time and effort?
I'm fairly new to a remote area in New England,
in a New England state, and I am finding
that the nearest leftist organizations are many hours away.
Previously, I've lived in a more densely populated state
and have been there long enough to have developed
community connections.
Here, I've tried online searches, social media searches,
but have not found any groups.
Colleges, look for colleges, start there.
Even if they are a ways away,
most of them have connections to other groups in more rural areas.
That's where I would start. What do you think is your demographic breakdown by age, gender,
political persuasion? About a third of the audience is under 34, then it's like 34 to
34 to 45 is another third, and then the other third is above that.
So it's pretty evenly spread out as far as age.
Gender is 60% men, 40% women.
And politically it's 15% right, about 50% liberal, and about 35% left.
Do you have any plans to travel outside the U.S. in 2024?
And how do you handle meeting random fans
when you're out and about?
Are they generally positive experiences?
No plans on outside the U.S.
We're still trying to get the U.S. stuff together.
Um, it's always been positive.
You know, there have been moments that have been awkward,
but it's never been bad.
And yeah, I think, so I feel bad.
And so I hope this person's watching this.
I was at a restaurant in Pensacola
and I was meeting somebody
and we were having this conversation
and this person came up and they're like,
I think you're on YouTube.
And I'm like, yes, yes.
And you know, and we talked for a brief moment
And I turned back around to talk to the person
I was there with.
And it was part of a pretty in-depth conversation.
And I completely forgot the person was still there.
And I felt really, really bad.
Because when I turned back around
to say something to them, when it popped back in my mind,
they were gone.
Like, that so far has been the worst experience.
And it was 100% my fault.
I really enjoy the community on your Discord.
I saw you have a channel for pen and paper RPGs.
Are you a gamer?
Was that by Patreon request?
If you do game, what's your favorite?
So I, you probably know this by now.
I am almost never in the discord that is that's for y'all and I feel like nobody is going
to agree with every one of my videos and if I'm in a a space that is for that that is
united by watching those videos there's going to be discussion I feel like if I was in there
it would upset and derail honest discussions about it, so I tried to stay away as much as
humanly possible. So that is definitely something that was done by the community.
Over the last two years, I, a 35-year master gardener, have had less and less luck after
growing the first anything save for flowers. I read up on regenerative gardening, also
spoke with a neighbor and his usual crops have lessened too. He's tried two slightly
different methods the last three years but nothing seems to help. Is this a never ending
trend at this point? There are fewer bees, so less flowers too. Please do a video or
or two on this.
So yeah, we probably will.
One of the things, and I did one recently,
the growing zones are changing.
The thing to remember about that,
that's just based on temperature.
Remember that the soil is different too.
So there's going to be a lot of adjustments.
As far as whether or not it's just a never-ending trend
at this point, I don't know.
Um, but this is something that's on my radar.
I'm turning 24 on New Years in a cheeky attempt to make you feel old.
What were you up to leading up to Y2K?
Were you preparing for the end of the world?
Leading up to Y2K?
No, I was preparing for a 230 mile canoe trip.
That's what I was doing.
Sometimes.
If you were to hypothetically leave the U.S. if x, which are the top three countries you'd
seriously consider moving to?
Canada, Ireland, or somewhere where the central government is so weak if need be, it could
easily be realigned by a minimum number of people.
or ignored outright?
I have traveled and read so much that I seem only to notice similarities of people and
cultures.
What was the most surprising person or place that you found to be exactly the same as everywhere
else. I think one of the most surprising, I've spent a little bit of time in like
the deep, deep Louisiana swamps. Like we're talking about places where mystical
beliefs that they are not a joke. And one of the things that I found the most
interesting was that it wasn't any different. Like everything was the same.
It was just a different, I mean obviously different dialect, all of that stuff, but
But the general attitude was very, very much the same.
But yeah, if you travel enough, you start to realize that you have more in common with
somebody on the other side of the planet than you think you do.
What country is on your bucket list for you to visit for a vacation?
What would you most like to see and or do most in that country?
I want to go to Egypt.
Pyramids.
Why did you walk away from O'Mellus?
I don't know that I did.
But I guess the answer would be a search for whiter joy.
Have you ever read the Discworld books or Good Elements?
No, I have not.
Do you have any irrational fears from childhood that no matter how much critical thinking
practice you've had, you just can't shake them, shake it or them?
None from childhood.
Yeah, none from childhood.
I bet you're pulling for actual rural broadband.
What is the first thing you'd do with a reliable,
high speed and low latency connection?
More streams, more streams here.
How much wood could a woodchuck chuck?
If a woodchuck could chuck wood?
700 pounds, Google it.
Aside from the pale moonlight, what's your favorite Star Trek
episode?
There's a lot of them.
Like, I like the ones that parallel stuff in the US.
I like the ones about unionization.
I liked the one from DS9 where they came to Earth
in a different period in time.
I like the ones that teach a lesson.
If I have to pick one just off the top of my head,
the one where they did the Dred Scott case using data.
I like that one.
OK, what are your thoughts on polyamory?
I really don't.
I don't really have any.
It's what people do that makes them happy.
When it comes to consenting adults
and how they structure their relationships
and all of that, I don't have a lot of hangups
when it comes to stuff like that.
It does not bother me.
I don't see it as really that abnormal and I don't see it as something that is ever
going to be accepted as a norm.
I'm sure that didn't make any sense.
I think it has a lot to do with the level of honesty that people can actually have with
each other and being honest about what they really want in life.
So for some people that's going to be the route, for others it isn't.
When it comes to affairs of the heart, I am super...yeah, whatever makes you happy among
consenting adults.
It is not something that I spend a lot of time thinking about.
What have the horses been up to lately?
Any fun stories to share?
Now they've been lazy, just hanging out there being in lawn ornaments.
Not really, actually.
They haven't done anything weird.
They haven't tried to hurt themselves.
They're just hanging out.
It's kind of nice, actually.
If you had a time machine, who or when would you go visit?
Wow.
Well, I'd go to, uh, like, everywhere.
I mean, you understand, like, the power that that would have, right?
Like, you don't have to choose one thing, because you can just go and do it again.
You would essentially have unlimited time for learning.
Do you have any seasonal traditions or foods that you look forward to all year?
And you can absolutely do it other times like have turkey and dressing, but you don't because
it's part of the tradition.
Part of the tradition is that thing being only for the season.
Pumpkin pie.
You do a lot of disaster response.
Can you comment on Team Rubicon?
I don't have any first-hand experience with them.
I know some people that do, and from what I have heard, they do good work, and the only
real complaint I've heard is like there's a lot of ego which I mean yeah of course there is I mean
like that that that's not a surprise. If you don't know it is a a group that that does a lot
of disaster response and they they also have like a side mission of trying to help reintegrate
veterans. So there is a lot of I know what I'm doing type of thing from what I understand,
which I mean, it is what it is. But I don't have, I don't have any first-hand knowledge and
the biggest complaint I've heard. And normally the complaints from my circle about a group are
about their effectiveness. So if there are no complaints about that, they probably do
pretty well at the actual job.
Tell us a story about one of your children using your own words back at you.
I remember hearing one over the years, but there must have been other examples.
Anytime my kids want to get involved in an activity that is some way beneficial to anything
aside from themselves, don't I have a responsibility?
And you can't say no, you know?
They just automatically win.
So now I've realized they probably spend more time trying to figure out how to frame whatever
Whatever it is they want to do as something to help other people, then they probably should.
We don't have a question, but maybe you could do a showcase of all your different t-shirts.
My husband is 86 and usually tells you on the TV to step back so he can see the shirt.
Oh, see I didn't think about it on a TV.
I don't know if you can use the little YouTube shopping button.
I don't even know if it's there, I don't know how that works.
People have asked for that.
At some point I'll do that, I'll like flip through all the shirts or something.
I know you say general traditions are peer pressure from dead people, but do you have
any traditions throughout the year that you've created for yourself besides the livestream
charity drive?
Yes, on holidays we do long videos, try to do them on every major holiday and it's because
early on I did one by happenstance and I got a bunch of messages from people saying, Hey,
you know, nobody puts out content today.
I was alone and it was fun to have something lengthy and the first time I did it, it was
completely by accident.
But it generated so many messages thanking me for an accident that it became a tradition.
It became something that we do.
You never seem to be surprised by things.
Was there anything in 2023 that really surprised you that you didn't see coming?
George Santos, like as a whole, like everything.
when I didn't see any of that happen. Let's see, I donate to Peace Action and
talk with those guys pretty often. The money in politics and the military
industrial complex are always the fuel for our hawkishness. I am struggling with
cynicism at the moment and am getting so exhausted by the stop the bleeding harm
reduction tactics that seem to be our only way to dampen the system of
of suffering caused by the root condition of corruption, is there any real movement effort
organization that is seeking, that is making a serious attempt to correct Citizens United and
fix this issue that seems to be the origin of all of political problems? It's not the origin of all
of them, but it certainly doesn't help with any of them.
As far as actually countering that, you're going to have to build a movement that is
grassroots that is widespread.
And by that I mean you're going to have to work to put people in a political party or
start one from scratch and build up and it has to start at the bottom.
See all the videos about third parties that we've done.
If you want real real movement on that, that's the way it has to go.
There are a whole lot of issues that are going to have to be done that way and people people
don't want to do them because it's super hard and it's years long.
they're looking for a a more immediate answer and it's just not there. What did
you do to the bows of the first through the fourth columns? We don't talk to
each other, we're compartmentalized. While everyone is distracted by the UFO, UAP
disinfo saga in the US, what myth, legend, biblical tale would you most like to find
out is real in 2024?
The idea that all of the legends and origin stories of different cultures that are all
around the world that include being created from mud or crawling out of the ground or
or something like that, really had to do with the fact that all of this has happened before
and everybody went underground and came back out.
That's one of the coolest, like, wild theories that I like.
Okay, let's see where we're at here.
I am an avid listener of your program and respect you very much.
I actually have had a lot of questions for you over the last couple of years, but one
that keeps the back of my mind, are you a something I can't talk about?
That's a big no-no.
I will send you an answer to that.
I'll send you an answer to that.
But that is not something that you can discuss on YouTube.
How would you imagine the world could look like for an 18-year-old 20 years from now?
That's hard to say.
Look back 20 years ago and see how much has changed.
It depends on whether or not we make any real progress when it comes to climate, when it
comes to the rising tide of authoritarianism.
Those are the real hangups and those are the things that are going to decide whether twenty
years from now looks more like Star Trek or Mad Max.
What's something new you want to learn about in 2024?
What has sparked your interest but you haven't had a chance to look into yet because of everything
that's going on?
communities, charities in general, and structuring endowments properly.
Can you please describe how you came up with the name Bow of the Fifth Column?
So years and years ago, this is probably 10 years ago, the, maybe not that long, yeah,
no, that might be right.
There were a whole bunch of us who were indie journalists who had our foot in the door at
larger places. And the idea, there was a lot of, quote, gatekeeping going on in journalism
at the time, keeping people out that didn't have the right credentials, so to speak, even
though they could do the job, had the skills. There is a story about the fifth column. There's
a general, he's approaching a city, one column coming from the north, south, east, and west.
there's a fifth column inside the city ready to open the gates.
So there were a whole bunch of us that used that term and it stuck with me.
And Bo's a nickname.
That's where it came from.
Okay, let's see.
Wall Street keeps talking about recession.
The numbers just don't justify any of that.
Why are they doing that?
And how does it help rich people to say these repeatedly two years now?
If I had to speculate, it keeps other people out of investing.
I don't know that there's some grand scheme behind it, but if I had to guess a reason,
that's why.
Okay, so let's see here.
We are almost halfway through.
We'll try to go a little bit further, and we're definitely going to be taking a break
on this one.
Could you tell us a little about the rabbits?
The rabbits have now all, assuming you're talking about the ones in the house, the rabbits
have now all found forever homes.
And so there's that.
If you're talking about the ones outside, oh, they're fat and happy off of my garden.
I have a good one that I'm still shocked all the lefty commentators missed and Bo never
brought it up once either.
liberal shill you. But this will hopefully give everyone a good laugh and
there's a link but I can't I can't read the link obviously. Michigan Republican
Party Faces Financial Turmoil Bank Record Show is in the is in the link. I
actually did talk about this when all of the the GOP parties started tanking
financially. I think I focused more on the ones in the Southwest now, and this
may have just gotten mentioned in those. Yeah, if you don't know, there are some
real financial issues at the at the state level in the Republican Party. Let's see.
How do you keep yourself grounded with the research constant news and the noise
of the world you keep up with? My kids in the ranch. That's really the the part
that really keeps me grounded, I guess.
Let's see.
Hey, Bo and friends.
Wondering if you're ever going to go off the road,
have a town hall, Bo chats on the couch type of thing.
Yes, that is something that we would like to do.
That's actually the whole reason this channel exists.
And we've just, we're not in a place
where we can break away from what we're doing.
We have to get all of the other projects finished up before we start that now.
Okay.
Have you been doing your viewing homework?
Have you watched any Babylon 5 yet?
No, I have not.
I have not.
Okay.
How did Sonic and Mario get to the Olympic Games anyway?
That's probably a meme I'm not aware of.
I know that there was a thing where the two companies cooperated, I think, to create one
game where it's in competition.
I don't know.
What's really behind the GOP's opposition to approve additional funding for Ukraine's
fight against Putin's aggression, and what calculations might be influencing their stance?
I think it's just for most of them.
I think it is just normal.
The Democratic Party wants to do this, so we want to oppose it.
And they don't have a good enough understanding of foreign policy to understand exactly how
damaging they are being.
And they don't care about the lives.
I have to ask, is the old injury that's been acting up your werewolf bite?
No, no, no, no.
That was pretty minor.
And for those who don't know, that's a Halloween decoration thing, not an actual werewolf.
I wasn't hanging out.
Never mind.
Let's see.
Are there any pictures of your pets we can see?
Not with me, but we have videos where Baroness is in one, and we have Marilyn, of course,
the one where she's just kind of hanging out.
But we'll try to put together some more.
Okay, so we are going to take a quick break and then come back and do, I guess, the other
half of these questions.
All right.
Let's see.
That looks like this.
This does seem like a good stopping point.
Yeah, so a little bit more information, a little bit more context, and having the right
information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}