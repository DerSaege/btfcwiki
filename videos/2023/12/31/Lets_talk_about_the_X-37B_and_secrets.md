---
title: Let's talk about the X-37B and secrets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Q4NZc6SocQo) |
| Published | 2023/12/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the X-37B, a secretive space plane resembling a mini space shuttle.
- The recent flight marks the seventh known mission since 2010.
- The program started in 1999 as a NASA DARPA project, later transferred to the Department of Defense in 2004.
- The X-37B has been orbiting for over 10 years with two separate vehicles existing.
- Despite public-facing experiments, its true purpose remains highly classified.
- The current mission's details, including its duration, are undisclosed.
- Speculations suggest it could remain in orbit until 2026.
- The program's secrecy goes beyond mere human interactions, with its technology being tightly guarded.
- Beau believes the reveal of its purpose will be as impressive as stealth technology.
- The level of secrecy surrounding the X-37B is unparalleled, leaving outsiders to only speculate its true mission.

### Quotes

- "This is a piece of technology that is kept very secure."
- "Nobody has a clue what it does."
- "Whatever it is, very much nobody gets to know."
- "They are keeping the secret very, very well."
- "It will be something along those lines but we don't know."

### Oneliner

Beau delves into the secretive world of the X-37B, a space plane shrouded in mystery and speculation, leaving all to wonder about its true purpose and potential impact.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Research more about the X-37B program and its history to gain a deeper understanding (suggested).
- Stay updated on any public disclosures regarding the X-37B to uncover more about its missions and experiments (suggested).

### Whats missing in summary

Exploration of the potential implications of the X-37B's secret technology and the importance of maintaining awareness about classified space programs.

### Tags

#Space #X37B #SecretiveTechnology #SpaceEnthusiasts #MilitaryTechnology


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the X37B
and secrets and what this thing is.
Because we got a question about it because it just went back up.
So this is a unique thing because, well, you'll find out.
OK, here's the message.
Can you do a video on the X37B?
It's a space thing, so you'd probably be interested.
They keep saying it's secret, but don't give any hints.
You always put stuff like this out there and are generally right.
What is this thing?
Okay, so about a year ago, I put out a video on this when the last one landed.
But it's worth going over again.
So what is the X-37B?
It's a space plane.
unmanned, it seems. At least it can be unmanned. Maybe there's space for people. We don't
know. It is a super secretive space plane. It looks like a little baby space shuttle.
The one that just went up, this is the seventh flight of the program that we know about since
It started flying in 2010, since we know about it starting to fly in 2010.
It has spent more than 10 years in orbit.
It seems like there are two of them.
There are two separate vehicles.
The last flight lasted two and a half years.
The current flight, the one that just went up, there are no details on the mission to
to include its length.
The guess is that this thing is going to stay up until 2026.
But it's a guess.
Make no mistake about it.
In the previous video, which I'll put down below, I list some of the public-facing experiments
because this is something that definitely appears to be a two-tier thing, which means
you have something that is public-facing that you're allowed to talk about and then
something else that that is very secret and it is so secret that they have
provided a a public-facing aspect of it to satisfy people's curiosity but make
no mistake about it they're definitely doing something beyond testing the
effects of gravity on seeds which is one of the experiments but I'll put that one
down below. Another thing to know about this thing is that, yeah, it started
flying in 2010. The program itself started as a NASA DARPA thing back in
1999, transferred to DOD in 2004. This thing's almost a quarter century old.
Nobody has a clue what it does.
This isn't one of those things where it's secretive and it has to do with human interactions or something like that.
This is a piece of technology that is kept very secure.
This is a long time for people to not really know much of anything about what
it does. I feel like once it is revealed it will be as as impressive as stealth
technology. It will be something along those lines but we don't know. Maybe it's
just a really cool scientific research space plane that, you know, they have just absolutely
airtight security on for some reason.
I'm guessing that's not the case.
But I don't think that anybody outside of aviation circles could really even hazard
a guess and based on what I know it's gonna be a guess because whatever it is
it is like very much nobody gets to know so I don't really know much beyond
what's in public reporting on this there there's they are keeping the secret
very very well. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}