---
title: Let's talk about stats, rates, correlation and causation...
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mWTcHWLHQeg) |
| Published | 2023/12/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reports a 12% decline in the murder rate, which is significant and might be a record.
- Mentions that despite surveys showing 77% of Americans believe crime is increasing, the numbers indicate otherwise.
- Points out the influence of media coverage in shaping perceptions of reality.
- Addresses statistics claiming 13% of a certain demographic (Black people) are responsible for 52% of murders.
- Disputes the accuracy of these statistics, explaining that they only represent cleared murder cases, not all murders.
- Raises concerns about missing data from unsolved cases and individuals who simply go missing.
- Challenges the notion that skin tone is linked to likelihood of committing murder, citing economic factors and poverty as more relevant indicators.
- Suggests a correlation between income inequality, poverty, and crime rates leading to higher murder rates.
- Poses questions about the impact of economic issues in 2020 and subsequent government relief efforts on crime rates.
- Encourages viewers to think critically about causation versus correlation in data interpretation.

### Quotes

- "It's weird."
- "The only interesting thing about it, when you actually get down and start going through the information, the real disparity I see is that people with brown eyes are far more likely to commit a murder."
- "100% of people who do not know the difference between correlation and causation will die."

### Oneliner

Beau breaks down misleading statistics on murder rates, challenges preconceptions on demographics and crime, and underscores the importance of understanding causation versus correlation.

### Audience

Data Consumers, Critical Thinkers

### On-the-ground actions from transcript

- Fact-check statistics before accepting and sharing them (suggested).
- Advocate for accurate representation of data in media coverage (implied).
- Support initiatives addressing income inequality and poverty to reduce crime rates (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how statistics on murder rates can be misleading and the importance of considering socioeconomic factors in understanding crime trends.

### Tags

#Statistics #Crime #Causation #Correlation #MediaCoverage #IncomeInequality


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today we are going to talk about all kinds of things.
We're gonna talk about some statistics.
We are going to talk about some statistics
that are used that aren't really accurate.
We're going to talk about causation and correlation.
We're going to, I mean, the list just goes on and on today.
And it's all one subject,
but it all ties together very neatly.
OK, so we'll start off with the news, the news that prompted
all the rest of this.
OK, so it looks like there is going to be a 12% decline
in the murder rate this year.
12%.
If you don't know, that's huge.
That's a pretty big decline.
It might be a record.
It puts it on par with, say, 1960, pretty close to that.
A little bit more, but pretty close.
And it's weird.
I imagine there's going to be people who have a hard time
believing that, because in surveys,
it says like 77% of Americans think that crime's going up.
But realistically, when you look at the numbers,
what you find out is that unless you own a Kia or a Hyundai,
car thefts, pretty much everything else is going down.
Shows how important media coverage is, and how it's
covered, and the information that is put out, and the
frequency with which it is put out.
It skews people's view of reality.
Along those lines, we're going to talk about a different set
of statistics.
And they surface every so often.
I know I've done a video about these stats in the past,
but it's been a few years and I just saw them
coming up again, and that is that 13%
of a certain demographic are responsible
for 52% of murders.
That's what they say.
And the 13%, that demographic, that's black people.
I mean, that they're responsible for that many murders
And they get this number from the Department of Justice.
But it's wrong.
It's wrong in the way that it's presented.
Because that's not of all murders.
If you go look at the statistics,
that's of cleared murder cases, solved ones.
Solved ones.
And I know because of shows like Law and Order and CSI,
people think that most of them get solved.
They don't.
The most recent year I could find numbers for was 2021.
And it was about 51%.
So off top, before we get into the other stuff,
you're missing half the data set.
Doesn't seem like high IQ to use stats that are missing half
the information, right?
That would be silly.
And then when you really think about it,
it's even worse than the 51%.
because that's only cleared cases.
For it to be a cleared case, that
means a case had to be opened.
So those people that just went missing,
they're not included in that.
This data set misses the majority of cases.
Not exactly definitive.
More importantly, it seems like it'd
be worth noting that the people who aren't figured into the survey, those who
weren't caught, are the ones you would need to worry about, right? And I mean
let's be honest, when you're talking about, you know, people just turning up
missing people in small towns that are watching this video, you're you're
snickering, right? Because everybody in every small town knows that one story
about that guy who did that thing that was wrong to somebody he shouldn't have
done it to and then he just left town. He was obviously scared old dude was gonna
catch up with him. Nobody ever hints at the possibility that old dude did. So
these numbers, they're garbage. They're not good stats. The only interesting
thing about it, when you actually get down and start going through the information, the
real disparity I see is that people with brown eyes are far more likely to commit a murder.
It's wild.
And if you think that's silly, that's kind of the point.
I mean, imagine thinking that pigment would make somebody
be more likely to be a killer.
It's the difference between causation and correlation.
And it's important to know the difference,
because 100% of people who do not
know the difference between correlation and causation
will die.
If you didn't get that, you really
need to pay attention.
OK, so if it isn't pigment, what might account for this?
There is no causative link between skin tone
and the likelihood of killing somebody.
That's not actually a thing.
So you'd have to think about other things.
See, what happened was in 2020, there was a giant spike in murder rates.
They went up, ran kind of flat through 2021, and then dropped to 2022, and then 2023 again.
Big drops.
It's weird.
So it spiked in 2020.
What was happening then?
Oh, yeah, yeah, a whole bunch of economic issues, right?
bunch of economic issues. There's not a causative link between skin tone, but
there's a whole lot of evidence that suggests that there might be one when it
comes to income inequality, poverty, and crime. And crime then feeds into murder.
So is there anything that occurred that would have addressed that from 2020 to
now. Maybe all that money that the federal government pumped out as COVID relief might
have something to do with it anyway it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}