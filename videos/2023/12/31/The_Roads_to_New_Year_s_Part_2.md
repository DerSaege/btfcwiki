---
title: The Roads to New Year's (Part 2)
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wysG70qaE8s) |
| Published | 2023/12/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New Year's Eve special edition Q&A with a variety of questions.
- Views on books and movies that haven't aged well.
- Thoughts on Tesla and Elon Musk.
- Height revealed, jokes about being a "short king."
- Advice on campaigning in a small city with limited funds.
- Social media usage and waiting for Twitter to crash.
- Encountering stickers on Tesla cars.
- Detailed explanation of gun ownership progression.
- Inspiration from various individuals rather than having one hero.
- Views on implementing laws like the Fairness Doctrine.
- Favorite movies and apps, including a recommendation for "Man from Earth."
- Availability of t-shirts for sale with potential link.

### Quotes

- "I compare them to shoes. They're for different purposes and different roles."
- "If we can't, we have to teach people good information consumption habits."
- "I don't think it is time to worry yet. I think it's time to work."
- "That sounds like it would either be delicious or just way too overwhelming."
- "Having a plan like that doesn't hurt."

### Oneliner

Beau tackles a range of questions, sharing insights on everything from gun ownership progression to campaign tips for a small city with limited funds, all while maintaining a light-hearted tone and engaging with his audience.

### Audience
Community members

### On-the-ground actions from transcript

- Research and find an issue to campaign against opponents (implied)
- Educate others on good information consumption habits (implied)
- Prepare an exit plan if part of a marginalized community (implied)

### Whats missing in summary

Insights on the importance of community engagement and staying informed to navigate uncertain times.

### Tags

#Q&A #CommunityEngagement #GunOwnership #CampaignTips #InfoConsumption


## Transcript
Well, howdy there, internet people.
It's Beau again, and welcome to the Rhodes with Beau.
This is our New Year's Eve special, part two,
because there are that many questions.
This is a Rhodes Not Taken special edition Q&A,
and we are answering a bunch of your questions,
so we're going to get right back into it
after the break here.
Okay, so how many t-shirts do you own?
I have no idea.
earthly clue. Three Xerox full. A lot. What is your feeling on books, movies that
haven't aged well? Can you enjoy things from the past for what they were at the
time or do you generally just avoid them? Example, The Sopranos, older books. Most
times I can still enjoy them. There are some that have like incredibly cringy
moments, you know, especially if you didn't pick up on it initially. Because
it's not just that the the film or the book didn't age well. It shows that you
were not there yet either because you didn't pick up on it before. I think
That's why most people avoid them.
For me, change is a part of it.
I would hope that most older films have not aged well.
It shows that we are moving forward.
I saw an item that Tesla is recalling.
All Tesla's sold in the US to adjust the self-driving computer
programming.
Do you think they could also recall Musk?
If that's a serious question about Tesla's relationship
with Musk, eventually they're going
to have to examine it if things keep going the way they are.
You know, I recently saw, in real life,
saw one of those stickers that says,
bought this before I knew who he was, on a Tesla.
There's a lot of negative press with Tesla
and because of Musk's Twitter antics.
How tall are you?
I feel like you might be what the kids call a short king,
but there's probably a chance you're a giant.
Either way, I'm 62 and I'm pretty sure if we'd met,
we'd be looking eye to clavicle one way or the other.
I am 5'11.
I don't know if that qualifies as a short king or not.
Several residents of my small, mega-centric Southern California city are trying to recall
several long-standing city council members I'm thinking of running to represent my district.
Any tips on campaigning in a small city with limited funds?
Find an issue that all of your opposition is wrong about and that is important to the
people of the district.
Find something that they haven't addressed, that they're not going to address, that their
opinion is in direct opposition to a majority of the base of that voting populace.
And that's your platform.
That key issue is what you talk about over and over and over again.
Will you or your wife please join threads?
Or have you both given up on posting to any social media?
I'm assuming you no longer use what was once called Twitter.
Yeah, I don't use Twitter anymore.
I'm kind of waiting for Twitter to completely crash and then I'm just going to go where
everybody else does.
My son is looking to arm all his gay friends because he thinks civil war is sure to come
and it will be rednecks against gays.
What about the gay rednecks?
They exist.
I thought it prudent for a cute guy in a skirt and cat tail ears to, driving around in the
middle of the night in a Honda Civic with his husband to carry a handgun.
He did this and joined the gun club.
then one handgun has taken root and propagated. What is it with guns? Why does this have the
feel of addiction to it? Why do people fondle their guns? He keeps trying to get me to join
this mindset. You can probably guess my response. Okay, so this is what happens. People buy a pistol.
They buy a handgun and then they kind of immerse themselves in that culture a little bit and they
they start thinking of worst case scenarios.
Eventually, they realize that the purpose of a sidearm
is to get to their rifle, but they don't have a rifle.
So they go out and buy one.
This is normally where people end up
getting an AR or an AK or something like that.
Then they realize, well, that really
doesn't have the range.
For some reason, people, even in areas
where there is no clear line of sight for 700 yards,
they want to go out and find a rifle
that they can accurately use at that range.
So then they end up with a long range rifle.
They realize they probably need a shotgun too,
and that's what happens, and it just keeps going.
And once they're finished with that core set,
they say, well, you know, the first handgun I got,
it really wasn't what I wanted.
And then they go find another one.
And that's how people end up with just dozens and dozens
and dozens of them.
I compare them to shoes.
They're for different purposes and different roles.
So yeah, it's not actually uncommon.
Who are your personal heroes and who inspired you growing up and or later in life to become
the person you are today?
It's a list that is, that's a list.
There are a whole bunch of people who I look at as inspiring in one way or another.
And one of the problems with creating a list of somebody that you view as a hero is that
nobody should be your hero because nobody's perfect type of thing.
As far as people who inspired me, it runs the spectrum.
I like to pick and choose traits from people.
I don't look at anybody and try to find somebody who is perfect in every way.
What would it take to implement and or bring back laws such as the Fairness Doctrine?
They need to get a hold of all the misinformation in the media.
Even YouTube channels that amass a certain number of followers should have some responsibility
of what they put out on the airwaves.
Your thoughts?
I don't think laws matter here.
It's education.
We have to teach people good information consumption habits.
The thing is, you have this issue because it sells.
People aren't accidentally, most people aren't accidentally putting out a bunch of bad information.
They're putting it out because it makes them money and it gets the revenue.
If we can educate people to the point where they have good information consumption habits,
there wouldn't be a market for it because people will feel like this is garbage and
stop watching it.
I think that would be far more effective than any legislation because there's always going
to be a way around the legislation.
Do you have a favorite action movie, comedy, I know you like The Fifth Element, is that
your favorite sci-fi film?
Um, I don't know that, see, it's the favorite thing that I have an issue with.
Is The Fifth Element my favorite sci-fi film?
I see, I like underrated stuff, but yeah, I mean, I do like The Fifth, that's why all
the movie stuff's back there.
Okay, sorry, all of the Easter eggs back there from earlier in the first part, I was commenting
like I didn't know why they were back there.
This is part of it, I'm sure.
As far as sci-fi, that's good.
There's a movie called Man from Earth, and the entire premise is a college professor
is leaving a college, and there's like a going away party, and there's a whole bunch
of people in the room, professors with different areas of study, and he basically says, I'm
immortal.
I've lived forever.
And he has like an anthropologist, all of these different people in the room who try
to poke holes in his story.
And I find that one incredibly interesting.
What are the phone apps you use most?
The YouTube ones, obviously.
You know, the normal one, Studio, Creator, all of that stuff.
Can you sell some of the really cool t-shirts you wear, like, DeTrumpification?
Also, how's the white horse who is so in love with you?
I can't remember her name, Marilyn.
Marilyn is fine, she's right outside now.
Let's see, as far as the t-shirts, those are for sale, actually.
There should be a little bar below if you're watching on YouTube, and you can find them
there.
I'll put a link in the description in case they're not there for whatever reason.
Normally, in a lot of them now, there's a little shopping bag icon.
I said this in the first part, too, but there's a little shopping bag icon that will allow
you to click on it and it'll take you to wherever you can order it from.
What do you expect the best thing to happen will be in 2024?
I think a lot of long running stories will come to an end, finally.
would be nice. Is this a specific one? Yeah, I don't know. I don't know. Maybe soon I'll
do a things I expect to happen this year type of video and we'll see how it plays out.
Are you familiar with Tommy Smothers and one of the most famous comedian cancellations
of all time?
Nixon pushed them off CBS for their work opposing war in Southeast Asia.
I'm actually not, but that sounds interesting, I'll look into that later.
Thought of Bo the other day when I saw a card with Teddy Roosevelt on a T-Rex.
the carpet, walks off and carry a big stick, and ride a T-Rex.
T-Rexes are cool.
TR on a TR.
That would be a very TR thing to do.
So there's somebody.
Do not get me wrong, Teddy Roosevelt had a whole lot of
faults, but there's also a lot you can be inspired from
there.
And that's what I mean as far as it's hard to pick a hero or
something like that because there are a whole lot of things that Teddy Roosevelt
did that definitely disqualify him from that title if you are looking at it
through the lens of well I mean in some cases I mean we're just talking about
basic stuff today but there are elements in everybody so this would be a fun
One thing to share.
What can Europeans do to support democracy in the United States?
The stability of our democratic systems is also negatively affected if things get worse
for you.
We need to have critical mass as democracies in the world, as democracies in the world,
where authoritarians grab power.
There's a typo in there.
Okay, what can you do?
Use your social media wisely and elevate pro-democracy messages.
while everybody is focused on Trump in the U.S., there's way more to it, it is
spreading, and it's something that we're going to have to deal with for a while.
This is not going to be easy.
The moment for it to be easy would have been if Biden had a decisive win last time.
That would have been, that probably would have been a more effective blow against
authoritarianism in the US. So if I should succeed in my New Year's
resolution of winning one billion dollars, how would you suggest I spend it
to have the most impact to do good in the world? I would set up one of those,
Because they've been doing these studies, non-profits have about giving free cash, just
like personal UBI to people, and they have had like remarkable results.
That may be a way to do it.
Something I've always been curious, or I've been curious about for a while, how do you
categorize and track your t-shirts?
Do you have a database or Excel spreadsheet that you use to keep track of them all?
No, they're just hanging up there.
Sometimes it takes a while to find the one I'm looking for.
Would you ever consider doing a TED Talk?
What would it be about community networking?
I truly think that the real solution is building bases of power at the local level that are
not dependent on leadership from the state capital or DC or something like that.
Being available in the area means that you know the issues a little bit better.
What is a good book that gives a fair and partial overview of the Middle East situation?
I'm trying really hard to stay out of the debates because I see far too many otherwise
good people getting completely or maybe rational people getting completely irrational about
the issue and I want to know facts rather than biased opinions.
There's not a book.
There's not a book.
You're going to have to read quite a few.
If you want to understand that, you're going to have to read quite a few that are of opposing
views to really get a good understanding of it from a historical level.
As far as the moral arguments, those are going to be influenced by your own personal values.
But there are a lot of people right now, irrational might be the word.
If there was a simple solution, it would have been implemented by now.
The rhetoric that has existed for the last 50 years pushing each side towards conflict
is not going to result in a solution either.
just going to continue the conflict.
I would, if you haven't seen it, there's a video on this channel about, and I'm standing
in front of a whiteboard, it's about foreign policy dynamics.
I would watch that as well because that is critical to understanding that these strategies
being deployed by each side feed into what we're seeing right now.
And if the rhetoric continues to be supportive of those strategies, what you're seeing right
now will happen again.
And I personally think it's guaranteed to happen again already.
I think that that next cycle is already primed.
It would take a lot to avoid that.
And I, this is one of those moments where I really wish people understood, understood
Even on a deep level, the way that type of conflict works, the way a small force, large
force dynamic really functions, because I see people who I know want peace using rhetoric
that guarantees more war.
And it is disheartening.
Who are the other members of the team?
Names and roles.
They like their privacy, so no names.
But roles, we have somebody that does paperwork, which there's way more than you might imagine.
We have somebody who fixes everything I break and does the construction stuff.
Some of it you haven't seen before, but this and everything behind me.
We have a person whose entire gig is pulling the YouTube video.
When I'm done recording this, I'll upload it to YouTube.
If you see it anywhere else, hear it anywhere else, they did that.
I have no idea how it happened.
And then we have a person who helps with like video editing when that occurs on those occasions.
Who helps with, who over the last year has been involved pretty heavily with making the
book happen.
And they're kind of a catch-all.
And then we have a person who makes sure that everybody knows what everybody else is doing
and kind of coordinates it all.
Because of them, I can just focus on doing the content.
Star Trek question.
Out of all the episodes you can recall,
what was one that you found told the most relevant and impactful
story when you see it?
And what was one you found the most fun?
Well, it's funny, because the next paragraph.
Also curious to know where the episode that Quarks Bar tries
to unionize falls for you.
That's in the most fun part.
I really liked that one.
And I answered something similar in the first part of this.
But I like the ones that parallel something that occurred or it has a deeper moral lesson
in it.
I think the one I said in the first part was the one where they did, they did the Dred Scott
case using that data.
I also like the one where there's the, it's the war, it's Vietnam is what it seems to
be and it's just ongoing. I like a lot of the wider story arc in DS9 where I mean
it's actually it deals with a small force and an establishment force and it
It gets a lot of the dynamics right, actually.
I found that entertaining.
Your team needs a funner definition
of lighthearted and fun.
Whenever you say that, I kind of brace myself for the opposite.
Like right now, here's my question.
To what do you attribute your moral and ethical development?
Your intellect, I get.
wide-ranging curiosity, and cognitive skills,
but you have a depth of kindness and connection
that amazes me.
Examples, your recent video on phone calls,
your advice to the questioner
about his trans female co-worker
that skirted the obvious issue of his attraction,
your attempt to broker a deeper relationship
between the daughter and her career military dad.
Uh, it comes from not being a nice person.
When I was younger, that has a lot to do with it.
Travel, a lot of mistakes, and actually trying to learn from them.
Being greedy at the end of the year.
What was your most romantic gift to your wife and what was hers to you?
I gave her the moon.
necklace that is has lunar dust. Her most romantic gift to me, all the kids I would
guess. I mean I am a very practical person. I think, I mean she spent years, years
pregnant you know we have a lot of kids that is that's a big gift oh wow this is
long I love your show so much I finally decided to become a patreon member so I
have a question I'm a progressive Democrat but because my dad was self
employed and I was the sole proprietor proprietor of my own business for a
while I kind of get conservative Republican positions on certain things
My sibling took over my dad's construction business and of course
vote to Republican too. They're staunchly anti-union and if Satan was the
Republican frontrunner for president they'd vote for Satan despite being
conservative Christian. Anyway, the family member believes that there are
thousands of immigrants flooding into the US from Mexico and those immigrants
can include people from Africa, Asia, and other countries not just Central and
America. They say immigrants are swarming into this country. Okay, just scanning
through this real quick and then there's a request for accurate unbiased info on
employment statistics because of course if you don't know this on some right
wing outlets there's they are leaving people with the impression that
that unemployment is like an all-time high,
which I found interesting.
Sibling is transphobic, thinks people are inventing
the whole thing.
OK, so this is what you do.
And there's more, just so you know.
The reason why housing prices are so high
is because wives are working.
What?
OK.
Pick one.
Pick one of these.
Doesn't matter which one at first.
Go through, get all of the information you can.
Like for the unemployment thing, go to the Bureau of Labor
Statistics, and go to the actual source information
everything and have it already pick one of the topics, completely have everything
lined up, and then start the conversation on that one topic. Don't deviate from it.
Once there is an acknowledgement that the information that they have is wrong
on that one topic, then and only then, after you give that a bit to digest, come
back and start with topic two.
And just, you're teaching, basically.
And an important part of good teaching techniques is allowing the information provided time
to digest before giving them the next thing.
And it also makes them less defensive.
You're not asking them to forget about everything that they believe is true.
You're focusing on one thing.
That would be my suggestion.
Hey, Beau, first off, I wanted to say thanks.
As a trans man who's pretty disillusioned with my adopted gender, you've consistently
been a model of the kind of man I want to be.
But I'd love to know, what's your favorite album?
An album people wouldn't expect you to like.
That's three things.
What's your favorite album?
An album people wouldn't expect you to like and an album you'd recommend to someone who
wanted to get to know you better.
I don't know that I have a favorite album.
An album people wouldn't expect you to like, Cheap and Evil Girl by Brie Sharpe.
An album you'd recommend to someone who wanted to get to know you better?
I don't know.
I have no idea.
Yeah I've got nothing on that.
As you stated, I will ask a simple question.
In 30 seconds, can you explain to a Canadian, do people actually get accountable vote in
the US federal elections or is it more like three card monty?
as I might, I cannot for the life of me understand how some members of the House or Senate get
elected."
Okay, so in federal elections, they're done by districts.
Those districts are often gerrymandered.
So you end up, in some cases, you will end up with candidates who play to the least informed
in that district, and there's enough of them to win. That's part of how that
happens. In wider federal elections, in a lot of states, they are, they're not
competitive. Like a Republican is not going to win California, okay? So the
The Republican votes in California, those who vote Republican for president, I mean,
sure they're counted, don't get me wrong, but because of the electoral college system,
they're counted, they're tabulated, but they're not going to impact the outcome.
So again, it comes down to education for when you're talking about the House and the Senate.
The reason you end up with, let's just say, more entertaining characters in the House
is that Senate races are generally statewide.
So it's harder to play to the least informed.
That's why you tend to have people who are more politically savvy in the Senate.
What's your perfect day?
I mean, work, hang out with the kids, play with the dogs.
At this point in my life, my requirements for perfection are pretty mundane.
What would your last meal be if you had to choose?
I would want to go to a restaurant called McGuire's and just eat a whole bunch of stuff
there.
Who is your favorite Star Trek character and why is it Garrick?
It is Garrick.
I find a certain amount of relatability there.
Why don't hybrid manufacturers put flex-fuel engines in their cars?
Sent a letter to the manufacturer of the hybrid I own and got a bland letter back thanking
me for my interest yet.
You know, I don't know.
To be honest, I've never even looked into it.
I don't know.
Let's see if there is some economic reason that it occurs.
What are your thoughts on comedians, like and then there's somebody named, who have
been progressive on many issues in the past but seem to fill their routines with insensitive
bad jokes, though occasionally make a good point?
How often can a joke about race, sexual identity, or gender status land in a way that is acceptable?
I asked Trey this question, too.
See, I don't know.
Like, my sense of humor is very dry, very,
it is very sarcastic and dry.
So most of the jokes about any marginal,
They don't land with me anyway.
It's never been something that I've really found that funny.
And when you're talking about when progressives do it, I mean, I feel like a lot of them may
do it for the shock value, you know, because there was a whole period in time where that
That was comedy, saying the most shocking thing you could think of.
And there's still elements of that left, so that may have something to do with it.
Do you have any thoughts or advice on encouraging progressives to get interested in agriculture?
I'm young and I really care about preserving rare heritage poultry breeds in the U.S.,
but every time I talk about it, people either shame me for not going fully vegan or assume
I'm from the country and therefore don't know anything about progressive politics. I get that one every once in a while
You know we talk a lot about
Community gardening and there's a you know video that's in production for like seven years on it
I
I think it would probably be best to approach it
from a position of breaking reliance on the system
and reducing carbon might be your best route.
How do you get your beard to fork like that?
I play with it when I'm talking, and it just does it naturally.
Let's see.
Where do babies come from?
The baby store, obviously.
Thank you for being so awesome that I can listen over the speaker while the kids are
in the room.
I love the alpha on the shelf with the mug shot of the former guy.
Is the New Year's Eve party overrated or underrated?
What headcount is just right for a party?
Either incredibly small, like six people, six to eight people, or huge.
No middle ground.
There's no middle ground.
That's my opinion on party size.
is something where there can be a lot of conversation and a lot of interaction and people get to
know each other better or it is huge and you are typically for me that's when you're introducing
different social circles and trying to bring them together.
Is it ethical to use the same conspiracy theory techniques to deprogram MAGA as were used
program them in the first place. Ethical. I don't know. Does it work? That's the more
important question right there. I personally like to try to promote good
information consumption habits and not lean into any theory. Although there have
been times, and we have joked a lot, about setting up a channel that is doing
literally this. You know, who using the constant fear and paranoia of the
right to direct it towards positive causes.
But it's always been a joke.
I don't know anybody who's actively tried it before.
Not a question, but an invitation.
You mentioned once about coming up to PA.
I live in Northwest PA, fairly rural.
That would be cool.
Yeah, there is a plan for I don't know if tour is the right word.
I'm going to go on a road trip and it's going to, it will put me relatively close to most
people in the United States at some point during this trip.
We'll see how it plays out.
Fun, if you were forced to choose one year to live in with your loved ones, Groundhog
Day style, what year would you choose?
Probably 2022.
Maybe this year.
Bo my kid is 19.
I have aimed to raise a conscientious citizen and still importance of voting in them as
my parents did for me.
My kid was proud to have voted for Ralph Warnock in the first election last year.
we were in line, I almost got thrown out for stage whispering, do you know a werewolf can
kill a vampire? And the poll worker shushed me, no political discussions. Oh wow. The
fact that that is interpreted as a political discussion in that race is something else.
So Bo, my kid could use talking points about what the Biden administration has done that
will tangibly help the people of their generation. Their goals include to choose a career fit
for the future, complete their education, buy an electric car, switch to solar and more
eco-friendly energy, and protect individual liberties, like choosing who to marry.
Well, I mean, actually, on most of this, he's done alright.
Can you help refine the elevator pitch to their peers?
I would say that infrastructure development and kicking off the transition to greener
energy is a big thing that is going to matter to them a lot whether they realize it or not.
I know that's not incredibly, that's not sound bite worthy, but that's going to be a huge
impact.
Something that is more sound bite worthy is that we have, I have a video going over how
much he's actually forgiven as far as student debt and there's a lot of people of that generation
were like, well, he didn't get everything. No, he didn't, but he's gotten a whole lot.
And I think that maybe understanding that opening the door to forgiving that much means
that others are going to try to duplicate it later. Even if they don't, even if he
He doesn't get it the way that he wanted to.
He has done so much in small amounts that the end number, it's going to be very hard
for a future president to get pushback on how much they forgive because, you know, he
did $100 billion.
I don't remember the numbers off the top of my head.
I actually think it was more than that.
I think those would be the two that might work.
What inspired you to have the Curious George Velcro patch and turn it over when being sarcastic?
The Curious George Patch, it's a remnant from days gone by.
I have a video on it somewhere.
It was a mascot of a group that I belonged to.
My kid liked monkey when he was little, so it all came back out.
As far as turning it over, I needed some way to identify that there was a big bait-and-switch
going on or I was being incredibly sarcastic or it was satire or something like that.
And that was just something that was easy to do.
I've lived abroad for nearly 15 years in multiple countries and keep wondering when
Americans will finally wake up and realize how bad things are for them in favor in giving
trillions in funding for war, foreign interventions, tax breaks for the wealthy, etc. Is it due
to a lack of international knowledge and travel, selective ignorance, highly effective distractions?
all of that. Travel is one of those things, you know, it is an
ignorance killer. It's a prejudice killer. It's, and it is not something
that most Americans can afford, you know. I mean, most people in the U.S., when you
talk to well-traveled people, a whole lot of them, how did you go to all those
places. I was in the army. It's not something that most Americans can afford. I think even
travel within the U.S. would be incredibly enlightening for a lot of people. And then
there is selective ignorance or cognitive dissonance at play, where they've been told
something so long that when they get information that contradicts it, they don't want to accept it.
And that plays into it.
And then we do have highly effective distractions that
serve as control mechanisms, basically.
They keep people pointed towards each other, fighting
over the last cookie type of thing.
There is a lot of talk from the usual talking heads about
states seceding.
My question is, should the rest of the US just let them?
See how it works out.
The US can't stand a breakup.
If the US was to break up, it would turn incredibly ugly,
incredibly quickly.
it would get real bad real quick. So I don't think so. I don't think that's the route to go.
Carter installed solar panels on the White House roof only to have Reagan remove them.
Why hasn't any president since moved to reinstall such panels? Wouldn't it be a great way of leading
by example? I mean, yeah, I don't have an answer for that, but yeah.
When Biden was elected in 2020, I assumed he'd be the equivalent of a band-aid,
something to stop the bleeding caused by Trump and stop the wound from getting infected.
Worse, while a better, younger candidate was found. However, despite some failings,
Would it be fair to say he surprised you as he has me by exceeding expectations?"
Yes, but I don't think people really understand how low my expectations were.
He has, obviously, there are major failings that I see.
And there have been things where I know what the end goal was, and I know that they tried
to do the right thing, but it didn't come off, you know, it didn't happen.
And you can say, well, you know, he tried, but at the end of the day, it didn't work.
And you have to count that.
But overall, yeah, he is better than I thought he was going to be.
Another long one here.
Dear Bo, I only started paying for no ads YouTube because of you, and now that I've
stopped working for money, I'm watching mostly YouTube, which has substantially diminished
my attention span into my great disgust now causes me to pay to watch in-video ads, which
I skipped through.
So what was the point of paying premium?
Oh, okay, I know what you're talking about.
So first, thank you for not doing that or marketing your own vitamins or whatever.
Second, I think I know the answer.
Should I unsubscribe from the $14 a month and put them into the Patreon kitty for those
few YouTubers I support. I did like Big Red Kelt at your suggestion, but it seems like
he's not making videos. He'll be back. He'll be back.
Anyway, lastly, it seems strange that at this point in my life I'm turning to a source
like YouTube for so much information. What's your thinking on the future of the social
media horizon? Will YouTubers organize? Is Google undermining the existing media unions?
What other platforms are reliable places for the desperate kinds of info we need to understand?
You know, when to stockpile and when to pack the bags. Okay, lots of questions. Yeah, as
As far as the end video ads, people do them because the amount they get for doing them,
it's not a small amount.
It really isn't.
We don't do them on the main channel because most people or most companies that want one
One of those ads in the middle of the content, they want 60 seconds.
Some of my videos are only four minutes long, that doesn't fit.
And then as far as on the longer format stuff, the other thing that has happened is the ones
that we get offered, there's only been two in the years that I have been doing this that
have even remotely considered.
We just, well I don't want to say, because I would, they're not like bad companies,
but like, it would be like, here, can you advertise macaroni and cheese?
Like how does that fit in with our content in any way?
It just doesn't fit.
And there's only been two that have ever even come close.
one I'm actually kind of considering but it would be on this and it would be
something that I think y'all would find useful but I'm not I am NOT through
evaluating it yet the then should you unsubscribe so the YouTube ads like the
ones that you might be able to skip or whatever and the in video ads those are
two separate things if so you if you stopped the $14 a month you'd start
seeing those again the putting it in the patreon kit I I think it would be you
know that's six one way half a dozen the other I don't think that that's going
to have... I don't think it would have a marked difference on your viewing
experience or for the creators that you'd be supporting that way. I don't... I
think whatever is best... whatever you feel is best. Like I don't think that
there's a wrong answer to that one. Will YouTubers organize? So this is
interesting because this is something that came up recently. I don't think
youtubers, as in the people that you see, I don't think that that's really on the
horizon. But the people who work for the people you see on your screen, I think
they might because there was a conversation that some of the
people on this channel were a part of.
And let's just say it made me feel really good
about the working environment that people have here.
Because the rates of compensation
in the general environment is not uniformly good for people.
Now, admittedly, a lot of the people
that they were talking to, they were
working for channels that weren't political in nature,
certainly not those that lean left,
that value that kind of stuff.
But I could see that happening after I wasn't
a part of the conversation, but I got bits and pieces of it
later.
And it would be wise of YouTube creators, the personalities
who are on screen, to treat the people who are offscreen
a little bit better.
because that's, those conversations are already
happening.
So OK.
What other reliable, what other platforms are reliable places?
So that's actually, one of the things that I'm reviewing
is a news platform.
And I'm looking at it, and it's interesting.
I don't want to talk about it yet, but it has a cool concept.
And it would be especially useful for people
who like to read their news rather than listen to somebody.
Right now, with Twitter going the way that it did,
YouTube is kind of your best bet.
That's where, well, no, because you can also get a lot of it on podcasts if you're somebody
who likes getting their information via audio.
You could do it that way too.
Okay, so what did you think of the Beatles' Now and Then single and video?
I found it refreshing to have this gift of new Beatles material with a positive message
after a long year of chaos and angst.
Hope you wear the proper t-shirt to answer this question."
I don't get to know him ahead of time.
Let's see.
Okay, so the now and then video.
The video was cool, but honestly, it seemed sad to me.
Like the video, the music, it was sad.
It was sad.
I get what you're saying, and I liked it, but it was sad.
We know that you sometimes prepare date agnostic videos to air on slow news days.
What's the longest one of those videos remained in the backup pile before airing, and what
was it about?
Oh, I have no idea what one was about, but I mean months?
Oh, I can think of one that just recently got used.
There was a video that was talking about the way men feel like they can survive insurvivable
situations.
That was a big part of it.
And I recorded it around the time the sub at the Titanic ceased to be a sub.
And it went out probably in the last 60 days.
That one waited a really long time, mainly because I forgot about it.
And then a lot of the talking points wound up resurfacing, so it got to come out again.
A little context before I get to the question.
One of the reasons I started watching your YouTube videos regularly is the length.
Not because they are short, but because they are exactly as long as they need to be.
Which is funny, because that's where that comes from.
Is something an editor at a news outlet told me once when I asked how many words.
I don't know how many it takes to tell the story.
Okay, so not because they are short, but because they are exactly as long as they need to be.
I don't want you to start asking us to like, subscribe, and comment, but I'm sure the almighty
algorithm punishes you for this.
I know you like that engagement is driven by your community and the comments and otherwise,
but it would help for you to talk about it from time to time.
Many viewers probably don't know that in addition to liking, subscribing, and turning on notifications,
algorithm is driven by comments, comments on comments, sharing and watching the
video through to the very end. Okay we have finally reached the question, are
you okay with your community driving YouTube engagement by occasionally
making you well played by occasionally making you read about these things in a
Q&A. Nice. Yes. So when you like, subscribe, comment, reply to somebody else's comment,
and turn on your notifications, it does help the channel grow. And I don't say that because
it messes up the flow of the videos. So, no, I don't mind. There are times when I
enjoy getting, you know, played like this. There's a... I am really bad at
my own advertising. Bo, I'm a mom of a gaggle of kidlets and always have a
collection of hilarious quotes by the end of the year. Is there a particularly memorable
one you could share with the internet people? Yeah, there's quite a few actually. See,
I don't know because like the ones that I find memorable are normally when one of the
little ones picks up something that they shouldn't have picked up from somebody who was around.
My oldest daughter was decorating something, and my youngest daughter, who had just spent
a lot of time around people who, she walked in and was basically like, that is absolutely
beautiful female dog, like, and not in like a derogatory way, but in like a, you're my
best friend kind of way.
keep in mind my youngest daughter has no idea what any of this means and to me
that was hilarious you know I know that I couldn't laugh or do anything like
that my wife was horrified partially because you know it was you know her
friends but but to me that was very memorable because everything about her
tone and the way she said it like it was just it was perfect and it just it
cracked me up and then I got in trouble because I couldn't not laugh so that
one's probably most memorable okay okay oh I'd like to be funny but I've lost
hope this year work fell out in May and as a 54 year old woman with
transferable skills, I seem to be pretty easily ignored. Instead of working in my
$60,000 a year field, I'm working at a local supermarket, not for lack of trying
for sure, but the hiring process is broken. I feel left behind. AI seems to be
doing nothing good for this process. I feel like businesses think AI will solve
all the needs for paying people that they consider just grunts, customer
service people, bankers, et cetera.
My husband thinks that a universal wage will need to be
paid if AI that's over as many jobs as big business seems to
think it will.
What are your thoughts?
So for my world, AI is not good enough to do what people
I think it is.
For what I do, it doesn't have a whole lot of impact,
because it isn't AI.
It scrapes information that's already out there.
That information has to be created
by somebody who analyzed it.
It's just not there yet to impact my world.
That being said, yeah, if a large enough percentage of people
are impacted by that, yeah, UBI, a basic income
is going to have to become a thing.
They're not going to have a choice.
So for me, because my field is very much insulated,
You know, there have been news outlets that have tried with like basic stuff like sports and it has just it has been an
unmitigated disaster in my field.  It just it's not there.
So I don't see I don't have any personal contact with those issues, but I can see with customer service and quite a few
other
industries where it is going to cause an issue and the lack of income for the people impacted
by it, it's going to have to be addressed.
They're not going to have a choice.
Would you rather fight one horse-sized duck or a hundred duck-sized horses?
Hundred duck-sized horses, easy.
But really, what's the most ridiculous altercation you've ever gotten into with a non-human
animal and who started it. I mean honestly to me the deer here and if I know I've told the story
before but if you haven't heard it I left and I'm walking out and I'm walking through the
through like the garden area and around the corner of this little greenhouse and there is a deer
standing there and I'm looking at it it's looking at me it doesn't appear spooked so I just kind of
I'm like, well, I'm going to pet it.
And I reach out, and it lets me pet the side of its face,
and everything's fine.
And it's just this very zen moment.
And then another deer comes crashing out of the tree line,
scares the deer that's beside me, who then rears up
and takes its front hooves and just hits me in the chest,
knocks me down, wheezing.
That, to me, is the most ridiculous,
because it went from, I'm a Disney princess petting a deer,
to I just got beat up by a Bambi,
like in the blink of an eye.
I think the only one that really comes close to that
is something that happened years and years and years ago.
I was walking along the side of this lake
in central Florida.
There are gators in this lake.
There is this husky that is running away from a woman screaming his name and he's running
towards me and he is going in the water.
And I know that there are gators nearby, like he's right on the edge of the water.
So I just reach down and try to grab him and I get my fingers under his collar.
I assume he is going to stop.
He does not and drags me into the water with him.
And I look over when I'm in the water, laying down, and I can see a gator, not like super
close, but closer than I want it to be.
So I grab the dog and we roll up onto the shore and we stand up and both of us like
run away from the water, like instinctively believing the gator is chasing us or whatever.
And we turn around, both me and the dog, and look back and of course the gator's just hanging
out being a gator in the water and the dog looks at me and I look at the dog and the
lady who was chasing him gets closer and he takes off running again and the lady runs
right by me.
That to me was really funny because then I had to, you know, walk back to my apartment
covered in swampy lake water.
Maybe not light-hearted, but seeking hope.
I have a friend who is still very worried because he hears that Trump has a good chance
to win the Republican candidacy election overall.
I've tried to convince him otherwise, but I also know that it is a very real fear and
don't want to seem as if I'm making the mistake of not taking him seriously.
Can you make a clear, concise case for why things don't look good for Trump?
Okay, so Trump's behavior is becoming more and more erratic.
His statements are becoming more and more off.
Trump didn't win last time, despite the coverage that is currently out there.
I do not believe that independents are going to go back to him.
I don't see that happening, especially because I personally believe he's going to become
even more erratic as all of the different proceedings speed up.
Right now, because of the polarization in the media, you have a lot of people who are
wish-casting what they believe independents are going to do.
Trump cannot win with just the MAGA base.
The numbers are not there.
He needs the independence.
If the economy is doing well, as it appears it's going to be, I don't believe that things
are as dim as a lot of people seem to believe they are for Biden.
I know in my world, which is a little bit further to the left than most even liberals,
I know that there are a lot of very unhappy people, but there's not enough of them to
outweigh the independents.
So it's a year out, it's still a ways away.
I don't think it is time to worry yet.
I think it's time to work.
Okay, firstly, thanks for what you do, silly question I suppose, compared to the other
being posed. You do so much for others, what do you do to refill your tank, so to
speak? Oddly enough, this, this, this itself. Actually, this isn't draining to me.
This is very rewarding, so it does fill my tank.
Bo, as a left-leaning Canadian, I find your content gives a really good, even perspective
of what's realistic and what's likely to happen.
Our federal elections are run by an independent, nonpartisan agency, Elections Canada, which
has its pros and cons, but more pros, in my opinion, at least for a multi-party country
like ours.
Do you think that something similar would be a good idea in the United States, or would
that be seen as federal government overreach?
And on a lighter note, have you ever tried real 100% maple syrup on French vanilla ice
cream?
I have not.
Answer that one first.
That sounds like it would either be delicious or just way too overwhelming.
I don't know which, but it sounds like worth a shot.
Okay, as far as a truly independent, nonpartisan agency that oversees everything, would it
be a good idea?
Absolutely.
Would it be seen as federal government overreach?
Absolutely.
It's the United States.
Everything is federal government overreach.
I think we're a long way off from something like that.
I don't think it's a bad idea, though.
You're my most trusted source of information.
I'd like to hear your thoughts on what, if anything, all of us should do to prepare for
another possible Trump presidency.
If you're part of a marginalized community, I would have a plan on how to leave.
I don't think it's likely, but having a plan like that doesn't hurt.
Curious as to how many emails you
receive on average in a week or month.
And there is actually an answer from the person
who goes through all of these.
2,000 to 3,000 per month.
Wow, that seems really high.
That's a lot.
OK.
Hi, Bo.
Happy impending new year.
I will ask a serious question and a lighthearted one, too.
Serious question every day, now as we see things taking new and seemingly devastating
turns for the former president, yet his delay tactics still seem to be effective, and he
seems never to run out of motions.
What if anything makes you think the lumbering machinery of our bureaucracy can respond to
the threat in time before the fascist seizes power?
question to this. If SCOTUS does uphold his immunity claims, why do I feel like Biden's
idea of patriotism is institutional enough that he would hand over the reins of power?
Okay, so that was a fun, fun question. Do your own Easter eggs? Okay, we'll go to this one first.
Okay. The machinery, you know, it does, it moves slowly, right? But now, all of a sudden,
you're having a whole bunch of cases hit for New York and for the lawsuits related to January 6.
And they're all moving forward right now.
Now, those were very delayed, and he couldn't do it.
He couldn't keep them delayed enough.
I don't know that he's going to be able to delay all of these cases for as long as he
would need to.
And realistically, only one of them is going to be necessary to sink him.
I actually think that if the vote came back and it was in favor of Trump, Biden would
absolutely hand over the reigns of power.
I think that that is something he would absolutely do to preserve a peaceful transition type
of thing.
Okay, so that wasn't fun.
question do your own Easter egg props ever crack you up? I am remembering the
fact that I had to rewind more than one of your videos because the big stuff
curious George was gagged with electrical tape. How much glee do you get
in deriving such little touches? A lot. A lot. It cracks me up and I like it when I
can slip one by some of y'all too. So okay, wow, that looks like it. All right, so
So that's it, happy new year, there's a little bit more information, a little bit more context,
and having the right information will make all the difference.
I hope y'all have a great year.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}