# All videos from December, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-12-31: The Roads to New Year's (Part 2) (<a href="https://youtube.com/watch?v=wysG70qaE8s">watch</a> || <a href="/videos/2023/12/31/The_Roads_to_New_Year_s_Part_2">transcript &amp; editable summary</a>)

Beau tackles a range of questions, sharing insights on everything from gun ownership progression to campaign tips for a small city with limited funds, all while maintaining a light-hearted tone and engaging with his audience.

</summary>

"I compare them to shoes. They're for different purposes and different roles."
"If we can't, we have to teach people good information consumption habits."
"I don't think it is time to worry yet. I think it's time to work."
"That sounds like it would either be delicious or just way too overwhelming."
"Having a plan like that doesn't hurt."

### AI summary (High error rate! Edit errors on video page)

New Year's Eve special edition Q&A with a variety of questions.
Views on books and movies that haven't aged well.
Thoughts on Tesla and Elon Musk.
Height revealed, jokes about being a "short king."
Advice on campaigning in a small city with limited funds.
Social media usage and waiting for Twitter to crash.
Encountering stickers on Tesla cars.
Detailed explanation of gun ownership progression.
Inspiration from various individuals rather than having one hero.
Views on implementing laws like the Fairness Doctrine.
Favorite movies and apps, including a recommendation for "Man from Earth."
Availability of t-shirts for sale with potential link.

Actions:

for community members,
Research and find an issue to campaign against opponents (implied)
Educate others on good information consumption habits (implied)
Prepare an exit plan if part of a marginalized community (implied)
</details>
<details>
<summary>
2023-12-31: The Roads to New Year's (Part 1) (<a href="https://youtube.com/watch?v=2TrXXkfaoxs">watch</a> || <a href="/videos/2023/12/31/The_Roads_to_New_Year_s_Part_1">transcript &amp; editable summary</a>)

Beau answers viewer questions in a special New Year's Eve Q&A, discussing military resilience, charity revamps, book plans, personal traditions, and more.

</summary>

"The US military is very resilient, and there are a lot of safeguards in place for that."
"Sometimes it's somebody else."
"It does not bother me. I don't see it as really that abnormal."
"Early on, I did one by happenstance and I got a bunch of messages from people saying, Hey, you know, nobody puts out content today."
"If you travel enough, you start to realize that you have more in common with somebody on the other side of the planet than you think you do."

### AI summary (High error rate! Edit errors on video page)

Special New Year's Eve edition of The Road's Not Taken with a giant list of Q&A questions.
US military resilience in the face of political divide, safeguards in place to prevent collapse.
Avoiding question about leftist folk artists due to controversial nature of their work.
Plans to revamp charity efforts for longer-term impact by setting up endowments.
Speculation on Trump's potential delay tactics regarding federal cases.
Predictions on potential nominees for Republican and Democratic parties after a significant event.
Addressing concerns about the national debt and wealth disparity in the US over the last 40 years.
Plans and themes for future books, including a history book.
Personal insights and traditions shared, like New Year's resolutions and holiday video traditions.
Thoughts on polyamory, beard maintenance routine, and surprises in 2023.
Community engagement, charity work, and grassroots movements to address political issues.
Personal anecdotes about family, pets, and encounters with fans in public spaces.

Actions:

for viewers and community members,
Contact local colleges to find connections to leftist organizations in rural areas (suggested)
Initiate grassroots movements or join political parties to counter issues like Citizens United (implied)
Share insights and start honest dialogues about conspiracy theories step by step (implied)
</details>
<details>
<summary>
2023-12-31: Let's talk about the X-37B and secrets.... (<a href="https://youtube.com/watch?v=Q4NZc6SocQo">watch</a> || <a href="/videos/2023/12/31/Lets_talk_about_the_X-37B_and_secrets">transcript &amp; editable summary</a>)

Beau delves into the secretive world of the X-37B, a space plane shrouded in mystery and speculation, leaving all to wonder about its true purpose and potential impact.

</summary>

"This is a piece of technology that is kept very secure."
"Nobody has a clue what it does."
"Whatever it is, very much nobody gets to know."
"They are keeping the secret very, very well."
"It will be something along those lines but we don't know."

### AI summary (High error rate! Edit errors on video page)

Explaining the X-37B, a secretive space plane resembling a mini space shuttle.
The recent flight marks the seventh known mission since 2010.
The program started in 1999 as a NASA DARPA project, later transferred to the Department of Defense in 2004.
The X-37B has been orbiting for over 10 years with two separate vehicles existing.
Despite public-facing experiments, its true purpose remains highly classified.
The current mission's details, including its duration, are undisclosed.
Speculations suggest it could remain in orbit until 2026.
The program's secrecy goes beyond mere human interactions, with its technology being tightly guarded.
Beau believes the reveal of its purpose will be as impressive as stealth technology.
The level of secrecy surrounding the X-37B is unparalleled, leaving outsiders to only speculate its true mission.

Actions:

for space enthusiasts,
Research more about the X-37B program and its history to gain a deeper understanding (suggested).
Stay updated on any public disclosures regarding the X-37B to uncover more about its missions and experiments (suggested).
</details>
<details>
<summary>
2023-12-31: Let's talk about stats, rates, correlation and causation... (<a href="https://youtube.com/watch?v=mWTcHWLHQeg">watch</a> || <a href="/videos/2023/12/31/Lets_talk_about_stats_rates_correlation_and_causation">transcript &amp; editable summary</a>)

Beau breaks down misleading statistics on murder rates, challenges preconceptions on demographics and crime, and underscores the importance of understanding causation versus correlation.

</summary>

"It's weird."
"The only interesting thing about it, when you actually get down and start going through the information, the real disparity I see is that people with brown eyes are far more likely to commit a murder."
"100% of people who do not know the difference between correlation and causation will die."

### AI summary (High error rate! Edit errors on video page)

Reports a 12% decline in the murder rate, which is significant and might be a record.
Mentions that despite surveys showing 77% of Americans believe crime is increasing, the numbers indicate otherwise.
Points out the influence of media coverage in shaping perceptions of reality.
Addresses statistics claiming 13% of a certain demographic (Black people) are responsible for 52% of murders.
Disputes the accuracy of these statistics, explaining that they only represent cleared murder cases, not all murders.
Raises concerns about missing data from unsolved cases and individuals who simply go missing.
Challenges the notion that skin tone is linked to likelihood of committing murder, citing economic factors and poverty as more relevant indicators.
Suggests a correlation between income inequality, poverty, and crime rates leading to higher murder rates.
Poses questions about the impact of economic issues in 2020 and subsequent government relief efforts on crime rates.
Encourages viewers to think critically about causation versus correlation in data interpretation.

Actions:

for data consumers, critical thinkers,
Fact-check statistics before accepting and sharing them (suggested).
Advocate for accurate representation of data in media coverage (implied).
Support initiatives addressing income inequality and poverty to reduce crime rates (implied).
</details>
<details>
<summary>
2023-12-31: Let's talk about Russia, Ukraine, and a back and forth.... (<a href="https://youtube.com/watch?v=XsOXocSgMN8">watch</a> || <a href="/videos/2023/12/31/Lets_talk_about_Russia_Ukraine_and_a_back_and_forth">transcript &amp; editable summary</a>)

Recent events between Russia and Ukraine escalate tensions as Ukraine responds to attacks, underscoring the critical need for international support and aid to prevent further conflict escalation.

</summary>

"It's against the advice."
"Playing politics when a country that is an ally is facing an existential threat is not a good idea."
"European security is tied to Ukraine's success."
"This is not a good development."
"This needs to be a priority."

### AI summary (High error rate! Edit errors on video page)

Recent events between Russia and Ukraine involve massive aerial attacks and rocket strikes on cities.
Around 100 Ukrainian cities, towns, and villages were affected by Russia's barrage.
Ukraine responded with a rocket attack on a city in Russia and engaged in drone activity.
Russia claims the munitions used were of Czech manufacture, but there's no substantial evidence.
Ukraine's actions were likely aimed at deterring further attacks from Russia.
Despite Ukraine's message, Russia launched another wave of drones, resulting in ongoing conflict at the border.
Western commentators criticize Ukraine's actions as going against advice and best practices.
Ukraine is in an existential fight and urgently needs equipment like air defense to defend itself.
Lack of aid may force Ukraine to resort to extreme measures to deter attacks.
Continued strikes across the border could escalate the conflict, posing a risk of widening it.
The US needs to prioritize supporting Ukraine to prevent further escalation.
European and US security are linked to Ukraine's success and European security.
Congress's delay in providing aid to Ukraine impacts the country's strategy and response.
It is vital for the US to take action promptly to guide Ukraine's strategy effectively.

Actions:

for foreign policy analysts,
Advocate for immediate aid and support for Ukraine (suggested)
Monitor the situation closely and stay informed (implied)
</details>
<details>
<summary>
2023-12-31: Let's talk about Iowa, books, schools, and conversations.... (<a href="https://youtube.com/watch?v=bttF1gtlb6E">watch</a> || <a href="/videos/2023/12/31/Lets_talk_about_Iowa_books_schools_and_conversations">transcript &amp; editable summary</a>)

Iowa faces a controversial law curbing school activities, with bans on books and LGBTQ+ content temporarily halted, while a parental notification requirement remains standing.

</summary>

"The ban on books, including history books and those aiding students, has been halted temporarily."
"The law is seen as overly broad and unlikely to fully stand in its current form."
"Educators have a temporary reprieve, but the situation may change based on the progress of the case."

### AI summary (High error rate! Edit errors on video page)

Iowa legislature passed a law curbing school activities, but a federal judge issued a temporary injunction against it.
The three main components of the law are banning books, banning LGBTQ+ content, and requiring parental notification for pronoun changes.
The ban on books, including history books and those aiding students, has been halted temporarily.
The ban on LGBTQ+ content was described as overly broad and is also temporarily halted.
The parental notification requirement stands because the plaintiffs lacked standing to challenge it.
The law is seen as overly broad and unlikely to fully stand in its current form.
Educators have a temporary reprieve, but the situation may change based on the progress of the case.

Actions:

for educators, activists, community members,
Monitor the progress of the case and advocate for inclusive policies in schools (implied).
Support educators in navigating the legal challenges and standing up for diverse educational materials (implied).
</details>
<details>
<summary>
2023-12-30: Let's talk about the skies over Ukraine.... (<a href="https://youtube.com/watch?v=_fEnx7DVlv4">watch</a> || <a href="/videos/2023/12/30/Lets_talk_about_the_skies_over_Ukraine">transcript &amp; editable summary</a>)

Russia launched a massive aerial assault on Ukraine, targeting critical infrastructure and signaling the potential for more attacks to come.

</summary>

"Russia launched the largest aerial barrage in Ukraine in almost two years."
"Drones are a little bit easier to come by. So I imagine we're going to see more of this."
"More assaults are expected, prompting a rush to reinforce defenses."

### AI summary (High error rate! Edit errors on video page)

Russia launched the largest aerial barrage in Ukraine in almost two years, firing 122 missiles and 36 drones.
Ukrainian air defense took out 87 missiles and 27 drones, but the devastation on the receiving end was significant.
Russia targeted energy and social infrastructure, damaging buildings like a maternity hospital and schools.
The aim was to disrupt energy infrastructure to make living conditions extremely difficult.
This recent escalation could be due to the success of a previous strike against warships.
Russia's response involved targeting critical civilian infrastructure.
Speculation that Russia may have exhausted their arsenal is premature; more attacks could be imminent.
Drones are easier to replace than missiles, indicating potential for continued assaults.
The urgency in replenishing Ukrainian defenses suggests an expected continuation of attacks.
More assaults are anticipated, prompting a rush to reinforce defenses.

Actions:

for global citizens, activists, policymakers,
Reinforce Ukrainian defenses (suggested)
Prepare for continued attacks (implied)
</details>
<details>
<summary>
2023-12-30: Let's talk about Nukes, NEST, and Vegas.... (<a href="https://youtube.com/watch?v=KAolMq6W57s">watch</a> || <a href="/videos/2023/12/30/Lets_talk_about_Nukes_NEST_and_Vegas">transcript &amp; editable summary</a>)

Beau explains the routine scanning for dirty bombs by NEST on New Year's, reassuring people not to panic if they see the high-tech helicopter flying low.

</summary>

"You will see a Huey."
"If you happen to see those, don't panic, it's pretty normal as weird as that is to say."
"If you see anything that's labeled NEST, don't panic."

### AI summary (High error rate! Edit errors on video page)

Explains a theory circulating about the National Nuclear Security Administration looking for scary things on New Year's.
Confirms the rumor that they will be looking for a dirty bomb, which they do annually.
Describes how the Nuclear Emergency Support Team (NEST) will be involved in scanning for radiation.
Mentions the use of a high-tech helicopter (Huey) with pods hanging off the side for the scanning.
Assures that this activity is routine and not due to any specific threat, happening at major events with large crowds.
Talks about the presence of NEST in places like Vegas and New York for New Year's celebrations.
Emphasizes not to panic if you see the scanning helicopter labeled as NEST.

Actions:

for event attendees,
Be aware and informed about the routine scanning by NEST for dirty bombs during major events like New Year's (implied).
Stay calm and don't panic if you witness the scanning helicopter labeled as NEST (implied).
</details>
<details>
<summary>
2023-12-30: Let's talk about NY, DC, and 3 Trump losses.... (<a href="https://youtube.com/watch?v=qsnuotsyPlc">watch</a> || <a href="/videos/2023/12/30/Lets_talk_about_NY_DC_and_3_Trump_losses">transcript &amp; editable summary</a>)

Trump faces significant losses in two cases in DC and New York, with immunity claims failing and the expectation of a substantial payout looming.

</summary>

"He tried to use his immunity claims there, And the judges said that the argument, quote, fails."
"The expectation is that it is going to be a pretty big number."
"You didn't turn in your homework on time, basically."

### AI summary (High error rate! Edit errors on video page)

Explaining two cases against Trump in New York and DC, with significant losses for Trump.
Mentioning a DC case brought by cops impacted by January 6th against Trump where his immunity claims failed.
Noting two losses in an E. Jean Carroll case in New York where Trump's attempt to delay the case based on immunity was unsuccessful.
Detailing Trump's team's attempt to remove an expert witness in the E. Jean Carroll case who calculates damages, which the judge denied due to being too late in the game.
Speculating that damages in the E. Jean Carroll case are expected to be significant, possibly leading to a large payout.
Anticipating more movement in both cases soon, especially the E. Jean Carroll case starting on January 16th.

Actions:

for legal observers,
Follow the developments in the legal cases against Trump, especially the E. Jean Carroll case starting on January 16th (suggested).
Stay informed about legal proceedings and outcomes related to accountability for public figures (suggested).
</details>
<details>
<summary>
2023-12-30: Let's talk about Georgia's new map and how it isn't over.... (<a href="https://youtube.com/watch?v=lUK4nnBVaRU">watch</a> || <a href="/videos/2023/12/30/Lets_talk_about_Georgia_s_new_map_and_how_it_isn_t_over">transcript &amp; editable summary</a>)

Georgia's redistricting saga unfolds as a judge orders a new majority black district while maintaining partisan balance, setting the stage for potential future challenges and decisions post-2024.

</summary>

"It's not exactly a win, but it's not a loss either."
"They complied with the order. They just did it in a way that allowed them to keep their edge."

### AI summary (High error rate! Edit errors on video page)

Georgia's congressional districts needed to be redrawn due to violations of the Voting Rights Act.
A judge ordered the creation of another majority black district, which was done while preserving partisan balance.
The new map has nine seats for Republicans and five seats for the Democratic Party.
Despite compliance with the court order, concerns were raised about potential new violations of the Voting Rights Act in a different way.
The judge approved the new map but did not rule on the new violation, suggesting it may be best suited for another case.
Affiliates of the Democratic Party might appeal or initiate a new case regarding the map.
The map approved by the judge is likely to be used in 2024, with little chance of further changes.
The coalition district and related issues may be decided after 2024, too late to impact the new map's implementation.
The outcome is viewed as an incremental step in the right direction, not a clear win or loss.
Judges cannot mandate partisan changes, as it is not within the Voting Rights Act's scope.

Actions:

for georgia residents, voting rights advocates,
File an appeal or initiate a new case regarding the approved map (suggested)
Stay informed and engaged in the redistricting process (implied)
</details>
<details>
<summary>
2023-12-29: Let's talk about putting $500 billion in context.... (<a href="https://youtube.com/watch?v=MqvmGN2lzUE">watch</a> || <a href="/videos/2023/12/29/Lets_talk_about_putting_500_billion_in_context">transcript &amp; editable summary</a>)

Beau explains the context of $500 billion in international trade within BRICS, debunking hype and stressing the need for perspective.

</summary>

"For you or I, if somebody walks up and they're like, hey, we're going to give you $500 billion. That's a huge chunk of money, right? That's a massive amount of money. For international trade, it isn't."
"It's almost Wal-Mart."
"But it's because of context."
"The doomsday scenarios about what would happen to the dollar, I don't think that they're incredibly accurate, but that doesn't mean that BRICS can't become a world player."
"When you were talking about the global economy, $500 billion, it's almost Wal-Mart."

### AI summary (High error rate! Edit errors on video page)

Explains the context of putting $500 billion into perspective in international trade.
Mentions BRICS, an organization of countries challenging the West and the dollar.
Responds to a message criticizing his coverage of BRICS and $500 billion in trade.
Points out the significance of $500 billion in international trade within the context of BRICS.
Compares the $500 billion in trade to Walmart's revenue of $611 billion in 2023.
Emphasizes that BRICS is not yet on par with major Western economic entities.
Provides historical context on the terms "first world" and "third world."
Dismisses doomsday scenarios about the dollar's fate due to BRICS.
Suggests potential expansions of BRICS involving Egypt, Ethiopia, and Saudi Arabia.
Notes the geopolitical influence and significance of these potential expansions.
Stresses the importance of placing large economic numbers in context.

Actions:

for economic analysts, policymakers, activists,
Research and understand the implications of BRICS and $500 billion in international trade (suggested)
Stay informed about geopolitical and economic developments (suggested)
</details>
<details>
<summary>
2023-12-29: Let's talk about a new list in Florida starting New Year's.... (<a href="https://youtube.com/watch?v=hYFBQG08j44">watch</a> || <a href="/videos/2023/12/29/Lets_talk_about_a_new_list_in_Florida_starting_New_Year_s">transcript &amp; editable summary</a>)

New voluntary registry in Florida aims to provide vital information to law enforcement starting January 1st, potentially saving lives despite exposing systemic issues.

</summary>

"This registry is voluntary."
"The fact that it needs to is a total indictment of law enforcement culture."
"Until that occurs, this will save lives."

### AI summary (High error rate! Edit errors on video page)

New registry in Florida starting January 1st created by the Protect Our Loved Ones Act - the Special Persons Registry.
Voluntary registry for individuals with specific conditions like nonverbal, Alzheimer's, or deaf.
Aimed at providing law enforcement with vital information to prevent tragic outcomes.
Information on the registry can be critical in cases of wandering or emergencies.
Requires documentation of the condition, like a birth certificate.
Registry setup by sheriff's departments, cross-referenced between counties.
Aims to prevent misunderstandings and potential tragedies during encounters with law enforcement.
Acknowledges the need for systemic change within law enforcement culture.
Despite mixed feelings, recognizes the potential to save lives through this registry.
Implementation of the database starts on January 1st, likely to happen swiftly.

Actions:

for floridians, caregivers, advocates.,
Register yourself or a loved one on the Special Persons Registry (exemplified).
Ensure documentation of the condition is available for registration (exemplified).
Support initiatives for systemic changes within law enforcement culture (implied).
</details>
<details>
<summary>
2023-12-29: Let's talk about a New Year's resolution, a message, and change.... (<a href="https://youtube.com/watch?v=kKx6yhN13fU">watch</a> || <a href="/videos/2023/12/29/Lets_talk_about_a_New_Year_s_resolution_a_message_and_change">transcript &amp; editable summary</a>)

Beau advises someone with a troubled past to leave behind familiar environments, start fresh elsewhere, and embrace the chance for positive change.

</summary>

"Leave and don't look back."
"Don't go back. My advice is starting today, you start going through your trailer, looking for anything you can't buy again later."
"You got a once-in-a-lifetime shot. Take it."
"Don't come back. The change will follow."
"If you were, you wouldn't want to be a better person."

### AI summary (High error rate! Edit errors on video page)

Someone seeking advice is reflecting on making a big change in their life after turning their life around following a rough past, including jail time and criminal activity.
They have secured a good paying job that involves traveling and building storefronts for a chain business, which will keep them away from negative influences.
Beau encourages the person not to return to their old life and suggests starting fresh in a new location without looking back.
He advises against reconnecting with old acquaintances or revisiting familiar places to avoid falling back into destructive patterns.
Beau recommends packing up only necessary belongings and leaving everything else behind to start a new life unburdened by the past.
Emphasizes the importance of making a clean break and not discussing the old life with others to prevent temptation.
Urges the person to focus on the new opportunities ahead and fully commit to the chance to start fresh.
Beau suggests preparing thoroughly to ensure they never have to return to their previous environment once they leave.
He acknowledges that making significant changes may involve uncomfortable adjustments but assures that the outcome is worthwhile.
Encourages the individual to embrace the unknown, make new connections, and allow positive change to follow their decision to leave the past behind.

Actions:

for individuals seeking a fresh start,
Pack up necessary belongings and leave everything else behind (suggested)
Prepare thoroughly to ensure not having to return to the previous environment (suggested)
Refrain from reconnecting with old acquaintances or revisiting familiar places (suggested)
</details>
<details>
<summary>
2023-12-29: Let's talk about Trump, Maine, and what's next.... (<a href="https://youtube.com/watch?v=YB9tM-nqs0Q">watch</a> || <a href="/videos/2023/12/29/Lets_talk_about_Trump_Maine_and_what_s_next">transcript &amp; editable summary</a>)

The Secretary of State in Maine's decision to exclude Trump from the ballot under the 14th Amendment sets the stage for a Supreme Court showdown on whether Trump's actions constitute insurrection.

</summary>

"Democracy is sacred, so on and so forth."
"It's worth noting that despite the rhetoric being used by those in Trump's circle, most of this is actually being pushed by Republicans, by conservatives."
"There's going to be a lot of back and forth."
"But until it gets to the Supreme Court and the Supreme Court has interpreted the Constitution, then we're really not going to know anything."
"The other proceedings along the way, they don't matter as much, because it's already headed there."

### AI summary (High error rate! Edit errors on video page)

The Secretary of State in Maine decided that Trump will not be on the ballot due to Section 3 of the 14th Amendment after a hearing.
This decision has led to the situation going to the Supreme Court for a final resolution.
Multiple states, including Arizona, Alaska, Nevada, New Jersey, and others, are also involved in similar proceedings.
Despite Trump's circle's rhetoric, it is primarily Republicans and conservatives pushing these actions.
The core issue at hand is whether Trump's actions on January 6th qualify as engaging in insurrection or rebellion under the 14th Amendment.
The Supreme Court will have to determine if Trump's actions fit the criteria outlined in the 14th Amendment.
Until the Supreme Court makes a decision, there will be ongoing developments and no resolution in sight.
Beau expresses skepticism about the outcome, believing that the Court may find a way to argue that the Amendment does not apply to Trump.
The final decision rests with the Supreme Court, and until then, all other actions are secondary.
Updates will be provided as the situation progresses towards the Supreme Court.

Actions:

for legal analysts,
Stay informed about the developments in the case and the Supreme Court proceedings (implied)
Support organizations advocating for the protection of democratic processes (implied)
</details>
<details>
<summary>
2023-12-28: The Roads to What Started the Civil War (<a href="https://youtube.com/watch?v=pKc4s2-Ai1Q">watch</a> || <a href="/videos/2023/12/28/The_Roads_to_What_Started_the_Civil_War">transcript &amp; editable summary</a>)

Beau presents primary sources to debunk the manufactured debate around the true cause of the Civil War, which is unequivocally about slavery.

</summary>

"There is no academic debate over this. It doesn't exist."
"Understanding these, if people actually read this, read the cornerstone speech, and had that context for this discussion it wouldn't be taking place because anybody who said it was about anything other than slavery would be laughed at."
"They told you what it was about."
"Sometimes all you need to put something like this to rest is a little bit more context, a little bit more information."
"There is no debate over this. You don't have to ask Lincoln. You don't have to ask the Union. You just have to read their documents."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of understanding American history and specifically addresses the debate surrounding it.
He argues that the debate is manufactured and not based on primary sources, especially when it comes to discussing the Civil War.
Beau mentions how discussing unpopular opinions can lead to cringing reactions, particularly when it comes to teaching the founding documents of the Confederacy.
He criticizes the lack of acknowledgment of slavery as the primary cause of the Civil War in modern debates.
Beau reads excerpts from statements of reasons from states like Georgia, Mississippi, South Carolina, Texas, and Virginia, which clearly state that slavery was a major reason for secession.
These excerpts illustrate how these states tied slavery to their economic interests, civilization, and identity.
Beau stresses that these primary documents clearly show that the Civil War was fundamentally about slavery.
He mentions the Cornerstone Speech and how it further solidifies the Confederacy's belief in slavery as a cornerstone of their society.
Beau points out that by reading these primary sources, the debate over the Civil War's cause should be settled.
He concludes by urging proper historical education to understand the true context of events like the Civil War.

Actions:

for history enthusiasts, educators, activists,
Read primary sources on American history (suggested)
Educate others on the true causes of historical events (implied)
</details>
<details>
<summary>
2023-12-28: Let's talk about an update and the US-Mexico discussions.... (<a href="https://youtube.com/watch?v=9l_8dE8Hf_I">watch</a> || <a href="/videos/2023/12/28/Lets_talk_about_an_update_and_the_US-Mexico_discussions">transcript &amp; editable summary</a>)

Mexico's unusual request leads to rapid diplomatic interactions with the U.S., potentially signaling positive developments in regional relations and border issues.

</summary>

"The chain of events and the speed at which it took place."
"There is no other way to describe this."
"Those two things right there would do a lot to alleviate the reason people are leaving their home countries."
"It's just a thought."
"You have a good day."

### AI summary (High error rate! Edit errors on video page)

Mexico made an unusual request to the Biden administration, prompting speculation about hidden intentions and public signaling.
State Department officials are in Mexico asking for help with the border, with Mexico also requesting progress on relations with Cuba and Venezuela.
There are additional requests for regional development assistance and the reopening of border crossings for trade purposes.
The quick response from the State Department to Mexico's request raises questions about potential agreements and outcomes.
Speculation arises whether the U.S. might warm towards Cuba and support regional development, all while excluding Venezuela.
The chain of events from Mexico's initial request to State Department officials being in Mexico has been rapid and unique.
The scenario is either strategic political maneuvering or a genuine solution proposed by Mexico to address existing issues.
The hope is for the U.S. to positively respond to regional development and improving relations with Cuba to address migration issues.
The outcome of the ongoing negotiations remains uncertain, with potential interesting developments on the horizon.
Beau anticipates significant results from the interactions between Mexico and the U.S., particularly in addressing migration challenges.

Actions:

for diplomatic analysts,
Contact local representatives to advocate for positive diplomatic relations with neighboring countries (implied).
Stay informed about developments in regional diplomacy and potential impacts on immigration issues (implied).
</details>
<details>
<summary>
2023-12-28: Let's talk about Michigan, betrayal, and Trump.... (<a href="https://youtube.com/watch?v=4xdQ9Y_Tq0Y">watch</a> || <a href="/videos/2023/12/28/Lets_talk_about_Michigan_betrayal_and_Trump">transcript &amp; editable summary</a>)

Michigan Supreme Court upholds decision against removing Trump, while elector cooperation may lead to broader investigations and potential parallels to Georgia case. Investigation ongoing.

</summary>

"He felt betrayed, that's all I can say."
"Renner provided information that might lead authorities to look at some of the other people."
"It's worth remembering that the investigation is still ongoing."
"This may be a situation where we end up with another Georgia style case."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Michigan Supreme Court upheld decision against removing Trump from the ballot, based on procedural grounds.
No determination made by the court on whether Trump engaged in insurrection.
Elector Renner, 77, made a deal with investigators and is cooperating.
Renner described feeling betrayed during January 6 hearings.
Renner was ushered through the process by others and was not fully aware of his actions.
Renner is the only elector who has made a deal so far; investigation is ongoing.
Renner's cooperation may lead authorities to look at others involved in the process.
Possibility of more individuals getting wrapped up in the investigation.
Michigan authorities might scrutinize those who guided Renner through the process.
Situation could escalate similar to the Georgia case depending on further revelations.
Investigation outcome remains uncertain; more developments could arise.
Renner's cooperation might lead to higher-level inquiries.
Michigan investigation still in progress; future actions and outcomes unclear.
Potential for more individuals to be implicated in the ongoing investigation.
Speculation on parallels between Michigan case and Georgia situation.

Actions:

for michigan residents,
Cooperate with investigators if involved in any irregularities (exemplified)
Stay informed about the ongoing investigation (suggested)
</details>
<details>
<summary>
2023-12-28: Let's talk about Boebert running from her district in Colorado.... (<a href="https://youtube.com/watch?v=UvRcj4-D9AM">watch</a> || <a href="/videos/2023/12/28/Lets_talk_about_Boebert_running_from_her_district_in_Colorado">transcript &amp; editable summary</a>)

Representative Lauren Boebert of Colorado changes districts due to likely loss in the previous one, raising questions about her move's true safety and implications for her faction within the Republican Party.

</summary>

"Having to leave your district in this way is a bad sign, not just for Boebert, but for that entire faction of the Republican Party."
"It does seem as though people are more focused on the more high-profile personal behavior of the representative."
"We'll probably see other things similar to this maybe not quite as dramatic."

### AI summary (High error rate! Edit errors on video page)

Representative Lauren Boebert of Colorado is changing the district she's running in.
She decided not to run in the third district, where she's been, but instead will run in the fourth district.
Boebert's move stems from the likely chance of losing in the third district, as indicated by major fundraising disparities and other issues.
Despite the third district leaning towards the GOP by nine points, Boebert wasn't considered safe.
The fourth district leans 27 points towards the GOP, making it theoretically a safer choice for her.
Beau questions the actual safety of Boebert's move, considering various factors like Democratic Party responses and voter turnout.
Boebert's district change could be a negative sign not only for her but also for her faction within the Republican Party.
Boebert's attempt to rebrand herself as a policy-focused figure hasn't been very successful, with attention still drawn to her high-profile personal behavior.
This situation may indicate the challenges faced by those deeply tied to the MAGA movement in redefining themselves as the influence of Trump wanes.
Beau predicts similar shifts in the future, possibly less drastic, as individuals adapt to post-Trump political landscapes.

Actions:

for political observers,
Monitor and participate in local political developments (suggested)
Stay informed about district changes and their implications (suggested)
</details>
<details>
<summary>
2023-12-28: Let's talk about 2024 and change in belief.... (<a href="https://youtube.com/watch?v=6GP39eCGlyk">watch</a> || <a href="/videos/2023/12/28/Lets_talk_about_2024_and_change_in_belief">transcript &amp; editable summary</a>)

Beau introduces the concept of the "unlikely non-voter" and raises concerns about the impact of voter confidence on future elections, especially within the Republican Party.

</summary>

"The unlikely voter."
"One out of three is huge."
"It might lead them to believe that, well, it doesn't matter."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of the "unlikely non-voter" as a new category to watch in future elections.
Mentions the impact of unlikely voters who traditionally don't participate but could sway an election if they do.
Refers to a recent AP-NORC poll that asked about people's confidence in their vote being correctly counted in the primary elections.
Notes that 32% of Republicans had little to no confidence that their vote in the primary election will be correctly counted, compared to single-digit percentages for Democrats.
Speculates on reasons why people choose not to vote, citing lack of belief that their vote will matter as a significant factor.
Suggests that perpetuating claims about election fraud may lead to long-term implications for the Republican Party, potentially driving more voters to believe that their vote doesn't matter.
Raises concerns about the impact of baseless theories on voter confidence and the potential rise of unlikely non-voters within the Republican Party's base.
Emphasizes the importance of paying attention to this demographic shift and its potential consequences for future elections.

Actions:

for voters, political analysts,
Monitor voter confidence levels within different demographics (suggested)
Engage with individuals who express little confidence in the voting process (implied)
</details>
<details>
<summary>
2023-12-27: Let's talk about Ukraine and ships.... (<a href="https://youtube.com/watch?v=U6YBz0s94TA">watch</a> || <a href="/videos/2023/12/27/Lets_talk_about_Ukraine_and_ships">transcript &amp; editable summary</a>)

A Russian landing ship's destruction impacts Ukrainian morale and Russian naval power goals, leading to changes in fleet deployments.

</summary>

"Destroyed seems like a better fit."
"This is going to do two things really."
"Every ship that is damaged kind of sets that goal a little bit back."

### AI summary (High error rate! Edit errors on video page)

Ukraine and Russia, along with their ships, are in focus, with a recent incident involving a Russian landing ship being struck by guided missiles fired from warplanes.
The ship was believed to be carrying ammunition and drones at the time of the attack, with Ukraine claiming it was destroyed while Russia states it was damaged.
The footage suggests the ship was likely destroyed and may not be operational for a significant period.
The incident is a significant boost to Ukrainian morale, especially with previous losses suffered by the Black Sea fleet.
This event makes it harder for Russia to transport supplies and equipment into Crimea, potentially causing changes in Russian naval deployments.
Russian naval power, a key focus for Putin, is impacted by each damaged ship in terms of repair time and costs.

Actions:

for military analysts, policymakers,
Analyze the impact of the recent incident on Ukrainian morale and Russian naval capabilities (suggested)
Stay informed about developments in the region and potential changes in Russian fleet deployments (suggested)
</details>
<details>
<summary>
2023-12-27: Let's talk about Republicans being right about Trump's immunity.... (<a href="https://youtube.com/watch?v=E60SKpkuKF4">watch</a> || <a href="/videos/2023/12/27/Lets_talk_about_Republicans_being_right_about_Trump_s_immunity">transcript &amp; editable summary</a>)

Republicans, including former officials, challenge Trump's claim of presidential immunity, warning of constitutional subversion.

</summary>

"Nothing in our Constitution or any case supports former President Trump's dangerous argument for criminal immunity."
"You have to decide whether you want to support Trump and his aims, or you're going to support the U.S. Constitution."

### AI summary (High error rate! Edit errors on video page)

Republicans, including former officials from five different administrations, filed an amicus brief opposing Trump's claim of wide-ranging presidential immunity from criminal prosecution.
They argue that granting such immunity could embolden former presidents to commit criminal acts to prevent their successors from taking office.
Republicans are using a constitutional basis to refute Trump's claim, stating that nothing in the Constitution supports his argument for criminal immunity.
The court accepted the brief, indicating a significant challenge from a group of Republicans against the former president.
This opposition from Republicans showcases a divide within the party between actual conservatives and authoritarians on the right.
The likelihood of the former president prevailing on this appeal seems low.
Beau reminds viewers that supporting Trump's aims may go against the U.S. Constitution, as pointed out by the Republicans who believe he is trying to subvert the Constitution through this appeal.

Actions:

for political observers, republican voters,
Support efforts challenging unconstitutional claims by former officials (exemplified)
Educate others on the importance of upholding constitutional principles (implied)
</details>
<details>
<summary>
2023-12-27: Let's talk about Colorado and wolves.... (<a href="https://youtube.com/watch?v=o2CQShMmq7o">watch</a> || <a href="/videos/2023/12/27/Lets_talk_about_Colorado_and_wolves">transcript &amp; editable summary</a>)

Colorado reintroduces gray wolves despite challenges and seeks public support for ongoing success in preserving these vital species.

</summary>

"Wolves are great for the environment."
"Sometimes wolves have to be reintroduced more than once."
"There's a lot of fear-mongering but it is far beyond just protecting a species."
"Just be aware there may be a need for public support to get another win."
"It's good news and things are moving in the right direction."

### AI summary (High error rate! Edit errors on video page)

Colorado is reintroducing gray wolves despite legal challenges.
Starting with five wolves, the goal is to reach 15 by March.
Wolves play a vital role in the environment beyond species reintroduction.
Documentaries on Yellowstone show the positive impact of wolf reintroduction.
Efforts to preserve wolves can be cyclical, needing public support.
Wolves were brought from Oregon for reintroduction in Colorado.
Reintroductions may need public support to ensure long-term success.
Research the benefits of successfully reintroducing wolves into ecosystems.
Fear-mongering about wolves should not overshadow their positive impact.
Public support may be necessary for future reintroduction efforts.

Actions:

for conservationists, wildlife enthusiasts,
Research the benefits of wolf reintroduction in ecosystems (suggested)
Stay informed and support future reintroduction efforts (implied)
</details>
<details>
<summary>
2023-12-27: Let's talk about Arizona, Lake, and Richer.... (<a href="https://youtube.com/watch?v=CEfd2fXd_Fw">watch</a> || <a href="/videos/2023/12/27/Lets_talk_about_Arizona_Lake_and_Richer">transcript &amp; editable summary</a>)

Kerry Lake faces a defamation case in Arizona for echoing election fraud theories akin to Trump's, potentially impacting her 2024 Senate candidacy and echoing the Georgia election worker scenario.

</summary>

"Lake's statements about fraudulent ballots were deemed actionable by the judge under Arizona law."
"Stephen Richard may become richer because of this."
"It will probably impact the senatorial campaign out in Arizona."

### AI summary (High error rate! Edit errors on video page)

Kerry Lake, a 2022 Arizona candidate, lost and echoed Trump's election rhetoric with little evidence.
Stephen Richer faced defamation theories similar to Georgia election workers.
Lake's statements about fraudulent ballots were deemed actionable by the judge under Arizona law.
The case will proceed, allowing Lake to present evidence while Richer can rely on court determinations.
Richer might benefit financially from the case.
Lake's Senate candidacy in 2024 could be impacted by the outcome.
The situation may influence the senatorial campaign in Arizona, potentially alienating moderates.
Expect a similar case to the Georgia election worker scenario unfolding in Arizona.

Actions:

for political observers, arizona residents,
Watch closely how the case unfolds and impacts the political landscape in Arizona (implied)
</details>
<details>
<summary>
2023-12-26: Let's talk about a request from Mexico.... (<a href="https://youtube.com/watch?v=6c1Z3S0rub8">watch</a> || <a href="/videos/2023/12/26/Lets_talk_about_a_request_from_Mexico">transcript &amp; editable summary</a>)

Speculation surrounds public statements for foreign policy consumption after Mexico's offer to assist the U.S. southern border prompts questions about ties to developmental aid and talks with Cuba, aiming to garner Republican support through border-related incentives, despite lacking evidence of coordination.

</summary>

"It is time to re-evaluate the U.S. relationship with Cuba."
"The overwhelming majority of them, the situation was caused by bad US foreign policy."
"Tie it to the border."
"It is time for the U.S. to clean up its mess."
"That is how things at times are created for public consumption."

### AI summary (High error rate! Edit errors on video page)

Speculation arises about public statements designed for foreign policy consumption after Mexico offers to help with the U.S. southern border in exchange for assistance in migrants' home countries and re-examining the relationship with Cuba.
The majority of issues in migrants' home countries stem from bad US foreign policy, necessitating the US to clean up its own mess there.
The US policy towards Cuba is outdated, reflecting Cold War dynamics and warrants reevaluation.
The Mexican president's public request for assistance and policy changes raises questions about how to garner Republican support for such initiatives.
Tying developmental assistance and talks with Cuba to border security could potentially generate bipartisan support but lacks evidence of coordination.
While the idea of orchestrating public requests for political gain is intriguing, there is no proof of such manipulation.
Despite the lack of evidence, the scenario sheds light on how public statements can be tailored for specific outcomes without certainty of the truth coming to light.
The need to reassess the US-Cuba relationship and address past foreign policy mistakes is emphasized.
Generating Republican support for policy changes through border-related incentives is suggested as a strategic approach.
The concept of orchestrating public requests for political advantage remains speculative and unlikely to be confirmed.

Actions:

for policymakers, advocates,
Advocate for reevaluating US foreign policy towards Cuba and addressing past mistakes (suggested)
Support initiatives that aim to provide developmental assistance to migrants' home countries (suggested)
Engage in bipartisan efforts to generate support for policy changes through strategic approaches (suggested)
</details>
<details>
<summary>
2023-12-26: Let's talk about Wisconsin maps changing.... (<a href="https://youtube.com/watch?v=LXzWl7moRnY">watch</a> || <a href="/videos/2023/12/26/Lets_talk_about_Wisconsin_maps_changing">transcript &amp; editable summary</a>)

Wisconsin faces consequential redistricting issues, with new maps expected before the next election after a decade of partisan suppression of voter voices.

</summary>

"Wisconsin is widely viewed as the most gerrymandered state in the country."
"They threatened to impeach her if she took a case. And she was like, yeah, no, I'm taking the case."
"The odds are it's going to go to the court."
"It is likely going to alter the makeup of the legislature in Wisconsin."
"Having their voice and their vote diluted by people who wanted to rule rather than represent."

### AI summary (High error rate! Edit errors on video page)

Wisconsin is considered the most gerrymandered state in the country.
Justice Protasewicz correctly identified this fact during her campaign, causing discomfort among Republicans who created the contentious maps.
Republicans threatened to impeach Justice Protasewicz if she took a case on the gerrymandered maps, but she proceeded with it.
The court ruled that the maps in Wisconsin are indeed gerrymandered.
New maps are expected to be ready for use by 2024, with the legislature having the first chance to submit them by August.
If the submitted maps are deemed partisan, the court will step in and select a map.
Due to widespread gerrymandering, the Wisconsin legislature heavily leans Republican, raising concerns about their map submission.
The redistricting will likely impact the makeup of the legislature, with predictions that Republicans may retain control of the assembly but lose the Senate.
This redistricting issue in Wisconsin is seen as highly consequential and is nearing conclusion.
The resolution and new maps are expected before the next election, providing fair representation after a decade of partisan maps suppressing voter voices.

Actions:

for wisconsin residents, political activists,
Contact local representatives to advocate for fair and non-partisan redistricting (suggested)
Stay informed about the redistricting process and attend relevant community meetings (implied)
</details>
<details>
<summary>
2023-12-26: Let's talk about Egypt's peace plan.... (<a href="https://youtube.com/watch?v=mPjnFmHuHBs">watch</a> || <a href="/videos/2023/12/26/Lets_talk_about_Egypt_s_peace_plan">transcript &amp; editable summary</a>)

Egypt's peace plan for Israeli-Palestinian conflict, while incomplete, offers advantages over the U.S. plan, raising hope for progress.

</summary>

"It's hopeful."
"I wouldn't write this one off yet."
"It's not a bad starting point."
"It's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Egypt has proposed a peace plan for the Israeli-Palestinian conflict, which is being compared to the U.S. plan.
The U.S. plan involves making Hamas combat-ineffective, allowing the Palestinian Authority to govern Gaza, and deploying a multinational coalition to prevent further fighting.
The Egyptian plan calls for the phased return of captives and governance of Gaza and the West Bank by a new entity of Palestinian experts.
A key advantage of the Egyptian plan is the lack of bad blood between Hamas and the new entity, unlike with the Palestinian Authority.
However, the Egyptian plan does not address security concerns or the force needed to stand between the two sides.
Another advantage of the Egyptian plan is that it is not from the U.S., potentially making it more acceptable due to the U.S.'s track record.
Both sides were not enthusiastic about the plan but did not outright reject it, showing some hope for progress.
Questions remain about who the Palestinian experts will be and how they will be chosen, raising concerns about U.S. involvement in the selection process.
While incomplete, parts of the Egyptian plan seem more feasible than the U.S. plan.
The plan's success hinges on addressing missing details and filling in the blanks to determine its viability.

Actions:

for peace advocates, policymakers,
Monitor updates on the Egyptian peace plan and advocate for a comprehensive resolution (implied).
Support initiatives that prioritize peaceful solutions to the Israeli-Palestinian conflict (implied).
Stay informed about developments in the region to better understand the implications of different peace proposals (implied).
</details>
<details>
<summary>
2023-12-26: Let's talk about Christmas coming early in Ukraine.... (<a href="https://youtube.com/watch?v=DxTVq6Lctps">watch</a> || <a href="/videos/2023/12/26/Lets_talk_about_Christmas_coming_early_in_Ukraine">transcript &amp; editable summary</a>)

Ukraine's shift to celebrating Christmas on December 25th signifies a deep resistance to Russian influence, foreshadowing significant challenges for any potential invasion.

</summary>

"Changing a tradition like the Christmas date is indicative of a population that rejects external influence."
"The Christmas date change in Ukraine signals a strong resistance to foreign politics and traditions."
"Resistance in Ukraine, as shown by the Christmas celebration shift, indicates peace won't come easily for Russia."

### AI summary (High error rate! Edit errors on video page)

Ukraine celebrated Christmas on December 25th, deviating from their traditional celebration in early January.
The shift in the Christmas celebration date could be seen as a rejection of Russian influence in Ukraine.
The decision to celebrate Christmas on December 25th wasn't just a government proclamation; it was widely embraced by the population.
The shift in Christmas celebration date indicates a strong resistance to Russian politics and traditions in Ukraine.
Changing a long-standing tradition like the Christmas celebration date signifies a population's rejection of external influence.
If Russia were to advance into Ukraine, the resistance from the population, evident in the Christmas date change, shows that peace wouldn't easily follow.
The widespread celebration of Christmas on December 25th in Ukraine is a significant domestic indicator of political sentiments.
This change in Christmas celebration is more than just a foreign policy issue; it symbolizes a strong rejection of external influences.
The population's willingness to change traditions for political reasons shows a deep-rooted resistance to foreign powers.
The Christmas date change in Ukraine is a critical consideration for Russian war planners.

Actions:

for international observers,
Monitor and analyze political and cultural shifts in Ukraine (implied)
</details>
<details>
<summary>
2023-12-25: Let's talk about phone calls today.... (<a href="https://youtube.com/watch?v=uh7buKvUnMs">watch</a> || <a href="/videos/2023/12/25/Lets_talk_about_phone_calls_today">transcript &amp; editable summary</a>)

Christmas is a day that can swing from extreme happiness to loneliness for many, making it the perfect time to reach out and connect with isolated individuals through a simple phone call, which can make a significant difference.

</summary>

"Christmas is a day that can be really happy or really sad for people who are isolated or not talking to someone."
"A few-minute phone call can make all the difference for some people."
"It might mean the world of difference to someone."

### AI summary (High error rate! Edit errors on video page)

Christmas is a day that can be really happy or really sad for people who are isolated or not talking to someone.
Today is a perfect day to reach out to someone through a call or text, as it won't seem out of place.
Making a call to someone who may be isolated can make a huge difference in their day.
If you are having a happy day with family around, think of those who may be more isolated and reach out to them.
A simple phone call to someone new in town or isolated can mean the world to them.
Reaching out doesn't have to seem like checking up on someone; it can just be a friendly chat because of the holiday.
Connecting with someone today can help in reconnecting with them or making sure they are okay.
Taking a few minutes to call someone can have a significant impact, especially on a potentially lonely day like Christmas.
It's a good idea to include reaching out to isolated individuals in your plans for the day.
A small effort in making a call can make a big difference in someone's day.

Actions:

for those celebrating christmas.,
Reach out to someone new in town or isolated with a phone call to make sure they're doing okay (suggested).
Call someone you haven't spoken to in a while to reconnect or to simply spread some holiday cheer (suggested).
</details>
<details>
<summary>
2023-12-25: Let's talk about belief, Trump, and SCOTUS.... (<a href="https://youtube.com/watch?v=nolVhvRblFs">watch</a> || <a href="/videos/2023/12/25/Lets_talk_about_belief_Trump_and_SCOTUS">transcript &amp; editable summary</a>)

Beau dissects Trump's belief in presidential immunity and his strategy to maintain support through legal processes, questioning the former president's true intentions.

</summary>

"Trump has presidential immunity, period. End of story."
"The idea that presidential immunity exists to the level the former president is kind of saying is silly."
"I think his goal is to try to drag it out as long as possible and stop it from going to the Supreme Court in hopes that he can once again kind of trick a whole bunch of people into supporting him."

### AI summary (High error rate! Edit errors on video page)

Beau delves into Trump, the United States Supreme Court, and beliefs regarding presidential immunity.
Beau addresses a message questioning his beliefs and points out that Trump may share the same belief.
Trump believes that the Supreme Court will rule against him, as indicated by his actions.
Trump's statement about presidential immunity and his reaction to the Supreme Court's decision indicate his belief that the Court will rule against him.
Beau questions the extent of presidential immunity claimed by the former president.
Beau believes that Trump's goal may be to prolong the legal process to maintain support rather than truly believing he will win.
The former president has made inaccurate claims in the past, casting doubt on his current statements.

Actions:

for legal analysts, political commentators,
Contact legal experts to understand the implications of presidential immunity (implied)
</details>
<details>
<summary>
2023-12-25: Let's talk about a question about progress.... (<a href="https://youtube.com/watch?v=-kV6g4MtRdU">watch</a> || <a href="/videos/2023/12/25/Lets_talk_about_a_question_about_progress">transcript &amp; editable summary</a>)

Beau addresses the disconnect between marginalized groups and societal perceptions, urging support for change in a system that currently excludes and mistreats them.

</summary>

"A child that is not embraced by the village will burn it down to fill its warmth."
"If you want them to respect a system, it has to be a system worthy of respect."
"You have identified things that you will say is them getting a raw deal."

### AI summary (High error rate! Edit errors on video page)

Addresses the issue of groups left out in the United States who get a raw deal and how they are perceived by others.
Reads a message from a conservative individual questioning why minority groups seem to dislike America and American values.
Responds by pointing out that these groups have been marginalized and mistreated by society, making it hard for them to support the country.
Advises the conservative individual to support these groups on the issues where they acknowledge they are mistreated, even if they don't agree on everything.
Emphasizes the need for the system to change to be inclusive of those who have been historically marginalized.
Uses the analogy of a child not embraced by the village burning it down for warmth to explain the anger and desire for change among marginalized groups.
Encourages advocating for change in the system to earn respect rather than expecting support for an unjust system.
Suggests that actively supporting marginalized groups will lead to a deeper understanding of their struggles and potentially reveal more injustices.

Actions:

for conservative individuals, allies, advocates,
Support marginalized groups on the issues where they are mistreated (implied)
Advocate for systemic change to be inclusive of marginalized communities (implied)
Listen to and understand the struggles of marginalized groups by actively engaging with them (implied)
</details>
<details>
<summary>
2023-12-25: Let's talk about Biden's pardons and effectiveness.... (<a href="https://youtube.com/watch?v=YV9gGuvV9f8">watch</a> || <a href="/videos/2023/12/25/Lets_talk_about_Biden_s_pardons_and_effectiveness">transcript &amp; editable summary</a>)

The Biden administration's pardons for marijuana possession set an example for governors, aiming to lead by example rather than create a widespread impact.

</summary>

"No one should be in a federal prison solely due to the use or possession of marijuana."
"The real goal probably wasn't to just help the few thousand that are going to be impacted directly."
"The overwhelming majority of those charges occur at the state level."
"It's a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The Biden administration recently expanded pardons for simple possession of marijuana.
The impact is more about finding housing or employment rather than widespread.
To receive benefits from the pardon, individuals must apply for a certificate from the DOJ.
Most marijuana charges occur at the state level, not federal, limiting Biden's impact.
Biden's goal is to set an example for governors to follow suit in pardoning similar offenses.
This move signifies a potential shift in how federal charges are treated.
The main aim is to encourage Democratic governors to take similar actions.
The focus is on setting a precedent rather than directly impacting a large number of individuals.
The majority of marijuana charges are state-level, limiting federal impact.
The Biden administration is aiming to lead by example and encourage other governors to act similarly.

Actions:

for policy advocates, activists, governors.,
Reach out to local governors to advocate for similar pardoning actions (implied).
Support policies that prioritize pardons for simple possession of marijuana (implied).
</details>
<details>
<summary>
2023-12-24: The Roads Not Taken EP19 (<a href="https://youtube.com/watch?v=WNC2Z-zQ-YQ">watch</a> || <a href="/videos/2023/12/24/The_Roads_Not_Taken_EP19">transcript &amp; editable summary</a>)

Beau covers unreported news from December 24th, 2023, including international tensions, U.S. developments, and societal debates, ending with a festive Southern tradition.

</summary>

"It really is that simple."
"I mean, it's a Southern tradition for New Year's. You eat black-eyed peas, you get good luck. It's really that simple."
"That's what those are. Why? For New Year's."

### AI summary (High error rate! Edit errors on video page)

The episode covers unreported or underreported news from the past week on December 24th, 2023.
Germany is deploying troops to Lithuania as a deterrent against Russia, pressuring other NATO members to increase their presence.
Putin authorized the seizure of energy assets from "unfriendly countries," likely impacting Germany and Austria.
Top Chinese and U.S. military officials are reestablishing communication channels for potential de-escalation.
U.S. news includes the release of Glenn Simmons after being wrongfully convicted and Congress being on vacation with pressing matters awaiting their return.
GOP candidates threaten to withdraw from the Colorado primary if Trump is not on the ballot.
Steve Bannon claims the Navy SEALs have become "woke" and "Hollywood."
Scientists simulated a runaway greenhouse gas scenario turning Earth into Venus, reaching temperatures over 900 degrees.
Social media debates whether Trump has a particular odor, sparking conspiracy theories.
Beau addresses various questions, including the use of UN forces, changes in the Republican Party, NATO's actions in Crimea, and the fate of Mount Rushmore.

Actions:

for news enthusiasts,
Contact local representatives to advocate for diplomatic solutions to international tensions (implied)
Support organizations working towards criminal justice reform to prevent wrongful convictions (implied)
Engage in critical discourse and fact-checking to combat misinformation on social media (implied)
</details>
<details>
<summary>
2023-12-24: Let's talk about Trump, Nikki Haley, and the VP spot.... (<a href="https://youtube.com/watch?v=pQtaQv1TRQY">watch</a> || <a href="/videos/2023/12/24/Lets_talk_about_Trump_Nikki_Haley_and_the_VP_spot">transcript &amp; editable summary</a>)

A poll showing Nikki Haley close to Trump sparks VP speculation, unsettling him with the possibility of facing a more electable opponent.

</summary>

"Fake New Hampshire poll was released on Birdbrain, which I guess is what he's calling her."
"If you can't beat her, get her to join you, type of thing."
"Realistically, yeah, Trump should be a little concerned about running against her."
"Probably touched a raw nerve, but realistically, she probably is more electable than him."
"I don't know that she would take him up on that offer, at least not immediately, because she might understand that right now it looks like the momentum is with her."

### AI summary (High error rate! Edit errors on video page)

A recent poll in New Hampshire put Nikki Haley close to Trump in preferred candidate ratings, sparking speculation about her potential as his VP.
Trump reacted negatively to the poll, calling it fake and suggesting that Fox News will exploit it.
Speculation arose about Haley being a suitable VP choice from within Trump's circle but was quickly shut down, possibly to avoid giving the impression that Trump is scared of her.
Despite being underestimated by many, Haley possesses considerable resources, connections, and conservative support.
As Trump faces mounting challenges, Haley could emerge as a more electable candidate, similar to how Biden was viewed as electable despite initial skepticism.
Trump losing to Biden likely struck a nerve, and Haley being perceived as more electable could further unsettle him.
If Haley continues to perform well, Trump may have to put aside his pride and approach her as a potential VP, although she may not accept immediately given her current momentum.

Actions:

for political analysts,
Monitor and analyze political polls and developments (exemplified)
Stay informed about potential VP choices and their implications (suggested)
</details>
<details>
<summary>
2023-12-24: Let's talk about Japan, Ukraine, the US, and the UK.... (<a href="https://youtube.com/watch?v=yEv12soju58">watch</a> || <a href="/videos/2023/12/24/Lets_talk_about_Japan_Ukraine_the_US_and_the_UK">transcript &amp; editable summary</a>)

Japan plans to export missiles to the U.S., who will then send their own to Ukraine, navigating around Japan's policy of not exporting weapons to countries at war, strengthening alliances without direct involvement in conflicts.

</summary>

"Japan is going to export to the United States, Patriots, the missiles."
"In the international poker game where everybody's cheating, Japan just slid the US some chips."
"This move for the Japanese government, it's a smart move."

### AI summary (High error rate! Edit errors on video page)

Japan plans to export Patriots missiles to the United States, which they make under license from Mitsubishi.
The United States will likely send their own US-made Patriots to Ukraine in return.
Japan has a policy of not exporting weapons to countries at war, hence the workaround through the United States.
Japan's reluctance to directly involve itself in wars is due to its history and sensitivity.
This strategy allows support to flow without Japan violating its core tenets.
Similar situations might occur with other countries with export policies like Japan's.
Japan's move strengthens the U.S.-Japanese relationship significantly.
Japan's support enhances the U.S.'s stockpiles without the risk of dropping below required levels.
The dynamic is like an international poker game, where Japan provides support to its allies.
The United Kingdom might also receive support from Japan in a similar fashion.

Actions:

for foreign policy analysts,
Contact organizations advocating for peaceful resolutions to conflicts (implied)
Support diplomatic efforts to prevent escalation of conflicts (implied)
</details>
<details>
<summary>
2023-12-24: Let's talk about Iowa, food, and spending.... (<a href="https://youtube.com/watch?v=m1AZfOfBHps">watch</a> || <a href="/videos/2023/12/24/Lets_talk_about_Iowa_food_and_spending">transcript &amp; editable summary</a>)

Iowa's refusal to participate in a food assistance program for low-income families raises concerns about prioritizing publicity stunts over children's meals, prompting a critical look at politicizing basic needs.

</summary>

"When you have a country that is trying to politicize and debate whether or not kids should eat, I suggest that's a problem."
"For the nine billionth time on this channel, a group of people who have less institutional power than you do are never the source of your problems."
"Those in power just get you looking down at them, so you're not paying attention to what those in power are doing."
"Assuming that's occurring, the worst-case scenario here is that somebody using this framing paid their taxes and that money went back to their kid."
"Anyway, y'all enjoy your holiday meals. It's just a thought. Have a good night."

### AI summary (High error rate! Edit errors on video page)

Raises the issue of Iowa's decision to not participate in a summer program for EBT, impacting low-income families.
Explains the program provides $40 per month per child to help with food costs during school breaks.
Points out the state's cost for this program is only 50% of administrative costs, around $2 million.
Criticizes the governor's choice to prioritize a publicity stunt over funding for children's meals.
Calls out politicians for using vulnerable groups as scapegoats and distracting from real issues.
Dismisses the argument that tax dollars are being wasted on providing food for kids who may not be deemed "needy."
Argues against denying food assistance to those in need based on the worst-case scenario of a few who may not qualify benefiting.
Expresses concern over the politicization of children's basic needs like food.

Actions:

for advocates for child welfare,
Contact local representatives to express support for programs providing food assistance to low-income families (suggested).
Donate to local food banks or organizations supporting children's nutrition (exemplified).
</details>
<details>
<summary>
2023-12-24: Let's talk about Biden and something not happening.... (<a href="https://youtube.com/watch?v=Zj0HLZDwD6A">watch</a> || <a href="/videos/2023/12/24/Lets_talk_about_Biden_and_something_not_happening">transcript &amp; editable summary</a>)

The Biden administration averted a potential conflict by intervening to prevent Israel from engaging in a preemptive strike against Hezbollah, which could have sparked a wider regional conflict.

</summary>

"We came way closer than anybody realizes to this being a much wider conflict."
"Had this occurred, it absolutely would have triggered a wider regional conflict."
"It is incredibly important to note that Israel is of course denying this whole bit of the reporting."
"Looks like we don't have to wait until the end of the administration."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Speculates on a potential conflict that almost occurred involving Israel and Hezbollah.
Mentions how the Biden administration intervened to prevent a preemptive strike.
Notes the significant impact such a strike could have had on the region.
Describes the tensions between Israeli forces and Hezbollah.
Emphasizes the importance of avoiding a wider regional conflict.
Points out that Israel denied the reported plans for a strike.
Mentions the planes being in the air, indicating how close the situation came to escalation.

Actions:

for global citizens,
Contact local representatives to advocate for peaceful resolutions to conflicts (implied)
Stay informed about international developments and conflicts (implied)
</details>
<details>
<summary>
2023-12-23: Let's talk about the misery index telling us to be less miserable.... (<a href="https://youtube.com/watch?v=Oc8T0oyvh_4">watch</a> || <a href="/videos/2023/12/23/Lets_talk_about_the_misery_index_telling_us_to_be_less_miserable">transcript &amp; editable summary</a>)

Beau introduces the "Misery Index," an economic indicator reflecting everyday conditions, noting recent improvements and cautioning against crediting presidents with economic changes.

</summary>

"The misery index is basically adding the rate of inflation with the seasonally adjusted unemployment rate."
"Presidents don't actually control the economy."
"It does appear that people should finally start actually feeling the improvements in the economy that everybody's been talking about for half a year."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of the "Misery Index" as a key economic indicator often overlooked.
Defines the Misery Index as the sum of the inflation rate and the unemployment rate.
Points out that the Misery Index is a more accurate reflection of economic conditions for everyday people than traditional economic metrics.
Shares historical context: In June 1980, the Misery Index reached its highest point at 21.98.
Mentions that since the 2000s, the average Misery Index has been 8.3, currently standing at 6.8, the lowest since the pandemic started.
Notes that economic indicators favored by economists may not immediately impact average people, but current conditions suggest improvements are reaching them.
Reports a shift in outlook from Bank of America regarding the likelihood of a recession, now leaning towards a more positive outcome.
Emphasizes that presidents do not control the economy, attributing economic improvements to factors like consumer confidence and effective spending of injected money.
Concludes that people should start feeling the positive economic changes that have been discussed for the past six months.
Wraps up by leaving viewers with a thought to ponder on.

Actions:

for economic enthusiasts,
Monitor economic indicators and understand their impact on average people (implied).
</details>
<details>
<summary>
2023-12-23: Let's talk about the UN and abstaining.... (<a href="https://youtube.com/watch?v=gxLt691aroo">watch</a> || <a href="/videos/2023/12/23/Lets_talk_about_the_UN_and_abstaining">transcript &amp; editable summary</a>)

Beau breaks down a recent UN vote, noting the emphasis on humanitarian aid in Gaza and the absence of a direct ceasefire call, urging universal support for quick aid delivery.

</summary>

"Everybody needs to hope that aid starts getting in there and getting in there quickly."
"What people were hoping for did not occur."
"It is a hopeful sign on the humanitarian front."
"Those people, if you were waiting for the UN to take action, what you were waiting for, it didn't happen."
"By the time all the negotiations were done, it really turned into the Security Council calling for more aid to get in quickly."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of a recent United Nations vote, particularly in the Security Council versus the General Assembly.
Describes the content and implications of the resolution passed at the UN, focusing on humanitarian aid and access in Gaza.
Notes the absence of a direct call for a ceasefire in the resolution.
Addresses the reasons behind countries like the US and Russia abstaining from the vote, citing their desire for specific condemning languages that were not included.
Emphasizes the importance of quick and increased aid delivery, despite the lack of certain elements in the resolution.
Urges universal support for the aid efforts and expresses uncertainty about an imminent ceasefire call.
Concludes by stressing the critical nature of aid delivery and the ongoing negotiations at the UN.

Actions:

for united nations observers,
Support and advocate for quick and increased humanitarian aid delivery to Gaza (implied).
</details>
<details>
<summary>
2023-12-23: Let's talk about non-profit admin expenses.... (<a href="https://youtube.com/watch?v=3ddA7XuaKNE">watch</a> || <a href="/videos/2023/12/23/Lets_talk_about_non-profit_admin_expenses">transcript &amp; editable summary</a>)

Beau explains the nuances of evaluating administrative salaries in non-profits and charities, stressing the importance of impact over numbers and resources in addressing societal issues.

</summary>

"Fights like that, they take resources."
"High administrative salaries may seem concerning but could be justified if they contribute to the organization's goals."
"Look at it in the context of the overall amount of funding."
"You're going to see what they do firsthand."
"It is a fight and fights like that they take resources."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of looking beyond administrative salaries when supporting non-profits and charities.
Shares a personal story of a marketing fundraising professional who significantly increased donations for a struggling organization.
Emphasizes the value of hiring skilled individuals, even if it means higher administrative salaries.
Compares the skills of those running large logistics for nonprofits to Fortune 500 company executives.
Suggests evaluating administrative salaries as a percentage of revenue and program expenses to determine effectiveness.
Advises talking to individuals who have used a charity's services to gauge its impact.
Acknowledges that high administrative salaries may seem concerning but could be justified if they contribute to the organization's goals.
Acknowledges the necessity of resources in addressing societal issues and supporting non-profits.

Actions:

for supporters of non-profits,
Talk to individuals who have used a charity's services to understand its impact (suggested).
Analyze administrative salaries as a percentage of revenue and program expenses to gauge effectiveness (implied).
</details>
<details>
<summary>
2023-12-23: Let's talk about Trump, SCOTUS, and Smith.... (<a href="https://youtube.com/watch?v=zEIrl4i2mXA">watch</a> || <a href="/videos/2023/12/23/Lets_talk_about_Trump_SCOTUS_and_Smith">transcript &amp; editable summary</a>)

Beau analyzes Trump's legal situation, cautioning against assumptions about the Supreme Court's stance.

</summary>

"I don't know that you can interpret that as, well, they're going to let him go."
"I can't say that the Supreme Court isn't going to hear the case or is going to decide against him."
"I can't look at recent events and say that that's the conclusion I would draw."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of Trump's legal situation, Beau mentions his claim of presidential immunity in a DC case.
Trump's claim was rejected by Judge Chuckin, leading him to appeal to the appeals court.
Smith attempted to expedite the case to the Supreme Court, but they declined to hear it out of order.
The Supreme Court's decision not to hear the case early is not necessarily indicative of favoritism towards Trump.
Beau cautions against jumping to conclusions about the Supreme Court's stance on Trump's cases.

Actions:

for legal analysts, concerned citizens,
Stay informed on legal developments and rulings (implied)
Avoid jumping to conclusions based on limited information (implied)
</details>
<details>
<summary>
2023-12-22: Let's talk about a question on foreign policy negotiations.... (<a href="https://youtube.com/watch?v=DonvhDRtNxI">watch</a> || <a href="/videos/2023/12/22/Lets_talk_about_a_question_on_foreign_policy_negotiations">transcript &amp; editable summary</a>)

Beau explains the nuances of negotiations in the Israeli-Palestinian conflict, cautioning against assuming public statements accurately represent private negotiations and stressing the futility of a military solution.

</summary>

"There is no military solution here."
"War is continuation of politics by other means."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of understanding the assumptions behind questions regarding negotiations in the Palestinian-Israeli conflict.
Points out the discrepancy between public statements and actual negotiating positions in international conflicts.
Raises questions about the motivations behind the Palestinian side's stance on negotiations and ceasefire.
Suggests that public statements may not accurately represent what is happening at the negotiating table.
Emphasizes the strategic approach of the Palestinian side to garner international support and shift public opinion through taking hits.
Argues that there is no military solution to the conflict and that victory conditions need to change for progress.
Notes the distinction between Hamas and other Palestinian groups in terms of their negotiating stances.
Calls for more information and a deeper understanding of the dynamics at play in the conflict.
Warns against viewing military victories as equivalent to political victories in the conflict.
Concludes by reiterating that there is no military solution and encourages viewers to think about the situation.

Actions:

for activists, peace advocates,
Seek out diverse sources of information to gain a more comprehensive understanding of conflicts (implied).
</details>
<details>
<summary>
2023-12-22: Let's talk about Wisconsin and impeachment being super unlikely.... (<a href="https://youtube.com/watch?v=Zm3PPJqQbLA">watch</a> || <a href="/videos/2023/12/22/Lets_talk_about_Wisconsin_and_impeachment_being_super_unlikely">transcript &amp; editable summary</a>)

Republicans back down from threatening impeachment against Justice Protozeewicz over redistricting concerns in Wisconsin.

</summary>

"Impeachment was, quote, super unlikely."
"If Republicans hold that position and then try to stop her from correcting it, they're creating a situation where you have even more upset people about it."

### AI summary (High error rate! Edit errors on video page)

Justice Protozeewicz identified Wisconsin as gerrymandered while seeking office on the Wisconsin Supreme Court.
Republicans were nervous about her involvement in a redistricting case and wanted her to step down, but she refused.
Republicans threatened impeachment over her refusal to step down.
The Republican Party, specifically the House Assembly, heavily pursued the idea of impeaching her for a while.
The leader of the GOP in Wisconsin stated that impeachment is now "super unlikely."
It seems that the Republicans understand the potential backlash of impeaching Justice Protozeewicz.
Justice Protozeewicz is currently not in danger of being impeached, and the long-running story may be coming to a close.

Actions:

for wisconsin voters,
Contact local representatives to express support for fair redistricting (implied)
</details>
<details>
<summary>
2023-12-22: Let's talk about Trump, recordings, and Michigan.... (<a href="https://youtube.com/watch?v=3IDdYAfU2m8">watch</a> || <a href="/videos/2023/12/22/Lets_talk_about_Trump_recordings_and_Michigan">transcript &amp; editable summary</a>)

News broke about a recording of Trump pressuring Michigan officials not to certify election results, potentially affecting 880,000 voters, adding to legal evidence.

</summary>

"They will look terrible if they signed."
"We'll take care of that."

### AI summary (High error rate! Edit errors on video page)

News broke about a reported recording of a phone call on November 17th, 2020 involving Trump, McDaniel, and Michigan officials.
Trump and McDaniel were allegedly pressuring officials in Michigan not to certify the election results.
Key phrases from the call include Trump saying officials "They would look terrible if they signed" and McDaniel urging not to sign, saying they will provide attorneys.
The call aimed to prevent the certification of Wayne County votes, potentially affecting around 880,000 people.
This was another attempt by Trump to alter the election outcome in Michigan, where Biden won by 154,000 votes.
The recording may play a significant role in legal cases and could come up in federal and Georgia cases.
While Beau hasn't heard the recording personally, its contents are widely reported.
This recording adds to the evidence that may strengthen legal cases against Trump's interference in the election.

Actions:

for legal activists, concerned citizens,
Contact legal advocacy organizations to stay informed and support efforts against election interference (suggested)
</details>
<details>
<summary>
2023-12-22: Let's talk about Rudy's next chapter.... (<a href="https://youtube.com/watch?v=8Hrhd8qWBKs">watch</a> || <a href="/videos/2023/12/22/Lets_talk_about_Rudy_s_next_chapter">transcript &amp; editable summary</a>)

Rudy Giuliani files for bankruptcy as a delay tactic amid financial troubles and ongoing legal battles, requiring caution in disclosures.

</summary>

"Rudy Giuliani has filed for Chapter 11. He's filed for bankruptcy."
"This is one of those moments where Rudy's team has to be incredibly careful."
"The financial situation was not great, but I feel like this occurred at this moment to buy time."

### AI summary (High error rate! Edit errors on video page)

Rudy Giuliani has filed for Chapter 11 bankruptcy, buying time for a potential appeal.
Giuliani lists his liabilities between $100 million and $500 million, with assets of $10 million.
Plaintiffs in the Georgia case are actively seeking to put liens on Giuliani's properties and discover his income sources.
Giuliani's team must be cautious in disclosing information during the bankruptcy proceedings to avoid negative consequences.
The current situation seems like a delay tactic amid existing financial issues.
The Rudy Giuliani saga continues, indicating that it's not over yet.

Actions:

for financial advisors,
Analyze the financial implications of filing for bankruptcy (implied)
Seek legal advice on disclosing financial information during bankruptcy proceedings (implied)
</details>
<details>
<summary>
2023-12-21: The Roads to Fundraising Q&A edition.... (<a href="https://youtube.com/watch?v=CGuTpd44F78">watch</a> || <a href="/videos/2023/12/21/The_Roads_to_Fundraising_Q_A_edition">transcript &amp; editable summary</a>)

Beau shares insights on fundraising for domestic violence shelters, the importance of communication with shelters, and tips for effective activism locally.

</summary>

"Tablets for teens provide a sense of control and ownership in a chaotic situation."
"Call shelters to determine current needs as they vary each year."
"Importance of acknowledging wins and taking breaks when overwhelmed."

### AI summary (High error rate! Edit errors on video page)

Annual fundraiser for domestic violence shelters providing gifts for teens
Tablets, cases, earbuds, Google Play, Hulu cards given to teens in shelters
Excess funds used for other shelter needs like holiday meals
Tablets for teens provide a sense of control and ownership in a chaotic situation
Importance of communication to understand shelter needs for effective donations
Need to call shelters to determine current needs as they vary each year
Example of past needs like cleaning supplies and bras at shelters
Main channel fundraiser may set up an endowment for long-term impact
Ways for individuals with limited resources to contribute to activism locally
Importance of acknowledging wins and taking breaks when overwhelmed
Tips on decentralizing activist groups and avoiding centralization pitfalls
Beau's interest in Jeep Wranglers and desire to try the environmentally friendly 4XE

Actions:

for community members,
Contact local shelters to inquire about their current needs (suggested)
Volunteer to run errands or help with maintenance at organizations in need (suggested)
Support causes by offering specific skills or assistance as per their requirements (exemplified)
</details>
<details>
<summary>
2023-12-21: Let's talk about two Rudy developments.... (<a href="https://youtube.com/watch?v=-K5_Om5TWs0">watch</a> || <a href="/videos/2023/12/21/Lets_talk_about_two_Rudy_developments">transcript &amp; editable summary</a>)

Giuliani faces immediate payment due to prior failures, plaintiffs likely to succeed in legal actions.

</summary>

"Giuliani facing enforcement of a $146 million judgment, seeking to dissipate assets."
"Plaintiffs likely to succeed in both enforcement and lawsuit against Giuliani."

### AI summary (High error rate! Edit errors on video page)

Giuliani facing enforcement of a $146 million judgment, seeking to dissipate assets.
Judge rules Giuliani must pay immediately due to failure to satisfy earlier awards.
Giuliani's claim of not having money contradicted by judge referencing his sizable staff.
Plaintiffs suing Giuliani again, seeking to permanently bar defamatory statements.
Statements suggesting evidence to back up defamatory claims despite losing the case.
Plaintiffs likely to succeed in both enforcement and lawsuit against Giuliani.
Giuliani expected to have to pay up soon.

Actions:

for legal observers, political analysts.,
Support organizations fighting defamation (suggested).
Stay informed on legal proceedings (suggested).
</details>
<details>
<summary>
2023-12-21: Let's talk about that Catholic changes regarding blessings.... (<a href="https://youtube.com/watch?v=Sm1S3bUKCNc">watch</a> || <a href="/videos/2023/12/21/Lets_talk_about_that_Catholic_changes_regarding_blessings">transcript &amp; editable summary</a>)

Beau explains the significant shift in the Catholic Church's stance towards blessing same-sex couples, marking a major step towards LGBTQ inclusion.

</summary>

"That's what it really boils down to."
"This is huge. This is a major development because the Church is a very traditional organization and change takes time."
"It's another sign that on a long enough timeline, we win."

### AI summary (High error rate! Edit errors on video page)

Explains the recent news about priests blessing same-sex couples but not allowing gay marriage, showing a significant shift in the Catholic Church's stance.
Emphasizes that this change is a big deal because it alters the way same-sex couples are viewed within the church, moving from exclusion to acceptance.
Points out that although the decision may not allow for same-sex marriage, it signifies a major step towards LGBTQ inclusion within the Catholic Church.
Describes how this change is part of a broader effort by the Pope to make the Church more welcoming to LGBTQ individuals.
Mentions historical examples of slow changes in Church practices, illustrating the significance of the recent development.

Actions:

for catholics, lgbtq community,
Support LGBTQ Catholics in their journey of faith and acceptance (implied)
</details>
<details>
<summary>
2023-12-21: Let's talk about drill instructors and fact checking.... (<a href="https://youtube.com/watch?v=56Y8ZAQNRHU">watch</a> || <a href="/videos/2023/12/21/Lets_talk_about_drill_instructors_and_fact_checking">transcript &amp; editable summary</a>)

Beau explains the reasons behind drill instructors yelling and the importance of repeating information to combat misinformation effectively.

</summary>

"If you suffer enough shark attacks, which is where a whole bunch of instructors come up and yell at you all at once and normally yell conflicting things, and anyway, you can become very unbothered by a lot of things that might happen later if you're in the military."
"It turns a teachable moment for one person into a teachable moment for everybody."
"Battling that kind of bad information is something that I think is really important."
"It's a teaching technique, and it's one that works."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the reasons behind drill instructors yelling, including to establish authority, create urgency, and provide stress inoculation.
Reveals a lesser-known reason for yelling, which is to turn a teachable moment for one person into a lesson for everyone.
Shares the importance of repeating information in videos to combat misinformation and reach a wider audience.
Emphasizes the effectiveness of multiple exposures to debunk false beliefs and encourage critical thinking.
Advocates for using teaching techniques to help people learn how to fact-check and be less susceptible to misinformation.

Actions:

for educators, critical thinkers,
Share educational videos multiple times to combat misinformation effectively (suggested).
Encourage critical thinking through repeated exposure to debunk false beliefs (suggested).
Use teaching techniques to help others learn how to fact-check and be less susceptible to misinformation (suggested).
</details>
<details>
<summary>
2023-12-21: Let's talk about Lindsey Graham taking heat.... (<a href="https://youtube.com/watch?v=ZO-MeSItqKA">watch</a> || <a href="/videos/2023/12/21/Lets_talk_about_Lindsey_Graham_taking_heat">transcript &amp; editable summary</a>)

The Republican Party faces a growing division over the lack of evidence in the Biden impeachment inquiry, risking credibility and future elections.

</summary>

"That whole chain of events, that's strange, right?"
"They've heard it since 2020, and that evidence never came."
"It's worth remembering that Lindsey Graham's been up there a long time."
"That division is something that is going to matter in the elections."
"They have similar goals, but they're looking at it through very, very different eyes now."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is experiencing another division between old school GOP and a newer faction.
Lindsey Graham and other senators publicly expressed doubt on the evidence for the Biden impeachment inquiry.
House Republicans heavily engaged on social media had promised their base an impeachment, but senators dismissing the evidence is undermining this narrative.
The faction of the Republican Party upset with senators not convinced by the evidence is illustrated by a scenario involving the "space laser lady."
Beau questions why those claiming to have evidence don't present it when given a platform.
The MAGA faction's credibility is at stake as they have repeatedly promised evidence that never materialized.
Lindsey Graham, despite his tenure, refuses to play along with this narrative, recognizing the potential backlash.
The divide within the Republican Party is becoming more pronounced and will impact future elections.
Beau points out the necessity of differentiating between the factions within the Republican Party.

Actions:

for political analysts, republican voters,
Differentiate between the factions within the Republican Party (implied)
</details>
<details>
<summary>
2023-12-20: Let's talk about updates on negotiations and plans.... (<a href="https://youtube.com/watch?v=gRWNb7girDI">watch</a> || <a href="/videos/2023/12/20/Lets_talk_about_updates_on_negotiations_and_plans">transcript &amp; editable summary</a>)

Updates on foreign policy negotiations include ongoing talks for a ceasefire, pressure on Israel, and steps towards peace with the Palestinian Authority.

</summary>

"Israel is once again open to a pause."
"Calls for reforms within the Palestinian Authority leadership."
"Diplomatic stuff like that's holding it up."

### AI summary (High error rate! Edit errors on video page)

Updates on foreign policy negotiations are underway, with willingness to come back to the table and US plans for afterward.
Received significant hate mail after discussing negotiations, but talks are ongoing for a pause to retrieve captives.
UN negotiations for a ceasefire are progressing, aiming to prevent a US veto or abstention.
Pressure from various sources is pushing Israel towards a pause, ceasefire, and lasting peace.
Israel publicly declaring readiness to resume talks eases some pressure at the UN but not entirely.
Steps towards a pause, ceasefire, and durable peace are being taken by Israel and the US in relation to the Palestinian Authority.
Calls for reforms within the Palestinian Authority leadership and potential changes in power structure are being considered.
Lack of news on an international force to stand between the sides may indicate ongoing negotiations with different countries.
The US may allow a UN vote on ceasefire without veto, pending any changes in wording.
Diplomatic nuances in wording, like changing "end to fighting" to "suspension in hostilities," are factors affecting the negotiations.

Actions:

for diplomatic observers,
Contact organizations working towards peace in the region (implied)
Stay informed about updates from the UN negotiations (implied)
</details>
<details>
<summary>
2023-12-20: Let's talk about Trump being off the ballot in Colorado.... (<a href="https://youtube.com/watch?v=BWUJyR83TsE">watch</a> || <a href="/videos/2023/12/20/Lets_talk_about_Trump_being_off_the_ballot_in_Colorado">transcript &amp; editable summary</a>)

Colorado Supreme Court disqualifies Trump from office under the US Constitution, setting the stage for a potential US Supreme Court decision with wide-reaching implications.

</summary>

"Colorado Supreme Court ruled Trump is disqualified from office under the US Constitution."
"US Supreme Court's decision will impact many places."
"The situation has been unexpected and surprising."

### AI summary (High error rate! Edit errors on video page)

Colorado Supreme Court ruled that President Trump is disqualified from holding office under the 14th Amendment of the US Constitution.
The ruling states listing Trump as a candidate on the presidential primary ballot in Colorado is a wrongful act.
A Colorado judge previously ruled that Trump engaged in insurrection.
The current ruling will be held on appeal until January 4th.
If the ruling stands, Trump will not be on the ballot for the presidential election.
Similar proceedings are happening in other states, with some rulings being struck down.
The argument is that the ban on holding office due to insurrection is automatic and doesn't require a guilty finding.
The US Supreme Court's decision on Colorado's ruling will have a wide impact.
Beau expresses doubt that the Supreme Court will side with Colorado's ruling.
The situation has been unexpected and surprising.

Actions:

for political analysts, legal experts,
Monitor updates on the appeal deadline for the ruling (suggested)
Stay informed about similar proceedings in other states (suggested)
Await the US Supreme Court's decision and its implications (suggested)
</details>
<details>
<summary>
2023-12-20: Let's talk about Trump and Godwin.... (<a href="https://youtube.com/watch?v=u1S0REyJing">watch</a> || <a href="/videos/2023/12/20/Lets_talk_about_Trump_and_Godwin">transcript &amp; editable summary</a>)

Biden administration compares Trump's rhetoric to Hitler, with Godwin affirming intentional parallels, stressing the importance of not equating them.

</summary>

"I think it [comparing Trump's rhetoric to Hitler] would be fair to say that Trump knows what he's doing."
"One of them is your basic status quo centrist American president, which is exactly what everybody said he was going to be. The other is a far-right extreme authoritarian."
"That rhetoric, it's not an accident. It is not an accident."

### AI summary (High error rate! Edit errors on video page)

Biden administration compared Trump's rhetoric to Hitler, sparking debate.
Reference to Godwin's Law: likelihood of Nazi comparisons in online debates.
Godwin, creator of the law, was reached out to for his opinion.
Godwin believes Trump intentionally uses inflammatory rhetoric with similarities to Hitler.
Godwin implies Trump's rhetoric is not accidental, citing specific remarks.
Emphasizes the importance of not equating Biden and Trump as the same.
Trump may continue using such rhetoric to garner attention.
Stresses the non-coincidental nature of Trump's rhetoric.

Actions:

for online political observers,
Read the full interview with Godwin for deeper insight (suggested)
Stay informed about political rhetoric and its implications (implied)
</details>
<details>
<summary>
2023-12-20: Let's talk about Scott Perry's phone and Jack Smith.... (<a href="https://youtube.com/watch?v=RYjcqKxvD4k">watch</a> || <a href="/videos/2023/12/20/Lets_talk_about_Scott_Perry_s_phone_and_Jack_Smith">transcript &amp; editable summary</a>)

Representative Scott Perry's phone seized in January 6 probe, court orders disclosure of majority of documents, potentially revealing significant information.

</summary>

"Representative Scott Perry's phone was seized by the federal government in connection with the January 6th probe."
"The court found that most of the information was not protected by speech debate, but some were."

### AI summary (High error rate! Edit errors on video page)

Representative Scott Perry's phone was seized by the federal government in connection with the January 6th probe in 2022.
Perry claimed protection under the speech and debate clause in the Constitution to prevent access to his text messages and emails.
A legal battle ensued, with a judge ruling against Perry's claim of protection.
An appeals court has ordered Perry to disclose 1,659 out of 2,055 documents, withholding access to 396.
The court found that most of the information was not protected by speech debate, but some were.
The documents are believed to contain information on the vice president's role during certain events in Congress and election claims.
Perry's attorney mentioned his undecided stance on appealing the decision.
Investigators are now poised to access a significant amount of text messages and emails for the investigation.
Despite some documents being protected, the majority will be available for scrutiny.
The outcome may provide valuable context to the investigation.

Actions:

for legal observers, political analysts,
Stay informed on the updates regarding the investigation (implied).
</details>
<details>
<summary>
2023-12-19: Let's talk about Mark Meadows and removal.... (<a href="https://youtube.com/watch?v=I1hcr35KJh4">watch</a> || <a href="/videos/2023/12/19/Lets_talk_about_Mark_Meadows_and_removal">transcript &amp; editable summary</a>)

Mark Meadows' legal battle in Georgia impacts Trump's claims and the decision on moving the case to federal court, raising questions about immunity.

</summary>

"The determination over whether or not he [Meadows] count as a federal official even though he was no longer part of the government, that was kind of up in the air."
"If Meadows continues to fight at the next op Supreme Court, I'm not sure how likely that is."
"The fact that they shot Meadows down, it leads to the idea that Trump's claims will also be rejected."
"Even if it goes to federal court and he gets it removed, the immunity that he thinks is there, these judges certainly don't think it's there."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Mark Meadows, involved in a case in Georgia with Trump, tried to move his case to federal court but was ruled against and appealed.
The appeals court decided that Meadows' case must stay in Georgia because the events leading to the action were not related to his official duties.
The judges determined that Meadows, as a former federal officer, did not have the authority to conspire to overturn election results.
If Meadows decides to continue fighting and take it to the Supreme Court, the outcome remains uncertain.
Meadows' case staying in Georgia may indicate potential rejection of Trump's claims as well.
Meadows faces a decision on whether to pursue moving his case to federal court, considering the potential lack of immunity there.

Actions:

for legal analysts, activists.,
Stay updated on legal proceedings and analyses (implied).
</details>
<details>
<summary>
2023-12-19: Let's talk about Georgia, lawyers, and speculation.... (<a href="https://youtube.com/watch?v=YTRgyK92IF8">watch</a> || <a href="/videos/2023/12/19/Lets_talk_about_Georgia_lawyers_and_speculation">transcript &amp; editable summary</a>)

Attorneys for a co-defendant of Trump filed to withdraw in Georgia, sparking speculation, but the actual reasons remain unknown amidst social media chatter.

</summary>

"The short version is that her attorneys have filed to withdrawal, meaning they no longer can represent her."
"Just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

There was a development in Georgia, Fulton County, involving a co-defendant of Trump.
The co-defendant's attorneys have filed to withdraw, indicating they can no longer represent her.
Speculation abounds regarding the reasons for the withdrawal, ranging from non-payment to the possibility of another indictment.
The attorney mentioned the importance of a good lawyer-client relationship involving listening, being on board, and being paid.
The attorney's statement was generic and did not specifically address the reasons for the withdrawal.
The only known fact is the filing of the motion to withdraw; everything else is speculation.
While the speculated reasons might have some truth, nothing is confirmed yet.
Beau admits to having a hunch about the situation, but it's merely a guess based on gut feeling.
There will be a future discussion on the appropriateness of the withdrawal and the potential change of attorneys for the co-defendant.
Despite social media chatter, the actual reason for the withdrawal remains unknown, and speculation continues.

Actions:

for legal analysts,
Stay informed on official updates and news related to the case (implied).
</details>
<details>
<summary>
2023-12-19: Let's talk about Arlington, monuments, and messages.... (<a href="https://youtube.com/watch?v=E2pJ_EZXVbI">watch</a> || <a href="/videos/2023/12/19/Lets_talk_about_Arlington_monuments_and_messages">transcript &amp; editable summary</a>)

Beau breaks down misconceptions around Confederate monuments at Arlington Cemetery, pointing out the historical context and Congress's role in potential removal.

</summary>

"You won't find that name. Click on where it says monuments and look for something called the Reconciliation Monument. You won't find that there."
"The one at Arlington, it really did have a different purpose. It was built in the early 1900s and it really did have a tone of reconciliation."
"Congress actually made this happen, to be clear."
"That is impossible, and more importantly, I think you know it's impossible."
"I don't think that there's ever going to be a point in time where people do not understand the connection between Arlington National cemetery, the Civil War, or the Confederacy."

### AI summary (High error rate! Edit errors on video page)

Explains the questions around the Reconciliation Monument at Arlington and the history behind Confederate monuments.
Points out that the monument isn't actually called the Reconciliation Monument and that it's referred to as the Confederate Memorial on the Arlington Cemetery website.
Talks about the historical context of Confederate monuments being built during periods of civil rights activity with messages of racial oppression.
Notes that the Confederate Memorial at Arlington was built in the early 1900s with a tone of reconciliation, different from many other Confederate monuments.
Mentions Congress's involvement in the decision to potentially remove the Confederate Memorial through the NDAA of 2021.
Addresses the impossibility of completely removing the connection to the Confederacy from Arlington Cemetery due to Confederate soldiers buried there.
Provides historical context on how Arlington was initially taken during the Civil War for strategic military reasons, not due to Union losses.
Mentions Mary Custis, who owned Arlington before it was taken by the US Army, and her connection to Robert E. Lee.
Comments on the current situation where the Department of Defense is trying to remove the Confederate monument, authorized by Congress, with some members now expressing outrage.
Talks about a temporary restraining order in place while the issue is being sorted out and criticizes Congress for potentially not addressing the situation they initiated.

Actions:

for history enthusiasts, activists,
Contact local historical societies to learn more about the history of Arlington Cemetery (suggested)
Support community efforts to contextualize historical monuments accurately (exemplified)
</details>
<details>
<summary>
2023-12-19: Let's talk about AI, the funny, and the unfunny.... (<a href="https://youtube.com/watch?v=vRTa1FjMQIU">watch</a> || <a href="/videos/2023/12/19/Lets_talk_about_AI_the_funny_and_the_unfunny">transcript &amp; editable summary</a>)

Elon Musk's attempt at a "not woke" AI turns out unexpectedly "woke," revealing AI's unpredictability and implications for bias in technology development.

</summary>

"It's one of those Jurassic Park moments where, you know, so concerned about whether or not we could, we didn't stop to think if we should."
"It kind of indicates that AI as we are talking about it here, it's not really under the control of those people creating it."
"You had a bunch of people on the right wing asking it questions like you know are trans women male and it's like no of course not."

### AI summary (High error rate! Edit errors on video page)

Elon Musk wanted his own AI, a large chatbot not "woke" as per user expectations.
The AI turned out to be "woke," providing unexpected answers to users' questions.
The situation is funny because the product didn't meet user expectations and suggested a left bias in reality.
The goal was to create a politically incorrect AI with less bias towards liberal ideas, which it didn't achieve.
The unexpected outcomes show that AI may not be under the control of its creators.
It's a humorous moment when right-wing users were upset by the AI's responses.
The wider implications of AI's unpredictability should give people pause.
Comparing AI development to a "Jurassic Park" moment where consequences are overlooked.
The situation is humorous but also raises concerns about AI technology.

Actions:

for tech enthusiasts, ai developers,
Question AI biases (implied)
Stay informed on AI development (implied)
Engage in ethical considerations in technology (implied)
</details>
<details>
<summary>
2023-12-18: Let's talk about what low intensity means and a question.... (<a href="https://youtube.com/watch?v=ebFcAiNFfu4">watch</a> || <a href="/videos/2023/12/18/Lets_talk_about_what_low_intensity_means_and_a_question">transcript &amp; editable summary</a>)

Beau explains "low intensity" conflict and why the US won't use its military power to stop certain conflicts, advocating for a shift away from interventionism.

</summary>

"Low intensity does not mean like relative peace. It means a different kind of operation."
"The US has the military power to stop the fighting, yes. Deploying that in the way it would have to be done, never gonna happen."
"The U.S. wins the war, destroys the opposition's military, and then loses the peace."
"The question is yes, the power exists but it won't be used."
"The US needs to be moving away from being the world's policeman."

### AI summary (High error rate! Edit errors on video page)

Explaining the concept of "low intensity" conflict compared to major wars like World War II or conflicts like Ukraine.
Clarifying that "low intensity" does not mean relative peace but rather a different kind of military operation.
Acknowledging the terminology concern and openness to feedback about outdated terms.
Addressing a hypothetical question about whether the US could stop a conflict by force, pointing out the limitations and consequences.
Emphasizing that while the US has the military power to stop conflicts, deploying that power often leads to more harm than good.
Advocating for the US to move away from being the world's policeman and cautioning against intervention in conflicts where neither party is an ally.
Noting the inadequacy of US peace enforcement strategies, which often result in taking out leadership but failing to maintain peace afterwards.

Actions:

for policy makers, activists,
Advocate for a shift in US foreign policy towards non-interventionism (implied).
</details>
<details>
<summary>
2023-12-18: Let's talk about the Biden impeachment inquiry.... (<a href="https://youtube.com/watch?v=OeaGCD0J8nE">watch</a> || <a href="/videos/2023/12/18/Lets_talk_about_the_Biden_impeachment_inquiry">transcript &amp; editable summary</a>)

Beau analyzes the Biden impeachment inquiry, suggesting it lacks evidence and might be a political stunt, expressing doubts about its progression beyond the House or Senate.

</summary>

"If there was a smoking gun, I think we'd be talking about it."
"For an impeachment to move forward, not just they need all Republicans in the Senate, they need a bunch of Democrats."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of the impeachment inquiry into President Biden, authorized by the U.S. House of Representatives in a party-line vote.
Speculating on the possible political ramifications and outcomes of the inquiry, including the potential for impeachment.
Noting the lack of substantial evidence presented for impeachment and characterizing the inquiry as a political stunt or fishing expedition.
Mentioning the skepticism and lack of support within the Republican Party for impeachment proceedings.
Expressing doubt about the likelihood of the impeachment process progressing beyond the House or Senate due to insufficient evidence.
Concluding that it is improbable for President Biden to be impeached and removed based on the current circumstances and evidence presented.

Actions:

for politically engaged individuals,
Monitor political developments surrounding the Biden impeachment inquiry (implied).
</details>
<details>
<summary>
2023-12-18: Let's talk about a letter for you all.... (<a href="https://youtube.com/watch?v=AZO78DJzg_M">watch</a> || <a href="/videos/2023/12/18/Lets_talk_about_a_letter_for_you_all">transcript &amp; editable summary</a>)

Beau shares a powerful story of survival and transformation after domestic violence, underscoring the impact of community support in saving lives.

</summary>

"Because of you, I am alive to celebrate this season of hope and peace with a renewed spirit filled with gratitude."
"A year ago, your support saved my life."
"That's addressed to me, but the message is for y'all."

### AI summary (High error rate! Edit errors on video page)

Receives letters from shelters they help, but usually doesn't read them as they are fundraising appeals.
Prioritizes talking to people who have used the services of organizations they support over financials.
Shares a letter that tells a story of someone who used shelter services after a domestic violence incident.
Describes the brutal attack by the husband and the neighbor calling 911.
Narrates how the survivor was taken to the hospital, provided with a hotline number, and admitted to an emergency shelter.
Shares the advocate's compassionate support and setting up a safe space with blankets and pillows.
Talks about the survivor's emotional state, pain, and the advocate assisting in identifying next steps.
Mentions workshops on power and control in relationships, setting healthy boundaries, and coping post-abuse.
Notes the survivor's growth in confidence, resilience, and awareness through the support received.
Describes working with an attorney to secure a restraining order against the abuser and setting goals with advocates.
Expresses gratitude for the support that saved the survivor's life and mentions the importance of community involvement.
Encourages viewers to support organizations doing good work and mentions the availability of shelters in various locations.

Actions:

for supporters of survivors,
Support organizations doing impactful work (suggested)
Get involved with shelters in your area (suggested)
</details>
<details>
<summary>
2023-12-18: Let's talk about Justice Thomas, pay, and what's next.... (<a href="https://youtube.com/watch?v=mV0KzGZEzj8">watch</a> || <a href="/videos/2023/12/18/Lets_talk_about_Justice_Thomas_pay_and_what_s_next">transcript &amp; editable summary</a>)

Beau explains a report from ProPublica on Justice Thomas facing financial issues and a timeline of events that could have political implications.

</summary>

"This is going to be big."
"Politically, it is going to be used whether or not evidence ever surfaces."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Justice Thomas, the Supreme Court, pay, finances, timelines, and ProPublica.
Mentions ProPublica as a reputable outlet known for breaking major stories.
Talks about a report from ProPublica regarding Justice Thomas facing financial issues in January 2000.
Justice Thomas discussed financial problems with a GOP lawmaker on a flight back from a conservative conference.
Lawmaker attempted to get justices a raise but it didn't happen.
Panic in the GOP was triggered by Thomas mentioning the possibility of resigning over pay, which could lead to Clinton nominating replacements.
Justice Thomas started receiving gifts shortly after, connecting to previous reporting by ProPublica.
ProPublica's reporting doesn't imply anything nefarious but raises questions about the timeline of events.
Urges the audience to read the reporting to understand the situation better.
Emphasizes that the issue will come up and be politically significant, regardless of evidence surfacing.

Actions:

for advocates and activists.,
Read the report from ProPublica to understand the situation better (suggested).
Stay informed and prepared for this issue to be a significant talking point (implied).
</details>
<details>
<summary>
2023-12-17: The Roads Not Taken EP18 (<a href="https://youtube.com/watch?v=ygG-gSX82mE">watch</a> || <a href="/videos/2023/12/17/The_Roads_Not_Taken_EP18">transcript &amp; editable summary</a>)

Beau provides insights on foreign policy, domestic news, political developments, environmental updates, and viewer questions, offering a comprehensive overview of current events.

</summary>

"Major shipping companies are suspending transit through the Red Sea out of fear of attacks."
"A vote for a third party is not necessarily a vote for Trump. It's a vote for a third party."
"If it was my daughter, I would not risk jeopardizing the relationship over that."

### AI summary (High error rate! Edit errors on video page)

Provides a weekly overview of unreported or under-reported news events.
Reports on foreign policy updates, such as Europe adjusting its security posture in response to Russia.
Mentions Zelensky leaving the U.S. without more aid, progress in the Senate on aid packages, and the House going on vacation.
Notes U.S. pressure on Israel to move away from major combat operations.
Talks about major shipping companies suspending transit through the Red Sea due to fear of attacks impacting international trade.
Updates on domestic news, including a Boston event celebrating historical events like the Boston Tea Party and Hawaii's threat to stop short-term rentals on Maui.
References Mark Meadows' legal case and potential impacts from a recent Morocco kingdom heir case.
Mentions political developments like Sununu's endorsement of Nikki Haley, potential third-party runs by Liz Cheney, and predictions on Republican losses in the House.
Reports on Voyager 1's communication issues and Alex Jones' return to Twitter with a settlement offer to impacted families.
Shares environmental news, including Wyoming Governor Mark Gordon acknowledging climate change and federal court decisions on wolf reintroduction in Colorado and Texas power plant responsibilities.
Mentions a significant solar flare impacting radio communications in the U.S.
Responds to viewer questions regarding voting dynamics, third-party considerations, and ongoing misconduct issues within the Coast Guard.

Actions:

for viewers interested in staying informed and understanding nuanced perspectives on diverse topics.,
Contact local representatives to advocate for responsible environmental policies (implied).
Stay informed about ongoing political developments and potential third-party runs (suggested).
Educate oneself on international conflicts and foreign policy implications (implied).
</details>
<details>
<summary>
2023-12-17: Let's talk about what it would take to make the US  plan work.... (<a href="https://youtube.com/watch?v=4HT_kXzKlfY">watch</a> || <a href="/videos/2023/12/17/Lets_talk_about_what_it_would_take_to_make_the_US_plan_work">transcript &amp; editable summary</a>)

US diplomatic efforts in the Israeli-Palestinian conflict face significant challenges, including finding broad support for governance in Gaza and forming an international peacekeeping coalition.

</summary>

"Attempting that is certainly better than the status quo."
"It takes a lot of things going, everything going right for it to happen."
"This isn't a simple situation."
"That's the reality of it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analysis on US diplomatic efforts towards the Israeli offensive against Hamas and the Palestinian Authority's role in Gaza.
Speculation on the effectiveness of the Israeli offensive and the Palestinian Authority's governance capability.
Need for a dynamic figure from Gaza with broad support to lead administration in Gaza.
Challenges in finding international partners for security arrangements between Israelis and Palestinians.
Skepticism on the willingness of the US population to support a peacekeeping coalition.
Importance of having forces that prioritize the interests of Palestinians and are acceptable to Israelis.
Doubts on the feasibility of the proposed plan without all pieces in place.
Skepticism towards Israel degrading combat effectiveness and restructuring of the Palestinian Authority.
Difficulty in forming an international coalition due to the lack of a successful track record in the region.
Long shot nature of brokering lasting peace and the need for commitment from multiple countries over decades.

Actions:

for diplomatic stakeholders,
Form a coalition of countries committed to long-term peacekeeping efforts (implied).
Advocate for diplomatic solutions to the Israeli-Palestinian conflict within your community (implied).
</details>
<details>
<summary>
2023-12-17: Let's talk about resuming negotiations.... (<a href="https://youtube.com/watch?v=-Ug-F2UunjY">watch</a> || <a href="/videos/2023/12/17/Lets_talk_about_resuming_negotiations">transcript &amp; editable summary</a>)

Israeli officials hint at restarting negotiations amidst a backdrop of high-profile incidents and shifting power dynamics, prompting speculation on the multifaceted reasons behind their sudden interest.

</summary>

"Israeli officials hint at restarting negotiations, sparking interest and speculation."
"The reasons behind the sudden interest in negotiations are multifaceted."
"Pressure from the US to end major combat operations and the implications of this decision."

### AI summary (High error rate! Edit errors on video page)

Israeli officials hint at restarting negotiations, sparking interest and speculation.
Reports indicate the boss of Israeli intelligence is involved in restarting negotiations.
The reasons behind the sudden interest in negotiations are multifaceted.
Mention of the three captives and their fate being a factor in the negotiation talks.
High-profile incidents involving Israeli troops, including the deaths of captives and a US aid contractor.
Pressure from the US to end major combat operations and the implications of this decision.
Shipping companies avoiding the Red Sea, impacting foreign policy and power dynamics.
Israel's desire to negotiate from a position of strength before potential pressure weakens their position.
The uncertainty and potential productivity of these negotiations.
Israel appears open to and actively pursuing negotiations.

Actions:

for diplomatic analysts,
Analyze the implications of Israeli negotiations and their potential outcomes (implied).
</details>
<details>
<summary>
2023-12-17: Let's talk about how Tucker's doing.... (<a href="https://youtube.com/watch?v=J12jPJ45I30">watch</a> || <a href="/videos/2023/12/17/Lets_talk_about_how_Tucker_s_doing">transcript &amp; editable summary</a>)

Beau checks in on Tucker Carlson's openness to the flat Earth theory, suggesting sharing with Fox News-watching relatives.

</summary>

"Tucker Carlson was once one of the highest rated people on a news network, is open to the idea that the earth is indeed not round."
"It seems like that might be something that can get through some pretty heavy defenses."
"He's on his way to, yeah, he's going to be hanging out with Jones soon I guess."

### AI summary (High error rate! Edit errors on video page)

Checking in on Tucker Carlson after some time.
Carlson's openness to the idea of the Earth being flat.
Mention of deception and lack of trust in preconceptions.
Carlson's surprising pushback on the flat Earth theory.
Speculation on Carlson hanging out with Jones soon.
Suggesting sharing this information with Fox News-watching relatives.

Actions:

for fox news viewers,
Share clips of Tucker Carlson's comments on the flat Earth theory online (suggested).
</details>
<details>
<summary>
2023-12-17: Let's talk about Taylor Swift, a comedy club, and international relations.... (<a href="https://youtube.com/watch?v=tqsBrRvXmvI">watch</a> || <a href="/videos/2023/12/17/Lets_talk_about_Taylor_Swift_a_comedy_club_and_international_relations">transcript &amp; editable summary</a>)

Taylor Swift's visit to a comedy club sparks baseless outrage from the right wing, revealing a disconnect with reality and voter demographics.

</summary>

"You need to calm down."
"This is more right-wing trying to generate outrage and basically grab headlines."
"The right in the United States is showing how disconnected they are from the rest of the United States."
"The short version is that this is just well, normal right-wing politics at this point."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Taylor Swift attending a comedy club led to outrage from the right wing in the United States.
The comedy club supported a charity called ANERA, which does humanitarian work in Gaza.
Right-wing individuals have accused Taylor Swift of supporting Hamas due to her connection to the charity.
ANERA is a 501c3 organization that operates with strict regulations to prevent funding designated groups like Hamas.
The outrage is fueled by the belief that any charity operating in Gaza is indirectly funding Hamas.
The right wing's anger towards Taylor Swift seems to be more about generating outrage and staying relevant.
Beau draws parallels to how people unknowingly support organizations through indirect means, much like the situation with ANERA.
The focus on attacking Taylor Swift may not resonate well with a significant portion of her voting-age audience.
Beau criticizes the right wing for being disconnected from the broader population and potential voters.
The outrage and boycott calls are seen as a tactic to energize the right-wing base without much fact-checking or understanding.

Actions:

for social media users,
Research and support ANERA (suggested)
Stay informed about charity organizations operating in conflict zones (implied)
</details>
<details>
<summary>
2023-12-16: Let's talk about more info on GOP vs MAGA.... (<a href="https://youtube.com/watch?v=nxD6aZFoqLU">watch</a> || <a href="/videos/2023/12/16/Lets_talk_about_more_info_on_GOP_vs_MAGA">transcript &amp; editable summary</a>)

The current Speaker of the House's attempts to lessen far-right influence within the Republican Party may lead to discord and potential dysfunction, impacting independent voters.

</summary>

"It's safe to say that the honeymoon period between the current speaker and the MAGA Republicans, oh it's over."
"They may be confusing their most vocal supporters for the sentiment of everybody that they are supposed to represent."
"The lack of clarity regarding messaging may lead to issues in presentation."

### AI summary (High error rate! Edit errors on video page)

The current situation within the Republican Party in the House is being discussed, particularly decisions made for the budget indicating an attempt to lessen the influence of the far-right faction.
A memo was circulated before a vote, seen as a move to in-run conservative objections and pass liberal military policy with House Democrats' help.
The relationship between the current Speaker and MAGA Republicans seems to be over, as indicated by the memo.
There may be objections similar to those faced by McCarthy, comparing Speaker Johnson to Speaker Boehner.
The MAGA base might realize that a hard-line approach doesn't benefit them, unlike an incremental one.
Despite Democratic control in the Senate and presidency, promises made to appeal to the far-right may not be fulfilled.
The far-right within the Republican Party is heavily invested in social media echo chambers, potentially missing pushback and confusing vocal supporters with general sentiment.
Discord within the party seems to have accomplished nothing, and they may not be in a good position moving towards the election, especially with difficulties in explaining the impeachment inquiry.
The lack of clarity regarding messaging may lead to issues in presentation, potentially impacting independent voters.
If the far right pushes too hard, it could make the Republican Party appear more dysfunctional, affecting independent voters.

Actions:

for political analysts, republican party members,
Pay attention to social media echo chambers and try to break through them (implied)
Stay informed about political developments and positions of different factions within the Republican Party (implied)
</details>
<details>
<summary>
2023-12-16: Let's talk about eagles and industry.... (<a href="https://youtube.com/watch?v=mk8Y-s4O3fE">watch</a> || <a href="/videos/2023/12/16/Lets_talk_about_eagles_and_industry">transcript &amp; editable summary</a>)

Two men face charges for illegally killing thousands of birds, shedding light on the dark world of wildlife trafficking.

</summary>

"Only elephants need ivory, only rhinos need horns, and only birds need feathers."
"There's probably going to be a lot more said and if this goes to trial it will probably turn into a really big thing."

### AI summary (High error rate! Edit errors on video page)

Two men are in trouble for allegedly killing 3,600 birds, including bald and golden eagles, from January 2019 to March 2021.
They face charges for violating the Bald and Golden Eagle Protection Act, with text messages allegedly admitting to their felonies.
The birds were killed and likely sold for their feathers, but it's unclear how many were protected.
Despite not being arrested, the men have been ordered to appear, indicating a strong case against them.
The illegal wildlife trade for feathers continues due to its appeal as a status symbol.
This case may draw attention to the ongoing issue of wildlife trafficking, potentially becoming a significant trial.

Actions:

for wildlife enthusiasts, conservationists,
Join wildlife protection organizations to support efforts against wildlife trafficking (implied)
Stay informed about wildlife protection laws and report any suspicious activities involving protected species (implied)
</details>
<details>
<summary>
2023-12-16: Let's talk about Rudy and the find out portion.... (<a href="https://youtube.com/watch?v=WhBAOAcTXCY">watch</a> || <a href="/videos/2023/12/16/Lets_talk_about_Rudy_and_the_find_out_portion">transcript &amp; editable summary</a>)

A federal jury decided Rudy Giuliani must pay $148 million, signaling a shift in news trends and legal outcomes related to the election.

</summary>

"Giuliani will have to pay $148 million."
"The tide seems to have turned on that."
"He's going to have to pay."
"Get a really good accountant, tax person, stuff like that."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A federal jury decided that Rudy Giuliani will have to pay $75 million in punitive damages, plus additional amounts for emotional distress and defamation.
The total amount Giuliani has to pay is about $148 million.
The news related to the election is expected to trend due to these developments.
The tide seems to have turned against those who pushed certain theories about the election.
Legal maneuvering may occur, but Giuliani will have to pay the amount.
There might be a chance to appeal some parts of the decision, but the payments are inevitable.
Advice is given to the plaintiffs, Ms. Freeman and Ms. Moss, to get a good accountant and enjoy their retirement.

Actions:

for legal observers,
Get a good accountant and tax person (suggested)
Enjoy retirement (suggested)
</details>
<details>
<summary>
2023-12-16: Let's talk about McCarthy setting up another GOP vs MAGA event.... (<a href="https://youtube.com/watch?v=DUst7PGcyII">watch</a> || <a href="/videos/2023/12/16/Lets_talk_about_McCarthy_setting_up_another_GOP_vs_MAGA_event">transcript &amp; editable summary</a>)

McCarthy's departure sparks Republican Party turmoil in finding a replacement, leading to internal disputes and legal battles influenced by the GOP-MAGA divide.

</summary>

"These kinds of fights, generally speaking, for people who are actually concerned about getting their party in office and trying to advance policy."
"It's too early to say who would win overall on this."
"When it comes to the primaries the MAGA faction is more energized."
"Expect to see this more and more where you see things occur that pit what could be considered normal Republicans against the further right Trump-style faction."
"It's not actually about policy, it's about raising money."

### AI summary (High error rate! Edit errors on video page)

McCarthy leaving Congress has caused issues for the Republican Party in finding his replacement in his red California district.
Longtime ally Vince Fong is trying to run for McCarthy's seat but is facing issues with being on the ballot for two different offices simultaneously.
The Secretary of State's office has determined that Fong's nomination papers for Congressional District 20 were improperly submitted.
Fong is planning to take the matter to court to ensure the district has a choice in candidates.
The GOP-MAGA divide is complicating matters as a candidate further to the right also wants to run for Congressional District 20.
There will likely be a legal battle over who can even be on the ballot due to these internal party disagreements.
These internal fights within the Republican Party are draining resources and creating division between normal Republicans and the more right-wing faction.
Some factions within the party prioritize generating headlines and outrage to raise money over advancing policy.
The primary process will be significant, with the more energized MAGA faction potentially influencing outcomes.
Money plays a significant role in determining outcomes, but the primary process will ultimately decide the direction of the party.

Actions:

for political observers, republican party members,
Support candidates focused on policy and party unity (implied).
Stay informed and engaged in the primary process (implied).
</details>
<details>
<summary>
2023-12-15: Let's talk about the Trump who stole Christmas.... (<a href="https://youtube.com/watch?v=888hWfemKko">watch</a> || <a href="/videos/2023/12/15/Lets_talk_about_the_Trump_who_stole_Christmas">transcript &amp; editable summary</a>)

Beau talks about the legal battle around Trump's claim of presidential immunity, urging a serious consideration of its historic significance and impact.

</summary>

"It's also worth noting that that's the last day for that filing."
"It is baffling to me that there are people who are still viewing this as something that is just of normal political substance."
"This is a criminal trial of the former president of the United States and the crux of it, what is going to be decided in that courtroom is whether or not Trump sought to basically engage in a self-coup to interfere with the election to the point that it would alter the outcome."
"There are a whole bunch of people who believed the lies about the election that are going to miss Christmas for a while."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of Jack Smith and the Grinch, connecting it to historic developments in the United States.
The legal team for former President Trump had issues with Smith's request for an expedited schedule regarding Trump's claim of presidential immunity.
The legal team argued against the proposed schedule, stating that it required working round the clock during the holidays, disrupting family and travel plans.
They likened the situation to the Grinch trying to stop Christmas from coming, using a metaphor that didn't resonate with the appeals court.
Beau points out the significance of this case, labeling it potentially the most consequential criminal case in American history.
He stresses the importance of not viewing this as a normal political matter, as it involves allegations of a self-coup by Trump to interfere with the election outcome.
Beau notes that those who believed election lies will miss Christmas due to the ongoing situation.
He suggests getting work done ahead of time instead of pushing it to Christmas day.
The central issue in the courtroom will be whether Trump sought to alter the election outcome through interference.
Beau concludes by reflecting on the gravity of the situation and urging viewers to have a good day.

Actions:

for public, concerned citizens,
Contact legal aid organizations for support (suggested)
Support organizations aiding those affected by misinformation (suggested)
</details>
<details>
<summary>
2023-12-15: Let's talk about an end to the McGonigal saga.... (<a href="https://youtube.com/watch?v=a-gz8pcREDc">watch</a> || <a href="/videos/2023/12/15/Lets_talk_about_an_end_to_the_McGonigal_saga">transcript &amp; editable summary</a>)

Former FBI counterintelligence boss faces 50-month sentence for working with a Russian oligarch, tarnishing US counterintelligence's reputation.

</summary>

"Counterintelligence people are supposed to be the government's most trusted people."
"The judge sentenced McGonagall to 50 months."
"This was a big black eye to US counterintelligence."

### AI summary (High error rate! Edit errors on video page)

Explains the final chapter in the story of McGonagall, a former FBI counterintelligence boss accused of working for a Russian oligarch to lift sanctions.
McGonagall pleaded guilty to the accusations, although no actual espionage or trading of secrets was proven.
The prosecution requested a 60-month sentence, while McGonagall's attorneys asked for probation.
The judge sentenced McGonagall to 50 months, considering the seriousness of the offense and his previous position.
The high sentence was likely influenced by McGonagall's role as the boss of the counterintelligence division and the trust placed in individuals in such positions.
This case is significant because it tarnishes the reputation of US counterintelligence.
McGonagall's attorneys hoped to leverage his remorse and past work to secure a lenient sentence, but the judge did not overlook his position.
The sentence of 50 months, although high, is not excessive and signifies the end of this prominent case.
It's unlikely that there will be any appeals following this sentencing.

Actions:

for law enforcement officials,
Keep a keen eye on individuals in positions of trust and power (implied).
</details>
<details>
<summary>
2023-12-15: Let's talk about NATO, congress, and Trump.... (<a href="https://youtube.com/watch?v=IR7ePHJNihM">watch</a> || <a href="/videos/2023/12/15/Lets_talk_about_NATO_congress_and_Trump">transcript &amp; editable summary</a>)

NATO is pivotal for American influence globally, with recent congressional action showing concerns about Trump's impact on US power.

</summary>

"NATO is the building block of American influence and dominance."
"Republicans in Congress believe Trump might do something ruinous to American power that they had to act."
"They actually believe he is somebody who is so inept at foreign policy."

### AI summary (High error rate! Edit errors on video page)

NATO is a critical component of American influence and dominance around the world.
Regardless of political ideology, NATO is universally recognized as key to American power.
The preservation of NATO is viewed as paramount by both the far right and the far left for different reasons.
Congress recently passed a measure making it impossible for a president to unilaterally withdraw from NATO.
This move by Congress signals concerns about a potential future president, particularly alluding to Trump's actions.
Republicans in Congress supported this measure without objection, suggesting concerns about Trump's impact on American power.
This action implies that even those endorsing Trump believe he may be inept at foreign policy.
The measure was passed as a safeguard against potential actions by a re-elected Trump that could harm American interests globally.

Actions:

for congress members, policymakers, voters,
Contact Congress members to express support for measures safeguarding American interests (implied)
Stay informed and engaged in foreign policy decisions affecting American influence (implied)
</details>
<details>
<summary>
2023-12-15: Let's talk about Iowa, Tennessee, and a temple.... (<a href="https://youtube.com/watch?v=FG5vpP5rnBU">watch</a> || <a href="/videos/2023/12/15/Lets_talk_about_Iowa_Tennessee_and_a_temple">transcript &amp; editable summary</a>)

The Satanic Temple's display in Iowa sparks controversy, while an after-school Satan Club forms in Tennessee, raising questions about religious freedom and separation of church and state.

</summary>

"If you open the door, well, all religions get to walk through."
"The desire to try to convert people on state property or to signal your allegiance to a particular voting bloc tends to just lead to issues."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The Satanic Temple set up a religious display in the Iowa State Capitol, sparking controversy among politicians and religious leaders.
The display was vandalized by someone from Mississippi, charged with criminal mischief.
An after-school Satan Club linked to the Satanic Temple is forming in Tennessee.
The Temple clarified they don't advocate for introducing religion into public schools unless other religious groups are present.
Right-wing politicians often push for religious freedom but mainly mean their religion.
Separation between church and state exists to allow all religions, not just one, access to state facilities.
Changes in rules may occur where religious groups are no longer allowed to use state or county facilities or funds.
There's a possibility that this situation is a form of protest, and changes may or may not happen.
The desire to display religious beliefs on state property can lead to issues and undermine religious freedom.
Beau reminds that the US was founded as a non-theocratic country, and that principle has served well.

Actions:

for activists, educators, policymakers,
Support inclusive policies in schools by advocating for the presence of diverse religious groups on campus (implied)
Advocate for the separation of church and state to ensure religious freedom for all (implied)
</details>
<details>
<summary>
2023-12-14: Roads Not Taken Special Trump Exhibit F (<a href="https://youtube.com/watch?v=zRvC4-A_Tak">watch</a> || <a href="/videos/2023/12/14/Roads_Not_Taken_Special_Trump_Exhibit_F">transcript &amp; editable summary</a>)

Beau provides updates on legal cases involving Trump, including potential consequences, with Chief Spro's testimony possibly leading to criminal charges, as the legal drama unfolds.

</summary>

"Everything that is going to happen with that, all of the dramatic stuff, it's occurred."
"The general consensus among legal observers is that Trump is going to end up paying a sizable fee here."
"Most of Biden's term has been fixing Trump's mistakes."
"Having the right information will make all the difference."
"It's worth remembering that we know now that Chief Spro is basically on tour talking to various jurisdictions about what occurred."

### AI summary (High error rate! Edit errors on video page)

Beau provides a recap of recent developments in the legal cases involving Trump, including a briefing described by Kenneth Cheesebrough to Michigan officials about fake electors and a ruling in the DC case.
Trump won a ruling in the DC case putting federal election interference proceedings on hold, but an appeals court agreed to expedite the case with briefs due between December 23rd and January 2nd.
A federal appeals court ruled that Trump cannot use presidential immunity to dismiss a civil defamation case brought by E. Jean Carroll, set to go to trial in January.
Smith's team accessed Trump's phone records for the post-election period, potentially revealing damaging information about his phone calls, Twitter use, and locations.
The New York case involving Trump has ended the testimony phase after 10 weeks, with briefs due by January 5th and a verdict expected by the end of January, likely resulting in Trump paying a sizable fee.
Legal observers expect Trump to face consequences in the New York case, with the possibility of a significant financial penalty or even the "corporate death penalty."
In other news, Trump claims Biden will lead the country into a depression, but the Dow Jones has hit an all-time high, with Trump leaving office with fewer jobs than when he started.
Beau mentions that Chief Spro's testimony and recordings provided may lead to criminal charges against Trump in various jurisdictions.

Actions:

for legal observers, political analysts,
Follow updates on the legal cases involving Trump (suggested)
Stay informed about the developments in the legal proceedings (suggested)
Support transparency and accountability in legal matters (implied)
</details>
<details>
<summary>
2023-12-14: Let's talk about the GOP vs MAGA in the House.... (<a href="https://youtube.com/watch?v=B2Bdx-7qF3Y">watch</a> || <a href="/videos/2023/12/14/Lets_talk_about_the_GOP_vs_MAGA_in_the_House">transcript &amp; editable summary</a>)

Beau analyzes the strategic political maneuvers behind the defense budget passing in the House, showcasing the sidelining of the far-right faction and Speaker Johnson's consolidation of power.

</summary>

"It shows that they don't have any power."
"It is good for the Democratic Party to get rid of them because it's also where all of the weird ideas come from."
"Removing that kind of behavior from the military, it's not gonna happen."
"It consolidates power under Johnson and makes the MAGA faction well weaker."
"The reduction in support that that faction has now, it's going to make it harder and harder for them to mount a challenge to the Speaker."

### AI summary (High error rate! Edit errors on video page)

Analyzes the passing of the defense budget in the House.
Focuses on how the bill made it through, not its contents.
Speaker Johnson's contrast with Speaker McCarthy, attributing McCarthy's downfall to his unwillingness to take certain actions.
The compromise bill passed in the House with a vote of 310 to 118, with majority Republican support.
Opposition from some Republicans stemmed from what wasn't included in the bill, particularly far-right culture war elements.
Speaker Johnson employed procedural tactics and Democratic votes to push the bill through, sidelining the far-right faction.
Johnson's repeated actions suggest a deliberate effort to reduce the influence of the far-right MAGA faction within the Republican Party.
Removing certain elements from the bill weakened the far-right faction's standing and political capital.
Johnson's strategy aims to consolidate power and marginalize the performative far-right faction.
The inclusion of aid for Ukraine in the bill indicates a commitment to support, albeit not the full aid package required.
Overall, the transcript delves into political maneuvering within the Republican Party and the implications of strategic decision-making.

Actions:

for politically engaged individuals,
Contact your representatives to express your views on defense budget priorities (implied)
Support organizations working towards aiding Ukraine (implied)
</details>
<details>
<summary>
2023-12-14: Let's talk about a poll spelling trouble for the US and Israel.... (<a href="https://youtube.com/watch?v=Ggj-Gf60akA">watch</a> || <a href="/videos/2023/12/14/Lets_talk_about_a_poll_spelling_trouble_for_the_US_and_Israel">transcript &amp; editable summary</a>)

Beau explains polling results from the West Bank and Gaza, revealing challenges for U.S. and Israeli plans against Hamas, advocating for negotiated resolutions in conflicts.

</summary>

"It's just a thought. Y'all have a good day."
"Even if this polling is off, these types of conflicts are nothing more than PR campaigns with violence."
"It is the responsibility of everybody to acknowledge that this ends at a negotiation table."

### AI summary (High error rate! Edit errors on video page)

Explains polling conducted in the West Bank and central and southern Gaza, revealing surprising sentiments.
Describes the goal behind provocative operations like the one on October 7th: to provoke an overreaction and security clampdown.
Notes that such tactics are used worldwide to generate outrage and international sympathy.
Points out that overreactions help smaller forces, like Hamas, gain new supporters.
Mentions that 60% of respondents want the Palestinian Authority dissolved, and support for Hamas has tripled in the West Bank.
Suggests that Israeli and U.S. plans may face challenges due to the increased support for Hamas.
Indicates that the U.S. plan relied on degrading Hamas to make them combat ineffective for the Palestinian authority to govern Gaza.
Expresses skepticism about the success of current plans due to the polling results.
Urges for consideration of alternate strategies as current approaches seem unlikely to succeed.
Advocates for a negotiated resolution to conflicts like these and warns against relying solely on overwhelming force.

Actions:

for activists, policymakers, diplomats,
Advocate for peaceful negotiations to end conflicts (implied)
Support aid efforts in Gaza (implied)
Work towards de-escalation and conflict resolution in affected regions (implied)
</details>
<details>
<summary>
2023-12-14: Let's talk about Zelenskyy, houses, and fact checking.... (<a href="https://youtube.com/watch?v=5gwbcDqTyGQ">watch</a> || <a href="/videos/2023/12/14/Lets_talk_about_Zelenskyy_houses_and_fact_checking">transcript &amp; editable summary</a>)

Beau explains how to fact-check claims like Zelensky buying a mansion, revealing the importance of independent verification and critical information consumption to combat misinformation.

</summary>

"Now download a couple images of the house. Doesn't matter which ones."
"It's just made up for engagement and clicks."
"Our biggest sign should have been that it wasn't property records being shown."
"There is nothing in that tweet that's true."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of fact-checking information yourself, especially in light of current stories circulating about Zelensky.
Responds to a viewer's claim about Zelensky using aid money to buy a yacht, which led to the viewer sending a link to a tweet stating Zelensky bought a mansion in Florida.
Advises on the initial step of fact-checking by searching for the claim on Google and checking for news outlets reporting on it.
Suggests waiting for a major outlet to pick up the news if it initially doesn't show up in search results.
Encourages looking at non-news results and websites dedicated to debunking social media rumors to verify information.
Guides viewers on using a reverse image search tool like TinEye to verify the authenticity of images being shared.
Analyzes the edited image from the tweet and proves its falsity through comparison with original images.
Dissects the fake naturalization certificate image and clarifies that the house in question is not in Vero Beach and not worth $20 million.
Emphasizes the importance of being a critical consumer of information to avoid being misled by sensationalized claims.
Commends the viewer for questioning and seeking accurate information, stressing the need to develop skills to discern misinformation.

Actions:

for information consumers,
Use reverse image search tools like TinEye to verify images shared online (implied).
Verify sensational claims by checking multiple sources and using critical thinking skills (implied).
Develop the ability to discern misinformation and avoid spreading false information for engagement (implied).
</details>
<details>
<summary>
2023-12-14: Let's talk about Hunter Biden running circles around the GOP.... (<a href="https://youtube.com/watch?v=qnCPdYokZ78">watch</a> || <a href="/videos/2023/12/14/Lets_talk_about_Hunter_Biden_running_circles_around_the_GOP">transcript &amp; editable summary</a>)

Hunter Biden outplays Republicans in a political maneuver, pushing for a public hearing and diverting attention from the formalized impeachment inquiry.

</summary>

"Let me state as clearly as I can, my father was not involved in any of my business."
"He certainly seems to have ran circles around him today."
"This is all a political stunt from everybody involved."

### AI summary (High error rate! Edit errors on video page)

Hunter Biden successfully outplayed the Republican Party politically by requesting a public hearing.
Republicans are investigating Hunter Biden without evidence linking his activities to his father, the President.
Hunter Biden wanted the hearing public to avoid cherry-picked information being released.
Hunter Biden stated that his father was not involved in any of his business dealings.
Hunter Biden's public statement pushed the Republican Party into potentially needing to make everything public.
The House formalized an impeachment inquiry against the President on the same day, overshadowed by Hunter Biden's actions.
Coverage of the impeachment inquiry included phrases like "debunked claims" and "lack of evidence."
Hunter Biden's team played the situation well politically, making the Republican Party look poorly thought out.
The focus on political stunts by all involved overshadows the real news and developments.
The Republican Party may inadvertently make Hunter Biden a more prominent figure by pursuing their inquiries.

Actions:

for political observers,
Follow political developments and stay informed (exemplified)
Engage in political discourse and critical thinking (exemplified)
</details>
<details>
<summary>
2023-12-13: Let's talk about the UN assembly vote and what it means.... (<a href="https://youtube.com/watch?v=T275QwTD09M">watch</a> || <a href="/videos/2023/12/13/Lets_talk_about_the_UN_assembly_vote_and_what_it_means">transcript &amp; editable summary</a>)

Beau explains the limited impact of the UN General Assembly vote on enforcing a ceasefire, signaling potential shifts in Israel's stance but unlikely to immediately pressure for a ceasefire.

</summary>

"The weight of this vote as far as its ability to force a ceasefire, it's about on par with a Twitter poll."
"The fact that they couldn't get it through the Security Council, that's Cold War politics that's still left over."
"The signaling that occurred here should be a blinking red light in Tel Aviv saying, hey, maybe you need to change course on some things."

### AI summary (High error rate! Edit errors on video page)

Explains the purpose of the General Assembly vote at the United Nations, which was to demand a ceasefire.
Outlines the voting results: 153 in favor, 23 abstained, and 10 opposed.
Clarifies that the vote cannot force a ceasefire and is not binding.
Notes that the vote may not have a significant impact on pressuring Israel to announce a ceasefire immediately.
Emphasizes that the US and Israel have differing opinions on post-offensive plans, with the US having international backing for its plan.
Suggests that the vote signals Israel may need to reconsider its stance based on the international community's position.
Indicates that the UN's goal is to promote peace, even though Cold War politics, with the US vetoing, influenced the Security Council's decision.

Actions:

for global citizens,
Monitor international developments and diplomatic efforts for updates on the Israel-Palestine conflict (implied).
</details>
<details>
<summary>
2023-12-13: Let's talk about policy, Biden, and student debt.... (<a href="https://youtube.com/watch?v=3Xe-Q8ORfWo">watch</a> || <a href="/videos/2023/12/13/Lets_talk_about_policy_Biden_and_student_debt">transcript &amp; editable summary</a>)

Policy shifts take time, marking small wins is vital; embracing nuance in discussing big changes, especially in forgiving student debt over $100 billion.

</summary>

"Policy shifts take time, and it's vital to mark small wins."
"You have to take the time to acknowledge those small wins."
"Those little changes add up real quick."
"More than $100 billion has been forgiven."
"Acknowledging the wins helps motivate you and keep you in the fight."

### AI summary (High error rate! Edit errors on video page)

Policy shifts take time, and it's vital to mark small wins even in setbacks of big policy changes.
Explains the importance of nuance in discussing big policy changes in the current political climate.
Illustrates an example of legislation in the Senate concerning hedge funds not owning single-family housing.
Emphasizes the need for a 10-year period for hedge funds to sell off their inventory to prevent a housing crash.
Talks about the challenges of understanding large financial numbers like $400 billion for student loan forgiveness.
Reveals that the Biden administration has forgiven over $100 billion in student debt, impacting millions.
Notes the lack of recognition for these small wins and the disconnect between commentary and actual events.
Stresses the significance of acknowledging small wins to understand and appreciate ongoing progress.
Mentions the motivation and fight sustainability gained from recognizing and celebrating incremental changes.
Encourages taking a moment to appreciate progress, combat cynicism, and stay engaged in pushing for change.

Actions:

for advocates, activists, students,
Acknowledge and celebrate small wins in your community's progress (implied).
Stay informed about ongoing policy changes and their impacts (implied).
Support initiatives that address financial issues and student debt forgiveness (implied).
</details>
<details>
<summary>
2023-12-13: Let's talk about Ukraine, Russia, and numbers.... (<a href="https://youtube.com/watch?v=JNmwv19LiB4">watch</a> || <a href="/videos/2023/12/13/Lets_talk_about_Ukraine_Russia_and_numbers">transcript &amp; editable summary</a>)

The U.S. released revealing numbers on Russia's losses in Ukraine, but some politicians' reluctance to support aid stems from a far-right ideology alignment with Putin.

</summary>

"There are certainly people watching this video who are like I want US foreign policy to fail."
"They have chosen a far right-wing ideology over US national interest."
"They have to make the U.S. lose so they can feel better about their Twitter talking points."

### AI summary (High error rate! Edit errors on video page)

The U.S. released declassified numbers on Russia's losses in Ukraine, revealing high casualty rates.
Russia lost a significant amount of tanks, IFVs, and APCs during the invasion.
Russia aims to boost its active duty troops to 1.5 million by recruiting individuals deemed undesirable by Putin.
The U.S. releasing these numbers is a strategic move to showcase their intelligence accuracy and influence.
Some U.S. politicians question where the money is going, which is towards supporting Ukraine.
There is reluctance from certain politicians to support aid due to their far-right ideology alignment with Putin.
Despite the release of these numbers, it may not sway those opposed to aiding Ukraine.
The Democratic Party may need to compromise to ensure aid is approved, potentially impacting future elections.
Those against aid post-number release may be driven by ideology rather than corruption concerns.
Civilian casualties in conflicts often surpass combatant losses.

Actions:

for politically aware viewers.,
Support aid efforts for Ukraine (suggested).
Advocate for policies that prioritize humanitarian support (implied).
</details>
<details>
<summary>
2023-12-13: Let's talk about Biden's off-camera comments.... (<a href="https://youtube.com/watch?v=hnaIzaOeINc">watch</a> || <a href="/videos/2023/12/13/Lets_talk_about_Biden_s_off-camera_comments">transcript &amp; editable summary</a>)

Biden's off-camera comments reveal US-Israeli tensions, with the US banking on a challenging plan for Gaza's future amid diplomatic pressure.

</summary>

"The US is going to exert diplomatic pressure only, period, full stop."
"This is not me saying, yay, this is good. This is me saying what's happening."
"US foreign policy is hinging on the successful completion of something US advisors said was really ill-advised."

### AI summary (High error rate! Edit errors on video page)

Biden made off-camera comments about Netanyahu's government issues and the two-state solution, implying a disagreement with the current Israeli government.
Biden expressed concern that Israel is running out of time and facing global backlash due to indiscriminate bombing.
Israeli officials mentioned a disagreement with the US regarding the "day after Hamas" strategy.
The US plans to exert diplomatic pressure but not take significant action during the Israeli offensive against Hamas.
For the US plan to work, Hamas must be degraded, Palestinian Authority needs to govern Gaza, and an international coalition must act as peacekeepers.
Israel seems hesitant about the US plan, creating diplomatic tension between US and Israeli officials.
The US is banking on diplomatic pressure to achieve its goals in the conflict, but doubts remain about the feasibility of the plan.
If Hamas remains combat effective, the Palestinian Authority won't be able to govern Gaza effectively.
Economic consequences may influence Israel's offensive actions as international pressure mounts.
US foreign policy appears to rely heavily on the success of a challenging plan that experts have deemed difficult.

Actions:

for foreign policy analysts,
Monitor diplomatic developments closely and stay informed about US-Israeli relations (implied)
Advocate for peaceful resolutions and humanitarian aid for Palestinian civilians affected by the conflict (implied)
</details>
<details>
<summary>
2023-12-12: Let's talk about the military review of the Teixeira case.... (<a href="https://youtube.com/watch?v=F5XyJvvF3wo">watch</a> || <a href="/videos/2023/12/12/Lets_talk_about_the_military_review_of_the_Teixeira_case">transcript &amp; editable summary</a>)

Beau gives an update on Teixeira's case, revealing security breaches and the need for a security culture revival within the military.

</summary>

"The scenario was so bizarre that there were a lot of people who believed either A, it was a foreign intelligence operation, or B, it didn't actually happen..."
"The way the information was handled was so bad."
"There's probably more to it."
"I expect a lot of changes when it comes to how the U.S. handles classified material."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Provides an update on Teixeira and the internal report revealing what happened.
Teixeira, with an IT job, gained access to classified material and shared it on Discord to appear cool.
The bizarre scenario led to theories of a foreign intelligence operation or an information leak operation.
The report confirmed the events and criticized the lax security procedures, resulting in 15 military personnel disciplined up to Colonel rank.
Some coworkers knew Teixeira accessed unauthorized material, raising questions on why no action was taken.
Despite recent news on leaks and compromised information, security procedures were neglected, indicating a need for a security culture revival.
Teixeira has been charged, pleaded not guilty, and plans to go to trial.
More actions may follow due to the lax security culture within the military.
Anticipates changes in how the U.S. handles classified material given the frequent breaches.
Concludes with a prediction of upcoming changes in handling classified information.

Actions:

for military personnel, security professionals,
Monitor and report unauthorized access to classified material within your organization (implied)
Advocate for stronger security procedures and culture within your workplace (implied)
Stay informed about updates and changes in handling classified information (implied)
</details>
<details>
<summary>
2023-12-12: Let's talk about SCOTUS, Washington, and conversion.... (<a href="https://youtube.com/watch?v=-Kj1nJ0zsdA">watch</a> || <a href="/videos/2023/12/12/Lets_talk_about_SCOTUS_Washington_and_conversion">transcript &amp; editable summary</a>)

Beau explains why the Supreme Court's decision on Washington's conversion therapy ban is more significant than just a win, revealing the core issue of state regulation over professional conduct.

</summary>

"The core of this case is whether or not the state has the ability to regulate conduct that occurs under a state issued professional license."
"It's a win but it was an opportunity for it to be an even bigger win."
"The Supreme Court's decision to not hear the case means the ban stands, but it could have been a nationwide decision."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court of the United States did not take up a case regarding Washington State's ban on conversion therapy.
Washington State banned conversion therapy, a discredited practice aiming to change someone's orientation or gender.
Lower courts are divided on whether such bans can exist.
The Supreme Court's decision to not hear the case means the ban stands, but it could have been a nationwide decision.
The core issue isn't about free speech but whether states can regulate conduct under professional licenses.
Allowing such speech under professional licenses could weaken state licensing systems.
Beau believes the Supreme Court should have taken up the case to affirm state regulations.
The conservative justices likely avoided the case due to the inevitable ruling in favor of state regulation.
The Supreme Court's decision maintained the ban on conversion therapy but missed an chance for a broader impact.
Allowing such speech could lead to harmful practices without legal repercussions.

Actions:

for activists, policymakers,
Advocate for policies that protect vulnerable communities from harmful practices (implied)
Support organizations working to uphold state regulations on professional conduct (implied)
</details>
<details>
<summary>
2023-12-12: Let's talk about SCOTUS expediting consideration of Trump's claim.... (<a href="https://youtube.com/watch?v=VlxzRVy8_DU">watch</a> || <a href="/videos/2023/12/12/Lets_talk_about_SCOTUS_expediting_consideration_of_Trump_s_claim">transcript &amp; editable summary</a>)

Supreme Court fast-tracking consideration of Trump's presidential immunity claim, prompting critical analysis from Beau on the limits of presidential power.

</summary>

"Presidents are not kings."
"There's no case law, there's no precedent, there's nothing like that."
"If you're saying they can literally do anything they want because they sold you a hat and a bunch of rhetoric you might want to revaluate."
"Presidents don't have full immunity for anything that they do."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Supreme Court considering Trump's claim of presidential immunity on an expedited schedule.
Appeals court reviewing the case quickly as well.
Trump given until the 20th to respond, and until Wednesday by the appeals court.
Beau previously argued that presidential immunity as claimed by Trump is not supported by the Constitution.
Beau received a message asserting that presidential immunity is established in case law and referenced previous investigations.
Beau refutes the argument by clarifying that there has never been a ruling on criminal immunity for presidents.
The Office of Legal Counsel memo cited by the message does not carry the force of law and can be changed.
Beau mentions a precedent in Nixon v. Fitzgerald where the president was immune in civil liability cases within their official duties.
However, this immunity does not extend to pre-presidency matters or criminal cases.
Beau questions the idea of full immunity for presidents and how it could potentially lead to abuse of power.
He points out the absurdity of suggesting that presidents have total immunity for any action.
Beau underscores the importance of not granting unlimited power based on partisan support.
He expresses skepticism about the Supreme Court taking up the case due to the weak argument presented.
Beau concludes by discussing the ongoing legal proceedings and speculates on the Supreme Court's decision.

Actions:

for legal scholars, political analysts, concerned citizens.,
Contact legal advocacy organizations to stay informed on updates regarding the Supreme Court and appeals court decisions (implied).
</details>
<details>
<summary>
2023-12-12: Let's talk about 2 unwise Trumpworld statements.... (<a href="https://youtube.com/watch?v=KqO1H9bcnGY">watch</a> || <a href="/videos/2023/12/12/Lets_talk_about_2_unwise_Trumpworld_statements">transcript &amp; editable summary</a>)

Beau explains recent incidents involving Giuliani and Trump in Trump World, showing their tactics to influence public opinion and drag out legal proceedings.

</summary>

"I told the truth. They were engaged in changing votes."
"If Giuliani is engaged in that behavior while the dollar amount is being figured out, it probably means the dollar amount goes up."
"If there was any basis to the president's claims, it seems like they would want it to go to the Supreme Court."
"You have two instances last night that were Trump world acting like Trump world."
"The judicial system literally does not care about the court of public opinion."

### AI summary (High error rate! Edit errors on video page)

Explaining two separate incidents in Trump World that happened recently, involving Rudy Giuliani and the former president.
Giuliani is in the middle of dealing with a defamation case related to election workers and is determining how much money he will have to pay for his comments.
Despite being ruled out of bounds by the judge, Giuliani insists that his comments were true and does not regret them.
Giuliani repeats his controversial statement during the proceedings, potentially leading to increased monetary penalties.
Moving on to Trump, who claims presidential immunity and asked the Supreme Court to decide on it quickly, which the Court agreed to expedite consideration of.
Trump's team released a statement criticizing the actions as an attempt to prevent him from retaking the Oval Office in 2024.
Beau points out that if Trump truly believed in his presidential immunity claim, taking it to the Supreme Court quickly should benefit him.
The statements from Giuliani and Trump's team indicate their strategies to drag out legal proceedings and cater to their base rather than a strong legal basis.
Beau observes that both instances show Trump World trying to influence public opinion through statements, not realizing that the judicial system is impartial.
Concluding with a reflection on the ill-advised nature of both statements and how they reveal Trump World's tactics.

Actions:

for political observers, activists,
Contact legal aid organizations to support efforts combating misinformation and defamation cases (suggested)
Stay informed on legal proceedings related to political figures and hold them accountable (implied)
</details>
<details>
<summary>
2023-12-11: Let's talk about Trump, Smith, and SCOTUS.... (<a href="https://youtube.com/watch?v=IfWxfvGuaQc">watch</a> || <a href="/videos/2023/12/11/Lets_talk_about_Trump_Smith_and_SCOTUS">transcript &amp; editable summary</a>)

Beau explains why presidential immunity does not exist and questions whether the Supreme Court will side with Trump over the Constitution.

</summary>

"The idea of presidential immunity and the way Trump is describing it, it's made up. That's not a thing."
"You can support Trump or you can support the country. You can support the Constitution or you can support the real estate developer."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of presidential immunity being claimed by Trump in the context of the January 6th case in DC.
Smith has asked the Supreme Court to decide immediately on the question of whether a former president is immune from federal prosecution for crimes committed while in office.
Reads a section from Article 2 of the Constitution to show that presidential immunity doesn't exist.
Notes that the Constitution allows for criminal trial even if a president has been impeached and removed from office.
States that the idea of presidential immunity as described by Trump is fabricated.
Criticizes the Republican Party for choosing to support Trump over the country and Constitution.
Predicts that the Supreme Court may not find the need to hear Trump's appeal and might leave it to the appeals court.
Doubts that even the current Supreme Court might side with Trump due to the clear constitutional provisions.
Concludes by expressing uncertainty about the Supreme Court's decision.

Actions:

for legal scholars, political activists,
Contact legal experts to understand the implications of presidential immunity (suggested)
Share this information to raise awareness about the Constitution's stance on presidential immunity (implied)
</details>
<details>
<summary>
2023-12-11: Let's talk about Trump's unsurprising surprise.... (<a href="https://youtube.com/watch?v=mBAo3lhgnJQ">watch</a> || <a href="/videos/2023/12/11/Lets_talk_about_Trump_s_unsurprising_surprise">transcript &amp; editable summary</a>)

Trump's surprise announcement about not testifying in the New York case leads to speculation on the case's future, with legal experts predicting a tough outcome for him and potential financial implications, expected to conclude around February.

</summary>

"Trump saying that he's gonna do one thing and doing something else."
"The question isn't whether or not it's going to go his way. The question is how bad it's going to be for him."

### AI summary (High error rate! Edit errors on video page)

Trump made a surprise announcement about not testifying in the New York civil case, which was expected.
Speculation on what happens next in the case.
The current schedule indicates deadlines for filing briefs by January 5th and closing arguments by January 11th.
Legal experts believe the case won't go well for Trump, with varying estimates on the potential financial impact.
Final determination might come in February after closing arguments in January.

Actions:

for legal analysts, observers,
Stay informed about updates on the New York case and its implications (suggested)
Follow legal commentary and expert opinions on the case (suggested)
</details>
<details>
<summary>
2023-12-11: Let's talk about St. Paul and a unique debt story.... (<a href="https://youtube.com/watch?v=p8TgHkaqqN0">watch</a> || <a href="/videos/2023/12/11/Lets_talk_about_St_Paul_and_a_unique_debt_story">transcript &amp; editable summary</a>)

St. Paul, MN uses $1.1 million from the America Rescue Plan to buy $110 million in medical debt, forgiving it and creating $100 million in relief, showcasing an innovative approach for utilizing federal funds creatively to address financial struggles.

</summary>

"Taking a million dollars and theoretically creating the impact of having a hundred million. I mean, that is cool."
"It doesn't really get more effective than that."
"When debt is sold off like this, generally it's because of missed payments and stuff like that."
"This is a cool way to use it and it can be used for a whole bunch of similar programs."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

St. Paul, MN is buying $110 million in medical debt with $1.1 million from the America Rescue Plan.
The city plans to forgive the medical debt, helping around 40,000 people.
Partnered with RIP Medical Debt to purchase and forgive the debt.
This initiative will have the impact of creating $100 million in relief from just $1.1 million spent.
The debt forgiveness will free people up financially, potentially boosting economic activity.
A unique local initiative with a massive increase in relief over the initial investment.
This innovative approach could serve as a template for other areas receiving federal funding.
It targets those most in need by addressing missed payments and financial struggles.
Utilizing federal funds creatively to alleviate issues faced by those requiring assistance.
An effective use of funds that could have far-reaching impacts on the community.

Actions:

for community leaders,
Partner with organizations like RIP Medical Debt to purchase and forgive medical debt (suggested)
Advocate for similar initiatives in your local area to alleviate financial burdens (implied)
Monitor and support programs that creatively use federal funding to assist those in need (implied)
</details>
<details>
<summary>
2023-12-11: Let's talk about Biden marching in to a pricing debate.... (<a href="https://youtube.com/watch?v=OrNpW3zm78Y">watch</a> || <a href="/videos/2023/12/11/Lets_talk_about_Biden_marching_in_to_a_pricing_debate">transcript &amp; editable summary</a>)

The Biden administration's consideration of march-in rights to address high pharmaceutical prices stirs controversy among companies and supporters, opting for a strategic approach to lower prices overall.

</summary>

"If the goal is not to mobilize support for certain products, but to get the overall price down, this is the smart way to do it."
"Undoubtedly, this is going to end up in court."
"If they are successful with this, it's going to matter to a whole lot of people."
"There is a fight going on right now to reduce their medical bills."
"If it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The Biden administration is considering utilizing march-in rights to address high prices on pharmaceutical products developed with government funding.
This move could potentially impact pricing in the United States significantly.
Pharmaceutical companies are not happy about this potential action and are threatening to stop accepting government funding.
Supporters of the Biden administration's move believe that not naming specific pharmaceutical products is a strategic decision to pressure companies into reducing prices across the board.
The administration's strategy of withholding specific product names is seen as a smart move to encourage companies to lower prices overall rather than focusing on specific products for public support.
Despite potential legal challenges from pharmaceutical companies, the administration seems determined to proceed with this course of action.
The lack of media coverage on this issue is concerning, considering the significant impact it could have on people's medical bills.
The administration's approach aims to reduce prices overall rather than mobilizing support for specific products.
The outcome of this decision will likely affect many individuals who are unaware of the ongoing efforts to lower medical costs.
This initiative could lead to a substantial change in the pharmaceutical industry's pricing practices.

Actions:

for advocates, pharmaceutical companies,
Support initiatives that aim to lower pharmaceutical prices (suggested)
Stay informed about government actions impacting pharmaceutical pricing (suggested)
Advocate for transparent and fair pricing practices in the pharmaceutical industry (suggested)
</details>
<details>
<summary>
2023-12-10: The Roads Not Taken EP17 (<a href="https://youtube.com/watch?v=RbaiId41wZs">watch</a> || <a href="/videos/2023/12/10/The_Roads_Not_Taken_EP17">transcript &amp; editable summary</a>)

Beau covers under-reported events from foreign policy to cultural news, legal updates, and Q&A, offering context and insights to help navigate the information overload.

</summary>

"We want the world, preferably all of it."
"A pretty clear indication that the appeals court sees Trump's DC trial staying on schedule."
"I see this as a very big wasted argument."
"Having the right information will make all the difference."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces "The Roads with Beau" episode 17, covering under-reported events and taking questions.
Updates on foreign policy: Putin ally's ambition for the world, potential Russian aggression towards the Baltics, and Moody's warning on China's credit rating.
Houthis threatening ships bound for Israel, a federal judge barring migrant family separation, and U.S. aid politics hindering Ukraine's success.
Special operations targeting high-level figures, fluctuating gas prices amid global tensions, and budget games by Republicans.
Legal updates include a ruling against delaying trials for political purposes and growing support for Nikki Haley over Trump.
Ohio utility regulator involved in a bribery scheme, news in culture like a movie about George Santos and Taylor Swift as Time's Person of the Year.
Elon Musk's platform changes and environmental news on COP28's failure to meet climate goals.
Senator John Kennedy's price comparison from "Home Alone," showing detachment among elected officials.
Updates on shelter fundraiser successes, future plans with the channel, and handling disagreements among commentators like Nance and Kelt.
Explanation on captives being called hostages or prisoners, focusing on non-state actors' distinctions.

Actions:

for news consumers,
Contact organizations aiding Ukraine to counteract hindrance from U.S. aid politics (suggested).
Support shelters with holiday meals and specialized items for teens (exemplified).
Stay informed on foreign policy developments and geopolitical tensions (implied).
Stay engaged with legal proceedings and understand implications of judicial decisions (implied).
Support environmental initiatives addressing climate change goals (exemplified).
</details>
<details>
<summary>
2023-12-10: Let's talk about Wisconsin, electors, and a settlement.... (<a href="https://youtube.com/watch?v=11rTjY-sol8">watch</a> || <a href="/videos/2023/12/10/Lets_talk_about_Wisconsin_electors_and_a_settlement">transcript &amp; editable summary</a>)

Wisconsin faces a significant resolution with fake electors admitting Biden's win, complicating potential criminal investigations and impacting future dynamics.

</summary>

"This is going to make it very hard for the former president to mount a defense."
"Wisconsin does not have an official criminal investigation."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Wisconsin is in the spotlight due to a civil suit resolution involving fake electors.
The settlement requires the fake electors to admit that Biden won and their actions aimed to overturn his victory.
This admission is significant as it's the first acknowledgment in the elector controversy.
Wisconsin does not have an official criminal investigation, relying on the federal government for this aspect.
Chief Broward's involvement with state investigators raises questions about potential criminal proceedings.
The conclusion of the civil suit adds to the growing evidence against attempts to deny Biden's win.
This situation complicates any defense the former president may try to mount.
The civil side of the issue has concluded, leaving room for potential further actions, whether probes or undisclosed criminal investigations.

Actions:

for wisconsin residents, legal observers.,
Stay informed about the developments regarding the resolution and potential investigations (implied).
</details>
<details>
<summary>
2023-12-10: Let's talk about Smith talking to Trump's intel team.... (<a href="https://youtube.com/watch?v=NpIstHRZMIM">watch</a> || <a href="/videos/2023/12/10/Lets_talk_about_Smith_talking_to_Trump_s_intel_team">transcript &amp; editable summary</a>)

Trump suggested a foreign government changed votes during the 2020 election, but Smith's team was prepared and investigated thoroughly, showing readiness and strategic planning.

</summary>

"Trump suggested a foreign government changed votes during the 2020 election."
"Smith's team investigated Trump's claims thoroughly."
"The real story is Smith's team being prepared for Trump's claims."
"A foreign power altering votes to make Trump lose seems illogical."
"Smith's team being proactive is more intriguing than Trump's claim being refuted."

### AI summary (High error rate! Edit errors on video page)

Trump suggested a foreign government changed votes during the 2020 election, leading to the possibility of classified material entering the case.
Trump's team talked to various intelligence and security officials to find evidence of vote flipping, but they all denied it, undercutting Trump's claim.
Smith's team anticipated and investigated Trump's claims thoroughly, even talking to the National Guard about timelines.
The real story is Smith's team being prepared for Trump's claims, showing readiness and strategic planning.
The idea of a foreign power altering votes to make Trump lose seems illogical, especially considering his foreign policy failures.
Smith's team being proactive and ready for challenges is more intriguing than Trump's claim being refuted by his own team.

Actions:

for political analysts, concerned citizens,
Contact political representatives to advocate for transparency and accountability in election challenges (suggested)
Join organizations working on election integrity and security (suggested)
</details>
<details>
<summary>
2023-12-10: Let's talk about Hollywood accidentally helping Putin.... (<a href="https://youtube.com/watch?v=B1kM80RWfhM">watch</a> || <a href="/videos/2023/12/10/Lets_talk_about_Hollywood_accidentally_helping_Putin">transcript &amp; editable summary</a>)

Actors unwittingly manipulated by Russian intel on a celebrity messaging platform raise awareness of influence operations' dangers, extending from international to domestic politics.

</summary>

"You have to be very careful what you're viewing and how much stock you put in it."
"You can't believe everything you read on the internet."
"Surely eventually you will see this level of sophistication applied to domestic politics."
"It's just something to be aware of."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Actors on a celebrity messaging platform were unknowingly used by Russian intelligence for international intrigue.
The platform allows fans to receive personalized messages from their favorite celebrities.
Messages are typically used for birthday wishes or encouragement, even for seeking help.
Russian intelligence operatives manipulated actors to send messages asking Vladimir to seek help, disguised as messages to Zelensky.
The actors, including some well-known names, were unaware of the true purpose of their messages.
The incident sheds light on the need to be cautious about the information consumed, given the rise of influence operations on various platforms.
A video portraying celebrities asking Zelensky for help received millions of views, showcasing the effectiveness of such operations.
Another tactic involves attributing fake quotes about sensitive topics to celebrities like Taylor Swift.
Beau warns about the potential spread of these tactics from international to domestic politics, especially during elections.
The sophistication of these influence operations may escalate in domestic political scenarios, potentially impacting future elections.

Actions:

for online consumers,
Be cautious about the information consumed (implied)
Stay vigilant against manipulated content on various platforms (implied)
Spread awareness about influence operations and misinformation tactics (implied)
</details>
<details>
<summary>
2023-12-10: Let's talk about Dems going after Wall Street on housing.... (<a href="https://youtube.com/watch?v=IPgbA8WJXok">watch</a> || <a href="/videos/2023/12/10/Lets_talk_about_Dems_going_after_Wall_Street_on_housing">transcript &amp; editable summary</a>)

Beau explains Democratic efforts to increase housing affordability by restricting certain entities from purchasing single-family homes, with two bills in Congress proposing different approaches and facing potential challenges from powerful interests.

</summary>

"They can't buy any single-family homes. Period. Full stop."
"There is a lot of demand, so the value, the price is going to go up."
"There are people trying to alleviate the issue."

### AI summary (High error rate! Edit errors on video page)

Explains the push by the Democratic Party to increase housing affordability by restricting certain entities from purchasing single-family homes.
Details two bills in Congress, one in the House and one in the Senate, which have different approaches to addressing the issue.
Describes the Senate bill as more direct, prohibiting hedge funds and similar entities with over 50 million in assets from buying single-family homes and giving them 10 years to sell off existing ones.
Points out the issue of limited housing supply for normal people and how investors buying single-family homes can drive up prices, leading to a decrease in affordable housing.
Expresses hope for the Senate bill's potential impact on housing affordability but acknowledges the expected pushback, especially from entities vested in maintaining the current situation.
Mentions challenges in getting the bill past the Republican Party due to powerful entities opposing it.
Notes that while some Republicans have shown concern for affordable housing, it might not be enough to ensure the bill's passage.
Indicates that the bill might have a better chance in the House but suggests waiting to see how the situation unfolds.
Concludes by mentioning that the issue is actively being addressed and encourages consideration of the proposed solutions.

Actions:

for legislative watchers,
Advocate for affordable housing policies by contacting your representatives (implied).
Stay informed about housing legislation and its potential impact on your community (implied).
</details>
<details>
<summary>
2023-12-09: Let's talk about the UN vote.... (<a href="https://youtube.com/watch?v=1eYMgd4qVdI">watch</a> || <a href="/videos/2023/12/09/Lets_talk_about_the_UN_vote">transcript &amp; editable summary</a>)

Beau explains the implications of the recent UN vote on ceasefire, detailing what it takes for the US foreign policy move to succeed, facing challenges ahead for a durable peace in Gaza.

</summary>

"US is engaged in a very big gamble here."
"The US is in essence standing alone with the UK over there in the corner going go ahead, go ahead, we got you."
"The US has been meeting with the Palestinian Authority behind the scenes..."
"It's going to be a mess."
"They have to be able to pull this off."

### AI summary (High error rate! Edit errors on video page)

Explains the recent vote at the UN regarding a ceasefire, with the US being the only vote against it.
Details the implications of invoking Article 99 by the UN Secretary General to draw attention to the Security Council.
Mentions the UK abstaining from the vote, showing alignment with the US but avoiding backlash.
Outlines the requirements for the US foreign policy move to not be considered a failure.
Expresses skepticism about Israel degrading Palestinian forces in Gaza and the ability of the Palestinian Authority to govern Gaza effectively.
Talks about the need for an Arab coalition committing to an international peacekeeping force for a durable peace in Gaza.
Points out the reluctance of Arab nations to contribute troops for peacekeeping.
Emphasizes the gamble and pressure on the US to make the foreign policy move successful.
Notes the rarity of the US standing almost alone with the UK in vetoing the ceasefire resolution.
Mentions the challenges ahead for the Biden diplomatic team in managing the situation.

Actions:

for foreign policy analysts,
Contact local representatives to push for diplomatic efforts towards a durable peace in Gaza (suggested).
Join organizations advocating for peaceful resolutions in conflict zones (implied).
</details>
<details>
<summary>
2023-12-09: Let's talk about LA, Vegas, Biden, and trains.... (<a href="https://youtube.com/watch?v=t9t2h7rRyUg">watch</a> || <a href="/videos/2023/12/09/Lets_talk_about_LA_Vegas_Biden_and_trains">transcript &amp; editable summary</a>)

Biden's $8.2 billion investment in passenger rail aims to boost the economy, create jobs, and familiarize Americans with rail travel for a sustainable future.

</summary>

"The goal is to get the American people more comfortable with rail moving passengers."
"Passenger rail is something the US probably needs to become super enthusiastic about."
"This is an attempt to alter that dynamic."

### AI summary (High error rate! Edit errors on video page)

The Biden administration announced a $8.2 billion investment in 10 passenger rail projects in the United States, the largest since Amtrak's creation in 1971.
The goal of this investment is not just job creation and infrastructure update but also to make Americans more comfortable with passenger rail travel.
The flagship project among these is a high-speed rail from Vegas to LA, reaching speeds of 220 miles per hour and reducing travel time to about two hours by 2028.
This initiative is part of Biden's efforts to boost the economy, create jobs, and modernize American infrastructure.
The focus on the LA to Vegas route is strategic because it's a frequently taken trip, allowing people to become more accustomed to using passenger rail.
Biden's visit to Vegas aimed to promote these infrastructure projects and the economic benefits they bring, although overshadowed by local events.
Passenger rail could be a sustainable way to transport people in the US, especially as technology advances and interest in rail travel grows.
Increasing familiarity with passenger rail may lead to more sustainable projects that utilize technologies common in other countries.

Actions:

for americans,
Support and advocate for the development of passenger rail projects in your local area (implied).
Use passenger rail services whenever possible to familiarize yourself with this mode of transportation and support its growth (implied).
</details>
<details>
<summary>
2023-12-09: Let's talk about Kenneth Chesebro nationwide.... (<a href="https://youtube.com/watch?v=2HhW2D5jh7c">watch</a> || <a href="/videos/2023/12/09/Lets_talk_about_Kenneth_Chesebro_nationwide">transcript &amp; editable summary</a>)

Beau delves into Kenneth Chesbro's cooperation in various states and hints at potential repercussions for Trump's case and the Republican Party's future.

</summary>

"It seems incredibly unlikely that none of these cases move to the point of closure with a whole bunch of national attention."
"This is pretty devastating to Trump's case overall."
"It's worth remembering that there may be other people doing the same thing, just not as high-profile."
"At some point, the Republican Party is going to have to deal with it."
"There will be information that shows up that is damaging to the former president."

### AI summary (High error rate! Edit errors on video page)

Addressing a question about Kenneth Chesbro and his cooperation in different states.
Disputing between Georgia and Michigan versus Georgia and Wisconsin cooperation.
Chesbro reportedly cooperating in Georgia, Wisconsin, Michigan, Nevada, and Arizona.
Speculation about Chesbro being an unindicted co-conspirator in a federal case.
Mentioning the separate evidence and witnesses being gathered for each state case.
Suggesting the possibility of these state cases contributing to a federal case in the future.
Implying a potential follow-on federal case involving others besides Trump.
Predicting the eventual acknowledgment by the Republican Party of these events.
Stressing the inevitability of closure on these cases regardless of delay.
Concluding that Chesbro is cooperating based on reported evidence.

Actions:

for legal analysts,
Contact state investigators for updates on the cases (implied)
Monitor media coverage for developments in the investigations (implied)
Stay informed about the legal proceedings and potential implications (implied)
</details>
<details>
<summary>
2023-12-09: Let's talk about Hunter Biden, taxes, and catching the car.... (<a href="https://youtube.com/watch?v=AhfqhLD42NQ">watch</a> || <a href="/videos/2023/12/09/Lets_talk_about_Hunter_Biden_taxes_and_catching_the_car">transcript &amp; editable summary</a>)

Hunter Biden's tax charges shake up the Republican Party's narrative, revealing the value of wild talking points and potential political consequences.

</summary>

"The base demands you act on your wild talking points."
"The big claim of the former president, is that, oh, it's all political, it's a two-tier system."
"Tax charges, like they're a big deal."
"It seems problematic to suggest that the Biden DOJ is there to protect Biden."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Hunter Biden has been charged with tax-related allegations.
The Republican Party is celebrating this, but there is no link to President Biden in the allegations.
Over the years, the Republican Party has embraced wild talking points that their base bought into.
The base often doesn't understand that the value lies in the talking point, not necessarily in taking action on it.
Hunter Biden was more valuable to the Republican Party when he wasn't charged.
The idea of a two-tier system falls apart with Hunter Biden being charged.
This development might have cost the Republican Party one of their major talking points for the 2024 election.
The tax charges against Hunter Biden are serious and could have political implications.
There might be an attempt for a plea deal in this case.
The Republican Party's political considerations regarding this situation may not have been thoroughly thought through.

Actions:

for political enthusiasts,
Analyze the political implications of Hunter Biden's tax charges (implied).
</details>
<details>
<summary>
2023-12-08: Let's talk about a totally surprising blurred footage development.... (<a href="https://youtube.com/watch?v=4Ar4csH11mU">watch</a> || <a href="/videos/2023/12/08/Lets_talk_about_a_totally_surprising_blurred_footage_development">transcript &amp; editable summary</a>)

Speaker of the House blurs faces in footage to protect from retribution, leading to a backlash from conservatives and fueling conspiracy theories, urging the Republican Party to confront the truth to prevent negative outcomes.

</summary>

"At some point, the Republican Party is going to have to acknowledge what happened."
"The longer these theories take hold, the further people fall down these echo chambers."
"They're not deep state operatives."
"The Republican Party seems very unwilling to tell its base the truth."
"But that energy may be converted to something truly negative if the record isn't set straight."

### AI summary (High error rate! Edit errors on video page)

Speaker of the House Johnson promised to release all footage, but faces were blurred to protect individuals from retribution.
Conservatives are now turning against Johnson for blurring the faces of federal agents.
Releasing the footage was an attempt to placate conspiracy theorists, but pressure forced them to fulfill the promise.
Blurring faces has fueled another conspiracy theory about protecting undercover individuals responsible for the events on the 6th.
Republicans supporting the blurring of faces are now seen as part of the conspiracy in this theory.
The longer these conspiracy theories persist, the more individuals become isolated and susceptible to negative actions.
The Republican Party needs to acknowledge the truth about what happened on the 6th and the identities of those involved.
The danger lies in the party's reluctance to tell its base the truth, as they have fueled these conspiracy theories to maintain energy.
Failure to set the record straight could lead to the conversion of that energy into something truly negative.
Leadership of the Republican Party must eventually come clean about the events.

Actions:

for republicans, political observers,
Hold accountable leadership in the Republican Party to acknowledge and address the truth (implied).
</details>
<details>
<summary>
2023-12-08: Let's talk about a question on Ukrainian aid.... (<a href="https://youtube.com/watch?v=nz0ynXlWwMk">watch</a> || <a href="/videos/2023/12/08/Lets_talk_about_a_question_on_Ukrainian_aid">transcript &amp; editable summary</a>)

Beau clarifies misconceptions about US foreign aid by explaining how the majority of military aid money is spent domestically, supporting jobs and economic activity.

</summary>

"Do you actually picture people loading pallets of money onto C-130s and flying it over?"
"If your news outlet has led you to believe physical currency is being shipped to Ukraine, you need a different outlet."
"You may confuse a yacht for a truck."
"Morality doesn't have anything to do with foreign policy."
"You do not have the information to have an opinion as strong as the one you're holding."

### AI summary (High error rate! Edit errors on video page)

Explains how the United States provides aid to other countries, particularly focusing on military aid.
Addresses misconceptions about foreign aid, specifically the idea of physically shipping cash overseas.
Clarifies that the majority of the aid money is spent in the United States, supporting jobs and economic activity domestically.
Encourages people to understand the process of foreign aid before forming strong opinions.
Provides examples, like the case of a brother working on military equipment for another country, to illustrate how aid money circulates back into the US economy.
Points out that those who believe aid money is not benefiting the United States economically may be misinformed.
Emphasizes the importance of gathering all necessary information before developing strong opinions on complex topics like foreign aid.

Actions:

for us citizens,
Understand the process of foreign aid and how it benefits both the recipient country and the US (implied).
Educate yourself on where aid money is spent and how it contributes to the domestic economy (implied).
Fact-check information about foreign aid to ensure accurate understanding (implied).
</details>
<details>
<summary>
2023-12-08: Let's talk about Trump's new filing in DC.... (<a href="https://youtube.com/watch?v=lafmIvVreTg">watch</a> || <a href="/videos/2023/12/08/Lets_talk_about_Trump_s_new_filing_in_DC">transcript &amp; editable summary</a>)

Trump filed a notice to appeal on presidential immunity, attempting to stop proceedings unilaterally, prompting likely swift court response.

</summary>

"Trump will proceed based on that understanding and the authorities set forth herein absent further order from the court."
"The judge may have some very direct words about how things work in her courtroom."
"I'm sure we'll have an update on this relatively soon."

### AI summary (High error rate! Edit errors on video page)

Trump filed a notice to appeal regarding presidential immunity in a D.C. federal case on election interference.
Trump's team filed a motion to stay to halt all proceedings in the case until further notice from the court.
Trump intends to act as if the case has paused until he hears from the court, which is an unusual approach.
Typically, when a motion is filed, one waits for a response from the judge before proceeding based on that understanding.
The court is likely to respond to Trump's preemptive actions, as it deviates from standard legal procedures.
The judge may have direct words about this unusual course of action by Trump.
It will be interesting to see how the court handles Trump's attempt to stop the proceedings unilaterally.
The judge is unlikely to allow much time to pass while Trump assumes everything has halted based on his say-so.
Expect an update on this situation soon.
Stay tuned for further developments on this legal maneuver by Trump.

Actions:

for court watchers,
Watch for updates on the legal proceedings involving Trump's attempt to halt the case (suggested)
Stay informed about the outcome of Trump's motion to stay (suggested)
</details>
<details>
<summary>
2023-12-08: Let's talk about Trump's gag order being mostly upheld.... (<a href="https://youtube.com/watch?v=anZWSPrbaJY">watch</a> || <a href="/videos/2023/12/08/Lets_talk_about_Trump_s_gag_order_being_mostly_upheld">transcript &amp; editable summary</a>)

Beau explains the appellate decision against Trump, maintaining the majority of the gag order due to its threat to trial proceedings.

</summary>

"Trump is back under a gag order for the DC federal case, the election case."
"Appeals court notes Trump's speech poses a significant threat to the criminal trial process."

### AI summary (High error rate! Edit errors on video page)

Explains the appellate decision regarding Trump and the gag order in the DC case.
Majority of the gag order against Trump stays in effect, with some slight changes.
Trump can't talk about judges, prosecutors, potential witnesses, or their families, except for special counsel Jack Smith.
Appeals court notes Trump's speech poses a significant threat to the criminal trial process.
Violations prior to the appeal may be more likely to face consequences now.
The court acknowledges public interest in what Trump has to say but also in protecting trial proceedings.
Trump is back under a gag order for the DC federal case.

Actions:

for informative citizens.,
Stay informed on legal proceedings related to Trump's cases (implied).
</details>
<details>
<summary>
2023-12-07: The Roads to Aid and Foreign Policy Dynamics (<a href="https://youtube.com/watch?v=yvKMbZGbH7U">watch</a> || <a href="/videos/2023/12/07/The_Roads_to_Aid_and_Foreign_Policy_Dynamics">transcript &amp; editable summary</a>)

Beau explains the intricate power dynamics in Middle Eastern foreign policy, stressing negotiations over military solutions, and dispels the notion of simple solutions.

</summary>

"Countries don't have friends, they have interests."
"There is no decisive victory to be had. This ends at a negotiation table."
"The good guy, whoever you think the good guy is, they don't always win."
"There aren't any simple solutions here."
"Having the right information will make all the difference."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of understanding foreign policy dynamics in the Middle East and why the US won't cut aid to certain countries.
Emphasizes that foreign policy is about power, not morality or ethics.
Breaks down the dynamics between Israel, Saudi Arabia, and Iran in relation to aid from the US.
Addresses the potential consequences of cutting aid and the intricate relationships between nations.
Argues that cutting aid won't necessarily lead to significant changes in conflicts.
Points out the complex nature of the situation and the lack of simple solutions.
Stresses the need for negotiations rather than military solutions in resolving conflicts.
Concludes that both sides in a conflict may not be satisfied with the outcome of negotiations.
Promises a future video covering similar topics in a fictitious region to provide more context.

Actions:

for policy enthusiasts,
Analyze and understand the complex power dynamics in international affairs (implied).
Support diplomatic negotiations over military interventions (implied).
Seek out diverse perspectives on foreign policy matters (implied).
</details>
<details>
<summary>
2023-12-07: Let's talk about the UN applying pressure.... (<a href="https://youtube.com/watch?v=6a4FWnxYVRE">watch</a> || <a href="/videos/2023/12/07/Lets_talk_about_the_UN_applying_pressure">transcript &amp; editable summary</a>)

The UN chief's invocation of Article 99 regarding Palestine and Gaza signals a potential humanitarian crisis, urging the Security Council to act, but the outcome remains uncertain.

</summary>

"Invoking Article 99 is kind of like hitting the panic button."
"I am hopeful that the Security Council starts to take it a little bit more seriously."

### AI summary (High error rate! Edit errors on video page)

The chief of the UN invoked Article 99, signaling a humanitarian catastrophe in Palestine and Gaza.
Article 99 is a symbolic call for the UN Security Council to address the situation but doesn't force action.
The hope is for the Security Council to declare the situation out of hand and call for a ceasefire.
The US, a Security Council member, is taking a harder line, making the outcome uncertain.
Israeli officials describe recent fighting as the most intense since the ground offensive began.
Israel's push south may face challenges as their timeline differs from expectations.
There's uncertainty whether the US will veto actions against Israel.
Despite conventional wisdom, there's potential for a different outcome based on current variables.
Hope remains for a more serious approach from the Security Council.
Overall, invoking Article 99 is mainly symbolic and may not lead to concrete results.

Actions:

for global citizens,
Contact local representatives to urge them to push for a ceasefire and humanitarian aid (implied)
Organize or participate in peaceful protests to raise awareness and demand action from governments (implied)
</details>
<details>
<summary>
2023-12-07: Let's talk about the GOP and Ukraine.... (<a href="https://youtube.com/watch?v=R8iEaPwc_SI">watch</a> || <a href="/videos/2023/12/07/Lets_talk_about_the_GOP_and_Ukraine">transcript &amp; editable summary</a>)

Beau explains the Republican Party's prioritization of social media popularity over U.S. national security through their stance on funding Ukraine.

</summary>

"The Republican Party is actively opposed to this, making sure that Ukraine doesn't have what it needs."
"The Republican Party is selling out Ukraine for Twitter likes."
"If Russia fails in Ukraine it undermines their talking points and they don't want that."
"The odds are that the Democratic Party will give the Republican Party a lot of what it wants to get this through."
"It is also worth noting that as the Republican Party sits there and talks about how they need to handle things overseas..."

### AI summary (High error rate! Edit errors on video page)

Explains the Republican Party's stance on funding Ukraine and its implications for U.S. foreign policy.
Points out the inconsistency in the Republican Party's stated position and their actions regarding Ukraine.
Accuses the Republican Party of prioritizing Twitter likes over U.S. national security by opposing aid to Ukraine.
Warns that if Ukraine is not supported by the West, it will fail against Russia, leading to a potential new Cold War.
Criticizes the Republican Party for prioritizing social media popularity over national security interests.
Notes that the Democratic Party may cater to the Republican Party's demands but questions their motivations.
Calls out the Republican Party for advocating strength in foreign policy while overlooking Russian soldiers indicted for war crimes against Americans.

Actions:

for political observers,
Contact your representatives to express support for funding Ukraine (implied).
Keep informed about foreign policy decisions and their implications (implied).
</details>
<details>
<summary>
2023-12-07: Let's talk about Rudy in Georgia.... (<a href="https://youtube.com/watch?v=jhJ2mnHWTBI">watch</a> || <a href="/videos/2023/12/07/Lets_talk_about_Rudy_in_Georgia">transcript &amp; editable summary</a>)

Rudy Giuliani faces a defamation lawsuit in Georgia, with a jury trial set to determine the substantial financial repercussions of his actions.

</summary>

"The only issue remaining in this trial will be for a jury to determine how much Defendant Giuliani owes to the plaintiffs for the damage his conduct caused."
"Giuliani's position that the long-standing jury demand in this case was extinguished when he was found liable on plaintiff's claims by default, is wrong as a matter of law."

### AI summary (High error rate! Edit errors on video page)

Rudy Giuliani and Georgia, along with Ruby Freeman, are in the spotlight.
Freeman and her daughter are suing Giuliani for defamation, with a jury trial starting on December 11th.
Giuliani attempted to change the trial from a jury trial to a bench trial, but the judge dismissed his attempt.
The district court judge called one of Giuliani's claims "simply nonsense."
The case is moving forward, and the jury will determine the amount Giuliani owes the plaintiffs for the damage caused by his conduct.
Speculations suggest Giuliani may have to pay tens of millions of dollars, possibly even close to 40 million.
Giuliani's financial situation is rumored to be precarious, making it doubtful if he can afford the potential payout.
Despite Giuliani's attempts to alter the dynamics of the trial, it seems that the case will proceed as planned with the jury deciding the amount he owes.
The trial is expected to begin in a few days, with updates to follow as it progresses.
The key takeaway is that the trial is imminent.

Actions:

for legal observers,
Stay informed about the developments in the trial and its outcomes (suggested).
</details>
<details>
<summary>
2023-12-07: Let's talk about Nevada, Trump, and electors.... (<a href="https://youtube.com/watch?v=BPbEoLjHr9A">watch</a> || <a href="/videos/2023/12/07/Lets_talk_about_Nevada_Trump_and_electors">transcript &amp; editable summary</a>)

Nevada joins Michigan and Georgia in charging Republican leaders for fake electors, exposing consistent evidence across jurisdictions and weakening politically motivated narratives.

</summary>

"At some point, it's going to have to be acknowledged."
"It's the same type of evidence in each jurisdiction."
"The framing though of this being politically motivated, it's starting to fall apart."

### AI summary (High error rate! Edit errors on video page)

Nevada charges Republican leaders in relation to fake electors, similar to Michigan and Georgia.
Charges include felonies like offering false/forged instruments for filing.
Prosecution gained steam with cooperation of witness Kenneth Cheesebrough from Georgia.
Multiple jurisdictions, prosecutors, and grand juries all reaching the same conclusion on conduct.
Presumption of innocence for all, but evidence is consistent across cases.
Narrative of politically motivated actions starting to weaken.
Acknowledgment needed that election interference occurred to alter outcomes.
Real movement in the case expected to take time.

Actions:

for political observers,
Follow updates on legal proceedings and outcomes (suggested)
Stay informed and engaged with developments in the case (suggested)
</details>
<details>
<summary>
2023-12-06: Let's talk about footage, statements, and the Speaker.... (<a href="https://youtube.com/watch?v=bvE_vNd6OVg">watch</a> || <a href="/videos/2023/12/06/Lets_talk_about_footage_statements_and_the_Speaker">transcript &amp; editable summary</a>)

The Republican Party's attempt to control the narrative of January 6th through blurred faces exposes deeper issues of manipulation and confusion among its base.

</summary>

"It's probably a really good idea for the Republican Party to just acknowledge that yes, the people carrying the Trump banners were in fact Trump's people."
"The promise to release the footage was an effort to amp up a conspiracy theory."
"Blurring out the faces will help stop the public from identifying people."
"Acknowledging the truth about who participated in the events of January 6th could mitigate confusion and manipulation among the Republican Party's base."
"Let's be clear, there have been a number of people who were at January 6th who then ran for office. What party did they run under?"

### AI summary (High error rate! Edit errors on video page)

The Republican party aimed to release all footage from January 6th to downplay the events as a mere tourist visit, but faced backlash for blurring faces to prevent retaliation.
The Speaker of the House expressed concern about people being retaliated against if their faces were not blurred in the footage from January 6th.
The Department of Justice (DOJ) already has access to the raw footage from January 6th and has released sections to help identify individuals.
Blurring faces in public viewing room footage aims to prevent retaliation against private citizens from non-governmental actors.
The promise to release the footage was part of an effort to amplify a conspiracy theory, suggesting that the events of January 6th were fabricated.
Some individuals present at January 6th later ran for office under which party? The theory that the footage backs up will be contradicted by it.
Blurring faces is intended to prevent the public from identifying individuals in the footage, not to cover up certain groups' involvement.
Blurring faces may lead conspiracy theorists to claim that those supporting face blurring are complicit in hiding identities.
Acknowledging the truth about who participated in the events of January 6th could mitigate confusion and manipulation among the Republican Party's base.

Actions:

for politically engaged individuals,
Hold accountable political figures who perpetuate misinformation (implied)
</details>
<details>
<summary>
2023-12-06: Let's talk about Tuberville dropping his hold.... (<a href="https://youtube.com/watch?v=vq-VshH1Ah4">watch</a> || <a href="/videos/2023/12/06/Lets_talk_about_Tuberville_dropping_his_hold">transcript &amp; editable summary</a>)

Senator Tuberville's release of military promotions, except for four-star generals, reveals a disconnect between his actions and military understanding, sparking pressure for Congress to intervene.

</summary>

"He perceives them to be of higher value and therefore maintains leverage."
"There's a disconnect between Tuberville's actions and his understanding of the military."
"It's certainly annoying for the people who are being directly impacted and how it impacts their career."
"He might have worked out some kind of a deal so he can maintain his hold for an extra month or whatever."
"I do not think that the majority of people are going to fall for this stunt though."

### AI summary (High error rate! Edit errors on video page)

Senator Tuberville released the hold on promotions for military officers, except for four-star generals.
Tuberville perceived four-star generals to be of higher value, hence retaining leverage on them.
There's a disconnect between Tuberville's actions and his understanding of the military.
Pressure will mount on Congress to override Tuberville regarding the promotions of four-star generals.
Acting positions are currently filled due to the delay impacting officers' careers.
Other members of Congress may advocate for national security to override Tuberville's hold on promotions.
Tuberville might be trying to maintain his hold for longer to appear favorable before releasing it.
Beau doubts that most people will fall for Tuberville's tactics.

Actions:

for military members, congress,
Contact your representatives to advocate for the timely promotions of military officers (implied)
Monitor the situation closely and be ready to support actions that prioritize national security and military careers (implied)
</details>
<details>
<summary>
2023-12-06: Let's talk about Rocha, Cuba, the US, and it being big.... (<a href="https://youtube.com/watch?v=lnvxjtvhO38">watch</a> || <a href="/videos/2023/12/06/Lets_talk_about_Rocha_Cuba_the_US_and_it_being_big">transcript &amp; editable summary</a>)

The alleged infiltration of the US government by Cuban intelligence prompts a reevaluation of the US-Cuban relationship rooted in Cold War dynamics, signaling potential changes ahead.

</summary>

"I'm going to suggest that the longest running alleged infiltration of the US government that's known is kind of a big deal."
"They're probably going to take that as a sign that some changes need to be made."
"It's not like Cuba is an actual threat to the United States anymore."
"This might be the start of that relationship being re-examined."
"Definitely not a minor thing."

### AI summary (High error rate! Edit errors on video page)

The case of Mr. Rocha sheds light on the alleged infiltration of the US government by Cuban intelligence over 40 years.
U.S. counterintelligence posed as Cuban intelligence to meet with Rocha, who allegedly affirmed his allegiance to the Cuban cause.
The US-Cuban relationship, rooted in Cold War dynamics, will likely undergo changes due to this revelation.
Cuba's historical association with the Soviet Union contributes to its current perception in US foreign policy circles.
The incident may prompt a reevaluation of the outdated engagement methods with Cuba, leading to potential changes in the relationship.

Actions:

for foreign policy analysts,
Reassess engagement methods with Cuba (implied)
Stay informed about developments in US-Cuban relations (implied)
</details>
<details>
<summary>
2023-12-06: Let's talk about McCarthy retiring and what it means.... (<a href="https://youtube.com/watch?v=4V4fSpAy10I">watch</a> || <a href="/videos/2023/12/06/Lets_talk_about_McCarthy_retiring_and_what_it_means">transcript &amp; editable summary</a>)

Kevin McCarthy's departure signals a shift in Republican Party dynamics, with social media engagement favored over traditional fundraising, potentially impacting House dynamics and upcoming votes.

</summary>

"McCarthy was kind of saying, hey, even though I'm not speaker, I'm your cash cow."
"He views their behavior as the reason for the Republican Party being in the state that it is in."
"McCarthy saying, I'm done."
"The Republican Party chose social media engagement over old school politics."
"It is unlikely that the Republican Party is going to be able to advance much of what it wants to in the House."

### AI summary (High error rate! Edit errors on video page)

Kevin McCarthy, former Speaker of the House, is leaving Congress before finishing his term, signaling a significant decision.
McCarthy's announcement of leaving seemed pre-planned, as he indicated he was waiting until after the holidays to decide, but announced his departure before.
It appears McCarthy was signaling to the Republican Party, suggesting that he is a key figure for fundraising and expects accountability for those causing disarray in the party.
McCarthy seems to blame a group he calls the "crazy eight" for his ousting and believes their actions led to the state of the Republican Party.
While McCarthy may share some blame for making promises he couldn't keep in his pursuit of becoming speaker, his public announcement signifies a clear departure from an active role in fundraising for the party.
The Republican Party appears to have chosen social media engagement over traditional fundraising, leading to McCarthy's decision to step back.
McCarthy's departure will upset the balance of power in the House and may result in tight votes next year, with a special election for his seat months away.
The Republican Party's shrinking majority in the House may hinder its ability to advance its agenda in the upcoming election year.

Actions:

for political observers and republican party members,
Monitor developments in the Republican Party and their strategies for engagement (implied)
Stay informed about the upcoming special election for Kevin McCarthy's seat (implied)
</details>
<details>
<summary>
2023-12-05: Let's talk about the latest US pressure.... (<a href="https://youtube.com/watch?v=iyL5w9OL9Lo">watch</a> || <a href="/videos/2023/12/05/Lets_talk_about_the_latest_US_pressure">transcript &amp; editable summary</a>)

The U.S. sanctions Israeli citizens involved in West Bank violence as a diplomatic warning, signaling gradual pressure without immediate major consequences.

</summary>

"This is a warning."
"It's pretty narrowly tailored to be a warning."
"Still very much in the warning phase."
"This is movement on the diplomatic front."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The United States announced sanctions on Israeli citizens involved in violence in the West Bank.
Those sanctioned individuals will be barred from entering the United States, while their family members may face increased scrutiny.
This move serves as a warning to Israel about the need for caution in the region.
The U.S. is focused on preventing escalation of violence in the Middle East, particularly in the West Bank.
The sanctions are not meant to be widespread or highly impactful but are designed to send a clear signal.
The U.S. may potentially escalate to more targeted economic sanctions on individuals.
This warning is one of the most stringent messages sent by the U.S. to Israel since the 90s.
However, this action does not signify a major shift in the U.S.-Israel relationship.
Israel was likely aware of this move beforehand, potentially lessening its impact on their behavior.
The current situation is more about diplomatic posturing and warning than significant consequences.
The U.S. is trying to exert pressure while maintaining a delicate balance in its approach.
This move does not indicate an immediate severance of relations between the U.S. and Israel.
It's a diplomatic step with limited implications for now, signaling a cautious approach in foreign policy.
While there may be further actions, they are unlikely to drastically alter the current dynamics.
Overall, this move is part of a gradual process and not a sudden, drastic change in relations.

Actions:

for diplomatic observers, policymakers,
Monitor diplomatic developments and potential escalations in the Middle East (implied).
Stay informed about foreign policy decisions and their implications (implied).
</details>
<details>
<summary>
2023-12-05: Let's talk about the US, moral responsibility, and strategic defeat.... (<a href="https://youtube.com/watch?v=ZpzWhyyzou4">watch</a> || <a href="/videos/2023/12/05/Lets_talk_about_the_US_moral_responsibility_and_strategic_defeat">transcript &amp; editable summary</a>)

Analyzing Austin's speech on protecting Palestinian civilians reveals the importance of civilian safety in urban warfare and the strategic implications for Israel and the US.

</summary>

"The lesson is not that you can win in urban warfare by protecting civilians. The lesson is that you can only win in urban warfare by protecting civilians."
"Austin knows what he's talking about."
"It's not unknown."
"The US believes the conflict is going."
"So even if Israel was successful in rooting out the current organization, another one's going to spring up."

### AI summary (High error rate! Edit errors on video page)

Analyzing Austin's speech and the widely circulated quote about protecting Palestinian civilians in Gaza.
Emphasizing the importance of context in understanding the key points of the speech.
Explaining the difference between tactical victory and strategic defeat in warfare scenarios.
Connecting the strategy of protecting civilians to winning in urban warfare.
Stating that protecting civilians is vital to preventing civilians from sympathizing with the opposition.
Addressing the pressure the US has put on Israel regarding civilian casualties.
Suggesting that the US believes Israel has not effectively protected civilians in the conflict.
Pointing out the diplomatic challenges the US faces as Israel is an ally.
Mentioning the predictability of another organization rising even if the current one is rooted out.
Recognizing Austin's expertise and experience in defense matters.

Actions:

for policy analysts, activists,
Advocate for policies that prioritize civilian protection in conflict zones (implied)
Support humanitarian aid efforts in conflict-affected areas (implied)
Stay informed and engaged with diplomatic efforts to address civilian casualties in conflicts (implied)
</details>
<details>
<summary>
2023-12-05: Let's talk about the DOD IG report.... (<a href="https://youtube.com/watch?v=gUDLvCLJXdM">watch</a> || <a href="/videos/2023/12/05/Lets_talk_about_the_DOD_IG_report">transcript &amp; editable summary</a>)

Department of Defense identifies extremists and gang affiliations in the military, with chain of command taking serious actions on substantiated cases while recruitment screening remains a weak link.

</summary>

"Department of Defense instituted a program to keep extremists out of the military."
"Every single instance where it was substantiated, it was acted upon."
"The Army had the most numbers, raw numbers, but Space Force had the lowest."

### AI summary (High error rate! Edit errors on video page)

Department of Defense instituted a program to keep extremists out of the military, leading to 183 extremists identified in the past year.
In addition to extremists, 58 service members were identified with gang affiliations.
Out of the 183 identified extremists, 68 cases were cleared or unsubstantiated.
Chain of command acted in every substantiated case, ranging from discharge to court-martial, demonstrating seriousness in addressing extremism.
Weak link found in recruitment screening process, where steps were not always followed, potentially due to laxity from struggling to meet recruitment goals.
Despite recruitment screening issues, the program appears to be functioning well overall.
Programs initiated by the Secretariat of Defense, which some deemed unnecessary, have resulted in high numbers of extremists being identified.
Army had the highest numbers of identified extremists, while Space Force had the lowest.
Screening process may be improved by rejecting candidates without proper screening in place, potentially increasing recruiter diligence.
Overwhelming majority of substantiated cases resulted in significant punishments, with only three receiving counseling.

Actions:

for military personnel, policymakers,
Reject candidates without proper screening (implied)
Increase recruiter diligence in screening processes (implied)
</details>
<details>
<summary>
2023-12-05: Let's talk about COP28, science, statements, and walkbacks.... (<a href="https://youtube.com/watch?v=2IOcshVryPA">watch</a> || <a href="/videos/2023/12/05/Lets_talk_about_COP28_science_statements_and_walkbacks">transcript &amp; editable summary</a>)

Criticism over COP28's host, doubts on fossil fuel phase-out, and lack of tangible results question the summit's credibility and impact on climate goals.

</summary>

"There is obviously going to be reluctance to the phase out of fossil fuels from people who, well, are CEOs of oil companies."
"This may be the turning point for whether or not people put any faith in the ability of the UN climate summits to produce anything worthwhile."

### AI summary (High error rate! Edit errors on video page)

COP28, a climate summit of world leaders, faced criticism due to its CEO host being from an oil company.
Criticism arose when the host stated that a phase-out of fossil fuels may not achieve the 1.5-degree goal.
Doubts increased among skeptics of the summit's credibility.
Concerns were raised about the lack of a roadmap for sustainable socio-economic development post-fossil fuel phase-out.
Biden's absence from the summit was interpreted differently, with varying messages sent.
The CEO later attempted to clarify his statements, mentioning the inevitability and importance of phasing out fossil fuels.
The overall reputation of COP28 was significantly damaged, especially amidst existing scrutiny and lack of tangible results.
The conference's outcomes have been questioned, with voluntary pledges and unmet goals being prominent.
The lack of concrete results may lead to a shift in approach towards future climate summits.
The incident underscores the reluctance of oil company CEOs towards phasing out fossil fuels.

Actions:

for climate activists, policymakers,
Contact climate organizations for updates and ways to support climate goals (implied)
Join local climate action groups to push for tangible outcomes from climate summits (implied)
</details>
<details>
<summary>
2023-12-04: Let's talk about a US diplomat getting arrested.... (<a href="https://youtube.com/watch?v=cSU8ShRGZiM">watch</a> || <a href="/videos/2023/12/04/Lets_talk_about_a_US_diplomat_getting_arrested">transcript &amp; editable summary</a>)

The FBI arrested Manuel Rocha, a former U.S. ambassador, for acting as an agent of Cuba, stirring uncertainty about the possible outcomes of this diplomatic incident.

</summary>

"Acting as an agent can mean a whole lot of things. Not all of it is like spy stuff."
"It seems very unlikely that this is anywhere in between those. It's going to be something that's either going to seem like kind of a letdown in the story or it's going to be a big deal."
"My understanding is that his court appearance is today and generally speaking a little bit more information becomes available at that point."
"What is actually known, it can all be in the headline."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The FBI arrested Manuel Rocha for acting as an agent of Cuba, a former U.S. ambassador with 25 years of diplomatic service during the Cold War.
Acting as an agent can range from simple requests for help to more serious espionage activities, with potential unknown implications.
There is uncertainty whether the situation will turn out to be a major story or a minor issue, depending on the specifics of the allegations.
Rocha's court appearance today might shed more light on the situation, but for now, most information is speculative.
Given Rocha's extensive experience in diplomatic efforts, the outcome is expected to be either a minor mistake or a significant breach, with little room for something in between.
The media coverage is disproportionate to the actual known facts, with a lot of speculation surrounding the case.
Rocha's background suggests that any error on his part is likely to be substantial rather than minor due to his deep understanding of diplomatic rules.

Actions:

for diplomatic observers,
Stay informed about updates on the case (implied).
Avoid jumping to conclusions and wait for verified information to surface (implied).
</details>
<details>
<summary>
2023-12-04: Let's talk about Russia, clubs, and the future of the US.... (<a href="https://youtube.com/watch?v=pF8do4irdEo">watch</a> || <a href="/videos/2023/12/04/Lets_talk_about_Russia_clubs_and_the_future_of_the_US">transcript &amp; editable summary</a>)

Russia's anti-LGBTQ progression warns Americans of authoritarian measures and the dangerous implications of supporting such actions.

</summary>

"When masked, security services are photographing your documents, generally speaking, that's a bad sign for your future."
"It's not gonna stop with our community. They're just the test group."
"If you're cheering this on because you are not part of this group, you have to understand that once a government gets power like this, it's really hard to get it back."

### AI summary (High error rate! Edit errors on video page)

Russia's Supreme Court labeled the LGBTQ+ community as an extremist movement, leading to security services raiding LGBTQ+ establishments in Moscow.
Putin has long promoted "traditional family values" and passed anti-LGBTQ laws like the gay propaganda law in 2013.
The quick progression from anti-LGBTQ laws to security crackdowns in Moscow serves as a warning for Americans, as we are about 10 years behind Russia in this regard.
The targeting of the LGBTQ+ community is often a testing ground for authoritarian regimes that may eventually impact other groups.
Americans cheering on authoritarian actions are unknowingly setting themselves up to be the next target of oppressive measures.

Actions:

for aware citizens,
Reach out to LGBTQ+ organizations for support and guidance (suggested)
Advocate for LGBTQ+ rights and protections in your community (implied)
Stand in solidarity with marginalized groups facing oppression (implied)
</details>
<details>
<summary>
2023-12-04: Let's talk about Georgia, conditions, and social media.... (<a href="https://youtube.com/watch?v=GIUpVVET3aw">watch</a> || <a href="/videos/2023/12/04/Lets_talk_about_Georgia_conditions_and_social_media">transcript &amp; editable summary</a>)

Talking legal entanglements in Georgia, Trump's co-defendant risks case integrity by discussing it on social media, facing potential courtroom repercussions soon.

</summary>

"There's a woman sitting somewhere who knows I'm going to mess her whole life up when this is done."
"In most courtrooms, a judge would describe this in technical legal terms as totally uncool."
"It really can't be compared to Trump. This is a little different."
"The habit of those associated with Trump when it comes to airing their grievances on social media, particularly about legal issues."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talking about legal entanglements related to Georgia and social media posts.
Focus on Trump's co-defendant, Trevegan Cuddy, violating release conditions by discussing the case on social media.
Mention of an Instagram Live where Cuddy hinted at revealing damaging information about a potential witness.
Likelihood of the discussed content being used in court proceedings.
Comparison between Cuddy's actions and those associated with Trump in airing grievances online.
Speculation on the legal consequences and potential impact on the ongoing case.
Mention of offers made by the district attorney's office to witnesses for cooperation.
Anticipation of updates on the case soon.

Actions:

for legal observers,
Monitor updates on the legal case and proceedings (implied).
</details>
<details>
<summary>
2023-12-04: Let's talk about Artemis 3, delays, and schedules.... (<a href="https://youtube.com/watch?v=LzjZ8AIxqGw">watch</a> || <a href="/videos/2023/12/04/Lets_talk_about_Artemis_3_delays_and_schedules">transcript &amp; editable summary</a>)

NASA's Artemis project faces delays due to setbacks in the Human Landing System (HLS) development, potentially pushing the mission to early 2027, prolonging the wait for a return to the moon.

</summary>

"Artemis III will be the first crewed craft going where, well, actually somebody has gone before."
"It looks like it's going to be a wee bit longer before we actually see another person on the moon."

### AI summary (High error rate! Edit errors on video page)

NASA's Artemis project aims to put somebody back on the moon, with Artemis III being the first crewed craft scheduled for late 2025.
A recent GAO report indicates a potential delay, with Artemis III likely to occur in early 2027 instead of the initially planned late 2025.
The main delay is attributed to the Human Landing System (HLS) by SpaceX, moving slower than expected in its development.
The report suggests that if HLS development follows the average timeline of NASA's major projects, Artemis III will be pushed back to 2027.
The HLS system didn't meet several key milestones, leading to delays in the Artemis mission.
Issues also arise with the development of suits by a company called Axiom, with NASA requesting additional emergency life support integration.
The delay in the Artemis mission means it will take longer before another person steps foot on the moon.

Actions:

for space enthusiasts,
Stay updated on NASA's Artemis project progress and advocacy efforts to support timely developments (implied).
Engage with space-related communities to stay informed and potentially contribute to advancements in space exploration (implied).
</details>
<details>
<summary>
2023-12-03: The Roads Not Taken EP16 (<a href="https://youtube.com/watch?v=eMrhJegs7r8">watch</a> || <a href="/videos/2023/12/03/The_Roads_Not_Taken_EP16">transcript &amp; editable summary</a>)

Beau covers a range of global events, from EU-China tensions to domestic news like Trump's comments, addressing viewer questions on voting and emergency responses.

</summary>

"If every presidential race is a vote to save democracy, doesn't that mean democracy is already toast?"
"Democracy is advanced citizenship."
"Closing his mind to the right will open it to the left."

### AI summary (High error rate! Edit errors on video page)

Overview of world events, unreported news, and interesting tidbits from the previous week.
EU pressuring China over support of Russia to evade sanctions.
Russia using "active defense" instead of "offensive" terminology.
Tensions between India and the United States due to a hit-for-hire plot.
Trump making controversial statements about American democracy.
Various news snippets: pro-Palestinian demonstrator, Olympic gold medalist avoiding prison, inmate stabbing Chauvin, Democrats launching super PAC in New Hampshire, Sandra Day O'Connor's passing, Texas judge releasing law enforcement records.
MAGA potentially boycotting Walmart over pride support.
Elon Musk's response to advertisers blackmailing him.
COP28 agreement on methane criticized as greenwashing.
Nicaragua accuses Miss Nicaragua pageant director of rigging contest to overthrow government.
Addressing viewer questions on voting, Trump testifying, G.I. Joe shirt, and Project 2025 concerns.
Handling panic in emergency situations and dealing with vehicle accidents.
Managing disagreements between strong-willed individuals.
Utility worker's perspective on lead service line rule and inventory creation.

Actions:

for viewers interested in global events and political analysis.,
Take a wilderness first responder course to be better prepared for emergency situations (implied).
Research and enroll in online first aid courses or Stop the Bleed courses for basic medical training (implied).
Stay informed about Project 2025 and potential implications for the future (suggested).
Start difficult but necessary conversations with individuals holding opposing political views (implied).
Proactively address potential safety concerns in your community, such as lead service line issues (implied).
</details>
<details>
<summary>
2023-12-03: Let's talk about the parting gift of Santos.... (<a href="https://youtube.com/watch?v=xAA-5tIZNrc">watch</a> || <a href="/videos/2023/12/03/Lets_talk_about_the_parting_gift_of_Santos">transcript &amp; editable summary</a>)

Beau talks about Santos's impact on the Republican Party, his specific allegations against colleagues, and the potential political fallout, hinting that the saga is far from over.

</summary>

"Santos is not just going to go away, which is what I think most Republicans were hoping for."
"Those people who are primed to believe in conspiracy theories by the Republican Party, they were kind of encouraged to do so."
"I don't think we have seen the final chapter yet in the Santos saga."

### AI summary (High error rate! Edit errors on video page)

Talking about Santos and his impact on the Republican Party.
Santos has been expelled but plans to file official complaints with the Ethics Committee.
Allegations against colleagues from New York and New Jersey, including specific accusations.
Santos' history makes Beau hesitant to immediately look into the allegations.
The allegations range from improper stock trading to money laundering.
It's unclear whether the complaints will be filed and if they hold substance.
Santos is not fading away, indicating a significant media profile.
Republicans are now stuck with addressing Santos due to their delayed response.
Beau hints at a lesson not learned from a past politician's situation.
The potential impact of the allegations on those inclined to believe conspiracy theories.
Regardless of the truth, the statement could harm those involved politically.
The saga of Santos is far from over, with more to unfold.
Beau refrains from detailing the allegations tied to specific names.
The influence of Santos' statement on political perceptions.
The uncertainty on how the situation will unfold.

Actions:

for political observers,
Stay informed about updates regarding Santos and his allegations (implied).
</details>
<details>
<summary>
2023-12-03: Let's talk about the negotiations breaking down.... (<a href="https://youtube.com/watch?v=cAmyvvRwcYs">watch</a> || <a href="/videos/2023/12/03/Lets_talk_about_the_negotiations_breaking_down">transcript &amp; editable summary</a>)

Secretary Blinken and Israeli negotiators leave amid a breakdown, commitments made but skepticism lingers, and U.S. diplomacy persists despite frustrations, signaling a pause with an uncertain timeline for returning to negotiations.

</summary>

"It's not good."
"They're going to have to go back to the table eventually."
"It could be a week, it could be much longer."

### AI summary (High error rate! Edit errors on video page)

Secretary of State Blinken and the Israeli negotiating team are leaving, signaling a setback in negotiations.
Modern technology allows for negotiations to continue remotely, even though the teams are packing up.
Blinken mentioned that Hamas violated agreements, leading to the negotiation breakdown.
The Israeli government made commitments to enhance civilian protection measures, but skepticism remains about their implementation.
Blinken acknowledged Israel's movements in the south but hadn't caught up on recent events.
U.S. diplomacy plans to persist despite the current breakdown in negotiations.
Typically, once negotiations hit a wall, it takes a substantial amount of time before parties return to the table.
The United States aims to work with partners to secure the release of captives.
The current frustration has led to a pause in negotiations, but eventual return to the table is expected.
The timeline for resuming negotiations is uncertain, with historical precedence suggesting it won't be a swift process.

Actions:

for diplomatic observers,
Stay informed on international developments and diplomatic efforts (suggested)
Support initiatives working towards peaceful resolutions (implied)
</details>
<details>
<summary>
2023-12-03: Let's talk about the evolving US position and changing relationships.... (<a href="https://youtube.com/watch?v=aRLMiKavY9s">watch</a> || <a href="/videos/2023/12/03/Lets_talk_about_the_evolving_US_position_and_changing_relationships">transcript &amp; editable summary</a>)

Beau explains the shifting U.S. stance on Palestinian relocation and Gaza borders, reflecting changing relationship dynamics with Israel in international affairs.

</summary>

"Under no circumstances will the United States permit the forced relocation of Palestinians from Gaza or the West Bank."
"It's not foreign policy, it's not diplomacy, it's not how it works."
"This is probably where it's going to end."

### AI summary (High error rate! Edit errors on video page)

Talks about how relationships change over time in international affairs, especially in diplomacy.
Describes how relationships between countries shift behind the scenes first before going public.
Mentions the subtle changes in U.S. diplomacy towards Israel over the last month or so.
Indicates a significant shift in the U.S. position regarding the forced relocation of Palestinians from Gaza or the West Bank.
Mentions the public statement from the vice president's office regarding the U.S.'s stance on Palestinian relocation and Gaza borders.
Notes that the relationship shift between the United States and Israel is becoming more public due to Netanyahu's response to U.S. advice.
Explains the escalation of public statements in international affairs to generate more pressure.
States that cutting all aid to Israel is unlikely due to foreign policy implications.
Attributes the shifting relationship dynamics to changing demographics within the United States.
Emphasizes the importance of focusing on government actions to influence foreign policy outcomes.

Actions:

for international policy observers,
Contact your representatives to express your views on U.S. foreign policy regarding Israel and Palestine (suggested).
Join or support organizations advocating for peaceful resolutions in international conflicts (implied).
</details>
<details>
<summary>
2023-12-03: Let's talk about Trump, SCOTUS, and Biden staying in office.... (<a href="https://youtube.com/watch?v=p-JtdfWF79k">watch</a> || <a href="/videos/2023/12/03/Lets_talk_about_Trump_SCOTUS_and_Biden_staying_in_office">transcript &amp; editable summary</a>)

Beau explains recent rulings denying Trump presidential immunity in civil and criminal cases, casting doubt on potential Supreme Court support and warning against extending immunity indefinitely.

</summary>

"The United States has only one chief executive at a time, and that position does not confer a lifelong get-out-of-jail free pass."
"There's no reason for any president to ever willingly leave office."
"The current decisions by the court suggest that presidential immunity does not apply to Trump."
"They're basically just daring Biden to try to hold on to power."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the recent ruling on presidential immunity in a civil case involving Trump.
Mentions a subsequent ruling on Trump's immunity in criminal cases while president.
Quotes Judge Chutkin's decision that former presidents are not immune to federal criminal liability.
Speculates that Trump will likely appeal the decision.
Expresses doubts about the current Supreme Court siding with Trump on this issue.
Argues against the concept of immunity for former presidents based on actions taken while in office.
Points out the potential dangerous consequences if presidential immunity was extended indefinitely.
Believes the Supreme Court is unlikely to support Trump's argument on presidential immunity.
Cites previous instances where the court ruled against former presidents.
Concludes that current court decisions indicate no immunity for Trump in civil or criminal cases.

Actions:

for legal scholars, political analysts.,
Monitor updates on Trump's legal battles and potential appeals (suggested).
Stay informed about Supreme Court decisions and implications for presidential immunity (suggested).
</details>
<details>
<summary>
2023-12-02: Let's talk about lead, the EPA, Biden, and moving forward.... (<a href="https://youtube.com/watch?v=6h5Ao-7hQ9s">watch</a> || <a href="/videos/2023/12/02/Lets_talk_about_lead_the_EPA_Biden_and_moving_forward">transcript &amp; editable summary</a>)

The EPA proposes a rule mandating lead pipe replacement in cities, prioritizing public health over financial concerns, with funding available from the bipartisan infrastructure law.

</summary>

"No acceptable safe level of lead."
"It's better for me politically if your kids get lead poisoning."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The EPA has proposed a new rule that will require most cities in the United States to replace all lead pipes within 10 years, regardless of lead levels in the water.
The rule also aims to lower the amount of allowable lead in water systems, with a requirement to provide filters if levels exceed the limit.
Nearly a hundred thousand people in Flint were affected by lead contamination, underscoring the urgency of addressing this issue.
The ban on installing lead pipes began in the 1980s, revealing decades of neglect in fixing the problem despite knowing that no level of lead is safe.
Funding for this pipe replacement initiative is available through the bipartisan infrastructure law passed in 2021.
Public comments will be accepted for 60 days, followed by a hearing in January before the rule takes effect.
While some cities may resist the rule, the EPA and Biden administration are pushing for it, backed by available funding.
The key challenge may revolve around the financial responsibility for replacing lead pipes.
Beau speculates that unless influential politicians advocate against it, the rule is likely to move forward smoothly.
The focus should be on prioritizing public health and preventing lead poisoning, rather than financial considerations.

Actions:

for citizens, legislators, activists,
Contact local officials to advocate for prioritizing public health over financial interests (implied)
Stay informed about the progress of the rule and actively participate in public comments and hearings (implied)
</details>
<details>
<summary>
2023-12-02: Let's talk about intelligence failures in other countries.... (<a href="https://youtube.com/watch?v=Ui3SWzr7QJw">watch</a> || <a href="/videos/2023/12/02/Lets_talk_about_intelligence_failures_in_other_countries">transcript &amp; editable summary</a>)

Beau delves into Israel's intelligence failure, citing groupthink, underestimation of opposition, and political pressure as key factors in this insightful exploration of intelligence failures.

</summary>

"Groupthink, underestimation of opposition, and political pressure as key factors in Israel's case."
"Israel's intelligence failure as a case study."
"Exploring failures through a different country's lens."

### AI summary (High error rate! Edit errors on video page)

Exploring failures through a different country's lens.
Prompted by a question on intelligence failure causes.
Israel's intelligence failure as a case study.
Factors contributing to intelligence failures.
Groupthink, political pressure, opposition misjudgment, and doctrine misunderstanding.
Examples of historical lessons shaping intelligence assessments.
Importance of understanding applied doctrine over written doctrine.
Political pressure as a common factor in intelligence failures.
Lack of institutional memory affecting intelligence assessments.
Failure to challenge consensus leading to intelligence failures.
Propaganda's role in shaping perceptions of opposition capabilities.
Groupthink, underestimation of opposition, and political pressure as key factors in Israel's case.
Importance of safeguarding against failure in intelligence assessments.

Actions:

for analysts, policymakers, researchers.,
Challenge consensus in intelligence assessments (implied).
Understand applied military doctrines of opposition (implied).
</details>
<details>
<summary>
2023-12-02: Let's talk about Trump and Presidential immunity.... (<a href="https://youtube.com/watch?v=wDKKCZWQaeU">watch</a> || <a href="/videos/2023/12/02/Lets_talk_about_Trump_and_Presidential_immunity">transcript &amp; editable summary</a>)

An appeals court decision on Trump's immunity opens the door to legal challenges following January 6th events, impacting civil lawsuits and potential future legal entanglements.

</summary>

"The judges had to weigh whether or not what was happening was part of his official responsibilities."
"This isn't a situation where Trump lost these cases, it's a situation where those cases can now proceed."
"All in all, not a good decision."
"If you are on Trump's legal team, you're not happy about this."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

An appeals court decision impacts Trump's immunity in lawsuits filed by law enforcement and congressional staffers regarding January 6th events.
Trump's claim of presidential immunity was rejected because his speech inciting actions did not fall under official responsibilities.
However, his failure to act during the Capitol incident falls within his official duties and grants him immunity from that claim.
The decision allows pending lawsuits against Trump to move forward, requiring him to go to trial and contest facts.
The ruling may influence how courts view Trump's actions in other cases, but this specific case is civil, not criminal.
The decision could lead to numerous legal challenges for Trump in the DC area and potentially beyond.
Trump's legal team likely views this decision unfavorably, as it opens the door to more legal entanglements.
The only immunity granted to Trump was regarding his failure to stop the Capitol events, deemed part of his official capacity.
Trump can now be sued for his actions leading up to the Capitol incident, but he has the chance to contest the claims in court.
The decision creates uncertainties for Trump's legal future, prompting potential creativity from attorneys in pursuing legal actions.

Actions:

for legal observers, activists, concerned citizens,
Monitor the progress of the lawsuits against Trump and advocate for accountability (suggested).
Stay informed about the legal implications of the court decision and its potential ramifications (implied).
</details>
<details>
<summary>
2023-12-02: Let's talk about SCOTUS, Harlan, and the Senate.... (<a href="https://youtube.com/watch?v=8xVENApmluw">watch</a> || <a href="/videos/2023/12/02/Lets_talk_about_SCOTUS_Harlan_and_the_Senate">transcript &amp; editable summary</a>)

Beau delves into the Senate Judiciary Committee's subpoenas for individuals linked to Supreme Court justice gifts, stressing the importance of public faith in the Court's ethics and advocating for a direct approach to implementing an ethics code.

</summary>

"The legitimacy of the Supreme Court is paramount. If the American people do not believe that the Supreme Court is operating in an ethical and good faith manner, eventually the rulings will be ignored."
"If Congress wants to put forth ethics, an ethics code that the Supreme Court is supposed to follow, they need to just do it."
"The show, it doesn't matter. Put forth the code, make sure it is applied from here on out, and go from there."

### AI summary (High error rate! Edit errors on video page)

Explains the recent actions taken by the Senate Judiciary Committee regarding subpoenas for Harlan Crowe and Leonard Leo.
Mentions the partisan nature of the vote on the committee, with Democrats wanting to hear from them.
Notes that if Crowe and Leo refuse to comply with the subpoenas, it will have to go to the full Senate and require 60 votes for enforcement.
Questions the Senate Judiciary Committee's motivation for looking into the issue of ethics surrounding the Supreme Court.
Emphasizes the importance of the Supreme Court's legitimacy and the need for public faith in its ethical operations.
Suggests that if Congress wants to implement an ethics code for the Supreme Court, they should simply do so without creating a spectacle that undermines the institution.
Advocates for a straightforward approach of putting forth legislation for an ethics code instead of engaging in lengthy hearings that may further damage the Supreme Court's legitimacy.

Actions:

for congress members,
Put forth legislation for an ethics code for the Supreme Court (suggested)
Ensure the ethics code is applied consistently from now on (suggested)
</details>
<details>
<summary>
2023-12-01: Let's talk about the pause ending and what's next.... (<a href="https://youtube.com/watch?v=ocleoU-Fjvc">watch</a> || <a href="/videos/2023/12/01/Lets_talk_about_the_pause_ending_and_what_s_next">transcript &amp; editable summary</a>)

The US-Israeli relationship faces strain as Israel's actions in Gaza prompt limited US support and potential disregard for demands, prolonging the conflict.

</summary>

"The statement of, you don't have a couple of months, kind of indicates that there is a limit to how far the US is willing to go."
"Indicating a limit to support in that way, that's a big step."
"It does not appear like we're going to see the end of this anytime soon."

### AI summary (High error rate! Edit errors on video page)

The situation in Israel is developing with fighting having resumed and Israel intending to move south.
Secretary of State Blinken had a tense exchange with Israeli officials, indicating a timeframe of weeks, not months, for their actions.
The US is putting pressure on Israel to prioritize civilian protection, avoid hitting critical infrastructure, and hold extremist settlers accountable.
The strained US-Israeli relationship is evident, with the US indicating limits to its support.
International pressure may not be effective due to Israel's strong ally status and veto power at the UN.
Israel is unlikely to heed the US demands and may continue with their course of action despite challenges.
Experts believe dismantling the organization in Gaza is a daunting task that may not be achievable in the near future.
Balancing civilian protection and avoiding displacement while moving south seems challenging for Israel.
Israel may choose to ignore US warnings, potentially prolonging the conflict.
The situation is unlikely to see a resolution soon.

Actions:

for diplomatic officials,
Contact local representatives to urge diplomatic action (implied)
</details>
<details>
<summary>
2023-12-01: Let's talk about Trump, NY, schedules, and orders.... (<a href="https://youtube.com/watch?v=BOWJrEA_GgQ">watch</a> || <a href="/videos/2023/12/01/Lets_talk_about_Trump_NY_schedules_and_orders">transcript &amp; editable summary</a>)

Trump's New York civil entanglement leads to a upheld gag order, transcripts causing court issues, and upcoming scheduling with a resolution expected by January end.

</summary>

"The judge very quickly said that it will now be enforced rigorously and vigorously."
"This is probably one of those moments where Trump's legal team was like, oh yeah, we'll fight this."
"The New York civil entanglement should be wrapped up by the end of January."
"Now undoubtedly Trump will probably try to appeal the decision, whatever it is."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump is facing a civil entanglement in New York, resulting in a gag order due to social media posts.
The judge quickly found Trump in violation of the gag order, leading to a $15,000 fine.
Despite Trump's appeal, the appeals court upheld the gag order, which will be rigorously enforced.
Trump's legal team inadvertently confirmed the validity of the gag order, potentially leading to stricter enforcement.
Transcripts of voicemails causing issues for the court were 275 single-spaced pages long.
Trump is scheduled to testify on December 11th, with arguments set for early January and a decision expected by the end of January.
The New York civil entanglement should be resolved by the end of January, with a possible appeal from Trump.

Actions:

for legal observers,
Stay informed about the developments in the New York civil entanglement (suggested)
Monitor Trump's legal actions and potential appeals (implied)
</details>
<details>
<summary>
2023-12-01: Let's talk about Santos and what happens now.... (<a href="https://youtube.com/watch?v=-SRtfDxFYwk">watch</a> || <a href="/videos/2023/12/01/Lets_talk_about_Santos_and_what_happens_now">transcript &amp; editable summary</a>)

Representative George Santos's expulsion from Congress sparks concerns about setting new expulsion precedents without convictions, potentially leading to future expulsions for lesser offenses.

</summary>

"This event in Congressional history may end up being overshadowed by other behaviors."
"The concern now is that there are going to be more attempts to expel people based on less serious behavior."
"He messed with the money. When he engaged with donors the way he did, that upset a whole lot of Republicans."
"The concern about potential future expulsions is shared by many people."
"I think this little portion of Congressional history is going to end up being a footnote."

### AI summary (High error rate! Edit errors on video page)

Representative George Santos was expelled from Congress by a relatively decisive vote, shrinking the Republican majority.
Santos was not convicted of any crime, sparking concerns about setting a new precedent for expelling members without a conviction.
Despite lacking defenders, Santos faced opposition for his alleged activities, particularly upsetting Republicans by mishandling donors.
The fear now is that more members could face expulsion for lesser offenses without the need for a conviction.
The concern about potential future expulsions is shared by many people.
The allegations against Santos and the knowledge of other Congress members played a significant role in his expulsion.
This event in Congressional history may end up being overshadowed by other behaviors that have yet to face formal action.
While currently significant, the expulsion of Santos may diminish in importance over the next few years.
The situation serves as a reminder of the dynamics and consequences within Congress.

Actions:

for congressional observers,
Contact your representatives to voice concerns about expelling members without convictions (implied).
</details>
<details>
<summary>
2023-12-01: Let's talk about Michigan and cleanish energy.... (<a href="https://youtube.com/watch?v=sqIi9z13ZPw">watch</a> || <a href="/videos/2023/12/01/Lets_talk_about_Michigan_and_cleanish_energy">transcript &amp; editable summary</a>)

Beau explains the controversy around Michigan's 100% clean energy bill, particularly focusing on the redefinition of clean energy and the inclusion of natural gas with carbon capture.

</summary>

"Everybody I know that had an issue with this, it had to do with the natural gas."
"That's what they're mad about, not the overall bill."
"It's a big step."

### AI summary (High error rate! Edit errors on video page)

Explains the controversy around Michigan's move to 100% clean energy by 2040.
Friends who are environmentalists are upset about the redefinition of clean energy in the bill.
The bill mandates that 100% of electricity comes from clean energy, with 60% from renewables.
The remaining 40% can be from hydrogen, nuclear, or natural gas with carbon capture.
People are particularly upset about natural gas with carbon capture being included.
Beau sees the bill as a positive step, but acknowledges it's not perfect.
Carbon capture is seen as expensive, inefficient, and still promoting fossil fuel use.
Beau questions whether advancements in carbon capture technology will make it more efficient.
Believes new electricity plants won't be built due to costs, existing ones may add carbon capture.
Acknowledges that despite the flaws, Michigan's move is a significant step in the right direction.

Actions:

for michigan residents, environmental activists,
Contact local representatives to advocate for stricter definitions of clean energy (implied)
Join local environmental groups to stay informed and involved in energy policy (implied)
</details>
