---
title: Let's talk about how Tucker's doing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=J12jPJ45I30) |
| Published | 2023/12/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Checking in on Tucker Carlson after some time.
- Carlson's openness to the idea of the Earth being flat.
- Mention of deception and lack of trust in preconceptions.
- Carlson's surprising pushback on the flat Earth theory.
- Speculation on Carlson hanging out with Jones soon.
- Suggesting sharing this information with Fox News-watching relatives.

### Quotes

- "Tucker Carlson was once one of the highest rated people on a news network, is open to the idea that the earth is indeed not round."
- "It seems like that might be something that can get through some pretty heavy defenses."
- "He's on his way to, yeah, he's going to be hanging out with Jones soon I guess."

### Oneliner

Beau checks in on Tucker Carlson's openness to the flat Earth theory, suggesting sharing with Fox News-watching relatives.

### Audience

Fox News viewers

### On-the-ground actions from transcript

- Share clips of Tucker Carlson's comments on the flat Earth theory online (suggested).

### Whats missing in summary

Context on the history between Tucker Carlson and Jones

### Tags

#TuckerCarlson #FlatEarthTheory #Deception #FoxNews #Jones


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to check in on somebody
who we haven't checked in on in quite some time,
somebody that used to be a constant source of things
that would end up needing to be talked about.
But ever since his show,
ever since he left Fox,
We haven't really been keeping up on him, but I feel like maybe it's time to check out.
So, I did that, and yeah.
Okay, so Tucker Carlson was asked what he thought about the ice wall and the idea of a flat earth.
Well I'm open to anything. I'm open to anything. But I just know from shooting, and then the person talking to him was
like, hey that's it, cut the sound bite.
And he goes on to say, how could I not be open to anything at this point? I mean there's
been so much deception that you can't trust your preconceptions.  deception, that you can't trust your preconceptions.
And then he goes on to talk about how what we have been
told about the Earth and about history, it's
just all not true.
Yeah, so that's what Tucker's been up to.
He's answering questions about the ice wall and the Earth
being flat. Now, he did later go on to say that basically because he likes
shooting that you know sometimes you have to account for that and it does
suggest that the earth might be round. That was really surprising to me, I mean
for two reasons really. One, that he would push back on the idea of a flat
earth. And two, I mean does anybody think Tucker's taking shots at that distance? I
mean that's wild to me. But yeah so Tucker Carlson who was once one of the
highest rated people on a news network is open to the idea that the earth is
indeed not round. So I feel like this is information that we might want to make
sure people know about, particularly our, you know, Fox News watching relatives.
It seems like that might be something that can get through some pretty heavy defenses.
There are clips of this available online.
It seems like that might actually be the thing that makes them say, huh, maybe all of that
other stuff that he insinuated or allowed other people to say, maybe that's not true.
Because yeah, there's, I gotta be honest, I didn't see this one comment.
He's on his way to, yeah, he's going to be hanging out with Jones soon I guess.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}