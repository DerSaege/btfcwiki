---
title: The Roads Not Taken EP18
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ygG-gSX82mE) |
| Published | 2023/12/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides a weekly overview of unreported or under-reported news events.
- Reports on foreign policy updates, such as Europe adjusting its security posture in response to Russia.
- Mentions Zelensky leaving the U.S. without more aid, progress in the Senate on aid packages, and the House going on vacation.
- Notes U.S. pressure on Israel to move away from major combat operations.
- Talks about major shipping companies suspending transit through the Red Sea due to fear of attacks impacting international trade.
- Updates on domestic news, including a Boston event celebrating historical events like the Boston Tea Party and Hawaii's threat to stop short-term rentals on Maui.
- References Mark Meadows' legal case and potential impacts from a recent Morocco kingdom heir case.
- Mentions political developments like Sununu's endorsement of Nikki Haley, potential third-party runs by Liz Cheney, and predictions on Republican losses in the House.
- Reports on Voyager 1's communication issues and Alex Jones' return to Twitter with a settlement offer to impacted families.
- Shares environmental news, including Wyoming Governor Mark Gordon acknowledging climate change and federal court decisions on wolf reintroduction in Colorado and Texas power plant responsibilities.
- Mentions a significant solar flare impacting radio communications in the U.S.
- Responds to viewer questions regarding voting dynamics, third-party considerations, and ongoing misconduct issues within the Coast Guard.

### Quotes

- "Major shipping companies are suspending transit through the Red Sea out of fear of attacks."
- "A vote for a third party is not necessarily a vote for Trump. It's a vote for a third party."
- "If it was my daughter, I would not risk jeopardizing the relationship over that."

### Oneliner

Beau provides insights on foreign policy, domestic news, political developments, environmental updates, and viewer questions, offering a comprehensive overview of current events.

### Audience

Viewers interested in staying informed and understanding nuanced perspectives on diverse topics.

### On-the-ground actions from transcript

- Contact local representatives to advocate for responsible environmental policies (implied).
- Stay informed about ongoing political developments and potential third-party runs (suggested).
- Educate oneself on international conflicts and foreign policy implications (implied).

### Whats missing in summary

Insights on viewer question responses and Beau's approach to providing informed perspectives on complex issues.

### Tags

#CurrentEvents #ForeignPolicy #DomesticNews #PoliticalDevelopments #EnvironmentalUpdates #ViewerQuestions


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to The Roads with Beau.
Today is December 17th, 2023,
and this is episode 18 of The Road's Not Taken,
which is a weekly series
where we go through the previous week's events
and look at unreported, under-reported news,
things that I just find interesting,
or things that will probably serve as context
to events later.
At the end, we go through some questions from y'all.
Okay, starting off in foreign policy, the German defense minister has basically said
that Europe needs to adjust its security posture.
It needs to rearm and get on a more defensive footing to counter Russia.
Zelensky left the U.S. without more aid, did not get an aid package, but there is progress
being made in the Senate on it.
The House, they like went on vacation or something because you know they're a super responsible
entity right now.
The U.S. is pressuring Israel to move away from major combat operations.
What does that mean?
It means a mission accomplished manner.
That's what it means.
If you are familiar with that, the whole,
that's what it means.
It's not actually an end.
Major shipping companies are suspending transit
through the Red Sea out of fear of attacks.
As the conflict starts to impact international trade,
expect more and more nations to become more and more vested in taking action
of some sort. It's foreign policy, it's all about power, that includes power
coupons. Okay, moving on to the U.S. Boston held a large event celebrating
looting, theft, vandalism, and lawlessness with a reenactment of the Boston Tea
party. The governor of Hawaii has threatened to stop all short-term rentals on Maui if housing
needs are not met by January. There are still roughly 6,000 residents living in hotels after
that devastating fire. Mark Meadows is still fighting to have his Georgia case removed to
federal court. The judges in the appeals court seemed skeptical of arguments in
his favor. If you are into weird legal quirks, there is a recent case with a
man who claims to be the heir of a kingdom in Morocco that plays into this
and it's worth looking into. It has to do with who qualifies for federal
removal, who qualifies as a federal officer, stuff like that, and it may impact Meadow's case.
Sununu either is endorsing or has already officially endorsed Nikki Haley. More big
donors have signed on. At this point, if Trump falls, she's your presumptive follow-up.
Republican strategist Susan Del Persico feels that recent events will lead to Republicans
losing the House.
It doesn't seem like she sees that going any other way.
There are more rumbles about a third party run by Liz Cheney.
The goal of this run would not be to win.
The goal of this run would be to make sure that Trump doesn't.
Voyager 1 is having issues communicating with Earth and sending information back.
There has been a little commentary about how NASA just isn't up to it and we need to privatize
and all that stuff.
I feel like it's an important moment to point out that that thing's been up there almost
half a century. It's like 46, 47 years old. I mean, the technology is a bit dated.
The man who wielded that folding chair in Montgomery, in that thing by the river,
he was reportedly ordered to perform 50 hours of community service.
And I definitely feel some way about that, I'm going to be honest.
Okay, moving on to cultural news.
Alex Jones is back on Twitter.
I'm sure that's going to help draw advertisers back to the platform.
In related news, he's offered a $55 million settlement to the families impacted by those
theories. And yes, that is to satisfy a judgment that's what, like $1.5 billion or something.
Related financial news. Trump is selling off pieces of his mugshot suit. From what I understand,
and you only need about five grand.
What a deal for a piece of history like that.
For comparison, you can get a piece of Alcatraz
or like the Blast Shield from the Manhattan Project
or a piece of the first supercomputer for like 50 bucks.
Moving on to environmental news,
the Republican governor in Wyoming, Mark Gordon,
He is telling people, climate change is real and it's coming,
and we need to get ready for it.
That's a welcome development.
Sadly, he is getting pushback.
So there's that.
Looks like federal courts blocked an attempt
to stop reintroduction of wolves into Colorado,
specifically, gray wolves.
According to judges, Texas power plants
have no responsibility to provide electricity
during an emergency.
This, of course, becomes important when
you factor in that nearly 250 people were
lost in that storm in 2021.
Moving on to oddities, massive solar
flares have been a staple of apocalyptic stories for quite some time. Well, a huge one,
huge one, is heading to Earth and will arrive a few days ago. It hit.
It knocked out radio communications for a couple hours in the U.S. Noah said it was,
quote, likely one of the largest solar radio events ever recorded. Hollywood may
need to readjust some things on the way that's portrayed. Okay, moving on to the
Q&A section. Note, say the email address. Say the email address. Fair enough. It is
question for Bo, that's question for F-O-R, Bo, B-E-A-U, at gmail. Okay.
Bostradamus is a joke, but it's also not. I've watched you predict the fall of Afghanistan
and exactly how it would happen a year before it did. Your video prior to Russia invading Ukraine
was amazingly accurate. You predicted how Soledaro was like Bunker Hill for Wagner,
and now your October 9 video on Palestine is also fortune telling in action. Can you please apply
this skill to the election and tell me how much I have to worry about? I'm not riding with Biden,
But I'm terrified and know my life will get worse if a Republican wins.
Can you please apply this skill to the election?
No.
I can't.
Look at your list.
Afghanistan, Ukraine, Solidar, Palestine.
The fortune-telling aspects, it's not a fortune-telling skill.
It's a lot of study of one particular topic.
It's unconventional conflict, it's political violence.
The fall of Afghanistan, even the invasion of Ukraine, when you look at the initial invasion,
which is what that video was about, the defense that Ukraine was likely to put up and did
put up was a defendee strategy.
It was an unconventional strategy.
We talk about a lot of things on this channel.
This is my area of study.
I am better at assessing what will happen because it's something I studied for decades.
I wish I could, but it doesn't work that way.
It's about having a bunch of knowledge to build off of.
How can I get over my sadness that my questions haven't been picked?
That's messed up that they put that in there.
I hope one of these other ones is from you.
Wow.
So when they go through and pick these, they tend to pick ones that can be used to make
a point, or are ones that they think will add some levity, which is probably why they
chose this, or they know it's something that I can speak freely and off the cuff about.
If your questions are like, detailed policy questions, they're probably not going to
put them in here.
The other thing is if a whole bunch of people ask similar questions.
Okay.
While speaking with my millennial daughter today, I was called the boomer label.
And more specifically, was told that because I was very concerned about a Trump election
when becoming an authoritarian nightmare, and that that was boomer think.
I'm barely even considered a boomer by birth year, so of course I was a little
bit offended, but that's not my question.
My parents are Republicans, but will not vote for Trump.
So they now say they just won't vote or will look at a third party.
Now, my daughter says I need to not even consider Biden and to look into a young woman I've
never heard of before that is running third party.
Should I just wait a while and see what plays out for Democrats and third parties?
Try to convince my daughter and parents to vote Biden over third party as third party
could help Trump, although Daughter blasted me when I tried to bring this up today, with
that's all she's heard for 20 years.
Just keep my mouth shut and let everyone cancel each other's vote out, as I bet we've been
doing for years.
Well, so a vote for a third party is not necessarily a vote for Trump.
It's a vote for a third party.
If they're not going to vote and those are their options, either not vote or vote third
party, I mean, it's a wash on that, younger people have a lot of, let's just call it
reluctance when it comes to Biden right now, that may change as people are reminded of
who Trump is, but Biden's handling of a lot of current situations does
not sit well with, with younger people.
Um, some of it is, is bumper sticker stuff and a lack of understanding it.
But some of it is a moral position that realistically, if it's the bumper sticker stuff, yeah, maybe
there's some education there that could be done.
But if they're doing it for a moral reason, they've suffered a moral injury, you're
not going to change that.
If it was my daughter, I would not risk jeopardizing the relationship over that because if it is
from a moral position, you are incredibly unlikely to be able to shift that at all.
As far as all the people saying that they're voting third party, this was something, and
I didn't, the person took it the wrong way,
but a friend of mine was like,
I'm gonna vote for Cornel West.
And I was like, spell it, spell his name.
And he kind of looked at me like I was saying
he couldn't spell.
And in point of fact, he did misspell his name.
The point I was trying to make by that
wasn't a spelling thing, it's that
A lot of them are not going to be on the ballot.
That's a whole issue with ballot access and that realistically, no matter how you feel
about third parties, that's a problem.
So I think there are a lot of people right now that are saying, I'm going to vote for
third party that realistically, when they get in that booth, they won't.
But at the same time, I think there's probably going to be more third party votes than normal.
Because there are a lot of disaffected people from both parties.
There are people who will not vote for Trump that are Republicans, but they're also not
going to vote for Biden.
And there are certainly people who are not going to vote for Biden who are typically
democratic voters.
So, yeah, I would, I would pay close attention to what her motivation is to
say, she's not voting for Biden.
If it's a misunderstanding of a policy thing, that's one thing.
If it is, he did this, and I can't stand for it, that's something else.
I realize you don't speak for the Democratic Party, and that you're not a Democrat, thanks.
But you have a keen sense in your voice of reason and calm in a world of chaos.
We've done several videos recently about the Civil War and the Republican Party.
My question is, why isn't the Democratic Party adding fuel to this fire and doing more
to encourage red on red fire?
In my way of thinking, if the moderate GOP wins, the Democratic Party can negotiate with
the GOP, which will be in a weaker state.
If the MAGA faction wins, a lot of people will be turned off, leaving them to destroy
themselves allowing another party to take their place, am I wishful thinking here?
I mean, I don't think you're, those are definitely options.
I don't know that they're the only options, but they're definitely options.
As far as why isn't the Democratic Party adding fuel to the fire, because that's the
Democratic Party.
One of my biggest critiques of the Democratic Party is its messaging and its unwillingness
to play hardball at times.
There are a number of races where some certain words are drawing attention
to certain things in certain races would leave Republicans duking it out amongst themselves.
The Democratic Party tries to play fairer than the Republican Party.
I don't know, last time they did a little bit of encouraging Red on Red when they funded
like really bad candidates during the primaries.
They funded Republican, I mean not the Democratic Party like officially, but some Democratic
donors funded some really bad Republican candidates during the primary, and it led to them winning
and being unelectable.
Why hasn't the missing binder of classified information been on the radar nonstop?
How does a super secret classified binder go from needing to be viewed in a special
room in a special building to being handled?
So, here's the thing, this is being reported as if it just happened.
People have been aware of this for a while.
It's just not being reported on a whole bunch.
So don't think that this wasn't on the radar before it was.
Do you plan to address the ongoing misconduct issues within the Coast Guard?
Yes.
I want a little bit more information, but yes.
Okay.
What should I get to fill a med kit trauma kit?
That depends on your level of training.
What I would do if you're just starting out,
get an IFAK, I-F-A-K, and make sure you know how to use
everything in it.
one and start there. I'm in a very red state and hear this opinion all the time.
The reason there are so many job openings is because so much of society
is lazy and living off the government. What is a good response that is
relatively short considering how complex it really is? First, point out that I mean
it is more of a problem in red states. And then the next thing I would point
out is that it probably has a lot to do with the unemployment rate being really low right
now.
Okay, so that looks like all of the questions.
All right, so there's a little bit more information, a little bit more context, and having the
right information will make all the difference.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}