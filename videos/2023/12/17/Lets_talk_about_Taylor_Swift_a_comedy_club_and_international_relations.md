---
title: Let's talk about Taylor Swift, a comedy club, and international relations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tqsBrRvXmvI) |
| Published | 2023/12/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Taylor Swift attending a comedy club led to outrage from the right wing in the United States.
- The comedy club supported a charity called ANERA, which does humanitarian work in Gaza.
- Right-wing individuals have accused Taylor Swift of supporting Hamas due to her connection to the charity.
- ANERA is a 501c3 organization that operates with strict regulations to prevent funding designated groups like Hamas.
- The outrage is fueled by the belief that any charity operating in Gaza is indirectly funding Hamas.
- The right wing's anger towards Taylor Swift seems to be more about generating outrage and staying relevant.
- Beau draws parallels to how people unknowingly support organizations through indirect means, much like the situation with ANERA.
- The focus on attacking Taylor Swift may not resonate well with a significant portion of her voting-age audience.
- Beau criticizes the right wing for being disconnected from the broader population and potential voters.
- The outrage and boycott calls are seen as a tactic to energize the right-wing base without much fact-checking or understanding.

### Quotes

- "You need to calm down."
- "This is more right-wing trying to generate outrage and basically grab headlines."
- "The right in the United States is showing how disconnected they are from the rest of the United States."
- "The short version is that this is just well, normal right-wing politics at this point."
- "Y'all have a good day."

### Oneliner

Taylor Swift's visit to a comedy club sparks baseless outrage from the right wing, revealing a disconnect with reality and voter demographics.

### Audience

Social Media Users

### On-the-ground actions from transcript

- Research and support ANERA (suggested)
- Stay informed about charity organizations operating in conflict zones (implied)

### Whats missing in summary

The full transcript provides detailed insights into the baseless outrage culture perpetuated by the right wing towards Taylor Swift, showcasing the disconnect between political agendas and factual understanding.

### Tags

#TaylorSwift #OutrageCulture #RightWing #Charity #ANERA


## Transcript
Well, howdy there internet people, let's bail again.
So today we are going to talk about Taylor Swift
going to a comedy club because apparently
that is international news.
So yeah, we'll talk about that and how
that has led to people in the right wing of the United
States being super mad with Taylor Swift.
We will talk about the logic that's behind this and then we're going to provide some information that I think is
incredibly funny, personally.  If you have no idea what's going on, Taylor Swift and some other people, they went to a
comedy club.
This comedy club, I don't know if it was all of the proceeds or a portion or whatever, but they were supporting a
charity.
charity that is A-N-E-R-A, if you want to look into them, okay. This organization, it
does a lot of charity work in the Near East. It does a lot of charity work in
Gaza, providing humanitarian relief. The right wing has decided that this is
totally not fetch, and they're really mad.
Megyn Kelly has called for a boycott.
Okay, so there is obviously a leap in logic that is occurring,
but the general logic that is coming from the right is,
she went to this comedy club,
this comedy club supports this charity that does work in Gaza,
therefore,
well, she supports Hamas.
Okay, you can't just ask people
why they support charity organizations.
Here's the thing about this.
We will obviously get to the leap in logic,
but there's an important factual thing
that I think needs to be acknowledged.
The reason people are making this jump
is because they believe that any charity organization
that operates in this area is really secretly
deep down funding Hamas.
Okay.
ANERA, they're kind of known for being super careful
being super careful about that. And here are some reasons why. They are a 501c3 in the U.S.
A U.S. organization that was providing funding or even allowing stuff to be diverted,
anything like that, well they'd find themselves in prison because you're talking about designated
groups here. So that's probably not happening. And you know, when I say it
like that, you know, there's room for doubt. Let's do it a different way. If
you're in the United States, you're watching this channel, to include Miss
Kelly, odds are you've helped fund this organization, even if you don't know it.
Even if you don't know it. The reason for that is because they are reportedly so
good at differentiating stuff like this that State Department and USAID give
them money. Some of your tax dollars have gone to them. So I mean you might say
that's kind of like going to a comedy club and the person putting on the
show or the club sending money over there, right? I mean you didn't do it, you
gave to somebody else who did it. That's pretty much the same dynamic. But you
didn't know that. Most people didn't know that. I doubt those people who are
screaming right now about this have any idea that they have helped fund this
organization as well. They just want something to be mad at because for
whatever reason the right wing in the United States has decided to make Taylor
Swift, public enemy number one. You need to calm down. So there's obviously a
number of leaps in logic here, but I think the important one is just because
somebody goes to a show or goes to a business that donates to a charity they
don't like or something like that, it doesn't necessarily mean that that
person supports that charity. They may not even know it's happening. As an
example there are people watching this right now who are super mad every time I
say something even remotely positive or just even acknowledging the humanity of
trans people I get hate mail that means those people watched this this channel
gives money to travel project that money came from them watching it's the same
thing. This is more right-wing trying to generate outrage and basically grab
headlines and try to stay a little bit more hip and with it by going after
Taylor Swift because to most of the right-wing in the United States, Taylor
Swift, much like the message about Pink, she's a teeny-bopper. That's the way it's
viewed. I think it might be important for the right in the United States to
understand that dynamic a little bit more clearly, so I'm gonna try to relate
it to something y'all may be more familiar with. You're going after Dolly.
same effect. You're going after Dolly. That's what's occurring. It's
probably not a wise political move. I mean, luckily, I'm going to suggest that
the 11 people who tune in to right-wing shows who also like Taylor Swift, I'm
going to suggest that they're probably not going to participate in the boycott.
it won't be that big of a deal. But at the end of it, it's another moment where
the right in the United States is showing how disconnected they are from
the rest of the United States and from the future voters and current voters
because she's not a teeny-bopper. Her audience, they're voting age. I mean not
all of them obviously, but a large portion of them. So the whole thing is
based on a series of leaps and logic. The organization that everybody is mad about
her going to a comedy club that then provided this charity with, you know, with
some funding. Odds are if you're in the United States, you pay your taxes, you in
some way have done the same thing. A series of leaps in logic and the whole
point is to outrage their base and energize them because they know they're
not going to check into any of this. So the short version is that this is just
well, normal right-wing politics at this point. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}