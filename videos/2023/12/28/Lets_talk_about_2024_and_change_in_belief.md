---
title: Let's talk about 2024 and change in belief....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6GP39eCGlyk) |
| Published | 2023/12/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of the "unlikely non-voter" as a new category to watch in future elections.
- Mentions the impact of unlikely voters who traditionally don't participate but could sway an election if they do.
- Refers to a recent AP-NORC poll that asked about people's confidence in their vote being correctly counted in the primary elections.
- Notes that 32% of Republicans had little to no confidence that their vote in the primary election will be correctly counted, compared to single-digit percentages for Democrats.
- Speculates on reasons why people choose not to vote, citing lack of belief that their vote will matter as a significant factor.
- Suggests that perpetuating claims about election fraud may lead to long-term implications for the Republican Party, potentially driving more voters to believe that their vote doesn't matter.
- Raises concerns about the impact of baseless theories on voter confidence and the potential rise of unlikely non-voters within the Republican Party's base.
- Emphasizes the importance of paying attention to this demographic shift and its potential consequences for future elections.

### Quotes

- "The unlikely voter."
- "One out of three is huge."
- "It might lead them to believe that, well, it doesn't matter."

### Oneliner

Beau introduces the concept of the "unlikely non-voter" and raises concerns about the impact of voter confidence on future elections, especially within the Republican Party.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Monitor voter confidence levels within different demographics (suggested)
- Engage with individuals who express little confidence in the voting process (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential influence of unlikely non-voters on future elections, especially within the Republican Party. Viewing the full transcript can offer additional insights into voter behavior and perceptions.

### Tags

#Elections #VoterConfidence #UnlikelyNonVoter #RepublicanParty #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about a new category
of person that we might should start
paying attention to when it comes to looking
at future elections and how they might turn out.
Over the last few cycles, we've talked a lot
about the unlikely voter, those people
who aren't normally counted in polling, but who show up.
They're part of demographics who traditionally
don't show up in large numbers,
but if they show up a little bit more than normal,
it's enough to sway an election.
The unlikely voter.
It's still early, but because of a recent AP-NORC poll,
example, we might need to start looking at the unlikely non-voter.
So the polling, it asked people about their confidence level and whether or not they had
a high degree of confidence that their vote would be correctly counted in the primary.
So the numbers that would matter here would be Democrats talking about the Democratic
primary and Republicans talking about the Republican primary and whether or not they
believe that their vote would be correctly counted.
Those would be the numbers to look at to see any potential deviation.
I don't remember exactly what the Democratic party percentage was of those who had little
or no confidence that their vote would be correctly counted but it was single
digits. I want to say eight or nine percent. The Republican Party, 32, 32
percent had little or no confidence that their vote would be correctly counted.
That's big. That is a big, that's a big percentage. You're talking about one out
of three, and this is in the primary.
These people who hold this view, you have to go through and think about the reasons
people you know don't vote.
Some don't vote because they're not political.
Some don't vote because they have a moral reason for abstaining.
What's the biggest reason?
It won't matter, right?
It won't matter so they don't show up.
If you have little to no confidence that your vote will be correctly counted, do you
think your vote would matter. Maybe it was a horrible idea for the Republican
Party to feed into all of the claims about the elections. Maybe that's going
to have long-lasting implications for the party, especially as more and more
elections fail to go their way because the way the information silos exist and
the way commentators cheerlead we're gonna have a red wave we're gonna have
a red wave when that's really not what was expected they hear that over and
over again and then the results are different it plays into the idea that
that their vote doesn't matter. It gives them
little or no confidence that their vote will be correctly counted.
It might lead them
to believe that, well, it doesn't matter.
It's not going to make a difference. And it
puts them in that category of
unlikely non-voter. Somebody who would normally show up
if the Republican Party had stood up to Trump, but now they may view it as
pointless because of a bunch of baseless theories that were echoed by people they
trust. Again, it's one poll and it's early on, but this is a demographic we're gonna
have to watch. It was stuff like this that led us to start looking at unlikely
voters. This, this might matter. One out of three is huge. Particularly when you
think about those people who would hold that opinion, they would be from the
faction of the Republican Party that is further right because those are the
Republicans who carried that message forward the most. Their constituents, those
people who follow them, who believe them, well they believed them. Might have been
a huge mistake.
Anyway, it's just a thought.
and y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}