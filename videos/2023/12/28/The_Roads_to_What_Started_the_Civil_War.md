---
title: The Roads to What Started the Civil War
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pKc4s2-Ai1Q) |
| Published | 2023/12/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of understanding American history and specifically addresses the debate surrounding it.
- He argues that the debate is manufactured and not based on primary sources, especially when it comes to discussing the Civil War.
- Beau mentions how discussing unpopular opinions can lead to cringing reactions, particularly when it comes to teaching the founding documents of the Confederacy.
- He criticizes the lack of acknowledgment of slavery as the primary cause of the Civil War in modern debates.
- Beau reads excerpts from statements of reasons from states like Georgia, Mississippi, South Carolina, Texas, and Virginia, which clearly state that slavery was a major reason for secession.
- These excerpts illustrate how these states tied slavery to their economic interests, civilization, and identity.
- Beau stresses that these primary documents clearly show that the Civil War was fundamentally about slavery.
- He mentions the Cornerstone Speech and how it further solidifies the Confederacy's belief in slavery as a cornerstone of their society.
- Beau points out that by reading these primary sources, the debate over the Civil War's cause should be settled.
- He concludes by urging proper historical education to understand the true context of events like the Civil War.

### Quotes

- "There is no academic debate over this. It doesn't exist."
- "Understanding these, if people actually read this, read the cornerstone speech, and had that context for this discussion it wouldn't be taking place because anybody who said it was about anything other than slavery would be laughed at."
- "They told you what it was about."
- "Sometimes all you need to put something like this to rest is a little bit more context, a little bit more information."
- "There is no debate over this. You don't have to ask Lincoln. You don't have to ask the Union. You just have to read their documents."

### Oneliner

Beau presents primary sources to debunk the manufactured debate around the true cause of the Civil War, which is unequivocally about slavery.

### Audience

History enthusiasts, educators, activists

### On-the-ground actions from transcript

- Read primary sources on American history (suggested)
- Educate others on the true causes of historical events (implied)

### Whats missing in summary

Beau's detailed explanations and historical context can best be experienced by watching the full transcript.

### Tags

#AmericanHistory #CivilWar #Slavery #PrimarySources #Debate #Education


## Transcript
Well, howdy there, internet people, it's Beau again,
and welcome to the Roads with Beau.
Today, we are going to talk about the roads
to understanding a piece of American history
that often gets debated.
But the thing is, there's no debate.
There is no actual debate about this.
People like to frame the discussion as an academic debate,
but if you were going to have an academic debate,
you would need sources.
and the first thing you would need is primary sources.
The only way to have a debate about this
is to ignore the primary sources.
It's not a real debate, it's manufactured.
And we're going to go through this.
You know, everybody has that one opinion about a subject
And when they state it, people cringe.
It happens so often, there's memes about it on social media.
What's your most unpopular opinion about X?
My most unpopular opinion about education
is that we should teach the founding
documents of the Confederacy.
And normally, when I say that, people
cringe because they hear that phrase and this accent,
And they make an assumption.
I have never had anybody who heard me say that, who then went and read the founding
documents that maintained that opinion.
The reason I think this is important is because, well, the discussion happens pretty often.
debate occurs what was the Civil War really about. But we just had a candidate
for president of the United States get asked what was the cause of the United
States Civil War and her response was don't come with an easy question right? I
mean I think the cause of the Civil War was basically how government was going to
run. The freedoms and what people could and couldn't do. What do you think the
cause of the Civil War was?" The person responded by saying, I'm not running for
president. And the candidate went on to say, I mean, I think it always comes down
to the role of government. We need to have capitalism. We need to have
economic freedom. We need to make sure that we do all things so that
individuals have the liberties so that they can have freedom of speech, freedom
of religion, freedom to do or be anything they want to be
without government getting in the way.
In 2023, a question about what was the cause
of the Civil War did not include the word slavery.
And that's the debate.
People like to argue what it was about, you know?
And you have people who say things like, well,
it was about states' rights.
And there's cute responses, states' rights to do what.
The thing is, you don't have to ask.
Because the Confederates told us.
I have talked about this before over on the other channel, but
the other channel is designed to be concise.
I can hint to these documents, but in the time allotted over
on the other channel, the way that one's set up, I can't
really go into them.
can hear. So what we're going to do is read just a little bit of some of the
statements of reasons from a few states that left the Union. And you need to keep
in mind the people who wrote these documents, they believed they were
setting up a new country, one that was going to last a whole lot longer than it
did, and they laid out the reasons for the establishment of this new country, this Confederacy.
It's pretty illuminating, and it ends this debate.
We're going to start with Georgia.
This is the first paragraph, mind you.
The people of Georgia, having dissolved their political connection with the government of
United States of America present to their Confederates and the world the causes which
have led to this separation.
For the last 10 years, we have had numerous and serious causes of complaint against our
non-slave-holding Confederate states, with reference to the subject of African slavery.
Paragraph 1.
Let's go to Mississippi, which is definitely one of the more interesting ones.
In the momentous step which our state has taken of dissolving its connection with the
government of which we so long formed a part, it is but just that we should declare the
prominent reasons which have induced our course.
That's the first paragraph.
The second paragraph.
Our position is thoroughly identified with the institution of slavery, the greatest material
interest in the world.
Its labor supplies the product which constitutes by far the largest and most important portions
of commerce of the earth.
These products are peculiar to the climate verging on the tropic regions.
And by an imperious law of nature, none but the black race can bear exposure to the tropical
sun.
These products have become necessities of the world, and a blow at slavery is a blow
at commerce and civilization.
That blow has long been aimed at the institution and was at the point of reaching its consummation.
There was no choice left us but submission to the mandates of abolition or a dissolution
of the Union.
Yes, Mississippi, in their first and second paragraphs, said, well, we have to have slavery
because white people can't handle the heat.
And they equated slavery with civilization itself.
Let's go to South Carolina.
The people of the state of South Carolina, this is the first paragraph.
In convention assembled on the 26th day of April A.D., 1852, declared that the frequent
violations of the Constitution of the United States by the federal government and its encroachments
upon the reserved rights of the states fully justified this state in then withdrawing from
the federal union, but in deference to the opinions and wishes of the other slave-holding
states basically Virginia wanted to to leave the Union earlier but the other
slave-holding states said no and because that was the thing that united them
Virginia stayed a little bit longer. Let's go to Texas. Texas waited till the
third paragraph and really it's worth noting that the first two paragraphs are
are stuff like, on this date, this happened,
this is how we became part of the union, blah, blah, blah.
But when it gets to the actual meat of their argument,
this is what you get.
Texas abandoned her separate national existence
and consented to become one of the confederated union
to promote her welfare, ensure domestic tranquility,
and secure more substantially the blessings of peace
and liberty to her people.
She was received into the Confederacy with her own Constitution under the guarantee of
the Federal Constitution and the Compact of Annexation that she should enjoy these blessings.
She was received as a Commonwealth holding, maintaining and protecting the institution
known as slavery.
And then, just in case you weren't sure what kind, the servitude of the African to the
white race within her limits, a relation that had existed from the first settlement of her
wilderness by the white race and which her people intended should exist in all future
time.
Virginia The people of Virginia, in their ratification
the Constitution of the United States of America adopted by them in convention on the 25th day of
June in the year of Our Lord 1788, having declared that the powers granted under the said
Constitution were derived from the people of the United States and might be resumed when so ever
same should be perverted to their injury and oppression. And the federal government,
having perverted said powers not only to the injury of the people of Virginia but
to the oppression of the southern slave-holding states." First paragraph.
There's no debate over this. You don't have to ask Lincoln. You don't have to
ask the Union. You just have to read their documents. They told you in the
first paragraph, it was about slavery. There is no academic debate over this. It
doesn't exist. And if this isn't enough for you, there is a speech given by the
the Confederate vice president. It's called the cornerstone speech. That is
something at some point in time, I actually read portions of it, what was
fit to be read, in a video over on the other channel. The short version was that
God wanted it that way. This is just a sampling of the initial paragraphs of
of these documents. When you get into them, it gets worse. Texas also said God wanted
it that way. There is no doubt, no debate when it comes to what the Civil War was over.
The fact that this is still a discussion is because we don't teach history properly.
We don't actually provide the context.
So people can say things like, you know, Lincoln really didn't want to free the slaves.
That really wasn't his goal.
Lincoln's not the person you need to ask.
The people who signed these documents, the people who put this together, those are the
people you have to ask because they were the people who left the Union, they were
the people who fired on Fort Sumter. That's what started the war. There's no
discussion about this. And as far as the Republican claim to Lincoln, it's
It's worth remembering that Lincoln, for his time, was a progressive who got fan mail from
Karl Marx.
You probably don't actually want to claim him.
When this topic comes up, direct people here.
There's no debate over these.
These documents are known as the Statement of Reasons.
A lot of it is plagiarized, a lot of the documents are plagiarized from the U.S. founding documents,
and they all carry the tone of listing out the acts that the king did, and they try to
mimic that language in that format, but they're called the Statements of Reasons, and most
states had them. Some were less obvious in why, but it's all there. Slave-holding
states and white people can't handle the heat. Can you imagine the mindset it
it would take to put that in what was supposed to be the founding documents of a country.
Understanding these, if people actually read this, read the corner sound speech, and had
that context for this discussion it wouldn't be taking place because anybody
who said it was about anything other than slavery would be laughed at. They
told you what it was about. A lot of times historical debates they get thrown
off and most times it's the fault of historians because historians find out
a new piece of information and to make it interesting, they put it out there.
But that little nugget of information, it doesn't provide full context.
And that's kind of how this debate started.
But it's not a debate among academics, because these are the primary documents.
These are the documents that you would have to actually look at to make these determinations.
And they're super clear, they're crystal clear.
Sometimes all you need to put something like this to rest is a little bit more context,
a little bit more information.
And having the right information will make all the difference.
Have a good night.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}