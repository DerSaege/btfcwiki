---
title: Let's talk about Michigan, betrayal, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4xdQ9Y_Tq0Y) |
| Published | 2023/12/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Michigan Supreme Court upheld decision against removing Trump from the ballot, based on procedural grounds.
- No determination made by the court on whether Trump engaged in insurrection.
- Elector Renner, 77, made a deal with investigators and is cooperating.
- Renner described feeling betrayed during January 6 hearings.
- Renner was ushered through the process by others and was not fully aware of his actions.
- Renner is the only elector who has made a deal so far; investigation is ongoing.
- Renner's cooperation may lead authorities to look at others involved in the process.
- Possibility of more individuals getting wrapped up in the investigation.
- Michigan authorities might scrutinize those who guided Renner through the process.
- Situation could escalate similar to the Georgia case depending on further revelations.
- Investigation outcome remains uncertain; more developments could arise.
- Renner's cooperation might lead to higher-level inquiries.
- Michigan investigation still in progress; future actions and outcomes unclear.
- Potential for more individuals to be implicated in the ongoing investigation.
- Speculation on parallels between Michigan case and Georgia situation.

### Quotes

- "He felt betrayed, that's all I can say."
- "Renner provided information that might lead authorities to look at some of the other people."
- "It's worth remembering that the investigation is still ongoing."
- "This may be a situation where we end up with another Georgia style case."
- "Y'all have a good day."

### Oneliner

Michigan Supreme Court upholds decision against removing Trump, while elector cooperation may lead to broader investigations and potential parallels to Georgia case. Investigation ongoing.

### Audience

Michigan residents

### On-the-ground actions from transcript

- Cooperate with investigators if involved in any irregularities (exemplified)
- Stay informed about the ongoing investigation (suggested)

### Whats missing in summary

The full transcript provides more context on the Michigan Supreme Court decision, the details of Renner's cooperation, and the potential implications for further investigations.

### Tags

#Michigan #Trump #Election #Investigation #Cooperation


## Transcript
Well, howdy there Internet people, it's Beau again.
So today we are going to talk about Michigan
and two stories coming out of Michigan and Trump.
They both relate to Trump.
The first and probably the biggest headline
is that the Michigan Supreme Court,
they decided against removing Trump from the ballot.
They upheld that decision.
The interesting thing to note about it is they didn't make a determination about whether
or not he engaged in insurrection or anything along those lines.
They rejected it on procedural grounds.
Basically it doesn't appear that there is a similar clause in Michigan election law
like there is in Colorado, and that was their basis for rejecting it.
The other news is one of the electors, one of the fake electors for Michigan.
Renner, who is 77, cut a deal with the investigators in the case and is cooperating.
He's also talked a little bit about it now, and basically he described how he was ushered
through the process by people who he thought knew what they were doing, that he wasn't
really aware of what he was a part of until much later.
In fact, the January 6 hearings were kind of a moment for him.
And describing how he felt, betrayed is an understatement, that's all I can say.
It's worth noting that out of those, out of the electorate, he's the only one that's
made a deal thus far, and it's worth remembering that the investigation is still ongoing.
It hasn't ended.
There may be more people who get wrapped up in it.
Based on the public-facing statements, I would imagine that Renner provided information that
might lead authorities in Michigan to consider looking at some of the other people, those
who guided him through the process, those people who he believed knew what they were
doing.
They might end up looking at some of them a little bit closer.
Depending on the information those people provide, if they are spoken to, it might go
even higher.
This may be a situation where we end up with another Georgia style case, but we'll have
to wait and see how it plays out. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}