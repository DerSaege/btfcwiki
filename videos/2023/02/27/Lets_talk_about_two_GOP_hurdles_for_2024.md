---
title: Let's talk about two GOP hurdles for 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ld3pYPGB-0g) |
| Published | 2023/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are split between "normal Republicans" and those further to the right like the America First, Trump-aligned factions.
- Many who supported baseless election claims in 2020 and lost elections are trying to take control of state Republican parties.
- The success of these individuals could influence and shape the candidate field in swing states.
- The Loyalty Pledge introduced by the National Republican Party requires candidates to pledge support for the eventual winner, causing division within the party.
- McDaniel is trying to bring back Reagan's "thou shalt never speak ill of another Republican" rule, but it may be challenging with the current candidate dynamics.
- The Loyalty Pledge could lead to candidates supporting someone they dislike or leaving the party if they lose.
- Trump might not accept election results he doesn't win, potentially leading to him running as a third-party candidate.
- The combination of state-level GOP chairs' rhetoric and national party dynamics may create challenges for the Republican Party in the upcoming primary season.
- The Republican primary season is expected to be intense, with candidates likely engaging in mudslinging and widening the existing division within the party.

### Quotes

- "The first development is a lot of people who supported the baseless claims about the 2020 election and who lost their bid for elected office in 2022, they're attempting to take the state-level chairs of the Republican Party."
- "The Loyalty Pledge introduced by the National Republican Party requires candidates to pledge support for the eventual winner, causing division within the party."
- "Trump might not accept election results he doesn't win, potentially leading to him running as a third-party candidate."

### Oneliner

Republicans face division as baseless election claim supporters attempt to control state parties while Loyalty Pledge causes rifts, potentially leading to Trump running third party.

### Audience

Political Observers, Activists

### On-the-ground actions from transcript

- Reach out to local Republican Party chapters to understand their stance and actions (implied).
- Stay informed about the developments within the Republican Party and how it may impact future elections (implied).

### Whats missing in summary

Insight into how these developments could affect the broader political landscape and voter perceptions.

### Tags

#RepublicanParty #ElectionClaims #LoyaltyPledge #Trump #Division


## Transcript
Well, howdy there, internet people, it's Beau again. So today we are going to talk about two
separate developments within the Republican Party and how when they converge, they may serve to
widen the division that exists within the Republican Party concerning those who might
be seen as, quote, normal Republicans and those who are decidedly further to the right.
Those who are the America First, Trump-aligned or Trump copy-aligned factions.
The first development is a lot of people who supported the baseless claims about the 2020
election and who lost their bid for elected office in 2022, they're attempting to take
the state-level chairs of the Republican Party.
seems to be an almost concerted effort for those who floated those claims or
backed those claims to seize control of the state party apparatus in a number of
states. It's worth remembering that in swing states when it comes to those who
supported those claims, those who denied the the outcome of the election, they
They didn't win in statewide races in swing states.
This development and the fact that some of them are actually succeeding puts them in
a position to where they can shape the primaries in those states.
They'll have a lot of sway over it and they will have a lot of sway over the candidates.
We like to pretend that that's not how it works, but they do.
So you may end up with a field of candidates who have signed on to these claims, even though
when it comes to any state that isn't deep red, it's been shown to be a political liability.
So that is a development in and of itself, and that is probably going to be very helpful
to the Democratic Party in swing states.
Not so great for the country,
but electorally it would help the Democratic Party.
Then you have the Loyalty Pledge.
The National Republican Party,
McDaniel, is deciding to go ahead with this.
Basically, if you want to be on the debate stage
and you want to participate in the primary
to try to get the nomination
to be the Republican candidate for president,
You have to sign a pledge saying that you will support the eventual winner.
This is going to lead to issues because you do have multiple factions within the Republican
Party now.
You have the Bush-era Republicans, the normal conservative Republicans.
You have the far-right Republicans, which they're factionalized as well between those
who hold their loyalty to Trump or those who are ready for a copy of Trump or whatever.
McDaniel is trying to reinstate the 11th Commandment, Ronald Reagan's thing, thou shalt never speak
ill of another Republican, trying to bring that back.
With these candidates, I don't see that as likely.
And having them sign a pledge that, in many ways, means that if they lose, they're really
kind of in a situation where they have to support somebody they dislike or leave the
party, maybe a decision that wasn't really thought out.
Aside from that, when you're talking about the major players such as Trump, do you really
think Trump would accept the results of an election that he lost? What leads anybody
to believe that's true, whether it be a primary or not? It's setting the stage for Trump to
run third party. McDaniel doesn't understand that, I don't think, but I think that that
may be something that happens. These two things working in conjunction add another level of
difficulty for Republicans, because even if McDaniels is able to pull it off and
everybody gets up there on the debate stage running for president and all they
do is go after Democrats and everybody stays stays at least semi cordial with
each other, the state-level GOP chairs, they're not gonna go for that. They're
They're doubling down on the wild rhetoric.
So even if McDaniel can pull it off on the national stage, it's going to be upset by
the state-level GOPs.
And once it starts, I find it really unlikely that the candidates for president don't engage
or at least don't comment.
And once it starts, once one of them comments, the rest of them are going to start.
I think we're looking at a very, very interesting Republican primary season coming up.
I think that by the time it's all said and done, they're going to sling a lot of mud
at each other.
And I think that the division that already exists within the Republican Party may actually
end up widening.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}