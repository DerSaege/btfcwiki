---
title: Let's talk about a South China Sea incident, trains, and coverage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Y1IC-fUteh4) |
| Published | 2023/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The media coverage of events like the South China Sea incident can create a false perception of reality by focusing on dramatic moments and not the overall context.
- Surveillance flights like the one in international airspace happen frequently, but they only become news when there is footage, like the CNN crew on board.
- Coverage of specific incidents, like train derailments, can lead to public misconceptions about the frequency and severity of such events.
- Media tends to amplify certain events based on public interest, creating a distorted view of reality for viewers.
- The focus should be on making changes and improvements based on incidents rather than solely on sensationalizing news for views and revenue.

### Quotes

- "Something being covered repeatedly doesn't actually mean that it's at an increase."
- "It's a really bad idea to shape your beliefs and your perception of the world based on frequency of coverage."
- "There should be a whole group of people trying to figure out how to make sure this never happens again."

### Oneliner

The media's focus on sensational events creates a false reality, shaping perceptions based on frequency rather than facts, hindering progress.

### Audience

News Consumers

### On-the-ground actions from transcript

- Advocate for responsible and accurate news coverage (implied)
- Support efforts to improve safety regulations and standards for incidents like train derailments (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of how media coverage can distort reality and the importance of focusing on facts over sensationalism.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about something
that happened in the South China Sea and media coverage
and how trends in the way the news covers the news
can often leave people with a false perception of things,
fictional interpretation of the world just based on how the media covers the
news. Recently in or over the South China Sea there was a I think it was a P-8. It
was a surveillance flight. Surveillance flights happen all the time. It was a US
surveillance flight and it was in international airspace but it was pretty
close to contested airspace so the Chinese sent up a plane and there was
that top gun moment where you know both planes are really close to each other
looking at each other gesturing I'm sure. The thing is the P-8 happened to have a
CNN crew aboard and they got footage of this dramatic incident, right? And in a
lot of the reporting it's used to kind of showcase how tensions between the US
and China are, they're just on the rise and look at this and that's how it's
being framed in a lot of the coverage. What's the reality? The commander of the
Navy mission was asked about it and was like, nah, it's more like, you know, just
another Friday over the South China Sea, because it's not actually incredibly uncommon.
Those types of games, they occur pretty frequently, we just don't know about them.
The coverage isn't there.
They occur, they happen, but they normally don't have a CNN crew literally on the plane,
so you don't have the footage, so it doesn't become a news story, so people don't think
about it. The same thing happened with the train in Ohio. That derailment, it was
severe. It's a big deal. It was a big deal, still is a big deal. Ecological
damage, and it was as train derailments go, it was severe just on that. Because
the public started expressing interest in train derailments, what did the media
do? They started covering train derailments in a pursuit to get clicks,
to get views. They started covering it more often than they would. So I want to
say there were four that they covered pretty prominently and it led to
to people developing this fictional interpretation of things.
You had people on social media actually floating theories about, are we under attack?
Is it sabotage and all of this stuff?
But what's the reality?
If you were to look at averages of train derailments since like 1990, it's a little more than 1,700
per year. 1,700 per year. Break that down into per day. Four over the course of a week
actually isn't bad. Now, admittedly, not all train derailments are the same. The one in
In Ohio, really bad.
A lot of those 1,700, not so much.
But when they started covering the other ones, it created a false perception because of how
the public has viewing habits that the media then plays into to get more views, to generate
more revenue. It's a really bad idea to shape your beliefs and your perception of
the world based on frequency of coverage. Something being covered repeatedly
doesn't actually mean that it's at an increase. I could be wrong about this
because I haven't seen recent numbers, but the last time I looked, we were actually kind of near
a historic low when it came to train derailments. But, because of the coverage, because of the
severity of what happened there, it's been brought to everybody's attention. And because of that,
hopefully there will be there will be changes in regulations there will be
changes in safety and those numbers will go down the averages will go down that
that should be what occurs here there should be a whole group of people trying
to figure out how to make sure this never happens again.
That should be the driving force behind the coverage, not simply scaring people.
But scaring people, it makes more money.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}