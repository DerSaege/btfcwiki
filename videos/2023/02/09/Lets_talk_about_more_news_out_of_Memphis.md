---
title: Let's talk about more news out of Memphis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0yXURYns1-s) |
| Published | 2023/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent developments in Memphis regarding texts, photos, videos, reports, and certification processes are confirmed.
- Footage shows officers taking photos of Mr. Nichols, which were then allegedly shared with others, including a female acquaintance.
- The department is moving forward with decertifying the officers involved and prohibiting them from being cops again.
- Decertification paperwork reveals scathing allegations about inaccurate reports and unprofessional behavior captured on body cameras.
- There will be 20 additional hours of footage to establish the officers' mindsets and capture their unprofessional comments.
- Expect more developments and major shake-ups within Memphis as this story progresses through the justice system.
- Decertification charges paint a bleak picture, with the possibility of more officers being implicated in the future.

### Quotes

- "A lot of that was speculated about. It's now confirmed."
- "This is not a story that's going to fade from the news."
- "The decertification charges paint a very bleak picture."
- "I see the possibility of other officers being implicated in this and in other things."
- "Y'all have a good day."

### Oneliner

Recent developments in Memphis confirm officers taking photos and sharing them, leading to decertification and scathing allegations of unprofessional behavior captured on body cameras.

### Audience

Community members

### On-the-ground actions from transcript

- Support initiatives advocating for police accountability (implied)
- Stay informed about the developments in Memphis and advocate for justice (implied)

### Whats missing in summary

The full transcript provides detailed insights into the confirmed developments in Memphis and the potential implications for other officers involved. Viewing the full transcript can offer a deeper understanding of the situation and its impact on the community.

### Tags

#Memphis #PoliceAccountability #JusticeSystem #Decertification #CommunityPolicing


## Transcript
Well, howdy there, internet people, let's go again.
So today we are going to talk about texts and photos
and videos and reports and certification processes
in Memphis, there have been some developments
and we're gonna kind of go through
what has been released.
A lot of this is stuff that was speculated on,
but it is now confirmed.
And then there are some additional details
that we didn't know at all.
OK.
So I know that there are a lot of people who, when they were
looking at that footage, and for those who may not know,
we are talking about the situation with Mr. Nichols in Memphis.
A lot of people, when looking at that footage, they're like, hey,
it looks like he took photos.
One of the cops took photos of Mr. Nichols.
That has been confirmed.
And according to the allegations, those photos
were then sent to a few people.
Some were officers, and then there was another person.
I am aware of the fringe theory that is out there.
The other person is described as, quote, a female acquaintance.
So there's that.
The department is moving ahead with the process
of decertifying them and prohibiting them
from ever being cops again.
The paperwork on that, it is scathing.
They're not holding anything back, and it certainly appears the allegations in the decertification
paperwork suggest that in addition to everything you see on the footage, they also were less
than accurate in their reports. We also found out that there was going to be 20
additional hours of footage. When that came out, I said the one thing that was
going to be really important was establishing the officer's mindsets and
that that may come from the additional footage as they are saying stuff that gets
picked up by other people's body cameras. This is part of the de-certification
allegations. You and other officers were captured on body-worn cameras making
multiple unprofessional comments, laughing and bragging about your
involvement. So there's also that. A lot of that was speculated about. It's
now confirmed. This is, I have a feeling that as this progresses there are going
to be more and more developments.
This is not a story that's going to fade from the news.
There are probably going to be major shake-ups within Memphis
as this moves through the justice system.
So, the decertification charges, they paint a very bleak picture, and it'll all be released
eventually, again at this point they're allegations, but I don't see the investigations stopping
where they're at and I see I see the possibility of other officers being implicated in this
and in other things.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}