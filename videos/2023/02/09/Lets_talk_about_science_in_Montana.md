---
title: Let's talk about science in Montana....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZSpH2N7VGws) |
| Published | 2023/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing science education in Montana, focusing on scientific theory and fact.
- Explaining the impact of Senate Bill 235, aiming to change how science is taught in schools.
- Pointing out the importance of teaching scientific theory alongside scientific fact.
- Emphasizing that science evolves and isn't just a static collection of facts.
- Warning about the consequences if theoretical science is eliminated from education.
- Criticizing politicians for prioritizing culture wars over proper education.
- Expressing concern for the future of Montana's students and their ability to compete.
- Questioning the motives behind the bill and its potential negative outcomes.
- Advocating for careful consideration of the implications of altering science education.
- Urging Montana to prioritize proper science education for the benefit of its children.

### Quotes

- "Teaching science without teaching theoretical science is pointless."
- "Montana is going to be looked at as that place where we definitely can't hire somebody from there."
- "The casualties of that culture war will be the children of Montana."
- "Be ready for drastic outcomes because the students won't be able to pass an HCT."
- "They want to force willful ignorance on the children of Montana."

### Oneliner

Beau warns Montana about the detrimental effects of eliminating scientific theory from education, potentially hindering students' future opportunities and competitiveness.

### Audience

Montana residents, educators, parents

### On-the-ground actions from transcript

- Challenge Senate Bill 235 through advocacy and education (implied)
- Support comprehensive science education for Montana's students (implied)
- Stay informed about legislative changes affecting education (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of altering science education in Montana and the potential negative outcomes for students' future opportunities and competitiveness.

### Tags

#ScienceEducation #Montana #SenateBill235 #STEM #CultureWar


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about science in Montana.
We're going to talk about scientific theory
and scientific fact.
We're going to talk about education
and how things may change in Montana
and what it would mean if it does change
due to a bill in the Senate, number 235.
The point of this bill is to alter the way science
is taught in Montana.
Specifically, it would eliminate the teaching of theory.
Only things that are deemed scientific fact, which
means they're indisputable, could be taught in schools.
That will make teaching science basically impossible because science
isn't static. It's constantly being disputed. It's constantly changing. That's
kind of the whole pursuit is to find out more information and as that occurs the
theory which isn't it doesn't mean the same thing as it does in common
conversation but the scientific theory evolves as new probably shouldn't use
the word evolve in this conversation the scientific theory changes as new
information becomes available science isn't about a collection of facts it's
about hey this is all of the information that we have this is what we can produce
and reproduce and this is where it leads us. This is our best guess as to what's
happening right now. That's what science is. It's constantly pushing forward. If
you yank that away from your kids over some culture war nonsense, your kids
will be behind. They will be far behind. If this passes in Montana, be ready for
drastic outcomes because the students won't be able to pass an HCT. They won't
be able to get into college. They'll have to take separate courses outside of
school to catch up because some politician wants to grandstand and
doesn't understand what science is. They want to force willful ignorance on the
children of Montana. They want to intentionally hold them back to win
points in a culture war. Teaching science without teaching theoretical science is
pointless. Teaching the scientific method kind of requires that you're going to
engage in experiments and dispute them.
The entire premise here is that some politician knows better than the
teachers, than the educators.
The entire premise is really about scoring points in a culture war.
And the casualties of that culture war will be the children of Montana.
They will not be able to compete.
They will have a harder time getting into college.
They will have a harder time getting jobs.
They're not going to be able to go out of state.
Montana is going to be looked at as that place where we definitely can't hire somebody from there.
How are you going to teach STEM programs?
As is usually the case when politicians want to grandstand, the people who really pay for
it, so they can get their 15 minutes of fame, are the kids.
Those that have to live with the mistakes of these politicians.
Montana should probably consider this one very carefully.
You know, run it through some, well, I don't want to say it that way, that may be illegal
to teach in Montana soon, I would think about it very carefully and think about whether
or not you want your children to have to take additional schooling just to, you know, meet
the bare minimum, because that's what's going to happen.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}