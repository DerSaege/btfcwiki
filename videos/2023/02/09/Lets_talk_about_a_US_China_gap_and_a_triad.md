---
title: Let's talk about a US China gap and a triad....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XJfoz0rEY9Y) |
| Published | 2023/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the concerns surrounding the report that China has more ICBM launchers than the US.
- Explaining that although China has more launchers, the US has four times as many warheads.
- Mentioning that the number of launchers China has is not a major concern if they lack warheads to put in them.
- Noting that both China and the US are upgrading and modernizing their nuclear capabilities.
- Explaining the concept of a nuclear triad, consisting of land, sea, and air-based nuclear arsenals.
- Emphasizing the importance of the triad in maintaining deterrence against first strikes.
- Stating that the land-based nuclear arsenal isn't the most critical component of the triad; submarines play a significant role.
- Pointing out that even if land-based silos are destroyed, submarines provide a hidden and effective deterrence.
- Indicating that having a triad helps ensure that a country can't be completely disarmed in a surprise attack.
- Expressing concern that Congress might view China's increased launchers as a reason to escalate spending on nuclear modernization.

### Quotes

- "It isn't really a big deal."
- "The other pieces of the triad still maintain deterrence."
- "I don't think it's worth starting an arms race over."
- "The mineshaft gap, it's not real."
- "The United States still maintains deterrence."

### Oneliner

Addressing concerns over China's increased ICBM launchers, Beau explains the insignificance of the launcher count compared to warheads, underlining the vital role of the nuclear triad in deterrence against first strikes.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Contact policymakers to advocate for responsible spending on nuclear modernization (implied).

### Whats missing in summary

Beau's detailed explanations and insights on nuclear capabilities can best be grasped by watching the full video.

### Tags

#NuclearCapabilities #ICBMs #Triad #Deterrence #ArmsRace


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to answer two questions, really.
But they're very closely related.
And we're going to explain whether or not something
is really a big deal, because it is certainly
going to be made out to be a big deal.
And we really have to consider if this
This is how we want to start off the next near-peer contest.
If you missed it, a report hit the Capitol, hit Congress, and that report says that the
number of ICBM intercontinental ballistic missile launchers that China has exceeds the
number of the US has. ICBM launchers, whether they be stationary or mobile, all
land-based. Of course, this is already fueling people in Congress to say, hey,
we need to modernize. We are already at, we cannot allow there to be a
mineshaft gap. We're already at that portion of the show. This is how an
arms race starts. One of the things that is definitely worth noting in this
report is that while they have more launchers, they don't have more missiles
and they don't have more warheads. In fact, the United States has about four
times as many warheads. The mineshaft gap, it's not real. Okay, but whether or
not that little fact matters to Congress and when it comes to spending money, I
have no idea. It is not a major deal that they have more launchers if they don't
have anything to put in them.
It does show that China is attempting to upgrade and modernize its nuclear capabilities, as
is the United States.
But the land-based stuff isn't really where it's at.
And this gets into the other question.
Recently in a video, I used the term triad.
Whole bunch of questions.
What is that?
explain it like at all. A triad is a concept when it comes to nuclear warfare.
Three things, land, sea, and air. This is where your nukes are. You have some in
silos, some on subs, some in strategic bombers. That makes up the triad. The US
has a triad. In many cases, it's not going to be the silos that would respond, really.
It would be subs. When you hear those stats, we have more nuclear arms than it would take
to blow up the world 15 times over. The reason that's true is because we have subs, like a
a single sub that can destroy an entire country.
The purpose of this is, let's say all of the silos on the land get taken out by some secret
surprise weapon.
The subs are still secret.
They're hidden, which means the United States still maintains deterrence.
A lot of countries operate triads.
And that is a major component to stopping first strikes because there's no way you're
going to get it all.
If somebody launched against the United States, they wouldn't be able to take out all the
silos, all the bombers, all the subs.
Therefore, they would have to deal with the response.
And the same thing is true in reverse.
where the deterrence comes from.
So the fact that one part of one part of the triad, the launchers, when it comes to the
land-based stuff, the fact that China has more launchers isn't really a huge deal.
I don't think it's worth starting an arm trace over, but I have a feeling that Congress is
not going to see things that way.
So yeah, I would expect the United States to spend a whole bunch of money modernizing
its nuclear capabilities, because of this report.
But it isn't really a big deal.
It isn't something that is putting you in jeopardy right
now, because the other pieces of the triad
still maintain deterrence.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}