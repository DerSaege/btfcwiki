---
title: Let's talk about Texas and public integrity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=t6HsXG_muhk) |
| Published | 2023/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the Texas Attorney General, Paxton, being under a corruption investigation handled by federal prosecutors in Texas.
- The investigation was recently transferred to the Public Integrity section of the Department of Justice in D.C.
- People are wondering if this transfer indicates an imminent indictment for Paxton, but Beau clarifies that it doesn't necessarily mean that.
- Beau mentions that federal prosecutors in Texas believed they had enough for an indictment, but the transfer doesn't confirm this.
- The transfer may be due to potential conflicts between federal prosecutors and state law enforcement assets in investigating a statewide office like the attorney general.
- The Public Integrity section specializes in going after political figures involved in alleged crimes.
- Beau speculates that the transfer could be related to a civil settlement rather than immediate plans for an indictment.
- He notes that if an indictment was imminent, federal prosecutors in Texas might have handled it themselves.
- The ongoing investigation into Paxton's corruption has been happening for a while, and the transfer doesn't guarantee an indictment.
- In summary, Beau shares thoughts on the situation without confirming any impending actions.

### Quotes

- "It doesn't necessarily mean that it's indictment time, that's not how it works."
- "The transfer doesn't mean they won't be indicting him soon either."

### Oneliner

Beau clarifies the transfer of the Texas Attorney General's corruption investigation to D.C. doesn't guarantee an imminent indictment.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor updates on the investigation (implied)

### Whats missing in summary

The potential implications of the transfer and ongoing investigation into the Texas Attorney General.

### Tags

#Texas #AttorneyGeneral #Corruption #Investigation #DepartmentOfJustice


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Texas and the attorney general and some
developments there that have led to people asking whether or not something
is right around the corner for a whole lot of people, the same thing that
everybody has been waiting for when it comes to Trump, there have been a lot
people waiting on that for the Attorney General of Texas as well. So if you don't
know, the Attorney General of Texas, Paxton, has been under investigation, it's a
corruption investigation, and the investigation was handled by federal
prosecutors in Texas. It was recently transferred to the Public Integrity
section of the Department of Justice, that in DC.
People are asking if the transfer of that case to the public integrity section means
that like an indictment is like right around the corner.
No, not necessarily.
It doesn't really mean that.
To be honest, it's kind of weird that it hadn't been transferred before.
speaking when it's a statewide office, especially one that the prosecutors in the state might
have to work with, like the attorney general, it gets transferred there almost immediately.
So it being transferred there now doesn't really mean anything in and of itself.
It should be noted that according to the AP, the prosecutors, the federal prosecutors
in Texas believed that they had enough for an indictment.
But that in and of itself doesn't mean that that's why it was transferred.
This may just be a moment when they realize the conflict that could occur between federal
prosecutors who often have to use, you know, state and local law enforcement assets, investigating
the attorney general of that state.
That could lead to a lot of uncomfortable conversations, I'm sure.
So it's been transferred to D.C., we'll see what it means.
The public integrity section has a pretty lengthy history, and this is what they do.
They go after political figures who are wrapped up in some kind of alleged crime.
I would point out that Smith, the counsel looking into Trump, used to have this section.
It's kind of their gig.
recently kind of settled a civil aspect of something that was kind of related to
the overall corruption investigation and he also, if I'm not mistaken, he still has
a 2015 indictment out there for securities, for something to do with
securities. So the transfer of this to the public integrity section, it might
have something to do with the civil settlement that appears to have occurred.
That may be a more likely reason for it to be transferred than, you know, they're
ready to indict. I would think that if they were ready to indict, they would
have let the federal prosecutors in Texas just do it themselves. But we'll
have to wait and see how it plays out because while it doesn't necessarily
mean that it's indictment time, that's not how it works, it doesn't get
transferred there for an indictment, that doesn't mean that they won't be
indicting him soon either. That investigation has been going on for a
So, anyway, it's just a thought.
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}