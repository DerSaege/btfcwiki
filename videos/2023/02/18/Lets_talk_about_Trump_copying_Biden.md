---
title: Let's talk about Trump copying Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Q3YDuftA3_A) |
| Published | 2023/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is copying Biden's strategy for dealing with possible primary opponents within the Republican party by dividing the party over issues like cutting Social Security, Medicare, and Medicaid.
- Biden baited the Republican Party during the State of the Union into booing the idea of cutting social safety nets, setting the stage for Trump to make a similar move.
- Trump's plan involves sparking debate among his opponents by coming out in favor of drastic cuts and fundamentally reshaping social safety nets.
- This strategy is considered politically smart because Trump will not have to defend his previous stances against popular safety nets like Medicare and Social Security.
- Trump will force his opponents to answer for their previous positions after the Republican Party as a whole has moved away from drastic cuts to social safety nets.
- Despite the divisive nature of the strategy, it is seen as one of Trump's shrewdest political moves, allowing him to maintain a popular position without directly attacking Medicare or Social Security.

### Quotes

- "Trump is copying Biden's strategy for dividing the Republican Party over issues like cutting Social Security, Medicare, and Medicaid."
- "This strategy is considered politically smart because Trump will not have to defend his previous stances against popular safety nets like Medicare and Social Security."

### Oneliner

Trump is copying Biden's strategy to divide the Republican Party over social safety net issues, a politically shrewd move that avoids defending unpopular positions on Medicare and Social Security.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Analyze political strategies and their potential impacts on social safety nets (implied)

### Whats missing in summary

The full transcript provides more context on Trump's political strategy and how it compares to Biden's tactics, offering insight into the dynamics within the Republican Party.

### Tags

#Trump #Biden #RepublicanParty #SocialSafetyNets #PoliticalStrategy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about how Trump is copying Biden.
Trump has figured out his strategy for dealing with possible primary
opponents within the Republican party.
And it's playing off of Biden's move at the State of the Union.
If you missed it, during the State of the Union, Biden kind of, he baited the Republican Party into
booing the idea of cutting Social Security, Medicare, Medicaid, stuff like that, pointing to
to a plan that existed within some sections of the Republican party.
Shortly after Nikki Haley launched her campaign officially, an email went out from Trump World
attacking her for her previous support of curtailing Social Security, Medicare, and Medicaid.
Trump's plan is to duplicate what Biden did.
He plans to divide the Republican Party over this.
As some sections of the Republican Party are calling for drastic cuts and fundamentally reshaping some of the social
safety nets  and programs that have existed in this country for a very long time, Trump is going to come out in favor of
them.
and he will spark debate among his opponents.
It is, it's actually a really smart move, politically it is a very smart move
because they will, they will have to answer for their previous stances
and they're going to have to do it after the Republican Party as a whole
in Congress booed their previous stances and as attitudes have shifted over time
and even a lot of Republicans want these kinds of safety nets.
This may actually be the smartest play, politically, that Trump's ever pulled
off, because he never really wanted to come after Medicare or Social Security.
It wasn't one of his planks.
It wasn't something that he was adamant about.
He doesn't have a huge record of calling for it to be destroyed.
So allowing his opponents to battle it out over that, when he has had the position that
is currently popular, it's pretty smart.
It is.
shrewdest political move the man's ever made. I do think it's funny that he came
with this move by watching Sleepy Joe. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}