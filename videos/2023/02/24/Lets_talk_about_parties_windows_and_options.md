---
title: Let's talk about parties, windows, and options....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=h6XiJh2_lQ8) |
| Published | 2023/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Historically, the lack of a left-wing party in the US can be attributed to the Cold War era propaganda against left-leaning ideologies.
- As demographics shift and younger generations grow up without Cold War indoctrination, there is more acceptance of left-leaning ideas.
- The two main parties in the US are the Democratic Party, considered center-right or centrist, and the Republican Party, which has shifted to the extreme right.
- Third parties in the US struggle to gain traction because they are outside the established range of acceptable political thought, mainly center to center-right.
- Lack of focus on local races, extreme candidate selection, and platforms outside the acceptable range contribute to the failure of third parties to succeed electorally.
- A party centered around social democracy, left by US standards but not truly leftist, could potentially gain ground in the future.
- Fear of far-right authoritarianism in the US leads many to vote for the Democratic Party out of self-defense, even if they do not fully support it.
- Until the threat of far-right authoritarianism diminishes, efforts to establish a social democratic or left-leaning party face significant hurdles.
- Building a social democratic movement within the Democratic Party may be a more feasible approach than starting a new party from scratch.
- Overcoming the challenges of propagandized views, historical traditions, and misconceptions about party ideologies is key to establishing effective left-wing representation in the US.

### Quotes

- "Their main issue is that they're outside the Overton window."
- "They're not what people think really have the power, but they do."
- "It's a fight for its life against far-right authoritarianism."
- "They're voting for the lesser of two evils."
- "A lot of hurdles for a left-wing party in the United States."

### Oneliner

The lack of an effective leftist party in the US stems from Cold War propaganda, fear of far-right authoritarianism, and challenges with third-party positioning and representation.

### Audience

Political activists and organizers.

### On-the-ground actions from transcript

- Mobilize at local levels within existing political parties to shift ideologies towards social democracy (suggested).
- Focus on building infrastructure and winning local elections over a 10-15 year period to establish effective left-wing representation (implied).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the obstacles facing the establishment of a successful left-wing party in the US and offers insights into potential strategies for overcoming these challenges.

### Tags

#USPolitics #LeftWingParty #SocialDemocracy #ColdWar #OvertonWindow


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the United States,
the political landscape, political parties,
and third parties, and the left and the rights,
and windows, and all kinds of things.
Because we got a question from Germany.
And on some level, it's amusing.
But I feel like there are probably a lot of people
in Europe who have the same doubts when they hear an American commentator say we don't
have a left-wing party in the United States, and it prompted some interesting questions.
So I live in Germany and have heard you and others say the U.S. does not have a leftist
party.
With a friend, I looked at the political positions of your two main parties.
then at the others. Why doesn't the U.S. have an effective leftist party? There seems to
be support for left ideas judging by commentary online." Yeah and all that makes sense. Okay,
so the reason the U.S. doesn't historically have a left-wing party is the Cold War. If
If you are my age or older, left leaning ideologies, they were propagandized against a lot during
the Cold War.
They became insults.
They became bad words, groups of people that were coming to get you type of thing.
Things are changing and the demographics that grew up without that propaganda, without that
indoctrination, they're now voting age.
And you're starting to see more people be accepting of left-leaning ideas.
Now we have the two main parties in the US.
You have the Democratic Party, which is really, by your definition, center-right.
Maybe just centrist, depending on what country in Europe you're at.
And then you have the Republican Party, which not too long ago, it would have been your
conservative party.
Today, it's your extreme-right party.
We have third parties that exist, parties outside of those.
Their main issue is that they're outside the Overton window.
Every country has a range of acceptable political thought.
And for the most part, the range that is established is center to center right.
That's the US.
Over the last few years, the far right has gained traction, and they have been capable
of actually getting electoral success on some of it for a little bit.
The left-leaning ideas are gaining traction too, but that hasn't converted into a lot
of electoral success yet.
The third parties that exist, they're too far outside of that to gain any ground.
And they tend to not put a lot of effort into local races, which they are not races that
get headlines.
They're not what people think really have the power, but they do.
it helps build a structure, a party, that could actually run somebody successfully on the national
scene. Instead, most try to run candidates for key positions, like president. And when they do it you
also end up with a habit of choosing a voice from within that third party that's kind of extreme,
that is at least on the extreme side of that party.
So not just is the party platform outside of the overton window of the United States,
the candidates that they're putting up on the national scene
tend to be even further outside of the normal accepted range of political view.
That's why they don't really succeed, those reasons.
Not building the ground game at the local level, running candidates that are even further
outside of the Overton window, and then the party platforms themselves are outside of
the acceptable range of thought.
Now as far as commentary online, yeah, it's changing.
It's changing.
But you run into another issue.
I would suggest that a party built around social democracy, which is left by US standards.
It's not actually leftist.
It's still capitalism, but it's acknowledging that there's major problems with capitalism
and dressing it up and all of that stuff.
I would imagine that that would be a successful platform.
Maybe as soon as 2028, that could be something that could win.
The issue that remains is, I don't know if you see it from Europe, but the US, it is
a fight for its life against far-right authoritarianism.
And until that's defeated, even people who might go for a social democrat, they're going
to vote for the democratic party out of fear.
They're going to be voting out of self-defense.
There are a whole lot of people who vote for democratic candidates who don't actually like
them.
They're voting for the lesser of two evils.
They're voting for the one that would do the least harm.
You actually hear it framed as harm reduction.
And until that far right threat is gone, that's what people are going to do because they're
not going to want to split that vote.
So it's the Republican Party, the right-wing party in the United States has to catch up
to everybody else.
They have to move left a little bit.
They have to progress.
They have to at least go back to George Bush republicanism before the democratic party
or a social democratic party can really start to move left.
I would suggest that a group of determined people who were going to launch a social democracy
based party wouldn't really even need to start their own party.
If they got active at the local levels and they built the power structure within the
Democratic party, they could actually shift it over that much because it's not a huge
jump because again, you're not crossing into actual leftist territory yet.
I think that could be done and it would probably be easier than trying to build the infrastructure
and fight all of the money that the Democratic Party already has.
Because then you'd be fighting them both.
And during the construction of that Social Democrat Party, you're going to be helping
the Conservative Party.
Because you're going to be pulling votes from them unless you can focus on getting out the
vote in bringing young people who don't vote because they don't want to vote for the lesser
of two evils.
They want somebody to actually support.
If a social democracy party focused on getting those voters out, and there's a lot of them,
they might be able to do it that way.
There's just a lot of hurdles for a left-wing party in the United States, and a whole lot
of it has to do with propaganda from the Cold War.
They're not being a historical tradition of having a party like that.
And also, I mean, keep in mind that a lot of Americans, I mean, they think the Democratic
Party is a left.
So I mean, they view it as a leftist party, even though it's not by any definition.
And it doesn't help because commentators like to conflate the two.
So that's why this exists.
Now I think it will change within my lifetime.
I think you'll see an actual, at bare minimum, a social democracy-styled party emerge in
the United States, or the Democratic Party get moved that way.
One of those two things I expect to see in my lifetime.
Moving further than that, it's possible if people started now and understood that they
would have to win those local elections and build that infrastructure and they're looking
at a 10 or 15 year fight to get to the point where they even matter, where they even register.
I don't know that there's an organizing effort to do that in the US, not yet anyway.
Right now, most people are focused on self-defense.
They're focused on voting, not for the candidate they want, but to stop the harm.
So if you're in Europe, and you hear that from American commentators, we don't have
a leftist party, an effective one.
We don't.
We actually have a few leftist parties in the United States.
They just don't win.
They don't get seats.
So that they don't accomplish a lot other than occasionally they'll get somebody who
resonates with people and the Democratic Party will pick up some of those ideas.
Not enough to make them qualify as leftists, but some of the general policy ideas.
And that, even if you're not winning, if your ideas are getting picked up, I mean to me
that's success. So I don't want it to sound like I don't think third parties
are a good idea because a lot of times those parties champion ideas that end up
getting sucked in to the Overton window and it helps people. So anyway it's just
Just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}