---
title: Let's talk about when the boss shows up and photos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nVW5Nm3pugY) |
| Published | 2023/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the impact of the boss or inspector showing up at a worksite, like a construction site, causing productivity to slow down due to safety concerns and increased attention to the visitor.
- Mentions how health inspectors at restaurants also lead to decreased productivity as workers focus on compliance during inspections.
- Draws parallels with white-collar jobs where the presence of a big boss results in more attention towards pleasing them rather than focusing on tasks.
- Points out the confusion between a photo op and actual leadership, where people expect politicians to show up for appearances even though it may not contribute meaningfully.
- Criticizes the expectation for politicians to attend photo ops as a measure of leadership, citing examples such as visits to disaster areas like the border.
- Emphasizes that true leadership involves ensuring disasters don't reoccur rather than just appearing for photo ops.
- Condemns the diversion of resources towards security and logistics when high-profile figures visit disaster areas, stating that their job is to prevent such incidents from happening again.

### Quotes

- "Their actual job should be to make sure it doesn't happen again."
- "It shows how little Americans actually expect of their leadership, of the people in government."
- "And when people start to confuse the two, we end up in a situation where they're running around doing photo ops and they're not doing their job."

### Oneliner

Beau explains how the focus on photo ops over actual leadership diverts resources and hinders disaster response efforts, reflecting low expectations from Americans regarding their elected officials.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Hold elected officials accountable for meaningful actions rather than performative gestures (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the detrimental effects of prioritizing photo ops over genuine leadership in disaster response situations.

### Tags

#Leadership #PhotoOps #DisasterResponse #Expectations #Accountability


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about what
happens when the boss shows up, or the inspector,
and how that should inform our thinking about something.
Because there's a common trend in the United States,
and it's pretty telling when it comes to our expectations
and how we confuse things at times.
OK, so you're on the job site.
You're at a construction site, whatever, and OSHA shows up.
Is that a productive day?
Is the productivity that day the same as when OSHA isn't there?
No, of course not, because everybody's
running around looking for the safety equipment they never
actually use, right?
Or they're afraid to do what they would normally
do because it's more than three feet off the ground.
What about a health inspector at a restaurant?
Same thing, right?
Productivity slows down.
I mean, so much so that at least around here,
the health inspectors actually try
to time their visits for the slow periods.
So they don't disrupt business as much.
I'm sure it's true with white collar jobs as well.
The big boss comes in and everybody is more interested in catering to that person than
they are doing their own jobs, right?
Do y'all think that Mayor Pete actually knows how to clean up that mess from that train?
No, of course not, because that's not his job.
That's not what he's there for, right?
Same way that the ocean inspector can probably not do the jobs of the people who got slowed
down.
Same way the health inspector doesn't know how to cook that dish.
It's not their job.
But for whatever reason, people in the United States have begun to confuse a photo op with
actual leadership to the point that people get mad when a politician doesn't show up
for a photo op, even though the photo op is really, it's just political campaigning.
It doesn't actually help.
In fact, it slows things down.
This isn't just about the train.
We've seen this before.
You had Republicans saying that Biden needed to go down there and visit the border.
to do what? If the people in charge of securing the border down there, if they
need the President of the United States to show up and pick up a pair of filled
glasses, they're probably not very good at their job. It's one of the few things
that I defended Trump on because he didn't show up. Like, never. Like, that
wasn't a thing. I mean, eventually he would, you know, show up to an impacted
area and like throw paper towels at people or whatever. But I defended him on
it after, I think it was Michael. He's been like, he didn't come down. He
shouldn't. When you're talking about elected leaders at that level, when they
show up, what happens? They need security, right? Those cops, the EMS, the fire
trucks, the helicopters, those are super in need after a disaster. You don't want
to waste the resources for a photo op. And I've talked about this before, but one
of the things that's really important to remember is that the standard for
Americans and what they expect as far as actual leadership when it comes to our
elected officials has fallen to the point where we get mad if they don't
show up for a photo op that they're gonna use in a campaign ad later. Like
that's the extent of what we're expecting. Or we've been so
conditioned to believe that that actually is leadership, that that's
helping. It's not. It's not. Their job is in an office somewhere. They're not
supposed to be there standing around, slowing things down. And we've seen this
happen. We've seen governors do tours of impacted areas and slow down relief
efforts. It occurs, but if they didn't show up, you'd have people complaining
that they didn't show up because we've grown so accustomed to the photo op.
It shows how little Americans actually expect of their leadership, of the people in government.
They don't actually want a solution.
They want them to show up and stand around, put on some high visibility clothes and carry
a shovel or something.
That's not actually helping.
And when people start to confuse the two, we end up in a situation where they're running
around doing photo ops and they're not doing their job.
Their actual job should be to make sure it doesn't happen again.
Not stand there and pretend like they're fixing the soil.
And this is an issue, one of the few issues you can honestly say, this is on both sides
of the aisle.
Politicians, elected leaders, people with political designs, showing up after some emergency.
Not only is it not leadership, it's not helpful, because it diverts resources from where they
actually need to be.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}