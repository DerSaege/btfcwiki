---
title: Let's talk about posture, China, the US, and more bases....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jn_wwXOraGY) |
| Published | 2023/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States has gained access to new military bases in the Philippines.
- The move is in response to the escalating tensions with China.
- Despite concerns about a new Cold War, the number of troops involved is relatively low.
- The bases are likely to be used for special purposes like surveillance.
- Both the US and China seem to believe that the conflict will remain cold or lukewarm.
- This move strengthens the US team in the region, including Korea, Japan, and Australia.
- The firming up of sides increases stability and predictability in the region.
- The focus of any potential conflict is likely to shift to Africa.
- China and the US are already active in Africa, mainly through soft power.
- The lack of strong reaction from China indicates a desire to avoid a direct confrontation.
- China aims to expand its national interests without engaging in large-scale wars.
- Despite the positive aspects, many question the need for the US to have more military bases.

### Quotes

- "The U.S. getting its team together."
- "Once everybody picks a side, any contests will occur elsewhere."
- "China has a record of trying to avoid war."
- "The U.S. does not need more bases."
- "Just a thought."

### Oneliner

The United States gains access to new military bases in the Philippines amid escalating tensions with China, signaling a shift in geopolitical dynamics towards stability in the region and a potential focus on Africa.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor developments in US-China relations and their impact on global stability (implied).
- Stay informed about geopolitical shifts and alliances in the Asia-Pacific region (implied).
  
### Whats missing in summary

Analysis of the implications of US-China relations on other countries in the region.

### Tags

#US #China #Philippines #Geopolitics #MilitaryBases #Stability #Africa


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about posture.
We're going to talk about posturing, and access,
and the Philippines, and the United States, and China,
and what the news means, what it means long term,
what we can pull away from the headlines that are coming out.
This is being presented in a couple of different ways
in the U.S. media, which is to be expected at this point.
There is good news and bad news in it.
So first, what's happening?
The United States has basically gotten access to a few new bases, three or four new bases
in the Philippines.
Philippines is going to allow the US to access these military bases.
Good news, bad news.
Starting with the bad news.
For a couple of years we have talked about the coming near-peer contest with China.
Yeah, we're in it.
We are in it.
And that's not good news.
The good news that goes along with this is that if you were around during the first Cold
War and you hear something like three or four new bases, you are picturing tens of thousands
of troops, a lot of them.
It's not like that.
That's not what's going on.
We're talking about incredibly low numbers of troops.
This is good, not just because the U.S. getting more bases is generally not great, but it's
good because of what it kind of signals.
It looks like these bases are going to be set up for the U.S. to use for special purposes,
surveillance, stuff like that.
The troops there may also, may, I don't know this, may also be tasked with making sure
it's ready to accept a large number of troops if something goes sideways. But, the fact that
these bases aren't being set up to hold a whole bunch of troops and have them stationed there,
that's good news. It means that the United States and China, by default, because of their reaction,
Both, they both believe that the new Cold War, if you want to call it that, is it's
going to remain cold or at best lukewarm.
That's good news.
Geopolitically what's happening, the US is getting its team together.
That's what's occurring.
There have been other less publicized moves dealing with, I want to say Guam, and I think
both Japan and Korea have some shuffling going on there.
But what this does, and the Philippines were kind of like a gap, but for the United States,
It builds a network of countries from Korea and Japan all the way down to Australia that
are on the US team, so to speak.
The firming up of the sides is actually good.
Stability and something like this and predictability is good news.
The bad news is that once everybody picks a side, that means that any contests will
occur elsewhere.
Elsewhere being Africa.
We've talked about that for a couple of years too.
Both the United States and China are already active there, both mainly using soft power.
The US definitely always has the US military there to back it up, but for the most part,
it's soft power now.
is also active through their contractors. They're not doing well there. They're
about to learn the lessons that the US learned in the 90s. But I would imagine
that by the time all of this shakes out, the two competitor nations vying for
control and influence in Africa are going to be China and the United States.
that is going to be what the Middle East was, you know, for quite some time, for as
long as most people watching this can remember. It's gonna shift. That's where
the important stuff is now. So that's a good rundown of it. The lack of just
This total outrage from China is good.
It demonstrates clearly that they don't really want to fight either.
They want to maintain their posture, they want to maintain their national interests,
they want to expand their national interests the same way the United States does.
But China has a record of, for the most part, trying to avoid war, especially like big ones.
So that's all good news, even though it's coming on the back of the US getting new bases,
which for pretty much everybody watching this channel, one thing most people can agree on
is that the U.S. does not need more bases.
So that's kind of a look behind the headlines because the headlines are framing this in
a bunch of different ways, but that's a look at the overall picture.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}