---
title: Let's talk about the 5th Circuit Ruling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L-HiSXQJg7g) |
| Published | 2023/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Fifth Circuit ruling allows those with restraining orders for domestic violence to have guns, based on historical precedent.
- The Department of Justice plans to further review the ruling, indicating the issue may reach the Supreme Court.
- The Supreme Court will have to determine if historical precedent is necessary for modern laws like the Bruin test.
- The stakes are high as a ruling allowing individuals with DV restraining orders access to firearms could result in lives lost.
- The decision could impact not only those directly affected but also the Second Amendment itself.
- If the court rules in favor of pro-gun individuals, support for repealing the Second Amendment may surge.
- This case represents a common-sense regulation with significant implications for many individuals.
- Given the current climate and connections between DV history and mass incidents, the decision holds great importance.
- Returning firearms to individuals with DV histories may fuel increased support for repealing the Second Amendment.
- The situation poses thought-provoking considerations in light of ongoing debates on gun rights.

### Quotes

- "A ruling saying that those with DV restraining orders are eligible for firearms, the cost of that will be counted in people lost."
- "If the court decides to go with the pro-gun side, undoubtedly they will declare victory, and then they will watch in absolute horror as support for repealing the Second Amendment increases."
- "This is one of those common sense regulations that people talk about."
- "If the Second Amendment crowd decides to go this route and wants to return firearms to people in this situation, I would imagine that the support for totally repealing the Second Amendment is going to shoot through the roof."
- "Y'all have a good day."

### Oneliner

The Fifth Circuit ruling allowing those with domestic violence restraining orders to possess guns could lead to a Supreme Court battle with far-reaching implications for lives lost and Second Amendment support.

### Audience

Legal experts, activists

### On-the-ground actions from transcript

- Contact legal advocacy organizations to stay informed and potentially participate in actions challenging the ruling (suggested).
- Engage in community dialogues about gun regulations and domestic violence prevention (implied).

### Whats missing in summary

The full transcript provides additional context on the legal intricacies and potential societal impacts of the Fifth Circuit ruling, offering a comprehensive understanding of the situation. 

### Tags

#FifthCircuit #SupremeCourt #DomesticViolence #SecondAmendment #GunControl


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about a ruling coming out
of the Fifth Circuit, and what happens next,
and what is likely to happen after that.
And we'll kind of go over how we got to this point.
If you missed it, the Fifth Circuit has determined that those who have a restraining order for
domestic violence against them, well, they should be able to have guns.
This ruling was based on the idea that the Supreme Court set down that a modern law should
have a historical precedent with a law from the time.
And that since it doesn't, well, then it can't be a law today.
The Department of Justice is saying
that they are going to take this for further review.
Short version is this is going to end up at the Supreme Court.
And at that point, the Supreme Court
going to have to decide whether or not the Bruin test, the idea that it has to have that
level of historical precedent, is really what they meant.
The Department of Justice relied on the idea that because there were laws at the time that
said dangerous people could have their arms removed, that that was a good enough analogy.
The Fifth Circuit decided it wasn't.
So the Supreme Court is going to have to make this determination.
They're going to have to decide whether or not they want it to be this strict.
And the stakes of this decision couldn't be higher.
Not just for the people who will be directly impacted by this.
no mistake about it, a ruling saying that those with DV restraining orders are eligible
for firearms, the cost of that will be counted in people lost.
So there is that impact, and then there is the impact to the Second Amendment itself.
If the court decides to go with the pro-gun side, undoubtedly they will declare victory,
and then they will watch in absolute horror as support for repealing the Second Amendment
increases, and increases, and increases.
This is one of those common sense regulations that people talk about.
This is one that will have a direct impact on a whole lot of people.
The Constitution can be changed and moving forward from that with everything that's going
on right now, with the climate in place, with the connections from having a DV history and
mass incidents.
If the Second Amendment crowd decides to go this route and wants to return firearms to
people in this situation, I would imagine that the support for totally repealing the
Second Amendment is going to shoot through the roof.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}