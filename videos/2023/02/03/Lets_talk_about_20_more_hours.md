---
title: Let's talk about 20 more hours....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gLJT5QnhjOc) |
| Published | 2023/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shelby County prosecutor indicated 20 more hours of police footage in Memphis involving Mr. Nichols.
- Ben Crump, representing the family, anticipates receiving the footage soon, suggesting it may reveal critical information.
- Beau expected there to be more footage than what was initially released due to missing commentary post-incident.
- Unreleased footage likely includes statements from other officers present, shedding light on their mindset and actions.
- The additional footage may implicate or exonerate the officers involved based on their commentary.
- The focus of the released footage was on the incident, but body cam footage typically includes more context.
- The 20 hours of footage may contain irrelevant content like dash cam footage or officers' idle moments.
- The importance of releasing all footage to provide a complete picture and connect the dots, as stated by Crump.
- Beau believes the additional footage is unlikely to exonerate the officers who engaged in violence.
- Seeking justifications or rationalizations for police violence is not productive; acknowledging the systemic issue is necessary.

### Quotes

- "Stop looking for a way to make this okay."
- "In some situations, if you look hard enough, you can create a rationalization for it. But even a rationalization doesn't mean that it's justified."
- "At some point, the American people have to look at what law enforcement has become in this country and face it and understand what's happening."
- "We just didn't get the footage."

### Oneliner

Shelby County prosecutor revealed 20 additional hours of police footage in Memphis; unreleased commentary may implicate or exonerate officers, urging to stop seeking justifications for police violence.

### Audience

Advocates for police accountability

### On-the-ground actions from transcript

- Contact local officials to demand transparency and accountability in the release of all relevant footage (suggested).
- Support organizations working towards police reform and accountability in your community (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of the importance of complete footage in understanding incidents of police engagement and the need to address systemic issues within law enforcement.

### Tags

#PoliceFootage #Transparency #PoliceAccountability #SystemicIssues #CommunityAction


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about 20 more hours of footage in Memphis.
And what we can expect to be on it.
And what it might do for the situation up there and the way everything is proceeding.
If you missed it, the Shelby County prosecutor indicated to CNN that there were still 20
hours or so of footage of the police engagement with Mr. Nichols.
The prosecutor seemed to indicate that when and if it would be released would be up to
the PD and the city.
Now, Ben Crump, the attorney who is representing the family, seems to be under the impression
that they will get it relatively soon.
And he has indicated that he thinks it might connect some dots.
I think he's right.
During the live stream, I'm pretty sure it was during the live stream, I said that I
expected there to be more footage, that we weren't getting everything.
And since this happened, I've had a bunch of messages about why did I say that.
Because it's too opportune.
It's too clean.
What we got did not include commentary that occurred after the fact.
It did not include commentary from other officers who were standing around.
Those statements, the statements that are going to be on the film, are going to shed
a lot of light on the mindset of some of the people involved and of some of the other officers
who were there.
That evidence can, for the other officers, it's either going to implicate or exonerate
them in some way.
The other officers and their commentary as to what they saw, that's something that is
probably going to be analyzed a lot.
If you had an officer talking about, man, what did they do, that's going to look really
bad for the cops.
I would imagine that there's probably commentary like that there.
When it came out, it was laser focused on showing the incident.
That's not how body cams work.
That's why I knew there was more footage.
There will probably also be a bunch of footage that's completely irrelevant, dash cam footage
or footage of officers walking around doing nothing and saying nothing.
That's why it's 20 hours.
Don't expect all 20 hours of this to matter.
It may boil down to 15 minutes, 2 minutes here, 30 seconds here, back and forth of all
of this footage that is going to, as Crump said, connect the dots.
It's good that the department is releasing it, appears to be ready to release it.
It's not good that they didn't include it to begin with.
If you're going to release something like this, you should put it all out there.
You should put it all out there.
I did get one question from somebody who apparently hasn't watched all of the videos I've done
on this, asking if I think anything on the new footage is going to exonerate the officers
involved.
As far as the ones who actually engaged in the violence, no, not at all.
I cannot envision a scenario in which that was okay.
I do not expect that.
It didn't even cross my mind until I got the message.
If there's one thing that everybody needs to learn from this, stop looking for justifications
for stuff like this.
In some situations, if you look hard enough, you can create a rationalization for it.
But even a rationalization doesn't mean that it's justified.
And I can't even imagine something that could be used to rationalize this.
At some point, the American people have to look at what law enforcement has become in
this country and face it and understand what's happening and then acknowledge it's been happening
a long time.
We just didn't get the footage.
Stop looking for a way to make this okay.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}