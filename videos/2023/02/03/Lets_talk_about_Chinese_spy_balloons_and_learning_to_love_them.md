---
title: Let's talk about Chinese spy balloons and learning to love them....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xuVk1zpp94k) |
| Published | 2023/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation of a Chinese spy balloon floating over the United States in the Northern US, causing some concern.
- Notes that surveillance flights, whether exotic like balloons or conventional like planes or satellites, are common occurrences near or over US airspace.
- Mentions the historical context of near-peer contest resembling a second Cold War and references the U2 incident involving Gary Powers during the first Cold War.
- Comments on China launching three spy satellites in November, which didn't receive much media attention.
- Speculates on the effectiveness of spy balloons compared to traditional methods like satellites and raises concerns about vulnerability to disinformation campaigns.
- Suggests that the only people who should be worried are Chinese counterintelligence, as the US's apparent nonchalance may indicate prior knowledge of the balloon's capabilities.
- Talks about triggering overreactions in counterintelligence through strategic responses and dismisses the current situation as a non-issue amplified by slow news.

### Quotes

- "There's only one group of people that should be worried about this."
- "It's not a big deal. Happens all the time."
- "Don't worry about it."
- "We're not moving into. We are in a second Cold War now."
- "There's really nothing to this."

### Oneliner

Beau explains the presence of a Chinese spy balloon over the US, downplaying concerns and attributing significance to Chinese counterintelligence worries.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Stay informed on international relations and historical incidents like the U2 incident (implied)

### Whats missing in summary

Deeper insights into historical parallels between current events and past conflicts.

### Tags

#SpyBalloon #SurveillanceFlights #InternationalRelations #ColdWar #Counterintelligence


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about balloons.
We're going to talk about balloons and flights
and concerns that you probably shouldn't have.
Right now, I know this is all over the news
and there are a lot of people
who are definitely drumming up a lot of worry.
Worry?
You don't need to be worried.
There's only one group of people
that should be worried about this.
Okay, if you've missed the news,
right now the United States is tracking a Chinese spy balloon
that is floating gracefully over the United States
up in the Northern US.
There are a lot of people
who are trying to create a lot of drama with this.
This happens all the time.
Surveillance flights go over this country daily,
multiple times per day.
It's not a big deal.
One way or another, whether it be something like this,
kind of exotic, or a plane, or a satellite,
surveillance flights occur all the time,
either right up against our airspace
or over our airspace or whatever.
This isn't something to be concerned about
in any way, shape, or form.
Not just is it not a rarity today,
this has been going on a really long time.
As we have talked about numerous times on the channel,
we are moving into a near-peer contest, a second Cold War.
If you wanna know what it's gonna look like,
look to the first one.
History doesn't repeat, but it does rhyme,
and this is definitely carrying the same tune.
If you weren't around back then,
Google U2 Gary Powers, the letter U, the number two,
and the name Gary Powers.
That will show you how long this type of stuff
has been happening.
And yeah, the US does it too.
Every major power does.
Undoubtedly, the news that China put up,
what, three Y-35s, UGAN-35s, check the name,
which are a type of spy satellite,
they put up three of them in November,
barely made the news,
because it's not really that big of a deal.
I know that there will be pundits saying that,
well, the balloons are gonna be more effective
because they don't have to get through the atmosphere
and it'll be better, more effective.
Mm, I don't know about that.
I mean, the images might be clearer,
but I don't know that they would be more effective.
Who remembers the movie Patriot Games?
Back in the day, Harrison Ford movie.
There's a scene in it where the bad guys
are all training in the desert, right?
And then an alarm on a watch goes off
and they all go inside
because that's when the satellite's passing over.
If you know it's there, it's not more effective.
In fact, it can be a detriment to the country operating it
because you could hypothetically inflate fake vehicles
to make it look like numbers are larger than they are.
You could move planes from one installation to another
to make it appear that they're stationed somewhere
that they're not.
It makes you vulnerable
to a lot of intentional disinformation campaigns.
Yeah, the only people who need to be worried about this
are Chinese counterintelligence.
Guaranteed in the last 24 hours,
there were conversations
between Chinese counterintelligence officers
trying to figure out whether or not the US
is just engaged in like the ultimate power move
or they have a real problem
because they've got to figure out
why the United States found out about it,
looked at it and was just like, meh.
If you are Chinese counterintelligence,
you have to wonder why they don't want to study it.
The US doesn't want to study it.
Why not get it out of the sky and take a look at it?
If you are Chinese counterintelligence,
there is only one answer.
And that's because they already know everything about it.
They already know its capabilities.
Now, it could just be that the US already knows
or it could be the US doesn't know
but understands it doesn't really pose a risk.
So they're acting like they do
in the hopes of triggering an overreaction
within Chinese counterintelligence,
which sometimes in a lot of countries
can lead to that country losing very productive people
because if they believe there was a leak,
oh, they're gonna find the source of that leak.
Even if one never existed.
This isn't a concern.
It's not a big deal.
Happens all the time.
It's a slow news week.
There's not a lot of new high drama news.
So this is getting a lot of coverage.
That's really what this is.
We're moving into...
We're not moving into.
We are in a second Cold War now.
If you weren't around back then,
it would really probably put you at ease
to read a lot about it.
Overflights like this, standoffs with ships and subs,
all of this stuff, it's coming.
Make no mistake about it.
And in many cases,
the media is going to be less than helpful,
especially if it occurs in a period
when there's not a lot of new drama
because they've got to keep those writings up.
There's really nothing to this.
Don't worry about it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}