---
title: Let's talk about men changing and steps....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LIV2M7n6ESE) |
| Published | 2023/02/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau revisits the topic of change, reflecting on a previous video about toxic masculinity and the desire for transformation.
- The individual is contemplating making amends but faces challenges in seeking forgiveness from those they have hurt.
- Reference is made to Reverend Ed's forgiveness after someone broke into his church, illustrating the concept of forgiveness being for oneself rather than the wrongdoer.
- The idea of making a public apology video and starting a YouTube channel to document the journey of change is discussed.
- Beau cautions against using apologies and forgiveness as manipulative tools and the potential backlash of starting a channel for clicks.
- The importance of documenting the journey of change is emphasized, suggesting recording daily thoughts and reflections for personal growth.
- Advice is given to wait until significant progress is made before releasing documented content to avoid discouraging others on a similar path.
- Beau explains the significance of showing change through actions over time rather than simply declaring it verbally.
- The focus is on helping others change and staying committed to the goal of transformation throughout the process.
- The importance of maintaining a genuine intention to assist others in their journey towards change is reiterated.

### Quotes

- "You can't tell people that you're changing, you have to show them, and it takes time."
- "Every day, record. Record every single day."
- "Don't release it like as a daily thing."
- "You are not the same person you were six months ago. You are not going to be the same person in six months."
- "You will be able to reach people that nobody else can because you speak their language."

### Oneliner

Beau revisits the concept of change, discussing the challenges of seeking forgiveness and the importance of documenting a genuine transformation journey to inspire others.

### Audience

Individuals seeking guidance on initiating and documenting personal change.

### On-the-ground actions from transcript

- Document your journey of transformation daily (suggested).
- Focus on genuine actions rather than verbal declarations to inspire change in others (implied).

### Whats missing in summary

The full transcript provides a detailed guide on navigating personal transformation through genuine actions and documentation.

### Tags

#Change #Forgiveness #Transformation #DocumentingJourney #PersonalGrowth


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about change.
And we're going to check in on somebody
that we talked about a lot last week or maybe the week before.
If you remember, there was a series of videos
that was prompted when a person who was just
immersed in content that promoted just toxic masculinity,
they sent in this message.
And it was clear they wanted to change.
And it was talking about some observations and some
questions that they had.
Thanks for this response video, or thanks
for the response videos.
Somebody in the comments said I should use the 12 steps from AA.
I am not religious, but try to apply it as best I could because I want to change.
As far as making amends and getting forgiveness of the four I've contacted to try to apologize
and explain, not one would talk to me.
They were pretty rude, but I guess that's to be expected.
and forgiveness guilt trips are things that commonly get used to manipulate people.
So I was thinking about making a video on YouTube to apologize and say that I'm changing.
Maybe even make a channel to document my journey.
But then I think about the pressure of a channel and I'm not sure.
I'm also curious about how to make amends or get forgiveness if I can't even talk to
them.
I got this shortly before I tuned in to Reverend Ed's live stream, if you don't know, he's
on YouTube.
Somebody had broken into his church, taken his AV equipment, and right away he was just
It's like they obviously needed something that was more than they thought we could give.
And basically kind of indicated that he had already decided they weren't going to press
charges and stuff like that.
Instant forgiveness, right?
But see, the person, they don't know that.
The person who did it, they don't know that.
The kind of forgiveness that you're looking for here, or somebody's like, I forgive you.
That's not for them, it's for you.
It helps you.
And most people are not like Reverend Ed, most people aren't, and you're not entitled
to it.
But that's okay, it really is, because from everything that you've said, that's not really
what you're looking for.
You want to change and you want to help other people change from everything that has been
discussed.
The forgiveness part of it, that's not, it's not necessary to do that.
You may just have to deal with it, carry it with you.
Now, as far as making an apology video, starting a channel, I like the idea, but I have some
questions about how you plan to do it.
Making a video like that, I get it, you know, coming out and saying, you know, I'm sorry
I did all of these things.
I was involved in this toxic culture and, you know, I'm changing.
Okay.
But as you said, apologizing and forgiveness guilt trips are things that are commonly used
to manipulate people.
Some people are going to take what you say in that video at face value.
Some people are not.
And there's going to be a backlash.
It's a good way to start a channel, though.
It would provide a lot of clicks right away.
But that's not why you're doing it, right?
At least not what you've indicated.
In those 12 steps, when it's talking about making amends,
I'm pretty sure that it has like a caveat saying
that you do that as long as it doesn't hurt anybody.
The obvious people that might be hurt by this in some way are going to be the people who
see it and just write it off as, great, some other guy just saying he's changed.
They're not going to believe it.
There's good reason for that.
But that's the obvious group of people that would be mad, be upset.
There's another group of people that I think you'll care more about, honestly, that you
could really be injuring if you do that.
You described it as a journey.
You know you're not done.
You want to document the journey.
You understand that you still have much more to do, right?
But you've come a good way from where you were.
I want you to think back to when you were just starting to leave that community.
When you were seeing those tweets over on Ask Aubrey and it was all starting to click.
You make the decision to start to get away.
You're contemplating it and you see a video from somebody just like you who's saying that
they're sorry and who's changed and in the comments there's all that backlash.
What do you think?
Don't think as you today, who actually has changed, think of you then.
What do you get?
Stuck up, right?
You get that or, see, this is what I mean.
Nice guys.
This guy's trying to do it their way, this is why he shouldn't even do it.
You will discourage people, guaranteed.
It will happen.
Somebody just like you will be discouraged from going down the road that you are currently
on if you do this.
I'm almost guaranteed.
That being said, I like the idea of documenting your journey.
I'm going to tell you something I wish I had done during several phases of my life.
Every day, record.
Record every single day.
When you have a thought on that topic, you record it.
If you don't have any thoughts on that topic, you record that day anyway.
And maybe once a month, once every three months, something like that, you go back and you watch
them all and you provide commentary.
You're going to provide commentary on the days before anyway as you're recording.
It will happen because, I mean, think about it.
You got the advice to go do the 12 steps, right?
You would make a video about it the day that you made that decision, and then the day that
you tried, you would make a video describing how it went, which was not well.
So you would see the changes.
You would see you going down that journey.
It's all recorded, but none of it's published yet.
Wait until the very end.
You know, you said in one of the earlier messages, you said that you'd pretty much given up
dating because you're just not you're not there and you know you're not there
it's the right move that's the right move starting a channel that would
eventually turn into you giving advice you're not there yet either go through
this journey record it do exactly everything that you're saying that you
want to do, but don't release it until you're comfortable dating. Until you're
at that point where you think that you've really got a handle on it. Then
cut it all together, release that. That would be a much better first video and
And you don't run the risk of injuring people along the way.
Think about the 12-step thing.
Think about that.
That first day, you make that video and you publish it.
Say, this is how we're going to get out of this.
We're going to make amends.
And hopefully, your video reaches people.
and a small percentage of those, they're going to believe it.
They're going to like the idea.
A small percentage of those, they're going to act on it.
So if you put that out there, what you end up doing
is encouraging a bunch of guys to call women
that don't want to talk to them.
It's not, this isn't something that you can do piece by piece.
And you have to show the attempt and the consequence, how everything worked out.
Don't release it like as a daily thing.
You'll put a bunch of bad information out there.
You are not the same person you were six months ago.
You are not going to be the same person in six months.
That's the whole point of this.
That's a better way to do it.
You run a lot less risk of discouraging or preventing change in the people that you want
to help change.
And that's how you make amends.
Trust me, if you can convince one or two guys to break away from that, I would be willing
to bet that the four that do not want to talk to you, they may still not want to talk to
you, but they would probably view that as immense, stopping it from happening to somebody
else.
And when it comes to coming out and saying, well, I'm changing, nobody cares, I don't
want to be mean.
You can't do it that way.
You can't tell people that you're changing, you have to show them, and it takes time.
And even once you do it, to a whole lot of people it still won't matter, but that's
not why you're doing it, right?
If you go down this road, you have to process all this stuff out of time.
You're not doing it for those reasons, you're doing it to help, you're doing it to change
people. You have to focus on that and keep everything that you're doing focused on that.
I think it's a good idea. I think it's a great idea really because the fact of the
matter is you will be able to reach people that nobody else can because you speak their
language.
If you were to document a year's worth, it would probably be very effective at doing
what you want.
And it would, I would imagine that it would count, at least to you, as immense, fixing
the problem, helping other people get out.
And that seems to have been your goal from the beginning.
But if you're going to do that, everything has to focus on what's going out and how effective
it's going to be.
Which means you can't do anything that even though it'll make you feel better, you can't
put your feelings over the goal of helping people change.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}