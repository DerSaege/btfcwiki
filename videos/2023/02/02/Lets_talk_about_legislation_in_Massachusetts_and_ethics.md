---
title: Let's talk about legislation in Massachusetts and ethics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8RD9MxMrIMU) |
| Published | 2023/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Massachusetts proposed legislation allows incarcerated individuals to reduce their sentence by donating bone marrow or organs, sparking controversy.
- The legislation offers a 60 to 365-day sentence reduction for eligible individuals who donate.
- A committee will determine which offenses are eligible, focusing on nonviolent crimes.
- Beau questions the ethical implications and coercion involved in exchanging organ donation for sentence reduction.
- He views the proposal as dystopian, raising concerns about the authorities coercing individuals into donating.
- Beau believes this legislation may not progress due to potential federal legal issues surrounding coercion and value exchange.
- He expresses hope that the federal government will intervene if necessary to prevent this proposal from moving forward.

### Quotes

- "How much is your family's love worth to you? What would you give up to get the hugs and kisses of your children again?"
- "This is pretty dystopian, if you really think about it."
- "This seems incredibly coercive."
- "I'm pretty sure this is against federal law."
- "I hope that this does not move forward."

### Oneliner

Massachusetts proposes controversial legislation allowing incarcerated individuals to reduce sentences by donating organs, prompting ethical concerns and potential federal legal issues.

### Audience

Legislators, activists, policymakers

### On-the-ground actions from transcript

- Contact local representatives to express opposition to the legislation (implied)
- Join advocacy groups working on prison reform and ethical treatment of incarcerated individuals (implied)

### Whats missing in summary

Beau's detailed analysis and ethical considerations can be best understood by watching the full segment. 

### Tags

#Legislation #PrisonReform #OrganDonation #Ethics #FederalLaw


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about some legislation,
some very unique legislation coming out of Massachusetts
that is certain to cause a stir.
And this is one of those things, when people first hear it,
they might be like, hey, that's not a bad idea.
And then as you dive into it, you
realize there's a lot of questions
that need to be answered.
To summarize it, the bone marrow and organ donation program
shall allow eligible incarcerated individuals
to gain not less than 60 and not more than 365 day
reduction in the length of their committed sentence
on the condition that the incarcerated individual has
donated bone marrow or organ or organs.
OK.
Yeah, that's kind of out there.
And for those who may think that this is a good idea,
start with this.
There's going to be a committee that determines
the eligible offenses.
It's bad political optics to release violent offenders early,
and certainly through this method.
So this would apply to nonviolent offenders.
This would be an offer provided by the authorities to them.
This isn't legislation.
It's the plot to a movie that comes out around Halloween.
Imagine the commercial to it.
How much do you love your family?
In the not too distant future, you
were found to be in possession of plants
the authorities said you shouldn't have.
Because of that, you will spend the next three years
in this cage.
To your left is a form and a pen.
If you sign the form, you get to go home a little bit early.
But the authorities will remove a piece of your body.
How much is your family's love worth to you?
What would you give up to get the hugs and kisses
of your children again?
Coming this October, informed consent.
Yeah, I mean, this is pretty dystopian,
if you really think about it.
This is not a good move, like at all.
There's a whole lot of ethical questions
here that I'm not sure that people in the prison system
are really going to be up to dealing with.
It is worth noting that this is probably not something that's
going to move ahead, regardless of the support that may or may
not exist in Massachusetts.
I haven't seen anything detailing that.
But I'm pretty sure this is against federal law.
I would imagine that a year of your life
would be considered consideration,
I think is the terminology they use.
Basically, you can't do this.
You can't give something of value in this way.
This seems incredibly coercive and in all likelihood
would really be something that's only available to those people
who probably didn't need to be separated from society
to begin with.
I would hope that this does not move forward,
and that if it does, the federal government
steps in on this one.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}