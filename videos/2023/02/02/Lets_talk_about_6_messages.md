---
title: Let's talk about 6 messages....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=s50TQn_oQEg) |
| Published | 2023/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining how creators develop video arcs and use context clues.
- Addressing the misconception of alpha and beta masculinity and the flawed science behind it.
- Sharing a personal experience of a concerning cab ride in Miami.
- Challenging the idea of a "traditional wife" and high expectations in relationships.
- Encouraging individuals to have high expectations for themselves if they seek a partner with similar standards.
- Offering insight into the dynamics of a relationship based on mutual high expectations and support.

### Quotes

- "Not just in my experience, but if you look at the comments, happens all the time."
- "If you want a woman that has high expectations, you have to have them too for yourself."
- "You're probably very different."

### Oneliner

Beau provides insight on dating, addresses misconceptions about masculinity, shares a concerning cab ride experience, challenges traditional relationship ideals, and encourages mutual high expectations for a healthy partnership.

### Audience

Dating individuals seeking clarity.

### On-the-ground actions from transcript

- Challenge misconceptions about masculinity and relationships (implied).
- Encourage mutual high expectations in relationships (implied).

### Whats missing in summary

Engaging with Beau's full transcript offers a deeper understanding of dating dynamics and societal expectations surrounding relationships. 

### Tags

#Dating #Masculinity #Relationships #Expectations #Challenge


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk a little bit more about dating.
After that last video, I got a number of messages from people,
and we're going to go through them.
One is an actual question.
I'll save that one till the end.
The others are comments that maybe reading them
and responding to them can provide a little bit of insight.
So we will just dive right into this.
Put on your hazmat suits.
You are such a beta, and a disingenuous one at that.
That video is fake.
They set those scenes up.
If you had reviewed the source channel,
you would have known that.
Previous videos give a hint.
They're called context clues.
Anybody who believed that was real was a beta anyway.
OK.
Remember that last part as we go through all of the messages
from your compatriots.
Anybody who believed it was real was a beta anyway.
Yeah, I am familiar with how channels run.
A lot of creators actually develop arcs.
They plan out arcs where videos are connected to each other.
And sometimes they'll put out a video
to kind of lead into something else.
Like as an example, if somebody was
to use a video that had a questionable origin,
they might put out a video talking
about videos of questionable origin like 12 hours before.
If they did that, it would be a context clue.
You're right.
OK.
Coming to the rescue of a woman because a guy asked her not
to embarrass him is a total beta move.
You're living the hashtag beta life
and are failing to display dominance
and will always be sad and lonely.
OK.
Yeah, I get it.
I get it because in your perception of being
an alpha male as opposed to a beta male,
that theory revolves around displaying dominance
over women.
Right?
That's what it is.
You might want to go back and look into the origin
of that theory and actually learn the dynamics.
And you may want to spend a little bit of time
after that learning about the person who first proposed
the theory and the fact that it was really bad science
because it was based on a bunch of wolves
that weren't pack members thrown into captivity together.
And it greatly altered their behavior.
And he spent the rest of his career
basically disproving his own theory because it was that bad.
What is so bad about what he did?
I understand not every woman is going to be into it.
But is it really that bad?
He didn't actually hurt her.
They just learned they weren't right for each other.
Isn't that a good date?
Both can move on.
Yeah, OK.
A long time ago, I was in Miami.
I was in Miami.
I'm coming out of this place.
And even though my hotel's only like 2 and 1
half, 3 miles away, I know I should not drive.
So as I'm coming out, I make the decision to walk.
But when I get outside, there's a cab there.
And it's like one of the cool old ones.
So I talked to the driver for a second.
I'm like, hey, can you run me down there?
He's like, yeah, I got you.
I get in.
And we're not even out of the parking lot, the drive,
the roundabout.
And I realized that maybe he shouldn't be driving either.
And I'm probably a block and a half down the road
before I'm certain of this fact.
And I'm like, hey, why don't you stop, let me off here.
And he's like, no, that's OK.
I'll get you to the hotel.
I'm like, no, man, I just want to go into this other place.
And he's like, no, I'm fine.
I got it.
I'm like, oh, great.
This is wonderful.
So I start planning my escape.
I roll down the window.
And he stops at a red light.
So I open the door, get out, and start walking away.
And he gets out and starts yelling
that I owe him like $5 or whatever.
And I just continue walking.
And he follows me yelling.
And eventually, probably a block from his vehicle,
he is right behind me.
And I turn around and I'm like, go ahead, call the cops.
What are you going to do?
And at that moment, he had a moment of clarity.
And he turned around and went back to his vehicle.
And I walked the rest of the way.
Nobody was harmed.
I was not harmed during that cab ride.
I wouldn't consider it a good one, though.
And it is worth noting that even after I got out of the cab,
it wasn't over.
And that happens a lot to women who
are dealing with certain personalities when
it comes to dating.
I would also suggest that if your bar for a good date
is nobody was physically harmed, you
might want to raise the bar a little bit.
Let's see.
Oh, here we go.
You are a beta dog, not his term.
Encouraging people to rescue some whiny woman who was just
shown a structured time.
Structured time is a really sanitized way
to say being controlled.
But a whiny woman who is being shown a structured time,
an alpha wouldn't have taken for your stupid ruse
and wouldn't have let you interrupt.
And I get it.
I get it because alphas are like tough guys
and it's all about dominance and all of that.
The weird thing is that they did, though, right?
That's the weird part.
Not just in my experience, but if you look at the comments,
happens all the time.
And they do allow the interruption.
I'm sure there's nothing to read into there.
That doesn't mean anything at all.
And even if it does mean something,
I'm sure it doesn't apply to you.
You're probably very different.
The video is as fake as your green screen.
It was put out by women to cast men in a bad light.
Nobody does that.
It's not a green screen.
Nobody does that, except for the guy who sent the message
saying, hey, I used to do that.
And all of your compatriots here defending it.
And all of the women who are like, hey,
this happens all the time.
And all of the men who have seen it, you're right.
Nobody does that, except for all the times they do.
It might be more likely that it's a situation where
you don't recognize your own behavior
because you have maybe been around a bunch of people
that told you that it was OK.
So you don't see it the same way.
You might want to do a little bit of inventory on that.
If you really believe that this doesn't happen,
you probably do it.
OK.
I follow you and like a lot of your takes.
But no offense, you're old AF.
It was probably way different when you were dating.
Women don't want to be like your wife.
I would give anything to find a stay-at-home horse mom.
Today, they want to have careers so they go to college
and suffer from liberal indoctrination.
And they want men to have everything
they offer traditionally and put up
with not having the rewards because the women want
to have their own lives.
And I'm not even saying that's wrong.
But their expectations are too high,
and they're too controlling.
Because controlling is a bad thing now.
Instead of saying, this won't work,
why don't you tell us what will?
Here's your challenge.
Tell us how to get a woman like your wife.
You aren't the only man who wanted a trad wife.
That would be a better service.
You're being mean, not his term, to people who only
want the same thing you do.
Women today have high expectations, unlike my wife.
Tell me you've never met my wife without telling me
you've never met my wife.
But that's good.
That's good.
Your wife is actually supposed to drive you
crazy every once in a while.
That's supposed to occur.
Why would you want a woman with low expectations, low ambition?
Why would you want that?
When you phrase it that way, all of a sudden,
it casts a lot of doubt on this philosophy.
And then as far as controlling, one of my wife's favorite
sayings, and it's a joke, but she says it a lot,
is that men are the head of the family.
And she will admit that men are the head of the family.
And then immediately follow it up with, and women are the neck.
Really put some thought into what that means.
You make a move with your head, without your neck.
And then the final part here, tell us
how to get a woman like your wife.
So what you're going to do is look for everything
that you just criticized.
That's what you're going to do.
You're going to look for somebody with a career who's
college educated, and is in fact so busy that when y'all decide
to have your first date, it takes weeks,
because you're both busy, because you too
have high expectations.
And you have a lot of things going on as well.
So it takes weeks just to get together for that first date.
And it's so hard to get the schedules together,
because you haven't started making time for each other yet,
that the first date actually consists of her bringing you
coffee and you hanging out where you work.
The trad wife that you have created via, I guess,
a narrow glimpse through social media
could also be described as a former captain who's
prior enlisted, who has more than one degree, who
is an RN, ER, ICU, TNCC, CCAT, or CSTARS.
I don't.
ICU in the sky.
Yeah.
That's what you're looking for is what you're criticizing.
Your image of what you want, it's not real.
It's not.
That's not real life.
And if you actually want a completely passive partner,
you're not actually looking for a trad wife, a traditional wife,
or whatever, however you want to frame it.
You're looking for a JPEG that moves.
You want to find somebody like that?
Forget everything that they've taught you.
None of that's real.
That's not real life.
And yeah, you should absolutely want a woman
with high expectations.
And I am certain that what you may deem as too controlling
could also be called being supportive or helping
motivate you.
The thing is, if you want a woman that
has high expectations, you have to have them too for yourself.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}