---
title: Let's talk about a laptop and a change of strategy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PRWiHAQ6fi0) |
| Published | 2023/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hunter Biden's attorneys sent letters requesting a criminal probe into the handling of his laptop and accusing individuals like Giuliani and Bannon of violating federal laws.
- Hunter Biden, being the President's son, may prompt the Department of Justice to look into the matter, although it's uncertain if a criminal probe will be opened.
- The focus for DOJ will be determining whether any crimes were committed in relation to the laptop's access and dissemination.
- Many involved individuals openly discussed their activities surrounding the laptop, potentially aiding in DOJ's investigation.
- The allegations involve computer crimes, but it remains unclear if federal laws were actually broken.
- Hunter Biden's decision to speak out marks a change from his previous silence on the matter.
- Despite baseless allegations against him, Hunter Biden remained quiet until now.
- The ongoing saga surrounding the laptop is expected to have more developments in the future.
- The story may experience periods of intense activity followed by silence, resurfacing periodically.
- Allegations made by prominent individuals without any factual basis could be considered defamatory.

### Quotes

- "The Hunter Biden is done being quiet."
- "This is an unforeseen development in this ongoing saga."
- "They are probably going to do other things as well."
- "There are a number of allegations that have no basis."
- "It's just a thought."

### Oneliner

Hunter Biden's attorneys request a criminal probe into his laptop, leading to unforeseen developments in an ongoing saga with potential future activity and baseless allegations.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Monitor developments in the investigation (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the legal implications and potential future developments surrounding Hunter Biden's laptop controversy.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a couple of letters and a laptop and an apparent change
of strategy.
You know for the last few years if you said the phrase laptop everybody knew who the owner
was.
You didn't have to be real specific.
And the owner has been very, very quiet for the most part.
That has changed.
There has been a change of strategy.
Attorneys for Hunter Biden have sent out some letters.
The first is one to Fox, basically saying hey you need to retract the idea that our
client had access to classified material that he wasn't authorized to have, blah blah blah.
That one we could have seen coming.
The other one is a letter to the Department of Justice requesting a criminal probe into
the handling of his laptop and asking for an investigation into individuals for whom
there is considerable reason to believe violated various federal laws in accessing, copying,
manipulating and or disseminating Mr. Biden's personal computer data.
There are some names mentioned.
They're the ones you would expect, Giuliani, Bannon, people like that.
That's an interesting turn of developments.
It really is.
Now if you or I were to send a letter to DOJ or have our attorneys send a letter to DOJ,
there is no guarantee, there's not even a high probability, that they're going to open
a criminal probe.
But Hunter Biden's not a normal person.
He's the President's son.
I would imagine they're at least going to look into it.
And what they're really going to have to determine is whether or not it's a crime.
The facts of the case and what happened and the fact that it was accessed and disseminated,
a lot of that's not really in dispute.
Many of the people involved in this little caper were very proud of their activities
and talked about them repeatedly in a bragging fashion.
So DOJ really isn't going to have to work too hard on that end.
I mean, the New York Post knows most of it.
What they're going to have to really determine is whether or not these activities actually
broke the law.
Now some of the allegations, I mean, it's all about computer crimes.
And as we have talked about on the channel, federal laws about this type of stuff, they're
wild because they're very dated and the sentences are just, they're huge.
But we have no idea at this point if a crime actually took place by the federal definition.
This suggestion that they transported stolen goods across state lines, all of this stuff,
and all of this stuff in the letter, we discussed it when all of it happened.
And there were questions then about whether it was even legal.
The Hunter Biden is done being quiet.
There are tons of allegations.
Many of them are baseless.
We've gone through quite a few on the channel that we actually went through and fact checked,
and there's no real basis for them.
And you never got a press release from him saying this isn't true and this is why.
He just was quiet.
This signals a change.
I don't think this is the end of their moves.
They are probably going to do other things as well.
And it's a story that is probably going to have a flurry of activity, then disappear,
and nobody's going to talk about it for a while, and then in like six months it'll pop
back up again, assuming DOJ does open a probe.
It is a very unique turn of events, and I don't think it's over.
I think there's more to it.
I think there will be more coming because there are a number of allegations that were
made by pretty prominent people that have no basis.
They have no basis.
Some might consider those allegations defamatory.
So this is an unforeseen development in this ongoing saga about a laptop that entered the
political circus.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}