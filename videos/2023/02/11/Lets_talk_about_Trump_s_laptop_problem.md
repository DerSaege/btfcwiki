---
title: Let's talk about Trump's laptop problem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Je1xylcbvcA) |
| Published | 2023/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice found new documents and devices in the Trump document case.
- A thumb drive and a laptop were discovered with classified documents copied onto them.
- The aide who copied the classified documents seemed unaware of their content.
- Beau speculates on how the copying could have occurred accidentally.
- The aide may have been a photographer involved in taking routine photos.
- Beau questions the plausibility of classified documents not being visibly marked.
- Beau expresses minimal commentary on the situation.
- Documents with classified markings were reportedly copied twice to an aide's insecure computer.
- People might be more concerned about Trump's laptop than Hunter Biden's.
- Beau concludes with a simple farewell message.

### Quotes

- "I'm fairly certain that people are going to be more concerned with Trump's laptop than Hunter Biden's."
- "Well, howdy there, internet people, it's Beau again."
- "Doesn't make any sense, but those are the photos."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau provides insights into new developments in the Trump document case, including the discovery of classified documents on a laptop, sparking speculation on how they were copied accidentally, and potential concerns over Trump's laptop compared to Hunter Biden's.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Stay informed about developments in the Trump document case (exemplified)
- Advocate for secure handling of classified information (implied)

### Whats missing in summary

More detailed analysis and implications of the potential mishandling of classified information.

### Tags

#Trump #ClassifiedDocuments #DepartmentOfJustice #PoliticalAnalysis #Speculation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the new developments
in the Trump document case.
I'm sure that you, like most Americans,
believe there wouldn't be any new developments in that case.
But there are.
So reporting suggests that the Department of Justice
is now in possession of a few more documents that
were found after the search and were turned over,
as well as a thumb drive and a laptop
that, according to reporting, had classified documents
copied to them.
So the reporting does suggest that the aide,
who copied the classified documents,
was unaware of what they were copying.
I know that there are going to be a lot of people who
are wondering how you could do that,
because generally speaking, documents
with classified markings are, well, marked.
I have come up with one possible scenario
in which it could legitimately have occurred by accident.
the aid is reported to have been a part of the pack.
Maybe they were a photographer.
I want you to picture your standard run-of-the-mill
politician-at-work photo at the desk, pin in hand, and a
bunch of printed documents.
I mean, for real, that's what they normally are.
Doesn't make any sense, but those are the photos.
if the documents that just happened to be nearby
or classified documents, I could see how
it could occur in that manner.
I'm not sure how it could occur in another manner.
But I mean, maybe.
I don't really have a whole lot of commentary on this one.
I mean, to be clear in, in relation to an investigation about whether or not
former president Donald Trump mishandled sensitive information, there has been a
development which is reported to be that some documents bearing classified
Markings were copied twice to an AIDS computer that was not secure.
And according to one set of reporting, was also not at his club.
Yeah, I mean, that's pretty much commentary in and of itself.
I'm fairly certain that people are going to be more concerned with Trump's
laptop than Hunter Biden's, because I'm fairly certain that Hunter Biden's
laptop didn't have classified documents on it.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}