---
title: Let's talk about the House GOP hearing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Z_dQYqMVxlA) |
| Published | 2023/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in the U.S. House of Representatives held a hearing to pin something on Hunter Biden, but it backfired.
- Testimony revealed no evidence of Twitter censoring the Hunter Biden laptop story to help the Democratic Party.
- Republicans believed false claims from right-wing commentators and scheduled hearings based on misinformation.
- Twitter received requests from Republicans and others to censor posts, debunking claims of colluding with the Democratic Party.
- The hearings are an attempt to relitigate the 2020 election outcome and cast doubt on Trump's loss.
- Despite the hearings, nobody is watching or taking them seriously, except for those who already believe the false narrative.
- The Republican Party's hearings are likely to continue to fail as long as they rely on misinformation and propaganda.

### Quotes

- "The only people who are interested in this are people who already fell for the idea that it exists."
- "Almost every hearing that you have announced is going to go down exactly like this one."
- "Expect to see a whole lot more of this because it's gonna happen again."

### Oneliner

Republicans' attempts to push false narratives through hearings based on misinformation are backfiring, with no one watching or taking them seriously.

### Audience

Politically Aware Individuals

### On-the-ground actions from transcript

- Fact-check misinformation spread by political figures and news sources (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how Republican hearings based on misinformation are failing to achieve their intended goals and are not being taken seriously by the public.

### Tags

#Republicans #Misinformation #Hearings #HunterBiden #Twitter #Propaganda


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Republicans in the U.S.
House of Representatives and their show and how things are going.
And we're, we're going to talk about believing your own stuff and how they
They certainly should have known that this is how it was going to go.
If you have no idea what I'm talking about, I'm not surprised because nobody watched it.
But they held their first hearing.
This one was part of the series that is apparently going to be trying to pin something on Hunter
Biden.
I don't actually know what they're really trying to look into here.
the first hearing was supposed to showcase how Twitter was like at the
command of the Democratic Party and how they, you know, censored everything to
help him and all of this stuff. And that's what they told people they were
going to see. What was the testimony? I am, I am aware of no unlawful collusion
with or direction from any government agency or political campaign on how
Twitter should have handled the Hunter Biden laptop situation. That's Twitter's
former deputy counsel. Testimony very, very similar to that came from a whole
bunch of people. This was very surprising to a lot of Republicans, particularly
those who have been asking me since the quote Twitter files came out why I
wasn't covering it and this especially goes out to the person who was down
there for like two weeks saying day 20 whatever of Bo covering up the Twitter
files. This is why I didn't cover it because I read it. So they believed that
they were going to get all of this information about how Twitter censored
the Hunter Biden laptop story. That's what Republicans believed. Why? Because a
whole bunch of right-wing commentators said that was true, and the Republicans
in Congress believe them. It is worth remembering that the person who put the
Twitter files out said, there is no evidence that I've seen of any government
involvement in the laptop story. Why couldn't they find it? Why didn't anybody
at Twitter admit it? Because there's never been any actual evidence to
suggest that occurred. They made it up. They got caught in their own propaganda
feed and they believed it. So how is a hearing supposed to run if you were
around during the sixth hearings, you remember me saying a certain phrase over and over again.
Tell them what you're going to tell them, tell them what you told them, right?
What has happened on day one of the Republican hearings?
Tell them what you're going to tell them, oh no, that's not what anybody said.
This is going to be repeated over and over again because a lot of the people involved
in these hearings, they consume what they believe is journalism, and it's really just
right-wing commentary, and they accept it as fact, and they have literally scheduled
congressional hearings about it.
You can expect multiple instances of these hearings just wildly backfiring.
So in addition to them not demonstrating what they set out to demonstrate, we also found
out that Twitter basically kind of has like a whole database of requests from Republicans
to censor posts.
The framing was that it was Twitter colluding with the Democratic Party to censor stuff
that was objectionable to those in power.
No, I mean, pretty much everybody in power made these kinds of requests.
One that I found particularly interesting was from the Trump White House, because Trump's
feelings apparently got hurt when somebody said mean and hurtful things
about him and called him a, I don't even know how to soften this, a PAB, we will
just abbreviate, and that means weak. And he really didn't like that. Now, if that
revelation had been made during the January 6th hearings, it would have
trended all over social media, that phrase, but it didn't. Why? Because nobody's
watching it, because nobody cares, because nobody believes them. The only people who
are interested in this are people who already fell for the idea that it
exists. That's it, nobody else cares because anybody who looked into it saw
it for what it was, selectively released documents to paint a narrative, and the
biggest claim that arose wasn't even supported by the documents.
Um, so you can expect a whole lot more of this.
It is also worth noting that it was discovered that despite claims of Twitter
being like super on the side of the democratic party, they altered the rules
to allow Trump to stay on longer because as soon as he said, go back to where you
came from, well that should have been it. Their whole thesis was blown out of the
water on day one because they have no idea what they're doing and because they
consumed a bunch of right-wing propaganda and mistook it for journalism.
Expect to see a whole lot more of this because it's gonna happen again. The good
news is that nobody's watching it. There aren't a lot of people watching it. I mean, other than,
weird people like me for humor's sake. The other thing to remember is why these hearings are taking
place. What are they really doing with this? The suggestion is that this laptop story would
have altered the outcome of the 2020 election. Even as Mike Pence gets his subpoena to testify
before grand jury, they're still, they are still trying to relitigate this and cast the
idea that Trump really should have won.
That's why nobody cares.
And this is something that is going to haunt the Republican Party as long as it goes on.
I'm going to go ahead and pro tip for the Republican Party.
Almost every hearing that you have announced is going to go down exactly like this one.
It's going to go that badly.
You might want to rethink.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}