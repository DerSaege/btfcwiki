---
title: Let's talk about sentences and journalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CQFLVtlOJIQ) |
| Published | 2023/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the intricacies of federal sentencing in the U.S. system, shedding light on common misconceptions.
- Emphasizes the discrepancy between maximum sentence and actual sentencing guidelines in federal courts.
- Mentions that journalists often focus on maximum potential sentences, creating unrealistic expectations for the public.
- Talks about the impact of criminal history and offense levels on sentencing outcomes.
- Provides examples of how different factors like criminal history and offense levels can affect sentences for individuals charged with the same crime.
- Points out the importance of understanding federal sentencing guidelines to accurately anticipate potential sentences.
- Raises awareness about journalists sometimes inaccurately predicting sentences based on maximum penalties rather than sentencing guidelines.
- Illustrates how sentences can drastically change based on new information during criminal proceedings.
- Clarifies that sentences in the federal system typically run concurrently for multiple charges, not consecutively.
- Stresses the need for accurate information dissemination, especially in cases involving law enforcement and civil rights violations.

### Quotes

- "Journalists end up setting expectations way too high when they talk about the maximums rather than the sentencing guidelines."
- "Because of a court case called Booker, I think, they pay attention to something called the Sentencing Guidelines."
- "The downside is in cases where, let's say it's a cop who violated somebody's civil rights, and the people in the street, they hear that he's looking at 10 years, maximum of 10 years, but then the judge sentences them to three. It's because that's what the guidelines said."
- "Generally speaking, the person has to have basically caught all of the enhancements and have a huge criminal history to get to the maximum."
- "You can't accurately guess what the sentence will be. You can't determine the range until the criminal proceedings are done."

### Oneliner

Beau clarifies federal sentencing guidelines, stressing the importance of understanding them to set accurate expectations, especially in cases involving law enforcement and civil rights.

### Audience

Journalists, Legal Advocates

### On-the-ground actions from transcript

- Understand federal sentencing guidelines and accurately convey information to the public (implied).

### Whats missing in summary

Detailed examples and explanations of how criminal history and offense levels impact federal sentencing outcomes.

### Tags

#FederalSentencing #Journalism #LegalSystem #CriminalJustice #CivilRights


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about sentences
in journalists, but not the writing kind of sentences.
We're gonna talk about something else.
Lately, we have been talking a lot
about how journalists convey information
and how they can be better in some ways.
And the thing is, what I'm about to say,
most journalists know, not all, but most.
And when they don't, when they don't convey it accurately, it doesn't help establish
accurate expectations.
And this is something that we are already getting questions about as far as Trump's
laptop, not Trump's laptop, Trump's aides laptop, I guess.
And it deals with sentencing in the federal system.
And this led to a lot of questions during the sentencing for those involved with the
sixth.
And I'm sure it's going to lead to questions when it comes to the seditious conspiracy
thing.
So we're going to talk about how federal sentencing works.
How many times have you heard the phrase, facing a maximum of 20 years?
And then the person walks out with, like, an 18-month sentence.
Does that mean that the judge was soft?
No, no.
The maximum amount is a number established by Congress.
This is the absolute most you can sentence this person to for this crime.
That's not what judges really pay attention to.
Because of a court case called Booker, I think, they pay attention to something called the
Sentencing Guidelines.
The federal courts, they're part of the federal government.
There's a chart.
There's a chart for everything.
The way federal sentences work is every crime is assigned an offense level.
offense level corresponds to a range of months. So on the chart, up and down, you
have the offense level. Going across the top of the chart, you have something
called criminal history category or something like that. And that is
basically their record. Criminal history one means you don't have much and it
goes up to, I think, six.
And where those two columns, where those two axes meet,
that's the sentencing range.
So if it's offense level 24 and then criminal history level 2,
the judge will go down to 24, go over to 2.
And it will present the judge with a range.
Typically, the judge sentences right in the middle of that range.
But they have discretion within that range.
If they go outside of it, they have to provide a reason.
So this is why you have seen people involved with the sixth
have headlines that say, facing a maximum of 10 years
and walk out with 18 months.
because their offense level was low
and they didn't have a big criminal history.
When it comes to some of the seditious conspiracy cases
that are currently going on up there,
you're going to see something even stranger.
You will see people charged with the same thing.
And if they are convicted, then their criminal histories
come into play.
And some of them don't have criminal histories.
Some of them have extensive ones.
Their sentences will be drastically different
for the exact same activity.
Now, as far as the espionage angle,
which is what most of the messages are about,
so I want to say that there are actually
two base offense levels there.
One is just generally gathering national defense information
or something like that.
And then there's another one that's a little bit higher
that is gathering national defense information that
poses a grave threat to US national security
or something along those lines.
Those offense levels, that's what's going to matter,
not the maximum sentence.
And you see this put out by journalists a lot.
They're facing a maximum of X amount of time.
And you have to wonder why.
And the answer is because you can't really guess
until the trial's over.
You can't accurately guess what the sentence will be.
You can't determine the range until the criminal proceedings
are done.
As an example, let's say there's a federal crime that
prohibits you from having plants, OK?
The base offense level, let's say it's 10.
I'm making this up, obviously.
But if you have one to five plants, well, then it's 10.
If you have five to 10, well, then it's going to be 11.
If you have 10 to 50, your offense level would be 13.
50 to 100, okay?
You see how it works?
Now, imagine that during the criminal proceedings,
the person who was arrested with 100 plants,
during one of the hearings,
they find out one of the search warrants wasn't valid.
And that's where half their plants were.
Their sentence just dropped dramatically.
So this is why journalists can't anticipate
the sentence beforehand.
But going to facing a maximum of that doesn't really add up.
Generally speaking, the person has
to have basically caught all of the enhancements
and have a huge criminal history to get to the maximum.
Now, the other thing that comes up is journalists adding stuff.
This occurred with the seditious conspiracy case up in DC.
You had multiple people who were each charged with multiple crimes.
You had some journalists say, well, they're looking at 20 years for this, 10 years for
this, five years for this, so they're looking at 35 years.
Now, in the federal system, generally speaking, the sentences run concurrently.
They will get sentenced on the most serious offense, and everything else runs concurrently.
It runs at the same time.
They don't do it consecutively.
So to directly answer the questions, what would somebody get sentenced for having this
information that they allegedly have on their laptop?
have no clue. There's no real way to know that yet. You would have to wait for all
the criminal proceedings to play out. So, and this is true of the cases involving
January 6th, and it's true of the seditious conspiracy cases. This is true
of everything in the federal system. I hope that clears that part up for
everybody. Because there are the downside to this. It doesn't seem like it's a big
deal, but the downside to it is in cases where, let's say it's a cop who violated
somebody's civil rights, and the people in the street, they hear that he's looking
at 10 years, maximum of 10 years, but then the judge sentences them to three. It's
because that's what the guidelines said.
Journalists end up setting expectations way too high
when they talk about the maximums
rather than the sentencing guidelines.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}