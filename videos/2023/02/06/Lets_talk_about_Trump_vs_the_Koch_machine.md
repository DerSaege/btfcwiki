---
title: Let's talk about Trump vs the Koch machine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cGreGD5Mp0E) |
| Published | 2023/02/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Charles Koch's political machine plans to interfere in the Republican primary for the 2024 election, specifically against Trump.
- The Koch machine aims to shift the Republican Party's direction by opposing candidates, like "Trump lights," who mirror Trump's leadership.
- Charles Koch, a billionaire libertarian, may oppose authoritarian Trump-like candidates based on his ideology.
- Nikki Haley, with close ties to the Koch group, may have increased odds if she runs for office.
- Other potential candidates need to withstand Trump's attacks and navigate the Republican Party's transformation desired by the Koch machine.
- The Koch machine's interference suggests a significant drop in Trump's chances of winning in 2024.

### Quotes

- "The Koch machine is coming out against Trump. That is welcome news, that's welcome news."
- "It takes a lot of work to make coincidences like that happen."
- "Trump's odds of winning just dropped dramatically anyway."
- "It's a very influential group."
- "The Koch machine wants to shift the Republican Party in a new direction."

### Oneliner

Charles Koch's political machine targets Trump and aims to reshape the Republican Party, potentially boosting Nikki Haley's odds.

### Audience

Political activists, Republicans

### On-the-ground actions from transcript

- Reach out to potential Republican candidates to support a shift in the party (implied).
- Stay informed about political developments and candidates' ties to influential groups (implied).

### Whats missing in summary

Insights on the potential impact of the Koch machine's involvement in the 2024 Republican primary elections.

### Tags

#2024Election #RepublicanParty #CharlesKoch #PoliticalMachine #NikkiHaley


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about
Trump versus the Koch Machine.
Sounds like it could be a children's novel.
It's not, it's actually a pretty big development
in the run up to 2024.
The political machine associated with Charles Koch
has indicated that it is it is going to
get involved
and inject itself into the republican primary
that's a big deal
because they've got money
money you're talking about a crew
that'll drop almost seventy million dollars in a midterm
uh...
they said
that they wanted to turn the page it was time for the republican party to turn
the page because they were choosing bad candidates.
It didn't use Trump's name, but it's very clear that's what they mean.
The Koch machine is coming out against Trump.
That is welcome news, that's welcome news.
It's a very influential group.
But it also leaves a lot of questions, like that's the coverage, hey they're coming out against him, good, and that's
kind of where it stops, but it leads us to, there's some follow-up questions that I feel like should be asked.
What about the Trump lights? The copies of a copy? Those Xerox copied governors that mimic his leadership style.
is Coke and company, are they turning the page on them too? And they might be, they
might be. So Charles Coke, he's a libertarian. For Europeans, he's an
American libertarian who's also a billionaire. A billionaire libertarian
in the United States, I mean that basically means somebody who's opposed to
any kind of regulation that stops billionaires from becoming trillionaires.
He's not like libertarian in Europe. No, it's just a bad kind of puppy. But here's
the thing he's he has been weirdly ideological at times and the copies of a
copy those Trump lights they are authoritarian if he is in any way loyal
to the basic premises of his stated ideology he should oppose them he should
oppose them as well. I don't know that he's going to, but it wouldn't be totally
shocking if he did. But for him to do that, he would need to have somebody else
to help out. At least one other candidate, right? If only there was a person who had
a really long history with the Coke machine. They go way back who also already
threw her hat in the ring. Nikki Haley has a lot of ties to this group, to this
crowd. When I was talking about her having access to money and infrastructure,
this is kind of part of what I was talking about. These announcements
coming out in close proximity, I mean maybe it's just a coincidence, but they
go way back. It takes a lot of work to make coincidences like that happen. If
that's the case, her odds just went up, but we'll have to wait and see. We'll have to
wait and see what other candidates announce and how they stack up to
going up against Trump and withstanding the hits that he is going to throw their
way while shuffling the Republican Party in a new direction because that's what
Coke machine wants and that's probably what they're gonna get. Trump's odds of
winning just dropped dramatically anyway it's just a thought y'all have a good
day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}