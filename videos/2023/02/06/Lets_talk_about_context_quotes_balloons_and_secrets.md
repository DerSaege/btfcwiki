---
title: Let's talk about context, quotes, balloons, and secrets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OTVwCh6bAow) |
| Published | 2023/02/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses criticism about being comfortable with secrecy at times.
- Mentions the importance of context and why certain things need to remain secret.
- Talks about Americans' lack of understanding of context and the need for secrecy.
- Provides examples related to surveillance flights and national security.
- Explains why certain information needs to remain classified.
- Talks about the need for deterrence in the context of nuclear weapons.
- Mentions the movie "Dr. Strangelove" as a reference to understanding Cold War mindset.
- Shares Kennedy's speech on secrecy and national security.
- Emphasizes the role of self-censorship for national security purposes.
- Acknowledges that not everything can be disclosed by the military, intelligence community, journalists, and politicians.
- Talks about the limited information shared with Congress and the purpose behind it.

### Quotes

- "The very word secrecy is repugnant in a free and open society." - John F. Kennedy
- "Context, lacking context and understanding why things need to be secret, an absence of those two things, that creates the need for secrecy."
- "If it makes you feel any better, please understand that this also applies to politicians."

### Oneliner

Beau explains the necessity of context in understanding secrecy, citing examples related to national security and the role of self-censorship.

### Audience

Citizens, policymakers, activists

### On-the-ground actions from transcript

- Re-examine standards and recognize the nature of national security (implied)
- Educate others on the importance of context in understanding secrecy (implied)
- Advocate for responsible information sharing in the interest of national security (implied)

### Whats missing in summary

The full transcript provides detailed insights into the nuances of secrecy, context, and transparency, offering a comprehensive understanding beyond a brief summary.

### Tags

#Secrecy #Context #NationalSecurity #Transparency #GovernmentSecrecy #SelfCensorship


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about context and quotes
and transparency and secrecy and balloons
and a whole bunch of things.
A couple of people have pointed out that at times
I'm very comfortable with things remaining secret.
And at times it's criticism,
at times it's surprise,
at times it's trying to figure out where the line is
and all of that stuff.
I have this message and I'm gonna read it
because it has some interesting points in it
and it kind of illustrates something.
So, you said that what the American public doesn't know
is what makes them the American public
and you seemed okay with it.
What happened to transparency?
Remember when liberals would say things like,
the very word secrecy is repugnant
in a free and open society.
That's a Democrat, John F. Kennedy.
You seem to be okay with a Chinese balloon
flying right over and taking pictures of our nuke fields.
You're okay with China knowing our secrets,
but not Americans.
What would you have said if Trump
allowed a surveillance flight over this country?
Okay, when it comes to secrets,
there are a couple of things that are important.
The first is context
and the second is why they're secret.
Generally speaking,
a lot of Americans are really bad at both of these things.
So, it actually ends up in some ways
increasing the need for secrecy
because Americans don't understand what needs to be secret
and they don't know how to put things into context.
Okay, we're gonna do this one in reverse.
I'm gonna start at the end of it and go back.
Okay, what would you have said
if Trump allowed a surveillance flight over this country?
When it comes to stuff like this,
I mean, I know my video titles are weird
and it's hard to find stuff,
but I'm on the record about a whole lot of stuff
and have been for years.
One of my biggest criticisms
when it came to Trump's diplomatic actions
involving treaties
was him destroying the Open Skies Treaty,
which allowed for surveillance flights.
My criticism of him was that he didn't want to allow them
because he doesn't understand why they exist.
You're okay with China knowing our secrets,
but not Americans because, this is being said,
because I seemed okay with a Chinese balloon
flying right over and taking pictures of our nuke fields.
How do you know that's what they were doing?
How do you know they were over a nuke field?
Think about it.
Because you know where they're at, right?
It's not a secret.
It's not a secret.
The location of our nuke fields are not secret.
You can, if you want photos of them,
you can order them on eBay.
So it goes to show that whole thing
that a lot of times people don't know
what needs to be secret.
Photos of what's visible on the ground of a silo,
nobody cares.
It doesn't matter.
But somebody out there threw that word nuke in.
They're flying over our nuke fields.
And it got scary.
Because it wasn't put into context with the fact
that I'm not joking, you can get images on eBay.
They will print them out and send them to you.
You can find it on Google Earth.
Because it wasn't put into context,
the idea of taking pictures of our nuke fields
became very scary.
The reality is we want other nuclear powers
to know about our nukes.
It's deterrence, mutually assured destruction.
That's the whole thing, right?
It's a deterrent.
For a deterrent to work, they have to know they exist.
They have to know they're maintained.
They have to know they haven't been degraded.
If you were not around during the Cold War,
if you are younger, there is a movie called Dr. Strangelove.
It's satire.
But go watch it.
Go find it and watch it.
Understand it's satire, but it will give you
a good glimpse into the mindset of the Cold War.
And there's an entire scene in it.
Because Russia has created this doomsday device.
And the American scientist is like, that only works.
If we know about it, why would you keep this a secret?
Because it doesn't provide the deterrence necessary
if they don't know about it.
You're OK with China knowing our secrets but not Americans.
You do know.
That's how you knew they flew over them.
Now, maybe they were trying to do
some other type of surveillance other than taking pictures,
which is what you said.
Maybe, but it's probably not going to be effective.
Why?
They'll never tell you.
There are things that need to remain secret to protect
means and methods.
Let me tell you some things they will never tell you
about how this happened.
You won't find this stuff out until 40 years from now.
Not the truth, anyway.
They will never tell you where and how they picked it up.
I know that they're saying they saw it in the Aleutian Islands
or something.
That's a lie.
That's just a method of telling the Chinese
that we picked it up very early, just as soon as it crossed in.
They had it before then.
But they won't tell you how or why,
because that would also tell the Chinese.
It would give away means and methods.
They won't tell you what countermeasures were used
as it flew over this country.
They won't be specific about that.
When I talked about the countermeasures that
could be used, I specifically used stuff
from the 1940s and 80s.
They won't get into any of that, because it gives away
means and methods.
That's the only things are secret.
People get very confused about what needs to be a secret.
A photo of the top of a silo does not need to be a secret.
On Twitter, as a joke, I actually
tweeted out photos of theirs, the Chinese silos, some of them
still under construction.
It's not a secret.
But because people have a hard time with context,
things can get scary real quick when
there's no need for them to be.
So what's the better response there?
Yeah, just don't even mention it.
There's no reason to scare people,
because they're going to get way out of hand with it,
which they did.
Remember when liberals would say things like, the very word
secrecy is repugnant in a free and open society?
That's a Democrat, John F. Kennedy.
Yeah, yeah.
I actually have a video in which I
think I read the entire speech.
I actually have a video in which I think I read the entire speech
to illustrate a point.
And it's a point about context.
The very word secrecy is repugnant
in a free and open society.
And we are, as a people, inherently and historically
opposed to secret societies, to secret oaths,
and to secret proceedings.
We decided long ago that the dangers
of excessive and unwarranted concealment of pertinent facts
far outweigh the dangers which are cited to justify.
It's beautiful, isn't it?
And it goes on like this for quite some time.
One thing I want you to keep in mind as I continue reading,
I'm not going to read the whole thing,
but read enough to get to the part that matters,
is when somebody is apologizing to you, and they say,
oh, I'm so sorry I did that, and all of this stuff happened.
But what do you do?
You always forget everything that comes before they say but,
right?
Because the part after the but is the point
they're trying to make.
Remember that in this.
OK.
Even today, there is little value
in opposing the threat of a closed society
by imitating its arbitrary restrictions.
Even today, there is little value
in ensuring the survival of our nation
if our traditions do not survive with it.
And there is very grave danger that an announced
need for increased security will be seized upon
by those anxious to expand its meaning to the very limits
of official censorship and concealment.
That I do not intend to permit to the extent that
is in my control.
And no official of my administration,
whether his rank is high or low, civilian or military,
should interpret my words here tonight
as an excuse to censor the news, to stifle dissent,
to cover up our mistakes, or to withhold
from the press and the public the facts they deserve to know.
But, but, I do ask every publisher, every editor,
and every newsman in the nation to re-examine his own standards
and to recognize the nature of our country's peril.
In time of war, the government and the press
have customarily joined in an effort based largely
on self-discipline to prevent unauthorized disclosures
to the enemy.
In time of clear and present danger,
the courts have held that even the privileged rights
of the First Amendment must yield to the public's need
for national security.
Today, no war has been declared.
And however fierce the struggle may be,
it may never be declared in the traditional fashion.
And it goes on and on and on.
But the real point, the part that matters,
every newspaper now asks itself with respect to every story,
is it news?
All I suggest is that you add the question,
is it in the interest of national security?
The quote that gets cited so often about our need
to not have secrets is literally from a speech
in which Kennedy was encouraging self-censorship
to maintain secrets for the purposes of national security
during a Cold War.
Context.
Lacking context and understanding
why things need to be secret, an absence of those two things,
that creates the need for secrecy.
And yeah, I'm pretty comfortable with it
because in a lot of cases, lives are literally on the line.
I'm not talking about covering up political misdeeds.
I'm not talking about maintaining a two-tier justice
system or any of the stuff that we constantly
talk about on this channel.
But there are moments, make no mistake about it,
when the military, when the intelligence community,
when journalists, when politicians
aren't going to tell you everything.
It's going to happen.
Part of it is because Americans don't know
what needs to stay secret.
And part of it is because Americans
are really bad at putting things into context.
And they end up being very emotional and getting scared,
which can lead to really bad things.
But if it makes you feel any better,
please understand that this also applies to politicians.
People in Congress.
There's a whole lot of people in Congress
who probably know less than you do about this.
They're not going to be told stuff.
This isn't a government-wide thing.
It is this group of people who are tasked
with this specific thing, who understand
the means and methods.
They say this part of it can't be talked about.
And so it isn't.
And I guarantee you that briefing
where they told all of those people in Congress,
oh, yeah, we picked it up over the Aleutian Islands.
The way they phrased that was something like,
well, the tracking station on Island Y
picked it up at 9.30 AM.
And that was the first time that station picked it up.
They didn't actually lie.
They just never mentioned that it was on two other stations.
It was picked up other ways before then.
Because they knew that those people in Congress
would leave that briefing and get on Twitter and tweet it out.
And they did.
And they did.
That's why you had letters to the committees that
are staffed with people who do not
need to be on those committees, that are basically
from these agencies.
Like, yeah, we're part of the executive branch.
We, you're allowed oversight.
You're not allowed access to everything.
It isn't just a situation where they're
keeping us peons in the dark.
They're keeping everybody in the dark
when it comes to certain things.
And as long as it remains confined to means and methods
and occasionally intent, yeah, it
doesn't bother me because it saves lives, like everywhere.
It's not even a thing where it saves American lives.
It saves lives, period.
Context, it matters.
Anyway, it's just a thought.
Y'all have a good day. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}