---
title: Let's talk about Fox, truth, lies, and tools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=k4BZK0q0qKE) |
| Published | 2023/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the topic of Fox News, truth, lies, and tools to combat misinformation.
- Mentions a filing revealing private messages from major figures at Fox News discussing baseless claims and lies about the 2020 election.
- Emphasizes that while Dominion may not win their case against Fox, the filing provides a tool to reach out to people who believe everything from Fox News.
- Suggests using the quotes from the filing to show family members or loved ones the reality of what was going on behind the scenes at Fox News.
- Encourages finding alternative news sources that family members may accept and using those to break them away from solely relying on Fox News.
- Stresses the importance of going through the quotes with family members to help them widen their information sources and not blindly accept everything from Fox News.
- Acknowledges that convincing as the filing may be, it does not guarantee Dominion's win due to the high bar for defamation cases in the United States.
- Proposes using the filing as a tool to help bring family members back to reality and prompt them to seek information from diverse sources.

### Quotes

- "Dominion may not win their case, but it's not worthless because it gives you a tool."
- "It might help them move to a point where they start to widen their information sources."
- "Will that guarantee Dominion win? No, no. It's a very high bar that they're going to have to overcome."
- "It might help you get a family member back."
- "It's a tool that you can use to reach out to people."

### Oneliner

Beau addresses using revelations about Fox News lies as a tool to combat misinformation and prompt loved ones to seek information from diverse sources.

### Audience

Family members and loved ones

### On-the-ground actions from transcript

- Reach out to family members using the revealed quotes from major figures at Fox News (suggested).
- Find out alternative news sources your family members might accept and share information from those sources (exemplified).

### Whats missing in summary

The full transcript provides detailed guidance on using revelations about Fox News to prompt loved ones to seek information from diverse sources, enhancing critical thinking and combating misinformation.

### Tags

#FoxNews #Misinformation #Dominion #Family #InformationSources


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Fox and truth
and lies and tools
and how you may be able to use something
that a lot of people don't see as going anywhere.
If you missed it, there was a filing
that included a whole bunch of quotes from major figures over at Fox News.
And these messages, the quotes come from messages that were made in private,
talking about all of the baseless claims, all of the lies about the 2020 election,
and how the major figures at Fox viewed those lies.
totally off the rails
was one of many many many quotes
it paints a
it paints a pretty clear picture
of
an outlet where a whole lot of people
did not believe what was going on the air
what was going out to people
now this is all part
of Dominion's case
against Fox
and part of the coverage
and almost any time I've seen it, includes that it's pretty unlikely that Dominion win
because, defamation in the United States, it's incredibly hard to prove, it has a very
high bar.
So people have asked about it and basically said, you know, is this worthless?
It seems really convincing, how can it be worthless?
And that word kept showing up, worthless.
Oh, it's not worthless.
It is not worthless at all.
Dominion may not win their case, but it's not worthless because it gives you a tool.
It gives you a tool to reach out to your family members, to people in your life who believe
everything they see on Fox.
Those quotes, and I'm not going to go through and list them all because I do think it's
actually important that people read them I will take and put a link to an article
down below that has some of it in it but the coverage is just expansive and all
of the coverage picks different quotes. It's worth looking at and it's also
worth finding out which outlets outside of Fox that your family members, your
or loved ones would accept information from because using an outlet of their choosing
and their coverage of it and just showing them the quotes, just showing them what was
going on behind the scenes at the time all of this information was going out, it really
might be something that can be used to break them away.
It can kind of bring them back to reality.
It may not help Dominion win their case, but it's not worthless.
It can help in other ways.
It's a tool that you can use to reach out to people.
So after you review it, find out what your uncle on Facebook, find out what sources he
might accept, whether it's CBS or NPR or whatever, and then go get their coverage of it and make
sure that he sees it.
And go through the quotes with them, you know, and just point out how, I mean, they didn't
believe Giuliani.
They didn't believe a lot of the stuff that they were putting on the air.
And while it may have nothing to do with the 2020 election anymore, you know, they may
just view that as something in the past that they want to turn the page on, quote, because
that's what they've been told, it might help them move
to a point where they don't just accept everything that
is read off of a teleprompter over on Fox.
It might help them move to a point
where they start to widen their information sources.
And it might help you get a family member back.
So will that filing, as convincing as it may seem,
will that guarantee Dominion win?
No, no.
It's a very high bar that they're
going to have to overcome, but it's not worthless.
And they did a whole lot of the work for you
to reach out to one of your family members.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}