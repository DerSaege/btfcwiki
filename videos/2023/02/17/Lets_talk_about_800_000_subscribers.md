---
title: Let's talk about 800,000 subscribers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=r0MrQi29xl8) |
| Published | 2023/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Celebrating 800,000 subscribers and reflecting on the journey over the past five years.
- Explaining the concept of the rules-based international order post-World War II.
- Addressing the question about calling Nikki Haley by her real name and political strategies.
- Hinting at potential charity suggestions for an upcoming Christmas idea.
- Updating on the delayed book release scheduled for January.
- Explaining the choice to maintain anonymity in messages for honesty and privacy reasons.
- Speculating on Biden's potential run for office and the political implications.
- Justifying the decision to limit historical context to simplify explanations.
- Contemplating the lack of accountability for Trump and potential responses.
- Clarifying the authenticity of the messages shared, albeit with minor edits for anonymity.
- Sharing personal reflections on addressing past controversies and the journey towards positive change.
- Teasing a community gardening video release in spring for practical advice.
- Hinting at upcoming changes in workflow to streamline message submissions.
- Demonstrating the ability to switch accents on command.
- Offering advice on building a community through a YouTube channel with a clear purpose.

### Quotes

- "On a long enough timeline, we win."
- "It's all about change."
- "Your past is your past, it's always there. But it can motivate you to do better, or it can make you bitter."

### Oneliner

Beau marks 800,000 subscribers, explains international order, hints at charity plans, updates on book delay, values anonymity for honesty, speculates on Biden, justifies historical context, contemplates accountability, shares real messages, teases gardening video, hints at workflow changes, demonstrates accent switch, offers community-building advice.

### Audience

Content Creators

### On-the-ground actions from transcript

- Start a YouTube channel with a clear purpose to build a community like Beau's (exemplified)
- Research and understand the concept of rules-based international order to foster global understanding (suggested)
- Support community gardening initiatives and learn about raised beds, greenhouses, and tree care (implied)

### Whats missing in summary

The full transcript provides deeper insights into Beau's journey, reflections on past mistakes, and the importance of positive change and community building.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about 800,000 subscribers.
Thank you.
Thank you for hanging in there with me.
You know, five years ago when all this started,
I could not have pictured any of this.
I am still incredibly sick,
but this is tradition now to mark these milestones this way,
so here we are.
So there are questions from y'all that have been picked out by the team and I will answer
them and give you an off-the-cuff answer.
They're supposed to be fun and we'll just kind of take a moment and be here.
What is the rules-based international order?
That doesn't seem like a fun question.
The rules-based international order, that's a term used to describe the various international
systems that kind of regulate the way countries interact on the global scene.
It came into being after World War II, after World War II is when it kind of surfaced.
Think like the UN.
If you're having trouble finding stuff using this term, look for liberal international
order. And that would also kind of guide you there. Why don't you call Nikki Haley by her
real name? I call people what they would like to be called. I get it from a partisan democratic
strategy standpoint. I understand putting her given name in front of Republicans is, I mean,
you know what's going to happen. But y'all don't need my help with that. And to be honest, y'all
I don't really need to do that either, other Republicans are going to do it to her.
Are you going to take suggestions for the charities for your 25 days of Christmas idea?
I hadn't planned on it, because it was supposed to be a secret.
Maybe.
That's still very much in development.
Why don't you send me another message, tell me who told you about it.
Okay, I'm still waiting on the book.
When's the book coming out?
Well, it's supposed to be January.
It's me, I'm running behind.
It's me being obsessive about content and stuff like that.
It's all on me.
We are behind on it.
But I will let y'all know.
Don't worry.
Why don't you ever name the people in your messages?
Because I respect people's privacy and I think that that anonymity creates a little
more honesty.
People are more willing to ask questions if they know their name isn't going to be tied
to it and they're not going to get jumped on on social media.
Do you think Biden is going to run again?
I don't know.
I don't know if he's going to run again, but I know that he wants you to think he's going
to run again.
If he says he's not going to run again, it means his political career is over.
He becomes ineffective immediately.
So he's not going to say he's not going to run again.
And then even if he's already decided that he's not going to run again, he would probably
still want to cast that image that he's going to so he can act as like a lightning rod for
whoever the new candidate is.
The Republican Party continues to attack him and then he steps down.
The new candidate won't have been attacked for four years straight.
I know you said you were only going back to the beginning of the Soviet Union, but why
didn't you go back further to explain how Ukraine came to be?
In a recent video I talked about the structure of the Soviet Union and how Ukraine existed
in that structure.
I only went back to the beginning of the Soviet Union because people who might need that like
broken down...
I mean, if we started talking about the ruse in Keeve, the context for it isn't there.
When you're trying to simplify something, and you're using history to do it, I have
found it helpful to only go back as far as you need to, and odds are you will provoke
enough curiosity that people will go and look it up on their own afterward.
What will you do if there's no accountability at all for Trump?
Become even more cynical?
Try to be prepared for somebody else to try to do it again in the future?
Are your messages real or are they made up as a rhetorical device?
real. So sometimes they may be obscured a little bit. Like let's say I talk to
somebody out here like in person. I may say it's I got a message because that
person it might identify who they are if I don't. Sometimes I will shorten a
message. Obviously you have seen me edit them as I'm reading through them, but
they're real. The content is real. The method of delivery might be obscured or
whatever, but yeah, it's real. I will cop to, if I get a bunch of messages
about one topic, I tend to either choose the absolute shortest or the one that is
structured in the best way for me to make my point. I will admit to doing that
but no, they're real. They're not like just totally fabricated or
anything as a jumping-off point but I mean I get the question. It makes sense.
Okay, why don't you ever talk about your past or at least defend yourself against
the parts that aren't true? I did. I did initially and I did it poorly. When I
first got out, I tried to talk about it and I would point to, see I don't even want to say that,
I would talk about it poorly and it wasn't the right way to do it. So I stopped for a while,
like totally and then I went into this phase where I would correct the just the
gross inaccuracies that often a company takes about it and then I realized that
that wasn't the right way to do it either and that happened you know on this
channel or during the time of this channel I realized that I was breaking
my own rules. I wasn't following my own advice. In fact it's advice I've given
recently on the channel. You don't do anything to undermine other people
trying to make the same change you did.
And I realized that was happening.
And then it really kind of hit home when I got a message.
And I made a video about it at the time, the significance of
it to me, probably more than most people that watched it.
But it was from a guy.
He joined a gang when he was, I don't know, 14, 15, I don't remember the ages.
And then when he was old enough to be tried as an adult, him and his crew drove by somebody
and they got the wrong person.
And I don't know if he did it, but he was, you know, he was in it.
And he went to prison for a very long time, and when he got out, he wanted to do like
a gang intervention thing where he would try to stop people from his neighborhood from
joining gangs.
And I got this message from him, and it was just full of self-doubt, and he didn't think
anybody would let him get past his past.
And it happened, I got the message shortly after somebody had brought up mine.
And I'm sitting here thinking, I'm like, you know, at that point it was 12 or 13 years
after the fact.
And the message that was being taken away from it when I talked about it was, it doesn't
matter what you do. That that's how you'll always be defined. And he had
this giant list of what he considered like redeeming things about me. And I
think most of it was actually all I just had the YouTube channel so I got the
credit you know. And he just said he didn't have anything like that. And it
created all of this self-doubt and it was just paralyzing for him. And I
realized then that it wasn't helpful. That when I tried to address it or
particularly, you know, against the parts that people get wrong or whatever, it
I undermined other people trying to make the same change.
And so I decided to stop, and I don't.
I kind of just put a little bit of faith in my assumption that people are generally good
and that people see change.
And it's worked out so far pretty well.
And it's a big part of this channel in a lot of ways.
When you think about the concept of change
and everything that's coming through this channel,
evolve
on a long enough timeline, we win.
It's all about change.
And I feel that
this community
accepts that.
And I think it also
has a lot to do with why this community is the way it is.
And I feel that there are probably a lot of people
who are also
not just wanting to change the world, change their community,
but change themselves.
And this,
anytime there is a moment when that can reveal itself,
it does.
In that recent video, let's talk about men changing.
I mentioned the twelve steps.
Go to the comments of that video
and see all of the people who have personal experience
with that program.
And it may seem like it's a weird community for that
reason, because I think that there's a lot of people that
are part of it that have that drive.
But it's also what makes it effective.
It's that drive to change, that drive to do better, that
strive to make amends or fix something or whatever it is that's pushing a whole lot
of people.
That's the reason a radio show can call and say, hey, there's this union and they're
on strike and it's near Christmas and they got 200 kids and will you come on and help
raise money for it?
Yeah.
But then I do a live stream with y'all and mention it, and in what, like 90 minutes,
two hours, raised all the money.
That comes from people who want change.
So to me, it seemed better to just let people look at it and interpret it however they want.
Because at this point, it's 15 years ago, they see the change or they don't, they accept
it or they don't, and there's nothing I can do about that.
And I don't want to undermine other people who are trying to make the same change.
And I don't want them to see it constantly being thrown up, even after all that time.
To the, statistically speaking, thousands of ex-cons who are watching this.
are watching this channel because you don't want change. You don't want to be like you
were before. If you wanted to be like you were you wouldn't be watching this, you'd
be watching something else. You want change, you want change in the world, you want change
in the community, you want change in yourself. And I believe that's true and I believe you
can do it. So it became important to me for that not to become part of the
common discussions on this channel. It became important for me for this channel
be about moving forward, not looking back.
You know, your past is your past, it's always there.
But it can motivate you to do better, or it can make you better.
I think that the people who tune in here, that they want positive change, and they're
willing to work for it. So a little bit more of a yes or no answer for that one
I guess. Yeah, fun questions.
Okay, when's the community gardening video coming out? Spring, spring right
before people need it. It will come out and we'll go through how to do raised
These beds, inexpensive greenhouses, trees I think are in it, and we'll put all of that
in a video.
Most of it was filmed last year.
You should just establish an email just for your messages.
Yeah, that's going to happen kind of.
The workflow will change around here, probably next month, and there will be a streamlined
way to get messages in.
Can you still hydra-accent on command?
I don't know if I'll be able to do it right now, I'm sick.
Yes.
If I think about it, I can still maintain a non-regional accent and enunciate things
very, very quickly and not sound like a redneck at all.
I've started a YouTube channel.
What's the biggest piece of advice you have for building a community like yours?
The YouTube channel is the tool.
It's the tool.
Figure out what you want to do and how you want the channel to interact with the world.
Having a successful YouTube channel, that's not a plan, that's a wish.
Figure out what you want to do and how you want your community to interact and purpose
build your channel for that, your content for that. Use it to change the world or
whatever. So, okay, we're going to kind of wrap this up. I really do appreciate all
of this. Like I said, it has been a wild ride and we have done a lot of good.
We have done a lot of good, and we will continue to do so.
I cannot wait to see what the next five years has in store.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}