---
title: Let's talk about the Colorado River and deals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8zcdjzZflkU) |
| Published | 2023/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Colorado River is in trouble due to over-reliance and drought.
- States along the river need to reduce water consumption.
- Six states have proposed a consensus plan, but California plans to release its own.
- Questions have been raised about the effectiveness of the consensus plan.
- The plan is more of a plan to continue planning rather than a concrete solution.
- States seem hesitant to take real climate mitigation actions independently.
- Lack of action is pushing decisions to the federal government.
- Delayed action will result in an inadequate solution when implemented.
- There's a comparison made to kids arguing over a TV until a parent steps in.
- States seem reluctant to take necessary mitigation efforts, leading to federal intervention.

### Quotes

- "This isn't agreed to by these states yet. This is really a plan to continue to plan."
- "Every day that passes, the situation gets worse."
- "It's probably going to be like kids arguing over a TV and then mom comes in and makes the decision for everybody and nobody's happy."
- "States don't want to make the mitigation efforts that have to occur a reality."
- "If you want to maintain the power there, you have to make the decisions, even if they're hard ones."

### Oneliner

The Colorado River faces crisis as states delay real climate action, pushing decisions to the federal government.

### Audience

Climate activists, policymakers, environmental advocates.

### On-the-ground actions from transcript

- Advocate for state-led climate mitigation efforts (implied).
- Push for states to take necessary actions to protect natural resources (implied).

### Whats missing in summary

Importance of states taking responsibility for climate mitigation efforts and potential consequences of delaying action.

### Tags

#ColoradoRiver #WaterCrisis #ClimateChange #StateResponsibility #FederalIntervention


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Colorado River and a plan, kind of, that is being put forth by the states, some
of the states.
So, if you need context on this, the Colorado River is in trouble, it is in trouble, it is, basically there are too many
people relying on it,  too much water is being taken out of it, there is a drought, it's been the driest 20 years
in like the last thousand, there's a lot of problems.
States have to make reductions when it comes to water consumption coming out of the river.
Six of those states have come up with a plan that is being referred to as the consensus
plan.
State, California, is like, yeah, no, we're going to put out our own plan later.
I know somebody who's looked over the consensus plan.
They have questions.
They know a whole lot more about it than I do.
I don't know.
I have no ability to kind of fact check their work.
But they think some of the math is fuzzy and they don't really think it's going to generate
the cuts that the plan says that it's going to.
But what I want to point out is that it doesn't matter because it's not really a plan.
This isn't agreed to by these states yet.
This is really a plan to continue to plan.
It's a plan to keep talking is what was submitted.
What is becoming more and more clear from the situation with the Colorado River is that
the states seem unable or unwilling to deal with real climate mitigation efforts by themselves.
That's what's happening.
This is one of the first real big tests.
We're going to see more of them, a lot more of them, and it was up to these states to
set the tone.
The tone that is being set is that the federal government has to deal with it.
That's what's being set.
Whether or not that is good or bad is up to you and your personal view of it, but the
The length of time this has been going on to have no real plan, no real action being
taken, it's pushing the decisions to D.C.
Every day that passes, the situation gets worse, which means when a plan finally gets
enacted, it's not going to be enough.
it's taking too much time to hammer out the details. I feel like this is
probably going to be like kids arguing over a TV and then mom comes in and
makes the decision for everybody and nobody's happy. It's probably what's
going to happen.
What is being demonstrated is that for political reasons, I would assume, the states don't
want to make the mitigation efforts that have to occur a reality, which means the federal
government is going to end up stepping in and doing it for them.
This is going to, of course, trigger a whole bunch of people who will talk about states'
rights and all of this stuff, and talk about how the feds have too much say over water
and land use, and all of this stuff that is very much a talking point among those who
Pretend that they're all about individual responsibility and maintaining power at a
lower level.
If you want to maintain the power there, you have to make the decisions, even if they're
hard ones.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}