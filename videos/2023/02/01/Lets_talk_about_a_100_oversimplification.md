---
title: Let's talk about a $100 oversimplification....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m72KxYdDshs) |
| Published | 2023/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing oversimplification of an incident over $100 and its implications.
- Explaining a specific incident in California where a fee for damaging a forklift led to an outburst.
- Emphasizing that the fee wasn't the cause but the final addition to a list of grievances.
- Identifying common themes among individuals involved in such incidents, including DV history, racism, misogyny, and economic grievances.
- Stressing the importance of understanding root causes and underlying themes rather than reducing it to a monetary value.
- Advocating for mental health care availability, coping skills education, and stronger social safety nets as solutions.
- Linking economic inequalities and lack of social safety nets to the escalation of grievances.
- Warning against focusing solely on the final straw rather than addressing the underlying issues.
- Pointing out the impact of societal pressures and economic disparities on individuals' mental health and behavior.
- Encouraging a shift in focus towards systemic change rather than individual incidents to prevent future outbursts.

### Quotes

- "It's not over $100."
- "Don't reduce it like that."
- "You want to fix it? You want to deter it? We have to change."
- "That's a recipe to lash out."
- "If you actually look at the surveys, if you look at the studies, if you look at the underlying themes, you find out, yeah, I mean, it seems like we should probably make mental health care available."

### Oneliner

Beau breaks down the dangers of oversimplification, urging a deeper understanding of root issues and systemic change to prevent future outbursts.

### Audience

Advocates for societal change

### On-the-ground actions from transcript

- Make mental health care available (suggested)
- Educate on coping skills (suggested)
- Advocate for stronger social safety nets (suggested)

### Whats missing in summary

The full transcript provides a comprehensive breakdown of the dangers of oversimplification and the importance of addressing root causes and systemic issues to prevent future incidents.

### Tags

#Oversimplification #RootCauses #SystemicChange #MentalHealth #SocialJustice


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we're going to talk about $100 worth
of oversimplification and how sometimes oversimplification
leads to a situation where you don't actually
understand what's happening and therefore you
can't come up with solutions.
Over the last few days, I've gotten quite a few messages.
One was just $100 with a question mark.
One was, was this really over $100?
I can't believe he did this over $100.
You shouldn't.
You shouldn't believe that, because it's
an oversimplification of the issue.
Now, if you have no idea what I'm talking about right now,
This is about one of the many incidents that has occurred over the last few weeks.
I know it is hard to keep track of them all.
This is one in California where the accused was told that he needed to pay a fee, a workplace
find something for damaging a forklift that he may or may not have damaged.
That is being held up as the reason for him losing it.
No.
That's a good soundbite.
It's a good headline.
it leads to the thought process of, well, oh, he was just unwell. He was a monster.
Thoughts and prayers, nothing we can do, right? If you look at the surveys, and I'm
sure it'll be the case here, if you look at the surveys and the studies of the
characteristics of the people who engage in these types of things, you see common
threads that show up in every single one of them, regardless of what they are
trying to show with their survey, regardless of what they set out trying to
show, there are certain things that always show up. One of them, a DV history,
if you watch this channel you know that because I constantly bring it up. Hey
look, my bias. What's another one? There's not an agreed upon term, but if you look
at the various surveys that are studying the people who commit these shootings,
what you find out is a running trend with phrases like multiple stressors,
grievance collector, blended grievances, grievance culture. The final thing that
sparks it isn't actually the problem. It's just the final addition to a long
list of grievances that they have collected and blended together. This is
incredibly common. Very, very underlying theme through all of them. You also have
racism and misogyny as a common theme through, I mean, most of them. Why? It all
fits together. It all lines up. And when you reduce it down to a hundred dollars,
it all gets lost. That was the straw that broke the camel's back, so to speak. It
wasn't the cause. The cause was people not having adequate coping skills, not
being able to let stuff go, being encouraged to hold on to grievances.
That's that middle part, multiple stressors.
An economic system that is less than fair, all right there.
If you have a whole bunch of grievances and you start to collect them, what do you need
to do with them?
You have to personify them, right?
Right into the racism and misogyny.
If you don't have coping skills, how might you first lash out?
That's how they end up with a DV history.
It's all connected.
It all ties together.
But if we reduce it down to, well, it was over $100,
you can't predict that.
Right, and so I guess we can't do anything.
But if you actually look at the surveys,
if you look at the studies, if you
look at the underlying themes, you find out, yeah, I mean, it seems like we should probably
make mental health care available.
We should teach coping skills.
We should have stronger social safety nets.
There's a list of solutions that deal with the motive that if people were informed of
these common threads, that they'd probably support.
Most of them have economic grievances.
Most of them have DV histories.
Most of them have racism or misogyny as an undertone or sometimes very much a controlling
element.
To me, that depends on how far they fell down that rabbit hole as to whether or not that's
plainly visible.
if you have a bunch of grievances that you've collected, you need a they, a them, those
others, people to blame it on. That's why it exists. It's their scapegoat for their
collected grievances. They're upset about a whole bunch of different things and then
there's that one thing that just sends them over the edge. Focusing on the final straw
actually protect the camel. More importantly, it doesn't protect the
people around the camel. The large amount of economic grievances, they stem from an
economic system that is less than great and a lack of social safety nets to
catch those people who fall through. The reason you can't believe it's about a
a hundred dollars is because it's not. It's not. That was just the final act. You want
to fix it? You want to deter it? We have to change. A lot of the problems in this country,
they come back to that same issue. An economic system that is out of whack. An economic system
that pushes people down, that keeps people down, a lack of social safety nets if somebody
starts to fall through the cracks.
This is particularly true and could be particularly upsetting if somebody's done everything that
they can.
They've done everything that they think they're supposed to, that they're told to by those
people who say, pull yourself up by your bootstraps, and then they fail after looking down at other
people.
And then they realize they're just like them.
That's a recipe to lash out.
It's not over $100.
Don't reduce it like that.
If you do, you will end up in a situation where you think there's nothing that can be done
is you can't predict the final straw for each camel.
But I mean, I think it's pretty easy to say that you probably
shouldn't overload a camel.
Anyway, it's just a thought.
you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}