---
title: Let's talk about dates, red flags, and a question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xtgEZnaGujs) |
| Published | 2023/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares a story from Twitter about a disastrous date where a man displays controlling behavior.
- Describes his past as following "pick-up artist" (PUA) and men's influencer accounts to learn how to be an alpha male.
- Acknowledges his past toxic behavior towards women and trolling online accounts.
- Expresses a realization after following Ask Aubrey's account and seeing the impact of his actions on women.
- Questions whether his past actions truly scared women to the point of needing rescue.
- Provides insights on how uncomfortable situations can escalate, with examples of strangers intervening.
- Advises on the long process of unlearning toxic behaviors and discovering one's true self.
- Encourages taking a break from dating to focus on self-discovery.
- Stresses the importance of understanding oneself before engaging in relationships to avoid harming others.
- Urges continuous efforts to unlearn toxic teachings and approach dating with authenticity and respect.

### Quotes

- "You don't want to try to exert control because even if the person is somebody who is strong enough to push back your moves, they're on guard."
- "If you're wanting to seriously date, you want the other person relaxed, not on guard."
- "Relax. Figure out who you are, and then you can re-approach all of this."

### Oneliner

Beau admits to past toxic behavior, advises on unlearning harmful teachings, and encourages self-discovery before dating.

### Audience

Men seeking to unlearn toxic behaviors.

### On-the-ground actions from transcript

- Take a break from dating to focus on self-discovery (suggested).
- Avoid returning to toxic online communities (implied).

### Whats missing in summary

The full transcript provides detailed insights on recognizing past toxic behaviors, unlearning harmful teachings, and prioritizing self-discovery before engaging in healthy relationships.

### Tags

#ToxicMasculinity #SelfDiscovery #UnlearnBehavior #HealthyRelationships #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about trains
and an account on social media, flags, and a really long message
that I'm going to read all of because I think some people maybe
should hear it.
And I think it's really illuminating.
And one of the questions is interesting.
Before I read the message, I have to give you
context, there is an account on Twitter called Ask Aubrey. This account shares a
lot of examples of men's behavior that is, let's just say, less than ideal at
this point. And last night I saw something that came from that account
that was a date. It was a video that looked like it had been pulled from TikTok.
It was a date that was an absolute train wreck if the train was pulling a cargo
of red flags. It was just really bad. A concise summary of it, he basically like
kind of forces his way into ordering for her, getting something that she didn't
want. She wanted a glass of red wine. He's like, no, that's too many calories for you.
It's just all bad. And then when the check comes, she wants to pay for her portion. And he's like,
no, you will not embarrass me. Like it's, it's bad. So I retweeted it. It led to this message.
I have a question that I feel like I should explain first. From the time I was 16 to the
time I was 22, I followed PUAs and men's influencers. If you're not familiar with
that terminology, these are the men who tell men how to pick up women and how to
be an alpha and all of that stuff. Influencers, and I did as you would say
what my betters told me. I was a trademark nice guy. If you don't know what
that means it means not a nice guy. About a year and a half ago, I started
following Ask Aubrey. I started following her to troll. Like I said, I was not a
nice guy. I would go to the profiles of the people who replied and find things
to mess with them about. Over time, I noticed that these were actually the
women I wanted so badly, and they gathered daily to talk about what a bad
guy people like me were. It was a real wake-up moment for me. Without her account, I'd probably
be cheering for a well-known figure within the men's influencer community who is currently
facing some legal troubles. I had seen you before, but didn't really start following you closely
until I saw you retweet her. The video you retweeted tonight could have been me a year
and a half ago. Now, I'm working to unlearn a lot of things, but sometimes I really don't
understand. I understand why everything the guy did in the video was wrong, and cringe
because I've done most of it myself. I see today how it would make women uncomfortable.
You said the guy in the video doesn't understand why his dates constantly run into someone
they haven't seen in years and lots of people liked it. I had women leave dates
when they'd run into old friends before. I never really thought about it before.
Today, I know I was wrong and I know I made women uncomfortable, but did I
really scare them to the point that they texted a friend to come rescue them?
I felt like every time I get a grip on how much of a not-nice person I was,
I discover I was way worse than I thought. I don't even know how to recover. I'm trying,
and I've all but stopped dating because I'm now honestly afraid I'm going to do something wrong,
which is funny because I used to say men were afraid of dating because they were afraid of
doing something wrong. When I was constantly doing stuff, I should have known was wrong.
Was I totally broken by this? Can I ever truly recover from this? Any advice for me? Signed
recovering bad guy TM PS I'd never message her because I'm embarrassed and
would hate to see myself as a screenshot but if you know her personally please
tell Aubrey she changed my life she started me down a road to something I
hope is better that is a cool message and I wanted to read the whole thing
because if I was the person behind Ask Aubrey, I would want to hear that.
Okay, so let's start with the questions.
Did I really scare them to the point that they texted a friend to come rescue them?
Probably not.
I mean, that may be what happened, but that's probably not.
That's not the dynamic that normally occurs.
It normally occurs one of two other ways, and it's actually worse than them texting
somebody.
One way a long lost friend appears in a situation like this is a total stranger overhears a
part of the conversation and decides to intervene because they see the red flag.
I have done this four or five times in my life. Out of the times I've done it
only once was it somebody that I knew and that was just I just happened to be
walking by. And yeah somebody walks up and they're like hey how are you doing?
Haven't seen you in forever. Are you doing okay? Out of the times I have done
this, I have never had the person sitting there be like, who are you?
What you said to trigger this, in this scenario, if this is how it happened, was so far-fetched
and out of line that a random stranger decided to intervene.
It wasn't that bad.
The other way that I'm aware of it happening is you end up making the person you are with
so uncomfortable that they signal their distress to somebody.
If you look in that thread, you will see a comment about being squeezed on the arm from
somebody and the woman was like, Josh, how are you?
Haven't seen you in forever.
He's not Josh.
Yeah, that is worse when you really think about it.
The woman is literally at the point where she would trust an absolute stranger to have
better intentions for her than you.
I mean, yeah, and it's probably going to take some time to process all of that, no
doubt.
I can tell you in that video, when he's like, you will not embarrass me, that's the moment
when if somebody nearby had heard, they would hopefully intervene.
That way out of line.
Now was I totally broken by this?
Can I ever truly recover from this?
Any advice for me?
Were you broken by this?
Yeah, yeah, of course you were.
You absolutely were.
From 16 to 22, you spent six years, formative years, the years you were supposed to spend
figuring out who you were, pretending to be somebody else, learning how to be somebody
else.
just you have to unlearn all of what they taught you. You have to
figure out who you are because that's what was supposed to be occurring in
that time. I mean it's tough. It's not the end of the world though. You can do it.
I mean you're in the phase now where you are inventorying all of the
horrible things that you've done. That's where you're at. It's a part of changing
and it's not a fun part but it's a necessary part and this process can take
years. You are apparently just now discovering that it's not just about
making them uncomfortable, it's that you might have instilled fear because their
Your dating world is different.
If a guy has a bad date, well, he has a bad date.
If a woman has one, she may never go home.
And red flags, they transfer from uncomfortable to terrifying very, very quickly.
And you're going to have to deal with all that.
going to have to work through it all. Now you say you've all but given up
dating. Yeah, that's a great idea. And not because you're broken and
irredeemable, but because you don't know who you are yet. You have no idea who you
are and you have to figure that out first. Otherwise dating is pretty
pointless. You can't find somebody who is going to compliment you if you
You don't know who you are.
If you don't know who you are, you don't know what you want beyond the physical.
And that's not where you need to be starting.
There's probably going to be a lot of other stuff that you find out that you did that
was messed up, that was wrong.
you have processed all of that and figured out who you are, yeah. There's no point in dating.
Aside from that, if everything you're saying is genuine, and I'm taking this at face value,
you are really putting in the work and trying to do this, you're probably going to come off as
It's really weird because now you actually are worried about doing something wrong and
might be on the other side of it.
You may be so conscientious of it that you come off as strange.
I do not understand why you wouldn't send this to her.
I get it.
You know, you don't want to end up a screenshot.
But if this is genuine, I mean, it's a good message.
This is something that as a person who puts out content
that is aimed at shifting thought,
this is the kind of message we love getting.
Like, this makes our day.
I mean, it's also one of the reasons
I broke the rule about putting an account name out there,
because I hope they do see it.
You've got a long way to go.
You're not irredeemable.
It's not something you can't fix.
But yeah, you have to unlearn everything.
And the easiest way to do it, just
assume that everything they said was wrong.
Everything.
Because it kind of is.
If you're wanting to seriously date,
You want the other person relaxed, not on guard.
You don't want to try to exert control,
because even if the person is somebody
who is strong enough to push back your moves, right,
and keep you in check, they're on guard.
You're not going to actually get to know somebody if they're on guard.
If they're trying to block your moves because you're
doing something that's formulaic, you're never going to get to know them,
which means your entire dating life will be a failure.
Don't do any of that.
Relax.
Figure out who you are, and then you can re-approach all of this.
If you're really putting in the work, see here or two, you'll be alright.
Don't give up, and certainly, no matter what, don't go back to that community.
They're not right.
Anyway, it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}