---
title: Let's talk about AI, deep fakes, and information consumption...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a_2V-SDJk0U) |
| Published | 2023/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Warning about the rise of AI and misinformation in the future.
- Deep fake incident on social media sparks debate about AI and deep fakes.
- Focus on wider geopolitical and information operation applications of AI technology.
- Urges viewers not to spread misleading content created via AI.
- Raises hypothetical scenarios involving deep fake videos of Trump and Biden.
- Questions whether digitally created content could be promoted by information machines.
- Mentions cultural touchstone of not trusting news from Facebook.
- Warns about the potential consequences of deep fake technology being accessible to more people.
- Emphasizes the need for safeguards and authentication processes for verifying information.
- Urges journalists to prioritize accuracy over speed in reporting.

### Quotes

- "Being right is most important."
- "We have to be ready for it."
- "Clips are going to have to go through authentication processes."
- "We've got to get ready for this."
- "Y'all have a good day."

### Oneliner

Beau warns about the rise of AI-generated misinformation, urging preparedness and authentication processes for verifying content to combat the spread of deep fakes.

### Audience

Content consumers

### On-the-ground actions from transcript

- Develop safeguards against misinformation operations (implied)
- Authenticate video clips before spreading them (implied)
- Prioritize accuracy over speed in sharing information (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of AI technology on misinformation and the importance of authentication processes in combating deep fakes.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about AI and information
consumption and how we really need
to get ready for the future.
Because the future is going to be confusing.
Because there's going to be a lot of opportunity for people
to engage in activities that will intentionally
mislead people.
Now, if you missed it, an incident
over on another social media platform
prompted a lot of discussion about AIs and particularly
deep fakes.
In that, a streamer apologized for watching deep fakes, which
are videos created via AI, to appear to be somebody else.
He apologized for watching videos of other streamers
in, let's just call them private situations.
This led to a whole bunch of conversation about the topic.
We're going to focus on the wider geopolitical and
information operation applications
of this technology.
On the off chance that it is even remotely helpful,
yeah, don't do that.
That's not cool.
OK?
Don't, don't, don't, don't do that.
Now, moving on, I want you to picture a video clip of Trump
on the phone saying, I don't care.
Get inside.
Stop the count.
Would you believe it?
You want to believe it, right?
Because I think most people watching this channel
would believe that maybe that's something
that could have occurred.
And they would love that evidence.
They would love to play into that confirmation bias.
They may be a little bit more skeptical, especially
during the course of this video.
But if it just surfaced, what would your initial reaction be?
Flip to the other side.
Let's say there's a clip of Biden on the phone saying,
I can't believe they bought that malarkey, Jack.
They have no idea we put those files there.
Do you think they'd believe it?
When that clip surfaced, do you think it would be believed?
Do you think that information machines within the United
States would latch onto it and put it out there constantly
to amplify it, even if they knew it was untrue?
Because you have to remember, there's a lot of information
machines in the United States that their entire business
model is basically out of context clips.
Is it really that much of a stretch
to believe that they would promote something they
knew was digitally created?
We already have this problem.
I mean, the idea of don't trust my father,
he gets his news from Facebook.
That's already a cultural touchstone.
I mean, some places even have successful merchandise lines
about it.
That's just the political within the United States.
That's just the implications there.
This technology is becoming more accessible to more people.
Let's go to something a little bit more drastic,
a little bit more consequential when
it comes to the wider state of the world.
I want you to imagine a clip of people sitting in chairs,
handcuffed, admitting to being Ukrainian special forces.
Admitting to being Ukrainian special forces
and being sent in to Russia to engage in all kinds of things.
Would it be believed, especially would it
be believed if the state press in the country amplified it?
Would that harden resolve?
This is a technology that is going to be used for information
operations in a whole bunch of ways.
We have to be ready for it.
We have to develop safeguards against it.
This is one of those times when a lot of journalists,
they need to break away from the idea
that being first is most important.
Being right is most important.
Clips are going to have to go through authentication processes.
There's a lot that is going to have
to change about the way we consume information
as this technology becomes even more accessible.
You might want to look into the clip of Obama
from like four years ago.
Think about how much the technology
has improved since then.
We've got to get ready for this.
And here's the twist to this video.
This video was created by a woman in California.
Do you know if it's true?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}