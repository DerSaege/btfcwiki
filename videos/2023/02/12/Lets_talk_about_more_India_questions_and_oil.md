---
title: Let's talk about more India questions and oil....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dUwQ0Au7NOU) |
| Published | 2023/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he didn't condemn India's ultra-nationalist government in a foreign policy video, citing the video's focus and the government's lack of responsibility for previous accomplishments.
- Compares India's nationalist government to NASA's achievements under the Biden administration, arguing against giving credit where it isn't due.
- Emphasizes that India will not be "coming out under" any other country, but rather becoming a competitor nation on its own terms.
- Analyzes India's strategic moves in purchasing Russian oil, detailing how it benefits both India and the West.
- Points out that the democracy-authoritarian framing in geopolitics is a narrative device, not a clear-cut reality of how countries will ally.
- Stresses that foreign policy is primarily about power and that countries, regardless of ideology, will always seek to increase their influence.
- Foresees India's rise as a major player on the international stage, surpassing Russia's power within the next two decades.

### Quotes

- "India is not going to be a country that aligns with the traditional poles of power anymore, because it's going to be its own pole."
- "Foreign policy is about one thing and one thing only, power."
- "It's a game of chance where every hand influences the next hand and everybody's cheating."
- "India is not coming out under anybody."
- "Countries don't have friends. They don't have ideology. They don't have morals."

### Oneliner

Beau explains India's emergence as a global competitor, showcasing strategic moves and debunking misconceptions about alliances and power dynamics in foreign policy.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Analyze and understand India's evolving role in global politics (implied).

### Whats missing in summary

Deeper insights into India's specific strategic moves and their implications on the global geopolitical landscape.

### Tags

#India #ForeignPolicy #Nationalism #GlobalPolitics #Geopolitics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about India
and answer some questions about India.
We've been talking about the country
over the last week or so,
and how it is likely to come into its own
over the next 10 years or so.
And it has prompted a whole lot of questions.
They mainly fit into just a few,
so we're gonna go over them.
One question in particular that came in multiple times
makes me feel like I'm not being clear about the single most
important point about this.
So I'm going to kind of dive deep into that one.
OK, so first question.
Why didn't you condemn or at least point out
the fact that India's government is ultra-nationalist?
OK, a bunch of reasons.
The first reason being that it was a foreign policy video,
not a video about the internal dynamics of the country.
The second being that the nationalist government that's
there, it didn't do this.
It's not responsible for this.
The current prime minister there,
1920, when India started this journey, when it got on this path.
This path started in the early 1970s.
The current government had nothing to do with it.
To put it into a different context, NASA has done a whole bunch of cool stuff under the
Biden administration.
Does Biden get credit for that?
No.
most of it was set out years ahead, sometimes decades ahead, building on stuff that happened
in the past.
So it's not really Biden's accomplishment.
It's not the current administration's accomplishment.
Same thing there.
More importantly, I mean, for me anyway, I don't like nationalist governments, and I
would not like to make it seem as though they should get credit for something that they
didn't do.
I wouldn't bring that up. And I don't feel the need to condemn it because, I
mean, a tagline on this channel, nationalism is politics for basic people.
I'm not exactly secretive about the fact that I don't like nationalist
governments. There's hundreds of videos on that topic. Didn't feel like I needed
to point it out. But yeah, so if you don't know, currently India's government is nationalist.
And I don't know how to say ultra, I mean, if somebody said ultra-nationalist, I wouldn't
argue it. I don't know that that's the word that I would use. They're definitely nationalists,
though. They are they are that and I mean that's a matter of perspective. Okay so
the next part. Wouldn't it be better for India to come out under Russia or China
than the United States? This question came in from a whole bunch of people and
And sometimes it was phrased, come out under the West.
And this is the one that I'm talking about, missing a
fundamental point here.
India's not coming out under anybody.
People are very accustomed to a very static dynamic.
The United States, Russia, and China.
Before then, the United States, the Soviet Union, and
China, very static dynamic.
India is going to be a competitor nation in that sense.
They're going to get to that point relatively quickly.
India isn't going to align with other countries.
Other countries will align with India.
The dynamics are shifting.
And that's the fundamental point here,
is India, for those people who are looking at foreign policy,
you don't need to look at India as a country that needs
to align with somebody else, because it's not
going to be very long.
And they won't be aligning with anybody.
And we're going to dive a little into this
and talk about how right now, this minute,
India is already making major moves
at that international poker game where everybody's cheating,
dealing with the United States and Russia.
They're sliding both countries a card at that table
that they're not really sitting at yet.
OK.
And to the person who's like, India's buying Russian oil,
doesn't that really mean that they're going to cozy up to them?
Kind of on the right track.
You're at least recognizing some weird things.
But you've got to take it from the perspective of everybody's
cheating as well.
OK, so if you don't know, Russian oil,
most Western countries aren't buying it, right?
You know, the whole sanction thing.
India is buying it.
India is buying a lot, a lot more than they used to.
In fact, 33 times more than they were at the same time
year, purchasing that much Russian oil. That's good for Russia because they're
getting to sell large volumes of oil at a consistent rate. It's good for India
because they're buying it at a cut price. Because of the sanctions, Russian oil
isn't worth as much. So India is looking out for its own national interests,
getting that oil cheaply, right? Why isn't the United States sanctioning them? Think
about it. The U.S. isn't mad. Why? Because India doing this is also good for the
West. They're sliding a card to the West at the same time. India didn't suddenly
need 33 times more Russian oil. They're buying Russian oil and not using oil
from other sources. Oil is a global market so that oil from other sources
that isn't being used stays on the market which means your gas prices are
lower. Gas prices in Europe are lower. India doing this, India kind of breaking
with the West on this, helps the West because it helps maintain the oil
markets, it helps maintain lower gas prices, and it helps Russia. They are
They're making a move right now, as we speak, not 10 years from now.
They're making a move right now that is collecting chips in that big game, and they're doing
it on their own.
India is not going to be a country that aligns with the traditional poles of power anymore,
because it's going to be its own pole.
will start off as the weakest, but it's going to grow.
If you're looking ahead, you can't sleep on India.
And then India is a nationalist government.
It would make more sense for them
to side with the authoritarian side of things,
in parentheses, China and Russia.
Okay, something that is really worth remembering,
the democracy authoritarian framing
of the new Cold War is framing.
It's framing.
It's a narrative device.
It's not really how things are gonna break down.
A whole lot of authoritarian countries
are gonna align with the West.
And I would imagine that a lot of countries with democracies
are going to align with China or Russia.
That's just the framing that is going to be used in the West.
The same way as during the first Cold War,
it was democracy versus communism.
Is that really what it was?
Or was it capitalism versus communism?
It's a framing device.
It's useful for conversation, but don't
think that's how countries are actually going to line up.
The other thing to remember is what we've talked about in most foreign policy videos.
Countries don't have friends.
They don't have ideology.
They don't have morals.
Foreign policy is about one thing and one thing only, power.
As long as nation states exist, that is what they will do.
They will seek power.
Every country.
There's no real exceptions to this.
Every country plays these games.
It's going to be weird for people in the West because for a long time, nobody looked at
India as a major player.
If they continue the route that they're on, if they continue to develop their defense
industry, they're going to become a major player, and that has nothing to...
And this is true even if the current government there was out of power tomorrow, because they
didn't set it on this path.
That happened back in the 70s.
It's just happening under their watch.
So when you're looking to the future, it's important to remember that while things have
remained static since the end of World War II, as far as the dynamics on the international
scene, they're changing, and they're changing in a lot of ways.
You're going to see countries that were regional powers become major players.
You're going to see countries that were really big players become less important.
I would imagine that within 20 years, India will be more powerful and more influential
on the international stage than Russia.
These changes, they're going to be uncomfortable for some people because a lot of people, they
use framing devices and they like to keep things simple.
It's not.
It's a game of chance where every hand influences the next hand and everybody's cheating.
It's not simple.
complicated. A lot of the narratives that people have had for the last 60 years
or so, they're gonna go out the window. They're not going to be, they're
not going to be viable and they're not going to be useful for explaining the
state of the world. So nationalism coming out under, yeah, main point here, India is
not coming out under anybody. Right now, even though it isn't at this
powerful state that I expected to get to, right now it's looking at the United
States and it's like, yeah, I'm buying the oil, what are you gonna do about it? I'm
I'm helping you, too, because I have that much economic power.
They're not going to be a subservient nation.
And you've got to stop pretending like they're going to be.
As far as the nationalism, yeah, I mean,
there's a lot of reforms that need to occur in India.
And I'm not a fan of the current government.
that's not the scope of these videos and I really do not feel like I need to say
I don't like nationalism. That seems a little silly. So I hope that helps and I
hope this kind of... I hope we can stop talking about India because there's
other countries that we want to get to. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}