---
title: Let's talk about watching the clock and education....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XJtXQ59J77M) |
| Published | 2023/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring why people disregard the Doomsday Clock despite it being a gauge of nuclear war risk.
- Americans tend to focus on the DEFCON rather than the Doomsday Clock.
- Reasons for this include disliking the methodology behind the clock and the belief that it's not updated frequently.
- The DEFCON provides immediate information, but its updates are not public, leading to lack of preparation time.
- The core issue is people's tendency to ignore vital information due to a sense of safety associated with military involvement.
- Anti-intellectualism in the US contributes to this phenomenon.
- The discomfort from looking at the Doomsday Clock mirrors confronting climate change models – both depict scary realities.
- Beau encourages younger viewers to value education as an asset that can't be taken away, despite societal perceptions.
- Investing in education is emphasized as a lifelong benefit that should not be undermined by societal stigmas.

### Quotes

- "Americans tend to focus on the DEFCON rather than the Doomsday Clock."
- "Your education is pretty much the only thing that can't be taken away from you."
- "Being smart, being educated, being informed is somehow bad."

### Oneliner

Exploring why Americans prioritize DEFCON over the Doomsday Clock and the impact of anti-intellectualism on societal perceptions of education.

### Audience

Young individuals, Americans

### On-the-ground actions from transcript

- Value education as an asset (suggested)
- Challenge societal stigmas around intellect and education (suggested)
- Encourage the importance of being informed and educated (suggested)

### Whats missing in summary

Exploration of societal stigmas surrounding education and the impact on critical decision-making and preparation.

### Tags

#Education #Anti-Intellectualism #SocietalPerceptions #DoomsdayClock #DEFCON


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about why people don't pay
attention to the time, why people don't pay attention
to a specific clock, when that's really the information
they want to know.
It was a question posed by one of y'all.
It's pretty thought provoking when you really sit down
and think about it.
In a recent video, we were talking
about how people in the United States
like to focus on the DEFCON, the defense condition,
and they use that as a gauge of how
at risk we are of nuclear war.
But there's actually the doomsday clock,
which is literally a gauge of how
at risk we are of nuclear war.
It seems like people wouldn't pay attention to that.
But generally speaking, Americans don't.
Generally speaking, Americans do not.
And when you try to figure out why,
you'll get a bunch of answers.
I've been asking people about this.
And the first one is, well, I don't like the methodology.
behind the doomsday clock.
And if they're saying that in good faith, I mean, fine.
They don't like the methodology.
OK, I mean, what are you going to say to that?
But then you'll also get stuff like, well, you know, they
don't update it often enough.
The DEFCON, that's immediate.
I mean, yeah, that's true, but you don't get to know that.
The DEFCON isn't public.
You get to find out the same time everybody else does, when
the radio, makes the weird noise, and what are you going to do in that in that little
amount of time?
It doesn't really help you prepare, and it's not really what you're wanting to know.
You want to know the risk, that's what it's really about.
And there is a specific tool to gauge that, but people tend to ignore it.
I don't think it's any of the stated reasons.
I think those people who obsess over the DEFCON
and do not look at the doomsday clock,
I think they do it for the same reason
that Americans will say that climate change isn't a thing
or that people don't have anything to do with it
or they walk around in the middle of a pandemic
pretending it doesn't exist.
For a very long time in this country,
There has been a popular culture theme that kind of demonizes academics.
Anti-intellectualism is really strong in the United States.
And I think that's what it has to do with.
I think that there's some weird sense of safety when you hear about the defense condition.
You hear about the DEFCON because there's this idea that the military is there and they're
going to help and they're going to save us and all of that stuff.
Understand if it ever goes to one, nobody wins.
Nobody wins.
The lucky ones will be the ones that didn't know it happened.
Whereas when you look at the clock, it's like looking at the climate models.
It's just scary.
There's no sense of comfort there because you realize how out of your hands a lot of
this stuff is.
And I think that is, I think that's what drives it.
And I'm not saying this to just, you know, depressed people.
I do have a point.
There are a lot of younger people who watch this channel.
Just remember that all of the stuff that you hear and see about nerds and all of this
stuff and outcasts and how you don't want to be like this and generally it
gets equated with being an academic, with being an intellectual. Just remember
that your education, whether it be formal or informal, you know, self-taught stuff
you learned on your own, your education is pretty much the only thing that can't
to be taken away from you. It's the one resource, it's the one asset that you
will always have. It's probably worth investing a lot in because it will help
you for the rest of your life and maybe at some point we can get away from the
idea that being smart, being educated, being informed is somehow bad. We live in
a country where a candidate screamed, I love the uneducated, and the people he was
talking about, cheered. You don't want to be one of those people. Anyway, it's just
thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}