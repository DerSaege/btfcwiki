---
title: Let's talk about disconnect and another statement on Poland....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5GZ2Yf2dGS8) |
| Published | 2023/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Addressing a statement from a high-ranking member of the Russian establishment regarding potential military action against Poland.
- Noting the disconnect between the statement and the current reality on the ground, especially in Ukraine.
- Expressing concern about the disconnect of the Russian hierarchy from the actual situation faced by soldiers and citizens.
- Analyzing the strategic shift in Russia towards consistently weakening the Ukrainian military rather than making territorial advances.
- Speculating on the potential consequences of the disconnect between the top Russian officials and the actual conditions faced in conflicts.
- Emphasizing the potential impact of such disconnect on decision-making and public support within Russia.
- Noting the historical tendency of leadership to be disconnected from the realities faced by common people.
- Critically examining the implications of openly admitting to territorial expansion goals by a high-ranking Russian official.
- Expressing skepticism towards potential negotiations and the true intentions behind them.
- Anticipating a continuation of the current situation based on the stated goals of the Russian establishment.

### Quotes

- "Pushing back the borders is by definition a land grab."
- "Those at the top literally do not understand what's happening in real life because they're so insulated by yes-men."
- "The idea that the Russian hierarchy is so disconnected from realities is mind-boggling."
- "If they are that insulated, they may make decisions harmful to the Russian people and the rest of the world."
- "Any negotiation that Russia enters into based on this statement is to keep the territory they have occupied thus far."

### Oneliner

Addressing the disconnect between Russian hierarchy's expansionist rhetoric and reality on the ground in Ukraine, Beau Young questions the implications of such detachment and its potential impact on decision-making and public perception.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Stay informed about the situation in Ukraine and Russia (suggested)
- Support organizations providing aid to those affected by conflict (implied)

### Whats missing in summary

Deeper insights into the historical context and geopolitical implications of the disconnect between Russian leadership rhetoric and military strategy.

### Tags

#Russia #Ukraine #Geopolitics #MilitaryStrategy #LeadershipDisconnect


## Transcript
Well, howdy there internet people, it's Bo Young. So today we're going to talk about a statement that came out of the
Russian hierarchy  and we're going to talk about the disconnect between that statement and the reality on the ground and
we're going to talk about how
This information might shape the next year a
A high-ranking member of the Russian establishment, Medvedev.
This is a person who was instrumental in Putin retaining power for as long as he has.
He in many ways is seen as a possible successor to Putin.
And currently I want to say he's the deputy chief of security for the Russian Federation
or something like that.
This is not a person who gets their kicks on TikTok.
This is a person who has been in the Russian establishment for a very long time.
This is what they had to say.
This is why it is so important to achieve all the goals of the special military operation,
to push back the borders that threaten our country as far as possible, even if they are
the borders of Poland.
So this is the second time you've had a high-ranking member within the Russian establishment mention
possible action against Poland recently.
The difference is this time it matters.
This time it really is somebody who is in the Russian establishment, not just a frontman,
so to speak.
Now this individual does know that they're a possible successor to Putin, so their rhetoric
has become a little bit more hawkish lately. So it could be written off just as that. But it's also
worth noting that a lot of governments, not just Russia, have a tendency of floating ideas to the
public to get them prepared for something they plan to do in the future. And it may be that.
we don't actually have a good read on what the intention is here other than
it's a very blunt statement saying they plan on taking a bunch of territory and
holding it creating new little countries to act as a buffer and to
maintain their sphere of influence through through military means that's
about all you can really take away from this does this mean that something that
an action against Poland is imminent? No, it doesn't. But it's just one more thing
where Poland is showing up in the future plans of the Russian
administration. Okay, now here's the flip side to this. This is a total
disconnect from the reality on the ground. A total disconnect. You have
everything that's going on with the infighting within the Russian military
establishment between Wagner and the normal generals and all of that stuff. I
know that they've put that to bed or whatever but not really that's right
underneath the surface still. It also is a disconnect from the realities on the
ground. The latest British intelligence assessments suggest that Russia has
adopted a new strategy and that that strategy is to degrade the Ukrainian
military over time, meaning rather than try to maneuver and take more territory
and drive towards the capital or destabilize the national government or
something like that, they're just going to try to consistently weaken it so
eventually they can move forward. This is a strategy that has been tried a few
times throughout modern history. It tends to not work, but that is the current
assessment and it may not be a strategy so much as a lack of options. They may
not have chosen to go this route. They just may not have the capability to
maneuver and drive forward at any substantial rate. This indicates that
the war in Ukraine from the Russian perspective is going to go on and on and
on and they are preparing for that. The idea that the Russian hierarchy, those
at the very top are so disconnected from the realities on the ground that they're
setting conditions for a second war when they're not moving ahead in their
current war is mind-boggling. And it shows how separate the life of the
Russian establishment is from those on the bottom, from those who are going to
be tasked with degrading the Ukrainian forces. It's one of those conditions
that historically, let them eat cake, those at the top literally do not
understand what's happening in real life because they're so insulated by yes-men.
That's a concern as far as the length at which this can go on, because if they
are that insulated, they don't understand how poorly things are going, they don't
understand what appears to be this strategy shift, which again may not be by
choice. They may make decisions that are actively harmful to the Russian people
and the rest of the world and it may create a situation where there is more
support for a change inside of Russia. Again, I do want to point out the Russian
military does not want to go into Poland. That's a horrible idea. But the open
acknowledgement of a desire to create more buffer states to engage in more
land grabs is a little jarring, especially given the previous establishment line
from Russia of, oh well this is really just about getting rid of bad people or
whatever, all of the different ways they've tried to frame it over the last
year. They have always kind of stopped short of openly admitting that it was a
land grab. Pushing back the borders is by definition a land grab. It is by
definition empire building. And you have a very high ranking person within the
Russian establishment saying, hey, this is the goal.
So while I know there are elements that are pushing for negotiations, it seems unlikely
that they're going to go anywhere, or if they do, they will be used for anything other than
to give the Russian military a break to regroup.
Any negotiation that Russia enters into based on this statement is going to be with the
goal of keeping the territory that they have occupied thus far.
And it seems really unlikely that the Ukrainian negotiators are even going to work from that
as a starting point. We can expect this to continue.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}