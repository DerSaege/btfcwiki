---
title: Let's talk about miners, Warrior Met, and emotions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WikZ-ixOQl0) |
| Published | 2023/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the situation with workers at Warrior Met, a coal mine in Alabama, and the push to return to work after a strike.
- Disappointment expressed towards the company, politicians in Alabama, and national politicians for not supporting the workers adequately.
- Acknowledging the disappointment felt by viewers who supported the workers during the strike.
- Clarifying the role of offering support and solidarity rather than making policy decisions.
- Expressing trust in the workers' decisions, whether to strike or move to a new phase.
- Emphasizing the importance of supporting causes worth fighting for, regardless of the outcome.
- Recognizing the families of the workers involved in the labor movement and their ongoing needs.
- Commitment to continue helping individual families if necessary.
- Stressing the significance of the American labor movement story and the continuous nature of fighting for workers' rights.

### Quotes

- "It's not a Hollywood story. It's an American labor movement story."
- "You put support towards something because it's a cause worth supporting."
- "You're there to back their play."
- "We trust the workers when they say it's time to strike."
- "It's an American labor movement story. There's not always a happy ending."

### Oneliner

Beau addresses the return-to-work push at Warrior Met, expressing disappointment and stressing the ongoing support for workers in the American labor movement.

### Audience

Supporters of workers' rights

### On-the-ground actions from transcript

- Support individual families if needed (implied)
- Continue offering assistance and solidarity to workers (implied)

### Whats missing in summary

The full transcript provides a deep dive into the dynamics of supporting workers in the American labor movement and the ongoing struggles faced by workers and their families.

### Tags

#WorkersRights #AmericanLaborMovement #Support #Solidarity #Disappointment


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about the workers in Alabama.
We're going to talk about the mind, and the developments
there, and what it can teach us, and hopefully will prevent
certain feelings from occurring in the future.
I know for some people, this might be the first time they
involved in something like this. So there are emotions and we're going to
kind of go through them. If you have no idea what I'm talking about, the workers
at Warrior Met, the coal mine in Alabama that we've kind of helped through the
strike, there's a push to return to work.
Generally, if I'm involved on a personal level with a cause,
I don't really cover it.
Sometimes that's unavoidable.
But in this case, there are a bunch of good journalists
providing coverage of this.
I will have a link down below to an article that kind of explains what's going on with
the push to return to work and all of that stuff, maybe the end of the strike and so
on and so forth.
If you don't want to read it, short version, the price of coal is really high right now.
So even with the decreased productivity, it isn't having the economic impact that people
had hoped. The news that the workers were returning to work, it has prompted a lot
of feelings and feelings from people who watch this channel. I saw the news about
the miners. I first found out about the strike through your channel and can't
help but feel disappointed that the workers couldn't hold out longer. I'm
wondering how you feel about it. I became very attached to this fight on the other
side of the country just because I gave $10. You met them. How do you handle the
disappointment you must be feeling?" Oh, I'm disappointed. I'm disappointed that
the company didn't come through for them. I'm disappointed that the politicians in
in Alabama didn't back the workers, their voters.
I'm disappointed that national politicians didn't express the level of solidarity that
I think they should have.
I'm disappointed with a lot of different groups.
I am not disappointed with the workers in any way, shape, or form.
They were on strike for 23 months, 23 months.
It doesn't say, it's not specific here, but the $10 that you gave, if you gave it through
this channel, it went to either buy their kids Christmas presents, one of two years,
were to help one of the organizers who was so busy leveraging her network to help the
strike that she didn't leverage it for herself.
So we did that for her.
Yeah, I mean, I get it.
You became attached.
But we were in a support role, we're not policy makers, decision makers in this case.
We were there to offer support, solidarity.
When you do that, you don't get a say.
You're there to back their play.
That's it.
We trust the workers when they say it's time to strike.
We have to trust them when they say it's time to move to a new phase.
Now my personal opinion on what they should do, I literally do not have one.
Long experience on this one.
When you're supplying assistance to another group and they're the ones that are making
the cause. It's better not to form an opinion. You give them the tools for them to put up
the best fight that they can. I'm a big supporter of the idea that you pick fights that are
big enough to matter and small enough to win, but sometimes one comes along and you can't
ignore it, and that's what happened here. These are workers who were taking on
a lot of money, deep, deep pockets, deep pockets, and they basically fought them
to a draw over the course of almost two years. Yeah, we helped out with their
Christmas presents for that time period. What about birthdays? When you see images
of the strike, generally you see the big burly guys and you see the auxiliary. You
don't often put it together and realize it's not just the photos. They have
families. The whole point of the labor movement is to do what's right by the
workers so they can have a decent standard of life. They have to do what's
best for their families and in this case even though there is a push to return to
work they still may need help. Individual families may need help because of the
certain things are happening and if we find out that's the case, we'll do what
we can there too. You don't put support towards something simply because
you think it's gonna win. You put support towards something because it's a cause
worth supporting. This is. It's not even past tense. It is. And aside from that, you
have to remember it's not a Hollywood story. It's an American labor movement
story. There's not always a happy ending. There's just another fight down the
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}