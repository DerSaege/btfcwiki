---
title: Let's talk about a statement by a Russian commander....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=w1fXmGEgjpk) |
| Published | 2023/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses a statement made by someone in the Russian command structure, shedding light on their command and control functioning and settling an ongoing debate.
- Previously, Beau talked about a split within the Russian military command structure between Wagner and the regular army, and now the commander of Wagner is openly accusing Gorosimov and the regular army of actions akin to high treason.
- Wagner, initially functioning as special operations, is now being treated more like shock troops, with tensions and fighting escalating between Wagner and the regular army.
- The split within the Russian military command structure is real, with Grasimov allegedly not providing support to Wagner's troops, leading to significant challenges for Russia's war efforts.
- Estimates suggest that Russia is losing around 5,000 fighters a week, with hundreds of Wagner fighters perishing daily in one location, further confirming the dire situation.
- The outburst from Wagner's commander, filled with emotion and raising his voice, has provided Western intelligence with significant insight and confirmation of the split within Russian factions.
- These conditions are detrimental for Putin, affecting troop morale, logistics, and command and control, with the risk of competing factions potentially turning against him, posing a severe threat.

### Quotes

- "Wagner's troops are just out there exposed with no support."
- "This split is very dangerous for the occupant of the Kremlin."

### Oneliner

Beau sheds light on the split within the Russian military command structure, with tensions rising between Wagner and the regular army, posing a significant threat to Putin's control.

### Audience

Observers, policymakers, analysts.

### On-the-ground actions from transcript

- Monitor developments in the Russian military command structure and potential impacts on international relations (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the escalating tensions within the Russian military command structure, offering valuable insight into the potential implications for Putin's control and Russia's war efforts.

### Tags

#RussianMilitary #CommandStructure #Tensions #Putin #Wagner #InternationalRelations


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a statement made by
somebody in the Russian command structure.
There are two important pieces to it.
One gives us some insight into how their command and
control is functioning at the moment.
And the other settles a debate that is occurring.
One of these I have a little bit of a personal interest in
because 30 days ago, I put out a video
and I talked about how the introduction of Grasimov,
bringing him to be the commander over what
was going on in Ukraine, was probably
indicative of a split within the Russian military command
structure with Wagner on one side and the regular military on the other, and I got a
lot of pushback for that, a whole lot of pushback for that.
It is now 30 days later.
The commander of Wagner is openly accusing Gorosimov and people within the regular army
Russian command of things that can, quote,
be likened to high treason.
According to him, the traditional Russian military
structure, keep in mind Wagner functions more as their special
operations.
Well, at least it did.
Now they've turned them into basically shock troops.
but it did at one time function as their special operations community
and the regular army. The two are separate
and now there's fighting between them.
The commander of Wagner is saying
that the regular military
is just
ignoring them.
That there are commands coming down from Grasimov
that uh...
Wagner's troops
Well, they don't get ammo, they don't get air, they don't get anything, and his troops
are just out there exposed with no support.
That split is real, and that is incredibly damaging to the Russian war effort because
when you look at the maps, it seems pretty clear that Wagner is going to play, or was
intended to play a large part in any offensive in the East. If they're not
getting support and this kind of tension exists between the two factions and now
it has spilled over into the public's view, that's gonna be really bad. It's
gonna be really hard to get things done for them. He said that the orders going
out to not provide them with air support, would not provide them with ammo, quote,
can be likened to high treason in the very moment when Wagner is fighting in
Buckland, losing hundreds of its fighters every day. And that's the other
piece of information. There are estimates that are being contested by those people
who don't want to admit the situation that Russia is in. Those estimates are
suggesting that Russia's lost equal about 5,000 a week. At Bakhmut, Wagner's
commander is saying that they are losing hundreds per day at that one location.
Those estimates are right. This settles the debate. This is coming not from
Western intelligence estimates. It's not coming from the Ukrainians. This is
coming from the commander of the group that is getting hit pretty heavily. Just
at one location, hundreds per day. Those numbers are correct. So this outburst,
and it was an outburst, if you hear it, he's raising his voice, he's yelling, he's
very emotional. Spilling into the public view provided Western intelligence with
a whole lot. And if you're looking for confirmation of those estimates, it's
there. If you're looking for confirmation of the split, it's there.
These conditions are really bad for Putin. It's bad for the troops, their
morale is going to suffer, logistics is going to suffer, command and control is
going to suffer, but for Putin it's really bad because now you have
competing factions that control armed groups and one of them is going to end
up being pushed out. If the one that gets pushed out decides they don't want
to be pushed out and decides to act, they're not going to act against the
generals. They're going to act against Putin. This split is very dangerous for
are the occupant of the Kremlin.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}