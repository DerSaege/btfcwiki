---
date: 2023-02-27 13:35:29.810000+00:00
dateCreated: 2023-02-24 08:09:55.196000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about some legislation in Idaho....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UWDKxTcLhSw) |
| Published | 2023/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Idaho legislation proposed to ban mRNA vaccines.
- Aimed at catering to anti-vaxxers from recent public health crisis.
- Proposed legislation could impact vaccines for RSV, HIV, and cancer.
- Undercuts the concept of medical freedom.
- Those in power shift from representing to ruling.
- Legislation targets bodily autonomy.
- Potential future step to prevent out-of-state vaccine access.
- Beau questions the legislative reasoning and state interest.
- Criticizes the authoritarian nature of the proposed legislation.
- Warns about the dangers of empowering authoritarians in governance.

### Quotes

- "It's amazing how the concept of freedom disappears the second those people who pretend to champion..."
- "They want to rule you. They want to tell you what you can do, what you can do with your own body."
- "You get to deny them the right to seek protection."
- "If you put an authoritarian in charge, they will behave as an authoritarian."
- "They will believe that they can run your life better than you can."

### Oneliner

Idaho legislation threatens vaccine access, targeting medical freedom and bodily autonomy, warning of authoritarian rule.

### Audience

Vaccine advocates

### On-the-ground actions from transcript

- Advocate for equitable vaccine access in Idaho (suggested)
- Support organizations fighting for medical freedom and bodily autonomy (implied)

### Whats missing in summary

Importance of resisting authoritarian legislation and protecting individual rights.

### Tags

#Idaho #Legislation #Vaccines #MedicalFreedom #Authoritarianism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Idaho
and some legislation proposed in Idaho.
And what the long-term impacts would be
if this legislation was adopted.
Now, it is worth noting, before we get too far into this,
that it seems incredibly unlikely that this legislation
actually move forward.
But the very fact that it was proposed
is worthy of discussion.
OK.
So two representatives in Idaho, Tammy Nichols and Judy Boyle,
have put forth HB154 that promises
that a person may not provide or administer
a vaccine developed using mRNA technology for use
an individual or any other mammal in this state. Violation of this would be a misdemeanor.
So obviously, this is designed to cater to those people who were opposed to vaccination during our
recent public health crisis. That's who it's meant to signal to. That's who it's meant to message to.
to message. It's designed to cater to the votes of those people who were encouraged
to be afraid. But that's not where it stops. If this was adopted as it's written right
Now, it would have other impacts.
Vaccines currently in development using this method,
they're there to counter everything from RSV to HIV
to various tons of different kinds of cancers.
Those vaccinations, well, you couldn't get them in Idaho.
They'd be illegal.
This undercuts the main talking point that was so present among those people who were
opposed to vaccination during our public health emergency.
Medical freedom.
It's amazing how the concept of freedom disappears the second those people who pretend to champion
Get a little bit of power because once that happens they're no longer interested in representing
you.
They want to rule you.
They want to tell you what you can do, what you can do with your own body, the decisions
that you can make.
And make no mistake about it, like with any other thing dealing with bodily autonomy and
the state stepping in, the next step here, well obviously it would be to prevent people
from going out of state to get that service. I mean, it's only logical, right?
I cannot think of a legitimate legislative reason for this legislation
to exist. I cannot think of what the compelling state interest is in this,
other than to limit the ability, curtail the freedom of the people in Idaho.
Because you can. Because you know what's best. You can tell them what to do with
their own body. They're not citizens. They're your property. You get to make
that determination. You get to deny them the right to seek protection. When a lot
of Republicans hear that strong man rhetoric, that authoritarian rhetoric, a
of them cheer it until it impacts them.
If you put an authoritarian in charge, they will behave as an authoritarian.
They will believe that they can run your life better than you can.
And if you give them the power of the state, believe me, they're going to try.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}