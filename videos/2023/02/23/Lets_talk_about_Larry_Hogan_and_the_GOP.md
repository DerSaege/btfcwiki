---
title: Let's talk about Larry Hogan and the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OI4MVryQy-U) |
| Published | 2023/02/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Larry Hogan, former Maryland governor and Republican, is showing interest in a potential presidential run by his actions.
- Hogan is critical of former President Trump and aims to bring the Republican Party back to a normal conservative stance.
- GOP officials confide in Hogan, indicating possible support for him over Trump.
- Hogan stated that he wouldn't run if it increased Trump's chances of winning, hinting at organized efforts to prevent Trump's nomination.
- The Loyalty Pledge in the Republican Party is seen as a farce, designed to benefit Trump rather than promoting party unity.
- Some Republicans are working to prevent Trump from getting the nomination, while others in the establishment still support him.

### Quotes

- "The Loyalty Pledge is a farce."
- "He's right. He's absolutely right."
- "There are people within the Republican establishment that still want Trump to come back."

### Oneliner

Larry Hogan's potential presidential run reveals divisions within the Republican Party regarding Trump's nomination and loyalty pledges.

### Audience

Political observers

### On-the-ground actions from transcript

- Organize within the Republican Party to support candidates who aim to prevent Trump's nomination (suggested).
- Challenge the Loyalty Pledge and work towards genuine party unity (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the political dynamics within the Republican Party and potential strategies for upcoming elections.

### Tags

#LarryHogan #RepublicanParty #PresidentialRun #TrumpNomination #PoliticalDynamics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Larry Hogan
and him turning his eyes towards the White House.
Larry Hogan is the former governor of Maryland.
He's a Republican.
And he hasn't announced officially that he's seeking the nomination
or that he's going to run for president or anything like that.
But his actions suggest that he's definitely entertaining it.
making it clear that he's interested in the position. Now, one thing that's
important to note is that Hogan is a vocal critic of former President Trump.
He'd been very open about that. He's also one of the few people in the Republican
establishment that's in a position to launch a bid to seek the nomination and
who is also in a position to attempt to bring the Republican Party back from
the brink of everything that it's become and return it to a normal right-wing party rather
than a far-right authoritarian party.
Over the last week, he's made some appearances and a couple of things that he said I have
found interesting and then one thing he said I found incredibly insightful.
So we're going to go over those real quick.
The first is that he said that during the aftermath of the 2020 election, while Trump
was making all of his claims, that GOP officials would talk to him in private and talk about
how they didn't believe Trump, that they knew these claims were baseless, but then go out
to the public and say the exact opposite, kind of echoing the allegations about what
occurred over at Fox.
That's interesting because it shows that a lot of people within the Republican Party,
they trust him.
They confide in him.
That may indicate that they would support him as opposed to Trump.
The second thing that he said that I found interesting was that he made it clear that
if he believed him running would increase the chances of Trump winning, meaning it would
split the vote of those who would like to return to a normal conservative party, that
he wouldn't run.
Saying that publicly at this point in time indicates to me that there is maybe an organized
effort within the Republican party to ensure that Trump does not get the nomination, that
there is a group of people who may all be interested in making their own runs or their
own bids, but they've kind of decided that any of them would be better than Trump getting
the nomination again.
is an interesting development and will probably cause some unique political
dynamics down the road. And then the third thing, the thing that I found
actually really insightful and kind of one of those things that I can't believe
I didn't think of it, is about the loyalty pledge. The Republican Party, the
establishment, is trying to get anybody who is seeking the nomination to pledge
to support whoever wins the nomination.
This is what he had to say about that.
If they say you're not going to be on the debate stage, if you won't commit to support
the nominee, then President Trump won't be on the debate stage, and I don't think anybody
believes that's going to happen.
He's right.
He's absolutely right.
The Loyalty Pledge is a farce.
It's a farce.
It's a show for the rank-and-file Republican voters that they have been tricking for so
long.
Trump is not going to support somebody who beats him.
We know that.
So the loyalty pledge isn't really designed to increase party unity or anything like that
or to get the candidates to agree to support each other.
It's there to help Trump.
there to keep candidates who won't agree to support him, like Hogan, off the stage, out
of the running.
Or at the bare minimum, embarrass them and say, you know, he's not a real Republican.
He won't back the nominee no matter who they are.
That indicates that there are people within the Republican establishment that still want
Trump to come back and to win the nomination and they're setting the stage to vilify anybody
who stands in their way.
So what it appears with the combination of these statements is that you have a group
of Republicans who are organizing to make sure that Trump doesn't get the nomination.
And you have a group of Republicans in the establishment of the Republican Party that's
still wanting and they're willing to use their positions to stack the deck against Republicans
that actually have a chance of winning.
It's a unique development, and it's a pretty sharp insight that I think is correct.
I think he's right here.
And as the primary season really starts to heat up, we're probably going to see this
dynamic at play.
And it might get really dirty.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}