---
title: Let's talk about rainbows, markets, and 1 out 5 Gen Z-ers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CnNk7pGkBVc) |
| Published | 2023/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Companies have been using rainbows on their packaging as a performative act to capture a wider audience, especially the LGBTQ+ community.
- The performative act of supporting marginalized communities through marketing tactics is shifting thought and capturing a growing market.
- However, the LGBTQ+ community and their allies are becoming more aware of companies' true intentions through their political affiliations and actions.
- Companies that solely rely on superficial gestures like rainbow packaging will soon face consequences as the targeted community becomes more informed.
- To truly support marginalized communities and secure future markets, companies need to take tangible actions like offering employee transfers and covering relocation costs.
- Large corporations prioritize profit over morals or ideology, and failing to support marginalized communities could result in losing more than 20% of the market share.
- This moment challenges companies to decide between genuine support for marginalized communities or facing consequences as the market becomes more socially conscious.
- Companies need to involve their marketing divisions in decision-making processes to ensure alignment with shifting demographics and values.
- Neglecting to do the right thing now may result in abandoning a significant portion of the market that includes Millennials, Gen Z, and their social circles.
- It is a critical juncture for companies to choose between staying viable in the future by authentically supporting marginalized communities or facing market backlash due to inaction.

### Quotes

- "For years, for years, you've had a lot of companies slap a rainbow on the packaging of their product."
- "A lot of people call it rainbow capitalism."
- "The cost of doing the right thing. It's not high."
- "Make no mistake about it."
- "It's just a thought, y'all have a good day."

### Oneliner

Companies using performative gestures like rainbow packaging face the risk of losing over 20% of the market share if they fail to authentically support marginalized communities.

### Audience

Business Leaders

### On-the-ground actions from transcript

- Offer employees in targeted jurisdictions the option to transfer within the company and cover relocation costs (implied).
- Companies need to refrain from supporting politicians who introduce legislation targeting marginalized communities (implied).
- Involve marketing divisions in decision-making processes to ensure alignment with shifting demographics (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of how companies can transition from performative gestures to genuine support for marginalized communities to secure their future market share.

### Tags

#RainbowCapitalism #PerformativeAllyship #CorporateResponsibility #MarketStrategy #InclusionInitiatives


## Transcript
Well, howdy there, internet people, it's Bogian.
So today, we are going to talk about the performative.
We're gonna talk about packaging.
We're gonna talk about rainbows.
And we're gonna talk about a lesson
that major players in the U.S. economy
are gonna learn soon, one way or another.
For years, for years, you've had a lot of companies
slap a rainbow on the packaging of their product. And we've talked about it on the
channel before. It's performative. It is performative. It's not really about
showing support for that group necessarily. For a lot of companies it's
math. It's them trying to capture a wider audience, a wider market, and they're
signaling, they're advertising in that way. We've also talked about how the
performative isn't always bad and how it helps to shift thought. For most
companies it is about capturing that market because it's a growing market.
Millennials, one out of ten, are part of that community. Gen Z, one out of five,
it's a huge market. It's a growing market. It's probably going to level off at one
out of five, but one out of five, that's just the people who are part of that
community. Doesn't include their friends, their loved ones, their family, those who
really care about. And for a long time, the packaging, that rainbow logo or
whatever, that was enough. That was enough to demonstrate support in the
sense that it would help capture that market. That community is being targeted
in jurisdictions all over this country just for living their lives just for
living their lives and one out of five of the future market they're part of
that community plus their friends plus their loved ones plus their families and
And these are people who are pretty savvy.
They understand the way the world works.
They understand how to gather information.
They know to go to OpenSecrets and see
what the major players in your company are supporting
and which politicians they're supporting,
and whether or not the key players in your company
are supporting the politicians putting forth
the legislation that targets them, or their families, or their loved ones, or their friends.
There's a whole lot of companies out there who have marketed with rainbows that are about
to find out the pot of gold they were seeking at the end of it.
It's going to require a little bit more than new packaging because the market they're trying
to capture, well, it's being targeted.
They'll know whether or not your company supported the politicians that went after them.
They'll know whether or not your company offered employees who live in these jurisdictions
where they're being targeted, the opportunity to transfer within the company outside of
the state, and whether or not you help cover relocation costs.
The wrapper on your product, the one-time donation, it's not going to be enough anymore
because there will be companies that step up.
competitor, they may step up and they're going to get that market. This is one of
those moments where a company has to realize that although in most ways it's
a lot like a country. You know, we say it all the time, countries don't have friends,
they don't have morals, they don't have ideology, they have interests. Large
corporations, they don't have friends, they don't have morals, they don't have
ideology. Everything they do is in pursuit of profit. Twenty percent of the
market will be driven away from you unless you stand up now. Unless you do
the right thing now. And it'll last long memories. The cost of doing the right
thing. It's not high. It's not high. Offering the ability to transfer, covering
relocation costs, not donating to certain politicians and groups. That's what it's
going to take to secure more than 20% of the market in the future. If the
companies don't do that, they will be left behind. Make no mistake about it.
it. A lot of people call it rainbow capitalism. It's a good term for it and
this right now is the moment when companies get to decide whether or not
they want to stay viable in the future. There aren't a lot of companies that can
withstand having more than 20% of the market turn against them because of their actions.
Not because of their product, but because of stuff that the top executives do and because
of their unwillingness to help fix a problem that in many ways they might have helped create.
There's a whole lot of companies that need to talk to their marketing divisions and start
including them in the larger decision-making process.
Because the reason your ad companies, your marketing departments have told you for years
to put a rainbow on your package, to play into that, is because they know the demographics.
They know how the market is shifting.
1 out of 10 of Millennials, 1 out of 5 of Gen Z, plus their friends, plus their families.
That's the market you're going to be abandoning by not doing the right thing now.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}