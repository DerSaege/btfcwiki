---
title: Let's talk about numbers, Russia, Ukraine, and charts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=W3mr_QDz-vs) |
| Published | 2023/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Analysis of Russia and Ukraine by the numbers and charts is circulating due to an anticipated strategy shift and the anniversary.
- Many are questioning how Ukraine can stand up to Russia with fewer armored fighting vehicles and troops on reserve.
- Charts comparing military equipment led analysts to believe Russia could quickly take Ukraine before the war began.
- Beau was skeptical of Russia's invasion due to logistical and financial constraints.
- Russia's equipment and personnel numbers are not all deployable, with a significant portion broken or obsolete.
- Russia's advantage lies in its people, but relying on sheer numbers to degrade the Ukrainian military is a horrific strategy.
- The quality of training is a significant factor, with the Ukrainian military being well-trained compared to the Russian military.
- Russian ability to take on Ukraine is dwindling rapidly, with many non-functional or obsolete armored vehicles.
- Russian military personnel numbers include a large portion of support roles that will not see combat deployment.
- Charts do not accurately represent the true available resources for the conflict, with Russia having more reasons to keep reserves than Ukraine.

### Quotes

- "Russia has to maintain probably half of those numbers spread around their country, maybe not a half, maybe a third of those numbers and those charts spread around the country to defend their home country."
- "Those charts do not accurately represent what's truly available for the conflict."
- "If Russia weakens itself going after Ukraine, there might be countries that want a little bit of their dirt."

### Oneliner

Beau breaks down the misconceptions behind Russia and Ukraine's military capabilities, revealing the true limitations and vulnerabilities on both sides.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Analyze the true capabilities of nations before forming conclusions (implied)
- Advocate for diplomatic solutions to conflicts to avoid devastating consequences (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the military capabilities of Russia and Ukraine, offering a nuanced understanding beyond surface-level comparisons.

### Tags

#Russia #Ukraine #MilitaryCapabilities #Analysis #Misconceptions


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today, we are going to talk about Russia and Ukraine by
the numbers and the charts, the graphics, all of that stuff,
the comparisons that are being made.
Because of the anticipated strategy shift, there are a
lot of people, a lot of outlets, that are
circulating charts.
And it also has a lot to do with it being the anniversary.
And they're comparing the military equipment that the two nations have, and it has led
to a lot of questions.
How on earth can Ukraine stand up to Russia when they have so many more armored fighting
vehicles, when they have so many more troops in reserve, so on and so forth?
And those charts, looking at those raw numbers, that is what led most defense analysts before
the war started to say Russia was going to take this quickly because they should have.
If they had decent logistics, if they put together a good plan, if they used basic maneuvers,
They really should have been able to do it in theory because of the mismatched nature
of those charts, those that are circulating.
If you want to see how this played out, you can go back.
There's a video.
I want to say it's called, let's talk about the futures of Russia and Ukraine, futures
being plural.
This was filmed before the shooting started, before the invasion occurred, and we were
talking about all of the different analysis that were going on.
The conventional wisdom at the time was Russia was just going to roll in and take the country
days, weeks maybe.
I was one of the few that was like, yeah, I don't think it's going to go that way.
fact I was in the camp that didn't actually believe Russia was going to
invade because it was a bad idea. It would lead to, well, this. Here we are a
year later. You know, there was the possibility that they pulled off a
lightning-fast takedown of the national government, but it didn't seem likely.
But those who believed it was going to be that one-sided event were looking at those charts.
So let's put those charts into focus and what they mean and what they definitely don't mean.
Those charts are the total number of pieces of equipment or personnel available to the country.
All of Ukraine's are in Ukraine.
They're available, all of it, 100%.
Russia's aren't.
First, based on what we've seen, you're
going to have to just assume that 30% to 50%
of those numbers are either not working, they're broken,
or they are obsolete.
So their numbers are cut in half to begin with.
And then you have to wonder why are they obsolete?
Because Russia doesn't have the money to replace them.
Why are they still using obsolete equipment?
Because Russia doesn't have the money to make those purchases, to replace that older equipment,
the stuff from the Soviet era.
And then you have to factor in the big part.
Russia cannot deploy everything that it has.
can't send it all there. It has a massive country to defend. Now, me personally, I
don't believe anybody's actually going to try to invade Russia. I don't see that
as a likely, even remotely possible thing. Russia is a lot like China or the
United States. They're not countries people are going to invade. But even
And with that acknowledgement, they have to maintain a lot of equipment and personnel
spread around their country to deter that possibility.
So they can't deploy everything that they have to begin with.
Half the stuff they have is broken or it's obsolete.
Those numbers are not as drastically different as they appear in those charts when it comes
to what is truly available.
The one thing that Russia has in abundance that it can tap into and use is people.
That's what it has.
That's the advantage it has, which is, it's horrible.
If that is how they intend on degrading the Ukrainian military, that's horrific.
I would hope that that isn't what they're counting on because that's going to be really
bad.
When you're looking at those charts, keep that in mind.
Russia has to maintain probably half of those numbers spread around their country, maybe
not a half, maybe a third of those numbers and those charts spread around the country
to defend their home country.
Out of what's left, a lot of that stuff doesn't work, or it's obsolete, or whatever.
And then the quality of training is also a big part of it.
The Ukrainian military is well-trained.
The Russian military is not, and the longer it goes on, the lower the standard of training
is, because the more trained troops are no longer on the front lines.
So if you're looking at how Russia may attempt to degrade Ukrainian military capability,
you have to look at their resources that are truly available, and that's people, and theoretically
They could get some of the armored fighting vehicles that they have that don't work.
They could fix them and get them to a functioning state and get them out there, but a lot of
those are obsolete to begin with.
The Russian ability to truly take on Ukraine at this point, it's dwindling and it's dwindling
quickly.
When you're looking at those charts, just remember it's not a video game or something
like that where all of those forces are going to be in play at the same time.
And then with the Russian military, you also have to remember that there are a lot of those
numbers when it comes to personnel that are support.
They're there to maintain the infrastructure of that massive military and they'll never,
will never see the front lines. They'll never be deployed in that way. I know
what the charts look like. Just remember that when the war started the charts
were even more pronounced. It was even more heavily in favor of Russia.
Those charts do not accurately reflect what's truly available for the
conflict. Russia has a lot more concerns and has a lot more reasons to keep
things in reserve than Ukraine does and the addition of why they have
so much obsolete equipment really should factor into this because Russia can't
afford to replace it and if Russia wants to maintain the image of being one of
world powers, they have to have those numbers, and they won't be able to
replace the equipment if they are destroyed. If those armored fighting
vehicles, if those, I don't know what were some of the other things on the charts,
I want to say they listed tanks with armored fighting vehicles as well, but
the artillery pieces, if they don't have that stuff because it got destroyed,
they're not going to have the money to replace it. If they did,
they wouldn't have all the obsolete stuff. They already would have replaced it.
And if Russia weakens itself
going after Ukraine, there might be countries
that want a little bit of their dirt.
And that's something that they have to keep in mind. As it stands,
nobody's going to go and try to invade Russia. But
if they weaken their military to the point where they can only defend key
cities or something like that,
that dynamic might change.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}