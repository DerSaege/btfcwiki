---
title: Let's talk about Russia on the other side....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gc3-XUasse4) |
| Published | 2023/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the concept of the "hard part" in a conflict, using the US invasion of Iraq as a comparison.
- Describing the hard part as the phase after the war and during the occupation, marked by intense resistance and difficulties.
- Pointing out that Russia, currently in Ukraine, is still in the invasion phase and hasn't reached the hard part yet.
- Noting the challenges of dealing with a steeled Ukrainian population that may continue to resist even if the national government falls.
- Mentioning the unconventional fighting style of Ukrainian forces and the lack of preparedness of Russian troops for the hard part.
- Emphasizing the importance of training, equipment, and personnel for handling the post-conflict stage effectively.
- Stating that even in modern warfare, resistance efforts like blending into the populace and fighting during the hard part are common tactics.
- Predicting that Russian forces may struggle to counter a sympathetic resistance movement in Ukraine, unlike coalition forces in Iraq.
- Warning that facing the hard part could be a much more challenging and prolonged phase for Russia in Ukraine.
- Comparing the situation to past conflicts like Afghanistan and underlining the universal nature of dealing with the hard part in modern warfare.

### Quotes

- "That mission accomplished banner, that marked the fall of the national government."
- "They're not up to it. Period. Full stop."
- "It's not something that is unique to Russia."

### Oneliner

Beau explains the "hard part" in conflicts using the US invasion of Iraq as a cautionary comparison, warning of the challenges ahead for Russia in Ukraine.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Train troops for post-conflict scenarios (implied)
- Equip personnel for occupation challenges (implied)

### Whats missing in summary

Context on the evolving nature of modern warfare and the need for strategic planning.

### Tags

#Conflict #MilitaryStrategy #Russia #Ukraine #Occupation


## Transcript
Well, howdy there, internet people, it's Bob again.
So today we are going to talk about the hard part because it's a phrase that I've
said a few times and somebody recently pointed out that while I have said Russia
hasn't gotten to the hard part yet, I haven't really explained what the hard
part is or proven that the hard part is actually
harder than what they're dealing with right now.
And I thought about it, and they're right.
I've repeatedly said it, but I haven't really
demonstrated it in any way.
So today, we're going to go through the looking
glass on this one.
And we're going to talk about what happens
when you hit the hard part.
And we're going to use the US in Iraq as an example,
because it's a pretty comparable situation.
OK.
So March 19, 2003, the coalition air game started.
March 20th, 2003, the ground game starts, May 1st, just a little more than a month later,
mission accomplished.
Y'all remember that?
The banner on the USS Lincoln, Bush standing there, declaring an end to major combat operations
in Iraq.
That was the invasion.
That was the invasion.
the part that Russia a year later is still in. During this this phase the
United States lost, I want to say, 139 troops. The thousands of others were lost
after the invasion, they were lost after the war, quote, and during the occupation,
the hard part. Russia is in a very, very similar situation. The resolve of the
people in Ukraine has steeled. When the national government, if the national
government in Ukraine was to fall, there is zero guarantee that the people of
Ukraine would stop fighting. So it continues and the conflict morphs from
what it is now where there's lines and general strategy to what US troops were
facing after quote mission accomplished. That's the hard part and what should
really be concerning to Russian command is that they are not the coalition
forces. They're not as well supplied, well funded, well trained. Most importantly
they're not trained in the hard part. US forces, British forces, they are. The
phase that occurs after the war and when an occupation sets in, that's when
things get really, really bad.
And I see no reason to believe that Ukraine would be any different.
The Ukrainian forces have blended modern technology with a very unconventional style of fighting
that is incredibly adaptable to resistance style of fighting.
If the national government there was to fall.
And frankly, Russian troops are not trying to deal with it.
It's an entirely different type of conflict.
You're not pushing forward.
You're patrolling, trying to get the people in that area to accept the new rule.
a very different kind of thing and they are not up to it. Period. Full stop. So
when people talk about the hard part or most would probably phrase it something
like if Russia was to enter an occupation stage or the post-conflict
stage, the pacification stage, these are all different phrases meaning the hard
part. They're not up to it. They don't have the training, they don't have the
equipment, they don't have the type of personnel that can actually handle
something like this. It's not the 1940s. Even in the 1940s there were still
resistance efforts. That type of stuff has become much, much more common. Many
countries such as Iran, North Korea, it's in their doctrine to fight that way, to basically
put up a stiff initial defense and then just go ahead and meld into the populace and fight
during the hard part, save the resources for then, because it's so much harder for the
occupation force to counter. The local populace, undoubtedly, especially after
the onslaught of the last year, they're going to be sympathetic to a resistance
movement inside Ukraine. You aren't going to have a lot of people who
are going to sympathize with the Russian troops. They won't get the level of
cooperation that the coalition forces got in Iraq. And again, look at Iraq. Russia is
facing a more difficult situation that might last just as long as longer or longer. So
when you hear people like me talk about the hard part, that's what they're referring to.
That mission accomplished banner, that marked the fall of the national government.
That marked the end of the invasion and the beginning of the occupation.
And most people will tell you 2007, 2008, four or five years later, that's when it was
really bad.
And you're talking about a military that trains for that.
I don't know that Russia ever has.
And I don't see any way they can deal with a stilled resistance, even if they were to
complete their objectives.
It seems incredibly unlikely to me based on everything that we've seen.
So that's what the term means and you can look to Afghanistan, you can look to a whole
bunch of different places and see that same dynamic play out.
It's not something that is unique to Russia.
It's just the way wars are fought now.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}