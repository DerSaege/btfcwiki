---
title: Let's talk about a PSA for Ohio and a question from Europe....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Dk3xKG_qxek) |
| Published | 2023/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing a PSA for people in Palestine, Ohio, with information on environmental safety.
- Officials claim everything is fine with the air, soil, and municipal water in Palestine.
- Residents skeptical of official statement can schedule testing for air and well water by calling 330-849-3919.
- NTSB will be investigating an accident in the area.
- Charges against a reporter arrested for covering the accident have been dropped.
- A town hall meeting was held where environmental agencies reassured residents.
- Concerns about animal health are dismissed as anecdotal.
- Lack of trust in environmental regulatory agencies due to past inaccuracies and overlooking issues involving big money.
- Beau suggests getting well water tested for peace of mind.
- Non-government environmental agencies have not conducted testing yet, raising concerns.
- Americans' skepticism towards official statements stems from past government failures in environmental matters.
- Beau encourages residents in Palestine to prioritize testing their well water and air.

### Quotes

- "Americans' skepticism towards official statements stems from past government failures in environmental matters."
- "Get your well water tested."
- "Non-government environmental agencies have not conducted testing yet, raising concerns."

### Oneliner

Providing environmental safety information for Palestine, Ohio residents, urging skepticism towards official statements and recommending testing of well water and air.

### Audience

Residents of Palestine, Ohio

### On-the-ground actions from transcript

- Schedule air and well water testing by calling 330-849-3919 (suggested)
- Get your well water tested (suggested)

### Whats missing in summary

Importance of independent testing for environmental safety in Palestine, Ohio.

### Tags

#Ohio #EnvironmentalSafety #CommunityHealth #Skepticism #Testing


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to provide a little PSA for people
in Palestine, Ohio.
We'll go over some information there that is relatively
important if you are in the area.
And then we're going to answer a question from somebody in
Europe who just doesn't understand the concern.
And that is pretty thought provoking.
OK, so the officials, as far as basically all of the government
environmental organizations, agencies, nothing to see here.
Everything's fine.
Air is fine.
There is still soil, I believe, that is being removed.
And municipal water is fine in Palestine.
That is the official statement.
If you are less than believing of the official statement
or if you have a private well, there are some resources.
You can call 330-849-3919 and schedule
testing of your own for air and of your well.
That number is 330-849-3919.
And it's open during normal business hours.
So the NTSB, National Transportation Safety Board,
has the will bearing.
It is in their possession now, from what I understand.
they will be looking into the cause of this accident.
The reporter that got picked up for covering it,
apparently literally covering it too loudly,
the charges have been dropped.
There was a town hall that was scheduled for, I believe,
last night, and the railroad company didn't show up.
They said they were afraid for their safety, which means
they're like everybody else in the room, I guess.
But it was at that town hall that the environmental
agencies said, this is where you're at.
And they pronounced everything as being OK.
As far as a lot of the animals, a lot of that
is being chalked up to being anecdotal.
And there's no real way to find out about it.
I would suggest that if a bunch of people
had their well water tested, that if it all
came back and everything's fine, that would provide me a little bit of comfort
if I was living there. That's something that I would be waiting for. So that's a
general rundown of the more recent news and some resources that you can use. Now
Now for the question, person in Europe, the message said, all of your environmental agencies
are saying everything's fine.
Why are people worried?
For an American, that is just a wild statement.
Can you imagine living in a country where your environmental regulatory body comes in
and says everything's fine and you actually believe them?
I mean, that'd be nice, wouldn't it?
Now just for people in Europe, I mean, understand the concept of just accepting what those agencies
say at face value, that is as wild to us as us not believing them is to you.
The reason Americans don't necessarily believe these agencies is because they have a long
history of being less than accurate at times when they really needed to be accurate.
And there have been a number of government officials over the years that when it comes
to environmental issues, well, they have looked the other way if there was big money involved.
railroads are big money. There isn't any evidence right now to say that there is a, you know,
that something is being hidden. But this is one of those moments. It has all of the characteristics
of a time when they might. So, as somebody who routinely is like, you know, you don't
need to worry about that, don't be paranoid, you know, all of that stuff, get your well
water tested. The only thing that is giving, that is like right now, would be something
that I would be taking comfort in if I was in that area, is that this has been going
on a while now. It's been a number of days. Non-government environmental agencies and
organizations, those that aren't government funded, those that aren't linked in that way,
they're aware of it. Typically when something like this happens, they show up and do their
own testing of everything kind of on the sly. I have been looking, even though I'm sick,
The one thing that I've constantly been checking for is press releases from non-government
entities doing testing, and I haven't found any.
That may change.
There may be one tomorrow, but I haven't seen anything yet.
The reason Americans have this attitude towards it is because there have been a number of
instances where the the state local federal government everybody dropped the
ball or looked the other way or things just did not go the way they were
supposed to and it hurt people so it has it has reduced faith in those
organizations across the board. So that's the information again if you're in
Palestine 3308493919 lets you schedule air testing even in your home and your
well water get your well water tested. Anyway it's just a thought you'll have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}