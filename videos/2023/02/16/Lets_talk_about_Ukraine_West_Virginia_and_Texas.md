---
title: Let's talk about Ukraine, West Virginia, and Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=sOYGtO7POpk) |
| Published | 2023/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ukraine is more like West Virginia than Texas, and he explains why.
- There is a common misconception about the dynamics in Ukraine that Beau wants to clear up.
- The Soviet Union was often described as Russia due to commentators like Beau.
- The Soviet Union was a union of Soviet Socialist Republics, not just Russia.
- The Ukrainian Republic existed for a long time under the Soviet Union, separate from Russia.
- Misconceptions arise from conflating Russia and the Soviet Union, especially during the Cold War.
- When the Soviet Union dissolved, the republics became independent countries.
- Russian propaganda relies on Americans confusing the Soviet Union with Russia to undermine Ukraine's independence.
- Understanding the history of Ukraine within the Soviet Union helps combat Russian propaganda.
- The dissolution of the USSR led to independent nations that were already part of it becoming separate countries.

### Quotes

- "It isn't that Ukraine broke away from Russia. It's that the USSR dissolved and all of the independent nations that were already part of the Soviet Union became independent nations outside of the Soviet Union."
- "The reason this is important is because a lot of Russian propaganda today relies on Americans conflating the Soviet Union and Russia."
- "Understanding this helps safeguard against Russian propaganda that is particularly geared towards Americans."

### Oneliner

Beau explains why Ukraine is more like West Virginia than Texas, clearing up misconceptions about its history within the Soviet Union and combating Russian propaganda.

### Audience

History Buffs, Anti-Propaganda Activists

### On-the-ground actions from transcript

- Educate others on the history of Ukraine within the Soviet Union to combat Russian propaganda (implied).
- Support Ukrainian independence and sovereignty through awareness and activism (implied).

### Whats missing in summary

The full transcript provides a detailed historical explanation of Ukraine's relationship with the Soviet Union, offering insights to counter Russian propaganda effectively.

### Tags

#Ukraine #SovietUnion #History #RussianPropaganda #Independence


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today we are going to talk about how Ukraine is more
like West Virginia than Texas.
That'll make sense later.
There is a very common misconception about the
dynamics over there.
And we're going to clear it up, because I saw this
misconception at play and after looking to see how widespread it was, I also
realized it was kind of the fault of people like me. So we're gonna clear it
up today. I saw somebody trying to explain the situation over there by
saying that it was like Texas broke away from the United States and then got
armed by China. These analogies are never perfect, but that one has a fundamental
flaw. Okay, so the Soviet Union, the Union of Soviet Socialist Republics, okay, it
It often got described as Russia because of people like me, people who were providing
political commentary or foreign policy takes.
They often said the Russians rather than the Soviets.
And there's a reason for that and we'll get to that.
But understand the Soviet Union was not Russia.
The Soviet Union was a union of Soviet Socialist Republics, just like the name says.
You have to look at it more like the Soviet Union was the federal government, and the
republics were the states, even though they were, and this is important, national-level
administrative units.
The Ukrainian Republic, under the Soviet Union, existed for a really long time.
Ukraine is, I don't know, a hundred years old, in just going back to the beginning of
the Soviet Union, to when that process started.
It didn't break away from Russia, it had been separate.
So why did people like me conflate Soviet and Russian?
Because the Soviet capital was in Moscow, which is in Russia, it's in that Soviet Republic.
So most of the people who worked for the Soviet government, meaning the overall government,
particularly those who would be engaged in discussing stuff with Americans, were Russian.
So the Russians want this became common, forgetting about all of the other ethnicities and the
other groups of people who were present in the Soviet Union.
This was said all the time during the Cold War.
Russia and the Soviet Union were interchangeably used, but they're not.
And that's led to this misconception.
So when the Soviet Union dissolved, all of the republics became countries of their own
because they were national level administrative units, technically they were nations on their
own the entire time under the Soviet Union.
Now that's theory, that's what's written in the Constitution and all of that stuff.
But I mean we do understand in practice it was very centralized and the Russians controlled
most of it.
that they were their own country even under the Soviet Union.
So when the Soviet Union dissolved, they became their own country.
They didn't break away from Russia because they weren't part of it.
If you want to make that analogy work, the United States dissolves.
Russia is Virginia, has an outsized role in the federal government and everything.
perfect. Ukraine is West Virginia. A long, long time ago, they were together, so Virginia
invades. That's more what it's like. The reason this is important is because a lot of Russian
propaganda today relies on Americans conflating the Soviet Union and Russia, because they
They want Americans to believe that Ukraine wasn't a country, that the Ukrainian people
aren't real.
They're just an offshoot of Russia and therefore belong to Russia.
This is why it's important to understand that.
The reason people don't really, I'm willing to bet it goes back to commentators during
the Cold War who knew what they were saying in their own mind, but when it came across,
it got lost.
the idea that it was just talking about how Russia had an outsized role, got lost, and
the Soviet Union became Russia in the minds of a lot of people.
So when the Soviet Union broke up, all of these little countries broke away from Russia.
But that's not really what it was.
When the Soviet Union dissolved, the republics inside the Union of Soviet Socialist Republics
became countries of their own outside of that union. That's it. Understanding this helps
kind of safeguard you against Russian propaganda that is particularly geared towards Americans
because of their habit of looking for the greater power and disregarding minority groups
and other ethnicities.
So understanding that the Ukrainian Soviet Socialist Republic, I think is what it was,
existed since, I don't know, 1922, somewhere around there.
important and it helps safeguard against this. It isn't that Ukraine broke away
from Russia. It's that the USSR dissolved and all of the independent nations that
were already part of the Soviet Union became independent nations outside of
the Soviet Union. That's how it happened. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}