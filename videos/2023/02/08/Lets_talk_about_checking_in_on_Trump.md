---
title: Let's talk about checking in on Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mt75tXFnKf8) |
| Published | 2023/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Former President Donald J. Trump seems to be struggling with adjusting to private life.
- Trump has done a complete about-face on early voting, now supporting ballot drop-off boxes while still claiming the election was stolen from him.
- Trump was upset about not being invited to a conservative donor retreat and lashed out on Twitter.
- He released his own "State of the Union" message, criticizing President Biden and lashing out at DeSantis, resurfacing old allegations about him.
- Trump's erratic behavior and potential for a third-party run could impact the Republican Party's chances in future elections.
- There are concerns about Trump running a third party out of spite, potentially splitting the Republican vote and ensuring Democratic victories.
- Trump's behavior may lead to conflicts with other Republicans, potentially harming the party's chances in elections.
- The Republican establishment should be prepared for a potential third-party run from Trump and its implications.
- Trump's actions may alienate moderate voters and independents, affecting the Republican Party's standing.
- The Republican Party does not seem prepared for Trump's potential third-party run, raising concerns about its impact on future elections.

### Quotes

- "Former President Donald J. Trump seems to be struggling with adjusting to private life."
- "It does not appear that Trump is going to pull punches when it comes to Republicans."
- "I haven't seen anything indicating that the Republican Party is really prepared for that eventuality."
- "It seems that he is becoming more and more erratic."
- "It's classic Trump."

### Oneliner

Former President Donald J. Trump's erratic behavior and potential third-party run could have significant implications for the Republican Party and future elections.

### Audience

Political observers

### On-the-ground actions from transcript

- Prepare for potential political shifts within the Republican Party, considering the implications of Trump's actions (implied).

### Whats missing in summary

Insights into the potential impact of Trump's behavior on the Republican Party's future direction.

### Tags

#DonaldJTrump #RepublicanParty #Elections #ThirdPartyRun #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to check in a little bit
on former President Donald J. Trump
and just kind of provide an update
on how things are going in his world.
The appearance from the outside
is that he is not adjusting well to private life.
He is becoming very cartoonish in a lot of ways.
Just over the last couple of days, he has done a complete about face on early voting.
He is now apparently in favor of having like ballot drop-off boxes and stuff like that,
while simultaneously saying that the election was taken from him.
He is still floating those baseless claims and leaning into that, but has now reversed on the ballot boxes.
He apparently had his feelings hurt when he was not invited to a donor retreat for a conservative group and lashed out
on his little Twitter thing about that.  He is Biden's state of the union, apparently really bothered him.
He released his own quote, state of the union that he said was the real one.
Um, it was exactly what you would expect.
It was Trump being Trump, but I think most notably he lashed out at
DeSantis in a unique way, on his social media platform there, he resurfaced images from
a scandal involving DeSantis when he was a teacher.
The allegations contained in the reporting about all of this are basically that he went
to parties with students.
That is where the allegations end as far as all reporting that I have seen.
The implications that seem to be present in Trump's statements go a wee bit beyond that.
I feel like I have strong enough, you know, I don't like DeSantis credibility to remind
everybody that you can't trust Trump.
You know, I don't want to see DeSantis in the White House at the same time.
That's a pretty big allegation to just kind of sling out there with no evidence.
I'm not sure how DeSantis is going to respond to that.
I don't think he can just stay quiet anymore.
If he continues to, Trump will continue to sling stuff at him and eventually it's going
to stick.
So we are probably closing in on the point where they are really going to go at each
other.
When that occurs and how that shakes out is going to determine a lot.
There is the possibility that the behavior of the two of them and the way they interact
puts off moderate voters and certainly independents.
There's also the possibility that one actually knocks the other one out of the race or that
they knock each other out of the race and send the Republican Party back towards more
of a Bush-style Republican party.
It does not appear that Trump is going to pull punches
when it comes to Republicans,
particularly those that he may feel
owe him some kind of loyalty.
If I was part of the Republican establishment right now,
I would be incredibly concerned about a potential third party run from Trump,
not because Trump would win, but because he might do it out of spite and out of a
potential for raising funds and that he might draw off enough to pretty much
ensure that the Democratic Party wins.
That is a very real possibility, and it has to be something that the Republican establishment
is preparing to counter.
How they plan on doing that, I don't have a clue.
Trump is more erratic than usual outside looking in, my personal opinion.
It seems that he is becoming more and more erratic.
a third party run casting himself as a maverick against the swamp, it's totally on brand
for him.
And if he doesn't care about winning and just cares about getting even with those who he
feels slighted him and maybe making a little money at the same time, I mean, let's be
honest, it's classic Trump.
I haven't seen anything indicating that the Republican Party is really prepared for that eventuality.
And I think they probably need to get prepared for it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}