---
title: Let's talk about Biden's State of the Union fiddle playing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SYeB7fPCK5I) |
| Published | 2023/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Provides an overview of the State of the Union address, focusing on the accomplishments of the Biden administration.
- Recognizes Biden's insertion of progressive ideas with good rhetorical devices and how it impacts messaging.
- Points out a notable example in the speech regarding veteran homelessness and housing as a human right.
- Describes a memorable moment where Biden sounded like a southern preacher while discussing democracy versus authoritarianism, particularly in the context of China.
- Speculates on Biden's possible reference to Trump during the speech.
- Analyzes Biden's strategic move in playing the Republican Party regarding proposed changes to Medicaid and Social Security.
- Comments on how Biden's speech may impact future legislative priorities and Republican leverage.

### Quotes

- "No person should be homeless, especially our veterans."
- "Name me one, name me one."
- "Trump."
- "He played them."
- "It's going to be really hard for them to play hardball."

### Oneliner

Beau provides insights on Biden's State of the Union address, from progressive messaging to strategic political maneuvers, shaping potential legislative priorities and Republican response.

### Audience

Political observers, Democrats

### On-the-ground actions from transcript

- Watch the State of the Union address for insights on Democratic accomplishments and future plans (suggested)
- Stay informed about political strategies and messaging for upcoming elections (implied)

### Whats missing in summary

Analysis of how Biden's speech may impact public perception and future political discourse.

### Tags

#StateOfTheUnion #BidenAdministration #ProgressiveIdeas #DemocracyVsAuthoritarianism #RepublicanParty #LegislativePriorities


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we're going to talk about the State of the Union.
We're gonna talk about the address.
What happened, we'll hit the highlights
and provide a general overview.
There are some entertaining moments in it, that's for sure.
Okay, so first, it's a normal State of the Union address.
Okay, it goes over a lot of the accomplishments
of the Biden administration.
That's something that I constantly talk about
on this channel where I'm like,
they don't do a good job of talking about what they do.
So if you're interested, maybe watch it
and you'll pick up some of what the Democratic Party
has been able to accomplish.
Another thing that I talk about is that Biden
is very status quo and he is.
But one thing that I always have to give him
is that he does insert very progressive ideas at times
in ways that matter because it helps carry that message forward,
especially when it's done with a good rhetorical device
and it's embedded in other language.
There was a really good example of this in the speech.
When he is talking about veteran homelessness,
He doesn't say that no veteran should be homeless.
He says no person should be homeless, especially our veterans.
This is one of those things that carries that idea forward.
It may not sound like a huge difference, but believe me, one is way closer to housing as
a human right than the other.
One is conditional on service.
One is, you're here, therefore you should have a place to be.
There was a very unique moment that I am certain is going to be memed over and over and over
again.
And that's, name me one, Biden got a little excited at one point and he honestly sounded
like a southern preacher. He was talking about how democracies are kind of
rising and authoritarian governments are in decline. This was in context of China
and of course the framing which we've talked about on the channel, that the new
Cold War is going to be democracy versus authoritarianism. That's going to be the
framing of it and he's talking about that and he says to name a world leader
that would want to change places with China's leader. Kind of suggesting that
China's really in check and has a lot of walls being thrown up and that's true. It
is. We've covered a lot of the military and foreign policy stuff on the
channel over the last week but there's also a lot of economic stuff going on
as well and he got excited when he said that and he repeated name me one name me
one and I don't know if it's just because it's the way that I talk but I
couldn't help that he was trying to say two things at one time one being that
China is kind of being isolated and that, you know, the democracy side of the Cold War
is winning.
But I think the other one was that he really wanted people to envision a leader who would
trade places with him, who would trade places with the leader of China.
I think that's why he said it the way he did and repeated it the way he did.
Go ahead.
Try to picture one.
Who do you come up with?
I can only come up with one.
The guy who fawned all over him called him king, but he said that he is not king.
And I said, you're president for life.
That makes you king.
And he really liked that.
Trump.
I think that was a shot at Trump.
I don't know that.
I mean, it could just be me seeing the way that I talk in his speech.
I don't know that that's what he was doing, but if he was trying to allude to Trump, it
certainly worked on me.
And then there was the great scene where he played the entire Republican Party like a
fiddle.
He was talking about the debt ceiling, the manufactured crisis that the Republicans have
created and he's like, you know, and they don't want for corporations to pay their
fair share.
They'd rather, and he says some Republicans would rather sunset Medicaid and Social Security
or Medicare and Social Security.
the Republican Party started booing and yelling and screaming liar. Fantastic. So
what he said was factual. Republicans certainly have proposed that. Some have
put pen to paper and turned it into a plan or a proposal. That absolutely
occurred. The question you have to ask yourself is why didn't the Republican
party respond the way they did in the State of the Union when one of their own
party members proposed it. They didn't have a problem with it being proposed.
They had a problem with Biden telling you that they had proposed it. That was
the real issue and they did. They, one person and you could probably guess who,
screamed out liar, you know, because they're super classy and he was telling
the truth and he followed it up with like, you know, if you have questions I can send
you the proposal, reach out to my office.
And they all had these looks on their face, just, you know, very upset that he would say
such a thing, but it's true.
That was absolutely a proposal.
But here's the interesting part about it, and this is how he played them.
Because they made such an uproar about it.
Because they literally screamed a liar.
They're going to have a real hard time going back on that now.
The proposal is shot.
It's done.
I don't think that the Republican Party is going to go after Social Security now.
And I think that Biden accomplished that in that speech.
He definitely seemed to play them, which reduces a lot of their leverage.
It's going to be really hard for them to play hardball and suggest that they're going to
try to cut this, something that is a Democratic Party legislative priority.
If they've already publicly screamed and booed about the idea and saying that it just wasn't
true. They would never do that. And he knew that he did it because afterward,
after they finished booing, he was like, you know, it's okay. I love conversions. I
love that. So I'm assuming that we could say that's off the table now. He played
them. So those are the real highlights of it as far as things that are going to
have an impact. I would suggest you watch it because if you want to know what the Democratic
Party has accomplished or what they plan to accomplish, you're going to see it there.
I would also view a lot of what he is saying is coming up next as kind of a road map to
potential run in 2024. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}