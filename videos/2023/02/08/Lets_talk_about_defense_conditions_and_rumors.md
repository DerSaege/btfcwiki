---
title: Let's talk about defense conditions and rumors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d9AuGAH_TTE) |
| Published | 2023/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Defense Condition (DEFCON) system, developed in the late 50s, as a scale measuring how close we are to catastrophic events.
- Points out the common misconception of people using DEFCON levels incorrectly, where DEFCON 5 is actually good and DEFCON 1 is catastrophic.
- Mentions the official instances when the U.S. went to DEFCON 2, including the Cuban Missile Crisis in 1962.
- Notes a debated instance during the first Gulf War where DEFCON 2 was raised, possibly for political reasons rather than imminent nuclear threat.
- Talks about the development of THREATCON (now FPcon) to gauge readiness against irregular actors.
- Emphasizes that DEFCON numbers are not public and are released after the fact, leading to rumors about current threat levels.
- Contrasts times when major global events like the breakup of the Soviet Union or the fall of the Berlin Wall did not trigger a move to DEFCON 2.
- Provides a tangible example to dispel the rumor that the U.S. is at DEFCON 2 by referencing the State of the Union address and the absence of key figures.
- Advises watching "Dr. Strangelove" for more context on these defense-related matters and to be aware of sensationalist rumors driving clicks for revenue.
- Concludes by reminding viewers to keep events in context and to not sensationalize unless they are on the scale of what happened in September.

### Quotes

- "DEF CON 5 is good. DEF CON 1 is, you know, fireballs from the sky."
- "That is not something that would happen at DEFCON 2."
- "If whatever's happening isn't on the scale of what happened in September, it's probably not occurring."

### Oneliner

Beau explains the DEFCON system, debunks rumors, and advises context to gauge plausible threats accurately.

### Audience

National security enthusiasts.

### On-the-ground actions from transcript

- Watch "Dr. Strangelove" for context on defense-related matters (suggested).
- Stay informed about global events to understand potential threat levels better (implied).
- Avoid sensationalizing current events without proper context (implied).

### Whats missing in summary

Watching the full video provides detailed insights into the DEFCON system and dispels common misconceptions about current threat levels.

### Tags

#DEFCON #NationalSecurity #Rumors #Context #ThreatLevels


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about rumors, again,
and kind of dispel one.
But this one's cool because in the process of it,
you will get some context, some history.
And with that context, you will be
able to gauge this stuff yourself in the future.
It will hand you the tools, so if this ever comes up again, you can kind of put it in a frame of reference and see if
it's even plausible.  The rumor is that right now, we're at DEFCON 2, or not.
Okay, so what is DEFCON? Defense Condition. It's a system developed in the late 50s,
and it measures, it's a scale that tells us how close we are to things going really, really bad.
People often get the scale inverted. You will see people say, I'm gonna go to DEFCON 5 on them.
Well, enjoy your picnic. DEF CON 5 is good. DEF CON 1 is, you know, fireballs from the sky.
Okay, so, developed in the late 50s. Been around a really, really long time.
How many times do you think we've actually gone to DEF CON 2?
There's one obvious one in 1962.
DEFCON 2 is one step below things going really bad.
The Cuban Missile Crisis, that's one.
That is one example of going to DEFCON 2.
There's another one.
And when I say this, there will probably
be bickering about it as to whether or not
it should really count.
and that is at the beginning of the first Gulf War.
I happen to agree with the people who say it shouldn't count.
I actually get their point, and I agree with it.
However, the reporting says that they actually
went to DEF CON 2, so you have to count it.
The reason there is a little bit of bickering
is because people think that it was political,
they think that it had more to do with raising readiness across the board than
what DEFCON is actually really supposed to be used for. Keep in mind in in 91 it
was a country that had bio, that had chem, that was a sponsor and there was a decent
risk of irregular hits going on at like bases in Europe and stuff like that.
So the theory is that that had a lot to do with it.
It was a way to get everybody on their toes to protect against that, not that we were
actually close to a nuclear exchange, which is what DEF CON is for.
Since then, the military has developed THREATCON, but it's not called THREATCON anymore.
called FPcon, force protection con, force protection condition, that that system
gauges specifically for irregular actors. So that's it. That's it. Those are the two
times that we officially went to DEF CON 2. Now might there be others that are
classified in the sense that, you know, either the US or the Soviets or the
Chinese lost something and everybody got really spooked and it went up, maybe.
But that's it. That's it as far as officially. So with the very rare
occurrences, why does this become a rumor? Because DEFCON numbers aren't public.
They aren't released into a way after the fact.
So it's one of those things, you have to be in the know to really know.
Rather than point to the times when we went to DEFCON 2, I'm going to point out some
times that we didn't.
I don't know, the breakup of the Soviet Union, I mean that was a pretty scary time.
you're talking about the nuclear arrangements of the world, we didn't go
to DEFCON 2. The fall of the Berlin Wall didn't go to DEFCON 2. That day in
September when the Pentagon was literally hit, didn't go to DEFCON 2. We
were at DEFCON 3 with like a standby to go to DEFCON 2, but we didn't actually
go. A balloon did not cause us to go to DEFCON 2. Going to DEFCON 3 is pretty
rare as well. Okay, so there's your context. It has to be somewhere above
what happened in September and below the Cuban Missile Crisis. That's what gets
you in that range. Now, how do I know for certain? I wanted to include this in the
previous video about the rumors, but I wanted to be able to point to something
and say this is how you know for sure we are not at DEFCON 2. Who watched the
State of the Union last night? Just as it always is, one designated survivor was
conducted in the normal place, the entire command and control of the US, with the exception
of one person, was in one location.
That is not something that would happen at DEFCON 2.
It's a rumor.
Again, I'm going to suggest going back and watching Dr. Strangelove.
If you're not familiar with this stuff, you should probably go watch it.
These rumors are going to get more and more common.
They're going to happen a lot because there are going to be a lot more standoffs.
There are going to be elevated tensions at times.
Keep that as your frame of reference.
What happened in September did not warrant going to DEFCON 2.
Always remember that because this is one that will certainly be used to drive clicks and
get revenue by being sensationalist.
This idea.
If whatever's happening isn't on the scale of what happened in September, it's probably
not occurring.
And then if you happen to see like the entire leadership of the US just like hanging out,
chilling together, that's probably also a good indication that we are not on the cusp
of nuclear war.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}