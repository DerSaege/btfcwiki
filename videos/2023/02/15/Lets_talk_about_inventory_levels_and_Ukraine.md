---
title: Let's talk about inventory levels and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Dd4o51xPfK0) |
| Published | 2023/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the lack of coverage on Ukraine, inventory levels, NATO, and potential Russian offensive.
- NATO countries, including the US, are experiencing a decline in ammo stockpiles below recommended levels.
- Reasons for commentators' disinterest in this topic, including different schools of thought.
- Some view the decline in inventory levels as fulfilling the purpose they were set for.
- Others speculate that defense contractors will profit from restocking munitions.
- Possibilities of the Russian offensive: they make significant gains or Ukrainian forces achieve a breakthrough.
- Potential outcomes include a Ukrainian counter-offensive impacting Russian-occupied areas.
- The situation is fluid, with uncertainties about the extent of Russia's capabilities.
- The conflict is likely to continue for an extended period, with no quick resolution in sight.

### Quotes

- "It's actually starting to happen now."
- "Do you think that Raytheon doesn't have lobbyists sleeping in congressional offices right now?"
- "It has started. Possible outcomes."
- "This isn't going to be over soon."
- "All of that devastation will continue to occur for quite some time."

### Oneliner

Beau delves into Ukraine, NATO, declining inventory levels, and the ongoing Russian offensive, exploring commentators' disinterest and potential outcomes in this fluid situation.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor the situation in Ukraine and stay informed about the conflict (suggested).
- Support organizations providing aid and resources to those affected by the conflict (exemplified).

### Whats missing in summary

In-depth analysis of the geopolitical implications and potential consequences of the conflict in Ukraine.

### Tags

#Ukraine #NATO #Russia #Geopolitics #Conflict #InventoryLevels #Commentary #DefenseContractors #GlobalCitizens #Aid


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Ukraine
and inventory levels and NATO and why some people
who provide commentary on this are disinterested
in this particular topic.
They're not talking about it and why that is.
And then we will talk about what may happen in the future,
what the outcomes may be in relation
to something else that is happening over there right now.
You got a message.
The three people I turn to for coverage, blank blank and you, aren't really talking about
how ammo stockpiles are getting lower for NATO and crossing into numbers that are lower
than what they're supposed to maintain.
Is there a reason why?
Also, what are the positive effects, I'm sorry, what are the possible effects of the Russian
offensive and when will it start? Okay, first question first. You named three
people. I don't like speaking for other people and particularly in this case
because there are three different elements to why a commentator might be
less than interested in this topic and you named three people. You can infer
that what you will. Okay, so if you have no idea what this is about, NATO countries, the US military,
they maintain levels, inventory levels, X number of this type of munition. We've talked about it
before on the channel, and before I was kind of debunking it and saying, yeah, we're not really
at that point yet where it's dipping below the recommended levels. We are now. It's actually
starting to happen now.
Why are some people less than concerned about that?
Okay, so why do those inventory levels exist?
This is school of thought one.
Why do those inventory levels exist?
Why is the US defense budget so bloated?
Because its doctrine says it needs to be able to fight its two largest competitors at the
same time.
That being Russia and China fighting both at once.
It's a doctrine that the U.S. has more or less held since World War II.
That's what those numbers are based off of.
Okay, so at the time those numbers were drawn up, China's military was considerably weaker
and Russia's military was considerably stronger.
They've changed, but the overall numbers, pretty much the same.
These munitions that are being pulled out of storage, they're not being set by the side
of the road.
They're being sent overseas.
They're being sent to Ukraine, and Ukraine is using them for hitting the Russian military.
As the inventories decline, so does the military power of Russia.
not just being wasted. So it is one school of thought would suggest that it
doesn't actually matter because it's accomplishing the reason those inventory
levels were set and that that was their purpose to begin with. What was to deal
with that eventuality. So that's one school of thought. It's it's just
performing its mission right now. Another, more cynical but not inaccurate
school of thought would say if this person who believes this was to put it
into a sentence it would be something like do you think that Raytheon doesn't
have lobbyists sleeping in congressional offices right now to
order more munitions, to raise them back up, to get the production underway, and to
deliver the resources to do it. Basically, yeah, they're going down, but they're
gonna be replaced. That's another one. A third school of thought comes from people
who see it as incredibly unlikely that the United States would actually be in a
scenario where it is fighting China and Russia at the same time.
Um, so those are the three reasons, those are the three different schools
of thought, why some people might just not be talking about this because it's
not particularly concerning for them.
Um, and I would say that school of thought too, saying that, you know,
there's a bunch of defense contractors who are going to make a whole bunch of
money by bringing these inventory levels back up, would also play into school of
thought one. Those two complement each other very, very nicely. Okay, now when is
the Russian offensive going to start and what are the possible outcomes? It's
started. It has started. Possible outcomes. Russia makes big gains, gets closer to
the hard part of the war.
The flip side of that is that they don't and Ukrainian forces are capable of achieving
some kind of breakthrough, breaking through the lines.
That would further undermine the Russian war effort and Putin.
Putin in a lot of ways is kind of make or break.
If for example, a Ukrainian counter offensive, and to be clear, this isn't like a rumor
or something.
This is something I am making up as I'm standing here right now.
This isn't something that I expect to occur or have heard anything about.
But as an example, if a Ukrainian counteroffensive was able to break the connection between Crimea
and the rest of Russian occupied area, that would be a huge loss.
a Ukrainian counter-offensive, again, something wild, was actually able to
retake Crimea. These things would be devastating. You're talking about the
beginning of an offensive that is probably going to be met by a
counter-offensive. Lines are probably going to change. Which way it's going to
go, you don't know yet. If what has started is the extent of Russia's capabilities, I don't think
that they're going to do well, but I don't know that this is the full force of the offensive that
that they have planned.
What they have started with, it hasn't really
performed exceedingly well.
And if that's all they've got, it's not going to amount to
much, but I don't think it's all they have.
I think there's going to be more to it.
This seems more like a softening than the real push.
So we're going to have to wait and see.
At this point, it's incredibly fluid.
And we're in that protracted stage.
Lines are going to go back and forth.
It's going, sadly, this isn't going to be over soon.
I know that's what everybody keeps
angling towards that, troops will be hung by Christmas and all that.
All of that devastation will continue to occur for quite some time.
And it doesn't matter who's quote winning, that's the future over there for, I would
say for at least a year.
Anyway, it's just a thought.
You have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}