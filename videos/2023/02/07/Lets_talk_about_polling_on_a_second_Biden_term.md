---
title: Let's talk about polling on a second Biden term....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KDI-UQJ1Leo) |
| Published | 2023/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The enthusiasm within the Democratic Party for a second Biden term is declining, with only 37% of Democrats wanting Biden to run again compared to 52% in October.
- The decline in enthusiasm is particularly noticeable among younger voters, dropping from 45% to 23%.
- Fairness in assessing the Biden administration's performance is questioned based on individuals' expectations and whether those expectations were reasonable.
- Concern arises for the Biden administration as low enthusiasm, especially among younger voters, could impact a potential second term.
- Younger voters get their news from platforms like apps, TikTok, Twitter, and YouTube, which shapes their perceptions differently from traditional media.
- The Democratic Party needs to address the lack of social media cheerleaders and better communicate their wins, especially those impacting younger demographics.
- Beau suggests that the Democratic Party needs to focus on policies directly affecting younger voters and improve messaging to bridge the enthusiasm gap, particularly among the youth.

### Quotes

- "Biden cannot win a second term with low enthusiasm among younger voters."
- "The Democratic Party as an organization needs to change that and change it quick."
- "It doesn't pay to be a Democratic cheerleader."

### Oneliner

Enthusiasm for a second Biden term is waning, particularly among younger voters, prompting concerns and the need for improved messaging and outreach by the Democratic Party.

### Audience

Democratic Party members

### On-the-ground actions from transcript

- Get actively involved in promoting Democratic wins and policies on social media platforms (suggested).
- Advocate for better communication strategies within the Democratic Party to bridge the enthusiasm gap, especially among younger voters (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the declining enthusiasm for a potential second Biden term and offers insights into the factors influencing this trend, along with suggestions for improving communication and outreach strategies.

### Tags

#DemocraticParty #BidenAdministration #YouthVote #SocialMedia #Messaging


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about a poll.
A poll that is measuring the enthusiasm within the Democratic Party for a second Biden term.
And it prompts a few questions and we're going to kind of go through those.
I will tell you the numbers are not wonderful for the Biden administration in this poll.
The way it is being framed in the media is that it's scary.
That it should be giving the Democratic Party chills.
I don't know that that's accurate either.
But the poll itself shows a decline with enthusiasm for a Biden second term.
Back in October, 52% of Democrats were like, yay, we want Biden to run again.
That number is now 37%.
Now, these numbers, they swing wildly throughout the years.
37, I mean, that's a little outside the normal range of the swing.
So they have to pay attention to it.
Another thing that is worth noting is that's the big number up at the top of the poll.
When you get into the demographic information, what you find out is that that swing,
it is in large part based on people my age and younger.
Back in October, 45% supported a second Biden term.
That is now down to 23%.
That's big.
Now, the questions that are prompted from this is, is this fair?
Is this a realistic assessment of how the Biden administration has performed?
And then the next question, should the Biden administration be concerned about it?
Because those actually aren't the same question.
And then the third thing is, if they do need to be concerned about it, what should they do?
Okay, first, is it fair?
I actually don't think so.
You know, when you are judging something like this and people are making this determination,
they're basing it on their own expectations.
Did this candidate with their time in office, did they exceed, meet, or fall below my expectations?
That's what determines how people vote in these polls.
That's what determines their response.
So it's only fair if they had reasonable expectations to begin with, and most people didn't.
If you go back to the videos from during the campaign,
I think it's safe to say that I did not have high expectations of a Biden administration
to the point where I continually reminded people, he's not a savior.
He's not going to fix everything.
He is very status quo, bringing things back out of Trumpism and back to normal.
This is not a guy where you're going to get a bunch of change.
Like, it's all there constantly because that's who Biden is.
This is somebody that I repeatedly was like, yeah, he's not my guy.
So has he met or exceeded my expectations?
He's far exceeded them because I set them at a realistic level.
I think a lot of younger people wanted a lot of change that isn't going to be delivered within
two years,
particularly with a House and Senate that don't have super majorities.
And that expectation of having a bunch of massive change quickly, that's what I think is driving the
lack of enthusiasm.
If you go back and you look at it the other way, what are the administration moves you disagree
with?
I have two, but when you compare that to an average politician or an average presidency, that's not
a lot.
So you might want to go back and look at it through that lens and see if you are one of those people
who is looking at the Biden administration now and being like,
I don't want this guy again, go back and look and see what he did and whether or not you agree with
those moves,  not whether or not he was able to deliver utopia in two years.
So do I think it's fair? No.
Does that matter? No, it doesn't.
Not at all. Politics isn't fair.
It's all about perception. And this is the perception. It's measuring it.
So should this concern the Biden administration? Absolutely.
The Biden administration is going to have to pay attention to these numbers.
Biden cannot win a second term with low enthusiasm among younger voters.
The Democratic Party cannot go toe to toe with Republicans based on the older vote.
Can't happen. They won't win. They'll get swamped.
So, yes, these numbers should be of concern.
They have to get their message out.
And this is part of the problem.
Younger voters don't get their their news from network TV.
They don't get their news from newspapers.
They get their news via apps, TikTok, Twitter, YouTube, Facebook, not really Facebook.
Some do. But this is where those perceptions are created.
The Democratic Party doesn't have cheerleaders like the Republican Party on social media platforms.
You know, the conservative commentators, they're not exactly critical of their own team, so to
speak.
They will get out there and they will cheerlead an obvious mistake.
The Democratic Party doesn't have that.
When you look at left and liberal commentators, I mean, just in this video,
you will have them say stuff like, yeah, your expectations of this were way too high.
They're a little bit more critical.
So that enthusiasm drops off.
The Democratic Party as an organization needs to change that and change it quick.
They've got to do something to mitigate that gap.
But that reality doesn't negate the concern.
These are concerning numbers.
Not overall, because it is those numbers do swing.
But the pretty substantial drop in the younger demographic.
That's a concern.
So what do they need to do?
First, they need to do better at getting their wins out.
Getting their wins out.
Particularly those that impact younger people.
They need somebody that is pushing that information out.
Because when you're on YouTube and you're looking at the more liberal or left side of things,
and you're looking at the commentators, and this is true on all of the other apps where these people
get their information,  it doesn't pay to be a cheerleader.
The audience wants an analysis and nothing is ever perfect.
And it also on the liberal and left side, there's a reason there are so many negative commentators.
It draws in more of an audience and they get paid well.
It doesn't pay to be a Democratic cheerleader.
So this problem will persist because that's where a lot of the information comes from.
That's where that perception gets shaped, even though it's not fair.
At least I don't think it's fair.
What can they do?
They can mitigate that gap and they can do a better job of explaining their wins.
They can also focus on policy that directly impacts this age group.
The thing is, that's not exactly a fair critique either, because they've tried or have done a lot of
things that aim at this age group.
They haven't accomplished everything because that's not a realistic expectation.
But they have to do better with getting their message out when it comes to the wins.
And hopefully that would start with the State of the Union and then it would carry on from there.
So yeah, the numbers, not overall, but the numbers when particularly looking at the younger
demographic, that's a concern.  Do I think it's fair? Not really.
Can they fix it? Yeah.
They just have to engage in a lot more self-promotion.
They need to get somebody out there that resonates with their audiences and says,
hey, we did this, we did this, we did this, and really get that information out there.
They need people who are as, I don't want to say it, but who are as disingenuous in their support as
conservative commentators are.  They desperately need that.
There aren't a lot of people who are on that side of the political spectrum who are commentators on
their own who want to do that.  So they have to astroturf it.
But I think it would matter if they actually had people out there every day.
You know, yay, yay, yay.
You know, we're supposed to, this is the 15th time the Republicans have screamed,
we're going to go into recession and look at these numbers over and over and over again and just
hammer it home.
I mean, the reality, if you look at that one, if you look at that particular issue, the doom and
gloom hasn't happened.  Is that Biden's responsibility? Not really.
We've talked about it on the channel.
Presidents don't really control the economy.
Some of what he has done has helped.
And you could even be honest and just focus on that part rather than just cheerleading, you know, go
Biden.
He saved the economy.
You could focus on the little things he did that helped.
They have a messaging issue.
And if they don't fix it, yeah, the Democratic Party is probably going to look somewhere else.
There may be a different candidate.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}