---
title: Let's talk about how to identify rumors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9dzDv7NlbGs) |
| Published | 2023/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing rumors circulating online about imminent war with China involving a four-star general in the Air Force.
- Identifying key elements in rumors, such as timelines, secrecy, and credibility.
- Analyzing the unlikely nature of the rumor and its origins.
- Explaining how the rumor likely stemmed from a motivational memo by a specific Air Force officer.
- Providing insight into the context behind the memo and the officer's motivational tactics.
- Urging caution and critical thinking when encountering military-related rumors online.
- Emphasizing the importance of verifying information and understanding context to combat fear-mongering.
- Warning about the prevalence of misinformation and disinformation in the age of rapid technology.
- Encouraging vigilance and discernment in consuming and sharing information online.

### Quotes

- "Always look for context."
- "If they assign the rumor to a four star who they can't name, just go ahead and forget about it."
- "Don't fall for the fear mongering because there's going to be a lot of it."
- "There is a new technology that exists today that did not exist during the Cold War that allows for the rapid transmission of rumor, misinformation, and disinformation."
- "You got to be on guard for it."

### Oneliner

Be cautious of military rumors online, verify information, and beware of fear-mongering amid rapid spread of misinformation and disinformation.

### Audience

Online users

### On-the-ground actions from transcript

- Verify information before sharing online (implied)
- Always seek context when encountering sensational news (implied)
- Be vigilant against fear-mongering and misinformation (implied)

### Whats missing in summary

The full transcript provides detailed insights into identifying and debunking rumors, particularly those related to the military, urging viewers to exercise critical thinking and caution in consuming and sharing information online.

### Tags

#Rumors #Military #OnlineInformation #Disinformation #CriticalThinking


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about rumors
and how to identify them.
Key things that you can look for that
exist in a lot of them when they circulate online
and how you can often track it back to its source
and find out that there are some exaggerations occurring
within the rumor.
So over the last, I don't know, 36 hours,
I've had a number of people send me a variation of a rumor.
And that is that a four-star general in the Air Force
pulled their friend aside, who in most of them is a pilot.
and that general told them that war with China is imminent and that they have to
be ready in six months and that maybe the Chinese would come ashore in
California and that the balloon is not what they're telling you. You have a
couple of things in this rumor that exist in most rumors that originate
within the military and then hit social media. First is it's imminent but there's
a timeline far away. Eminent in six months. Those two things don't go
together, especially in military language.
Eminent is now, right now, this moment, not six months from now.
But Eminent is scary in six months, provides a period of time
for the rumor to prove to not be true,
but for people to forget about where it came from.
The other part of this that is very common
is a piece of information you're not allowed to know,
that the balloon wasn't really what they're telling you
type of thing.
And then the big one, the big flag that should always exist,
a four star told me, or a four star told my buddy.
Generally speaking, four stars do not
engage in a lot of rumors.
They don't do that.
Aside from that, this is used to give credibility to people outside of the military.
Because when you watch a movie, there's always a four-star in the room.
If it was a four-star in the Air Force, who was it?
Alvin, Richardson, Brown, Minahan, Kelly, Wilspuk, Boussier, or Hecker.
That's it.
There aren't a lot of them in real life.
They're not everywhere.
Aside from that, with something like this, three of those wouldn't even know.
So you can cut it almost in half.
There'd only be five.
I know US counterintelligence hasn't displayed
that it's totally on the ball lately,
but I'm pretty sure it could narrow it down from five people.
It's something that is injected into a lot of rumors,
particularly those that are fear-mongery,
to give it credibility.
But it should destroy the credibility,
Because it's very unlikely that that occurred.
Now, let's play the game of where did this come from?
If there was somebody, one of these eight,
that were going to say something like this, who would it be?
Minahan, no doubt.
It's absolutely where it came from.
It is classic Minahan, to be honest.
but it wasn't his buddy pilot getting pulled aside.
It was from a memo from before the balloon was even known about.
A few weeks ago, Minahan, who was in charge of,
I want to say mobility for the Air Force,
put out a memo, said that his gut told him that by 2025,
we would go to war with China, and that he wanted everybody
his command to go to the range and fire a clip at a target and aim for the head if you
are going to send me a message about that term, send it to him, not me.
He said it, not me.
He also wanted a report that detailed what each group did over the previous year to get
ready for this potential conflict and what they're going to do over the next year to
be ready for this potential conflict.
It's just classic him.
It's how he motivates his troops.
He is known for saying stuff like this.
This is a person who once said something to the effect of when you know that you can kill
your enemy, your food tastes better and things within your marriage are better.
It's just him.
So on the off chance that some four star actually did say this to a pilot in his command, it's
probably been an act.
But more than likely what has occurred is somebody saw that memo, wasn't aware of the
context and that it is a very common motivational thing when trying to
maintain readiness to talk about the big enemy and being ready for that. They
aren't aware of that context so then it got turned into a rumor that then hit
social media and many people who might want to profit from that rumor or
or establish their credibility as being in the know, spread it.
I don't think, I do not think that Minahan actually
pulled the pilot aside and said, we're going to war imminently.
Be ready in six months.
I don't think that that occurred.
I think somebody saw the memo or maybe even
saw some of the coverage of the memo,
Because there are articles talking about it.
Because for those outside of that community,
it was kind of shocking.
And then you can find articles from blogs
that talk about this kind of stuff joking about it.
Because it's common from him.
It doesn't necessarily mean anything.
It is how he chooses to motivate the troops in his command.
Do I think that's a good way to do it?
No, but I'm also not a four star.
That's kind of his personal decision.
So when you are looking at rumors online
and trying to determine whether or not they are true,
if they originated in the military sphere,
if they assign the rumor to a four star who they can't name,
just go ahead and forget about it.
Because there aren't a lot of four stars.
If there was a security prohibition
from talking about this information, just saying four
star, that's enough to completely out the person.
So when you see that, which if you go back and look,
they always originate with a four star pretty much,
you can safely assume that it's not true.
If it also includes secretive information that there's no way to confirm, such as the
balloon is not what they're telling you, that's also a pretty good sign, because let's say
that it's true for a second.
Let's say the balloon isn't what they're telling us.
You know who doesn't get to know?
Some pilot.
It's not a thing where they tell everybody.
Information like that would be incredibly compartmented.
And despite certain very high profile lapses, it doesn't spread like that.
And then the combination of it's going to happen sometime between right now, this second,
six months from now. When you see those things together, just assume that it
isn't true or it's being misrepresented, and oftentimes you can
find the source of where it came from. Like, I would bet almost anything
that somebody saw that memo from Minnan and didn't understand the context of it,
didn't understand his history of making comments like this, didn't understand
what he's trying to accomplish, didn't put it together the whole thing of, oh
we're going to war but tell me what you're going to do over the next year to
get ready for it. All that being said, we are in a near-pure contest with China.
Things could spark at any moment. That's a true statement. But that was true
throughout the entirety of the Cold War. Don't fall for the fear mongering
because there's going to be a lot of it. There is a new technology that
exists today that did not exist during the Cold War that allows for the rapid
transmission of rumor, misinformation, and disinformation. You got to be on guard
for it. Always look for context. If you want to protect yourself against this,
always look for context because there will be those people who try to monetize
this or build their own credibility that way. Anyway, it's just a thought. Y'all
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}