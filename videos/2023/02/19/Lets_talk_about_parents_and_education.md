---
title: Let's talk about parents and education....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QbzFL4TOHhk) |
| Published | 2023/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises the topic of parents' role in public education and the influence they should have.
- Questions the common belief that parents always know what's best for their children's education.
- Challenges the idea that creating a child qualifies someone to plan out their education.
- Criticizes the push for a basic education focused on reading, writing, and arithmetic as insufficient.
- Warns against cutting out anything beyond the bare minimum in education, which could create a permanent underclass.
- Points out the class divide perpetuated by advocating for basic education in public schools while elite children receive a more comprehensive education.
- Emphasizes the importance of a well-rounded education that includes critical thinking, understanding society, and passing entrance exams.
- Expresses concern about politicians rallying for minimal education standards in public schools while ensuring their own children receive a richer educational experience.
- Criticizes parents who support limiting their children's educational options by advocating for a basic curriculum.
- Argues that children are sponges who constantly absorb information, underscoring the significance of a comprehensive education.
- Predicts a culture war that may alienate children from their parents over educational differences.
- Urges parents to reconsider supporting slogans that undermine their children's future in the name of basic education.
- Condemns the notion that advocating for a minimal educational curriculum is in the best interest of children.
- Encourages parents to think critically about how their choices impact their children's future opportunities.

### Quotes

- "Parents know what's best for their children's education."
- "Children, they're sponges."
- "You're rallying to destroy your child's future."
- "If you believe it is a good idea for 12 years to be spent learning the bare minimum, you do not know what's best for your child's education."
- "It's just a thought, y'all have a good day."

### Oneliner

Beau questions the belief that parents always know best for their children's education, warning against limiting educational options to the bare minimum.

### Audience

Parents, Educators, Policymakers

### On-the-ground actions from transcript

- Advocate for a well-rounded education that includes critical thinking, understanding society, and diverse learning opportunities (implied).
- Support policies that prioritize comprehensive education over minimal standards to ensure all children have access to a quality learning experience (implied).

### Whats missing in summary

The full transcript covers the importance of providing children with a well-rounded education that goes beyond basic skills to prepare them for a successful future. Watching the full video can offer more insights into the implications of limiting educational opportunities for children.

### Tags

#Education #ParentalInfluence #WellRoundedEducation #PublicSchools #Children'sFuture


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about education, and parents,
and how much say parents should have in public education,
because I feel like this is going to be a big topic come 2024.
And there's a certain sentence that
is being used to rally people behind a certain idea.
And I get the sentence, I understand what's being said there.
I just don't know that there's a whole lot of truth in it.
Parents know what's best for their children's education.
I mean, do we, though?
Really?
I'm not sure about that.
I'm pretty sure that there's an entire genre of videos and clips
TV shows that demonstrate, no, in fact, many parents are not smarter than a fifth
grader. I don't know what about the act of creating a child qualifies
somebody to be an educator or, more importantly, to plan out an education
over the course of a dozen or more years. I don't know that there's any basis for
for that. And when you look at the people who are using this sentence to rally
people and you look at what they want, they may want to spend a dozen or so
years teaching reading, writing, and arithmetic, the three Rs. I don't know
that that's what's best for a child's education. It will certainly put them in a
position to where they can only be a good worker bee and there's nothing wrong
with being a worker bee but I think most parents would want their children to
have options. If a person graduating high school doesn't have what it takes to pass
an entrance exam, doesn't understand society enough, doesn't know how to
critically think, doesn't have a good base of the world, good understanding, what are
they going to do?
This idea of cutting out anything that isn't just the bare minimum, that's not going to
benefit children.
It's going to hinder them.
It's going to hinder them and it's going to create a permanent underclass.
It's going to sharpen a class divide between us commoners and our betters.
See this type of education that all of these politicians are rallying people to demand
in public schools, that is not the education their kids are getting.
Your kids are going to a private school where, yeah, they're learning about all kinds of
critical theories.
They're learning how to entertain an idea without accepting it.
They're learning how to critically think.
They're learning about philosophy.
They have an arts program.
your kids because you know what's best for them. All they need to do is know
how to read and write and do some basic math. That's just ensuring that the
children of today's upper class stay upper class and those kids that went to
public school, there'll be no competition for them.
The drive for education is pretty intense.
Children, they're sponges.
They absorb information constantly.
It's going to be a wild sight to see a culture war divide that splits families, splits children
away from their parents, teens getting angry with their parents because their parents don't
want them to read, because their parents want them dumb, want them uneducated.
When you have a politician that can rally you to undermine your child's options, your
child's future, just because they used some slogan and waved a flag, you need to rethink.
Because if you believe it's a good idea, if you believe it is a good idea in this day and age for 12 years to be spent
learning the bare minimum,  you in fact do not know what's best for your child's education.
No matter how many times you say it, you don't because you're limiting their options.
you're rallying to destroy your child's future. That doesn't make a whole lot of
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}