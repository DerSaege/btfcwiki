---
title: Let's talk about Ohio, trains, coverage, and a lack of answers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=D1BByT564pk) |
| Published | 2023/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a train derailment in a small Ohio town that led to a fire due to chemicals on the train.
- Mentions the lack of in-depth media coverage on the environmental impacts of the incident.
- Points out uncertainty about the future environmental impacts on soil, water, and air.
- States that the scale and scope of the impacts are still unknown.
- Notes the potential for long-term issues like cancer clusters and contaminated well water.
- Talks about different jurisdictions handling the situation differently, with West Virginia being proactive in water supply testing.
- Suggests that more testing needs to be done to understand the situation better.
- Raises questions about why the incident occurred and what safety measures can be taken to prevent future accidents.
- Emphasizes the need for clear reporting, understanding the environmental impact, and identifying the root cause of the derailment.
- Calls for accountability from companies to invest in safety measures.

### Quotes

- "We nuked a town with chemicals over this train."
- "We need a clear picture. We need real models about the environmental impact of this."
- "The problems need to be fixed. It needs to be mitigated so this doesn't happen to some other small town."

### Oneliner

Beau delves into a train derailment in Ohio, addressing the lack of media coverage on environmental impacts, uncertainties about future consequences, and the necessity for accountability and prevention measures.

### Audience

Environmental activists, concerned citizens

### On-the-ground actions from transcript

- Contact local authorities to inquire about safety procedures and prevention measures (implied)
- Support proactive water testing initiatives like those in West Virginia (exemplified)
- Advocate for thorough testing and reporting on environmental impacts (implied)

### Whats missing in summary

The emotional impact on the affected community and potential long-term consequences beyond environmental damage.

### Tags

#Ohio #TrainDerailment #EnvironmentalImpact #Accountability #Prevention


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Ohio and trains
and impacts and coverage and why the coverage hasn't been
as in-depth as people want it to be.
People rightfully think it should be.
And just kind of run through everything
and take a look at it.
So if you don't know what I'm talking about,
there was a train derailment in a small town in Ohio.
Because of circumstances as far as putting it out,
there was a fire.
And it seems like they kind of just
let it burn for a little bit.
There were chemicals on the train.
The, initially everybody was focused on one specific chemical.
It turns out that there are, I want to say at least three more that are also really
bad to just have dispersed everywhere, which is what kind of occurred.
One of the quotes I said, or one of the quotes I saw said that we
nuked a town with chemicals over this train.
something to that effect. Now the question that most people have is about
the environmental impacts, the long-term environmental impacts, and why it's not
getting the coverage that it should. A whole lot of questions about this and I
I started looking into it and I made a couple of calls again reminder I do not
feel well so this isn't super in-depth but what I have been able to kind of
determine is that they don't know how bad it's going to be yet. That is
probably the root of the media not covering the future environmental
impacts. You have a general consensus that there are going to be future
environmental impacts in soil, water, and air. The scale and scope of which, it's
all over the place. You look at the worst-case scenarios, it's huge, like a
regional event. You look at the best case scenarios and you're like well I mean
that can be that can be remedied and it doesn't look so bad. Now I have seen
people indicate that you know 5, 10, 15, 20 years from now they're gonna be
talking about cancer clusters, they're gonna be talking about the well
water and there's going to be a lot of issues but it all boils down to the
scale and the scope and this is the part that there hasn't been a consensus
reached so I think that media outlets may be very leery about putting out a
bunch of information that turns out to be wrong. Now as far as all of the
different jurisdictions involved. Some of them, you don't know what they're doing
yet. Some of them are being very proactive. West Virginia, as an example, if
I have this right, and I understand this correctly, they're already set up. They
have it set up so they can switch their water supply as soon as something shows
up in the river and they're testing for it. And if something shows up, they'll
just switch over to something else.
And they're being proactive in trying
to make sure that the water doesn't become an issue
for the people in their state.
Other areas, there's not information that I could find.
Now, with all of this in mind and looking
at all of the various models, and I don't even
want to call them models, because they're not models yet.
They're informed guesses.
But they're very wide ranging.
What it is going to take to get something a little bit more
concrete, which is when you'll probably start seeing
real reporting on it, is for a lot of testing
to be done over the next few days.
So over the next few days, you should
start to see a clearer picture emerge in the media.
Now, one of the things that I don't see being covered,
which I think it should be as well, is why did it happen?
And more importantly, what can we
do to stop it from happening again?
Were safety procedures followed?
Were the breaks up to date?
Were the workers right with the unions
when they said there were safety issues because
of the scheduling and because of being shorthanded?
Did that have a role in this?
These questions have to be answered.
They've got to determine exactly what caused this
so it doesn't happen again.
And that doesn't seem to be getting enough attention
either.
There's a lot going on.
There's a whole bunch of news.
And for whatever reason, this one does seem to be,
I don't want to say it's being ignored because it's not.
But the questions people want answered,
the answers aren't there yet.
And the coverage doesn't focus on asking for the answers.
and it probably should, we need a clear picture.
We need real models about the environmental impact of this
because the worst cases are really bad
and we need to figure out where that's at.
The feds might need to step in and make sure
that that occurs and then we definitely need to know
why it happened and how to mitigate it.
Are companies that are posting record profits
not investing in safety, not doing checks they should?
This is stuff we need to know,
and the problems need to be fixed.
It needs to be mitigated so this doesn't happen
to some other small town.
Anyway, it's just a thought.
Y'all have a good day.
you

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}