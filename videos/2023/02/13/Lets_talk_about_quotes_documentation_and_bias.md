---
title: Let's talk about quotes, documentation, and bias....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3IpuqAezvGQ) |
| Published | 2023/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the topics of quotes, documentation, evidence, and bias.
- Analyzes an article claiming the U.S. was behind a pipeline hit.
- Questions the credibility of quotes from President Biden and other officials.
- Challenges the believability of the President on such covert actions.
- Raises the issue of confirmation bias in interpreting statements.
- Emphasizes the need for evidence to support credibility judgments.
- Suggests that official statements lack credibility due to potential deception.
- Talks about the importance of documentation to support claims, especially with anonymous sources.
- Explains the types of individuals intelligence agencies typically use for covert operations.
- Dissects the claim made by Hirsch about the individuals involved in the alleged operation.
- Speculates on the possible explanations for the absence of instructors in a training facility.
- Mentions the necessity of documentation to explain the absence of personnel.

### Quotes

- "If you don't have the evidence, you're just...it's probably confirmation bias at play."
- "His statements and administration official statements they're awash. They don't count for anything one way or the other to my way of thinking."
- "But there's no documentation in the article."
- "It's just one little piece of the story."
- "y'all have a good day."

### Oneliner

Beau delves into quotes, documentation, and evidence to analyze a claim about US involvement in a pipeline hit, challenging biases and credibility along the way.

### Audience

Critical Thinkers, Researchers

### On-the-ground actions from transcript

- Verify sources and documentation before forming opinions (suggested)
- Question biases and preconceived notions when evaluating information (implied)

### Whats missing in summary

Deep dive into evaluating credibility, bias, and evidence in analyzing information.

### Tags

#Quotes #Documentation #Bias #Credibility #Evidence


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about quotes,
and documentation, and evidence, and bias,
and all of that stuff.
After that recent video about the article,
there were a bunch of questions that came in,
but they really kind of fell into two categories.
And one directly leads into the other.
And this is one of those things that can help us kind of sift
through not just bad reporting, but our own biases,
our own feelings about things.
OK.
So if you have no idea what I'm talking about,
there was an article put out by Hirsch
that said the U.S. was behind the pipeline hit.
And I went through the article and I'm like,
there's actually no real evidence to support the claim.
And that led people to say, well, what about the quotes
from Biden and other U.S. officials?
They said before it happened
that they were gonna put a stop to the pipeline.
Yeah, I mean, that happened.
I would like to point out they didn't say
they were gonna take it out
in quite that dramatic of a fashion,
but they did say it.
So it shows that they at least had the desire, right?
But can you really believe the quotes?
Do you believe that President Biden is a credible source
on this topic?
It's a yes or no question,
and it doesn't matter what you answer,
you're in trouble.
If you say no, you can't believe the quote. If you say yes, you have to find a way to explain the contradictory quote
that says they didn't do it.
He's either credible or he's not. And when you're picking between the two, if you believe he's credible, he's just lying
in one case,  then you have to have evidence to make that determination. If you don't have the
evidence, you're just...it's probably confirmation bias at play. This is what
you want to believe because it fits. This totally sounds like something the US
would do, but if you're just going off of what can be demonstrated, what you can
backup with evidence, what you can prove, you can't do it that way. So you would
have to find some way to back up one side of his statement or the other if
you choose to give him any credibility at all. Now I would suggest that when you
were talking about the President of the United States commenting on possible US
covert actions that he doesn't have any credibility whatsoever because it would
be his job to kind of lie. So his statements and administration official
statements they're awash. They don't count for anything one way or the other
to my way of thinking. If you want to put some weight behind it you have to have a
reason to go one way or the other.
And that leads us to the documentation.
I said you needed documentation to back up the anonymous source
if you're going to use an anonymous source.
And a whole lot of people were like,
what documentation would possibly exist way more
than you think?
OK, under normal circumstances, an intelligence agency
wants to do something super shady like this.
They have three pools of people that they would typically
use for something like this.
One is their own people.
The second would be those people who
have already been laundered.
Maybe they were military and they got pulled out.
Or maybe they retired and now they contract
or something like that.
And then the third pool is that super shady pool of people who were never in the military
but had training and stuff like that and they would get pulled into stuff like this every
once in a while.
That is not a very large pool.
That's how this would normally happen, but that's not what Hirsch said happened.
He said they used normal Navy guys.
Does that make sense?
In this case, kind of.
This is a little bit of a specialized skill.
So maybe the pools that they had didn't contain people that had the skill they needed.
So where would they get them?
Kind of exactly where he said they would get them in the article.
They would come from the training facility there in Panama City.
Which is what he said, okay?
But who are they going to take?
Are they going to take the students?
would take the instructors, right? People that know what they're doing. How do you
explain their absence? Did they go on leave? Did they go for quote training
somewhere? That training would be incredibly dangerous to explain any
injuries that they might have and that's the documentation that would be there
because they have to explain the absence of the instructors at the base, otherwise
rumors would already start. If you pull a bunch of dive instructors and then a
pipeline goes up, people are going to kind of put two and two together. So you
have to have a place for them to be, even if they were never there. The
documentation wouldn't say the CIA is sheep dipping these people to go take
out a pipeline. It would say they were going somewhere for training. And that's
It's just one little piece of the story.
There would be documentation as far as scheduling and all kinds of other stuff as well.
If it happened, the documentation's out there.
You just have to find it.
But there's no documentation in the article.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}