---
title: Let's talk about 4 more balloons or other objects over the weekend....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4x2cU-fNxpE) |
| Published | 2023/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Over the weekend, four new objects were discovered in either US or Canadian airspace, prompting immediate responses.
- One theory suggests aliens, but Beau finds it unlikely due to the timing and location.
- Chinese surveillance balloons were suggested as a more plausible explanation, with different perspectives on their origin.
- A recent Chinese spy balloon flying over the US raised concerns and sparked intelligence gathering efforts.
- The US tracked the balloon coast to coast, learning how to better identify and classify such objects.
- Beau speculates that improved tracking equipment led to the increased detection of such objects.
- While not overly concerning to Beau, he cautions against drawing excessive attention to China.
- Beau reminds viewers that surveillance flights are common and that countries regularly conduct them.
- He urges viewers not to be overly alarmed by propaganda and to view the Chinese government as composed of people.
- Beau concludes by suggesting that the heightened detection of objects is likely due to improved analysis and tracking capabilities.

### Quotes

- "Just a thought. Have a good day."
- "Don't let it scare you."
- "The Chinese government is made up of people, not monsters."
- "Surveillance flights are really common."
- "It's just a thought."

### Oneliner

Beau provides insights on recent aerial discoveries, dismissing alien theories and cautioning against undue alarm towards China's surveillance activities, reminding viewers of the commonality of such flights.

### Audience

Skywatchers and geopolitical observers

### On-the-ground actions from transcript

- Stay informed about aerial activities in your area (suggested)
- Advocate for transparency in surveillance practices (implied)
- Engage in respectful discourse on international relations (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on recent aerial sightings, offering a balanced view of the situation and encouraging critical thinking about surveillance activities.

### Tags

#AerialDiscoveries #Surveillance #China #Geopolitics #Analysis


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about all the stuff that
was in the sky over the weekend.
Had a whole lot of questions come in about it
over the last couple of days.
If you don't know, I don't feel well,
or I would have answered these sooner.
So if you missed it a whole bunch at time of filming,
I want to say four new objects were discovered within either US or Canadian airspace that
have had pretty immediate responses from the countries in question.
And they have taken a number of items down.
That is what the reporting suggests.
People are asking what it is.
Obviously because, you know, everyone's a Captain Kirk, one of the theories that has
been suggested is aliens.
I find that a little unlikely that they would show up in the exact way and area and right
after a Chinese surveillance balloon.
That doesn't seem really likely to me, but it was in the messages that will address it.
is that when the Chinese put up their balloons, they put up a whole bunch of
them at once to kind of test them all out together and they don't have a lot
of control over them. I like it. Maybe. Attracts. Some people have suggested that
they are not Chinese balloons, that they are civilian balloons or you know from
companies, universities, and their legitimate research balloons.
Yeah, I mean that could be it too.
That's definitely plausible.
I have another piece of information that might help shed some light on this.
Recently a Chinese spy balloon flew over the United States.
might have heard about it. I do believe there were some people who suggested
that those types of surveillance overflights were really common and
happened all the time and it was nothing to worry about because they happened a
lot. I believe they said pretty much every day whether it be satellite, plane, or
something exotic. So this one that they tracked made it coast to coast and
And everybody had an opinion about that.
While it was up there, they kept contact with it.
They watched it, kept visual contact with it, a lot.
They had orders to identify, clarify, and classify it.
And my guess is that they've kind of dialed in their sensors to 11.
So now they're picking up all of them.
During the time it was flying over the country, they were gathering intelligence on it, and
they were learning how to track it and track it better, and this is going to be a concern
for Chinese counterintelligence.
So once they figured out how to track it, they get their equipment kind of dialed in
to get it right, and they start looking.
Back at base, sparks in the software, flash of the message, something's out there.
And then the war machine springs to life, and they got to knock it down.
Now that being said, I think that that, them dialing in their equipment and really now
having a good read on how to track these things, I think that has a whole lot to do with it.
That being said, options two and three, they're still totally in play.
The Chinese could have released a whole bunch at once as a test of their system and of the
equipment and they really just can't recall them.
So that could also be a play at the same time as them using the intelligence they gathered
by allowing it to fly over to make their equipment and their tracking and their warning systems
better.
And it could also be that it is now really good at picking this stuff up and old, legitimate
research equipment is now being tracked to where it wouldn't have been before.
These are all possibilities, none of this stuff is incredibly scary to me, I'm not incredibly
concerned about it.
I would be real careful with how much attention is being drawn to China, yeah, they're a
They're a competitor nation and all of that stuff from the US point of view, that is how
it is viewed.
Yes, we are now entering, we're in that contest.
Just remember that the Chinese government is made up of people, not monsters.
They are going to behave like any other country.
undoubtedly there's going to be a lot of propaganda coming. Don't let it scare you.
Don't do that. My guess is the reason there are so many now, it's not that
there are so many now, it's that the US is better picking them up because they
took the time to analyze all of the data from the balloon that they tracked across
the country. Makes it easier for them to pick it up and they're engaging them.
that's my guess. Could some of them be legitimate, you know, research? Yep. Could
it happen this way because a whole bunch of them got released at once? Yep. But I
think it has to do with the information gained by tracking it across the country.
Made them better at identifying them and spotting them. Remember, surveillance
flights are really common. They happen all the time. There's one happening right
now, I assure you, as you watch this. More importantly, and to keep focus on this, I
assure you the United States or whatever country you're in, it's doing it to
somebody else right now. They're really common. Now, yeah, these are a little bit
more intrusive, but the concept's the same. Anyway, it's just a thought. I have
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}