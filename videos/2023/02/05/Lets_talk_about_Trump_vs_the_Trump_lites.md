---
title: Let's talk about Trump vs the Trump lites....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eEdgUbpcMwk) |
| Published | 2023/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Identifies "Trump-lites" as Republican governors mimicking Trump's leadership style in red states, trying to out-Trump Trump with inflammatory rhetoric and power grabs.
- Notes the potential for a Trump-lite to become the Republican nominee due to the habit of voting for the person with an "R" after their name in red states.
- Expresses faith in the American electorate to see through the Trump-like leadership style.
- Observes that while this approach may work in red states' primaries, it may not translate well on the national stage.
- Mentions the open engagement between Trump and DeSantis, with Trump criticizing DeSantis and DeSantis responding by referencing his reelection.
- Describes Trump labeling DeSantis as a "rhino-globalist," noting its anti-Semitic connotations and potential impact on DeSantis.
- Speculates on how DeSantis will respond to Trump's attacks, with possibilities of moving further towards the far-right MAGA fringe or disavowing the rhetoric.
- Comments on the fractured state of the Republican Party, from normal conservatives to the MAGA crowd and the fringe within it.
- Anticipates the response from DeSantis and how it will play out in the context of Trump's legal troubles and potential primary challenges.
- Encourages staying alert for the escalating dynamics between Trump and the Trump-lites, especially as Trump's behavior becomes more erratic.

### Quotes

- "Trump-lites, the copy versions of Trump."
- "You have those that are leaning into far-right authoritarianism."
- "The response from the governor of Florida, it should be interesting to watch."
- "It's probably going to get really wild."
- "Y'all have a good day."

### Oneliner

Beau dives into the rise of "Trump-lites" mimicking Trump's style in red states, the clash between Trump and DeSantis, and the potential ramifications for the Republican Party's future.

### Audience

Political observers

### On-the-ground actions from transcript

- Watch and analyze the interactions between Trump and DeSantis to understand political dynamics (suggested).
- Stay informed about how political rhetoric and attacks can influence public perception and discourse (implied).

### Whats missing in summary

Insights into the specific strategies that DeSantis or other "Trump-lites" might employ in response to Trump's criticisms.

### Tags

#Trump #RepublicanParty #DeSantis #PoliticalDynamics #Authoritarianism


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the Trump-lites,
the copy versions of Trump,
and we will talk about how Trump and DeSantis
are finally going directly at each other,
because that has started.
Okay. In a recent video, I used the term Trump-lites,
got a bunch of questions asking what I meant.
I'm talking about Republican governors
in relatively red states, very safe states,
who are mimicking Trump's leadership style
and trying to out-Trump Trump,
being very inflammatory, using a lot of wild rhetoric,
trying to paint themselves as, I mean, almost intentionally,
paint themselves as an authoritarian,
making lots of power grabs that don't go along
with the traditional histories of the states,
stuff like that.
There is a lot of expectation that one of them
may become the Republican nominee,
and that may be true.
I'm not sure why,
but I tend to have a little bit of faith
in the American electorate.
I hope they see through it.
It's also worth noting that
that particular leadership style can be used in red states
because of the habit of just voting for the person
with the R after their name.
And when you're playing to just Republicans in the primaries,
you can try to out-Trump Trump,
and you look okay, you do all right.
Taking that to the national stage, I don't know.
I mean, it didn't work for Trump.
I don't know why they think it'll work for them.
That is what I mean by Trump-lite,
and there are quite a few governors doing this.
In totally unrelated news,
DeSantis and Trump are finally openly engaging each other.
Recently, Trump criticized DeSantis' handling
of the public health issue,
to which DeSantis was basically like,
yeah, well, I got reelected.
You didn't.
And then Trump has decided to break out the big guns.
He called DeSantis a rhino-globalist.
If you're not familiar with that term,
it's an anti-Semitic dog whistle.
It has roots in a lot of fringe theories,
conspiracy theories, a lot of baseless stuff.
But that term resonates with the MAGA base.
There's probably a reason why.
DeSantis is going to have to respond to that in a big way,
and I'm not sure how he's going to do it
without painting himself into the even further right,
fringe part of MAGA.
You know, as the Republican Party becomes more fractured,
you have the few normal conservatives left.
There aren't many.
You have those that are leaning into far-right authoritarianism.
You have the MAGA crowd that has already embraced it.
And then you have like the fringe of the MAGA crowd.
Any response from DeSantis,
it may actually push him into that fringe.
If he tries to hit back and continue to out-Trump Trump,
the alternative for him would be to disavow it
and call it out for what it is and basically say,
you know, this is this kind of dog whistle.
You shouldn't be doing that.
I don't know that DeSantis would do that.
That is probably what the majority of Americans would expect.
I don't know that DeSantis is going to try to play to that crowd.
The response from the governor of Florida,
it should be interesting to watch because we're going to get to see
how the Trump-lites plan on taking on Trump.
And with Trump becoming more erratic
because of all of the legal troubles,
the insults and the rhetoric that they're going to have to respond to
during any potential primary, it's probably going to get really wild.
But that is now underway.
So keep your eyes open for that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}