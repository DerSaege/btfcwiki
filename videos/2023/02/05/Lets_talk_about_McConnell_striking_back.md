---
title: Let's talk about McConnell striking back....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7Q_Kc8_In7A) |
| Published | 2023/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- McConnell faced a challenge to his leadership by Rick Scott after the midterms.
- Rick Scott and Mike Lee attempted to get rid of McConnell as the leader of the Republican Party in the Senate.
- The attempt to oust McConnell failed by a large margin.
- McConnell didn't take any action after surviving the challenge.
- McConnell removed Rick Scott and Mike Lee from the Commerce Committee, a powerful committee.
- Mitch McConnell is consistently underestimated by others in politics.
- McCarthy criticized McConnell, which may not have been a wise move given McConnell's history of retaliation.
- McConnell is strategic and may speak up if things go wrong for McCarthy regarding the debt ceiling issue.
- McConnell may use such situations to remind others that maybe he shouldn't be there.
- Republicans planning to go to Congress should be wary of figures like McConnell who may appear supportive but actually be setting up for failure.

### Quotes

- "This. This is classic McConnell."
- "McCarthy's doing a fine job. He's doing great. No, no, no, no."
- "We're not going to help. We're just going to stand here and watch him do a great job."

### Oneliner

Beau explains McConnell's strategic maneuvers and warns Republicans to be cautious of his actions in politics.

### Audience

Political observers

### On-the-ground actions from transcript

- Watch out for political figures like McConnell in Congress (implied)
- Be cautious of individuals who may not have your best interests despite appearing supportive (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Mitch McConnell's political tactics and serves as a cautionary tale for those entering politics.

### Tags

#MitchMcConnell #RepublicanParty #PoliticalStrategy #Leadership #Caution


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about McConnell being McConnell,
and a few people actually being surprised that Mitch McConnell was Mitch McConnell.
So after the midterms, there was a challenge to McConnell's leadership by Rick Scott.
And Mike Lee kind of gave his nominating speech.
The idea was to get rid of McConnell, make sure he wasn't the leader of the Republican Party there in the Senate.
The attempt to oust McConnell failed, reportedly by a pretty large margin.
And McConnell, well, didn't really do anything.
It is what it is, you know, that's just politics.
So in a totally surprising move, Rick Scott and Mike Lee are no longer on the Commerce Committee,
which is an incredibly powerful committee.
They were removed by none other than Mitch McConnell, who could have seen that coming.
McConnell is somebody that everybody keeps underestimating up there.
Recently, McCarthy kind of took some shots at McConnell.
And I suggest it probably wasn't a good idea to do that because McConnell has a history of, well, getting even.
If you are in a situation where being House Speaker took, you know, more than a dozen votes to get to that point,
you probably don't want to make an enemy of somebody like Mitch McConnell.
People asked what I was talking about.
This. This is classic McConnell.
So when you are analyzing McConnell's moves, remember stuff like this.
You always have to factor it in.
As you watch McConnell take a backseat to McCarthy when it comes to the debt ceiling thing,
remember that this is probably in play.
They're not going to get out there in front of the cameras and support McCarthy.
And if it goes the wrong way, well, I imagine McConnell will have a whole lot to say at that point.
He will remind everybody that, you know, maybe he just shouldn't be there.
And you know, those deals would make it really easy to get rid of them.
For all Republicans who plan on going to Congress, you need to watch out for the people like McConnell,
because they will walk out there and say, McCarthy's doing a fine job.
He's doing great.
No, no, no, no.
We're not going to we're definitely not going to help.
We're just going to stand here and watch him do a great job.
There's no way that he's actually setting himself up for failure.
And they will smile and nod.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}