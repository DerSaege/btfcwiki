---
title: Let's talk about feedback, critiques, and Cattle ranchers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OIqvaIg0o24) |
| Published | 2023/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Collaborations should be carefully chosen to avoid negative perceptions.
- Metallurgical coal is different from thermal coal and has environmental benefits.
- Supporting workers is vital regardless of the industry they are in.
- Stereotyping ranchers as all MAGA supporters is inaccurate.
- Republicans and Democrats have different approaches to addressing economic issues with ranchers.
- Building solidarity and understanding with ranchers is key for systemic change.
- Convincing people in rural areas about left economic principles is challenging but necessary for change.
- Rural people already practice leftist economic principles, albeit in a capitalist system.
- Framing economic ideas in a way that resonates with rural communities is essential for progress.
- Systemic change requires reaching out to all sectors of society, even those harder to reach.

### Quotes

- "It's you have to support the workers."
- "Don't buy into the stereotypes."
- "If you want systemic change, it has to be system wide."

### Oneliner

Beau dives into the importance of carefully chosen collaborations, debunking stereotypes, and reaching out to rural communities for systemic change.

### Audience

Advocates for systemic change

### On-the-ground actions from transcript

- Reach out to rural communities and build understanding and solidarity (suggested)
- Support workers regardless of the industry they are in (implied)
- Avoid stereotyping individuals based on political beliefs (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the importance of collaboration, worker support, and breaking stereotypes to achieve systemic change. Viewing the full transcript will offer a comprehensive understanding of these key points.

### Tags

#Collaborations #SystemicChange #RuralCommunities #WorkerSupport #Stereotypes #EconomicPrinciples


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about feedback and critiques
and cattle and progress and how to achieve it.
If you missed it recently, I, me as an individual,
I lent my voice to another channel, More Perfect Union,
to help them with a video that they were doing
about consolidation inside the cattle industry.
It led to a lot of feedback and critiques
from a bunch of different people,
and they had a couple of different points.
I have one message that actually wound up
touching on everything,
all the points that the different people made.
So we're gonna go through that.
If you are somebody who wants deep systemic change,
if you are somebody who's looking for real progress,
maybe this is a video you watch.
Okay.
You need to hire someone to pick your collaborations better.
Coal mines and cattle ranchers, not a good look.
It's like you hate the environment.
Yeah, I get it.
Nobody likes coal.
I got that. It's dirty.
It's not what people want.
You want wind, you want solar, you want hydroelectric,
you want clean and green,
all of which require metallurgical coal.
The workers that we were helping at the mine,
the name of the mine is Warrior Matt Metallurgical.
It's not thermal coal.
It's not for energy like that.
That's for the environmental side of this statement.
At the same time, I want to point out you can't do that.
You can't separate people off by their industry like that.
It's you have to support the workers.
You make your ideology very well known via an image on your profile.
Is the quote associated with that,
is it workers of the world or is it workers of industries I condone?
You can't do that.
That is definitely how the bosses will break down solidarity.
And then cattle ranching, as far as the environment.
Yeah, there are major issues when it comes to the environment and cattle ranching.
I would point out that a whole bunch of people sent me the same image,
a whole bunch, like three, sent me the same image,
like meme, has a whole bunch of little facts on it.
That was specific to Brazil, to South America.
Not saying it doesn't matter, just saying it doesn't relate to Texas.
There's not a lot of deforestation of that type happening there.
But with that aside, there are still issues,
particularly with high density feedlots,
which are kind of a byproduct of consolidation within the cattle industry.
So if the point of that video was taken and acted upon,
it would actually be a step in the right direction.
It really would.
It would help.
And by help, I want to be clear because I know somebody's going to say this later.
It would not totally solve the issue.
Could you just not help the far right?
All of these miners and ranchers are big time MAGA.
That's not true.
That's a stereotype.
That isn't true.
There will be, guaranteed, there's going to be cattle ranchers down in the comments on YouTube
because they're there pretty much every day.
They're not all MAGA people.
I know you know one person who is probably standing on a ranch as you watch this.
It's a stereotype.
Don't buy into it.
And to the real ranchers, I understand we're different.
These are Republicans who got exactly what they voted for.
Let them live it.
The cattle industry needs to be destroyed anyway.
Go vegan!
They're not all Republicans.
Most, I will say most, most are.
Absolutely.
But who talks to them?
You don't have a lot of lefties or liberals reaching out to this community.
And I would imagine that there are going to be ranchers who find out for the first time
in that video how much Reagan had to do with the situation that they're in right now.
Because the only people talking to them are Republican politicians.
And they talk to them about social issues.
They don't talk to them about economic stuff.
What happens?
We've all seen it.
A Republican politician shows up in his pavement princess that has never pulled anything other
than maybe his boat.
He's obviously far more at home in a boardroom than a tack room.
And if he was watching this, he'd have to Google what a tack room is.
But he steps out of that big truck.
He's got cowboy boots on.
He's rocking a car heart.
And he talks to them about issues they care about.
Social issues.
He doesn't talk to them about the economic stuff.
But he does play into it a little bit.
He talks about how they're going to go against big this and big that.
And the ranchers assume that that means he'd help them.
But he never does.
The only thing that they get from the left when they do get spoken to on those rare occasions,
the cattle industry needs to be destroyed anyway.
Go vegan.
I would imagine most of them vote Republican.
That tracks.
And we'll come back to that part about go vegan.
But that's if the only thing you hear from one group is that your entire way of life
needs to be destroyed, you're not going to see them as being on your side.
I've watched you long enough to know that if I'm wrong, you'll make a video about it.
But I don't see how.
How is helping people suffering because of their own votes helpful?
That's what I really want to know.
You wouldn't say that about anything else.
You're angry.
You are probably very passionate about animals.
And by probably, I mean, I looked at your profile.
I know you are.
So you're viewing it through a lens.
You wouldn't say, well, they can pick themselves up by their bootstraps in any other situation.
You wouldn't look at somebody who maybe has a substance issue and say, well, they got
themselves into that.
That's not you.
Don't let it be you here either.
How is it helpful?
That's what I really want to know.
Why should I give a care?
What happens to these people?
Because you need them.
You don't know it, but you need them.
When it comes to being a vegan, you have good argumentation.
I love looking at the argumentation that different people use.
And I am very vested in finding a good counter argument to ending the concept of eating meat.
Why?
Because I eat meat, right?
I don't have one.
I do not have good argumentation when it comes to the core issues, the core things that people
are bringing up, particularly about animal welfare and all this.
I don't have an argument against that.
You have near perfect argumentation.
Why isn't it taking off?
Why isn't it a thing already, like a big thing?
It's been going on a while.
Where's that forward momentum?
You're at the tip of the spear here.
You have a liberation mindset, one that is so all-encompassing it includes animals.
That's pretty far ahead of most people.
To get there, you need a long shaft to that spear to get that forward momentum to keep
moving forward, which means you have to bring people who may not have a liberation mindset.
And you've got to get them at least on the staff.
You've got to get them to at least be part of the spear.
And you're talking to people, they're not on a liberation mindset.
They're in survival mode.
You need them.
When you're talking about deep systemic change, it can't occur in pockets.
You need it everywhere.
You need it system-wide, which means you need people that a lot of people aren't reaching
out to.
So that's why you should care.
You do good work and I like your channel, but the things you seem to go above and beyond
on are just not the things I would expect from somebody who's on the left.
Yeah, yeah.
That's because I believe what I just said.
It's by design.
I am far more likely to help with a cause if there aren't going to be a lot of people
of a progressive nature there, because that's where I could do the most good.
So when you are talking about rural people, just remember, don't buy into the stereotypes
and understand that when it comes to social issues, they're in a different place.
Most are in a different place.
Like, the overwhelming majority are in a different place.
That's not a stereotype.
That's pretty accurate.
But when it comes to economic issues, that's a little different.
That's a little different.
And there's that saying that is something to the effect of, you know, it's really hard
to convince a person of something if their paycheck requires that they don't understand.
The flip side of that is true as well.
It's really easy to convince a person of something if their paycheck requires they understand.
And if you were to go to a rural area, particularly speaking to those people who are actively
involved in agriculture, and you started talking about your ideal utopian visions, and you're
like, well, you know, we would want to set up this type of organization.
Most of them are going to look at you like, yeah, we already do that.
Rural people live their lives by left economic principles every day.
You want to set up a worker co-op.
You want to set up a co-op for workers of the same industry.
Theirs is a hundred years old.
They've been doing that.
You want to set up a committee where enterprises from the same area can get together and talk
to each other and work out their problems.
And there's like voting and everybody's helping out.
Yeah, it's the association.
They live by leftist economic principles, those concepts.
They do it in a capitalist fashion because it's a capitalist system.
But what they know to be good and true and works, it's what you advocate for.
They've been doing it for decades.
It just doesn't get framed to them in that way.
Nobody talks to them anymore because they view them as MAGA people.
They're not.
I mean, some are.
But it isn't the stereotype that you might believe.
And these are people you have to have.
If you want systemic change, it has to be system wide.
You have to reach out to people that are going to be harder to reach.
But they'll never be reached if the only people talking to them are some politicians who went
to Harvard and Yale and showed up in a truck they never drive.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}