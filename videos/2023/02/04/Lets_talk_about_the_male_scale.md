---
title: Let's talk about the male scale....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KK28i9WQK28) |
| Published | 2023/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Continuing an arc of videos discussing alphas, betas, and sigmas.
- Exploring the system of classification for masculinity exhibited by males.
- Separating biology from gender and acknowledging gender exists on a spectrum.
- Critiquing conservatives for creating social constructs around masculinity boundaries.
- Noting that conservatives confirm the spectrum of gender by creating their own classification system.
- Suggesting that those adhering to specific classifications may be suppressing their true selves.
- Pointing out that using these terms implies an acceptance of gender existing beyond biology.
- Encouraging people to focus on being good individuals and accepting others for who they are.

### Quotes

- "Stop worrying about what a good man is and be one."
- "You created an entire language to classify things in a way that was meant to debunk this idea. And in the process, you confirmed it."
- "Just stop worrying about how other people choose to be the best person they can be."

### Oneliner

Beau delves into the classification of masculinity, criticizing conservatives for creating social constructs and confirming the spectrum of gender in the process.

### Audience

Activists, Progressives, Conservatives

### On-the-ground actions from transcript

- Accept and respect individuals for who they are (suggested).
- Stop worrying about conforming to societal constructs of masculinity or femininity (implied).

### Whats missing in summary

The full transcript provides a nuanced exploration of how societal constructs shape perceptions of gender and masculinity, urging individuals to embrace authenticity and acceptance.

### Tags

#Gender #Masculinity #SocialConstructs #Acceptance #Individuality


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to continue an arc of videos,
hopefully conclude it.
We'll see how that works out.
As discussed earlier in the week,
oftentimes people on YouTube,
they'll plan out an arc of videos,
a series of videos that all link together
if you're paying attention.
This is something I do pretty often.
I started one a few days ago
and after numerous interruptions and detours,
we are back on it.
So today we are going to talk about alphas
and betas and sigmas, oh my.
And we're gonna talk about what we can learn from them.
It's not a joke.
And by we, I mean, if you identify yourself as liberal,
progressive, leftist, whatever, any of these terms,
there's something that you can learn of value
from this predominantly right-wing thing that's occurring.
And conservatives can also learn something from it
if they're paying attention.
So when you were talking about this particular
pop psychology, pop culture thing, there's six of them.
Alpha, beta, gamma, delta, sigma, and omega, I think,
are the six that are commonly used.
There's actually other ones,
but those are the ones that are commonly referenced.
What is it?
Like, what is that?
And don't say the remnants from a debunked study.
I mean, yes, that's true, but that's not the point right now.
This is one of those videos.
Let's just say it's true for a second.
Because if you do that, you can learn something of value.
What is it?
If you were to try to define this system, what is it?
It's a system of classification
for how males exhibit their masculinity.
That would be a definition of it.
Which is really cool.
It's a separation of biology from gender, number one.
Being masculine is a gender, right?
And they have categorized that gender by the way people feel,
by how masculine they are, how they present it to the world.
And they've developed this whole system
for kind of keeping track of it all.
And it all exists separate from biology.
That's cool.
I mean, that's cool.
And I'm sure there's some people who already see
where this is going.
And if you're going to send a message or complaint about it,
please understand, I'm not going to read it
unless you can explain to me the biological need for boys
to have blue sheets and girls to have pink sheets
when they're babies.
There's not one.
It's a social construct.
Just like all of this stuff.
It's a social construct.
And it identifies the way men express their gender.
And it's on a spectrum.
That is wild.
That is wild.
This is the language of the right.
No weird academic leftist came in and proposed this language.
They generated it on their own because they acknowledge
these things are separate and gender exists on a spectrum.
The problem with conservatives, the problem with the right,
is they're very interested in the status quo.
They accept a lot of peer pressure from dead people.
So they create an additional social construct,
a line that says anything beyond this point,
well, that's not real.
All that stuff over there, that's fake.
That's made up.
It's not like the entire language
that we created to express this phenomenon that gender
exists on a spectrum.
That stuff over there, if it gets too close to being
feminine, that's just not real.
But you have to acknowledge that that line is also
a social construct.
It's not real.
It's just something you made up.
It goes to further something that I've
believed for a long time.
There are many occasions where conservatives
are so eager to prove something wrong that they will go out
of their way to create a whole new system of belief,
a whole new language in this case,
and end up confirming the thing they were
trying to say wasn't true.
That's how this started.
This started with the idea that men weren't men anymore.
They were getting too feminine.
And you created a system by which to classify men's gender.
It's wild.
You can take this a lot further if you want to.
If you want to follow this train,
you end up realizing that many of the people who
strive to be on one end or the other
or strive to fit into a particular classification,
they have to learn how to do it because that's not really
who they are.
That really isn't how they want to present to the world.
I would have to assume with the massive level of categories
that they have created that the only reason that they would go
out of their way to try to relearn their masculinity
and present it in a specific fashion
is because the way they feel, the way they want to act,
the way they want to present is on the other side
of that socially constructed line.
And I mean, that's true of a lot of people.
A lot of people who don't even buy into this,
this whole classification system,
they would never use this without a heavy dose of irony.
They're on the other side of that socially constructed line,
too.
If you are a person who has ever used these terms
with any degree of sincerity, please just understand,
you have acknowledged that gender exists on a spectrum.
You have acknowledged that gender
exists separate from biology.
And you should acknowledge that you never
get to make a comment about there being too many genders,
or you don't get all of these pronouns,
or anything like that ever again.
You created an entire language to classify things
in a way that was meant to debunk this idea.
And in the process, you confirmed it.
Just stop worrying about what a good man is and be one.
Stop worrying about what a good person is and be one.
More importantly, for the rest of the world,
stop worrying about how other people choose to be
the best person they can be.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}