---
title: Let's talk about the new assistance to Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=koiT1Rw1tKg) |
| Published | 2023/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of ground-launched small diameter bombs (GLSDBs) in assisting Ukraine.
- Mentions that the West has been hesitant to send this equipment due to its increased range.
- Describes the GLSDB as a rocket that is GPS guided and can glide in, providing an effective range of 150 kilometers.
- Notes Ukraine's current range capability at 80 kilometers.
- Speculates on the potential use of GLSDBs to disrupt Russian supply lines and logistics.
- Points out that if Ukraine receives GLSDBs before a Russian offensive, it could be a game changer.
- Emphasizes the importance of timing in getting the GLSDBs to Ukraine before any potential offensive.
- Indicates that disruptions to Russian movements could be significant with the deployment of GLSDBs.
- Mentions the lack of a specific timeline for the arrival of the GLSDBs in Ukraine.
- Suggests that providing Ukraine with these tools may enhance their ability to counter Russian actions effectively.

### Quotes

- "If they stretch the lines far enough, there's a greater chance of disruption, and that can be duplicated. That is a game changer."
- "That is where I think it would really turn into a game changer."
- "The good thing about this is it really is like a couple of components that are pretty common."

### Oneliner

Beau explains the potential game-changing impact of ground-launched small diameter bombs (GLSDBs) on disrupting Russian supply lines and logistics in Ukraine, if deployed strategically before any offensive.

### Audience

Strategists, supporters, policymakers

### On-the-ground actions from transcript

- Contact organizations supporting Ukraine to understand how assistance can be provided (implied)
- Keep updated on developments in Ukraine and support initiatives that aim to enhance their defense capabilities (implied)

### Whats missing in summary

Context on the broader geopolitical implications and potential consequences of providing Ukraine with advanced military equipment.

### Tags

#Ukraine #GLSDBs #Russia #MilitaryAid #Geopolitics


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
more assistance to Ukraine and GLSDBs. Don't worry, we'll get there. I got a message from somebody
who's basically their dad was like super excited because he found out this stuff was going over
and either he trained on it or had something to do with it being developed or something like that,
and he was saying that it was a game changer. And the questions that came along with this were
basically, well, if it's going to be a game changer, why haven't you talked about it? And
is it really going to be a game changer or is this just my dad being nostalgic?
Okay, first question first, why haven't I talked about it? I didn't know it was in the package.
I did not know that they were sending these. This is actually something that the West has
been pretty leery about sending because it increases range a lot. Can it be a game changer?
Depends on how it's used. But yeah, depending on when it gets there and how it's used.
So let's start with what is it? Ground-launched small diameter bomb, GLSDB. For those who
understand the jargon, this is basically a GBU-39 duct taped to an M26 rocket. It's not really
duct taped. For everybody else, this is a rocket that is GPS guided and it kind of glides in.
Why it matters is that it has an effective range of, I want to say, 150 kilometers.
Currently, Ukraine is running with, I want to say, 80.
So it gives them a substantial increase in their ability to hit stuff at distance.
Now, I don't know if this is something the media is going to latch on to or not.
I would imagine, though, if pundits do start talking about it,
most will probably focus on its potential use for disrupting command and control, I would guess.
I don't think that's where it would really shine, though. If they get these prior to
the anticipated Russian offensive and then get them fielded, it can really be a game changer.
It can be used to disrupt supply lines. Right now, Russia can have its supply depots at 90 kilometers.
They're going to have to move them back to like 160. Stretching those supply lines when Russia
has put so much effort into working out its logistics and disrupting what they have to do
just before an offensive, that could really help. Russia is not great with
their supply lines. These could be used to stretch those lines and basically duplicate
all the incompetence we saw during the initial invasion. Remember when all the tanks were
running out of gas and stuff like that? People didn't have ammo. If they stretch the lines far
enough, there's a greater chance of disruption, and that can be duplicated. That is a game changer.
That is where I think it would really turn into a game changer. If they get it there, it could be used for that.
I haven't seen a timeline. I looked. This does appear to be a relatively
recent decision. I haven't seen a timeline on when it's supposed to get there, though,
and that's going to factor into it a lot. If they can get it there and get it fielded
before the offensive and they can force the supply depots back, yeah, it could definitely
provide disruptions to Russian moves that we already know they're not good at dealing with
because we've seen it over and over again. That's where I would imagine they would want to use it,
but we're gonna have to wait and see. The interesting thing is the increased range,
the ability to hit stuff at that distance, because again, the West has been very
uncomfortable providing equipment like that. This to me suggests they may be getting over some of that
and that they might start providing the tools that Ukraine really needs to win.
The good thing about this is it really is like a couple of components that are pretty common,
so there shouldn't be issues as far as drawdown and production and all of that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}