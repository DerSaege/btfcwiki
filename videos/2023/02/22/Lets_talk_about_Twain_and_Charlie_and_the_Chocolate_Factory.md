---
title: Let's talk about Twain and Charlie and the Chocolate Factory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ldH4L3yTVIE) |
| Published | 2023/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Charlie and the Chocolate Factory and Mark Twain's works are being edited to use more inclusive languages, sparking debates and questions.
- An older copy of Huckleberry Finn by Mark Twain is examined, revealing instances of potentially offensive content.
- Twain's context as a satirist and his relationships with black individuals are discussed to counter accusations of racism.
- Twain's advocacy for issues like reparations and his views on race as a social construct are pointed out.
- Misunderstandings of Twain's satirical elements, particularly in relation to race, are addressed.
- The possibility of editing Twain's work for younger audiences is discussed, focusing on maintaining the original message.
- Beau compares the editing of Mark Twain's work in 2011 to the current edits in children's books like Charlie and the Chocolate Factory.
- Editing children's books for inclusivity is seen as a commercial decision to widen the audience rather than censorship.
- Beau explains that the changes in these books are driven by capitalism, aimed at appealing to a broader market.
- The impact of capitalism on art is mentioned, suggesting that it can sometimes lead to less impactful content due to the pursuit of profit.

### Quotes

- "It's not censorship. It's not woke nonsense either, it's capitalism."
- "It's just capitalism."
- "And capitalism does have a habit of making art less impactful."
- "It's really that simple."
- "Anyway, it's just a thought."

### Oneliner

Beau examines the editing of classic literature for inclusivity through a lens of capitalism, challenging notions of censorship and oppression.

### Audience

Educators, Book Lovers, Parents

### On-the-ground actions from transcript

- Support initiatives that teach critical thinking and provide context for literature (implied).
- Encourage open-mindedness and understanding of satire in educational settings (implied).

### Whats missing in summary

The detailed analysis and examples provided by Beau in the full transcript are missing from this summary.

### Tags

#Inclusivity #ClassicLiterature #Capitalism #Education #Satire


## Transcript
Well, howdy there, internet people.
Let's begin.
So today we are going to talk about Charlie
and the Chocolate Factory and Mark Twain.
If you missed the news, Charlie and the Chocolate Factory
and other books by the same author, they're being edited.
They've been edited to use more inclusive language
throughout the book.
This has prompted a lot of discussion, a lot of questions.
One in particular said, I know what
you're going to say, you're going to say they're just making it more inclusive for children,
but how would you feel if they did this to something like Mark Twain?
Okay.
Hold in my hand an older copy of Huckleberry Finn by Mark Twain.
If I was just to open this up and start reading at the beginning, how many pages would I get
through before I said something that YouTube might decide this channel didn't
need to be here anymore for. About three and a half. Second chapter, first paragraph.
That's where it first talks about Jim. Jim was not just called Jim. If you were to take this
book and hand it to a student today, middle school, and you asked him what they
thought of Twain after they read it. They would probably say he's a racist or
they might say well that's just the way people were way back then, you know, and
And that explains his comments.
And sure, for some of what Twain has said throughout his life,
that would apply.
But when gauged by his contemporaries,
Twain wasn't a racist.
Not really.
And that context isn't going to be talked,
because we don't want to teach anything critically
about race, you know?
So it gets lost.
Twain is not accurately depicted by what's written in this book because this book was
satire.
This book, if he had a patch on his hat, it would be upside down when he was writing it.
Twain was a person who hung out with Booker T. Washington, raised funds for black schools
in the South.
He advocated and talked about the concept of reparations.
I don't remember the exact quote, but it was something to the effect of the idea that black
people had had their manhood ground out of them, and that it was our shame, and that
we had to pay for it.
And then he put his money where his mouth was.
When a black student got accepted to Yale Law, he provided financial assistance.
When a black painter needed money, I think for traveling, I don't remember exactly,
he paid for it.
He was somebody who referred to race as a fiction, a custom.
Wasn't real.
Today we would say it was a social construct, right?
In fact, one of the passages that often gets cited to show how racist Twain was, is meant
to show the exact opposite.
There's a part in this book where Huck's dad is just super angry, very mad, because
a black man's going to be allowed to vote, and he talks about how, well, that's just
going to be the end of it.
We're all going to be living in decay.
The joke, the point that's being made, is that, to put it nicely, Huck's dad, he's
already at the bottom of the socioeconomic ladder.
He's already living in decay.
But because he had somebody to look down on, based on a fictional custom, well, it made
him feel better about his own station.
him looking down, kicking down, rather than looking up at the people who engineered a
system that kept him in that spot.
The book's often misunderstood.
Now could you edit this book and have it carry the same punch for younger readers?
You could go through and switch out one word for slave, as an example. So, slave
Jim. You could switch out another word for Indian, and then you could just
explain what I just did. It didn't take long, and younger readers might be more
inclined to dive into it. Now, how would I feel about it? Well, when it happened in
2011, I didn't really feel anything about it. It seemed like a way to make this
work more accessible to a lot of people. Outside of those who really follow
literary news, it didn't really cause waves because the culture war
wasn't in full effect and people weren't treating every decision made by a large
company or state as some form of massive oppression. It did happen. They did. Back
in 2011. You can find articles about it. We can find people in the
literary community talking about how it was horrible, but it wasn't framed as some
woke nonsense. People understood what was happening. Their complaint was that you
should just teach it the way it is and provide the context, which is fine for
older readers, but for younger readers, I don't know. I didn't really have an
issue with it. And I'm somebody who has like literally everything Dwayne ever
wrote on my shelves. Now, when it comes to Charlie and the Chocolate Factory, it's a
children's book. They're making it more inclusive. They're taking out language
that makes fun of people. That's what they're doing. It's not censorship.
it's not woke nonsense either, it's capitalism. The estate and the publisher
decided to do this. Why? Because people don't want to buy a book that makes fun
of their kid. They're making it more inclusive to widen their audience. That's
it. Like most times when people say, oh this is woke censorship or whatever, no
No, no. It's just capitalism. It's just the free market. It's capitalism.
You know, that thing you say you love, it's not, you know, the left doing it.
It's in pursuit of profit.
I don't really have a problem with it.
And make no mistake about it, you can still find original versions of Mark Twain today.
And I'm sure that will be the case when it comes to Charlie and the Chocolate Factory.
If you really want the character to be called enormously fat instead of enormous, I'm sure
you can find it.
This is one of those things where a lot of people may be trying to manufacture a feeling
of oppression. It's just capitalism. It's just capitalism. And capitalism does have
a habit of making art less impactful. Because art that is impactful, well, it's harder to
consume. And if it's harder to consume, it generates less profits. It's really
that simple. It would be great if we had an education system that would teach
context, that would provide the education necessary to understand satire, but
Generally speaking, we don't.
Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}