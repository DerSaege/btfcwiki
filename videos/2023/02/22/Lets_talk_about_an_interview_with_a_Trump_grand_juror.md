---
title: Let's talk about an interview with a Trump grand juror....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=GF-b3BsBohw) |
| Published | 2023/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about an interview regarding proceedings in Georgia, focusing on high profile individuals and their interactions with the special purpose grand jury.
- Mentions Lindsey Graham's behavior, fighting a subpoena and then being polite during the proceedings.
- Finds the offer of immunity to a witness who initially refused to answer questions particularly intriguing.
- Suggests that the immunity offer leading to cooperation indicates potential wrongdoing or illegal activity.
- Emphasizes how the absence of fear due to immunity resulted in the person freely sharing information.
- Points out the significance of this revelation amidst claims of a witch hunt.
- Describes the coverage of the grand jury proceedings, including security measures taken.
- Stresses the importance of understanding how grand juries operate.
- Speculates on the implications of witnesses coming in with immunity deals.
- Surmises that evidence or testimonies presented with immunity may not be favorable for Trump.

### Quotes

- "That is telling."
- "That's not something that normally happens in a witch hunt."
- "Somebody saying, I'm not talking, well, we'll give you immunity. Well, let me tell you what I know."
- "There was probably some evidence that was presented, some testimony that was presented that Trump is going to wish hadn't been presented."
- "That part right there, really interesting."

### Oneliner

Beau delves into an interview revealing immunity offers leading to cooperation in Georgia's proceedings, suggesting potential evidence against Trump.

### Audience

Legal analysts, political observers

### On-the-ground actions from transcript

- Analyze grand jury proceedings and legal implications (suggested)
- Stay informed about developments in Georgia's legal proceedings (suggested)

### Whats missing in summary

Insights into potential legal consequences and the broader impact of immunity deals in investigations.

### Tags

#Georgia #ImmunityDeals #LegalProceedings #Trump #Investigations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk a little bit about
what's going on up in Georgia.
Because there was an interview with one of the people
who participated in the proceedings up there.
And the coverage of it is focusing mainly on
that person's view of various high profile people
that came in and talked and told their side
the story or didn't really want to answer questions in front of the special purpose
grand jury up there in Georgia.
And while that's all fine and good, it's definitely interesting to hear how somebody like Lindsey
Graham fought the subpoena and all of that stuff and then got in there and was incredibly
polite.
That's interesting, but when it comes to the way certain people would like to frame it
as a witch hunt, there's one piece of information that I found way more interesting.
It's this in the reporting from the AP.
At least one person who resisted answering questions became much more cooperative when
prosecutors offered him immunity in front of the jurors, Korsett.
Other witnesses came in with immunity deals already in place.
from what? That is pretty telling to me. That says a lot to me. If I was Trump's
attorney I would be very very concerned about that coverage because that was
somebody who didn't want to answer questions because they believed a crime
had been committed and then once they were offered immunity well that fear
wasn't there anymore, so they talked. That is, that's not something that normally
happens in a witch hunt. That seems like there might be something that
was wrong, that people knew about, something that might have been illegal.
And the person exercising their Fifth Amendment right against self-incrimination,
information, they decided that once they had immunity, well, they didn't really need to
worry about it anymore.
And they talked.
The coverage of this, the interviews with somebody who was sitting on the grand jury,
it's cool.
And it's definitely worth reading them, because it shows how grand juries work.
And it also talks about the lengths that they went to for security.
And all of it is incredibly interesting.
But that little tidbit, somebody saying, I'm not talking, well, we'll give you immunity.
Well, let me tell you what I know.
That's telling.
That is telling.
And that is something that I imagine we're going to see that material again.
This is one of those things where the best case for Trump was that everybody went in
there and said they didn't know what happened and that it was all a witch hunt, it's politically
motivated, it's a farce, so on and so forth.
If people had immunity deals walking in and some were given them in the courtroom, as
this interview suggests, there was probably some evidence that was
presented, some testimony that was presented that Trump is going to wish
hadn't been presented. So while the whole interview and all the coverage is
interesting, that part right there, really interesting. That may lead
to future developments.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}