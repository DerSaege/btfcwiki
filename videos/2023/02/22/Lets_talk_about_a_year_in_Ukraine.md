---
title: Let's talk about a year in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aYuUOifr6Mk) |
| Published | 2023/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the ongoing situation in Russia and Ukraine regarding the competing narratives and the long-term outcomes.
- Explains that there is a difference between winning battles and losing the war, indicating that Russia may be making gains on the ground but has lost the wider geopolitical war.
- Mentions that Russia's geopolitical goals were lost eight months ago, and they are now focused on land acquisition.
- Notes that Russia's attempts to weaken NATO have backfired, as NATO has become more unified and countries on Russia's border are applying to join.
- Points out that Russia is struggling on the ground despite some gains, and this offensive should have been completed much quicker.
- Emphasizes that Russia's logistical issues and troop quality are hindering their progress, especially in facing potential occupation challenges.
- Warns that Russia's current losses of troops are unsustainable and will lead to further challenges in the future.
- States that the Russian offensive was a failure and that they are not walking away from this conflict in a better position.
- Expresses sadness over the conflict and the loss incurred, stressing the need to end wars as soon as possible.
- Concludes by pointing out the ongoing struggles for Russia and the importance of understanding the two narratives surrounding the conflict.

### Quotes

- "The reason they are seen as performing poorly, even if they make gains during this offensive, is because realistically, this should have been done."
- "Wars are bad. They need to be ended as soon as possible."
- "These aren't soldiers. These aren't warriors. These are people yanked out of factories and jails or off the street."

### Oneliner

Beau explains the ongoing struggles of Russia in Ukraine, noting their failure in the wider geopolitical war despite some gains on the ground.

### Audience

Analysts, policymakers, activists

### On-the-ground actions from transcript

- Support Ukrainian forces by raising awareness and providing aid (suggested)
- Advocate for an end to the conflict and the withdrawal of Russian troops (implied)
- Educate others on the geopolitical implications of the Russia-Ukraine conflict (implied)

### Whats missing in summary

Insights on the implications of ongoing conflicts and the importance of understanding both the tactical battles and broader geopolitical outcomes.

### Tags

#Russia #Ukraine #Geopolitics #Warfare #NATO


## Transcript
Well, howdy there internet people, Lidsbo again.
So as we are around the anniversary,
we're going to talk about how things
are going in Russia and Ukraine.
And we got a question that provides a really nice way
to kind of sum it all up and to bring it into focus
as to why there are competing narratives
as to what's happening over there
and how things will shape up in the long term.
In a video last week, you said you expected the lines to change.
And it seemed like you expected Russia to make gains.
But today, yesterday when you all are watching this,
you said that Russia was performing poorly.
I'm not following.
did something happen? There's two different things being discussed. Have you ever heard
the expression, won the battles and lost the war? It indicates there is a difference between
the battles, the fighting, the lines on the map, and the war. Wars are a continuation
politics by other means. When a war starts, there are geopolitical goals to be accomplished.
Russia lost those eight months ago. The only thing they have left now is the acquisition
of land. We have said that Russia has lost those wider concerns for eight months now.
And I would note that Foreign Policy magazine is finally on board with this idea, it came
out in the last month or so.
Those concerns would be weakening NATO.
That would be a goal.
Didn't happen.
In fact, the opposite occurred.
NATO became more unified.
Countries on Russia's border started applying to NATO.
It strengthened it.
The energy that Russia used as leverage for so long, because of this war, Europe is developing
ways to go without it, which weakens Russia.
There's a whole bunch of things like this.
When you're talking about the wider geopolitical war, Russia lost, and it lost a while ago.
cautious and formalized analysts are just now coming to that conclusion
publicly but that's been the case. The fighting on the ground, oh that's an
entirely different story. The lines, that's determining territory acquisition
which is the last thing that Russia can hope to achieve here.
It's not the cost, or it's not worth the cost. Even if they take the whole country,
it's not worth the cost when it comes to the geopolitical goals
that didn't get accomplished
and in fact backfired.
They lost the war,
the wider geopolitical war,
and there's no coming back from it.
The reason they are seen as performing poorly, even if they make gains during this offensive,
is because realistically, this should have been done.
They should have been able to take the country in weeks, if not months.
If you're going to engage in an elective war, you have to be prepared to win.
And I know there's going to be some people who say, well it was never their goal to take
the whole country.
Yes, it was, stop, stop.
It's been a year.
Yes, it absolutely was.
That's why they had the column headed towards the Capitol.
That's why they did the airdrops.
They absolutely were trying to take the whole country and they failed.
Just like shortly after that there was the cope, for lack of a better word, that said
Oh, well they're waiting to send in their good troops.
Y'all remember that?
It was a big thing.
They're going to send in their better equipment and their better troops later.
Yeah, here we are a year later.
They're pulling people out of prisons to go fight.
They're not performing well.
When you're looking at the lines
in the map,
you're
looking at tactics and strategy.
Think about what Bradley said.
amateurs talk strategy, professionals talk logistics. They don't have the logistics,
and it's worth remembering that they're still in a phase of the fighting where
there's lines. Think to recent US wars. How quickly did it take to or how quickly
did it happen that Western forces took the national governments in those
countries. But the wars didn't end, right? The hard part started, the occupation.
Russia hasn't got to the hard part yet. They're still in the phase where there
are lines and they're not performing well there either. To put it into a
different kind of cost, think about it like this. Current estimates suggest
Russia is losing about 5,000 troops a week right now. 5,000 troops a week. That
means in two weeks they will lose what the US lost in two wars over two decades.
That's not sustainable. That is not sustainable. They have to continue
bringing in new troops, new troops that aren't well trained. And every new
generation of call-up, the quality of the troop becomes less. They're not doing
well and this is in the part of the fighting that has lines. Once those
lines are gone, the Russian military is not up to it. They're not up to dealing
with an occupation. This is not the 1940s. When the national government falls, the
populace doesn't always say, okay well we're done too. Oftentimes, more often
than not, they fight on. If Russia has to contend with that, they will fail in a
glorious fashion. They do not have the troops. They had them in the beginning.
They had troops that were trained, at least in theory, to be able to handle that kind of resistance.
They used them as normal infantry. They're gone.
This is not going well for Russia.
All of the talk and all of the focus on the lines on the map, that's fine.
fine, I get why people become fixated with it, but it's kind of like watching a football
game and paying attention to every play, but never looking at the scoreboard.
At this point, all Ukraine has to do is run out the clock.
The Russian offensive that started it during the actual invasion, it was a failure.
It was an unmitigated failure.
The logistics weren't there to back it up.
They didn't achieve any of their victory conditions, any of their victory goals.
And it brought them into the protracted stage.
Once that happened, and they didn't correct their logistical issues, and Ukraine was able
to mount that first counteroffensive that retook all of that territory, Kharkiv, Khursan,
all of that.
At that point, it doesn't really matter geopolitically what occurs.
Russia doesn't walk away from this better than when it started.
If they were willing to commit to five, ten years, maybe twenty, maybe they can come out
of it on the other side with substantial territorial gains.
But there's going to be a lot of loss along the way.
And somebody commented about how I appeared sad when I gave news, when I was reporting
news that wasn't great for the Ukrainian side.
Well, yeah.
The length of this is going to be determined by the number of Russian, quote, victories.
The more they have, the more things that they can show back home as, look, we're winning,
the longer they can keep it up.
the more loss and destruction will occur.
Wars are bad.
They need to be ended as soon as possible.
When Russia wasn't able to quickly take the country, they should have left because they're
drawn into a quagmire.
look at recent U.S. wars and just compare it and understand after all this time, a year,
they haven't got to the hard part.
They haven't got to the truly difficult portion where there are no lines, where the front
line is all around you.
troops are not going to be up for that. What we have seen from this conflict is that Ukrainian
forces are incredibly adaptable and they have really high morale. They will probably continue
fighting no matter what. And we have seen that Russian forces are relying on brute force.
In some bizarre scenario where they actually capture the country, that's what they'll
they'll try to rely on. And all that does is make a resistance movement grow.
They aren't coming away from this better than when they started. The war is going
poorly for Russia. Not just geopolitically, but in the amount of loss. And at this point,
These aren't soldiers.
These aren't warriors.
These are people yanked out of factories and jails or off the street.
It's not, to me, it's sad.
I don't want Russian troops there.
I want them to go home.
But at the same time, when they are lost, to me, it's not a victory.
It's just a waste because of the ego of some guy in Moscow.
Geopolitically, Russia lost the war in the first couple of months.
The wider war, the reason for the conflict.
They lost, and they continued fighting, and not well.
The lines portion of it, the actual battles.
In this case, they lost the war, and they're not doing great at the battles either, and
it's going to continue until those inside Russia say that they've had enough.
And yeah, that makes me sad, to use the word.
But it also explains the two narratives.
And it's why you see commentators that are very supportive of Russia focus on every hundred
meters gained.
Every hundred meters.
And they act like that's a huge deal.
Not when the cost is what it is.
That's still a loss.
It's not sustainable.
Even with 10% of the current rate of loss, it's still not sustainable.
Anyway, it's just a thought.
I think I have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}