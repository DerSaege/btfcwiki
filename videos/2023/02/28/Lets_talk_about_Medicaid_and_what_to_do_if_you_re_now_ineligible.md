---
title: Let's talk about Medicaid and what to do if you're now ineligible....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=g3mefQA1DtI) |
| Published | 2023/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Public service announcement about changes in Medicaid eligibility
- Anticipated loss of coverage for 10 to 15 million people
- Eligibility checks paused during public health issue, now resuming
- Children's coverage through Medicaid or CHIP may still be valid even if adult coverage is lost
- People making slightly more may become ineligible for Medicaid
- Affordable Care Act's marketplace may provide options for those losing Medicaid coverage
- Timeline for eligibility checks varies by state
- Red states likely to move quickly in checking eligibility
- Urges people to gather information and prepare, as changes could start as soon as April
- Emphasizes importance of checking children's coverage separately

### Quotes

- "Just because you lose your coverage through Medicaid doesn't mean that your children do."
- "For those people, it's probably a little bit more important to gain as much information as you can now."
- "But most importantly, and this addresses 80% of the questions coming in, just because you lose your coverage doesn't mean that your kids do."

### Oneliner

Beau issues a public service announcement regarding upcoming Medicaid eligibility changes, warning of potential coverage loss for millions and advising on checking children's coverage separately.

### Audience

Medicaid beneficiaries

### On-the-ground actions from transcript

- Research Medicaid eligibility changes in your state and prepare for possible coverage loss (implied)
- Verify children's Medicaid or CHIP coverage separately if your own Medicaid eligibility changes (implied)

### Whats missing in summary

Importance of taking proactive steps and staying informed to navigate potential Medicaid coverage changes effectively.

### Tags

#Medicaid #Healthcare #EligibilityChanges #PublicServiceAnnouncement #AffordableCareAct


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to do a little public service
announcement when it comes to what's going on with Medicaid
because there are some changes.
The stats and the estimates suggest
that somewhere between 10 million and 15 million people
will lose access, will become ineligible for Medicaid
over the next year or so.
What has occurred is that during our public health issue,
the process of checking eligibility,
it was kind of paused.
So if you change states, you made more money, whatever,
and it affected your eligibility,
that wasn't being checked.
They're going to start checking again.
So the anticipated outcome is that
whole bunch of people will be losing their coverage. There are a number of
things to know about this. First, and this is going to be important for a whole lot
of people, just because you lose your coverage through Medicaid doesn't mean
that your children do. The numbers I've seen say that like 80 to 90 percent of
the kids are still going to be eligible either through Medicaid or through CHIP.
which covers people that make too much for Medicaid but not enough to be able
to afford private insurance. So if you get a notification, make sure you check
that part out. Make sure you check individually on your children's
coverage because that may still be in effect. Aside from that, for a whole lot
of people, they're just gonna be just gonna be over the eligibility marker,
meaning they're making just a little bit too much to qualify for Medicaid. In that
case, going to the Affordable Care Act's marketplace, you will probably be able to
find something there that is relatively inexpensive, certainly, certainly less
expensive than private insurance. So there are options. A lot of people want
to know, like when is this going to happen?
It varies from state to state.
Every state will be checking eligibility at their own rate.
So there's no real way to know without looking
into your state headlines and trying to find out there
from a state level news source how quickly they're
going to be moving forward with it.
You can safely assume, just based on other politics,
that if you live in a red state, it's probably going to happen pretty quickly.
They're going to be in a hurry
to get people off of Medicaid
and they're going to be looking at the eligibility
quickly.
So
for those people it's probably a little bit more important to gain as much
information as you can now
because this could start as soon as April I think.
So
Go ahead and start looking around and start making preparations if your status has changed
significantly.
If you started making more money, if you changed jobs, if you changed states, if your financial
situation changed greatly, for the better, you need to be prepared for it.
But most importantly, and this addresses 80% of the questions coming in, just because you
lose your coverage doesn't mean that your kids do, so make sure you check that
separately. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}