---
title: Let's talk about the Virginia General Assembly....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Lm1uQg9nhaU) |
| Published | 2023/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Virginia General Assembly is experiencing an unusual amount of retirements among politicians.
- This surge in retirements is attributed to the redistricting process that led to more contested elections.
- The bipartisan committee in charge of redistricting couldn't reach an agreement, leading special masters to draw up maps that weren't designed to protect incumbents.
- The upcoming election in Virginia will have all 140 seats in the General Assembly up for grabs.
- There is an expectation of up to a 20% turnover in the assembly due to the changes in the districts.
- The new district alignments have made elections more competitive, causing many incumbents to opt out of running again.
- Voter turnout and candidate quality will play a significant role in the next election, unlike before where district lines heavily influenced outcomes.
- Independents are likely to have more influence in the upcoming election due to the competitive districts.
- The retiring incumbents could signal a shift towards term limits being indirectly instituted.
- Virginia's upcoming election is predicted to be full of surprises and could provide insight into the impact of competitive districts on incumbent longevity.

### Quotes

- "They drew up the maps. And they didn't do things that politicians normally do, like protect incumbents."
- "There's going to be a lot of surprises because it's going to hinge on weird things like candidate quality and voter turnout."
- "The new district may not be quite so secure. So voter turnout is going to be a really, really deciding factor in this one."

### Oneliner

The Virginia General Assembly faces a wave of retirements due to more competitive districts, setting the stage for a surprising and pivotal election focused on voter turnout and candidate quality.

### Audience

Virginia voters

### On-the-ground actions from transcript

- Stay informed about the candidates running in your district and their platforms (implied).
- Encourage voter turnout among your community by discussing the importance of each vote in shaping the future of Virginia (implied).

### Whats missing in summary

Insights on the potential long-term effects of competitive districts on incumbent turnover and the democratic process.

### Tags

#Virginia #GeneralAssembly #Election #Redistricting #Incumbents


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about
the Virginia General Assembly
and what's happening up there.
Because, well, it seems like a whole lot of politicians,
a whole lot of incumbents, they're just getting lazy.
They don't wanna run and we're gonna talk about
why that's happening, how this situation came to be
and what it means for the next election.
As this session closes, you've got a whole bunch of people
announcing their retirement way more than normal.
They've just decided they're not up to running.
They don't feel like it.
And they're going to pursue a different path.
This next election coming up, all 140 seats
in the General Assembly will be up for grabs.
There is an expectation of a up to 20% turnover in the assembly
because things have changed.
So what happened was back in 2021,
there was a bipartisan committee or commission
that was in charge of redistricting.
They were going to redraw all of the maps.
Well, despite it being bipartisan,
They really couldn't get to the bipartisan part.
They couldn't agree on anything.
So special masters got appointed.
They drew up the maps.
And they didn't do things that politicians normally do, like protect incumbents.
The districts were just drawn.
They didn't care about that.
And it led to a lot of situations where people of the same party were in the same new district
now, because there you have to run in the district where you live.
And they weren't as guaranteed, the incumbents weren't protected.
Another way to say this is that they weren't gerrymandered quite as much.
So because of the more contested elections, a lot of people are just deciding it's not
worth it.
So what does this mean for the next election?
It means it's going to be wild.
There's going to be a lot of surprises because it's going to hinge on weird things like candidate
quality and voter turnout, not just how the district's drawn.
There are probably going to be a lot of surprises in the next election there in Virginia.
It stands to reason that because districts there were historically shaped to lean one
way or another, there are a lot of people who are just like, well, I'm in a red district.
I don't need to go vote even though, you know, I'm a Republican or whatever.
The new district may not be quite so secure.
So voter turnout is going to be a really, really deciding factor in this one.
And having a candidate that the people in that district can actually get behind, because
independents are going to have a lot more sway. At least that's the way it seems. So
there are a lot of incumbents from both parties that are just like, yeah, you know what? I'm
kind of done. This is, it should be interesting to watch because it might give us a peek into
A method of kind of instituting term limits from the other side.
If the districts are competitive, well, it's less likely that you're going to have incumbents
that stay in office year after year after year after year.
Some of the people who are retiring were first elected in the 1900s.
They've been there the whole time, and they've just decided it's over.
So this fall when it rolls around, Virginia's probably a state to watch at the state level
there.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}