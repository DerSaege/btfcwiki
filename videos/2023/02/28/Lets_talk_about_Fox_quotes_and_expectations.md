---
title: Let's talk about Fox, quotes, and expectations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=P7AWjOltaJg) |
| Published | 2023/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fox is facing defamation suits related to their election coverage, but proving defamation is difficult.
- Many quotes are circulating, but they lack context and are being used to paint a one-sided narrative against Fox.
- The coverage of Fox's legal situation is biased due to media interests and personal views.
- There is a difference between believing something privately and proving it in court for defamation.
- Viewer expectations of Fox losing the case may be too high, as proving defamation against the network is challenging.
- If Fox perceives a risk of losing, they might opt for a settlement rather than going to court.
- The machine companies suing Fox seem interested in clearing their name publicly, even though most people may not truly believe the claims.
- Beau urges for managing expectations and understanding the difficulty of proving defamation in this case.
- The outcome of the legal proceedings against Fox will likely take months to be known.

### Quotes

- "There's a difference between believing it and proving it in court to the level it has to be proved for defamation."
- "Expectations should be managed on this one. This is a very hard case, and the coverage on it right now just seems like no matter what happens, Fox is going to lose."
- "The machine companies do seem very interested in having their name publicly cleared, which to me is kind of ridiculous."

### Oneliner

Beau cautions against high expectations on Fox losing the defamation case due to missing context in circulating quotes and biased media coverage.

### Audience

Legal observers

### On-the-ground actions from transcript

- Follow the legal proceedings closely and stay informed about the developments (implied).
- Manage expectations by understanding the challenges of proving defamation in court (exemplified).

### Whats missing in summary

The full transcript provides detailed insights into the challenges Fox faces in the defamation case and the importance of managing expectations around its outcome.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Fox and Quotes
and managing expectations.
Over the last couple of weeks, we've seen a steady stream
of quotes come out related to Fox and their coverage
of the election.
A lot of them are surprising, some of them not so much.
But the way they're coming out and the way they're being
covered is leading to expectations that are forming
that, well, Fox is going to lose this case no matter what.
It's slam dunk for the machine companies.
That may not be the case.
So if you don't know what I'm talking about,
The voting companies, the machine companies,
they filed suit against Fox for defamation.
Now, defamation is an incredibly hard thing to prove.
It has a very high bar.
There are a lot of filings that are coming out.
And the quotes are finding their way
into all kinds of coverage about different things.
The thing is, they're filings.
That is the lawyer presenting the best case possible
for their client.
It's not all totally in context yet.
The way it's being covered makes it seem as though, well,
no matter what, Fox is paying on this.
And that may be the case.
But the way the quotes are used, as an example,
there's one from Murdoch saying, yeah,
They endorsed it.
Something to that effect.
Talking about Fox News commentators endorsing the baseless claims about the election.
They endorsed it.
That seems like a slam dunk right there.
I mean, that's the end of the case, right?
Not really.
What's the next question?
We don't know yet.
But if the next question was when, when did they endorse it?
And the answer is, well, up until they knew it was wrong.
And then they brought other people on because the allegations were being made and they had
to cover it.
That changes the whole dynamic.
Now do I expect that to be the case?
No.
No, I don't, actually.
But with a lot of the quotes that are circulating, there's probably some missing context.
It's also worth remembering that it is a situation of media covering media.
Pretty much every media outlet would benefit from Fox taking a hit.
And aside from the corporate angle to it, there's also the journalist who's covering
it.
They may have ideological qualms with Fox.
They may have ethical qualms with Fox.
They may not like the way they do things.
They may think they're too sensationalist.
I am not an exception to this.
But unless they're very, very careful, those journalists, their coverage will be colored
by that.
I think most people knew, or at least believed, pretty much everything we've seen come out.
All of these quotes, Sean Hannity being privately, let's just say, disbelieving of Trump's claims,
I think most of us knew this.
I think most of us believed that the people who were up there pushing this narrative didn't
believe it themselves and they were just using it to rile people up.
But even though we believe that, there's a big difference between believing it and proving
it in court to the level it has to be proved for defamation.
That's a whole other topic.
Now all of this information should weigh heavily in the minds of Fox News viewers.
We'll see if it does.
I'm skeptical.
But for those who are just adamant and they want Fox to pay, I think expectations are
being set a little bit high.
This is a hard case for the machine companies to prove.
My guess would be if it gets to the point where the attorneys for Fox believe that they'll
lose, they'll try to settle and we won't hear about it anyway.
But then again, the machine companies do seem very interested in having their name publicly
cleared, which to me is kind of ridiculous.
I don't think that there are many people who truly believe these claims.
I know there's a lot of people who echo them, but I don't think there's a lot of people
who actually believe them, particularly of those who would be clients of these companies.
So this is just one of those reminders that I know what I would like to see happen here,
and I think a whole lot of other people feel the same way.
But expectations should be managed on this one.
This is a very hard case, and the coverage on it right now just seems like no matter
what happens, Fox is going to lose.
That isn't the case.
That's not the situation.
Right now, we're seeing findings or filings.
We're seeing the best case being presented.
It hasn't been tested in court.
It hasn't been crossed.
There's a whole lot of other stuff that has to happen.
probably not going to know a real outcome on this for months. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}