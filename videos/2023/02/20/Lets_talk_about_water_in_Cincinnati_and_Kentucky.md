---
title: Let's talk about water in Cincinnati and Kentucky....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PnVE_ZqyrS8) |
| Published | 2023/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Cincinnati shut off their intakes from the river around 2 AM due to a train derailment's impact.
- The chemical found downriver wasn't one of the main concerns, but the city closed intakes out of caution.
- The city can sustain on reserves, showing no signs of water rationing.
- The small amount of chemical found could have been filtered out in the treatment process.
- Monitoring both at the intakes and downriver, they aim to prevent issues proactively.
- Northern Kentucky also closed intakes as a precaution without identifying a specific issue.
- Official sources provide information, addressing potential distrust in the area.
- Thomas More University in Kentucky is contributing to testing and ensuring water safety.
- Major water systems are taking the situation seriously within a 300-mile radius.
- Continuous monitoring and preemptive measures are being taken to avoid problems downstream.

### Quotes

- "Monitoring both at the intakes and downriver, they aim to prevent issues proactively."
- "Major water systems are taking the situation seriously within a 300-mile radius."

### Oneliner

Cincinnati and Kentucky proactively monitor water safety post-train derailment, closing intakes as a cautionary measure to prevent potential issues downstream.

### Audience

Residents, Environmentalists

### On-the-ground actions from transcript

- Contact local officials to inquire about water safety measures (suggested)
- Support institutions like Thomas More University in ensuring water testing and safety (exemplified)

### Whats missing in summary

Further details on potential long-term effects and community involvement in monitoring and addressing water safety concerns.

### Tags

#Cincinnati #Kentucky #WaterSafety #ProactiveMeasures #CommunityInvolvement


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today we are going to talk a little bit about Cincinnati
and Kentucky and water and what's going on.
So yesterday, around 2 AM, Cincinnati shut off their
intakes from the river.
This is the river that would have been impacted by the
train in that derailment. The coverage I have seen in the official statements
indicate a number of things. First is that this wasn't one of the four
chemicals that they were really worried about, but what they found downriver, not
at the intake, was the presence of a chemical that gets used in fragrances a
lot and they're not even sure if it came from the the train derailment itself. But
out of an abundance of caution, they closed the intakes and the city is
running on its reserves. The city can apparently last on those reserves a
while because in none of the coverage, in none of the statements, is there any
indication that people need to be worried about rationing water? It's also
worth noting that it appears that the small amount of this chemical that was
found in the water, if it had gone into the intake, it looks as though the normal
treatment process would have filtered it out. It looks like it would have got
caught up in the activated charcoal. So it seems as though this is all out of an
abundance of caution. They're continuing to monitor it and once they have deemed
it safe they'll reopen the intakes. But the fact that at least according to
statements they are acting before stuff gets to the intakes and they're
monitoring not just at the intakes but also downriver and they're making moves
to preempt a problem I mean that's a nice change that's that's kind of cool
it's worth noting that in northern Kentucky it seems to be the same
situation. They have also closed off their intakes from the river, but it also
seems to be out of an abundance of caution. It doesn't seem like there's an
issue that they have identified yet. They're just seeing traces of stuff that
they don't like, so they're trying to maintain the water systems as best they
can using early warning, which is, again, nice and kind of unexpected to be honest.
It is worth noting that all of this information is coming through official sources.
So those who have that dose of distrust due to events in the area just figured I would
let you know, it does appear that Thomas More University is
helping, at least in Kentucky, a lot and trying to make sure
that they're testing and everything is up to speed.
So what does this mean?
It means that at least in the immediate area, by immediate
300 miles or so. The major water systems are taking it seriously and as the flows
move downriver they're monitoring it and they're trying to preempt any problems
before they arise. We will see how effective this is as time moves on. I
I will continue following this and I know some people have sent in messages, I still
have not seen any press releases from non-government entities suggesting that there's another narrative
or that their testing is revealing something other than the official testing.
But I am still looking for it.
So anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}