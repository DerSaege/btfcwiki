---
title: Let's talk about that article about the pipeline....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XEaWpOxxeEQ) |
| Published | 2023/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about an article causing a stir, focusing on Hirsch, a Pulitzer Prize winner, behind it.
- The article claims the US was behind a pipeline attack, Beau respects Hirsch but questions lack of evidence.
- Beau mentions his past video discussing potential culprits behind the attack, including the US.
- Beau deconstructs the article's lack of named sources and evidence to support claims.
- Emphasizes the need for evidence and documentation to support such claims, especially from reputable sources.
- Points out that even though the theory is plausible, without evidence, it falls short.
- Beau questions the credibility of the article without Hirsch's name attached, suggesting it lacks convincing reporting.
- Expresses disappointment in the lack of substantial evidence presented in the article.
- Concludes that while the story could be true, without new evidence, it doesn't change the initial understanding of the event.
- Beau humorously mentions having as much evidence for the pipeline attack being the work of the Little Mermaid or Cobra as presented in the article.

### Quotes

- "There's no evidence."
- "Could this story be true? Yeah, but it doesn't change anything..."
- "We are in the exact same spot as we were then."
- "If an article is making a huge claim, it requires a huge amount of evidence to back it up."
- "It's just a thought."

### Oneliner

Beau questions the credibility of a Pulitzer Prize winner's article claiming US involvement in a pipeline attack due to lack of evidence, maintaining the need for substantial proof behind significant claims.

### Audience

Readers, Researchers, Journalists

### On-the-ground actions from transcript

- Fact-check articles making significant claims (implied)
- Demand evidence and documentation to support claims in reporting (implied)

### Whats missing in summary

Beau's detailed analysis and commentary on the lack of evidence in a controversial article by a Pulitzer Prize winner.

### Tags

#Evidence #Journalism #Credibility #Deconstruction #Analysis


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about an article that came out
and has caused quite a stir.
What we're going to do is we're going to talk about the person
behind the article a little bit.
And then we're going to deconstruct the article.
And then we're going to go from there.
There's a lot of people that are very curious
this article. Okay, so what's the article? It's by Hirsch. It's by Hirsch, and if you
don't know, he's a Pulitzer Prize winner. This guy at one point in time,
journalistic demigod. Okay, big deal. If you have heard of my lie, you heard about
it because of him. He's not new. He's been around a long time. He released an
article that says that the US was behind that pipeline getting hit. Now, I have a
lot of respect for Hirsch. I do and we'll get to, I know what everybody's gonna say
right now, and we'll get to that in a minute.
And if you go back to September of 2022, you'll find a video from me in which I say I'm listing
off the people who could be behind it, and the US is on the list.
And if I was asked to flesh out how the US would have done it, it would be pretty close
to what's in his article.
There are some things that I have questions about,
but it would be pretty close to that.
So it's from somebody that has a pretty good record.
He's a Pulitzer Prize winner.
And the story is plausible.
Now we're going to deconstruct the article,
acting as if this is something that somebody sent in
for us to publish.
it was me I would print it out and then I'd get my pen and I would go through
and I would mark out everything that's background information because he goes
through and he provides a lot of background information about like how
the US has put listening devices on stuff in the past and stuff like that
and stuff that people said, and all of this, that is history.
Then I would go through and mark out all of the quotes from the people who said that his
article was wrong, block those out, and then I'd read it, and I'd look for a name.
not one. Then I would look for some kind of evidence, documentation to back up the
claims. There's not any. It certainly appears that this entire article is
That is based off of one unnamed source.
That doesn't cut it.
People have asked for me to give my take on it.
I can't.
There's no evidence.
The funny thing is that original video, the one that I'll link, the whole point of it
was to not develop a theory until there's evidence.
What is described is plausible, but he
didn't present us with any evidence,
with anything to support the anonymous source.
Anonymous sources should be there
either backup documentation or to provide background to on-the-record
sources. He doesn't have that. He's a Pulitzer Prize winner. He's done a lot of
good stuff. He's not winning a Pulitzer Prize for this. And if you were to look
into him you would find out that he's made other mistakes in the past. I mean I
I still have, I'm a little miffed about the whole JFK thing.
It's if you look at it and you remove who wrote it, you wouldn't be convinced by it.
The only reason this is convincing is because it's from him.
his credibility. It's the fact that he won a Pulitzer. But if you're looking at
that without his name attached, you've seen better reporting from April O'Neill.
And again, from somebody who thinks the theory is plausible and really, this
guy's accomplishments back in the day, huge, huge. But if you look at those, they
had a lot of documentation. They had a lot of stuff that is just missing from
this. So, end result. Could this story be true? Yeah, but it doesn't change
anything from what was said back in September, back when it happened. There's
no new evidence. It is just as plausible. At the end of that video I make a joke
about me personally believing that it was the work of either the Little
mermaid or cobra. They all still have the same amount of actual evidence that has
been presented. Now, it's Hirsch, so maybe he has evidence that he's going to
release at a later date, but based off of what's in that article, I can't have a
take because there's nothing there. There's an anonymous source who is said
to have direct knowledge of the planning. That's it. With no documentation, with
nothing else to back it up, there's no take to have. We are in the exact
same spot as we were then. So, when you're looking at articles, especially
those that are making huge claims, if an article is making a huge claim, it
requires a huge amount of evidence to back it up. Going through, removing the
background information, removing the denials, and looking for the sources and
and the documentation, that's a good way to find how much evidence is in it.
And in this case it's pretty lacking.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}