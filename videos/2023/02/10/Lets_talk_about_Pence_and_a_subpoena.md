---
title: Let's talk about Pence and a subpoena....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=asVRBVs1KBo) |
| Published | 2023/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former Vice President Mike Pence has been given a subpoena from Smith in relation to the January 6 investigation.
- Nobody has officially commented on this, and it's currently a leak.
- Assuming the leak is true, Pence might be one of the last people Smith wants to talk to.
- If Pence claims he didn't know anything, it might lead to more interviews with others.
- The leak probably came from Pence's team, with possible reasons being to assert executive privilege or to give Trump a heads up.
- It is likely that Trump will try to assert executive privilege but may not succeed.
- Pence's testimony will play a significant role in shaping possible charges against the former president.
- This development marks progress in the accountability process but may not immediately lead to indictments.
- The outcome of Pence's testimony will greatly impact how charges are framed.
- The process is supposed to be secret, so the leaked information is significant.

### Quotes

- "Former Vice President Mike Pence has been given a subpoena from Smith."
- "Pence's testimony is going to matter when it comes to how the charges get framed and shaped."
- "This is a big step in that direction."
- "It's going to be mostly speculation."
- "It's just a thought."

### Oneliner

Former Vice President Mike Pence receives a subpoena in relation to the January 6 investigation, marking progress in accountability, with his testimony playing a vital role in shaping potential charges.

### Audience

Political enthusiasts, accountability advocates.

### On-the-ground actions from transcript

- Stay informed about the developments surrounding the January 6 investigation (suggested).
- Pay attention to the potential implications of Pence's testimony on accountability (suggested).

### Whats missing in summary

Insights into the potential consequences of Pence's testimony and how it may impact the ongoing investigation.

### Tags

#MikePence #January6Investigation #Accountability #Subpoena #ExecutivePrivilege


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about former Vice President Mike Pence
and whether or not he's about to go talk to some people.
We're going to talk about what we know, we're going to talk about what we can safely assume,
and we're going to talk about options.
So if you have missed the news, you have no idea what I'm talking about,
it does appear that Mike Pence has been given a subpoena from Smith.
Now, this would be in relation to the January 6 investigation
and probably have something to do with the fake electors stuff, too.
OK.
So first, it's worth noting that nobody's officially commented on this.
It's a leak at this point.
to reporting. Nobody has commented on it. Not Smith's office. Not Pence's people. Nobody's
commented at time of filming. Assuming it's true, we can safely assume that Smith is nearing
the end of his investigation when it comes to the 6th. Pence would be one of the last
people he wanted to talk to. And then, assuming there aren't any giant bombshells in the testimony,
that would be very close to the end of it. Now, if Pence goes in there and says a bunch
of stuff that he didn't know anything about, he would have to go interview other people.
seems unlikely. It is being framed as if it's like a major escalation. I don't
know that that's true. It is safe to assume that this leak did not come from
Smith's office. So it probably came from one of Pence's people. There are three
realistic reasons why they would do that. One is just somebody talking out of
school not knowing what should stay quiet. That's a problem that's been
happening a lot lately. Another would be that Pence asked for the subpoena and
wanted it leaked so he could say I had no choice I had to talk to him. Keep in
mind, Pence's team's been relatively cooperative throughout most of this.
Um, another, another option would be that it was leaked to give Trump a
heads up, so Trump could attempt to assert executive privilege, maybe.
I mean, it's, it's a realistic option.
I don't know how realistic.
No matter what, I do think it is safe to assume that Trump is going to try to assert executive
privilege and I'm fairly certain that he'll lose that.
The courts have been pretty clear about this and even recently they have been siding with
no, you need to go talk to them.
So that's what we know and what we can reasonably assume.
Everything beyond that is going to be mostly speculation and people need to remember that.
There's going to be a lot of it because this is a big deal.
If the reporting is accurate and Pence really did get a subpoena, we are nearing the end
at least one part of what Jack Smith is looking into. But it's worth remembering
he's looking into a lot of stuff so even if this part gets wrapped up that
doesn't mean that he would immediately go to pursue indictments because he
very well may hold it all until the end, but it does show progress.
What Pence is going to say is going to matter a lot when it comes to what type of charges,
if any, are available for the former president.
If Pence goes in there and says, Hey, you know, I really felt pressured to, uh, to throw
out these votes and Trump knew that he had lost, that's, that's pretty much charge.
Um, so Pence's testimony is going to matter when it comes to how the charges get framed
and shaped.
If Pence goes in there and just kind of repeats what he's mostly said in other places already,
it's not good for Trump.
So, this is a... for those who are waiting for accountability, this is a big step in
that direction.
I just don't want to oversell it, kind of manage expectations on this one.
It still could be a while because he may want to wrap up, Smith may want to wrap up all
of the other stuff that he's looking into.
Or Pence could open up a whole new line of investigation through his testimony that we
don't even know about yet.
So there are still a lot of options on the table that we can't even guess on because
we don't have that information.
Remember this process is supposed to be secret.
We're not even supposed to know this is happening.
That being said, it's going to be the biggest news story of the day, and undoubtedly the
news outlets are going to want to fill time, so you're going to get a lot of speculation.
Just remember that it's going to be mostly speculation.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}