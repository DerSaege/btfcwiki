---
title: Let's talk about a mindset on colonialism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=msgeFj0t_u0) |
| Published | 2023/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the mindset of certain individuals developing contradictory opinions based on superficial understandings of India's foreign policy decisions.
- Critiquing the suggestion that India should befriend China due to colonial history, despite past conflicts and hostilities between the two nations.
- Analyzing the attitude of individuals believing they understand India's situation better and should dictate their national interests.
- Condemning the colonial mindset that implies lesser nations should submit to historically dominant powers.
- Defending India's right to determine its own foreign policy and rejecting the notion of being subservient to other nations.
- Emphasizing India's significant growth as a nuclear power with upcoming multiple carrier groups, challenging the idea of needing external advice.
- Warning against underestimating India's progress and capabilities in force projection, particularly in the defense industry.
- Urging people to pay attention to India's rapid advancement and readiness to assert its interests on the global stage.

### Quotes

- "I understand the Indian experience under colonialism better than Indians."
- "Ukraine is a country, free. They're allowed to do as they want."
- "Maybe they don't need your advice."

### Oneliner

Exploring the colonial mindset influencing opinions on India's foreign policy decisions and defending their right to determine their own interests and assert themselves on the global stage.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Call out and challenge colonial mindsets in foreign policy debates (implied).
- Acknowledge and respect countries' rights to determine their own foreign policies (implied).
- Stay informed about global power shifts, particularly concerning nations like India (implied).

### Whats missing in summary

The nuanced analysis and detailed examples provided by Beau, exploring the implications of colonial mindsets in modern foreign policy debates and the importance of respecting nations' sovereignty in decision-making.

### Tags

#Colonialism #ForeignPolicy #India #NationalInterests #GlobalPower


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about a mindset that
is becoming more common among a certain subset of people.
And we're going to kind of dive into it a little bit,
because it kind of shows how a superficial understanding
of something may lead people to develop opinions that
are contradictory to the opinion that they say they hold,
to a belief that they believe to be right and good and true.
So we're going to talk a little bit more about India
and their foreign policy decisions.
In a recent video, I talked about how
India is getting ready to step into a new role.
They are going to become a much more powerful nation.
And part of that, part of that role
is basically checking China in a way.
There were a number of people who
didn't like that for various reasons.
But we're going to talk about one particular reason
people didn't like it. And there were some very colorful comments about this
in the comment section under that video. And then a couple of people sent
messages expressing the same idea, the same take. And that take is that India is
doing it wrong. They're messing up. They shouldn't be trying to check China. They
should be leaning in and becoming more friendly with them because of the United
Kingdom's actions during colonialism." Okay, that is a wild take and we'll get
to why it's in some ways kind of absurd, but first I do want to point out that
India and China went to war in 1962 and they have had skirmishes and standoffs
on a pretty frequent basis, the most recent of which was in 2021, it's super
hard to become aligned with a country that you are actively hostile towards at
times. But setting aside the actual like foreign policy considerations that
would need to be taken into consideration, I want to talk about the
mindset and the attitude that led to these statements because the general
tone is, hey, I understand the Indian experience under colonialism better than
Indians and because of that I can tell them how to pursue their national
interest in in a better way. That's bad. I mean if you truly believe that's the
case your only option, I mean you kind of have a duty or a responsibility to call
the Indian Embassy and explain to them that you as a white man living in the
United States understand their situation better than them and that you are happy
to volunteer to save the Indians from themselves and teach them how to interact with the rest
of the world in a civilized manner."
Gross, isn't it?
That's a horrible thing.
It comes from a superficial understanding of foreign policy and a reliance on wanting
things to remain the way they were and not understanding that a lot of countries are
going to come into their own and they don't want to bend their knee to China.
India is not becoming a country that's going to be under the thumb of some other nation.
This is a nuclear power that is going to start operating multiple carrier groups.
There are countries in NATO that don't have a single carrier group.
They are going to become a pretty powerful country, way more than they are now.
There are some people who are like, you know, you're kind of downplaying how much they force
project using soft power.
Yeah, I mean, that's valid.
I did.
Because this was more focused on the military aspects of it, because now they're going to
have the carrot and the stick so to speak. This attitude of wanting countries
that for whatever reason the person views as lesser to bow to a country
that has historically been a major power, that's that leads to colonialism. It
It leads to imperialism.
It leads to spheres of influence and all of that stuff.
And it's a take we're seeing more and more frequently.
We see the same thing with Ukraine.
You see that moment where they're like, well,
Ukraine is part of Russia's sphere of influence.
That is a colonial mindset.
That is an imperialist mindset.
Ukraine is a country.
free. They're allowed to do as they want. Ukrainians and Indians get to determine their
own foreign policy. And saying that they're making this drastic error because of colonialism,
something that they experienced, I mean, that's wild. This isn't saying, hey, you know, maybe
Maybe you should focus on getting your oil from a different source because it would be
better for these normal foreign policy reasons.
This is literally saying, you don't understand your own history.
You don't understand it.
You don't know because you're a lesser power.
Made up by lesser people is the subtext.
And they need a white guy to come in and tell them how to do things.
historically white guys came in and told them how to do things. This is horrible.
When you actually analyze the take, it's a defense of colonialism. It's the same
mindset. I've met people from India. They seem pretty smart. I bet they can run
their own country. I'm willing to bet that their foreign policy experts are
pretty good. I mean they have gone from a country that just became a
country out of colonialism and now they're a nuclear power prepared to run
multiple carrier groups. When this is up and running they're in the top five as
far as carriers and that may be part of it too because most people who watch
this are Americans and we have what? We have 11 real carriers, real big carriers
and then like nine carriers for helicopters. Most countries don't have
one. The US has a bunch, the US has more than than most countries that have them
actually, like together. There are a whole lot of powerful nations, major players
carriers on the international stage that don't have two carrier groups.
They're moving into a higher stakes table.
The level of advance that they've had and how far they've come suggests that maybe
they don't need your advice.
they're they're they're looking out for their own interests rather than being
subservient to the country you want them to be subservient to. And then there were
a couple of comments as far as like you know well their their defense industry
really isn't up to this yet and all of that stuff and a lot of that is true but
But I want to point out that in almost all of those comments, the word yet is there.
If you are somebody who has foreign policy takes where you're looking years into the
future, don't sleep on India.
They are moving forward way faster than I think a lot of people are ready for.
And I think once they really start force projecting, I think there are a lot of people who are
going to be surprised.
So anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}