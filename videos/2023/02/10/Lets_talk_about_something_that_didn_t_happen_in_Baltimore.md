---
title: Let's talk about something that didn't happen in Baltimore....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gr29nuxnBfo) |
| Published | 2023/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a plan to knock out the power in Baltimore using an anti-Semitic group's template.
- Mentions that the plan was disrupted but warns that similar incidents may occur in the future.
- Notes the high expectation on authorities to prevent every attack and the inevitability of success for those attempting it.
- Encourages viewers to develop their own response and prepare for potential power outages and infrastructure loss.
- Emphasizes the importance of communication, mobility, and having a plan in place.
- Urges people to talk to their network and be ready in case such incidents happen.
- Stresses the need for individual preparedness since authorities can't stop every attempt.
- Advises taking a few minutes to plan for potential power outages to ensure safety and comfort.
- Points out that most viewers likely already have what they need and just need to work out a game plan.
- Suggests setting aside time now to plan for potential future incidents.

### Quotes

- "You just have to work out the game plan."
- "Put a little bit of thought into it."
- "You want to be able to maintain a level of safety and comfort if something like this happens."

### Oneliner

Be prepared for potential power outages by developing a response plan to ensure safety and comfort.

### Audience

Community members

### On-the-ground actions from transcript

- Develop your own response plan in case of power outages (suggested)
- Communicate with your network about emergency plans (suggested)
- Ensure you have necessary supplies for potential infrastructure loss (suggested)

### Whats missing in summary

The full transcript provides additional context on the importance of personal preparedness and the potential risks associated with infrastructure attacks.

### Tags

#Preparedness #Infrastructure #Safety #Community #ResponsePlan


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about something
that did not happen in Baltimore.
But even though it didn't happen, it could have.
And it can teach us a lot and give us a heads up
and put us in a situation where we can prepare our own responses
and be ready for something that appears
be something that's going to be on the horizon for a while.
If you missed the news, there was allegedly a plan to knock out the power in Baltimore
using the same operational template as the other incidents.
This one was disrupted by the authorities who are saying it is linked to an anti-Semitic
group and it's a clear indication that this is something that groups like this
believe they can accomplish and the thing you have to remember is even
though it was stopped this time the people trying to do it they can hit and
miss. The authorities have to hit every single time. They have to be right every
single time. They can't miss once. That's a really high expectation.
So, on a long enough timeline, the people trying to do it will be successful. In a
city like Baltimore, that's gonna be really bad. So what can you do? Develop
your own response. On this channel we talk about it all the time. It's been a
while, but normally it's about being ready for natural disasters. That's what
it's about, but it's the same stuff. Getting prepared to be without power,
without infrastructure for a pretty decent amount of time. If you are
prepared for natural disasters, you probably already have everything you
need for this. You just need to work out a new plan because this is going to be
different. You might want to work out who you're going to communicate with, the
people that you need to contact because mobility will probably be decently okay.
Your communications will probably be all right as long as you can keep them
charged and you will probably have the ability to remove yourself from the area.
So work out how to do that. Talk to the people in your circle, in your network,
and just be prepared in case it does happen. This seems to be a pretty clear
indication that there are groups that are going to continue to try to do this.
You can't count on the authorities stopping every single one of them, and you don't want to be one of the people caught
up in it.  You want to be able to maintain a level of safety and comfort if something like this happens.
And for most people who are going to, you know, hear this and actually do something with it, odds are you already have
everything you need.  You just have to work out the game plan.
the game plan. So take a few minutes and do that. Set the time aside to figure out exactly what
you're going to do if something like this is successful near you. It will, uh, five or ten
minutes right now, it'll pay off if your power's out for two weeks. If you know where to go to get
get to electricity so you can maintain a a decent level of comfort and safety. So
put a little bit of thought into it. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}