---
title: Let's talk about how social media is changing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=w7J0l0GjCEw) |
| Published | 2023/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Major social media platforms are starting to shift their standards regarding verification, offering paid verifications for accounts.
- Twitter has experimented with paid verifications, and Facebook appears to be planning something similar.
- Verified accounts may gain increased visibility and reach, which raises concerns about potential misuse by bad actors.
- There is a worry that verified accounts with increased reach could shape narratives or spread harmful information.
- The shift towards a pay-to-play scenario on platforms may make it easier for bad non-state actors to run information operations.
- Verified accounts may not necessarily be trustworthy or accurate; they could simply have paid for verification.
- Beau personally decided to take a step back from larger platforms due to concerns about authenticity and profit-driven content.
- He suggests fact-checking information from verified accounts and not blindly trusting them.
- Beau advocates for being cautious about narratives from verified accounts, as they could be manipulated by bad actors.
- There is a need to be vigilant and aware of the potential misuse of increased reach and visibility on social media platforms.

### Quotes

- "There's a big risk to that and it's something that everybody should be on guard for."
- "The verified label indicated a level of authenticity and accuracy. Whereas now it may not be the case."
- "I'm going to be kind of backing off the larger platforms and continuing to look for something that is more open, that is more organic."
- "To me, there seems like there's going to be a lot of room for it to be misused."
- "Y'all have a good day."

### Oneliner

Major platforms are shifting towards paid verifications, raising concerns about the misuse of increased reach and visibility by bad actors; be cautious and fact-check information.

### Audience

Social media users

### On-the-ground actions from transcript

- Fact-check information from social media platforms (suggested)
- Be cautious about narratives from verified accounts (implied)

### Whats missing in summary

Importance of fact-checking and being cautious about narratives from verified accounts to combat potential misinformation on social media.

### Tags

#SocialMedia #Verification #Misinformation #FactChecking #OnlineSafety


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about social media
and how it's changing, how something
that has been the standard for a long time
is beginning to shift at major social media platforms.
For a while now, Twitter has experimented
with paid verifications.
And it has definitely had its ups and downs.
And there's a whole bunch of commentary
on how that was rolled out that could happen.
Facebook now appears to also be planning something similar,
where they will trial verified accounts that are basically
the reason they're verified is because somebody paid for it.
One of the key features that is being
promoted along with this, according to the reporting,
is increased visibility and reach.
With everything that we talk about on this channel,
that gives me pause.
It does appear that increased reach and visibility,
these are two things that people running information operations
would love, and they would certainly be willing to pay for.
There's definitely a concern that a coordinated effort by verified accounts
that have increased reach and visibility, they might use it to shape narratives, to
promote things or whatever. Now it could be something simple, it could be
advertising a new soft drink. And if that's the case, that's no big deal.
That's just standard advertising. But it also creates a situation where the
likelihood of bad actors who are attempting to shape an untrue narrative
or promote things that are harmful, they have increased reach and visibility and
acting in concert they would be able to run an information operation and this is
something that is has generally for a long time kind of been the purview of
state actors but now as things move in this direction and they definitely seem
to be trending more towards a pay-to-play type of scenario on some
platforms, it will make it easier for bad non-state actors. You know, we're not
talking about activist groups coordinating a campaign to tell you to
clean the environment or something, but people who are promoting harmful
behaviors might be able to tap into this new feature to get an inorganic idea or
narrative out there and make it seem as though it is something you should
believe. There's a big risk to that and it's something that everybody
should be on guard for. These accounts that will now be verified, they're not
going to be verified because they're trustworthy. They're not going to be
verified because they have a long history of accuracy. They're going to be
verified because they paid 15 bucks or however much it's going to be. It's
something that everybody has to kind of keep in mind because for a long time the
the verified label indicated a level of authenticity and accuracy. Whereas now it
It may not be the case.
That may not be how things play out.
And people need to be on guard for it.
Me personally, when I was sick, I kind of put myself on social media quarantine.
And I didn't look at anything for like a solid week.
And I have to say, I was happier and realized I was spending way too much time on social
media.
And I've kind of decided because of that and then this news coming out that I'm going to
be kind of backing off the larger platforms and continuing to look for something that
is more open, that is more organic, that isn't ruled by the idea that profit is what should
drive the conversation.
I haven't found one yet because a lot of them that are out there, they're not in widespread
use yet or they're not really user friendly, but I'm looking around.
In the meantime, I would strongly suggest making sure that you fact check things.
fall into a narrative simply because it comes from a verified account because it may just
be somebody in a group of bad actors trying to put out information that is counter to
public health or it could be a political group wanting to spread information that is less
than accurate about a political opponent. The way it's being structured, to me, seems
like there's going to be a lot of room for it to be misused. So it's something that
everybody should be aware of and be on guard for. Anyway, it's just a thought. Y'all have
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}