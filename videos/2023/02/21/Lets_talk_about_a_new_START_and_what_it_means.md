---
title: Let's talk about a new START and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0zIfXH5hgSw) |
| Published | 2023/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the New START Treaty and its recent coverage regarding Russia suspending compliance.
- Clarifies misconceptions about the treaty limiting the number of deployed, ready-to-go nuclear weapons, not the total available.
- Mentions the treaty does not limit tactical nukes and has some discrepancies in counting warheads, like bombers.
- Talks about Putin's statement on suspending inspections, noting other methods like surveillance flights for compliance.
- Addresses the symbolic and performative nature of Putin's speech and the treaty's potential expiration in two to three years.
- Analyzes the messaging behind Putin's speech, indicating possible signals for wider mobilization and longer wars in Russia.
- Emphasizes that the real takeaway is the potential for a longer war, not the nuclear aspects, and that nobody wants a nuclear exchange.
- Stresses that despite portrayals of Putin, he is calculated and likely cares more about his legacy than wiping out civilization.
- Assures that the situation with nuclear deterrence remains unchanged despite the symbolic actions taken.
- Encourages not to be scared by media sensationalism and to understand the context and implications of the treaty and recent developments.

### Quotes

- "Don't let this scare you."
- "Nobody wants a nuclear exchange."
- "That's the real takeaway, not the nuclear stuff."

### Oneliner

Beau explains the New START Treaty, clarifies misconceptions, and analyzes Putin's speech, revealing the real takeaway beyond nuclear concerns.

### Audience

World citizens

### On-the-ground actions from transcript

- Stay informed on international relations and nuclear policies (implied).

### Whats missing in summary

Deeper insights into international relations and potential global conflicts.

### Tags

#NewSTART #NuclearWeapons #Putin #InternationalRelations #GlobalSecurity


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about New Starts.
We're going to talk about the New Start Treaty, what it is,
what it does, what it doesn't do,
and kind of put it into focus.
It's been getting a lot of coverage lately.
And it hasn't actually been bad coverage, to be honest.
For the most part, most outlets are avoiding the obvious fear-mongering that could accompany this
at this point. But there are some who are engaging in it. So we're going to kind of go over it.
So if you have no idea what I'm talking about, Putin in his speech said that Russia was suspending
compliance with the New START Treaty. This is a treaty that provided regulations
on how nuclear arms are deployed. There are some common misconceptions and we'll
clear those up because even though there isn't a lot of fear mongering going on,
on, some additional context might put people more at ease.
OK, so the first thing you hear about this
is that it limits the number of nuclear weapons.
No, that's not true.
It's really not.
That's not what it does.
It limits the number deployed, ready to go
at any point in time.
That's what it limits.
Not the number that are available,
just the number deployed. Now, people seem, there are some who seem concerned because
now we're at greater risk. Not really. We're really not. I want to say the treaty limited
it to 1,550 warheads, something in that range. If you were to pull up modeling on nuclear
exchange, most of them are around 500. It's a 500 warhead exchange. It presents enough
to do that and remain in compliance with the treaty three times over. It doesn't actually
limit the potential destruction at all. It also does not limit tactical nukes.
Even in the limitations, there is some fuzzy math. A bomber, as an example, is
counted as one warhead, I believe. Anybody who is familiar with a B-52 has
questions. Yeah, it's fuzzy. If you're interested, look and see how many warheads
a B-52 or a like aircraft can carry. So the idea of total devastation, that still
occurs and is in complete compliance with the treaty, if it was in full effect.
It's also worth noting that it appears Putin is talking about the inspections.
Part of the treaty allowed each party, the US and Russia, to inspect the others'
capabilities, kind of. And Putin's saying that he's not going to allow that anymore.
anymore. Okay, and that sounds scary, except for the fact that I'm not even
sure it's been happening. They were suspended during the public health issue
and I actually haven't seen any news of them restarting. They might have, but
understand there was a very lengthy period where it wasn't occurring at all.
There are other methods, such as surveillance flights, that can help determine a lot of this.
So, measuring compliance, not really that big of a deal.
Now, overall, for most people, if you are a Cold War kid, none of this is surprising.
If you're Gen Z, you might just be realizing how many nukes are pointed at
the United States as you watch this, and it might be a little unnerving. Please
remember that this situation has existed for like half a century. Another thing
that's worth pointing out in Putin's speech is that he said he wouldn't start
testing unless the US did. That's developing new stuff and testing the
weapons. That indicates that they really don't want another arms race, assuming
that he is telling the truth, and I have no reason to believe he's not, because
frankly Russia can't afford an arms race. So that's good. Now, hearing all of this,
you would say that that speech is symbolic or performative, and it is. And
And this move is symbolic or performative, especially considering, I want to say it expires,
the treaty expires in like two years anyway, maybe three.
But as we have talked about numerous times on the channel, symbolic and performative
actually matter.
They tell us a lot and they help shape narratives.
So what is this saying?
What is the messaging?
couple of different parts to it. One is nuclear saber rattling, which from Russia, okay, whatever.
If you were to take all of the Russian government to include the state media, they
rattle that saber as often as I say, so today we're going to talk about whatever. It's a very
common occurrence. That doesn't mean much. What it may signal is a willingness
on the part of the leadership in Russia in an attempt to set the conditions for
the populace in Russia to accept even more mobilization. Another draft, a wider
war, a longer war. In doing this, Putin admitted how badly the war's going for
them. He didn't come out and say it, but to go from a special military operation
that's supposed to last days to making a speech near the one-year anniversary
talking about how, well, now we're gonna have to, you know, re-examine our nuclear
posture, it's an acknowledgement of how bad that war has gone for them. And it
might be part of a campaign to introduce the idea to the Russian people of a
wider, longer war. A majority of Russians at this point want negotiations to begin.
begin. So they're already kind of done with it. That idea what we talked about
in a recent video, all wars are popular for the first 90 days, that's true over
there too. There's fatigue setting in. So this may be a signal, a message, the
beginning of setting the conditions to prepare the Russian populace for a much
longer war than anticipated. That's the real takeaway, not the nuclear stuff. It
is important to remember that the government in Russia is made up of
Russians, people, not monsters. Nobody wants a nuclear exchange. That's not a
thing. People don't actually want that. No sane person anyway. And I know there's a
lot vested in portraying Putin as a madman. And sure, you can say that some of the things
that he's done are monstrous. You can say that it's just callous and all of this stuff
and that's true. But it's been calculated. In my opinion, it has been very poorly calculated.
But it's been calculated.
It's a person who is attempting to establish a wider empire with Russia, Ukraine, and Belarus
all together, that's somebody who cares about their legacy.
That's not somebody who's going to want to wipe out civilization.
a full exchange, even countries that weren't involved would have major issues.
So that's kind of an overview on it.
Don't let this become something that scares you, that keeps you up at night.
Realistically, it doesn't change anything other than the symbolism and the performative
action. That's about it. That's all it really changes. The status quo as far as the situation
with nukes, the deterrence, all of that stuff, it all remains unchanged. It's just people
are going to become more and more aware of it. Again, we are entering, we're in that
new Cold War and at the moment Russia is still a player. I feel like if they
double down on Ukraine they won't be a player for much longer. They will
degrade themselves into irrelevance. But that's just an overview. So if the tone
of the media does change, keep all of this in mind. The number of nukes
hasn't changed. The number of nukes that were allowed by the treaty is more than
enough to completely destroy the US. So the idea of needing to go beyond that,
little silly. It didn't regulate tactical nukes, which if there is something that
that is even remotely in the realm of possibility of being used, it's that.
And it wasn't even covered by this treaty.
And it didn't actually limit the number of nukes.
Just those deployed, those ready to go, those that are part of that active triad.
So don't let this scare you.
Because if it becomes a slow news week, believe me, that's going to be the goal.
They'll drum up ratings by making this sound absolutely terrifying.
It doesn't need to be.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}