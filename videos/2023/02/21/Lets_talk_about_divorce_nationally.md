---
title: Let's talk about divorce, nationally....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ohU1xuQ1CqM) |
| Published | 2023/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Gyan says:

- Divorce on a national level, splitting up red states and blue states.
- A member of the US House of Representatives suggested this idea.
- Far-right Republicans are interested because their platform doesn't win elections.
- Splitting up to create an authoritarian state rather than adapting their ideas.
- Lack of support for the idea as more information comes out.
- Red states, if they break away, are mostly net takers from the federal government.
- These states rely on federal assistance, which they might not get post-separation.
- Economies of these states are resource-producing, making them vulnerable.
- They wouldn't have the same power, economy, infrastructure, or trade deals.
- The breakaway republic won't benefit the people economically, especially those at the bottom.

### Quotes

- "Far-right Republicans are interested because their platform doesn't win elections."
- "They wouldn't have the same power, economy, infrastructure, or trade deals."
- "It's a horrible idea economically."

### Oneliner

Dividing the US into red and blue states to create an authoritarian state economically devastates the nations involved.

### Audience

American citizens

### On-the-ground actions from transcript

- Educate others on the potential consequences of advocating for dividing the country (implied)
- Advocate for unity and understanding across political divides (implied)
- Support policies that aim to strengthen the entire nation rather than divide it (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the economic and social implications of splitting the nation based on political ideologies.


## Transcript
Well, howdy there, internet people.
It's Bo Gyan.
So today we are going to talk about divorce nationally,
on the national level, splitting everything up.
Red states one way, blue states the other,
and kind of the process and some of the things
that people might not really be thinking about
when they hear this.
The reason we're going to do it is because a person
who is in the US House of Representatives
and who also happens to sit on the Homeland Security Committee
suggested it was a good idea.
So we will talk about why some members of
the Republican Party might be interested in this.
We're going to talk about whether or not the support for it is really there.
And we're going to talk about what would happen
if such a thing occurred.
If you want to know why they're interested in it, look at their tweets. It's there.
split it up red states, blue states, right?
It's worth noting the person who brought this up today
is in a blue state.
Georgia went for Biden.
She's in a blue state.
So why would they be interested in this?
The answer is simple.
The far-right Republican Party platform does not win elections.
It doesn't win.
And they don't want to change their platform
Because they don't want to represent, despite some of their titles, they want to rule.
They want to make up the rules and make other people follow them.
They don't want to represent the will of the people.
So if they can't win elections and they can't rule, in the current system, what's the answer?
Split it up.
Create an authoritarian state.
Break away.
Rather than admit that their ideas are dated and don't reflect the majority of Americans,
they would rather tear the country apart, make it weaker.
Very much an America first idea.
Okay.
Is there really support for this?
No, not really.
I mean, there's more than I think there should be.
But as more and more information came out about how this would actually function, I
I think a lot of people would cool on the idea.
What would happen if in some weird situation it occurred,
red states would break away, form their own country?
And then what?
They'd have an authoritarian government, got that.
But are these rich states?
They're not.
They're not.
Most of them are net takers from the federal government.
They get money from the federal government, money they would not be getting anymore.
Since they are far right, they're not going to tax rich folk to get it.
They're going to tax the working class.
But they don't have that much money to begin with.
When you actually look at social safety nets, when you actually look at what states get
the most assistance from the federal government. It's the ones that think they
can do without it.
Um...
Then you look at the economies themselves. I mean, let's forget about the fact that
they wouldn't be using the US dollar
anymore, so they would actually have even weaker economies.
But what kind of economy
do these states have?
They're resource producing.
their raw resource economies. That's what they sell.
On the foreign policy scene, they would be referred to as developing nations
or non-industrialized.
And that's how they'd be treated.
Wealthier powers, stronger powers would come into these states,
cut deals with the authoritarians in charge,
and extract the wealth.
Same way it happens all over the world.
There are a whole lot of people in the United States
who do not understand the situation they would be in
if they led their lifestyle in a different country.
And that's what they're advocating for.
This breakaway republic, it will not have the same power,
it will not have the same economy,
it won't have the same infrastructure,
It won't have the same trade deals.
It will become a developing nation.
And I know some people are like, but they have oil.
Lots of developing nations have oil.
That doesn't mean anything.
And I would point out that the way that oil would be
distributed, the money from that, in an authoritarian
state, that doesn't benefit people on the bottom.
I mean, even in the current state, I mean, the state that has the most oil that would
be part of this group, I mean, they have a hard time keeping the lights on.
It's one of those things that to the far right, it sounds good.
It sounds good because they are so accustomed to all of the ease that the federal government's
assistance to their state provides. I mean, let's be honest, there's not a lot
of high finance going on in Jackson. Their economy would not be good. Their
standard of life, their way of going about their day, would go
down very quickly. And when you have a whole bunch of authoritarians in power
who only care about themselves and are more interested in ruling than representing, well,
that the amount of freedom you have, the liberties that you enjoy, they would disappear.
You would also have a mass exodus of people from these countries, or new country, if they
all decided to band together.
It would economically devastate the countries involved.
And this is assuming it all happened peacefully.
It is a new pipe dream to sell the far right.
It's a new buzzword that they can use to motivate people who don't really want to think anything
through.
It's the same repackaged baseless claims that they have used to keep their base energized
this entire time.
It's not a real possibility.
Those who want it, want it for their own ends.
And economically, it's a horrible idea.
It is a horrible idea.
There are a whole lot of states out there that could not make it without the assistance
that the federal government gives them.
And that assistance comes from the states that they don't like.
The assistance comes from the taxes from the Northeast, from California, from places like
that.
That's what funds the subsidies.
They keep those states running and they want to get rid of them because they can't win
an election on the platform that the far-right has decided they want.
Rather than change their opinion, they would prefer to destroy the country.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}