---
title: Let's talk about Texas, schools, and mistakes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=24qyLLpGVHw) |
| Published | 2023/02/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Story about superintendent leaving weapon in school restroom.
- Weapon found by third-grader, reported to teacher.
- Superintendent resigned after the incident.
- Acknowledgment of danger of unsecured firearms in schools.
- Children shouldn't be expected to be vigilant about firearms.
- Arming teachers is a bad idea.
- Near misses with firearms should be learning opportunities.
- Solution lies in changing the culture around firearms.

### Quotes

- "Acknowledge that they are dangerous, they're not a prop, and the good guy doesn't always win."
- "Near misses should be teachable moments."
- "The solution is a change in the culture that surrounds firearms."

### Oneliner

Beau talks about a superintendent leaving a weapon in a school restroom, the dangers of unsecured firearms in schools, and the need for a cultural shift around firearms safety.

### Audience

Parents, educators, community members

### On-the-ground actions from transcript

- Advocate for stricter gun safety measures in schools (implied)
- Educate children about the dangers of unsecured firearms (implied)
- Support initiatives promoting responsible firearm ownership (implied)

### Whats missing in summary

The emotional impact of near misses with firearms and the importance of prioritizing firearm safety education.

### Tags

#FirearmSafety #Schools #CommunityPolicing #CulturalShift #GunControl


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a story that has been a
few weeks in the making, even though it just kind of hit the
news cycle.
We'll talk about that story.
We will talk about the resolution to that story, how
it was totally foreseeable, and the expectations that are
placed on kits. Okay, so a few weeks ago, about three, I guess, according to
reporting, the superintendent of schools in Rising Star, Texas was at one of the
schools and went into the restroom where they removed their weapon and left it in
a stall in the restroom. The weapon was discovered by a student, I want to say a third grader,
who luckily did the right thing. Went and told the teacher immediately. Didn't touch it,
didn't mess with it. Good on them. The teacher then sent another student to see if it was
I think it's safe to say mistakes were made, but that student also didn't mess
with it, said it was real. It was recovered and luckily nobody was hurt.
Now according to reporting the school didn't tell anybody this happened. The
The media reported on it, parents were understandably upset, and they made their voices known.
The superintendent has resigned, which is probably the appropriate response here.
Now the superintendent has resigned, obviously understands the error.
So I don't want to come down too hard on this, but there is one quote that I really
think we need to kind of pay attention to.
This is one of those examples of guns in schools.
Regardless of who takes responsibility, they are a considerable danger and one should school
their child to be on the lookout for any unusual placement, a weapon, or anything out of place.
They are a considerable danger, and one being left unattended, that is totally foreseeable.
I believe it's been discussed on this channel.
The acknowledgement that it is dangerous, that's good.
That is good.
I think there are a lot of people who do not understand the danger.
I would imagine that the reportedly pretty short period that the superintendent was without
their weapon, I would imagine that during that time the realities of that danger became
very clear to them.
So that acknowledgement, it's good.
It's good.
However, one should school their child to be on the lookout for any unusual placement
of a weapon or anything out of place.
I mean, yeah, sure, that's true, but I do not think that it is fair to expect elementary
school children to remember their training when the adults cannot.
The idea of arming teachers, it's been a bad idea since it came around.
And there have been a bunch of near misses.
This is just the latest.
The acknowledgement of somebody who reportedly carried their weapon every day, that it is
dangerous.
I really hope that hits home with those who support this idea and see it as a fix for
the problem.
It's not.
It isn't.
And the expectation that children should be on the lookout for unsecured firearms in a
a school. If that doesn't signal that this is a really bad idea, I don't know
what will. There have been quite a few incidents like this, and so far we
We haven't seen anybody hurt.
It would be great if we could learn the lesson that all of these near misses are trying to
teach us.
This isn't the solution.
The solution is a change in the culture that surrounds firearms.
It's a change in the way people look at them.
It's acknowledging that they are dangerous, they're not a prop, and the good guy doesn't
always win, and the good guy might even forget their weapon somewhere, and it lead to tragedy.
It's near miss.
Near misses should be teachable moments.
This isn't the solution.
Anyway it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}