---
title: Let's talk about misconceptions about Trump in Florida....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vH72bk74dYQ) |
| Published | 2023/06/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's case in Florida is still a federal case in the southern district of Florida, not a state case.
- The misconception that Florida's governor could pardon Trump in this case is incorrect.
- Contrary to belief, the high conviction rates in the Southern District of Florida make it a suitable venue for the case.
- Prosecutors from DC are likely to handle the case in Florida to avoid venue-related delays.
- Speculation about the charges against Trump is rampant, but the true details will be revealed on Tuesday.
- Expect charges like willful retention and obstruction, with conspiracy likely involving multiple people.
- Despite assumptions that being in Florida gives Trump an edge, the federal system is complex and not swayed by location.
- Anticipate more criminal proceedings beyond the initial case in Florida, possibly including proceedings in DC.

### Quotes

- "Contrary to belief, the high conviction rates in the Southern District of Florida make it a suitable venue for the case."
- "Despite assumptions that being in Florida gives Trump an edge, the federal system is complex and not swayed by location."

### Oneliner

Trump's federal case in Florida debunks misconceptions while detailing potential charges and venues, with the federal system's impartiality prevailing.

### Audience

Legal Observers

### On-the-ground actions from transcript

- Follow updates on the case to stay informed (implied).

### Whats missing in summary

Insight into the nuances and potential developments beyond the initial federal proceedings in Florida.

### Tags

#Trump #FederalCase #Florida #LegalSystem #Conspiracy #CriminalProceedings


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk a little
bit more about Trump's case and it being in Florida. And we are going to clear up two
misconceptions that occurred and started to take hold just as soon as the news broke.
the fact that he is facing this case in Florida has led to this okay so the
first thing this is still a federal case this is not a Florida case it's a
federal case occurring in the southern district of Florida it's still a federal
judge federal prosecutors all of that the governor of Florida has no sway over
this case. The governor of Florida cannot pardon in relationship to this case.
That's starting to take hold. There's already confusion about that. It
was not referred to the state. It's still a federal case. The other thing is people
saying well it should have been done in DC because DC ran a hundred percent
conviction rate on the January 6th case, so they're obviously better. Okay, the
Southern District of Florida runs on average somewhere mid to high 90s
conviction rates year after year after year, and this is on all kinds of cases,
not just ones that are on video. They are very good at what they do down
there. Okay, so this isn't a ploy to put Trump somewhere where he's going to have
a more favorable jury or, you know, prosecutors who aren't up to it.
Realistically, the prosecutors from DC will be coming down to Florida. My guess
is they did this to avoid endless motions about venue because the at least
some of the alleged criminal activity would have occurred in Florida. So
they're trying to remove a delay tactic is my read on this. There are already a
whole bunch of lists of charges and what's going to be in the indictment.
Here's the thing, if you notice the lists aren't all the same. There's a lot of
speculation going on right now. We'll find out on Tuesday. I wouldn't put a
whole lot of stock in what you're hearing right now. Yes, it's almost
certain that willful retention will be one of the charges. Obstruction will be
one of the charges. These things are, it's, it is, you're safe in assuming that.
The other thing that's worth noting is that most of the lists include a
conspiracy. What that means is that Trump's not the only person who is going
to be going through the the criminal justice system. A conspiracy requires
more than one person. Odds are there will be some form of conspiracy charge but I
I've seen a lot of people already start to say because it's in Florida he's
going to have an edge. That's not really true. It's a misconception there the
federal system is a it's a beast. It's a beast and it occurring in the southern
district does not give him any substantial edge, if any at all. So I
wouldn't worry about that. I wouldn't go ahead and start, you know, casting a
bunch of doom and gloom. The likelihood here is this is just the first, you
You know, first set of criminal proceedings dealing with the alleged witch hunts.
There will probably be more and I have a feeling that at least one of those proceedings will
be in D.C.
Remember this is the start of the federal proceedings, not the end.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}