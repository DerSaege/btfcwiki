---
title: Let's talk about Judge Cannon and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fbk8ujLN0pk) |
| Published | 2023/06/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Judge Cannon, known for controversial rulings, is handling the case involving the former president.
- People familiar with the process believe Judge Cannon may not retain control of the case.
- There are concerns that Judge Cannon’s perceived bias could impact the proceedings.
- Despite some expecting Trump to evade accountability, he is now a criminal defendant in the courtroom.
- The legal system is viewed as a process, and perceived bias may cause delays and appeals.
- Many individuals who doubted Trump's accountability now have to face the reality of a trial.
- Trump, despite having the presumption of innocence, may face further indictments.
- The decision to indict a former president indicates strong evidence and serious charges.
- The American legal system, flawed as it may be, is progressing in holding Trump accountable.

### Quotes

- "The legal system is just that. It's a system."
- "They were probably right about that. There was probably a lot of reservation about doing it."
- "You're talking about a former president of the United States who has been federally indicted on some pretty serious charges."
- "The fact that this has gotten this far indicates that it's probably going to go further."

### Oneliner

Judge Cannon handling case involving former president; concerns about bias impact proceedings, despite doubts, legal system progressing in holding Trump accountable.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed on the legal proceedings involving the former president (suggested)
- Support accountability in the legal system by following the case developments (suggested)

### Whats missing in summary

Context on the potential implications of these legal proceedings for future cases involving high-profile individuals.

### Tags

#JudgeCannon #LegalProceedings #TrumpAccountability #Indictment #AmericanLegalSystem


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Judge Cannon
and the proceedings involving the former president.
Judge Cannon, not a name I expected to see again,
but here we are.
You might remember her as the judge who issued some,
well, let's just call them controversial rulings
regarding the special master back in the first season.
She will initially be handling the case.
Now, the people that I have talked to
who are familiar with this process
and familiar with this district
have suggested that they don't believe
she will retain control of this case.
And they point out that I guess the other judge,
the one that signed off on the warrant, was also mentioned.
They seem to be under the impression that this is only occurring because it's being
viewed as a continuing matter.
And basically they don't expect this to stay where it is for a whole bunch of reasons.
But there are a number of people who are very vested in doom and gloom right now.
They have staked a lot on the idea that Trump won't be held accountable.
So let's say that the case does stay there.
Let's say that all the perception is accurate.
She made the ruling she made before because she has a bias that she can't set aside and
that she will retain control of this.
She made those rulings to help him.
That's the perception.
all true. What I want to point out is that he is now in her courtroom not as
former President Trump but as criminal defendant Trump. And I'm not saying that
to suggest that if her behavior was biased to the degree the perception, you
know, kind of indicates, I'm not saying that that behavior would change. What
What I'm saying is that her earlier endeavors did not work.
The same would probably be true here.
The legal system is just that.
It's a system.
Would her perceived bias, would that delay things?
Probably.
would all end up going through the appeals, being overturned just like last
time. There are a whole lot of people who, even though they believe Trump is guilty
and they say they want him held accountable, they have put a whole lot
into the idea that he won't be. And I think it's going to be hard for those
people who said you know nothing was going to happen to him, who said Garland
wasn't doing anything, who said the search warrant didn't matter, who said the
special counsel was just a tool to let him off politically that way nobody had
to take the blame, who said the the special master process was a way to you
know, get him out of it, who said that it's taking too long, he'll never be
indicted. The people who have chosen that track, they now have to contend with the
fact that they're going to see a trial. Now, does that mean that he's going to be
found guilty? No. Even the former president, who is apparently on tape
about some of this to an alarming degree, he has the presumption of innocence. So
maybe the trial doesn't go the way some people think it should.
That's a possibility, but I don't think it's likely. I would also point out
that I don't think that this is going to be the last indictment. I think he has
more coming his way. It is worth remembering that you are dealing with not
just a criminal case but a historic criminal case. The one thing that all of
the naysayers and those who just you know constantly cast doom on the idea
of him being held accountable, the one thing that they were
definitely right about is that they probably didn't want to indict him.
They were probably right about that. There was probably a lot of reservation
about doing it. What that means is that for them to have brought the indictment,
for the grand jury to have returned an indictment, the evidence is probably
pretty overwhelming. There's only so much that that can be done to alter that.
You're talking about a former president of the United States who has been
federally indicted on some pretty serious charges. It wasn't a decision they
took lightly. It wasn't a decision that they made thinking they might lose. Yeah the American
legal system is broke, okay? But in this case, in all the ways that it is broken, the fact
that this has gotten this far indicates that it's probably going to go further. Anyway,
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}