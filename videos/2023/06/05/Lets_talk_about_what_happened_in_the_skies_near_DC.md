---
title: Let's talk about what happened in the skies near DC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CF5tA-gwzbM) |
| Published | 2023/06/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A civilian plane overshoots its destination by 315 miles, prompting an alert in DC and Virginia.
- NORAD intercepted the plane with F-16s authorized to travel at supersonic speeds, causing a sonic boom.
- The F-16s deployed flares to get the pilot's attention, not shooting down the plane.
- The plane eventually crashed in Virginia, reportedly carrying four people on board.
- Speculation surrounded the incident, but agencies involved suggest no foul play.
- The NTSB is investigating the crash, with possibilities of it being an accident or a medical emergency.
- The incident seems to be accidental, awaiting further information from the investigation.
- Agencies are beginning investigations, with more details expected to emerge.
- The pilot's condition or circumstances leading to the crash are not confirmed yet.
- The incident near sensitive airspace prompted a response, but no malicious intent is suspected.

### Quotes

- "There will undoubtedly be more information coming."
- "It seems like an accident that occurred near airspace that prompted a response."

### Oneliner

A civilian plane overshoots its destination, leading to a crash near DC and Virginia, sparking speculation and investigations.

### Audience

Aviation enthusiasts, Safety regulators

### On-the-ground actions from transcript

- Support the ongoing investigation by the National Transportation Safety Board (implied).

### Whats missing in summary

Further details on the ongoing investigation and any updates following the incident.

### Tags

#Aviation #PlaneCrash #NTSB #Investigation #Safety


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about what happened
in the skies over DC and Virginia.
And just kind of run through the information
that is available at time of filming,
which is early this morning, by the time y'all watch this.
Okay, so a civilian plane,
I believe it was a Cessna Citation,
overshot its destination.
By the time this was all said and done,
had overshadowed by 315 miles. It flew near DC, which prompted an elevated alert at the
Capitol complex. NORAD picked up the plane and F-16s were scrambled to intercept. These
F-16s, according to official statements, were authorized to travel at supersonic speeds,
which produced a sonic boom, which a whole lot of people in the DC area heard.
According to those involved, the F-16s did not
shoot down the plane. However, they deployed
flares in an attempt to get the attention
of the pilot. They were trying to make contact with the pilot.
Now, it eventually crashed in Virginia.
There is reporting that suggests
there were four people on board.
The owner of the plane is out of Florida.
There is a Facebook post that indicates
the likelihood of three of the people
who were on the plane being the daughter,
granddaughter and nanny of the owners, the granddaughter's nanny.
The fourth person is being assumed at this point to be the pilot, but that isn't actually
confirmed yet.
The National Transportation Safety Board is currently investigating.
So that's what's known.
There was, when it happened, there was a whole bunch of speculation.
Based on the statements of the spokespeople of the agencies who were actually involved
with this, I don't think there was anything nefarious about this.
This could be a medical emergency on the pilot's part.
There are a lot of various situations that could result in something like this.
So far, it doesn't seem to be that this was anything planned.
It seems like an accident.
There will undoubtedly be more information coming, but right now, it seems like an accident
that occurred near airspace that prompted a response and that's that's really all that's
known at the moment ntsb is supposed to be beginning their investigation in earnest on monday so
We'll find out more as time goes on.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}