---
title: Let's talk about the Pence charging decision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Bv5dpCImxMI) |
| Published | 2023/06/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the differences between Pence and Trump regarding charging decisions by the Department of Justice.
- Mentions the key element in these types of crimes is intent, specifically willful retention.
- Describes how Team Pence found documents and cooperated fully with the feds to return them.
- Points out Pence's voluntary actions and his lack of intent to retain the documents illegally.
- Speculates that Pence might use this example in his run for presidency to show contrast with Trump.
- Suggests that Pence's actions demonstrate that these standards are consistent and not politically motivated.
- Indicates that Trump may play the victim and cast Pence in a negative light, feeling singled out.
- Concludes by mentioning the unlikelihood of Trump reflecting on his behavior leading to the different outcome.

### Quotes

- "I returned the papers."
- "They had documents they weren't supposed to have."
- "One person returned them, did everything they were supposed to, by the book."
- "It seems unlikely that Trump is going to engage in any introspection."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau explains the differences between Pence and Trump's charging decisions by the Department of Justice, focusing on intent and cooperation, potentially impacting Pence's presidential run and shedding light on consistent standards. Trump may play the victim, unlikely to introspect.

### Audience

Political observers

### On-the-ground actions from transcript

- Follow updates on political developments (implied)

### Whats missing in summary

Insight into the potential implications of these charging decisions for future political scenarios.

### Tags

#Pence #Trump #DepartmentOfJustice #Intent #Cooperation #PresidentialRun


## Transcript
Well, howdy there internet people, Lidsbo again. So today we are going to talk about Pence and the
Department of Justice and their charging decision and what it means and how it's different because
questions have come in. A lot of this we have gone over before on the channel, but it's been a while
and there are newer people to the channel. It also factors into some other things and it will
will showcase some things.
It may have impacts beyond what people are initially kind of looking at.
So if you don't know, Vice President Pence, the Department of Justice, the National Security
Division was like, yeah, we're not charging you over these documents.
So how is it different from Trump?
That's the question.
The key element when you are talking about these types of crimes is intent, willful retention.
What happened with the Pence situation in those documents?
Team Pence found some documents.
Team Pence called the feds and were like, hey, come get these now.
they did all kinds of interviews, interviews with aides, answered all of
their questions. They invited the feds in to come do say like a five or six hour
search in which the feds actually found another document. But it's really hard to
say somebody is willfully retaining something if you're like, hey come get
it. In fact, why don't you come and look around and make sure there's nothing
else there. Generally speaking, I am a strong supporter of the idea that it is
always shut up Friday. This is like the one situation where that doesn't apply.
If you don't have any intent, that's all they're looking for. They want
stuff back. So for Pence, fully cooperated, very voluntary, did everything he could to
get the documents back to where they're supposed to be. Didn't try to keep them, didn't try
to obstruct any attempt to look for them further, stuff like that. Okay, so Pence is going to
be announcing his run for presidency.
My guess is that he will probably use this as an example and say, look, this is how we're
different.
I returned the papers.
I don't know how that's going to go over, but I would imagine he's going to try to use
it and show a little bit of contrast between him and Trump.
The other thing that's going to come of this is it's going to show that these standards
are there.
Anything that Trump did to offend some shadowy organization that makes these decisions and
causes, witch hunts, or whatever, Pence would have done too, right? I mean, they
were buddy-buddy up until, you know, Pence didn't have the courage. So, it
shows that it's not a political thing, and I think that that may actually go, it
It may go some distance with moderates who might have thought, well, maybe that is what's going on.
Anything that would have offended anybody with that much power to go after Trump, Pence was in on.
So that's not what this is.
This is, they had documents they weren't supposed to have.
One person returned them, did everything that they were supposed to, buy the book.
the other person allegedly did not.
That is going to result in different outcomes,
especially when you're talking about a crime
that heavily relies on what the person was trying to do.
So that's where we're at.
I would imagine that Trump is going to go,
poor, poor, pitiful me over this,
and try to just cast Pence as a liberal or a rhino or whatever, because he's going
to feel like he's being singled out.
It seems unlikely that Trump is going to engage in any introspection that might
lead to him believing that it was his own behavior that led to the different outcome.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}