---
title: Let's talk about Trump, a DOJ meeting, and an all caps complaint....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=8MzesGv86Y8) |
| Published | 2023/06/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special counsel's investigation into the documents case is winding down.
- People believe a charging decision regarding the former president is imminent.
- Trump's attorneys were observed entering and leaving the Department of Justice.
- Speculation arises after Trump's fake tweet questioning potential charges against him.
- Former president compares his potential charges to other presidents not being charged.
- Trump claims he did nothing wrong and questions the integrity of the DOJ.
- Trump's message suggests he may have received bad news regarding potential charges.
- Information shared by Trump about other individuals is characterized as inaccurate spin.
- Trump employs whataboutism in his defense, questioning why others were not charged.
- Beau questions the effectiveness of whataboutism as a defense strategy in court.

### Quotes
- "How can DOJ possibly charge me when no other presidents were charged?"
- "Only Trump. Trump, the greatest witch hunt of all time."
- "It certainly seems as though the former president got some bad news today."
- "People need to hide the ketchup bottles type of thing."
- "Whataboutism is not an effective defense in a courtroom."

### Oneliner
Former President Trump speculates on potential charges, comparing himself to uncharged predecessors, while his attorneys visit the Department of Justice, possibly hinting at impending bad news.

### Audience
Political analysts

### On-the-ground actions from transcript
- Monitor developments in the special counsel's investigation (implied)
- Stay informed about legal proceedings and potential outcomes (implied)

### Whats missing in summary
The full transcript provides detailed insights into the speculation surrounding potential charges against the former president and the implications of his defense strategies.

### Tags
#Politics #DonaldTrump #DepartmentOfJustice #ChargingDecision #Whataboutism


## Transcript
Well, howdy there internet people, it's Bob again.
So today, we are going to talk about a message
from the former president of the United States
and some speculation that has arisen from it.
First, to set the scene,
the special counsel's investigation
into the documents case has been winding down.
It has led a lot of people due to the pace.
It's led a lot of people to believe that he is close to a charging decision.
Today, Trump's attorneys were seen headed into the Department of Justice.
Conventional wisdom says that they were there to basically kind of make a final
plea to not be indicted and lay out all of the things that they felt DOJ had
done wrong during this and basically say, you know, Smith is a big meanie and all
that. They were seen leaving. Shortly thereafter, about the length of time it would take to make
a phone call and deliver some bad news, the former president of the United States, Donald J. Trump,
fake tweeted out over on whatever the name of that website is, he said, I do want to point out
before I read this, there's a lot of inaccurate information about other people in it, but I
want to read it as it is. How can DOJ possibly charge me, who did nothing wrong, when no other
presidents were charged? When Joe Biden won't be charged for anything, including the fact that he
has 1,850 boxes, much of it classified and some of it dating back to his Senate day? When even
Democrat senators are shocked. Also, President Clinton had documents, and one in court. Crooked
Hillary deleted 33,000 emails, many classified, and wasn't even close to being charged. Only Trump.
Trump, the greatest witch hunt of all time.
How can DOJ possibly charge me when no other presidents were charged?
It certainly seems as though the former president got some bad news today.
It seems as though his attorneys might have relayed an unofficial charging decision.
We of course don't know that, but the chain of events and the all caps message here certainly
appears as though Trump got some bad news and people need to hide the ketchup bottles
type of thing.
It's worth noting, again, a lot of the information that he's saying about other people is it's
really spin that has been put on things over the years and doesn't accurately reflect the
situations, which is why they were handled differently.
I would hope that the former president and his attorneys know that whataboutism is not
an effective defense in a courtroom.
But there's a part of me that feels like he would be able to test out that defense relatively
soon.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}