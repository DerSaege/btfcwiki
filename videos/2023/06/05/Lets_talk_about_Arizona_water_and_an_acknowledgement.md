---
title: Let's talk about Arizona, water, and an acknowledgement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tDYqSVU2TyA) |
| Published | 2023/06/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Officials in Arizona have announced a halt to new subdivisions in Phoenix due to a water shortage.
- The current demand for water exceeds the groundwater supply by about 4%.
- No new developments can move forward without demonstrating a secure water supply for the next hundred years.
- The significant aspect is the acknowledgment by local and state officials that growth cannot be unlimited with finite resources.
- Hope exists that a profit-driven motive will lead to solutions like desalination plants to address the water scarcity issue.
- This move signifies a rare acceptance of reality by the state apparatus before the problem escalates.
- Despite a 4% shortfall currently, there are strategies to mitigate this over time by looking ahead a hundred years.
- While some may be upset, the restriction on new developments is a necessary step to ensure a sustainable future.
- It's becoming increasingly evident that growth cannot happen without adequate resources, like water.
- This decision stands out positively amid a lack of good environmental news and questionable attitudes of elected officials.
- Even with imperfections, the acknowledgment that growth must be limited is a significant step forward.
- Individuals considering buying land on the outskirts of Phoenix should reassess their plans due to the water shortage issue.
- This decision prompts reflection on the necessity of balancing development with resource sustainability.
- The action taken by Arizona officials sets a valuable example in addressing resource scarcity for future generations.

### Quotes

- "You can't have more developments without water."
- "It's nice to see a moment where even if all of the details aren't perfect, there's an acknowledgement."
- "Wait, we actually can't continue to grow."

### Oneliner

Officials in Arizona acknowledge the finite water supply, halting new subdivisions in Phoenix and signaling a necessary step towards sustainable growth.

### Audience

Environmental advocates, Arizona residents

### On-the-ground actions from transcript

- Reassess plans to buy land outside Phoenix due to the water shortage issue (implied).

### Whats missing in summary

The full transcript provides additional context on Arizona officials' response to water scarcity and the implications for future development planning.


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today, we are going to talk about Arizona, Phoenix, water,
and making the right move.
OK.
So officials in Arizona have basically
said there's a whole lot more details to it
than I'm about to say.
But the short version is no new subdivisions,
No new subdivisions in the Phoenix area.
And the reason is that basically they don't have the water.
They have a study that says their current demand exceeds
what they have when it comes to groundwater by about 4%.
Over the next 100 years, with current conditions,
there's a there's a shortfall. So basically no new subdivisions. If the
division has already been approved it can go in, but new developments are going
to have to show that they have an assured water supply. An assured water
supply means you have water for a hundred years. The details of this really
aren't the big news. The big news is you are starting to have local and state
officials acknowledge you cannot have infinite growth when you have finite
supplies. This is really good news. Now deep down I think that what they're
hoping for is for the profit motivator to take over and maybe a rich developer
spot all of this land that is going to be really cheap now and say you know
what maybe we should put in the desalinization plant. I think that's what
they're hoping for but either way there's an acknowledgement that you can't
have continued growth without the resources. This is something that is, it's lacking in
a lot of places. The just acceptance of reality on this one. And you're talking about a state
apparatus doing it before it becomes a huge issue. Yeah, there's a four percent shortfall.
They can make up that over time.
They can cut that number down because they're looking 100 years into the future.
I know that there are going to be some people that are going to be really upset about this.
I get it.
At the same time, it has to happen.
It has to happen.
You can't have more developments without water.
It seems like it should go without saying, but here we are.
So in a time when we don't have a lot of good environmental news and we're looking at the
attitudes of a lot of elected officials and just saying, what are you thinking?
It's nice to see a moment where even if all of the details aren't perfect, there's an
acknowledgement.
that moment where they're saying, wait, we actually can't continue to grow.
So just one of those things to just kind of process, and this is especially important
if you are looking at buying land outside, in the outskirts of Phoenix, you might want
to reconsider that at the moment. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}