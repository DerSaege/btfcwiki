---
title: Let's talk about Eastman and seeing it again later....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Sl1caKWVPUA) |
| Published | 2023/06/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Eastman is facing 11 disciplinary charges in California that could lead to the loss of his license due to his involvement in trying to alter the outcome of the 2020 election.
- His actions were aimed at obstructing the electoral count on January 6th to prevent Vice President Pence from certifying Joe Biden as the winner.
- The proceedings against Eastman are expected to last about a week with multiple witnesses being called.
- Testimonies from these proceedings could have implications for investigations related to January 6th and Trump in Georgia.
- Many in the legal profession believe that the proceedings against Eastman are necessary for ethical reasons, as his actions were not grounded in the law.
- Witnesses called by Eastman might have direct knowledge of any potential plan.
- The judge in the proceedings does not seem receptive to Eastman's arguments or style, similar to the situation with Meadows.
- Information from Eastman could be critical for potential charges in Georgia and related to January 6th.
- Testimonies from these proceedings might lead to further cooperation from Eastman with federal investigations.
- Despite seeming unimportant now, the proceedings against Eastman could have significant future implications.

### Quotes

- "His actions were aimed at obstructing the electoral count on January 6th."
- "Testimonies from these proceedings could have implications for investigations related to January 6th and Trump in Georgia."
- "Many in the legal profession believe that the proceedings against Eastman are necessary for ethical reasons."

### Oneliner

Eastman's disciplinary proceedings in California, involving attempts to alter the 2020 election outcome, could have significant implications for future investigations and ethical considerations in the legal profession.

### Audience

Legal professionals

### On-the-ground actions from transcript

- Attend or follow updates on Eastman's disciplinary proceedings (exemplified)
- Stay informed about the implications of these proceedings for future investigations (implied)
- Support ethical considerations in legal practice (implied)

### Whats missing in summary

Insights on the potential impact of Eastman's testimony and cooperation on federal investigations and legal ethics.

### Tags

#Eastman #DisciplinaryProceedings #2020Election #LegalEthics #FutureImplications


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're gonna talk about Eastman out there
in California and what's going on out there.
Eastman is the attorney, well, at least for the time being,
who assisted the former president during his attempt
to alter the outcome of the 2020 election.
Out in California, he is going through proceedings,
of a trial and he is looking at 11 disciplinary charges that could result in the loss of his
license. He could be disbarred. These proceedings produce a recommendation. That recommendation
would then go to the California Supreme Court who gets to make the final determination if I
understand that process correctly. Eastman's activities were described as all of his
misconduct was done with one singular purpose to obstruct the electoral count on January 6th
and stop Vice President Pence from certifying Joe Biden as the winner of the election. He was
fully aware in real time that his plan was damaging the nation. Pretty, pretty serious
allegations against Eastman. Now, this proceeding probably going to last about a week. Probably
gonna last about a week because they intend on calling a whole bunch of
people as witnesses. While most of the country could really really couldn't
care about Eastman, the testimony that occurs during these proceedings, I can
assure you those people investigating January 6th, those people in Georgia
related to any potential prosecution of Trump there, they are paying very close
attention. Odds are there are going to be things said under oath during these
proceedings that will come up again later. So the proceedings themselves, I
know a whole lot of people in the legal profession who definitely believe these
proceedings need to occur, just for ethical reasons. And I'm trying really
hard not to make a lawyer joke when it comes to ethics and all of that, but I
mean every attorney I know, it's like this guy needs to not be a lawyer
anymore. The general consensus among those that I know is that his activities
weren't really grounded in the law. He was floating ideas that he
knew were false just to kind of prolong and obstruct. And that seems to be
the core of the allegation out there. But he has the proceedings to go
through and the Supreme Court out there will make the final determination. The
reason it's important to note, this is one of those you will see this material
again type of things. I am almost positive that something said during
these proceedings will end up labeled as an exhibit later. Some of the witnesses
that Eastman plans on calling are people who would have direct knowledge of any
potential plan. The first day of the proceedings did not go particularly well
for Eastman. The judge does not seem willing to entertain a lot of Eastman's
experts and general style even. But this is a lot like the Meadows thing where
the information that Eastman possesses is probably critical to the prosecution
in Georgia and anything related to January 6th. Any potential charges
coming out of Georgia or the January 6th investigation, they might really like to
have Eastman. So any testimony here might lead to Eastman being leaned on pretty
heavily by the federal government trying to get him to cooperate and provide
more more detailed testimony to them in relation people that might have been
higher up and put context to some of the events and actions. So even though this
doesn't seem to be important, it probably will be later. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}