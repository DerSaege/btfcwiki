---
title: Let's talk about Trump getting the witness list....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_dGRP88P2PQ) |
| Published | 2023/06/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's team now has access to the list of people set to testify against him at trial.
- The discovery process has started in the Trump documents case.
- Speculation surrounds who within Trump's circle is cooperating or will testify against him.
- Previously, when the DOJ had sole control, no one knew who was cooperating.
- Team Trump now holds this information, raising concerns about potential leaks.
- The speed at which the information was gathered and prepared for discovery is noteworthy.
- Trump's strategy and reaction to the information now available to him are in question.
- Harassing witnesses or leaking names is advised against, yet it's uncertain how Trump will proceed.
- DOJ may have provided the information early to challenge Trump and deter interference with witnesses.
- There is anticipation around how Trump will handle the situation as he gains access to critical information.

### Quotes

- "Harassing witnesses, leaking names, all of this is a really bad idea for Trump. It is a horrible idea."
- "There's a part of me that thinks DOJ is providing him this stuff this early to just be like, do it, go ahead, and basically dare him to tamper, bother witnesses."
- "He probably won't want to go down that route."

### Oneliner

Trump's team now has access to key information on witnesses in the Trump documents case, raising concerns about leaks and potential interference.

### Audience

Legal analysts, political strategists

### On-the-ground actions from transcript

- Monitor developments in the legal proceedings (implied)
- Stay informed about the case progress and potential implications (implied)

### Whats missing in summary

Insight into the potential legal implications and consequences for Trump in light of the information now available to his team.

### Tags

#Trump #LegalCase #DOJ #Witnesses #Discovery #Speculation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and how his team now has access to the
list of people who the government plans to have testify against him at trial.
The discovery process in the Trump documents case, in the Wolf Hall retention
case. That has started. And Team Trump was provided with basically a whole lot
of evidence right up front. And we're gonna kind of go through the different
little pieces of information that we can kind of pull out of this. First thing
that I want to note is that we're all still guessing as to who flipped, who
within Trump's circle is cooperating, who's really cut a deal, who's really
going to testify against him, so on and so forth. We're all still guessing. As
recently as yesterday on the channel, we were speculating about Mark Meadows. To
To me, that one seems kind of obvious, but we still don't actually know that.
The reason I'm saying this is because the entire time that DOJ had sole control over
this information, nobody knew anything.
Nobody knew who was cooperating.
This information has now been handed over to Team Trump.
If suddenly there are a bunch of leaks, if there are a bunch of comments, if the right
wing commentators get their hands on a list of people that they feel have been disloyal,
It seems like we should keep in mind that the entire time DOJ had sole control over
that information, we didn't know anything.
But now Team Trump has control of that information.
They have access to that information.
Now they're not supposed to share it.
We'll see how that works out.
Okay, something else that we can kind of pull from this.
This was fast.
This was really fast.
As far as having the information to turn over, having Discovery ready at the level it apparently
is, it's not just a list.
It's testimony.
It's a whole bunch of different things, recordings that Smith had ready.
So what does that tell us?
Tells us that he had all this put together when they indicted him.
When Trump was indicted, Smith already had all of this stuff ready.
This wasn't just pulled together recently.
They were ready to start the discovery process immediately, which means they already had
it pulled together to include the memos, everything. So that kind of tells us a
little bit about well what's taking so long. Remember that? Whole lot of people,
myself included, getting impatient. You know I have a whole lot of patience but
towards the end even I was like alright I mean this is kind of ironclad I don't
what y'all are y'all are doing. This might account for a lot of that time. So
what does that tell us about the perceived delays when it comes to the
January 6th investigation? The same thing's probably playing out. If there
are indictments to come from that investigation, my guess is they will be
ready for discovery immediately then as well. So that might explain some of the
delay up there, some of that perceived delay. Okay, so what's Trump gonna do?
That's the kind of the big question. I mean there's little bits of information
that we can pull out of this. They could put other things in context, but what's
he going to do now that he has access? Team Trump now knows who is going to
testify against him. Now, going after these people, harassing them, leaking their
names, all of this stuff, it's a really bad idea for Trump. It is a horrible idea.
And there's a part of me that thinks DOJ is providing him this stuff this early
to just be like, do it, go ahead, and basically dare him to tamper, bother
witnesses. It's Trump. The way he typically handles situations like this
is to start attacking and discrediting anybody he thinks is going to turn on
him. His attorney should be advising against this behavior. It's also, it's
Trump. He doesn't listen to his attorneys. That's part of the whole problem.
I would imagine that we're going to get information we shouldn't know within the
first handful of times he speaks alive somewhere because that's normally when
he starts going through his grievances. I have a feeling that when he starts
that process, some of the information he's not supposed to disclose is going
to leak out. Or maybe his attorneys are smart and they're, I don't want to say
not showing their client something, but maybe they are limiting his access as
as much as humanly possible to basically protect him from himself.
This is the start of a process that has to occur before it goes to trial.
Trump is entitled to basically have access to everything that DOJ might use against him.
what this process is. My guess is that it's a pretty extensive amount of
information. In this case in particular I think there's going to be a lot of
information. The reason I have repeatedly said if it gets to that point talking
about trial is because there still is the possibility that Trump changes his
plea. He is a very, let's just call it self-assured person, so I don't expect
that. I don't expect him to look for a deal, but it's still within the realm of
possibility, especially if he is confronted in this discovery process
with just loads of information and testimony and just a whole bunch of
stuff that he is not going to be able to overcome. His attorneys might start
advising that. But it's Trump and he probably won't want to go down that
route. We're going to have to wait and see but this is really when you can
expect the start of Trump trying to manage the situation, trying to play
politics with it. Because up until now he didn't really have access to
much information. We're going to see if he adopts a different strategy here
because typically he would be on social media blasting anybody's name who
he saw in this. He would be saying bad things about them. That is incredibly
ill-advised, but that doesn't mean he's not going to do it. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}