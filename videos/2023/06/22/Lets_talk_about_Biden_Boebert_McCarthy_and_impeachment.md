---
title: Let's talk about Biden, Boebert, McCarthy, and impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=osPKUjOvmvo) |
| Published | 2023/06/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Boebert attempted a procedural move to force a vote on impeaching Biden, which seemed illogical.
- McCarthy, the Republican Speaker of the House, intervened to delay the process, sending it to committees.
- The pressure on far-right Republicans, like Boebert, is growing due to their impatience and belief in inflammatory rhetoric.
- McCarthy's intervention was likely because the impeachment attempt wouldn't progress and could put swing district Republicans in a difficult position.
- McCarthy needs moderate Republicans in vulnerable districts more than the far-right, hence his actions to move towards the center.
- Protecting Biden from impeachment may be a strategic move by McCarthy to ensure his longevity as Speaker of the House.
- The conflict within the Republican Party is driven by the tension between moderate Republicans and the far right, fueled by their own inflammatory rhetoric.
- Toning down the rhetoric might be the only way out for Republicans facing pressure from their base.
- More conflicts within the Republican Party are expected, with McCarthy likely to navigate towards the center to maintain his position.
- The pressure on Republicans is a result of their own rhetoric, with the far right posing a hindrance rather than a help to McCarthy's goals.

### Quotes

- "It was a pizza cutter motion, all edge, no point."
- "McCarthy needs those representatives who are in vulnerable districts far more than he needs the hardline far-right Republicans."
- "Protecting Biden from an impeachment process."
- "Far-right Republicans, they're not a help to him. They're a hindrance."
- "Toning down the rhetoric isn't something they've considered yet."

### Oneliner

Representative Boebert's illogical impeachment attempt on Biden prompts McCarthy's intervention to protect swing district Republicans from a divisive vote, illustrating the growing conflict within the Republican Party and the need for a shift towards moderation.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Contact your representatives to express your views on the tactics and rhetoric within the Republican Party (suggested).
- Join or support moderate Republican groups advocating for a shift towards the center in political discourse (implied).

### Whats missing in summary

Insight into the potential consequences of the ongoing conflict within the Republican Party and its impact on future political dynamics.

### Tags

#Republicans #Impeachment #McCarthy #Boebert #PoliticalConflict


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Boebert and Biden
in impeachment and McCarthy and how he handled it
and why he did what he did
and how all of that is shaping up and why it's happening.
So if you have no clue what occurred,
Representative Boebert used a procedural move to attempt to force a vote on the
House floor to impeach Biden. Why specifically? That's kind of a mystery.
It doesn't really make a whole lot of sense but whatever. The attempt was made.
It was a pizza cutter motion, all edge, no point, okay. McCorthy intervened. The
Republican Speaker of the House kind of didn't stop it, but definitely delayed it
a little bit. It appears that this attempt will be heading to the judiciary
and Homeland Security committees. Okay, so why is all of this happening? We were
just talking about how the Republican base, particularly the base of the far
right Republicans like Boebert, that they're getting impatient. They've been
told that it's all treason, but nobody's doing anything. The Republicans
leaned into that inflammatory rhetoric, that inaccurate rhetoric, they're not acting like the
Republican is at stake. The pressure is more intense on the far right. So this was Boebert
trying to quote do something. McCarthy intervened. Why would McCarthy do this? One, it wasn't going
anywhere, realistically. Let's say it makes it out of the House. It's certainly
not going anywhere in the Senate. And in the process of making it out of the
House, what would happen? It would put the Republicans who are in vulnerable
districts, those that need those swing votes, in a position where they had to
cast a vote that would alienate either the swing voter, the independent, or it
would alienate the energized Republican base. Those who believe that rhetoric.
It's funny because when that video came out, when we're talking about how this
was going to become an issue, people were like, what do you have specifics? And for
the record what I talked about as far as the responses on social media those are
quotes that that actually happened but if you're talking about a specific
policy here you go this is one of those times and this was very dramatic I mean
you're talking about a snap decision on impeachment obviously in normal political
times this wouldn't occur. But Republicans, particularly those who leaned
in to that inaccurate rhetoric, they're filling the pressure and they're trying
to appease their base. However, for those Republicans who aren't in, you know, just
ridiculously red districts, they can't take a vote like that and make it, they
won't get reelected, they can't use that rhetoric. McCarthy needs those
representatives who are in vulnerable districts far more than he needs the
hardline far-right Republicans.
So you will see McCarthy move to lean towards center and do things that don't
make a whole lot of sense, like protecting Biden from an impeachment process.
You know, McCarthy wants what every first year Speaker of the House wants.
Another year, and then another, and then another.
In order to do that, whether he likes it or not, he has to move the Republican Party back
towards center.
far-right Republicans, they're not a help to him. They're a hindrance because that
inflammatory rhetoric is a liability. So that's what occurred. Expect to see more
of it. That type of conflict within the Republican Party between
what passes as a moderate Republican and the far right and the source of it is
the pressure that they're feeling because of their own rhetoric. I guess
you know toning down the rhetoric isn't something they've considered yet but I
feel like that's kind of their only way out at this point. Now I had no idea that
that was going to happen when that video went out. Just a happy coincidence, but I
would expect more of it even though it's probably not going to be quite as
dramatic as a snap impeachment vote. Anyway, it's just a thought. Y'all have a
good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}