---
title: Let's talk about Trump, Smith, and a pair of Queens....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lyM__1CGLrc) |
| Published | 2023/06/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump and Smith are engaged in a strategic game, with Smith seemingly holding at least a pair of queens.
- Mike Ronan, a campaign official for Trump in 2020, has entered into a cooperation agreement with the federal authorities.
- Ronan is not a high-profile figure but is believed to have had significant access to alternate electors and worked closely with Rudy Giuliani.
- During House Committee hearings, Ronan refused to answer questions about his interactions with Giuliani post-election by invoking the fifth amendment.
- The cooperation agreement was initiated once the Department of Justice informed Ronan he was to testify before a grand jury.
- Ronan's phone was confiscated in a related investigation that likely involves election interference, January 6th events, and potential fundraising irregularities.
- These issues could potentially lead to separate legal cases if pursued by the Department of Justice.
- The protective walls shielding Trump seem to be weakening, with visible cracks emerging.
- Beau speculates that Ronan might have more undisclosed information, possibly another "queen" up his sleeve.
- The increasing rate of information disclosure suggests a substantial amount of evidence could be amassed by the time indictments are sought.
- Beau humorously imagines the potential volume of evidence being so extensive that Ronan might need to store some of it in his bathroom.
- The narrative implies escalating legal troubles for Trump's associates and hints at mounting evidence against them.

### Quotes

- "The cracks are starting to show."
- "The rate at which this information is now coming out leads me to believe that by the time this actually goes for, it reaches the point where they're actually seeking the indictment, going to have boxes of evidence."
- "I am of the opinion that Smith actually has another queen up his sleeve."

### Oneliner

Beau speculates on Trump associate's legal troubles, hinting at mounting evidence and potential undisclosed information.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor updates on legal proceedings and investigations involving Trump associates (implied).

### Whats missing in summary

The full transcript provides additional context and humor, enhancing understanding of the ongoing political dynamics and legal challenges facing Trump and his associates.

### Tags

#Trump #LegalTroubles #PoliticalAnalysis #Smith #RudyGiuliani


## Transcript
Well, howdy there internet people, Lidsabow again.
So today, we are going to talk about Trump and Smith
and the little game they are playing
because at this point in time it,
well, it does appear that Smith
is holding at least a pair of queens.
Reporting now suggests that Mike Ronan,
who was a campaign official for Trump in 2020,
has entered into a proper agreement with the feds.
Okay, so this is not a person
that has a high public profile, okay?
But this is a person that many believe had a lot of access
when it comes to alternate electors and stuff of that. He also is reported or
believed, I should say, believed to have worked closely with Rudy Giuliani. In
fact, during the House Committee, during those hearings, he was asked specifically
about his conversations with Giuliani after the election. He pled the fifth.
So, something else to know is that this proffer agreement was entered into reportedly once
the Department of Justice was like, hey, by the way, you're gonna talk to the grand jury.
it's also worth noting that a while back they took his phone. Okay, so all of this
would be related to the election interference, January 6th, maybe the
fundraising stuff. Keep in mind that if the Department of Justice wanted to, all
All three of those could be completely separate cases.
The walls that have protected Trump for so long, there's a lot of cracks in them right now.
The cracks are starting to show.
I am of the opinion that Smith actually has another queen up his sleeve that
that hasn't been publicly confirmed yet.
The rate at which this information is now coming out leads me to believe that by the
time this actually goes for, it reaches the point where they're actually seeking the indictment,
going to have boxes of evidence. So much so that Smith might have to store some of
in his bathroom. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}