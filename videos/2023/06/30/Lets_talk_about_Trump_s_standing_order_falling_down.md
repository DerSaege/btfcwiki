---
title: Let's talk about Trump's standing order falling down....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3Anx__UCTLk) |
| Published | 2023/06/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the concept of standing orders that Trump claimed to have.
- Recalling Trump's assertion of having a standing order to declassify any file at his discretion.
- Mentioning his initial skepticism about the existence of such standing orders.
- Noting that a judge compelled the Department of Justice and the Office of the Director of National Intelligence to respond to a Freedom of Information Act request.
- Revealing that the DOJ and ODNI confirmed possessing no records responsive to the request.
- Speculating on the reason behind their reluctance to provide an answer initially.
- Stating that the idea of these standing orders goes against tradition, practice, and common sense.
- Describing this confirmation as one more defense tactic of Trump's that has been debunked.
- Implying that Trump's defense strategy around standing orders has been unsuccessful.
- Signing off by wishing a good day to the viewers.

### Quotes

- "Those kind of standing orders are not real, don't pretend like it is."
- "That's them saying that this doesn't exist."
- "This isn't something that could really disrupt that."
- "It's one more defense that Trump tried to float that, well, nah, that's not going to work."
- "Anyway, it's just a thought."

### Oneliner

Beau reveals the non-existence of Trump's claimed standing order for declassification, debunking a defense tactic.

### Audience

Media consumers

### On-the-ground actions from transcript

- File Freedom of Information Act requests to seek transparency from authorities (suggested)
  
### Whats missing in summary

Full context and detailed analysis of Trump's defense strategies regarding classified information and standing orders.

### Tags

#Trump #Declassification #StandingOrders #FreedomOfInformationAct #Transparency


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're going to talk about standing orders
that were never standing.
And something that we, I mean, I would say we knew,
but now we definitely have confirmation of.
If you remember when the documents case
first started coming into public view
and people started talking about it,
and the idea that some of the information
might have still been classified, came up. Trump said that he had a standing order
to declassify any file that he, you know, wiggled his nose at, blinked twice,
took to his room, whatever. He had a standing order to declassify information
if it would help him now. Now, when this came out, I was like,
Yeah, that doesn't exist, that's not a thing.
Those kind of standing orders are not real, don't pretend like it is.
At the same time, it's Trump, okay, and it's his staff.
They could have done something just ridiculously outside of the norms.
Thanks to Jason Leopold, and I want to say it was with Bloomberg, we now kind of have
confirmation of that. They did a Freedom of Information Act request asking for
any such standing order, copy of it, whatever, and it was through the
Department of Justice and the Office of the Director of National Intelligence,
and they were told that each agency, let's see, what does it say, possesses no
records responsive to your request. That's them saying that this doesn't
exist. They were compelled by a judge. The way I understand it is that a judge
actually had to tell them that they had to provide an answer. Initially I think
they said they didn't want to because of probably because of the ongoing case
but it's not this isn't something that could really disrupt that. So this would
have been one of his defenses. Through tradition, practice, basic common sense,
rules, all of that stuff, this isn't a thing. Now through the Freedom of
Information Act request, we now know that at least the DOJ and the ODNI are saying
that it doesn't exist either. So it's one more defense that Trump tried to float that
That is, well, nah, that's not going to work.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}