---
title: Let's talk about why malaria is in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pMqD1DJ2joA) |
| Published | 2023/06/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the spread of two things, one real and one not, particularly in Florida and Texas.
- Confirming at least five cases of malaria in Florida and Texas, sparking fear and misinformation.
- Explaining that climate change has expanded the range of insects, leading to the spread of diseases like malaria.
- Mentioning a Department of Defense study predicting the rise of diseases due to climate change, including malaria.
- Warning about the increasing prevalence of diseases due to climate change and profit-driven motives to maintain dirty energy.
- Encouraging viewers to watch a video discussing the national security threat posed by climate change and disease spread.

### Quotes

- "Climate change has increased the range of insects."
- "We're going to have to deal with diseases that we normally didn't."
- "It's not because of some conspiracy other than one that is profit-driven to keep you using dirty energy."

### Oneliner

Beau explains the spread of diseases like malaria in Florida and Texas due to climate change, debunking conspiracy theories and exposing profit-driven motives behind dirty energy use.

### Audience

Climate activists, public health advocates.

### On-the-ground actions from transcript

- Watch the video discussing the national security threat posed by climate change and disease spread (suggested).

### Whats missing in summary

The tone and delivery style of Beau's message, as well as the additional context provided in the full transcript.

### Tags

#ClimateChange #DiseaseSpread #Malaria #ConspiracyTheories #DirtyEnergy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about something spreading.
Well, two things spreading, really.
One is real and one is not.
And we're going to kind of fill in some gaps.
Because right now in the United States,
something unusual is present, particularly
in Florida and Texas.
But that has caused other spread elsewhere.
So we are going to address that, provide accurate information
and context, and hopefully get rid of some theories here.
OK, so if you have no idea what I'm talking about in Florida
and Texas, there are at least five
confirmed cases of malaria.
Why?
That's the question.
It's unusual, and it's scary.
What does that lead people to do?
Fill in the gaps with bad information.
This is what creates conspiracy theories.
Right now, I wanna say this is being blamed
on like Melinda Gates.
What's the reality?
Climate change has increased the range of insects.
Those insects, well, sometimes they carry illnesses.
This sounds like something that would be made up
after the fact to provide a cover story for the conspiracy.
Years ago, I did a video
And it went over a DOD study, Department of Defense study,
that basically outlined
how climate change had become a national security threat.
And it talked about how insects were going to have greater range
and they were going to carry disease.
The very first one mentioned is, of course, malaria.
The Department of Defense said this was coming.
This was going to happen in the coming years.
It is now the coming years. Now if you go back and watch that video please
understand I did a lot more satire back then. I'll have it down below. Just
understand the framing device being used is that the report was put out by AOC
because back then if you said AOC was behind something you could get all kinds
of conservatives to watch it just to like hate watch it and then they would
get the information. It's not till like halfway through you realize the report
that I'm citing was not created by AOC but it was created by DOD. So make sure
you watch the whole thing. But this is going to be the new normal. We're going
to have to deal with diseases that we normally didn't. And it's not because of
of some conspiracy other than one that is profit-driven to keep you using dirty energy.
After this we're going to get dingy just so you know.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}