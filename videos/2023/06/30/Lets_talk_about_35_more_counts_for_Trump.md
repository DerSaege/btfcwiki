---
title: Let's talk about 35 more counts for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Bt7wuTIyDEw) |
| Published | 2023/06/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Light reporting suggests Trump may face 35 to 40 additional charges soon.
- Origin of the reporting is from sources not authorized to speak publicly.
- Speculation on whether charges will be added to the original case in Southern District of Florida.
- Possibility of charges related to willful retention or election interference.
- Mention of potential indictment for attorneys who assisted Trump in his attempts to stay in power.
- Uncertainty on whether the charges for Trump and his circle will be under the same indictment.
- Speculation on the impact of new charges on Trump's legal situation.
- Prediction that Trump may end up facing over a hundred counts in various districts.
- Emphasizing ongoing legal challenges and potential criminal exposure for Trump and his associates.
- Cautioning about the limited information available and the need to wait for more details to emerge.

### Quotes

- "If it's something tacked on in the Southern District of Florida, it really doesn't mean much at all."
- "There is a lot of potential criminal exposure for the former president and those in his circle."
- "Everything else is speculation."
- "The devil's always in the details on this stuff."
- "Just know to be ready for something in the coming weeks."

### Oneliner

Light reporting hints at Trump facing 35 to 40 additional charges soon, with uncertainties surrounding their nature and impact, underscoring ongoing legal challenges and potential criminal exposure for him and his associates.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay updated on the latest developments in the legal proceedings against Trump and his associates (suggested).
- Monitor reputable sources for accurate information on the potential charges (suggested).

### Whats missing in summary

Analysis of potential implications for the political landscape

### Tags

#Trump #LegalChallenges #PotentialCharges #Speculation #CriminalExposure


## Transcript
Well, howdy there internet people, it's Beau again. So today, we are going to talk about some light reporting that has
surfaced and  The former president of the United States, Donald J. Trump
Along with the possibility that he is about to add to his collection of counts
It's worth noting before we get too far into this this is based off a very light reporting and
And the reporting originates from people familiar with the matter, people not authorized to speak in public, so on and
so forth.  It is, this is one step above like the good rumor mill, like the confirmed rumor mill.
So what's it saying?
Trump is about to be hit with an additional 35 to 40 counts.
in the coming weeks. Now, that obviously brings up the question of, well, what are the charges?
That may not be the most important question, but we'll go over it.
The real important question is where? It very well could be a superseding indictment,
where they're just tacking on charges to the original documents case indictment down in the
Southern District of Florida. Completely possible. We know that there were additional willful
retention counts that could have been charged that weren't. So it could be those. That is
completely possible. It could be something completely unrelated. We don't know. It could
be something related to the documents case in a different venue, in a different district.
to say, I don't know, hypothetically speaking, somewhere up north, maybe by one
of his properties up there. That may be a way to help mitigate the influence of a
judge that some see as less than impartial. So it's that possibility. A lot
of people have suggested that it is the election interference indictment. I don't
think so, not at this point. I know that's conventional wisdom because that's one
that everybody's waiting on, but realistically with the information that
we learned about Queen Rudy, they'd probably want to wait to get all of that
processed first before they start proceeding with an indictment there. It's
possible, but it doesn't seem super likely at this point. We're going to have
to wait and see, but the important part here is that the one thing that all of the various
people familiar with the matter confirm is that there are more indictments coming.
In parallel rumor mill activity, it seems that some of the attorneys who assisted the
former president in his attempt to cling to power, they should probably get
attorneys as well because the same rumor mill is suggesting that they're about to
be hit with with charges as well. Now, whether or not those are on the same
indictment, we don't know yet. There's a whole bunch we don't know. Again, it's
early on this, but the the information is coming from coming through the same
routes that have provided accurate information before. So it's worth paying
attention to. What does this mean long term? If it's something tacked on in the
Southern District of Florida, it really doesn't mean much at all. It's unlikely
that it would change the sentence even, assuming he was found guilty of
everything. It wouldn't really matter much at this point if they are the
willful retention charges. If there's something new, perhaps they discovered he
shared that information with, I don't know, a foreign power. That might alter
things a little bit, but we don't know yet. The information is going to start
coming out. At this point in time, it is very light. So as speculation occurs, just
remember we actually don't know much yet. Just know to be ready for something in
the coming weeks. I would point out that a few weeks after whatever this is
happens, there's likely to be yet another set of counts. My guess is by the time
this is all said and done, Trump is going to break a hundred. He will have more than
a hundred counts in various districts, jurisdictions, and cases. It's going to be ongoing. Eventually,
it may get to the point where it's kind of old news when he picks up new charges. There
is a lot of potential criminal exposure for the former president and those in his circle.
That circle is getting smaller and smaller and smaller.
So this is probably something that is going to continue to occur.
Just remember, oftentimes a source may say, hey, we've got this coming down, but not
elaborate on it.
And the devil's always in the details on this stuff.
So as the speculation occurs, just remember, all that is known for certain right now is
that a source that seems well-placed and a confirming source that seems well-placed has
said there are 35 to 40 more charges coming.
That's it.
Everything else is speculation.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}