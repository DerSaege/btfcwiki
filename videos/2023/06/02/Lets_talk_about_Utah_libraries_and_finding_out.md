---
title: Let's talk about Utah, libraries, and finding out....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aqgJJfKRWjw) |
| Published | 2023/06/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Utah's Davis School District is removing Bibles from elementary and middle school libraries due to vulgarity and violence in the King James Version.
- This action is a result of a 2022 law targeting marginalized groups and free speech.
- The law aims to have parents flag objectionable content in books.
- Beau criticizes the law for attempting to marginalize groups and appeal to bigoted sentiments.
- The removal of Bibles from libraries is seen as a form of malicious compliance.
- Parents likely already monitor their children's reading choices from the library.
- Beau suggests that similar actions may occur in other locations with similar laws.
- The situation is likened to reaping what you sow, even for those who don't read the Bible.
- The underlying message is about the consequences of targeting specific groups through legislation.

### Quotes

- "For whatsoever a man soweth, that shall he also reap."
- "You're not allowed to say other people can't read that book."
- "Expect this to occur in pretty much every location a law like this was passed."

### Oneliner

Utah's Davis School District removes Bibles due to objectionable content, a consequence of legislation targeting marginalized groups.

### Audience

Educators, parents, activists

### On-the-ground actions from transcript

- Contact local school boards to advocate for diverse and inclusive library collections (implied)
- Monitor and guide children's reading choices from school libraries (implied)

### Whats missing in summary

The full transcript provides deeper insights into the implications of legislative actions on marginalized communities and the importance of vigilance in upholding free speech and inclusivity.


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about Utah and a
book being removed. But before we get into that, we're going to start off today a little bit
different with something from Galatians. For whatsoever a man soweth, that shall he also reap.
The Davis School District in Utah in the elementary and middle schools will no
longer have Bibles in the library. The King James Version of the Bible has been
reviewed by the district and it was found to have vulgarity and violence. This
This is of course occurring under a 2022 law that was put on the books that was aimed
at, you know, othering people and stifling free speech.
It's important to remember that you are allowed to say, you know, I don't want to read that
book in the library.
You're not allowed to say other people can't read that book.
When you pass legislation that is specifically designed to go after marginalized groups,
you're anticipating compliance because you can't get it through persuasion, you can't
get it through reason, you want to use the force of law to do it.
You want compliance.
Utah, apparently those in the Davis School District have decided to use my
favorite kind of compliance, malicious compliance. The people behind that law,
they wanted parents to review those books and find things that they found
That's exactly what they did.
It's a school library.
It is likely that those people who are truly concerned about what their kids are picking
up and reading from the library are probably involved enough to know.
This really seems more like signaling and an attempt to marginalize a group for no other
purpose than to drum up the bigot vote.
That's it.
who actually really cared about this. They would know what their kid is reading.
Expect this to occur in pretty much every location a law like this was passed.
The reaping what you sow thing, even those who don't read the Bible, they have
their own version of that and y'all have reached the find out stage. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}