---
title: Let's talk about AOC vs Musk....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nnDKd9JvfUk) |
| Published | 2023/06/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk changed the Twitter verification system to mean users paid him $8, not that the account was authentic.
- There is a verified Twitter account impersonating AOC, causing confusion as the parody part is not always visible.
- Musk interacted with the fake AOC account, causing further damage and confusion.
- The upcoming election will be flooded with misinformation from fake verified accounts on Twitter.
- Fidelity assessed Twitter's value at $15 billion, down from the $44 billion investors paid for it.
- Businesses relying on social media may need to find alternatives to Twitter due to its decreasing relevance.
- Twitter may not be a reliable source of news leading up to the election.

### Quotes

- "Elon Musk changed the Twitter verification system to mean users paid him $8, not that the account was authentic."
- "The upcoming election will be flooded with misinformation from fake verified accounts on Twitter."
- "Businesses relying on social media may need to find alternatives to Twitter due to its decreasing relevance."

### Oneliner

Elon Musk's Twitter changes create confusion with fake verified accounts like AOC, leading to election misinformation and a decrease in Twitter's relevance for businesses.

### Audience

Business owners, social media users.

### On-the-ground actions from transcript

- Find alternative social media platforms for business needs (suggested).
- Be cautious of news coming from Twitter leading up to the election (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of Elon Musk's changes to the Twitter verification system, the presence of fake verified accounts like AOC's, potential election misinformation, and the declining relevance of Twitter for businesses.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Twitter and AOC
and the next election and fidelity and assessments
and all kinds of things, okay.
So one of the things that Musk did early on
when after purchasing Twitter
to alter the way the verification system works. Verified accounts used to mean
you are interacting with the real account. Now it means you were willing to
give Elon eight dollars. There is an account on Twitter that is impersonating
AOC. It is verified. Now the account it actually has her full name and then like
I don't know, press briefings or something like that, and then parody off at the end.
The problem is the way Twitter abbreviates names, you don't actually see that most of
the time.
You don't see the parody part.
What you see is an AOC account that's verified.
To make matters worse, Musk has actually interacted with this account and put like a little flame
emoji when the account joked about AOC having a crush on him.
Obviously, this is causing damage to AOC, to the real AOC, and the
reason this matters is because when the election rolls around, be ready.
nothing you see on Twitter is going to matter. You cannot trust it, you can't
quote it, you can't source it, you can't act like it matters because there are
going to be too many screenshots and too many people who fall for fake verified
accounts to to really trust any of it. It's worth noting that the verification
system that Musk changed, was there to prevent, well, well, this.
So as election season nears, be very leery of any news coming off Twitter.
In completely unrelated and totally unperceivable news, Fidelity, which is a
company that has a stake in Twitter just did their assessment. They now value the
company at about 15 billion dollars. If you do not remember, the investors
purchased it for 44 billion dollars. If you are somebody who needs social media
for your business, you advertise your business on social media or anything
like that, you probably need to be looking for an alternative to Twitter, because unless
there are massive changes, it seems unlikely that it will remain super relevant past the
election.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}