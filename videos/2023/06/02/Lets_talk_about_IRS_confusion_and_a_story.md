---
title: Let's talk about IRS confusion and a story....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AOgZhHCDQy8) |
| Published | 2023/06/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the confusion surrounding the different numbers mentioned regarding IRS cuts in the debt ceiling bill.
- Illustrates the concept of different interpretations leading to different numbers with a personal story about a security job.
- Reads out the specific legislation from the bill, rescinding $1.3 billion of unobligated balances.
- Mentions an additional $10 billion per year to be pulled back in fiscal years 2024 and 2025, part of a separate deal with Speaker McCarthy.
- Emphasizes that the money doesn't disappear immediately and could be replaced through various means.
- Points out that the $2 billion, $10 billion, and $20 billion figures correspond to different perspectives and deals, not just the bill.
- Acknowledges underestimating the Biden negotiating team and expresses caution in assuming the money is gone.
- Concludes by stating that the money isn't truly gone until it's gone and leaves with a reflective comment.

### Quotes

- "They're all correct, depending on how you figure it."
- "That money's not gone until it's actually gone in my book."
- "They don't want to give up this money."

### Oneliner

Beau clarifies the confusion around IRS cuts in the debt ceiling bill, pointing out that different figures are due to varied interpretations and deals, not just the bill itself.

### Audience

Budget analysts, policymakers

### On-the-ground actions from transcript

- Monitor legislative developments closely to understand the implications (implied)
- Stay informed about fiscal policies and their potential impacts (implied)

### Whats missing in summary

Deeper insights into the nuances of fiscal policy negotiations and the importance of understanding differing perspectives in interpreting financial figures.

### Tags

#IRS #DebtCeilingBill #FiscalPolicy #Interpretation #Negotiations


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to talk about the IRS cuts
in the debt ceiling bill.
And we're going to talk about the different numbers that
are getting thrown out, because there's
a little bit of confusion here.
And I got a message that basically said, hey,
you said $2 billion.
This politician said $12 billion.
This reporting says $20 billion.
So I looked in the bill, and I can only find $1.3 billion.
What's the right number?
They're all right, depending on how you figure it.
They're all correct, depending on how you figure it.
Okay, before we get into this, I want to tell you a story,
because it's gonna illustrate something, stick with me.
Years and years ago, a friend of mine called me,
and he's like, hey, look, I started my security company,
I got this job, it includes doing this,
and I have no idea how to do it.
Will you come up here and show me how to do this?
No, absolutely not, because I don't like doing it.
Please.
So I go up there and I do it and I show him how to do it.
And he gives me like $300 just to cover my gas or whatever.
It's important to understand that this is probably
a $4,000 job.
This is a guy I've known a long time.
So I go back home, don't think anything of it.
More than a year goes by, and I get a call,
and it's from this company that I've never heard of,
but they are convinced that I once worked for them
through a subcontractor,
and they want me to do a $4,000 or $5,000 job
for like $300 bucks.
No, no.
New boss, new deal, okay?
Okay, so what is actually in the legislation, I'm going to read it, of the
unobligated balances of amounts appropriated or otherwise made
available for activities of the Internal Revenue Service by paragraphs blah blah
blah blah blah, of public law 117-169 commonly known as the Inflation
and Reduction Act of 2022, as of the date of the enactment of this act, $1,389,525,000
are hereby rescinded."
I'd like to point out I said less than $2 billion.
That's what's in the bill.
There is a deal, a separate deal, that says in fiscal year 2024 and 2025, they'll pull
back an additional $10 billion a year.
Those would be different bills, that would be appropriations, and that's a deal made
with Speaker McCarthy, the current boss.
I'm going to wait to see what happens before I count that money is gone.
The other thing is that even under the deal, that money doesn't disappear.
It actually goes to other non-defense spending, which isn't really defined in the deal, at
least as far as I know.
It's also worth noting that there are a whole bunch of ways that money can be replaced.
That money's not gone until it's actually gone in my book.
The people who are saying two billion, that's what they're operating under.
Those who are saying 10 billion expect Speaker McCarthy to be around for a year, I guess.
And those who are saying 20 billion or more are reporting on the deal, not the bill.
I am somebody who has underestimated the Biden negotiating team more than once.
I'm not doing it again.
They don't want to give up this money.
And there's a whole bunch of ways that money could be put right back where it's supposed
to be because it's not even gone yet.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}