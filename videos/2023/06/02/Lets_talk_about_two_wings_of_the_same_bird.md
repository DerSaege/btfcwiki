---
title: Let's talk about two wings of the same bird....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wWEBisftHio) |
| Published | 2023/06/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how the debt ceiling vote showcases Democrats voting to hinder good programs and making it harder for the poor, similar to Republicans.
- Questions why Democrats give Republicans votes, suggesting they have similar policies.
- Urges for a video that convinces him otherwise, feeling that the parties are indistinguishable.
- Points out that the programs Democrats support were initiated by the Democratic Party.
- Illustrates the back-and-forth nature of politics and the challenges of a representative democracy.
- Emphasizes the severe consequences of a default if Democrats did not provide votes, leading to the non-existence of beneficial programs.
- Acknowledges dissatisfaction with the political game rather than solely with the Democratic Party.
- Challenges the notion of both parties being identical by citing various marginalized groups who face different treatment under Republicans.
- Encourages building the desired society from the ground up rather than expecting immediate revolution through voting.
- Argues against the narrative that both parties are the same, stressing the tangible harm reduction achieved through Democratic actions.

### Quotes

- "Two sides of the same coin, two wings of the same bird."
- "I wish somebody could tell me I'm wrong, but I just don't see a difference."
- "Ask any woman in the South, in a red state, who wants to engage in family planning."
- "The society you want, the revolution you want, you're not going to vote that in."
- "The idea that they're the same, that is coming from a position with a lot of privilege."

### Oneliner

Beau questions the similarities between Republicans and Democrats while advocating for understanding the nuances and impacts of political actions.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Engage in community-building efforts to work towards desired societal changes (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the perceived similarities between the two major political parties in the US and challenges individuals to critically analyze the nuances of political actions.

### Tags

#Politics #Democrats #Republicans #PoliticalSystem #DebtCeiling


## Transcript
Well, howdy there, internet people, it's Bill again.
So today, we are going to talk about
two sides of the same coin,
two wings of the same bird and all of that.
Republicans and Democrats and the vote on the debt ceiling
because we got a message and we're gonna go through it.
All the debt ceiling showed me is that
Dems will vote to cut or hinder good programs.
They're the same as Republicans.
They voted to make it harder for the poor and voted to cut money for the IRS to go after
billionaires.
Why would they give Republicans the votes?
Because they're the same.
They've got the same garbage policies.
I'm writing this to you in hopes that you make one of those videos where you convince
the person they're wrong.
I don't want to feel this way, but right now the memes seem to be right.
Two sides of the same coin, two wings of the same bird.
I wish somebody could tell me I'm wrong, but I just don't see the difference.
Okay, so let's start with this.
You support democratic policies.
You said so in your message.
I mean, not using those words, but you described them as good programs.
The programs that you are upset about being cut, how did they get there to begin with?
The Democratic Party.
And I know somebody's going to say, well, then they always roll it back.
But that's not really what happens.
So let's do it this way.
Let's say the Democratic Party came out six months or a year ago and said, hey, we just
voted to give the IRS $78 billion to go after billionaires.
Would you say, then, you should have given them 80?
Of course not, right?
But they gave them 80, two got chipped away, and now they don't have any good programs.
Not really how it works, right?
You actually support what they do, what you don't like is the back and forth of politics,
which, I mean, that I can understand.
That's one of the issues with a representative democracy.
It doesn't always move in the direction of progress.
Let's see.
Why would they give Republicans the votes?
Those poor people you care about, right?
Make it harder for the poor.
You don't like that.
What do you think happens if we default?
A whole lot worse, a whole lot.
It's not even remotely the same.
Again, the change is annoying, not good.
It's going to be a pain, absolutely.
It's also worth noting something I didn't mention
in the earlier videos is that they're on a timer.
Those laws expire.
The work requirements, they go away.
It's a Sunset program, but the program was expanded.
More people are going to get benefits.
That's with them voting for it.
If they didn't provide the votes, the US defaults.
None of those programs exist.
There's more to it.
It's not that you're upset with the Democratic Party.
You're upset with the game, which, believe me,
I really do, I get that.
Two sides of the same coin, two wings of the same bird.
I wish somebody could tell me I'm wrong,
but I just don't see a difference.
You're asking the wrong people then.
Ask any woman in the South, in a red state,
who wants to engage in family planning.
Ask the parent of a trans kid.
there's a whole bunch of people you can ask
and they'll tell you
that they're not the same.
You want the tax man and you want him to go after billionaires. I got you, I
understand, believe me.
I looked at your profile.
You are definitely to the left of the Democratic Party.
The society you want,
the revolution
you want,
you're not going to vote that in.
got to build that from the ground up. It takes a long time, a lot of work from a
whole bunch of different people. You are talking about fundamentally altering the
fabric of society. You say you want a revolution, you got to work for it.
The thing about the two sides of the same coin, if I was somebody and my goal
was to depress voter turn out for a political party. You know the number one
thing I would be trying to do? Convince people that there was no difference.
Convince them that there's no difference. They're going to do the same thing so
there's no reason to show up. You notice the Republican Party they don't have
that idea. They don't have people aligned with them that say, oh, there's no
difference. No, anything the Democratic Party does is the end of the country.
They're gonna give kids lunch at school. That's it. China's taking over. I don't
think fear-mongering is a good thing, but I don't believe in both sides and
things that don't have two sides. A lot of people try to do that to increase
enthusiasm. There's no difference between the two and the hope is that they'll
move outside the Overton window to a different philosophy. I've actually never
seen that work. I've seen people try it for years but I've never seen it work.
What I've seen happen is the person become disaffected and not engage in
politics at all. That's what I've seen happen. The two sides of the same coin,
two wings of the same bird, I get it. You go high enough up, they are all friends,
it's a big club and we're not in it. Got it. But while you are hopefully working
to build that better society, there's a whole lot of harm reduction that can occur.
And it isn't going to happen through the Republican Party.
At the end of this, more people are going to have access to that program.
And $78 billion is going to be used to audit the billionaires.
That's the reality.
The back and forth, I got it.
Like it's annoying.
And you have to keep people engaged.
Because if you don't, the back is more than the forth.
Let's be real.
If the Democratic Party had the House, would we be having this conversation?
No.
There wouldn't have been any cuts.
Wouldn't have happened.
The thing that I find entertaining in a really bizarre way is when you really look at this
and you look at everything that was in that deal, the thing that is objectively moving
totally the wrong direction, not just chipping away at something that's headed the right
direction, is the mountain valley thing.
That approval.
That's the big issue, and that's not getting a lot of press.
And the reason is because it doesn't fit into predefined narratives, and everybody
knows that that's part of the game.
That's in there, so when it gets to the Senate, Manchin, and the other people who are very
invested in dirty energy, well, they're going to vote for it.
People understand that part as it just being part of the game.
That's why that's in there.
All of this is part of the game, the political game that has very real impacts on people's
lives.
Even if you are somebody who just absolutely cannot fathom supporting the structure that
we're in, this system, that's fine.
Ask yourself this, would you rather have the Democratic party as your opposition or the
Republican party?
Would you rather have somebody who is fighting to go the exact opposite way and take away
rights from people, and really kick down at the poor as much as possible, and
really help the billionaires as much as possible, and get rid of any semblance
of representation, or would you rather be fighting against a group that is just
really bad at getting to where you want to go. You can choose your opposition. If
you are that far outside the Overton window, you can look at it that way. The
idea that they're the same, that is coming from a position with a lot of
privilege because there are people all over this country who will flat out tell
you, the Republican Party is trying to erase me. They are trying to take away
my rights. They are trying to make it to where I can't vote. They're trying to
kick me out of the country because of something my parents did.
There's definitely a difference. May not be as big as you want it to be, but what
you want based on your profile, you're never getting that through voting in a
two-party system. You got to build it. You have to build it and attract people
to it. The infrastructure for what you want doesn't exist. You have to build it.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}