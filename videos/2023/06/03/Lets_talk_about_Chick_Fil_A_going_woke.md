---
title: Let's talk about Chick Fil A going woke....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_wjuUO3tnsw) |
| Published | 2023/06/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chick-fil-A is being called "woke" because of their Diversity, Equity, and Inclusion (DEI) section, which has been in place for years.
- Right-wing commentators are riling up their audience by labeling companies as "woke" to generate clicks and views.
- DEI is not a symbol of being "woke" but rather a part of capitalism to widen a company's market.
- Beau personally chooses not to support Chick-fil-A due to past donations, but acknowledges the company's attempt to appeal to a broader audience through DEI.
- The outrage over Chick-fil-A being labeled as "woke" is fueled by buzzwords and manipulated by commentators for profit.
- Beau challenges the idea that having a DEI office goes against Christianity and urges viewers not to be misled by manufactured outrage.
- The focus on Chick-fil-A's DEI office is a distraction from the reality of capitalism driving businesses to expand their consumer base.
- Being woke and aware of social injustices should be seen as positive, and companies like Chick-fil-A implementing DEI initiatives should be appreciated.
- Despite establishing a DEI office, Chick-fil-A is not likely to actively participate in events like sponsoring pride parades.
- The narrative around Chick-fil-A's "wokeness" is a ploy to keep viewers engaged for ad revenue, ultimately benefiting the commentators.

### Quotes

- "DEI isn't woke. It's capitalism."
- "Woke is good, actually. Being alert to injustice is good."
- "Chick-fil-A establishing a DEI office. Yeah, I mean, that's cool."
- "They're playing you."
- "Your commentators, they're playing you."

### Oneliner

Chick-fil-A's "wokeness" is a marketing strategy, not a political statement, revealing the manipulation behind outrage-driven narratives.

### Audience

Conservative viewers

### On-the-ground actions from transcript

- Question manufactured outrage and look beyond buzzwords (implied)
- Support companies implementing Diversity, Equity, and Inclusion initiatives (implied)
- Challenge the profit-driven manipulation by commentators (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of how companies navigate capitalism and social issues, urging critical thinking and awareness of media manipulation.

### Tags

#ChickfilA #Wokeness #Capitalism #DEI #MediaManipulation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we're going to talk about Chick-fil-A going woke
because that's a thing that people are saying
is happening now.
Conservatives, please, please watch this video
because this is very much a teachable moment.
Okay, we'll start at the beginning
for people who are not familiar with this company.
This is a fast food joint here in the US
that is super Christian, super Christian.
I don't even think they open on Sunday.
It is a company that has a less than stellar reputation when
it comes to donations that they have
given to various organizations.
Right now, a number of right-wing commentators
have realized they can get a whole bunch of clicks
by gathering a group of middle-aged, middle-income,
middle-Americans and telling them which company to hate.
Which company has suddenly gone woke?
Somehow Chick-fil-A has started to surface in these conversations because they have a DEI
section, Diversity, Equity, and Inclusion. They've had it for years.
DEI is very much a buzzword in the right-wing community like CRT. It's something that
many commentators have told their viewers or their listeners that this is
super bad and they've never explained why but if you have this you're woke and
therefore conservatives shouldn't shop with you. Okay. DEI isn't woke. It's
capitalism. It's capitalism. Conservatives, pay attention. What you're calling woke
is just capitalism. They're trying to widen their market. That's it. All the
companies you're going after, that's what it's about. It's about capitalism.
Nothing more. To be clear, I still don't eat Chick-fil-A because of the donations
that they gave in the past. They're not a woke company. If you want to boycott
that's fine. I have no issue with that. But this seems like a moment where
something might get through. Your commentators, they're playing you. They've
given you buzzwords to hate and they use that outrage to get clicks, to get views,
to get ad revenue. That's what it's about. Chick-fil-A has not gone woke and I
would, I personally would question what about anything that revolves around a
DEI office is somehow opposed to Christianity. You're being played. You're
being played. Like I said, I have no stake in this. I, because I actually kind of
lean to the woke side of things, I don't eat their products, okay? But this might
Might be the moment you can realize a company, a traditionally conservative company, a super
Christian company, has now been labeled woke because it is engaging in capitalism.
Because there's a buzzword that you have been taught to fear.
Just because it makes somebody money.
If you view Chick-fil-A through any objective lens, there's no way you can view it as
woke.
It's not.
So what does that tell you about a DEI office?
Chick-fil-A has one.
Tells you that that's not really indicative of it being a progressive organization, right?
It's just capitalism, and the people who told you otherwise are farming you for clicks.
They get you mad, they get you angry, they get you tuned in, you watch the ads, they
make money.
It's that simple.
That's what's going on.
Woke is good, actually.
Being alert to injustice is good.
Chick-fil-A establishing a DEI office.
Yeah, I mean, that's cool.
But I mean, it's not like Chick-fil-A is going to be out
there sponsoring a pride parade this month.
They're playing you.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}