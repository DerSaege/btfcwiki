---
title: Let's talk about Biden, student loan forgiveness, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=okrXz2La73Q) |
| Published | 2023/06/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The House passed a resolution to cancel the student loan forgiveness program, which then went to the Senate where it passed with the support of key figures like Manchin, Tester, and Sinema.
- Biden is likely to veto the resolution, as it is expected to reach the Supreme Court soon.
- The move by Republicans to push this resolution seems questionable from a political standpoint, as Biden has little to lose by vetoing it.
- The resolution appears to be a half-hearted attempt to challenge Biden's policies, possibly for future campaign ads.
- There is anticipation around the Supreme Court's ruling on this issue, making the Senate's passing of the resolution seem almost moot.
- Beau speculates that Biden will likely sign and veto the debt ceiling issue simultaneously, with no grand ceremony expected for the veto.
- Overall, Beau views this resolution as another waste of time in the political arena.

### Quotes

- "The House passed a resolution to cancel the student loan forgiveness program."
- "Biden is likely to veto the resolution, as it is expected to reach the Supreme Court soon."
- "It's just another waste of time."

### Oneliner

Beau shares insights on the House passing a resolution to cancel student loan forgiveness, Senate's support, and Biden's expected veto, calling it a waste of time.

### Audience

Voters, Biden supporters

### On-the-ground actions from transcript

- Stay informed on the developments related to student loan forgiveness and share accurate information with others (implied).
- Advocate for policies that support student loan forgiveness and affordable education (implied).

### Whats missing in summary

Insights on potential impacts of the Supreme Court ruling and the significance of student loan forgiveness for individuals and the economy.

### Tags

#StudentLoanForgiveness #Biden #Senate #Politics #SupremeCourt


## Transcript
Well, howdy there internet people, let's vote again.
So today we are going to talk about Biden
and student loan forgiveness
as opposed to the student loan repayment pause
and what's going on in the Senate.
Okay, so the house with, I wanna say 218 votes,
passed a resolution saying,
hey, cancel the student loan forgiveness program that you had, okay, went to the Senate.
The usual suspects, Manchin and Tester, along with Sinema, who's now an independent, crossed
over and it passed in the Senate.
So now it goes to Biden.
What's going to happen?
Biden is probably going to veto it.
I don't really foresee this moving forward. It's worth noting this is already like before the
Supreme Court, like this has already gone through a whole lot of process. I'm curious as to why the
resolution was put forward to begin with. It doesn't even seem like a good move from the
Republicans for just a political capital standpoint because Biden realistically has nothing to
to lose by vetoing it. The ruling from the court is expected like any time now, sometime
in the next month. So them passing it, it seems almost moot. It doesn't make a whole
lot of sense, but hey, they did it. So it's going to be vetoed almost certainly, and we're
all going to wait to find out what the Supreme Court has in store. Again, this is for the
forgiveness, not for the repayment pause. It's probably just another kind of half-hearted
attack on Biden's key policies in an attempt to maybe create campaign ads
later? I mean they can't... I doubt the people in the House actually expected it
to get through the Senate. It seems unlikely that they expect Biden to sign
it, especially when we are so close to a resolution from the Supreme Court. So
So that's the other news that's going on up on Capitol Hill in addition to the death
ceiling stuff.
I would imagine that he will probably sign and veto the death ceiling and this at the
same time.
I wouldn't expect a giant ceremony or any kind of real press about him vetoing this
because it just seems kind of pointless, to be honest.
It's just another waste of time.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}