---
title: Let's talk about Montana and constitutional case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=z0f_jCK7NnM) |
| Published | 2023/06/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about a case in Montana called Held vs. Montana where 16 kids are suing the state for supporting dirty energy and contributing to climate change.
- Montana's constitution guarantees the right to a clean and healthful environment since 1972, which the plaintiffs believe the state is not upholding.
- The state's defense seems to be downplaying their contribution to climate change by arguing it's a global issue and their impact is minimal.
- Beau compares this defense to denying other constitutional rights like free speech or due process based on global standards.
- The case has entered trial, and Beau anticipates a lengthy legal process before a resolution is reached.
- Despite potential appeals and a prolonged legal battle, Beau sees the case as a significant step towards holding the government accountable for environmental protection.
- Beau believes that protecting the environment should be a top priority for the government, falling under promoting the general welfare.
- He questions the value of a massive defense budget when the country's own actions are harming it.

### Quotes

- "You're going to have a government that has the authority that the one we have does, I think that protecting the environment should be pretty high upon that list."
- "It doesn't really do a whole lot of good to have an 800 billion dollar a year defense budget if you're destroying the country yourself."

### Oneliner

Kids in Montana sue the state for supporting dirty energy, questioning the government's role in protecting the environment and promoting general welfare.

### Audience

Environmental activists, advocates

### On-the-ground actions from transcript

- Support environmental organizations in Montana to amplify the message of protecting the environment (implied).
- Join local climate action groups to advocate for sustainable practices in your community (implied).

### Whats missing in summary

The full transcript provides detailed insights into a legal case challenging the government's environmental responsibilities and the importance of holding authorities accountable for protecting the environment.


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Montana and the climate
and a case that is moving forward,
entering an incredibly important phase,
a phase that most people associate with the end
of things like this, but realistically in this case,
it's gonna go on and on for years
because it's very unique.
The name of the case is Held vs. Montana.
We've talked about it on the channel before.
But as a refresher, it was first brought back in 2020, I think, by 16 people who were kids.
I want to say aged 5 to 22, something like that.
And they're suing the state because the state is supporting dirty energy, to keep it simple.
The state is engaging in activities that are bad for the environment, that contribute to
climate change.
They're suing the state to stop it.
Seems odd, right?
Doesn't seem like something that should happen.
But Montana has something very unique in its constitution.
since 1972 if you live in the state of Montana you kind of have a right to a
clean and healthful environment. It's in their Constitution and that the state is
supposed to kind of maintain that. The plaintiffs are saying the state's not
living up to end to the bargain. It's gonna be hard to argue against that. In
In fact, it doesn't seem like the attorney general and the lawyers for the
state intend on arguing against that.
Instead, it appears that their defense
is going to be that, well, yeah,
Montana's doing that, but it's only contributing a little bit.
It's only hurting a little bit and climate change
is a global issue. Therefore,
it doesn't matter. Try that with something else.
Free speech. You have a right to free speech.
But there are other countries that don't have that.
So if we don't give it to you here, since it is a global issue, I guess it
doesn't matter.
What about due process? You know, other countries, they don't have due
process and that's a global issue.
So I guess it doesn't matter if we infringe on your due process just a
little bit, right? Yeah, I don't think that's gonna hold up, but that appears to
be the route they're going. This case has now made it to trial. So we'll have to
wait and see. I will definitely be watching this. I know there are other
important legal proceedings going on at the moment, but this is one I've been
watching for a while. I think the most likely outcome here is that whatever
the outcome is at the end of the trial, it's going to be appealed. So you're
looking at a probably another three to five years before this actually
gets resolved, but you now have a trial occurring in the United States where
Where the summary of it is that the state is not doing enough to mitigate climate change.
They're not doing enough to protect the environment.
I can only see that as a good thing.
you're going to have a government that has the authority that the one we have
does, I think that you know protecting the environment should be pretty high
upon that list and for those who would say that's not the government's job, I
would suggest that that falls under promoting the general welfare, you know
clean and helpful environment. I would also suggest that it doesn't really do a
whole lot of good to have an 800 billion dollar a year defense budget if you're
destroying the country yourself.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}