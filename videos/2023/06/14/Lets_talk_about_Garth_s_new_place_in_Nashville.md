---
title: Let's talk about Garth's new place in Nashville....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2bTd_79ZH1M) |
| Published | 2023/06/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Taking a break from national news to address a question about Garth Brooks' new place in Nashville.
- Clarifying that Garth Brooks is not opening a gay bar but a place where good manners are expected.
- Brooks wants the place to be safe and where people love and respect one another.
- Despite being in Tennessee, there seems to be some resistance to this concept.
- Brooks mentioned serving every brand of beer and promoting love and respect for one another.
- He doesn't seem concerned about how this might affect his brand, as he has always been advocating for inclusivity.
- Brooks has a history of being woke and advocating for equality, even before it became a popular term.
- He believes that as society progresses, the majority accepting stance outweighs the bigoted views.
- Brooks isn't fazed by potential backlash from those who do not share his values.
- His actions are seen as good capitalism as he appeals to a growing demographic that values kindness and respect.

### Quotes

- "If you are let into our house, love one another. Again, love your neighbor."
- "30 years this man has been telling you who he is. He is super woke."
- "As society moves forward, the population of people that are accepting far outweigh those that are bigoted."
- "He is one of the biggest brands in country music."
- "I don't think he needs assistance from people who outrage farm on Twitter."

### Oneliner

Beau clarifies Garth Brooks isn't opening a gay bar but a place promoting good manners and respect, standing by his inclusive brand amidst potential backlash.

### Audience

Country music fans

### On-the-ground actions from transcript

- Support inclusive businesses like Garth Brooks' new place (exemplified)
- Promote love and respect in your community (exemplified)

### Whats missing in summary

The full transcript provides a detailed look at Garth Brooks' inclusive values and the potential impact on his brand amidst societal progress.

### Tags

#GarthBrooks #Inclusivity #CountryMusic #Tolerance #Respect


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we're going to take a break
from all of the national news that's going on
and answer a question about Garth Brooks' new place
opening up in Nashville.
This is a topic that I have been over before,
but this question and the way it was phrased
was just way too good to pass up.
I will have a video down below that y'all might find interesting.
Okay, so here's the question.
I know Garth Brooks is kind of liberal, but you always say going woke is really just capitalism.
Please explain to me how his new gay bar is good capitalism in Nashville.
He's just walking into a firestorm for no reason.
How is this good for his brand?
Okay, first to clarify something, it's not a gay bar.
He's not opening up a gay bar, that's not what's happening.
He's just opening up a place where he expects people to have manners.
That's it.
He wants people to have good manners.
His words, I want it to be a place you feel safe in.
I want it to be a place where you feel like there are manners and
like one another. Love your neighbor. That's all he's asking. Seems like that
should go over in Tennessee, in Nashville, right? Except among a certain group.
Okay. He also said, and this is what drove this question and sent everybody out
calling him woke, and yes we're going to serve every brand of beer. We just are.
It's not our decision to make. Our thing is this, if you are let into our house,
love one another. Again, love your neighbor. It seems like a pretty simple
request. Seems like something that should be easy to honor in the very Christian
state of Tennessee. And he also said that it, I mean basically, if you didn't like
it well there's plenty of other places on lower Broadway and you can go
somewhere else. Yeah that's that's Garth Brooks. Okay how's it bad for his brand
or how's it good for his brand? He's Garth Brooks. He doesn't need to worry
about his brand. Aside from that this has always been his brand. Always. Always.
He's just walking into a firestorm
for no reason. I mean I get it. He does. He risks the tables being turned.
I get it. Because there might be
a very, very small group of people that can make a lot of noise
and he ends up getting burned. I get it.
It could happen. And
on some note, he's not walking into a firestorm.
I mean, he's straight up dancing in it, right? I mean, he's picking the one spot where there
very well might be some pushback. Why would he do that? Because that's who he's always
been. He's probably unable to resist the slightest chance that might exist and on that forsake
at all. He probably feels like life is not tried, it is merely survived if you're
standing outside the fire. 30 years this man has been telling you who he is. He
is super woke. He was woke before that was a term that was used in that way. He
was an ally before it was used in that way. This man advocated for marriage
equality in country music in the 90s. 30 years he's been telling you who he is.
And every once in a while somebody wants to provoke some outrage for clicks and
they throw it out there that he's gone woke. This is the second video I've made
talking about him suddenly going woke. He's always been like this, always. And as
far as his brand, he is Garth Brooks. If a whole bunch of bigots don't want to
come to his place, I promise you he's not gonna lose any sleep over it. It's good
capitalism because as, did y'all hear the thunder roll, the, as society moves
forward the population of people that are accepting, they far outweigh those
that are bigoted. Those people who would avoid his place because of the type of
drinks they serve or that he wants everybody to be nice to each other, that
demographic is shrinking. Those people who want people to be nice to each other,
those people who espouse that Christian value of love your neighbor even though
they may not be Christian, that's a growing demographic. It's a growing
market. He is one of the biggest brands in country music. I don't think that he
needs assistance from people who outrage farm on Twitter. I don't think he needs
that advice. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}