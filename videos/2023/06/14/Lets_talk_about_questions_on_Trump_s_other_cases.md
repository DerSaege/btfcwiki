---
title: Let's talk about questions on Trump's other cases....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ooFG63lVTZM) |
| Published | 2023/06/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains that Trump's arraignment in a documents case does not mean the federal inquiry into the January 6th issue will stop.
- Notes that two people involved in the fake electors scheme appeared before the January 6th grand jury on the same day Trump was arraigned.
- Clarifies that adjourned in the context of state cases does not mean dismissed but rather paused.
- States that adjourning the state-level proceedings due to federal scheduling conflicts means Trump's time facing criminal charges is extended.
- Emphasizes that once the federal case is over, the state cases will resume, leading to a prolonged period of Trump dealing with criminal charges.

### Quotes

- "Adjourned does not mean dismissed."
- "Words have definitions."
- "That is absolutely not good news."
- "Once the federal case is over, the state cases resume."
- "It's just a pause."

### Oneliner

Beau clarifies misconceptions surrounding Trump's arraignment and explains why adjournment of state cases is bad news for him, stressing that adjournment means a pause, not dismissal.

### Audience

Legal observers, political analysts.

### On-the-ground actions from transcript

- Stay informed on the legal proceedings involving Trump to understand the implications for his potential criminal charges (implied).

### Whats missing in summary

Detailed breakdown of the potential impact on Trump and the Republican Party due to the adjournment of state cases. 

### Tags

#Trump #Arraignment #LegalProceedings #Misconceptions #Adjournment


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about two questions
that have surfaced in the aftermath of Trump's arraignment.
And we're going to talk about the answers to those questions
and what they mean and what some words mean
because some of them are being misunderstood.
Okay, so the first question,
does Trump's arraignment in the documents case
mean that the federal inquiry into the January 6th issue is going to stop.
No, no, no. In fact, today, the day Trump was arraigned, two people involved in the
fake electors thing, they appeared before the January 6th grand jury, the
federal grand jury up in DC. That would be the Nevada GOP chair and vice chair.
One of them, when asked how they felt about appearing before the grand jury the
same time as Trump being arraigned, they said it was not on their bucket list. So
no, that will continue to move ahead. That would be an entirely separate
indictment, assuming one is to come. It's a different case. Now another piece of
news that is coming out that is giving a lot of people on the right wing those who are supportive
of Trump. It's giving them a lot of encouragement needs to be addressed because apparently they
don't understand what a word means. Letitia James indicated that these state cases, New York and
Georgia, well, they may be adjourned because of the federal proceedings.
Adjourned does not mean dismissed.
Those are not the same word.
Adjourned is more like paused.
This is not good news for Trump.
This is actually really, really bad news for Trump and the Republican Party in general.
They might, and again, it's worth noting that this is something that could happen,
something that at this point has been decided, they might need to adjourn the
the state-level proceedings because of scheduling conflicts with the federal
proceedings which will take precedence here. Yeah, all that means is that once
the federal case is over, the state cases resume. That doesn't mean they
go away. It means that the time that Trump is going to be facing criminal
charges has been extended and in all likelihood if they do adjourn the cases
pending the federal outcome it seems incredibly likely that not through the
primary season but through the general season the former president will be busy
with with criminal charges. That is absolutely not good news. Words have
definitions.
Adjourned does not mean dismissed. It's just a pause. So that's what's happening
with some of the other cases. This would apply to the adjournment would apply to
to the civil and criminal case in New York and a potential criminal case in Georgia.
The federal proceedings, as far as the grand jury, those will continue to go ahead.
Now if they were to return an indictment say tomorrow, they may not immediately move forward
with that either. That may be paused until there is a resolution in the case dealing
with the documents.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}