---
title: Let's talk about the $6.2 billion error....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ii0WKJH5ky8) |
| Published | 2023/06/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a $6.2 billion accounting error made by the Pentagon in relation to money given to Ukraine.
- Clarifies that the Pentagon doesn't hand cash to Ukraine but provides equipment.
- Mentions the concept of depreciation and how it affected the accounting error.
- Gives a detailed example of vehicle depreciation to illustrate the issue.
- Points out that the equipment provided to Ukraine is not brand new but includes older stock.
- Suggests visiting GovPlanet, an auction website, to see military Humvees for sale at low prices.
- Notes that the missing $6.2 billion is not where all the money that disappears from the Pentagon goes.
- Emphasizes that $6.2 billion is significant to normal people but a small fraction of the Department of Defense's budget.
- Acknowledges the potential for audits of the Pentagon's finances but indicates that the accounting error is not the primary concern.

### Quotes

- "To us, $6.2 billion is a lot of money. To the Department of Defense, that's a rounding error."
- "If you are one of those people calling for an audit of the Pentagon's finances, oh no, you're still good."
- "The military did not account for depreciation."
- "The equipment provided to Ukraine was not made this year."

### Oneliner

Beau breaks down a $6.2 billion Pentagon accounting error regarding equipment for Ukraine, revealing the impact of depreciation and the insignificance of the sum in the defense budget.

### Audience

Budget analysts

### On-the-ground actions from transcript

- Visit GovPlanet to see military equipment for sale (suggested)
- Conduct audits of Pentagon finances (exemplified)

### Whats missing in summary

The full transcript provides a detailed breakdown of the accounting error, depreciation impact, and insignificance of $6.2 billion in the Pentagon's budget.

### Tags

#Pentagon #AccountingError #Depreciation #MilitaryEquipment #Audit


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about an accounting error.
$6.2 billion.
And we're going to talk about that error, how that occurred.
We're going to talk about a contextual error,
because it is not being explained well.
And it has led people to believe that there's
like $6.2 billion in cash that was somehow missing.
And we're going to show where that money went
and how it disappeared.
And you'll get to see it in real life.
I can show you where you can witness this occur for yourself.
OK, so if you have no idea what I'm talking about,
the Pentagon said that it made a $6.2 billion
accounting error when it comes to the amount of money it has given Ukraine.
And that's the way it's being said, the amount of money.
The Pentagon's not actually handing wads of cash to Ukraine.
That's not what's occurring.
They're giving them equipment.
So did they lose $6.2 billion worth of equipment?
but they lost it a long time ago. Okay, I want you to picture you buying a $60,000
suburban, brand new, okay? When you drive it off the lot, you drive it from the
dealership to your house, your $60,000 suburban is now worth $54,000.
Generally speaking, you lose between 9% and 11% the moment you drive a brand new vehicle
off the lot.
Military vehicles are the same way.
You then lose basically 20% the first year, and then 15% to 25% every year thereafter
for the first five years.
So the value drops pretty quickly.
$60,000 suburban, drops to $54,000 on the drive home, $48,000 after the first year,
$41,000 after the second year, $35,000 after the third year, and so on and so forth. I shouldn't
have used a suburban for this. They actually hold their value pretty well, but the point remains.
Okay, so that's where the money went. The military did not account for
depreciation. The equipment that the United States is providing to Ukraine
was not made this year. These vehicles are not brand new. A lot of them are old
stock. Some of them are things the US wouldn't call them obsolete, but they're
definitely not top tier. So this money was figured by initially saying this is
what we bought this thing for and we gave it to them. Taking into account the
depreciation erases 6.2 billion dollars. You want to see this in real life.
There's a website called GovPlanet. It's an auction website. If you go to it and
scroll down you're gonna see a button that says Humvees, Military Humvees for
sale that you could buy if you wanted. Click on it. Right now in Pennsylvania
you will see a bunch of Humvees that are about $3,500 a piece. $3,500. The original
purchase cost for a base Humvee is about $70,000. Up armored ones, closer to a
quarter million, they're about $220,000. The ones that I saw that are up in
Pennsylvania, they're all base models. They look to be in pretty good shape, but
they are now $3,500 and I want to say they're from 2009. Before anybody goes
and you know says hey I want to um V please keep in mind that you know PFC
Smith drove that thing like he stalled it. So there there will be repairs
necessary. But that is where that money went to. Now, this leads into the next
question. Is this where all of the money that disappears from the Pentagon goes?
No. No. If you are one of those people calling for an audit of the Pentagon's
finances, oh no, you're still good. You are still good. There is definitely
justification for that. It's just not this part. Please keep in mind, $6.2
billion to us, to like normal people, that's a lot of money. To the Department
of Defense, that's a rounding error. That's less than what, 1% of
their yearly budget. Sounds like a lot. It is a lot, and a lot of good could be
done with that money. But when you are talking about it in relationship to
just the Leviathan that is DOD's budget, it's nothing. It's not a lot of money.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}