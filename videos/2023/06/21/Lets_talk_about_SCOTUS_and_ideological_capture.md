---
title: Let's talk about SCOTUS and ideological capture....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rhNyNYvWXZg) |
| Published | 2023/06/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Many doubted the Supreme Court's conservative reputation after some surprise decisions.
- Justices on the far-right wing have been writing dissenting opinions.
- There's a belief that the court may not be as overwhelmingly conservative as thought.
- The upcoming rulings on various issues will shed light on the court's true makeup.
- Speculation suggests the Republican Party may have focused too narrowly on certain issues in selecting justices.
- Beau advises cautious optimism until the court's decisions on contentious cases are revealed.

### Quotes

- "It's hopeful news. It's interesting news."
- "The Republican Party messed up with their Supreme Court selections."
- "We're gonna find out real quick."

### Oneliner

Many doubted the Supreme Court's conservative image, but upcoming rulings will reveal its true makeup, cautioning against premature assumptions.

### Audience

Court observers

### On-the-ground actions from transcript

- Stay informed on upcoming Supreme Court rulings (implied)

### Whats missing in summary

The full transcript provides a nuanced look at the shifting perceptions of the Supreme Court and the need to wait for upcoming rulings before drawing conclusions.


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we're gonna talk about the Supreme Court
and getting lucky, maybe?
And why a number of people are starting to question
their original assessments
of the makeup of the Supreme Court.
You know, it wasn't that long ago
when everybody was like,
the Supreme Court has been ideologically captured,
it is now a right-wing court, it's just going to be a rubber stamp for the Republican Party,
so on and so forth, and it really hasn't turned out that way.
People are pointing to a couple of surprise decisions and then they're pointing to the
fact that it's the conservative wing, not even the conservative wing, it's the far-right
wing of the Supreme Court that has to keep writing dissenting opinions.
The opinion comes down from the court and a justice has the ability to write a differing
opinion.
Generally speaking, those are people who are mad at the decision the court as a whole handed
down.
The people who have wrote the most have been Alito, Gorsuch, and Thomas with 10, 7, and
9.
So there is the idea that it's not a 6-3 court anymore.
That it's three liberals, three centrists, and three far right.
Is that true?
don't know, but will know soon. In a very short amount of time, a whole bunch of
opinions are going to come down from the court. Prior to them leaving for their
summer vacation, they will be handing down rulings on affirmative action, a
kind of an independent state legislature theory case, student debt, same-sex
marriage, so on and so forth. Let's wait and see how those go. I would rather be
surprised than get my hopes up, but there is the chance that the Republican Party
was hyper focused when choosing justices. They were hyper focused on
reproductive rights and gun control, and they wanted justices who would block
those two issues. Maybe they didn't look at everything else and those justices
may be more to the center than initially believed. Maybe. It's possible. I
understand where people are coming from when they say this. Like I get it. I see
the same thing, but I'm not willing to say that. I'm not willing to say that
until this next batch of rulings come down that are pretty contentious. Once we
see those, then we can talk about maybe it's a 3-3 and 3 court. Until then I
think everybody should act accordingly and just assume that there have been a
couple of wins for the liberal side, but the court is ideologically captured by
the far right. Until we see otherwise on big cases, that's what we have to assume.
I don't want to get my hopes up and then all of these decisions go the wrong way.
So, but it is, it's hopeful news. It's interesting news. It's a, it's a unique
take that is starting to gain ground. That basically the Republican Party
messed up with their Supreme Court selections. It's, it's not as wild as it
sounds because they really could have just been hyper focused on those two
issues and weren't paying attention to everything else but there's no need to
speculate a whole lot about it because we're gonna find out real quick. Anyway
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}