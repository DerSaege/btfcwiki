---
title: Let's talk about Trump and Meadows or Tom and Jerry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VzjZuVvqAkM) |
| Published | 2023/06/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's former chief of staff, Mark Meadows, is at the center of a cat-and-mouse game with Trump's inner circle.
- Trump's team investigated whether Meadows had flipped and was cooperating with federal authorities.
- Meadows has cut off communication with Trump, causing nervousness in the Trump community.
- Meadows' attorney made a statement suggesting Meadows is committed to telling the truth under legal obligation.
- Reports indicate Trump's team is now using a rat emoji to depict Meadows in their communication.
- There is no concrete evidence that Meadows has flipped and is cooperating.
- Severing ties with someone under multiple investigations, like Trump, is standard advice from attorneys.
- Cooperation could benefit Meadows, but there is no proof that he is providing incriminating information.
- Trump's concern about Meadows cooperating may intensify pressure from the Department of Justice on Meadows.
- The fact-finding mission conducted by Trump's team may have inadvertently signaled the importance of flipping Meadows to the Department of Justice.

### Quotes

- "They are so concerned about this, they're actually sending people out to try to figure out what's going on."
- "Y'all have a good day."

### Oneliner

Trump's team investigates if Mark Meadows has flipped, intensifying DOJ pressure and signaling potential cooperation, but solid proof remains elusive.

### Audience

Political analysts

### On-the-ground actions from transcript

- Follow legal proceedings related to Meadows (implied)

### Whats missing in summary

Insights on the potential legal implications and outcomes of Meadows cooperating fully with federal authorities.

### Tags

#MarkMeadows #Trump #Cooperation #DOJ #LegalImplications


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and his inner circle and his former chief of staff,
Mark Meadows and Tom and Jerry,
because there appears to be a cat and mouse game going on
between Meadows and Trump world.
Okay, so according to Rolling Stone, earlier this year,
Trump sent some of his lawyers and advisors on what they called a fact
finding mission. They were trying to figure out whether or not Meadows has
flipped, whether or not he is actively cooperating with the federal government.
It's worth remembering, I don't remember how long ago, maybe eight months, we
talked about it on the channel Meadows if you are trying to build a case or
multiple cases against Trump Meadows is a high priority to flip it would be very
important because of his position he had access to well everything now Meadows
has pretty much cut off communication with Trump and company. This of course
has made the Trump community nervous. Then a statement was made by Meadows
attorney without commenting on whether or not Mr. Meadows has testified before
the grand jury or in any other proceedings, Mr. Meadows has maintained a
commitment to tell the truth where he has a legal obligation to do so. It's
quite the statement. Now realistically that statement could mean I'm not saying
anything unless I get subpoenaed, but it could also mean that he's signed an
agreement, and that has created a legal obligation to say more. And Trump team
doesn't know which. It is being reported that they are now using a rat emoji in
conversation to depict Mark Meadows. So, do we know that Meadows has flipped? No. To
honest, severing ties with somebody under as many investigations as Trump is, is
advice that pretty much any attorney would provide to their client. I mean,
that seems like really good advice. So that alone doesn't say that he is
cooperating. His statement, the attorney's statement, doesn't actually say that he's
cooperating. We don't actually know that he is. Now, from Meadow's standpoint, there's a whole lot
be gained by cooperating. There really is. For him, he is in a position where the amount of
information that he is capable of providing might offer him a door out.
But there's no evidence that he's actually doing that.
Trump and his community being this concerned and that information coming to light, all
that is going to actually do is intensify any pressure that DOJ might have been putting
on Meadows, like assuming he hadn't flipped, DOJ is definitely going to be
increasing the pressure there because what this just told the Department of Justice is that Trump
is incredibly concerned that Meadows might cooperate.
I mean again you don't have to be Matlock to figure out that that might
indicate that Meadows knows a whole lot of incriminating information. The fact
finding mission was probably really ill-advised because all it did was tell
the Department of Justice, hey it's really important to flip Meadows. They
are so concerned about this, they're actually sending people out to try to
figure out what's going on. Not a wise move, but again that's assuming that
Meadows hadn't flipped. He very well might have, but we're gonna have to wait
and see. I have a feeling that that will be one of the last pieces of information
that we find out because he may be the testimony that ties a whole bunch of
things together. So they'll want to keep that as quiet for as long as possible,
as quiet as it can. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}