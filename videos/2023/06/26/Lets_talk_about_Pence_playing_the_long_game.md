---
title: Let's talk about Pence playing the long game....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9iYW-g12dLk) |
| Published | 2023/06/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques former Vice President Pence's campaign strategy for not capitalizing on a unique tool he possesses.
- Expresses dislike for Pence's policies and philosophy but acknowledges his actions on January 6th in not contesting the electors.
- Questions why Pence is not leveraging his role in protecting American democracy on January 6th in his campaign.
- Suggests two possible reasons for Pence's strategy: either he has undisclosed wrongdoing or he is playing a long game, waiting for Trump's downfall to claim credit for preserving democracy.
- Speculates that Pence might be waiting for certain proceedings related to January 6th to come to light before using this information in his campaign.
- Points out that Pence's campaign is currently inactive, not utilizing the potentially advantageous information he has.
- Raises the possibility that Pence's advisors are aware of the political value of his actions on January 6th and are strategically waiting to reveal it at a more opportune time.

### Quotes

- "I don't understand why he's not using that and this is the only reason I can come up with other than he's super guilty of something and we don't know it."
- "He was the one person who really stood in the way that day and he's not bringing it up."
- "I think Pence might be playing the long game here."
- "Politically, that's gold and they're not using it yet."
- "He's waiting for somebody else to do it and then he can come out and explain."

### Oneliner

Beau questions former Vice President Pence's campaign strategy and speculates if he is playing a long game by waiting for Trump's downfall to claim credit for preserving democracy.

### Audience

Campaign strategists, political analysts, supporters of democracy

### On-the-ground actions from transcript

- Analyze Pence's actions and statements critically to understand his political strategy (implied)

### Whats missing in summary

Insights on Pence's potential political strategy and the impact of waiting for a more opportune time to reveal significant information.

### Tags

#CampaignStrategy #Pence #PreservingDemocracy #PoliticalSpeculation #January6th


## Transcript
Well, howdy there internet people, Lidsbo again. So today we are going to talk
about former Vice President Pence
and his campaign strategy.
And why it doesn't make sense.
Pence has an amazing
campaign tool. He has something that nobody else has.
But he's not using it. And it's
odd. It's odd.
because he has good advisors. They obviously have to recognize the political
worth of this, but he hasn't brought that tool out of the toolbox.
Okay, so let me start by saying this. I don't like Pence. I really don't like
anything about him. I don't like his policies. I don't like his philosophy.
I actually don't have many nice things to say about the man.
That being said, by all evidence that we have seen, by all accounts that we've been able
to get access to, he was one of the few that had power that didn't trade his country for
red hat that didn't go along with the events of the 6th and he could have. Now I don't
think that, I don't believe that if he had rejected the electors that it would have really
altered the final outcome but I think it would have been much worse in that time when it's
being sorted out. But he didn't do that. He's the one person who really stood in
the way that day and he's not bringing it up. And I know many people would say
well he's not doing that because he wants to get the MAGA base. Yeah, I don't
think the people who were chanting what they were chanting that day are going to
vote for him no matter what. That seems super unlikely. And then there's this
other thing. Recently, you know, you've had all of the Republican presidential
hopefuls out there saying, you know, I would pardon Trump. Vote for me and I'll
I'll pardon Trump. I mean I don't know what
good that does politically. Anybody who thinks that's a good idea
they're going to vote for Trump. You're not getting any votes with that statement.
Pence was asked the same thing.
You know what he said? He said it was premature.
We don't even know if he's going to be convicted.
And that sounds incredibly supportive of Trump, but that's also a really good way
not to answer that question.
When you look at the evidence that's available from that day and you look at
his position, what he did, what everybody says he did, and you compare it to his
actions in his campaign and not bringing that up, you know, not being, I'm the person who
protected American democracy.
It seems odd.
You're really kind of only left with two options.
One is that he has his hands dirty somewhere and we don't know about it.
It's a possibility.
unlikely given his actions that day. But you always have to entertain the unknown
and so maybe there's something out there that just hasn't come to light yet. The
other option is that he's playing the long game. He is waiting for Trump to no
longer be in the race and then he will come out and be like, do you know who
actually saved the Republic. And I feel like he might be hoping for, I don't know,
maybe some proceeding somewhere to reveal the extent of alleged deception
by the former president. And I feel like he's hoping that once that comes to
light and he comes out and says, I'm the one that wouldn't sell you out. Look at
what's being discussed in this trial. Or he may be waiting for a criminal
proceeding to move forward so he doesn't have to say it. So he doesn't have to
say, I'm the one who did it. So he doesn't have to toot his own horn so it
just comes out in the trial and he looks really good and modest, which kind of
fits with his personality. I think Pence might be playing the long game here and
if there are proceedings about the sixth, I feel like that's when this information
is going to come out. That's when his campaign is really going to swing into
action because right now it's pretty mellow. They're not really doing
anything. All he's doing is treading water when he could be swinging. And he
has, he's got a weighted glove. He has something that he can use. But he
probably doesn't want to be the person to sink Trump. He's waiting for somebody
else to do it and then he can come out and explain, yeah, he was trying to
engage in preserving his power over the will of the people and me, I could not
bear that. I could not sell out the Republic. I think it's possible, I think
likely actually because his advisors know that that's gold. Politically that's
gold and they're not using it yet and it's not like they have any reason to
wait other than to let somebody else take him down and then to come out. But
it's just one of those things that has been bothering me because I don't
understand why he's not using that and this is the only reason I can come up
with other than he's super guilty of something and we don't know it. Anyway
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}