---
title: Let's talk about upcoming SCOTUS decisions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Wj2wynwft34) |
| Published | 2023/06/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of significant upcoming decisions from the Supreme Court before their break.
- The Moore v. Harper case involves the independent state legislature theory, potentially granting more power to state legislatures over elections.
- 303 Creative v. Alenis tackles whether artists' websites are considered public accommodations and subject to non-discrimination laws.
- Counterman v. Colorado revolves around a stalking case and the justices clarifying the threshold for threats under the First Amendment.
- Groff v. DeJoy is about religious accommodation for days off, impacting those needing specific days off for religious reasons.
- There are two affirmative action cases involving college admissions that may signal the end of affirmative action.
- Two student debt cases, Biden v. Nebraska and Department of Education v. Brown, could impact Biden's student debt relief plans.
- The Supreme Court will also announce its next docket during this period.

### Quotes

- "The politicians will be able to pick their voters."
- "I think the justices are going to say that affirmative action needs to go away in college admissions."
- "If either one of these goes against Biden, they're back to the drawing board on student debt relief."

### Oneliner

Beau outlines key Supreme Court decisions, from state legislature power to affirmative action's future, impacting elections, discrimination laws, and college admissions.

### Audience

Legal enthusiasts, activists

### On-the-ground actions from transcript

- Stay informed about the Supreme Court decisions and their implications (suggested).
- Join legal advocacy groups to stay updated and engaged with ongoing cases (exemplified).

### Whats missing in summary

Detailed analysis and deep dives into each case are missing in the summary.

### Tags

#SupremeCourt #LegalSystem #AffirmativeAction #Elections #DiscriminationLaws


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Supreme Court
and the decisions we are waiting on.
Traditionally, the court will be releasing
a whole bunch of decisions, well, now,
sometime very soon, before their break.
There are a number that are pretty important,
so what we're going to do is go through
briefly describe what the case is, the decision that people are waiting on, and
just kind of run over them. The reason we're gonna do this is because I got a
message from somebody saying, hey you know when these court cases come out
they all have these names and nobody actually knows what they are when the
news first breaks. So it would be nice to have a little bit of an understanding.
Okay, so the first one, Moore versus Harper. Moore v. Harper. This is the
independent state legislature theory case. If this goes the wrong way, there's
going to be a whole lot more power in the hands of state legislatures to fix
elections, to predetermine the outcome of elections. Rather than voters picking
picking their politicians, the politicians will be able to pick their
voters. There is one called 303 Creative versus Alenis, no connection. And this is
a weird one. This is, so it has to do with same-sex weddings, but at the heart of
it's whether or not artists' websites are a public accommodation and therefore
subject to non-discrimination laws. The argument is that by making them a public
accommodation it's a First Amendment violation because it's compelling the
artist to do something they don't want to do. We have counterman V Colorado. So
this is an appeal of a stalking case and its heart is also a First Amendment
thing and the justices will kind of have to decide where the where the carve out
for threats is. Is it the intent of the speaker or is it what a reasonable
person would believe? And that's what they're going to be kind of clarifying
on that one. You have Groff v. DeJoy. So this is a religious
accommodation one dealing with days off, it won't have like massive impacts
really with the exception of those people who for religious reasons need
specific days off. That's really what this is. Kind of a simple case. There are
two affirmative action cases when it comes to college admissions. It is
students for fair admissions the president and fellows of Harvard and then
students for fair admissions the the University of North Carolina. This I got
to be honest this one is probably I feel like the justices are going to say that
affirmative action needs to go away in college admissions. I think that's where this one's
headed. There are two student debt cases, Biden v. Nebraska and Department of Education
v. Brown. If either one of these goes against Biden, they're back to the drawing board on
student debt relief. So those are what I think are the big cases that people are
going to be waiting on. It's also worth noting that during the same time period
the court will announce its next docket and it'll let everybody know what else
they're going to be looking at in the next term. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}