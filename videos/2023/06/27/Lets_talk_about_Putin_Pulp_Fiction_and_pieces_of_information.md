---
title: Let's talk about Putin, Pulp Fiction, and pieces of information....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_eHnTG2NB60) |
| Published | 2023/06/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains how movies can teach us about processing information out of chronological order.
- Uses examples like "The Usual Suspects" and "Pulp Fiction" to illustrate the importance of critical pieces of information.
- Analyzes the Wagner coup attempt and how different pieces of information changed the narrative.
- Emphasizes the need to re-evaluate timelines and not base opinions on initial information.
- Warns against forming incorrect assessments by not considering all the pieces together.

### Quotes

- "Don't base your opinion on the first information you get."
- "Understand the order in which the story breaks is not necessarily the order in which it occurred."
- "If you don't plug that in and acknowledge that that changes the order of events and it changes the reason for Russia's response, your view of it is going to be off."

### Oneliner

Beau explains how movies like "The Usual Suspects" and "Pulp Fiction" teach us to re-evaluate timelines and not base opinions on initial information, using the Wagner coup attempt as an example.

### Audience

Information Consumers

### On-the-ground actions from transcript

- Re-evaluate timelines based on new information (implied)

### Whats missing in summary

Beau's engaging storytelling and detailed analysis.

### Tags

#InformationProcessing #CriticalThinking #WagnerCoupAttempt #ReevaluatingInformation #MoviesAndLearning


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about what movies,
movies can teach us about how to process information,
particularly information that comes
in out of chronological order.
Because we have a habit as humans
to take a piece of information
and use that as our basis,
the first piece of information we get,
and then everything else we try to fit it in.
That's not always the best way to do it.
Sometimes the timeline completely disrupts
whatever it is you believe happened
when you get all of the little pieces of information.
You ever see a movie that you don't get a critical piece
of information until the very end?
You know, a movie with a big twist at the end.
Think of The Usual Suspects.
The most important piece of information in that movie is not revealed until the final
few seconds.
What happens if you watch the movie but you got up five minutes before it ended?
You have a very different take on that film than everybody else because you're missing
a critical piece of information.
Or what about a movie where you see the same incident portrayed from different people's
perspectives?
Every new perspective gives you a little bit more context, makes you understand that incident
a little bit more clearly.
more related to what's happening, information that comes in out of order.
Think about Pulp Fiction.
Okay, so what we're going to do is we're going to go through the Wagner coup attempt thing,
and we're going to talk about it as the information came in, and then we're going to add the final
pieces of information to put everything in a context. Okay, so what did people
first learn? Well, for a lot of people the first thing they learned was that
Wagner was like doing a coup. A whole lot of people understood that they had been
hit by the regular army. Their camps had suffered a strike by the regular army
just before. So obviously that's the inciting incident. That's what started
it. They got hit, so they moved on. Maybe. Okay, as time goes on, you find out that
the regular army, that Russia, that Putin was planning on rolling Wagner into the
regular army. Okay, well that obviously upset them. Okay, then the coup starts,
they do their little march and then it stops and we get the public-facing deal
right and people are like well that's strange it doesn't make sense and that's
when a whole bunch of conspiracy theories start okay then we get some news
that isn't dramatic so it doesn't get coverage and people don't plug that
piece of information in. U.S. intelligence said that they were aware of
the uprising plan weeks in advance. What does that mean? The artillery strike is
not the inciting incident. The strike on the camps was not the inciting incident.
incident, the plan might have incited the strike, right?
That might have been Russia's response to finding out because while Russian intelligence
has definitely been lacking lately, their counterintelligence, oh, it is still top tier.
So Russian intelligence knew about it at least when the US did, maybe even before, probably
even before. So that accidental strike, well maybe that was on purpose. Maybe the
boss of Wagner was right because it was targeting him or his command or just
sending a message. Maybe the order to roll Wagner in to the regular army, that
was also because they found out they were planning an uprising. That little
bit of information changes what started it, the inciting incident. Now you have
to wonder why they were planning it, right? You can draw your own conclusions,
But from there, you follow the story along again. There's the strike, the
roll-in, the march, then they stop and change their mind. There is
information now suggesting that Russian counterintelligence, well, they knew where
the family members of all the leadership were, and that that might have been a
deciding factor in what the leadership decided to do. We also have access to
Wagner Telegram channels, their chat channels. They're not happy
about the fact that it stopped. They're really upset about the way everything is
shaping up. Now Putin gave another speech and he said that Wagner troops, well they
can be rolled into the regular military, they can sign a contract with the
Ministry of Defense, they can go to work for law enforcement, or they can just go
home. Okay. I understand that in Putin's mind it's not a good idea to have a
bunch of people together who were willing to coup you. I mean that tracks,
that totally makes sense. So it's not like they're gonna sign with the Ministry
defense and stay their own unit, odds are they will be separated out among the
regular military. Kind of break up their power structure. That's the way
they're looking at it. I'm not sure that it's a great idea for Russia to take a
whole bunch of people who know each other and who can still contact each
other through, I don't know, telegram channels and put them in a whole bunch of
different units among regular Russian troops, regular army, where the Wagner
troop will be the most experienced, the one that gets looked up to, the one that
the younger guys follow. Yeah, I got a feeling that's not gonna necessarily go
the way that that Putin wants it to. I think that may be another mistake and a
long chain of mistakes here, but we'll have to wait and see on that. The point
here is when you are getting information from the news. Understand the order in
which the story breaks is not necessarily the order in which it
occurred. Don't base your opinion on the first information you get. Anytime you get a new
piece of information, even if it doesn't seem dramatic, it's worth re-evaluating your timeline.
Because we knew about it weeks in advance, we knew it was going to happen, that's not
a big deal. US intelligence knew about it. If you don't plug that in and acknowledge
that that changes the order of events and it changes the reason for Russia's
response, your view of it is going to be off. If you don't really sit down
and think about all of the pieces together, you can come away with a very
incorrect assessment of what was happening. A good example,
Pulp Fiction, if you actually put all the pieces together and watch it,
it's not a movie about like criminals.
It's a movie about couples.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}