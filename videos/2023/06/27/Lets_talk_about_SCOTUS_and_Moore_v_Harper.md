---
title: Let's talk about SCOTUS and Moore v Harper....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=dJalbSwZNwU) |
| Published | 2023/06/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Supreme Court rejected independent state legislature theory, a case that could have jeopardized American democracy.
- State legislatures could have had unlimited authority to influence federal elections if the theory was upheld.
- Rejecting the theory ensures that state legislatures are not insulated from judicial oversight.
- Decision puts independent state legislature theory in critical condition, but doesn't completely eradicate it.
- The ruling is a significant win for democracy, but the issue isn't entirely resolved.
- Some Republicans have been supporting the theory, but it lacks enough traction to move forward.

### Quotes

- "It was more like this is moot, you know, we don't really need to deal with this right now."
- "So that is good news."
- "The Supreme Court made the right decision."

### Oneliner

The Supreme Court's rejection of the independent state legislature theory is a significant win for democracy, putting the theory in critical condition and safeguarding against potential threats to American democracy, although the issue remains unresolved.

### Audience

American citizens

### On-the-ground actions from transcript

- Stay informed about legal and political developments impacting democracy (implied)
- Support organizations advocating for fair elections and democratic processes (implied)

### Whats missing in summary

The full transcript provides detailed insights into the potential threat to American democracy and the significance of the Supreme Court's decision in safeguarding democratic processes.


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Morvey Harper
and the decision that came down from the Supreme Court.
Okay, so they rejected it.
They rejected independent state legislature theory.
Six to three, it was the usual suspects dissenting.
For those who need a recap, this was the case
that would really pave the way for state legislatures
to destroy American democracy.
There's really no other way to frame this.
It would give state legislatures just unlimited authority
outside of the scope of judicial review
to set the stage to make sure that they could determine
the outcome of federal elections.
It would allow them to reject anything they didn't like.
It would allow them to say that certain votes wouldn't count.
It would allow them to pick their voters rather than the voters pick the politicians.
Now there was a point in time in the United States where the idea of this would just be
laughable.
wouldn't have gotten anywhere. It certainly wouldn't have been something
that anybody was kind of apprehensive about a Supreme Court ruling on. At the
end, the majority of justices, they were basically like, hey, words have
definitions. And the way you're reading this clause of the Constitution, that's
That's not a normal reading.
You're adding stuff.
You are trying to subvert the republic through this means.
They weren't quite that direct with it, but that was definitely the tone.
The most important part.
The elections clause does not insulate state legislatures from the ordinary exercise of
state judicial review.
Okay, so does this ruling, does it just mean that independent state legislature theory
is dead?
No, it doesn't.
But they did kind of put it in critical condition today.
They really did.
So that is good news.
At the same time, this isn't over.
will have people who want to adopt this because over the last few years there
have been a number of corrupting influences within the Republican Party
that have led many Republicans to abandon the Republic. The Supreme Court
made the right decision. Even the dissents were pretty weak to be honest.
It was more like this is moot, you know, we don't really need to deal with this right now.
So while there was certainly some interest and support for this theory,
it doesn't look like it has enough traction anytime soon to go anywhere.
And again, that is really, really good news.
This is a case that when we first started covering it,
an existential threat to American democracy. So you have some good news
today. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}