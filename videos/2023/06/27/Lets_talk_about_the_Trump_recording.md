---
title: Let's talk about the Trump recording....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LgsnD1rUoHg) |
| Published | 2023/06/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Team Trump recently began receiving discovery related to a leaked audio recording involving Trump discussing a plan to attack Iran.
- The leaked audio combined with incriminating documents found at Trump's place seems to create a damaging case against him.
- The audio recording clearly shows Trump acknowledging the classified nature of the information discussed.
- Speculation arises about the motives behind leaking the audio, including potential ego-driven reasons or strategic moves for trial.
- Beau questions how Trump could defend himself given the damning nature of the audio and recovered documents.
- The possibility of the leak being orchestrated by someone within the Department of Justice is considered.
- The leaked audio's value in trial lies in its shocking nature, potentially making it harder for Trump to defend himself.

### Quotes

- "I personally don't know how you could create more damaging audio."
- "I have no idea how you're going to mount a defense now."
- "I have no idea what Trump intends to do."
- "I've got no idea where… Just those two things make it seem from my perspective to be pretty insurmountable."
- "It is definitely a witch hunt if Trump is one of the Sanderson sisters."

### Oneliner

Team Trump receives leaked audio discussing an Iran attack plan, creating a damaging case against him and raising questions on defense strategies.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Speculate on the potential motives behind the leaked audio (suggested)
- Analyze the implications of the leaked information in the context of national security (suggested)

### Whats missing in summary

Detailed analysis and context that can only be fully grasped by watching the full video.

### Tags

#Trump #LeakedAudio #IranAttack #DepartmentOfJustice #WitchHunt


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about audio
and what the country is going to hear.
Okay, so recently, Team Trump started getting discovery.
On Friday, I made a video saying,
hey, it's probably important to remember
that the Department of Justice has had
soul control over a lot of this evidence for all this time, and we didn't know anything
about it.
So if there starts to be a flurry of leaks, now that Team Trump is getting access to this
information, it's worth remembering.
Four days later.
Okay, so there was a leak.
And is the audio recording of the conversation that has been discussed in the media that
people knew about.
And that was the conversation with Trump and somebody else and they were talking about
what certainly sounds like a plan to attack Iran.
Okay, so I'm not going to go through the transcript because undoubtedly you are going to hear this audio.
Short version, I personally...
Short of actually saying the phrase,
Hey, look at this Iran invasion plan that I am willfully retaining in violation of 18-7-9-3.
I don't know how you could create more damaging audio. That audio combined with
you know the documents that were at his place, I have no idea how you're going to
mount a defense now. I have no idea what Trump intends to do. That's pretty
bulletproof. He makes it very clear that he understands he is showing something
that is currently classified, that it is definitely national defense information,
and that he has kept it. So there you go. Now people might be wondering why this
was leaked. There's a whole bunch of reasons. Now you do have to entertain the
possibility that it is somebody from DOJ that just waited until they had cover.
Okay, seems unlikely but it's possible. Another motivation for this would be
what it sounds like the motivation is for Trump in that video. Ego. Look at me.
Look how important I am. I have access to this. Do you? Of course not, peon.
That's what it seems like. So it could be that or it could be more calculated.
Somebody in Trump world realizing that the value of audio like that at trial is
is the shocking nature. If it is old news it might be less damaging. I don't know.
Realistically, I have no idea how you mount a defense with that audio and the documents
that were recovered there. I've got no idea where... Forget everything else. Just
those two things make it seem from my perspective to be pretty
insurmountable. It is definitely a witch hunt if Trump is one of the Sanderson
sisters. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}