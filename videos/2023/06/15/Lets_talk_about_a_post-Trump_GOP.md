---
title: Let's talk about a post-Trump GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ss-Z1Z3QKRE) |
| Published | 2023/06/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are imagining a post-Trump GOP as some loyalists pivot away from the former president.
- GOP operatives are suggesting that Trump may end up in prison, leading to pressure on major candidates to distance themselves from him.
- The shift within the Republican Party is slowly beginning, with more Republicans expected to abandon Trump.
- Candidates still need the MAGA base's support but also have to distance themselves from Trump to protect the party's image.
- To keep the MAGA base energized, candidates may heavily lean into the idea of pardoning Trump.
- The plan to keep control of the base involves promising to pardon Trump for any serious allegations or criminal actions.
- The GOP wants to maintain the MAGA movement without being overshadowed by specific personalities.
- Overall, there is a potential shift within the Republican Party away from Trump, with focus on keeping the base energized and united.

### Quotes

- "I cannot defend what's alleged. These are serious allegations."
- "It's happening. So at this point where we are is who's going to be the nominee."
- "There's nothing they can do about that because they don't have the centrist vote..."
- "But that leaves candidates in a bizarre position. They still need the MAGA base."
- "That's how they plan on keeping control of the MAGA base."

### Oneliner

Republicans are imagining a post-Trump GOP as loyalists pivot away, facing the challenge of keeping the MAGA base while distancing from Trump through potential pardons.

### Audience

GOP Members

### On-the-ground actions from transcript

- Pressure major candidates to distance themselves from Trump (implied)
- Support candidates who focus on maintaining the MAGA base without being overshadowed by specific personalities (implied)

### Whats missing in summary

Insights on the potential implications of a post-Trump GOP and the dynamics within the Republican Party.

### Tags

#GOP #Trump #MAGA #RepublicanParty #Pardons


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about imagining a post-Trump GOP because Republicans are starting
to do just that.
You're starting to get movement from those within the Republican Party who up until now
had been very loyal to the former president, who are starting to slowly pivot away.
have seen new criticism and new strength of criticism come from Scott, Barr, Pompeo,
Haley, even Pence. Pence said, I cannot defend what's alleged. These are serious
allegations. You also have reporting that says GOP operatives are saying things
like this. I don't know what's going to happen but the man is going to prison.
It's happening. So at this point where we are is who's going to be the nominee.
Donald Trump broke the law and frankly I'm not an ever Trumper. I'm really not
but this is just too much. The shift is beginning from with inside the
Republican Party. It'll probably be slow at first, but you're going to see more
and more Republicans start to abandon him. Those in political office
and those with political influence. The reporting suggests that major candidates,
other candidates, are already being pressured by their donors to distance
themselves and kind of push back hard on Trump because they're worried it is
going to damage the entire Republican Party as this moves forward. I can't say
that I think they're wrong. I think they're right. But that leaves candidates
in a bizarre position. They still need the MAGA base. They absolutely need it.
there's nothing they can do about that because they don't have the centrist
vote because of catching the car with Roe and all of the other things they've
done to alienate the voters that they need. For them to stand even a remote
chance they have to keep that MAGA base energized. So how can they do it?
Pardons. Pardons. They're going to probably start to lean real heavy into
idea that they would absolutely pardon Trump. So if you really love Trump, if
you want to help a dear leader, you need to elect me because I'll pardon him. I'll
get him out of jail for that thing that he did that was serious. That's where I
think they're probably headed. You've already had some talk about pardons but
I would expect that to ramp up. I would expect the criticism of Trump's actions
to kind of slowly walk forward from these are serious allegations. I can't
defend what's alleged to, you know, he's going to have to deal with what he did or
moving from it was reckless, which is a term that has been thrown out, to it was
criminal. But there's no reason dear leader should stay in prison, vote for me
and I'll pardon him. That's how they plan on keeping control of the MAGA base. And
I think that there's an element within the Republican Party that wants to see
MAGA as a movement make it, but without too many of the personalities associated
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}