---
title: Let's talk about Fox calling Biden a wannabe....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=SHkDzgS5b-E) |
| Published | 2023/06/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fox News labeled President Biden as a "wannabe dictator," sparking attention and controversy.
- Beau points out the contrast between Biden and the former president who attempted a self-coup and promised to strip people of citizenship through executive orders.
- Fox News' pattern of being inflammatory, misleading, and absurd is nothing new, according to Beau.
- Beau raises concerns about Fox News still having access to numerous military installations.
- Beau questions the consequences if a veteran on a military base distributes flyers undermining the chain of command.
- There's a push to remove Fox News from military installations to prevent morale issues and chain of command disruptions.
- Failure to address the situation may impact commanders' careers and be viewed negatively in the future.

### Quotes

- "Fox News labeled President Biden as a 'wannabe dictator,' sparking attention and controversy."
- "There's a push to remove Fox News from military installations to prevent morale issues and chain of command disruptions."
- "Failure to address the situation may impact commanders' careers and be viewed negatively in the future."

### Oneliner

Fox News sparks controversy by labeling President Biden while raising concerns about its presence in military installations.

### Audience

Military commanders

### On-the-ground actions from transcript

- Remove Fox News from military installations (suggested)
- Address morale issues caused by Fox News presence (implied)

### Whats missing in summary

The importance of addressing misinformation and maintaining military discipline is best understood by watching the full transcript. 

### Tags

#FoxNews #PresidentBiden #MilitaryInstallations #Misinformation #ChainOfCommand


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk a little bit about Fox
and their opinion of President Biden
and what they put out on that little tagline
below the footage.
It has caught a lot of people's attention.
There is certainly a whole bunch that can be said
about what was written there but realistically,
there's only two things that really should be addressed. The first is the
obvious normal rebuttal of this type of rhetoric. The second is a little bit more
important and it applies to a much smaller group of people but has much
further reaching effects. Okay, so if you have no idea what I'm talking about Fox
news, ran a little tagline referring to President Biden as a wannabe dictator.
Okay, sure, whatever. That's fine. They're allowed to have that opinion. I get it. I
would point out that he is not the person who attempted a self-coup. He
didn't do that. He didn't lose by millions of votes and then tried to
to subvert the will of the people and just basically stay in power because he
wanted to and felt entitled to that because nobody's ever told him no in his
entire life. That wasn't Biden. It also wasn't Biden that just recently promised
that if he was re-elected, the very first thing he would do, a day one
achievement would be to issue an executive order stripping people of
their citizenship. Something that was outlined in the Constitution. He's not
the person who believes that he can dictate changes to the Constitution.
Constitution. That's the other guy. That's Trump. He said that he was going to
alter the way citizenship was granted in defiance of the Constitution, day one, if
re-elected.
Okay, so you have that. That's the normal stuff. Foxes being fox, being inflammatory,
being absurd, honestly, and misleading their viewers. No big change there. But
here's something else. Fox is still on, still has access to a whole lot of
military installations. A whole bunch. Here's my question for every post
commander out there, for every commander over one of those facilities. Let's say
you have a veteran and that veteran uses their ID card to gain access and they go
to the PX and they stand there and they hand out flyers that undermine the chain
command. What happens? Where does that person do their shopping in the future?
Not on post, right? There has been a push to get Fox removed from military
installations. The situation is reaching a point where failure to act in that
regard because a whole lot of things are at commander's discretion does a whole
lot of lifting in the regulations. This is going to be one of those things that
years from now it's going to be viewed as you should have known better. When
you're going for your next star, it's going to matter. They're going to
correctly identify this as a morale issue. They're going to correctly identify this as
an attempt to undermine the chain of command. And if a high-ranking officer was aware of
it and did nothing, it's going to look bad on them. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}