---
title: Let's talk about the Biden tapes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hv7h80Guyxs) |
| Published | 2023/06/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the Biden recordings and the need to verify information.
- Acknowledging a message questioning why he hasn't covered the tapes.
- Expressing willingness to read a transcript if provided and if it's breaking news.
- Not having access to the tapes and questioning their existence.
- Explaining the FD 1023 form used to record unverified information about the tapes.
- Challenging the belief that people have actually listened to the tapes.
- Being open to covering the tapes if they are proven to exist.
- Criticizing the focus on nonexistent tapes rather than documented evidence against Trump.
- Emphasizing the importance of reading the actual indictment and transcripts.
- Refusing to cover unverified information and maintaining integrity in reporting.

### Quotes

- "If they show up, believe me, I will give you a play-by-play on those tapes."
- "Nobody has. Nobody has. Believe me, I would not shy away from what would be the biggest story in 10 years if you have these recordings."
- "I have a feeling that this is going to turn out to be a giant nothing burger."
- "Until the tapes are actually shown to exist, I can't cover them. I don't make things up."
- "Rather than sending messages about tapes that nobody knows anything about, you should sit down and read the indictment."

### Oneliner

Beau addresses the Biden tapes, challenging their existence and refusing to cover unverified information until proven, urging focus on actual evidence.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Read the indictment and listen to or read the transcripts of documented evidence against Trump (suggested).
- Share verified information to combat misinformation in media (implied).

### Whats missing in summary

Beau provides insight into the controversy surrounding the Biden tapes and stresses the importance of verifying information before spreading it.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the recordings,
the Biden recordings, and what is said to be on those tapes.
And we're gonna kinda go through
and talk about everything that we can
and everything that we can verify and all of that stuff.
And we're gonna do this because I got a message.
and I feel like this is important to address.
Why aren't you talking about the Biden tapes? The recordings sound pretty
incriminating to me. Why don't you listen to them and give us
a play-by-play? You won't. Go ahead and make another video
about Trump while ignoring recordings of the president
taking bribes.
You're right, I haven't listened to him yet. I haven't listened to him. I'm not as
plugged in as you are. I don't know anybody that would give me
access to them yet. If you have a transcript of the recordings that you
listen to, feel free to send it over. I will happily read it in a video because
it would be breaking news. I don't know if you're just lying about having listened
the tapes, or you are misinformed about something, but here's the thing, they
don't have the tapes, the Biden tapes they're talking about, there's actually
no physical evidence that they even exist at this point. If they show up,
believe me, I will give you a play-by-play on those tapes, but at this
point, all that exists is an FD 1023 which is a form specifically designed to
record unverified, unvetted information in which somebody says that they heard
the tapes exist. I can't listen to them because nobody has them. My guess is you
are so far down a right-wing echo chamber that you have heard about these
tapes over and over again and heard people's takes on what they think is in
them, to the point where you believe somebody's actually listened to them.
Nobody has. Nobody has. Believe me, I would not shy away from what would, I don't know,
be the biggest story in 10 years if you have these recordings. Seriously, send
to me. I'd be more than willing to listen to them. I have a feeling that this is
going to turn out to be a giant nothing burger because I actually covered this
as soon as they started asking for that specific form because I knew what that
form was. I know what it's used for and I said that they were going to do exactly
what they're doing now. Holding it up because it has a DOJ logo on it and
acting like it's evidence. The whole reason it's on that form is because it's
not evidence. There are no Biden tapes as of yet. If they surface, yeah, I'll cover
them. But you know where there are tapes? Of Trump. Maybe, rather than sending
messages about tapes that nobody knows anything about, you should sit down and
read the indictment. Sit down, read the indictment, listen to or read the
transcripts of what they have on tape. That might be a better use of your time
rather than acting like a baby staring at keys that are being waved by House
Republicans. Until the tapes are actually shown to exist, I can't cover them. I don't
make things up. I'm not whoever it is that you follow. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}