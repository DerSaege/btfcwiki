---
title: Let's talk about the Army's recruitment goals being missed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZphoE7NIufg) |
| Published | 2023/06/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The U.S. Army missed its recruitment goal, leading to speculation about a "woke military."
- Recruitment numbers have been declining since 2018, contrary to the perception of recent events causing the decline.
- In 2018, the Army set a goal of recruiting 80,000 troops but fell short at 70,000.
- The trend continued in subsequent years, with goals decreasing and recruitment numbers also dropping.
- Factors contributing to the recruitment decline include generational shifts, awareness of military realities, and job availability.
- Veterans' influence on younger generations and evolving perceptions of the military impact recruitment.
- The idea that recent events or a "woke military" are to blame for recruitment challenges is debunked.
- Recruitment issues date back to 2018, marking the first time the Army missed its goals since 2005.
- The decline in recruitment numbers coincides with generational changes influenced by past military engagements.

### Quotes

- "Recruitment issues date back to 2018, marking the first time the Army missed its goals since 2005."
- "The decline in recruitment numbers coincides with generational changes influenced by past military engagements."
- "The idea that recent events or a 'woke military' are to blame for recruitment challenges is debunked."

### Oneliner

The declining U.S. Army recruitment numbers since 2018 are influenced by generational shifts and awareness, refuting claims of a "woke military."

### Audience

Military analysts, policymakers, citizens

### On-the-ground actions from transcript

- Inform young people about military realities (implied)
- Advocate for progressive military reforms (implied)
- Support veterans in sharing their experiences (implied)

### Whats missing in summary

The full transcript provides detailed insights into the factors affecting U.S. Army recruitment numbers and challenges, offering a comprehensive view of the situation.

### Tags

#USArmy #Recruitment #GenerationalShifts #MilitaryPerceptions #WokeMilitary


## Transcript
Well, howdy there, Internet people. Let's bow again.
So today we are going to talk about recruitment for the U.S.
Army, because it's become a talking point, which is kind of odd
because these numbers have actually been out for a while.
But basically the U.S.
Army missed its recruitment goal, and this suddenly has led to the idea that this
occurred because the U.S. Army went woke. You knew that was coming, right? So what
we're gonna do is we're going to kind of go over all of the numbers and put it
into context. We are going to provide the actual information, some information that
is definitely being left out of most of the reporting, and then we're gonna talk
about why recruitment numbers are probably actually going down. Okay so
what happened was the Army set a recruitment goal of 80,000 troops by
April. They knew they weren't going to make that so they dropped the number to
76,500. They still didn't get it. They only got 70,000 and a whole bunch of
people right now who are familiar with the coverage, those aren't the right
numbers. Yeah, they are. From 2018, which is when this issue started. Go ahead and
do the math. You can guess who's president at that point in time. I didn't
know that it was woke then. I thought that was when America was great again.
But the reality is Trump didn't actually have a lot to do with this. So you're
safe for once. Okay so in 2018 the initial recruiting goal 80,000 they cut
it to 76,500 they got 70,000. In 2019 what was the recruitment goal? They tried
to cook the books they dropped it 68,000 and they got it. Their new recruitment
goal was less than they were able to recruit the previous year and they got
it. They got, I want to say, 68,000 and change. Less than 69,000. Then you go to 2020. What's the
new recruitment goal? They dropped it again, 61,200. What did they get? 61,251. Continuing that
downward trend of how many they're able to recruit. 2021, they dropped the recruitment goal again.
57,500. What did they get? 57,600. Again, continuing the trend of less and less recruitment.
2022, 60,000 new recruitment goal went up. It went up. And they didn't hit it. They got like 45,000.
5,000, continuing the trend of being able to recruit less and less.
Now it is worth noting that when the US Army missed its recruitment goal in 2018, the first
time, that was the first time they missed it since 2005.
It's not common.
It wasn't common.
Okay, so what you have is a trend stretching back from 2018 when they were able to recruit
70,000 and it declining every year to present day.
Goals declined every year except for this year where it went back up.
So it made the miss bigger, the percentage wise.
Okay, so why?
I mean, I don't think anybody who's saying that it was the woke military is going to
say that for 2018, which is when it started, which is good because that's not
what it is, but it's weird that it's 2018. You know, most people start thinking
about joining the military around 17. That's when those conversations take
place. I wonder if there was anything that happened in 2001 that might have
altered people's perception of the military. A whole lot of people join the
military because their parents were in the military. I don't know many vets
today, in fact I really only know two who would be okay with their parents or with
their kids joining the military. Out of those two, one is Air Force, one is Coast
Guard. But what you have is a 17-year gap from the start of the forever wars to
the decline of military recruitment. That may have something to do with it just
saying. That might have a little bit to do with it.
Veterans telling kids, telling their kids' friends not to sign up.
That probably plays a pretty big part of it.
I think another thing that might have something to do with it is the fact that kids today
are smarter.
They know that whatever the reason for Iraq was, it was absolutely not what the troops
were told in 2003 when they headed in.
That might have something to do with it as well.
They're also far more progressive, and the military is seen as being very regressive.
Not just are we talking about the whole maintaining US dominance aspect of it that a whole lot
of people, younger people aren't going to be happy about.
also seen as regressive when it comes to their treatment of troops and we're not
talking about realities right now because I know a bunch of vets are going to
want to chime in on that we're talking about the perception because the loudest
vets I mean they're out there wearing red hats and waving Confederate flags so
that's the perception recruitment is PR it's about perception so you have that
And then you have the other thing that's going on right now.
Can't find anybody to work.
There's jobs everywhere.
All of this is combining to impact recruitment numbers.
The decline started about the time that a young enlisted person that who was young and
enlisted at the start of the forever wars, the decline started when their
kids would be making their decisions. That probably has a whole lot to do with
it. And then just kids being more aware of the world, more aware of how the US
military is used and they don't want to be a part of it. There's no big secret
here. So the idea that it was the woke military or had something to do with
Biden, not true unless you count his involvement in the forever wars and then
you can pin it on him. But that's the only way that it really has a whole lot
to do with it. This issue started in earnest in 2018. First time the US Army
missed its recruitment goals since 2005. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}