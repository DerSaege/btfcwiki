---
title: Let's talk about Trump polling post indictment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DgwjcGFbJF4) |
| Published | 2023/06/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the current polling numbers regarding whether Trump should have been charged.
- Pointing out the issue with polling and likely voters skewing in favor of Trump supporters.
- Speculating on how undecided voters may lean towards believing Trump should have been charged as more information comes out.
- Drawing comparisons to historical events like Watergate to analyze public opinion shifts.
- Predicting a quick drop in approval ratings for Trump once more evidence is revealed and potential trial occurs.
- Expressing confidence in a significant change in polls as more people are exposed to information, potentially impacting Republican base opinions.
- Mentioning the timing of potential events and the uncertainty around Trump's delay tactics in a federal trial.
- Emphasizing that as evidence unfolds, political motivations will be revealed and opinions may shift, especially among conservative-leaning independents.

### Quotes

- "If 51% of the country believes you should have been charged with serious crimes that are likely to land you in prison, it is really unlikely that you end up in the White House."
- "They will ride with that person until they don't."
- "There's a whole lot more evidence of wrongdoing than their current news outlet may lead them to believe."
- "I think that there's going to be a large change in the polls as more people get exposed to the actual information."
- "Not today. Anyway, it's just a thought."

### Oneliner

48% believe Trump should have been charged, but undecided voters may sway as more information emerges, potentially leading to a significant shift in polls and Republican base opinions.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Stay informed on the unfolding events and evidence (implied)
- Encourage others to read the indictment and seek out reliable information (implied)

### Whats missing in summary

Insights on the potential impact of unfolding events on Trump's political future.

### Tags

#Trump #Polling #PublicOpinion #Watergate #RepublicanBase


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and polling about whether or not
he should have been charged because it has led to some questions and I don't
know what people were expecting and we don't have a lot of good historical
analogies for something like this because I hate to use the word, but it's
unprecedented. Okay, so what does the polling say? Let's start with that. 48 percent believe that he
should have been charged. 48 percent. And when you hear it that way, it seems like more people think
he shouldn't have been. That's not actually the case, even though that's what a lot of headlines
are kind of leading people to believe. Only 35% think he should not have been
charged. The rest are undecided. They don't know. Okay, those aren't winning
numbers for an election. Let's start with that. The other thing to remember is
there is an issue with polling when it comes to what they are determining as
likely voters. All of the stuff we talked about prior to the midterms that we were
correct about, none of that's changed. So those, any of the numbers that you see
that are based on likely voters, just throw those out because those are going
to skew more heavily in favor of people who support Trump.
But here's the other part.
You have 48% think he should have been charged.
A whole bunch that don't know, 35% think he should not have been charged.
The group that doesn't know, what do they not know?
the question. I have a feeling that a whole lot of people from the don't know
category are going to move into the should have been charged. As more
information comes out about what's actually in the indictment, maybe they
will take the time and read the indictment themselves, and then it moves
towards the actual criminal proceedings.
All of that's going to sway people.
My opinion is that it is going to sway people towards the should have been charged camp.
I would point out that if 51% of the country believes you should have been charged with
serious crimes that are likely to land you in prison, it is really unlikely that you
end up in the White House.
Okay, so what is the closest historical analogy that we have to this?
Watergate, right?
It's really the only thing that comes even close.
And I think there's a lot of misconceptions about Nixon and about how things went down
because of how he is viewed today.
So we're going to go through a little timeline.
Nixon, in 1973, started off with a 68% approval rating.
68%, that's a lot.
That is a lot.
That's a really high approval rating.
May rolls around, Senate Watergate hearings.
I want to say it's 18%, I know it's less than 20%.
At that point, felt he should be removed.
It wasn't until October that the percentage of people who believed he should be removed
was higher than his approval rating.
Took all of that time.
It wasn't until July of 1974 that a majority of Americans believed that he should be removed.
It's worth noting in August he was gone.
When it comes to political bases, and if you look and you break this down by political
party, it's a very partisan divide when it comes to those who still support Trump.
The thing about political bases is that they will stick with you until they don't.
They will ride with that person until they don't.
And it happens kind of, I don't want to say it all at once, but it happens very, very
quickly when it moves from they're in a rough patch to this is completely unsustainable.
It happens very quickly.
That drop off, boom, it just occurs.
That is likely, in my opinion, what is going to happen with Trump.
I would hope that it starts as more and more people read the indictment and see the evidence
that is contained in the indictment.
Because even if you look at that evidence and for whatever reason you think it was an
honest mistake, you can't honestly believe that somebody who would make an honest mistake
of that size should be president.
I think that there's going to be a large change in the polls as more people get exposed to
the actual information.
And then, assuming that it goes to a trial, during that trial, I feel like things are
going to go really bad for the former president.
I think his approval ratings will change it very, very quickly.
The thing is, I imagine that's going to happen at the worst possible time for the Republican
Party.
As you look at the timelines and how this stuff may pan out, I feel like it's probably
going to happen.
It might actually occur after the primaries, and Trump wins the primaries.
then this occurs. I know that Smith has said, you know, speedy trial. Maybe he
wants it to happen very, very quickly, which would probably be best for
literally everybody involved, but there's no guarantee that that
happens. Trump's typical tactic is to delay. He probably will not be as
successful in federal court delaying but we're gonna have to wait and see. I would
not worry about the polls when it comes to should he have been charged or should
he not have been charged, was it politically motivated or not. None of
that matters as the evidence starts to come out. If it's politically motivated
that will be found out. I've read the indictment, it's not, but if it was that
will come to, that would come to light. I think most people are going to find out
that there's a whole lot more evidence of wrongdoing than their current news
outlet may lead them to believe. And I think that that may alter some people's
opinions even within the Republican base. I'm certain it is going to alter the
opinions of independents who lean conservative, so I wouldn't worry about
this too much. Not today. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}