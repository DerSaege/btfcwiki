---
title: Let's talk about SCOTUS, Alabama, and voting rights....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=R0x4PB6nAY0) |
| Published | 2023/06/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Alabama Supreme Court decided to draw districts as they pleased, resulting in packing the black vote into one district and diluting their voting power.
- Out of seven districts, only one was majority black when realistically there should be at least two.
- The court ruled against the government in Alabama, surprising many with a 5-4 decision in favor of the voters.
- Short term implications include the likely redrawing of maps in Alabama and a positive sign for similar cases in other states like Louisiana and Georgia.
- This decision may have longer-term implications related to the Voting Rights Act and the Independent State Legislature Theory.
- The ruling indicates the court's interest in the theory may not be as strong as previously believed.
- The decision in favor of the voters contradicts the idea of an independent state legislature, reflecting an ideological shift.
- This victory is perceived as a win for the people in Alabama.

### Quotes

- "They packed the black vote into one district, diluting their voting power."
- "The court sided with the voters."
- "This is a win for the people in Alabama."

### Oneliner

Alabama Supreme Court packed black vote into one district, diluting power, but surprising 5-4 decision favored voters, signaling hope for similar cases elsewhere and suggesting a shift in the court's interpretation of Voting Rights Act implications.

### Audience
Voters, Activists, Legal Advocates

### On-the-ground actions from transcript
- Redraw district maps to ensure fair representation (exemplified)

### Whats missing in summary
The full transcript provides a deeper understanding of the nuances of the court's decision and its potential long-term effects on voting rights legislation.

### Tags
#Alabama #SupremeCourt #VotingRightsAct #Districts #Voters


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the Supreme Court
in Alabama and a surprising unexpected decision
that came from the court, why it was unexpected,
what it means in the short term,
and what it might maybe possibly mean long term.
Okay, so what happened?
Short version is that the state of Alabama,
the rulers there in Alabama,
decided they could draw those districts any way they wanted.
And they packed the black vote into one district.
Therefore, they diluted their voting power.
Out of seven districts,
basically only one was majority black.
Realistically, there should be at least two.
The court was like, nah, that's not how it works, you know, and sided with the voters.
It was unexpected because during the initial phase when the court was hearing about it,
they seemed to really open to the idea that the government in Alabama could do whatever
it wanted.
They really seemed to be leaning that way, and that's not the way they voted.
It was 5-4.
Okay, so what does this mean short term?
Maps in Alabama probably going to have to be redrawn.
This also, it signals a good sign for those that have similar cases.
There are cases I know of in Louisiana and Georgia that share similar elements and that
kind of, this signals that the voters are going to win there too over the ruling class.
Now if you want to stretch this out and you want to look at it from a longer perspective
and maybe read a little bit too much into it.
This is part of the Voting Rights Act.
That's really what's at play here.
The court has expressed interest in a theory
that most people would say is pretty half-baked
called the Independent State Legislature Theory.
Siding with the voters here over the legislature
might indicate that the court's interest in that theory
is not as strong as previously believed.
We don't know that, of course.
But ideologically, a decision in favor of the voters here,
It kind of runs contrary to the idea of the independent state legislature and again, it's
an ideological, it's a principled view.
The independent state legislature theory was not at play in this decision, it wasn't at
play at the court here, but it gives a good view of where the justices' minds are.
So this is a win for the people in Alabama.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}