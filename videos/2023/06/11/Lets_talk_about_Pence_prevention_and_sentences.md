---
title: Let's talk about Pence, prevention, and sentences....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UZnLnzKKAJQ) |
| Published | 2023/06/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the Republican perspective on prevention and deterrence through long prison sentences.
- Analyzing the disconnect between advocating for harsh sentences and actual prevention.
- Pointing out Vice President Pence's focus on punishment over prevention in crime approaches.
- Questioning the effectiveness of harsh sentences as a deterrent, especially in cases of mass shooters.
- Criticizing the lack of prevention in sentencing, particularly in cases like the Parkland shooter.
- Arguing that sentencing disparities are more about punishment and othering than sending a message to stop.
- Noting Trump's previous support for harsh penalties, contrasting with his current situation.

### Quotes

- "It's always about prevention."
- "It's about casting that tough guy image about punishment, not prevention, not deterrence, not rehabilitation."
- "It's not about sending a message to stop."
- "If you truly believe that harsh sentences are the way to send that message, then you should want a really harsh one for Trump."
- "It's part of your in-group."

### Oneliner

Beau delves into the disconnect between advocating for harsh sentences and actual prevention, questioning the true motives behind punitive approaches in the criminal justice system.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Advocate for justice reform (suggested)
- Challenge punitive approaches in criminal justice (implied)

### Whats missing in summary

Full context and depth of analysis on Republican perspectives on crime prevention and deterrence.

### Tags

#Prevention #Deterrence #CriminalJustice #Sentencing #RepublicanPerspective #Trump


## Transcript
Well, howdy there internet people, it's Bill again.
So today we are going to talk about prevention
and deterrence
and pence
and a message
that I received
talking about
long sentences
and that I am misreading
why Republicans
want to deploy them so often.
RE
long prison sentences.
about deterrence and that means it's about prevention. When Republicans advocate for harsh
sentences it's because we know it's the only way to send a message to stop. It's not about kicking
down as you say, it's always about prevention. Okay, so let's just assume this is true for a second.
then you would have to want a harsh sentence for Trump because regardless of
whatever charges might be brought, whatever the outcome may be, what is
known by all reporting at this point and kind of by his own words is that a whole
bunch of information that was damaging to the United States was not where it
was supposed to be, left unsecured, and shared. This is information that could
cost people their lives. Certainly you would want to deter that, right? You would
want to prevent that since it's always about prevention, so you would have to want that.
But here's the thing, it's not about prevention, and I can show it.
Okay, so recently, Pence, who is a Republican, even if you view him as in name only or whatever,
he's a Republican, and he was talking about how his get tough on crime approach, you know,
it is, it's about perception, it's about wanting to appear tough, it's about
punishment, not prevention. He was saying that he thinks that those involved in
mass incidents, those who are responsible, well they should get the ultimate penalty.
And I'm going to be honest, on some level I'm kind of like, yeah okay, but it
goes to the idea of it's always about prevention. He was asked, a lot of people
who are mass shooters, they go in with the intention to die. So how would the
threat of execution be a deterrent? Okay, at this point, deer in the headlights. It
was so bad I could swear that I heard Jeopardy music. And this is what he
finally came up with. I follow these stories as closely as you do, of course,
in our years in the White House, we saw one tragedy after another. And I know we
hear that we see evidence oftentimes in the aftermath that they went in without
regard to whether they would survive. But I just believe in the deterrent of law.
I believe perhaps if we made it clear, think about this, the Parkland shooter is
going to spend the rest of his life in jail in Florida. That's not justice.
This is a clear example of a time when it is definitely not about prevention.
The people responsible for these acts, they don't care. It doesn't send a
message to stop. If anything, it says, if you fail, we'll help you out. It's not
about that. It's about casting that tough guy image about punishment, not
prevention, not deterrence, not rehabilitation, nothing like that. That's
not what it's about. But if you truly believe that harsh sentences are the way
to send that message, then you should want a really harsh one for Trump. Not
just because of the severity of the alleged crime, but because he's at the
top, right? He held the most powerful office in the United States and if you
want to send a message, if you want deterrence, if you want prevention, if you
show that he'll go down for it, that means everybody knows that it'll apply
to them, right? If that's really what it's about. But it's not. There's a reason
sentencing disparities are the way they are in this country. It has nothing to do
with sending a message to stop. It's about punishing people that you have
successfully othered in most cases. You would never find yourself in that
situation. I would point out if I'm not mistaken Trump himself actually kind of
ramped up the sentencing on some of the things related to this.
He himself advocated for the ultimate penalty for people who do what he's accused of.
But I guess now it's different because it's not some other.
It's part of your in-group.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}