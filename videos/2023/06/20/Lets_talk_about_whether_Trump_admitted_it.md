---
title: Let's talk about whether Trump admitted it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=g5JARmKKzTY) |
| Published | 2023/06/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's interview on Fox News, where Fox News questioned and pushed back effectively.
- Trump insisted he won the election despite losing, and defended his retention of documents with personal items in them.
- The problem lies in the intent to retain the documents, not the reason for retention.
- Trump's explanation in the interview may be problematic legally, especially regarding willful retention.
- His statements in the interview may not be helpful in court, and his attorneys likely weren't happy with them.
- Trump's defense of needing to take personal items out before returning documents doesn't clear him of charges.
- The interview likely didn't do Trump any favors, and his explanations might not hold up legally.
- Trump's statements during the interview, including his timeline for returning documents, may be revisited in the future.
- Setting a timeline for returning documents is not within Trump's purview, especially when authorities are actively pursuing them.
- Overall, Trump's interview statements may not serve as a strong defense for him legally.

### Quotes

- "Before I send the boxes over, I have to take all of my things out."
- "When you say before I return the documents, I have to do X, you are saying I am willfully retaining them until this time."
- "The president, former president did himself no favors during that interview."

### Oneliner

Former President Trump's interview statements on Fox News may not provide a strong legal defense, particularly concerning the intent behind document retention.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Analyze legal implications of statements (suggested)
- Prepare for potential legal challenges (implied)

### Whats missing in summary

Deeper analysis of legal implications and potential consequences of Trump's interview statements.

### Tags

#Trump #Interview #LegalImplications #DocumentRetention #FoxNews


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about that Trump interview and the different
reads on what Trump said.
If you don't know what happened, the former president went on Fox News and Fox
News actually questioned him and pushed back pretty effectively at times.
Not what I was expecting to see, I'm going to be honest.
But they were like, hey, you lost.
You lost the election.
And Trump's like, no, I won.
And they're like, you took it to court and in front of judges you appointed, you lost.
You lost.
And they just hammered it home.
They also asked him about the documents and the recordings and all kinds of things.
They really did do a good job at pushing back on his attempt to be economical with the truth.
But one part in particular has led to a bunch of questions.
Some people heard it and they were like, oh no, he has a defense for willful retention.
people heard it like is this really gonna fly you know and kind of believed
that he might have admitted to the crime on TV okay so he was asked you know why
why didn't you give the boxes back and he basically said well I had personal
stuff in them and I had to get my personal stuff out. Given the whole
timeline, that part isn't going to fly. In fact, there's one particular piece
of this that is probably going to become incredibly problematic for the former
president. So in relevant part, the the willful retention part of this, it says
that somebody's guilty of it if with the intent to retain such documents or
materials at an unauthorized location. The intent to retain the documents
is actually the problem. Again, the intent to retain such documents or
materials at an unauthorized location shall be fined under this title or
imprisoned, blah blah blah blah blah. Trump in the interview said, before I send boxes
over, I have to take all of my things out. These boxes were interspersed with all
sorts of things. I was very busy, as you've sort of seen. Okay, to be clear the
law does not say, you know, it's okay if you intend to retain these documents at
an unauthorized location until you get your golf shirts out of the box. It's not
what it says. It says if you have the intent to retain those documents, it's an
issue. And before I send the boxes over, I have to take all of my things out.
Before he returns the documents, he has to take his things out and he's gonna do
that on whatever schedule I guess he has because he's a very busy man. Can't be
bothered with little things like classified information, I guess. When you
say before I return the documents, I have to do X, you are saying I am willfully
retaining them until this time. He definitely did not clear himself of
those charges in that interview. I think he probably hurt himself a lot. I do not
believe the explanation that he provided in that interview is going to be
helpful in court in any way shape or form. He might have admitted to one of
elements during the interview. Again, I'm not a lawyer but when the title of the
charge is willful retention, it's probably a really bad idea to get on TV
and explain why you retained them longer than you should have.
That's not helpful. I'm certain that his attorneys were not happy with
that statement. There were a number of other statements in this interview that
will probably come up later. This is one of those things. You will see this
material again. I'm going to suggest that his explanation of the recording is not
going to fly either. It did, the president, former president did himself no
favors during that interview. So for those who heard it and said hey that
sounds like a real defense because he he makes it sound like he was gonna give
him back. Yeah you don't get to set the timeline for that. That's not how it
works, especially when they are actively pursuing them, there is an issue that this was not
a silver bullet defense for him in any way, shape, or form.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}