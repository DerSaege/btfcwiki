---
title: Let's talk about Hunter Biden's charges....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=f5DqO2hi188) |
| Published | 2023/06/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hunter Biden, the sitting president's son, was charged with two counts of willful failure to pay taxes and a pretrial diversion on a gun charge related to a substance issue.
- The charges could lead to anywhere from probation to up to a year in jail, but Beau believes jail time is unlikely.
- Beau has a rule not to talk about private citizens, but he's breaking it because Republicans made false claims about Hunter Biden receiving a sweetheart deal from the Biden DOJ.
- The prosecutor responsible for Hunter Biden's deal is actually a Republican and a Trump appointee.
- President Biden did not interfere with the Department of Justice (DOJ) and could have pardoned his son but chose not to.
- Beau contrasts Biden's hands-off approach with what he believes Trump might have done if one of his children were in a similar situation.
- Beau mentions comparisons to Kushner's actions but cautions against seeking retribution based on partisanship.
- Beau expresses surprise at learning Hunter Biden's full name is Robert.

### Quotes

- "President Biden did not interfere with the Department of Justice (DOJ) and could have pardoned his son but chose not to."
- "Can you imagine one of Trump's kids getting arrested while he was president? Of course not!"
- "The crime becomes evident and then you look for the person. You don't look at a person and try to find a crime."
- "It seems pretty clear that isn't how DOJ works."
- "Did y'all know his name was Robert?"

### Oneliner

Beau breaks his rule to debunk false claims about Hunter Biden's case and praises President Biden's hands-off approach towards DOJ.

### Audience

Those interested in clarifying misinformation and understanding political dynamics.

### On-the-ground actions from transcript

- Fact-check misinformation spread about political figures (implied)

### Whats missing in summary

Context on the importance of separating political interference from legal proceedings.

### Tags

#HunterBiden #DOJ #Misinformation #PoliticalDynamics #FactChecking


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Hunter Biden.
And I know there's a whole bunch of people
who have been watching this channel a long time
who are going, why?
Right, we'll get there, there is a reason.
But we will basically go over what happened
and then we'll talk about why we're actually talking about it.
So if you don't know,
Hunter Biden, the sitting president's son, was charged by information which means a prosecutor
or federal agent basically just wrote down what happened.
Charged by information after a four-year investigation with two counts of willful failure to pay
taxes, which are misdemeanor counts, and then there is a pretrial diversion on a gun charge.
That charge is related to Hunter having a substance issue.
If you have a substance issue, you are federally prohibited from having a firearm.
A pretrial diversion basically means you're going to go through a program, you're going
to do these steps.
If you're a good little boy and complete all of the steps, it kind of goes away.
People are asking what kind of time is he looking at?
Anywhere from absolutely nothing and probation only to up to a year.
He could be sentenced up to a year in jail.
Do I think that's likely?
No.
be shocked if he got a little bit of time, like 30 to 90 days? No. So that's
what occurred. That's the story. Why are we talking about it? For those
that don't know, I have a long-running rule on this channel. I don't talk about
private citizens, even if they are relatives of politicians, unless whatever
it is they did somehow impacted the official capacity of the politician. This
rule was in place throughout the Trump administration for the Republicans
watching this channel. That's why you never heard me talk about Melania,
except to talk about when she wore the jacket when she was in an official
capacity and I defended her when it came to the photos because that had nothing
to do with her official capacity. So why are we breaking this rule? Because
Republicans made it political and they put out a whole bunch of information
that is not true so now we have to clear it up. Okay, Republicans are saying he got
a sweetheart deal by the Biden DOJ. No, no. The prosecutor is responsible for that deal.
The prosecutor is a Republican and a Trump appointee. It is not the Biden DOJ doing it.
Plea deals get you leeway. That's why they exist. That is why they exist. If you save
the government the trouble of going to trial, they treat you better. This is
something that the Trump team should probably take into consideration. Okay,
now as far as it being Biden-DOJ, let's be super clear on something. The
The sitting president's son has been charged.
That's because Biden is not interfering with DOJ.
Make no mistake about it, at any point in time, President Biden, the person who retains
full control, full power when it comes to pardons, could have pardoned his son at any
point in time. He chose not to. He elected not to. Regardless of what you
think of Biden, whether you like him, you don't like him, whatever, the idea that he
is standing by that commitment and all of that rhetoric about the rule of law,
He didn't pardon his son when there's absolutely nothing anybody could do to stop him.
What's the worst that could happen? Give him some bad press?
Again, no matter what you say about him, how you feel about him, when it comes to his
integrity on that issue, on the rule of law issue? I mean, this is kind of a big
marker here. Now, as far as the political nature of DOJ, can you imagine one of
Trump's kids getting arrested while he was president? Of course not! Of course
not because he would have interfered with the investigation. He would have
stopped it. He would have put pressure on it, and if need be he would have pardoned
them. You know it's true.
Now, a lot of people have brought up Kushner. Yeah, I get it, and for those who
don't know, there are many people who believe that Kushner actually did
something very similar to what Republicans thought Hunter Biden did in
Ukraine. I understand where people are coming from but you don't want to do
that. The crime becomes evident and then you look for the person. You don't look
at a person and try to find a crime. It's not how it's supposed to work. So I
get the the partisan call for some kind of like return fire that's not how this
is supposed to work and it seems pretty clear that isn't how DOJ works. If that
was how DOJ functioned we wouldn't be having this conversation because they
never would have charged Hunter.
Okay and now for the biggest bombshell of all.
The one thing about all of this that truly surprised me, did y'all know his name was
Robert?
Robert Hunter Biden?
I have no clue.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}