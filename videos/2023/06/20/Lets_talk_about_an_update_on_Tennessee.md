---
title: Let's talk about an update on Tennessee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=KTMqMuSog6s) |
| Published | 2023/06/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on a story involving the expulsion of two black men and a white woman from the Tennessee state legislature.
- The local areas responsible for the representatives sent them right back despite the expulsion.
- Both Pearson and Jones advanced through the Democratic primary.
- Pearson will face John Johnston, an independent, while Jones will face Laura Nelson, a Republican in the special election on August 3rd.
- The special election is costing Tennessee close to a million dollars.
- Beau believes the special election is just a political stunt by Republicans to show racism.
- He predicts that both Pearson and Jones will win the special election, rendering the entire process pointless.
- Criticizes the state legislature for not believing in the will of the people and sending a message of young black men needing to stay in their place.

### Quotes

- "This election is costing the people of Tennessee probably close to a million dollars, just so the state legislature could prove that, well, they're still pretty racist."
- "All of this was a political stunt by Republicans and all it showed was that the majority of the state legislature in Tennessee does not believe in the will of the people."
- "I have a feeling they're gonna stay in their place and their place is apparently the Tennessee State Legislature."

### Oneliner

Beau provides an update on the expulsion of two black men and a white woman from the Tennessee state legislature, criticizing it as a political stunt showing institutional racism.

### Audience

Advocates for social justice.

### On-the-ground actions from transcript

- Support Pearson and Jones in the special election (suggested).

### Whats missing in summary

More details on the initial situation that led to the expulsion of the representatives.

### Tags

#Tennessee #StateLegislature #Racism #SpecialElection #PoliticalStunt


## Transcript
Well howdy there internet people, it's Bo again. So today we're going to talk about some news
out of Tennessee, provide a little bit of an update on a story that I'm sure everybody
is going to remember, and just kind of fill everybody in on how that story's progressing
because it's kind of fallen out of the news cycle, though it's still pretty interesting
to me.
So if you remember a while back, there was a situation at the state capitol, and there
were three representatives, three members of the state legislature there, that, well,
the Republicans in the state legislature, they said that they shouldn't do that.
There were two black men and a white woman.
And the state legislature in Tennessee expelled the two black men, right, removed them from
their positions.
Now the districts, the local areas that were responsible were like, yeah, no, and sent
them right back.
But because they were expelled, there still had to be a special election.
worth noting that both Pearson and Jones have advanced through the Democratic primary. So
that has occurred. Now Pearson will be facing John Johnston, who is an independent. There is no
Republican running in that area because it is incredibly blue and, well, realistically they
can't win. Jones will be facing Laura Nelson, a Republican. The special election is on August 3rd.
This election is costing the people of Tennessee probably close to a million dollars,
just so the state legislature could prove that, well, they're still pretty racist. I mean, that's
really all it's accomplished so far, because I have a feeling that just like the districts,
you know, the the city council and the various people that had a say in it were like, yeah,
no. I feel like the voters are gonna do the exact same thing and send both Pearson and Jones back
to, well, they're already their interim, but I feel like they're going to win the special election,
which means all of this was for nothing. All of this was a political stunt by
Republicans and all it showed was that the majority of the state legislature in
Tennessee does not believe in the will of the people, does not believe that the
Connor should have a say and the real message they were trying to send as
evidenced by you know the vote was well you two young men better stay in your
place. I have a feeling they're gonna stay in their place and their place is
apparently the Tennessee State Legislature. Anyway it's just a thought
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}