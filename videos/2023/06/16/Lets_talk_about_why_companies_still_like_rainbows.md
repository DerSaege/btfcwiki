---
title: Let's talk about why companies still like rainbows....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7iaTejXGYpo) |
| Published | 2023/06/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the prevalence of marketing rainbows and why they will likely continue despite efforts to stop them.
- Mentions a survey where 70% of non-LGBTQ+ individuals believe companies should show support through advertising, sponsorship, and hiring practices.
- Points out that there are an estimated 17 million LGBTQ+ individuals in the U.S. who are supportive and hold significant economic power.
- Companies are interested in tapping into this trillion-dollar market and are more likely to support a community with immutable characteristics for long-term gains.
- Notes the risk of losing customers permanently by alienating a community versus temporarily upsetting others who may eventually return.
- States that companies prioritize profit over social responsibility, akin to countries pursuing power in foreign policy.
- Attributes the widespread appearance of rainbows to capitalism and the profitability of being accepting and inclusive.
- Suggests that there are not enough individuals invested in culture wars to significantly impact the prevalent display of rainbows.
- Mentions examples like Target moving displays but also investing in pride celebrations, showing that acceptance can be profitable.
- Concludes that being a good person and accepting leads to a wider market and ultimately more profit.

### Quotes

- "It is more profitable to be a good person. It is more profitable to be accepting."
- "The reason you see rainbows everywhere is because of, well, the right wing. Capitalism."
- "The money is always an acceptance."

### Oneliner

Despite resistance, marketing rainbows persists due to capitalist interests and the economic power of supportive LGBTQ+ individuals.

### Audience

Marketers, LGBTQ+ allies

### On-the-ground actions from transcript

- Support LGBTQ+ businesses and initiatives (exemplified)
- Attend and participate in pride celebrations and events (exemplified)

### Whats missing in summary

The full transcript provides more insight into the economic motivations behind companies' support for LGBTQ+ communities and the profitability of inclusivity in marketing.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about marketing
and rainbows and why you should probably expect
to continue to see them
and that it's not really gonna go away
despite a semi-organized push to make them go away.
I had a question, short version is,
hey, with all of the pushback,
Why are so many companies going forward with it?
It's math, it's capitalism, it's capitalism.
Okay, so let's start with this.
There was a survey conducted,
it's called Accelerating Acceptance, okay?
Survey of 2,500 people who were not members
of the LGBTQ plus community.
70% said that companies should show their support
through advertising, sponsorship, and hiring practices. 70%. That's pretty
big. But aside from that, there's another reason. The estimation that I saw said
17 million people in the U.S. are LGBTQ+. That survey was of people who
weren't. Seventy percent of them are supportive. You know the 17 million are
supportive and they're worth about a trillion dollars. That's a lot of money
changing hands over the course of a year, right? Companies want their share of it.
It's capitalism. And if you are looking at building your brand, you're looking at
building your company, who are you more interested in showing support for? The
17 million people who have a trillion dollars worth of economic activity, who
have an immutable characteristic, something that isn't going to change.
And if you back away from them, well you may lose that customer forever because
characteristic? That's with them forever, right? So you lose that customer forever
or you might lose a customer who is currently upset about rainbows in on-store
shelves but who realistically cannot remember why exactly they smashed their
cure egg because they'll forget about it. Over time, they will forget about it, and
they'll get that, they will get them back. But if they abandon a community, they may
never get them back, and they're probably worth more.
There are probably more people in the LGBTQ plus community than are in the demographic
of people who will participate in trying to show these woke companies who's boss.
It's just math.
It's no big secret.
Again, these companies, some may actually
be vaguely socially responsible.
But the overwhelming majority, companies are like countries.
They don't have friends.
They don't have interests.
Rather than the just unbridled pursuit of power,
which is what countries do on the foreign policy scene,
is the unbridled pursuit of profit. There's no big mystery here. The reason
you see rainbows everywhere is because of, well, the right wing. Capitalism.
capitalism is a right-wing thing. It pays to have rainbows up, so they're
going to. And there's not enough people who are that vested in the culture wars
to put a dent in it. Yeah, I mean Target may move some displays around in certain
stores but they're also what dumping a hundred and seventy five thousand
dollars into the pride celebration up in New York. The money is always an
acceptance. It is more profitable to be a good person. It is more profitable to be
accepting. You have a wider market. Anyway, it's just a thought. You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}