---
title: Let's talk about one of Trump's trial dates being set....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=C3IyTOBkXVE) |
| Published | 2023/06/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trial date set in Trump's legal entanglement from 2019 suit by E. Jean Carroll.
- First suit caught up in delays finally moving forward to trial on January 15th, 2024.
- Trump found liable in second suit for battery and defamation last month.
- Trump unlikely to attend trial as it is a civil matter.
- Trial date just one week before Iowa Republican caucuses.
- Judge allows Carroll to amend her suit to include new allegedly defamatory statements by Trump.
- New statements made after Trump was found liable in the second suit.
- Trump's new statements will now be part of the first case to be decided in January.
- Trump is still pursuing the Republican nomination while under indictment.
- Uncertainty if this trial will conflict with other potential trials Trump may face.

### Quotes

- "Trial date set in Trump's legal entanglement."
- "Trump is still pursuing the Republican nomination while under indictment."
- "New defamatory statements will be rolled into the first case."
- "Just a thought."
- "Y'all have a good day."

### Oneliner

Trial date set for Trump's 2019 legal entanglement with E. Jean Carroll, with new defamatory statements potentially impacting his pursuit of the Republican nomination while under indictment.

### Audience

Legal analysts, political commentators, concerned citizens

### On-the-ground actions from transcript

- Monitor updates on the trial proceedings and outcomes (implied)
- Stay informed about political and legal developments related to Trump's cases (implied)

### Whats missing in summary

Context on the potential impact of this trial on Trump's legal battles and political aspirations.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the trial date
being set in one of Trump's legal entanglements.
The original suit from 2019 from E. Jean Carroll
is going to be moving forward.
So just to clarify this, the most recent suit, the one last month, where Trump was found
liable for battery and defamation, that was actually the second suit from E. Jean Carroll.
The first one has just been caught up in delay after delay after delay.
It is finally moving to trial and the trial will begin on January 15th, 2024, which is
one week before the Iowa Republican caucuses.
It's safe to assume that Trump is really unhappy about that.
He's not going to have to attend this in person because it's a civil matter, but that going
on and perhaps being decided right around the time of Iowa.
That's just all bad news for the former president who is still pursuing the Republican nomination
while under indictment.
Okay.
In related news, the judge also ruled that E. Jean Carroll could amend her suit to include
new defamatory statements, allegedly defamatory statements, made after the second suit, the
the one that was resolved first, after that concluded and Trump was found liable.
So immediately after that suit, Trump took to his little pretend Twitter and repeated a lot of statements
that E. Jean Carroll says are defamatory.
which are remarkably similar to the ones that he was found liable for. So those
new statements will be rolled in to the first case, which will be decided in
January, which is definitely less than ideal timing for the former president.
This is, of course, assuming that this proceeding does not conflict with the
many other trials that Trump may be facing. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}