---
title: Let's talk about Trump ignoring advice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ENWyExLFeDc) |
| Published | 2023/06/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent reporting on former president's decision-making process regarding documents case.
- Advisors urged return of documents, even suggesting a negotiated settlement with the Department of Justice to avoid criminal charges.
- Trump decided to hang on to the documents based on advice from a partisan organization.
- Beau points out that returning the documents could have made the issue disappear without charges.
- Department of Justice and FBI extended courtesies to former president, which were not accepted.
- Ignoring counsel's advice to return the documents could be a huge problem for the former president in court.
- Willful retention of documents despite advice is a damaging move for Trump's defense.
- Beau concludes by expressing his thoughts and wishing the viewers a good day.

### Quotes

- "Returning the documents could have made the issue disappear without charges."
- "Ignoring counsel's advice could be a huge problem for the former president in court."

### Oneliner

Recent reporting reveals former president's decision to retain documents despite advice, risking legal consequences.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Contact legal representatives for advice on handling sensitive documents (implied)
- Stay informed about legal implications of document retention (implied)

### Whats missing in summary

Analysis of potential legal implications and consequences for the former president.

### Tags

#FormerPresident #DocumentsCase #LegalAdvice #DecisionMaking #Consequences


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some recent reporting
about the former president and his decision-making process
when it comes to the documents case.
It is now being reported that a number of his advisors
had repeatedly attempted to get him to cooperate and return the documents. One
attorney going as far as to suggest a negotiated settlement with the
Department of Justice to basically return everything and work out some kind
of deal that would preclude the president, former president, from facing
criminal charges. Trump decided that wasn't the right move and took advice
reportedly from a partisan organization and decided to try to hang on to the
documents. That was obviously an ill-advised decision. It's worth noting
that we have talked about this before. Even prior to the search, we talked about
how if he just gave them back this would all go away. It wouldn't matter. He's
making a case out of nothing. He's literally, through his own inaction,
creating a federal case. I am of the opinion that had he really went all in
on getting everything back, even immediately after they searched his
place, it probably all still would have gone away then without charges. Again, the
Department of Justice and the FBI attempted to extend every possible
courtesy to the former president, and he didn't accept them along the way.
The reporting that suggests he was advised by counsel that he needs to return the documents
and work out some form of settlement, if that's something that the prosecution can actually
bring in to the case, that's going to be a huge problem for the former president.
Nothing says willful retention like ignoring the advice of counsel and choosing to retain
the documents when you have attorneys and advisors saying you need to give them back.
If that reporting is something that the prosecution can bring into that courtroom, that's going
to be a huge, huge blow to Trump's defense of any kind to those charges.
Anyway, it's just a thought.
So have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}