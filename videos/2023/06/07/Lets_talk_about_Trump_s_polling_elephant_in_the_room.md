---
title: Let's talk about Trump's polling elephant in the room....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=K6t9H_vK7oY) |
| Published | 2023/06/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring Trump's legal entanglements and polling among Republican primary voters.
- 23% of Americans believe a convicted person should still be able to serve, while 62% disagree.
- Among likely GOP primary voters, 52% prefer Trump over another candidate.
- Even if convicted, 46% of GOP primary voters think Trump should still be allowed to serve.
- Despite potential convictions, Trump remains the clear winner in Republican primary polling.
- Trump's support among GOP voters suggests he could win the primary even if convicted.
- The situation may lead Democrats to prepare to face Trump in the general election.
- Trump's strong support among primary voters poses challenges for other GOP candidates.

### Quotes

- "46% of primary voters think he should be allowed to serve even if convicted."
- "It's almost like he can't lose."
- "Even if he is convicted, he wins the Republican primary."
- "It's going to take something really out there to alter the course of the Republican primary at this point."
- "You're probably going to see the Democratic Party go ahead and just start gearing up for a matchup against Trump."

### Oneliner

Beau delves into Trump's legal issues, polling among GOP voters, and the likelihood of Trump winning the Republican primary even if convicted, possibly setting the stage for a Democratic showdown.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Prepare for potential matchups against Trump in upcoming elections (implied).

### Whats missing in summary

The full transcript provides an in-depth analysis of Trump's standing among GOP primary voters and the implications of his potential legal troubles in the upcoming elections.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump, his legal entanglements, and polling,
and particularly among Republican primary voters.
And we're going to talk about the proverbial elephant in the room here.
Okay.
Among Americans, if somebody is convicted of a serious crime,
Okay? 23% believe he should still be able to serve. 62% say no. Okay? That's
Americans across the board. It would be really hard to win the presidency with
62% saying that some of your behavior is totally disqualifying. That that's going
be a hard task right there, but when you switch to just likely GOP primary voters, the numbers
change and they change drastically.
52% more than half want Trump, 36% want somebody else.
He is still the clear winner when it comes to the Republican primary right now and all
polling. And it's not even close. When you look at the individual matchups, it's just really not
close. Okay, but this is where it gets interesting. Among GOP primary voters, 46% say he should be
allowed to serve even if convicted, even if convicted, won't change their opinion, 46%.
In a crowded primary field as the GOP has, 46%, that's a win.
That's a winning percentage.
With as many candidates as there are, 46%, that's enough to win.
That is enough to win the GOP primary, even if convicted.
That should say a whole lot to the rank-and-file Republicans, those people who aren't primary
voters. That should tell you a whole lot about where the leadership of your party is. Forty-six
percent believe that even if convicted... I mean, okay. So what you have here is a situation where
it is still likely that even if he is convicted, he wins the Republican primary based on this
bowling. Even if he is convicted, he wins the Republican primary. But what does
that mean for the general? Probably not.
Obviously this doesn't have to do with indictment, it has to do with conviction.
So the numbers may change if he was only indicted and not convicted yet.
But it's definitely an interesting set of numbers that is probably going to lead a lot
of people to the conclusion that even if he is indicted and convicted, the Democratic
party is probably going to be gearing up to face Trump in the general.
Because based on the polling, it's almost like he can't lose.
It's almost like he can't lose.
46% of primary voters think he should be allowed to serve even if convicted.
46%, that's enough to win the primary right there.
And that's a conviction, an indictment that number is probably higher.
So it's going to take something really out there to alter the course of the Republican
primary at this point.
if convictions of serious crimes won't do it, I mean I don't know what will. So
you're probably going to see the Democratic Party go ahead and just start
gearing up for a matchup against Trump. Anyway, it's just a thought. Y'all have a
good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}