---
title: Let's talk about Ukraine, pipelines, discord, and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qfb4xgkvuMY) |
| Published | 2023/06/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses two videos he didn't make about Ukrainians, pipelines, and Discord leaks, prompted by messages he received.
- He questions why he didn't follow up on particular information in those videos that turned out to be accurate.
- The Discord leaks revealed information that fit perfectly into certain narratives but raised doubts about its accuracy.
- He expresses skepticism about the leaks, suspecting that some information may have been altered or misleading.
- Regarding the pipeline incident involving six Ukrainians, Beau questions the narrative of non-state actors acting without support.
- Beau speculates that there may have been a rogue element within the Ukrainian government involved in the pipeline incident.
- He acknowledges his theory but expresses distrust in the leaked information that confirms it.
- Beau points out the convenient timing of the leaks coinciding with global debates on Ukraine and Russia's involvement in certain incidents.
- He stresses the importance of questioning information in the intelligence world where failures are known, and successes remain hidden.
- Beau remains cautious about drawing definitive conclusions despite the alignment of events and his theories.

### Quotes

- "I can't take a victory lap on something that I have doubts on."
- "There are a lot of information operations going on, and if everything just lines up too neatly, question it."

### Oneliner

Beau questions the accuracy and timing of leaked information on Ukrainians, pipelines, and Discord, urging skepticism in the shadowy world of intelligence.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Question information operations (implied)
- Stay cautious of neatly lining narratives (implied)

### Whats missing in summary

Insights on navigating through the murky waters of intelligence and leaked information.

### Tags

#Ukrainians #Pipelines #DiscordLeaks #Skepticism #Intelligence #InformationOperations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about two videos I didn't make.
We're going to talk about Ukrainians and pipelines
and information in Discord.
We're going to do this because I've gotten a couple of messages
along these lines.
But this one in particular, I definitely
feel like it's worth talking about because it kind of put everything
together. I have questions about two times you didn't take a victory lap when
you should have and had every right to do it. First when talking about the
leaks you weirdly said you would be quote unsurprised to find out that he
had been identified much earlier than we knew. That turned out to be the case and
you never followed up on what was a really weird lead-in to the video. When
When talking about the pipeline, you said you thought it was six Ukrainians, but didn't
think it was just quote random Ukrainians, and that they had state help.
They now say the Ukrainians had help from a rogue element within the Ukrainian government,
and the information came from the leaks, but no told you so video.
Are these two absent videos related?
this mincemeat? Nice. Okay, so to catch everybody up, the Discord leaks.
Initially I wasn't covering it because I had questions. In the video when I
finally started talking about it, I'll put it down below, at the very beginning I
was like, you know, I have a hunch, I have a lot of questions about this, and I feel
like we're gonna find out that US counterintelligence knew about him way
earlier than we knew at the time. That was the case. Okay, they didn't do
anything though, right? They did absolutely nothing. Now, you could chalk that up to
just gross negligence, okay? But what's another reason that you might allow
somebody to continue to leak information once you have them identified. The
information's not any good. You have altered what they have the ability to
access, right? Okay, now to be clear on this, I don't know that. I just have a
whole lot of questions about the information that came out via those
leaks, a bunch. In real life, there's a bunch of loose ends. That information fit
into the narratives so perfectly. It was just, it was ideal. There were no
loose ends, and everything had value to the U.S., to the West. Everything that
came out in those leaks, it was actually beneficial for that information to go
out the information that we could verify, that we knew was accurate. There was a
bunch of information that didn't really seem to be accurate, okay. So therefore I
don't trust the leaks, period. I don't trust the information in them. I can say this now
without really hurting anything. I don't trust the information that's
contained in them. Then when you get to the pipeline, the information came out that it
was six Ukrainians and from the beginning I was very much saying, hey, people are
overlooking the idea that this could be non-state actors. And the information
came out and said, hey, looks like it was non-state actors, but they had no support
from anybody. Yeah, that doesn't seem accurate. That seems very unlikely. The
level of skill that was demonstrated not with actually taking out the pipeline
but with their movements and their travel and how hard it is to actually
identify them. That's not something that six random people know how to do. They
had training they had help from who we don't we don't know right but now
there's information saying it was a rogue element within the Ukrainian
government yeah I mean it's my theory of course I like it but I don't trust the
source of the information even though it confirms what I believe I don't actually
I can't put any stock in anything that came out of those leaks. There's just
way too many questions there for my taste. And I would point out that at this
point in time it coming out the way it did at the exact moment when the world
started discussing whether or not Ukraine or Russia was behind a dam getting taken out,
again that's super convenient timing.
I have questions.
Now I don't actually, again I don't disbelieve the idea that Ukraine had something to do
with this, with the pipeline. That's kind of the theory that I put the most weight behind.
And yeah, I absolutely said that I think they had state help, but I can't trust the information
that is confirming my own theory. I drag people for that all the time. I don't want to make
the same mistake. My doubts about the leaks very much very much open about
that. So I can't use that to back up a theory that I might believe in. Now as
far as is this mincemeat? I mean maybe it might be Google operation mincemeat.
if you don't know what that's a reference to. Here's the thing, both of
these, the leaks, the pipeline, even the dam, they are part of that intelligence
world, that shadowy world. The failures are known, the successes are not. All of
All of these have been pretty successful because nobody has a clue what happened.
I can't take a victory lap on something that I have doubts on.
Even though I have expressed those doubts and everything's lining up, I still can't
prove anything that I believe.
It's just things are lining up nicely to one day hopefully be able to prove it.
So the only reason I'm really talking about it now is because there have been so many
questions about it.
But again, there is a major war going on in Europe.
There are a lot of information operations going on, and if everything just lines up
too neatly, question it.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}