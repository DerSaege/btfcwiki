---
title: Let's talk about Trump juries and true believers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3EDQANTh5-I) |
| Published | 2023/06/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the power of one person on a jury in the legal system.
- Mentions multiple ongoing cases against Trump in different states.
- Indicates the difficulty in obtaining indictments.
- Suggests that the jury selection process aims to eliminate bias.
- Speculates on the possibility of multiple "true believers" needed to sway a jury.
- Emphasizes the importance of waiting for actual indictments before predicting outcomes.

### Quotes

- "One person on the jury wields a whole lot of power."
- "The indictments themselves are hard to get."
- "At the end of this, it would take four true believers, minimum."
- "You're looking for ways for things to go wrong. There's a bunch of them."
- "Let's wait for the indictments before we start talking about what the jury may or may not do."

### Oneliner

One person on the jury holds significant power in legal proceedings against Trump, with multiple cases ongoing and the necessity of waiting for indictments before predicting outcomes.

### Audience

Legal observers

### On-the-ground actions from transcript

- Wait for actual indictments before making assumptions (implied)

### What's missing in summary

The full transcript provides a nuanced understanding of the legal process and the potential implications of one juror's beliefs in Trump's cases.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Trump,
juries, and true believers, and whether or not they matter,
and whether or not they matter,
because I got this message.
I have a question for you, Beau,
and I think it's a question that may need a video.
So, evidence, indictments, charges, trials.
My question is this, at the end of the day,
it only takes one, right?
Jury of peers, 12 citizens selected to pass judgment.
He only needs one true believer and we're back to square one.
Actually worse than that, thanks to double jeopardy.
Is this an actual concern?
Can one person undo literally years of painstaking work in this situation or are there contingencies?
Okay, so I mean, yeah, that's the way the jury system works.
One person on the jury wields a whole lot of power.
At the same time, it's not one person, not really.
This is a very unique situation.
There is the New York case, there's the Georgia case, I actually think that there's a third
state investigation kind of lurking in the background. There is the documents
case, there is the federal election interference, the overturning of 2020,
however you want to phrase it, and then there's a possibility of a federal
financial investigation as well, or that may be part of the election one. It's not
one person, it's one person on each one of those. The most likely scenario, it's
not the jury that's going to be the issue. The indictments themselves are
are hard to get. If the indictments come back in this situation, you can be fairly certain
that the prosecutor believes their case is airtight. And the jury will be selected to
hopefully weed out those who are going to disregard the evidence. You don't know
that, but that would be a goal of the prosecution. At the end of this, it would
take four true believers, minimum, assuming all of these results in
indictments at minimum in the right spot that completely put their reason aside and say,
okay, this is an airtight case, but I'm going to say no anyway.
It's possible.
It doesn't seem very likely.
I think your bigger concern is an endless string of the pills afterward.
I think that's probably the more likely thing that is going to delay any real resolution
on it.
But again, this is counting chickens at this point.
We might want to wait until there are actually indictments for the federal cases.
Those are the ones that are going to have the most resources behind them.
Those are the ones that are going to be set up to be absolutely airtight because they
wouldn't pursue it if it wasn't.
This is one of those things where you're looking for ways for things to go wrong.
There's a bunch of them.
There's a bunch of them.
But you have to play with what's already on the table.
So let's wait for the indictments before we start talking about what the jury may or may
not do.
version yeah one single juror could just hold out but at the same time it's not
just one juror because I I have a feeling there's gonna be more than one
jury. So anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}