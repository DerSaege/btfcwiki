---
title: Let's talk about Meadows and the USSS testifying about Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mUrLXJArY-0) |
| Published | 2023/06/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Meadows and other key figures have reportedly spoken to grand juries, spelling bad news for Trump's team.
- Meadows' attorney stated his commitment to truth in legal obligations, potentially hinting at cooperation with investigations.
- Meadows' decision to cooperate may stem from his vested interest in protecting himself due to his position within the Trump administration.
- Two dozen Secret Service agents have also reportedly spoken to the grand jury, providing critical evidence due to their proximity to high-profile individuals.
- The best evidence may come from individuals like housekeepers, security personnel, and assistants who have detailed knowledge of events.
- Secret Service agents' compliance and thoroughness in recording information could be detrimental to Trump's defense.
- Testimonies have occurred in the past, with Secret Service testimonies happening over the last two months.
- The emerging information indicates a potential conclusion to the ongoing investigations.

### Quotes

- "This is the information that would establish timelines and intent."
- "The best evidence doesn't come from the names you know."
- "If they were even remotely forthcoming, anything that Trump did is now on record because they knew."
- "We're near the end of this."
- "It's just a thought."

### Oneliner

Key figures like Meadows and Secret Service agents cooperating with grand juries spell trouble for Trump's team, with emerging evidence potentially leading to significant revelations.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Contact legal experts for insights on the legal implications of key figures cooperating with investigations (implied).

### Whats missing in summary

Context on the potential implications of the emerging evidence for ongoing investigations.

### Tags

#Trump #Investigations #Cooperation #LegalImplications #SecretService #GrandJury


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Meadows and Trump and the other people
that may have provided information, because there is a bunch of reporting
coming out now about who has spoken with the grand juries and it's
all super bad news for team Trump.
Okay, so first and foremost, there's reporting that suggests Meadows, Mark Meadows, has talked
to the grand jury.
Now it's worth noting that the reporting doesn't say which alleged crime Meadows was talking
about, which is just, I mean, that's just a sign of the Trump administration right there.
Meadows' attorney said, without commenting on whether or not Mr. Meadows has testified
before the grand jury or in any other proceeding, Mr. Meadows has maintained a commitment to
tell the truth where he has a legal obligation to do so.
Yeah, okay.
About eight months ago, I'll put the video down below, there was kind of a detailed discussion
on why Meadows was incredibly likely to make the decision of, hey, you know, I have a very
vested interest in helping out the special counsel and, you know, doing everything I
should with DOJ and, you know, really, really coming forward.
And that vested interest being not a love of the truth, but because of Meadows' position
within the Trump administration, anything that Trump did, Meadows is probably also guilty of.
So him deciding to cooperate, if that is indeed what happened, that's not a huge surprise.
I will say you have now seen that material again.
The other set of people that have reportedly spoken to the grand jury are
about two dozen Secret Service agents. As we talked about during the January 6th
committee hearings and a couple of times after that, when you are talking about
incredibly powerful people, when you're talking about those people with staffs,
your best evidence is not going to come from other high-profile people.
People at that level, they view other people who are high-profile as their equals.
They take precautions. They engage in basically operational security when it comes to them.
They don't with the help. The best evidence doesn't come from the names you know.
It doesn't come from the people that are in like a photograph of you know, this is Trump's inner circle
No, it comes from the people who are just outside the frame of that photograph holding the phones
comms people housekeepers
assistance
security personnel
They know everything and in many cases it is their job to
to document and record meetings, times, what was said, what might be a threat, all kinds
of things.
If around two dozen Secret Service agents fully complied, which is their reporting,
and talked to the grand jury, if they were even remotely forthcoming, anything that Trump
did is now on record because they knew. Their job is to basically make sure that
somebody is with him at all times. Somebody's within earshot. They know
everything. I would also suggest that given Smith and how thorough he appears
to be proceeding. I'm going to suggest he has also talked to housekeepers,
bellmen, concierge, all of those people at Trump's club. These are probably going
to be the most damaging witnesses against Trump. This is the information
that would establish timelines and intent again the scope of the discussions
isn't really known but the the likelihood that all of the Secret
Service agents withheld is pretty slim especially when you're talking about a
case involving national security and these people need their clearances.
Meadows, again, has a pretty personal interest in providing accurate
information as well. So I'll put the link to the other video down below but that's
where we're at right now. This testimony has occurred at various points in the
past. It's not new testimony. The Secret Service testimony, as I
understand it, has occurred over the last two months, and we're
just finding out about it. Again, the information starting
to come out the way it is suggests we're near the end of
this.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}