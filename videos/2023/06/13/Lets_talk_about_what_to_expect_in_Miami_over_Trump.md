---
title: Let's talk about what to expect in Miami over Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Ix-X_7cTdDI) |
| Published | 2023/06/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Law enforcement sources do not have any specific credible threat in Miami, but they anticipate large rallies that may escalate.
- People are advised to avoid the area in Miami due to the possibility of things getting out of hand.
- Despite the lack of social media posts seen before other events, there is a risk of things escalating.
- Emotions are running high among supporters who have faith in Trump and his rhetoric, viewing him as anti-establishment.
- Some supporters may not be looking at objective reality, potentially leading to disruptive actions.
- The more militant the rallies become, the more likely it is that the judge may view the situation as a societal risk and keep Trump in custody.
- Mixed messages from supporters and warnings to stay civil indicate an understanding of the risks involved.
- Supporting these rallies poses a risk of them spiraling out of control.
- Law enforcement entities are prepared to respond if anything goes wrong in Miami.
- Overall, it is advisable to avoid the area in Miami due to the uncertain and volatile nature of the situation.

### Quotes

- "The more militant the rallies, the more likely that they get the exact opposite result of what they want."
- "Supporting these rallies poses a risk of them getting out of hand."

### Oneliner

Law enforcement anticipates rallies in Miami escalating, urging people to avoid the area due to the volatile situation and risks involved.

### Audience

Miami residents and spectators

### On-the-ground actions from transcript

- Avoid the area in Miami (suggested)
- Stay informed about the situation (implied)

### Whats missing in summary

Insights into the potential consequences of escalating rallies and the importance of understanding supporters' perspectives.

### Tags

#Miami #Rallies #TrumpSupporters #LawEnforcement #RiskManagement


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about what to expect
in Miami, the information that we have,
because a whole bunch of questions came in
about whether or not people in Miami should avoid the area.
So according to the law enforcement sources at this point,
they don't have any specific credible threat.
That's the information they have.
That being said, they are anticipating large rallies that may get out of hand.
Do you have a reason to go down to that area today?
I mean, there's not much down there.
I would avoid it if it was me, or I would try to stay out of the area if you normally
drive through that area to go somewhere.
Take a different route.
I don't foresee.
I haven't seen any of the type of social media posts that preceded other events.
But that doesn't mean that something can't get out of hand.
So if it was me, I would just avoid the area.
I would stay out of there.
Now you have law enforcement saying no specific credible threats.
You have them saying that they expect rallies.
The numbers in the estimates vary wildly.
They are all over the place.
in the thousands or maybe even 10,000 others that are generally more plugged
into the area are looking and thinking 1,000 and then you have some that are
not law enforcement that are more of non-government entities that monitor
this kind of stuff they're not even expecting a thousand but it doesn't take
a lot of people to send things sideways. And as we have talked about, emotions are
running high. There are a lot of people who put a whole lot of faith in this man,
in Trump, and they truly believe all of their rhetoric. And they're not, they're
They're not looking at the indictment, they're not looking at the photos, they're not looking
at the transcripts or any of that.
What they see is, you know, big government coming after their guy who they believe is
anti-establishment.
Yes, I understand the irony of anti-establishment being defined as, you know, the former president
billionaire, but that's what they believe. And when you're talking about
something like this and you're trying to gauge their actions, you have to look at
what they believe, not what you believe, not even objective reality. You have to
look at it from their point of view. There are a lot of people who fell down
a whole lot of rabbit holes over the last few years. Some of them believe
things that are objectively false, and it might drive them to do things that are
disruptive to the normal goings-on in the area. So if it was me, I would avoid it. I
would avoid the area, that way you don't get wrapped up in it. The really funny
part about all this is that if things do go sideways, it will have the exact
opposite result of what they want. What they want is for him to be released. If
the judge perceives it as a societal risk for him to have access to social
media and be out making speeches, please understand that the judge would be well
within their bounds to remand him and keep him in custody. It's not a likely
outcome, but the wilder the protests, the more likely it becomes. You know, he's a
former president of the United States, a judge is not going to want to put him in
detention, not going to want to put him in jail, but if the alternative to that
is an emboldened movement of people who might be destructive, the judge may view
that is the only option. So, in a weird way, the more militant the rallies, the
more likely that they get the exact opposite result of what they want because
they're not going to be at the level where they actually scare the
federal government into letting him out if they were going to hold him. That
won't occur but it might they might be able to scare them to the point of
saying him being out as a continuing danger. So I think that's why you have a
lot of mixed messages coming from his supporters and you have a whole lot of
people who normally use a lot of dog whistles to try to kind of you know rile
people up saying be civil, be calm, be legal, you know all of that because they
understand the risks. It's realistically it's a bad move for the the Trump team
to support these rallies because there's always a risk of them getting out of
hand. But that's where we're at and there are a whole lot of law enforcement
entities in that area that would respond to anything going going sideways. So
short version, yeah if you're in Miami I would avoid the area. There's no reason
to risk it. If you're just a spectator from outside the area and wondering
what's going to happen, the assessments are all over the place and given the
volatile nature of some of his supporters, you can't rule out the
possibility of it going bad. At the same time, his support is dwindling and given
the fact that he wasn't entirely supportive of those people who went to
bat for him on the sixth, there may be a lot of people who are supportive of him
online who don't see the value in risking themselves for him because they
know that he's not gonna back them up. So anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}