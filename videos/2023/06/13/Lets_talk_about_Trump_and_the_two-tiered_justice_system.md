---
title: Let's talk about Trump and the two-tiered justice system....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=VYpsDUfGbgA) |
| Published | 2023/06/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans claim Trump is being treated differently under the justice system, and they're right.
- Trump is facing charges in a unique way compared to others, with special accommodations such as being told to come in on a specific day.
- In contrast, regular people under the justice system face forceful arrests and immediate custody for similar charges.
- Despite facing charges, Trump is expected to walk out after his arraignment due to his resources and status.
- The justice system seems to favor the wealthy and powerful, providing them with leniency and special treatment.
- Critics who claim Trump is being targeted more harshly are overlooking the special treatment he has received.
- Trump could have avoided much of this scrutiny by simply returning the requested documents.
- The two-tiered justice system benefits the former president, allowing him privileges that others wouldn't receive.

### Quotes

- "There is a two-tier justice system, but it is working in favor of the former president."
- "They think that you are just totally incapable of acknowledging all of the favors, all of the courtesies that have been extended to the former president."

### Oneliner

Republicans admit Trump faces a different justice system, with special treatment favoring the wealthy and powerful, while critics claim he's targeted unfairly.

### Audience

Legal reform advocates

### On-the-ground actions from transcript

- Challenge disparities in the justice system (implied)
- Advocate for fair treatment under the law (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of how Trump is being treated differently under the justice system compared to regular individuals.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and the idea of a two-tier justice system.
Because right now, Republicans are saying
that it's different.
They're going after him in a different way,
and it shows that the justice system is, well, it's off.
It's not fair.
Things are being done differently with the former president.
and I have to be honest,
for once they're right. They are.
I know nobody wants to admit that, but it is being done differently.
They are going after him
in a decidedly different way. Name
another person charged
with willful retention, charged
with things under the Espionage Act, that has constant contact with foreign
nationals,
that was told, hey just come on in on Tuesday, we'll take care of it then.
Because that's different.
Normal people, those under the normal justice system tier, yeah they get their door kicked
in, they have an MP5 shoved in their face, they get drug out of bed, cuffed and taken
and they are held. Yeah that's different, that's different. How many people could
have had that quantity of documents at their home during a search after saying
they didn't have any and not be taken into custody at that moment? No, not many
right? Just people in that other tier.
Tomorrow he's going to
have his arraignment and a man
with a whole bunch of resources with his own plane
is more than likely going to walk out of there.
He's not going to be remanded to custody. They're not going to put the former
president down there in Miami, FDC. Right? That is the two-tier system. It exists,
but it's not working in opposition to the former president. It's working in his
favor. They're going after him hard. They're hitting him hard. No, they are
hitting him with a Nerf bat. Everybody else that would be accused of similar
crimes, they don't get a Nerf bat. They get that bat from the Walking Dead with
the barbed wire wrapped around it. There is a two-tier justice system, but as is
typically the case, it works in favor of the wealthy and powerful. When you hear
people tell you that, oh it's a two-tiered justice system and they're going after him
so much harder than they would anybody else. All they are saying is that they think you
are ignorant. They think that you are just totally incapable of acknowledging all of
the favors, all of the courtesies that have been extended to the former president. Keep
Keep in mind this has been going on a long time and all he had to do was return the documents.
If he had done that, this all probably would have gone away, but he didn't.
I would venture to guess that most people wouldn't have even been extended that courtesy.
Not with that quantity.
Not for that length of time.
There is a two-tier justice system, but it is working in favor of the former president.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}