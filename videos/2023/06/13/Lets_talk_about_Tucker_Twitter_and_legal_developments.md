---
title: Let's talk about Tucker, Twitter, and legal developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OK7xsBQ5eaU) |
| Published | 2023/06/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tucker Carlson received a cease and desist order from Fox News for posting content on Twitter, violating their contract.
- Fox News claims sole rights to Tucker's content until December 31st, 2024, and he needs to stop posting on Twitter since he's still under contract.
- Tucker argues it's a free speech issue, portraying himself as a victim, while his supporters believe it's an attempt to silence him until after the election.
- If Tucker continues with his planned episode today, it suggests he wants to go to court with Fox. If he doesn't air the content, he might be aiming to return to Fox.
- Twitter's future may be impacted as Tucker's presence influences the platform's dynamics, potentially turning it into a right-wing echo chamber.

### Quotes

- "I think they're going to end up being the biggest loser here because I don't actually think anybody involved, Tucker or Fox, wants Tucker on Twitter."
- "Tucker has gotten a cease and desist order from Fox News."
  
### Oneliner

Beau says Tucker Carlson's cease and desist order from Fox News over Twitter posts could lead to a court battle, impacting Twitter's dynamics significantly.

### Audience

Social media users

### On-the-ground actions from transcript

- Stay informed about the developments in the Tucker Carlson and Fox News issue (implied).

### Whats missing in summary

Insight on the potential long-term impacts of Tucker Carlson's situation on media platforms and free speech.

### Tags

#TuckerCarlson #FoxNews #CeaseAndDesist #Twitter #FreeSpeech #MediaImpact


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Tucker Carlson
and Fox and Twitter in a cease and desist order.
And I want y'all to stop comparing his set to mine.
Okay, so,
Tucker has gotten a cease and desist order from Fox News,
basically saying that he shouldn't be putting
his content on Twitter,
that that is a violation of their contract and their position basically is that they have
they have sole rights to his content through December 31st, 2024 and that he needs to
stop because he's still being paid and that by him putting his monologues on Twitter,
he's in violation of the contract. For Tucker's side of things, he's basically
saying that it's a free speech issue, which I don't know how that's going to
play out when it comes to contract law, and he's kind of casting himself as the
victim. His supporters are saying, look, they want to silence him until after the
election. It's a plot, obviously. No, that's when his contract expires. If his
contract had been through the end of 2025, that's when they would have wanted
him to stay quiet till. I don't think they're trying to silence him over his
election views. If anything, those who are in Fox's management probably want his
views out there for the election, especially since they wouldn't be held liable for him.
But he's under contract.
Now when are we going to find out whether or not he's going to do what he's supposed
to as far as the cease and desist order?
Today.
My understanding is that his next episode is today, Tuesday, and it is supposed to be
his response to former President Trump's indictment and he's going to give his
much anticipated, certainly very well-informed and not at all, you know,
hyperbolic take on the former president's indictment. So what we can kind
to guess from here is that if Tucker goes ahead with the episode that is
currently planned that he actually wants to go to court with Fox. If he decides to
not produce that content or not air it then it's probably a sign that he's
really kind of angling to get back on Fox. At least that would be my guess. So
we'll have to wait and see what he does but either way it's probably the
beginning of a long saga and it matters a whole lot to Twitter because as
as Twitter devolves into, you know, a right-wing echo chamber, the presence of
Tucker is going to matter for Twitter. I think they're going to end up being the
biggest loser here because I don't actually think anybody involved, Tucker
or Fox, wants Tucker on Twitter. I think he would like a more dignified platform.
is probably the way he's looking at it. But we'll have to wait and see. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}