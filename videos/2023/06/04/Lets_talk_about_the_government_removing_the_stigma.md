---
title: Let's talk about the government removing the stigma....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1vqpeV7i-cw) |
| Published | 2023/06/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of UAPs and UFOs, pointing out the increasing seriousness with which the US government is treating the subject.
- The government is interested in investigating these cases despite 98% of them being explainable.
- The focus is on removing the stigma associated with reporting such sightings to encourage pilots to come forward with strange occurrences.
- Beau suggests that the government's interest in these sightings may be more related to defense against foreign threats rather than extraterrestrial ones.
- The goal is to have commercial, private, and military pilots report anything unusual to potentially counteract surveillance threats.
- The government's efforts to destigmatize reporting could help in identifying and countering new technologies in the airspace and waters around the US.

### Quotes

- "They want to remove the stigma when it comes to reporting this stuff."
- "I think that the reason the government is starting to show more interest in this and especially now that they're trying and actually using the word trying to get rid of the stigma."
- "They're worried about surveillance over flights. They're worried about drones."
- "They want to use the massive amount of naval traffic and air traffic that the US has as an additional hedge against surveillance over flights."
- "That's my guess. Or maybe they're just, you know, saying, hey, maybe there are aliens out there."

### Oneliner

Beau delves into the US government's renewed interest in UAPs and UFOs, aiming to destigmatize reports for potential defense purposes rather than a search for extraterrestrial life.

### Audience

Defense analysts, pilots, enthusiasts

### On-the-ground actions from transcript

- Report any unusual sightings immediately to relevant authorities (suggested)
- Encourage open communication among pilots and military personnel about strange occurrences in airspace (implied)

### Whats missing in summary

Beau's engaging delivery and further insights can be best experienced by watching the full transcript.

### Tags

#UAPs #UFOs #GovernmentInterest #DestigmatizeReporting #DefenseStrategy


## Transcript
Well, howdy there internet people, it's Bo again. So today we are going to
Talk about
UAPs, UFOs. That's what we're going to talk about
because I got a question and
if you don't know the US government is starting to take this
concept a little bit more seriously and
one of the things
that has prompted a lot of people to have questions is they look at these cases, the government looks at them and
98% of them are completely explainable.
But they want to keep
the
investigations going, they want more data, and a word that keeps coming up is stigma.
They want to remove the stigma when it comes to reporting this stuff.
And the question is, if everything's pretty much explainable, why do they want to keep
looking into it and why do they care about the stigma?
Being explainable doesn't necessarily mean it's something that's supposed to be there.
I think a lot of people are looking at this through the lens of X-Files.
They might should be looking more through the lens of air defense or naval defense as
I guess this now includes undersea entities.
They want to remove this stigma, my guess is because they want commercial pilots, private
pilots, military pilots, to be very free in coming forward and saying, hey I saw
something really weird and it wasn't supposed to be there. Not because they
think it's from outside of this planet, but it might be from outside of the
United States. I think that the reason the government is starting to show more
more interest in this and especially now that they're trying and actually using
the word trying to get rid of the stigma. I think it's because they want people to
report things that might be of foreign origin rather than alien origin. They're
worried about surveillance over flights. They're worried about drones. They're
worried about naval drones and they want to basically create a situation where
you see something, you say something. They want to use the massive amount of
naval traffic and air traffic that the US has as an additional hedge against
surveillance over flights, which happen all the time, and there's a lot of new
technology, some of which they may not be totally up to speed on, and this would
help identify it and therefore counter it. That's my guess. That's especially
considering they're actively trying to get rid of the stigma. They want more
information. They want people calling them and saying, hey I saw this weird
balloon. Even if it turns out to be nothing, they want to be aware of it
because they're starting to look at the airspace and the waters around the U.S.
as an information battle space for surveillance and they want to be able to
counter it. That's my guess. Or maybe they're just, you know, saying, hey, maybe
there are aliens out there. You shouldn't feel bad about calling us and telling us.
But generally speaking, when the United States government is pursuing something
of a scientific endeavor and they're willing to burn a bunch of resources to
say hypothetically check out a bunch of reports, it generally has to do with
defense, not necessarily science for, you know, information's sake. Anyway, it's just
a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}