---
title: Let's talk about Ukraine, Kuwait, and supplies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=5LbmOIq0P9g) |
| Published | 2023/06/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Outlines issues with supply and drawdowns in Ukraine, warning those in the military reserves and National Guard to pay attention.
- Reveals that equipment being sent to Ukraine is not newly purchased but part of existing U.S. military material from pre-positioned stock worldwide.
- Details concerning failures in maintenance: none of the M777 howitzers were mission ready, and only three out of 29 Humvees were.
- Points out a contracting company responsible for maintenance and the army supply group failed in their duties.
- Warns military personnel and maintenance providers worldwide to expect increased scrutiny after this failure.
- Predicts delays in equipment reaching Ukraine due to necessary fixes, but most issues can be quickly resolved.
- Anticipates thorough inspections of all pre-positioned stocks, especially those managed by contracting companies.

### Quotes

- "A 100% failure rate is just, I mean that is just out of, that should be out of the realm of possibility."
- "Be ready for the world to fall in on you."
- "It was just all bad."

### Oneliner

Issues with equipment readiness in pre-positioned stock may delay supplies to Ukraine, prompting global military maintenance scrutiny.

### Audience

Military personnel, maintenance providers

### On-the-ground actions from transcript

- Inspect and maintain equipment thoroughly to ensure mission readiness (implied)
- Stay prepared for heightened accountability and scrutiny in maintenance practices (implied)

### Whats missing in summary

Importance of maintaining military equipment for global operations. 

### Tags

#Ukraine #Military #SupplyChain #Maintenance #GlobalSecurity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about supply
and drawdowns in Ukraine
and how a whole bunch of people are about to be in trouble.
If you are in the reserves, if you are National Guard,
if you are supply, if you are part of any organization
within the US military that has to keep things mission ready,
pay attention because heads are about to roll.
Okay.
So,
a lot of the equipment that is being sent to Ukraine,
it's not being purchased.
You know, you hear the dollar amounts and you assume that
they're calling up
a production company
and getting them to make new stuff.
That's not actually happening.
A lot of the stuff is taking place through what's called a drawdown,
which is where existing U.S. military material
is being shipped over.
The United States has stuff all over the world pre-positioned
in case it ever needs to be used.
This stuff is supposed to be maintained to be
mission-ready at all times.
So the U.S. doesn't have to fly in a bunch of equipment.
They just have to fly in the troops.
The vehicles are already there.
Okay, so equipment from the pre-positioned stock in Kuwait was headed to Ukraine.
One hundred percent of the M777s were not mission ready, all of them.
Some of them had improperly aligned breech blocks, danger to the operator.
These are howitzers.
Of the Humvees, three out of 29 were mission ready.
My understanding is that a contracting company was supposed to be doing the maintenance
on this and the regular army supply group that was supposed to be watching them were
obviously not keeping up with the accountability on it.
The short version here is that all over the world, if you are in the military, if you
are part of a company that provides maintenance, be ready for the world to fall in on you.
Because after this, rest assured, everything is going to be checked all over the place.
A 100% failure rate is just, I mean that is just out of, that should be out of the realm
of possibility.
A lot of the fluids in them hadn't been replaced or changed.
It was old.
In some cases, it appeared to be reused.
It was just all bad.
So this is going to lead to two things in the short term.
One, some of the equipment headed to Ukraine is going to be at least somewhat delayed.
Most of this stuff doesn't take long to fix.
And given the route it's taking to get to Ukraine, it's going to go through places
where the parts needed are just going to be like hanging out.
So they will probably be checked on the way.
Like once they get to Europe, they'll be checked and fixed and head in.
And then obviously the other thing is that all pre-positioned stocks are probably going
to have to be examined and inspected particularly if they are under the
current care of a contracting company. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}