---
title: Let's talk about the ruling in Tennessee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZoxDx40NNw4) |
| Published | 2023/06/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A federal judge in Tennessee deemed a ban on certain shows unconstitutionally vague and substantially overbroad.
- Most bans on these types of shows are boilerplate, with the same goal of being unconstitutional.
- States must provide an incredibly compelling reason to ban such shows, tailored narrowly to that reason.
- Over 500 bills targeting the LGBTQ community have been introduced, but most are likely to be struck down in court.
- The government must show a significant reason to intervene in matters of speech, including LGBTQ expression.
- Despite the ruling in Tennessee, the fight against discrimination is far from over.
- The state's lack of compelling interest in these bans suggests they will continue to be struck down in court.
- Efforts to scapegoat the LGBTQ community are likely to persist to divert attention from other issues.
- Blaming marginalized communities for institutional issues is a diversion tactic by those in power.
- The fight for equality continues, even after legal victories.

### Quotes

- "States must provide an incredibly compelling reason to ban such shows, tailored narrowly to that reason."
- "The fight against discrimination is far from over."
- "Blaming marginalized communities for institutional issues is a diversion tactic by those in power."

### Oneliner

A federal judge in Tennessee strikes down a ban on certain shows, revealing the unconstitutional nature of most bans targeting LGBTQ communities, but the fight against discrimination continues.

### Audience

Legal advocates, LGBTQ activists

### On-the-ground actions from transcript

- Support organizations advocating for LGBTQ rights (suggested)
- Stay informed and engaged in legal battles against discriminatory legislation (implied)

### Whats missing in summary

Detailed examples of discriminatory legislation and the potential impacts on LGBTQ communities.

### Tags

#LGBTQ #Discrimination #LegalRights #Equality #Activism


## Transcript
Well, howdy there, internet people, let's bow again.
So today we are going to talk about Tennessee
and a decision that was made there
and what to expect in the future
because we're probably going to see this play out
over and over again in quite a few different states.
Okay, so what happened?
A federal judge in Tennessee
basically looked at that ban that said you know you can't have these kinds of
shows and all of that stuff, and said that it was unconstitutionally vague and
substantially over brawled, okay. That ruling is what you can expect on pretty
much all of it. That terminology. Most of the the bans are, they're kind of boilerplate.
They use a lot of the same language. They have the same goal. The goal is unconstitutional.
The state has to show an incredibly compelling reason to ban a drag show and they have to
tailor the legislation to be incredibly narrow and focus just on their compelling reason.
Realistically they're not going to be able to show the compelling reason for the state
to intervene, and any legislation that they draft that is narrow enough to address any
potential real reason that they might be able to demonstrate legally is going to be so narrow
they're not going to be interested in it.
So this ruling is probably going to happen over and over again in various states.
It doesn't mean they're going to stop trying, at least not anytime soon, because last time
I checked, there were more than 500 bills that have been introduced to regulate, curtail,
other, and kick down at the LGBTQ community.
Most of them are going to suffer this fate when they get to court.
The state really doesn't have an interest in this, and there has not been a Supreme
Court that has held there was some varying standard when it comes to the type of speech.
This is going to be treated the same way as political, religious, scientific, any other
kind of speech.
So the government is going to have to show an incredibly important reason for them to
intervene and they don't have one.
Now one thing I do want to point out because a whole bunch of people are certainly going
to say, well this is just some liberal federal judge, no this was a Trump appointee.
This was a Trump appointee.
I know that if you are part of this community, the win in Tennessee, it's good, you're happy
about it, but it doesn't feel like the end because it's not.
I'd be surprised if Tennessee didn't try to fight it, but realistically, assuming the
constitution matters at all in federal courts, this is going to continue to be struck down.
And it's going to be struck down in most places because the language in the legislation is
pretty much the same, and the state really doesn't have a compelling interest here.
It's not the end, but it's the beginning of the end of this.
Believe me, they'll find some other way to try to scapegoat that community because they
want to draw attention away from their own failures.
So they want to give their base somebody to point and stare at.
Blame them.
Once again, a group of people that has less institutional power than you do, is never
are responsible for the institutional issues that you are facing. They're not
responsible for the position you find yourself in. The people telling you to
blame them are.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}