---
title: Let's talk about Ken Paxton's impeachment trial date....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4H1gB5mbmTo) |
| Published | 2023/06/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas Attorney General Ken Paxton was impeached by a Republican-controlled house, with allegations of serious wrongdoing.
- Paxton denies any wrongdoing, but the Senate trial is set for September 5th.
- Paxton's wife, State Senator Angela Paxton, will not be voting in the impeachment trial.
- The political makeup of the Senate, with 19 Republicans and 12 Democrats, plays a significant role in the potential removal of Paxton.
- If half of the State Senate Republicans vote to convict and remove Paxton, he will be gone.
- Despite the allegations, the possibility of Paxton being removed is high due to the political dynamics.
- The rarity of impeachment in Texas history adds weight to the decision facing State Senators.
- Rules were created to force Paxton's wife to recuse herself from voting, indicating a strong likelihood of Paxton not remaining as Attorney General.
- The political implications and historical significance of this impeachment trial are substantial.
- The situation may lead to Paxton's removal, considering the unique circumstances surrounding this impeachment in Texas.

### Quotes

- "Paxton, for his part, has denied any and all wrongdoing."
- "If half of the State Senate Republicans in Texas vote to convict and remove, he's gone."
- "The recusal, to me, signals that there is an incredibly strong chance that after this trial Paxton will not be Attorney General Paxton."

### Oneliner

Texas Attorney General Ken Paxton faces potential removal through a Senate trial, with political dynamics and historical rarity influencing the outcome.

### Audience

Texans, Political Observers

### On-the-ground actions from transcript

- Contact your State Senators to express your views on the impeachment trial (suggested)
- Stay informed about the proceedings and outcomes of the Senate trial (suggested)

### What's missing in summary

Analysis of specific allegations against Ken Paxton and potential impacts of his removal or continuation as Attorney General.

### Tags

#Texas #Impeachment #KenPaxton #SenateTrial #PoliticalDynamics


## Transcript
Well, howdy there internet people, let's bow again. So today we are going to talk
about Texas
and the Attorney General out there,
the proceedings, and how things are moving forward.
If you are unaware of this situation,
the Attorney General of Texas, Ken Paxton,
was impeached.
He was impeached by a Republican
controlled house
and
it's moving forward to the Senate.
The allegations are pretty serious.
Paxton, for his part, has denied any and all wrongdoing.
But it was Republicans
who impeached a Republican Attorney General.
So that's what's going on.
At this point, they're allegations.
It moves to the Senate for trial.
If he is convicted at this trial,
The odds are he will be removed.
They have set the date for the trial.
It is September 5th.
One of the other outstanding issues that needed to be resolved was what State Senator Angela
Paxton was going to do while this was happening.
His wife, the Attorney General's wife, is a state senator.
It appeared earlier this week that she intended on voting in her husband's impeachment trial.
The rules apparently bar her, so she will not be voting.
Okay, so the question that is really kind of on everybody's mind, does he stand any
chance of being removed?
We know that the house, the Republican controlled house, impeached him, but then you have the
trial.
Now you do have to entertain the idea that at the trial all of the allegations are proven
to be untrue.
But what people typically look at in a situation like this is the political makeup.
And the makeup of the Senate, under normal circumstances, is 19 Republicans and 12 members
of the Democratic Party.
Now the Democrats, they're probably not going to stand in the way of a Republican Attorney
General being removed.
Not just because it's good for them politically, but also because it was a Republican House
that impeached him.
the Democratic Party's standpoint, they very well may be looking that this is like an internal
Republican thing, and y'all can solve this amongst yourselves.
And since the Republican House voted to impeach, we're going to honor their wishes.
It may be that simple.
That leaves 18 Republicans, though, because Paxton's wife won't be voting.
They need two-thirds, so if half of the State Senate Republicans in Texas vote to convict
and remove, he's gone.
That's looking at it through a purely political lens, not taking any of the facts into account.
Just politics.
Generally speaking, yeah, he stands a pretty good chance of being removed, which is surprising
because in the entire history of the state of Texas, I think it's only happened twice.
It's only happened two times before.
It is not a process that is used there very often, and I think that is probably going
to weigh pretty heavily on these state senators because they know that this is going to be
looked at.
This is going to be a history books thing.
It's also important to note that they created rules and basically kind of forced his wife
to recuse herself.
If you were the Republican Party and you were plotting an out for the Attorney General,
the one way you could do it and not suffer a whole bunch of backlash would be for the
deciding vote to be his wife.
That directs all of the backlash to them.
The recusal, to me, signals that there is an incredibly strong chance that after this
trial Paxton will not be Attorney General Paxton.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}