---
title: Let's talk about Biden, the rails, and sick days....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=EM6jMtG_MB8) |
| Published | 2023/06/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration intervened in a labor dispute on railroads regarding paid sick days.
- Unions were pushing for paid sick days and short notice sick days, but negotiations weren't progressing.
- Biden tried to help the unions but didn't fully meet their demands, risking a rail strike.
- Biden considered using Congress's power to break the strike, angering union supporters.
- Beau critiqued this as the first major mistake of Biden's administration.
- Despite promises for paid sick days, no concrete action or legislation was seen initially.
- Eventually, Biden's administration quietly worked on securing paid sick days for railroad workers.
- Biden and Bernie Sanders played key roles in ensuring agreements with major freight carriers for sick days.
- Unions, including IBEW, now have significant benefits like paid short notice sick days and more.
- Beau acknowledges his initial skepticism but admits he was incorrect in doubting Biden's commitment.
- Biden and Bernie continue to work behind the scenes to assist other unions in achieving their goals.

### Quotes

- "I believe in paid sick days and I think railroad workers should get that, blah, blah, blah."
- "Biden and Bernie appear to have really, really came through."
- "If I'm reading between the lines on all of this correctly, Biden and Bernie appear to have really, really came through."
- "I was openly skeptical and I made kind of a very direct statement that I did not think that the Biden administration was actually going to continue to work on this."
- "It's just a thought."

### Oneliner

Biden's administration quietly secures paid sick days for railroad workers, proving skeptics wrong and aiding unions behind the scenes.

### Audience

Railroad workers, union supporters

### On-the-ground actions from transcript

- Support and advocate for unions in your community (implied)
- Stay informed about labor disputes and negotiations (implied)

### Whats missing in summary

The full transcript gives a detailed account of how behind-the-scenes efforts by Biden and Bernie helped secure significant benefits for railroad workers, showcasing a positive outcome despite initial skepticism. 

### Tags

#Biden #RailroadWorkers #UnionSupport #PaidSickDays #LaborDispute


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today we are going to talk about Biden
and railways and sick days
and something I called the first real mistake
of his administration and an update on that story.
Okay, so if you have no idea what happened
and where all of this is coming from,
not too long ago, there was a labor dispute
on the railroads.
The unions wanted a number of things, something that was very, very important to them was
paid sick days and the ability short notice paid sick days.
And that, that normal union struggle wasn't moving along.
The Biden administration stepped in, tried to negotiate and try to help on behalf of
the unions, got them some stuff, but did not get them what they wanted.
And it set up a situation where it was quickly moving towards a rail strike that would have
just absolutely ground the US economy to a halt.
And the Biden administration got on board with using the power of Congress to break
the strike, okay? When he did that, it angered a whole lot of people who are
supportive of unions, Koff-Koff, me, and a whole bunch of other people. And it was
at that time that I called it the first real mistake of his administration.
And it was at that time that he made this promise, politician's promise, standard stuff.
I believe in paid sick days and I think railroad workers should get that, blah, blah, blah.
And we're going to continue to work to get it for them.
Yeah, I never saw any legislation.
I never saw any action.
Okay, so now that I've said all that, I want to read this.
We're thankful that the Biden administration played the long game on sick days.
stuck with us for months after congress imposed our updated national agreement. Without making
a big show of it, Joe Biden and members of his administration and transportation and
labor departments have been working continuously to get guaranteed paid sick days for all railroad
workers. It goes on to talk about how they know the members of the union weren't real
happy with that first agreement, but through it all we had faith in our friends in the
White House and Congress who would keep pressure on the railroad employers to get us the sick
day benefits we deserve.
Short version, if I'm reading between the lines on all of this correctly, Biden and
Bernie, Biden and Bernie appear to have really, really came through. IBEW, big, big
union. I want to say they now have agreements with, I think they're the four
largest freight carriers in the country. And those agreements have, I want to say
is four paid short notice sick days with the ability to convert three personal
days to sick days and if they it's not like a use it or lose it thing if they
don't use the four designated sick days during the year they can convert it to
cash or retirement benefits, whatever. I mean, that's a win. That's a win. I was openly skeptical
and I made kind of a very direct statement that I did not think that the Biden administration
was actually going to continue to work on this. I appear to have been incorrect,
so I feel like that that's that's worth mentioning since I I did openly state
that opinion now there are other unions there are other agreements there are
other things going on it is it's worth noting that if I'm reading this
correctly, the same people, Biden and Bernie, are working to help other unions
achieve their goals as well on the rails. I think it's important to draw
attention to this because a whole lot of us were very critical of this move and
this is something that because it is no longer national news, I don't feel like a
a whole lot of people are going to follow up on especially since those of
us who had the most let's just call it the the most militant opinion of this
move we were wrong so I know I don't feel like it's going to be covered as
well as it should because those people who were supportive of the move of the
deal to begin with, they're not following it anymore because they feel
like they already got what they wanted. Those who were opposed to it, I mean
most don't really have incentive to cover it, but it does appear that the
Biden administration through and with Bernie have been working behind the
scenes to help the the unions and to put pressure on the railroad owners. Anyway
It's just a thought.
So that's a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}