---
title: Let's talk about learning from ERCOT's advice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pbvPzedYdAM) |
| Published | 2023/06/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- ERCOT advised Texans during a heat wave to limit energy consumption by not running large appliances and raising thermostats.
- Many people complied with the advice to prevent strain on the energy system.
- Experts provide recommendations like ERCOT to avoid larger problems down the road.
- Recommendations are voluntary and aim to prevent future issues.
- Some individuals turn climate advice into a culture war issue, creating unnecessary conflict.
- Pushing narratives against responsible actions distracts from addressing real issues.
- Political motives sometimes drive opposition to sensible recommendations.
- Changes to energy infrastructure and consumption are inevitable for the future.
- Listening to experts and acting on their advice is necessary to prevent future crises.
- Climate change demands collective action to secure a better future for the next generation.

### Quotes

- "They're trying to provide you with information so you can make an informed decision."
- "You have to stop allowing people who provoke you into reaction, people who play on your emotions."
- "It's going to take everybody doing their part."
- "Those who use climate issues for culture war probably shouldn't be in office."
- "Do this little thing now to avoid a problem down the road."

### Oneliner

ERCOT advised Texans to limit energy consumption voluntarily during a heat wave, showcasing the importance of acting on expert advice to prevent future crises.

### Audience

Texans, Climate Advocates

### On-the-ground actions from transcript

- Follow recommendations from experts on energy conservation (implied)
- Act voluntarily to reduce energy consumption and mitigate strain on the system (exemplified)

### Whats missing in summary

The full transcript provides a detailed insight into the importance of heeding expert advice on climate-related issues and the necessity of collective action for a sustainable future.

### Tags

#ERCOT #EnergyConsumption #ClimateChange #ExpertAdvice #CollectiveAction


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about what we can learn from
ERCOT out in Texas.
That's the energy people out there.
It's kind of a blockbuster heat wave out there right now.
So ERCOT put out the advice to the people in Texas saying,
hey, don't run your large appliances.
Bump your thermostat up a couple degrees.
Otherwise, things are going to go bad.
too much strain on the system.
Now, there were a lot of people who weren't happy about that.
But they understood why that information was being provided
to them.
And a whole lot of people did it.
I would suggest that most probably
did something to limit their energy consumption.
Because they knew that if they didn't change their behavior,
that they would have a bigger problem down the road.
If nobody altered their behavior,
odds are the system would be strained too much,
power would go out and everybody would get really hot, right?
So when ERCOT did this,
they weren't coming for your thermostat, right?
They were just providing you the information you needed
to help mitigate an issue.
so you didn't have a larger problem down the road.
Pretty simple.
So the question is
why
when
people who are
experts
when it comes to
climate and climate mitigation
when they make recommendations
it turns into
They're coming for your whatever.
They're doing the exact same thing.
They're trying to provide you with information so you can make an informed decision, everybody
chip in a little bit, and avoid a larger problem down the road.
When they talk about stuff like this, it's voluntary, just like the ARCOT thing was.
It's a voluntary thing at first.
If we do this, it helps, right?
And then some Republican, generally, comes along and turns it into a culture war issue
and says, they're coming for your thermostat.
They're coming for your light bulb.
They're coming for whatever, your toilet.
And people turn it into a point of honor to not do the responsible thing.
You have to wonder why the individuals who push those narratives, the individuals who
come out there and turn it into some conspiratorial global plot to take away
your toilet that uses too much water, you have to wonder why they're focused on
that. Why they're not focused on a real issue, why they're not actually out there
pushing their policy, but rather just getting you to react to good advice.
It's interesting that in the GOP controlled state of Texas you didn't
have a whole bunch of people coming out saying that you know you shouldn't do
that. They're coming for your thermostat, they're coming for your AC. You didn't
have any of that right. A niche market didn't develop selling yellow flags with
thermostat on it that says come and take it. Didn't happen. Because it is
political. It is a way of providing hyperbolic information to their base, to
get them rallied up, to get them focused on something other than policy, just to
keep control of them, to keep them under their thumb. If you're mad
at some ill-defined,
unnamed group of people who are coming for your
toilet, or your stove, or your thermostat,
or your light bulbs, well
you're not paying attention to them.
We have to make changes. It's going to happen.
it doesn't matter if you want it to or not.
We're gonna have to make changes to our energy infrastructure.
We're going to have to make changes when it comes to consumption in general.
We're going to have to make a lot of changes in the coming years. And
if you don't do it voluntarily, much like you know,
bump it up a couple degrees voluntarily,
that'll be alright, but if you don't do it, your power is going to go out.
and you're going to be forced to bump it up a couple of degrees
because it's going to be off.
The same thing applies.
If you
want
to hand your kids
a similar standard of living,
you have to stop allowing
people who
provoke you into reaction,
people who play on your emotions,
people who try to control you by distracting you.
You have to stop letting them have that power.
You have to start listening to the people who know what they're talking about,
those people who are trying
to provide you the advice you need
so you can avoid a larger problem down the road.
When they say stuff like this,
it's not them
trying to upset your way of life
impose something on you, they're trying to stop a larger problem down the road.
With the ERCOT thing, it's, you know, almost immediate, so people see the
need. The Republican Party keeps its base so angry that they're always focused on
the immediate and they miss down the road. The slogans, the advice, it's not
new. Be kind, rewind, it's the exact same type of thing. Do this little thing now
to avoid a problem down the road. Everybody chips in, it makes everything
better. It's the same type of situation. Climate is one of the, probably the
largest issue our generation, this generation, is going to face and it's
It's going to take everybody doing their part.
Those who would try to use that as culture war stuff, they're probably not fit to be
in office because what they're doing is undermining the advice that is going to protect your kids
simply to get a vote.
Simply to maintain power and keep you under their thumb.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}