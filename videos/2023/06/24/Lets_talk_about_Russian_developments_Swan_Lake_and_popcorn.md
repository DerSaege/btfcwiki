---
date: 2023-08-06 06:26:01.617000+00:00
dateCreated: 2023-06-25 11:13:07.802000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about Russian developments, Swan Lake, and popcorn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7pjcKan2m94) |
| Published | 2023/06/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reports suggest Russian military troops struck Wagner, a private force, leading to tension.
- Wagner believes the strike was intentional, even though the source hasn't been confirmed.
- The boss of Wagner hinted at an open call to arms against the traditional Russian military leadership.
- Russia's internal security has started a criminal probe into the boss of Wagner's statements.
- There are reports of troop movements that could signify tension escalating between Wagner and the conventional military.
- Major buildings in Russia have reportedly increased security, potentially related to governing stability.
- The situation is not fully confirmed, with some possibilities of de-escalation or spiraling out of control.
- Back channel pressure and phone calls could potentially resolve the situation without major issues.
- There's uncertainty about whether the conflict will intensify or resolve peacefully.

### Quotes

- "Does this mean that you should be popping your popcorn to get ready to watch Swan Lake? No, not necessarily."
- "It is also possible that it completely spirals out of control."
- "But if you wanted to watch the ballerinas warm up, you might want to pop your popcorn for that."

### Oneliner

Reports of a strike between Russian military and Wagner private forces spark tensions and potential conflict escalation in Russia.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor the situation closely and stay informed (implied)

### Whats missing in summary

Context on potential implications and broader consequences of the conflict escalation in Russia.

### Tags

#Russia #MilitaryConflict #Tensions #GlobalConcerns #SecurityIssues


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about
what's happening in Russia.
We're gonna talk about what's confirmed,
what's reported, what's possible,
what's likely, where it goes from here,
if anywhere, we will talk about whether or not
we're gonna be watching Swan Lake really soon.
Okay, and I've referenced that in the past
and every time I get questions.
If there is a surprising change of leadership in Russia,
often times Swan Lake will be will be played over the TV's. It's kind of a
tradition there.
Ok, so what's going on? The reporting suggests that Russian troops, military
troops, traditional military, launched a strike that hit Wagner. Wagner is their
private, the private forces. Wagner is understandably upset about this. They
believe it to be intentional, okay. What we know is that Wagner troops got hit,
okay. Do we know that it was a Russian military? Not really. That hasn't really
been confirmed yet, but it also doesn't matter because Wagner believes that's who
it was. There has been a lot of tension between Wagner and the traditional
military. It is coming to a boil. The boss of Wagner made some statements that some
might interpret as an open call to arms and indicated his desire to punish the
leadership of the traditional, the conventional, military. The FSB, Russia's
internal security, has reportedly started a criminal probe into the statements of
of the boss of Wagner, because that, if true, they're like way out of line. Okay,
so all of this pretty well confirmed. This is stuff that has occurred, whether or not
the interpretations of the people on the ground are accurate doesn't really matter.
That's the lay of the land at the moment. There are reports of troop movements.
Some of the reports are pretty significant numbers. I don't necessarily
believe the upper end of those numbers, but if these reports are accurate, the
movements could certainly be interpreted as Wagner making a move against the
traditional conventional Russian military leadership. Again, possible, kind
of likely, scale up for debate, but not 100% confirmed at time of filming. There
is also reporting suggesting that major buildings within Russia that are
important for the continued governing of the country by the people currently in
power have had some increased security. It's reported, not confirmed. I know
people are gonna say well we've seen these clips I haven't had time to
verify that those are now. This, given everything else, this is possible, likely
even, okay? I will have a link to a video down below, I can't remember how long ago
it was, but less than 60 days ago that might provide a little bit of context to
this. So that's what's going on. Does this mean that you should be popping your
popcorn to get ready to watch Swan Lake? No, not necessarily. This could resolve
itself relatively quickly. It could resolve itself without major issue even.
This is something that theoretically could be handled via back channel
pressure and phone calls. Will it? No clue, but that's a possible outcome. It is
also possible that it completely spirals out of control. We don't know what's
going to happen, so it's not time to pop your popcorn for Swan Lake. But if you
wanted to watch the ballerinas warm up, you might want to pop your popcorn for
that because that's kind of what's going on right now. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
Popcorn at end of video
Spy vs Spy patch
[Zartan figurine](https://gijoe.fandom.com/wiki/Zartan_(RAH))