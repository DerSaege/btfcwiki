---
title: Let's talk about Trump, documents, and delays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uE9uAcJsi0A) |
| Published | 2023/06/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Federal government requesting a delay from August 14th to December 11th for a case involving Trump and classified information.
- Case involves classified documents recovered from Trump's place, still considered so secret that they require a full clearance process.
- Documents, despite being exposed and in the wild for a year, are deemed highly damaging to U.S. national security.
- Defense information on these documents is still classified as extremely damaging even after a long period.
- Only two attorneys involved in this case, both familiar with the Southern District, anticipate a delay past December 11th.
- The judge's acceptance of the government's proposed timeline could push the start further into the new year, possibly after the holiday season.

### Quotes

- "Documents recovered from Trump's place are still deemed so secret that they require a full clearance process."
- "Defense information on these documents is still classified as extremely damaging even after a long period."
- "The December 11th date seems about right, but there will probably be a delay."

### Oneliner

Federal government requests a delay to December 11th for a case involving highly classified documents tied to Trump, still deemed too secret to waive clearance processes, anticipating a start past December 11th.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay updated on the developments of the case (suggested)
- Monitor the timeline proposed by the government and potential delays (implied)

### Whats missing in summary

Context on the legal implications and significance of the delay in the case involving Trump's classified documents.

### Tags

#Trump #LegalCase #ClassifiedInformation #Delay #SecurityClearance


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump,
the documents and delays.
Most people, when the initial date for selection
and the start was made,
most people anticipated that being changed
and a delay occurring.
That delay has now been requested
by the federal government, they're asking for it to be pushed back.
The initial date was August 14th, the government is requesting December 11th to begin jury
selection and stuff like that.
Now some of that delay is normal trial preparation time, making sure that everybody has time
to go over everything, stuff like that.
The judge has said, hey, you know, this actually really isn't that complicated of a case when
it comes to legal theory and liability, so on and so forth.
There's also this from the Assistant Special Counsel.
The case does involve classified information and will necessitate defense counsel obtaining
the requisite security clearances.
Now the government has agreed to expedite the clearance process on some
documents. Basically give them an interim clearance so they can start work. However
some of the documents in question, some of the documents will be discussed, used
as evidence, are so secret they really can't waive the process. This process
takes around two months, sometimes a little less, sometimes a little more. It's
worth acknowledging that it is worth really understanding that documents that
will be used in this case, even though they were exposed and in the wild for a
a year are still deemed so secret that they would cause grave damage to U.S. national
security to the point that the full clearance process has to be done.
These are documents that by all accounts were recovered from Trump's place.
Some of them appeared to have been stored by a copying machine.
Even though they were left exposed, and generally speaking, once a secret is out, it is out.
They are still deemed to be so secret and so damaging, they can't waive the clearance
process.
It is definitely worth acknowledging that and understanding the gravity of the documents.
The defense information that was contained on those documents is so damaging that even
after all of this time they still haven't been downgraded.
Now, normally when I say all the attorneys, there's more than two.
This time it was just two.
But both of them are people who are familiar with the Southern District.
And they say that the December 11th period, that seems about right.
However, it's really just going to get started and it'll end up being pushed into the new
year.
And that is assuming that the judge accepts the government's timeline for the rest of
it because the government proposed a timeline for when do you submit motions by and stuff
like that.
If that timeline is accepted by the judge, they're saying the December 11th date seems
about right, but there will probably be a delay, as in they may start it, but it may
end up being delayed until after the holiday. So that's what we're looking at
now. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}