---
date: 2023-08-06 06:29:43.168000+00:00
dateCreated: 2023-06-25 11:12:57.641000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about Putin's speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oXI00VAnHfQ) |
| Published | 2023/06/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Private forces used by Russia in Ukraine have turned around and marched on positions within Russia.
- Putin accused these private forces of treason and betrayal in a national address.
- There has not been a successful response to the private forces moving through Russia at the time of filming.
- Russia went from being one of the most powerful militaries to potentially being challenged in its own country.
- Putin described the situation as "complicated," referring to battle-hardened troops holding positions in Russia.
- The Russian National Guard has been mobilized but seems ineffective against the contractors.
- Putin may be reaching out to non-Russian state militaries like the Chechens for support.
- Bringing in one group of contractors to combat another may not be a wise move due to their lack of loyalty.
- Russian transportation personnel are removing recruitment posters for the private forces.
- The situation seems to be escalating towards a conflict beyond what has been seen so far.

### Quotes

- "They have betrayed Russia. They are committing treason. They've stabbed Russia in the back."
- "At this point in time, it does appear that he has mobilized what amounts to the Russian National Guard."
- "Mercenaries are not known for their loyalty when it comes to stuff like this."
- "This has definitely spiraled out of control."
- "It's just a thought. Y'all have a good day."

### Oneliner

Private forces turn on Russia, Putin mobilizes National Guard, escalating towards potential conflict.

### Audience

Politically aware individuals.

### On-the-ground actions from transcript

- Monitor the situation closely and be prepared for potential developments (implied).
- Stay informed through reputable news sources to understand the evolving situation (implied).

### Whats missing in summary

The emotional intensity and potential ramifications of private forces turning against Russia and the implications for future conflicts. 

### Tags

#Putin #Russia #PrivateForces #NationalGuard #Conflict #Chechens


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Putin's speech.
If you're watching this when you wake up in the morning,
watch the two previous videos.
Some things occurred overnight in Russia
that you may not be aware of.
Short recap is that the private forces,
the contractors that Russia was using inside of Ukraine,
have decided that they don't want
be used in Ukraine anymore and have turned around and marched on some positions within
Russia.
And at time of filming, they have secured a couple of key positions in Russia.
Putin gave a national address.
In the address, he said that those associated with the private forces, they have betrayed
Russia.
They are committing treason.
They've stabbed Russia in the back.
They're making Russia weaker, so on and so forth, which is expected.
That is what a head of state would say in a situation like this.
also said that they would be dealt with harshly. It's worth noting that at time
of filming, which is early morning, this may change by the time this video
publishes, but thus far there has not been a successful response to Wagner's
forces moving through Russia. It hasn't taken long for Russia to go from, you
know, the second most powerful military in the world to the second most powerful
military in Ukraine and now it may be the second most powerful military in
Russia. When talking about some of the positions that the contracting forces
took, primarily the ones in Rostov, they took a headquarters there, Putin described
this situation as complicated. To be clear, he is talking about a group of
battle-hardened, well-armed troops organized by a core of incredibly
experienced contractors who are taking and holding positions in his country.
Complicated. He's not talking about like an ex-girlfriend who keyed his car and
then sent him a text saying, I love you, because that's complicated. What he's
dealing with I'm not sure that complicated is the appropriate term for
it. At this point in time it does appear that he has mobilized what amounts to
the Russian National Guard. The contracting forces have brushed them
aside for the most part at time of filming, not even to the point of having
to engage, but basically most of the people in the Russian National Guard, they're young,
they might be conscripted, they might be somebody whose family is politically connected and
got them into that so they didn't have to go to Ukraine and now the war in Ukraine came
to them.
has not been incredibly effective. Because of that, Putin appears to have
reached out to some of the other non-Russian state militaries that have
been working with him, like the Chechens, to see if they might do something. I'm
I'm not sure how that's going to play out.
There might be other moves that get made along the way, but generally speaking, bringing
in one group of contractors to combat another group of contractors isn't always a good move.
Mercenaries are not known for their loyalty when it comes to stuff like this.
I am half envisioning a scene from Braveheart.
It's not out of the realm of possibility, although Putin has a lot of money, and that
money might buy the loyalty of the Chechens.
It does at this point seem to be on a route that is going to lead at some point to a conflict,
to the use of force beyond what we have seen at time of filming.
So this drama will probably play out over the weekend.
will see more and we will get more news. Interesting tidbits include right now
at time of filming, Russian transportation personnel are going around
the city and taking down recruitment posters for Wagner and their billboards.
This has definitely definitely spiraled out of control. There are a lot of
theories about this being some kind of 5D chess. Now that's not what it looks
like. This looks like some very angry contractors. The boss of Wagner has
described it not as a coup attempt but as a march for justice and it does appear
that in the beginning he was seeking a a limited a limited operation where he was
really trying to remove some military leadership within the traditional
conventional state military of Russia. Due to Putin's statements those
objectives might shift. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
Black shirt with white san sarif text `[In Walks Weirdo]`
## Easter Eggs on Shelf
Rip It can, which [a later video](https://youtu.be/szPcP4l2uZ8?t=315) states was an accident