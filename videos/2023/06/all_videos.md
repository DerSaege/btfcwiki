# All videos from June, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-06-30: Let's talk about why malaria is in the US.... (<a href="https://youtube.com/watch?v=pMqD1DJ2joA">watch</a> || <a href="/videos/2023/06/30/Lets_talk_about_why_malaria_is_in_the_US">transcript &amp; editable summary</a>)

Beau explains the spread of diseases like malaria in Florida and Texas due to climate change, debunking conspiracy theories and exposing profit-driven motives behind dirty energy use.

</summary>

"Climate change has increased the range of insects."
"We're going to have to deal with diseases that we normally didn't."
"It's not because of some conspiracy other than one that is profit-driven to keep you using dirty energy."

### AI summary (High error rate! Edit errors on video page)

Addressing the spread of two things, one real and one not, particularly in Florida and Texas.
Confirming at least five cases of malaria in Florida and Texas, sparking fear and misinformation.
Explaining that climate change has expanded the range of insects, leading to the spread of diseases like malaria.
Mentioning a Department of Defense study predicting the rise of diseases due to climate change, including malaria.
Warning about the increasing prevalence of diseases due to climate change and profit-driven motives to maintain dirty energy.
Encouraging viewers to watch a video discussing the national security threat posed by climate change and disease spread.

Actions:

for climate activists, public health advocates.,
Watch the video discussing the national security threat posed by climate change and disease spread (suggested).
</details>
<details>
<summary>
2023-06-30: Let's talk about Trump, Smith, and a pair of Queens.... (<a href="https://youtube.com/watch?v=lyM__1CGLrc">watch</a> || <a href="/videos/2023/06/30/Lets_talk_about_Trump_Smith_and_a_pair_of_Queens">transcript &amp; editable summary</a>)

Beau speculates on Trump associate's legal troubles, hinting at mounting evidence and potential undisclosed information.

</summary>

"The cracks are starting to show."
"The rate at which this information is now coming out leads me to believe that by the time this actually goes for, it reaches the point where they're actually seeking the indictment, going to have boxes of evidence."
"I am of the opinion that Smith actually has another queen up his sleeve."

### AI summary (High error rate! Edit errors on video page)

Trump and Smith are engaged in a strategic game, with Smith seemingly holding at least a pair of queens.
Mike Ronan, a campaign official for Trump in 2020, has entered into a cooperation agreement with the federal authorities.
Ronan is not a high-profile figure but is believed to have had significant access to alternate electors and worked closely with Rudy Giuliani.
During House Committee hearings, Ronan refused to answer questions about his interactions with Giuliani post-election by invoking the fifth amendment.
The cooperation agreement was initiated once the Department of Justice informed Ronan he was to testify before a grand jury.
Ronan's phone was confiscated in a related investigation that likely involves election interference, January 6th events, and potential fundraising irregularities.
These issues could potentially lead to separate legal cases if pursued by the Department of Justice.
The protective walls shielding Trump seem to be weakening, with visible cracks emerging.
Beau speculates that Ronan might have more undisclosed information, possibly another "queen" up his sleeve.
The increasing rate of information disclosure suggests a substantial amount of evidence could be amassed by the time indictments are sought.
Beau humorously imagines the potential volume of evidence being so extensive that Ronan might need to store some of it in his bathroom.
The narrative implies escalating legal troubles for Trump's associates and hints at mounting evidence against them.

Actions:

for political observers,
Monitor updates on legal proceedings and investigations involving Trump associates (implied).
</details>
<details>
<summary>
2023-06-30: Let's talk about Trump's standing order falling down.... (<a href="https://youtube.com/watch?v=3Anx__UCTLk">watch</a> || <a href="/videos/2023/06/30/Lets_talk_about_Trump_s_standing_order_falling_down">transcript &amp; editable summary</a>)

Beau reveals the non-existence of Trump's claimed standing order for declassification, debunking a defense tactic.

</summary>

"Those kind of standing orders are not real, don't pretend like it is."
"That's them saying that this doesn't exist."
"This isn't something that could really disrupt that."
"It's one more defense that Trump tried to float that, well, nah, that's not going to work."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explaining the concept of standing orders that Trump claimed to have.
Recalling Trump's assertion of having a standing order to declassify any file at his discretion.
Mentioning his initial skepticism about the existence of such standing orders.
Noting that a judge compelled the Department of Justice and the Office of the Director of National Intelligence to respond to a Freedom of Information Act request.
Revealing that the DOJ and ODNI confirmed possessing no records responsive to the request.
Speculating on the reason behind their reluctance to provide an answer initially.
Stating that the idea of these standing orders goes against tradition, practice, and common sense.
Describing this confirmation as one more defense tactic of Trump's that has been debunked.
Implying that Trump's defense strategy around standing orders has been unsuccessful.
Signing off by wishing a good day to the viewers.

Actions:

for media consumers,
File Freedom of Information Act requests to seek transparency from authorities (suggested)
</details>
<details>
<summary>
2023-06-30: Let's talk about 35 more counts for Trump.... (<a href="https://youtube.com/watch?v=Bt7wuTIyDEw">watch</a> || <a href="/videos/2023/06/30/Lets_talk_about_35_more_counts_for_Trump">transcript &amp; editable summary</a>)

Light reporting hints at Trump facing 35 to 40 additional charges soon, with uncertainties surrounding their nature and impact, underscoring ongoing legal challenges and potential criminal exposure for him and his associates.

</summary>

"If it's something tacked on in the Southern District of Florida, it really doesn't mean much at all."
"There is a lot of potential criminal exposure for the former president and those in his circle."
"Everything else is speculation."
"The devil's always in the details on this stuff."
"Just know to be ready for something in the coming weeks."

### AI summary (High error rate! Edit errors on video page)

Light reporting suggests Trump may face 35 to 40 additional charges soon.
Origin of the reporting is from sources not authorized to speak publicly.
Speculation on whether charges will be added to the original case in Southern District of Florida.
Possibility of charges related to willful retention or election interference.
Mention of potential indictment for attorneys who assisted Trump in his attempts to stay in power.
Uncertainty on whether the charges for Trump and his circle will be under the same indictment.
Speculation on the impact of new charges on Trump's legal situation.
Prediction that Trump may end up facing over a hundred counts in various districts.
Emphasizing ongoing legal challenges and potential criminal exposure for Trump and his associates.
Cautioning about the limited information available and the need to wait for more details to emerge.

Actions:

for legal observers,
Stay updated on the latest developments in the legal proceedings against Trump and his associates (suggested).
Monitor reputable sources for accurate information on the potential charges (suggested).
</details>
<details>
<summary>
2023-06-29: Let's talk about the new We Didn't Start the Fire.... (<a href="https://youtube.com/watch?v=p-vpSIZToAA">watch</a> || <a href="/videos/2023/06/29/Lets_talk_about_the_new_We_Didn_t_Start_the_Fire">transcript &amp; editable summary</a>)

Beau explains why Fallout Boy's updated "We Didn't Start the Fire" may not have the same impact as the original for teaching due to its lack of chronological order and focus on pop culture.

</summary>

"The reason history teachers use it is because it's a timeline."
"The new lyrics won't stand the test of time the way the original ones did."
"It's not in order. So it can't be used the same way."

### AI summary (High error rate! Edit errors on video page)

Fallout Boy updated the lyrics of "We Didn't Start the Fire" to reference recent events, sparking questions about its impact.
Teachers previously used Beau's videos referencing historical events alongside the original version of the song as a teaching tool.
The original song's significance lies in its chronological order of historical events, making it effective for teaching.
Beau won't be analyzing the new version like he did with the original due to its lack of chronological order and pop culture references.
The original version, despite attempting to reference significant historical events, also faced issues in standing the test of time.
Beau predicts that the new version's focus on pop culture events will not age well compared to the original's historical references.
The future relevance of pop culture references like Kanye and Taylor Swift in the new version is questioned.
The original song faced similar issues in predicting future relevance, as seen with its mention of "terror on the airline" now seeming outdated.
Beau concludes that the new version may not be as impactful as a teaching tool due to its lack of chronological order and focus on pop culture.

Actions:

for history teachers, music enthusiasts,
Analyze historical events in chronological order with students (implied)
Encourage critical thinking about the relevance of cultural references in music (implied)
</details>
<details>
<summary>
2023-06-29: Let's talk about how Putin is doing.... (<a href="https://youtube.com/watch?v=hWqO_UdZ2B4">watch</a> || <a href="/videos/2023/06/29/Lets_talk_about_how_Putin_is_doing">transcript &amp; editable summary</a>)

Beau dives into Putin's situation, faction dynamics, and uncertainty about the future, questioning Putin's grip on power amidst intense political jockeying.

</summary>

"Putin is having to walk around a whole lot and tell everybody he's the king."
"There's too many variables at this point. I mean I can't even guess."
"The boss of Wagner don't think that just because he is losing his loyalty to Putin that he's a good guy."
"But generally speaking that's not what occurs."
"I wouldn't want anybody behind me."

### AI summary (High error rate! Edit errors on video page)

Addressing Putin's situation and the factions involved.
Nationalists critical of Putin, calling him pitiful and questioning his leadership.
Pro-Wagner channels are anti-Putin with no unifying ideology.
Pro-Kremlin commentators disappointed in Putin's lack of decisiveness.
Public-facing Kremlin response portrays everything as fine, while internally, blame games are happening.
Political jockeying and palace intrigue are intense due to expected personnel changes.
Putin is at risk but could stay in power if factions don't unify.
Putin is seen as weak, needing to assert his power constantly.
Uncertainty about the future due to various factions and variables.
Not in Russia's or the West's interest for the boss of Wagner to take control.
Speculation on potential outcomes and power shifts.

Actions:

for political analysts,
Connect with organizations monitoring Russian politics (suggested)
Stay informed about Russian political developments (suggested)
</details>
<details>
<summary>
2023-06-29: Let's talk about flags, gators, and vacations.... (<a href="https://youtube.com/watch?v=OrzDQRXKIRI">watch</a> || <a href="/videos/2023/06/29/Lets_talk_about_flags_gators_and_vacations">transcript &amp; editable summary</a>)

Beau explains the meaning of different beach flags in Florida to warn visitors of potential dangers and urges them to stay safe by paying attention to the warnings.

</summary>

"The surf is rough. The currents are high and strong, so avoid those."
"Just red flag, double red flag, just stay out of the water."
"When you come to Florida just assume that every single body of water has a gator in it until you have confirmed otherwise."

### AI summary (High error rate! Edit errors on video page)

Giving a public service announcement about flags in Florida due to the influx of visitors.
Explaining the meaning of five different flags used on Florida beaches - green, yellow, red, double red, and purple.
Green flag signifies relatively safe conditions.
Red flag indicates dangerous surf conditions, advising people to avoid the water.
Double red flag means law enforcement is involved, and it's extremely unsafe to enter the water.
Mentioning Florida's lack of strict safety regulations and the seriousness of the situation.
Warning about dangerous marine life indicated by a purple flag, often jellyfish in his area.
Emphasizing the rapid escalation from a red flag to a double red flag if the currents are not understood.
Cautioning about all bodies of water potentially having alligators in Florida.
Sharing incidents of people getting harmed due to not following safety precautions near water bodies.

Actions:

for florida visitors,
Pay attention to beach warning flags (suggested)
Stay out of the water when red or double red flags are up (suggested)
</details>
<details>
<summary>
2023-06-29: Let's talk about Trump's queen Rudy.... (<a href="https://youtube.com/watch?v=ds5YIDVA9zQ">watch</a> || <a href="/videos/2023/06/29/Lets_talk_about_Trump_s_queen_Rudy">transcript &amp; editable summary</a>)

Beau provides insights into Giuliani's proffer agreement with federal authorities and the potential implications for Trump and his associates, hinting at significant legal consequences.

</summary>

"He tells them everything. I mean everything and he can't lie."
"Giuliani, because of his history, he knows how this process works."
"If he's telling the truth he's wanting one, he's gonna cooperate, he will probably get a deal."
"Those are the two people that Smith is probably trying to get to cooperate the most."
"That entirely voluntary interview that took place under a proffer agreement. That's really bad news for Trump."

### AI summary (High error rate! Edit errors on video page)

Beau provides an overview of Rudy Giuliani's recent legal developments concerning a "proffer agreement" or "Queen for a Day" deal with federal authorities.
Giuliani agreed to an entirely voluntary interview where he must disclose everything truthfully, or risk losing the deal.
The purpose behind Giuliani's engagement in this agreement seems to be either to try to talk his way out of trouble or to make a deal.
There is speculation that Giuliani might receive a good deal if he cooperates truthfully due to the evidence against him and his understanding of the process.
Beau suggests that if Giuliani and another key figure, Meadows, cooperate, it could lead to significant revelations about election interference on January 6th.
The potential cooperation of Giuliani and Meadows could provide valuable information to the Department of Justice about schemes not yet publicly known.
Ultimately, this development is seen as bad news for Trump and those associated with him, indicating potentially significant legal ramifications.

Actions:

for legal analysts,
Contact legal experts for further analysis and understanding of proffer agreements (suggested)
Stay informed about the legal developments surrounding Giuliani and Meadows (implied)
</details>
<details>
<summary>
2023-06-28: Let's talk about single moms and a question.... (<a href="https://youtube.com/watch?v=yOWptv2xfl8">watch</a> || <a href="/videos/2023/06/28/Lets_talk_about_single_moms_and_a_question">transcript &amp; editable summary</a>)

Beau breaks down why a single mom may wait before introducing her child to someone she's dating, aiming to protect her family and potentially theirs in the future.

</summary>

"This is the length she's willing to go to protect what might be your family."
"Don't take it as an insult. It's her trying to make sure that those who influence her children are good people."

### AI summary (High error rate! Edit errors on video page)

Explains the scenario of a woman dating a man for two months who doesn't want him to meet her son yet because she wants to ensure he will be around long-term.
Acknowledges that some men might find this decision insulting.
Breaks down the logic behind why a single mom might take time before introducing her child to someone she's dating.
Puts the situation into perspective using the amount of time spent together to show that two months might not be sufficient for evaluation.
Encourages men to see this boundary as a way for the woman to protect her family and potentially theirs in the future.
Suggests that until a couple has gone through challenges and seen each other's true selves, meeting the child might not happen.
Emphasizes that this boundary is about ensuring that those around her children are good influences.
Concludes by framing this boundary as a positive step towards safeguarding their potential future family.

Actions:

for men in dating situations.,
Understand the logic behind a single mom wanting to take time before introducing her child to someone she's dating (implied).
Respect and support the boundaries set by single parents when it comes to their children (implied).
Put yourself in the position of the single parent and think about what's best for the family unit (implied).
</details>
<details>
<summary>
2023-06-28: Let's talk about lab grown chicken.... (<a href="https://youtube.com/watch?v=pGm3X0SAukc">watch</a> || <a href="/videos/2023/06/28/Lets_talk_about_lab_grown_chicken">transcript &amp; editable summary</a>)

USDA clears the way for cultured meat production, facing skepticism from Americans but holding promise for environmental impact and food safety.

</summary>

"If this can be scaled enough, this can alleviate a whole lot of problems."
"I am obviously one of those people that is extremely likely to try this."
"Their argumentation is rock solid."
"Cultured meat is safer."
"There's a lot of benefits to this."

### AI summary (High error rate! Edit errors on video page)

USDA cleared the way for companies like Upside Foods and Good Meat to sell cultured, lab-grown meat, initially in high-end restaurants due to pricing.
Half of Americans are unlikely to try cultured meat, with only 18% very or extremely likely, citing reasons like it sounding weird or unsafe.
Cultured meat production could alleviate environmental impact caused by traditional agriculture.
Cultured meat is safer as certain risks like salmonella or E. coli are eliminated.
Beau is extremely likely to try cultured meat to address arguments about veganism and animal rights without feeling guilty.
Scaling cultured meat production could solve various issues beyond initial expectations, reaching wider markets and aiding in times of famine.
Despite widespread distrust of lab-produced animal products, the benefits for the environment and food safety are significant.
There is a potential positive impact of cultured meat production, despite initial skepticism.

Actions:

for food consumers, environmentalists, policymakers,
Support companies like Upside Foods and Good Meat by trying out cultured meat when available (implied)
</details>
<details>
<summary>
2023-06-28: Let's talk about Rudy and interviews.... (<a href="https://youtube.com/watch?v=r4lZ9cjTrgw">watch</a> || <a href="/videos/2023/06/28/Lets_talk_about_Rudy_and_interviews">transcript &amp; editable summary</a>)

Beau provides insights on Rudy Giuliani's voluntary meeting with the special counsel's office, potentially stirring discomfort in Trump's circle as the probe progresses towards individuals directly linked to Trump, with fundraising inquiries adding another layer of intrigue.

</summary>

"It's another sign that probe is definitely progressing, getting to the names that everybody's heard in the media."
"The entirely voluntary nature of this is probably going to raise eyebrows and paranoia."
"It's worth remembering that there's a whole lot of reporting that suggests Rudy might have overseen the fake electors."
"For all we know Giuliani went in there and was like, I don't know any of these people."
"We don't know what the special counsel's office is really doing because well I mean they don't tell anybody."

### AI summary (High error rate! Edit errors on video page)

Mention of two significant interviews involving Rudy Giuliani and Georgia Secretary of State Raffensperger.
Recall of Trump's phone calls to Raffensperger requesting additional votes.
Rudy Giuliani's voluntary meeting with the special counsel's office regarding election interference and January 6 events.
Speculation on Rudy Giuliani's involvement in overseeing fake electors.
Questions directed at Giuliani about meetings and fundraising between November 3, 2020, and January 6, 2021.
Mention of Jeffrey Clark, Sidney Powell, and John Eastman in the reports.
Potential discomfort in Trump's circle due to the voluntary nature of Giuliani's meeting.
Progression of the probe towards individuals directly connected to Trump.
Significance of fundraising inquiries by the special counsel's office.
Uncertainty surrounding the details of Giuliani's meeting and its potential implications.

Actions:

for political analysts,
Stay informed about ongoing investigations and legal proceedings (implied).
Support accountability and transparency in governmental activities (implied).
Stay engaged in political developments and be aware of potential implications (implied).
</details>
<details>
<summary>
2023-06-27: Let's talk about the Trump recording.... (<a href="https://youtube.com/watch?v=LgsnD1rUoHg">watch</a> || <a href="/videos/2023/06/27/Lets_talk_about_the_Trump_recording">transcript &amp; editable summary</a>)

Team Trump receives leaked audio discussing an Iran attack plan, creating a damaging case against him and raising questions on defense strategies.

</summary>

"I personally don't know how you could create more damaging audio."
"I have no idea how you're going to mount a defense now."
"I have no idea what Trump intends to do."
"I've got no idea where… Just those two things make it seem from my perspective to be pretty insurmountable."
"It is definitely a witch hunt if Trump is one of the Sanderson sisters."

### AI summary (High error rate! Edit errors on video page)

Team Trump recently began receiving discovery related to a leaked audio recording involving Trump discussing a plan to attack Iran.
The leaked audio combined with incriminating documents found at Trump's place seems to create a damaging case against him.
The audio recording clearly shows Trump acknowledging the classified nature of the information discussed.
Speculation arises about the motives behind leaking the audio, including potential ego-driven reasons or strategic moves for trial.
Beau questions how Trump could defend himself given the damning nature of the audio and recovered documents.
The possibility of the leak being orchestrated by someone within the Department of Justice is considered.
The leaked audio's value in trial lies in its shocking nature, potentially making it harder for Trump to defend himself.

Actions:

for political analysts, concerned citizens,
Speculate on the potential motives behind the leaked audio (suggested)
Analyze the implications of the leaked information in the context of national security (suggested)
</details>
<details>
<summary>
2023-06-27: Let's talk about heat, Texas, and a PSA.... (<a href="https://youtube.com/watch?v=J6QFcx998Kk">watch</a> || <a href="/videos/2023/06/27/Lets_talk_about_heat_Texas_and_a_PSA">transcript &amp; editable summary</a>)

Heat wave in Texas leads to tragic deaths, stressing the importance of acclimating to local weather and staying cool and hydrated, especially for vulnerable individuals.

</summary>

"The heat is dangerous."
"The heat is a killer."
"Make sure you take care of yourself. You stay cool, you stay hydrated."

### AI summary (High error rate! Edit errors on video page)

Heat wave in Texas with 10,000 outages, dangerous heat at 119 degrees.
Tragic incident in Big Ben park where a 14-year-old hiking boy and his dad succumbed to extreme heat.
Military bases use color-coded flags - at 90 degrees, a black flag means all activity stops.
Acclimation to local weather is critical due to varying heat impacts.
Reminder to stay cool, hydrated, and look out for vulnerable individuals during extreme heat.

Actions:

for texans, hikers, community members,
Stay cool, hydrated, and look out for vulnerable individuals in extreme heat (suggested).
Be aware of temperature warnings and adjust activities accordingly (implied).
</details>
<details>
<summary>
2023-06-27: Let's talk about SCOTUS and Moore v Harper.... (<a href="https://youtube.com/watch?v=dJalbSwZNwU">watch</a> || <a href="/videos/2023/06/27/Lets_talk_about_SCOTUS_and_Moore_v_Harper">transcript &amp; editable summary</a>)

The Supreme Court's rejection of the independent state legislature theory is a significant win for democracy, putting the theory in critical condition and safeguarding against potential threats to American democracy, although the issue remains unresolved.

</summary>

"It was more like this is moot, you know, we don't really need to deal with this right now."
"So that is good news."
"The Supreme Court made the right decision."

### AI summary (High error rate! Edit errors on video page)

Supreme Court rejected independent state legislature theory, a case that could have jeopardized American democracy.
State legislatures could have had unlimited authority to influence federal elections if the theory was upheld.
Rejecting the theory ensures that state legislatures are not insulated from judicial oversight.
Decision puts independent state legislature theory in critical condition, but doesn't completely eradicate it.
The ruling is a significant win for democracy, but the issue isn't entirely resolved.
Some Republicans have been supporting the theory, but it lacks enough traction to move forward.

Actions:

for american citizens,
Stay informed about legal and political developments impacting democracy (implied)
Support organizations advocating for fair elections and democratic processes (implied)
</details>
<details>
<summary>
2023-06-27: Let's talk about Putin, Pulp Fiction, and pieces of information.... (<a href="https://youtube.com/watch?v=_eHnTG2NB60">watch</a> || <a href="/videos/2023/06/27/Lets_talk_about_Putin_Pulp_Fiction_and_pieces_of_information">transcript &amp; editable summary</a>)

Beau explains how movies like "The Usual Suspects" and "Pulp Fiction" teach us to re-evaluate timelines and not base opinions on initial information, using the Wagner coup attempt as an example.

</summary>

"Don't base your opinion on the first information you get."
"Understand the order in which the story breaks is not necessarily the order in which it occurred."
"If you don't plug that in and acknowledge that that changes the order of events and it changes the reason for Russia's response, your view of it is going to be off."

### AI summary (High error rate! Edit errors on video page)

Explains how movies can teach us about processing information out of chronological order.
Uses examples like "The Usual Suspects" and "Pulp Fiction" to illustrate the importance of critical pieces of information.
Analyzes the Wagner coup attempt and how different pieces of information changed the narrative.
Emphasizes the need to re-evaluate timelines and not base opinions on initial information.
Warns against forming incorrect assessments by not considering all the pieces together.

Actions:

for information consumers,
Re-evaluate timelines based on new information (implied)
</details>
<details>
<summary>
2023-06-26: Let's talk about upcoming SCOTUS decisions.... (<a href="https://youtube.com/watch?v=Wj2wynwft34">watch</a> || <a href="/videos/2023/06/26/Lets_talk_about_upcoming_SCOTUS_decisions">transcript &amp; editable summary</a>)

Beau outlines key Supreme Court decisions, from state legislature power to affirmative action's future, impacting elections, discrimination laws, and college admissions.

</summary>

"The politicians will be able to pick their voters."
"I think the justices are going to say that affirmative action needs to go away in college admissions."
"If either one of these goes against Biden, they're back to the drawing board on student debt relief."

### AI summary (High error rate! Edit errors on video page)

Beau provides an overview of significant upcoming decisions from the Supreme Court before their break.
The Moore v. Harper case involves the independent state legislature theory, potentially granting more power to state legislatures over elections.
303 Creative v. Alenis tackles whether artists' websites are considered public accommodations and subject to non-discrimination laws.
Counterman v. Colorado revolves around a stalking case and the justices clarifying the threshold for threats under the First Amendment.
Groff v. DeJoy is about religious accommodation for days off, impacting those needing specific days off for religious reasons.
There are two affirmative action cases involving college admissions that may signal the end of affirmative action.
Two student debt cases, Biden v. Nebraska and Department of Education v. Brown, could impact Biden's student debt relief plans.
The Supreme Court will also announce its next docket during this period.

Actions:

for legal enthusiasts, activists,
Stay informed about the Supreme Court decisions and their implications (suggested).
Join legal advocacy groups to stay updated and engaged with ongoing cases (exemplified).
</details>
<details>
<summary>
2023-06-26: Let's talk about Trump, immunity deals, and the other grand jury.... (<a href="https://youtube.com/watch?v=svIuevRLIDc">watch</a> || <a href="/videos/2023/06/26/Lets_talk_about_Trump_immunity_deals_and_the_other_grand_jury">transcript &amp; editable summary</a>)

The special counsel offers immunity to key figures in the January 6th investigation, signaling the pursuit of compelling testimony and probable criminal charges.

</summary>

"You have a Fifth Amendment right to not incriminate yourself. If you have immunity, you are no longer at risk of incriminating yourself."
"By the time they've reached this point, they've already got a pretty good case because really what they're looking for now is not somebody to give them new information."
"This is a pretty clear indication that the special counsel's office has uncovered what they believe to be criminal conduct that should be charged."

### AI summary (High error rate! Edit errors on video page)

The special counsel's office has extended immunity offers to individuals involved in the grand jury investigation of January 6th, including fake electors.
Immunity allows individuals to testify without the risk of incriminating themselves and not invoke their Fifth Amendment right against self-incrimination.
By offering immunity, the special counsel is aiming to compel testimony from these individuals to gain more information and potentially build a case for criminal charges.
Providing immunity signifies that the special counsel has likely uncovered probable criminal conduct and is seeking individuals to provide context to existing information.
Those granted immunity must choose between telling the truth, which allows them to walk away unscathed, or lying, which could result in facing additional charges without immunity protection.

Actions:

for legal analysts, activists,
Reach out to legal organizations for updates and ways to support accountability efforts (suggested)
Stay informed about the developments in the investigation and share accurate information with others (exemplified)
</details>
<details>
<summary>
2023-06-26: Let's talk about Pence playing the long game.... (<a href="https://youtube.com/watch?v=9iYW-g12dLk">watch</a> || <a href="/videos/2023/06/26/Lets_talk_about_Pence_playing_the_long_game">transcript &amp; editable summary</a>)

Beau questions former Vice President Pence's campaign strategy and speculates if he is playing a long game by waiting for Trump's downfall to claim credit for preserving democracy.

</summary>

"I don't understand why he's not using that and this is the only reason I can come up with other than he's super guilty of something and we don't know it."
"He was the one person who really stood in the way that day and he's not bringing it up."
"I think Pence might be playing the long game here."
"Politically, that's gold and they're not using it yet."
"He's waiting for somebody else to do it and then he can come out and explain."

### AI summary (High error rate! Edit errors on video page)

Critiques former Vice President Pence's campaign strategy for not capitalizing on a unique tool he possesses.
Expresses dislike for Pence's policies and philosophy but acknowledges his actions on January 6th in not contesting the electors.
Questions why Pence is not leveraging his role in protecting American democracy on January 6th in his campaign.
Suggests two possible reasons for Pence's strategy: either he has undisclosed wrongdoing or he is playing a long game, waiting for Trump's downfall to claim credit for preserving democracy.
Speculates that Pence might be waiting for certain proceedings related to January 6th to come to light before using this information in his campaign.
Points out that Pence's campaign is currently inactive, not utilizing the potentially advantageous information he has.
Raises the possibility that Pence's advisors are aware of the political value of his actions on January 6th and are strategically waiting to reveal it at a more opportune time.

Actions:

for campaign strategists, political analysts, supporters of democracy,
Analyze Pence's actions and statements critically to understand his political strategy (implied)
</details>
<details>
<summary>
2023-06-26: Let's talk about Musk vs Zuck in the octagon.... (<a href="https://youtube.com/watch?v=8vKE4_8vaZE">watch</a> || <a href="/videos/2023/06/26/Lets_talk_about_Musk_vs_Zuck_in_the_octagon">transcript &amp; editable summary</a>)

Beau suggests billionaires like Musk and Zuckerberg engaging in physical competitions could potentially shed light on their business practices, holding them to higher standards, and encouraging charitable donations.

</summary>

"Musk versus Zuck, the Battle of the Billionaires."
"I think the idea of billionaires stepping into a ring, hopefully donating any purse to charity, I think that can only be good."
"I think it might actually be a vehicle for a whole lot of people to find out what kind of business practices these companies engage in."
"Zuck is going to win because you can't fight somebody that doesn't Blink."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of Elon Musk challenging Mark Zuckerberg to a cage match, dubbing it "Musk versus Zuck, the Battle of the Billionaires."
Musk challenged Zuckerberg to step into the octagon for a cage match, sparking a buzz.
Beau expresses his opinion that encouraging billionaires to physically compete could be positive, as it might hold them to higher standards compared to their usual business behavior.
He suggests that billionaires participating in physical competitions and donating their earnings to charity could shed light on their business practices.
Beau likens the scenario to a US election where people might not necessarily support one side but rather root for the person they want to lose.
He proposes the idea of billionaires engaging in other physically demanding competitions, like a race across the Sahara, to alter the current dynamics.
Beau mentions Epic Rap Battles on YouTube predicting this showdown and humorously predicts Zuckerberg winning because "you can't fight somebody that doesn't Blink."
In conclusion, Beau leaves his audience with his thoughts and wishes them a good day.

Actions:

for social media users,
Watch Epic Rap Battles on YouTube to see their prediction (suggested)
Share thoughts on social media about billionaires engaging in physical competitions (implied)
</details>
<details>
<summary>
2023-06-25: The roads to dating advice.... (<a href="https://youtube.com/watch?v=Xh2uXdL5Os8">watch</a> || <a href="/videos/2023/06/25/The_roads_to_dating_advice">transcript &amp; editable summary</a>)

Beau shares dating insights, from addressing age gaps to discussing core values and maintaining privacy boundaries in relationships.

</summary>

"It's about core values."
"Everything's attractive to men."
"Everybody needs privacy, everybody needs their boundaries."

### AI summary (High error rate! Edit errors on video page)

Hosting a Q&A session on dating while on the roads.
Sharing the worst habits between him and his wife - leaving coffee sips and over-explaining.
Addressing age differences in relationships and advising communication.
Giving advice on introducing a partner to a child after two months of dating.
Explaining the importance of core values over shared interests in relationships.
Sharing a humorous take on how to spot military personnel in a crowd.
Suggesting ways to gauge a man's views on women's rights through observations.
Sharing a dark humor joke related to EOD patches.
Dismissing the concept of women hitting a "wall" as unrealistic.
Addressing misconceptions about dating an ex-dancer.
Encouraging self-acceptance by noting that everyone has attractive qualities.
Providing insight on how to maintain a spark in a long-term relationship.
Offering advice on disclosing past relationships during the dating phase.
Exploring the root of why men may value purity in relationships.
Addressing privacy boundaries in relationships regarding phone access.

Actions:

for dating individuals,
Have an open and honest communication with your partner about habits that bother you (implied).
Communicate openly with your partner about age differences and concerns (implied).
Set boundaries and communicate your feelings about introducing partners to your children (exemplified).
Observe how potential partners treat others to gauge their beliefs on gender rights (exemplified).
Respect privacy boundaries in relationships, and communicate openly about them (exemplified).
</details>
<details>
<summary>
2023-06-25: Let's talk about why it happened in Russia.... (<a href="https://youtube.com/watch?v=Y2hVaLhUtdM">watch</a> || <a href="/videos/2023/06/25/Lets_talk_about_why_it_happened_in_Russia">transcript &amp; editable summary</a>)

Overnight questions on Russian forces in Ukraine point blame at Putin, Ukrainian intelligence, CIA, and Biden, revealing failures from waste and loss.

</summary>

"Don't look for some man behind the curtain here."
"This whole operation has been an unmitigated failure."
"You don't have to look for some great big conspiracy."

### AI summary (High error rate! Edit errors on video page)

Overnight, questions poured in about the chain of events leading to Russian forces advancing towards Russian cities inside Ukraine.
The blame was placed on Putin, Ukrainian intelligence, the CIA, and Biden.
The town of Solidar was where Wagner, Russian private contractors, faced heavy losses and were later given replacements who were normal Russian citizens, not experienced contractors.
The replacements were sent to Bakhmut, where heavy fighting continued, leading to experienced contractors being upset at the loss of normal citizens.
Putin viewed private forces and traditional military as in competition, leading to unacceptable waste and losses.
The rank-and-file troops were tired of the waste and supported the boss, potentially due to Ukrainian intelligence playing a role.
The boss criticized logistical issues and blamed the traditional military, hinting at the reasons behind the failure of the operation.
The whole operation was deemed a failure, costing too much for minimal gain, especially impacting contractors who witnessed the losses of both experienced contractors and normal citizens.

Actions:

for military analysts,
Support organizations aiding war victims (implied)
Advocate for transparency and accountability in military operations (implied)
</details>
<details>
<summary>
2023-06-25: Let's talk about good and bad guys in Russia.... (<a href="https://youtube.com/watch?v=6QPU3ggknPw">watch</a> || <a href="/videos/2023/06/25/Lets_talk_about_good_and_bad_guys_in_Russia">transcript &amp; editable summary</a>)

Beau examines how individuals justify heinous acts for a greater good, delving into loyalty, rationalization, and the universal nature of war justifications.

</summary>

"Just remember before you can commit a great evil you have to be convinced that it's for a greater good."
"Soldiers do not fight against what's in front of them. They fight for what's behind them, what they believe they're protecting."
"You have to believe that. If they don't, well then they have to deal with everything that they did."
"Because at the end of it, you have to remember, everything that occurs in war is horrible."
"We identify ourselves by lines on maps and flags, the little things that separate us."

### AI summary (High error rate! Edit errors on video page)

Examines the concept of individuals viewed as bad doing good things and vice versa.
Talks about motivations and how people rationalize committing evil acts for a greater good.
Gives examples of Americans justifying heinous actions in the name of national security.
Explores the idea of individuals fighting not against what's in front of them, but for what they believe they're protecting.
Illustrates how loyalty and the belief in fighting for a greater good can lead individuals to participate in actions they may find questionable.
Touches on the importance of perceived waste and the illusion of doing the right thing in conflicts.
Concludes by reflecting on the universal nature of justifying actions in war and the influence of societal norms.

Actions:

for advocates for peace,
Question societal norms that perpetuate the justification of violence (implied)
Educate others on the dangers of blindly following authority figures (implied)
</details>
<details>
<summary>
2023-06-25: Let's talk about expunging Trump's impeachments.... (<a href="https://youtube.com/watch?v=n65JqQ0jWJY">watch</a> || <a href="/videos/2023/06/25/Lets_talk_about_expunging_Trump_s_impeachments">transcript &amp; editable summary</a>)

Beau addresses the symbolic expungement of Trump's impeachments, outlining its historical impact and criticizing it as a performative act with no real consequence.

</summary>

"It's a show with nothing more."
"It doesn't actually go away."
"They might go on to do something, but they will forever be tied to Trump."

### AI summary (High error rate! Edit errors on video page)

Addressing the possible expungement of former President Trump's impeachments.
McCarthy's support for the idea of expunging the impeachments.
Legal scholars doubting the feasibility of expunging impeachments since it's not a criminal process.
Mentioning the precedent of Andrew Jackson having his Senate censure reversed in 1837.
Emphasizing that even if the impeachments are expunged, they will still remain in the historical record.
Describing the expungement as a symbolic gesture for the base, adding an asterisk to Trump's record.
Pointing out that Trump's focus may be more on his legal troubles rather than the impeachments.
Criticizing the House GOP for using such actions as a show for social media engagement without real impact.
Stating that the expungement won't change public perception of Trump but will tie House GOP members to him historically.
Concluding that the expungement is merely a performative act with no substantial effect.

Actions:

for politically engaged individuals,
Educate on the implications of performative political acts (implied)
</details>
<details>
<summary>
2023-06-25: Let's talk about Russia cooling off and stranger things to come.... (<a href="https://youtube.com/watch?v=szPcP4l2uZ8">watch</a> || <a href="/videos/2023/06/25/Lets_talk_about_Russia_cooling_off_and_stranger_things_to_come">transcript &amp; editable summary</a>)

A deal struck in Russia reveals Putin's weakness and encourages challenges to his authority, urging Russian troops not to risk their lives for his ego.

</summary>

"Don't die for that, man."
"People have been saying the whole thing seems really strange."
"He is no longer somebody that is seen as invincible."
"If you're going to take a shot, don't miss and don't stop."
"It's the beginning."

### AI summary (High error rate! Edit errors on video page)

A deal has been struck in Russia, prompting questions about its details.
Two types of deals exist: a public-facing one and a private one.
Private contractors who don't sign with the Ministry of Defense may be redeployed to pursue Russian interests in Africa.
The firm involved previously functioned as Russia's special operations command.
Putin made the deal because he couldn't stop the private side's initiative.
The deal demonstrates Putin's weakness and encourages challenges to his authority.
Putin may have to visit Belarus, and there are rumors of leadership changes in the Ministry of Defense.
The situation in Russia is expected to continue with turmoil in the upper echelons of society.
Russian troops in Ukraine are urged not to risk their lives for Putin's ego.
Beau will release a video explaining the situation further.

Actions:

for world observers,
Watch Beau's upcoming video for further insights (suggested)
Stay informed about the situation in Russia and its implications (implied)
</details>
<details>
<summary>
2023-06-24: Let's talk about more updates on Russia.... (<a href="https://youtube.com/watch?v=3Jp_CVK7ZYA">watch</a> || <a href="/videos/2023/06/24/Lets_talk_about_more_updates_on_Russia">transcript &amp; editable summary</a>)

Tensions rise in Russia as a direct confrontation looms between traditional military and private forces, potentially leading to a coup or popular uprising, pending Putin's response.

</summary>

"There have been confirmed direct engagements between Russian state forces and the private forces."
"The response has to be the same, which is overwhelming."
"Russian state media is doing a great job of basically telling everybody, hey, everything is fine, there's nothing to see here."
"The possibility of a full-blown coup or popular uprising cannot be discounted."
"The real decision here, it's not going to be made on the battlefield between the private forces and the state forces."

### AI summary (High error rate! Edit errors on video page)

Russia is on the brink of a direct confrontation between the traditional military and the private contracting force, Wagner.
Wagner has crossed into Russian territory from Ukraine with a force estimated around 25,000.
There have been confirmed direct engagements between Russian state forces and Wagner.
Wagner claims to have taken the Southern Military District HQ in Rostov-on-Don.
The situation is escalating rapidly, and Moscow has responded, indicating there's no turning back.
The response from Moscow must be overwhelming to prevent any notion of leadership removal by force.
Russian state media is downplaying the situation, but the reality is military vehicles are visible in the streets.
The possibility of a full-blown coup or popular uprising cannot be discounted, especially if the civilian populace sides with the private forces.
The involvement and response of Putin will heavily impact the direction this conflict takes.
The situation is complex, with different schools of thought on how to respond, including potential Ukrainian involvement.

Actions:

for international observers, policymakers,
Monitor the situation closely and stay informed about developments (implied)
Advocate for peaceful resolution and de-escalation efforts within diplomatic channels (implied)
Support initiatives that aim to protect civilian populations amidst escalating tensions (implied)
</details>
<details>
<summary>
2023-06-24: Let's talk about Trump, documents, and delays.... (<a href="https://youtube.com/watch?v=uE9uAcJsi0A">watch</a> || <a href="/videos/2023/06/24/Lets_talk_about_Trump_documents_and_delays">transcript &amp; editable summary</a>)

Federal government requests a delay to December 11th for a case involving highly classified documents tied to Trump, still deemed too secret to waive clearance processes, anticipating a start past December 11th.

</summary>

"Documents recovered from Trump's place are still deemed so secret that they require a full clearance process."
"Defense information on these documents is still classified as extremely damaging even after a long period."
"The December 11th date seems about right, but there will probably be a delay."

### AI summary (High error rate! Edit errors on video page)

Federal government requesting a delay from August 14th to December 11th for a case involving Trump and classified information.
Case involves classified documents recovered from Trump's place, still considered so secret that they require a full clearance process.
Documents, despite being exposed and in the wild for a year, are deemed highly damaging to U.S. national security.
Defense information on these documents is still classified as extremely damaging even after a long period.
Only two attorneys involved in this case, both familiar with the Southern District, anticipate a delay past December 11th.
The judge's acceptance of the government's proposed timeline could push the start further into the new year, possibly after the holiday season.

Actions:

for legal observers,
Stay updated on the developments of the case (suggested)
Monitor the timeline proposed by the government and potential delays (implied)
</details>
<details>
<summary>
2023-06-24: Let's talk about Russian developments, Swan Lake, and popcorn.... (<a href="https://youtube.com/watch?v=7pjcKan2m94">watch</a> || <a href="/videos/2023/06/24/Lets_talk_about_Russian_developments_Swan_Lake_and_popcorn">transcript &amp; editable summary</a>)

Reports of a strike between Russian military and Wagner private forces spark tensions and potential conflict escalation in Russia.

</summary>

"Does this mean that you should be popping your popcorn to get ready to watch Swan Lake? No, not necessarily."
"It is also possible that it completely spirals out of control."
"But if you wanted to watch the ballerinas warm up, you might want to pop your popcorn for that."

### AI summary (High error rate! Edit errors on video page)

Reports suggest Russian military troops struck Wagner, a private force, leading to tension.
Wagner believes the strike was intentional, even though the source hasn't been confirmed.
The boss of Wagner hinted at an open call to arms against the traditional Russian military leadership.
Russia's internal security has started a criminal probe into the boss of Wagner's statements.
There are reports of troop movements that could signify tension escalating between Wagner and the conventional military.
Major buildings in Russia have reportedly increased security, potentially related to governing stability.
The situation is not fully confirmed, with some possibilities of de-escalation or spiraling out of control.
Back channel pressure and phone calls could potentially resolve the situation without major issues.
There's uncertainty about whether the conflict will intensify or resolve peacefully.

Actions:

for global citizens,
Monitor the situation closely and stay informed (implied)
</details>
<details>
<summary>
2023-06-24: Let's talk about Putin's speech.... (<a href="https://youtube.com/watch?v=oXI00VAnHfQ">watch</a> || <a href="/videos/2023/06/24/Lets_talk_about_Putin_s_speech">transcript &amp; editable summary</a>)

Private forces turn on Russia, Putin mobilizes National Guard, escalating towards potential conflict.

</summary>

"They have betrayed Russia. They are committing treason. They've stabbed Russia in the back."
"At this point in time, it does appear that he has mobilized what amounts to the Russian National Guard."
"Mercenaries are not known for their loyalty when it comes to stuff like this."
"This has definitely spiraled out of control."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Private forces used by Russia in Ukraine have turned around and marched on positions within Russia.
Putin accused these private forces of treason and betrayal in a national address.
There has not been a successful response to the private forces moving through Russia at the time of filming.
Russia went from being one of the most powerful militaries to potentially being challenged in its own country.
Putin described the situation as "complicated," referring to battle-hardened troops holding positions in Russia.
The Russian National Guard has been mobilized but seems ineffective against the contractors.
Putin may be reaching out to non-Russian state militaries like the Chechens for support.
Bringing in one group of contractors to combat another may not be a wise move due to their lack of loyalty.
Russian transportation personnel are removing recruitment posters for the private forces.
The situation seems to be escalating towards a conflict beyond what has been seen so far.

Actions:

for politically aware individuals.,
Monitor the situation closely and be prepared for potential developments (implied).
Stay informed through reputable news sources to understand the evolving situation (implied).
</details>
<details>
<summary>
2023-06-23: Let's talk about the GOP disinterest in impeaching Biden.... (<a href="https://youtube.com/watch?v=YB3xEWoZOjU">watch</a> || <a href="/videos/2023/06/23/Lets_talk_about_the_GOP_disinterest_in_impeaching_Biden">transcript &amp; editable summary</a>)

Republicans in the House push for impeachment, while Senate Republicans focus on policy, creating a divide in approach and priorities within the party.

</summary>

"House Republicans have used a lot of inflammatory rhetoric."
"It does not appear that Republicans in the Senate are willing to play ball."
"This puts the far-right Republicans in the House in a very difficult position."

### AI summary (High error rate! Edit errors on video page)

Republicans in the House are pushing for impeachment of President Biden or his appointees, feeling the need to create a scandal to appease their base.
House Republicans have used inflammatory rhetoric, leading to calls for accountability from their base.
Senate Republicans, on the other hand, seem uninterested in politicizing the impeachment process and are more focused on policy agendas.
Senators are running in statewide races and need to appeal to a broader audience, unlike House Republicans catering to a small, red district.
The Senate Republicans' lack of interest in catering to far-right demands puts House Republicans in a difficult position.
Moving forward with impeachment in the House may lead to failure in the Senate and put Senate members at risk.
The Senate is hesitant to politicize the impeachment process and is not willing to play ball at the House's level.
Far-right rhetoric is causing alienation not only among center voters but also among Senators.
The cost of far-right rhetoric includes the potential loss of Senate seats and failure in impeachment endeavors.
The exact impeachable offense is unclear at this point.

Actions:

for political activists and voters,
Contact your representatives to express your views on impeachment (suggested)
Stay informed about political developments and rhetoric in your area (implied)
</details>
<details>
<summary>
2023-06-23: Let's talk about learning from ERCOT's advice.... (<a href="https://youtube.com/watch?v=pbvPzedYdAM">watch</a> || <a href="/videos/2023/06/23/Lets_talk_about_learning_from_ERCOT_s_advice">transcript &amp; editable summary</a>)

ERCOT advised Texans to limit energy consumption voluntarily during a heat wave, showcasing the importance of acting on expert advice to prevent future crises.

</summary>

"They're trying to provide you with information so you can make an informed decision."
"You have to stop allowing people who provoke you into reaction, people who play on your emotions."
"It's going to take everybody doing their part."
"Those who use climate issues for culture war probably shouldn't be in office."
"Do this little thing now to avoid a problem down the road."

### AI summary (High error rate! Edit errors on video page)

ERCOT advised Texans during a heat wave to limit energy consumption by not running large appliances and raising thermostats.
Many people complied with the advice to prevent strain on the energy system.
Experts provide recommendations like ERCOT to avoid larger problems down the road.
Recommendations are voluntary and aim to prevent future issues.
Some individuals turn climate advice into a culture war issue, creating unnecessary conflict.
Pushing narratives against responsible actions distracts from addressing real issues.
Political motives sometimes drive opposition to sensible recommendations.
Changes to energy infrastructure and consumption are inevitable for the future.
Listening to experts and acting on their advice is necessary to prevent future crises.
Climate change demands collective action to secure a better future for the next generation.

Actions:

for texans, climate advocates,
Follow recommendations from experts on energy conservation (implied)
Act voluntarily to reduce energy consumption and mitigate strain on the system (exemplified)
</details>
<details>
<summary>
2023-06-23: Let's talk about Ken Paxton's impeachment trial date.... (<a href="https://youtube.com/watch?v=4H1gB5mbmTo">watch</a> || <a href="/videos/2023/06/23/Lets_talk_about_Ken_Paxton_s_impeachment_trial_date">transcript &amp; editable summary</a>)

Texas Attorney General Ken Paxton faces potential removal through a Senate trial, with political dynamics and historical rarity influencing the outcome.

</summary>

"Paxton, for his part, has denied any and all wrongdoing."
"If half of the State Senate Republicans in Texas vote to convict and remove, he's gone."
"The recusal, to me, signals that there is an incredibly strong chance that after this trial Paxton will not be Attorney General Paxton."

### AI summary (High error rate! Edit errors on video page)

Texas Attorney General Ken Paxton was impeached by a Republican-controlled house, with allegations of serious wrongdoing.
Paxton denies any wrongdoing, but the Senate trial is set for September 5th.
Paxton's wife, State Senator Angela Paxton, will not be voting in the impeachment trial.
The political makeup of the Senate, with 19 Republicans and 12 Democrats, plays a significant role in the potential removal of Paxton.
If half of the State Senate Republicans vote to convict and remove Paxton, he will be gone.
Despite the allegations, the possibility of Paxton being removed is high due to the political dynamics.
The rarity of impeachment in Texas history adds weight to the decision facing State Senators.
Rules were created to force Paxton's wife to recuse herself from voting, indicating a strong likelihood of Paxton not remaining as Attorney General.
The political implications and historical significance of this impeachment trial are substantial.
The situation may lead to Paxton's removal, considering the unique circumstances surrounding this impeachment in Texas.

Actions:

for texans, political observers,
Contact your State Senators to express your views on the impeachment trial (suggested)
Stay informed about the proceedings and outcomes of the Senate trial (suggested)
</details>
<details>
<summary>
2023-06-23: Let's talk about Biden, the rails, and sick days.... (<a href="https://youtube.com/watch?v=EM6jMtG_MB8">watch</a> || <a href="/videos/2023/06/23/Lets_talk_about_Biden_the_rails_and_sick_days">transcript &amp; editable summary</a>)

Biden's administration quietly secures paid sick days for railroad workers, proving skeptics wrong and aiding unions behind the scenes.

</summary>

"I believe in paid sick days and I think railroad workers should get that, blah, blah, blah."
"Biden and Bernie appear to have really, really came through."
"If I'm reading between the lines on all of this correctly, Biden and Bernie appear to have really, really came through."
"I was openly skeptical and I made kind of a very direct statement that I did not think that the Biden administration was actually going to continue to work on this."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Biden administration intervened in a labor dispute on railroads regarding paid sick days.
Unions were pushing for paid sick days and short notice sick days, but negotiations weren't progressing.
Biden tried to help the unions but didn't fully meet their demands, risking a rail strike.
Biden considered using Congress's power to break the strike, angering union supporters.
Beau critiqued this as the first major mistake of Biden's administration.
Despite promises for paid sick days, no concrete action or legislation was seen initially.
Eventually, Biden's administration quietly worked on securing paid sick days for railroad workers.
Biden and Bernie Sanders played key roles in ensuring agreements with major freight carriers for sick days.
Unions, including IBEW, now have significant benefits like paid short notice sick days and more.
Beau acknowledges his initial skepticism but admits he was incorrect in doubting Biden's commitment.
Biden and Bernie continue to work behind the scenes to assist other unions in achieving their goals.

Actions:

for railroad workers, union supporters,
Support and advocate for unions in your community (implied)
Stay informed about labor disputes and negotiations (implied)
</details>
<details>
<summary>
2023-06-22: Let's talk about a GOP mistake with over energizing their base.... (<a href="https://youtube.com/watch?v=ABjqoOS7m3A">watch</a> || <a href="/videos/2023/06/22/Lets_talk_about_a_GOP_mistake_with_over_energizing_their_base">transcript &amp; editable summary</a>)

Republican Party's inflammatory rhetoric has backfired, leaving their base expecting action on manufactured issues without real solutions.

</summary>

"The Republican Party has been using incredibly inflammatory rhetoric, convincing a small group of their base that things are just out of control."
"That base that they put all of that effort into mobilizing and riling up. They're so riled up now that they expect the Republicans in the House to actually do something about these issues."
"It's kind of like running around yelling fire and then not grabbing a hose, not calling the fire department."
"The Republican Party has made an error. They've made a pretty big error here."
"They've made a mistake and I have no idea how they're going to turn it into happy little trees."

### AI summary (High error rate! Edit errors on video page)

Republican Party's inflammatory rhetoric may have backfired with their base.
Base expected action on manufactured issues pushed by the party.
The base believed the hyperbolic rhetoric and now expects real action.
The disconnect between manufactured issues and lack of real legislative action.
Base feeling let down by the Republican Party's failure to act on their manufactured concerns.
Republican Party facing internal discord due to unrealistic expectations from their base.
The rhetoric of an all-out battle to save America is now causing issues for the party.
Base realizing the disconnect between state and federal level claims made by Republicans.
Republican Party's mistake in riling up their base without a plan for action.
Uncertainty about how the Republican Party will address the fallout of their rhetoric.

Actions:

for political analysts, republican voters,
Mobilize within the Republican Party to address genuine concerns and avoid creating false narratives (implied).
Encourage open and honest communication within political parties to prevent misleading rhetoric (implied).
</details>
<details>
<summary>
2023-06-22: Let's talk about Trump getting the witness list.... (<a href="https://youtube.com/watch?v=_dGRP88P2PQ">watch</a> || <a href="/videos/2023/06/22/Lets_talk_about_Trump_getting_the_witness_list">transcript &amp; editable summary</a>)

Trump's team now has access to key information on witnesses in the Trump documents case, raising concerns about leaks and potential interference.

</summary>

"Harassing witnesses, leaking names, all of this is a really bad idea for Trump. It is a horrible idea."
"There's a part of me that thinks DOJ is providing him this stuff this early to just be like, do it, go ahead, and basically dare him to tamper, bother witnesses."
"He probably won't want to go down that route."

### AI summary (High error rate! Edit errors on video page)

Trump's team now has access to the list of people set to testify against him at trial.
The discovery process has started in the Trump documents case.
Speculation surrounds who within Trump's circle is cooperating or will testify against him.
Previously, when the DOJ had sole control, no one knew who was cooperating.
Team Trump now holds this information, raising concerns about potential leaks.
The speed at which the information was gathered and prepared for discovery is noteworthy.
Trump's strategy and reaction to the information now available to him are in question.
Harassing witnesses or leaking names is advised against, yet it's uncertain how Trump will proceed.
DOJ may have provided the information early to challenge Trump and deter interference with witnesses.
There is anticipation around how Trump will handle the situation as he gains access to critical information.

Actions:

for legal analysts, political strategists,
Monitor developments in the legal proceedings (implied)
Stay informed about the case progress and potential implications (implied)
</details>
<details>
<summary>
2023-06-22: Let's talk about Eastman and seeing it again later.... (<a href="https://youtube.com/watch?v=Sl1caKWVPUA">watch</a> || <a href="/videos/2023/06/22/Lets_talk_about_Eastman_and_seeing_it_again_later">transcript &amp; editable summary</a>)

Eastman's disciplinary proceedings in California, involving attempts to alter the 2020 election outcome, could have significant implications for future investigations and ethical considerations in the legal profession.

</summary>

"His actions were aimed at obstructing the electoral count on January 6th."
"Testimonies from these proceedings could have implications for investigations related to January 6th and Trump in Georgia."
"Many in the legal profession believe that the proceedings against Eastman are necessary for ethical reasons."

### AI summary (High error rate! Edit errors on video page)

Eastman is facing 11 disciplinary charges in California that could lead to the loss of his license due to his involvement in trying to alter the outcome of the 2020 election.
His actions were aimed at obstructing the electoral count on January 6th to prevent Vice President Pence from certifying Joe Biden as the winner.
The proceedings against Eastman are expected to last about a week with multiple witnesses being called.
Testimonies from these proceedings could have implications for investigations related to January 6th and Trump in Georgia.
Many in the legal profession believe that the proceedings against Eastman are necessary for ethical reasons, as his actions were not grounded in the law.
Witnesses called by Eastman might have direct knowledge of any potential plan.
The judge in the proceedings does not seem receptive to Eastman's arguments or style, similar to the situation with Meadows.
Information from Eastman could be critical for potential charges in Georgia and related to January 6th.
Testimonies from these proceedings might lead to further cooperation from Eastman with federal investigations.
Despite seeming unimportant now, the proceedings against Eastman could have significant future implications.

Actions:

for legal professionals,
Attend or follow updates on Eastman's disciplinary proceedings (exemplified)
Stay informed about the implications of these proceedings for future investigations (implied)
Support ethical considerations in legal practice (implied)
</details>
<details>
<summary>
2023-06-22: Let's talk about Biden, Boebert, McCarthy, and impeachment.... (<a href="https://youtube.com/watch?v=osPKUjOvmvo">watch</a> || <a href="/videos/2023/06/22/Lets_talk_about_Biden_Boebert_McCarthy_and_impeachment">transcript &amp; editable summary</a>)

Representative Boebert's illogical impeachment attempt on Biden prompts McCarthy's intervention to protect swing district Republicans from a divisive vote, illustrating the growing conflict within the Republican Party and the need for a shift towards moderation.

</summary>

"It was a pizza cutter motion, all edge, no point."
"McCarthy needs those representatives who are in vulnerable districts far more than he needs the hardline far-right Republicans."
"Protecting Biden from an impeachment process."
"Far-right Republicans, they're not a help to him. They're a hindrance."
"Toning down the rhetoric isn't something they've considered yet."

### AI summary (High error rate! Edit errors on video page)

Representative Boebert attempted a procedural move to force a vote on impeaching Biden, which seemed illogical.
McCarthy, the Republican Speaker of the House, intervened to delay the process, sending it to committees.
The pressure on far-right Republicans, like Boebert, is growing due to their impatience and belief in inflammatory rhetoric.
McCarthy's intervention was likely because the impeachment attempt wouldn't progress and could put swing district Republicans in a difficult position.
McCarthy needs moderate Republicans in vulnerable districts more than the far-right, hence his actions to move towards the center.
Protecting Biden from impeachment may be a strategic move by McCarthy to ensure his longevity as Speaker of the House.
The conflict within the Republican Party is driven by the tension between moderate Republicans and the far right, fueled by their own inflammatory rhetoric.
Toning down the rhetoric might be the only way out for Republicans facing pressure from their base.
More conflicts within the Republican Party are expected, with McCarthy likely to navigate towards the center to maintain his position.
The pressure on Republicans is a result of their own rhetoric, with the far right posing a hindrance rather than a help to McCarthy's goals.

Actions:

for politically active individuals,
Contact your representatives to express your views on the tactics and rhetoric within the Republican Party (suggested).
Join or support moderate Republican groups advocating for a shift towards the center in political discourse (implied).
</details>
<details>
<summary>
2023-06-21: Let's talk about the US-China hotline phone.... (<a href="https://youtube.com/watch?v=YS6X-hFh_WI">watch</a> || <a href="/videos/2023/06/21/Lets_talk_about_the_US-China_hotline_phone">transcript &amp; editable summary</a>)

China's strategic ambiguity in military communication with the U.S. serves its goals of keeping the U.S. off balance and maintaining flexibility.

</summary>

"China prefers ambiguity to keep the U.S. uncertain and off balance."
"Having an official hotline could limit China's flexibility in taking certain actions."
"China sees benefit in keeping the U.S. unsure about their intentions regarding Taiwan."

### AI summary (High error rate! Edit errors on video page)

China didn't give the U.S. a hotline for military communication, which the U.S. really wants.
Unofficial lines of communication already exist between the two countries' militaries.
Official hotlines are meant to maintain the status quo and prevent misunderstandings.
China's reluctance for an official hotline may be due to their strategic goals, like aiming for Taiwan.
China prefers ambiguity to keep the U.S. uncertain and off balance.
Having an official hotline could limit China's flexibility in taking certain actions.
The absence of an official line of communication allows China more freedom in its movements.
China may eventually agree to the hotline but is cautious not to appear too eager.
China wants to avoid signaling their intentions too clearly to the U.S.
China sees benefit in keeping the U.S. unsure about their intentions regarding Taiwan.

Actions:

for foreign policy analysts,
Establish unofficial lines of communication with international counterparts (implied)
Exercise caution in revealing strategic military intentions to maintain leverage (implied)
Take time in considering agreements that may impact national interests (implied)
</details>
<details>
<summary>
2023-06-21: Let's talk about the $6.2 billion error.... (<a href="https://youtube.com/watch?v=Ii0WKJH5ky8">watch</a> || <a href="/videos/2023/06/21/Lets_talk_about_the_6_2_billion_error">transcript &amp; editable summary</a>)

Beau breaks down a $6.2 billion Pentagon accounting error regarding equipment for Ukraine, revealing the impact of depreciation and the insignificance of the sum in the defense budget.

</summary>

"To us, $6.2 billion is a lot of money. To the Department of Defense, that's a rounding error."
"If you are one of those people calling for an audit of the Pentagon's finances, oh no, you're still good."
"The military did not account for depreciation."
"The equipment provided to Ukraine was not made this year."

### AI summary (High error rate! Edit errors on video page)

Explains a $6.2 billion accounting error made by the Pentagon in relation to money given to Ukraine.
Clarifies that the Pentagon doesn't hand cash to Ukraine but provides equipment.
Mentions the concept of depreciation and how it affected the accounting error.
Gives a detailed example of vehicle depreciation to illustrate the issue.
Points out that the equipment provided to Ukraine is not brand new but includes older stock.
Suggests visiting GovPlanet, an auction website, to see military Humvees for sale at low prices.
Notes that the missing $6.2 billion is not where all the money that disappears from the Pentagon goes.
Emphasizes that $6.2 billion is significant to normal people but a small fraction of the Department of Defense's budget.
Acknowledges the potential for audits of the Pentagon's finances but indicates that the accounting error is not the primary concern.

Actions:

for budget analysts,
Visit GovPlanet to see military equipment for sale (suggested)
Conduct audits of Pentagon finances (exemplified)
</details>
<details>
<summary>
2023-06-21: Let's talk about Trump and Meadows or Tom and Jerry.... (<a href="https://youtube.com/watch?v=VzjZuVvqAkM">watch</a> || <a href="/videos/2023/06/21/Lets_talk_about_Trump_and_Meadows_or_Tom_and_Jerry">transcript &amp; editable summary</a>)

Trump's team investigates if Mark Meadows has flipped, intensifying DOJ pressure and signaling potential cooperation, but solid proof remains elusive.

</summary>

"They are so concerned about this, they're actually sending people out to try to figure out what's going on."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump's former chief of staff, Mark Meadows, is at the center of a cat-and-mouse game with Trump's inner circle.
Trump's team investigated whether Meadows had flipped and was cooperating with federal authorities.
Meadows has cut off communication with Trump, causing nervousness in the Trump community.
Meadows' attorney made a statement suggesting Meadows is committed to telling the truth under legal obligation.
Reports indicate Trump's team is now using a rat emoji to depict Meadows in their communication.
There is no concrete evidence that Meadows has flipped and is cooperating.
Severing ties with someone under multiple investigations, like Trump, is standard advice from attorneys.
Cooperation could benefit Meadows, but there is no proof that he is providing incriminating information.
Trump's concern about Meadows cooperating may intensify pressure from the Department of Justice on Meadows.
The fact-finding mission conducted by Trump's team may have inadvertently signaled the importance of flipping Meadows to the Department of Justice.

Actions:

for political analysts,
Follow legal proceedings related to Meadows (implied)
</details>
<details>
<summary>
2023-06-21: Let's talk about SCOTUS and ideological capture.... (<a href="https://youtube.com/watch?v=rhNyNYvWXZg">watch</a> || <a href="/videos/2023/06/21/Lets_talk_about_SCOTUS_and_ideological_capture">transcript &amp; editable summary</a>)

Many doubted the Supreme Court's conservative image, but upcoming rulings will reveal its true makeup, cautioning against premature assumptions.

</summary>

"It's hopeful news. It's interesting news."
"The Republican Party messed up with their Supreme Court selections."
"We're gonna find out real quick."

### AI summary (High error rate! Edit errors on video page)

Many doubted the Supreme Court's conservative reputation after some surprise decisions.
Justices on the far-right wing have been writing dissenting opinions.
There's a belief that the court may not be as overwhelmingly conservative as thought.
The upcoming rulings on various issues will shed light on the court's true makeup.
Speculation suggests the Republican Party may have focused too narrowly on certain issues in selecting justices.
Beau advises cautious optimism until the court's decisions on contentious cases are revealed.

Actions:

for court observers,
Stay informed on upcoming Supreme Court rulings (implied)
</details>
<details>
<summary>
2023-06-20: Let's talk about your yearly reminder about hurricanes.... (<a href="https://youtube.com/watch?v=Nxpri68FIf8">watch</a> || <a href="/videos/2023/06/20/Lets_talk_about_your_yearly_reminder_about_hurricanes">transcript &amp; editable summary</a>)

Beau urges early hurricane preparedness, stressing the importance of supplies, evacuation plans, and readiness to aid others in the southeastern United States.

</summary>

"Do it now. Don't wait."
"Put the thought into it now. Put the thought into it this weekend."
"It makes it easier for people who come in and do relief if more people are set up to begin with."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Providing a yearly heads up for hurricane season, especially for those in the southeastern United States.
A tropical depression in the Gulf is expected to strengthen into a hurricane.
Urging people to prepare early and not wait until the storm is imminent.
Emphasizing the importance of having necessary supplies like non-perishable food, water, medications, and pet carriers.
Advising to plan evacuation routes and communicate with potential places to stay.
Recommending gradually building up supplies if resources are limited.
Suggesting getting tools for recovery and mentioning a specific inexpensive brand.
Encouraging people to make preparations now to avoid being caught off guard.
Mentioning the importance of being prepared to help neighbors and aid relief efforts.
Stressing the need for a battery-powered radio for emergency communication.

Actions:

for residents in hurricane-prone areas,
Gather non-perishable food and water supplies (suggested)
Prepare evacuation routes and communicate with potential hosts (suggested)
Purchase pet carriers or kennels for animals (suggested)
Gradually build up supplies if resources are limited (suggested)
Ensure you have necessary tools for recovery (suggested)
Stock up on supplies like batteries and a battery-powered radio (suggested)
</details>
<details>
<summary>
2023-06-20: Let's talk about whether Trump admitted it.... (<a href="https://youtube.com/watch?v=g5JARmKKzTY">watch</a> || <a href="/videos/2023/06/20/Lets_talk_about_whether_Trump_admitted_it">transcript &amp; editable summary</a>)

Former President Trump's interview statements on Fox News may not provide a strong legal defense, particularly concerning the intent behind document retention.

</summary>

"Before I send the boxes over, I have to take all of my things out."
"When you say before I return the documents, I have to do X, you are saying I am willfully retaining them until this time."
"The president, former president did himself no favors during that interview."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's interview on Fox News, where Fox News questioned and pushed back effectively.
Trump insisted he won the election despite losing, and defended his retention of documents with personal items in them.
The problem lies in the intent to retain the documents, not the reason for retention.
Trump's explanation in the interview may be problematic legally, especially regarding willful retention.
His statements in the interview may not be helpful in court, and his attorneys likely weren't happy with them.
Trump's defense of needing to take personal items out before returning documents doesn't clear him of charges.
The interview likely didn't do Trump any favors, and his explanations might not hold up legally.
Trump's statements during the interview, including his timeline for returning documents, may be revisited in the future.
Setting a timeline for returning documents is not within Trump's purview, especially when authorities are actively pursuing them.
Overall, Trump's interview statements may not serve as a strong defense for him legally.

Actions:

for legal analysts,
Analyze legal implications of statements (suggested)
Prepare for potential legal challenges (implied)
</details>
<details>
<summary>
2023-06-20: Let's talk about an update on Tennessee.... (<a href="https://youtube.com/watch?v=KTMqMuSog6s">watch</a> || <a href="/videos/2023/06/20/Lets_talk_about_an_update_on_Tennessee">transcript &amp; editable summary</a>)

Beau provides an update on the expulsion of two black men and a white woman from the Tennessee state legislature, criticizing it as a political stunt showing institutional racism.

</summary>

"This election is costing the people of Tennessee probably close to a million dollars, just so the state legislature could prove that, well, they're still pretty racist."
"All of this was a political stunt by Republicans and all it showed was that the majority of the state legislature in Tennessee does not believe in the will of the people."
"I have a feeling they're gonna stay in their place and their place is apparently the Tennessee State Legislature."

### AI summary (High error rate! Edit errors on video page)

Provides an update on a story involving the expulsion of two black men and a white woman from the Tennessee state legislature.
The local areas responsible for the representatives sent them right back despite the expulsion.
Both Pearson and Jones advanced through the Democratic primary.
Pearson will face John Johnston, an independent, while Jones will face Laura Nelson, a Republican in the special election on August 3rd.
The special election is costing Tennessee close to a million dollars.
Beau believes the special election is just a political stunt by Republicans to show racism.
He predicts that both Pearson and Jones will win the special election, rendering the entire process pointless.
Criticizes the state legislature for not believing in the will of the people and sending a message of young black men needing to stay in their place.

Actions:

for advocates for social justice.,
Support Pearson and Jones in the special election (suggested).
</details>
<details>
<summary>
2023-06-20: Let's talk about Hunter Biden's charges.... (<a href="https://youtube.com/watch?v=f5DqO2hi188">watch</a> || <a href="/videos/2023/06/20/Lets_talk_about_Hunter_Biden_s_charges">transcript &amp; editable summary</a>)

Beau breaks his rule to debunk false claims about Hunter Biden's case and praises President Biden's hands-off approach towards DOJ.

</summary>

"President Biden did not interfere with the Department of Justice (DOJ) and could have pardoned his son but chose not to."
"Can you imagine one of Trump's kids getting arrested while he was president? Of course not!"
"The crime becomes evident and then you look for the person. You don't look at a person and try to find a crime."
"It seems pretty clear that isn't how DOJ works."
"Did y'all know his name was Robert?"

### AI summary (High error rate! Edit errors on video page)

Hunter Biden, the sitting president's son, was charged with two counts of willful failure to pay taxes and a pretrial diversion on a gun charge related to a substance issue.
The charges could lead to anywhere from probation to up to a year in jail, but Beau believes jail time is unlikely.
Beau has a rule not to talk about private citizens, but he's breaking it because Republicans made false claims about Hunter Biden receiving a sweetheart deal from the Biden DOJ.
The prosecutor responsible for Hunter Biden's deal is actually a Republican and a Trump appointee.
President Biden did not interfere with the Department of Justice (DOJ) and could have pardoned his son but chose not to.
Beau contrasts Biden's hands-off approach with what he believes Trump might have done if one of his children were in a similar situation.
Beau mentions comparisons to Kushner's actions but cautions against seeking retribution based on partisanship.
Beau expresses surprise at learning Hunter Biden's full name is Robert.

Actions:

for those interested in clarifying misinformation and understanding political dynamics.,
Fact-check misinformation spread about political figures (implied)
</details>
<details>
<summary>
2023-06-19: Let's talk about Trump's trial being televised.... (<a href="https://youtube.com/watch?v=bPbllVNFgRU">watch</a> || <a href="/videos/2023/06/19/Lets_talk_about_Trump_s_trial_being_televised">transcript &amp; editable summary</a>)

Major outlets push for Trump's trial to be televised for profit, but Beau believes the outcome, not the broadcast, truly matters.

</summary>

"Don't let them lie to you."
"What matters to the soul of America is not whether or not this is televised."
"It's probably part of a larger strategy to avoid giving Trump the delays that he wants."

### AI summary (High error rate! Edit errors on video page)

Major outlets are pushing for Trump's trial to be televised as vital for American education, but their true motive is profit.
Televising the trial will result in major outlets making significant money off sound bites and commentary.
Despite potential benefits for some viewers, the trial being broadcast may lead to out-of-context clips favoring Trump, causing misinformation.
Out-of-context statements will be made regardless of whether the trial is televised.
Beau personally believes the televised trial is irrelevant and not vital to America's soul; the outcome of the trial is what truly matters.
Advises not to panic if the special counsel appears to be giving Trump what he wants in early motions, as it may be part of a larger strategy to avoid delays.
Special counsel's focus seems to be on moving the case forward swiftly due to its perceived ironclad nature.
Beau suggests viewing the special counsel's actions as a positive sign and part of a strategy to prevent unnecessary delays for Trump.

Actions:

for media consumers,
Stay informed on the trial progress and outcomes (implied)
Do not panic over early motions in the trial; view them as part of a strategic move (implied)
</details>
<details>
<summary>
2023-06-19: Let's talk about Esper talking about Trump.... (<a href="https://youtube.com/watch?v=Y96MO7xYrVY">watch</a> || <a href="/videos/2023/06/19/Lets_talk_about_Esper_talking_about_Trump">transcript &amp; editable summary</a>)

Former Defense Secretary Esper and Beau stress the grave danger of Trump's mishandling of classified information, urging a focus on national security over personal loyalty or political affiliations.

</summary>

"This man can never have access to classified material again."
"He's lying to you."
"Nobody who loves this country, nobody who values American supremacy the way that he claims he does,  "
"The amount of lives that would be lost if these documents fell into opposition hands is immense."
"Trump's already put U.S. troops at risk."

### AI summary (High error rate! Edit errors on video page)

Former Defense Secretary Esper emphasized the danger of Trump's actions in mishandling classified documents.
Unauthorized, illegal, and dangerous were the key terms Esper used to describe the situation.
Imagining the consequences of foreign agents accessing U.S. vulnerabilities was a focal point of Esper's concerns.
The impact on America's military readiness and ability to execute attacks was underscored by Esper.
The gravity of unauthorized disclosure of war plans and sensitive information was compared to historical scenarios like World War II.
The potential for adversaries to exploit leaked information to develop countermeasures against U.S. plans was a significant worry.
Beau pointed out the risk Trump's actions posed to national security and military operations.
The necessity to prevent Trump from accessing classified material in the future was strongly advocated by Esper and Beau.
Loyalty to an individual over the safety of troops and national security was criticized by Beau.
Beau emphasized the serious implications of Trump's actions, urging viewers to prioritize country over personality or politics.

Actions:

for concerned citizens, patriots.,
Advocate for strict measures preventing individuals like Trump from accessing classified information (implied).
Prioritize national security over personal loyalty or political affiliations by engaging in informed discourse and decision-making (implied).
</details>
<details>
<summary>
2023-06-19: Let's talk about California and Ospreys.... (<a href="https://youtube.com/watch?v=wluc2N1dgsA">watch</a> || <a href="/videos/2023/06/19/Lets_talk_about_California_and_Ospreys">transcript &amp; editable summary</a>)

Residents in California witness military activity as preparation for the President's visit, prompting Beau to address unwarranted concerns and stress the importance of conserving resources for disasters.

</summary>

"The concern is unwarranted. It's not a big deal."
"The one thing that is important to note about this..."
"But everything you're seeing out there in California, totally normal, no big deal."
"The context is that it's just normal pre-gaming for a presidential appearance."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Military aircraft movement in California is causing concern in suburban areas.
Residents are witnessing a buildup of aircraft, ground vehicles, Humvees, ambulances, and SWAT teams.
This activity is due to the President's upcoming visit, not an invasion.
People in areas frequented by presidents are familiar with this pre-visit preparation.
Beau explains the importance of preserving resources for disaster response rather than allocating them for presidential visits.
He underscores the normalcy of the situation and dismisses unwarranted concerns.
The heightened security measures are typical before a presidential appearance.
Beau addresses the potential confusion and alarm caused by the unfamiliar sight of military presence in California.
He contrasts the current situation with the need to conserve resources for disasters.
The activity observed is part of routine preparations for the President's visit and should not raise alarm.

Actions:

for california residents,
Stay informed about upcoming presidential visits in your area (implied).
</details>
<details>
<summary>
2023-06-18: Let's talk about orcas and needing a bigger boat.... (<a href="https://youtube.com/watch?v=-qTAJ0TA988">watch</a> || <a href="/videos/2023/06/18/Lets_talk_about_orcas_and_needing_a_bigger_boat">transcript &amp; editable summary</a>)

Orcas attacking boats off Europe's coast puzzles experts with theories ranging from playfulness to revenge, sparking media coverage and memes.

</summary>

"Orcas have been attacking, disabling, ramming, and in some cases, sinking boats off the coast of Europe."
"Why is it happening? Short answer is they don't know."
"The situation has garnered media coverage and inspired memes."
"The incident raises questions about the motivations behind the orcas' actions."
"Ultimately, the mystery of why orcas are attacking boats remains unsolved."

### AI summary (High error rate! Edit errors on video page)

Orcas have been attacking, disabling, ramming, and in some cases, sinking boats off the coast of Europe.
Experts are unsure why this behavior is happening, but some leading theories include the idea that orcas may be playing with boats because they enjoy the feeling of propellers running.
Another theory involves a specific orca named White Gladys, who may be teaching other orcas to attack boats as revenge for a past incident.
There's also a theory that the same orca incident involving nets or other altercations could have made orcas more defensive, trying to signal ships to leave.
Researchers initially thought this behavior might be a passing fad, but with an increase in incidents, that theory seems less likely.
The situation has garnered media coverage and inspired memes, with some suggesting that orcas are targeting yachts owned by wealthy individuals.
Despite the entertainment value of the coverage, there is still no definitive answer as to why the orcas are behaving this way.
Some have speculated that perhaps Mother Nature is reacting in some way to these events, though it remains uncertain.
The incident raises questions about the motivations behind the orcas' actions and whether there is a deeper reason for their behavior.
Ultimately, the mystery of why orcas are attacking boats remains unsolved, leaving room for speculation and debate.

Actions:

for marine conservationists,
Monitor and report any incidents of orca interactions with boats to relevant authorities (implied)
Support research and conservation efforts for marine ecosystems and wildlife (implied)
</details>
<details>
<summary>
2023-06-18: Let's talk about Ukraine and the US defense budget.... (<a href="https://youtube.com/watch?v=mzvQ-rYly2k">watch</a> || <a href="/videos/2023/06/18/Lets_talk_about_Ukraine_and_the_US_defense_budget">transcript &amp; editable summary</a>)

Beau explains the significance of providing Ukraine with longer-range missiles in the defense budget, potentially saving lives and impacting the conflict dynamics.

</summary>

"It's about range. It is about range."
"Providing Ukraine with these missiles could potentially help in any attempt to retake Crimea."
"Providing these missile systems could save lives on both the Russian and Ukrainian sides."
"There's ongoing debate and opposition due to concerns about providing Ukraine with weapons that could potentially hit within Russia."
"It's not a wonder weapon, but it's really going to matter in a couple of different places."

### AI summary (High error rate! Edit errors on video page)

Explaining the inclusion of an earmark in the next year's defense budget to provide Ukraine with ATACMS, a missile box with longer range than what Ukraine currently has.
Initially, the U.S. was hesitant to provide these to Ukraine due to concerns about their potential use.
The U.S. appears more open to providing these missiles now, possibly due to a plan being worked out for their use.
Providing Ukraine with these missiles could potentially help in any attempt to retake Crimea, closing the gap between Russian weapons and Ukrainian locations.
Beau believes that providing these missile systems could save lives on both the Russian and Ukrainian sides by limiting frontline fighting.
He doesn't think Ukraine will misuse these weapons and sees a strong need for them.
There's ongoing debate and opposition due to concerns about providing Ukraine with weapons that could potentially hit within Russia.
The missile system's significance lies in its range and how it could impact different areas, rather than being a wonder weapon.
Ukraine is still strategizing and trying to draw Russia out in certain areas amidst the ongoing conflict.
The missile system could play a significant role in various scenarios, but it's mainly about range and its impact.

Actions:

for global citizens,
Support organizations advocating for peaceful resolutions and diplomacy in international conflicts (implied)
</details>
<details>
<summary>
2023-06-18: Let's talk about Russian nukes in Belarus.... (<a href="https://youtube.com/watch?v=cC2bDZeQZl8">watch</a> || <a href="/videos/2023/06/18/Lets_talk_about_Russian_nukes_in_Belarus">transcript &amp; editable summary</a>)

Russia's transfer of tactical nukes to Belarus is a foreign policy move mirroring US strategies, aiming at containment and prevention, not immediate military action.

</summary>

"Containment was not actually a super successful policy, but it's interesting to see Russian foreign policy being described in terms from the Cold War."
"It's worth noting that anytime nuclear weapons get discussed, the US gets nervous."
"This isn't something I would get particularly to be alarmed about."

### AI summary (High error rate! Edit errors on video page)

Russia has transferred tactical nukes to Belarus, sparking questions.
Russia placed its nukes in Belarus, not giving them to the country.
The move is a foreign policy decision, not a military one.
Putin mentioned extreme measures if Russian statehood is threatened.
Russia's posture on using nukes remains unchanged: only in real existential threat scenarios.
The announcement emphasized that the nukes are there to prevent a strategic defeat from the West.
Putin cited containment as the reason behind the move, a term from the Cold War era.
The transfer of nukes is more about messaging rather than preparation for use.
This action mirrors US policies of deploying tactical nukes in certain regions.
The move signifies a soft colonialism approach in Russian foreign policy.

Actions:

for foreign policy analysts,
Monitor and analyze the geopolitical implications of Russia's transfer of tactical nukes to Belarus (suggested).
Stay informed about nuclear policies and developments globally (suggested).
</details>
<details>
<summary>
2023-06-18: Let's talk about Minneapolis, cops, and decrees.... (<a href="https://youtube.com/watch?v=XqJ4pBZ26dw">watch</a> || <a href="/videos/2023/06/18/Lets_talk_about_Minneapolis_cops_and_decrees">transcript &amp; editable summary</a>)

A two-year probe in Minneapolis uncovered systemic discrimination and excessive force within the police department, leading to a consent decree and ongoing efforts to address deep-rooted issues.

</summary>

"A two-year probe in Minneapolis revealed systemic discrimination in the police department."
"Throwing out the bad apples as quickly as possible is what matters."
"The fallout from George Floyd's death is ongoing."

### AI summary (High error rate! Edit errors on video page)

A two-year probe in Minneapolis revealed systemic discrimination in the police department, including excessive force and unjustified use of lethal force.
Marginalized groups in the city faced the brunt of the department's behavior, particularly Black Americans, Native Americans, and those with behavioral health issues.
The report uncovered that the restraint used on George Floyd was applied hundreds of times, with over 40 instances where an arrest wasn't necessary.
The city and police department have now entered into a consent decree, with federal oversight and mandated reforms.
The success of consent decrees depends on the willingness of department leadership to remove problematic officers, which can have a greater impact on culture than policy changes.
Addressing illegal behavior during the reviewed period varies, sometimes resulting in charges depending on the strength of the case.
The fallout from George Floyd's death is ongoing, with the need for long-term corrections to address the systemic issues uncovered.

Actions:

for reform advocates, community members,
Join or support local organizations advocating for police reform (suggested)
Stay informed about the progress of mandated reforms in the Minneapolis police department (exemplified)
</details>
<details>
<summary>
2023-06-17: The roads to cultural shifts and change.... (<a href="https://youtube.com/watch?v=IztFQtTd1A0">watch</a> || <a href="/videos/2023/06/17/The_roads_to_cultural_shifts_and_change">transcript &amp; editable summary</a>)

Beau addresses discomfort with LGBTQ issues, urging self-reflection and acceptance of evolving gender norms to facilitate change and understanding.

</summary>

"It's not a new thing. It's been around."
"Sometimes once you acknowledge something like this, it goes away."
"You're probably not hateful."
"You're not too old to change. You are changing."
"Just acknowledge that things are changing."

### AI summary (High error rate! Edit errors on video page)

Explains cultural shifts, unconscious bias, and discomfort surrounding LGBTQ issues.
Responds to a viewer who struggles with acceptance of trans people in the public eye.
Analyzes the viewer's discomfort and unconscious bias towards trans individuals.
Points out the viewer's "passing privilege" and the societal acceptance of those who fit neatly into traditional gender norms.
Encourages self-reflection and understanding of discomfort to facilitate change and acceptance.
Provides historical examples of gender representation in media to illustrate the evolution over time.
Suggests that discomfort may stem from unfamiliarity with non-traditional gender presentations.
Emphasizes the importance of acknowledging changing societal norms and embracing diversity.
Validates the viewer's willingness to learn and change, despite their initial discomfort.
Offers advice on addressing discomfort and adapting to societal progress regarding gender identity.

Actions:

for individuals grappling with unconscious bias.,
Analyze moments of discomfort around unfamiliar gender presentations (suggested).
Acknowledge societal changes in gender norms and embrace diversity (implied).
</details>
<details>
<summary>
2023-06-17: Let's talk about the Illinois book ban ban.... (<a href="https://youtube.com/watch?v=OiseaHrxSbE">watch</a> || <a href="/videos/2023/06/17/Lets_talk_about_the_Illinois_book_ban_ban">transcript &amp; editable summary</a>)

Beau outlines Illinois' book ban ban, requiring libraries to uphold ethical standards and provide books based on public demand.

</summary>

"The states that are forcing libraries to remove books because they hurt somebody's feelings or you know they disagree with somebody's personal religious beliefs, they are forcing libraries to go against their own code of ethics."
"A library isn't there to be an archive. It's there to provide services to the public. It's there to provide books that people want."
"This legislation is a good move."
"I really hope that other states follow suit and create a situation where public libraries can operate under the ethics that they've set for themselves."
"They're trying to fulfill the promises they've made to the public, let them do it."

### AI summary (High error rate! Edit errors on video page)

Explains the Illinois book ban ban, clarifying that it doesn't actually ban books but restricts funding for libraries.
Outlines what libraries in Illinois must do to comply with the legislation, including adopting the American Library Association's Library Bill of Rights.
Emphasizes the importance of not excluding materials based on origin, background, or views, and avoiding removal due to partisan or doctrinal disapproval.
Argues that libraries should provide books based on public demand, even if some individuals disagree, as their role is to serve the community.
Commends Illinois for taking a positive step and hopes other states will follow suit to allow public libraries to uphold their ethical standards.

Actions:

for library advocates, book lovers,
Support your local library by advocating for their autonomy and adherence to ethical standards (exemplified)
</details>
<details>
<summary>
2023-06-17: Let's talk about loyalty tests and Asa.... (<a href="https://youtube.com/watch?v=sgmwoYHNtwU">watch</a> || <a href="/videos/2023/06/17/Lets_talk_about_loyalty_tests_and_Asa">transcript &amp; editable summary</a>)

Asa Hutchinson questions loyalty oaths in politics, raising concerns about supporting a potentially convicted nominee like Trump.

</summary>

"They're demanding people who apparently take an oath seriously, take a pledge seriously."
"A politician who apparently cares enough about an oath or a pledge to make sure it's something that they can live up to."
"That doesn't seem to be good long-term planning to me."

### AI summary (High error rate! Edit errors on video page)

Asa Hutchinson and the Republican Party require candidates to sign a pledge to support the eventual nominee for president, regardless of legal issues.
Hutchinson sought clarification on whether he should remain loyal to a candidate facing legal troubles.
Hutchinson refuses to support a candidate convicted under the Espionage Act or serious crimes.
The demand for loyalty to Trump seems ironic given his disregard for pledges and oaths.
Trump's handling of classified information raises doubts about extending loyalty and oaths to him.
Hutchinson's call to amend the pledge shows a rare concern for honoring oaths in politics.
The Republican Party may face consequences for demanding loyalty to Trump if he faces conviction.
Candidates pledging loyalty to Trump could suffer from attack ads if he is convicted and still wins the nomination.
Taking a loyalty oath to a potentially convicted individual doesn't seem like a wise long-term strategy for the Republican Party.

Actions:

for political observers,
Question the loyalty demands placed on political candidates (suggested)
Advocate for ethical and accountable leadership in political parties (implied)
</details>
<details>
<summary>
2023-06-17: Let's talk about Raskin asking for Biden tape info.... (<a href="https://youtube.com/watch?v=-Rn0MBhdeg0">watch</a> || <a href="/videos/2023/06/17/Lets_talk_about_Raskin_asking_for_Biden_tape_info">transcript &amp; editable summary</a>)

Raskin seeks FBI information on Biden allegations; Republicans knowingly mislead base with unverified claims to distract from failures.

</summary>

"Who could have seen this coming? So my guess here is that he is looking for what I called the determination memo in that first video."
"It's not surprising at this point that politicians are doing this, surprising that the same people keep falling for it over and over and over again."
"They needed a scandal. They needed something to distract from their failed policies or candidates getting in trouble."

### AI summary (High error rate! Edit errors on video page)

Raskin, from the Democratic Party, is reaching out to the FBI for more information on allegations against President Biden.
Raskin is likely seeking an assessment memo from the FBI regarding unverified information on the FD-1023.
The assessment memo may reveal that the FBI found insufficient evidence to escalate the probe on Giuliani's allegation.
Republicans were aware of the FBI's assessment but still chose to mislead their base with conspiracy theories.
Politicians, particularly the Republican Party, continue to mislead their supporters with baseless claims.
The pursuit of the allegations against President Biden is deemed as a distraction tactic by the Republican Party.
Republicans intentionally focused on certain unverified information to fuel a scandal.
Beau suggests that the Republican Party needed a scandal to divert attention from their failed policies.
Beau hints at the possibility of damaging search results if people were to google "President Admits to Crime on Tape" related to Biden.

Actions:

for political activists, democratic supporters,
Contact your representatives to demand transparency and accountability in political dealings (implied).
Share verified information to combat baseless conspiracy theories within your community (implied).
</details>
<details>
<summary>
2023-06-17: Let's talk about Jack Smith's motion on discovery.... (<a href="https://youtube.com/watch?v=RsEQKxN6tlM">watch</a> || <a href="/videos/2023/06/17/Lets_talk_about_Jack_Smith_s_motion_on_discovery">transcript &amp; editable summary</a>)

Special counsel's office files a motion to restrict disclosure of discovery material in Trump's case, aiming to control public statements without muzzling his campaign.

</summary>

"This motion is basically a shut your client up motion."
"It's not abnormal, it's not an attempt to muzzle his campaign or anything like that."
"But at the same time I'm doubtful he'll follow the protective orders."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Special counsel's office filed a motion related to Trump, seeking a protective order regarding discovery material.
The motion aims to prevent Trump and his lawyers from disclosing information that could compromise ongoing investigations or identify uncharged individuals.
Trump's defense team is unlikely to oppose the motion, as it may help them control Trump's public statements.
The protective order restricts the disclosure of materials to specific individuals involved in the defense, witness interviews, and court-approved entities.
Trump is allowed to express opinions on social media but must refrain from sharing specific discovery material.
The motion does not inhibit Trump from campaigning or expressing his views but focuses solely on the handling of discovery material.
While Trump's attorneys may advise him to refrain from discussing the case, his compliance with protective orders remains uncertain.

Actions:

for legal analysts, political commentators,
Follow updates on the legal proceedings related to Trump's case (suggested)
Analyze the implications of protective orders in high-profile legal cases (implied)
</details>
<details>
<summary>
2023-06-16: Let's talk about why companies still like rainbows.... (<a href="https://youtube.com/watch?v=7iaTejXGYpo">watch</a> || <a href="/videos/2023/06/16/Lets_talk_about_why_companies_still_like_rainbows">transcript &amp; editable summary</a>)

Despite resistance, marketing rainbows persists due to capitalist interests and the economic power of supportive LGBTQ+ individuals.

</summary>

"It is more profitable to be a good person. It is more profitable to be accepting."
"The reason you see rainbows everywhere is because of, well, the right wing. Capitalism."
"The money is always an acceptance."

### AI summary (High error rate! Edit errors on video page)

Explains the prevalence of marketing rainbows and why they will likely continue despite efforts to stop them.
Mentions a survey where 70% of non-LGBTQ+ individuals believe companies should show support through advertising, sponsorship, and hiring practices.
Points out that there are an estimated 17 million LGBTQ+ individuals in the U.S. who are supportive and hold significant economic power.
Companies are interested in tapping into this trillion-dollar market and are more likely to support a community with immutable characteristics for long-term gains.
Notes the risk of losing customers permanently by alienating a community versus temporarily upsetting others who may eventually return.
States that companies prioritize profit over social responsibility, akin to countries pursuing power in foreign policy.
Attributes the widespread appearance of rainbows to capitalism and the profitability of being accepting and inclusive.
Suggests that there are not enough individuals invested in culture wars to significantly impact the prevalent display of rainbows.
Mentions examples like Target moving displays but also investing in pride celebrations, showing that acceptance can be profitable.
Concludes that being a good person and accepting leads to a wider market and ultimately more profit.

Actions:

for marketers, lgbtq+ allies,
Support LGBTQ+ businesses and initiatives (exemplified)
Attend and participate in pride celebrations and events (exemplified)
</details>
<details>
<summary>
2023-06-16: Let's talk about one of Trump's trial dates being set.... (<a href="https://youtube.com/watch?v=C3IyTOBkXVE">watch</a> || <a href="/videos/2023/06/16/Lets_talk_about_one_of_Trump_s_trial_dates_being_set">transcript &amp; editable summary</a>)

Trial date set for Trump's 2019 legal entanglement with E. Jean Carroll, with new defamatory statements potentially impacting his pursuit of the Republican nomination while under indictment.

</summary>

"Trial date set in Trump's legal entanglement."
"Trump is still pursuing the Republican nomination while under indictment."
"New defamatory statements will be rolled into the first case."
"Just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trial date set in Trump's legal entanglement from 2019 suit by E. Jean Carroll.
First suit caught up in delays finally moving forward to trial on January 15th, 2024.
Trump found liable in second suit for battery and defamation last month.
Trump unlikely to attend trial as it is a civil matter.
Trial date just one week before Iowa Republican caucuses.
Judge allows Carroll to amend her suit to include new allegedly defamatory statements by Trump.
New statements made after Trump was found liable in the second suit.
Trump's new statements will now be part of the first case to be decided in January.
Trump is still pursuing the Republican nomination while under indictment.
Uncertainty if this trial will conflict with other potential trials Trump may face.

Actions:

for legal analysts, political commentators, concerned citizens,
Monitor updates on the trial proceedings and outcomes (implied)
Stay informed about political and legal developments related to Trump's cases (implied)
</details>
<details>
<summary>
2023-06-16: Let's talk about Trump ignoring advice.... (<a href="https://youtube.com/watch?v=ENWyExLFeDc">watch</a> || <a href="/videos/2023/06/16/Lets_talk_about_Trump_ignoring_advice">transcript &amp; editable summary</a>)

Recent reporting reveals former president's decision to retain documents despite advice, risking legal consequences.

</summary>

"Returning the documents could have made the issue disappear without charges."
"Ignoring counsel's advice could be a huge problem for the former president in court."

### AI summary (High error rate! Edit errors on video page)

Recent reporting on former president's decision-making process regarding documents case.
Advisors urged return of documents, even suggesting a negotiated settlement with the Department of Justice to avoid criminal charges.
Trump decided to hang on to the documents based on advice from a partisan organization.
Beau points out that returning the documents could have made the issue disappear without charges.
Department of Justice and FBI extended courtesies to former president, which were not accepted.
Ignoring counsel's advice to return the documents could be a huge problem for the former president in court.
Willful retention of documents despite advice is a damaging move for Trump's defense.
Beau concludes by expressing his thoughts and wishing the viewers a good day.

Actions:

for legal analysts,
Contact legal representatives for advice on handling sensitive documents (implied)
Stay informed about legal implications of document retention (implied)
</details>
<details>
<summary>
2023-06-16: Let's talk about Miami's Mayor announcing.... (<a href="https://youtube.com/watch?v=kJaCF_0MdUQ">watch</a> || <a href="/videos/2023/06/16/Lets_talk_about_Miami_s_Mayor_announcing">transcript &amp; editable summary</a>)

Miami Mayor's unlikely presidential bid strategically aims to boost profile for potential future political runs, impacting Florida's political landscape and potentially splitting Republican votes.

</summary>

"The mayor of Miami has announced that they are going to seek the Republican Party nomination."
"Nobody has ever accused this man of not understanding politics."
"It's a unique development that is probably very unwelcome by a lot of Republicans."
"I think this is a very calculated move to raise his profile, to put him in a position to win in other places later."
"It may create a situation during the primary where people who would have voted for DeSantis in an area he actually has a chance of winning in while they vote for somebody else."

### AI summary (High error rate! Edit errors on video page)

The mayor of Miami announced his intention to seek the Republican Party nomination for president.
It is unlikely that the mayor will actually secure the nomination.
The move is seen as a strategic attempt by the mayor to raise his profile for potential future political runs.
Mayor Suarez's popularity in Miami could impact the current governor's chances during the primary.
This decision may have consequences for the Republican Party in terms of splitting votes and potentially benefiting former President Trump.
Despite the slim chance of winning, the mayor's move is not entirely irrelevant and could have significant implications.
The mayor's calculated decision is likely aimed at positioning himself for success in other political roles.
The primary motive behind the mayor's announcement seems to be elevating his national and state stage presence.
There are speculations that this move could serve as an audition for a vice-presidential candidacy.
The mayor understands the political landscape and is not under the illusion of a guaranteed nomination.

Actions:

for political observers,
Support local political candidates in Miami to understand their platforms and impact (implied)
Stay informed about the political developments in Florida and how they might influence national politics (implied)
</details>
<details>
<summary>
2023-06-15: Let's talk about the Dems ending the influence of MAGA Republicans.... (<a href="https://youtube.com/watch?v=e3zQq81-iNY">watch</a> || <a href="/videos/2023/06/15/Lets_talk_about_the_Dems_ending_the_influence_of_MAGA_Republicans">transcript &amp; editable summary</a>)

The US House faces internal conflicts as moderate Republicans seek collaboration with Democrats to sideline the far-right faction and shift towards centrist policies.

</summary>

"Democrats might assist in procedural votes to moderate legislation and curb far-right demands."
"Progressive Democrats may not always agree with the outcomes but view it as a political maneuver to prevent far-right domination."
"The move is seen as a win-win for those involved, allowing for a shift towards more centrist policies and increased Democratic influence in the House."

### AI summary (High error rate! Edit errors on video page)

The US House of Representatives is facing internal conflicts between moderate Republicans and MAGA Republicans, leading to potential changes in party dynamics.
Dan Bacon, a centrist Republican from Nebraska, suggested working with Democrats for procedural votes instead of catering to extreme factions.
MAGA Republicans sabotaged a rule vote, a rare occurrence that disrupted the legislative agenda and prompted a push to sideline them.
The plan to sideline the extreme faction involves moderate Republicans teaming up with Democrats to shift the center of gravity in the House.
Democrats might assist in procedural votes to moderate legislation and curb far-right demands, benefiting both parties.
This strategy aims to reduce the influence of extreme Republicans and secure votes for Democrats in competitive districts.
Progressive Democrats may not always agree with the outcomes but view it as a political maneuver to prevent far-right domination.
The potential collaboration between moderate Republicans and Democrats could reshape the legislative landscape and benefit both parties.
There is a political calculation involved in this strategy to prevent the Republican Party from yielding to far-right demands.
The move is seen as a win-win for those involved, allowing for a shift towards more centrist policies and increased Democratic influence in the House.

Actions:

for politically active individuals,
Reach out to representatives for transparency on internal party dynamics and support centrist policies (suggested)
Stay informed on how collaboration between parties may impact legislative outcomes (implied)
</details>
<details>
<summary>
2023-06-15: Let's talk about the Biden tapes.... (<a href="https://youtube.com/watch?v=hv7h80Guyxs">watch</a> || <a href="/videos/2023/06/15/Lets_talk_about_the_Biden_tapes">transcript &amp; editable summary</a>)

Beau addresses the Biden tapes, challenging their existence and refusing to cover unverified information until proven, urging focus on actual evidence.

</summary>

"If they show up, believe me, I will give you a play-by-play on those tapes."
"Nobody has. Nobody has. Believe me, I would not shy away from what would be the biggest story in 10 years if you have these recordings."
"I have a feeling that this is going to turn out to be a giant nothing burger."
"Until the tapes are actually shown to exist, I can't cover them. I don't make things up."
"Rather than sending messages about tapes that nobody knows anything about, you should sit down and read the indictment."

### AI summary (High error rate! Edit errors on video page)

Addressing the Biden recordings and the need to verify information.
Acknowledging a message questioning why he hasn't covered the tapes.
Expressing willingness to read a transcript if provided and if it's breaking news.
Not having access to the tapes and questioning their existence.
Explaining the FD 1023 form used to record unverified information about the tapes.
Challenging the belief that people have actually listened to the tapes.
Being open to covering the tapes if they are proven to exist.
Criticizing the focus on nonexistent tapes rather than documented evidence against Trump.
Emphasizing the importance of reading the actual indictment and transcripts.
Refusing to cover unverified information and maintaining integrity in reporting.

Actions:

for media consumers,
Read the indictment and listen to or read the transcripts of documented evidence against Trump (suggested).
Share verified information to combat misinformation in media (implied).
</details>
<details>
<summary>
2023-06-15: Let's talk about a post-Trump GOP.... (<a href="https://youtube.com/watch?v=Ss-Z1Z3QKRE">watch</a> || <a href="/videos/2023/06/15/Lets_talk_about_a_post-Trump_GOP">transcript &amp; editable summary</a>)

Republicans are imagining a post-Trump GOP as loyalists pivot away, facing the challenge of keeping the MAGA base while distancing from Trump through potential pardons.

</summary>

"I cannot defend what's alleged. These are serious allegations."
"It's happening. So at this point where we are is who's going to be the nominee."
"There's nothing they can do about that because they don't have the centrist vote..."
"But that leaves candidates in a bizarre position. They still need the MAGA base."
"That's how they plan on keeping control of the MAGA base."

### AI summary (High error rate! Edit errors on video page)

Republicans are imagining a post-Trump GOP as some loyalists pivot away from the former president.
GOP operatives are suggesting that Trump may end up in prison, leading to pressure on major candidates to distance themselves from him.
The shift within the Republican Party is slowly beginning, with more Republicans expected to abandon Trump.
Candidates still need the MAGA base's support but also have to distance themselves from Trump to protect the party's image.
To keep the MAGA base energized, candidates may heavily lean into the idea of pardoning Trump.
The plan to keep control of the base involves promising to pardon Trump for any serious allegations or criminal actions.
The GOP wants to maintain the MAGA movement without being overshadowed by specific personalities.
Overall, there is a potential shift within the Republican Party away from Trump, with focus on keeping the base energized and united.

Actions:

for gop members,
Pressure major candidates to distance themselves from Trump (implied)
Support candidates who focus on maintaining the MAGA base without being overshadowed by specific personalities (implied)
</details>
<details>
<summary>
2023-06-15: Let's talk about Fox calling Biden a wannabe.... (<a href="https://youtube.com/watch?v=SHkDzgS5b-E">watch</a> || <a href="/videos/2023/06/15/Lets_talk_about_Fox_calling_Biden_a_wannabe">transcript &amp; editable summary</a>)

Fox News sparks controversy by labeling President Biden while raising concerns about its presence in military installations.

</summary>

"Fox News labeled President Biden as a 'wannabe dictator,' sparking attention and controversy."
"There's a push to remove Fox News from military installations to prevent morale issues and chain of command disruptions."
"Failure to address the situation may impact commanders' careers and be viewed negatively in the future."

### AI summary (High error rate! Edit errors on video page)

Fox News labeled President Biden as a "wannabe dictator," sparking attention and controversy.
Beau points out the contrast between Biden and the former president who attempted a self-coup and promised to strip people of citizenship through executive orders.
Fox News' pattern of being inflammatory, misleading, and absurd is nothing new, according to Beau.
Beau raises concerns about Fox News still having access to numerous military installations.
Beau questions the consequences if a veteran on a military base distributes flyers undermining the chain of command.
There's a push to remove Fox News from military installations to prevent morale issues and chain of command disruptions.
Failure to address the situation may impact commanders' careers and be viewed negatively in the future.

Actions:

for military commanders,
Remove Fox News from military installations (suggested)
Address morale issues caused by Fox News presence (implied)
</details>
<details>
<summary>
2023-06-14: Let's talk about questions on Trump's other cases.... (<a href="https://youtube.com/watch?v=ooFG63lVTZM">watch</a> || <a href="/videos/2023/06/14/Lets_talk_about_questions_on_Trump_s_other_cases">transcript &amp; editable summary</a>)

Beau clarifies misconceptions surrounding Trump's arraignment and explains why adjournment of state cases is bad news for him, stressing that adjournment means a pause, not dismissal.

</summary>

"Adjourned does not mean dismissed."
"Words have definitions."
"That is absolutely not good news."
"Once the federal case is over, the state cases resume."
"It's just a pause."

### AI summary (High error rate! Edit errors on video page)

Explains that Trump's arraignment in a documents case does not mean the federal inquiry into the January 6th issue will stop.
Notes that two people involved in the fake electors scheme appeared before the January 6th grand jury on the same day Trump was arraigned.
Clarifies that adjourned in the context of state cases does not mean dismissed but rather paused.
States that adjourning the state-level proceedings due to federal scheduling conflicts means Trump's time facing criminal charges is extended.
Emphasizes that once the federal case is over, the state cases will resume, leading to a prolonged period of Trump dealing with criminal charges.

Actions:

for legal observers, political analysts.,
Stay informed on the legal proceedings involving Trump to understand the implications for his potential criminal charges (implied).
</details>
<details>
<summary>
2023-06-14: Let's talk about Trump's arraignment.... (<a href="https://youtube.com/watch?v=o55f-1Ndrbk">watch</a> || <a href="/videos/2023/06/14/Lets_talk_about_Trump_s_arraignment">transcript &amp; editable summary</a>)

Former President Trump arraigned on serious charges, lack of restrictions could hinder accountability, and trial process shaped by motions.

</summary>

"Former President Donald J. Trump was arraigned in Miami on serious charges."
"Lack of restrictions could hinder accountability for Trump's actions."
"The trial process will involve numerous motions shaping its course."

### AI summary (High error rate! Edit errors on video page)

Former President Donald J. Trump was arraigned in Miami on serious charges.
Law enforcement assessments overestimated the number of supporters showing up.
Despite expectations after January 6th, there were fewer supporters present.
The Justice Department's actions suggest this is not a political matter.
The former president left the courthouse without restrictions, contrary to expectations.
The judge could have imposed several restrictions but chose not to.
Lack of restrictions could hinder accountability for Trump's actions.
There may be a need for restrictions in the future.
Trump's campaigning ability remained unaffected despite the severity of the charges.
The trial process will involve numerous motions shaping its course.

Actions:

for legal analysts, political commentators,
Monitor the legal proceedings and implications for accountability (implied)
Stay informed about the developments in the case (implied)
</details>
<details>
<summary>
2023-06-14: Let's talk about Montana and constitutional case.... (<a href="https://youtube.com/watch?v=z0f_jCK7NnM">watch</a> || <a href="/videos/2023/06/14/Lets_talk_about_Montana_and_constitutional_case">transcript &amp; editable summary</a>)

Kids in Montana sue the state for supporting dirty energy, questioning the government's role in protecting the environment and promoting general welfare.

</summary>

"You're going to have a government that has the authority that the one we have does, I think that protecting the environment should be pretty high upon that list."
"It doesn't really do a whole lot of good to have an 800 billion dollar a year defense budget if you're destroying the country yourself."

### AI summary (High error rate! Edit errors on video page)

Talking about a case in Montana called Held vs. Montana where 16 kids are suing the state for supporting dirty energy and contributing to climate change.
Montana's constitution guarantees the right to a clean and healthful environment since 1972, which the plaintiffs believe the state is not upholding.
The state's defense seems to be downplaying their contribution to climate change by arguing it's a global issue and their impact is minimal.
Beau compares this defense to denying other constitutional rights like free speech or due process based on global standards.
The case has entered trial, and Beau anticipates a lengthy legal process before a resolution is reached.
Despite potential appeals and a prolonged legal battle, Beau sees the case as a significant step towards holding the government accountable for environmental protection.
Beau believes that protecting the environment should be a top priority for the government, falling under promoting the general welfare.
He questions the value of a massive defense budget when the country's own actions are harming it.

Actions:

for environmental activists, advocates,
Support environmental organizations in Montana to amplify the message of protecting the environment (implied).
Join local climate action groups to advocate for sustainable practices in your community (implied).
</details>
<details>
<summary>
2023-06-14: Let's talk about Garth's new place in Nashville.... (<a href="https://youtube.com/watch?v=2bTd_79ZH1M">watch</a> || <a href="/videos/2023/06/14/Lets_talk_about_Garth_s_new_place_in_Nashville">transcript &amp; editable summary</a>)

Beau clarifies Garth Brooks isn't opening a gay bar but a place promoting good manners and respect, standing by his inclusive brand amidst potential backlash.

</summary>

"If you are let into our house, love one another. Again, love your neighbor."
"30 years this man has been telling you who he is. He is super woke."
"As society moves forward, the population of people that are accepting far outweigh those that are bigoted."
"He is one of the biggest brands in country music."
"I don't think he needs assistance from people who outrage farm on Twitter."

### AI summary (High error rate! Edit errors on video page)

Taking a break from national news to address a question about Garth Brooks' new place in Nashville.
Clarifying that Garth Brooks is not opening a gay bar but a place where good manners are expected.
Brooks wants the place to be safe and where people love and respect one another.
Despite being in Tennessee, there seems to be some resistance to this concept.
Brooks mentioned serving every brand of beer and promoting love and respect for one another.
He doesn't seem concerned about how this might affect his brand, as he has always been advocating for inclusivity.
Brooks has a history of being woke and advocating for equality, even before it became a popular term.
He believes that as society progresses, the majority accepting stance outweighs the bigoted views.
Brooks isn't fazed by potential backlash from those who do not share his values.
His actions are seen as good capitalism as he appeals to a growing demographic that values kindness and respect.

Actions:

for country music fans,
Support inclusive businesses like Garth Brooks' new place (exemplified)
Promote love and respect in your community (exemplified)
</details>
<details>
<summary>
2023-06-13: Let's talk about what to expect in Miami over Trump.... (<a href="https://youtube.com/watch?v=Ix-X_7cTdDI">watch</a> || <a href="/videos/2023/06/13/Lets_talk_about_what_to_expect_in_Miami_over_Trump">transcript &amp; editable summary</a>)

Law enforcement anticipates rallies in Miami escalating, urging people to avoid the area due to the volatile situation and risks involved.

</summary>

"The more militant the rallies, the more likely that they get the exact opposite result of what they want."
"Supporting these rallies poses a risk of them getting out of hand."

### AI summary (High error rate! Edit errors on video page)

Law enforcement sources do not have any specific credible threat in Miami, but they anticipate large rallies that may escalate.
People are advised to avoid the area in Miami due to the possibility of things getting out of hand.
Despite the lack of social media posts seen before other events, there is a risk of things escalating.
Emotions are running high among supporters who have faith in Trump and his rhetoric, viewing him as anti-establishment.
Some supporters may not be looking at objective reality, potentially leading to disruptive actions.
The more militant the rallies become, the more likely it is that the judge may view the situation as a societal risk and keep Trump in custody.
Mixed messages from supporters and warnings to stay civil indicate an understanding of the risks involved.
Supporting these rallies poses a risk of them spiraling out of control.
Law enforcement entities are prepared to respond if anything goes wrong in Miami.
Overall, it is advisable to avoid the area in Miami due to the uncertain and volatile nature of the situation.

Actions:

for miami residents and spectators,
Avoid the area in Miami (suggested)
Stay informed about the situation (implied)
</details>
<details>
<summary>
2023-06-13: Let's talk about Tucker, Twitter, and legal developments.... (<a href="https://youtube.com/watch?v=OK7xsBQ5eaU">watch</a> || <a href="/videos/2023/06/13/Lets_talk_about_Tucker_Twitter_and_legal_developments">transcript &amp; editable summary</a>)



</summary>

"I think they're going to end up being the biggest loser here because I don't actually think anybody involved, Tucker or Fox, wants Tucker on Twitter."
"Tucker has gotten a cease and desist order from Fox News."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson received a cease and desist order from Fox News for posting content on Twitter, violating their contract.
Fox News claims sole rights to Tucker's content until December 31st, 2024, and he needs to stop posting on Twitter since he's still under contract.
Tucker argues it's a free speech issue, portraying himself as a victim, while his supporters believe it's an attempt to silence him until after the election.
If Tucker continues with his planned episode today, it suggests he wants to go to court with Fox. If he doesn't air the content, he might be aiming to return to Fox.
Twitter's future may be impacted as Tucker's presence influences the platform's dynamics, potentially turning it into a right-wing echo chamber.

Actions:

for social media users,
Stay informed about the developments in the Tucker Carlson and Fox News issue (implied).
</details>
<details>
<summary>
2023-06-13: Let's talk about Trump, the House, and the Senate.... (<a href="https://youtube.com/watch?v=MsZ9ZmqKOMw">watch</a> || <a href="/videos/2023/06/13/Lets_talk_about_Trump_the_House_and_the_Senate">transcript &amp; editable summary</a>)

Exploring the differing reactions of House Republicans and Senators to news about Trump, revealing electoral concerns and self-preservation over moral principles.

</summary>

"Generally speaking, senators are more deliberate."
"Senate's electoral concerns may be why senators are less vocal in support of Trump."
"It's not necessarily about higher ethical standards but about self-preservation in elections."
"Loyalty to Trump is about electoral concerns rather than moral principles."
"McConnell's knowledge and experience may also be a factor in Senate Republicans' behavior."

### AI summary (High error rate! Edit errors on video page)

Explains the differing reactions of Republicans in the House and Senate to news about Trump.
Senators are generally more deliberate while House Republicans in heavily gerrymandered districts defend Trump.
Mitch McConnell's influence and information may play a role in senators' actions.
House Republicans are in safe, red districts, so they don't worry about voter turnout like senators do.
Senate's electoral concerns may be why senators are less vocal in support of Trump.
It's not necessarily about higher ethical standards but about self-preservation in elections.
Loyalty to Trump is about electoral concerns rather than moral principles.
McConnell's knowledge and experience may also be a factor in Senate Republicans' behavior.

Actions:

for political analysts, voters,
Analyze and understand the electoral dynamics in your district or state (implied)
</details>
<details>
<summary>
2023-06-13: Let's talk about Trump and the two-tiered justice system.... (<a href="https://youtube.com/watch?v=VYpsDUfGbgA">watch</a> || <a href="/videos/2023/06/13/Lets_talk_about_Trump_and_the_two-tiered_justice_system">transcript &amp; editable summary</a>)

Republicans admit Trump faces a different justice system, with special treatment favoring the wealthy and powerful, while critics claim he's targeted unfairly.

</summary>

"There is a two-tier justice system, but it is working in favor of the former president."
"They think that you are just totally incapable of acknowledging all of the favors, all of the courtesies that have been extended to the former president."

### AI summary (High error rate! Edit errors on video page)

Republicans claim Trump is being treated differently under the justice system, and they're right.
Trump is facing charges in a unique way compared to others, with special accommodations such as being told to come in on a specific day.
In contrast, regular people under the justice system face forceful arrests and immediate custody for similar charges.
Despite facing charges, Trump is expected to walk out after his arraignment due to his resources and status.
The justice system seems to favor the wealthy and powerful, providing them with leniency and special treatment.
Critics who claim Trump is being targeted more harshly are overlooking the special treatment he has received.
Trump could have avoided much of this scrutiny by simply returning the requested documents.
The two-tiered justice system benefits the former president, allowing him privileges that others wouldn't receive.

Actions:

for legal reform advocates,
Challenge disparities in the justice system (implied)
Advocate for fair treatment under the law (suggested)
</details>
<details>
<summary>
2023-06-12: Let's talk about a european cop asking about black Americans.... (<a href="https://youtube.com/watch?v=gGdqYh5F5LE">watch</a> || <a href="/videos/2023/06/12/Lets_talk_about_a_european_cop_asking_about_black_Americans">transcript &amp; editable summary</a>)

A cop in Europe seeks advice on handling black Americans, focusing on calming interactions and understanding the fear caused by U.S. law enforcement behavior.

</summary>

"It's cool, we're not American cops."
"American law enforcement behavior is so out of line with the rest of the world."
"Calling Danish police does not come with the chance of a death sentence."
"The behavior exhibited by a large portion of American law enforcement is so out of line."

### AI summary (High error rate! Edit errors on video page)

A cop in Europe reached out for advice on dealing with black Americans in a good way.
The cop in Denmark handled a situation involving a black American couple on vacation arguing over insulin left at home.
The European cop was bothered by the reaction of the black American man, throwing his hands up in compliance due to fear from U.S. law enforcement behavior.
Beau suggests starting interactions with "It's cool, we're not American cops."
European cops practice consent-based policing compared to the domination-based policing in the U.S.
The European cop aims to make people more at ease when interacting with law enforcement.
Suggestions are welcomed from black Americans on what can set them at ease in such situations.
American law enforcement behavior is seen as out of line compared to the rest of the world.
The European cop hopes that calling Danish police won't be associated with a death sentence for Americans.
Beau encourages sharing suggestions to improve interactions with law enforcement.

Actions:

for european law enforcement,
Share suggestions on how law enforcement can make interactions more comfortable (suggested).
Practice consent-based policing techniques to create safer interactions (exemplified).
</details>
<details>
<summary>
2023-06-12: Let's talk about Trump grief and what we can learn.... (<a href="https://youtube.com/watch?v=1wgi08czj9c">watch</a> || <a href="/videos/2023/06/12/Lets_talk_about_Trump_grief_and_what_we_can_learn">transcript &amp; editable summary</a>)

Exploring conservative reactions to Trump's indictment reveals a shift from personality to policy, advocating for a more informed and less emotionally driven approach to politics.

</summary>

"He didn't do anything wrong. Denial."
"It's the end of the republic, depression, eventually there's acceptance, so stages of grief."
"Don't elevate them. Focus on policy."
"Maybe you should base that on a little bit more than, well I like that he makes liberals mad, or he says what I want to hear."
"Focus on policy."

### AI summary (High error rate! Edit errors on video page)

Exploring reactions from conservative circles regarding Trump's indictment, focusing on emotions and the shift from personality to policy.
Themes include denial ("He didn't do anything wrong"), anger ("It's time for war"), bargaining ("But Hillary's emails"), depression ("It's the end of the republic"), and acceptance.
People invested in Trump as a personality are experiencing grief as they confront the metaphorical death of that image.
Beau advocates for focusing on policy over personality in politics to reduce polarization and move the country forward.
Encourages people to view politicians as flawed individuals rather than saviors, advocating for a more policy-centric approach to governance.

Actions:

for political observers,
Analyze policies of politicians (implied)
Shift focus from personality to policy when evaluating political figures (implied)
Advocate for informed and objective political discourse (implied)
</details>
<details>
<summary>
2023-06-12: Let's talk about Trump and republican spin.... (<a href="https://youtube.com/watch?v=AthFFXZoRac">watch</a> || <a href="/videos/2023/06/12/Lets_talk_about_Trump_and_republican_spin">transcript &amp; editable summary</a>)

Addressing misinformation on Trump's indictment, Beau clarifies this is a criminal, not political matter, with the former president likely facing charges.

</summary>

"It's about the jury box."
"This is a criminal matter in a federal courtroom."
"The trial is going to be covered, the information is going to come out, people are going to read the indictment."
"It's not good for accuracy."
"Let him."

### AI summary (High error rate! Edit errors on video page)

Addressing the misinformation being spread by certain right-wing commentators regarding Trump's indictment.
Acknowledging the push for wild and inaccurate information about Trump's situation.
Emphasizing that the situation is a criminal matter in a federal courtroom, not just a political one.
Pointing out that Trump's political strength has actually increased post-indictment, contrary to what some are claiming.
Asserting that the former president is likely to be convicted of some charges, with this being seen as the first indictment, not the last.
Noting that commentators are focusing on the political implications rather than the criminal nature of the case.
Stating that the trial's outcome will not be determined by political commentators' opinions.
Warning against reporting hopes rather than what is most likely to occur in reality.
Mentioning the potential damage to credibility for outlets and commentators spreading inaccurate information.
Stressing that the proceedings are not influenced by political commentators' narratives.
Reminding that this is a criminal proceeding with political implications, not a political event.
Concluding by underlining that the most likely outcome is the former president facing some charges.

Actions:

for viewers,
Read the indictment and follow the trial proceedings (implied)
Ensure accuracy in reporting and information sharing (implied)
</details>
<details>
<summary>
2023-06-12: Let's talk about New Haven and a settlement.... (<a href="https://youtube.com/watch?v=6XnL22QYX74">watch</a> || <a href="/videos/2023/06/12/Lets_talk_about_New_Haven_and_a_settlement">transcript &amp; editable summary</a>)

In June 2022, Randy Cox was paralyzed in a police van leading to a $45 million settlement, the largest police misconduct settlement in U.S. history, revealing systemic issues in law enforcement accountability.

</summary>

"A $45 million settlement, again, largest reported police misconduct settlement in U.S. history."
"Smaller settlements don't alter law enforcement behavior; they view it as a cost of doing business."
"The city's insurance only covers $30 million of the settlement."

### AI summary (High error rate! Edit errors on video page)

In June 2022, Randy Cox was paralyzed from the chest down after being thrown forward in a police transport van due to sudden braking.
The settlement for Randy Cox's case in New Haven, Connecticut is $45 million, the largest police misconduct settlement in U.S. history.
The city's insurance only covers $30 million of the settlement, leaving the city to pay the remaining $15 million out of pocket.
Smaller settlements have not effectively changed law enforcement behavior, leading to larger settlements to send a message.
The five officers involved in Randy Cox's case have been charged with reckless endangerment and cruelty to persons, but have not gone to trial yet.
Some officers have left the force, been fired, or retired following the incident.
Attorney Ben Crump, known for handling cases of police misconduct, represented Randy Cox in this case.
The magnitude of this settlement and its implications are likely to set a standard for future cases involving police misconduct.

Actions:

for legal advocates, activists,
Contact legal advocates for support (suggested)
Stay informed about cases of police misconduct (exemplified)
</details>
<details>
<summary>
2023-06-11: Let's talk about the Army's recruitment goals being missed.... (<a href="https://youtube.com/watch?v=ZphoE7NIufg">watch</a> || <a href="/videos/2023/06/11/Lets_talk_about_the_Army_s_recruitment_goals_being_missed">transcript &amp; editable summary</a>)

The declining U.S. Army recruitment numbers since 2018 are influenced by generational shifts and awareness, refuting claims of a "woke military."

</summary>

"Recruitment issues date back to 2018, marking the first time the Army missed its goals since 2005."
"The decline in recruitment numbers coincides with generational changes influenced by past military engagements."
"The idea that recent events or a 'woke military' are to blame for recruitment challenges is debunked."

### AI summary (High error rate! Edit errors on video page)

The U.S. Army missed its recruitment goal, leading to speculation about a "woke military."
Recruitment numbers have been declining since 2018, contrary to the perception of recent events causing the decline.
In 2018, the Army set a goal of recruiting 80,000 troops but fell short at 70,000.
The trend continued in subsequent years, with goals decreasing and recruitment numbers also dropping.
Factors contributing to the recruitment decline include generational shifts, awareness of military realities, and job availability.
Veterans' influence on younger generations and evolving perceptions of the military impact recruitment.
The idea that recent events or a "woke military" are to blame for recruitment challenges is debunked.
Recruitment issues date back to 2018, marking the first time the Army missed its goals since 2005.
The decline in recruitment numbers coincides with generational changes influenced by past military engagements.

Actions:

for military analysts, policymakers, citizens,
Inform young people about military realities (implied)
Advocate for progressive military reforms (implied)
Support veterans in sharing their experiences (implied)
</details>
<details>
<summary>
2023-06-11: Let's talk about Trump polling post indictment.... (<a href="https://youtube.com/watch?v=DgwjcGFbJF4">watch</a> || <a href="/videos/2023/06/11/Lets_talk_about_Trump_polling_post_indictment">transcript &amp; editable summary</a>)

48% believe Trump should have been charged, but undecided voters may sway as more information emerges, potentially leading to a significant shift in polls and Republican base opinions.

</summary>

"If 51% of the country believes you should have been charged with serious crimes that are likely to land you in prison, it is really unlikely that you end up in the White House."
"They will ride with that person until they don't."
"There's a whole lot more evidence of wrongdoing than their current news outlet may lead them to believe."
"I think that there's going to be a large change in the polls as more people get exposed to the actual information."
"Not today. Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explaining the current polling numbers regarding whether Trump should have been charged.
Pointing out the issue with polling and likely voters skewing in favor of Trump supporters.
Speculating on how undecided voters may lean towards believing Trump should have been charged as more information comes out.
Drawing comparisons to historical events like Watergate to analyze public opinion shifts.
Predicting a quick drop in approval ratings for Trump once more evidence is revealed and potential trial occurs.
Expressing confidence in a significant change in polls as more people are exposed to information, potentially impacting Republican base opinions.
Mentioning the timing of potential events and the uncertainty around Trump's delay tactics in a federal trial.
Emphasizing that as evidence unfolds, political motivations will be revealed and opinions may shift, especially among conservative-leaning independents.

Actions:

for voters, political analysts,
Stay informed on the unfolding events and evidence (implied)
Encourage others to read the indictment and seek out reliable information (implied)
</details>
<details>
<summary>
2023-06-11: Let's talk about SCOTUS, Alabama, and voting rights.... (<a href="https://youtube.com/watch?v=R0x4PB6nAY0">watch</a> || <a href="/videos/2023/06/11/Lets_talk_about_SCOTUS_Alabama_and_voting_rights">transcript &amp; editable summary</a>)

Alabama Supreme Court packed black vote into one district, diluting power, but surprising 5-4 decision favored voters, signaling hope for similar cases elsewhere and suggesting a shift in the court's interpretation of Voting Rights Act implications.

</summary>

"They packed the black vote into one district, diluting their voting power."
"The court sided with the voters."
"This is a win for the people in Alabama."

### AI summary (High error rate! Edit errors on video page)

Alabama Supreme Court decided to draw districts as they pleased, resulting in packing the black vote into one district and diluting their voting power.
Out of seven districts, only one was majority black when realistically there should be at least two.
The court ruled against the government in Alabama, surprising many with a 5-4 decision in favor of the voters.
Short term implications include the likely redrawing of maps in Alabama and a positive sign for similar cases in other states like Louisiana and Georgia.
This decision may have longer-term implications related to the Voting Rights Act and the Independent State Legislature Theory.
The ruling indicates the court's interest in the theory may not be as strong as previously believed.
The decision in favor of the voters contradicts the idea of an independent state legislature, reflecting an ideological shift.
This victory is perceived as a win for the people in Alabama.

Actions:

for voters, activists, legal advocates,
Redraw district maps to ensure fair representation (exemplified)
</details>
<details>
<summary>
2023-06-11: Let's talk about Pence, prevention, and sentences.... (<a href="https://youtube.com/watch?v=UZnLnzKKAJQ">watch</a> || <a href="/videos/2023/06/11/Lets_talk_about_Pence_prevention_and_sentences">transcript &amp; editable summary</a>)

Beau delves into the disconnect between advocating for harsh sentences and actual prevention, questioning the true motives behind punitive approaches in the criminal justice system.

</summary>

"It's always about prevention."
"It's about casting that tough guy image about punishment, not prevention, not deterrence, not rehabilitation."
"It's not about sending a message to stop."
"If you truly believe that harsh sentences are the way to send that message, then you should want a really harsh one for Trump."
"It's part of your in-group."

### AI summary (High error rate! Edit errors on video page)

Exploring the Republican perspective on prevention and deterrence through long prison sentences.
Analyzing the disconnect between advocating for harsh sentences and actual prevention.
Pointing out Vice President Pence's focus on punishment over prevention in crime approaches.
Questioning the effectiveness of harsh sentences as a deterrent, especially in cases of mass shooters.
Criticizing the lack of prevention in sentencing, particularly in cases like the Parkland shooter.
Arguing that sentencing disparities are more about punishment and othering than sending a message to stop.
Noting Trump's previous support for harsh penalties, contrasting with his current situation.

Actions:

for policy analysts,
Advocate for justice reform (suggested)
Challenge punitive approaches in criminal justice (implied)
</details>
<details>
<summary>
2023-06-10: Let's talk about too harsh a punishment for Trump.... (<a href="https://youtube.com/watch?v=X2egY7JKohY">watch</a> || <a href="/videos/2023/06/10/Lets_talk_about_too_harsh_a_punishment_for_Trump">transcript &amp; editable summary</a>)

Beau questions the fairness of long prison sentences, advocates for rehabilitation programs, and challenges ideological consistency around Trump's potential leniency.

</summary>

"I don't believe that long prison sentences are just."
"I think those [rehabilitation programs] would be far more effective and far more just."
"Develop some ideological consistency here."
"He is a rich old white guy and he will probably be extended every courtesy imaginable."
"Do you think that's just?"

### AI summary (High error rate! Edit errors on video page)

Addressing Trump and Republicans' perspective on his potential sentence.
Exploring whether the punishment fits the crime.
Clarifying misconceptions about Trump facing a 10-year sentence.
Expressing personal views on long prison sentences.
Advocating for alternative rehabilitation programs over extended prison terms.
Criticizing the U.S. system for excessive incarceration.
Challenging the belief that people cannot change.
Pointing out the contradiction in advocating for 10 years for damaging symbols versus damaging the nation.
Encouraging ideological consistency and introspection.
Acknowledging the likelihood of Trump receiving special treatment due to his status.
Posing a question on the fairness of potential leniency towards Trump.

Actions:

for republicans, criminal justice reform advocates,
Advocate for rehabilitation programs over long prison sentences (suggested)
Challenge inconsistencies in beliefs regarding sentencing (implied)
</details>
<details>
<summary>
2023-06-10: Let's talk about republicans deflecting from Trump.... (<a href="https://youtube.com/watch?v=ixbvVc85akU">watch</a> || <a href="/videos/2023/06/10/Lets_talk_about_republicans_deflecting_from_Trump">transcript &amp; editable summary</a>)

Republican Party doubling down on false claims, using deflection and lies to defend Trump amidst incriminating evidence leakage.

</summary>

"They're shaking their keys. Oh baby, here, here baby, look at this, look at this."
"The Republican Party appears to be doubling down with the president, with the former president, with Trump."
"Just consistently lie to your base and, well, they'll believe you."

### AI summary (High error rate! Edit errors on video page)

Republican Party doubling down on false claims about Biden indicting Trump, hoping to manipulate their base and independents.
Holding up unverified information as proof, hoping to mislead voters.
Republicans using deflection and claiming a witch hunt to defend Trump despite incriminating evidence leaking out.
Predicts Trump's trial will be widely covered and evidence will continuously pour out, making their lies unsustainable.
Republicans lying to their base about Biden securing Trump's nomination through indictment.
Implying that Republican politicians prioritize deception over truth and integrity.
Suggests that individuals close to Trump are likely cooperating with federal authorities.
Speculates that Republican tactics of consistent lying may not work this time with independent voters.
Expresses uncertainty about why Republicans continue to lie, possibly out of habit and past success in deceiving their base.

Actions:

for voters,
Challenge misinformation spread by politicians (implied)
Stay informed and fact-check claims made by political figures (implied)
Encourage others to critically analyze information presented by political parties (implied)
</details>
<details>
<summary>
2023-06-10: Let's talk about Paxton, Paul, and proceedings in Texas.... (<a href="https://youtube.com/watch?v=mRiroqQWaP8">watch</a> || <a href="/videos/2023/06/10/Lets_talk_about_Paxton_Paul_and_proceedings_in_Texas">transcript &amp; editable summary</a>)

Attorney General Paxton's impeachment, ties to Trump, and Nate Paul's arrest raise concerns about political corruption with potential election implications in Texas.

</summary>

"There's probably a whole bunch more that is going to come out."
"We can't lose sight of it just because something a little bit more historic is ongoing as well."

### AI summary (High error rate! Edit errors on video page)

Attorney General of Texas, Ken Paxton, facing impeachment proceedings.
Central figure in allegations related to the impeachment is Nate Paul, who was recently arrested by the feds.
Nate Paul facing charges of making false statements while seeking loans in 2017 and 2018.
Whistleblowers allege Paxton tried to use his office to benefit Paul.
Paxton under FBI investigation while impeachment proceedings move forward.
Republican party in Texas likely had prior knowledge of the situation.
More information expected to surface before the trial.
Links between Paxton and Trump's efforts to alter the 2020 election outcome.
Anticipated impact on Texas elections and Paxton's political future.
Reminder not to overlook significant events amidst other high-profile news.

Actions:

for texan voters,
Stay informed about developments in the impeachment proceedings and related investigations (implied).
Pay attention to the connections between local politicians and national events that may impact elections (implied).
</details>
<details>
<summary>
2023-06-10: Let's talk about FD-1023s and shows.... (<a href="https://youtube.com/watch?v=-Jw0TxLHS0A">watch</a> || <a href="/videos/2023/06/10/Lets_talk_about_FD-1023s_and_shows">transcript &amp; editable summary</a>)

Beau explains Republican manipulation using an unverified FBI form to push baseless allegations against Biden, calling for transparency to combat politicization, casting doubt on the claims' validity.

</summary>

"They know that the information is unvetted, unverified, but they're trying to trick their base."
"It's more like taking notes of a conversation."
"They're going to lie to their base."
"If there was any behavior that was wrong, it would have been done by the Trump administration in how it was handled."
"I find it incredibly unlikely that there is going to be any evidence to back up the claims on this form."

### AI summary (High error rate! Edit errors on video page)

Explains the Republican Party's use of an FD 1023 form from the FBI to push allegations against Biden.
Clarifies that the FD 1023 is for unverified reporting from a confidential source, not proof of truth.
Illustrates with a hypothetical scenario involving Marjorie Taylor Greene to explain the nature of the form.
Addresses the repetition of allegations regarding Ukraine and Hunter Biden.
Mentions Bill Barr's dismissal of the information as garbage.
Suggests that the FBI should release further information to counter politicization by the Republican Party.
Speculates on the unlikely validity of the claims on the FD 1023 form.
Concludes by questioning why the Trump administration did not take action if the allegations were true.

Actions:

for political observers,
Contact your representatives to demand transparency from the FBI in releasing further information (suggested).
Share this analysis to debunk baseless allegations and encourage critical thinking within your community (implied).
</details>
<details>
<summary>
2023-06-09: Let's talk about misconceptions about Trump in Florida.... (<a href="https://youtube.com/watch?v=vH72bk74dYQ">watch</a> || <a href="/videos/2023/06/09/Lets_talk_about_misconceptions_about_Trump_in_Florida">transcript &amp; editable summary</a>)

Trump's federal case in Florida debunks misconceptions while detailing potential charges and venues, with the federal system's impartiality prevailing.

</summary>

"Contrary to belief, the high conviction rates in the Southern District of Florida make it a suitable venue for the case."
"Despite assumptions that being in Florida gives Trump an edge, the federal system is complex and not swayed by location."

### AI summary (High error rate! Edit errors on video page)

Trump's case in Florida is still a federal case in the southern district of Florida, not a state case.
The misconception that Florida's governor could pardon Trump in this case is incorrect.
Contrary to belief, the high conviction rates in the Southern District of Florida make it a suitable venue for the case.
Prosecutors from DC are likely to handle the case in Florida to avoid venue-related delays.
Speculation about the charges against Trump is rampant, but the true details will be revealed on Tuesday.
Expect charges like willful retention and obstruction, with conspiracy likely involving multiple people.
Despite assumptions that being in Florida gives Trump an edge, the federal system is complex and not swayed by location.
Anticipate more criminal proceedings beyond the initial case in Florida, possibly including proceedings in DC.

Actions:

for legal observers,
Follow updates on the case to stay informed (implied).
</details>
<details>
<summary>
2023-06-09: Let's talk about Trump's reported indictment.... (<a href="https://youtube.com/watch?v=afroQYckvwk">watch</a> || <a href="/videos/2023/06/09/Lets_talk_about_Trump_s_reported_indictment">transcript &amp; editable summary</a>)

Former President Trump indicted in a federal case with anticipated charges including obstruction and retention of national defense information, marking the start of potential criminal cases against him amidst ongoing legal challenges.

</summary>

"A former president of the United States has been indicted federally."
"Please try to stay calm and just wait and see where it goes from here."

### AI summary (High error rate! Edit errors on video page)

Former President Donald J. Trump has been indicted in relation to a documents case in Florida.
The news was first shared by Trump himself on social media and later confirmed by major outlets.
Trump has been summoned to appear in Miami on Tuesday.
Specific charges are not yet known, but anticipated charges include retention of national defense information and obstruction.
Trump still faces an active case in New York, a potential case in Georgia, and an investigation into financial issues.
This indictment marks the beginning of potential criminal cases against the former president.
The impact of these legal issues on Trump's potential reelection bid remains to be seen.
There is a lot of speculation surrounding this development, urging caution until more information is available.
This event signifies a significant moment in history as a former U.S. president faces federal indictment.
People are likely having emotional reactions to this news, but Beau advises staying calm and observing how things unfold.

Actions:

for legal observers, political analysts,
Stay informed on the developments surrounding the indictment and related legal proceedings (suggested)
Monitor reputable news sources for updates on the case and any new information that emerges (suggested)
</details>
<details>
<summary>
2023-06-09: Let's talk about Judge Cannon and Trump.... (<a href="https://youtube.com/watch?v=fbk8ujLN0pk">watch</a> || <a href="/videos/2023/06/09/Lets_talk_about_Judge_Cannon_and_Trump">transcript &amp; editable summary</a>)

Judge Cannon handling case involving former president; concerns about bias impact proceedings, despite doubts, legal system progressing in holding Trump accountable.

</summary>

"The legal system is just that. It's a system."
"They were probably right about that. There was probably a lot of reservation about doing it."
"You're talking about a former president of the United States who has been federally indicted on some pretty serious charges."
"The fact that this has gotten this far indicates that it's probably going to go further."

### AI summary (High error rate! Edit errors on video page)

Judge Cannon, known for controversial rulings, is handling the case involving the former president.
People familiar with the process believe Judge Cannon may not retain control of the case.
There are concerns that Judge Cannon’s perceived bias could impact the proceedings.
Despite some expecting Trump to evade accountability, he is now a criminal defendant in the courtroom.
The legal system is viewed as a process, and perceived bias may cause delays and appeals.
Many individuals who doubted Trump's accountability now have to face the reality of a trial.
Trump, despite having the presumption of innocence, may face further indictments.
The decision to indict a former president indicates strong evidence and serious charges.
The American legal system, flawed as it may be, is progressing in holding Trump accountable.

Actions:

for legal observers,
Stay informed on the legal proceedings involving the former president (suggested)
Support accountability in the legal system by following the case developments (suggested)
</details>
<details>
<summary>
2023-06-09: Let's talk about Cuba, China, and the US.... (<a href="https://youtube.com/watch?v=YGl1LqAkI4o">watch</a> || <a href="/videos/2023/06/09/Lets_talk_about_Cuba_China_and_the_US">transcript &amp; editable summary</a>)

China and Cuba's intelligence facility aimed at the US sparks questions of aggression, but Beau argues it's information gathering, not an act of war.

</summary>

"Opposition nations having decent intelligence gathering capabilities is good actually."
"No, it's not grounds for the U.S. to do anything. It's not aggression. It's information gathering."
"A large country near a smaller country does not give the larger country say over the smaller country's internal affairs."

### AI summary (High error rate! Edit errors on video page)

China and Cuba have made an agreement for a potential listening post, prompting questions about aggression towards the US.
The facility being established is aimed at gathering signal or electronic intelligence, likely targeting the US.
Some argue that allowing China to spy on the US through Cuba is an act of war, constituting aggression.
Beau compares the situation to hypothetical scenarios involving US intelligence activities aimed at other countries.
He notes that intelligence gathering is a common practice among nations and doesn't necessarily equate to aggression.
Beau mentions that the US has permitted other countries, like China, to establish intelligence facilities within its borders, citing embassies as examples.
Having opposition nations with good intelligence capabilities can prevent misunderstandings and potentially avoid conflicts.
He stresses that Cuba, as a sovereign nation, has the right to make decisions regarding its land without it warranting military intervention from the US.
Beau argues against the idea of larger countries having influence over smaller nations' internal affairs, pointing out the need to eliminate spheres of influence globally.
He questions the justification of a US invasion of Cuba over perceived security risks, drawing parallels with the Russian invasion of Ukraine.

Actions:

for foreign policy analysts,
Use diplomatic channels to foster better relationships between nations (suggested)
Advocate for the elimination of spheres of influence in global affairs (implied)
</details>
<details>
<summary>
2023-06-08: Let's talk about fact-checking the Trump and Meadows news.... (<a href="https://youtube.com/watch?v=4gEvi2CJ07E">watch</a> || <a href="/videos/2023/06/08/Lets_talk_about_fact-checking_the_Trump_and_Meadows_news">transcript &amp; editable summary</a>)

Beau provides fact-checking on the Trump and Meadows news circulating on social media, urging everyone to calm down and let the legal process unfold.

</summary>

"Everybody just calm down."
"Just relax and let this take its course."
"If it happens, it's going to happen whether or not you're anxious about it or not."

### AI summary (High error rate! Edit errors on video page)

Providing fact-checking on breaking news about Trump and Meadows circulating on social media.
Meadows is rumored to have flipped and cooperating with an immunity deal, while Trump is speculated to be indicted under section 793 and facing 10 years in prison.
The information circulating is not entirely accurate and largely sourced from an article by The Independent.
Meadows' lawyer denies the plea deal but does not address the immunity aspect.
If Trump is indicted, it is likely to remain sealed for a while, and the reporting indicates the case may be in the end game with Meadows talking to federal authorities.
The article details may be accurate, but there is an immediacy placed on the information that may not be true, leading to potential false alarms.
Multiple grand juries are looking at the case, seeking an indictment, but there is no guarantee of immediate confirmation or coverage.
Beau advises everyone to calm down, let the legal process unfold, and not get anxious about the outcome.

Actions:

for interested observers,
Stay informed on accurate news sources (suggested)
Avoid spreading unverified information online (suggested)
</details>
<details>
<summary>
2023-06-08: Let's talk about Newsom's Constitutional amendment.... (<a href="https://youtube.com/watch?v=DBAiqZM8CSA">watch</a> || <a href="/videos/2023/06/08/Lets_talk_about_Newsom_s_Constitutional_amendment">transcript &amp; editable summary</a>)

Governor Newsom's proposed constitutional amendment on firearms faces hurdles due to including an assault weapons ban, hindering its potential progress towards becoming part of the U.S. Constitution.

</summary>

"It's supposed to be hard to change the US Constitution."
"Realistically, if this amendment had just the first three elements… that might have a chance."
"Newsome, he's not an unintelligent person. He knows this, so why is he doing it?"
"The odds of this going anywhere now are slim to none."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Governor Newsom from California proposed a constitutional amendment focusing on firearms regulations at the national level.
The proposed amendment includes raising the age to purchase a firearm to 21, universal background checks, creating a waiting period, and instituting an assault weapons ban.
To become part of the U.S. Constitution, the amendment needs to pass a two-thirds vote in the House and Senate and be ratified by three-fourths of the states.
The inclusion of an assault weapons ban is a significant barrier to the amendment's progress, especially at the national level.
States have the authority to implement assault weapons bans individually, and 10 states have already done so.
Despite the potential for a constitutional convention called by state legislatures, none of the amendments to the U.S. Constitution have taken that route.
Beau speculates that Governor Newsom's proposal may be a strategic move to boost his presidential candidacy in 2028 when demographics might be more favorable towards such regulations.
Newsom's inclusion of the assault weapons ban in the proposal may hinder its success, as it faces significant opposition.
Beau suggests that a version of the amendment without the assault weapons ban might have had a better chance of progressing through the legislative process.
Changing the U.S. Constitution is intentionally challenging, and the difficulty in amending it serves a purpose in maintaining stability and consistency in governance.

Actions:

for legislators, activists, voters,
Advocate for firearms regulations at the state level (implied)
Stay informed about proposed amendments and their potential impact on gun laws (implied)
</details>
<details>
<summary>
2023-06-08: Let's talk about McCarthy's dream and the GOP's future.... (<a href="https://youtube.com/watch?v=Jrd3N6uEMRs">watch</a> || <a href="/videos/2023/06/08/Lets_talk_about_McCarthy_s_dream_and_the_GOP_s_future">transcript &amp; editable summary</a>)

McCarthy's struggles with internal factionalism reveal the Republican party's lack of unity and leadership, jeopardizing their future success.

</summary>

"The Republican Party is becoming more factionalized and less cohesive."
"They're trying to show how radical they are in defense of ideas from the 50s."
"Get ready to watch McCarthy and other Republicans fail where they should win."
"They're not going to have it over internal feuds."
"Unless a actual leader emerges within the Republican Party, you'll see it for even longer than that."

### AI summary (High error rate! Edit errors on video page)

McCarthy believed passing the debt ceiling and a budget could secure wins for the Republican party, but internal factionalism is hindering progress.
The Republican party is becoming more factionalized and less cohesive, with different groups like the "never Trumpers" and "MAGA."
Republicans are struggling with ideological differences and a lack of unity, leading to internal conflicts and grudges.
The party is divided on issues like gas stoves, where some factions sabotaged McCarthy's bill as a form of payback.
There is a lack of leadership within the Republican party to unify the different factions and navigate these internal conflicts.
Beau predicts more infighting and struggles within the party unless a strong leader emerges to address these challenges.
The Republican party is facing similar issues that the Democratic party has struggled with, such as purity testing and lack of unity.
Beau believes that without a unifying figure, the internal feuds within the Republican party will continue to sabotage their efforts.
McCarthy and other Republicans may face challenges and failures within their party due to ongoing internal conflicts.
The future of the Republican party seems uncertain, with internal divisions posing significant obstacles to their progress.

Actions:

for political analysts, republican party members,
Support and advocate for leaders within the Republican party who can unify factions and address internal conflicts (implied).
</details>
<details>
<summary>
2023-06-08: Let's talk about CNN changing.... (<a href="https://youtube.com/watch?v=tSMEwmxu42g">watch</a> || <a href="/videos/2023/06/08/Lets_talk_about_CNN_changing">transcript &amp; editable summary</a>)

Leadership changes at CNN prompt speculation on capturing a right-wing audience and the risks of alienating their existing viewership.

</summary>

"CNN will be unable to capture that audience without losing the audience that it had for decades."
"Trying to capture the Trump base could result in losing their long-standing audience."
"It's worth remembering that the guy who had that U-Haul truck that went into the barriers up there in DC."
"He wanted to gain access to the White House and announce the end to American democracy."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Leadership changes at CNN, with Chris Lick, chair and CEO, out.
Departure due to plunging ratings, unhappy staff, lack of trust among viewers, and negative press.
Trunk Town Hall incident believed to have initiated the changes.
CNN has been a liberal mainstay but is trying to appeal to a right-wing audience.
New leadership may continue the shift to the right but in a more subtle manner.
Attempting to capture a polarized audience may lead to losing their original viewership.
CNN might struggle to appeal to an audience not accustomed to news but to biased reporting.
Trying to capture the Trump base could result in losing their long-standing audience.
Warning about giving a platform to extreme individuals like the U-Haul truck incident perpetrator.
Speculation on CNN's future direction and audience-capturing strategies.

Actions:

for media analysts, news consumers,
Analyze and understand media shifts and biases (implied)
Stay informed and critical of news sources (implied)
Advocate for diverse and balanced news coverage (implied)
</details>
<details>
<summary>
2023-06-07: Let's talk about Ukraine, pipelines, discord, and questions.... (<a href="https://youtube.com/watch?v=qfb4xgkvuMY">watch</a> || <a href="/videos/2023/06/07/Lets_talk_about_Ukraine_pipelines_discord_and_questions">transcript &amp; editable summary</a>)

Beau questions the accuracy and timing of leaked information on Ukrainians, pipelines, and Discord, urging skepticism in the shadowy world of intelligence.

</summary>

"I can't take a victory lap on something that I have doubts on."
"There are a lot of information operations going on, and if everything just lines up too neatly, question it."

### AI summary (High error rate! Edit errors on video page)

Beau addresses two videos he didn't make about Ukrainians, pipelines, and Discord leaks, prompted by messages he received.
He questions why he didn't follow up on particular information in those videos that turned out to be accurate.
The Discord leaks revealed information that fit perfectly into certain narratives but raised doubts about its accuracy.
He expresses skepticism about the leaks, suspecting that some information may have been altered or misleading.
Regarding the pipeline incident involving six Ukrainians, Beau questions the narrative of non-state actors acting without support.
Beau speculates that there may have been a rogue element within the Ukrainian government involved in the pipeline incident.
He acknowledges his theory but expresses distrust in the leaked information that confirms it.
Beau points out the convenient timing of the leaks coinciding with global debates on Ukraine and Russia's involvement in certain incidents.
He stresses the importance of questioning information in the intelligence world where failures are known, and successes remain hidden.
Beau remains cautious about drawing definitive conclusions despite the alignment of events and his theories.

Actions:

for media consumers,
Question information operations (implied)
Stay cautious of neatly lining narratives (implied)
</details>
<details>
<summary>
2023-06-07: Let's talk about Trump's polling elephant in the room.... (<a href="https://youtube.com/watch?v=K6t9H_vK7oY">watch</a> || <a href="/videos/2023/06/07/Lets_talk_about_Trump_s_polling_elephant_in_the_room">transcript &amp; editable summary</a>)

Beau delves into Trump's legal issues, polling among GOP voters, and the likelihood of Trump winning the Republican primary even if convicted, possibly setting the stage for a Democratic showdown.

</summary>

"46% of primary voters think he should be allowed to serve even if convicted."
"It's almost like he can't lose."
"Even if he is convicted, he wins the Republican primary."
"It's going to take something really out there to alter the course of the Republican primary at this point."
"You're probably going to see the Democratic Party go ahead and just start gearing up for a matchup against Trump."

### AI summary (High error rate! Edit errors on video page)

Exploring Trump's legal entanglements and polling among Republican primary voters.
23% of Americans believe a convicted person should still be able to serve, while 62% disagree.
Among likely GOP primary voters, 52% prefer Trump over another candidate.
Even if convicted, 46% of GOP primary voters think Trump should still be allowed to serve.
Despite potential convictions, Trump remains the clear winner in Republican primary polling.
Trump's support among GOP voters suggests he could win the primary even if convicted.
The situation may lead Democrats to prepare to face Trump in the general election.
Trump's strong support among primary voters poses challenges for other GOP candidates.

Actions:

for political observers, voters,
Prepare for potential matchups against Trump in upcoming elections (implied).
</details>
<details>
<summary>
2023-06-07: Let's talk about Trump juries and true believers.... (<a href="https://youtube.com/watch?v=3EDQANTh5-I">watch</a> || <a href="/videos/2023/06/07/Lets_talk_about_Trump_juries_and_true_believers">transcript &amp; editable summary</a>)

One person on the jury holds significant power in legal proceedings against Trump, with multiple cases ongoing and the necessity of waiting for indictments before predicting outcomes.

</summary>

"One person on the jury wields a whole lot of power."
"The indictments themselves are hard to get."
"At the end of this, it would take four true believers, minimum."
"You're looking for ways for things to go wrong. There's a bunch of them."
"Let's wait for the indictments before we start talking about what the jury may or may not do."

### AI summary (High error rate! Edit errors on video page)

Explains the power of one person on a jury in the legal system.
Mentions multiple ongoing cases against Trump in different states.
Indicates the difficulty in obtaining indictments.
Suggests that the jury selection process aims to eliminate bias.
Speculates on the possibility of multiple "true believers" needed to sway a jury.
Emphasizes the importance of waiting for actual indictments before predicting outcomes.

Actions:

for legal observers,
Wait for actual indictments before making assumptions (implied)
</details>
<details>
<summary>
2023-06-07: Let's talk about Meadows and the USSS testifying about Trump.... (<a href="https://youtube.com/watch?v=mUrLXJArY-0">watch</a> || <a href="/videos/2023/06/07/Lets_talk_about_Meadows_and_the_USSS_testifying_about_Trump">transcript &amp; editable summary</a>)

Key figures like Meadows and Secret Service agents cooperating with grand juries spell trouble for Trump's team, with emerging evidence potentially leading to significant revelations.

</summary>

"This is the information that would establish timelines and intent."
"The best evidence doesn't come from the names you know."
"If they were even remotely forthcoming, anything that Trump did is now on record because they knew."
"We're near the end of this."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Meadows and other key figures have reportedly spoken to grand juries, spelling bad news for Trump's team.
Meadows' attorney stated his commitment to truth in legal obligations, potentially hinting at cooperation with investigations.
Meadows' decision to cooperate may stem from his vested interest in protecting himself due to his position within the Trump administration.
Two dozen Secret Service agents have also reportedly spoken to the grand jury, providing critical evidence due to their proximity to high-profile individuals.
The best evidence may come from individuals like housekeepers, security personnel, and assistants who have detailed knowledge of events.
Secret Service agents' compliance and thoroughness in recording information could be detrimental to Trump's defense.
Testimonies have occurred in the past, with Secret Service testimonies happening over the last two months.
The emerging information indicates a potential conclusion to the ongoing investigations.

Actions:

for legal observers, political analysts,
Contact legal experts for insights on the legal implications of key figures cooperating with investigations (implied).
</details>
<details>
<summary>
2023-06-06: Let's talk about old men bumping their heads.... (<a href="https://youtube.com/watch?v=i-fBMEnj0NE">watch</a> || <a href="/videos/2023/06/06/Lets_talk_about_old_men_bumping_their_heads">transcript &amp; editable summary</a>)

Beau questions the focus on an old man's fall, suggesting it's not a significant national priority and might be used as a distraction tactic by right-wing outlets.

</summary>

"I don't think that McConnell needs to be removed. I mean, not for falling."
"It almost seems like this is just kind of something that right-wing outlets would be running with to fill some time and maybe distract from other things that are going on with their candidates."
"And if you didn't know the Senate minority leader was out for over a month because he fell and bumped his head, that probably says a whole lot about the news you consume if they didn't make a big deal out of that but they made a big deal about it with Biden."
"Anyway, it's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

People have been asking about an old man falling down, prompting a need to address the topic.
The old man fell, bumped his head, hurt his ribs, and suffered a concussion, leading to a few days in the hospital under observation and almost six weeks off work.
Despite not liking the man in question, Beau doesn't believe he should be removed from office for falling.
Beau questions the focus on this incident, implying it's not a significant national priority.
He suggests that right-wing outlets might be using this incident to distract from other issues.
Beau believes that unless an injury directly impacts someone's duties, it shouldn't be a matter of national concern.
He points out the disparity in news coverage between this incident and others, implying bias in reporting.
Beau concludes by encouraging everyone to have a good day.

Actions:

for news consumers,
Question biased news coverage (implied)
Be critical of distracting tactics in reporting (implied)
</details>
<details>
<summary>
2023-06-06: Let's talk about changes and explanations in a small town.... (<a href="https://youtube.com/watch?v=yrueEoLOxP8">watch</a> || <a href="/videos/2023/06/06/Lets_talk_about_changes_and_explanations_in_a_small_town">transcript &amp; editable summary</a>)

Beau addresses managing change in a small town, advocating for personal growth amidst resistance and the importance of surrounding oneself with supportive individuals.

</summary>

"Those who don't want you to change, those who wouldn't be accepting of that, they don't matter."
"You don't owe them anything, nothing you have to worry about, only you and those people who are accepting."
"Don't let anybody stand in that way."
"If you don't look back on the person you were five years ago and cringe, you wasted five years."
"People will change, they should change."

### AI summary (High error rate! Edit errors on video page)

Addressing change in a small town and managing obstacles that arise.
A message about being part of the LGBTQ+ community and distancing from friends.
Feeling terrified of confrontation in a right-wing rural town.
Questioning the right decisions and seeking advice on handling the situation.
Being accommodating and conflict avoidant, struggling to express discomfort.
Exploring whether it's right to distance oneself and how to handle explanations.
Emphasizing that those who are not accepting do not deserve an explanation.
People are not entitled to know the reasons behind positive changes in your life.
Small towns often resist change, making it challenging for individuals evolving.
Encouraging personal growth and being true to oneself without needing approval.
Advocating for surrounding oneself with supportive and accepting people.
Asserting that explanations are only owed to those who truly care and support.
Advising caution in revealing full explanations in small towns due to gossip.
Urging individuals not to let anyone hinder their personal growth and progress.

Actions:

for community members seeking guidance on navigating personal growth in challenging environments.,
Surround yourself with supportive and accepting individuals (implied).
</details>
<details>
<summary>
2023-06-06: Let's talk about Ukraine, water, and a breach.... (<a href="https://youtube.com/watch?v=r44E6B2wt8s">watch</a> || <a href="/videos/2023/06/06/Lets_talk_about_Ukraine_water_and_a_breach">transcript &amp; editable summary</a>)

A major dam breach in Ukraine leads to accusations between Ukraine and Russia, with concerns about motives and long-term consequences.

</summary>

"Nobody in the long term has anything to gain from this, not really."
"This is going to be a major issue for years and years and years to come."
"It doesn't need sensationalism. This is going to be pretty bad."

### AI summary (High error rate! Edit errors on video page)

Major dam breach in Ukraine causing evacuations downstream, potential for disaster.
Ukraine blames Russia, while Russia blames Ukraine for the breach.
Speculations on who has the most to gain from the situation.
Short-term gains but no long-term benefits for either Ukraine or Russia.
Analysts doubt Russia's involvement due to potential long-term consequences.
Possible motives for Russia: throwing Ukraine off balance, moving sympathizers out of the area, securing drinking water source.
Possible motives for Ukraine: throwing Russians off balance, cutting off drinking water supply to Crimea.
Uncertainty whether the breach was intentional or accidental.
Water from the breached dam helps cool a nearby nuclear plant.
Concerns about the ecological disaster and ongoing implications.

Actions:

for environmental advocates, international relations analysts.,
Support organizations aiding in evacuation efforts (suggested).
Stay informed about the situation and contribute to relief efforts (implied).
</details>
<details>
<summary>
2023-06-06: Let's talk about Oklahoma, religious schools, and what's next.... (<a href="https://youtube.com/watch?v=ZEojR26aWrc">watch</a> || <a href="/videos/2023/06/06/Lets_talk_about_Oklahoma_religious_schools_and_what_s_next">transcript &amp; editable summary</a>)

Approval of an online religious charter school in Oklahoma sparks legal concerns as state officials clash over its constitutionality, likely facing swift opposition.

</summary>

"This is probably going to get ground down in the state courts because a whole lot of states have state constitutions that closely align with the U.S. Constitution."
"This isn't going to fly, I wouldn't worry about it too much."

### AI summary (High error rate! Edit errors on video page)

The approval of an online religious charter school in Oklahoma by the Statewide Virtual Charter School Board.
Governor's support for the school based on religious liberty, contrasting with the Oklahoma Attorney General's view of it being a violation of the state Constitution.
Potential legal challenges at the state level due to state constitutions closely resembling the U.S. Constitution.
Predictions that the program will likely be short-lived and face opposition either at the state or federal level.
Criticism towards those claiming to defend the Constitution while being ignorant of its contents.

Actions:

for legal advocates, activists,
Challenge the approval of the online religious charter school through legal action (implied)
Stay informed about the developments regarding this issue and support efforts to oppose it (suggested)
</details>
<details>
<summary>
2023-06-05: Let's talk about what happened in the skies near DC.... (<a href="https://youtube.com/watch?v=CF5tA-gwzbM">watch</a> || <a href="/videos/2023/06/05/Lets_talk_about_what_happened_in_the_skies_near_DC">transcript &amp; editable summary</a>)

A civilian plane overshoots its destination, leading to a crash near DC and Virginia, sparking speculation and investigations.

</summary>

"There will undoubtedly be more information coming."
"It seems like an accident that occurred near airspace that prompted a response."

### AI summary (High error rate! Edit errors on video page)

A civilian plane overshoots its destination by 315 miles, prompting an alert in DC and Virginia.
NORAD intercepted the plane with F-16s authorized to travel at supersonic speeds, causing a sonic boom.
The F-16s deployed flares to get the pilot's attention, not shooting down the plane.
The plane eventually crashed in Virginia, reportedly carrying four people on board.
Speculation surrounded the incident, but agencies involved suggest no foul play.
The NTSB is investigating the crash, with possibilities of it being an accident or a medical emergency.
The incident seems to be accidental, awaiting further information from the investigation.
Agencies are beginning investigations, with more details expected to emerge.
The pilot's condition or circumstances leading to the crash are not confirmed yet.
The incident near sensitive airspace prompted a response, but no malicious intent is suspected.

Actions:

for aviation enthusiasts, safety regulators,
Support the ongoing investigation by the National Transportation Safety Board (implied).
</details>
<details>
<summary>
2023-06-05: Let's talk about the Pence charging decision.... (<a href="https://youtube.com/watch?v=Bv5dpCImxMI">watch</a> || <a href="/videos/2023/06/05/Lets_talk_about_the_Pence_charging_decision">transcript &amp; editable summary</a>)

Beau explains the differences between Pence and Trump's charging decisions by the Department of Justice, focusing on intent and cooperation, potentially impacting Pence's presidential run and shedding light on consistent standards. Trump may play the victim, unlikely to introspect.

</summary>

"I returned the papers."
"They had documents they weren't supposed to have."
"One person returned them, did everything they were supposed to, by the book."
"It seems unlikely that Trump is going to engage in any introspection."
"Anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the differences between Pence and Trump regarding charging decisions by the Department of Justice.
Mentions the key element in these types of crimes is intent, specifically willful retention.
Describes how Team Pence found documents and cooperated fully with the feds to return them.
Points out Pence's voluntary actions and his lack of intent to retain the documents illegally.
Speculates that Pence might use this example in his run for presidency to show contrast with Trump.
Suggests that Pence's actions demonstrate that these standards are consistent and not politically motivated.
Indicates that Trump may play the victim and cast Pence in a negative light, feeling singled out.
Concludes by mentioning the unlikelihood of Trump reflecting on his behavior leading to the different outcome.

Actions:

for political observers,
Follow updates on political developments (implied)
</details>
<details>
<summary>
2023-06-05: Let's talk about Trump, a DOJ meeting, and an all caps complaint.... (<a href="https://youtube.com/watch?v=8MzesGv86Y8">watch</a> || <a href="/videos/2023/06/05/Lets_talk_about_Trump_a_DOJ_meeting_and_an_all_caps_complaint">transcript &amp; editable summary</a>)

Former President Trump speculates on potential charges, comparing himself to uncharged predecessors, while his attorneys visit the Department of Justice, possibly hinting at impending bad news.

</summary>

"How can DOJ possibly charge me when no other presidents were charged?"
"Only Trump. Trump, the greatest witch hunt of all time."
"It certainly seems as though the former president got some bad news today."
"People need to hide the ketchup bottles type of thing."
"Whataboutism is not an effective defense in a courtroom."

### AI summary (High error rate! Edit errors on video page)

Special counsel's investigation into the documents case is winding down.
People believe a charging decision regarding the former president is imminent.
Trump's attorneys were observed entering and leaving the Department of Justice.
Speculation arises after Trump's fake tweet questioning potential charges against him.
Former president compares his potential charges to other presidents not being charged.
Trump claims he did nothing wrong and questions the integrity of the DOJ.
Trump's message suggests he may have received bad news regarding potential charges.
Information shared by Trump about other individuals is characterized as inaccurate spin.
Trump employs whataboutism in his defense, questioning why others were not charged.
Beau questions the effectiveness of whataboutism as a defense strategy in court.

Actions:

for political analysts,
Monitor developments in the special counsel's investigation (implied)
Stay informed about legal proceedings and potential outcomes (implied)
</details>
<details>
<summary>
2023-06-05: Let's talk about Arizona, water, and an acknowledgement.... (<a href="https://youtube.com/watch?v=tDYqSVU2TyA">watch</a> || <a href="/videos/2023/06/05/Lets_talk_about_Arizona_water_and_an_acknowledgement">transcript &amp; editable summary</a>)

Officials in Arizona acknowledge the finite water supply, halting new subdivisions in Phoenix and signaling a necessary step towards sustainable growth.

</summary>

"You can't have more developments without water."
"It's nice to see a moment where even if all of the details aren't perfect, there's an acknowledgement."
"Wait, we actually can't continue to grow."

### AI summary (High error rate! Edit errors on video page)

Officials in Arizona have announced a halt to new subdivisions in Phoenix due to a water shortage.
The current demand for water exceeds the groundwater supply by about 4%.
No new developments can move forward without demonstrating a secure water supply for the next hundred years.
The significant aspect is the acknowledgment by local and state officials that growth cannot be unlimited with finite resources.
Hope exists that a profit-driven motive will lead to solutions like desalination plants to address the water scarcity issue.
This move signifies a rare acceptance of reality by the state apparatus before the problem escalates.
Despite a 4% shortfall currently, there are strategies to mitigate this over time by looking ahead a hundred years.
While some may be upset, the restriction on new developments is a necessary step to ensure a sustainable future.
It's becoming increasingly evident that growth cannot happen without adequate resources, like water.
This decision stands out positively amid a lack of good environmental news and questionable attitudes of elected officials.
Even with imperfections, the acknowledgment that growth must be limited is a significant step forward.
Individuals considering buying land on the outskirts of Phoenix should reassess their plans due to the water shortage issue.
This decision prompts reflection on the necessity of balancing development with resource sustainability.
The action taken by Arizona officials sets a valuable example in addressing resource scarcity for future generations.

Actions:

for environmental advocates, arizona residents,
Reassess plans to buy land outside Phoenix due to the water shortage issue (implied).
</details>
<details>
<summary>
2023-06-04: Let's talk about the ruling in Tennessee.... (<a href="https://youtube.com/watch?v=ZoxDx40NNw4">watch</a> || <a href="/videos/2023/06/04/Lets_talk_about_the_ruling_in_Tennessee">transcript &amp; editable summary</a>)

A federal judge in Tennessee strikes down a ban on certain shows, revealing the unconstitutional nature of most bans targeting LGBTQ communities, but the fight against discrimination continues.

</summary>

"States must provide an incredibly compelling reason to ban such shows, tailored narrowly to that reason."
"The fight against discrimination is far from over."
"Blaming marginalized communities for institutional issues is a diversion tactic by those in power."

### AI summary (High error rate! Edit errors on video page)

A federal judge in Tennessee deemed a ban on certain shows unconstitutionally vague and substantially overbroad.
Most bans on these types of shows are boilerplate, with the same goal of being unconstitutional.
States must provide an incredibly compelling reason to ban such shows, tailored narrowly to that reason.
Over 500 bills targeting the LGBTQ community have been introduced, but most are likely to be struck down in court.
The government must show a significant reason to intervene in matters of speech, including LGBTQ expression.
Despite the ruling in Tennessee, the fight against discrimination is far from over.
The state's lack of compelling interest in these bans suggests they will continue to be struck down in court.
Efforts to scapegoat the LGBTQ community are likely to persist to divert attention from other issues.
Blaming marginalized communities for institutional issues is a diversion tactic by those in power.
The fight for equality continues, even after legal victories.

Actions:

for legal advocates, lgbtq activists,
Support organizations advocating for LGBTQ rights (suggested)
Stay informed and engaged in legal battles against discriminatory legislation (implied)
</details>
<details>
<summary>
2023-06-04: Let's talk about the government removing the stigma.... (<a href="https://youtube.com/watch?v=1vqpeV7i-cw">watch</a> || <a href="/videos/2023/06/04/Lets_talk_about_the_government_removing_the_stigma">transcript &amp; editable summary</a>)

Beau delves into the US government's renewed interest in UAPs and UFOs, aiming to destigmatize reports for potential defense purposes rather than a search for extraterrestrial life.

</summary>

"They want to remove the stigma when it comes to reporting this stuff."
"I think that the reason the government is starting to show more interest in this and especially now that they're trying and actually using the word trying to get rid of the stigma."
"They're worried about surveillance over flights. They're worried about drones."
"They want to use the massive amount of naval traffic and air traffic that the US has as an additional hedge against surveillance over flights."
"That's my guess. Or maybe they're just, you know, saying, hey, maybe there are aliens out there."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of UAPs and UFOs, pointing out the increasing seriousness with which the US government is treating the subject.
The government is interested in investigating these cases despite 98% of them being explainable.
The focus is on removing the stigma associated with reporting such sightings to encourage pilots to come forward with strange occurrences.
Beau suggests that the government's interest in these sightings may be more related to defense against foreign threats rather than extraterrestrial ones.
The goal is to have commercial, private, and military pilots report anything unusual to potentially counteract surveillance threats.
The government's efforts to destigmatize reporting could help in identifying and countering new technologies in the airspace and waters around the US.

Actions:

for defense analysts, pilots, enthusiasts,
Report any unusual sightings immediately to relevant authorities (suggested)
Encourage open communication among pilots and military personnel about strange occurrences in airspace (implied)
</details>
<details>
<summary>
2023-06-04: Let's talk about the Trump documents grand jury coming back.... (<a href="https://youtube.com/watch?v=wB-PHutVLWg">watch</a> || <a href="/videos/2023/06/04/Lets_talk_about_the_Trump_documents_grand_jury_coming_back">transcript &amp; editable summary</a>)

Beau explains the speculation surrounding possible indictments, advising against getting caught up in the excitement until any concrete developments occur.

</summary>

"It's speculation."
"I wouldn't get too caught up in any speculation."
"And I have a feeling we won't know until it does."
"Temper the speculation and temper any excitement until it happens."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the news about the grand jury meeting again to look into Trump documents case.
Acknowledges the speculation surrounding the possibility of indicting Trump this week.
Emphasizes the importance of sticking to what is known rather than engaging in speculation.
Points out the strength of the documents case based on publicly available information.
Believes that the prosecutor is likely pursuing an indictment against Trump.
Mentions the special counsel's office efforts to undermine Trump's potential defenses at trial.
Notes the slowed pace of the investigation with recent information focusing on past activities.
Suggests that although signs point towards an indictment, it may not necessarily happen this week.
Advises against getting caught up in constant speculation given the fatigue surrounding the case.
Indicates the complex nature of the case involving national defense information.
Considers the case straightforward unless there is a surprise move from Trump's legal team.
Encourages tempering speculation and excitement until any actual developments occur.

Actions:

for legal analysts, news followers,
Temper speculation and excitement until actual developments happen (suggested)
</details>
<details>
<summary>
2023-06-04: Let's talk about Ukraine, Kuwait, and supplies.... (<a href="https://youtube.com/watch?v=5LbmOIq0P9g">watch</a> || <a href="/videos/2023/06/04/Lets_talk_about_Ukraine_Kuwait_and_supplies">transcript &amp; editable summary</a>)

Issues with equipment readiness in pre-positioned stock may delay supplies to Ukraine, prompting global military maintenance scrutiny.

</summary>

"A 100% failure rate is just, I mean that is just out of, that should be out of the realm of possibility."
"Be ready for the world to fall in on you."
"It was just all bad."

### AI summary (High error rate! Edit errors on video page)

Outlines issues with supply and drawdowns in Ukraine, warning those in the military reserves and National Guard to pay attention.
Reveals that equipment being sent to Ukraine is not newly purchased but part of existing U.S. military material from pre-positioned stock worldwide.
Details concerning failures in maintenance: none of the M777 howitzers were mission ready, and only three out of 29 Humvees were.
Points out a contracting company responsible for maintenance and the army supply group failed in their duties.
Warns military personnel and maintenance providers worldwide to expect increased scrutiny after this failure.
Predicts delays in equipment reaching Ukraine due to necessary fixes, but most issues can be quickly resolved.
Anticipates thorough inspections of all pre-positioned stocks, especially those managed by contracting companies.

Actions:

for military personnel, maintenance providers,
Inspect and maintain equipment thoroughly to ensure mission readiness (implied)
Stay prepared for heightened accountability and scrutiny in maintenance practices (implied)
</details>
<details>
<summary>
2023-06-03: Let's talk about the lawyers going after Paxton.... (<a href="https://youtube.com/watch?v=y3zSvnDnPic">watch</a> || <a href="/videos/2023/06/03/Lets_talk_about_the_lawyers_going_after_Paxton">transcript &amp; editable summary</a>)

Beau warns of shocking revelations in the upcoming impeachment of Texas's attorney general, hinting at deeper misconduct and potential consequences for the Republican Party in 2024.

</summary>

"This is not about a one-time misuse of office. This is not about a two-time misuse of office. It's about a pattern of misconduct."
"They know something we don't."
"There is more to the story than we know."
"The delay is probably going to be something the Republican Party is going to regret."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talks about the upcoming impeachment of Texas's suspended attorney general, Ken Paxton.
Mentions the involvement of top attorneys, including Rusty Hardin, a Texas legal legend.
Indicates that the attorneys brought in are known for their winning track record.
Hardin points out that the impeachment isn't about isolated incidents but a pattern of misconduct.
Strongly suggests that there are shocking details yet to be revealed during the impeachment hearings.
Implies that there is more to the story beyond what has been publicly disclosed.
Speculates that the delay in the impeachment process may have consequences for the Republican Party leading up to 2024.

Actions:

for texans, legal enthusiasts,
Stay informed on the developments surrounding the impeachment proceedings (suggested).
Pay attention to the details that unfold during the hearings (implied).
</details>
<details>
<summary>
2023-06-03: Let's talk about Trump, wokeness, and branding.... (<a href="https://youtube.com/watch?v=1cIDXSlMYTI">watch</a> || <a href="/videos/2023/06/03/Lets_talk_about_Trump_wokeness_and_branding">transcript &amp; editable summary</a>)

Beau analyzes Trump's strategic attack on "woke," potentially changing political rhetoric and unmasking hidden sentiments.

</summary>

"Half the people can't define it. They don't know what it is."
"Woke is a dog whistle."
"If they get rid of the word woke, they may just start saying what they mean."

### AI summary (High error rate! Edit errors on video page)

Beau begins by discussing Trump, terminology, and recent political news with a South Park vibe.
Trump's criticism of the term "woke" is analyzed, noting his branding expertise.
Beau explains how Trump's attack on "woke" could damage his leading opponent's brand.
The term "woke" is likened to a dog whistle for marginalized communities.
Beau predicts Trump's continued efforts to distance the Republican Party from "woke" terminology.
The potential consequences of this shift in rhetoric are examined, including a more direct and aggressive political climate.

Actions:

for political observers,
Keep an eye on Trump's influence on Republican Party rhetoric (implied)
Monitor how the Republican base responds to the shift away from "woke" terminology (implied)
Stay informed about potential rebranding efforts by primary candidates (implied)
</details>
<details>
<summary>
2023-06-03: Let's talk about Chick Fil A going woke.... (<a href="https://youtube.com/watch?v=_wjuUO3tnsw">watch</a> || <a href="/videos/2023/06/03/Lets_talk_about_Chick_Fil_A_going_woke">transcript &amp; editable summary</a>)

Chick-fil-A's "wokeness" is a marketing strategy, not a political statement, revealing the manipulation behind outrage-driven narratives.

</summary>

"DEI isn't woke. It's capitalism."
"Woke is good, actually. Being alert to injustice is good."
"Chick-fil-A establishing a DEI office. Yeah, I mean, that's cool."
"They're playing you."
"Your commentators, they're playing you."

### AI summary (High error rate! Edit errors on video page)

Chick-fil-A is being called "woke" because of their Diversity, Equity, and Inclusion (DEI) section, which has been in place for years.
Right-wing commentators are riling up their audience by labeling companies as "woke" to generate clicks and views.
DEI is not a symbol of being "woke" but rather a part of capitalism to widen a company's market.
Beau personally chooses not to support Chick-fil-A due to past donations, but acknowledges the company's attempt to appeal to a broader audience through DEI.
The outrage over Chick-fil-A being labeled as "woke" is fueled by buzzwords and manipulated by commentators for profit.
Beau challenges the idea that having a DEI office goes against Christianity and urges viewers not to be misled by manufactured outrage.
The focus on Chick-fil-A's DEI office is a distraction from the reality of capitalism driving businesses to expand their consumer base.
Being woke and aware of social injustices should be seen as positive, and companies like Chick-fil-A implementing DEI initiatives should be appreciated.
Despite establishing a DEI office, Chick-fil-A is not likely to actively participate in events like sponsoring pride parades.
The narrative around Chick-fil-A's "wokeness" is a ploy to keep viewers engaged for ad revenue, ultimately benefiting the commentators.

Actions:

for conservative viewers,
Question manufactured outrage and look beyond buzzwords (implied)
Support companies implementing Diversity, Equity, and Inclusion initiatives (implied)
Challenge the profit-driven manipulation by commentators (implied)
</details>
<details>
<summary>
2023-06-03: Let's talk about Biden, student loan forgiveness, and what's next.... (<a href="https://youtube.com/watch?v=okrXz2La73Q">watch</a> || <a href="/videos/2023/06/03/Lets_talk_about_Biden_student_loan_forgiveness_and_what_s_next">transcript &amp; editable summary</a>)

Beau shares insights on the House passing a resolution to cancel student loan forgiveness, Senate's support, and Biden's expected veto, calling it a waste of time.

</summary>

"The House passed a resolution to cancel the student loan forgiveness program."
"Biden is likely to veto the resolution, as it is expected to reach the Supreme Court soon."
"It's just another waste of time."

### AI summary (High error rate! Edit errors on video page)

The House passed a resolution to cancel the student loan forgiveness program, which then went to the Senate where it passed with the support of key figures like Manchin, Tester, and Sinema.
Biden is likely to veto the resolution, as it is expected to reach the Supreme Court soon.
The move by Republicans to push this resolution seems questionable from a political standpoint, as Biden has little to lose by vetoing it.
The resolution appears to be a half-hearted attempt to challenge Biden's policies, possibly for future campaign ads.
There is anticipation around the Supreme Court's ruling on this issue, making the Senate's passing of the resolution seem almost moot.
Beau speculates that Biden will likely sign and veto the debt ceiling issue simultaneously, with no grand ceremony expected for the veto.
Overall, Beau views this resolution as another waste of time in the political arena.

Actions:

for voters, biden supporters,
Stay informed on the developments related to student loan forgiveness and share accurate information with others (implied).
Advocate for policies that support student loan forgiveness and affordable education (implied).
</details>
<details>
<summary>
2023-06-02: Let's talk about two wings of the same bird.... (<a href="https://youtube.com/watch?v=wWEBisftHio">watch</a> || <a href="/videos/2023/06/02/Lets_talk_about_two_wings_of_the_same_bird">transcript &amp; editable summary</a>)

Beau questions the similarities between Republicans and Democrats while advocating for understanding the nuances and impacts of political actions.

</summary>

"Two sides of the same coin, two wings of the same bird."
"I wish somebody could tell me I'm wrong, but I just don't see a difference."
"Ask any woman in the South, in a red state, who wants to engage in family planning."
"The society you want, the revolution you want, you're not going to vote that in."
"The idea that they're the same, that is coming from a position with a lot of privilege."

### AI summary (High error rate! Edit errors on video page)

Explains how the debt ceiling vote showcases Democrats voting to hinder good programs and making it harder for the poor, similar to Republicans.
Questions why Democrats give Republicans votes, suggesting they have similar policies.
Urges for a video that convinces him otherwise, feeling that the parties are indistinguishable.
Points out that the programs Democrats support were initiated by the Democratic Party.
Illustrates the back-and-forth nature of politics and the challenges of a representative democracy.
Emphasizes the severe consequences of a default if Democrats did not provide votes, leading to the non-existence of beneficial programs.
Acknowledges dissatisfaction with the political game rather than solely with the Democratic Party.
Challenges the notion of both parties being identical by citing various marginalized groups who face different treatment under Republicans.
Encourages building the desired society from the ground up rather than expecting immediate revolution through voting.
Argues against the narrative that both parties are the same, stressing the tangible harm reduction achieved through Democratic actions.

Actions:

for political enthusiasts,
Engage in community-building efforts to work towards desired societal changes (implied)
</details>
<details>
<summary>
2023-06-02: Let's talk about Utah, libraries, and finding out.... (<a href="https://youtube.com/watch?v=aqgJJfKRWjw">watch</a> || <a href="/videos/2023/06/02/Lets_talk_about_Utah_libraries_and_finding_out">transcript &amp; editable summary</a>)

Utah's Davis School District removes Bibles due to objectionable content, a consequence of legislation targeting marginalized groups.

</summary>

"For whatsoever a man soweth, that shall he also reap."
"You're not allowed to say other people can't read that book."
"Expect this to occur in pretty much every location a law like this was passed."

### AI summary (High error rate! Edit errors on video page)

Utah's Davis School District is removing Bibles from elementary and middle school libraries due to vulgarity and violence in the King James Version.
This action is a result of a 2022 law targeting marginalized groups and free speech.
The law aims to have parents flag objectionable content in books.
Beau criticizes the law for attempting to marginalize groups and appeal to bigoted sentiments.
The removal of Bibles from libraries is seen as a form of malicious compliance.
Parents likely already monitor their children's reading choices from the library.
Beau suggests that similar actions may occur in other locations with similar laws.
The situation is likened to reaping what you sow, even for those who don't read the Bible.
The underlying message is about the consequences of targeting specific groups through legislation.

Actions:

for educators, parents, activists,
Contact local school boards to advocate for diverse and inclusive library collections (implied)
Monitor and guide children's reading choices from school libraries (implied)
</details>
<details>
<summary>
2023-06-02: Let's talk about IRS confusion and a story.... (<a href="https://youtube.com/watch?v=AOgZhHCDQy8">watch</a> || <a href="/videos/2023/06/02/Lets_talk_about_IRS_confusion_and_a_story">transcript &amp; editable summary</a>)

Beau clarifies the confusion around IRS cuts in the debt ceiling bill, pointing out that different figures are due to varied interpretations and deals, not just the bill itself.

</summary>

"They're all correct, depending on how you figure it."
"That money's not gone until it's actually gone in my book."
"They don't want to give up this money."

### AI summary (High error rate! Edit errors on video page)

Explains the confusion surrounding the different numbers mentioned regarding IRS cuts in the debt ceiling bill.
Illustrates the concept of different interpretations leading to different numbers with a personal story about a security job.
Reads out the specific legislation from the bill, rescinding $1.3 billion of unobligated balances.
Mentions an additional $10 billion per year to be pulled back in fiscal years 2024 and 2025, part of a separate deal with Speaker McCarthy.
Emphasizes that the money doesn't disappear immediately and could be replaced through various means.
Points out that the $2 billion, $10 billion, and $20 billion figures correspond to different perspectives and deals, not just the bill.
Acknowledges underestimating the Biden negotiating team and expresses caution in assuming the money is gone.
Concludes by stating that the money isn't truly gone until it's gone and leaves with a reflective comment.

Actions:

for budget analysts, policymakers,
Monitor legislative developments closely to understand the implications (implied)
Stay informed about fiscal policies and their potential impacts (implied)
</details>
<details>
<summary>
2023-06-02: Let's talk about AOC vs Musk.... (<a href="https://youtube.com/watch?v=nnDKd9JvfUk">watch</a> || <a href="/videos/2023/06/02/Lets_talk_about_AOC_vs_Musk">transcript &amp; editable summary</a>)

Elon Musk's Twitter changes create confusion with fake verified accounts like AOC, leading to election misinformation and a decrease in Twitter's relevance for businesses.

</summary>

"Elon Musk changed the Twitter verification system to mean users paid him $8, not that the account was authentic."
"The upcoming election will be flooded with misinformation from fake verified accounts on Twitter."
"Businesses relying on social media may need to find alternatives to Twitter due to its decreasing relevance."

### AI summary (High error rate! Edit errors on video page)

Elon Musk changed the Twitter verification system to mean users paid him $8, not that the account was authentic.
There is a verified Twitter account impersonating AOC, causing confusion as the parody part is not always visible.
Musk interacted with the fake AOC account, causing further damage and confusion.
The upcoming election will be flooded with misinformation from fake verified accounts on Twitter.
Fidelity assessed Twitter's value at $15 billion, down from the $44 billion investors paid for it.
Businesses relying on social media may need to find alternatives to Twitter due to its decreasing relevance.
Twitter may not be a reliable source of news leading up to the election.

Actions:

for business owners, social media users.,
Find alternative social media platforms for business needs (suggested).
Be cautious of news coming from Twitter leading up to the election (suggested).
</details>
<details>
<summary>
2023-06-01: Let's talk about the debt deal and what's in it.... (<a href="https://youtube.com/watch?v=13u7rHlvzhI">watch</a> || <a href="/videos/2023/06/01/Lets_talk_about_the_debt_deal_and_what_s_in_it">transcript &amp; editable summary</a>)

Beau explains inaccuracies in the recent deal, refutes the Republican victory narrative, clarifies key points like student loans and work requirements, and criticizes far-right Republicans for undermining their party's success.

</summary>

"The Republican Party certainly doesn't feel like it was a Republican victory."
"Congress controls the budget, not the President."
"The reason the Republican Party really didn't get anything that it wanted was because of all of those saying it's a bad deal right now."
"Biden can put on his aviators and walk out of the room."
"The far-right MAGA Republicans destroyed any chance the Republican Party had of actually getting anywhere."

### AI summary (High error rate! Edit errors on video page)

Explains the inaccuracies and confusion surrounding the recent deal.
Mentions the number of votes McCarthy needed from Democrats to pass the Republican budget.
Democrats had more votes in favor of the bill than Republicans.
Refutes the narrative of Republican victory, mentioning the party's dissatisfaction.
Details what the Republicans wanted in the deal and what they actually got.
Clarifies the difference between student loan forgiveness and the student loan pause.
Addresses misconceptions about Biden's agenda and clarifies that major pieces of legislation remain intact.
Talks about work requirements for social safety nets and clarifies it's not a cut but an expansion.
Mentions the approval of the mountain valley project as a sweetener for senators.
Emphasizes that Congress controls the budget, not the President.
Criticizes the far-right Republicans for their extreme tactics that led to an unfavorable outcome for the Republican Party.
Predicts that the deal is likely to pass through the Senate with minor changes.

Actions:

for political enthusiasts,
Contact your senators to express your opinion on the deal and any amendments you'd like to see (suggested).
Stay informed about the developments in the Senate regarding the deal (implied).
</details>
<details>
<summary>
2023-06-01: Let's talk about pride vs Memorial Day.... (<a href="https://youtube.com/watch?v=eFH8gFMQiNw">watch</a> || <a href="/videos/2023/06/01/Lets_talk_about_pride_vs_Memorial_Day">transcript &amp; editable summary</a>)

Beau explains the somber significance of Memorial Day and contrasts it with joyous celebrations like Pride Month, clarifying the importance of solemn remembrance over wild festivities.

</summary>

"Pride is a happy thing. It's supposed to be a happy thing."
"It's not happily celebrated because it's not a happy day."
"Memorial Day is not about wild celebrations but about remembering and honoring the fallen."

### AI summary (High error rate! Edit errors on video page)

Explains the different ways certain events are celebrated and why some aren't advertised or celebrated as loudly as others.
Responds to a question about the difference in celebration between Military Appreciation Month and Pride Month.
Points out the minimal acknowledgment given by companies like Google for Memorial Day compared to the enthusiasm for Pride Month.
Clarifies that Memorial Day is a day of remembrance, not celebration, for those who died serving.
Notes that Armed Forces Day celebrates anyone in the military, Veterans Day honors those who served, and Memorial Day remembers those who died.
Mentions that while Pride is a joyous occasion with parades, Memorial Day is typically observed somberly.
States that Memorial Day is not about wild celebrations but about remembering and honoring the fallen.
Emphasizes that the understated recognition of Memorial Day is a sign of good taste, acknowledging the day without going overboard.
Suggests that people are free to celebrate Memorial Day joyously if they choose but should understand the day's true purpose.
Concludes by sharing thoughts on the somber nature of Memorial Day and wishing everyone a good day.

Actions:

for community members,
Attend a Memorial Day event to honor and pay respects to those who died serving (implied).
Educate others on the true meaning of Memorial Day and encourage solemn observance (suggested).
</details>
<details>
<summary>
2023-06-01: Let's talk about Trump, memos, and tapes.... (<a href="https://youtube.com/watch?v=MHrULso1GqA">watch</a> || <a href="/videos/2023/06/01/Lets_talk_about_Trump_memos_and_tapes">transcript &amp; editable summary</a>)

Trump's potential legal troubles escalate as evidence mounts, raising concerns for his defense in a push-play trial amid classified document allegations.

</summary>

"Here's the recording of Trump saying that he knew this document was classified and that he should have declassified it."
"Personally I think it's gonna be hard to argue that he didn't know about this based on a lot of the reporting that has come out."
"The more the information comes out, the more that seems likely."
"Generally speaking, the amount of information that comes into public tends to pick up speed just before an indictment."

### AI summary (High error rate! Edit errors on video page)

Trump allegedly had a meeting in July 2021 discussing a document related to a potential attack on Iran.
The document discussed in the meeting is still classified, and Trump acknowledges he should have declassified it before leaving.
Charges related to the document case focus on whether the information is national defense information, not just on its classification status.
Trump's legal team may face difficulties in a push-play trial due to multimedia evidence, including recordings and recovered documents.
If the specific document discussed in the recording was not returned initially, it could present a significant challenge for Trump's defense.
The increasing information coming out could potentially lead to an indictment.
Public information tends to accelerate before an indictment, and there is talk about what the prosecution possesses.

Actions:

for legal analysts, political commentators,
Stay informed on the developments of the documents case (implied).
Follow reputable sources for updates on legal proceedings (implied).
</details>
<details>
<summary>
2023-06-01: Let's talk about Paxton and turning Texas blue.... (<a href="https://youtube.com/watch?v=To-mcXmMCrs">watch</a> || <a href="/videos/2023/06/01/Lets_talk_about_Paxton_and_turning_Texas_blue">transcript &amp; editable summary</a>)

Paxton claims credit for Trump's win in Texas by stopping mail-in ballot applications, suggesting Texas may already be a blue state waiting for enthusiasm.

</summary>

"Red states don't really exist."
"Texas may already be a blue state."
"Just because a state has been red before doesn't mean that it's a red state."

### AI summary (High error rate! Edit errors on video page)

Paxton claims credit for Trump winning in Texas by stopping mail-in ballot applications.
Harris County had 2.5 million mail-in ballot applications, which Paxton deemed illegal.
The lawsuit to stop mail-in ballot applications is credited with Trump's win in Texas.
Democratic Party in Texas needs enthusiasm to win, already has the numbers.
Republican Attorney General of Texas suggests Trump won due to reduced enthusiasm and turnout in democratic areas.
Texans tired of current leadership can make a difference by showing up to vote.
Texas may already be a blue state, waiting for enthusiasm to show up.
Red states don't truly exist; it's about enthusiasm, not inherent political color.

Actions:

for texans,
Show up to vote in Texas elections (exemplified)
Increase enthusiasm and turnout in democratic areas (exemplified)
</details>
