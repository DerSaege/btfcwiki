---
title: Let's talk about Russia cooling off and stranger things to come....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=szPcP4l2uZ8) |
| Published | 2023/06/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A deal has been struck in Russia, prompting questions about its details.
- Two types of deals exist: a public-facing one and a private one.
- Private contractors who don't sign with the Ministry of Defense may be redeployed to pursue Russian interests in Africa.
- The firm involved previously functioned as Russia's special operations command.
- Putin made the deal because he couldn't stop the private side's initiative.
- The deal demonstrates Putin's weakness and encourages challenges to his authority.
- Putin may have to visit Belarus, and there are rumors of leadership changes in the Ministry of Defense.
- The situation in Russia is expected to continue with turmoil in the upper echelons of society.
- Russian troops in Ukraine are urged not to risk their lives for Putin's ego.
- Beau will release a video explaining the situation further.

### Quotes

- "Don't die for that, man."
- "People have been saying the whole thing seems really strange."
- "He is no longer somebody that is seen as invincible."
- "If you're going to take a shot, don't miss and don't stop."
- "It's the beginning."

### Oneliner

A deal struck in Russia reveals Putin's weakness and encourages challenges to his authority, urging Russian troops not to risk their lives for his ego.

### Audience

World observers

### On-the-ground actions from transcript

- Watch Beau's upcoming video for further insights (suggested)
- Stay informed about the situation in Russia and its implications (implied)

### Whats missing in summary

Insights on the significance of understanding the evolving situation in Russia for global stability

### Tags

#Russia #Putin #PrivateContracts #Military #Geopolitics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna provide a little bit of an update
on what's been going on in Russia.
Because apparently a deal has been struck.
The contracting side, the private side,
is it looks like they're standing down, okay?
That has prompted a bunch of questions
about what the deal is.
You have two deals in a situation like this.
One is the public-facing one. That's the one that we'll get to know about eventually.
Looks like the rumor mill says
those private guys that want to sign on with the Ministry of Defense can take
contracts with them. Those who don't
will be redeployed
and sent to
pursue Russian national interests in other places
like Africa.
So they will become
Russia's soft imperial force.
It's important to remember that's actually their job.
That's what that firm is for.
Prior to Ukraine,
that firm was used as kind of like
their special operations command.
They weren't really ever supposed to be infantry,
which is how they were being used there.
It looks like the boss may have to go to Belarus and chill there.
Not real sure about that.
There are rumors about some changes in the Ministry of Defense's leadership.
But again, all of this is rumor and realistically, none of this really matters.
This is the public facing deal.
There was also a private deal struck.
What that is, don't know.
We will probably never know.
The real question is why did Putin make the deal?
The answer is simple.
He couldn't stop it.
He could not stop it.
They cut a deal because realistically the private side had the initiative.
The Russian National Guard, the Russian military, the traditional, conventional, state-sponsored
side was unable to mount any real defense.
They weren't able to put armor on the road.
They weren't able to stop anything.
So Putin cut a deal.
It's really that simple.
It may appear from the outside that the private side lost.
That's not what happened.
Be super clear about this.
They won, and while it may seem like Putin is getting his way, that's not what occurred.
People have been saying the whole thing seems really strange.
Get ready for stranger things, because what has really occurred here is that Putin has
been demonstrated as weak.
The Wagner boss did a direct public armed challenge and got a deal that will encourage
other people to do the same thing.
It doesn't matter if Putin serves him some tea later or he finds a window or whatever.
All that will teach others is that if you're going to take a shot, don't miss and don't
stop.
Don't take the deal.
I would expect continued turmoil when it comes to the upper echelons of Russian society.
I would expect that to continue.
It will probably go in cycles until it just spills out of control.
Putin is no longer the illusion.
He is no longer somebody that is seen as invincible.
caterer used force and got a deal. If you want to portray yourself as some
authoritarian strongman, let me tell you what's really bad to do. Cut a deal with
somebody who challenges you. And that is what just happened. Okay, now a couple of
things real quick. Later tonight there will be a video I recorded last night
about why it happened and why the boss had the support of the contractors. I
think that's an important video to watch. It just keeps getting bumped back. The
other thing is I got a whole bunch of questions about the can of Rippet that
was sitting on the shelves back there. That wasn't an Easter egg. I spent all
night recording and I drank a Rippet and set it down back there. It wasn't a
Yeah, that was just a can that I forgot was back there.
No hidden meaning in that one.
For the time being, it looks like this situation is starting to slow down.
But understand, it's the beginning.
It is the beginning.
Russian troops in Ukraine, they really need to understand that the person who sent them there
and put them in the situation they're in for his ego, when faced with what they face every day,
cut a deal and caved like a house of cards. Don't die for that, man. Anyway, it's just a thought.
You have a good day.
Go to www.beadaholique.com to purchase beading supplies and to get design ideas! Thanks for watching!
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}