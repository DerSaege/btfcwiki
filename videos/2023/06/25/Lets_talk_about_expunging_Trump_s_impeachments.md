---
title: Let's talk about expunging Trump's impeachments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=n65JqQ0jWJY) |
| Published | 2023/06/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Addressing the possible expungement of former President Trump's impeachments.
- McCarthy's support for the idea of expunging the impeachments.
- Legal scholars doubting the feasibility of expunging impeachments since it's not a criminal process.
- Mentioning the precedent of Andrew Jackson having his Senate censure reversed in 1837.
- Emphasizing that even if the impeachments are expunged, they will still remain in the historical record.
- Describing the expungement as a symbolic gesture for the base, adding an asterisk to Trump's record.
- Pointing out that Trump's focus may be more on his legal troubles rather than the impeachments.
- Criticizing the House GOP for using such actions as a show for social media engagement without real impact.
- Stating that the expungement won't change public perception of Trump but will tie House GOP members to him historically.
- Concluding that the expungement is merely a performative act with no substantial effect.

### Quotes
- "It's a show with nothing more."
- "It doesn't actually go away."
- "They might go on to do something, but they will forever be tied to Trump."

### Oneliner
Beau addresses the symbolic expungement of Trump's impeachments, outlining its historical impact and criticizing it as a performative act with no real consequence.

### Audience
Politically engaged individuals

### On-the-ground actions from transcript
- Educate on the implications of performative political acts (implied)

### Whats missing in summary
The detailed nuances of historical precedents and legal scholars' perspectives on expunging impeachments.

### Tags
#Trump #Impeachments #HouseGOP #HistoricalRecord #PoliticalSymbolism


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the possible expungement
of former President Trump's impeachments.
Because that's apparently a thing that's being considered.
It looks like McCarthy is kind of on board with the idea.
Generally, what it means is that the House
is going to vote to, quote, expunge the former president's impeachments.
There are a lot of legal scholars saying that this can't be done.
You know, it's not a criminal thing.
It doesn't work the same way.
This isn't something that's really, really possible.
I would like to point out that in 1837, Andrew Jackson had his censure by the Senate reversed.
So it's not like there's not precedent for it in some ways, although those scholars will
argue that that was just done by the Senate, it wasn't a two chamber process.
So there's a little bit of a difference there, and that will certainly be argued, but the
key thing that I want to point out about the Andrew Jackson situation is that I'm telling
you about it.
It happened in 1837, and I'm telling you about it.
What does that mean?
It means that it doesn't actually go away.
If the House was to, quote, expunge the former president's impeachments, it would still
be part of the historical record.
It's not something that's going to disappear, regardless of what they do.
This is, once again, a show for their base, to show them that they're doing something
something is apparently basically adding a line in a history book later. It will
say, you know, the former President Trump was impeached twice. Later a different
session of the House decided to expunge those impeachments, but it's still there.
It doesn't go away, it will just have an extra asterisk beside it.
At this point, the least of Trump's worries are his previous impeachments.
Yes he is a twice-impeached president.
He's also a twice-indicted president.
That's probably more pressing to him.
This isn't about Trump, it's about them playing their base and just assuming that their base
thinks that this will actually do something.
It's the standard thing they're getting from the House GOP right now.
Show legislation, nothing to help them at all.
They're not doing anything to help their base, they're not doing anything to govern, they're
doing stuff to get social media clicks and keep the base energized and angry, and that's it.
Can they do it? Sure. Why not? Does it matter? No, of course not. It's not going to alter
his perception. All it will do is, historically, tie their names to him. So maybe later some of
these people in the House GOP, they might go on to do something, but they will, at
that point, they will forever be tied to Trump, who is the twice impeached, twice
indicted, possibly twice expunged former president. It's a show with nothing more.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}