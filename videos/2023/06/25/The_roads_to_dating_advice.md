---
title: The roads to dating advice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Xh2uXdL5Os8) |
| Published | 2023/06/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hosting a Q&A session on dating while on the roads.
- Sharing the worst habits between him and his wife - leaving coffee sips and over-explaining.
- Addressing age differences in relationships and advising communication.
- Giving advice on introducing a partner to a child after two months of dating.
- Explaining the importance of core values over shared interests in relationships.
- Sharing a humorous take on how to spot military personnel in a crowd.
- Suggesting ways to gauge a man's views on women's rights through observations.
- Sharing a dark humor joke related to EOD patches.
- Dismissing the concept of women hitting a "wall" as unrealistic.
- Addressing misconceptions about dating an ex-dancer.
- Encouraging self-acceptance by noting that everyone has attractive qualities.
- Providing insight on how to maintain a spark in a long-term relationship.
- Offering advice on disclosing past relationships during the dating phase.
- Exploring the root of why men may value purity in relationships.
- Addressing privacy boundaries in relationships regarding phone access.

### Quotes

- "It's about core values."
- "Everything's attractive to men."
- "Everybody needs privacy, everybody needs their boundaries."

### Oneliner

Beau shares dating insights, from addressing age gaps to discussing core values and maintaining privacy boundaries in relationships.

### Audience

Dating individuals

### On-the-ground actions from transcript

- Have an open and honest communication with your partner about habits that bother you (implied).
- Communicate openly with your partner about age differences and concerns (implied).
- Set boundaries and communicate your feelings about introducing partners to your children (exemplified).
- Observe how potential partners treat others to gauge their beliefs on gender rights (exemplified).
- Respect privacy boundaries in relationships, and communicate openly about them (exemplified).

### Whats missing in summary

Insights on maintaining a healthy relationship dynamic by prioritizing core values, communication, and respect for boundaries.

### Tags

#Dating #Relationships #Communication #Values #Boundaries


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to do another Q&A here on the roads with Beau
This is all
questions about dating
I'm very curious to see what y'all have to ask, to be honest
Okay, so we're gonna jump right in. Same thing as before
Haven't seen the questions, and I'll give you the best answer I can give you
Okay, with all the drama between internet couples, what's the worst thing your wife
does and what's the worst thing you do?
Yeah, our drama isn't any fun.
The worst thing I do is I'll drink coffee and leave like three sips in the bottom of
it and mean to come back and get it later and not.
And that's not good.
The worst thing my wife does?
She is very...
She explains things.
I don't know how to say that.
Ask her what time it is and she will tell me how to build a clock.
Those are our issues.
We don't have drama, really.
way too mellow for that. Okay, how do you feel about large age differences?
Obviously I'm talking about among adults, but I'm 24 and met this guy who is 34.
We had hung out for almost a week straight because we really clicked. Then
he found out I was 24 and got kind of weird. He didn't ghost me, but now he's
doing this whole older man protective bit. I really liked this guy until he got weird
just because all of my friends asked. We hadn't done the thing that normally makes men get
weird. Is this something he'll get over, or should I just end this now?
Okay, so you've talked to your friends and you have talked to an internet person.
Have you talked to him and told him?
That would be my first step.
I know that's one of the steps that people don't want to take often.
I mean, if you really like this person, you don't mind the 10-year gap, then I would bring
it up to him and be like, hey, you realize you're being like super condescending and
just go with it from there.
Can you get over it?
Maybe.
Maybe not.
going to have to be something you actually address.
I just started dating this guy and he wants to meet my son.
He's brought it up a couple of times.
I don't want to introduce him to my son until I know he's the guy who's going to be around.
He took this as an insult and said I was planning for us to break up.
We've been dating two months.
actually not a question in that but I mean the implied question is do I think
you're right yeah absolutely you're absolutely correct yeah if you do not
know that this is the person that that's going to you know be around and you
don't want your son to meet people who aren't going to be around that is
totally your call. Nobody gets to influence that. I'm going to be honest. I would question
a man who doesn't understand this logic because this is like cold calculating logic. There's
absolutely nothing insulting about this these are the these are the rules for this like that you
have said they make complete sense i yeah you're you're definitely in the right if if that's the
question. And yeah, I don't know... a man not understanding that is... maybe not a
red flag, but definitely an orange one. Especially after two months. Yeah, you're
You're definitely in the right.
Do opposites attract?
So when it comes to interests, that's normally what people mean when they say that.
They're talking about interests.
Do people of different interests attract?
And the answer is sometimes.
It's not about interests.
It's about core values.
If your core values are the same, your interests can be totally different and everything will
be fine, but you have to find the person that is your level of weird.
Whatever your core values are, they have to be very close to that, otherwise there's always
going to be conflict.
I am not a horse person, I like the horses, I like to play with the horses.
I do not view taking care of the horses as fun, my wife does.
And there are a lot of things like that where we have very different interests, but the
core values are the same.
reason we have, you know, so many horses out there is because it's turned into, you
know, a home for broken horses. And on that note, for those who, like, follow the
horses, Charlotte has found a forever home. The first horse that has ever
actually left my wife's foster system. So that's an exciting development. I do have
I have to admit I'm a little sad about that though.
Okay.
I'm seeing this guy who has a running joke with me and a bet with his friends where he
picks people out of the crowd who are military.
There is nothing, there's nothing you can see that would indicate they're military.
How does he do it?
They take their hat off when they walk indoors.
They're carrying everything in their left hand.
The watch is from the PX.
And they're a 22 year old with an $800 a month car payment.
Just guessing.
It's probably the carrying everything in their left hand if they're right handed though.
Okay.
Dating question from a female friend.
Alternate questions to ask a guy on your first few dates to suss out if he believes I should
have rights or not without him feeling like he's telling on himself or that I'm trying
to trick him into giving me proper information.
It's not questions, observations.
Watch how he treats waitresses, bartenders, see how he treats people that are perceived
be of a different social station. You know, if animals, you know, no-collar service workers,
the homeless people, people at a, in a position that is like viewed as a service job,
See how he treats them and go from there. Generally speaking, those people
who want, who are not happy about women having rights, they are also very, they
believe in social classes and they're pretty rigid with it. So I would watch
that.
In one of your videos you mentioned what an EOD patch meant.
My husband was EOD and he and his friends all wore little Lego patches and when I asked
what he meant he said it was dark humor and that I didn't want to know.
I lost him last year and now I'm wondering if you know.
But I sent her a message.
He was lost in a car accident, which is important information to know.
The little LEGO people, they come apart in pieces.
This is not something I would have told you if he had been lost at work.
But that's the joke.
I understand like where the joke came from. I think somebody, I think it started in Iraq,
somebody had lost their hand and they took the hand off of a Lego guy and put in his room.
I think that's where it started. What age do you believe women hit the wall?
I don't believe the wall is a thing.
Yeah, I don't believe that's not a thing.
That's internet.
That's not...
Man.
The idea that there is a certain age that women start to become less attractive or it's
It's all downhill from here or something like that.
Don't take dating advice from anybody who told you that was real.
My friend is dating an ex-dancer.
She seems nice, but I just don't see him with somebody who would do that.
She just stopped last year and now she acts like it never happened.
I feel like he should be more concerned.
Concerned about what exactly?
And she acts like it never happened, like she doesn't go...
I don't know what you're expecting here.
That's a job.
It's a job like any other job.
If you quit working at Target because you didn't like it, would you talk about it?
Or if you just decided to move on from there, maybe you did like it.
But is that going to be a common topic of conversation?
It's just a job.
And if he's not concerned about it, I don't see why you should be.
Are stretch marks attractive to men?
Everything's attractive to men.
Okay, so just to go ahead and preempt any other similar questions like this, whatever
it is that you think is wrong with you, that you think is not attractive.
That is somebody's thing.
Somebody out there really likes whatever it is that you don't like about yourself.
Yeah, that's because of the beauty standards they get imposed, and this goes for men too.
It's very standardized.
A lot of people want to try to fit into that standard beauty type.
The reality is there are a whole lot of people out there, I would suggest most people out
there are attracted to things that aren't necessarily that standard.
The deviation is what makes it interesting.
If everybody looked that standard beauty model, that standard type, then that isn't what would
be attractive.
Okay, I'm the guy from your video, Let's Talk About 8 Months of Change.
After that long video about the guy with the same problem, lots of people said they wished
they had an update.
So this is the guy who was working with the trans woman and they hung out and he thought
she was very sweet and he didn't understand why it was so easy to relate to her.
It was incredibly obvious that he was attracted to her.
Okay, so here's the update.
We've been dating two months, she's went home with me and met my mom, and they really
got along well.
My dad pulled me aside and told me he thought it was weird, but that I was always weird
anyway, and that all he cares about is that I'm happy.
My brother was a total not nice person, but my mom told him to treat her with respect,
and he did.
My girlfriend was really happy about my mom insisting we sleep in different rooms.
I didn't understand it at first.
Just think about it.
You'll get it.
Got it.
Mom doesn't want you sleeping in the same room because she's a girl.
Got it.
views her as a woman. Got it. Yeah. I imagine that would make her very happy. That's cool.
So there's the update for everybody who wanted to know that. And a whole lot of people did
ask even I noticed that. Okay. So much in the world going on. It's hard to focus. How
do you keep your, how do you and your family keep yourselves happy? I mean, after 47 years
of marriage, we have our little routines that make us laugh, smile, and feel good about
our life together, how do you and your wife keep that spark alive when you've
both spent so much of your time learning, reacting, and commenting on the bad parts?
I'm trying to figure out how to say this delicately. We have a whole lot of kids.
kids. We have a whole lot of kids. The spark aspect, that has come very natural to us.
As far as routines that we do, I think one of the most important things is that right
right after we eat dinner, we'll go outside, sit on the back porch together and have like
a moment without the kids and just it allows us to kind of recenter, talk about our days,
talk about anything that might be like pressing later, distracting later, something that might
come up later and get all of that discussed then so the evenings are more free.
I really hope I'm understanding this question correctly.
But again, it's also not the interest.
It's the values.
It's those core things that keep everything moving.
And I tell my current boyfriend about my ex-girlfriend.
He doesn't know I ever experimented and I'm not sure how he would react."
So there's two different parts here.
The first is, do you want to be with somebody that you feel you have to hide, who you are
or your past from?
That's a question you have to ask.
At the same time, you don't have to tell him everything either.
This is something that I don't see any reason why he has to know.
You're in the dating phase, I'm assuming, but you're not sure how he would react.
I'll go ahead and tell you, unless he's like super religious, this isn't going to be a
deal breaker for him.
So I think you might be worried about something that doesn't really need to be worried about,
but at the same time, what you disclose about your past is... that's a decision that you
make that you need to be comfortable with.
You don't owe anybody anything when it comes to who you were.
If this is somebody who's interested in who you are, they understand or they should understand
that who you are came from who you were.
So even if they don't like it, if they like who you are now, that's what should matter.
Why do men value purity?
Insecurity.
Generally speaking, almost, again, with the exception of those people who are incredibly
religious and have a religious motive for that belief, the root of it is insecurity.
That some other person was better in some way.
know name redacted. I've seen her at a name of a school that I'm not gonna say. I
want to ask her out but I don't want to seem like somebody who is asking her out
because she used to make movies, grown-up movies. In my experience she has been a
an incredibly like approachable nice person. So I mean as far as talking to
her I don't know that that would be an issue. At the same time I want to ask her
out but I don't want to seem like somebody who is asking her out for this
reason if you haven't really talked to her why are you asking her out I mean I
would start there I would just I would just talk to her but I would I actually
think she is she's dating somebody I think so okay my boyfriend wants to go
through my phone and when I said no he said all of his exes let him he's brought
it up repeatedly and now he's on this thing where he's saying I don't trust
him with my phone info I don't trust him I'm not asking to see his phone am I
wrong about this no you're not wrong about this you're allowed to set
boundaries. I know this is becoming like actually kind of common that that couples
might go through each other's phones and stuff like that. I will tell you I have
never gone through my wife's phone, she has never gone through mine. Long-term
everybody needs privacy. Everybody needs their boundaries, their barriers, or
respected. And I don't think you're wrong about this. He said all of his exes let
him. Well, that's why they're his ex. Brought it up repeatedly. He said that you
don't trust him when he's trying yeah that's a red flag I'm gonna be yeah
that's that's not good you not allowing him to go through your phone is you not
trusting him. Yeah, that seems like a very early statement that kind of
betrays a habit of maybe being kind of manipulative, just off-top. Like, again, I
know that this is becoming more common. I think it's super weird. I think that
people might be better off if they understood that everybody needs privacy, everybody needs
their own space, everybody needs time to themselves, and maybe constant surveillance isn't a good thing.
So yeah, I don't think you're wrong about that. Okay, and that's it. I actually kind of thought
thought there would be more and I was really expecting them to be... I don't know
what I was picturing but I was expecting this to be worse in some way. I was not
looking forward to making this video. Anyway, so there you go. There are your
answers. It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}