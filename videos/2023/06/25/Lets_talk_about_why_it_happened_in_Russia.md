---
title: Let's talk about why it happened in Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Y2hVaLhUtdM) |
| Published | 2023/06/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overnight, questions poured in about the chain of events leading to Russian forces advancing towards Russian cities inside Ukraine.
- The blame was placed on Putin, Ukrainian intelligence, the CIA, and Biden.
- The town of Solidar was where Wagner, Russian private contractors, faced heavy losses and were later given replacements who were normal Russian citizens, not experienced contractors.
- The replacements were sent to Bakhmut, where heavy fighting continued, leading to experienced contractors being upset at the loss of normal citizens.
- Putin viewed private forces and traditional military as in competition, leading to unacceptable waste and losses.
- The rank-and-file troops were tired of the waste and supported the boss, potentially due to Ukrainian intelligence playing a role.
- The boss criticized logistical issues and blamed the traditional military, hinting at the reasons behind the failure of the operation.
- The whole operation was deemed a failure, costing too much for minimal gain, especially impacting contractors who witnessed the losses of both experienced contractors and normal citizens.

### Quotes

- "Don't look for some man behind the curtain here."
- "This whole operation has been an unmitigated failure."
- "You don't have to look for some great big conspiracy."

### Oneliner

Overnight questions on Russian forces in Ukraine point blame at Putin, Ukrainian intelligence, CIA, and Biden, revealing failures from waste and loss.

### Audience

Military analysts

### On-the-ground actions from transcript

- Support organizations aiding war victims (implied)
- Advocate for transparency and accountability in military operations (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the events leading to the Russian forces' advance in Ukraine, including insights into the motivations behind the failures and losses incurred.

### Tags

#RussianForces #UkraineConflict #BlameGame #MilitaryOperations #Failure


## Transcript
Well, howdy there, internet people. Let's bow again.
So today, we're going to talk about why it happened.
Because overnight, a whole bunch of questions came in
trying to figure out how this chain of events was set into motion.
And most people were looking for who was to blame, who was behind it.
And of course, we are talking about the Russian forces
Russian forces, the private contractors that were working for Russia inside of Ukraine
and their advance towards the rear, them marching on Russian cities.
Okay, so the messages that came in suggested it was Putin, he's really behind it.
He's doing this to take out the generals he doesn't like.
Another was that it's Ukrainian intelligence that they played the boss of Wagner and set
all of this in motion.
Another of course is that it's the CIA.
And still another is that it is Biden and that this is all a distraction from his kid.
Don't worry.
in control. Way to not give him the power. And we'll come back to those options,
but let's talk about what actually set all of this in motion. How this happened.
It starts in a town called Solidar. Wagner was ordered to take this town and
they did it, along with a whole bunch of losses. A lot of them. It was really,
really bad fighting. When it was over they were given replacements but see
the replacements they weren't like the rest of Wagner. That firm was made up
mostly of very experienced contractors, warriors. The replacements they were
prisoners. They were normal Russian citizens. It's a little bit of a different
thing. And then after Solidar, which did in fact turn out to be the anvil on
which Wagner and Ukraine was broke, they were sent to Bakhmut. And the same thing
played out. Really heavy fighting trying to take this town. The thing is, those
experienced people, they weren't watching other warriors be lost anymore.
Because that's one thing. They were watching normal Russian citizens be lost.
And it made them mad. The waste. They were watching normal people be thrown
into the grinder and they didn't like it. This was very evident. There were a lot
of videos about this. There were a lot of communications from Wagner about this,
from their troops about this. Those decisions, how that played out, all of
those people lost. The massive amount of waste. Who's behind it? Who's to blame?
Putin. He viewed the private forces and the traditional military being in
competition as a good thing because it would motivate him because that's the
way he looked at stuff. That was an error. It was an error if you're sitting in the
Kremlin. If you're on one of those lines, it was unforgiving. They got tired of
the waste. Guaranteed. That's why the rank-and-file is behind the boss. Now, if
If you want to say the CIA or Biden or whoever got to the boss and made this happen, maybe,
maybe, but I would lean towards Ukrainian intelligence because they were actively trying
to play him.
It's possible.
It's completely possible.
But none of that matters without the rank and file.
The boss being flipped, that doesn't do anything if he doesn't have the support of the troops.
I would also remind everybody that the boss, well he was out there making videos too, standing
and fields with the lost around him, talking about the horrible leadership.
Now he never said Putin because that would be something that might lead to his early
retirement with a nice window view, but he made it clear that logistical issues were
causing a bunch of loss.
He made it clear that he believed the traditional military was behind it.
That's why this happened.
Don't look for some man behind the curtain here.
The answer is plain and simple.
This whole operation has been an unmitigated failure.
It has cost way too much to achieve way too little.
And those contractors saw the worst of it.
then they saw it happen to normal Russian citizens. That's what's behind this.
You don't have to look for some great big conspiracy. Now I know that it's easy
for me to say this now, today. I will have several videos down below, watch them in
order. The chain of events that led up to this was incredibly apparent if you
weren't believing every bit of Russian propaganda that came out. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}