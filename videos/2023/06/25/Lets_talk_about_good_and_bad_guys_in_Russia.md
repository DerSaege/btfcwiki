---
title: Let's talk about good and bad guys in Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6QPU3ggknPw) |
| Published | 2023/06/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines the concept of individuals viewed as bad doing good things and vice versa.
- Talks about motivations and how people rationalize committing evil acts for a greater good.
- Gives examples of Americans justifying heinous actions in the name of national security.
- Explores the idea of individuals fighting not against what's in front of them, but for what they believe they're protecting.
- Illustrates how loyalty and the belief in fighting for a greater good can lead individuals to participate in actions they may find questionable.
- Touches on the importance of perceived waste and the illusion of doing the right thing in conflicts.
- Concludes by reflecting on the universal nature of justifying actions in war and the influence of societal norms.

### Quotes

- "Just remember before you can commit a great evil you have to be convinced that it's for a greater good."
- "Soldiers do not fight against what's in front of them. They fight for what's behind them, what they believe they're protecting."
- "You have to believe that. If they don't, well then they have to deal with everything that they did."
- "Because at the end of it, you have to remember, everything that occurs in war is horrible."
- "We identify ourselves by lines on maps and flags, the little things that separate us."

### Oneliner

Beau examines how individuals justify heinous acts for a greater good, delving into loyalty, rationalization, and the universal nature of war justifications.

### Audience

Advocates for Peace

### On-the-ground actions from transcript

- Question societal norms that perpetuate the justification of violence (implied)
- Educate others on the dangers of blindly following authority figures (implied)

### Whats missing in summary

The full transcript dives deep into the psychology behind justifying immoral actions in the name of a greater cause, urging viewers to critically analyze motives beyond blind loyalty.

### Tags

#War #Ethics #Loyalty #NationalInterest #HumanPsychology


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the good and the bad
and why sometimes people that you would view as bad
do good things.
Conversely, you could see it the other way as well.
We're going to talk about motivations
and how things happen and how sometimes you
see a situation and it's hard to really make sense of it because it goes against
what you believe. After the video laying out the likelihood of why the
contractor troops in Ukraine, the Russians, participated and followed
their boss on that little excursion into Russia. A whole lot of questions because
it seems it seems very counterintuitive to believe that that group Wagner who's
responsible for some pretty heinous stuff would even remotely care about
conscripts or anything like that. It doesn't make sense and it doesn't make
sense because most people view them as evil and it's hard to picture them doing
something good. Just remember before you can commit a great evil you have to be
convinced that it's for a greater good. The story is old as time. But it's hard
to see when you are talking about a group you see as evil, a group you see as
opposition, and I'm willing to bet that 99% of the people watching this channel
view Wagner that way. Okay? And make no mistake about it, this is not a video
saying that they're the good guys. But it's hard to understand the concept if
you are talking about the opposition. So we're not going to use them to start off
with. We're going to talk about Americans. We're going to talk about Americans doing
something pretty heinous, something objectively wrong, something against the
laws of armed conflict. We're going to talk about how it was justified. We're not going
to reach too far back into the past because we don't need to. This happens all the time.
We're going to talk about the U.S. enhanced program, you know, the one that involved boards
and water and towels trying to get information. When politicians tried to justify that to
the American people. What did they do? They did one of two things. One group was
like, no, that's definitely not torture. It's something else. Just
know that if you're one of those people who believe that, give me 10 minutes. I'm
willing to bet I can make you see that it is, because after that third or fourth
poor, you'll tell me it is whether you believe it or not. So you had that group.
They said, oh it really it's not that bad. The denial group. And then you had the
rationalization group. And when they sold it, what did they say? You know if there
are only minutes and it takes that to save one American life, you can bet I
that I would do it, politician, yeah, because, you know, they think it makes them seem tough.
Person strapped to a table, it doesn't take a whole lot of, you know, toughness to do
that, but that's how they sold it, creating this scenario.
It was in furtherance of what was perceived to be a greater good, saving the American
life, because that American life, well it's worth more, right? That was the logic that got used.
It's not just a U.S. thing, it's not just a U.S. and Russia thing, it's not just a world power thing,
it's not just a modern thing. This mindset, this justifying the evil for the greater good,
this is a human thing that goes back to the first time lines were drawn on a map
and somebody got the idea to maintain power by saying those people over there
those others better them than you. They're the bad people. It's a story as
old as time. Make no mistake about it even though right now this minute most
people in the United States would probably say that that type of
questioning was wrong and out of line. It wouldn't take much to justify the
United States right back into that position and get that that manufactured
consent going. It wouldn't take a lot at all. A couple of action movies, a few politicians,
be right back to it. Widely accepted. Okay, so the concept is in play. It's out there.
We understand how this happens. Now we're going to talk about Russia. You're a 19 year
old Russian. You sign up, you join the army, you do a few years. You get an offer from Wagner.
You're going to the big leagues now. Why'd you join? Now realistically, there are material
concerns in your life, right? It could be poverty. It could be wanting to get out of the place where
you lived. It could be a whole bunch of things. But you signed up. Once you signed up, why were
you doing that job? Why were you fighting? Why were you traveling the world and doing that?
Would you tell yourself it's to protect Russia, right? Now in reality when you're talking about
Wagner, it's protecting Russian national interests because they are, prior to this conflict, they were
were Russia's soft imperial arm. So that's what they were doing, but they
wouldn't say it that way. They would say, you know, protect the Russian way of life.
You can switch out all of these terms for American, by the way. And that's
how they view it. Soldiers do not fight against what's in front of them. They
They fight for what's behind them, what they believe they're protecting.
Because of that, they can often rationalize, justify pretty heinous acts because they're
doing it for that reason.
years goes by you're you're done with a couple of stints in Africa you're sent
to Ukraine you're at Solidar horrible horrible fighting lots of lots of loss
right but it's okay because you're protecting Russia that's the way you're
reviewing it.
And then you're sent to Buckmoot.
The whole time you're telling yourself you're protecting Russia, it's not lines on a map.
You're personifying it in your mind.
You're not thinking about national concerns.
You're thinking about the fact that you're protecting some fresh-faced 19-year-old Russian
kid just like you were a few years before. And then you see fresh-faced 19
year old Russian kids thrown into the grinder with just reckless abandon by
your leadership. You start to question it. It's a super lethal form of cognitive
dissonance. Your rationalization, your justification for all of your heinous
acts, well, it doesn't ring true. You realize it might be more of an excuse, but there
is one way you could make it a justification again, and that's to stop
it, to follow through with your own rhetoric. Now imagine you have a boss, and
your boss is telling you, you know what? This isn't us. We didn't do this. This is
the Ministry of Defense. Them, those, the others, those part of that other group,
not you, not the people you have a bond with, not the people that you have fought
with says other people. They're the ones that are responsible for it. They're not
giving these fresh-faced kids the right equipment. They're not giving the ammo.
They're not providing the air support. You probably believe it because it's
just a continuation of the same process. A smaller group that you owe loyalty to
and those others over there, they're to blame. So if the Russian military
intentionally or on accident hits your position and your boss tells you it was
on purpose and says, hey we're gonna go deal with this, you probably go along
with it. You probably feel a lot of loyalty to that boss. Now your boss, he
may be doing it for entirely political reasons, monetary
reasons, ego reasons. Could be any of that. But if he sells it to you, you'll buy it.
You have a lot of loyalty to it. That was one of the things in the comments
under that video, a lot of people were like, well, no, they did it because they were paid.
No, that's a common misconception.
Make no mistake about it, there's a lot of loyalty.
How much do you think one of those Wagner troops could have collected from Putin for
putting their boss in their sights?
But they didn't.
Because they believed they were doing the greater good.
have to believe that. If they don't, well then they have to deal with everything
that they did. Yeah, the most likely reason that Wagner troops moved in this
way stayed loyal to the boss. We're willing to go up against the Ministry of
defense, it has its root in conscripts being thrown away, but understand its
rationalization. They were angry about it. They didn't like to see the waste
because it runs counter to their own programming, to the things that they say
they hold to be right and good and true to the things that justified their
actions. Once that goes away, that's probably pretty hard to deal with. You
might be willing to go up against your own country to keep that illusion intact.
act. So that's your most likely reason. Now as far as the boss, understand that
video is not about the boss. There could be a whole bunch of motivations. Now I do
believe that he was upset about the waste, but not caring about the
individual, you know, fresh-faced 19 year old. He viewed it more as equipment that
was being wasted. You got to remember, no matter how you feel about what he was
doing, even if he was successful in removing Putin, even if that went all the
way through. He's still not a good guy. He's a pretty evil dude. Sometimes, and this is
a very American thing to do, Americans like to see good guys and bad guys. Sometimes it's
two bad guys fighting, and that was definitely the case there. You know, and it goes back
to that question, who do you want to win? I want them both to lose. So when you're
struggling with why you might agree with their cause, understand that you're
probably coming to that conclusion from a place of it's waste. It's a bunch of
for no reason. Most of the people on this channel believe that there's really
only one kind of acceptable war. And the people who were on that march, they
They probably got to that place from a position of this is what I have to do to continue to
look at myself and say I believe these things.
They don't want to look at themselves if they don't believe those things.
Believe me, it's not just a Russian thing.
It's not just an American thing.
This is a story as old as time.
It's true of every nation in the world, whether they follow the rules or they're brutal.
Because at the end of it, you have to remember, everything that occurs in war is horrible.
Everything.
Anything that is done in combat.
you did it without a flag flying over you, it would be a crime. It's a
justification. It's those lines on the map. Most people, they have some of that ingrained
in them from the society around them and it doesn't take a huge push to get most
people to accept it because it's so pervasive. We identify ourselves by
lines on maps and flags, language, the little things that separate us, and it's
very easy for those in power to convince people that those things really matter
and they make us different and therefore those others, well, better than them than you.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}