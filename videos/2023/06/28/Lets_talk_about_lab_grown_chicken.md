---
title: Let's talk about lab grown chicken....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pGm3X0SAukc) |
| Published | 2023/06/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- USDA cleared the way for companies like Upside Foods and Good Meat to sell cultured, lab-grown meat, initially in high-end restaurants due to pricing.
- Half of Americans are unlikely to try cultured meat, with only 18% very or extremely likely, citing reasons like it sounding weird or unsafe.
- Cultured meat production could alleviate environmental impact caused by traditional agriculture.
- Cultured meat is safer as certain risks like salmonella or E. coli are eliminated.
- Beau is extremely likely to try cultured meat to address arguments about veganism and animal rights without feeling guilty.
- Scaling cultured meat production could solve various issues beyond initial expectations, reaching wider markets and aiding in times of famine.
- Despite widespread distrust of lab-produced animal products, the benefits for the environment and food safety are significant.
- There is a potential positive impact of cultured meat production, despite initial skepticism.

### Quotes

- "If this can be scaled enough, this can alleviate a whole lot of problems."
- "I am obviously one of those people that is extremely likely to try this."
- "Their argumentation is rock solid."
- "Cultured meat is safer."
- "There's a lot of benefits to this."

### Oneliner

USDA clears the way for cultured meat production, facing skepticism from Americans but holding promise for environmental impact and food safety.

### Audience

Food consumers, environmentalists, policymakers

### On-the-ground actions from transcript

- Support companies like Upside Foods and Good Meat by trying out cultured meat when available (implied)

### Whats missing in summary

The full transcript provides deeper insights into the potential benefits of cultured meat production and the shift it could bring in addressing environmental concerns and food safety on a larger scale.

### Tags

#CulturedMeat #LabGrown #FoodSafety #Environment #Sustainability


## Transcript
Well, howdy there internet people, it's Bill again.
So today we are going to talk about cultured meat, lab grown,
and the USDA not chickening out on us.
Okay, so we'll go through that, go over some polling about it,
and just give it a good once over.
The USDA has kind of cleared the way for two companies.
one is called Upside Foods and the other one is called Good Meat to sell cultured meat, lab-grown meat.
Initially it's not going to be in your grocery store, it'll be in restaurants,
probably high-end restaurants because it looks like it's going to be pretty pricey to start with.
They have to figure out how to scale production first. Companies seem to believe they have a way
to get there. So it's on its way. It is starting. The American people do not
really seem enthusiastic about it from from the polling. Half of Americans are
unlikely to try it. Only 18% were very likely or extremely likely. The reasons
to not try it are let's see 56% of those who said they were unlikely to try it
said it sounds weird 48% says it doesn't sound safe 35% no reason to change the
meat I eat and 27% fear I won't like it okay I can't do anything about sounds
weird. Doesn't sound safe. I mean, it went through the process. It went through the
process. It's gone through all of the clearing. I know there's a whole lot of
people that don't trust that. You know, maybe I do have something that when it
comes to sounds weird. Just openly describe the entire process of how you
get your current meat. I mean, that probably sounds a little bit
stranger. Then you have no reason to change the meat I eat. Oh, yeah you do.
Yeah, you do. You know, right now it's just chicken, but I know one of these
companies is already working on how to do it with beef and pork is not
supposed to be far behind. Here's the thing. Large agriculture like that, it is
really hard on the environment. It is very hard on the environment. Cultured
meat would help alleviate a lot if it saw widespread use. You also have the
reality that it is in many ways it's going to be safer. Like with chicken
you're not gonna have to worry about salmonella or E. coli, anything like that
because the part of the animal that that kind of comes from, well that part's not
grown. So you don't have to worry about it. So you do have reasons. If you
won't like the taste. Yeah, I can't do anything about that. I am obviously one
of those people that is extremely likely to try this. The main reason is that when
it comes to people who talk about veganism, animal rights, stuff like that,
I don't have a counter-argument for their arguments when they're like these
These are the reasons people should go this way.
I don't have an argument against them.
Their argumentation is rock solid.
And this is a way for me to still eat meat and not feel guilty.
So this is one of those things that we've kind of been waiting for.
If this can be scaled enough, this can alleviate a whole lot of problems.
Like a lot.
Way more than you're thinking initially.
Yeah, just like everything else, it will start catering to the wealthy, high-end restaurants.
Then as it scales, it'll hit grocery stores.
As it scales more, man, can you imagine just being able to produce food?
there's, when there's famine? There's a lot of benefits to this. I know there's a
massive distrust of science right now, especially when it comes to anything
related to producing an animal in a lab. We've had a whole series of
progressively worse movies about that but the the benefits of this if they
could scale it up they're pretty big for the environment for people for food
safety when it comes to diseases stuff like that. I doubt that this is going to
change many people's opinions, but there's definitely a wider picture to
look at here. And overall, even though it sounds weird, this may turn out to be a
really good thing. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}