---
title: Let's talk about single moms and a question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=yOWptv2xfl8) |
| Published | 2023/06/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the scenario of a woman dating a man for two months who doesn't want him to meet her son yet because she wants to ensure he will be around long-term.
- Acknowledges that some men might find this decision insulting.
- Breaks down the logic behind why a single mom might take time before introducing her child to someone she's dating.
- Puts the situation into perspective using the amount of time spent together to show that two months might not be sufficient for evaluation.
- Encourages men to see this boundary as a way for the woman to protect her family and potentially theirs in the future.
- Suggests that until a couple has gone through challenges and seen each other's true selves, meeting the child might not happen.
- Emphasizes that this boundary is about ensuring that those around her children are good influences.
- Concludes by framing this boundary as a positive step towards safeguarding their potential future family.

### Quotes

- "This is the length she's willing to go to protect what might be your family."
- "Don't take it as an insult. It's her trying to make sure that those who influence her children are good people."

### Oneliner

Beau breaks down why a single mom may wait before introducing her child to someone she's dating, aiming to protect her family and potentially theirs in the future.

### Audience

Men in dating situations.

### On-the-ground actions from transcript

- Understand the logic behind a single mom wanting to take time before introducing her child to someone she's dating (implied).
- Respect and support the boundaries set by single parents when it comes to their children (implied).
- Put yourself in the position of the single parent and think about what's best for the family unit (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of why it's reasonable for a single mom to delay introducing her child to someone she's dating, focusing on the importance of protecting both her family and a potential future family.

### Tags

#Dating #SingleParent #Family #Boundaries #Respect


## Transcript
Well howdy there internet people, it's Bob again. So today we are going to talk about
when you get to meet the kid. Over on the other channel I did a Q&A that was full of
a bunch of questions that had to do with dating and relationship that had come in over the
last couple of months. One of them was a question from a woman who had been dating a guy for
two months and she has a kid and he wanted to meet the kid and she's like
yeah I'm not really ready for that yet and she only wants she doesn't want to
introduce her son to somebody until she knows he's going to be around and the guy
was insulted by this. I said that that is cold hard logic it should totally make
sense. I got this message. I watched your dating video. The question about the
single mom kind of hit home. I'm in the exact same situation with a woman and
she watches you. I don't think it's her because she doesn't seem like she would
send you a message, but she would probably say the same about me. Side note,
do you know how hard it is to find this email? Question for Bo at Gmail. I didn't
make a big deal about it, but I definitely felt it was a little
insulting to be told she wasn't ready for me to meet her son. You said it was
cold logic. Can you maybe make a video breaking down the logic? Because look at
the comments. Lots of men didn't understand this idea. Yeah, I got you. And
I promise this is going to make sense. Okay, so start with this. The society as
as it is, okay? Mom
dating, it's not necessarily great,
right? The patriarchal structure
of our society kind of indicates that mom
should, you know, not have any needs. So
she separates that from
her son, okay? It's a choice she made. Then you have the
the aspect of protecting her son, okay. Now, people say two months, like when
they're dating, two months. To me, two months is not a long time, but for some
it might be. Let's do it a different way. Let's look at hours. She's a single mom.
At best, you're talking about two dates a week, right? You want to hang it
out two times a week. Let's say it's eight hours each time, 16 hours a week.
So you're looking at what 64 hours a month, 128 hours. I know guys who can't
decide whether or not they like a video game after 128 hours. That is the amount
time she has had to evaluate you. That's probably not enough. Now you may say, well, I want
to know, you know, what if the kid doesn't like me? What if that, well, that's just part
of it. You're going to have to wait one step at a time. It's a package deal, but you have
to kind of go through the first part to get there. And you need to look at it through
the lens of what you would want your wife to do when it came to your family.
This whole behavior, this is not an insult to you.
This is her protecting her family, maybe your family one day.
Don't take it as an insult.
It's not.
It's her trying to make sure that those who would be around or have influence over her
children are good people.
And you may say, you know, it's been a long time, but it probably hasn't, not really.
I mean, again, you're talking about 128 hours, like max.
And that 128 hours is in the best time, you know?
That's the dating phase where everybody's putting their best foot forward and all of
that stuff.
There's a comment under that video where it's basically a guy's like, yeah, I was in this
situation.
an incredibly long time to meet the kid. It wasn't until however many months and
we'd had a huge fight over something else. And after thinking about it, every
woman I know that has adopted this stance, the fight is also there before
they met. And that may be kind of wanting to see at your worst. I don't even know
that that is a conscious thing, but until you are past that, you know,
honeymoon phase of dating and she's seeing the real you, that's probably
not gonna happen. Not if it is somebody who has set that as a
boundary. And again, if you're thinking one day, this is going to be
your family. These are... this is something I think that you could only
see as a good thing. This is the length she's willing to go to protect what
might be your family. I think that that's something that most men could
appreciate. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}