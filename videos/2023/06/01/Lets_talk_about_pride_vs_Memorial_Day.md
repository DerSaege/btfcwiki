---
title: Let's talk about pride vs Memorial Day....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eFH8gFMQiNw) |
| Published | 2023/06/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the different ways certain events are celebrated and why some aren't advertised or celebrated as loudly as others.
- Responds to a question about the difference in celebration between Military Appreciation Month and Pride Month.
- Points out the minimal acknowledgment given by companies like Google for Memorial Day compared to the enthusiasm for Pride Month.
- Clarifies that Memorial Day is a day of remembrance, not celebration, for those who died serving.
- Notes that Armed Forces Day celebrates anyone in the military, Veterans Day honors those who served, and Memorial Day remembers those who died.
- Mentions that while Pride is a joyous occasion with parades, Memorial Day is typically observed somberly.
- States that Memorial Day is not about wild celebrations but about remembering and honoring the fallen.
- Emphasizes that the understated recognition of Memorial Day is a sign of good taste, acknowledging the day without going overboard.
- Suggests that people are free to celebrate Memorial Day joyously if they choose but should understand the day's true purpose.
- Concludes by sharing thoughts on the somber nature of Memorial Day and wishing everyone a good day.

### Quotes

- "Pride is a happy thing. It's supposed to be a happy thing."
- "It's not happily celebrated because it's not a happy day."
- "Memorial Day is not about wild celebrations but about remembering and honoring the fallen."

### Oneliner

Beau explains the somber significance of Memorial Day and contrasts it with joyous celebrations like Pride Month, clarifying the importance of solemn remembrance over wild festivities.

### Audience

Community members

### On-the-ground actions from transcript

- Attend a Memorial Day event to honor and pay respects to those who died serving (implied).
- Educate others on the true meaning of Memorial Day and encourage solemn observance (suggested).

### Whats missing in summary

The full transcript provides a deeper understanding of the reasons behind the subdued commemoration of Memorial Day and the importance of honoring the fallen in a solemn manner.

### Tags

#MemorialDay #PrideMonth #Celebration #Remembrance #MilitaryAppreciation #Community


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we're going to talk about why different things are
celebrated in different ways and why some things aren't
happily advertised everywhere and celebrated to the same
degree as other things.
We're going to do this because I got a message, and this is
something that I am happy to provide clarification on.
OK.
I'll admit, I didn't know about Military Appreciation Month.
But please explain why Pride Month is loudly celebrated and
Memorial Day isn't.
Google always has a giant logo change and is
openly happy on Pride.
Do you know what they did for Memorial Day?
a tiny American flag and gray letters,
the bare minimum to celebrate the day.
The people pride is for can have a giant parade and dance.
The people Memorial Day is for are silent
for fear of the woke mob.
You'll say it's a what about-ism and that if we cared,
we'd make it a wild celebration ourselves.
But I want to know why companies don't celebrate
tradition with the same degree of enthusiasm. Oh, I got you. I got you. I can explain this pretty easily.
Because you are partially right. The People Memorial Day is for they are silent, absolutely.
But it's not out of fear of the woke mob. It's because they're dead.
Armed Forces Day, anybody in the military. Veterans Day, those who served. Memorial Day,
those who died. It's not happily celebrated because it's not a happy day. You don't celebrate
Memorial Day you observe it. That's why it's treated differently. It's a day to
remember people who died. It's not a day of celebration. The understated Google
logo, which yes I was aware of it because I read the same outrage farming article
you did. That was what most people would call good taste. You're acknowledging it,
but you're not going over the top with it. It's not actually a day to buy a
mattress. But as far as the rest of it, you'll say it's a what aboutism and that
if we cared we'd make it a wild celebration. Yeah, you're free to do that
if you choose. But just understand that's not what the day is for, the tradition that
you seem so interested in. Pride is a happy thing. It's supposed to be a happy thing.
The parades and everything that you're looking for, those typically occur on Veterans Day,
Memorial Day. Most people I know who actually stick with the tradition of
Memorial Day and have reason to observe it, they normally don't like being around
anybody on that day. There are some exceptions where like a whole unit will
get together and have a barbecue or something like that, but it's not actually
a joyous occasion. It's supposed to be somber and understated. Anyway, it's just a
thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}