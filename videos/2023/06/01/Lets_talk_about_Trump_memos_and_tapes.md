---
title: Let's talk about Trump, memos, and tapes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MHrULso1GqA) |
| Published | 2023/06/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump allegedly had a meeting in July 2021 discussing a document related to a potential attack on Iran.
- The document discussed in the meeting is still classified, and Trump acknowledges he should have declassified it before leaving.
- Charges related to the document case focus on whether the information is national defense information, not just on its classification status.
- Trump's legal team may face difficulties in a push-play trial due to multimedia evidence, including recordings and recovered documents.
- If the specific document discussed in the recording was not returned initially, it could present a significant challenge for Trump's defense.
- The increasing information coming out could potentially lead to an indictment.
- Public information tends to accelerate before an indictment, and there is talk about what the prosecution possesses.

### Quotes

- "Here's the recording of Trump saying that he knew this document was classified and that he should have declassified it."
- "Personally I think it's gonna be hard to argue that he didn't know about this based on a lot of the reporting that has come out."
- "The more the information comes out, the more that seems likely."
- "Generally speaking, the amount of information that comes into public tends to pick up speed just before an indictment."

### Oneliner

Trump's potential legal troubles escalate as evidence mounts, raising concerns for his defense in a push-play trial amid classified document allegations.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Stay informed on the developments of the documents case (implied).
- Follow reputable sources for updates on legal proceedings (implied).

### Whats missing in summary

Insight into the potential implications of the ongoing documents case for Trump's legal defense and public perception.

### Tags

#Trump #LegalIssues #Defense #PushPlayTrial #NationalDefense


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and a memo and tapes, because there's tapes.
Of course there's tapes, allegedly.
Okay, so the news is saying that in July of 2021,
Trump had a meeting with a few people.
During this meeting, he was talking about a document that at least part of it dealt
with a potential attack on Iran.
According to the reporting on the tape, he acknowledges that it's still classified and
that he should have declassified it before he left.
That's going to be an issue for the defense.
It's worth reminding everybody that the majority of likely charges stemming from the documents
case do not actually hinge on whether or not the information was classified or declassified.
It hinges on whether or not it's national defense information.
I can assure you that anything that dealt with a potential attack on Iran or planning
of that nature, anything like that, is going to be national defense information.
One of the things that Trump's legal team is probably starting to worry about is that
they may be walking into a push-play trial.
It's a type of trial that has so much multimedia evidence that there's not a lot of testimony
required.
Here's the documents we recovered from Trump, from his place.
Here's the letter saying that he had returned everything.
Here's the recording of us removing those documents after he said everything was gone
and returned.
Here's a recording of Trump saying that he knew this document was classified and that
he should have declassified it.
I mean, that's a real issue.
That's going to be hard for a defense team to overcome.
would be especially true if whatever document is being referenced in this
recording is one that wasn't returned initially. You know, Trump's team said
everything was returned and they returned some stuff, right? If this
particular document was not in that batch, it shows that Trump knew of its
existence and didn't return it and it shows it in a multimedia fashion. I mean
personally I think it's gonna be hard to argue that he didn't know about this
based on a lot of the the reporting that has come out as far as surveillance
videos and stuff like that. I think he's in a world of trouble when it comes to
to being able to just deny or pretend that it was all an accident, but a
recording of him talking about a document, a specific document that wasn't
returned, that's going to be almost impossible to overcome. Again at this
point all of these are allegations. There hasn't even been an indictment yet, but
But the more the information comes out, the more that seems likely.
I would also point out that generally speaking, the amount of information that comes into
public tends to pick up speed just before an indictment.
And we are seeing a lot of people talking about things that the prosecution is said
to have.
So there's that.
Yeah, so basically that's a nice neat little update on the documents case, which is still
ongoing and in my opinion, probably the one he needs to worry about the most.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}