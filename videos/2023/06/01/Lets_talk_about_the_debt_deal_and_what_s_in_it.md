---
title: Let's talk about the debt deal and what's in it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=13u7rHlvzhI) |
| Published | 2023/06/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the inaccuracies and confusion surrounding the recent deal.
- Mentions the number of votes McCarthy needed from Democrats to pass the Republican budget.
- Democrats had more votes in favor of the bill than Republicans.
- Refutes the narrative of Republican victory, mentioning the party's dissatisfaction.
- Details what the Republicans wanted in the deal and what they actually got.
- Clarifies the difference between student loan forgiveness and the student loan pause.
- Addresses misconceptions about Biden's agenda and clarifies that major pieces of legislation remain intact.
- Talks about work requirements for social safety nets and clarifies it's not a cut but an expansion.
- Mentions the approval of the mountain valley project as a sweetener for senators.
- Emphasizes that Congress controls the budget, not the President.
- Criticizes the far-right Republicans for their extreme tactics that led to an unfavorable outcome for the Republican Party.
- Predicts that the deal is likely to pass through the Senate with minor changes.

### Quotes

- "The Republican Party certainly doesn't feel like it was a Republican victory."
- "Congress controls the budget, not the President."
- "The reason the Republican Party really didn't get anything that it wanted was because of all of those saying it's a bad deal right now."
- "Biden can put on his aviators and walk out of the room."
- "The far-right MAGA Republicans destroyed any chance the Republican Party had of actually getting anywhere."

### Oneliner

Beau explains inaccuracies in the recent deal, refutes the Republican victory narrative, clarifies key points like student loans and work requirements, and criticizes far-right Republicans for undermining their party's success.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Contact your senators to express your opinion on the deal and any amendments you'd like to see (suggested).
- Stay informed about the developments in the Senate regarding the deal (implied).

### Whats missing in summary

More in-depth analysis on the potential impacts of the deal and its implications for future negotiations.

### Tags

#PoliticalAnalysis #RecentDeal #Republicans #Democrats #Congress


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we're going to talk about the deal.
We're going to go over what's in the deal
because there's some inaccurate information.
Some of it's inaccurate, some of it's incomplete.
Some of it is inaccurate because it's confusing.
Some of it is inaccurate
because some people want to stick to a narrative.
So we're gonna go over that
and we will also answer the question
everybody wants to know who got the better of whom. And we'll start there. When we were talking
about this a few days ago, I said the easiest way to tell who won the negotiations was to see how
many votes McCarthy needed from the Democratic Party to pass the Republican budget. Okay.
Okay, when I said that, I was picturing they might need, I don't know, 20.
McCarthy needed votes to get the bill to the floor.
71 Republicans voted against it, 149 voted for it, 165 Democrats voted for it.
More Democrats voted for it than Republicans.
I understand that there are a lot of commentators out there trying to frame it as a Republican
victory.
The Republican Party certainly doesn't feel like it was a Republican victory.
They're all wearing capes right now because they are super mad.
Okay, what's in it?
We're going to start with the thing they wanted most, their top line portion of this.
They wanted spending kind of pinned to fiscal year 2022.
That did not happen.
That's not in there.
They wanted to claw back $80 billion that the Democratic Party had allocated for the
IRS to basically kind of upgrade higher auditors to audit wealthy people.
The Republican Party wanted to basically pull all that money back, make sure they didn't
get it, so that didn't happen.
They got a cut of less than $2 billion out of 80.
They got a cut just big enough so they could go and tell their constituents that they got
a cut.
not a big one. Okay, the student loan pause, and this is one that there's a lot of confusion over.
This is not student loan forgiveness. This is the student loan pause. When I was talking about this
before, I didn't bring it up and a lot of people asked about it. So in the bill, student loan pause
will end in August. When was Biden going to end it prior to the negotiations? August.
There's no change in that. He was going to end it in August before it was August, or 60 days
after the Supreme Court decided on student loan forgiveness, which is
separate. Okay, Biden's agenda as a whole. A lot of the people who are very vested
in the narrative that Biden caved are saying that he gave up major pieces of
his legislation. No, he did not. Pretty much everything's intact. There is
nothing really transformative. I think the biggest is actually the the two
billion dollars to the IRS. The Climate, Health Care, CHIPS, PACT, Inflation
Reduction Act, all of that is pretty much the same as it was before. Okay? There is
a weird provision in it where if all of the conditions are right and there's a
a continuing resolution and so on and so forth.
Later on, there could be a 1% cut
to both defense and non-defense spending,
if I'm reading this right.
Okay, the Republican Party got their work requirements
for social safety nets.
Now, there's a lot of people framing this as a cut.
It's not a cut.
It isn't.
It's annoying, it's pointless, generally does more harm than good, the work requirements.
But there were some fine print points that were in there.
And I kind of hinted to this early on and said, it's weird.
It looks like there's work requirements and an expansion.
There's an expansion.
Wasn't cut.
The Congressional Budget Office says an additional 78,000 people will be getting benefits.
Now as far as the work requirements, there are a lot of exemptions to that as well.
This does not apply to those who need it most.
Homeless people, kids in foster care, it's not there.
They're exempted.
Some of this also seems like something they can waive.
There were slight cuts to the temporary assistance to needy families.
But again, they're small and they are cuts to the grants that the feds give the states.
There is the mountain valley thing, that approval.
That is a sweetener for people in the Senate.
That's really what that is.
That's just to make sure that once it went through the House that it was going to coast
the Senate. There is something that's kind of good in that the environmental
assessments will move faster now and this is it's something that dirty
energy companies wanted but it's also something that clean energy companies
needed because a lot of those projects are being held up because some of these
environmental studies can take five years. My understanding and I've got to
look into this because this is something I actually care like a whole lot about
my understanding is it doesn't actually lower the requirements as to what they
have to demonstrate it's that it has to be done quickly which is good and they're
They're going to claw back $28 billion in unspent COVID relief funds.
That's it.
That's what all of this was over.
So it's important to remember, Congress controls the budget, not the President.
Republican Party, had they behaved in the House like normal people, instead of
trying to show how radical they were and, you know, take the debt ceiling hostage
and all of this stuff, they would have gotten a much better deal. They would
have gotten more. They were interested in showing they were radical, getting those
social media clicks and that, I mean, if you're, if you're going to take a hostage, you have to
be willing to pull the trigger, right? And they couldn't on the default. That's what happened.
Then once they put up the wild budget that everybody knew was just ludicrous,
It was the far right Republicans in the house, who really created this.
McCarthy is going to get all the blame for it.
Don't get me wrong, because their base is not going to accept that them
trying to be extreme didn't work.
trying to be extreme
didn't work so McCarthy's gonna get all the blame for it.
But at the end of this the idea to tie it to the debt ceiling
that was from the far-right Republicans.
That ridiculous budget that they sent up that nobody was going to approve
it came from them.
The reason the Republican Party really didn't get anything that it wanted was
because of all of those saying it's a bad deal right now. They did it. Now what
happens from here goes to the Senate. It's going to get through the Senate. It
is incredibly unlikely that it gets held up in the Senate. The Senate is a much
more deliberative body, there is very little chance that McConnell is going to
allow members of his party to throw the United States into default. So it's not
over yet. There's probably going to be a little bit of drama in the Senate, but
nothing to worry about and this is going to end up being the deal. Minor changes
but nothing substantial. At the end of this there are a whole lot of people who
are very vested in saying you know it was a McCarthy's fault, it was you know
Biden out-negotiated them, Biden caved because everybody had their narratives
about what was going to happen prior to the negotiations being completed. At the
end of this, Biden can put on his aviators and walk out of the room, but
the reason he can do it is because the far-right MAGA Republicans destroyed
played any chance the Republican Party had of actually getting anywhere.
And once they did that, Biden and his team took advantage of it.
They got played by Sleepy Joe.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}