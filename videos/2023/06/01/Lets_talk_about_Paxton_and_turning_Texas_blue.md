---
title: Let's talk about Paxton and turning Texas blue....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=To-mcXmMCrs) |
| Published | 2023/06/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Paxton claims credit for Trump winning in Texas by stopping mail-in ballot applications.
- Harris County had 2.5 million mail-in ballot applications, which Paxton deemed illegal.
- The lawsuit to stop mail-in ballot applications is credited with Trump's win in Texas.
- Democratic Party in Texas needs enthusiasm to win, already has the numbers.
- Republican Attorney General of Texas suggests Trump won due to reduced enthusiasm and turnout in democratic areas.
- Texans tired of current leadership can make a difference by showing up to vote.
- Texas may already be a blue state, waiting for enthusiasm to show up.
- Red states don't truly exist; it's about enthusiasm, not inherent political color.

### Quotes

- "Red states don't really exist."
- "Texas may already be a blue state."
- "Just because a state has been red before doesn't mean that it's a red state."

### Oneliner

Paxton claims credit for Trump's win in Texas by stopping mail-in ballot applications, suggesting Texas may already be a blue state waiting for enthusiasm.

### Audience

Texans

### On-the-ground actions from transcript

- Show up to vote in Texas elections (exemplified)
- Increase enthusiasm and turnout in democratic areas (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of the political dynamics in Texas and the importance of voter enthusiasm in potentially shifting the state's political landscape.


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Paxton and Texas and turning Texas blue.
And something interesting that Paxton said that might provide a lot of
enthusiasm for people in Texas.
So he was on a podcast and this is what he'd said.
If we'd lost Harris County, Trump won by 620,000 votes in Texas.
Harris County Mellon ballots that they wanted to send out were 2.5 million.
Those were all illegal and we were able to stop every one of them.
He's taking credit for Trump winning, saying that his suit to stop this is what allowed Trump to win.
Now, a quick fact check, they weren't ballots, they were applications for mail-in ballots,
but he was able to stop them.
The attorney general of Texas is claiming credit for Trump winning in 2020, for him
taking Texas.
He's saying that if that didn't happen, if their suit to stop the applications for those
mail-in ballots. If that wasn't successful, Trump would have lost. Texas
would have turned blue. That's what he is saying here. He is taking credit for
that win. He's saying the only reason Trump was able to hang on to Texas is
because they made it harder to vote. Now we don't know that that's true. We
We really don't, but the attorney general of Texas, the one currently impeached, is
saying that it is definitely within the realm of possibility.
In fact, Texas may already be blue.
It may already be a blue state.
In a recent video, we talked about this and we went over the numbers and we talked about
how really all the Democratic Party needs in Texas to win is enthusiasm.
They have the numbers.
They just have to show up.
Now it's not just random math saying it.
It's not something that can be written off as, you know, a political commentator saying
something to try to make people more enthusiastic, it's a fear on the other side.
The Republican Attorney General of Texas is saying the only reason Trump won is because,
well, that made it hard for a very democratic area to vote, reduced enthusiasm, reduced
turnout.
That that suit is what stopped that.
If you are somebody who is in Texas, who is tired of the leadership that you have in Texas,
please understand you can do something about it.
It's not something that is unwinnable.
You're not in an area that is so lopsided and so red that you don't stand a chance.
You absolutely do.
In fact, the attorney general thinks that it is so likely he's taking credit.
He's saying that he did it.
He gave Trump Texas through this lawsuit.
It might be worth remembering that just because a state has been read before doesn't mean
that it's a red state.
Red states don't really exist.
The numbers are there.
enthusiasm isn't. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}