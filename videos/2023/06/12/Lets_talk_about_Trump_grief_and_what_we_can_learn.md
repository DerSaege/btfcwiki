---
title: Let's talk about Trump grief and what we can learn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1wgi08czj9c) |
| Published | 2023/06/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring reactions from conservative circles regarding Trump's indictment, focusing on emotions and the shift from personality to policy.
- Themes include denial ("He didn't do anything wrong"), anger ("It's time for war"), bargaining ("But Hillary's emails"), depression ("It's the end of the republic"), and acceptance.
- People invested in Trump as a personality are experiencing grief as they confront the metaphorical death of that image.
- Beau advocates for focusing on policy over personality in politics to reduce polarization and move the country forward.
- Encourages people to view politicians as flawed individuals rather than saviors, advocating for a more policy-centric approach to governance.

### Quotes

- "He didn't do anything wrong. Denial."
- "It's the end of the republic, depression, eventually there's acceptance, so stages of grief."
- "Don't elevate them. Focus on policy."
- "Maybe you should base that on a little bit more than, well I like that he makes liberals mad, or he says what I want to hear."
- "Focus on policy."

### Oneliner

Exploring conservative reactions to Trump's indictment reveals a shift from personality to policy, advocating for a more informed and less emotionally driven approach to politics.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze policies of politicians (implied)
- Shift focus from personality to policy when evaluating political figures (implied)
- Advocate for informed and objective political discourse (implied)

### Whats missing in summary

The detailed emotional journey of individuals invested in Trump's persona and the potential consequences of prioritizing personality over policy.

### Tags

#Trump #ConservativeCircles #PolicyOverPersonality #PoliticalAnalysis #Polarization


## Transcript
Well, howdy there internet people, it's Bo again. So today, we are going to talk about some of the things that we are
hearing from conservative circles, particularly those who are more emotional than others, related to Trump's indictment,
and it's kind of revealing, if you look at the
types of emotion that are being expressed and it leads to the idea of
maybe looking at policy is better than looking at personality. What are some of
the things you've heard? He didn't do anything wrong. Trump didn't do anything
wrong. He was allowed to do it. He didn't do anything wrong. All of those boxes
were totally supposed to be there, right?
Yeah, that one. You have,
well that's it, now it's time for war.
Okay. But Hillary's emails
and, well that's it, it's the end of the republic.
Those are the most common kind of themes you're hearing.
He didn't do anything wrong. Denial.
all the Civil War stuff, anger, but Hillary's emails or but Biden's son or whatever, bargaining.
It's the end of the republic, depression, eventually there's acceptance, so stages
of grief.
This is something that, I mean while it's entertaining on some level, it isn't because
are people who are actually experiencing these emotions. Now many of the people
who are feeding into it, they're not. They're just doing it for ratings or
whatever. But there are people who were that vested in Trump as a personality,
as a savior. Only he can fix this. They got invested in the person rather than
the policy. They got invested in following an image of somebody who was
just unstoppable and infallible, knew everything, very stable genius. Now
they're being confronted with the metaphorical death of that image and it
it really is painful for them. It's probably, it is probably experiencing actual grief.
If you want to guard yourself against this in the future, just remember that every politician,
even the ones that, you know, the majority of people who are watching this channel like,
They're people, they're flawed, they're not saviors.
They're politicians who found their way to power and fame.
If the American people focused more on policy and less on personality or party, this country
would be a whole lot better off.
There would be a whole lot less polarization.
There would be less anger.
The country would move forward rather than falling behind, denial, anger, bargaining,
depression and acceptance.
We're seeing it play out.
They don't go in order, you know, they go back and forth, but we're seeing it play
out for a significant portion of Americans who became so invested in a
myth, an image of a person that even the slightest objective look at would tell
you that it is just image. All politicians, everybody, people, they make
mistakes, they do bad things, they're people. Don't elevate them. Focus on
policy. How they're actually going to help you. You are entrusting this person with
your consent to be your better, to govern you. Maybe you should base that on a
little bit more than, well I like that he makes liberals mad, or he says what I
want to hear. Look at the policy. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}