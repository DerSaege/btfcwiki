---
title: Let's talk about Trump and republican spin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AthFFXZoRac) |
| Published | 2023/06/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the misinformation being spread by certain right-wing commentators regarding Trump's indictment.
- Acknowledging the push for wild and inaccurate information about Trump's situation.
- Emphasizing that the situation is a criminal matter in a federal courtroom, not just a political one.
- Pointing out that Trump's political strength has actually increased post-indictment, contrary to what some are claiming.
- Asserting that the former president is likely to be convicted of some charges, with this being seen as the first indictment, not the last.
- Noting that commentators are focusing on the political implications rather than the criminal nature of the case.
- Stating that the trial's outcome will not be determined by political commentators' opinions.
- Warning against reporting hopes rather than what is most likely to occur in reality.
- Mentioning the potential damage to credibility for outlets and commentators spreading inaccurate information.
- Stressing that the proceedings are not influenced by political commentators' narratives.
- Reminding that this is a criminal proceeding with political implications, not a political event.
- Concluding by underlining that the most likely outcome is the former president facing some charges.

### Quotes

- "It's about the jury box."
- "This is a criminal matter in a federal courtroom."
- "The trial is going to be covered, the information is going to come out, people are going to read the indictment."
- "It's not good for accuracy."
- "Let him."

### Oneliner

Addressing misinformation on Trump's indictment, Beau clarifies this is a criminal, not political matter, with the former president likely facing charges.

### Audience

Viewers

### On-the-ground actions from transcript

- Read the indictment and follow the trial proceedings (implied)
- Ensure accuracy in reporting and information sharing (implied)

### What's missing in summary

The tone and nuances of Beau's delivery that add depth to the analysis.

### Tags

#Trump #Indictment #Misinformation #CriminalProceeding #Accuracy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump and spin
and statistical realities and likelihoods
and the drive among certain commentators
to put out less than accurate information to their viewers.
Because I got a message that said,
hey, you know, normally you're always out there
countering the spin that's being put out.
And right now, for right-wing commentators,
it's a race to see who can put out the wildest information
in regards to Trump's indictment.
Yeah, I mean, normally that is something I do.
Let them.
Ready, set, go, race, spin, have fun with it, do it.
Let them.
I've seen it.
Talk about the Presidential Records Act, making it seem like that's central to this.
It's not.
Trump is stronger politically now than he was before the indictment.
It's just boosting his poll numbers, guaranteed him a second term, so on and so forth.
I've seen it.
Let him run with it.
They are still treating this like it's a political matter.
It is not.
is a criminal matter in a federal courtroom. The most likely outcome from
this is that the former president of the United States will be convicted of at
least some of those charges. And keep in mind, this is widely seen as the first
indictment, not the last.
they're treating it as a political
a political development
rather than a criminal one
that has political implications.
Because of that
they're actually ignoring
the real political implications.
They think it's going to help him win. He's going to win the primary.
Fine!
Fine, that's fine.
Go ahead, put him on the ballot in all 50 states, run with that.
Let's see how it works out.
The statistically speaking, he is going to court in a district that doesn't lose.
They don't lose very often.
the reality and they don't lose very often with normal cases. Can you imagine
how much effort they went to to make sure that every single piece of
information was verifiable? Why do you think there were only 31 counts for
willful retention instead of however many there could have been? A whole lot
more because those are the ones that without a doubt they think they have in
mind. There are a whole lot of political commentators on the right wing that are
pushing fantasy to their audience. If they want to say it's politically
motivated, that's fine. The trial is going to be covered, the information is going
to come out, people are going to read the indictment.
The verdict certainly will, assuming it gets that far.
This is a whole lot like the run-up to the midterms.
There's going to be a red wave, we're going to have a 40 seat majority in the house, it's
going to be just amazing." And then you had people saying, yeah, I don't know
about that. Y'all are kind of counting on a definition of likely voter that
altered when you started, you know, taking rights away from people. But they
pushed it. It wasn't that they didn't have the information to see what
everybody else saw, it's that they wanted to push the fantasy because that fantasy,
that cheerleading, it's good for them.
It's good for their brand.
It's good for views, all of that stuff.
It's not good for accuracy.
Many people are reporting what they hope will happen rather than what is most likely to
car rather than what has actually happened. And all it's going to do is
further undermine the credibility of those outlets, of those commentators. If
they want to go out there and say, you know, it's a political witch hunt and
you know, he's going to be found not guilty of everything because he
didn't do anything wrong and there's no evidence and this is baseless and so on
and so forth and, you know, he's stronger today politically than he ever was despite all information
to the contrary. Let him. Let him. That courtroom, those proceedings, they're not going to be
determined by political commentators. Their opinion doesn't matter and believe me, having a whole
bunch of political commentators as friends, that's going to hurt them a lot, but it doesn't
matter. That's not what is going to decide this. This isn't a run-up to an election where
commentators have sway. This is a criminal proceeding that has political implications.
It's not a political event.
The most likely outcome is that the former president of the United States catches some
of these charges.
That's the most likely outcome when you look at all the statistics from that district.
If they want to say otherwise, they want to tell you they're based that, that's on them.
Unless something becomes actively harmful, I'm not going to address that spin.
It doesn't matter.
If you address it, in some ways you may end up giving it validity that it doesn't have.
Because this isn't politics.
This isn't about the ballot box anymore.
It's about the jury box.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}