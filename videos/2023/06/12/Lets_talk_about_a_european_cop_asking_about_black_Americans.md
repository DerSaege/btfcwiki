---
title: Let's talk about a european cop asking about black Americans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gGdqYh5F5LE) |
| Published | 2023/06/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A cop in Europe reached out for advice on dealing with black Americans in a good way.
- The cop in Denmark handled a situation involving a black American couple on vacation arguing over insulin left at home.
- The European cop was bothered by the reaction of the black American man, throwing his hands up in compliance due to fear from U.S. law enforcement behavior.
- Beau suggests starting interactions with "It's cool, we're not American cops."
- European cops practice consent-based policing compared to the domination-based policing in the U.S.
- The European cop aims to make people more at ease when interacting with law enforcement.
- Suggestions are welcomed from black Americans on what can set them at ease in such situations.
- American law enforcement behavior is seen as out of line compared to the rest of the world.
- The European cop hopes that calling Danish police won't be associated with a death sentence for Americans.
- Beau encourages sharing suggestions to improve interactions with law enforcement.

### Quotes

- "It's cool, we're not American cops."
- "American law enforcement behavior is so out of line with the rest of the world."
- "Calling Danish police does not come with the chance of a death sentence."
- "The behavior exhibited by a large portion of American law enforcement is so out of line."

### Oneliner

A cop in Europe seeks advice on handling black Americans, focusing on calming interactions and understanding the fear caused by U.S. law enforcement behavior.

### Audience

European Law Enforcement 

### On-the-ground actions from transcript

- Share suggestions on how law enforcement can make interactions more comfortable (suggested).
- Practice consent-based policing techniques to create safer interactions (exemplified).

### Whats missing in summary

The full transcript provides a nuanced look at the differences in policing approaches between the U.S. and Europe, aiming to improve interactions and ease fears caused by American law enforcement behavior.

### Tags

#LawEnforcement #BlackAmericans #CommunityPolicing #PoliceReform #Fear


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about a question
that came in from a cop in Europe.
Basically asking how to deal with black Americans
in a good way.
I will provide my suggestion,
but if you are a black American,
down in the comments, put what you would like to hear,
what would set you at ease if you were in this situation.
The officer said that they would share the information with their colleagues.
So what happened?
What prompted the message?
Short version.
A cop in Denmark is walking along just doing their thing in a shopping district.
They get a report that there's a disturbance.
So they walk over.
They are better trained there than in the United States.
So when they make visual contact, they can see the couple.
they realize it's not, nobody's getting hurt. So they take their time and
they're listening to the argument as they walk closer using time, distance,
you know, trying to evaluate what's going on. And they kind of figure out as they're
walking closer that this is a black American couple on vacation, husband and
wife and they're arguing over who left the insulin at home. That's it. That's
what's going on. Now it's Europe, you know, you need insulin. The solution to
this issue is not to lock somebody up, it's to give you insulin, you know, solve
the problem. So the officer walks over and as soon as they call out, the guy, you know,
he throws his hands up, I'm complying, I'm complying. This bothered the cop because from
their perspective, this is just a wild reaction to their presence. And they say they understand
it because of the behavior of cops in the U.S., but they would like to know a way to
set people at ease.
And again, I'll provide my suggestions, but there might be better ones in the comments
section.
I would say exactly what you're thinking.
It's cool.
We're not American cops.
I might actually start with that, and if your boss doesn't, you know, want you to
constantly mock American police, maybe just say, start off with, you know,
nobody's in trouble, it's okay, how can we help? Because there is a, there is a
very conditioned response to the presence of law enforcement. And we don't have a lot of Andy
Griffith cops left in the U.S. We don't have cops that use consent-based policing, which is what I
think y'all use there. It's very different. It's very domination-based. So their reaction isn't,
I mean, it's not, it actually, it's not weird. In fact, that's what you saw is kind of what
they get told to do so they can stay safe. And, you know, the officer said this, it's
not the first situation that they've had like this, but it was the worst. You know, there
were a number of times before then that they could see the fear in the body
language and stuff like that and it makes it harder for them to do their
jobs. I would I would I would flat-out say just start the conversation with
it's it's okay we're not like American cops. They'll understand what you mean
and I would I would start there if your boss would allow you there's because of
because of the situation here I don't know that there's a subtle way to do it
you might have to be pretty direct with this or maybe consider treating the
person that you want to talk to, the way you would somebody who is nonverbal and
needs an intermediary, talk to somebody that's with them and just be like, hey
it's okay, just wondering what we can do to help. That way there's a
buffer, there's not a direct confrontation because I mean that's
that's what they're conditioned and you know that. I'm gonna read something from
the email. I am hopeful that with your network and reach at least some would in
preparation for a Danish vacation see and know that calling on the police in
Denmark does not come with the chance of a death sentence.
Like, they get it. They understand what it's like dealing with American cops.
They're just trying to figure out how to make the situation more, more at ease
there. So if you have suggestions on what to say or something that might make you
feel more comfortable, put it down below. Don't read it. And to American cops,
please understand, the behavior exhibited by a large portion of
American law enforcement is so out of line with the rest of the world that you
have people in other countries, law enforcement in other countries, trying to
figure out how to defuse the situation that you have created.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}