---
title: Let's talk about New Haven and a settlement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6XnL22QYX74) |
| Published | 2023/06/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- In June 2022, Randy Cox was paralyzed from the chest down after being thrown forward in a police transport van due to sudden braking.
- The settlement for Randy Cox's case in New Haven, Connecticut is $45 million, the largest police misconduct settlement in U.S. history.
- The city's insurance only covers $30 million of the settlement, leaving the city to pay the remaining $15 million out of pocket.
- Smaller settlements have not effectively changed law enforcement behavior, leading to larger settlements to send a message.
- The five officers involved in Randy Cox's case have been charged with reckless endangerment and cruelty to persons, but have not gone to trial yet.
- Some officers have left the force, been fired, or retired following the incident.
- Attorney Ben Crump, known for handling cases of police misconduct, represented Randy Cox in this case.
- The magnitude of this settlement and its implications are likely to set a standard for future cases involving police misconduct.

### Quotes

- "A $45 million settlement, again, largest reported police misconduct settlement in U.S. history."
- "Smaller settlements don't alter law enforcement behavior; they view it as a cost of doing business."
- "The city's insurance only covers $30 million of the settlement."

### Oneliner

In June 2022, Randy Cox was paralyzed in a police van leading to a $45 million settlement, the largest police misconduct settlement in U.S. history, revealing systemic issues in law enforcement accountability.

### Audience

Legal advocates, activists

### On-the-ground actions from transcript

- Contact legal advocates for support (suggested)
- Stay informed about cases of police misconduct (exemplified)

### Whats missing in summary

Detailed information on the specific changes needed in law enforcement practices to prevent similar incidents in the future.

### Tags

#PoliceMisconduct #Settlement #Accountability #LawEnforcement #BenCrump


## Transcript
Well howdy there internet people, it's Bo again. So today we are going to talk about New Haven,
Connecticut, a case that is going on there, a settlement that has occurred there, and why
it's interesting for two very different reasons. Okay, so what happened back in June of 2022?
A man named Randy Cox was in the back of a van. He was cuffed. It was a police
transport van. The van slammed on brakes. He was thrown forward into the divider.
It paralyzed him from the chest down. The officers mocked him, said he was faking,
drug him out of the vehicle, so on and so forth. Okay, so a settlement was
reached. It's $45 million. Reporting is saying this is the largest police
misconduct settlement in US history. That in and of itself is interesting and
noteworthy, but there's also something else about it. 45 million dollars, but
the city's insurance looks like it only covers 30 of it. The city is going to
have to come out of pocket on this one. That's kind of a newer development. As
these settlements occur more and more often, they're going to get larger and
larger because, well, people are realizing that these smaller settlements
don't send a message. They don't actually alter the behavior of law enforcement,
they don't offer to alter the standards, it just continues. They view it as a
cost of doing business. Once these settlements start getting well above
the amount that the city's insurance covers,
there's likely to be some changes there.
Now, the five officers involved
have been charged. A number of them have
already left the force. Some have been fired, some have retired,
but they have been charged with reckless endangerment
and cruelty to persons. They have not gone to trial for that
at this point in time. They have pled not guilty.
So, that part of it will be resolved later.
But a $45 million settlement, again, largest reported police misconduct settlement in U.S.
history, and it exceeds the city's insurance by about 50%.
I would imagine that this is going to be a case that gets referenced a lot in the future.
And given the fact that the attorney was Ben Crump, who we have talked about on the channel
before, I would imagine that this is going to get a lot of press and this is going to
be kind of a standard now that they try to reach.
you're not familiar with Crump, if you ever are hurt by law enforcement, probably
somebody to contact. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}