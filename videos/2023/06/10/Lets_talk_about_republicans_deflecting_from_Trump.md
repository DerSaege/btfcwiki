---
title: Let's talk about republicans deflecting from Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ixbvVc85akU) |
| Published | 2023/06/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Party doubling down on false claims about Biden indicting Trump, hoping to manipulate their base and independents.
- Holding up unverified information as proof, hoping to mislead voters.
- Republicans using deflection and claiming a witch hunt to defend Trump despite incriminating evidence leaking out.
- Predicts Trump's trial will be widely covered and evidence will continuously pour out, making their lies unsustainable.
- Republicans lying to their base about Biden securing Trump's nomination through indictment.
- Implying that Republican politicians prioritize deception over truth and integrity.
- Suggests that individuals close to Trump are likely cooperating with federal authorities.
- Speculates that Republican tactics of consistent lying may not work this time with independent voters.
- Expresses uncertainty about why Republicans continue to lie, possibly out of habit and past success in deceiving their base.

### Quotes

- "They're shaking their keys. Oh baby, here, here baby, look at this, look at this."
- "The Republican Party appears to be doubling down with the president, with the former president, with Trump."
- "Just consistently lie to your base and, well, they'll believe you."

### Oneliner

Republican Party doubling down on false claims, using deflection and lies to defend Trump amidst incriminating evidence leakage.

### Audience

Voters

### On-the-ground actions from transcript

- Challenge misinformation spread by politicians (implied)
- Stay informed and fact-check claims made by political figures (implied)
- Encourage others to critically analyze information presented by political parties (implied)

### Whats missing in summary

Insight into the potential consequences of the Republican Party's continued deception and manipulation of voters.

### Tags

#RepublicanParty #Trump #Deception #Manipulation #PoliticalLies


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're going to talk about Trump
and the Republican Party and shaking your keys
and the Republican Party probably making a huge mistake.
So a whole bunch of questions have come in
basically saying, hey, why is the Republican Party
doubling down on this?
And why are they saying that Biden did it,
Biden, you know, Biden indicted it
because they expect not just their base
but the moderates they need to win elections, the independents,
to be
as easily manipulated
as their core base. They're shaking their keys.
Oh baby, here, here baby, look at this, look at this.
No, you're not about to get a shot, just look here, look at my keys.
That's what's going on. Most of them
are lawyers. They understand that Biden didn't indict
anybody. A grand jury did. A grand jury
indicted Trump, not Biden. They know that.
They know that the form
that they're holding up as proof, they know that the whole purpose of that
form
is to collect information that
isn't verified. They know that. They're hoping
that independents, moderates, and their base
can be misled, can be tricked.
They're hoping that if they shake their keys hard enough,
people will believe it's a witch hunt. Even
after they hear a sentence like this,
pages long, look, wait a minute, let's see here.
I just found, isn't that amazing?
This totally wins my case, you know, except it is like highly confidential.
Secret.
This is secret information.
Look, look at this.
This is secret information.
Look, look at this.
This was in 2021.
That's Trump.
Look I'm not a lawyer.
I didn't even stay at a Holiday Inn Express last night.
But that certainly sounds like willful retention and dissemination to me.
But they think if they shake their keys enough, if they throw up enough deflection, that the
average voter will both side something that doesn't have two sides.
Republican Party, large segments of it, appears to be doubling down with the
president, with the former president, with Trump. And, you know, we're going to defend
him, we're going to use this to show weaponization of law enforcement and so
on and so forth.
Yeah, I mean that all sounds great until the evidence shows up. Just the stuff
that is leaking is just incredibly incriminating. So what you're going to have is you're going
to have sound bites where prominent Republicans are saying, Trump's innocent. He didn't do
anything wrong. This is a witch hunt, so on and so forth. And then the evidence is going
come out. Make no mistake about it, this is going to be the most widely covered
trial in American history. Everybody is going to know every piece of evidence
that comes out. There's no saying that, don't worry, I have the evidence, you know,
about the election. You'll get it in two weeks. That's not going to fly here
because the evidence is going to be continuously pouring out and again I'm
not a lawyer but this looks really bad and that's not even the worst of it. The
Republican Party as they sit there and say oh he guaranteed Trump the
nomination Biden by indicting him even though Biden doesn't indict anybody and
most of these people are lawyers and they know that they're just lying, they're
saying that Biden guaranteed him the nomination.
Yeah that says more about the Republican Party than it does Biden and it says
more about Republican politicians who aren't actually willing to tell their
base the truth.
The stuff that is coming out appears to be very incriminating.
There is also a very high likelihood that there are people who are within Trump's immediate
circle who are cooperating with the feds and have been cooperating with the feds
for quite some time. This isn't something they're going to be able to
bluff their way out of. The independent voters that they need to win, they're
going to see right through this. So why are they doing it? I don't know, force of
habit because it worked so far for the last six years.
Just consistently lie to your base and, well, they'll believe you.
Repeat it over and over again.
I don't think it's going to work this time.
Anyway, it's just a thought.
you'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}