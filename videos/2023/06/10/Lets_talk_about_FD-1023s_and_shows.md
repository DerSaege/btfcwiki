---
title: Let's talk about FD-1023s and shows....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-Jw0TxLHS0A) |
| Published | 2023/06/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the Republican Party's use of an FD 1023 form from the FBI to push allegations against Biden.
- Clarifies that the FD 1023 is for unverified reporting from a confidential source, not proof of truth.
- Illustrates with a hypothetical scenario involving Marjorie Taylor Greene to explain the nature of the form.
- Addresses the repetition of allegations regarding Ukraine and Hunter Biden.
- Mentions Bill Barr's dismissal of the information as garbage.
- Suggests that the FBI should release further information to counter politicization by the Republican Party.
- Speculates on the unlikely validity of the claims on the FD 1023 form.
- Concludes by questioning why the Trump administration did not take action if the allegations were true.

### Quotes

- "They know that the information is unvetted, unverified, but they're trying to trick their base."
- "It's more like taking notes of a conversation."
- "They're going to lie to their base."
- "If there was any behavior that was wrong, it would have been done by the Trump administration in how it was handled."
- "I find it incredibly unlikely that there is going to be any evidence to back up the claims on this form."

### Oneliner

Beau explains Republican manipulation using an unverified FBI form to push baseless allegations against Biden, calling for transparency to combat politicization, casting doubt on the claims' validity.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to demand transparency from the FBI in releasing further information (suggested).
- Share this analysis to debunk baseless allegations and encourage critical thinking within your community (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of how the Republican Party is using misleading tactics with unverified information to deceive their base and cast doubt on the Biden administration, urging for transparency to counter political manipulation.

### Tags

#RepublicanParty #FBI #BaselessAllegations #Transparency #PoliticalManipulation


## Transcript
Well, howdy there Internet people, it's Bo again. So today we are going to talk
about the latest Republican Scooby-Doo mystery.
Something to entertain children.
And we'll go over what it means because it is
exactly as predicted. Okay.
So the Republican Party got their hands on an FD 1023.
It's a form the FBI uses.
And this, the contents of it suggest that Biden engaged
in like some pay for play stuff
when it comes to foreign policy decisions, okay.
So let's start with this.
What is an FD 1023?
The form that they're holding up
as proof of their allegations.
It is, and I quote,
to record unverified reporting
from a confidential human source.
That's what it's used for.
Unverified being the key term.
When this subject first came up,
I said that they were specifically looking for this form
because they knew the information on it was wrong.
But it has the FBI logo on it
and they can hold it up as proof,
which is exactly what they're doing.
They know that the information is unvetted, unverified,
but they're trying to trick their base. To put this into clearer perspective, if
you were a confidential source for the FBI, you call up the FBI, you say, hey,
Marjorie Taylor Greene met with me. She gave me $15,000 of campaign money
illegally, for me to drive out to Area 51 and ask them about Jewish space lasers
and she has an alien in her basement. They would write that information on an
FD-1023 without ever looking into it because that's not what the form is for.
The form is to record the information presented to them by a source, not
say that it's real. It's more like taking notes of a conversation. That's what
they have. What is the allegation? It's the Ukraine stuff again. It's the Ukraine
stuff. Stuff that has been gone over time and time again and I have a video on it
if you want to watch. I think it's called like Journalism 101 and the Biden
Ukraine timeline or something like that. What you find out if you look into this
is that sure it looks like Hunter Biden did something shady, something that I
would personally consider unethical leasing out his name like that, but
nothing illegal. It's also worth noting that as far as I can tell, the report,
the FD-1023 that the Republican Party got their hands on, it kind of says that
a source knows somebody who made this claim about the pay-for-play, but the
proof? Well, they don't know where it is. They can't find that, of course. My
understanding is that this is information that when they looked at it, it was so
let's just call it questionable, that even Bill Barr was like, this is garbage.
So, what are they going to do with it?
They're going to hold it up, they're going to lie to their base.
They're going to say it's proof that the Biden administration did something wrong.
They're going to try to rehash something from, at this point, a decade ago, and try to cast
doubt as a distraction because dear leader is in legal trouble. That's what
this is. When we talked about this, I want to say a month ago, I said that the FBI
should absolutely release the form and then release the determinations, the
memos that come after the form. Like once the information is checked out, they
should release those as well. As of yet, they have not done that. I kind of hope
they're holding them in reserve so the Republican Party goes out, makes all of
the claims that they are currently making, and then they release them. Because
I have a feeling that the FBI is getting tired of the Republican Party attempting
to use them in a politicized manner and casting them in the light that they are.
bar. It's not hard for an entity like that to make fools of people. Okay, so that's your
most likely scenario. That's everything that we know. What if it's true? Well, then I would
wonder why Barr, you know, Trump's guy, didn't do anything about it. If there
was any behavior that was wrong, it would have been done by the Trump
administration in how it was handled because they were the people that were
over it. I find it incredibly unlikely that there is going to be any evidence
to back up the claims on this form. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}