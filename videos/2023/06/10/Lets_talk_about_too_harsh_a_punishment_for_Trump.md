---
title: Let's talk about too harsh a punishment for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=X2egY7JKohY) |
| Published | 2023/06/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Trump and Republicans' perspective on his potential sentence.
- Exploring whether the punishment fits the crime.
- Clarifying misconceptions about Trump facing a 10-year sentence.
- Expressing personal views on long prison sentences.
- Advocating for alternative rehabilitation programs over extended prison terms.
- Criticizing the U.S. system for excessive incarceration.
- Challenging the belief that people cannot change.
- Pointing out the contradiction in advocating for 10 years for damaging symbols versus damaging the nation.
- Encouraging ideological consistency and introspection.
- Acknowledging the likelihood of Trump receiving special treatment due to his status.
- Posing a question on the fairness of potential leniency towards Trump.

### Quotes

- "I don't believe that long prison sentences are just."
- "I think those [rehabilitation programs] would be far more effective and far more just."
- "Develop some ideological consistency here."
- "He is a rich old white guy and he will probably be extended every courtesy imaginable."
- "Do you think that's just?"

### Oneliner

Beau questions the fairness of long prison sentences, advocates for rehabilitation programs, and challenges ideological consistency around Trump's potential leniency.

### Audience

Republicans, Criminal Justice Reform Advocates

### On-the-ground actions from transcript

- Advocate for rehabilitation programs over long prison sentences (suggested)
- Challenge inconsistencies in beliefs regarding sentencing (implied)

### Whats missing in summary

Insights on the implications of ideological inconsistencies and the need for a fairer approach to sentencing in the criminal justice system.

### Tags

#Trump #Republicans #CriminalJusticeReform #PrisonSentences #Rehabilitation #IdeologicalConsistency


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Trump
and the way Republicans are looking at his potential sentence
and whether or not the punishment fits the crime.
It's a question that comes around a whole lot
but normally it's the other side of the political spectrum asking it
and now that question is being asked.
I have a message.
Bo, I'm an independent but lean Republican normally.
I've had people tell me Trump is going to get 10 years
for his crime.
I'm just wondering if you feel that's just.
It just seems like a long time to me.
It seems unreasonable.
So you have a number of questions in here.
Let's start with this, and I feel
like this is something I'm going to be saying a lot over
the next few months.
Trump is not looking at 10 years.
that's the max sentence, it's the federal system,
there's points and charts. He's not looking at anywhere near that
based on everything that we know so far.
That's not how it works. But,
that doesn't actually answer the question. The real question here at the heart of it
is do I feel that long prison sentences are just?
No. No, I don't, actually.
and not just for rich white guys. I don't think they're just in most
circumstances. The United States has a real issue with locking people up and
throwing away the key when they don't necessarily need to. It's my belief that
they don't work, that long prison sentences don't work, and that in the
absence of long prison sentences, we would have other programs that move
to rehabilitate, that alter the standing of that person. And I think those
would be far more effective and far more just. Sure there are cases when somebody
is a genuine continuing danger to society, that separation from society
make sense, but those cases to me are few and far between.
That's not the bulk of people.
So no, I don't
believe that long prison sentences
are just. There's a whole bunch of people doing a whole bunch of time that I don't
really think they need to.
But here's the thing, that's
me, that's my ideal world, that's the world
that conservatives, Republicans, generally say is impossible.
That's not how it works. You can't do that.
People don't change. Blah, blah, blah.
It's the world that is mocked by Republicans, but suddenly they have a
pretty keen interest in it.
Let's talk about
not my ideal world.
Let's talk about what exists and what was cheered by the people who are saying
that 10 years might be too much today for Trump. I want to read you something.
I have authorized the federal government to arrest anyone who vandalizes or
destroys any monument, statue, or other federal property in the US with up to 10
years in prison. Damaging a symbol, a statue, a monument, damaging a symbol of
the nation is 10 years and this was cheered, that was Trump. Ten years for
damaging a symbol of the nation. I would assume that a max of 10 years for
actually damaging the nation would fit in line with those who believe that this
was just. It's one of those things that occurs more often than we'd like to
admit. You look down on people who are in various situations, you kick down on
them and you say yeah that's how it should be until it's you or yours who
are in that situation. Trump himself advocated for 10 years for damaging a
symbol of the nation. The fact that he might be looking at 10 years for
actually damaging the nation. It's hard to see how Republicans would feel that
10 years for a symbol is just but 10 years for the nation isn't. And I'm not
saying this is a gotcha. I'm saying this is hopefully an opportunity for a little
bit of introspection, develop some ideological consistency here. I don't
think that Trump should go to prison for 10 years over this. I really don't, but
that's not going to happen because in addition to the points thing, please keep
in mind he is a rich old white guy and he will probably be extended every
courtesy imaginable. Now my question is do you think that's just? Anyway it's
If it's just a thought, you'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}