---
title: Let's talk about fact-checking the Trump and Meadows news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4gEvi2CJ07E) |
| Published | 2023/06/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing fact-checking on breaking news about Trump and Meadows circulating on social media.
- Meadows is rumored to have flipped and cooperating with an immunity deal, while Trump is speculated to be indicted under section 793 and facing 10 years in prison.
- The information circulating is not entirely accurate and largely sourced from an article by The Independent.
- Meadows' lawyer denies the plea deal but does not address the immunity aspect.
- If Trump is indicted, it is likely to remain sealed for a while, and the reporting indicates the case may be in the end game with Meadows talking to federal authorities.
- The article details may be accurate, but there is an immediacy placed on the information that may not be true, leading to potential false alarms.
- Multiple grand juries are looking at the case, seeking an indictment, but there is no guarantee of immediate confirmation or coverage.
- Beau advises everyone to calm down, let the legal process unfold, and not get anxious about the outcome.

### Quotes

- "Everybody just calm down."
- "Just relax and let this take its course."
- "If it happens, it's going to happen whether or not you're anxious about it or not."

### Oneliner

Beau provides fact-checking on the Trump and Meadows news circulating on social media, urging everyone to calm down and let the legal process unfold.

### Audience

Interested Observers

### On-the-ground actions from transcript

- Stay informed on accurate news sources (suggested)
- Avoid spreading unverified information online (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the inaccuracies surrounding the Trump and Meadows news circulating on social media, reminding people to stay calm and let the legal process progress.

### Tags

#Trump #Meadows #FactChecking #LegalProcess #SocialMedia


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about some breaking news.
And we are going to kind of provide some fact checking
because what has hit social media
is not all necessarily entirely accurate.
It's about Trump and Meadows.
OK, so let's start with what is,
What can you find all over social media right now?
Before we get into this, please understand,
not all of this is necessarily accurate.
Some of it we know already it's not.
Okay, but what you will see is Meadows has flipped.
Meadows flipped, and he's cooperating.
He has an immunity deal, he is going to plea
to some lesser offenses and Trump is going to be indicted as early as tomorrow under
section 793.
And Trump is going to get 10 years or he's looking at 10 years.
This is the, these are the key points in most social media posts about this, a lot of this
is not accurate. Most of this
is coming directly from an article
by the Independent and
most of the accurate information is actually in the article at this point.
Okay, so when it comes to Meadows,
according to the lawyer,
according to Meadows' lawyer, there is no
Pleadill. Now that part was denied. He said that it was a complete something I have in my pasture.
What I do find interesting is that while the idea that he was going to plead to crimes was denied,
I didn't see anything denying the immunity aspect of it. That may mean absolutely nothing,
nothing, but it's worth noting. Okay, Trump's going to be indicted as early as
tomorrow. Maybe, maybe, but even if he is, please understand it is likely to remain
under seal for at least a little bit. They're gonna use section 793. Yeah, that
tracks, that tracks. Longtime viewers of this channel will know why, because that
particular section, it relies on national defense information, not classified
information, so that makes sense. He's gonna get 10 years? No, no. Don't even
think that he's gonna get 10 years. That's the max. It's the federal system.
There's points and charts. His sentence will be determined later, assuming he's
convicted. There will be points, like an offense level in criminal history, and
that's how it works. Those max sentences that you see, just never pay attention to
those. Okay, so the overall tone of the reporting is that it's in the end game
and that Meadows is talking to the feds. Yeah, I mean all that tracks but this
isn't new. The reason this just went out everywhere on social media is the
article by The Independent and it just kind of caught and a lot of
pieces of information, critical pieces of information, didn't get transferred from
article to the social media posts. Now I think that the denial from Meadow's lawyer, I think that
might have came after publication and so the article was updated, I'm not sure. But there's
probably going to be a number of false alarms because most people can tell that it's in the
in game, that the charging decision is imminent, and there's a lot of reporters that are going
to want to be first.
So you're probably going to have something like this happen again.
When it comes to this case, there are so many eyes on it.
Everybody's waiting.
Please understand that if the former president is indicted, it's going to get a lot of immediate
confirmation.
It's going to get a lot of coverage all at once from a bunch of different outlets.
A lot of what is detailed in that article seems like it's probably accurate.
But there's an immediacy that is being kind of placed on the information that may not
be true.
So everybody just calm down.
I know there's a lot of people who have been waiting for this.
If it happens, it's going to happen whether or not you're anxious about it or not.
So just kind of chill out and let them do their thing because at this point you have
multiple grand juries that are looking at it in a way that definitely indicates they
are trying to seek an indictment.
The idea that we're going to find out about that tomorrow, I mean sure it could happen,
but there's no guarantee of that.
Even if an indictment came down tomorrow, we might not know it.
So just relax and let this take its course.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}