---
title: Let's talk about Newsom's Constitutional amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DBAiqZM8CSA) |
| Published | 2023/06/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor Newsom from California proposed a constitutional amendment focusing on firearms regulations at the national level.
- The proposed amendment includes raising the age to purchase a firearm to 21, universal background checks, creating a waiting period, and instituting an assault weapons ban.
- To become part of the U.S. Constitution, the amendment needs to pass a two-thirds vote in the House and Senate and be ratified by three-fourths of the states.
- The inclusion of an assault weapons ban is a significant barrier to the amendment's progress, especially at the national level.
- States have the authority to implement assault weapons bans individually, and 10 states have already done so.
- Despite the potential for a constitutional convention called by state legislatures, none of the amendments to the U.S. Constitution have taken that route.
- Beau speculates that Governor Newsom's proposal may be a strategic move to boost his presidential candidacy in 2028 when demographics might be more favorable towards such regulations.
- Newsom's inclusion of the assault weapons ban in the proposal may hinder its success, as it faces significant opposition.
- Beau suggests that a version of the amendment without the assault weapons ban might have had a better chance of progressing through the legislative process.
- Changing the U.S. Constitution is intentionally challenging, and the difficulty in amending it serves a purpose in maintaining stability and consistency in governance.

### Quotes

- "It's supposed to be hard to change the US Constitution."
- "Realistically, if this amendment had just the first three elements… that might have a chance."
- "Newsome, he's not an unintelligent person. He knows this, so why is he doing it?"
- "The odds of this going anywhere now are slim to none."
- "Y'all have a good day."

### Oneliner

Governor Newsom's proposed constitutional amendment on firearms faces hurdles due to including an assault weapons ban, hindering its potential progress towards becoming part of the U.S. Constitution.

### Audience

Legislators, Activists, Voters

### On-the-ground actions from transcript

- Advocate for firearms regulations at the state level (implied)
- Stay informed about proposed amendments and their potential impact on gun laws (implied)

### Whats missing in summary

Further insights on Governor Newsom's potential motivations and strategies behind proposing the constitutional amendment.

### Tags

#GovernorNewsom #ConstitutionalAmendment #FirearmsRegulations #AssaultWeaponsBan #LegislativeProcess


## Transcript
Well howdy there internet people, it's Bo again. So today we are going to talk about Governor
Newsom out there in California and the proposal for a constitutional amendment. We will talk about
what's in the amendment, we'll talk about the process that this would have to go through to
make it and become part of the U.S. Constitution, and we'll talk about why he might have proposed
it in the way that he did. Okay, so if you have no idea what I'm talking about, the Governor of
of California is proposing an amendment to the U.S. Constitution.
It has four components.
The first part is to raise the age to purchase a firearm to 21.
The second is to institute universal background checks.
The third is to create a reasonable waiting period.
The fourth is to institute an assault weapons ban.
This would be at the national level.
What does it take for this to actually become part of the U.S. Constitution?
Step one, it has to pass a two-thirds vote in the House and Senate.
Do you believe that two-thirds of the current House would vote for this?
Odds are this isn't going anywhere, but let's say that it does.
say, for whatever reason, it makes it through that part. What happens next? It
has to be ratified by three-fourths of the states, okay? The assault weapons ban
portion of this is what is really just stopping it from going anywhere. When you
are talking about at the federal level, you're not going to get Republicans to
cross the aisle on that. When you're talking about at the state level, when it
comes to the ratification process,
please understand that states can institute
an assault weapons ban.
10 have.
10.
How many do you need to ratify this?
30,
eight?
It's not going anywhere.
I know somebody's gonna say there's another method
using a constitutional convention,
if it's called for by the state legislatures.
Yes, that is true.
I would point out that out of all of the amendments
to the U.S. Constitution,
None have actually gone that route.
It's way more complicated, and it would be even harder
for this to move forward.
Realistically, if this amendment had just the first three
elements, raising age to 21, universal background checks,
and a reasonable waiting period, but the term reasonable
was actually defined, say, however many days,
that might have a chance.
It's the assault weapons ban
that is going to stop this from going anywhere.
It's just not gonna make it.
So, Newsome, he's not an unintelligent person.
He knows this, so why is he doing it?
My guess is that he is building his resume
to run for president in 2028.
that's that's my guess. By that point the demographics will have shifted in the
United States a little bit. The idea of an assault weapons ban at that point
that might be a little bit more tenable. It will certainly be more tenable than
it is now because those 12 year olds that are dealing with drills at their
school, well they'll be 18 then. The odds of this going anywhere now are slim to
none. I mean I would say none but there's always a chance, right? The odds of it
making it through everything it has to to actually become part of the US
Constitution. It's just not, it's not a realistic possibility, but please
remember that's actually a good thing. It's supposed to be hard to change the
US Constitution. I do believe that if he was serious about this and wanting to
actually get it to go somewhere, this would have just just incorporated the
the first three elements, because that probably would have gone somewhere. I
don't know that it would have made it through the whole process, but it
wouldn't have been doomed from the start, and this is. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}