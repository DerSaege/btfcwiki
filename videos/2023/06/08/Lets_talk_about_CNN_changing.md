---
title: Let's talk about CNN changing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=tSMEwmxu42g) |
| Published | 2023/06/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Leadership changes at CNN, with Chris Lick, chair and CEO, out.
- Departure due to plunging ratings, unhappy staff, lack of trust among viewers, and negative press.
- Trunk Town Hall incident believed to have initiated the changes.
- CNN has been a liberal mainstay but is trying to appeal to a right-wing audience.
- New leadership may continue the shift to the right but in a more subtle manner.
- Attempting to capture a polarized audience may lead to losing their original viewership.
- CNN might struggle to appeal to an audience not accustomed to news but to biased reporting.
- Trying to capture the Trump base could result in losing their long-standing audience.
- Warning about giving a platform to extreme individuals like the U-Haul truck incident perpetrator.
- Speculation on CNN's future direction and audience-capturing strategies.

### Quotes

- "CNN will be unable to capture that audience without losing the audience that it had for decades."
- "Trying to capture the Trump base could result in losing their long-standing audience."
- "It's worth remembering that the guy who had that U-Haul truck that went into the barriers up there in DC."
- "He wanted to gain access to the White House and announce the end to American democracy."
- "Y'all have a good day."

### Oneliner

Leadership changes at CNN prompt speculation on capturing a right-wing audience and the risks of alienating their existing viewership.

### Audience

Media Analysts, News Consumers

### On-the-ground actions from transcript

- Analyze and understand media shifts and biases (implied)
- Stay informed and critical of news sources (implied)
- Advocate for diverse and balanced news coverage (implied)

### Whats missing in summary

Insight into the potential consequences of media outlets shifting their political alignment and the challenges they face in balancing audiences.

### Tags

#CNN #Media #News #RightWing #AudienceCapture


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about some leadership changes
at CNN and what happened, what's likely to occur
in the future, and how CNN, if they chose,
could continue along the same route
if they still want to try to capture
that right-wing audience.
Okay, so what happened?
Chris Lick, chair and CEO over there at CNN, out, gone.
In the interim, there will be a leadership team
that will make decisions.
The departure is widely seen as being a result
of plunging ratings, a less than happy staff,
a perceived lack of trust among viewers,
a, well let's just call it an unflattering article from the Atlantic, and then of course
the Trunk Town Hall, which in my opinion is really what set all of this in motion. CNN,
it's been a liberal like mainstay for a long time, and they are trying to move to the right.
the perception. Whether or not the new leadership team is going to continue to
try that is it's debatable. My guess is they're going to still attempt to move
right, but they will try to be a little bit more subtle about it. I don't know
how effective they're going to be at that.
the alternative would be that they're going to still go down the same path and
try to capture an audience that wants nothing to do with them and maintain the
audience that they had. That's a really hard needle to thread when you are
talking about when you're talking about a very polarized country and a base
that CNN appears to be trying to capture that isn't actually accustomed to news
reporting. They're accustomed to people playing into their own biases. CNN will
be unable to capture that audience without losing the audience that it had
for decades. So that's where it sits now on the off chance that the leadership
team does want to continue to try to capture that Trump base. It's worth
remembering that the guy who had that U-Haul truck that went into the barriers
up there in DC. He wanted to gain access to the White House and announce the end
to American democracy. So maybe they could give him a town hall next. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}