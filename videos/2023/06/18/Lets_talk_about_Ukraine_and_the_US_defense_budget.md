---
title: Let's talk about Ukraine and the US defense budget....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mzvQ-rYly2k) |
| Published | 2023/06/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the inclusion of an earmark in the next year's defense budget to provide Ukraine with ATACMS, a missile box with longer range than what Ukraine currently has.
- Initially, the U.S. was hesitant to provide these to Ukraine due to concerns about their potential use.
- The U.S. appears more open to providing these missiles now, possibly due to a plan being worked out for their use.
- Providing Ukraine with these missiles could potentially help in any attempt to retake Crimea, closing the gap between Russian weapons and Ukrainian locations.
- Beau believes that providing these missile systems could save lives on both the Russian and Ukrainian sides by limiting frontline fighting.
- He doesn't think Ukraine will misuse these weapons and sees a strong need for them.
- There's ongoing debate and opposition due to concerns about providing Ukraine with weapons that could potentially hit within Russia.
- The missile system's significance lies in its range and how it could impact different areas, rather than being a wonder weapon.
- Ukraine is still strategizing and trying to draw Russia out in certain areas amidst the ongoing conflict.
- The missile system could play a significant role in various scenarios, but it's mainly about range and its impact.

### Quotes

- "It's about range. It is about range."
- "Providing Ukraine with these missiles could potentially help in any attempt to retake Crimea."
- "Providing these missile systems could save lives on both the Russian and Ukrainian sides."
- "There's ongoing debate and opposition due to concerns about providing Ukraine with weapons that could potentially hit within Russia."
- "It's not a wonder weapon, but it's really going to matter in a couple of different places."

### Oneliner

Beau explains the significance of providing Ukraine with longer-range missiles in the defense budget, potentially saving lives and impacting the conflict dynamics.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support organizations advocating for peaceful resolutions and diplomacy in international conflicts (implied)

### Whats missing in summary

The full transcript provides deeper insights into the geopolitical implications of providing Ukraine with longer-range missiles and the potential effects on the conflict dynamics.

### Tags

#Ukraine #US #DefenseBudget #Geopolitics #ConflictResolution


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Ukraine
and the US defense budget
and why a whole bunch of eyebrows got raised.
Some people were very excited,
some people were concerned what it means.
Okay, so in next year's defense budget,
what they're asking for,
There's an earmark to provide Ukraine with ATACMS.
My favorite way to say that acronym is ATACMS.
It's like a Stacom's, but there's a long-range missile
in the middle of it.
So what is it?
It's a missile box, basically.
How does it differ from what Ukraine currently has?
It has a longer range.
It has a longer range.
Initially, the United States was very hesitant
to provide these to Ukraine because they were concerned
about how Ukraine was going to use them.
That's the real reason.
It appears there has been a plan worked out for their use.
That's the only reason they would be included.
This is probably going to be all over the news by the time this video goes out.
The two questions that are going to be thrown out there, well, probably three questions.
First, what if Ukraine uses them in the wrong way?
I don't think they will because I think they will want them for the reason I suspect the
U.S. is willing to provide them. Is it a wonder weapon? Next question. No, no, it's
not a wonder weapon. Is it going to matter? Oh yeah, it will matter, particularly
in relation to any potential attempt to retake Crimea. That's when it would
really matter. There might be other uses along the way, but I think that in
in particular is why the U.S. is more open to providing them.
The short version here, without getting too far into it, is that currently Russia has
weapons that it can deploy from a safe distance.
It has weapons that are outside the range of Ukraine, and the locations where they're
deployed from, Ukraine really can't do anything about it right now. They would
have to close that gap. They would have to take the dirt so they can get closer
to it to use the weapon systems that they currently have. If they had the
system that the Pentagon is apparently earmarking to send, it's a whole lot
easier. That would save lives, okay, and to be clear about this, it would realistically,
it would save Russian and Ukrainian lives. I know that sounds weird, but it really would,
because it would limit the amount of fighting that has to be done on the, what would be called the
front lines. So my guess is that's what they're providing them for and I imagine
that's what Ukraine would use them for. I don't think they're going to misuse the
weapons systems. They have a really strong need for them and I don't feel
like they're gonna waste them. So because this is coming out I'm sure people are
gonna ask what about the counter-offensive? It's... I don't know that
it's truly started in earnest yet. There's a lot of fighting. There's a lot
going on as far as large territorial gains. Last time I checked was a couple
days ago they took converting it from kilometers to miles somewhere between
40 and 60 square miles. Ukraine has gotten back but again that information
is a couple days old to put that into a clearer perspective for people they took
two to three Manhattans back. That's what's going on. At this time they
haven't found a hole and I think that's what they're looking for.
They're still trying to kind of draw Russia out in certain areas. So when
people start talking about this missile system, that's what it's about. It's
about range. It is about range. It's not a wonder weapon, but it's really going to
matter in a couple of different places. So that's the breakdown on it.
There's going to be tons of debate over this because a whole lot of people are
very stuck into the mindset of we don't want to provide them anything that can
hit within Russia.
That's really going to be their reasoning.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}