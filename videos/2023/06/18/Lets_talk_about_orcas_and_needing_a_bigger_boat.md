---
date: 2023-08-06 06:16:14.637000+00:00
dateCreated: 2023-06-18 11:10:07.090000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about orcas and needing a bigger boat....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-qTAJ0TA988) |
| Published | 2023/06/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Orcas have been attacking, disabling, ramming, and in some cases, sinking boats off the coast of Europe.
- Experts are unsure why this behavior is happening, but some leading theories include the idea that orcas may be playing with boats because they enjoy the feeling of propellers running.
- Another theory involves a specific orca named White Gladys, who may be teaching other orcas to attack boats as revenge for a past incident.
- There's also a theory that the same orca incident involving nets or other altercations could have made orcas more defensive, trying to signal ships to leave.
- Researchers initially thought this behavior might be a passing fad, but with an increase in incidents, that theory seems less likely.
- The situation has garnered media coverage and inspired memes, with some suggesting that orcas are targeting yachts owned by wealthy individuals.
- Despite the entertainment value of the coverage, there is still no definitive answer as to why the orcas are behaving this way.
- Some have speculated that perhaps Mother Nature is reacting in some way to these events, though it remains uncertain.
- The incident raises questions about the motivations behind the orcas' actions and whether there is a deeper reason for their behavior.
- Ultimately, the mystery of why orcas are attacking boats remains unsolved, leaving room for speculation and debate.

### Quotes

- "Orcas have been attacking, disabling, ramming, and in some cases, sinking boats off the coast of Europe."
- "Why is it happening? Short answer is they don't know."
- "The situation has garnered media coverage and inspired memes."
- "The incident raises questions about the motivations behind the orcas' actions."
- "Ultimately, the mystery of why orcas are attacking boats remains unsolved."

### Oneliner

Orcas attacking boats off Europe's coast puzzles experts with theories ranging from playfulness to revenge, sparking media coverage and memes.

### Audience

Marine conservationists

### On-the-ground actions from transcript

- Monitor and report any incidents of orca interactions with boats to relevant authorities (implied)
- Support research and conservation efforts for marine ecosystems and wildlife (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the various theories surrounding why orcas have been attacking boats off the coast of Europe, offering insights into potential motivations and implications beyond surface-level explanations.

### Tags

#Orcas #MarineLife #Conservation #Europe #BoatAttacks


## Transcript
Well, howdy there, internet people. It's Beau again.
So today, we're going to talk about orcas.
You know, like free willy.
We're going to talk about orcas.
We've got a lot of questions over the last couple of days
about this and people looking for an answer.
OK, so if you don't know what's going on,
Orcas have been, quote, attacking boats.
This has actually been going on a while,
but it has picked up in frequency,
and it has picked up in media coverage.
So there are a lot of questions about why this is happening.
Now, by attacking, disabling, ramming, in some cases,
sinking boats. Now, why is it happening? Short answer is they don't know.
Looked into the the experts in the field, there are a couple of like leading theories,
but nobody's really certain as of yet. What they do know is that it is intentional
and seems to be confined to an area off of Europe at the moment. Okay, so what are
the theories? The first one is that they're playing. They're doing it
intentionally, but they're not actually attacking the boats. They're playing with
them. The leading theory in in that idea is that the orcas enjoy the the fill of
the propellers running. So on a sailboat or a boat that has its propellers off
they go up and hit the rudder. Kind of like saying hey turn this on I want to
play with it. And if it's a sailboat that doesn't have propellers, well, they get
frustrated and rip off the rudder or further damage the boat. Another theory
is that a specific orca, White Gladys, I think is her name, she had an incident of
some kind. She got tangled in a net or had some other altercation with a boat
that was bad and she is teaching the orcas that she leads to attack the
boats as revenge for this incident. A separate theory is that the exact same
thing happened, nets or some altercation involving the same orca, and that it's
not revenge, there's not that kind of motivation with it, but because of that
incident they are more defensive and are trying to tell the ship to leave. Those
Those are the three leading theories.
This has been going on a while.
Researchers in the past have suggested that it's a fad and it's just going to go away
and they're just, you know, trying out something new.
Given an uptick in the number of incidents, that doesn't seem to be the case.
I mean, maybe, you know, Mother Nature has come to arms.
I don't know.
It's an interesting development, but at this point there is no real answer, although the
coverage of it and the memes associated with it are definitely entertaining.
A lot of people have taken this to suggest that, well, the orcas are mad at the rich
people who own yachts as well.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
White JAWS movie shirt
## Easter Eggs on Shelf
Louisiana License Plate 007 0 981 was the plate taken out of the tiger shark in the movie Jaws.