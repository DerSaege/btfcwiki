---
title: Let's talk about Minneapolis, cops, and decrees....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XqJ4pBZ26dw) |
| Published | 2023/06/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A two-year probe in Minneapolis revealed systemic discrimination in the police department, including excessive force and unjustified use of lethal force.
- Marginalized groups in the city faced the brunt of the department's behavior, particularly Black Americans, Native Americans, and those with behavioral health issues.
- The report uncovered that the restraint used on George Floyd was applied hundreds of times, with over 40 instances where an arrest wasn't necessary.
- The city and police department have now entered into a consent decree, with federal oversight and mandated reforms.
- The success of consent decrees depends on the willingness of department leadership to remove problematic officers, which can have a greater impact on culture than policy changes.
- Addressing illegal behavior during the reviewed period varies, sometimes resulting in charges depending on the strength of the case.
- The fallout from George Floyd's death is ongoing, with the need for long-term corrections to address the systemic issues uncovered.

### Quotes

- "A two-year probe in Minneapolis revealed systemic discrimination in the police department."
- "Throwing out the bad apples as quickly as possible is what matters."
- "The fallout from George Floyd's death is ongoing."

### Oneliner

A two-year probe in Minneapolis uncovered systemic discrimination and excessive force within the police department, leading to a consent decree and ongoing efforts to address deep-rooted issues.

### Audience

Reform advocates, community members

### On-the-ground actions from transcript

- Join or support local organizations advocating for police reform (suggested)
- Stay informed about the progress of mandated reforms in the Minneapolis police department (exemplified)

### Whats missing in summary

Detailed examples and nuances from the full transcript can provide a deeper understanding of the systemic issues within the Minneapolis police department.

### Tags

#Minneapolis #PoliceReform #SystemicDiscrimination #ConsentDecree #GeorgeFloyd #CommunityAction


## Transcript
Well, howdy there, internet people. It's Bo again.
So, today we're going to talk about Minneapolis and
decrees and the report
and what it all means. So,
starting off the news, what occurred? A two-year probe
basically determined that the bad apple spoiled the bunch.
The feds were looking into
the police department there. It was, it started in the, in the wake of Floyd and
what happened there. They started looking into it. They chose a period the way
they normally do this. I want to say it was from January of 2016 to sometime in
2022. And they just reviewed everything. And they came to some conclusions, short
version of the conclusions is that basically everything that every single
marginalized group in that city said about the cops was true. If you want to
sum it up in a sentence, that's the sentence. Okay, so they found systemic
discrimination of racial minorities. They found excessive force. They found
unjustified use of lethal force. They found violations of constitutionally
protected speech. They found all kinds of things. It appears that the favorite
targets of the department there were black Americans, Native Americans, and
Americans with behavioral health issues. Those were the most affected by the
behavior of this department. One of the things that jumped out at me, and this is
to be clear, there's a lot of stuff in this report. So if this is a topic that
you're interested in, there's no way I can detail it all in a video like this.
either read the report or read like an AP summary of it or something like that.
But one of the things that really caught my eye was that during this period they
used the type of quote restraint they used on Floyd a couple hundred times.
That's bad. That's really bad. But it gets worse. In more than 40 of those cases, it was in a situation in which an
arrest was not required.  That's not law enforcement. That's just harming the citizens.
Okay, so what happens from here?
The city and the police department have entered into a consent decree.
What does this mean?
It means that the feds basically oversee everything now and will for years to come.
During this period, the feds will mandate reforms.
They will say, you have to meet these benchmarks by this point in time.
You have to do a whole bunch of different retraining.
You have to institute certain policies, so on and so forth.
The obvious question is, do consent decrees work?
And the answer, like many things, is it depends.
It depends.
It's a mixed bag.
Sometimes it's pretty effective.
Sometimes it's not.
I don't have any data to back up what I'm about to say, but on observation, it appears
that the deciding factor in whether or not this is actually something that works is the
willingness of the leadership in the department to say, hey, you don't work here anymore.
That's what alters the culture.
And that may, it may alter it more than the policy changes.
Again, the saying is that a few bad apples spoils the bunch.
Throwing out the bad apples as quickly as possible is what matters.
And when you have a situation where it is this widespread, it's not a few bad apples,
it's a whole orchard.
So another question that always comes up is what happens to those who engaged in illegal
behavior during the review period or a violated policy during the review period, the answer
there again is it depends.
Sometimes they're charged.
That does happen at times, depending on how much time has passed, whether or not there's
more evidence to go along.
And one thing that seems to be a deciding factor there is do they have a really strong
case on something and then a whole bunch of other issues with this officer.
And then in other cases, they don't pursue it.
It's made like in any...the decision is made the same way any prosecutorial decision is
made.
It really depends on the strength of it.
I have a feeling based on how widespread it appears from the report.
There will probably be some.
How much?
There's no way to know.
This is not the end of the fallout from George Floyd.
the beginning of the actual corrections or at least the attempt to make
corrections. You know, a lot of people want closure. That's not coming, not any
time soon because what happened there and certainly what was uncovered in this
probe is going to... it's going to have echoes for years to come. Anyway, it's
It's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}