---
title: Let's talk about California and Ospreys....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wluc2N1dgsA) |
| Published | 2023/06/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Military aircraft movement in California is causing concern in suburban areas.
- Residents are witnessing a buildup of aircraft, ground vehicles, Humvees, ambulances, and SWAT teams.
- This activity is due to the President's upcoming visit, not an invasion.
- People in areas frequented by presidents are familiar with this pre-visit preparation.
- Beau explains the importance of preserving resources for disaster response rather than allocating them for presidential visits.
- He underscores the normalcy of the situation and dismisses unwarranted concerns.
- The heightened security measures are typical before a presidential appearance.
- Beau addresses the potential confusion and alarm caused by the unfamiliar sight of military presence in California.
- He contrasts the current situation with the need to conserve resources for disasters.
- The activity observed is part of routine preparations for the President's visit and should not raise alarm.

### Quotes

- "The concern is unwarranted. It's not a big deal."
- "The one thing that is important to note about this..."
- "But everything you're seeing out there in California, totally normal, no big deal."
- "The context is that it's just normal pre-gaming for a presidential appearance."
- "Anyway, it's just a thought, y'all have a good day."

### Oneliner

Residents in California witness military activity as preparation for the President's visit, prompting Beau to address unwarranted concerns and stress the importance of conserving resources for disasters.

### Audience

California residents

### On-the-ground actions from transcript

- Stay informed about upcoming presidential visits in your area (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of the military activity in California and the reasons behind it, offering reassurance to concerned residents and educating on resource management during disasters.

### Tags

#California #MilitaryActivity #PresidentialVisit #ResourceManagement #CommunityConcern


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Ospreys in California.
And some news that is news, but is obviously going to be a little out of context by the
way it's being presented.
Okay, so a bunch of military aircraft are on the move in California right now.
Some of them are over suburban areas.
of activity is occurring. Got a message saying hey I saw this weird you know
conspiracy theory post on on Facebook and then I walked outside and actually
saw them. Is this you know something to worry about? Because the post said my
contact that sent this video is local military and says it is not a drill. Okay
it's not a drill you're right it's not a drill but it's also not something to
worry about it is a normal pre-gaming because the President of the United
States is coming there tomorrow that's what it is in addition to the aircraft
you will probably also see movement of ground vehicles you're probably gonna
see a lot of Humvees you may see eventually you will start to see a lot
of ambulances start to move, you will see SWAT teams start to stage, stuff like that.
It is normal, it is not a drill, but it's also nothing to be concerned about.
The Marines have not decided to invade California.
It's something that is occurring, it's just this time it's occurring in an area that I
I guess hasn't seen it before.
If you are, if you're somebody who lives in areas where presidents tend to visit,
you're probably accustomed to this.
You've seen it before.
You've seen the helicopters.
You've seen the Ospreys.
You've seen, you've seen all of the SUVs.
you've seen all of the pregame that starts to occur prior to a presidential
appearance. I guess this community isn't familiar with it and it has led to a lot
of concern. The concern is unwarranted. It's not a big deal. The one thing that
is important to note about this, when I talk about how it's good for presidents
not to visit disaster zones, for them not to show up after a hurricane, for them
not to show up immediately after an earthquake and stuff like that,
look at all of the vehicles you're seeing. Those are resources that get
expended for the presidential visit that if it's after a disaster could be used
to help mitigate the disaster and you're getting to see it in real time now
without the disaster. That's a lot of resources. That's a lot of help that
could be given. So when you have some natural disaster in your area, this is
why you don't want the governor, you don't want the president showing up. You
don't want them showing up for a photo op and diverting resources from people
who need it. But everything you're seeing out there in California, totally normal,
no big deal. I guess it's just maybe a slow newsweek in the conspiracy circles
and you saw it, you walked outside, you actually saw the bird and this is how
people start to believe them. The context is that it's just normal pre-gaming for a presidential appearance.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}