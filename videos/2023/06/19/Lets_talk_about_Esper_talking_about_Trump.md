---
title: Let's talk about Esper talking about Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Y96MO7xYrVY) |
| Published | 2023/06/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former Defense Secretary Esper emphasized the danger of Trump's actions in mishandling classified documents.
- Unauthorized, illegal, and dangerous were the key terms Esper used to describe the situation.
- Imagining the consequences of foreign agents accessing U.S. vulnerabilities was a focal point of Esper's concerns.
- The impact on America's military readiness and ability to execute attacks was underscored by Esper.
- The gravity of unauthorized disclosure of war plans and sensitive information was compared to historical scenarios like World War II.
- The potential for adversaries to exploit leaked information to develop countermeasures against U.S. plans was a significant worry.
- Beau pointed out the risk Trump's actions posed to national security and military operations.
- The necessity to prevent Trump from accessing classified material in the future was strongly advocated by Esper and Beau.
- Loyalty to an individual over the safety of troops and national security was criticized by Beau.
- Beau emphasized the serious implications of Trump's actions, urging viewers to prioritize country over personality or politics.

### Quotes

- "This man can never have access to classified material again."
- "He's lying to you."
- "Nobody who loves this country, nobody who values American supremacy the way that he claims he does,  "
- "The amount of lives that would be lost if these documents fell into opposition hands is immense."
- "Trump's already put U.S. troops at risk."

### Oneliner

Former Defense Secretary Esper and Beau stress the grave danger of Trump's mishandling of classified information, urging a focus on national security over personal loyalty or political affiliations.

### Audience

Concerned citizens, patriots.

### On-the-ground actions from transcript

- Advocate for strict measures preventing individuals like Trump from accessing classified information (implied).
- Prioritize national security over personal loyalty or political affiliations by engaging in informed discourse and decision-making (implied).
  
### Whats missing in summary

The full transcript provides a detailed analysis of the risks associated with mishandling classified information and urges viewers to prioritize national security over personal allegiances.

### Tags

#NationalSecurity #ClassifiedInformation #TrumpAdministration #PoliticalAccountability #RiskMitigation


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about
former Secretary of Defense Esper's statements on Trump
in his read.
And it's worth noting this is Trump's
former defense secretary.
Because he brought up something
and I think it's kind of important.
Right now people are focusing more on
the legal aspects of it, the nuances of the law.
looking at the letter of the law.
Esper really kind of leans into the spirit of the law
and why that part of this shouldn't be lost.
OK, so what did he say?
People have described him as a hoarder when it comes
to these type of documents.
But clearly, it was unauthorized, illegal,
and dangerous.
That's what he said, unauthorized, that's apparent, there's no arguing that at this
point.
Illegal, that's going to be decided in court, dangerous, this is the part we need to focus
on because it's the part that's getting lost.
He went on to say, imagine if a foreign agent, another country, were to discover documents
that outline America's vulnerabilities or the weaknesses of the United States military.
Think about how that could be exploited, how that could be used against us in a conflict,
how an enemy could develop countermeasures, things like that.
Or in the case of the most significant piece that was raised in the allegation about U.S.
plans to attack Iran, think about how that affects our readiness, our ability to prosecute
an attack. This is the part that can't be lost in this. You can argue whatever you
want. At this point, I don't actually understand what Trump's defense really
is. I'm not even sure that he really understands what the charges are, to be
honest, but you can argue that maybe he didn't really violate this law in some
way. How you're going to argue that I have no clue, but let's get to the facts.
Those documents were unsecured in an area that a whole bunch of people had
access to, and according to the allegations they contain SCI
information, SAP information, information related to the initial opening gambit of
an attack on a specific country. The war plans. War plans. I want you to think
about all the World War II movies you've ever seen. Those plans. Imagine what would
happen, what would have happened if the Germans had access to the Allied plans
for D-Day? Imagine what would happen right now if Russia actually knew what
was going on with the Ukrainian counter-offensive. Information is power
when you're talking about something like that and when you are talking about
that opening assault, that is where the US shines. That is where the US is set up
to basically destroy the opposition's conventional military capability. If Iran
actually had access to those plans and could look at them and say okay well we
can develop a countermeasure for this, we can do this, we can alter this. If
tensions get high what we need to do is move these items and replace them with
inflatables or something like that. The plans could be drawn up to defeat the
U.S. plan. It doesn't matter what you think of the former president. In this
regard, what you think about his politics, what you think about his policy, whether
you think he's a horrible person or you think he's a pretty swell guy, one thing
Everything is super clear from this incident.
This man can never have access to classified material again.
It's that simple.
If you don't agree with that statement, it means that you are putting your loyalty to
personality because it's not even party.
You're putting your loyalty to Trump, an establishment billionaire that does not care about you in
in any way shape or form. You're putting your loyalty to Him above the loyalty
you have to the people that you're going to send into harm's way. The information
that was contained in these documents, it's the stuff spy movies are made
about. It's the stuff that when it actually gets out into the wild through
somebody who is employed with an intelligence agency, it creates a task
force, it creates SWAT teams kicking in doors, it creates all kinds of things. But
there is a significant portion of the American public who's just like, well
it's okay because a rich dude did it. It's okay because, you know, he has an R
after his name. It's okay because he says he loves this country. Nobody who loves
this country, nobody who values American supremacy the way that he claims he does,
would ever put those documents at risk. He's lying to you. That's the thing
that's important. I know there's a whole bunch of like lefties and liberals
watching this right now and they're like yeah well I don't really care about that.
That's fine his supporters do and it's gonna be his supporters who put him back
in office or at least try to. The amount of lives that would be lost if these
documents fell into opposition hands is immense. Beyond that because the security
was so lax, the U.S. has to act like they fell into opposition hands. Which means
they have to move to a different plan. A plan that is going to avoid some of the
key strategic things that was in the one that Trump had. I'm willing to bet the
one presented to the then President of the United States was the best they had
to offer and now they can't use it. Trump's already put U.S. troops at
risk. It's already happened because that best plan, the one that was presented to
the president. It's worthless because he wanted to show some stuff off. Best case
scenario. You can't talk about wanting to make America great and continue to
support what's probably going to turn out to be the largest, most
widespread single security breach in American history. Esper, not my favorite
person, but refocusing on what would be caused by Trump's lackadaisical attitude
towards security, probably pretty important. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}