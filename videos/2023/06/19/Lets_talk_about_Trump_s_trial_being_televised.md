---
title: Let's talk about Trump's trial being televised....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bPbllVNFgRU) |
| Published | 2023/06/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Major outlets are pushing for Trump's trial to be televised as vital for American education, but their true motive is profit.
- Televising the trial will result in major outlets making significant money off sound bites and commentary.
- Despite potential benefits for some viewers, the trial being broadcast may lead to out-of-context clips favoring Trump, causing misinformation.
- Out-of-context statements will be made regardless of whether the trial is televised.
- Beau personally believes the televised trial is irrelevant and not vital to America's soul; the outcome of the trial is what truly matters.
- Advises not to panic if the special counsel appears to be giving Trump what he wants in early motions, as it may be part of a larger strategy to avoid delays.
- Special counsel's focus seems to be on moving the case forward swiftly due to its perceived ironclad nature.
- Beau suggests viewing the special counsel's actions as a positive sign and part of a strategy to prevent unnecessary delays for Trump.

### Quotes

- "Don't let them lie to you."
- "What matters to the soul of America is not whether or not this is televised."
- "It's probably part of a larger strategy to avoid giving Trump the delays that he wants."

### Oneliner

Major outlets push for Trump's trial to be televised for profit, but Beau believes the outcome, not the broadcast, truly matters.

### Audience

Media consumers

### On-the-ground actions from transcript

- Stay informed on the trial progress and outcomes (implied)
- Do not panic over early motions in the trial; view them as part of a strategic move (implied)

### Whats missing in summary

Detailed analysis and context on the potential consequences of televising Trump's trial.

### Tags

#Trump #TeleviseTrial #Media #Education #Profit


## Transcript
Well, howdy there, internet people. Let's go again.
So today we are going to talk about televising Trump's trial
and how important it is according to the major outlets.
Let's get real for one second.
The major outlets are portraying it as incredibly important
to the American people.
That way the American people can be educated and they can understand what's
going on and blah blah blah.
Most major outlets today, do you believe that they go out of their way to provide
context
to educate the American public?
Probably not, right?
That is not
going to be
a widespread view
that that's their purpose because it's not.
They're businesses.
They are businesses.
What's their purpose? The unbridled pursuit of profit.
Why do they want it televised? Because they're gonna make a dump truck full of
cash off of it.
That's the reason they're interested.
Don't let them lie to you.
They know that if that trial is televised, their hosts will have an
endless stream
of sound bites.
Tons of commentary.
They're gonna make a bunch of money.
Now, would it benefit some people
for the trial to be televised in real life?
know, not in the argument of the major outlets. Yeah, yeah, there are a bunch of
people who could stand to see the evidence that is likely to emerge.
Absolutely, but they're not going to watch it. They'll get out of context clips
that will show them that Trump's winning because that's what their preferred
news outlets are going to do. They're going to show them these clips that make it
look like Trump is winning. That could have bad consequences if Trump is then
found guilty. Think about the red wave, think about the election, think about all
the times that the major outlets that align with Trump misled the public and
And then when reality finally hit, they were upset.
The same thing could happen with Trump's case, in fact it's likely to.
At the same time, those out of context statements, they'll be made either way, they'll be made
either way.
If the trial is broadcast in its entirety, it will provide other people with the opportunity
to provide context.
But then you're back to that game of trying to defeat misinformation, and it's hard.
Me personally, I don't think it matters one way or another.
I don't think that it is something vital to the soul of America to have this televised.
I don't think that it's a thing where if it's not televised, it's going to be damaging.
I think it's irrelevant.
What matters to the soul of America is not whether or not this is televised.
It's not a televised trial.
It's the outcome of the trial.
That's what matters.
And that's something to keep in mind.
Now since we're on the subject of the trial, I do want to point something else out because
people have already sent in some some messages about some of the motions and stuff like that.
I'm going to suggest that it might be a really good idea to not panic if it seems like the special
counsel is just giving Trump everything that he wants in the early stages of these motions.
I wouldn't panic over that. I would view it as a good thing. If that's what starts
to occur, understand it's because Smith wants to get to trial. Why? Because by all
evidence that we've seen, this case is ironclad. What is Trump's preferred
legal strategy? Delay, delay, delay. Every time there is a motion put forward and
and the government tries to fight it, the special counsel's office tries to
fight it, it gives him a delay. So unless it
matters, he's probably going to give him whatever he
wants, because most of those motions are not
going to impact the trial, which is what Smith appears
to be focusing on. Again, we've only seen a few motions so
I don't know that it's early enough to say this. I'm just suggesting that if this starts to become
a pattern, I wouldn't worry. It's probably part of a larger strategy to avoid giving Trump the
delays that he wants.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}