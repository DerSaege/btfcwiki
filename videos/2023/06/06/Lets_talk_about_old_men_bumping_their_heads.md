---
title: Let's talk about old men bumping their heads....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=i-fBMEnj0NE) |
| Published | 2023/06/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People have been asking about an old man falling down, prompting a need to address the topic.
- The old man fell, bumped his head, hurt his ribs, and suffered a concussion, leading to a few days in the hospital under observation and almost six weeks off work.
- Despite not liking the man in question, Beau doesn't believe he should be removed from office for falling.
- Beau questions the focus on this incident, implying it's not a significant national priority.
- He suggests that right-wing outlets might be using this incident to distract from other issues.
- Beau believes that unless an injury directly impacts someone's duties, it shouldn't be a matter of national concern.
- He points out the disparity in news coverage between this incident and others, implying bias in reporting.
- Beau concludes by encouraging everyone to have a good day.

### Quotes

- "I don't think that McConnell needs to be removed. I mean, not for falling."
- "It almost seems like this is just kind of something that right-wing outlets would be running with to fill some time and maybe distract from other things that are going on with their candidates."
- "And if you didn't know the Senate minority leader was out for over a month because he fell and bumped his head, that probably says a whole lot about the news you consume if they didn't make a big deal out of that but they made a big deal about it with Biden."
- "Anyway, it's just a thought."
- "Y'all have a good day."

### Oneliner

Beau questions the focus on an old man's fall, suggesting it's not a significant national priority and might be used as a distraction tactic by right-wing outlets.

### Audience

News consumers

### On-the-ground actions from transcript

- Question biased news coverage (implied)
- Be critical of distracting tactics in reporting (implied)

### Whats missing in summary

Beau's tone and delivery nuances are missing in the summary.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about old men falling down,
I guess, because a whole bunch of people
have asked questions about it.
And it seems like a topic, even though I don't really
understand why people care.
But we'll go over it.
Here's one of the messages.
Why aren't you talking about that old man falling and then
bumping his head?
Figure you'd have lots to say about that.
Don't you think somebody in his state should be removed?
I mean, I don't know.
He fell, bumped his head, hurt his ribs, concussion.
Few days in the hospital under observation.
He was out for almost six weeks.
But, I mean, he's back now.
He seemed to do fine getting the debt ceiling
bill through over in the Senate.
I mean, I don't like the guy, but I don't think that McConnell needs to be removed.
I mean, not for falling.
I mean, I can think of a whole bunch of reasons I would like to see voters decide that he
shouldn't be in office.
But, I mean, I think a bump to the noggin might be like the least of the concerns about
him.
I mean, that is who you're talking about, right?
the person who fell and actually hurt themselves to the point where they
couldn't do their job for more than a month that that's got to be the person
that you're you're talking about needing to be removed because it seems really
unlikely that shortly after that happened that you would believe that
somebody who tripped over a sandbag or bumped their head getting out of a
helicopter needs to be removed. I mean I've tripped over sandbags. I've bumped
my head getting out of the helicopter. I mean I've bumped my head getting out of
the Jeep. It almost seems like this is just kind of something that right-wing
outlets would be running with to fill some time and maybe distract from other
things that are going on with their candidates. I mean, that's how it seems to me.
It doesn't seem like it would be like a high national priority. I mean, what
happens if one of them stubs their toe? Unless it actually impacts their
duties, I don't think it matters. And to be honest, even if it's, even if it
impacts their duties as long as it's short-term, like any other person. I don't think it should
be a matter of national conversation. And if you didn't know the Senate minority leader
was out for over a month because he fell and bumped his head, that probably says a whole
lot about the news you consume if they didn't make a big deal out of that but
they made a big deal about it with Biden it's probably a garbage outlet just
saying. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}