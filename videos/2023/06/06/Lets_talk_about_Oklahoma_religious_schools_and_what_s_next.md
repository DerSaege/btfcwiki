---
title: Let's talk about Oklahoma, religious schools, and what's next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ZEojR26aWrc) |
| Published | 2023/06/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The approval of an online religious charter school in Oklahoma by the Statewide Virtual Charter School Board.
- Governor's support for the school based on religious liberty, contrasting with the Oklahoma Attorney General's view of it being a violation of the state Constitution.
- Potential legal challenges at the state level due to state constitutions closely resembling the U.S. Constitution.
- Predictions that the program will likely be short-lived and face opposition either at the state or federal level.
- Criticism towards those claiming to defend the Constitution while being ignorant of its contents.

### Quotes

- "This is probably going to get ground down in the state courts because a whole lot of states have state constitutions that closely align with the U.S. Constitution."
- "This isn't going to fly, I wouldn't worry about it too much."

### Oneliner

Approval of an online religious charter school in Oklahoma sparks legal concerns as state officials clash over its constitutionality, likely facing swift opposition.

### Audience

Legal advocates, activists

### On-the-ground actions from transcript

- Challenge the approval of the online religious charter school through legal action (implied)
- Stay informed about the developments regarding this issue and support efforts to oppose it (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the approval of an online religious charter school in Oklahoma and the legal challenges it may face at both the state and federal levels.

### Tags

#Oklahoma #ReligiousCharterSchool #Constitutionality #LegalChallenge #StateVsFederal


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about Oklahoma
and the approval of an online school there
that has certainly caused some waves
and definitely is going to get some attention
even though it's probably gonna get more than it should.
Okay, so if you don't know what happened,
The Statewide Virtual Charter School Board of Oklahoma,
something like that, voted three to two
to approve an online religious charter school that's
like taxpayer funded.
Obviously, this prompted a whole lot of questions from people.
It's because most of the coverage is centering on what the governor said in response to this.
The governor's like, yes, this is a great moment for religious liberty and so on and
so forth and seems very encouraging and all of this.
It seems by that, that it has the support of the state.
People are wondering, well, are the feds going to step in?
What about the separation of church and state?
What's going to happen?
There's a lot of concern.
Rather than looking at what the governor said, I would look at what the attorney general
of Oklahoma said, which was, I'm going to paraphrase here, but this is a blatant violation
of the Constitution of Oklahoma, I don't even know that it's going to get to the federal
level.
This is probably going to get ground down in the state courts because a whole lot of
states have state constitutions that closely align with the U.S. Constitution.
My guess is that the Oklahoma Attorney General, who reportedly told the board before they
did this that it was against the Oklahoma State Constitution, is probably going to step
in, or is going to wait for the first suit to be filed and be like, we can't defend this.
So I don't think that this is going to be a long-lived program out there in Oklahoma.
probably going to be shot down pretty quickly. Rest assured, if it doesn't
happen at the state level, the feds will definitely step in through one of like a
dozen mechanisms they have. This is not going to, this is not something that is
going to continue. This will be struck down. It's just another example of those
people who market themselves as, you know, defenders of the Constitution, and people
who want to keep their oath, and people who really love the principles this country was
founded on, being completely unaware of what that document says, or what those principles
are.
This isn't going to fly, I wouldn't worry about it too much.
Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}