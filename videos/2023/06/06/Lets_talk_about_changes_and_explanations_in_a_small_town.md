---
title: Let's talk about changes and explanations in a small town....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=yrueEoLOxP8) |
| Published | 2023/06/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing change in a small town and managing obstacles that arise.
- A message about being part of the LGBTQ+ community and distancing from friends.
- Feeling terrified of confrontation in a right-wing rural town.
- Questioning the right decisions and seeking advice on handling the situation.
- Being accommodating and conflict avoidant, struggling to express discomfort.
- Exploring whether it's right to distance oneself and how to handle explanations.
- Emphasizing that those who are not accepting do not deserve an explanation.
- People are not entitled to know the reasons behind positive changes in your life.
- Small towns often resist change, making it challenging for individuals evolving.
- Encouraging personal growth and being true to oneself without needing approval.
- Advocating for surrounding oneself with supportive and accepting people.
- Asserting that explanations are only owed to those who truly care and support.
- Advising caution in revealing full explanations in small towns due to gossip.
- Urging individuals not to let anyone hinder their personal growth and progress.

### Quotes

- "Those who don't want you to change, those who wouldn't be accepting of that, they don't matter."
- "You don't owe them anything, nothing you have to worry about, only you and those people who are accepting."
- "Don't let anybody stand in that way."
- "If you don't look back on the person you were five years ago and cringe, you wasted five years."
- "People will change, they should change."

### Oneliner

Beau addresses managing change in a small town, advocating for personal growth amidst resistance and the importance of surrounding oneself with supportive individuals.

### Audience

Community members seeking guidance on navigating personal growth in challenging environments.

### On-the-ground actions from transcript

- Surround yourself with supportive and accepting individuals (implied).

### Whats missing in summary

Beau's insightful perspective on embracing personal growth and change while navigating resistance in a small town.

### Tags

#Change #Acceptance #PersonalGrowth #SmallTown #LGBTQ+ #Support


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about change
as a person in a small town.
We're going to talk about acceptance.
We're going to talk about explanations
and basically how to manage some of the obstacles
that arise when you are trying to change
places that are just notoriously static. We do this because I got a message. I was
wondering if you could give me some advice. Recently I realized that I am
part of the LGBTQ plus community and it has put many things in perspective for
me. I have a few friends that because of that discovery I no longer feel
comfortable hanging around with and I doubt I can bring them back from the
right-wing rabbit hole they fell into. So I have been quietly distancing myself
from them. I am terrified of a confrontation as I live in a small
right-wing rural town and any sort of explanation I fear might out me. I am
someone who is terrible at saying when I have a problem with something, extremely
accommodating and conflict avoidant. Am I making the right decisions here or is
Is there a better way to handle this?
Generally, people like to give the advice
of be direct as possible.
I mean, that only works for people who are direct.
You're flat out saying, that's not you.
What you are really asking about,
is it right to distance yourself?
And what do you do when it comes time for an explanation?
Anybody who would not be accepting
of positive changes in your life isn't
entitled to an explanation.
You do not have to explain your desire
to exist as the person you are supposed to be to anybody.
It's, and I know that's hard because if you are from a small town, you know that everybody
knows everything and people are consistent and when you change, people want to know why.
They're not entitled to it.
If they aren't going to accept you, they're not entitled to it and it doesn't just have
to go, it's not just this.
It's not just realizing that one day you may put a rainbow flag on your truck.
This is true of any changes that you make like that in small towns.
And the reason it's so pronounced is because people don't change.
A lot of people don't change.
I had to deal with this on a much smaller level.
I left and was gone for years and when I came back, I had a number of people say, man, you've
changed, yeah dude, it's been five years, why haven't you changed?
And for me, it was leaving and coming back and one of these conversations took place
the same pool table in the same place. Like it was the last time I saw the
person and the first time I saw them after years in the same location because
nothing had changed. You should change as a person. You should strive to be who you
are. Those people who don't want you to do that, those people who wouldn't be
accepting of that. They don't matter. I mean, they really don't. They're not
entitled to an explanation from you. Your chosen route is to distance yourself
from them? Yeah. You don't have to tell them why. Hey, do you want to go do this?
Nah, I'm not up to it. Eventually they stop asking. They'll say, oh, you fell off or
whatever. And you don't want to be around them anyway, right? That's what you say.
You're not comfortable around them. When you're going through changes, when you are
going through those positive changes and you are trying to be a better person, the
person you are supposed to be, those who would stand in your way, those who would
question your motives those who whatever you don't owe them anything nothing you
have to worry about only you and those people who are accepting those people
who do care those people who would be supportive of the changes you're making
That's it. None of the other people rate an explanation for anything. Not why you don't
want to hang around them, and not why you're not the same as you used to be. Because, for
me, I've always felt that the reason they don't want other people to change, they don't
want other people to become their best self is because when they witness
somebody else do it, they have to face that they haven't. Those aren't people you
need around you. You need people who are going to be supportive. Those are the
only people that are entitled to an explanation. Now I will tell you that in
a small town, full explanations might be better left to those who you truly
trust. You know, small towns are known for doing one thing really well, keeping
secrets right not so much so I would be careful about that but if you're not if
you aren't ready to be fully open about it just remember how quickly things
spread other than that nobody nobody is entitled to an explanation people will
change, they should change. If you don't look back on the person you were five
years ago and cringe, you wasted five years. Don't let anybody stand in that
way. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}