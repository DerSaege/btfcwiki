---
title: Let's talk about how Putin is doing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hWqO_UdZ2B4) |
| Published | 2023/06/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Putin's situation and the factions involved.
- Nationalists critical of Putin, calling him pitiful and questioning his leadership.
- Pro-Wagner channels are anti-Putin with no unifying ideology.
- Pro-Kremlin commentators disappointed in Putin's lack of decisiveness.
- Public-facing Kremlin response portrays everything as fine, while internally, blame games are happening.
- Political jockeying and palace intrigue are intense due to expected personnel changes.
- Putin is at risk but could stay in power if factions don't unify.
- Putin is seen as weak, needing to assert his power constantly.
- Uncertainty about the future due to various factions and variables.
- Not in Russia's or the West's interest for the boss of Wagner to take control.
- Speculation on potential outcomes and power shifts.

### Quotes

- "Putin is having to walk around a whole lot and tell everybody he's the king."
- "There's too many variables at this point. I mean I can't even guess."
- "The boss of Wagner don't think that just because he is losing his loyalty to Putin that he's a good guy."
- "But generally speaking that's not what occurs."
- "I wouldn't want anybody behind me."

### Oneliner

Beau dives into Putin's situation, faction dynamics, and uncertainty about the future, questioning Putin's grip on power amidst intense political jockeying.

### Audience

Political analysts

### On-the-ground actions from transcript

- Connect with organizations monitoring Russian politics (suggested)
- Stay informed about Russian political developments (suggested)

### Whats missing in summary

Insights into the potential impacts on global politics and stability.

### Tags

#Putin #Russia #Factions #PoliticalAnalysis #PowerDynamics


## Transcript
Well, howdy there internet people, Lidsbo again. So today we are going to talk about our man in the Kremlin.
We're going to talk about Putin and the situation he finds himself in.
A whole lot of people are asking what's going to happen? How at risk is he?
Is he going to be able to retain control? Stuff like that.
Short answer is we don't know yet.
we don't know yet, and there's a reason that nobody's really guessing at this point.
There's a lot of factions, so we're going to go through some of them and talk about what we can kind of pull from the
reporting
and different places where they talk, and kind of get a feel for where they're at.
We will start with Putin himself.
Basically, he's saying the entire country is united behind him.
Okay, so we'll go to the rest of the factions now.
The nationalists, the hardcore nationalist people.
It's worth noting that before the little dust-up with Wagner, Gherkin was already like, Putin
can't handle the war, he needs to transfer his war powers.
They are calling him pitiful, saying that his view of what's happening inside Russia
is from a parallel reality.
They have zero faith in the man at this point.
The pro-Wagner channels, they're basically just anti-Putin at this point.
No uniting philosophy, ideology, anything like that.
They're just mad.
They just don't like Putin.
The pro-Kremlin commentators, they're mad too, but they're mad because Putin didn't
act decisively when it comes to Wagner.
What they mean there is that they were expecting him to kind of double-cross the leadership
and find a permanent solution to those who questioned his rule.
bare minimum, they expected him to act against the leadership.
They're seeing him as weak as well.
They're still loyal, but they view this as a weakness, not a mistake.
The public-facing Kremlin response, like the official responses, everything is fine.
Putin's image is untarnished.
anybody saying otherwise is just a baby throwing a tantrum.
Privately, inside the Kremlin, it's the blame game.
They're all blaming each other for various things.
There's a lot of political jockeying for position going on, a lot of palace intrigue.
This is normal.
It's always occurring in the background, but right now it's really intense because they're
expecting personnel changes. That's it, that's what we know. Is Putin at risk? Yeah,
yeah, but it would require multiple factions kind of working out a deal. If
they don't unify, he might actually be able to stay in power, but the reality
is at this point Putin is having to walk around a whole lot and tell everybody
he's the king. If you're the king you don't have to tell people and he's doing
that a whole lot lately. This is the reason nobody really wants to kind of
say what's going to happen. There's too many variables at this point. I mean I
can't even guess. There are multiple options. You could see the Nationalists
siding with Wagner. You could see the Nationalists siding with the pro-Kremlin
commentators. There's a lot of overlap between these groups that could form
larger groups, groups large enough to get Swan Lake on the air. One of the
questions that came in that I definitely want to address, would it be better for
for Russia or for the the West if the boss of Wagner had succeeded in taking
control of Russia? No, no, absolutely not. The boss of Wagner don't think that just
because he is losing his loyalty to Putin that he's a good guy. He's not, he's
He's not.
He is not somebody that you would want in control of a strategic arsenal.
But the good news about that is, generally speaking, the person who wields the knife
doesn't sit on the throne next.
So even if something was to happen and he was the person ultimately seen as being responsible
for removing Putin, he probably wouldn't be in charge anyway. It's happened, but
generally speaking that's not what occurs. Somebody else who is aligned with
him would actually take power. But somebody aligned with him would be that
bad, you know, would be just as bad. As far as would it be better for the West?
No, no it wouldn't. I mean besides that where would Republicans get their
talking points from? Generally speaking it's a giant mess and a whole lot of
stuff was stirred up and everybody has to wait for it to kind of flow back down
to the bottom and see where it lands. He really could pull it out. He really could.
Putin might remain in power. At the same time, if there was some deal struck between the various
factions, we probably wouldn't know about it ahead of time. So you have that. And as far as
the idea of Putin saying Russia is united behind him, that's the one thing we know isn't true.
And if I was Putin, I wouldn't want anybody behind me.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}