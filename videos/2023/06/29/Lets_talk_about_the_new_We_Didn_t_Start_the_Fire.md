---
title: Let's talk about the new We Didn't Start the Fire....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=p-vpSIZToAA) |
| Published | 2023/06/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fallout Boy updated the lyrics of "We Didn't Start the Fire" to reference recent events, sparking questions about its impact.
- Teachers previously used Beau's videos referencing historical events alongside the original version of the song as a teaching tool.
- The original song's significance lies in its chronological order of historical events, making it effective for teaching.
- Beau won't be analyzing the new version like he did with the original due to its lack of chronological order and pop culture references.
- The original version, despite attempting to reference significant historical events, also faced issues in standing the test of time.
- Beau predicts that the new version's focus on pop culture events will not age well compared to the original's historical references.
- The future relevance of pop culture references like Kanye and Taylor Swift in the new version is questioned.
- The original song faced similar issues in predicting future relevance, as seen with its mention of "terror on the airline" now seeming outdated.
- Beau concludes that the new version may not be as impactful as a teaching tool due to its lack of chronological order and focus on pop culture.

### Quotes

- "The reason history teachers use it is because it's a timeline."
- "The new lyrics won't stand the test of time the way the original ones did."
- "It's not in order. So it can't be used the same way."

### Oneliner

Beau explains why Fallout Boy's updated "We Didn't Start the Fire" may not have the same impact as the original for teaching due to its lack of chronological order and focus on pop culture.

### Audience

History teachers, music enthusiasts

### On-the-ground actions from transcript

- Analyze historical events in chronological order with students (implied)
- Encourage critical thinking about the relevance of cultural references in music (implied)

### Whats missing in summary

Insights into the potential educational value of discussing music's cultural references with students.

### Tags

#Teaching #Music #HistoricalEvents #PopCulture #Impact #ChronologicalOrder #Education


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Fallout Boy.
We didn't start the fire.
And we're going to talk about the song,
and we're going to answer a couple of questions.
As soon as that song came out, my inboxes just overflowed.
But they were all pretty much one of two questions.
One is, am I going to do the same thing with the new version of the song that I did with
the old one?
And the other is, is it going to be as impactful as the older version?
So to clarify, Fallout Boy updated the lyrics to reference more recent events.
And the reason a whole bunch of people messaged about it is because years ago I had some teachers
reach out and they used some of my videos that make reference to historical events in
their classes to show that history is still relevant.
And they were like, hey, we use We Didn't Start the Fire as kind of like a teaching
tool. Can you make reference to some of the events in the song so we can use
those together?" And I'm like, I got you. By the time it was all said and done,
because of something that happened on a live stream, we actually wound up with
all of the lyrics. So it got put together. Okay, so am I going to do the same thing
with the new one? No. No. Almost certainly not. Because teachers aren't going to ask
me to. Something that I found out is that a whole lot of people don't know something
really important about the original version. It's in order. The reason history teachers
use it is because it's it's a timeline. It's not just a bunch of references to
historical events. It's in order by year. Einstein, James Dean, Brooklyn's got a
winning team, Davy Crockett, Peter Pan, Elvis Presley, Disneyland, 1955. Bardo,
Budapest, Alabama, Khrushchev, Princess Grace, Peyton Place, Trouble in the Suez,
1956. It's in order. So that's why they use it. The new song is not in order which
means teachers won't use it. Which means it won't be as impactful. I mean my
history teacher used it when I was in school. It is still being used today.
That's a long, that's a long timeline for something to be used. Another reason it
won't be used by history teachers, at least not as much, is because it
references pop culture events more than historical events. They, the new lyrics
won't stand the test of time the way the original ones did. You know I promise in
15 years nobody is going to care about Kanye and Taylor Swift and their little
thing. Nobody's going to care about it. But to be fair the original version
actually has some similar issues where even though that one was definitely
trying to reference important historical events, it didn't really stand the test
of time because nobody can tell the future. I mean, terror on the airline, right?
what it was referencing, it seems almost quaint by what came later. So there are,
the original song actually had the same issues, but because there was at least
what seems to be a very concerted effort to pick things that were going to stand
the test of time. The new version is probably not going to fare as well. Now
all of this is about it being used as a teaching tool. If you want to discuss the
song, the music, all of that stuff, go right ahead. I don't really have an
opinion on that. But the big question is there, will I be doing the same thing? No,
even though I'm pretty sure I actually talked about most of that there's
probably clips that could be used except for maybe like the GMO thing I think
everything else I actually covered so won't be doing that and I don't think
it's going to I don't think it's gonna have the same impact simply because it
won't. It won't stand the test of time and it's not in order. So it can't be used
did the same way.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}