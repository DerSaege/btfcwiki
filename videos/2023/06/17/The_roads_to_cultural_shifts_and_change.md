---
date: 2023-08-06 06:18:06.951000+00:00
dateCreated: 2023-06-18 03:12:15.655000+00:00
description: null
editor: markdown
published: true
tags: null
title: The roads to cultural shifts and change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IztFQtTd1A0) |
| Published | 2023/06/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains cultural shifts, unconscious bias, and discomfort surrounding LGBTQ issues.
- Responds to a viewer who struggles with acceptance of trans people in the public eye.
- Analyzes the viewer's discomfort and unconscious bias towards trans individuals.
- Points out the viewer's "passing privilege" and the societal acceptance of those who fit neatly into traditional gender norms.
- Encourages self-reflection and understanding of discomfort to facilitate change and acceptance.
- Provides historical examples of gender representation in media to illustrate the evolution over time.
- Suggests that discomfort may stem from unfamiliarity with non-traditional gender presentations.
- Emphasizes the importance of acknowledging changing societal norms and embracing diversity.
- Validates the viewer's willingness to learn and change, despite their initial discomfort.
- Offers advice on addressing discomfort and adapting to societal progress regarding gender identity.

### Quotes

- "It's not a new thing. It's been around."
- "Sometimes once you acknowledge something like this, it goes away."
- "You're probably not hateful."
- "You're not too old to change. You are changing."
- "Just acknowledge that things are changing."

### Oneliner

Beau addresses discomfort with LGBTQ issues, urging self-reflection and acceptance of evolving gender norms to facilitate change and understanding.

### Audience

Individuals grappling with unconscious bias.

### On-the-ground actions from transcript

- Analyze moments of discomfort around unfamiliar gender presentations (suggested).
- Acknowledge societal changes in gender norms and embrace diversity (implied).

### Whats missing in summary

The full transcript provides a comprehensive exploration of unconscious bias and discomfort towards LGBTQ individuals, offering valuable insights for personal growth and acceptance.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about cultural shifts
and unconscious bias, being uncomfortable,
fear of the unknown, and not being able to identify
why you're uncomfortable at times.
We're gonna do this because I got a message,
and it's a really long one, in fact,
this is probably gonna be a really long video.
I'm going to read the whole thing because I think it's in good faith, which is a nice
change.
Oh, go ahead and warn you, it doesn't start off good.
Bo, I've been following you a long time and agree with most of your positions.
The one thing I have a really hard time with is LGBTQ stuff.
I know it's Pride Month, so it's more public, and maybe why it's that much more annoying
to me.
I accepted gay people, but when it comes to trans people especially, it just bothers me
that it's so public at times.
I feel bad admitting this, but I just want them to go back to how it used to be.
I'm about your age.
When we were growing up, a man in a dress was unwell and we wanted them to get help.
Today it's on TV all the time.
It's in public all the time.
Can you imagine when we were growing up if there were men on TV wearing dresses and people
just acted like it was normal?
I guess I don't get where it came from.
I want to be clear about something.
I'm not a hateful person.
I work with a trans person who is very nice, and I always call him a her. I treat her like a woman,
and it does make sense for her to go to the women's bathroom. She could be attacked in a men's room.
It just makes me uncomfortable in some way when I'm around some of them.
Am I wrong about this? Have I just reached the age where I can't change anymore?
I even read about the difference between gender and sex because the girl, the girl I mentioned
earlier, gave me a book after I asked some questions. I read it and understand the difference,
but it still makes me uncomfortable at times. I'm all for people living their lives and know it's
not right for me to say, hey, get back into the closet to make me feel better. But deep down in
my gut. Sometimes I just don't like it. Do you have any advice? Okay, so let's start with
when did it start? With us. With us. In a very less than ideal way, it started with us, with our
generation. You said, can you imagine what it would be like when we were growing up if there was a man
on TV, wearing a dress, and everybody just acted like it was normal.
Whistling.
Only about a whole lot of people just whistled the rest of that.
That was the whole gimmick for Klinger, right?
Man wearing a dress, and he wasn't unwell.
That was the whole thing.
It's why he couldn't get a Section 8. If that doesn't do it for you, we could time warp
back to Rocky Horror, or we could go the other way to when Jenny McCarthy played a trans
character. It was on TV. It was. And that's spanning a quarter of a century right there.
It's not a new thing. It's been around. Now, for people from that community,
yes, I understand that those are not good examples. Like, that's not ideal
representation in any way, shape, or form, but that's also kind of a point. It
started with us as a gimmick, and our generation treated it that way, but the
underlying message, it still was something that was heard and it kind of
helped open the door. I find it very hard to believe that a generation of
people watching watching Klinger be told that there was nothing wrong. I feel like
that had to have some impact and it might have led to a little bit more understanding,
the kind that would lead somebody to say, yeah, I want people to lead their lives.
Now as time moved on and it was no longer just a gimmick, you know, the representation
became better, became real. That's all that's happened there. It was always
around in various ways. It was cliche. It was tokenized. It was bad, but it was
there. It's not something that just started in the last few years. It's been
around in a whole bunch of different ways. And many times I don't think it was intentional.
I don't think that the people behind some of these shows really had any intention of,
to use your term, bringing this out of the closet. I think it was just them writing something
and trying to be original in some way. But it opened the door. It helped move things
along. Okay, so you feel uncomfortable. You never say why in your message. You
can't identify why you're uncomfortable. You didn't include it and I feel like
it's because you don't know. You know, I think I've told this story on the
channel before. There was a time when I encountered Arab males. I found myself
always stepping back and I felt uneasy and it wasn't for any reason that
people might associate with Arab males but I didn't know what it was. Until one
of my friends, their family came over from the old country. They were Italians
and I got the exact same feeling around them. It had absolutely nothing to do
with them being Arab, just that they stood close. Unconscious bias. My people,
the culture that I come from, we don't stand close to each other. They do.
Sometimes it's something really simple but if you can't identify it, it makes you
uncomfortable. If it doesn't fit in with your normal patterns, it makes you
uncomfortable. I want to point something out. When you're talking about the
girl from work. You identify that she's trans by saying, you know, I always call
him a her. And then throughout the rest of the message, it's feminine pronouns.
Even going as far as to call her a girl. Now, just so you know, today that is a
a no-no, but being from your generation, I know what this means.
She's petite and cute, isn't she, right?
And she fits into your pattern of woman, neatly.
So neatly, in fact, that you can understand why she should use the women's bathroom because
she might be attacked in a men's room because nobody would mistake her for a man, right?
It's there.
When you're talking about feeling uncomfortable, you say, at times, sometimes, not all the
time, right?
Because there are some trans people that you don't feel uncomfortable around.
That's very evident in this message.
So what might it be?
The times you feel uncomfortable.
It might do you some good to analyze when that occurs because it may be something as
simple as it not being the pattern that you're used to, like standing far apart.
could be something along those lines. When when we were growing up, okay, gender
and sex, they lined up, right? And there were two options. You've read about it,
you know it's a spectrum now, okay? You've read about it, you said you understand it,
don't need to go into that. It's a spectrum. The woman you work with, I'm
willing to bet just about anything, she fits very neatly into one of those boxes,
one of those patterns that you're accustomed to. She looks like a woman,
right? The people you feel uncomfortable around, do they fit neatly into one of
the the two options that you grew up with. I'm willing to bet just about anything they
don't and the only reason I'm saying this is because I have seen this so many
times from NRH. People who pass, people who pass, they're accepted. People who fit
neatly into that box. They're accepted so much faster by so many more people
because it lines up with the preconceived pattern with what you're
used to. Those who don't, they fall outside of that pattern. It's something
new, it's something unknown, generates discomfort. The reason I'm doing this and
saying this is because I hope it's a whole lot like identifying that, hey, they
just stand close. Sometimes once you acknowledge something like this, it goes
away. You don't have the discomfort anymore or the unease anymore. I am
willing to bet just about anything you are a walking example of passing
privilege. It's not actually uncommon. It's just not something that gets
talked about outside of that community very often. Those who don't fit neatly
they have a harder time. Trans people who don't pass and don't fit perfectly
and they're very easily identified as trans people, they have a rough time. And
And it's just not something that gets talked about.
And maybe it should.
Because if you're somebody who will sit here and have a
conversation with somebody, ask them questions, and
obviously do it in a respectful enough way for them
to come back with a book for you to read, and then you
actually read the book, you're right.
You're probably not hateful.
And the fact that you're reaching out trying to figure out what's going on and why you
feel uncomfortable indicates that you don't want to be like that.
If you did, this would have a very different tone.
And the fact that you know, you know it's not right to say, hey, get back in the closet
because, you know, I don't feel comfortable.
know that's not right. You're not too old to change. You are changing. You just hit
a spot that you can't get through yet. Take a moment and analyze the
sometimes, at times, that you feel uncomfortable because I'm fairly
certain that's what it is. I know somebody's gonna say I'm reading too much
into this but I've just seen this so much. You know that gender, sexuality, it's
It's all a spectrum, you've read a book, you've got it.
The patterns that we grew up with, two choices.
There's more choices today.
There's more ways that people present and it doesn't have to be uncomfortable, just
acknowledge that things are changing.
I think you'll be fine and
I will send you this before it publishes. So if y'all are watching this,
yes that's exactly what it was. Anyway,
It's just a thought.
Y'all have a good day.
## Beau's Shirt 
Black shirt with bubble text "Be Kind" repeating.
## Easter Eggs on Shelf
Med-Kit for M.A.S.H.