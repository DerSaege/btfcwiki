---
title: Let's talk about Raskin asking for Biden tape info....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=-Rn0MBhdeg0) |
| Published | 2023/06/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raskin, from the Democratic Party, is reaching out to the FBI for more information on allegations against President Biden.
- Raskin is likely seeking an assessment memo from the FBI regarding unverified information on the FD-1023.
- The assessment memo may reveal that the FBI found insufficient evidence to escalate the probe on Giuliani's allegation.
- Republicans were aware of the FBI's assessment but still chose to mislead their base with conspiracy theories.
- Politicians, particularly the Republican Party, continue to mislead their supporters with baseless claims.
- The pursuit of the allegations against President Biden is deemed as a distraction tactic by the Republican Party.
- Republicans intentionally focused on certain unverified information to fuel a scandal.
- Beau suggests that the Republican Party needed a scandal to divert attention from their failed policies.
- Beau hints at the possibility of damaging search results if people were to google "President Admits to Crime on Tape" related to Biden.


### Quotes

- "Who could have seen this coming? So my guess here is that he is looking for what I called the determination memo in that first video."
- "It's not surprising at this point that politicians are doing this, surprising that the same people keep falling for it over and over and over again."
- "They needed a scandal. They needed something to distract from their failed policies or candidates getting in trouble."


### Oneliner

Raskin seeks FBI information on Biden allegations; Republicans knowingly mislead base with unverified claims to distract from failures.


### Audience

Political activists, Democratic supporters


### On-the-ground actions from transcript

- Contact your representatives to demand transparency and accountability in political dealings (implied).
- Share verified information to combat baseless conspiracy theories within your community (implied).


### Whats missing in summary

The full transcript provides detailed insights into the manipulation of information by politicians and the detrimental impact of baseless claims on public perception. It also hints at the need for accountability and transparency in political actions.


### Tags

#Politics #BidenTapes #Republicans #ConspiracyTheories #Transparency


## Transcript
Well, howdy there Internet people, it's Bo again.
So today we're going to talk about an update
on the whole Biden tapes thing,
the recordings that are supposedly out there somewhere
because there's been an unforeseeable development,
something that is a little surprising.
Raskin, who is the top member of the Democratic Party
the Democratic Party on the House Oversight Committee. Well, he seems to
be reaching out to the FBI and asking them to release more information about
these allegations. Kind of put that FD-1023 in context. Are you shocked? I'm
shocked. This is my shocked face. Who could have seen this coming? So my guess
here is that he is looking for what I called the determination memo in that
first video when we first started talking about this. I have since been
informed that the new language for that is an assessment memo, but that's what
he's looking for. It's a document that explains what the FBI thought about the
unvetted, unverified information contained on the FD-1023. And I mean it's
weird when you think about it because why would somebody from the Democratic
Party be asking the FBI to release more information about this super major, very
real allegation against President Biden. I mean he probably wouldn't do that
unless he knows something, maybe he was told something by the FBI, perhaps on
June 5th, that would lead him to believe that that information would be very
favorable to President Biden. He might describe it as the FBI explained that
assessment was closed because Mr. Brady's team found insufficient evidence to warrant escalating
the probe of Mr. Giuliani's allegation from an assessment of the allegations to a preliminary
or full investigation. I mean, that sounds like he probably knows something. The FBI read an
excerpt from a memorandum closing down the assessment. So it certainly appears
that what we suspected about this the entire time is exactly what's been going
on. It's worth noting that from my understanding, Republicans got the exact
same information which means they knew when they held up that form with the DOJ
logo on it and said oh we have to take these allegations very seriously they
knew that the FBI had already done the assessment on it and they knew that well
it just didn't it just didn't really add up for a whole bunch of reasons. What
What that means is that once again the Republican Party intentionally misled its base to follow
some conspiracy theory.
Another Scooby-Doo mystery that at the end of it, well there's nothing really there.
It's not surprising at this point that politicians are doing this.
surprising that the same people keep falling for it over and over and over
again. You could have just googled at any point in time, FD 1023, what is it? And it
would tell you what it is, and it would tell you the information on it is not
verified, it's not vetted, it's not proven, so there's nothing to disprove.
And when the Republican Party initially first started chasing this ghost, they kept asking
for that specific form.
Not all of the forms that went along with it.
Not the information that would put it in context.
Just that form.
as I said in that first video, I feel like they already knew what the
assessment said. They just didn't want to tell their base that. They needed a
scandal. They needed something to distract from their failed policies or
candidates getting in trouble. If I was somebody in the Republican Party, I would
suggest that it's probably actually a really bad idea right now to have a
whole bunch of people googling President Admits to Crime on Tape because let me
tell you those results they don't come back to Biden. Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}