---
title: Let's talk about Jack Smith's motion on discovery....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RsEQKxN6tlM) |
| Published | 2023/06/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special counsel's office filed a motion related to Trump, seeking a protective order regarding discovery material.
- The motion aims to prevent Trump and his lawyers from disclosing information that could compromise ongoing investigations or identify uncharged individuals.
- Trump's defense team is unlikely to oppose the motion, as it may help them control Trump's public statements.
- The protective order restricts the disclosure of materials to specific individuals involved in the defense, witness interviews, and court-approved entities.
- Trump is allowed to express opinions on social media but must refrain from sharing specific discovery material.
- The motion does not inhibit Trump from campaigning or expressing his views but focuses solely on the handling of discovery material.
- While Trump's attorneys may advise him to refrain from discussing the case, his compliance with protective orders remains uncertain.

### Quotes

- "This motion is basically a shut your client up motion."
- "It's not abnormal, it's not an attempt to muzzle his campaign or anything like that."
- "But at the same time I'm doubtful he'll follow the protective orders."
- "Y'all have a good day."

### Oneliner

Special counsel's office files a motion to restrict disclosure of discovery material in Trump's case, aiming to control public statements without muzzling his campaign.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Follow updates on the legal proceedings related to Trump's case (suggested)
- Analyze the implications of protective orders in high-profile legal cases (implied)

### Whats missing in summary

Insights into the potential consequences of Trump's public statements on ongoing investigations and uncharged individuals.

### Tags

#Trump #SpecialCounsel #LegalProceedings #ProtectiveOrder #DiscoveryMaterial


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about the motion
that the special counsel's office filed
dealing with Trump and what it means.
Before we get into this, I want to say
we are absolutely not going to cover
every motion that gets filed.
There's going to be dozens of these
and most of them are gonna be pretty standard.
and I'm only going to cover anything that's just abnormal, okay.
This is actually pretty normal.
We've already seen something very similar to it
in another Trump case up in New York.
This motion is basically a shut your client up motion.
It's really what it boils down to.
The special counsel's office says
that in the discovery material,
the material that the special counsel's office
will turn over to Trump's defense team
to prepare, there will be information.
Let's see.
The materials include information
pertaining to ongoing investigations,
the disclosure of which could compromise those investigations
and identify uncharged individuals.
The motion is basically asking for a protective order
that stops Trump and the other defendant and their lawyers
from saying they can't disclose the material to anybody directly or indirectly to any person
or entity other than persons employed to assist in the defense, persons who are interviewed as
potential witnesses, counsel for potential witnesses, and other persons to whom the court
may authorize disclosure. So basically, you can't tweet this stuff out, which is really what it
boils down to. In a later passage, let's see, the discovery materials along with
any information derived therefrom shall not be disclosed to the public or the
news media or disseminated on any news or social media platform without prior
notice to and consent of the United States or approval of the court. Okay.
This really is pretty normal. My understanding is that Trump's defense
team isn't even going to fight this. They may actually be very thankful for
it, to be honest, because lately anytime Trump speaks about this, it's not well
thought out. We'll just leave it at that. So his defense team may actually be
very grateful for this motion because they might be able to control their
client a little bit better. The other reason I would imagine they're not
going to oppose it is because they'll probably lose even if they try. This is
something that it's not necessarily standard but it it's not uncommon and it
definitely make sense in this case. So that's what it is.
It isn't something trying to stop him from campaigning which really was the
kind of the angle behind most of the questions. He's totally allowed to be on
social media and talk about the case and talk about his opinion if he wants to.
It's not a good idea, but he's allowed to do it. This is specific to material
that the federal government is going to turn over to him as discovery material. I
think there's also something in it like what we saw up in New York that says
like he can't have access to it alone and can't copy it, which again makes sense
in this case. But it's not abnormal, it's not an attempt to muzzle his campaign or
anything like that. This is very specific, it's a very narrow, just talking about
discovery material. Now his attorneys may widen the advice and say don't talk
about these things at all and that would probably be really good advice coming
from them. But at the same time I'm doubtful he'll follow the protective
orders so I don't think that he would take the advice even if his attorneys
provided it to him. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}