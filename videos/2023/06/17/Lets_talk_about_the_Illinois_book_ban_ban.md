---
title: Let's talk about the Illinois book ban ban....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OiseaHrxSbE) |
| Published | 2023/06/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Illinois book ban ban, clarifying that it doesn't actually ban books but restricts funding for libraries.
- Outlines what libraries in Illinois must do to comply with the legislation, including adopting the American Library Association's Library Bill of Rights.
- Emphasizes the importance of not excluding materials based on origin, background, or views, and avoiding removal due to partisan or doctrinal disapproval.
- Argues that libraries should provide books based on public demand, even if some individuals disagree, as their role is to serve the community.
- Commends Illinois for taking a positive step and hopes other states will follow suit to allow public libraries to uphold their ethical standards.

### Quotes

- "The states that are forcing libraries to remove books because they hurt somebody's feelings or you know they disagree with somebody's personal religious beliefs, they are forcing libraries to go against their own code of ethics."
- "A library isn't there to be an archive. It's there to provide services to the public. It's there to provide books that people want."
- "This legislation is a good move."
- "I really hope that other states follow suit and create a situation where public libraries can operate under the ethics that they've set for themselves."
- "They're trying to fulfill the promises they've made to the public, let them do it."

### Oneliner

Beau outlines Illinois' book ban ban, requiring libraries to uphold ethical standards and provide books based on public demand.

### Audience

Library advocates, book lovers

### On-the-ground actions from transcript

- Support your local library by advocating for their autonomy and adherence to ethical standards (exemplified)

### Whats missing in summary

Importance of supporting public libraries and ensuring access to diverse materials for all community members.

### Tags

#LibraryEthics #BookBans #PublicLibraries #CommunitySupport #Illinois


## Transcript
Well, howdy there internet people, it's Bo again. So today,
we're going to talk about the Illinois
book ban ban. The ban of book bans
in Illinois. That's how it's being framed. Is that really what it is? We're going to talk about what it does
and what libraries are required to do to
avoid conflicts with this legislation.
Okay, so first, is it a book
ban, ban. Not really. It's not. The legislation doesn't actually stop,
it doesn't stop libraries from removing things on a partisan basis.
It really doesn't. Not outright. It doesn't make it a criminal offense to do this.
What it does is it says, yeah you can do that if you want to,
but if you do you don't get any funding. So it's an effective book ban, ban,
but not actually a legitimate
book band ban.
So what do libraries
in Illinois had to do to avoid
this? Basically they have to adopt
and apply the American Library Association's
Library Bill of Rights. It's a thing.
It doesn't have a whole lot of literal sections to it,
but it covers things like removing books for various reasons, privacy who has
access, making sure that things are equitable. It's really the purpose. It's
a quick read if you actually want to look into it. But in relevant part material
should not be excluded because of their origin, background, or views of those
contributing to their creation. Materials should not be prescribed or removed
because of partisan or doctrinal disapproval. Those are the two that are
really going to come into play here. So as long as libraries adopt this, most have
incidentally, like most have nationwide, there's no issue as long as they
apply it. The states that are forcing libraries to remove books because they
hurt somebody's feelings or you know they disagree with somebody's personal
religious beliefs or whatever, they are forcing libraries to go against their
own code of ethics. They're forcing libraries to violate the basic tenets
that libraries are supposed to be fulfilling. It's worth remembering that a
library isn't there to be an archive. It's not there to say these are the
books you're supposed to read. It's there to provide services to the public. It's
there to provide books that people want. So if there is a book that is being
consistently checked out or used in the library, even if there is a segment of
the population that is upset about it, the library should maintain it. That's
its whole job. It's what it's supposed to do. This legislation is a good move. It's
a good move and it's the first state that I'm aware of to do something like
this. I would really hope that other states follow suit and create a
situation where public libraries can operate under the ethics that they've
set for themselves. They're trying to fulfill the promises they've made to the
public, let them do it. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}