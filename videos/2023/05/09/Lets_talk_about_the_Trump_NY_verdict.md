---
title: Let's talk about the Trump NY verdict....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cLEwxyndbeA) |
| Published | 2023/05/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A federal jury in New York found former President Trump liable for battery and defamation in the E. Jean Carroll case, with a potential $5 million payout to her.
- The case stems from allegations of sexual assault in a dressing room, with Carroll also requesting a public retraction of Trump's statements.
- Trump falsely claimed he was not allowed to speak or defend himself, which is contradicted by public records.
- Trump's rhetoric, like claiming he couldn't present a defense, is designed to push others down to make his base believe they are moving forward.
- Trump's lies have separated supporters from their families and eroded their belief systems, focusing solely on loyalty to him.
- This situation should mark the end of the MAGA movement, revealing the constant lies and manipulation from Trump and his supporters.
- Trump and those who mimic his leadership style keep supporters angry and looking down rather than up at how they are being exploited.
- Trump's lies and manipulation have led supporters to abandon their previous beliefs little by little, prioritizing loyalty to him above all else.

### Quotes

- "He lied about that too. That's not true. He absolutely could have presented a defense he chose not to."
- "He lied to you. He's still lying to you."
- "They lie to you constantly. They keep you angry."

### Oneliner

Former President Trump found liable for battery and defamation, exemplifying his pattern of lies and manipulation, eroding supporters' beliefs and loyalty.

### Audience

Supporters of the Republican party

### On-the-ground actions from transcript

- Reassess your beliefs and loyalty towards leaders who manipulate and lie to you (implied)
- Educate yourself on the facts of cases involving public figures before forming opinions (implied)

### Whats missing in summary

The emotional impact on supporters who have been misled and manipulated by Trump's lies and rhetoric.

### Tags

#Trump #RepublicanParty #Manipulation #Lies #Beliefs


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today we are going to talk about a finding
that came out of New York
and former president Donald J. Trump
and what this finding means
and what it should mean for the Republican party.
For those people who might have been convinced,
might have supported him,
might have been swayed by him,
might have abandoned things that they used to believe in
because they believed in his rhetoric.
A jury, a federal jury in New York has found that former President Trump is liable for
battery and defamation in the E. Jean Carroll case.
At time of filming, it looks like she's going to get about $5 million.
There was also a request from her to have him retract those statements publicly.
time of filming. Don't know if that's part of it yet. We'll have to wait and see.
This is, of course, stemming from the allegations of a sexual assault in a dressing room.
Trump has told his supporters that he was, quote, not allowed to speak or defend myself.
even as hard-nosed reporters screamed questions about this case at me. He lied
about that too. That's not true. That's not true. He absolutely could have
presented a defense he chose not to. This is something that is public record.
He is lying to you still. If you believe this man, you need to look at that statement.
He is flat out saying he couldn't present a defense, but he could. He's still lying.
It's who he is. You can sit there and say, well we didn't know that he was like this.
Yes you did. You did when that tape surfaced. When you're a star, they let you do it.
it. And it got dismissed. It's just locker room talk. No, it was dressing room talk, apparently.
This is not just Trump. This is anybody whose rhetoric is designed to kick down at other people.
Those people who can't lead and can't move the country forward, their goal is to push other people
back. So their base, those people who believe their lies, seem like they're moving forward.
They feel like they're in a better position because those people who are under them are
further down. You didn't move. You're still in the same spot you were. He did nothing for you
except make you angry and separate you from your family. He lied to you. He's still lying to you.
And all of those people who mimic his leadership style, who use that same kind of rhetoric
they're doing the same thing. They are doing the same thing. Not allowed to
speak or defend myself. This is a lie. It's a verifiable lie. He is lying about
a case that was brought because he was lying and people are still gonna buy it.
People are still gonna believe it. You are still going to give up on the things
that in 2014 you might have actually believed but little by little he chipped
away at your belief system and now all that matters is dear leader and those
who pledge their loyalty to him. This should, it really should, be the end of
MAGA. It should be the end of this. These are who these people are. They're not who
you think. They lie to you constantly. They keep you angry. They keep you
looking down so you're not looking up at them and seeing how they're coming after
you. Because they are. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}