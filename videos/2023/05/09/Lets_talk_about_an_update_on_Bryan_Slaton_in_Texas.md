---
title: Let's talk about an update on Bryan Slaton in Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TDjEZ5pXH0c) |
| Published | 2023/05/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on Texas State Representative Brian Slaton and the findings of the House Committee regarding his activities and behavior.
- Slaton provided drinks to underage workers, leading to one needing Plan B with questions about consent.
- Slaton resigned two days after the findings were released, citing the need for a new representative to meet expectations.
- Despite the resignation, Representative Murr plans to call for a vote to expel Slaton from the Texas House of Representatives.
- Slaton is considered an officer of the state until a successor is elected and takes office, with the expulsion vote scheduled for that day.
- The expulsion vote is likely to succeed as Slaton has already resigned, making it the first expulsion in almost a hundred years in the Texas legislature.
- This situation is unique as it involves a Republican majority body holding another Republican accountable, a rare occurrence.
- Rumors suggest that Slaton was offered the chance to resign earlier but declined, leading to the investigation and subsequent events.
- There are questions about potential criminal activities as some findings by the committee were misdemeanors, possibly leading to law enforcement involvement.
- The outcome of the expulsion may cover these potential legal implications, but the exact course of action remains uncertain.

### Quotes

- "It's one of those cases that don't happen very often where you have a Republican majority body actually moving forward to hold another Republican accountable for their actions and their behavior."
- "There are questions about some of the activities because some of the things that the committee found were, in fact, crimes."
- "For those who were wondering if anything was going to come of it, yes, something absolutely came of it, and apparently came of it pretty quickly."

### Oneliner

Texas State Representative Brian Slaton resigns after misconduct findings, faces potential expulsion from a Republican-majority body for providing drinks to underage workers.

### Audience

Legislative observers, accountability advocates

### On-the-ground actions from transcript

- Contact Texas legislators to express support for holding accountable those who violate ethical standards (implied)
- Stay informed about local political developments and hold elected officials accountable for their actions (implied)

### Whats missing in summary

Details on the specific findings of the House Committee's investigation into Brian Slaton's activities and behavior.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
when a resignation isn't enough
and when it's too late.
So we will be providing a little bit of an update
on Texas State Representative Brian Slaton.
We talked about him a few days ago
and talked about what the House Committee found
their investigation into his activities and behavior. For those who missed it or
don't know, I'll have a link down below, but the short version is the committee
found that he had provided drinks to people who were working for him who were
underage. One of them wound up having to take Plan B and there was a question
about whether or not she was capable of consenting at that point. Now, Slayton
has resigned on Monday, two days after the the findings came out. His statement
says, it has been an honor to represent my friends, neighbors, and the great
people and communities of House District 2. They voted overwhelmingly to send me
to the Capitol as their representative in two elections, and I worked daily to meet their
expectations. My decision today is to ensure that their expectations will continue to be met by a
new representative who will also work hard on their behalf." Okay, so normally in politics this
This is the end of it.
This is it.
It's over.
This would generally end a vote for somebody to be expelled.
The person who oversaw the investigation, I believe Representative Murr, has other plans.
Let's just say it was basically like, yeah, you resigned.
That's great.
where it stops. He said, it remains my intent to call up House Resolution 1542
to expel Representative Slayton from the Texas House of Representatives. Though
Representative Slayton has submitted his resignation from office under Texas law,
he is still considered to be an officer of this state until a successor is
elected and takes the oath of office to represent House District 2. That vote
should be taking place today if everything goes according to what has
been previously scheduled. Given the fact that Sleighton has already resigned, it
seems as though the vote to expel him will probably will probably succeed. My
guess is that Sleighton was hoping to avoid that. This will be the first time
the Texas legislature has expelled somebody in almost a hundred years. This
development is interesting because it's one of those cases that don't happen
very often where you have a Republican majority body actually moving forward to
hold another Republican accountable for their actions and their behavior. It's
It's not every day that we see that happen.
Generally, they close ranks.
It's worth noting that, according to the rumor mill,
Slayton was offered the opportunity
to resign when the allegations first surfaced.
And it seems, according to rumor,
that he declined to do so, which led to the investigation,
which led to everything that's happening now.
But it does appear that Slayton will be expelled, not just resign.
The resigning was just a bonus.
There are questions about some of the activities because some of the things that the committee
found were, in fact, crimes.
Now, I do not know the inner workings of Texas state law on that, but my guess, given the
fact that they listed that they were misdemeanors. My guess is that this
situation will be turned over to law enforcement, but it's a guess. I don't
know that. It might be a situation where the expulsion is going to kind of cover
everything in their eyes.
It's politics.
We'll have to wait and see.
But for those who were wondering if anything was going to come of it, yes, something absolutely
came of it, and apparently came of it pretty quickly.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}