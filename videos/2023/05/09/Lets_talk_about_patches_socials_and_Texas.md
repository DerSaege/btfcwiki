---
title: Let's talk about patches, socials, and Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=6SDWAjAWhtk) |
| Published | 2023/05/09 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the hesitation in discussing a patch linked to a specific group and the shooter in Texas.
- Describing the commercial availability and widespread use of patches once they leave an in-group.
- Noting hidden meanings of patches and how they can be misunderstood by many who wear them.
- Mentioning the discovery of social media profiles that indicate far-right and white supremacist views of the shooter.
- Speculating on the lack of direct links between the shooter and the group associated with the patch.
- Pointing out the recent conviction of the leadership of a group associated with the patch, which could deter any potential connection to the shooter.
- Anticipating intense scrutiny from domestic counter-terror efforts on the organization due to the proximity of the patch to the convicted leadership.
- Stating the preference to wait for concrete evidence before discussing such sensitive topics to ensure accuracy.

### Quotes

- "The patch, you know, it is what it is."
- "The socials, which do appear to be his, that pretty much confirms his ideological makeup."
- "I like to be right, I don't like to jump the gun."

### Oneliner

Beau explains his cautious approach in discussing a patch linked to a specific group and the shooter in Texas, along with the discovery of social media profiles indicating far-right views but no direct connection to the group, anticipating increased scrutiny on the organization.

### Audience

Community members

### On-the-ground actions from transcript

- Monitor and report extremist activity in your community (implied)
- Support efforts combatting far-right and white supremacist groups (implied)

### Whats missing in summary

The nuances of the dynamics of patches and their hidden meanings in different contexts.

### Tags

#Texas #Patches #SocialMedia #FarRight #WhiteSupremacy #CommunityPolicing


## Transcript
Well, howdy there internet people, it's Beau again. So today we are going to talk
about Texas and patches
and social media and delays
in talking about something.
A lot of people have been asking why I wasn't discussing
the patch and
why I wasn't going into what appeared to be
the motivating factor
for the assailant in Texas.
If you don't know, the
shooter there in Texas apparently had a patch
on his gear that said RWDS.
That patch is very closely linked to a specific
group. It was popularized by a specific group like five years ago. The belief that
there's a connection there, I get it, it makes sense, but at the same time I am
pretty familiar with how the dynamics of those patches work. I would point out
that part of the branding of this channel now is because a bunch of cops
were wearing a patch they had no business wearing. Those patches once they
become commercially available and they leave the in-group they get used by a
whole bunch of different people. I mean aside from the one that everybody knows
about on this channel I would point out that there's a whole bunch of Goonies
patches, referencing the movie from the 80s. They have a pretty specific meaning.
Anything you see that's something like Copper Pot School of Exploration or
something like that, when those originally kind of surfaced, that was
that was basically declaring, hey I know how to take something that is designed
to go boom that's tamper resistant and make it not go boom. That's really what
it was about, the ability to diffuse booby traps. Today you see those all the
time and I'm willing to bet that 90% of the people that have them have no idea
what the origin is. They have no clue that they are loudly proclaiming not just
my EOD, I'm really good. There's a lot of them that are like that. They have hidden
meanings to in-groups. I don't know where the RWDS patch could go and still be
used but there was a possibility. So I wasn't ready to say yeah that's what it
was. All that being said, earlier today somebody sent me forwarded over what
appears to be the socials of the shooter. I have no way of confirming that those
are in fact his socials but they certainly appear to be. If they are, yeah
he's phash, he's phash. Lots of far-right stuff, lots of white supremacist stuff,
it's all there and it does appear to be his. Now going back to that group, I
didn't see any direct links, not the way you might expect, to the group that
popularize that patch and I know that those people who are you know working to
combat these groups, you see that as bad news.
It's really good news. Please remember their leadership just got convicted.
If there was a heavy link between the shooter and that organization it very
likely could mean that they were starting a campaign. It's actually good
news that there doesn't appear to be, at least from what I've seen so far, a heavy
connection. I mean, you know, all those groups kind of blend together at some
point, but I haven't seen anything direct yet. But rest assured, just because of
the patch in the proximity to the leadership being convicted because of their stuff on
Jan 6, the domestic counter-terror guys, they're going to be all over this.
That organization is going to feel so much heat right now, it's not even funny.
So the short answer, why wasn't I talking about it?
Because I like to be right, I don't like to jump the gun.
really that simple. The patch, you know, it is what it is. The socials, which do
appear to be his, that pretty much confirms his ideological makeup, but it
doesn't provide a direct link to that group, at least not from what I've seen,
and that's good news. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}