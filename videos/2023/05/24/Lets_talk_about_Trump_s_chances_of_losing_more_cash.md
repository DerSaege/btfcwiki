---
title: Let's talk about Trump's chances of losing more cash....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=OPr9swb7Wk4) |
| Published | 2023/05/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the timeline involving a recent verdict in favor of E. Jean Carroll.
- Mentions the first lawsuit tied up in court because Trump claims immunity as a former president.
- Details the amendment to the first lawsuit to include information from the second lawsuit's verdict and a CNN town hall transcript.
- Notes that Trump doubled down on his claims post-second lawsuit verdict, prompting Carroll's team to argue for an additional 10 million in damages.
- Suggests Trump may end up paying between 15 and 25 million due to his actions.

### Quotes

- "Just another bad day for Trump."
- "Every time he doubles down on it, the likelihood of him having to pay more increases."

### Oneliner

Beau clarifies legal entanglements in lawsuits against Trump, indicating potential significant financial repercussions for his actions.

### Audience

Legal analysts, political observers.

### On-the-ground actions from transcript

- Support organizations fighting for justice for victims of defamation and assault (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of legal developments and implications of Trump's actions, offering a comprehensive analysis beyond a brief summary.

### Tags

#LegalAnalysis #TrumpLawsuits #EJeanCarroll #Defamation #TimelineClarification


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
and doubling down and what it may cost him.
We're also going to clear up some timelines
because I feel like it might get confusing.
And we're going to talk about how E. Jean Carroll is going
to be amending one of her suits.
Okay, so the first thing that's important to do here
is clear up the timeline because everybody knows
about the recent verdict in favor of E. Jean Carroll.
The jury found that the battery and the defamation
was there, two million for the battery,
three million for the defamation.
The thing is, that's actually the second lawsuit,
even though it was resolved first. The first lawsuit
is still tied up in court because Trump is basically arguing
that he can't be held accountable because he was president. I mean that's kind of
what it boils down to.
Saying that his statements related to the office of the presidency.
So, her lawyers
have filed to amend the first lawsuit
which is the one that is still tied up, and they want to include information
about the verdict from the second suit, obviously, but they also are adding the
transcript from the CNN town hall. Why? Because shortly after the verdict in the
second lawsuit, Trump got on TV and doubled down and basically repeated all
the claims and added some new ones. So her team is trying to show that the
initial amount of money, that initial verdict, well it's not enough to deter
him because he did it again right afterward. It's gonna be hard to argue
against that. He got on a national platform and repeated the same type of
of stuff, attacked her again, right after the verdict.
There's probably a good chance.
Now, you still have the underlying claim, the thing that's holding it up, is whether
or not a president can be held liable for those kind of remarks.
And her team has to show that his reason for doing it was personal,
not related to the office of the presidency. Given the fact that he
insulted her appearance and all kinds of other things, it should, I mean it seems
to me like they can do that, but that's what's tying it up in court. Either way,
it appears that they're asking for an additional 10 million in that suit. So
Just another bad day for Trump. There's actually a whole bunch of legal
developments that have occurred with Trump and his plethora of investigations
that we'll be going over soon, but this one in particular because we've been
kind of treating it as separate from all of his other legal entanglements seemed
like it she get its own video. So by the time this is all said and done Trump
very well may be paying somewhere between 15 and 15 million and up because
of this and every time he doubles down on it every time he talks about it the
the likelihood of him having to pay more increases.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}