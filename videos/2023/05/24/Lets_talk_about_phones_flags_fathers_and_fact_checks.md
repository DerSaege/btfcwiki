---
title: Let's talk about phones, flags, fathers, and fact checks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=m206m4WOdo0) |
| Published | 2023/05/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A viewer reached out about a conspiracy theory involving satellite phones given to senators for a coup attempt to keep Biden in office.
- Beau spent 25 minutes fact-checking the claims and found the accurate information in just under 13 minutes.
- Satellite phones were indeed given to some senators, but it was part of upgrading Senate security post-January 6th, not a nefarious plot.
- The upgrade was voluntary, and only a limited number of senators opted for the phones.
- The distribution of these phones is part of broader security upgrades and not connected to anything alarming.
- Beau encourages sending wild theories for fact-checking to prevent spreading misinformation.
- Addressing another issue, some believe a flag being laid out during an incident was staged, but it was likely done for documentation purposes by law enforcement.
- Beau advises reading the full articles cited in such theories to understand the context and authority behind the information.

### Quotes

- "If you are looking for a fact-check on some kind of wild theory that is spreading all over Facebook, send it to me right away."
- "There's no big mystery behind the phones. No big mystery behind the flag. All of this stuff is super normal."
- "It's dangerous to go along, here, take this."

### Oneliner

A fact-check on conspiracy theories involving satellite phones given to senators reveals routine security upgrades rather than a coup plot, urging early debunking requests.

### Audience

Social media users

### On-the-ground actions from transcript

- Send wild theories for fact-checking promptly (suggested)
- Read full articles cited in conspiracy theories for context (suggested)

### Whats missing in summary

The full transcript provides in-depth insights into fact-checking and understanding the context behind conspiracy theories circulating on social media platforms.

### Tags

#FactChecking #ConspiracyTheories #SenateSecurity #Misinformation #SocialMediaUsers


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about phones,
and flags, and fathers, and fact-checking.
Because I got a message,
and it's from somebody who doesn't want
to share bad information,
but is somebody who might do it unintentionally.
but is somebody who might do it unintentionally.
And they're kinda asking for a fact-check on something.
So we're going to give it to them.
Here's the message.
I started watching your videos because my daughter posts them below posts I share on
Facebook.
I've been waiting for one on the satellite phone senators were given.
Does your silence mean this is real or does it mean you haven't heard about it?
Could you email me back or make a video?
If you don't know what's being said, they claim the phones were given to senators to
be ready for a coup to keep Biden in office. It's from the name of a Facebook page that I'm
absolutely not going to advertise. It purports to be a home for
journalists. It's from this page and they've been wrong before.
Okay, so I spent about 25 minutes looking at the various claims.
Now the claims are all pretty much the same with the exception of why they were given.
First part of it is that half of senators were given this special secure satellite phone.
From there it deviates.
because there's a coup attempt coming from Biden, there's, you know, it's nukes,
it's chemicals, it's the train, and then once the whole U-Haul thing happened,
they tried to tie it into that as well. There's a bunch of different claims
circulating around this. Now, once I had a good read on what the claims were, I
timed myself to find out how long it was going to take to fact-check it. 12 minutes
46 seconds. Keep that in mind when you are viewing these outlets in the future.
Didn't take long to find the accurate information. Didn't take long at all.
They could have done this. Okay, so like any good theory, part of it has to be
true. There were satellite phones given to some senators. That's not really
unusual though. You know we've talked about cog plans on the channel before,
continuity of government plans. They exist for all kinds of things. So secure
communications being distributed, yeah I mean that's a thing. It could definitely
happen. Nothing unusual about that. What is weird is that it's only the Senate,
right? That's unusual. Like continuity of government without the House of
Representatives, that doesn't make any sense. And that only a limited number of
them got it. So that's where you start looking. The Senate, it's specific to the
Senate. What you will find out is that back in April the new sergeant of arms
at the Senate gave testimony about it. In relevant part it says you know
responsible for overseeing the engineering implementation and
operations for the Senate's information technology continuity and disaster
recovery programs as well as emergency and secure communications. This includes
the Senate's radio infrastructure, communication security, mobile command
vehicles, satellite communications, data center continuity, and support for
national security special events. Okay, so the reason these phones were given
It actually does have something to do with what I would call a coup attempt.
These are upgrades to Senate security because of January 6th.
That's what it's about.
That's why it's happening.
The new Sergeant of Arms there is implementing some new procedures.
That's why it's just the Senate, because it's the Senate Sergeant in Arms.
It's not a secret.
There was testimony about it more than a month ago.
That's all there is to it.
It's not a nefarious plot.
It's just upgrading their security.
It's dangerous to go along, here, take this.
The reason only a limited number of them got the phone was because it's voluntary.
They don't have to take the phone if they don't want it.
A little more than half at this point have taken the phone, but some of them just don't
want it.
They probably don't see the need or they figure their normal phones will work.
Maybe they have their own security, maybe they have their own satellite phones.
But given the fact that it wasn't mandatory, rest assured they are not on an elevated alert
status.
So this has been in the works since the new security chief there took over.
Now it's being distributed.
There's all kinds of other security upgrades going on.
So no, it's not anything to worry about.
It's not connected to anything else.
Okay, so next time don't wait.
If you are looking for a fact check on some kind of wild theory that is spreading all
over Facebook and you don't want to be somebody whose daughter says don't listen to my dad,
he gets his news from Facebook, send it to me right away.
I will happily go through and fact check anything like that.
It's seeing people realize that some of these outlets, and you're paying attention, they've
been wrong before, right? Seeing people start to realize that they're being kind
of farmed for clicks, that's really exciting for me. So I will happily fact
check anything else you have. Now let's talk about flags because while I was
scrolling looking at this, I found another one. The U-Haul thing. A whole
bunch of people are convinced that one of the reasons it's a setup is because
the flag was taken out and laid on the ground open like it was there for a
photo op, making sure that everybody could see it. To make, again, to make
those people look bad. Okay, here's the wild part. It was almost certainly
laid out like that for a photo op. That's true. Generally speaking, when you're
collecting evidence, you take a photo of it and you spread it out. Y'all have never seen
CSI? So there's just an extra one that at this moment is only on Twitter but I'm
sure eventually it'll make it to Facebook. Yeah, it was spread out. It was
probably spread out to be photographed by law enforcement to catalog all of
this stuff. You're not dealing with local cops. That was the Secret Service. They
document everything. I would imagine they probably did the same thing with just
about everything in that van. So there you go. No big mystery behind the phones.
No big mystery behind the flag. All of this stuff is super normal. When you get
this kind of information, one of the things that you should do is actually
read the full article that they cite. A lot of them, and it's true in this
case, they'll put out an article and they'll quote just a section of it. Find
the article that it's quoted from, because that's what gives it the authority, right?
CBS, I think, was the outlet, reported on the satellite phones.
And they just had one little quote for, uh, in the event of disruption, something along
those lines.
And it sounded really scary, but if you go actually read the original article, I mean,
it says everything that I just said with the exception of it was discussed in April in
And the testimony, it's new security measures because of January 6th, and there's more
coming.
It lays it all out pretty well.
That will help you fact check on your own.
But seriously, anytime you see something like this, send me a message.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}