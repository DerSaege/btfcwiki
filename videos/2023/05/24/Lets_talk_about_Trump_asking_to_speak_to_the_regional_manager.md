---
title: Let's talk about Trump asking to speak to the regional manager....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ISWUZ2w_kD0) |
| Published | 2023/05/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The special counsel's office is nearing a charging decision on the documents case involving Trump.
- Trump requested a meeting with Attorney General Merrick Garland and representatives of Congress, a move often made when a client is anticipating indictment.
- Recent news indicates evidence gathering related to potential trial rather than justifying charges in the documents case.
- Trump's request for a meeting seems like a panic-driven move, given the mounting negative news and his upcoming trial date in New York.
- Garland is unlikely to intervene in the special counsel's office's decision-making process, as seen in previous cases like Durham's investigation.
- Trump's actions, like not heeding counsel's advice to stay quiet, are contributing to his current predicament.
- Trump's belief that he will be indicted soon contrasts with public opinion on the matter.
- The timing of Trump's trial date in the New York case during the primaries is unfavorable for him politically.
- The recent developments suggest that there will be significant news regarding the documents case in the near future.

### Quotes

- "Is that justified? There has been a lot of news coming out little bits and pieces about evidence..."
- "At the end of this, Jack Smith is a special counsel, okay."
- "So he's been getting a whole lot of bad news lately."
- "Undoubtedly we will have some interesting news when it comes to the documents case in the coming weeks."

### Oneliner

The special counsel's office nears a decision on Trump, who, in a panic-driven move, seeks a meeting with Garland amid mounting negative news and an impending trial date, with significant developments expected soon.

### Audience

Political analysts, legal experts

### On-the-ground actions from transcript

- Contact your representatives to advocate for transparency and accountability in legal proceedings (implied).

### Whats missing in summary

Insights on how Trump's actions and decisions are influencing his legal situation and public perception.

### Tags

#Trump #DocumentsCase #SpecialCounsel #LegalProceedings #MerrickGarland


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Trump and the documents
case and some late breaking developments from last night
and panic.
Panic.
OK, so the Wall Street Journal put out a report.
and basically it indicated that the special counsel's office is close to a
charging decision on the documents case. Shortly thereafter Trump asked to speak
to the manager. A letter was posted to his wannabe Twitter that was addressed
to Merrick Garland and CC'd, representatives of Congress, asking for a
meeting. Now, this is not uncommon. When high-end attorneys feel like their
client is about to be indicted, they will often ask for a meeting with senior DOJ
people. And it's kind of like a last-ditch effort to stop their clients
being indicted. Generally speaking, the attorney general is not the person they
asked for a meeting with, but you know Trump needs to speak to not just the
manager, the regional manager I guess, going all the way to corporate with this.
It seems like a move that is motivated by panic. Is that justified? There has
been a lot of news coming out little bits and pieces about evidence that
doesn't really relate to a charging decision such as the archive stuff. You
know that's one of those things they don't have to prove the classification
stuff that they really don't it's not necessary for the for most of the
anticipated charges. But it would be useful to have all of that stuff on hand
if you were planning to indict and take him to trial because that is an apparent
defense. So most of the news that has been coming out in this case recently
has dealt with the special counsel's office gathering information that would
related to going to trial on this. So there is grounds for Trump to be worried
about this, absolutely. I do not think that a meeting with Merrick Garland,
especially the way in which he apparently has requested it, is gonna do
him a lot of good. I would give just about anything for Garland to respond
DOJ letterhead simply saying, sir this is a Wendy's. At the end of this, Jack
Smith is a special counsel, okay. The Attorney General is not going to get
involved in this. This is not a normal criminal case where sometimes senior DOJ
decide to intervene in certain ways. It seems incredibly unlikely. Generally
speaking, when you are talking about the special counsel's office, that doesn't
happen. It's worth noting that Garland didn't interfere with Durham and we saw
that report. So I don't think that this is going to bear fruit and whether or not
you or I believe that Trump is going to be indicted soon, I think it's safe to
say that Trump believes he's going to be indicted soon and all of this is coming
on the news of him getting his trial date for the New York case which is
basically smack in the middle of the primaries which is definitely not going
to help him. So he's been getting a whole lot of bad news lately. A lot of this
he's bringing on himself by not taking the advice of counsel that typically
occurs, which is be quiet. So undoubtedly we will have some interesting news when
it comes to the documents case in the coming weeks. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}