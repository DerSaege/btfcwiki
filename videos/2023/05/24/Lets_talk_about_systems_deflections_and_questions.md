---
title: Let's talk about systems, deflections, and questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vhFLoklZ-TE) |
| Published | 2023/05/24 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a question that arises frequently: why do individuals with darker skin tones support fascist groups by displaying symbols like flags or tattoos?
- Drawing parallels between the acceptance of patriarchy and the acceptance of systems of oppression like white supremacy.
- Explaining how victims of oppressive systems can sometimes support and defend those systems due to fear of change and adherence to the status quo.
- Mentioning individuals like Enrique Tario and Nick Fuentes as names associated with fascist movements in the US.
- Noting that the concept of "whiteness" in the US is fluid and has evolved over time to include different ethnic groups for the benefit of maintaining power structures.
- Emphasizing that systems of oppression are a means for certain groups to gain and maintain control by dividing people into in-groups and out-groups.
- Stressing that the ultimate goal of oppressive systems is not to keep a specific group down but to consolidate power and authority for those in control.
- Pointing out the link between individuals supporting oppressive systems and the perpetuation of authoritarianism.
- Encouraging further exploration of racial hierarchies and systems of oppression beyond the US borders.
- Concluding with the idea that individuals supporting oppressive systems, regardless of their background, are not anomalies but rather common occurrences.

### Quotes

- "The system itself isn't about the system. The system is a means to an end, power, authority, authoritarianism."
- "When it comes to systems of oppression, the goal isn't actually to keep a certain group of people down. That's not the end."
- "It's not some historical anomaly. It occurs, and it occurs in other systems as well, all the time."

### Oneliner

Addressing the question of why individuals with darker skin tones support fascist groups, Beau draws parallels between the acceptance of patriarchy and systems of oppression like white supremacy, illustrating how victims of oppressive systems can perpetuate them.

### Audience

Social justice advocates

### On-the-ground actions from transcript

- Research racial hierarchies and systems of oppression beyond the US borders (suggested)
- Have open, honest dialogues with individuals who may unknowingly support oppressive systems (implied)

### Whats missing in summary

Exploration of how societal norms and the fear of change contribute to individuals supporting oppressive systems despite being victims of those systems.

### Tags

#Oppression #Fascism #Patriarchy #WhiteSupremacy #Authoritarianism


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about a question that has come
in repeatedly.
And from some people, it's a genuine question.
Like, they really don't understand how it happens.
For other people, it's a deflection.
It's a way of protecting a group that they want protected.
The thing is, talking about it directly in the beginning is
hard because people have instinctive reactions to certain things. So rather
than talk about it directly at first, we're going to talk about something
else where the exact same thing happens all the time and nobody questions it.
Nobody sits there and says, hey that's weird, because it's still more prevalent
than the other. It's more, it is still more accepted. I don't know if prevalence is the right word, but we're going to
start by talking about the patriarchy. What is a patriarchy? A male-dominated society, let's keep it simple for right
now.
now. The US was literally founded as a patriarchy, but today you will find
conservative women who support it. Nobody really questions it, but it's incredibly
common to find women who will say, no, no, no, the man is supposed to take care of
that and do that stuff out there, I'm supposed to stay at home. You have
conservative political commentators who are women say things like, I don't know
if we ever should have done the 19th Amendment, actively going against their
own interests. You had some say there's no way that Hillary Clinton should be
president because you know a few days each month she's just going to be
emotional and uncontrollable, right? It's not uncommon. When you were talking about
that ideology, that movement, that system of oppression, it is incredibly common to
find people who are the victim of that system, the most pronounced victims of
of that system in agreement with it.
They may not call it patriarchy.
They'll call it tradition or something like that because
conservatives are afraid of change.
And it's the status quo. It's the social order that they grew up with.
And they've just
accepted it.
They've brought it inside of them and they said, yep, this is the way it's supposed to be.
And they'll defend it.
totally normal, right? Like, this isn't surprising. We've seen this enough to
realize it occurs. Okay, so now on to the questions that have actually come in. And
they deal with people who have darker skin tones supporting fasci groups to
extent of having flags or tattoos. They can't really be supremacists. They're
the wrong color. It would be bad for them. That kind of supremacy is a system,
system of oppression, just like the patriarchy, right? Just like the
patriarchy, you find people who will be victims of that system of oppression in
in support of it because it's the status quo, it's the way it's been.
It's not really hard to see the analogy here.
The other thing to kind of remember here is that the system itself isn't about the system.
The system is a means to an end, power, authority, authoritarianism.
Being on the in group of that might seem better than being on the out group.
If you were to ask people who track fasci movements in the United States, at like the
street level, the ground level, and you were to ask them, who are the first names in fasci
right now?
What names would you get?
Enrique Tario might be one, right?
That might be one that they say.
The other, I don't know, maybe Nick Fuentes?
It's not hard if you're not trying to deflect.
Or you just open up your mind to the idea that sometimes people do things against their
interests. The other thing to remember is that white in the context of the United
States, it's made up. It doesn't mean what people think it means. It changes over
time. Look at me. I'm super white, right? I mean like I'm the stereotypical white
did, but there was a point in time when I wouldn't have been the right kind. No
Irish need apply because it's fluid. There was a time when Italians occupied
this really weird middle ground in the south between white and black, but
eventually they were accepted as white. Why'd that happen? Because bigots needed
more numbers. The whole idea is to elevate a group of people to the top, so
you need to make sure you have the numbers to make that happen. What were
all the bigots talking about, what, two years ago? Birth rates, right? What might
be happening right now? Is it possible that people who originate south of our border are
going to start being included in the American definition of white? And it will be quickly
rationalized. Well, you know, their ancestors were from Spain. They're European. It's going
to be that easy and it'll give them the numbers to maintain the system. Now it's
also worth noting and I will have a video down below that goes into this in
pretty quick but pretty thorough detail talking about different systems south of
our border and how there was a racial hierarchy. Really interesting stuff. It
might be worth looking into if this is something you're interested in, if you have questions
about it.
Just remember that when it comes to systems of oppression, the goal isn't actually to
keep a certain group of people down.
That's not the end.
That's the means for the people who want control and power to get it.
They just have to convince the group that they trick into believing they're part of
the in group to kick down.
And then they can stay up top doing whatever they want.
It's the root of authoritarianism.
The fact that you have people who are not white supporting groups like this, it shouldn't
be a surprise.
It's not some historical anomaly.
It occurs, and it occurs in other systems as well, all the time.
So often that when it occurs, you don't think of them as being people who uphold the patriarchy.
Well, they're just old-fashioned.
Anyway, it's just a thought.
you all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}