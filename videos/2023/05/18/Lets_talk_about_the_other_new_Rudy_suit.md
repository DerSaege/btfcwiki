---
title: Let's talk about the other new Rudy suit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=eS_Dk0rocYA) |
| Published | 2023/05/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Rudy Giuliani is facing a new lawsuit brought in federal court by Daniel Gill for false arrest, civil rights conspiracy, defamation, and emotional distress.
- Gill's lawyer claims his client patted Giuliani on the back without malice to get his attention, as seen in viral video footage.
- Giuliani described the incident as feeling like he'd been shot or hit by a boulder.
- The lawsuit not only targets Giuliani but also the city and some police officers involved.
- Giuliani filed charges against Gill after the incident, leading to Gill's arrest.
- The lawsuit is likely to proceed swiftly due to the video evidence available.
- Multiple lawsuits are being brought against Giuliani currently, indicating a trend of legal actions against him.
- The outcome of these lawsuits will depend on court decisions and potentially jury rulings.
- The situation involving Giuliani and these legal challenges is ongoing and may continue in the future.
- Beau concludes by expressing his thoughts and wishing everyone a good day.

### Quotes

- "Our client merely patted Mr. Giuliani, who sustained nothing remotely resembling physical injuries."
- "Giuliani apparently was unaware of the footage."
- "It seems like there's a whole lot of finding out going on right now."

### Oneliner

Beau examines a new lawsuit against Rudy Giuliani, discussing false arrest, viral video evidence, and the ongoing legal challenges against Giuliani.

### Audience

Legal observers

### On-the-ground actions from transcript

- Watch the viral video footage to understand the context of the incident (suggested).
- Stay informed about the developments in the legal cases involving Rudy Giuliani (suggested).

### Whats missing in summary

The full transcript provides more context on the lawsuit against Rudy Giuliani and the details surrounding the incident, offering a comprehensive view of the situation.


## Transcript
Well, howdy there internet people, it's Bo again.
So today we're gonna talk about Rudy again.
He's having a bad week.
So we are going to talk about Rudy Giuliani
and another suit, an entirely separate one,
not the one that we discussed briefly the other day.
This is a different one
that was brought in federal court by Daniel Gill.
He is seeking monetary damages for false arrest,
civil rights conspiracy resulting in false arrest,
and false imprisonment, defamation,
intentional infliction of emotional distress,
and negligent infliction of emotional distress.
The incident in question is Gill, according to his lawyer.
Our client merely patted Mr. Giuliani, who sustained nothing remotely resembling physical injuries,
without malice, to simply get his attention, as the video footage clearly showed.
If you don't remember this, the footage went viral.
Gil walked up behind him, patted him on the back, and said what's up scumbag?
I mean, it wasn't nice, that's true.
But that's what happened.
Giuliani apparently was unaware of the footage.
Giuliani's description of it, I mean, I think he said it, he felt like he'd been shot
felt like he'd been shot or been hit by a boulder or something like that.
Now obviously it's up for the courts to decide, but you can review the footage yourself to
determine whether or not it was, you know, a massive blow to Rudy.
The suit is also, not just is it going after Rudy, it's going after the city and some
of the cops. Given that it is, you know, on film, I would imagine that this suit
will probably move forward relatively quickly. We'll probably get a
resolution on this one within a decent time frame. It's worth noting that after
that incident. Giuliani
actually, like, filed
charges. You know,
Gill got arrested and went through the process and all of this stuff.
And again, if you review the footage
you'll probably believe that was unwarranted.
But, you know, that's up to
a jury to decide. But this is just the latest suit being brought against Giuliani.
It seems like there's a whole lot of finding out going on right now. I would
imagine that that trend is going to continue for the foreseeable future.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}