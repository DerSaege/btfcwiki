---
title: Let's talk about TikTok and bad information....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=ToJck_yekKA) |
| Published | 2023/05/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Montana banned TikTok, sparking First Amendment arguments for residents and the company.
- A screenshot shared falsely claimed Montana residents faced penalties for using TikTok.
- The inaccurate information in the screenshot was attributed to Politico.
- Beau verified the screenshot's text on Google and found a corresponding Politico article.
- The actual article did not match the misleading screenshot.
- The screenshot intentionally misrepresented the information, possibly to stir drama or alter perceptions.
- Beau warns about being cautious with screenshots from mobile devices with banners.
- The new technique involves altering screenshots to create misinformation.
- Beau encourages people to verify information before reacting to misleading screenshots.
- Despite the controversy, Beau plans to continue following the TikTok ban situation in Montana.

### Quotes

- "The screenshot intentionally misrepresented the information."
- "Be cautious with screenshots from mobile devices with banners."
- "Verify information before reacting to misleading screenshots."

### Oneliner

Montana's TikTok ban sparked misinformation through intentionally misleading screenshots, urging caution and verification. 

### Audience

Social media users

### On-the-ground actions from transcript

- Verify information before sharing screenshots (implied)
- Be cautious with mobile device screenshots containing banners (implied)

### Whats missing in summary

The full transcript provides detailed insights into the spread of misinformation through altered screenshots and the importance of verification in combating false narratives.

### Tags

#Montana #TikTok #Misinformation #Screenshots #Verification


## Transcript
Well, howdy there internet people, it's Bo again. So today we are going to talk about Montana and
tik-tok and a
New technique I found that I think is being intentionally used to spread bad misinformation
So what happened?
Montana banned tik-tok
They banned TikTok. What do you need to know about that? That is definitely going to court, okay?
There's no doubt about that. That's going to court. There is a First Amendment argument on behalf of
the residents. There's a First Amendment argument on behalf of the company. I'm not even sure that
Montana can do this. This is going to court. But, that's not really what I want to talk about.
somebody sent me a screenshot and it has the Politico, the outlet Politico, has
their logo and it says 1.1 million residents risk facing penalties
enforced by the Montana Department of Justice. That penalty starts at an
initial $10,000 fine per violation and then $10,000 a day that a violation
continues. First, that's not the law. The law doesn't actually have any real enforcement as far
as the individual. Certainly not the residents. This is not the law. So that's odd. The other
thing that's odd is that no matter what you think of politico, as far as factual accuracy,
accuracy, you would not expect to find something this wrong on that outlet.
You may disagree with their commentary, their slant, their political takes, but when it
comes to factual accuracy, I mean this is a huge mistake.
So I didn't really think it was real.
So what I did was I went to Google and I typed in the text that appears on the screenshot.
And sure enough, it showed up on a Politico article.
And that was a little unnerving.
And I was sitting here thinking, I'm like, do I know anybody that works there that I
can reach out to because this is huge.
So I went to the article itself.
Let me read you the whole paragraph.
The ban will go into effect on January 1st, 2024, if courts don't act first, and any
app stores offering TikTok to the state's 1.1 million residents risk facing penalties
enforced by the Montana Department of Justice.
Okay.
So the article actually is accurate.
But the screenshot, they rolled the text down to where it just had the logo and started
it at the 1.1 million.
So they created a factual inaccuracy where there isn't one.
I don't think that was by accident.
I feel like that we may need to be on guard for that because, generally, when you see
a screenshot that is attributed to an outlet that just seems odd, you type in the text
to see if the article exists.
When you do it this way and there's that free-floating banner at the top and they just scroll down
to the text that they want, remove the initial part of the sentence.
If you just apply that and you don't investigate further, you'll type it in and that article
will exist.
I don't know how many people would go the extra step of actually re-reading the information
they think they've already seen.
So I would be on guard when it comes to screenshots that come from mobile devices that have a
banner up top. The banner where the logo is. And I will put the link to this
article down below so you can actually like go to it and see how they did it.
The screenshot started with 1.1 million residents. The actual sentence starts
with the phrase the band will go into effect on. And that just seems, it seems
very intentional. I don't think you could make a mistake like that while
taking a screenshot if you didn't mean to. Like if that doesn't seem like a
natural mistake. So just be aware there are people out there that will always
want to cause drama, they will always want to alter the way things are being
perceived for whatever reason, there's a new technique that they're using. So just
be on the lookout for it. As far as TikTok is concerned, yeah I'll keep
following it. I would imagine that they're not actually gonna be able to do
this at the state level, but I mean it's kind of new ground. We'll have to wait
can see. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}