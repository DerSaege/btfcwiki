---
title: Let's talk about Democratic messaging issues....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=DSNdeBznWtU) |
| Published | 2023/05/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Democratic Party struggles with messaging due to its diverse coalition of subgroups.
- The party is filled with politicians who may be out of touch with average Americans.
- The PR teams within the Democratic Party are often ideologically motivated and may lack relatability.
- Newer figures like Katie Porter and AOC are successful in messaging due to their effective use of social media and simple explanations.
- The Democratic Party sometimes fails to tailor its messaging to different regions and demographics.
- Democrats tend to focus on high-minded ideas, which can backfire when attacked by the opposition.
- The Republican Party motivates through fear, projecting strength rather than engaging in high-minded debates.
- Unaffiliated PACs handle the negative attacks that the Democratic Party avoids due to their high-minded approach.
- The Democratic Party struggles to explain popular policies in a simple, relatable manner.
- Beau points out the importance of understanding local politics and tailoring messaging accordingly.

### Quotes

- "The Democratic Party struggles with messaging due to its diverse coalition of subgroups."
- "Democrats tend to focus on high-minded ideas, which can backfire when attacked by the opposition."
- "The Republican Party motivates through fear, projecting strength rather than engaging in high-minded debates."
- "The Democratic Party struggles to explain popular policies in a simple, relatable manner."
- "Beau points out the importance of understanding local politics and tailoring messaging accordingly."

### Oneliner

Beau explains the Democratic Party's messaging struggles, attributing them to diverse subgroups, high-minded ideals, and a lack of local focus.

### Audience

Democratic strategists

### On-the-ground actions from transcript

- Break down policies in a simple, relatable manner (suggested)
- Tailor messaging to different regions and demographics (suggested)
- Utilize social media and simplified explanations for effective messaging (exemplified)

### What's missing in summary

A deep dive into how the Democratic Party can bridge the messaging gap and effectively communicate with a diverse electorate.

### Tags

#DemocraticParty #Messaging #PoliticalStrategy #CoalitionBuilding #LocalFocus


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the Democratic Party's messaging and answer a question that
came in about it and go over, I mean, for lack of a softer way of putting it, why the
Democratic Party is bad at messaging at times.
the message. I've been following the channel for a little over a year now, been a Patreon for about
half that. I've gone back through most of Bo's catalog, and one thing I've noticed is that Bo
is better at messaging than most of the Democratic establishment. My question is pretty simple.
How do politicians, specifically Democrats with entire PR teams, constantly put out subpar
messaging again and again, whereas Bo, who I know is not a Democrat, thank you, picks up on messages
and methods that would resonate with voters.
There are a couple of different components to this, but it's actually pretty simple.
The first part is to understand that the Democratic Party is a coalition party.
It has different subgroups within it.
It's not like the Republican Party, which is conservative, and that means all they really
have to do is say, I don't want things to change and change is scary. Okay? The
Democratic Party, you have liberals, progressives, urban liberals, dirt road
Democrats, leftists, you have all of these different groups. It's much harder to
create a unified message that all of the different candidates can get behind
because they have different ideological beliefs. That's a big part of it. Another
problem is that the Democratic Party is in fact full of politicians with
everything that that entails. All of the negative connotations that go with it.
the Democratic establishment, they're politicians. Rich, elitist people who it
may have been a really, really long time since they've interacted with like the
average American. And that impacts their messaging ability. Now obviously not all
Democrats, but that definitely plays into it. The PR people you're talking about,
They generally come into flavors. People who want to be politicians or true
believers. Either way, odds are they are very ideologically motivated. They fall
into one of those subgroups and they believe in that. A whole lot of them are
Ivy League educated and it creates some elitism, some classism that causes
problems with their messaging. For some, this is obviously the right
thing to do and the fact that you don't know that means you're bad and we're not
going to bother reaching out to you. And then for others, it's literally the
belief that everybody should already know this so they don't explain things.
Now again, not all Democrats, but I want to point something out. Katie Porter, okay,
AOC. Two relatively new people, up-and-comers, right? Household names. Why
are they different? What do they do differently? One uses social media, the
other one uses a whiteboard to explain things. To explain why the high-minded
progressive idea is actually good for the average person. That's why they're
household names, because they actually have better messaging than most. The
Democratic Party also thinks that some of its party platforms, its planks, things
that most Democrats agree on, are universal. You see it time and time
again where the Democratic Party runs a a candidate that has perfect messaging
for California, but they run them in the South. It's not the same. It's not going
to work. The things that would motivate a dirt road Democrat, they're not the
same things that would motivate a a liberal in New York or California. It
has to be addressed differently. The Democratic Party has a real hard time
with that because they have to create enthusiasm if they want to win in the
South. And then I think one of the biggest things that the Democratic
party messes up, is that when they go negative, they go negative about things
that would bother them. If it was said about them, it would bother them, and
that's kind of what they go to attack on, and it doesn't make sense. Because most
Democrats are progressive of some kind, they tend to have high-minded ideas.
being called a hypocrite that's that's really bad for them. The Republican
Party doesn't care. They do not care about being called a hypocrite. They
don't try to energize their base through high-minded ideas about philosophy or
principle or even policy. They motivate by fear. Fear of the other, fear of change.
They try to project strength. Don't call them a hypocrite, call them weak and
afraid. That's what would damage them. You show that their policy isn't
about keeping America strong. It's about making sure that America is terrified
and weak and they don't do that. The reason they don't do that is because a
lot of times in order to be effective those kinds of ads have to feed into
things that are in and of themselves a negative. If you are going to call a
politician weak and make fun of him because he says he's an alpha male and
you're going to show that he's not, well you're kind of feeding into toxic
masculinity, right? And the Democratic Party doesn't want to do that because
they'll be called hypocrites and that would bother them because they're
high-minded. That's what the unaffiliated PACs are for, just saying.
It's stuff like that. It's the Democratic Party trying to play fairer. To be
progressive in any way, you have to believe in the best in people. The
Democratic Party has a hard time acknowledging that they're not going up
against the best people, and they don't explain the policies that they have that are just
overwhelmingly popular if they were explained.
When people are asked simple questions about do you support X, well yes, yeah of course
I support that, that's a good idea.
But the Democratic Party doesn't portray it as supporting X, they try to put it into
this high-minded philosophy.
And they don't break down things that most people don't know about.
Perfect example of that was that thing the other day.
So many questions from people saying, hey, how is a 1% increase a cut?
You had Democrats all over the country saying that an increase was a cut.
course they look nuts. Of course that messaging fell flat because it doesn't
make any sense. They have to explain how that's a cut because it's not keeping up
with inflation. Katie Porter needed to break out a whiteboard. It's a little
stuff like that and not remembering that all politics is local and trying to
create that larger platform when they have a coalition. They have to
acknowledge that. I don't run into any of those issues because it's just me, so.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}