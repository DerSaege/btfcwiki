---
title: Let's talk about the new Trump evidence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fFABLwBMaWY) |
| Published | 2023/05/18 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The National Archives plans to hand over 16 records involving close presidential advisors to the special counsel's office, prompting potential serious charges or trial preparation.
- Special Counsel's office may be looking at a more severe charge or preparing for court based on the evidence, not just investigating willful retention.
- Willful retention involves national defense information, not just classified information, and evidence may not be necessary for the charges initially assumed.
- Trump's claim of having a standing order to declassify anything he takes from a room is not valid and may not hold up in court.
- Special Counsel's office may be trying to show Trump intentionally removed certain records and looking for a motive behind it.
- The evidence appears to be unnecessary for the charges expected from the documents case, pointing towards potential serious charges.
- Trump has the chance to fight this in court, but it may not be successful.
- There might be more surprises in this case as the reason behind obtaining this evidence seems unclear.

### Quotes

- "They're looking for a more severe charge, and that may be why it is taking a little bit longer than expected."
- "There's not a real logical reason for them trying to get this."
- "This seems like it's probably them looking at a more serious charge or they're getting ready for court."
- "I don't think it will go anywhere."
- "Sure, maybe they're just making it airtight, but I think the more likely answer is that they're looking for a more severe charge."

### Oneliner

The evidence handed over to the special counsel's office could point towards potential serious charges, not just investigating willful retention, with unclear motives behind it.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Prepare for potential legal proceedings by staying informed on the developments (implied)
- Stay updated on the case to understand the implications of the evidence being handed over (implied)

### Whats missing in summary

Context on the broader implications of the evidence and potential legal ramifications.

### Tags

#Trump #SpecialCounsel #LegalCharges #NationalArchives #Evidence


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the new Trump evidence, what it is, what it
means and what it really means because the, the evidence itself, it seems kind
of like it would be part of the investigation, but not really.
So, if you've missed it, the National Archives has 16 records that they plan on turning over
to the special counsel's office.
They sent a letter to Trump basically saying, hey, unless you intervene in the courts, we're
turning this over.
It says, the 16 records in question all reflect communications involving close presidential
advisors, some of them directed to you personally, you personally meaning Trump,
concerning whether, why, and how you should declassify certain classified
records. Okay, so obviously this has to do with the documents case. I mean that
much as apparent, right? But there's something else here. For most of the time
we've been covering this, it has seemed pretty apparent that the Special
Counsel is looking at willful retention as a charge. You don't need this for
that. You do not need this for that charge. This is not about investigating
for that charge. This is for one of two other things. Either the special counsel's
office has decided to pursue a more serious charge or they're already
prepping for the trial. When it comes to willful retention, it's basically, hey
you've got national defense information, give it back. No. Okay. You broke the law.
It's really kind of that simple and it's willful retention of national defense
information, not of classified information. Those two things aren't the
same. Information can be national defense information without being classified. You
You don't have to prove that the stuff was classified for it to count as national defense
information, therefore it's not necessary to talk about his magical declassification
process for willful retention.
This is probably them looking at a more serious charge or they're getting ready for court
And they're assuming that his defense in court is going to be what he has said in public.
I will go ahead and tell you, that isn't going to hold up.
This seems like it would be material explaining how the declassification process actually
works.
Trump's quote, standing order of declassifying anything that he takes from the room, that's
That's not a thing.
But if you're going to go to trial, and since he has repeatedly said this, I can declassify
something by thinking about it.
They're going to have to be prepared for that in court.
But you're still left with that question, why are they even worrying about it?
they don't need to prove that for the charge that we have believed is most likely, willful
retention.
They don't have to prove this.
Sure, maybe they're just making it airtight, but I think the more likely answer is that
they're looking for a more severe charge, and that may be why it is taking a little
bit longer than expected.
Because as far as willful retention goes, there was enough to charge on that publicly
available shortly after the search.
So your best explanation for this is that they're looking for a more serious charge,
which there aren't many that would really require this.
At this point, it seems like they're trying to demonstrate that he intentionally removed
them.
To do that, you would have to assume that the special counsel's office also has some
motive as to why he did it.
We don't know that part yet, but this information isn't necessary for the charges that we assumed
were going to kind of follow on from the documents case.
This would only be necessary for more serious charges.
So we'll have to wait and see what he does because he does have the opportunity to try
to fight this in court.
I don't think it will go anywhere.
But he has that chance.
I would just, I would expect that there might be some more surprises in this case.
Because there's not a real logical reason for them trying to get this.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}