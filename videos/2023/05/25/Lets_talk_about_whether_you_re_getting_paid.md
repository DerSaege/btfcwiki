---
title: Let's talk about whether you're getting paid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=lfnabxTGKQ4) |
| Published | 2023/05/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urgent talk about the impending debt ceiling crisis, with the possibility of the US defaulting on the 1st, just days away.
- Major payments like Medicare, VA benefits, military pay, and Social Security are at risk if the debt ceiling issue isn't resolved.
- Individuals receiving checks need to be cautious with spending until the situation is resolved.
- Blames Republicans in the House for not raising the debt ceiling, using people's anxiety and fear as leverage against Biden.
- Republicans could have easily sent a clean debt ceiling proposal, but they chose not to, preferring to make the public suffer.
- Criticizes Republicans for not presenting a real budget and using citizens as leverage instead of negotiating in good faith.
- Calls out McCarthy for deflecting blame, reminding that the House is responsible for the deadlock.
- Encourages people to prepare for the possibility of payments not going out if the issue isn't resolved soon.
- Stresses the importance of Republicans caring about the country to resolve the crisis.
- Expresses hope for a resolution but warns about potential payment disruptions if action isn't taken.

### Quotes

- "If you get that [payment] check, you need to be as thrifty as possible with it until this is resolved."
- "They could have sent up a clean debt ceiling. They could do it at any point in time."
- "They wanted to use you as leverage. They wanted to damage your economic stability."
- "All it takes is for the Republican party to actually care about the country for once."
- "I'm still pretty hopeful that this gets resolved."

### Oneliner

The impending debt ceiling crisis looms, with Republicans blamed for holding up a resolution and jeopardizing vital payments.

### Audience

US Citizens

### On-the-ground actions from transcript

- Contact your representatives and urge them to prioritize resolving the debt ceiling issue (suggested).
- Stay informed about the situation and its potential impacts on your payments and benefits (implied).

### Whats missing in summary

Further details on the potential consequences of a failure to raise the debt ceiling and the broader economic implications.

### Tags

#DebtCeiling #USGovernment #RepublicanParty #FinancialCrisis #EconomicStability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about the debt ceiling
and whether or not you're going to get paid.
We have to talk about the elephant in the room
because we're kind of down to the wire here.
The earliest that the US could default is on the 1st,
just a few short days away.
That's the earliest it could happen,
according to the projections.
Now on the first and second, a whole bunch of money goes out.
Medicare, VA benefits, military pay, retirement pay,
Social Security, Medicaid, all of that.
All that money starts going out.
If you get that check, you need to be as thrifty as possible
with it until this is resolved.
because if it is not, it will be the last one.
That's where they let it get to.
That's where we're at.
If you are wondering who to blame, it's the Republicans in the House.
Biden can't magically raise the debt ceiling.
I mean, the 14th Amendment thing's on the table, but we'll have to wait
see. Through normal process, it has to go through the house. They could have sent up a clean debt
ceiling at any point in time, but they want to use your anxiety, your fear. They want to use you
as leverage against Biden. Do what we say or all these people are going to get it. That's what's
going on. They could have sent up a clean debt ceiling. They could do it at any point in time.
that it would fly through the process, be in Biden's hand probably within 24
hours, but they're not going to because they want you to suffer. That's how they
can get their way. That's the way they see it. They could have sent up a clean
debt ceiling. They could have, I don't know, tried with a real budget instead of
that imaginary one. They didn't even have to pass that. They could have wrote
something down on a cocktail napkin and gone to Biden with an actual budget and tried to
negotiate from there.
But they didn't want to do that.
They wanted to use you as leverage.
They wanted to damage your economic stability.
And now you have McCarthy out there daily, oh, it's not my fault.
You're the Speaker of the House.
It's the House that's holding it up.
I mean, he wanted the job.
I'm still pretty hopeful that this gets resolved, but at this point, people need to start thinking
about what they're going to do if it doesn't get resolved because, I mean, there's a chance
that these don't go out.
But realistically these will probably, these payments will probably be made.
But there won't be more after that unless this is resolved.
And it could be resolved at any point in time.
All it takes is for the Republican party to actually care about the country for once.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}