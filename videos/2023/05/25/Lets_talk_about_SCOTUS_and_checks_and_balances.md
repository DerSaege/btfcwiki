---
title: Let's talk about SCOTUS and checks and balances....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PMdt35vXpGM) |
| Published | 2023/05/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the Supreme Court, branches of government, and Congress's power to legislate about other branches.
- Chief Justice Roberts considering new ethical standards and Harlan Crowe dismissing Congress's legislative interest in his friendships.
- Congress's authority to legislate and its impact on the court's independence.
- Pelosi's subpoena power to call Justice Roberts for a hearing investigation.
- The necessity for Congress to show a legislative interest to conduct a hearing investigation.
- The concept of independent but not immune branches of government with checks and balances.
- Comparing Congress enforcing ethics on the Supreme Court to the Presidential Records Act enforced after Nixon.
- Congress's power to impose ethical standards and the need for Roberts to self-regulate to keep Congress out of the Supreme Court's internal affairs.
- Congress's ability to alter aspects of the Supreme Court except those outlined in the U.S. Constitution.
- Speculation on Roberts' intention to self-regulate and the ongoing saga regarding ethical standards and the relationship between Harlan Crow and Clarence Thomas.

### Quotes

- "Independent doesn't mean immune from, the system was set up to have checks and balances."
- "The whole idea is to have each branch checks the others."
- "They absolutely have the power to impose ethical standards they have in the past, and I'm sure they will in the future."
- "Congress can actually alter a lot of things about the Supreme Court."
- "I definitely don't think this saga is over."

### Oneliner

Beau dives into the Supreme Court, Congress's authority, ethical standards, and the need for checks and balances in a system of independent branches.

### Audience

Civics enthusiasts

### On-the-ground actions from transcript

- Stay informed on the developments regarding Congress's legislative interest in ethical standards for the Supreme Court (implied).

### Whats missing in summary

Insight into the potential future implications of Congress's actions and the Supreme Court's response.

### Tags

#SupremeCourt #Congress #ChecksAndBalances #EthicalStandards #Legislation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the Supreme Court.
We are going to talk about the branches of government
and whether or not Congress has power to legislate
about the other branches.
We're going to do this because we got a message.
What do you think of Roberts, Chief Justice Roberts,
saying he was considering new ethical standards and Harlan Crowe saying Congress doesn't have a
legislative interest in his friendships. Does Congress actually have power to legislate
here or does it jeopardize the court's independence as a branch of government? Okay, so
Roberts has said that he is considering new ethical standards. This of course is
occurring after Pelosi has returned and with her subpoena power that could be
used to actually call Justice Roberts in rather than just invite him. As far as
Harlan Crowe saying Congress doesn't have a legislative interest in his
friendships. I mean that's what I would say too if I was him because they do in
order to conduct a hearing investigation that type of thing they have to show
that they have a legislative interest and to my knowledge they actually
haven't put pen to paper on that. I definitely think they do but he would
end up making them jump through the hoops on that. And then the interesting
question. Does Congress actually have the power to legislate here or does it
jeopardize the court's independence as a branch of government? Three, independent
co-equal branches of government. Independent doesn't mean immune from, the
system was set up to have checks and balances. The branches are supposed to
check each other. They are absolutely supposed to check each other. As far as
Congress forcing ethical standards on the Supreme Court? Is that within bounds?
Well, I think most people today in the United States are familiar with
something called the Presidential Records Act. Do you know why it exists?
Nixon. The Presidential Records Act is Congress enforcing ethics on another
branch. It's not outside of their purview. The whole idea is to have each branch
check the others. So, I mean, that's, I mean, this is actually as close to, you
know, a functioning checks and balances as we've seen in a really long time. So,
yeah, they do have a legislative interest, but they have to actually state it. They
They absolutely have the power to impose ethical standards they have in the past, and I'm sure
they will in the future.
I don't know that it will happen here because whatever it is, we'll have to get through
the house.
And Roberts, his best move is probably to self-regulate here.
he is actually interested in keeping Congress out of the Supreme Court's
internal affairs, yeah, he should probably do something on his own. That
would be the the best course of action because they absolutely do have that
ability. They have done it before. Congress can actually alter a lot of
things about the Supreme Court, because really the only things that Congress can't alter
are the few items outlined in the U.S. Constitution.
Everything else is within their purview because they are supposed to act as a check against
judicial tyranny, just as the Supreme Court is supposed to act as a check against legislative tyranny.
So, my guess is this public statement from Roberts is an indication that he does plan on self-regulating.
So, we'll have to wait and see. I definitely don't think this saga is over. I do not think
Congress is going to let the Harlan Crow Clarence Thomas thing go? I think that's
going to be around for a while. As far as imposing ethical standards, Roberts is
probably going to do that on his own, I would think. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}