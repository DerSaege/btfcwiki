---
title: Let's talk about DeSantis and a failure to launch....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=9gYLTZceLj0) |
| Published | 2023/05/25 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor of Florida launched his presidential campaign in a SpaceX-like event.
- The launch faced technical difficulties and plummeted in listener numbers.
- Despite Musk's promotion, the event had only 161,000 to 250,000 listeners.
- DeSantis may try to spin the low numbers due to crashing Twitter servers.
- Comparisons show DeSantis won't draw crowds like Trump did.
- DeSantis is seen as a mini-Trump with a similar leadership style.
- DeSantis' success depends on Trump not running or losing support.
- DeSantis needs a significant shift in Trump's status for a chance at success.

### Quotes

- "For an announcement of this size, that's a really bad sign."
- "You can't out-Trump Trump."
- "His only real hope is for Trump to no longer be in the race."
- "The enthusiasm, 161,000 to 250,000 people. Those are not numbers that are going to be able to defeat Trump."
- "This was his opening announcement. This was his launch."

### Oneliner

Governor DeSantis' lackluster presidential campaign launch hints at challenges in competing with Trump's shadow and style.

### Audience

Political Analysts

### On-the-ground actions from transcript

- Analyze campaign strategies realistically (suggested)
- Monitor shifts in public sentiment towards political figures (suggested)

### Whats missing in summary

Insights on the potential impact of unforeseen events on political dynamics.

### Tags

#DeSantis #Trump #PresidentialCampaign #PoliticalStrategy #Election2024


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a launch,
or a failure to launch, depending on how you view it.
We're gonna talk about numbers,
we are going to talk about realities
and just how things are likely to move going forward.
And just kind of run through the events
that occurred earlier tonight.
Okay, so if you missed it on Twitter via their spaces thing, which is like an audio livestream,
the current governor of Florida launched his campaign for presidency.
Launch in the way that SpaceX launched the Starship.
I mean, that's not fair.
The Starship rocket got off the platform.
OK, so what happened?
The space opened.
It was advertised and pushed.
Musk himself pushed it.
So it should have had huge numbers, massive numbers,
because you're talking about not just the potential power
DeSantis, but Musk as well. And then there were a whole bunch of other right-wing
commentators and political figures that were involved. Almost as soon as it
started, it started having technical difficulties. It was horrible, just
absolutely horrible. It was a train wreck. It was really bad. When it
started, it had somewhere between half a million and six hundred thousand
listeners. Once the issues were resolved, those numbers had dropped to two
hundred and fifty thousand to a hundred and sixty one thousand, somewhere in there. Those
are not good numbers. Those are not winning numbers. May sound like a lot of
people, those are not good numbers because you're not just talking about
people who were interested in what DeSantis had to say. You're talking about
people who were on Twitter who are Musk, you know, fanatics. Those are not good
numbers. So the obvious play for DeSantis at this point is to say we had so many
people on there, we crashed Twitter servers. Yeah, that's how good we are. I
mean sure there may be some truth to that but that's that's an indictment of
Twitter's infrastructure at this point having you know gotten rid of a whole
a lot of people, more than an indication of his popularity, only having a quarter
million people being interested enough to stick around for a 20 minute technical
issue, that's not good.
When he gets out there and starts campaigning, he is not going to pack the
house the way that Trump did. Think about what people have to go through to get to
one of Trump's events. It's a lot more than just sitting around at home, right?
And there's a very long wait. They couldn't handle 20 minutes without losing
you know more than half the audience. For an announcement of this size, that's a
really bad sign, especially when you had Musk there promoting it. And this is,
Twitter has evolved into Musk's personal play toy. There should have been more
people. There should have been more people initially and certainly more
should have hung around afterward. I would, I would guess that most of those
people who hung around through the 20-minute glitch were there to hear
Musk, not DeSantis. So overall that's a bad sign, but realistically as long as
Trump is in the race, DeSantis is going to be second place. I don't really see
any way for that to change. DeSantis is mini-Trump, you know, he's a Trump clone.
He's using that same brash authoritarian style of leadership, trying to push that
same platform and image. If Trump is in the race, people who liked that, they're
going to go with Trump. You can't out-Trump Trump. So his only real hope is for Trump
to no longer be in the race. And there are a bunch of ways that that could actually happen.
So at this point, after seeing this, DeSantis' best hope is that it isn't some grand GOP
strategy.
It's not bringing in, you know, political analysts to tell him how to run things.
better hope that Jack Smith is really good at his job because I think that's
probably going to be the only thing that that will allow DeSantis to eclipse
Trump. The other thing he has to worry about here is that if something
happens with Trump and it causes people to lose faith in Trump. It might send
them away from other people who have that same style of leadership. Republicans
may just be like, it is just not worth it to deal with this. And because
DeSantis has modeled himself so closely, not modeled himself, modeled his
political image so closely on Trump that there will probably be a whole lot of
Republicans who see the two as indistinguishable. The level of
excitement really wasn't there. The enthusiasm, 161,000 to 250,000 people.
Those are not numbers that are going to be able to defeat Trump.
This was his opening announcement.
This was his launch.
It was well advertised.
Everybody knew about it.
If you were a supporter, even remotely, and you were on Twitter, you could have it playing
in the background.
It's just not there.
I don't know that there is any political maneuvering that can really help DeSantis at this point.
Nothing that he can do.
The thing that can happen that could alter this is people fleeing Trump for whatever
reason.
Due to an indictment, another scandal, he finally says something that the Republican
base just can't deal with.
But then the worry for DeSantis' team is when they flee Trump, do they run so fast that
they run right past people that are similar?
We'll have to wait and see, but this was not a good start.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}