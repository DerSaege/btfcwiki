---
title: Let's talk about brains, reputation, and Elon Musk....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=wVz_XuEw-Zs) |
| Published | 2023/05/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Harris Poll rates companies based on their reputation, with Tesla dropping from 11th to 62nd place.
- Speculation arises that Elon Musk's Twitter behavior may have influenced Tesla's reputation.
- Neuralink, a company Musk is associated with, has received FDA approval for human studies on brain chips.
- Despite controversy, Neuralink's technology has potential applications in healthcare and commercial sectors.
- Concerns are raised about Musk's personal perception impacting companies he's associated with.
- Musk's shift from being seen as a humanitarian to endorsing wild conspiracies may harm his reputation and affiliated companies.

### Quotes

- "There was a time not too long ago where he was kind of portrayed as somebody out to really better humanity."
- "And now he has tweets that dabble in the wilder conspiracies."

### Oneliner

The Harris Poll rates Tesla's reputation drop, prompting speculation on Elon Musk's influence and Neuralink's FDA approval for brain chips.

### Audience

Business analysts, tech enthusiasts

### On-the-ground actions from transcript

- Research Neuralink's developments and controversies (suggested)
- Stay informed about how public perception impacts companies (implied)

### Whats missing in summary

The full transcript provides more insight into Elon Musk's shifting public image and the potential consequences for companies he's associated with.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about polls
and reputation and brains and Elon Musk
and what all of these things have to do with each other.
The Harris Poll came out
and this is a poll that rates companies
based on their reputation.
The more reputable the company is perceived to be,
the better the score.
Tesla, last year, occupied the number 11 spot.
That's really good.
That is really good.
That's a fantastic spot to be on.
This year, they have fallen to, I want to say, 62.
That's fair, I guess.
puts them in the same category as Bed Bath and Beyond.
Now, there's no way to know for certain from the poll what
exactly caused the change in perception when it comes to
the reputation of the company.
But you might be forgiven for thinking that it could
possibly have something to do with Elon Musk's antics on
Twitter, and that his statements and his very visible presence now is starting to
impact how other companies that he's associated with are viewed. In other
news, Neuralink, which is a company that Musk is associated with, that basically
wants to put chips in people's brains and kind of link them through Bluetooth to
other things. It has reportedly obtained FDA approval to begin kind of like human
studies. Now I will be honest the health care implications of this product, pretty
cool, pretty cool. There's a lot of controversy about the company in a whole
bunch of different ways, feel free to look into it. But the final applications,
I mean we're talking about stuff that might help people see or walk, it's unique.
The technology itself could also be applied to commercial applications at
some other point. It might be something that could help you, you know, direct your
Tesla autopilot or log into Twitter spaces. But the question people are now
have to ask is whether or not Musk's perception as a person is going to start
impacting all of the other companies that he's associated with. You know there
There was a time not too long ago where he was kind of portrayed as somebody out to really
better humanity.
And now he has tweets that dabble in the wilder conspiracies.
And over time, the reputation that Musk enjoyed is probably going to deteriorate unless he
either takes a much less visible role or alters his views on some things.
the timing of all of this seemed worth mentioning. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}