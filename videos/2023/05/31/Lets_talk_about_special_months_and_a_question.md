---
title: Let's talk about special months and a question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4HJanN8xGFI) |
| Published | 2023/05/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the controversy around special months in the United States, particularly regarding Pride Month and Military Appreciation Month.
- Responding to a question from an individual whose dad criticized Pride Month, questioning why "the gays" have a month when veterans only have a day.
- Advising the individual on how to respond to their dad's comments, suggesting pointing out that May is Military Appreciation Month.
- Emphasizing that individuals parroting such statements online may not have accurate information and encouraging a common-sense approach to the topic.
- Noting that veterans actually have several days and an entire month dedicated to appreciation.
- Sharing a combat veteran's response to the issue, criticizing those who politicize veteran-related matters.
- Suggesting ways to address the misinformation about Military Appreciation Month, either in May or when it circulates heavily in the following month.
- Encouraging questioning the source of misinformation and prompting critical thinking about blindly believing such claims.
- Pointing out that using veterans as a prop for political purposes is manipulative and misleading.
- Urging individuals to challenge misleading sources and attempt to reach out to them with accurate information.

### Quotes

- "May is military appreciation month. If you post something like this today, go have fun with yourself. Signed, a combat veteran."
- "They're asking for something that already exists, which means they really don't care."
- "Maybe pointing out that they're literally using veterans as a prop, it might upset them."

### Oneliner

Beau addresses controversy over special months in the US, providing a thoughtful response to misinformation about Pride Month and Military Appreciation Month.

### Audience

Family members, allies

### On-the-ground actions from transcript

- Challenge misinformation about special months (suggested)
- Educate others about Military Appreciation Month (implied)
- Encourage critical thinking and fact-checking (suggested)

### Whats missing in summary

The full transcript provides a comprehensive guide on responding to misinformation and promoting understanding around special months in the US.

### Tags

#PrideMonth #MilitaryAppreciation #Veterans #Misinformation #CommunityEngagement


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about special months
in the United States and celebrating those months.
And we're gonna go over a question I received.
This video will be sent to the person who sent the message
ahead of time before it publishes
to make sure I don't say anything I'm not supposed to
and to make sure that they get it in time.
I have a feeling other people may be dealing
with similar questions over the next month or so,
so it still feels like it should be a video.
Okay, so here's the message.
My dad just went off on Pride Month,
saying there's no way, quote, the gays,
should get a month when veterans only get a day.
My dad thinks I'm a tomboy
and has no idea I'm one of, quote, the gays.
So I have to be creative
when talking to him about this stuff.
I need a response to this and don't know what to say.
OK.
So you have one day to do this.
Walk up to him and say, happy Military Appreciation Month.
It's May.
May.
The military does have an entire month.
It's May.
It's the month we're in.
Now, just so you know, this isn't your dad talking.
And for anybody else who's going to hear this over the next month because of the coming
recognition, understand this is your dad, your uncle, whoever, parroting something that
they heard online, a meme that they saw.
But it gives you an opportunity to actually kind of inject maybe a little bit of common
sense to the conversation.
What happened was this went out on Twitter and then it made it to Facebook.
People saying that this was the case, that the military only got a day, the veterans
only got a day.
I would point out that even excluding Military Appreciation Month, veterans, people in the
military get Memorial Day, Armed Forces Day, and Veterans Day, and then they also
get Military Appreciation Month. Since most of you, or at least a large portion
of you, know who Big Red Kelt is, I'm gonna read his response.
Conservatives love to ignore veteran issues while simultaneously using us as
prop for... I'm going to clean this up. I just... yeah. As I go through this, just fill in any
changes with something that a combat vet might say. Conservatives love to ignore veteran issues
while simultaneously using us as a prop for their stuff. May is military appreciation month.
If you post something like this today, go have fun with yourself. Signed, a combat veteran.
This really circulated a lot on Memorial Day, which is the way wrong time for something like this, as far as actually
reaching out to the military community.  Not a good day to politicize, just saying.
So, once you make this point, if this goes out in May, and you see this video in May,
video in May, and you run into this, you know, just say, hey, happy Military Appreciation
Month.
If you don't have that opportunity or it happens next month, which is when I imagine it's going
to circulate a lot, that's the moment to ask them.
Who told them that, veterans, that the military didn't have a month?
And then ask why they would ever believe them again.
This is an obvious ploy.
They're asking for something that already exists, which means they really don't care.
If they cared, they would already know it happens, that it's a thing.
It's obviously a way to play people, to manipulate them.
And if this is a person who your dad, uncle, whoever looks up to, maybe they shouldn't.
And maybe pointing out that they're literally using veterans as a prop, it might upset them.
It might be one of those things that helps erode confidence in that source, a source
that is actively misleading them.
So while it's going to obviously not be great to hear it, there's an opportunity to try
to reach them.
Anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}