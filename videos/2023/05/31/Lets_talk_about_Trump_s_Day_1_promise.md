---
title: Let's talk about Trump's Day 1 promise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=z_il5VlcAP8) |
| Published | 2023/05/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Donald Trump's promise to sign an executive order on his first day in office to deny automatic US citizenship to the children of illegal aliens.
- Trump's plan involves altering the US Constitution, specifically the 14th Amendment, through an executive order.
- The oath of office Trump took to uphold and defend the Constitution seems contradictory to his plan.
- Trump's disregard for the Constitution is evident in his promise to undermine it for his own power.
- Trump's focus is on scapegoating people and gaining power for himself, rather than upholding constitutional values.

### Quotes

- "He's promising to undo the US Constitution with an executive order on day one."
- "He absolutely would try because he doesn't care about the Constitution. He doesn't care about the country."
- "He has promised on day one to undermine the U.S. Constitution."

### Oneliner

Donald Trump plans to undermine the US Constitution by signing an executive order on his first day in office, showing his disregard for the country's foundational values and his pursuit of power through scapegoating.

### Audience

Voters, Constitution defenders

### On-the-ground actions from transcript

- Defend the US Constitution by advocating for its values and principles (implied).

### Whats missing in summary

The full transcript provides a deeper analysis of Trump's intentions and how they conflict with constitutional values, which can be better understood by watching the entire video.

### Tags

#DonaldTrump #USConstitution #Citizenship #ExecutiveOrder #Presidency


## Transcript
Well, howdy there, internet people, Lidsbo again.
So today we are going to talk about Donald J.
Trump's promise, his campaign pledge on what he plans to do on his first day in
office, if he is elected back to the office of the presidency.
And it is a doozy.
He says, as part of my plan to secure the border on day one, I will sign an
executive order making clear to federal agencies that under the correct
interpretation of the law going forward the future children of illegal aliens
will not receive automatic US citizenship. Okay, I would like to read you
something else. All persons born or naturalized in the United States and
subject to the jurisdiction thereof are citizens of the United States and the
state wherein they reside. That is the US Constitution. The former president of the
United States plans to use an executive order to alter the US Constitution if
elected again. I'm gonna read one more thing. I do solemnly swear that I will
faithfully execute the office of the presidency of the United States, and will, to the best of my
ability, preserve, protect, and defend the Constitution of the United States."
Okay, so you are left with just a few options when it comes to the former president. Either
he swore an oath to uphold a document he never read or didn't understand on the off chance he
did read it, he couldn't make out those words, or he is aware of what's in the
Constitution and plans to undermine it via an executive order and him just
dictating what he thinks the laws should be. I think there's probably a word for
that somewhere. He's promising to undo the US Constitution with an executive order
on day one. Please remember that Trump is not somebody who... he's not somebody who
you could write this off as just wild rhetoric. He absolutely would try
because he doesn't care about the Constitution. He doesn't care about the
country. He cares about scapegoating people and using them as a base with
which to obtain power, power for himself and nobody else.
He has promised on day one to undermine the U.S. Constitution.
Anyway, it's just a thought.
and y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}