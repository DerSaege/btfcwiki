---
title: Let's talk about Disney's non-move....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=XDKKjwezOeo) |
| Published | 2023/05/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Disney decided not to move forward with a project that involved putting 2,000 Imagineers in Florida, leading to significant economic implications.
- Speculation surrounds whether Disney's decision was influenced by political factors in Florida, but there is no concrete evidence to support this.
- The project cancellation will result in a loss of $200 million annually from the economy due to the high salaries of the Imagineers.
- The economic impact goes beyond just salaries, including the money spent by the company in the area and the ripple effects on local businesses.
- The decision not only affects the employees but also has repercussions on various other aspects like landscapers, truckers, and overall economic activity in the region.
- The long-lasting effects of this decision will result in billions of dollars not being pumped into Florida's economy.
- Similar situations, with less attention, occur frequently, impacting regions without making headlines.
- Beau mentions a personal connection to a situation where a project was diverted from Florida to Georgia, Minnesota, or Maine based on future political conditions.
- These decisions have substantial long-term impacts on economically disadvantaged areas, causing significant financial losses.
- Companies make decisions based on future-oriented policies, and regions that do not adapt risk losing out on profitable ventures.

### Quotes

- "It is cheaper to be a good person."
- "When Disney puts something somewhere, it stays."
- "The amount of money that is lost because of it is huge."
- "These decisions have long-term impacts for an area that is economically disadvantaged."
- "Companies that look towards the future, institutions that look towards the future, they're not going to want to be there."

### Oneliner

Disney's decision not to proceed with a project in Florida showcases the massive economic repercussions and long-lasting impacts on regions, reflecting the broader consequences of political climates on business decisions.

### Audience

Economic policymakers

### On-the-ground actions from transcript

- Advocate for policies that support future-oriented companies and attract profitable ventures (implied)
- Support economically disadvantaged areas facing substantial financial losses (implied)
- Stay informed about the economic impacts of business decisions influenced by political climates (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how business decisions are influenced by political climates, resulting in significant economic repercussions for regions and underscoring the importance of future-oriented policies.

### Tags

#EconomicImpact #BusinessDecisions #PoliticalClimate #FuturePolicies #CommunityImpact


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about Disney
and Disney's decision to not set up a new project.
And we're gonna use that to illustrate the economic impact
that something like this causes
because it's way more than most people think.
And in this case,
we actually have some decent numbers to work with.
if you missed the news Disney has decided to not move forward with a
project to put a bunch of Imagineers in Florida. 2,000 of them. And they've
decided that that project, that's California, okay. Now there are a lot of
people that are saying, hey this is because of you know everything that's
going on in Florida dealing with Disney. I mean maybe, maybe. We don't know that
though, to be honest. We don't actually have any real evidence that that's true.
My personal belief is that, yeah, it probably does have something to do with
it. It definitely, I think it definitely factored into it, even if it wasn't the
entire reason for not continuing this project. But Disney's never gonna say
that, because contrary to the way people are trying to paint it, Disney isn't
actually trying to get involved in the political aspects of things. They're
really not. But either way you have a situation where a project that is being
described as either a billion dollar project or 2,000 jobs is no longer going
to be in Florida.
OK, $1 billion, that doesn't actually really cover
what this is going to cost.
I think that's actually the amount to set up the facility.
2,000 jobs, they're Imagineers.
I've seen where people say the median income is $120,000.
We're just going to say $100,000 because I
would imagine part of the reason they might have ended up here
because you can pay people less.
So $100,000 a year times 2,000 people, that is $200 million a year ripped out of the economy.
It's not going to be there now.
So every five years, from salary alone, it's a billion dollars gone.
But wait, there's more.
Because that's just the salary.
That doesn't include the money the company itself would spend in the area on everything
from replacement parts for equipment to coffee and doughnuts.
There's a whole lot of economic activity that would be generated that way, easily, easily
matching the salary.
But wait, there's more.
That also just includes what the company itself spends for products in their own employees,
their own salaried people.
What about like the landscapers?
That's gone too.
What about the increased economic activity just outside?
People go in and get coffee on their way into work.
That's gone too.
going out to dinner. That's gone too. Let's say you have a married couple who
are both Imagineers and they just love Disney. They have a couple kids. That
nanny's job, it's gone. Beyond that, if you have a facility of that size, stuff is
getting trucked into the area. Those truckers will not be coming because they
don't have to bring it anymore. All of the increased economic activity just, it
snowballs. This is way more than a billion dollars gone. Way more. And it is super long-lasting.
This is a bunch of money that could have been pumped into that economy over the long term.
When Disney puts something somewhere, it stays. You're talking about billions upon billions
of dollars that will not be in Florida because of this. Now, here's the thing. We
know about Disney because it made headlines, right? Because it's very high
profile. The thing is, this is happening all the time. I personally know somebody
who was looking at setting something up in the next couple of years. They were
gonna put it up in the panhandle. Salary would have been 12 to 15 people making
almost 100k a year so you're looking at 1.2 million a year in salary. The company
itself would probably spend as much if not more on materials in the area. So
you're talking about two and a half maybe three million dollars that's not
going to be put into this area and the difference is in the panhandle they
don't have the money to lose like that and this isn't going to make headlines
nobody's even going to know about it the current the current decision they made
was that they're going to put it in Georgia as long as Georgia does what
it's supposed to in the next election if they don't they're looking at Minnesota
and Maine. Those jobs, they're gone. The money, it's gone. And I know this person,
this person is not going to want to go back and forth from Minnesota or
Maine to here. That means that eventually the entire enterprise will go to
Minnesota or Maine.
These decisions, they have long-term impacts for an area that is economically disadvantaged
to lose $15 million a year.
The GDP of some of the counties here barely breaks half a billion.
That's huge.
When you are talking about large things like Disney or like a military installation, the
impacts are just amazing and they are really long term because Disney is like the military.
When it goes somewhere it stays.
Whether or not this particular decision had anything to do with the current political
climate. We don't know, not really. We can all suspect, we can all believe, but we
don't know that. What we do know is that companies will make that decision,
whether or not it happened this time or not. It will happen in the future and the
amount of money that is lost because of it is huge. So the policies that are
getting enacted that are, let's just say, more old-fashioned. What that means is
that companies that look towards the future, institutions that look towards
the future, they're not going to want to be there. And companies that plan 10 or
20 years out, they tend to be the most profitable, which means they tend to
bringing the most money.
It is cheaper to be a good person.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}