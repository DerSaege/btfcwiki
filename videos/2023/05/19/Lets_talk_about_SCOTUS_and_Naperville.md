---
title: Let's talk about SCOTUS and Naperville....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=t4Fyzru8tJM) |
| Published | 2023/05/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent resolution of a case related to assault weapons and high-capacity magazines in Naperville.
- Mentions the case reaching the emergency docket, also known as the shadow docket, of the Supreme Court.
- Notes the expectation for the Supreme Court to issue an injunction to halt enforcement of the ban, but that did not happen.
- Justice Barrett denied the application for a writ of injunction pending appeal without providing an explanation, common for the emergency docket.
- Warns against prematurely claiming victory, as the case could still return to the Supreme Court as it progresses through the legal system.
- Points out the Supreme Court's recent trend of avoiding major decisions via the emergency docket, possibly indicating a cautious approach.
- Emphasizes that while many groups are celebrating the denial of the injunction, it's premature to declare a major victory.
- Indicates the likelihood of continued pro-gun rulings from the Supreme Court, even if not through the emergency docket.
- Advises against assuming the case is over, as the ban will still be enforced during the legal process, with potential ongoing court battles ahead.
- Suggests that the general tendency of the court, based on previous decisions, leans towards supporting the gun owner, so any relief should be seen as temporary rather than definitive.

### Quotes

- "The ban will be enforced as it makes its way through the court system but the court battles are nowhere near over."
- "I wouldn't go around claiming victory on this just yet."
- "So if you're supportive of this more of a breather than a victory."
- "Anyway it's just a thought."
- "Y'all have a good day."

### Oneliner

The Supreme Court's denial of an injunction on an assault weapons ban in Naperville prompts caution rather than celebration as ongoing legal battles loom.

### Audience

Legal observers, gun control advocates

### On-the-ground actions from transcript

- Stay informed about the progress of the case and any future legal developments (implied).

### Whats missing in summary

Analysis of the potential implications of this case on gun control legislation and the broader legal landscape.

### Tags

#SupremeCourt #AssaultWeaponsBan #Naperville #GunControl #LegalSystem


## Transcript
Well howdy there internet people, Lidsbo again. So today we are going to talk about the
Supreme Court shadow docket again and the case that we brought up I guess last week
I think and its resolution because it has been decided in what I think is a surprising
way but I think it may also be falsely comforting. There are a lot of entities claiming victory
when maybe they shouldn't do that yet,
but we'll go over it.
Okay, so if you have no idea what I'm talking about,
there was an assault weapons man in Naperville,
also a high-capacity magazine,
what they're calling high-capacity magazines.
A gun store owner in town filed suit,
went through the process, and it got to the emergency docket,
often called the shadow docket, of the Supreme Court.
I think most people expected the Supreme Court
to issue an injunction to basically stop
enforcement of the ban as it moved through the court system.
This is what came out.
The application for a writ of injunction pending a pill
presented to Justice Barrett and by her referred to the court is denied.
No explanation. That's kind of normal when it comes to the emergency docket.
They don't always like explain what they're doing.
You have a whole lot of groups that are very in favor of this ban
declaring victory, calling it a major victory,
you know, and basically kind of putting it out there that the case was won.
This case is still something that could end up back at the Supreme Court as it moves through.
The court, for whatever reason, over the last, I don't know, maybe a year, they have shied
away from making major decisions or kind of telegraphing their moves via the
emergency docket. I understand there's a lot of people that want to be really
excited about this. I would be cautious. The Supreme Court has issued a whole lot
rulings that are very pro-gun and that's likely to continue. They may just not
want to use the emergency docket to do it. So I wouldn't go around claiming
victory on this just yet. The ban will be enforced as it makes its way through
the court system but the court battles are nowhere near over and I think it's
important for people to really acknowledge that and understand that
this is nowhere near over and the court's general lien would be to support
the gun owner if you look at previous decisions. So if you're supportive of
this more of a breather than a victory. Just bear that in mind. Anyway it's just
a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}