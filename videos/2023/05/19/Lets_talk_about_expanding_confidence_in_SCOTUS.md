---
title: Let's talk about expanding confidence in SCOTUS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2qP0uPb87r4) |
| Published | 2023/05/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democratic lawmakers reintroduced legislation to expand the Supreme Court by four seats, bringing the total number of justices up to 13, the same as the number of federal appellate courts.
- Motivations for expansion include ethical questions, unique rulings not in line with history, and concerns about the current far-right majority retaining control until 2065.
- Recent polling shows low confidence in the Supreme Court, with only 18% having a great deal of confidence, and a third of the country having hardly any confidence.
- Altering the institution might be necessary to protect it, especially with repeated attacks on institutions by one political party.
- While Beau doesn't foresee the expansion passing soon, he believes many running in 2024 will have to address how they plan to vote on it.
- Beau suggests that candidates supporting expanding the Supreme Court may receive a positive response, especially considering the low confidence levels in the court.
- Despite generally disliking messaging votes, Beau sees expanding the Supreme Court as something that needs to be done and likely wanted by the majority of Americans.
- The issue could become a unifying campaign topic for the Democratic Party, energizing various demographics like supporters of gun control, reproductive rights, conservationists, and younger people whose rights are under attack.
- Beau believes that while the proposal may fail initially, it could motivate people to give the Democratic Party a majority in the House and Senate to eventually pass it.
- Beau concludes by mentioning the broken justice system and leaves with a thought for the day.

### Quotes

- "The justice system is broke."
- "This is something that honestly the Democratic Party needs."
- "A third of the country has hardly any confidence."
- "Those people who want reproductive rights would get behind that."
- "The odd thing is I don't think that's why it's being proposed."

### Oneliner

Democratic lawmakers reintroduce legislation to expand the Supreme Court by four seats amidst low public confidence and concerns about the far-right majority's control until 2065, aiming to address ethical issues and unique rulings.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Mobilize support for expanding the Supreme Court through community outreach and education (implied)
- Advocate for reforming the justice system to address issues of legitimacy and public confidence (implied)

### Whats missing in summary

The full transcript provides a nuanced understanding of the motivations behind expanding the Supreme Court and its potential implications for American politics.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the Supreme Court
of the United States and expanding confidence
in the Supreme Court by, well, expanding the Supreme Court.
Democratic lawmakers in both the House and the Senate
have introduced legislation,
I should say reintroduced legislation,
that would expand the Supreme Court
by four seats, bringing the total number of justices
up to 13, which happens to be the same number as the number
of federal appellate courts.
So it makes sense.
OK.
Now, the motivations for this are pretty clear.
There's the constant string of ethical questions coming out
of the Supreme Court.
There are unique rulings that do not
align with the history of the court.
There's the partisan aspect, which basically says
that unless something drastic happens,
the current far-right majority
will retain control until about 2065.
But there's another reason why this
might go further than you might imagine.
According to recent polling, only 18 percent of Americans have a great deal
of confidence
in the Supreme Court. Only 12 percent of women
can't imagine why. 46% say they have only some confidence and 36% say they have
hardly any. A third of the country has hardly any confidence in what is
supposed to be a respected court, the highest court in the land. That's a
problem when it comes to legitimacy. Under normal circumstances, in a normal
period of American history, I would say there's no way this is going to go
anywhere because people in DC are very much entrenched in tradition and
institutions. But the reality is one political party has attacked those
institutions repeatedly. And sometimes the only way to protect an institution is
to alter it a little bit. And I have a feeling there's going to be more support
for this than I think people are imagining right now. I don't foresee this
going through anytime soon, but I would imagine that there will be a lot of
of people that are running in 2024 that will have to make a statement on how they plan
on voting on it.
And I think that those who say they will vote to expand the Supreme Court will probably
get a more positive response.
And you are talking about numbers like this, more than a third of the country, hardly any
confidence.
That's a problem.
And it might also be something that motivates people.
Generally speaking, something like this, I would say, is a messaging vote.
And I generally don't like those.
In this case, it's probably something that needs to be done.
It's also probably something that the majority of Americans want done.
And while the Republican Party is going to fight this tooth and nail, if this turns into
a campaign issue, it could be a very unifying thing for a party that has a hard time unifying.
The Democratic Party can use this as a campaign issue, something that everybody in the country
can get behind.
Every Democratic candidate can say, I'm going to vote to expand the Supreme Court.
And it's likely to energize a lot of people.
just those that you're imagining. I mean, there are the obvious demographics, those
that are in favor of gun control, okay? Based on the fact that there is an
expectation of this court controlling those decisions until 2065, those people
who want gun control, they would get behind that. Those people who want
reproductive rights would get behind that. Conservationists would get behind
that. Younger people who are having their rights constantly attacked by the
Republican Party would get behind that. This is something that honestly the
Democratic Party needs. The odd thing is I don't think that's why it's being
proposed. This is something that those who want to reform the court have been
talking about for almost a decade, at least since 2016. So I don't actually
think that has anything to do with it, but realistically this time it's
probably going to fail. It will probably fail. But if they keep bringing it up it's
something that might motivate the American people to give the Democratic
Party a majority in the House and the Senate so it can pass. Because right now
I mean, let's say it with me. The justice system is broke.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}