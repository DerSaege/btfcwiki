---
title: Let's talk about planes, runways, and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TKEcs-q49Qs) |
| Published | 2023/05/19 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ukraine expressed the need for air power, particularly the F-16, with the Biden administration showing some reluctance but not defiance.
- European powers require U.S. approval to provide F-16s to Ukraine since it is a U.S. design, and this approval seems likely.
- Getting F-16s does not guarantee air superiority for Ukraine, but it will help level the playing field.
- Concerns have been raised about Ukrainian runways being inadequate for F-16s due to debris, length, and other issues.
- There is uncertainty about whether the runway situation is a minor performance issue or a major problem.
- Beau questions if the high standards set by the U.S. military are necessary or if they could adapt like the Ukrainian military.
- Beau suggests hearing from specialized Air Force construction units like Red Horse and CVs to understand the runway preparation process.
- He expresses confidence in the Ukrainian military's adaptability and problem-solving abilities based on past performance.
- Beau recalls instances where commentators doubted Ukrainian capabilities but were proven wrong, indicating potential success in handling F-16 logistics.
- He prompts the audience to think about the runway preparation process and its potential challenges compared to managing logistics for other military equipment.

### Quotes

- "The Ukrainian military has demonstrated time and again that they're adaptable."
- "I see no reason to believe that we won't be wrong this time as well."
- "I don't have any reason to believe that this time will be different."

### Oneliner

Ukraine's potential acquisition of F-16s prompts questions about runway readiness and Ukrainian military's adaptability, challenging doubts on logistical capabilities.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Contact specialized Air Force construction units like Red Horse and CVs for insights on runway preparation (suggested)
- Support efforts to clear debris or resurface runways for potential F-16 deployment (implied)
- Stay informed about the ongoing developments regarding Ukraine's airpower needs and potential F-16 acquisition (implied)

### Whats missing in summary

A deeper dive into the specific challenges and processes involved in preparing Ukrainian runways for F-16 deployment.

### Tags

#Ukraine #F16 #Bidenadministration #MilitaryLogistics #AirPower


## Transcript
Well, howdy there, internet people.
Let's go again.
So today we are going to talk about airplanes and runways and Ukraine
and Biden and pundits.
Okay.
So if you missed it, Ukraine has repeatedly said they need air power.
They've had their eye on the F-16.
The Biden administration has been reluctant, but not defiant when it comes to okaying this.
It appears that the Biden administration has kind of given the nod.
And if European powers want to provide F-16s to Ukraine, well, that's okay.
The F-16 is a U.S. design, therefore European powers need U.S. approval to re-export it.
to re-export it.
And it looks like that approval is forthcoming.
It's worth noting that officially,
no European country has asked for it.
But we'll see how this plays out.
OK, so the next obvious question.
If Ukraine gets F-16s, does that mean
they're going to get air superiority?
No, no, not necessarily.
Will it help?
Yeah.
Yeah, it'll help level the playing field.
But it doesn't mean they will have air superiority the way
the US typically acquires it.
But it's definitely not going to hurt.
Now that this is becoming more of a realistic possibility,
that this occurs, you are probably
going to hear from commentators.
And there's a couple of different things
they're going to talk about.
The first is, well, it's runways, is really what it boils down to.
And the general idea is that Ukraine just can't handle the F-16 because the F-16 has
a giant hole on the front of it that likes to suck everything in and the Ukrainian runways
are old and they have debris and some of them legitimately are too short and they would
have to be extended. But it's stuff like that. Okay, so, and I don't know the
answer to this. I don't know how severe this issue is from hearing people talk
about it, but my question really has to do with whether or not this is a perfect
performance issue or it's a real problem? US standards when it comes to stuff like this
is really, really high. They're in combat. You know, there's that saying, no combat-ready
unit has ever passed inspection and no inspection-ready unit has ever survived combat. Is it a situation
where this is how the United States has always done it because of a few incidents or is it
a major issue?
And I do not know the answer to that.
The other thing is that when commentators are talking about this, they are talking to
logistics people, they are talking to intelligence people, they are talking to pilots, which
makes sense because it's an airplane.
But at the same time, we don't need to hear from a lieutenant colonel who flew an F-16
15 years. We need to hear from Red Horse. They are the Air Force construction people
to severely downplay what they do. We need to hear from Red Horse, we need to hear from
CVs, people like that. People that you can throw a smoke grenade into the middle of the
woods and say, hey, I need an airfield. Come back two days later and there's an airfield.
Come back four days later and they're installing a game room and landscaping. That's who we
need to hear from? How hard is it to get the runway up to the level it needs to be at?
Because if there is one thing the Ukrainian military has demonstrated time and again is
that they're adaptable. Give them the specs, they can probably do it by hand if they need
to. I don't know that this is going to be the issue that people are making it out to
be. I want to remind everybody that I can think of at least six pieces of
equipment that commentators, and I'm going to include myself in this because I did
it with the Abrams, commentators in the West said there's no way they can handle
the logistics, there's no way they can handle the maintenance, and we have
apparently been wrong every single time. They apparently worked out all the
issues with the aprons. I see no reason to believe that we won't be wrong this
time as well. The Ukrainian military has demonstrated time and time again that if
you tell them what they have to do they will figure it out. I don't have
any reason to believe that this time will be different. So the only person I
know that can actually answer these questions is currently out of pocket, but
I have seen a lot of red horse icons down in the comments section. How hard is
it to clear the debris from a runway or resurface it if need be? I mean
because that's really what we're talking about here, right? I don't... that seems a
whole lot easier than managing the logistics train on an Abrams. I'm pretty
sure that we want to handle it.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}