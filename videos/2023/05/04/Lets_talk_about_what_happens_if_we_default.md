---
title: Let's talk about what happens if we default....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rnkWq7QmKS0) |
| Published | 2023/05/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the potential consequences of the Republican party not raising the debt ceiling, with three different types of projections.
- Points out that economic damage begins before an actual default occurs.
- Criticizes the Republican party for using the debt ceiling as leverage and sending a budget to Biden that even Republicans don't want.
- Describes the internal conflicts within the Republican party regarding budget cuts and their lack of seriousness in negotiations.
- Warns about the economic impact: 200,000 job losses if it runs up to the brink, half a million in case of a short default, and eight million in a protracted default.
- Notes that all scenarios lead to a recessionary period and that the government won't have resources to soften the blow without raising the debt ceiling.
- Mentions McCarthy's failed attempt to use leverage and how time is running out for political games.
- Emphasizes that blame will fall on the Republican party if the debt ceiling isn't raised and that they risk losing credibility for 2024.
- States the necessity for the Republican party to act quickly to avoid Senate or House Democrats taking control of the situation.

### Quotes

- "They have to make their move and they have to do it quick."
- "Any hopes they have of going anywhere in 2024, they're gone."
- "The blame will rest with the Republican Party."
- "200,000 people lose their jobs because they can't get their act together."
- "It's over now."

### Oneliner

Beau explains the consequences of the Republican party not raising the debt ceiling and warns about job losses and economic recessions if action isn't taken quickly.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to urge them to prioritize raising the debt ceiling quickly (implied).

### Whats missing in summary

Detailed breakdown of the potential economic consequences and the urgency for the Republican party to act swiftly.

### Tags

#DebtCeiling #RepublicanParty #EconomicImpact #JobLosses #PoliticalResponsibility


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about what's likely to happen.
We'll talk about the projections of what will occur
if the Republican party does not raise the debt ceiling.
The new projections are out.
There are three different types.
One goes over just running right up to the brink
because the economic damage starts
before we actually default.
The other is like a brief default and there's like the doomsday scenario for a protracted
default.
And we're just going to keep it simple focusing on numbers that people actually care about.
And I know somebody's going to have a problem with the way I said that.
Yeah, it's up to the Republicans.
Why do Republicans have to raise the debt ceiling?
Why is it their fault?
Because they control the House.
They tried to use the debt ceiling as leverage for their budget, but then they sent a budget
to Biden that Republicans don't even want.
You can't ask them to negotiate when you don't even want what you told them.
You have one faction of the Republican Party that's saying the budget that was sent up,
that's the floor.
They want even bigger cuts.
And you have another faction of the Republican Party saying they don't want the cuts that
they voted for in that budget. They're not serious people. They tried to use the debt
ceiling as leverage. The only reason they're still acting like that budget is real is to
trick the people who still believe them. Okay, so what happens if this runs right up to the
brink. The economic penalties begin. A lot of them are things
that for the average person doesn't really matter a whole
lot. Doesn't seem to anyway. But the number that is going to
matter 200,000 people lose their jobs because they can't get
their act together. If there is a default, a short one, about
A half a million people lose their jobs.
The doomsday scenario where there is a protracted default,
about eight million more than lose their jobs.
All three begin a recessionary period.
Now, what's important to remember
is that if the debt ceiling isn't raised,
the government can't do anything
to soften a blow from a recession
Because they don't have any money.
So those are the key parts I think people actually care about.
McCarthy tried.
He tried to use it as leverage.
If you're a Republican, rest assured he actually made the attempt.
But it's over now.
It is over.
The people who voted for this budget, a whole lot of them are already telling their constituents
that they didn't want it, that it was just a symbol.
Biden can't negotiate from a symbol.
It's what McCarthy wanted, but he, he failed.
He didn't get what he wanted.
They are running out of time to play their games.
The brinksmanship part of this, what, it starts in like three weeks?
They don't have time for this.
If this happens, rest assured, the blame will rest with the Republican Party.
Any hopes they have of going anywhere in 2024, they're gone.
There's no way that they're going to be able to spin it as, well, Biden didn't raise the debt ceiling.
Biden can't raise the debt ceiling. They have to do it.
it. Well, we would have done it if he negotiated with us about a budget that
we're on record not wanting. It doesn't make sense. They have to make their move
and they have to do it quick. Or the Senate will move forward with its plan
or the House will move forward with its, the Democrats in the House, and they'll
just try to peel off a few Republicans who care more about your job than they
do their party.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}