---
title: Let's talk about some off Republican primary polling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=CR3xG6dE2Xg) |
| Published | 2023/05/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Discussed polling data revealing primary voters' preferences.
- Primary voters expressed wanting a nominee who challenges "woke" ideas, opposes gun regulations, claims Trump won in 2020, and angers liberals.
- Trump has 24% definite support, 49% considering, and 27% not considering among Republican primary voters.
- Notably, 27% of Republican primary voters are not considering voting for Trump.
- Despite likely winning the primary, Trump faces decreasing support within the Republican Party at large.
- A significant portion of Republicans not voting for Trump could lead to his inability to win the general election.
- Speculation that Trump's association may prevent some Republicans from voting altogether.
- Concerns about potential impact on Republican voter turnout if Trump remains in the race.
- Acknowledges the need to monitor further polling data to see if the lack of enthusiasm for Trump persists.

### Quotes

- "85% said that they wanted a nominee who challenges woke ideas."
- "Trump appears likely to win the primary, but his lack of support among the Republican Party at large could hinder his general election prospects."
- "Trump may depress Republican voter turnout just by being Trump."

### Oneliner

Beau examines polling data on Republican primary voters' views on Trump and speculates on potential implications for his general election prospects.

### Audience

Political analysts, Republican voters

### On-the-ground actions from transcript

- Watch for further polling data on Republican voter sentiments towards Trump (implied).

### Whats missing in summary

Further details on the potential consequences of Trump's support among Republican primary voters for the upcoming general election.

### Tags

#RepublicanParty #Trump #PrimaryElection #GeneralElection #Polling


## Transcript
Well, howdy there Internet people, it's Bo again.
So today we are going to talk about Trump.
We are going to talk about Trump not being able to win a primary without Trump and not being able to win a general with
him.
There was some really interesting polling, CBS News, YouGov, and a lot of the polling has been like just all over social
media because  There's a certain section of what they asked.
It led to responses that honestly, they're so sad,
they make you laugh type of thing of primary voters.
And it's important that these are primary voters.
And we'll come back to that.
But 85% said that they wanted a nominee
who would challenge woke ideas.
66% said they wanted a nominee who would oppose any gun
regulations.
61% said they wanted a candidate who would say that Trump won in 2020, and 57% said they
wanted a nominee that would make liberals angry.
That is a policy-driven party right there.
Okay, but that's just the part that got splattered all over social media.
That's actually not the most interesting part to me.
When they're polling primary voters, they normally give them three options.
They say, I'm definitely voting for this candidate, I'm considering this candidate, or I'm not
considering this candidate, because there's a whole bunch of them.
There's a whole bunch of different options, and they give them those three options to
pick from.
So what you normally end up with is a frontrunner that has a whole bunch of people who have
already decided they're voting for that nominee, that candidate.
Here's what Trump has, 24% have decided they are definitely voting for Trump, 49% are considering
Considering Trump and other candidates, 27% are not considering Trump.
They won't vote for Trump.
It is interesting that there are more people who have decided they are not going to vote
for Trump than have decided they will among Republican primary voters.
That's interesting, but at the same time, they're still half the votes up for grabs.
Realistically, unless something wild happens, it does look like he has the primary on lock.
It really does.
But 27% not considering Trump, 27% of Republican primary voters.
Voters are normally the most energized, to use a polite term, they are the
people who are very much into politics, they are very adamant, and given the
Republican Party over the last few years, they should be the biggest Trump
supporters, they should be leaning in really hard on Trump because they're the
most energized group, 27% of those people saying they're not considering voting for
Trump means that among the general Republican population, let's just say
that number holds, and half of them stick to it.
I'm not voting for Trump.
Doesn't necessarily mean they go vote for the Democratic candidate, it just
This means they're not voting for Trump.
If half of that number holds, he can't win.
The math doesn't math.
He can't win.
The enthusiasm, there is more enthusiasm among those who are not going to vote for Trump,
know that, than there is among those who are going to.
Trump appears likely to win the primary, but his lack of support, the dwindling support
among the Republican Party at large, outside of those who are super invested in politics,
It doesn't look like there's a real path to even a percentage that would be close to
getting the electoral votes needed.
If you're going to lose 13 or 14% of Republican voters just because they're not going to
vote for Trump, you can't win.
They don't have the numbers.
Now, the one thing I do want to say is that this is the first time I've seen this.
This is the first time I've seen this.
It's one poll.
It's from a pollster that does good work, but it's just one poll.
But this is something we should probably watch for because I have repeatedly said that there
are a whole lot of Republicans that can't win a primary without Trump, but can't win
general once they tied themselves to it. That may apply to Trump too. That may
apply to Trump too. It looks like it's shaping up that way because there are a
lot of Republicans who are exhausted quote from the polling when it comes to
Trump. They may be so exhausted they don't go vote. Not where they vote
for somebody else, not where they vote for a third party, where they're just like,
I can't deal with this anymore, and they don't vote.
Trump may depress Republican voter turnout just by being Trump.
Now this is all assuming that he doesn't have any legal entanglements and no other major
issues impede him running.
But those numbers are interesting and I can't wait to see more to see if that holds.
If that level of enthusiasm for not considering Trump holds.
That's what we need to be watching for right now.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}