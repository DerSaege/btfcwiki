---
title: Let's talk about two Senate plans for the debt ceiling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=O4zZoHIbV8c) |
| Published | 2023/05/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains two plans regarding the debt ceiling coming out of the Senate.
- The first plan is to repeal the debt ceiling entirely, arguing it's not real and only leads to unnecessary brinksmanship.
- Senator Brian Schatz from Hawaii is behind the idea of eliminating the debt ceiling to force Congress to handle the budget more responsibly.
- The second plan, proposed by Senator Tim Kaine, suggests changing how the debt ceiling works by letting the president set it annually, with Congress having the power to block or alter it.
- Kaine's plan aims to prevent the debt ceiling from being used for political leverage in budget negotiations.
- Republicans might find Kaine's plan more appealing as it allows for party power retention while limiting the debt ceiling's manipulation.
- The likelihood of Kaine's plan passing through the House seems higher due to potential bipartisan support from Republicans who prioritize national interests over party politics.
- Both ideas are initial proposals, hinting at more developments to come in the future.
- The longer the debt ceiling issue persists, the more leverage Republican leader McCarthy loses, potentially leading to pressure for a change in House leadership.
- The Senate seems to be growing tired of the situation, indicating a shift in dynamics that may impact McCarthy's position as Speaker of the House.

### Quotes

- "The debt ceiling isn't real."
- "It is more like Visa gives you a credit limit of $100,000, and you and your partner decide, well, we're only going to spend $1,000 this month."
- "Those people who are more concerned about their country than their party inside the Republican Party, they'd be more likely to go for it."
- "The longer this goes on, the more leverage McCarthy loses."
- "If he doesn't come up with something soon, Republicans may decide they want a different speaker."

### Oneliner

Beau breaks down two Senate plans concerning the debt ceiling, from outright repeal to restructuring, with shifting bipartisan dynamics potentially favoring a new approach.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Reach out to representatives to express support for a more responsible approach to handling the debt ceiling (suggested).
- Stay informed about further developments regarding the debt ceiling proposals (implied).

### Whats missing in summary

Insights on the potential consequences of continued deadlock and brinksmanship surrounding the debt ceiling debates.

### Tags

#Senate #DebtCeiling #BipartisanSupport #Budget #PoliticalLeverage


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the debt ceiling,
the Senate, and two plans coming out of the Senate,
and a little bit of bipartisan support.
So we're gonna go through those and talk about
how likely they are to get anywhere and what it all means.
Okay, so the first plan is coming from
the Senator from Hawaii, pretty progressive.
can't remember his name right now, Brian Schatz, maybe.
The general idea, repel the debt ceiling.
Just get rid of it.
It is important to remember that the debt ceiling is not real.
It's fake.
It doesn't actually exist.
When people in Congress are doing their little brinksmanship
games, like are going on right now,
they often tell the press or tell their constituents
that the debt ceiling is like the US's credit card limit.
That's not true.
That's not what it is.
It's more like Visa gives you a credit limit of $100,000,
and you and your partner decide, well, we're only
going to spend $1,000 this month.
It's the $1,000 number, not the $100,000 number.
Visa would not stop you from spending $1,020.
The debt ceiling isn't real.
So the idea there is to just get rid of it.
Make it go away and that would force, in theory, force Congress to deal with the budget in
a more responsible manner.
That's the idea and it would eliminate the brinksmanship.
Me personally, I think that's the right move.
The problem is it seems really unlikely that that is going to get through the House.
Republicans would just be giving up way too much leverage.
There's nothing in it for them, even if it is the better idea.
The other idea of being floated is coming from Senator Tim Kaine, and that one is to
change the way the debt ceiling works. Rather than Congress arguing about the debt ceiling
every year or every couple of years, the president sets the debt ceiling every year. If Congress
has a problem with it, they can block it and they can change it. That's the general idea
just changing the order of how it works. Also a much better idea because it
removes the likelihood of this being used for ridiculous budgets.
You're not going to leverage the debt ceiling over a budget your party
doesn't even want, as is happening right now. It would only be used in extreme
cases. It allows the parties to retain a little bit of power, doesn't make them
give it up, but it limits its use. This seems more likely to get through the
House because those people who are, I'm not saying this to be mean, it is what it
is, those people who are more concerned about their country than their party
inside the Republican Party, they'd be more likely to go for it. Those people
who are truly concerned about China as a competitor, those people who are
concerned about the US status as the reserve currency, they would see this as
something that's a little bit more manageable. It benefits those goals of
theirs and they still retain their power. So that seems like the more likely
option. At the same time, these are the opening ideas. There's more to come. I
would say at the moment Tim Kaine's idea is probably the one that's going to move
forward, but I'm sure there will be more that come out and we're going to have to
wait and see. The longer this goes on, the more leverage McCarthy loses, the
more leverage the Republican Party loses. And the Senate starting to say yeah we're
kind of done with this and we have a you know our little secret weapon over in
the House. I think it might start putting more and more pressure on McCarthy. If he
doesn't come up with something soon, Republicans may decide they want a
different speaker.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}