---
title: Let's talk about Twitter, free speech, and a new feature....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kFab_DqgU_U) |
| Published | 2023/05/04 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Twitter, under Musk's rule, has a 100% compliance rate with surveillance and censorship requests from governments.
- Musk aims to introduce a feature allowing major media companies to charge Twitter users per article read.
- Beau is skeptical about major companies taking up this offer and believes it could worsen media literacy issues in the US.
- The proposed pay-per-article feature could lead to people being misled by headlines without reading the full articles.
- Beau questions whether this feature supports free speech and open discussion or if it hinders real debate.

### Quotes

- "Twitter has a 100% score of complying with surveillance and censorship requests."
- "One of the real issues that we have in the US is media literacy and people being energized by a headline and not reading anymore."
- "I don't think that's something that would foster a whole lot of free speech."

### Oneliner

Under Musk's Twitter rule, compliance with government requests is 100%, and a new pay-per-article feature may worsen media literacy and hinder free speech.

### Audience

Social media users

### On-the-ground actions from transcript

- Stay informed about social media platform policies and how they impact free speech and information access (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Twitter's compliance with government requests under Musk's rule and the potential impact of a new pay-per-article feature on media literacy and free speech.

### Tags

#Twitter #FreeSpeech #MediaLiteracy #GovernmentRequests #SocialMedia


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Twitter and free speech
and a new product feature that is said to be rolling out on
Twitter soon for major media publications.
It's been about six months since Musk began his rule over
Twitter.
So we're just going to check in. You know, one of the things that many people were
excited about
when Musk took over
is the fact that he's a free speech absolutist
and he was going to make sure
that governments weren't censoring speech.
People were really excited about this.
So, let's check in.
By self-reported data, when it comes to surveillance and censorship requests
from governments,
Twitter has a 100% score of complying with them.
By their own information, they fully complied with 808 requests, partially complied with
154, and then there's no information on nine of them, bringing them to a total of 971,
which was the total they said they got.
Prior to Musk taking over, Twitter complied with about half of them.
Okay, so in other news, Twitter plans to roll out a function for major media companies that
allows them to charge Twitter users on a per article basis for any articles they may read.
And he sees this, Musk sees this, as a win-win for everybody because the average viewer,
you know, if they don't want to pay for a whole subscription, they can just buy the
one article and it's good for the company for some reason too.
I'll start by saying I don't actually think a lot of major companies are going to take
him up on this wonderful offer. But I would point out that if this does take
off this is actually going to be really bad. You know, one of the real issues that
we have in the US is media literacy and people being energized by a headline
and not reading anymore. I would imagine that problem would get much, much worse
if people had to pay to read beyond the headline. They already don't do it in a
lot of cases, which is why you have stories that get out of control so
quickly and people believe things that just aren't true. So I don't necessarily
think that that is good for free and open discussion. I don't think that's
something that would foster a whole lot of free speech. It might actually create
a situation where those who were interested in real discussion would have
to spend a lot of time cleaning up what people believed based on sloppy
headlines.
So that's what's happening at Twitter.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}