---
title: Let's talk about Santos from 3 views....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YDOPjVp4ol8) |
| Published | 2023/05/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democratic attempt to oust George Santos failed.
- Republicans turned it into a motion to refer Santos to the Ethics Committee.
- Santos gets to stay in office during a long drawn-out process.
- Republicans can claim they held one of their party members accountable.
- Independent voice finds expelling Santos pointless.
- Independent questions the concept of a "real independent."
- Republican message defends their party against accusations of not caring about principles.
- Republicans show support for a candidate under indictment, citing polling data.
- Symbolic votes can backfire, as Democrats were outmaneuvered in this case.
- Independents may be more easily swayed by criminal indictments.
- Republicans may ignore facts and logic in favor of base emotions and feelings.
- Calling out Republican hypocrisy may not be effective due to their disregard for new information.
- The messages reveal divides among idealistic, pragmatic, and echo chamber individuals.
- The dynamics show a failed attempt at accountability, differing perspectives on expelling Santos, and challenges in reaching Republicans effectively.

### Quotes

- "They're leading candidate for the presidential nomination is currently under indictment."
- "They will absolutely send a message saying that there's no way their party would support somebody who's being indicted."
- "The messages reveal divides among those who are very idealistic, those who are very pragmatic, and those who have fallen into an echo chamber."

### Oneliner

Democratic attempt to oust George Santos fails, Republicans use a motion to refer Santos to Ethics Committee, Independents find expulsion pointless, and Republicans defend their party's stance on indicted candidates, showcasing divides in perspectives.

### Audience

Political Observers

### On-the-ground actions from transcript

- Analyze polling data to better understand voter perspectives (implied).
- Engage in meaningful dialogues with individuals holding different political views (implied).
  
### Whats missing in summary

The full transcript provides deeper insights into the challenges of political accountability, differing perspectives on expelling politicians, and the impact of partisan divides on rational discourse.

### Tags

#Politics #Accountability #Partisanship #Divides #Polling


## Transcript
Well, howdy there, internet people.
Let's vote again.
So a Democrat, an Independent, and a Republican are walking
down the street.
And they run into George Santos.
I have three messages here about the Democratic attempt
to oust Santos, one's from a Democrat, one's from an
Independent, and one is from a Republican.
And we're going to go through all three, because I think
this is uh, it's just enlightening. Okay, so the Democratic Party member says,
the Dems were right to force the vote to expel Santos. It puts Republicans on
record as voting against expelling him. Okay, so this was sent before the vote
was taken, like obviously we know that isn't how it turned out. I would like to
point out that, yeah, I knew it was not really gonna matter. I did not think it
was gonna go as bad as it did. If you don't know what happened, the Republicans
managed to turn it into a motion to refer him to the Ethics Committee, which
is gonna be this really long drawn-out process, and he'll get to stay in during
this time. Meanwhile, they get to run back home and say that they held one of their
party members accountable. Like it went it went bad, like way worse than I
imagined it going. The Independent. I'm normally a right-leaning independent. The
people in your comments seem to think expelling Santos is for my benefit.
Maybe I'd care if he was my rep, but really it just seems pointless. He's
probably going to jail. Let him write out his time. Either way, no real
independent will vote for him again. Okay, I mean, all right, I'm just boggled by the
idea of real independent. Like, I don't know what that means. I didn't know that independence
like purity tested. And then the Republican. You paint all Republicans as not caring about
principle, it's offensive, it's inconceivable to think a majority of Republicans put party
first or want that indicted loser in office.
Inconceivable, huh?
You keep using this word, I do not think it means what you think it means.
You love polls, so show me a poll that says Republicans still support that criminal.
recent poll on it is the Morning Consult and he's up, he has 61%. 43 points higher
than his next challenger. I mean I'm assuming you're talking about Trump and
you said loser, Santos won. The indicted loser, I just assumed that's Trump. Look,
don't pretend like the Republican Party actually cares about that. They're
leading candidate for the presidential nomination is currently under
indictment. The false, oh I can't believe you said that about us, that
just doesn't even make sense. There is polling to show that the Republican
Party as a majority of the Republican Party would absolutely support a
candidate under indictment. So, it's happened now. I don't think that many
people thought it was going to go the way that it did, but I think there was a
lot of discussion about whether or not it was a good idea to try it anyway
because no matter what happened it wasn't going to succeed. What occurred in
it being switched up and now Republicans actually have a talking point and they
can say you know we held him accountable we did our job he's just getting due
process because we love the Constitution and whatnot. That's the risk when you're
making symbolic votes because it can always go sideways. In this case the
Democratic Party got just wildly outmaneuvered. And yeah, as somebody who
was opposed to the whole idea, even I didn't think it was going to go that bad.
But the message from the independent, basically people who are independent, when
When it comes to stuff like this, I think when it comes to criminal indictments, they're
probably more quickly swayed than other people.
When you look at the Republican and that message, there's no way Republicans would do that.
This is why calling out hypocrisy doesn't work with them.
They will absolutely send a message saying that there's no way their party would support
somebody who's being indicted while the frontrunner for the presidential
nomination is leading in the polls and running under indictment it's they are
beyond facts mattering in a lot of ways even those who may be indignant at that
idea the Republican Party has devolved to that point where everything is about
base emotions and how it feels. The hypocrisy and cognitive dissonance kicks
in and that new information that just gets tossed right out the window. They
don't even acknowledge it. So using that as a method of trying to reach
them, I just I don't see it working. But I found the three messages in
total as kind of enlightening because it shows some divides among those who are
very idealistic, those who are very pragmatic, and those who have fallen into
an echo chamber. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}