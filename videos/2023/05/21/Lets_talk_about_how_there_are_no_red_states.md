---
title: Let's talk about how there are no red states....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=J1VT-4t6s3Y) |
| Published | 2023/05/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received two messages on messaging and red states in response to a video about the Democratic Party's messaging.
- One message questioned the ability to win in red states like Texas.
- Ran the numbers for Texas, Alabama, and West Virginia, finding no statistically red states.
- Used Texas as an example: Abbott won with 4.4 million votes, O'Rourke had 3.5 million.
- Democrats need to focus on getting their voters to show up and create enthusiasm.
- There are roughly 10 million voters up for grabs in Texas.
- Democratic Party needs to run candidates that people in the state want to vote for.
- The idea of red states is self-reinforcing, causing people not to show up to vote.
- Enthusiasm and voter turnout are key for Democrats to win in places like Texas.
- Creating messaging to encourage people to vote and take others to the polls is vital.
- If Democrats flip Texas blue, it can have a significant impact on national politics.
- The Republican Party will take notice if Texas turns blue during a national election.
- There are enough Democratic voters in Texas to beat a statewide Republican candidate.
- Focus on progress and getting Democratic voters to show up is key.

### Quotes

- "Red states don't exist the way people think they do."
- "Friends don't let friends vote alone."
- "You can't win here. Yeah, you can."

### Oneliner

Beau breaks down the myth of red states, urging the Democratic Party to focus on voter turnout and enthusiasm to flip traditionally red states like Texas blue.

### Audience

Voters, Democratic Party members

### On-the-ground actions from transcript

- Mobilize voters to show up at the polls (suggested)
- Create messaging campaigns to encourage voter turnout (suggested)
- Take friends and family to vote (suggested)

### What's missing in summary

The full transcript provides a detailed breakdown of voter numbers and the potential for flipping traditionally red states blue through increased Democratic voter turnout and enthusiasm.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about two messages
that I got that intersect.
And they deal with messaging and red states.
They were both in response to that video
about the Democratic Party not being great at messaging.
One said, I don't like that you said
that it's just running the wrong candidate in the South.
They're red states.
You can't win in a red state.
The other one was a question that basically asked, hey,
if there was one piece of messaging
you could give to the Democratic Party, what would it be?
A campaign to explain that there's
no such thing as a red state.
Not really.
So the person who sent the message about not being able to
win in the South because they are red states came from
Texas.
So we're going to use Texas as our example.
Now, real quickly, I ran all of the numbers I'm about to go
over for Texas.
I did the same thing for Alabama and West Virginia.
Generally speaking, there aren't any places that are
statistically a red state. They exist, but it's places like Wyoming and Utah, okay?
They're not really the southern states. So in Texas, the last election, last
statewide election, right? Abbott won. He got 4.4 million votes.
O'Rourke got 3.5 million votes. That seems pretty insurmountable right there.
Almost a million votes. If you calculate it out to percentages, Abbott got 54.8 percent.
O'Rourke got 43.9. Insurmountable. Okay, add 4.4 and 3.5. 7.9. 7.9 million people voted.
How many registered voters does Texas have? 17.7. 17.7, leaving roughly 10 million voters
up for grabs there. So if you just took the normal percentages here, the Democratic Party
under O'Rourke, who I would suggest is one of those candidates who lined up with the
party planks, but not so much for the geographic area they were running in.
O'Rourke very open about being anti-gun in Texas.
That hurt him a lot.
Okay, but you take the 43.9% that he got.
Let's just say 40% to keep the math easy.
That means there's 4 million Democratic voters in that 10 million.
In order to win, the Democratic Party has to get 1 out of 4 of those voters to show
up.
That's it.
Red states don't exist.
the way people think they do. And I would suggest that out of that 10 million
there's actually a higher percentage of Democratic voters. People who didn't show
up because they didn't think it was worth it because they can't win because
it's a red state. Self-reinforcing. The votes are there. They have to create
enthusiasm, which is what I said in that messaging video. You have to run people
that the people in that state really want to vote for. You got to get them to
show up. That's how it can be done. Abbott is not invincible. I mean when
When you think about a red state and you have just this small fraction of people showing
up, they're not enthusiastic about him either.
This same thing holds true in other places, and the Democratic Party does it all the time.
In Florida, they had a candidate that was moderately pro-gun and pro-plant, didn't
make it through the primaries. Would have been a really good candidate for
Florida. Maybe not on the national level. That doesn't align with the Democratic
values everywhere but the Democratic Party is a coalition. Red states, they're
not really a thing. Texas can be flipped blue. All it takes is for people to show
up. They have to show up in greater numbers. A greater percentage of the
Democratic Party has to show up to vote than the percentage of the Republican
Party that shows up to vote. It's not impossible. It's not like the numbers
don't exist. There are enough Democratic voters in Texas to beat a statewide
wide Republican candidate there, assuming normal turnout for the Republican Party.
The Democratic Party has to create enthusiasm.
You know, MTV did that thing, rock the vote, trying to get people to show up.
The Democratic Party needs something like that nationwide.
Friends don't let friends vote alone, something like that, to encourage people to take others
the polls. If you want messaging for the Democratic Party, that's it. Because there
are a lot of places that are just written off. You can't win here. Yeah, you can.
Yeah, you can. It's harder, but you can totally do it. That's what they
need to focus on. You flip Texas blue once, you're gonna watch the Republican
party suddenly become a lot more progressive with a lot of policies.
They're still going to be conservatives, but they're going to realize the tides
are changing. If there's one thing that matters, because again I'm not actually a
Democrat, I just care about progress, that would be my move because it'll speed
things along. If you can pull something off like that in Texas, especially during
national election, rob them of those electoral votes, oh believe me the Republican Party will
notice. And it's not out of the realm of possibility. It's not out of the realm of possibility.
Even with those numbers, a million votes can be made up. The number of voters in Texas are there.
Anyway, it's just a thought.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}