---
title: Let's talk about what Republicans are saying about Trump's legal exposure....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=RTwXkxJ9Iuk) |
| Published | 2023/05/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining what Republicans are saying about Trump's situation regarding the documents.
- Bill Barr's comments on Trump's exposure in the documents case, indicating a bad sign.
- Barr's assertion that Trump had no business keeping the documents and could be in trouble for not returning them.
- Speculation on potential consequences if games were played after the documents were requested back.
- Noting that Barr, who stood by Trump through a lot, is now speaking the truth about the situation.
- Linking individuals supporting Trump's re-election and claiming he has no criminal liability to a vested interest in Trump returning to office.
- Pointing out the risk of individuals denying Trump's wrongdoing being implicated in activities surrounding the sixth.
- Mentioning that those not understanding or blindly supporting Trump may have a different perspective.
- Observing that individuals trying to spin the situation to defend Trump often have their names linked to the events of the sixth.
- Implying a motive for those trying to ensure Trump does not implicate them in any potential wrongdoing.

### Quotes

- "It's very clear that he had no business having those documents."
- "Barr stood by Trump through a lot. A whole lot. A whole lot that I personally don't think he should have. Why is he saying this? Because it's true."
- "Y'all have a good day."

### Oneliner

Beau explains Republican views on Trump's document situation, Bill Barr's warning of trouble, and vested interests in Trump's exoneration.

### Audience

Interested viewers

### On-the-ground actions from transcript

- Question individuals supporting Trump's exoneration and their potential vested interests (implied).
- Stay informed about the unfolding events and statements made by key figures (exemplified).

### Whats missing in summary

Insights on the potential legal implications and consequences for Trump based on the document situation.

### Tags

#Trump #RepublicanParty #BillBarr #DocumentCase #VestedInterests


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about what Republicans are saying about Trump's
situation when it comes to the documents.
Um, a lot of times when we cover that, there are people down in the comments
section saying, you know, well, that's not what this person says.
That's not what this person says.
Pointing to Republicans, typically those that were very involved with
other activities involving the sixth, and they have their opinions that they express.
So I'm going to go over what Bill Barr has said about Trump's exposure when it comes
to the documents case, and in case you can't guess, it's a bad sign.
So he's talking about how that law, those laws, since they may be looking at something
more serious than willful retention, actually work.
And he says, it doesn't go a lot on intent or anything like that.
It's very clear that he had no business having those documents.
He was given a long time to send them back and they were subpoenaed.
And I've said all along that he wouldn't get in trouble, probably, just for taking
them, just as Biden, I don't think, is going to get in trouble or Pence is not
going to get in trouble the problem is what did he do after the government asked
for them back and subpoenaed them and if there's any games being played there
he's going to be very exposed at this point it's pretty clear that there were
games being played there. Now, the thing you have to wonder is, this is Bill Barr.
Barr stood by Trump through a lot. A whole lot. A whole lot that I personally
don't think he should have. Why is he saying this? Because it's true. Look at
the people who are on the Republican side of the aisle who are saying, you
know, this is nothing, it's TDS, there's nothing here, Trump can't get in trouble,
so on and so forth. They're the same people that are supporting Trump's
re-election. They're the same people who were, let's say, maybe they were at a
hotel prior to the 6th. Maybe they talked to people about the alternate
electors. Generally speaking, those people who are speaking out in favor of Trump
when it comes to saying that he has no criminal liability whatsoever,
particularly in this case, they have a very vested interest in Trump returning
to office because maybe they got two mill laying around and they can get a
pardon or maybe he'll feel that he owes them and they're banking on him paying
his bills for once.
If you look at those people who are saying flat out, there's no criminal liability here,
he didn't do anything wrong, there's no way he's going to get in trouble, it's a witch
hunt, all of that stuff.
Try to find one that doesn't have a risk of also being implicated in the activities of
the sixth.
Now you do have a few out there that are just like, he didn't do anything wrong, or I don't
care if he did something wrong, he's still my president.
Those people, they may just not understand.
But those who get into the details of it and try to spin it, every one of them that I've
seen. I've also seen their names mentioned in reporting dealing with the
sixth. There's probably a reason they are very interested in making sure he's
not in a position to where he feels like he might have to, I don't know, say a
bunch of people's names.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}