---
title: The roads to a May 2023 Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=h_U9x1c8MFE) |
| Published | 2023/05/21 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Answers Patreon questions in a Q&A session for the channel.
- Addresses various topics such as border situations, AI advancements, historical events, personal stories, and advice on diverse subjects.
- Expresses views on political parties, military strategies, community networking, grief, time management, and team roles.
- Shares anecdotes, opinions, and advice with a mix of seriousness and humor.
- Provides insights, reflections, and personal experiences in response to the questions.
- Shows a down-to-earth approach and willingness to connect with the audience.
- Balances informative responses with engaging storytelling.
- Offers practical and thoughtful advice on different situations and dilemmas.
- Demonstrates a wide range of knowledge and experiences in his responses.
- Engages with the audience in a conversational and relatable manner.

### Quotes

- "Allow yourself to experience the feelings."
- "Every firearm is lethal."
- "It gets easier."
- "Do what you do best."
- "Experience the feelings."

### Oneliner

Beau answers Patreon questions on a variety of topics, sharing personal experiences, advice, and insights in a relatable and engaging manner.

### Audience

Creators, learners, curious minds.

### On-the-ground actions from transcript

- Join or support existing mutual aid groups ( suggested ).
- Get involved in disaster relief efforts ( exemplified ).
- Take time to understand different perspectives and widen your view ( exemplified ).

### Whats missing in summary

Beau's engaging storytelling and down-to-earth approach create a relatable and informative space for diverse topics and advice.

### Tags

#Q&A #Community #Advice #PoliticalViews #TeamRoles #PersonalExperiences #MutualAid #DisasterRelief #Engagement #Storytelling


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to do a Q&A.
We promised we were going to do these a little bit more often
on this channel.
Just an informal question and answer session
from stuff that has come in.
The questions themselves were pulled by the team,
so I haven't read them yet.
So you get to get an off-the-cuff answer.
And I think these are from Patreon.
And we have a note here.
And it says, some of the questions relate to upcoming
themed Q&As, so those got rolled into those files.
You're going to be doing a whole episode on dating advice.
Also, you need to say the email people can send messages to.
It's questionforboe at Gmail if you don't know.
They have a lot of faith in me.
I don't know if y'all know that.
OK, so the actual first real question.
Haven't heard much on the border situation for a while,
and it's in the news.
What's to be believed?
So there was a whole lot of fear-mongering
about what was going to happen when the change occurred.
And there were predictions of just utter chaos.
None of that has happened.
As far as I know, the numbers have actually been declining.
As far as why there wasn't a surge of people,
I mean, your guess is as good as mine.
Even people who were supportive and were like, yay,
get them over, get them processed,
let them claim asylum, we're kind of expecting a larger surge.
Um, let's see, if given the opportunity to sit down for a one-on-one
conversation with Joe Biden, would you do it and what would you say if you
want a transformative administration, you have to act, you have to act,
You have to go for it, and I would get the advice of the youngest, most progressive people
in the Democratic Party and try to fulfill those goals.
That's what will give him that FDR-style administration that he said he wanted.
AI is advancing, and so too are the applications of it.
Trump NFTs looking like Chief Krazy Horse to fully realized scripts and AI created art.
What does this mean in the age of miss slash disinformation?
How can we know what's real or what's fabricated?
What does it mean in the age of miss slash disinformation?
The end of hot takes.
Especially on YouTube, like y'all know this, there's like a whole genre of
of commentary that is immediate, as soon as it happens.
That's probably going to stop, because eventually those
people will get burned by commenting on something that
didn't really happen, that was AI created.
In a weird way, it may lead to more fact checking.
The misinformation, the disinformation, the bad actors who would use somebody's voice
or video.
In theory, after the first few times, people will learn their lesson and they will fact
check more.
your kids still think you're eating German shepherds?
Okay, so to explain that, if apple pie is made with apples and cherry pie is made with
cherries, in your entire life as a child you grew up with a giant German shepherd following
you around, what's shepherd's pie made with?
My kid, from the time he was old enough to talk, he would not eat them.
He didn't eat anything, and he was just like, no, I'm not eating that.
It wasn't for quite some time that we found out why he thought we were eating German Shepherds.
Okay.
You mentioned the movie The Bradley Wars in a video a while back.
What's your take on Boyd Burton and the fighter plane mafia?
Well, I mean, history kind of answered that, right?
If you're talking about the original crew, they really wanted specialized aircraft.
And a lot of the things they wanted, as time progressed, you realize that would have been
a really bad idea.
If you're talking about the newer crop, those opposed to the F-22, F-35, it's going to
be the same thing.
History will let us know.
I personally think there is definitely room for a multipurpose aircraft and specialized
aircraft as well.
Do you have a single older brother who likes Italian women or a cousin?
I can't say that I do.
I don't have an older brother.
I'm not sure about my cousins.
It's not a question I've ever asked.
Do you know what percentage of your viewers are conservative?
I would imagine that it's a pretty small number, but you seem to always have messages to respond
to.
How much reach do you think you actually have since most conservatives prefer to stay in
their silo?
It's around 15%, it is around 15%, but when you do the numbers, it's actually, I mean
it's a small percentage, but I mean it's worth doing.
Like it's worth putting in the effort to try to reach out to them.
That's why, if you don't know, getting into the silos, that's why my titles are the way
they are.
It's random words that don't kind of give away what the take is going to be.
Because a lot of conservatives will see me, they will see this background.
The title doesn't say anything that is off-putting, so they listen.
I'm of the opinion that once you've heard it, you can't un-hear it.
More serious question.
You've said before, if you plan to purchase a firearm, you need to train.
Would the type of shooting IPSCUSPSA do count?
You have to remember, I don't shoot anymore.
Okay, so I'll give you more generalized.
Personally I like, I think it's better to have very individualized, like if you can
find a combat vet and get instruction from them.
That is what I think is best.
At the same time, any training, as long as it is good training, is valuable.
As far as what everybody is teaching today, it's been a really, really long time.
I have no idea what's going on in that community or what they're teaching really.
Did you hear about the nine red wolf pups born at the WNC Nature Center?
I did not.
That's really cool.
It says this alone is an almost 5% increase in the total population.
One male and eight females.
I didn't know that.
That's cool and very welcome.
Is there a clearinghouse for mutual aid groups?
I am for sure not up to starting one, but I'd like to
join something established.
I just don't know how to find one.
I wish.
There are a few directories that exist, but I don't know
how often they're updated.
I would imagine that they end up being
obsolete pretty quickly.
The other thing to remember is that a lot of groups like that
are very, let's call it security conscious.
so they don't like being put on lists with other groups for fear of for fear of one group doing
something and somehow everybody gets blamed for it. If you're looking to find one in your area,
I would go and check out colleges that colleges in the areas around them, the event boards,
stuff like that. Look for flyers because that can kind of point you into the right direction sometimes.
Sometimes you may not find a flyer that says, hey you know join our group, but you'll find one
that says, hey we're cooking for people that don't have houses at this park at this time.
You'll find them there.
This is probably unanswerable or maybe just your opinion.
Is the country government really teetering on the edge, or is this just a big game and
both sides are playing us for fools?
Professional wrestling, Kabuki theater...
Ah, I mean, there's a lot to that.
So is it just a big game for a whole lot of them, yeah.
Is it coordinated in the sense of, you know, one side's going to do this, then the other
side's going to do this just to keep everybody on edge?
No.
And you have one party that is heavily flirting with truly flashy ideas.
And I don't know that the country is teetering on the edge, but there's definitely a risk.
And it's going to be around for a while.
It's not one of those things that's going to go away.
The chance for it to go away was at the end of Trump's first term.
Biden, if he had gotten like a landslide victory or a mandate, something like that, it would
have been possible for it to just go away nicely and quickly.
Because it wasn't shown to be a losing electoral strategy right away, more people are going
to try it, which means we have to be on guard longer.
Any updates on the Bow and Company Roadshow?
They say the pandemic is over, but it's still a concern, so I'm curious if it's coming
soon.
Okay, so for those that don't know, this channel actually started for us to go and do stuff
on the road, go and help people, go, you know, and for longer format content.
Right about the time we started getting everything up and running, the world stopped.
We had that whole pandemic thing, so we couldn't go anywhere.
During that time, we kind of waited, assuming that it wasn't going to take very long.
We were wrong.
And as we realized that it was going to be around a while, we started other projects.
So what we're doing now is clearing out everything that we have going on to include the infamous
gardening community garden video that's been coming for you know 16 years the
Jeep video during that time we we did this so there's the simple answer here
is yes we're eventually going to do the road stuff but we're gonna clear out
everything else first you made frequent videos about how some things are
national security issues in ways that most people wouldn't think, such as COVID, universal
health care, LGBTQ bigotry, causing readiness issues, energy dependence, etc.
Have you considered making one video to summarize all of the different issues that affect our
security?
Um, I haven't.
When it comes to stuff like that, I try to hit things like one topic at a time.
And the reason being is you might be able to move somebody on energy dependence, but
they grew up in an evangelical family and they would just never accept gay people.
If you stick it together and you put everything in one package, they may reject the parts
you could move them on because of the parts they don't like.
So I haven't done that.
At the same time, it might not be a bad idea for this channel, for the long format stuff.
I try to keep everything in small bits, but it's not a bad idea.
How would you publish and promote a book, traditional or indie?
And do you think that promoting it on social media, YouTube for instance, would
be effective. I'll let you know in a couple months. There's a book coming.
There's a book coming and it will be indie and we will be promoting it here.
So I'll let you know whether or not that was a good idea after we see
what happens.
How do you feel about those of us who are quietly trying to do God's work of caring
for the sick, the oppressed, the needy, and doing our best to treat every human being
with the dignity and respect they equally deserve, show that there is still good in
the Christian church and not all Christians are hypocrites as those who get the most media
attention for being as hypocritical as they are in their views, words, and actions.
John 1335.
When it comes to stuff like that, you can't tell, you have to show.
The Christian church has gotten a really bad name because of a lot of bad actors.
If you're out there doing good, and you're doing it under the auspices of a church, there's
a whole lot of people that are going to be very skeptical of you.
So you're going to have to just do it, and put up with the skepticism, and maybe you
change people's opinions.
Hi Bo, I've noticed your videos have started ending without your reaching over to turn
off the camera.
I know part of your brand is posting the best take with no cuts slash edits, so is it a
production quality decision you've made recently to edit out the camera reach or
did you get a remote or something I got a remote the I'm actually not sure if I
like it yet I'm trying to let it grow on me but there there's something
satisfying about turning off the camera when when I'm done but yeah we we
definitely haven't started editing anything then the reason we don't do
that is... I mean simply we couldn't keep up with the workflow. Like there's no way
we could do four videos a day and then be edited. Like even just something as simple
as clipping off the end would just take too much. Do you ever think you'll tell the full
story of your journey? I've always viewed memoirs as something you do when you're done.
So I mean, yeah, eventually, but hopefully it won't be for a long time.
What if anything do you think the Democratic Party can do in the state of Florida to message
better in an attempt to get a better representation in the state legislature?
Let's fill as if they are complacent with the status quo, because by having been a minority
forever they literally cannot accomplish anything, and it almost takes away any incentive to
work in my opinion. I think the new chair in Florida is exactly what the
Democratic Party needs and it's interesting because I know these
messages came in before this happened but take a look at Jacksonville. They
just you know they just flipped to that blue. I think you're gonna see some
change, and just remember, you know, she could have been governor.
With the Fed still trying to fight inflation, I feel like a perfect storm is
brewing where the U.S.
hits an economic recession going into 2024.
This causes voters to vote for Trump or DeSantis just out of desperation
for something to change.
My fear is that it could be the last true fair election if one of them get
into office, thoughts on that possibility.
I mean, there's a whole bunch of things that could occur when it comes to the economy.
If a Trump or Trump-lite gets elected, yeah, we're in real trouble because
they've already practiced, you know?
It's a risk.
I don't know that I would let it go to the point of fear.
I mean, it is definitely something that can happen, and it's something you have to be
aware of.
But you can't plan for every possibility when it comes to, you know, well, what if
a recession happens, what if, you know, I mean, there's, you have to play the cards
that are currently on the table.
What topics do you want to cover to get drowned out by updates for ongoing issues?
Oh wow, that's a list.
Cultural and philosophical issues, because I think if we could address those, then a
lot of the breaking news topics wouldn't be happening.
More like how-to on mutual assistance and community networking and stuff like that.
There's always like a hundred videos that I want to do at any given time.
But the breaking news is what kind of ties everything together.
And I also know that I can't, like as far as from a YouTube standpoint, the news has
to be there.
It can't just be me talking about, you know, doing good in the community.
don't watch that. So what do you think of Bob Dylan? Any musical favorites of yours?
The Beatles, etc. I love all music and I know a lot of people say that. I mean I
have a very eclectic taste when it comes to music and it's not so much any
particular artist, I tend to listen to music that reflects the mood I want to
be in rather than the mood I'm in, so it varies a lot. With so many voices
filled with such vitriol and hate, how do we calm down the fire beneath the pot
of our fellow citizens who are just itching for violence or a fight. I'm losing hope that people
will see that we don't have to actually fight to get solutions, but these people they want to draw
hard lines around good and bad citizens and I don't like not being able to trust that my own
neighbor won't start seeing me as his enemy. The good news about that is most of those who are
wanting to separate out the bad citizens." They're from a shrinking demographic. They're
older, they are more conservative, more far, far right, rather than just normal conservative.
And the people that they want to single out are a growing demographic.
So it's one of those things where every day that goes by, there's less of them and more
of us type of thing.
As far as calming things down, it has to be conservatives.
It has to be Republicans who acknowledge that this isn't the right way.
This isn't the right path.
There are a lot of changes going on at Fox, and I don't know that I'm optimistic, but
I'm hopeful that that's going to help a lot.
Hope all is well.
I am reaching out because I am in the process of learning to be a bit more handy.
First time home owner, soon to be first time dad, but I have encountered a highly frustrating
obstacle, having enough time.
I know there is a good chance that the likely problem is capitalism keeping me chained to
a desk for at least 40 hours a week, and the solution is a glorious people's revolution
It will liberate us all, and create the Star Trek future many of us want... post-scarcity
equitable society.
But since I would like to learn this in my lifetime, any pointers.
I have an interest in starting some sort of mutual aid type org, but more geared towards
technical practical education.
I live in a college town that is a little full of itself.
Not looking to put tradespeople out of business, in fact I'd welcome and appreciate their help.
disclosure I went to a 12 year college prep school don't worry I was tricked
into it by my dad who still teaches there so I got to be a peasant amongst
the children of the landed gentry we didn't have a shop class that is peasant
work you pay someone else to do such things I realized that many of my
neighbors picked up these skills and hobbies along the way so I must have
taking a wrong turn somewhere.
Was there a question in that?
Okay.
Having enough time and learning to be a bit more handy.
And you're about to become a dad?
Yeah, you're in a world of trouble.
Okay.
So as far as like handy work, doing stuff like that, the greatest, the greatest teacher
The greatest thing that you can do is mess up.
I once framed an entire room 18 inches on center.
I don't know why.
I was just learning how to frame and yeah, reframing the entire room was super annoying
and I will never make that mistake again.
There's a lot of things that you're going to mess up.
save on time. You know when you're doing a project they say you know you get the
materials you need plus 10% go ahead and do 20% you're gonna need it. There's
no substitute. I mean you can get all of the skills you know from YouTube or
TimeLife books or whatever but actually doing it you are going to mess up and
there's no way around that. There's no shortcut on that one. As far as having
Having enough time, don't worry, you know, you're about to be a dad, you're not going
to sleep, so it's going to be alright.
Just learn to embrace that and, you know, do stuff during that period.
Is it possible that rifles that shoot high-velocity bullets shoot low-velocity bullets instead?
If they outlawed high velocity ammunition, would it reduce the bodily damage if different?
Low velocity bullets were used.
Nice thinking outside the box on this one, but no.
The round in the AR, 223, moving roughly 3,000 feet per second.
The 45, what is it, 840 feet per second, something like that.
It's not necessarily the speed.
That has something to do with it.
It's also the design.
You have two ideas.
You have big and slow, like the 45, or small and fast.
At the end of it, every firearm is lethal and if you just start altering the velocity
which would basically be altering the internals of the round and what is typically in it,
it probably won't make its cycle.
The weapon itself won't function anymore.
I mean, yeah, it's outside the box thinking, but realistically, that's not it.
I've been a fan, a subscriber, and a Patreon supporter since you started your channel or
soon after.
My question is, how did you decide on your approach, and were you proposed to do it by
someone else, or was it an idea that you had on your own?
for the constant delivery of well-spoken, well-constructed content and advice.
And I only wish there were more people exposed to your channel.
It certainly couldn't hurt.
Well spoken.
And I'm like, I feel like I'm slurring every other word recording this.
How did I decide to do this?
I didn't.
It was a joke.
It was a joke.
The whole channel started as a joke.
I hid my accent for a really, really long time and yeah, I was celebrating with some
friends and celebrated a whole lot and we're sitting around and basically, I don't remember
what it was, but I said something like super redneck and they're like, you know, they had
never heard that side of me before.
At the time I was writing, doing like international affairs stuff and they thought it would be
like funny to be like hey you know the guy that's informing you about what's going on in all of
these weird places actually sounds like a guy from justified you know um so the first videos
were jokes and then from there i just had my phone in the pocket in my pocket and did some
in the shop and eventually realized hey i could actually do some good with this
And then the social commentary came and then I started actually taking it seriously so like
by the time I was giving any thought to approach or anything like that it was already up and running.
So I mean did someone else say to do it? I mean kind of. I mean I kind of got like goaded into
doing it, and I'm really glad that happened.
Apologize in advance if I am being impertinent, but the extreme authoritarian-like government
in your state seems opposite to what you tell us to believe.
You also know, you have mentioned it quite a bit, that climate change will wreak havoc,
especially there.
So my question is, why do you live in Florida?
Okay, they're the ones who are bad, why do I need to leave?
So basically, when it comes to climate, I'm actually in a decent area for climate.
I am at is basically going to end up being like Panama, the country, not Panama City.
But that's a hundred years from now, roughly.
As far as the government here, yeah, it's, that's a relatively new thing.
Castro was very much a leave-me-alone state until very, very recently.
And I'm hoping that that will return.
So yeah, I don't need to leave, they do.
So, okay.
Was really hoping you would do a follow-up, perhaps even a video on Anderson Cooper's
attempt to justify the Trump Downhall meeting.
As a Canadian, this looked as far from democracy and rural journalism that CNN could do.
I always respected Cooper, but after that speech, dot dot dot.
They wanted ratings.
They wanted people talking about it.
I'm not going to give them that because I agree with you.
I don't think they should have done it.
So I am not going to feed into the outrage.
There are a lot of times when somebody is intentionally trying to provoke outrage and
get clicks that way that I will just completely ignore it because it annoys me.
CNN knew what they were doing.
It's CNN.
They knew what was going to happen putting him on the air.
So I don't think that any of their justifications really cut it.
Okay, I've got no other Southerners whom I can ask that I think would both know the
answer to this and also not try to whitewash it.
When I was a kid, we used to drive from Kansas to the East Coast, and I was always super
freaked out by driving through bird country in West Virginia because of the three crosses
that would randomly pop up out of those trees on the mountains along the side of the road.
What are those?
My child mind, having watched Eyes on the Prize when it came out when I was in second
grade, always was really scared that it was some kind of white supremacist signaling.
Maybe they were Christo-fascists.
Okay, so those are all over the South.
Google Coffin daffer crosses, Coffin dollar crosses, something like that.
It really, as far as I know, it just represents Jesus and the two thieves.
That's it. There is no deeper meaning. I think the person behind him had a vision and I think
it actually started in West Virginia. He said that God came to him and said to build crosses
on the side of the road, but now they are all over the place, all over the South. They're
not just up there anymore. But yeah, to my knowledge, there's nothing militant about
it. How about that? It's just, you know, super Christian stuff. But now that I've said that,
I realize I've never actually looked into the guy, which I'm pretty sure it's Coffin
And now I'm like, I know I'm going to go when this is over and go and look it up and he's
going to be like a wizard or something, okay, let's see, okay, so, Americans with your world
worldview were an impossibility due to cultural isolation and the absolute predominance of
capitalist predicates and principles in every aspect of everyday life.
Here's my question, what experiences and readings do you identify as fundamental in
shaping your worldview?
What experiences and readings?
It was experiences first.
Watching people suffer.
I did not move left out of some grand theory or reading something that really moved me.
I got tired of watching people suffer.
That was what actually sent me that way.
From there it was more personal observations about the nature of government and how it
functions and that just sent me further left and made me more anti-authoritarian.
I saw a picture of your old EDC kit, and you had a MacGyver knife.
My dad gave me one, and I have no idea how to use it, and now he's gone.
Yeah, I have a Swiss Army knife.
So probably one of the big ones, all the different blades.
I don't know, I could do a video on that.
Hang on to it.
People learn how to use it, and it's a very useful thing.
I'd like to ask if there's anyone Bo respects professionally who provides quality news from
the other side of the political spectrum.
The absolute other side, like the authoritarian right?
No.
I don't respect any of them.
There are some anti-authoritarian right that I respect as people, even though I think their
views are wrong.
But most of them, they tend to just like live stream.
They don't actually, they don't really provide a lot of commentary.
They just show up and report.
I think a good one, Ford Fisher.
I think he's, I think he's right.
right-ish. As far as, let's see, somebody who, um, Carrie Wedler is anti-authoritarian
right. And, like, she does commentary. I definitely respect her. Again, we don't see eye to eye
eye on everything, but if you're looking for something to kind of widen your view,
there's that as an option.
She is very, very anti-authoritarian, just so you know.
That got me thinking, oh, okay, that got me thinking, should I have called the police
on that truck.
Should I have just minded my business?
Now, I'm the type of person who doesn't like to stand by and watch as someone is hurt,
but I also don't want to put someone in jeopardy with the police just because maybe their door
was broken, or whatever reason I cannot think of.
That brings me to the question, when is it worth it to call the police?
I've watched a lot of your videos, and a few of them have a series of questions that
you pose to yourself before making a decision.
Is there a series of questions I should ask myself before calling the cops?
Is whatever is going on worth lethal force?
That's generally my test there.
If whatever is happening isn't actually worth lethal force, there's probably a better way
to deal with it than calling the cops.
Because that's what you have to acknowledge.
When you call law enforcement, you are summoning somebody whose job is to enforce the law,
and every law is backed up by penalty of death.
There should be other options.
There should be other options than just calling the cops or doing nothing.
It really depends on your area, you know, it depends on where you live, the way the
cops are around you, stuff like that.
I mean, the, let's see, well, I don't want to say that because, yeah, good example.
The area where I live in.
We have one sheriff's department where the last time that they killed
somebody was in the sixties.
Um, and then we have another one that is just, I mean, they
are, they're pretty trigger happy.
Um, so it really does vary a lot there as well.
Like if you still live somewhere where you have like an Andy
Griffith style police department, you know, a consent-based policing
type of thing, maybe there's a little room but generally speaking
that's that's the question I ask myself
What can I do to help that does not require
peopleing or cash? Amplify the message of those people who people well
do things in private and drop them off that's another one
There are, like when it comes to, I'm assuming this is about mutual aid, there are always
things that can be done that just require time, you know, because, good example, okay,
So when we were doing Relief After Michael, we got all these orange buckets, and it was
weird because I think they were like Home Depot buckets or something, but anyway, you
open them up and inside it had a line to dry clothes, it had cleaning supplies, it had
gloves, it had like all kinds of stuff.
Somebody had to put that together somewhere, and that's the type of thing that you could
totally show up at the place where they have those resources and be like, hey, give me
the stuff to make 30 and go home and do it like by yourself if you just don't like being
around people.
The good news is when you're talking about those who actively get involved and try to
help, they're pretty accommodating.
They understand that people work better in different ways.
So a lot of times, you can just find the outfits near you that are doing something, which might
take a little bit of people in, but not a whole lot, and then just explain, hey, I would
like to help, but I don't like being around people, or you know.
There's always an option, you just have to find it.
A lot of times it's just a matter of explaining the accommodation that you need.
Just wanted to ask your opinion, I want to do something to help the Ukrainians, buying
a coffee mug just ain't doing it, and then a question about NAFO in general.
You know, if you like debunking stuff and stuff like that, yeah, I mean, that's what
they do.
So I mean, it wouldn't, I, you know, it's one of those things where it depends on your
personality type because they do have a lot of odd humor that some people may not really
find their cup of tea.
But if you've seen their posts and you're just wondering, you know, is there anything
like bad about them, no, they are exactly what they seem like on the surface.
Do I understand you to say that some men and women are born warriors while others, by nature,
workers who can't be trained to become competent fighters?
Would you expand on that idea, please?
It suggests that an all-volunteer army is necessarily stronger than an army of conscripts,
and that the Vietnam Draft was a wasteful use of human capital by military and competent
leaderships."
Yeah, okay.
So there's a couple of questions in here.
Are some men and women born warriors?
There are some men and women who are born killers.
There's been some research on this and generally speaking it's about 4% of the population.
Now whether or not from there them having that predisposition, I guess, whether or not
they're actually trained into being warriors is something else.
When you're talking about conscripts, people have to be conditioned to kill.
Most people are not killers.
The issue when you're talking about the difference between a volunteer army and conscripts is
that if you have an all-volunteer army, generally speaking, even though they don't, initially
everybody thinks they know what they signed up for.
Until everything starts getting really loud, everybody's like, I know what I'm doing.
When it comes to conscripts, not necessarily.
They may not want to be there, so they're going to have lower morale.
And then, because they aren't volunteers, typically conscripts get less training.
There are some countries that have conscription-based systems that appear to be effective.
We haven't really seen them in use in modern combat yet, but they appear to be effective.
But I would point out that there's a lot of training there.
And that's what it has a lot to do with, is how much training the individual is going
to get, how much conditioning is going to be there.
And generally by the time a military is conscripting people, they're in a bad way on the battlefield
and they're just trying to get people there, that they're not focused on the training.
very quick like what we're seeing with the Russian troops in Ukraine.
They're not getting good training and it's showing.
Is there a way to improve messaging for the Democratic Party?
It's funny because there's a whole bunch of videos going out right now about all of this.
I'd rather not resort to three-word chants like the MAGAs, but they do seem effective.
The DIMMs have a lot to boast about, but they are constantly drowned out by culture-reward
nonsense.
There is no cohesive message in the party.
It's because of the fact that it's a coalition party.
They're never going to have a cohesive message.
They probably need to work on their messaging at the caucus level.
Each little faction developed their own messaging and everybody agreed not to attack each other
over it because what is going to work in some areas will not work in others.
Okay.
There is a...
So the question is, why is the Russian military seeming to direct a lot of weapons at civilian
areas rather than the Ukrainian military.
One of the assumptions is that the Russian military has the capabilities that the US
military does when it comes to precision guided munitions.
You think back to the Gulf War, what do you remember?
The footage of the bombs hitting right on the cross, right?
going through windows type of stuff. Odds are their stuff just isn't that good.
Now I'm not saying that they are not deliberately targeting
civilians. They are definitely targeting civilian infrastructure. They are
definitely hitting places that they shouldn't be according to the laws of
armed conflict. Some of that always happens in any conflict. Some of it is
because of poor technology and them just not being up to modern standards.
And then the rest of it, I don't have a clue.
You're looking for a military advantage here, I don't get it.
The idea of demoralizing your opposition that way, it doesn't seem to work the way it used
to, like in the first half of the 1900s, yeah, that actually did work.
It doesn't work as much today because people tend to just adopt unconventional means and
become non-state actors, but I have a feeling Russia is going to have to learn that the
hard way.
Eventually a young co-worker of mine passed away unexpectedly.
He was a mentee and a friend.
I spent most of the last year teaching him and getting to know him and very much being
his work dad.
And one day he just didn't show up and the next day we were informed that he had passed
away.
This was a few weeks ago and I am starting to emerge from the thunderstruck feeling.
I'm in my mid-forties and he was in his mid-twenties.
It's the first time in a long time suffering a loss, not since my teens, as a person I
know personally died.
I've read all the advice about grief and grieving process and how it's different for everyone.
I was hoping you might have some thoughts based on your own experiences and those of
some folks you know personally who have had to live through this many times.
I'm sorry for your loss. I don't have any good advice on this. I am very detached, I
think might be an inappropriate word. I don't want to say you get used to it. I don't know
I don't know how to answer this.
Um, there's, I mean it's right, it is different for everybody.
And your, what you're feeling, whatever it is, allow yourself to feel it.
You know, if it made you question your own mortality, allow yourself to feel that.
Um, and eventually, I mean, hopefully this doesn't happen, but it, yeah, it gets easier.
That sounds so horrible to say.
Um, yeah, it's, it is a very unpleasant and something that isn't predictable.
How it affects you is not predictable.
It may be an entirely different experience the next time something like that happens.
Be in the moment with it.
Experience the feelings.
Allow yourself to experience it, but also acknowledge that it's an inevitability of
life.
Some of your videos reply to a particularly ill-advised question.
Do those people ever send you replies after you post the video?
What's the best one?
I'll let you decide what best means.
I had a guy, a rural guy, send me a message once asking for proof of systemic racism.
And I thought I was being pretty nice in the video.
But people in the comments were like, you know, you kind of went in hard on the guy.
And then it wasn't even that long ago, maybe a month or two ago.
He showed up again, and was basically like, you know, it wasn't something in, you know,
video that really caught my attention was something in the comments section and it was
just, I don't know, that to me was the best because it showed not just did it actually
lead to change with this person, it led to change and they stuck around the channel.
And it showed the interaction and it wasn't me, it was y'all and you know a big part of
the channel is fostering the community.
So that to me was the best because it kind of brought everything together at once.
How do you manage your time?
Your video posts are frequent and you have a clear and deep understanding of the issues
on which you're commenting, not just major headline news, but low, mid-level candidates
in other states as well as ballot measures the list goes on.
All of this on top of being a husband and a father, your work fostering, community networks,
local disaster aid, and apparently taking care of horses and who knows what else on
the ranch.
I'm not married, nor do I have children.
While I've got a few accomplishments under my belt I'm proud of, I feel overwhelmed
if I've got more than one or two long-term projects, to say nothing of an obligatory
errand before or after work.
do I manage my time? First I don't really sleep. I don't sleep a whole lot. I would
like to point out because if I don't say this, she's like, I can't believe you took credit
for taking care of the horses. My wife pretty much does like almost everything for the horses.
To me they're big dogs. I get to go play with them. But so one of the things that I can't
wait to be able to change and this will this will be happening soon you know I
get a lot of credit for the stuff that this channel does because I'm the person
on your screen but there are like five other people that that do stuff for this
and that helps a lot with the time management I I research I make videos if
you're watching this video anywhere other than YouTube I have no idea how I
I got there. It might be on a platform I've never even heard of. Somebody else does that.
When it comes to doing the disaster relief or a lot of the community networking stuff.
When we did the stuff for the mines and got all the Christmas presents and stuff like
that. I didn't order any of it. I know that it took like three hours to order all of that
stuff but only because they were in the room doing it.
It takes a team to do what we do.
I do feel like a lot of times I get credit for like the work of like a whole lot of people.
So for me, as far as managing time, do what you do best.
Research and make the videos.
Everything else that goes on with a YouTube channel, somebody else does, I make the content.
That's my time management secret right there.
You have people who, when they start a YouTube channel or whatever, they try to do it all
or they bring on people to help them with what they do.
If the YouTube channel is going well, you don't need help with that.
You need help with everything else because there's a lot of stuff that goes on behind
the scenes and that's on any channel and you need to find the people who are who
are good at doing whatever it is their thing you know your thing is making the
content their thing may be being super detail oriented and getting all of the
stuff together it may be making sure that all of the set and the construction
stuff goes right it may be you know you have to have to learn how to work as a
team that's a big part of you know managing time but I mean aside from that
it's also like I really I sleep like four hours a night so that helps you
know functional insomnia okay are there any Easter eggs we missed since you came
to the new shop. Probably. I've been using them a lot. Since we've been here, I know
a lot of people miss the fifth element stones. In the video that was an empty that just had
empty shelves. They're actually stones on the shelves. Let's see in a video recently
Recently I referenced the Federalist Papers, and there was a little figure of Hamilton
over my shoulder, Zartan was there recently, the Lament Cube from the spooky movie, that
was on a shelf recently.
Being here in the new shop, I actually do them a whole lot more, because I don't have
to go like as far to get home.
Can you share how, see I should have just waited for this, can you share with us how
big your team is and what the various roles are?
I'm curious what the scale of your operation is and if there are plans in motion for your
own journalistic org to blossom with your values.
That's funny.
So let's start with the first part.
How big the team is.
There's me plus five.
And we have one person who does the logistic stuff, like all of the stuff like ordering
the stuff for the Christmas presents, or making sure that we have the stuff we need when we
go do disaster relief. We have somebody who does all of the transferring. I put it on
YouTube, they pull it from YouTube and send it all over the internet. We have a person
who handles all of the finances, the money side of things. Make sure that when I do a
a live stream that I don't forget to actually send the money where it needs to go, you know?
And then we have a person who kind of does like catch-all stuff, and this person can
do video editing.
This person can do like a lot of stuff that has to do with YouTube itself.
And then we have a person to basically fix everything that I break.
I mean, that's not really what the, but I mean,
it's kind of what it is.
Now, most of these people you will actually meet
now that we're in the new shop,
because in some of the stuff that we're filming,
you'll see them, they'll be present.
Some of them very much value their privacy
and you'll never see them.
But that's kind of the roles and the team.
the team. Now, as far as it blossoming into a, you know,
journalism organization, probably not. Because that's how
I got here. And I actually like this a whole lot better. So,
okay, oh, I guess that's it. All right. So I have no idea how
this has been going on, but it looks like we are at the end of those questions. And I know that
there are more. I think I read this aloud. There are other themed Q&As coming. I guess one of them
is a dating video, which should be fun. And we'll get those out soon. So anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}