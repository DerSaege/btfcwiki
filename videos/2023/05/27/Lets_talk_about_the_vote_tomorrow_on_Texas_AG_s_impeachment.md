---
title: Let's talk about the vote tomorrow on Texas AG's impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HR-24CYhuEI) |
| Published | 2023/05/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas Attorney General Ken Paxton faces impeachment with unprecedented speed, with the process happening on a weekend with 20 articles of impeachment.
- Paxton claims his impeachment is illegal, citing the need for voters to be aware, but experts argue that the Texas state constitution supersedes any statute.
- The rapid impeachment process suggests that the House likely already has the votes secured, possibly due to allegations against the Speaker of the House.
- Paxton, an influential figure with insider knowledge, may be making calls to prevent potential impeachment threats.
- Despite potential legal challenges, the House seems determined to remove Paxton from office, with some members seeing it as their duty to do so.

### Quotes

- "He will be looking at 20 articles of impeachment."
- "One of them said that to not move forward, they would be derelict in their duty after hearing the evidence."
- "The impeachment is moving quickly."

### Oneliner

Texas Attorney General faces rapid impeachment process with potential legal challenges, signaling strong House support and urgency for removal.

### Audience

Texas Residents

### On-the-ground actions from transcript

- Contact local representatives to express support or opposition to the impeachment process (implied)
- Stay informed about the developments in the impeachment process and potential legal battles (implied)

### Whats missing in summary

Insights on potential implications of Paxton's removal and the impact on Texas politics. 

### Tags

#KenPaxton #ImpeachmentProcess #TexasPolitics #LegalChallenges #HouseSupport


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about the impeachment process of
attorney general, Ken Paxton of Texas again, um, I mentioned in an earlier
video that it could happen very, very quickly.
It's happening Saturday.
Um, if you're watching this on a Saturday, I mean today, uh, they are
coming in on the weekend to do it.
He will be looking at 20 articles of impeachment.
They do not seem to be willing to slow walk this at all.
Now in totally unsurprising news, the Attorney General has said that his impeachment is of
course illegal.
You know, nobody's allowed to do that.
And he is using the argument that I laid out in that other video, talking about the statute
saying that the voters had to know about it and all of this stuff.
I talked to somebody who actually does understand the constitutional law in Texas way better
than I do, and his way of addressing this was pretty entertaining.
He's like, hey, do you know what doesn't supersede the Texas state constitution?
Some random statute you found.
So he doesn't seem to think that's going to hold water because the Texas state constitution
would supersede any statute.
To amend the state constitution, you would have to amend it, not just enact a statute.
While I'm not an expert on Texas constitutional law, I'm going to say, yeah, that tracks.
That's the way it is in every state that I do know about.
So what can we kind of infer from the speed?
They probably already have the votes.
This went on for months very, very quietly.
And it appears that the allegations directed towards the Speaker of the House about being
a liberal and perhaps celebrating a little too much while at work, those came out according
to rumor when Paxton found out about it, which was the day before it was publicly announced.
So, it seems pretty likely that they have already mustered the votes for it.
It doesn't seem like a move a bunch of Republicans would do without knowing they had the backing
of the House already.
Now Paxton is an influential person, and he's got the governor's ear, and he's been around
a long time.
He knows a lot of secrets about a lot of people.
So I would imagine those people are having their phones ring tonight saying, you better
not impeach me.
That would make sense as far as a move from Paxton's side, because the speed at which
this is moving suggests they definitely intend on following through all the way and removing
him from office.
In fact, one of them said that to not move forward, they would be derelict in their duty
after hearing the evidence.
So there will probably be some litigation about whether or not the House really has
the power to do this.
The people I have talked to say they absolutely do, but the Attorney General will probably
still try to fight that.
But if they impeach Saturday afternoon, he will be fighting that while basically suspended
from office.
So the impeachment is moving quickly.
The trial may or may not carry the same sense of urgency.
We're going to have to wait and see how they react once the final vote is taken.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}