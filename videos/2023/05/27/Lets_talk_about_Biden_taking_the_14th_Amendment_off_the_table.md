---
title: Let's talk about Biden taking the 14th Amendment off the table....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mkhL9a8AAbc) |
| Published | 2023/05/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration taking the 14th amendment off the table regarding the debt ceiling sparks questions on the reasoning behind this decision.
- Speculations arise on whether Biden's choice is to appease Republicans or to avoid economic damage through legal challenges.
- Biden's announcement of not using the 14th amendment is criticized as a mistake in negotiations, akin to Trump telegraphing moves in Afghanistan.
- Telegraphing moves in negotiations is seen as detrimental, as it diminishes leverage and allows opposition to prepare.
- Dealing with Republicans who are not negotiating in good faith requires treating them as the opposition to protect the country's interests.
- The Republican party's indifference to economic stability under Biden leads to the necessity of cautious and strategic negotiation tactics.
- Public statements during negotiations are cautioned against, as they can weaken one's position.
- Keeping quiet during negotiations is viewed as a prudent move, allowing for strategic advantage by not revealing intentions.
- The importance of maintaining secrecy and strategic ambiguity during negotiations is emphasized to prevent undermining one's position.
- Biden's decision to announce taking the 14th amendment off the table is deemed unwise and potentially damaging to negotiation outcomes.

### Quotes

- "You can't telegraph during negotiations."
- "You have to treat them as if they are actively trying to damage the country."
- "Telegraphing moves in negotiations is bad."
- "Biden, if you notice, has been really quiet during all the negotiations."
- "It wasn't smart."

### Oneliner

Biden's public withdrawal of the 14th amendment option in debt ceiling negotiations risks weakening leverage and echoes past mistakes in telegraphing moves, urging strategic silence in dealing with uncooperative Republicans.

### Audience

Negotiators, policymakers

### On-the-ground actions from transcript

- Maintain strategic silence during negotiations (suggested)
- Treat uncooperative parties as opposition for protection of interests (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Biden's decision to remove the 14th amendment as an option in debt ceiling negotiations, discussing the impact on negotiations and the necessity for strategic secrecy.

### Tags

#Biden #Negotiations #DebtCeiling #RepublicanParty #StrategicSilence


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the Biden administration, taking the 14th
amendment off the table when it comes to the debt ceiling and what it means.
Because spokespeople have kind of said, you know, he's not going that route, not
something to even talk about, so on and so forth.
A bunch of messages have come in saying, is this the right move?
You know, why is he doing this?
Is this, you know, him trying to make the Republicans feel at ease?
You know, what's going on?
Well, I think the reason he doesn't want to do it is because when it makes its way through
the court system, because it will certainly be challenged, it would cause damage to the
economy.
And unlike the Republicans in the House, Biden isn't trying to damage the economy.
I think that's the reason he's not going to use it.
Now as far as announcing it, why did he do that?
he made a mistake. That was a mistake. It's always a mistake. When you're
negotiating with somebody, you don't rule things out. That's not
something that you do, especially in public. Why did the withdrawal from
Afghanistan lead to the opposition retaking the country almost immediately?
immediately because Trump telegraphed his moves.
It was a bad idea then, it's a bad idea now.
Trump got out there over and over again and said, we're going to leave, we're going to
leave.
You know, I want to get out.
So his negotiators didn't have any leverage.
By saying it's off the table, Biden did the same thing.
It's not a good move.
Now, they are closer to a deal today, so maybe he feels more comfortable with it and really
doesn't think it needs to be an option.
But generally speaking, in any negotiation, if you are telegraphing your move, it's bad.
When Trump did it, he told the opposition, you don't have to worry about the United States
anymore, gear up to fight the national government.
As soon as he did that, it was over.
It was going to go bad.
And I hate to be the one to say it, but at this point, when you are dealing with Republicans
who are not negotiating in good faith, you have to deal with them as if they're the opposition,
because they are.
The Republican party has made it pretty clear, they don't care if your economic stability
is damaged.
In fact, it's good for them because if things go bad under Biden, a whole lot of Americans
who don't follow politics will blame Biden.
So you have to treat them the same way.
You have to treat them as if they are actively trying to damage the country, which means
you can't telegraph your moves.
The normal way of discussing things among politicians and everybody just sitting around
and no cameras around and everybody's rhetoric is very, very different, you can't do that
anymore because they are not operating in good faith.
They are not operating in the best interests of the country.
That's clear.
So why did he do it?
Because he doesn't want to damage the economy.
it a good move? No, I don't think so. I think it was a mistake when Trump did it.
I think it's a mistake now that Biden's done it. You can't telegraph during
negotiations. You can't take things off the table. You can't really make public
statements about what you want. Biden, if you notice, has been really quiet during
all the negotiations. They haven't really been talking about it, which is the
right move. Why he did this right now? No clue. But it wasn't smart. Anyway, it's
It's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}