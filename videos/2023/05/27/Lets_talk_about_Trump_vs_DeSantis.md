---
title: Let's talk about Trump vs DeSantis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UpRCllEUS6A) |
| Published | 2023/05/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contrasts Trump and DeSantis in their political approaches and personas.
- Describes Trump as defined by grievance, rage, and drama, while DeSantis is trying to run as Trump without the drama.
- Explains that DeSantis is focusing on being anti-woke and policy-oriented, which may not be a winning strategy.
- Points out that DeSantis is trying to adopt Trump's policies without the anger and drama associated with them.
- Criticizes Trump's policies, such as mishandling a pandemic and building a failed vanity project, as not appealing to the majority.
- Suggests that DeSantis's efforts may work within a hard-right Republican base but could face challenges on a national level.
- States that most people didn't vote for Trump based on policy but rather for the excuse he gave them to be their worst selves.
- Expresses skepticism about DeSantis's chances of winning as long as Trump is in the race.
- Speculates that if Trump exits the race, DeSantis might become a more significant contender but still faces obstacles in a general election due to his policies.
- Concludes that DeSantis may struggle to gain broader acceptance beyond his hard-right base and favorable media coverage.

### Quotes

- "DeSantis is trying to cast himself as Trump without the drama."
- "The cruelty is the point. The drama is the point."
- "I don't see how he can make it through the primaries."
- "Let's make America Florida."
- "Anyway, it's just a thought."

### Oneliner

DeSantis attempts to emulate Trump without the drama, facing challenges in policies and national appeal, while Trump's base remains rooted in cruelty and drama.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze political candidates without bias (implied)
- Stay informed about political strategies and policy implications (implied)

### Whats missing in summary

Insights into the potential impact of political personas and policies on public perception and electoral outcomes.

### Tags

#Politics #Trump #DeSantis #Election #Policies


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about DeSantis and Trump,
how they're different, how they're the same,
what each one is running as, and why
a lot of political commentators aren't paying
a lot of attention to DeSantis, while a lot of major outlets
are.
OK, so first, who is Trump?
Who is he?
He's grievance defined, right?
Rage, anger.
If you're trying to describe him in a word, it's an emotion.
That's how he appealed to people.
DeSantis is trying to cast himself
as Trump without the drama.
That's how he's running.
He's Trump without the drama.
He's talking about being anti-woke and policy.
We've talked about it before on the channel.
Anti-woke isn't a winning strategy.
The majority of Americans have a positive connotation
of the word woke.
When you step out of that conservative echo chamber
and you get into the independents, people like woke.
They know what it means.
Woke means alert to injustice.
Anti-woke means bigot.
They've got that figured out.
That's not going to help him get the independence he needs.
Policy running on Republican policy?
That's a horrible idea.
It's not good objectively.
It's not good.
It doesn't perform well.
He's going to have Trump's policy without the anger, without the drama.
Okay.
What was Trump's policy?
Writing Obama's economy, disrupting the military, undermining the
United States foreign policy efforts, mishandling a pandemic, building a
giant vanity project on the Southern wall that failed.
I don't know that anybody really wants that.
A lot of DeSantis, a lot of the efforts that he has pushed forward here in Florida, yeah,
they work to a Republican base, to a hard right Republican base.
They don't even work to all Republicans.
Once that stuff hits the national scene, it's probably not going to go over well.
If you are running as a candidate who's trying to absorb another popular candidate's base,
you've got a huge task ahead of you.
And when you're talking about Trump, nobody voted for Trump for policy.
Most of them have no idea what it is.
They voted for him because he gave them an excuse to be their worst.
The cruelty is the point.
The drama is the point.
If DeSantis is going to try to market himself as Trump without the drama, I don't know
that that's a winning strategy.
As long as Trump is in the race, I don't see how DeSantis can win.
I really don't.
Now if Trump falls out of the race for whatever reason, and there are a number of ways that
could happen, then people should probably start paying more attention to DeSantis.
Because there is a chance that he would win the primary then.
Not because he inspired the same level of enthusiasm, but because he's the next best
thing.
And even then, I don't know that that's going to hold up in a general election.
There's a whole lot of policies that people are going to have to assume he'll want to
take to the national level, which will alienate a lot of people.
He played to a hard right base, and it's worked.
It's turned him into a star within the GOP.
But without the drama, he can't beat Trump.
And that hard right base, the things he did to get that cred, the legislation he supported,
that's probably not something that's going to play well on the national scene.
So until Trump is out of the race, I don't see how he can make it through the primaries.
I mean, maybe something wild happens, but it's going to have to be a wild card event.
I don't see how he's going to get there via campaigning.
Then if he gets through the primaries, I would be utterly shocked if the American people
actually accepted his policies once they are put on full display.
Once they get out of the very favorable media outlets, I don't think that the average American
is going to be, hey, let's make America Florida.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}