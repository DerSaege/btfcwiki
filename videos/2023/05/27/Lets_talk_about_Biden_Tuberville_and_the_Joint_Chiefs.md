---
title: Let's talk about Biden, Tuberville, and the Joint Chiefs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oivvIsXgwhc) |
| Published | 2023/05/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's selection for the new chairman of the Joint Chiefs, General C.Q. Brown, is about to intersect with Senator Tuberville's roadblocking of military promotions.
- Senator Tuberville has been blocking around 180 military promotions, damaging military readiness, including those of the current Air Force Chief of Staff and the incoming Chairman of the Joint Chiefs.
- Huntsville, Alabama, is striving to support the military for the Space Command presence at Redstone to boost the economy.
- Despite Huntsville's efforts, if politicians in a state create an environment that undermines military readiness, it deters the military from being there.
- Senator Tuberville's actions show a lack of commitment to military readiness in Alabama, impacting the state's reputation.
- Alabama faces a choice between damaging actions by politicians or striving for a stronger economy and better quality of life.
- Senator Tuberville's remarks about inner city teachers' literacy, coming from a state with low education rankings, reveal a disconnect from reality and potentially harmful attitudes.
- The goal behind such comments is to incite division and maintain a status quo of kicking down on others.
- Outdated approaches and bigoted legislation can deter people from joining the military, affecting recruitment and retention.
- Alabama must decide its priorities and voting patterns to shape its future and military investments.

### Quotes

- "It doesn't matter what cool little incentives you offer. If politicians in the state create an environment that damages military readiness, the military is not going to want to be there."
- "Do you want to kick down at people? Or do you want a stronger economy?"
- "People may not join a force if they think they're going to be sent to a place like that."
- "The people of Alabama need to decide what they want and they need to start voting that way."

### Oneliner

President Biden's pick for the Joint Chiefs intersects with Senator Tuberville's damaging block on military promotions, posing a choice for Alabama between military readiness or a stronger economy.

### Audience

Alabama Voters

### On-the-ground actions from transcript

- Vote in alignment with policies supporting military readiness and economic growth in Alabama (implied).
- Advocate for inclusive and supportive legislation that enhances recruitment and retention in the military (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how political decisions in Alabama can impact military readiness and economic opportunities, urging voters to prioritize these factors in their political choices.

### Tags

#Alabama #MilitaryReadiness #SenatorTuberville #PoliticalDecisions #VotingPriorities


## Transcript
Well, howdy there, internet people.
Let's vote again.
So today we're going to talk about President Biden and his selection for
the new chairman of the joint chiefs, and we're going to talk about Senator
Tuberville from Alabama and his campaign to roadblock military promotions and
how those two things are about to intersect and the people of Huntsville,
Alabama are going to probably end up paying the price.
Okay, so General Milley, he's leaving, so there's going to be a
new chairman of the Joint Chiefs.
Biden wants General C.Q.
Brown, been around a very, very long time.
He's the current Air Force Chief of Staff.
Now, Senator Tuberville has been engaging in a roadblock when it comes to the
military promotions. There is a process that high-ranking people have to go
through in the military and it involves being confirmed. It involves basically
the Senate and he is blocking these paths. He's holding up I want to say 180
promotions at this point, I can't keep track, a bunch, and this is damaging
military readiness. The new chairman of the Joint Chiefs is going to have to go
through this process. So the senator's roadblock is going to impact the current
Air Force chief of staff, the person who is about to become the new chairman of
Joint Chiefs. And right now, Huntsville, Alabama is doing everything it can to show that it supports
the military because they want space command there. They want it at Redstone because it's going to be
good for their economy. The problem is it doesn't matter what cool little incentives you offer. It
doesn't matter how many American flags or yellow ribbons you put out on the road. It doesn't matter
what political statements you make. If the politicians in the state create an
environment that damages military readiness, the military is not going to
want to be there. If politicians in the state pass legislation that is bigoted
in some way, that targets certain demographics, that does things that
would undermine recruitment and retention, and therefore readiness, the military does
not want to be there.
That's the situation they're in now.
Now the current Air Force Chief of Staff, soon to be Chairman of the Joint Chiefs, is
going to have his own promotion held up by Senator Tuberville from Alabama.
This does not show that the state of Alabama is committed to military readiness.
It shows the opposite.
The people of Alabama have a choice to make.
Do you want to kick down at people?
Do you want to pass legislation that doesn't make your life better but it does make somebody
else's life worse and therefore you feel better about the lot you find yourself in because
you keep letting these politicians trick you into voting against your own interests?
Or do you want a stronger economy?
The politicians in Alabama are damaging Alabama.
There's no doubt about that.
It's occurring.
Senator Tuberville is also out there talking about how he doesn't know if inner city teachers
can read and write.
The senator from Alabama, a state whose ranking in education is normally in the 40s, has very
serious opinions about the state of education across the country.
Again, what's the goal of that?
Inner city, right?
The goal here is to make people feel better about kicking down.
It's to dog whistle.
That's what it's about.
like the legislation. The thing is, it's a new age. People don't want to be around
that. So, people may not join a force if they think they're going to be sent to a
place like that. Therefore, it undermines military readiness. Therefore, the
military will look for different locations to put major investments in
the future. The people of Alabama need to decide what they want and they need to
start voting that way.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}