---
title: Let's talk about Montana, Missouri, and outcomes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mC5eBX_4KOI) |
| Published | 2023/05/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Missouri's ban on gender-affirming care has a temporary restraining order, set to expire on May 15th with a hearing on May 11th.
- In Montana, trans representative Zoe Zephyr is being targeted by the legislature, similar to Tennessee, for speaking against a ban on gender-affirming care.
- The legislature is attempting to silence Zoe Zephyr because she hurt their feelings by warning about the ban's detrimental effects.
- There are protests and legal actions to ensure Zoe Zephyr can represent her constituents.
- A lawsuit has been filed, claiming people are being denied adequate representation by silencing Zoe Zephyr.
- The likelihood of Zoe Zephyr's lawsuit succeeding is high, but it may not be a quick process.
- A historical case involving Julian Bond in Georgia shows legislators can't exclude representatives for expressing unpopular opinions.
- The situation in Montana mirrors the Julian Bond case and may result in a similar legal outcome.
- Judges in Montana may sympathize with legislators' hurt feelings, potentially affecting the case's progress.
- Denying representation based on offensive speech is a problematic power play by the legislature.
- Republicans in power likely aim to avoid negative commentary rather than being influenced by Zoe Zephyr's statements.
- More lawsuits against the legislature's actions targeting Zoe Zephyr are anticipated.
- Beau predicts further legal actions from constituents in addition to Zoe Zephyr.
- The situation raises concerns about the abuse of power and suppression of representation.
- Expect ongoing legal battles and challenges to ensure fair representation for marginalized communities.

### Quotes

- "Missouri's ban on gender-affirming care has a temporary restraining order."
- "The legislature is attempting to silence Zoe Zephyr because she hurt their feelings."
- "Denying representation based on offensive speech is a problematic power play."
- "More lawsuits against the legislature's actions targeting Zoe Zephyr are anticipated."
- "The situation raises concerns about the abuse of power and suppression of representation."

### Oneliner

Missouri and Montana face battles over gender-affirming care bans and silencing of a trans representative, raising concerns about abuse of power and representation. 

### Audience

Advocates for marginalized communities

### On-the-ground actions from transcript

- Support legal actions challenging the legislature's suppression of Zoe Zephyr's representation (implied).

### Whats missing in summary

Details on the specific legal strategies and community support efforts to aid Zoe Zephyr in her fight for adequate representation.

### Tags

#GenderAffirmingCare #Representation #LegalBattle #TransRights #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about what's going on
in Montana and representation and Zoe Zephyr.
And we'll talk about the likelihood of her suit succeeding.
Before we get into that,
we're gonna talk real quick about Missouri
because there's probably a huge overlap
in the people that are concerned about both issues.
In Missouri, the ban that was set to take place on gender-affirming care, the temporary
hold on enforcement has turned into a temporary restraining order.
That's good.
Currently it is set to expire on May 15th, but there is a hearing on May 11th.
So that is moving along there.
in Montana if you are not familiar with what's been happening Zoe Zephyr is a
trans representative. The legislature there much like in Tennessee is trying
to silence her, trying to exclude her. Very similar situations. A very red
legislature overstepping their bounds and they're doing this mainly because
Well, she hurt their feelings. I mean, it's really what it boils down to.
She was arguing against a ban on gender-affirming care, and she said,
Hey, if this passes, you know, it's going to have detrimental effects.
You're going to have blood on your hands. And this bothered them.
And it led to her being silenced. It led to protests. It led to a lot of things.
things and now it's at the point where they're doing everything they can to
make sure she can't actually represent the people who sent her there, which has
prompted a lawsuit. Lawsuit is saying, hey, people are being denied adequate
representation. So the real question, is her suit going to succeed? Probably.
Probably. I mean, it seems incredibly likely. However, probably won't be quick.
The judges in Montana they may just know where this is headed and go ahead and say you can't
do this bad legislature.
It seems as though the people in the legislature who were really behind this are saying well
no that's going to violate separation of powers and you know the judicial branch can't do
that to us and we get to that's not how it works at least that's not what the Supreme
Court has said. They have ruled on similar situations in the past, the Supreme
Court has. In 1966 there was Julian Bond. The case is Floyd versus Bond or Bond
versus Floyd, one of the two. It's a Georgia legislature that, or Georgia
legislator that they tried to exclude because, well, let's just say Bond was
not incredibly supportive of the war in Vietnam and made that opinion known.
Georgia legislature tried to stop, tried to exclude the representative. The
Supreme Court found that they couldn't do that. This will probably be a
duplicate of that case. Legislators don't lose their First Amendment rights even
if it hurts other legislators feelings. So over the long run she's probably going
to win but it's it's probably going to take some time. I have a feeling that
judges in Montana may be very sympathetic towards the those with hurt
feelings. The line when it comes to speech of this sort on the house
floor in various states, this is a hard line to find. What would be
offensive or out of decorum or whatever, that's very much open to interpretation.
denying a district their representation based on that seems a little much. It
almost seems like it's just a power play because they don't want this stuff on
the record when when it comes to pass. If they pass this ban, she has said a lot of
things that if it happens, and it likely will, after they pass that ban it's going
look bad on them and I think that that may have something to do with their
reasoning behind this because it's not like her statements are going to
influence the vote. The Republicans have a super majority. They're not at risk of
losing this. They just don't want the commentary, certainly how it seems to me.
So that's what's happening there. I would guess that there will probably be other
suits filed. I don't think that this is just going to be a singular suit brought
by her and I want to say a few constituents, not many though. I feel
like there's going to be more coming. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}