---
title: Let's talk about possible President Manchin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mheVyeMLNso) |
| Published | 2023/05/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides insights into the possibility of Senator Manchin running for president in 2024 and his ties to the centrist group No Labels.
- Senator Manchin's potential strategic move to leverage the idea of running as a third-party candidate to gain more support from the Democratic Party for his reelection campaign is discussed.
- Despite speculations, Beau believes that it is unlikely for Senator Manchin to launch a third-party campaign during a polarized election cycle dominated by the two major parties.
- Beau points out that Senator Manchin's position as a Democrat, despite his Republican-leaning voting record, is valuable to the Democratic Party as it contributes to their Senate majority and legislative agenda setting.
- The importance of campaign funding and support for Senator Manchin's Senate race, especially considering the popularity of his potential opponent, is emphasized.
- Beau suggests that Senator Manchin may be overly focused on West Virginia's attitudes, potentially leading to a miscalculation regarding his political strategies.
- The likelihood of Senator Manchin actually running for president and the impact of such a decision are questioned by Beau, who views it as more of a strategic positioning for campaign funding.
- Overall, Beau sees Senator Manchin's actions as a means to secure support and funding for his Senate race rather than a serious presidential bid.

### Quotes

- "If he plays up the idea that he might run as a third-party candidate and somebody who might siphon off votes from Biden, he might have a little bit more leverage."
- "I don't actually think that Manchin is going to launch a third-party candidacy. Even if he does, I don't think he's gonna pull any votes."
- "He seems to be a pretty business-oriented person and he does seem to do what is in his own best interest."
- "I think there's a big miscalculation going on with Manchin."
- "At the end of the day, I think that this is all about trying to establish a position to get more campaign funding for the Senate race."

### Oneliner

Beau deciphers Senator Manchin's potential presidential run as a strategic move for more Democratic Party support in his Senate reelection bid, downplaying the likelihood of an actual third-party candidacy.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact local Democratic Party chapters to inquire about their support for candidates (implied)
- Stay informed about political developments and funding strategies in your state (implied)

### Whats missing in summary

Insights into the potential impact of Senator Manchin's decisions on the political landscape and the Democratic Party's strategies.

### Tags

#SenatorManchin #PresidentialRun #NoLabels #DemocraticParty #SenateMajority


## Transcript
Well, howdy there, internet people.
It's Bo again, so today we are going to talk about the Senator from
West Virginia, Manchin and the possibility of him running for president in 2024.
And all of the, all of the conversation that has circulated over the
last couple of days and what it likely really means when you
get past all of the talk. So there is an organization called No Labels. They
self-describe as a centrist group. They are trying to get ballot access in all
50 states. Manchin has had a long relationship with this group. There have
been questions about whether or not he would run for a nationwide office with
them and he won't answer. He's basically saying he won't make a decision about
his political future until later this year or something like that. He's always
kind of evasive about it. But right now, it coming up at this moment seems
tactical in nature. As we talked about a few days ago Manchin has real
competition in in West Virginia. Justice, the the governor there, has indicated an
interest in running for his seat. Justice is incredibly popular. So the
conventional wisdom right away, the knee-jerk reaction that people have had,
is Manchin knows he's gonna lose so he's going to try this wild play via a third
party to become president. No. No. If there's anything that we have learned
about Senator Manchin over the years, it's that there's no mystery. You can
always count on him to do what is best for him. If he plays up the idea that he
might run as a third-party candidate and somebody who might siphon off votes from
Biden, he might have a little bit more leverage when it comes to talking to
the Democratic Party and getting support for his reelection campaign, getting some
extra money, getting some reassurances that the Democratic Party's gonna come
through for him and they're gonna make sure that he has the cash to defeat
justice. That to me seems like the most likely answer here. Manchin doesn't
exactly strike me as a dreamer. He seems to be a pretty business-oriented person
and he does seem to do what is in his own best interest no matter what. I think
it's unlikely that he is going to launch a third-party campaign during an
election cycle that is likely to be incredibly polarized and dominated by
the two major parties. That just seems like a surefire way to fail to be honest.
But playing into that rumor, allowing that mystery to flourish, it gives them
leverage. Well you know if you don't want me to run with this organization that
I've had a lot of ties to over the years, I mean all you got to do is just make
sure that I win in West Virginia, you know, and I may need some money, may need some ad
buys, I think that's what it's about.
The last time we talked about this, a whole lot of people in the comments section were
asking like, why does it even matter?
He votes like a Republican.
Yeah, I'm not going to argue that.
voting record is it leaves a lot to be desired, no doubt, but as long as he is a Democrat
in name, it counts towards them having the majority in the Senate, which means they get
to set the legislative agenda.
That's where it helps.
Even if he was to vote against them every single time, if his seat is the deciding factor
between them having the majority and them not, it matters.
And that's honestly probably the only reason
the Democratic Party really tolerates them.
Now, that being said, I don't know how much money
the Democratic Party is willing to commit to West Virginia
for Manchin, who hasn't really been a team
player over the years, especially when
you look at the numbers.
I don't know that there is a dollar amount that can override Justice's popularity.
I would point out that as soon as he announced we put out that video, you know, we were talking
about it on this channel, days later you're starting to see moves from Manchin that if
I'm reading them correctly, could best be described as Ruttrow. He's panicking.
Should be. Justice is popular. So that's what I think this is. I don't actually
think that Manchin is going to launch a third-party candidate, candidacy. Even if
he does, I don't think he's gonna pull any votes. Not enough to matter in any
race, to be honest, in any state. He is not well liked by Republicans because he's
a Democrat and he is not well liked by the Democratic Party. Those people who
are kind of part of the coalition that makes up the Democratic Party that would
be energized by a third-party run, it would have to be to the left of the
Democratic Party, not to the right. I think there's a big miscalculation going
on with Manchin, and I think it has to do with him being hyper focused
on West Virginia and thinking that the attitudes there are representative of
the country. They are not. So I'm sure that this is probably going to get a lot of coverage over
the next couple of days. At the end of the day, I think that this is all about trying to establish
a position to get more campaign funding for the Senate race. Might he run for president? Sure.
Is it something to truly worry about? I doubt it. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}