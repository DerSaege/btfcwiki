---
title: Let's talk about Texas, the Constitution, and Pennsylvania....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=j55HN1X3xfQ) |
| Published | 2023/05/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the message he received regarding the Texas Constitution and Satanist literature in classrooms.
- Clarifies that he did not insinuate but outright stated that if Texas allowed the Ten Commandments in class, Satanist literature will follow.
- Disputes the claim that the United States was founded as a Christian nation, referencing the 1797 Treaty of Tripoli.
- Addresses the misunderstanding about the Constitution protecting only Christian religions, outlining that it safeguards all religions.
- Distinguishes between the law of unintended consequences and cause and effect regarding religion in schools.
- Mentions a federal court decision in Pennsylvania allowing the after-school Satan Club alongside the Christian evangelical club.
- Notes similar cases in Virginia where the Satanic Temple prevailed in gaining access to schools for their club.

### Quotes

- "If that legislation goes forward, they put the Ten Commandments into the classroom, I assure you Satanist literature will be in there alongside it."
- "The United States was not founded as a Christian nation."
- "The Constitution protects the free exercise of all religions, any religion, not just yours."
- "Your law of unintended consequences is just a slippery slope fallacy. No, it's cause and effect."
- "Y'all have a good day. Thank you."

### Oneliner

Beau dismantles misconceptions about religion in schools, citing historical treaties and legal precedents while addressing fear-mongering around Satanist literature.

### Audience

Students, educators, activists.

### On-the-ground actions from transcript

- Support and advocate for inclusive religious education in schools (suggested).
- Stay informed about legal battles surrounding religious clubs in schools (suggested).

### Whats missing in summary

Detailed analysis of the historical context and legal implications surrounding religion in American schools.

### Tags

#Religion #Education #Constitution #Texas #Satanism


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Texas
and the Constitution and a really old treaty
and Virginia and Pennsylvania
and a whole bunch of things coming together
to answer a question for somebody.
I got a message, the person is very passionate
about their response and I think it's worth going through.
Okay, so here is the message.
You lying female dog, normally when I say that,
I'm editing what somebody wrote, okay?
This person literally wrote, you lying female dog, okay?
In your video about Texas, you insinuated that if Texas
allowed the 10 Commandments to be in class,
they'd have Satanist literature eventually.
The United States was founded as a Christian nation, and the Constitution protects the
free exercising of that religion.
It's just blatant fear-mongering to say that just because Christianity is allowed in a
Christian country that Satan would be too.
Your law of unintended consequences is just a slippery slope fallacy, trying to scare
people out of putting God back into the classroom."
Okay, so we'll go through piece by piece.
You insinuated that if Texas allowed the Ten Commandments to be in class, they'd have
Satanist literature eventually.
I did not insinuate that.
I flat out said it.
That is what will happen.
If that legislation goes forward, they put the Ten Commandments into the classroom, I
assure you Satanist literature will be in there alongside it.
Promise.
Write it down, it's going to happen.
Okay.
The United States was founded as a Christian nation.
No it wasn't.
it was not. It didn't have a religion, still doesn't. I would suggest you pay
particular attention to the 1797 Treaty of Tripoli in which there's a specific
portion you might find interesting which says the United States was not in any
sense founded on blank and just go from there. Keep in mind 1797 really early on
in the country's history. The Constitution protects the free exercising
of that religion. No, it protects the free exercise of all religions, any religion,
not just yours. And it also has this thing, the establishment part of it. If
you're gonna use government funds, like let's say a school, to promote your
religion, other religions have to have equal access basically. Otherwise it's
like the government co-signing one particular religion and that's not how
it works. Your law of unintended consequences is just a slippery slope
fallacy. No, it's cause and effect. Not the same thing. Trying to scare people out
of putting God back in the classroom. Now I have videos on that. I am all about
teaching the philosophical aspects of the various religions in school. Like I'm
on record on that. I think it's a great idea. Okay, so I feel like we've answered
that. Now we're gonna move on to completely unrelated news that has
nothing to do with this message whatsoever. On Monday, in the Eastern
District of Pennsylvania in federal court, a judge has decided that the
after-school Satan Club has to be allowed in the school because they
do allow the Good News Club, which is a Christian evangelical club. The suit was
brought by the ACLU and the Satanic Temple.
And just so you don't think it's a one-off,
earlier this year in Virginia, the exact same club
had a very similar case, and they won there as well.
It's how it works, just so you know.
Anyway, it's just a thought.
Y'all have a good day.
Thank you.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}