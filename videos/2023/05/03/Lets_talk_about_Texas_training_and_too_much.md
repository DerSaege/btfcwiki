---
title: Let's talk about Texas, training, and too much....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=d_Px833tzfs) |
| Published | 2023/05/03 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the legislation HB 1147 in Texas that requires schools to provide bleeding control station training annually.
- Criticizes the legislation for aiming to turn students into combat medics rather than teaching basic first aid.
- Argues that the training proposed for third graders is unrealistic and too intense.
- Shares his personal experience of teaching his son first aid skills due to living far from a hospital.
- Challenges the effectiveness of the proposed training and suggests it cannot be the best solution.
- Emphasizes the glorification of violence in society and how it contributes to the issues being faced.
- Condemns the idea of teaching third graders combat medic skills while not addressing underlying societal problems.
- Calls out politicians for glorifying violence and contributing to the issue.

### Quotes
- "You're asking third graders to become combat medics. That's what this is for."
- "If you vote for this, you never get to talk about a book in a library again."
- "If you're a politician and you stand up there showing off your AR, you're a part of the issue even if you vote to teach them how to plug the holes."

### Oneliner
Proposed legislation in Texas aims to train third graders as combat medics, reflecting deeper societal issues and glorification of violence.

### Audience
Educators, policymakers, parents

### On-the-ground actions from transcript
- Advocate for realistic and age-appropriate emergency training in schools (implied)
- Challenge legislation that may not address the root causes of societal issues (implied)
- Prioritize nurturing creativity and understanding over glorification of violence (implied)

### What's missing in summary
The emotional weight and personal anecdotes from Beau's experience teaching his son first aid are best understood by watching the full transcript.

### Tags
#Texas #Legislation #CombatMedics #ViolenceGlorification #SocietalIssues


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Texas and training
and having realistic expectations of that training
and whether or not it can actually be taught
in the way it would need to be
to achieve the desired results.
We're going to do this because there is some legislation.
It is HB 1147 if you would like to read this
yourself. Before we get into it, I want to point out this is the second version
of this video. Not the second take, second version. The first version was me just
providing the training in the way it would need to be done for this particular
situation. After reviewing it, I realized that might be a little much. So
So I'm still going to talk about it, so just a heads up.
Okay, so the legislation would require the district or charter school to annually offer
instruction on the use of a bleeding control station from a school resource officer or
other appropriate district or school personnel who has received the training under Subdivision
3.
students enrolled at the campus in grade three or higher. Third graders. A bleeding control
station required under this section must contain all of the following required supplies in
quantities determined appropriate by the superintendent of the district or the director of the school.
One, tourniquets approved for use in battlefield trauma care by the armed forces of the United
States. Allow me to stop right now. That's not good enough. I understand that the phrasing of
that is going to really bother some people because it's real, but that's not actually
great. I would suggest cat generation seven tourniquets because there are studies demonstrating
that they work on small limbs. Chest seals, compression bandages, bleeding
control bandages, space emergency blankets, latex-free gloves, markers,
scissors, instructional documents developed by the American College of
Surgeons or the United States Department of Homeland Security detailing methods
to prevent blood loss following a traumatic event." This in and of itself
is commentary on the United States. But here's the thing. My son, who is roughly
this age, he knows how to use all this. He knows how to use all of this. I taught
him. We live out in the middle of nowhere. It's a ways to a hospital. So first aid is
important. If we were to roll up on a car accident and I said put a tourniquet on that
person while I give CPR. I have 100% confidence in his ability to do so.
Here's the thing. This isn't for first aid though, right? Let's be real for a
second. These students can't be taught the way I taught him. This isn't for
first aid. This is to turn them into combat medics. That's really what it's
for. It can't be done the same way.
Think about what you are realistically asking them to do. It's not just perform
the task which is bad enough. It's take fire, react appropriately, take cover,
process what they witness, determine it's safe, move through the fear they're
definitely going to have, rush to the side of a classmate or teacher, shear off
their clothing and apply a chest seal. That's what's being asked of them. To
fight on and complete the third grade objective though they be the lone
survivor. That's what this is for. If you want to accomplish that, it can't be a
clinical class. It can't be the way I taught my kid. It can't be an SRO
standing up at the front of the room because you have to train like you fight,
right? They have to be ready for the stress and the pressure that is going to
be there. Before anybody votes on this, I think they need to go through something
that is similar. I think they need to go through the kind of training it would
actually take to get somebody ready to do this in a combat situation. They need
to hear the instructor yell at them and say, no, you took too long, he's dead, move
gone. Did you just stand up? Drop and scream for a medic. You're hit. Let's be real about
what this is for. Aside from that, if you were talking about realistic expectations,
I think it would be wise to hand a third grader a pack of hot dogs, you know, the Coney Pill
open and ask them to open it and then get a chest seal and open up that.
Peel the plastic off the sticky side. I know third graders that can't open a Capri Sun.
I don't want to come down on the person who proposed this because on some level
I get it. You're trying to come up with the best you can with a bad
situation. This cannot be the best we can do.
You're asking third graders to become combat medics. That's what this is for.
and I think that people should have realistic expectations of it. A classroom
setting, you know, using a plastic leg, this is how it works, this is how you apply the
tourniquet, that is not going to cut it. Not if you want this to be effective, but
describing what the kids would have to hear, well that's it's too much, it's too
much. Having recorded it, it's too much. But apparently it's not too much to ask
them to deal with it for some feel-good measure. And this is coming from somebody
who actually taught their kid how to use this stuff but he wasn't taught for that
situation. The other part to this and it's a different topic but it's not
because it's it's interlinked. Part of our problem is a willingness and a
glorification of the violence. That's part of the problem. That's why we're
dealing with this. Because we aren't offended by the violence. If you vote for
this, you never get to talk about a book in a library again. If you think it is
age-appropriate for a third grader to learn how to apply a chest seal in the
chance that they have to use it as they watch one of their classmates life drain
from their eyes, you do not get to complain about a book. If we had a little
bit more understanding, if we had a little bit more nurturing of creativity,
if we spent more time on the things that matter instead of glorifying the
violence. Maybe this wouldn't be a topic. We have an issue in this country. The
fact that this is proposed shows we have two issues. One is the symptom of the
other. If you're a politician and you stand up there showing off your AR,
you're a part of the issue even if you vote to teach them how to plug the holes.
You're glorifying it at every turn.
You're not owning the libs.
You're owning your kids.
Anyway it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}