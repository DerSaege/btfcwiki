---
title: Let's talk about Biden, the FBI, and Republican pursuits....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=IiJHncOJl5w) |
| Published | 2023/05/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two Republicans in Congress have requested all FD 1023 forms related to the term "Biden" from the FBI, aiming to dig up dirt on President Biden.
- The FD-1023 forms are used by the FBI to record notes from meetings, even if the information is unverified.
- Beau suspects that the Republicans may be trying to create a scandal, even though they might already know the allegations are false.
- Beau suggests that all information, including the final disposition of the case, should be released to prevent political weaponization of the FBI.
- He advocates for transparency to prevent future attempts at manipulating the FBI for political gain.
- The speaker believes that this situation may turn out to be another Republican "Scooby-Doo mystery" with no substantial findings.
- Regardless of the outcome, Beau calls for releasing all information to the public.

### Quotes

- "Release it all."
- "Don't just release the FD-1023."
- "Prohibits politicians from weaponizing the FBI."
- "Another Republican Scooby-Doo mystery."
- "Y'all have a good day."

### Oneliner

Two Republicans seek dirt on Biden through requesting FD-1023 forms from the FBI, prompting Beau to advocate for full transparency to prevent political manipulation of the agency.

### Audience

Congressional watchdogs

### On-the-ground actions from transcript

- Contact representatives to advocate for full transparency and release of information (suggested)
- Monitor the situation for updates and demand accountability from politicians (implied)

### Whats missing in summary

The detailed breakdown and analysis of the potential political motives behind the request for FD-1023 forms and the importance of transparency in such situations.

### Tags

#FBI #Transparency #PoliticalManipulation #RepublicanParty #PresidentBiden


## Transcript
Well, howdy there, internet people.
Led Zebo again.
So today we are going to talk about Biden and Republicans
and what they're looking for in FD 1023 forms
and what they are and why they're probably specifically
asking for that specific form.
And we're gonna talk about what it all means
and whether or not it is likely to go anywhere.
If you have no idea what I'm talking about, two Republicans, one in the House and one
in the Senate have reached out to the FBI, actually, I think one of them is a subpoena,
and they are asking for an FD 1023, specifically all FD 1023 forms, including within any open,
closed or restricted access case files created or modified in June 2020, containing the term
Biden, including all accompanying attachments and documents to those FD-1023 forms.
Okay, so what are they trying to do? Trying to dig up some dirt on Biden.
An unnamed person apparently alleged an unknown crime involving then vice president Biden.
And the Republican party has a tip about this.
They want to know more about it.
My guess is that they already know more about it.
guess is they know way more than they're saying. And they probably know
that the FBI decided not to look into it further, that there was something amiss,
but they don't care. So the FD-1023 is a form that the FBI uses to kind of make
notes about meetings that they had. Hypothetically speaking, if you were to
somehow have a meeting with the FBI and you said there's this guy on YouTube
named Deep Goat and I really think he's somebody else and he's sending me secret
messages and maybe he's a spy, that information would be recorded on an FD
1023. It will have information on it that is not vetted or verified by the FBI, but it will be on
a form with the FBI logo. And man, that looks good on social media. My guess is that they know
whatever the allegations are, are false. They already know this, but they're trying to generate
a scandal. That's my best read on the situation, that at the end of the day this is going to be
nothing. That being said, release it. Release it and then release the memos about what was decided
to do and why with the information. Okay? Odds are, and you do that at the same
time, the same day that you send the information to the members of Congress
the FBI should publicly release the the final result of that information and
what they determined and how and why and all of that stuff. That's it. This way it
prohibits politicians from weaponizing the FBI politically. And it discourages
this kind of attempt in the future. Now all of this is assuming that it is, you
know, something that isn't true. If it is true, yeah, release that too. The American
people need to know. My guess is that this is going to be another Republican
Scooby-Doo mystery that ends up with nothing. There's no ghost at the end of
it. So at the end of this, the FBI should just provide them the information, but
provide the public with the final disposition of that case, of those
allegations. Don't just release the the FD-1023 because I promise that will end
online and no matter how wild the allegations are there will be people who
believe it without evidence. So release it all. And again I mean if there is
credible evidence that the then vice president now current president engaged
in some form of criminal scheme which appears to have something to do with
exchanging money for policy decisions, yeah I mean the American people should
You probably know that, just saying, but I wouldn't hold your breath for some giant report
that indicates Biden was selling foreign policy decisions or something.
That seems incredibly unlikely, but we'll have to wait and see.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}