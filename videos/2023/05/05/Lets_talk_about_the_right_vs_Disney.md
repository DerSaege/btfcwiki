---
title: Let's talk about the right vs Disney....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Cfx-6KLcBPE) |
| Published | 2023/05/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The right wing in the United States is becoming increasingly critical of Disney, a company that many working-class families cherish as a symbol of Americana.
- The Republican Party, historically able to portray themselves as a party for the working class, is now attacking American staples like Disney, alienating their base.
- Disney, through its ownership of various icons and symbols, has become intertwined with the very fabric of American culture.
- Beau observes a truck with a bumper sticker saying "F Disney" while also featuring a mural with the Punisher skull, a character owned by Disney through Marvel.
- The irony of attacking Disney while driving a vehicle adorned with symbols owned by the company is not lost on Beau.
- The disconnect between the Republican Party and the traditional American values they claim to uphold is becoming increasingly apparent.
- Beau predicts that the Republican Party's attacks on iconic symbols like Disney may lead to pushback from their working-class base.
- The potential backlash from attacking cherished American symbols could prove detrimental for the Republican Party's image.
- Beau humorously suggests that elephants, symbolizing the Republican Party, should perhaps fear the mouse (Disney) they are targeting.
- The transcript ends with Beau signing off, leaving the audience to ponder the implications of attacking symbols deeply ingrained in American culture.

### Quotes

- "Elephants are afraid of mice. They probably should be."
- "This is going to be like the InBev thing, only worse."
- "They might just better leave that mouse alone."
- "They're going to hit traditions that are just, they're not worth giving up for the social media clicks."
- "It's a unique thing but it shows how the Republican Party, you know they lost touch with the youth of the country."

### Oneliner

The Republican Party's attacks on American icons like Disney risk alienating their working-class base and may lead to significant pushback.

### Audience

Working-class Americans

### On-the-ground actions from transcript

- Support initiatives that uphold American values and traditions in your community (implied).
- Encourage political representatives to focus on issues that truly benefit working-class families (implied).

### Whats missing in summary

The full transcript provides a humorous yet thought-provoking commentary on the disconnect between the Republican Party's actions and the traditional American values they claim to represent.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Disney and the right
and a message I got and a truck that I saw.
If you don't know the right wing of the United States,
there are segments of it.
It's not throughout the right wing yet, but it's growing.
There are segments of it that are really mad at Disney and they're taking like
little shots at Disney and stuff like that. I got this message and some
political commentator said something to the effect of, you know, I don't like
Disney because I'm a grown man and the person was like, you know, what do you
think of that? I'm like, what a softball question. Y'all know I like Disney, but
But it got me thinking about the whole image that the right wing is casting now.
The Republican Party used to be able to cast themselves as the party of the working class.
They weren't, but they could pretend to be.
Now they're attacking so many things that are just American staples.
For a lot of people going to Disney, if you're talking about average family size, average
income, this is a twice in a lifetime experience.
You go once when you're a kid and your parents can afford to take you, and then once when
you're an adult and you can afford to take your kids, and that's it.
This is something that a lot of working class families, they work extra hours, they save
be able to take their kids there because their kids love it, right? And I just
don't understand what they hope to gain from this outside of the the social
media echo chamber. And as I'm thinking about this total softball of a question,
I have to go into town and I pull out and I get behind this truck on the
highway and it's the exact kind of truck you would expect in my area. You know,
a giant diesel and I can tell that it's one of those trucks that has the wrap
behind it, you know, the mural. I can't tell what it is on the sides but I can
see it coming around the edges of the tailgate and that's when I see the
bumper sticker that says F Disney on it and I'm just sitting here thinking that
this person is probably somebody that is in that social media echo chamber but
that a few years before they probably would have taken that truck somewhere
and pulled extra shifts for two weeks to take their little girl to Disney and I'm
following it along and I'm looking at the truck and then I'm looking at the
back window which if you don't know it's very common to have murals in the back
windows of a lot of big trucks and this is exactly what you would expect it's an
American flag but the stars are replaced with little AR-15s and the middle
stripe turns into the thin blue line and then it turns into the firefighter thing
and the right in the middle is a big old Punisher skull. And I'm sitting here
thinking I'm like how are you gonna have a bumper sticker that says F Disney
and then advertise their castle? Not the castle the Disney World, Frank
castle and for most those people that have a Punisher skull on their truck
that's the Punisher's name. Disney owns Marvel. If y'all are gonna go on one of
your little tirades where y'all break stuff and you know throw out all the
stuff you already paid for, man y'all are gonna have a lot of stuff to smash and
scrape off. Disney owns everything. A lot of the things that the right wing just
holds up as their symbols, their icons. Disney owns them. This is going to be
like the InBev thing, only worse. If it reaches that critical mass and it
turns into a whole, you know, we're not gonna buy anything from Disney and they
start smashing stuff on social media, I mean, it's gonna be wild. They
just own so much stuff. They're going to have a real hard time sorting through
the products trying to figure out what they're allowed to buy because you know
their betters are angry at Disney. It's a it's a unique thing but it shows how
the Republican Party you know they lost touch with the youth of the country.
that's obvious and becoming more obvious as they you know try to raise the
voting age and stuff but it shows that they're going to lose touch with their
base as well with that working-class base that base that they always try to
appeal to because as they attack all of these icons that are icons of the
traditional American family, which is what they're supposed to stand for,
eventually they're going to get pushback. They're going to hit traditions that are
just, they're not worth giving up for the social media clicks. And I have a
feeling that going after Mickey and his favorite hitter Frank Castle, that might
be one of those items. They might just better leave that mouse alone. Anyway. Elephants are
afraid of mice. They probably should be. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}