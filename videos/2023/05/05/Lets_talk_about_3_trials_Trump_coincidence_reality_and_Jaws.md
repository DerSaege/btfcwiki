---
date: 2023-08-06 06:15:59.234000+00:00
dateCreated: 2023-05-19 17:35:34.313000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about 3 trials, Trump, coincidence, reality, and Jaws....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=P5eMU_QDW7Q) |
| Published | 2023/05/05 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A jury has returned a guilty verdict in a seditious conspiracy case involving the Proud Boys, separate from previous cases involving the oath keepers.
- Drawing parallels to the movie Jaws, where people were reluctant to accept a reality that could impact them economically.
- Raises questions about the likelihood of multiple organizations coincidentally acting together for the same goal.
- Speculates on whether there was a central organizer above these groups and potential connections to Trump.
- Suggests that the special counsel's case against those with direct access to the White House may strengthen over time.
- Points out that while details are still unclear, the pieces of the puzzle are starting to come together.

### Quotes

- "They were looking for a coincidence."
- "Takes a lot of work to make a coincidence like that happen."
- "It is worth noting these sentences, they're probably pretty lengthy."

### Oneliner

A jury's guilty verdict in a seditious conspiracy case involving the Proud Boys raises questions about organization, potential connections to Trump, and the strengthening case against those with White House access.

### Audience

Activists, Investigators, Concerned Citizens

### On-the-ground actions from transcript

- Investigate potential connections between different groups and central organizers (implied)
- Stay informed on updates related to ongoing investigations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the recent guilty verdict in a seditious conspiracy case, speculating on organizational links, potential central figures, and implications for the White House.

### Tags

#Conspiracy #Seditious #ProudBoys #Trump #Investigation


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about coincidence,
the reality, accepting reality.
What an old movie can teach us about it
and why all of this is kind of really bad news
for Trump and his inner circle,
particularly those who were kind of on the periphery
of Trump's inner circle when he was still in office. For the third time, a jury has
returned a verdict of guilty in relation to a seditious conspiracy case. But it's
two different organizations. It has now moved into a second organization. The
first two, they dealt with the oath keepers. This most recent one,
well, it's with the Proud Boys and it is worth noting that one of the defendants
in this case was found not guilty of the seditious conspiracy
but guilty of other underlying offenses. I'm really interested to hear
the the jury's reasoning on that but it doesn't matter to the overall point
because you have two
separate organizations.
The beginning of the movie Jaws.
The uh...
a lot of people didn't want to believe what they were saying.
They came up with other explanations for it.
Yeah, they...
they were motivated
to come up with other explanations for it. Absolutely.
You know, they wanted
it to be anything other than what it was
because it was going to impact them economically.
But they were looking for something else.
They were looking for something else to blame it on, some other
reason to explain what they were saying.
They were looking for a coincidence.
When it comes to the verdicts, I want you to think about the
coincidence that you would have to believe if you wanted to
say that these weren't connected. You would have to believe that two separate
organizations organized to act on the same day at the same time in the same
location for the same goal. Takes a lot of work to make a coincidence like that
happen. So if you're accepting reality you have to acknowledge that this wasn't
to coincidence, the likelihood of this being spontaneous. That's pretty small.
So then you have to ask who organized it. Right? Did the groups themselves organize amongst
themselves or was there a focal point? Was there somebody above them that said this time,
This time, this place, we have to act.
I would imagine right now that's the question that the feds are asking and they're trying
to figure out who it was and whether or not that person had contact with Trump.
Because it's unlikely that it was Trump himself, it was somebody below Trump that had contact
with both these groups.
would be the the reasonable conclusion. The unreasonable one is to believe that
well it's just a a tourist event that got out of hand. These cases, following
the way that they have, it strengthens the special counsel's hand in pursuing a
a seditious conspiracy case against
those who might have had more direct access to the White House,
which as time goes on might strengthen
the case against the occupant of the White House.
But, we're gonna have to wait and see. It is worth noting these sentences, they're
probably pretty lengthy.
but while we don't know
yet what exactly happened as far as organization
we have bits and pieces but we don't know the whole story yet
I don't think there's many people left
who really believe this was a boating accident. Anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
White JAWS movie shirt
## Easter Eggs on Shelf
Louisiana License Plate 007 0 981 was the plate taken out of the tiger shark in the movie Jaws.