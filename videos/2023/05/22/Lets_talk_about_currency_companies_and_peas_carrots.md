---
title: Let's talk about currency & companies and peas & carrots....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QRRuydIzSkY) |
| Published | 2023/05/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the backing of the US dollar and terms like soft colonialism.
- Addresses the supremacy of US corporations and how they outcompete others.
- Talks about legal mechanisms and incentives that benefit US corporations.
- Mentions the extraction of wealth through corporate colonization.
- Provides insights into the US defense industry's role in maintaining the dollar's backing.
- Acknowledges the not-so-ideal practices in the system and the need for understanding to bring change.
- Emphasizes the prevalence of such practices in the West.
- Calls for a shift towards cooperation over competition in economic systems.
- Offers a critical perspective on global economic structures and their implications.
- Encourages reflection on the current state of affairs and the potential for improvement.

### Quotes

- "We don't colonize with flags anymore. We colonize with corporate logos."
- "The US dollar is backed up by that 800 billion dollar a year defense industry."
- "It's dirty right? It's not really how we would want things."
- "The wealth is extracted from the states that we colonize with corporate logos."
- "It will stay this way until we move to a more cooperative rather than competitive."

### Oneliner

Beau explains the backing of the US dollar, supremacy of corporations, and soft colonialism, calling for a shift towards cooperation in economic systems.

### Audience

Economic Justice Advocates

### On-the-ground actions from transcript

- Challenge exploitative economic systems (implied)
- Advocate for fair trade practices (implied)
- Support policies that prioritize cooperation over competition (implied)

### Whats missing in summary

In-depth analysis of specific examples and historical contexts shaping current economic structures.

### Tags

#USdollar #CorporateSupremacy #SoftColonialism #EconomicJustice #Cooperation


## Transcript
Well, howdy there internet people, it's Bo again.
So today, we are going to talk a little bit about terms
that get used that are sometimes not really well explained.
We are going to talk a little bit about foreign policy,
a little bit about international economics,
and we'll talk about what really backs up the US dollar.
Okay, so this last couple days I've been going through my messages trying to catch
up if you couldn't tell from the videos coming out, but I have two here from two
different people and man they go together like peas and carrots. Fantastic.
Okay, so here's the first one. In your last video, it's a few videos back now,
Now, you said our economy and currency was built on faith, saying it was just paper.
That's not true.
It's backed by U.S. corporations consistently outcompeting others on the market.
The supremacy of the U.S. economic system is backed by the supremacy of the U.S. corporations.
Easy.
Don't drink all the capitalism.
Save some for the rest of us.
Okay, the second message.
You and others on YouTube use the term soft colonialism or colonialism light.
Maybe I'm just dumb, but I don't see colonialism.
I know the system isn't fair, but calling it colonialism seems like a stretch.
Okay, so let's go back to that first message first.
these questions when you answer them, both of them get answered at the same
time. The supremacy of US corporations. Why are US corporations supreme? Like how
does that happen? I mean it's weird because like all the assemblies, the
sub-assemblies, the components, the materials, like all that stuff's made in
other countries. How does US corporations stay on top? I mean they
have everything they need except what? A brand? They have marketing departments
and developing countries. So how does that happen? It's not that US companies
out compete. There's more to it than that. Why do they out compete? Because there
legal mechanisms that make it beneficial to give US corporations a cut. You make
those components, ship them here, we'll just put everything together real
quick here even though you've made all of the individual parts, slap a Made in
America logo on it, don't worry we'll take care of you. Those legal
mechanisms, those incentives to behave in that way, they help extract the wealth.
from these countries. We don't colonize with flags anymore. We colonize with corporate logos.
So that's where the term soft colonialism comes from. But it doesn't answer the question of what
actually backs up the US dollar because faith is just a nice way to say it. It is more than faith.
there is something that actually backs it up as long as you understand that
that soft colonialism is going on. In a recent video I talked about Chad the
country doing a very Chad the meme thing. Chad was basically it was kind of like
hey we're gonna nationalize the oil industry go read the comments of that
video and you will see what backs up the US dollar because the comments are stuff
like great everybody's gonna have to pretend they care about human rights and
Chad now sounds like Chad needs some freedom how long do you think it's gonna
to take till we invade. The US dollar is backed up by that 800 billion dollar a
year defense industry. That's what keeps the legal mechanisms in place because
people don't want to play ball well maybe they need to go. That's how it
happens. It's dirty right? It's not really how we would want things. It's not how I
I would want things. I want to be clear this is one of those times when I'm
explaining how something works not saying it's a good thing but if you want
to fix it you have to know what's broke. The West in general, make no mistake
about it, we're not the only country that does this. The West in general does this
a lot. We're probably one of the worst offenders. Other countries outside of the
West do it occasionally, but that's how it works. The wealth is
extracted from the states that we colonize with corporate logos, those
countries via incentives, via trade deals, stuff like that. And if a country has
resources that the market really needs, so US corporations can maintain
supremacy, well, maybe we care about human rights in their country all of a
sudden. It's not good, but that's that's the nuts and bolts of it. It will stay
this way until we move to a more cooperative rather than competitive
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}