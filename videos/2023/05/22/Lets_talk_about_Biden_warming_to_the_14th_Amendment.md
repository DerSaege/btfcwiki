---
title: Let's talk about Biden warming to the 14th Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=kqv8h8JsdgE) |
| Published | 2023/05/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Biden's shift towards considering using the 14th Amendment to address the debt ceiling issue.
- Outlines the potential consequences of invoking the 14th Amendment, including legal challenges and economic uncertainty.
- Describes Biden's initial reluctance to take this action to maintain a sense of normalcy and stability.
- Points out that with the looming threat of default, Biden may now be more willing to take bold actions.
- Suggests a hypothetical scenario where Biden delegates authority under the 14th Amendment to ensure U.S. debts are paid.
- Speculates on the potential reactions and consequences of such a move, including political and economic impacts.
- Emphasizes the importance of honoring the Constitution and avoiding a default scenario.
- Raises concerns about the current Republican budget proposal and its potential negative impact on the economy.
- Criticizes the Republican Party for sticking to outdated talking points and policies that may harm their own base.
- Concludes with a cautionary note about the consequences of pursuing certain policies without considering their real-world effects.

### Quotes

- "He might be willing to take that risk."
- "The longer the Republican Party holds this up, the less Biden has to worry about."
- "The economy goes down because a bunch of judges that Republicans appointed couldn't read the Constitution."
- "The current Republican budget would damage the economy, probably more than a default."
- "They may end up catching the car on this one, too."

### Oneliner

Biden's potential shift towards using the 14th Amendment to address the debt ceiling crisis signals a willingness to take risks for the economy's sake, amid Republican budget proposals that could harm their own base.

### Audience

Policy Analysts, Political Commentators

### On-the-ground actions from transcript

- Contact political representatives to urge them to prioritize economic stability and responsible budgeting (implied).
- Join advocacy groups focused on fiscal policy and government spending to stay informed and push for prudent financial decisions (implied).

### Whats missing in summary

In watching the full transcript, viewers can gain a deeper understanding of the intricacies surrounding the debt ceiling debate and the potential implications of invoking the 14th Amendment in addressing the economic challenges.

### Tags

#DebtCeiling #BidenAdministration #PoliticalAnalysis #EconomicPolicy #RepublicanBudget


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and McCarthy
and the debt ceiling and the 14th Amendment
and why Biden suddenly seems to be warming
to the idea of using the 14th Amendment.
If you remember, initially Biden was like,
yeah, we're not gonna do that, that's silly.
But now he's kind of indicating, hey, you know,
maybe I do have the legal authority to do this.
So first, we have to talk about why he didn't want to do it to begin with.
And the answer to that is simple.
Saying that U.S. debts are unquestionable, as the Constitution says, and saying that
Congress, intentionally and willfully defaulting, is unconstitutional, issuing an executive
order to just pay the bills, that's going to go to court.
That would shake the economy, because the economy likes certainty.
They don't know how the court's going to decide, so it would cause economic damage.
That's the reason he hasn't done it.
The other side to that, politically, is that Biden has decided that, well, I'm the normal
one.
He's kind of branding himself as a return to normalcy.
Doing something strange isn't that.
So he has shied away from it.
However, we're now at that point where we're so close to a potential default that the economic
issues, the faith, can already start to shake and damage the economy.
So if the economy starts to get damaged, well, he has nothing left to lose.
Therefore he has the freedom.
It might be time for Dark Brandon to make an appearance here.
Biden and McCarthy are supposed to be meeting today.
Biden might want to slide a laptop across the table to McCarthy with some text on it
that might read something like this.
By the authority vested in me as President by the Constitution of the United States,
I hereby delegate to the Secretary of Treasury the authority under the 14th Amendment to
the Constitution to ensure all U.S. debts remain unquestionable and are paid.
order shall be in effect from midnight tonight until such time as the U.S. Congress brings
itself back in accordance with the Constitution of the United States and presents a bill to
raise or eliminate the debt ceiling or otherwise ensure U.S. debts are unquestionable that
is then signed into law.
You are authorized and directed to publish this memorandum in the Federal Register."
And have it on the WordPress or whatever it is that the White House website runs by.
And then let McCarthy see Biden's aid, schedule it for close of business tomorrow.
And say, I'm still trying to negotiate in good faith, but please understand, I'm not
going to let the country default.
We have to honor the Constitution here.
Why don't y'all come back with some kind of deal?
kind of plan that makes sense, something that maybe even if it has a budget, I know you want
to tie it to that, but it can't be a budget that's so bad it's going to disrupt the economy like the
one you're trying to push now. Aviators on, walk out of the room. The longer the Republican Party
holds this up, the less Biden has to worry about. The more important worry becomes protecting the
economy. Now I know, as many people have pointed out, well, the Supreme Court,
they're gonna side with the Republicans. Okay, so the Biden administration
attempted to do something that is in a plain reading of the Constitution. The
Supreme Court says no. What do you think the split on the votes will be? All the
conservative judges would be the ones to say no, right? Man, that sounds like a
sure, surefire winner right there for 2024. The economy goes down because a
bunch of judges that Republicans appointed couldn't read the Constitution.
This isn't something that's like written in language that's confusing. It's very
clear. So he may be willing to take that risk. Now Biden, he seems pretty risk
averse when it comes to the economy, but if he starts to run out of options,
again, nothing left to lose, that's freedom, and he might do it. I don't know
that he would go that route with the negotiations. I would hope that he does,
but I would suggest that him talking about it publicly the way he has been
isn't out of a desire to actually do it. It's out of a desire to signal to the
Republican Party that if he has to, he will. So I wouldn't expect an executive
order similar to the one that I wrote to come out anytime soon, but if the
economy really starts to shake, we may have to come back and revisit this topic.
I think right now he's just signaling to Republicans, yeah I might do this, you
know, don't force my hand, I don't want to do this, but if I have to I will.
And he's trying to get them to come back with a budget that won't wreck the
economy, because make no mistake about it, the current Republican budget would
damage the economy, probably more than a default, at least a short one, so the Republican hardliners
have put Biden in a position where if he wants to save the economy, he may have to do something
a little weird, but they've also put themselves in a position of saying these are the cuts we're
going to have in this budget and those cuts they are going to really hurt their
own voters and I know people like to say you know Republicans don't care as long
as they can punish and other somebody else and to a degree that's true but I
don't think y'all understand exactly how bad this budget is and how much it's
going to hurt the Republican base more so than the Democratic base. It's the
Republicans trying to base their policy on talking points from 20 years ago.
You know these are people who grew up with this talking point we need this
kind of budget we need a balanced budget blah blah blah. They were talking points
Since then, the reason that no Republican majority ever did it is because it's actually
not great for the economy.
This is a whole lot like their family planning stance.
They may end up catching the car on this one, too.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}