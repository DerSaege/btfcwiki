---
title: Let's talk about 5 concepts and political commentary....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=HvvudAZtv8I) |
| Published | 2023/05/22 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the concept of objectivity in journalism and commentary, acknowledging the challenge of true objectivity due to unconscious biases.
- Proposing five concepts to ponder on whether they are objectively good or bad, such as bigotry and authoritarianism.
- Asserting that denying people equal protection under the law, curtailing free speech, and favoring one religion over another are objectively bad.
- Questioning if objectivity means treating both political parties equally when one has embraced objectively bad principles.
- Arguing that objectivity does not equate to treating objectively bad actions as equal and showing favoritism to something detrimental.
- Emphasizing the importance of recognizing when one party's actions are objectively bad and not treating both sides as equal for the sake of false impartiality.

### Quotes

- "Bigotry is objectively bad."
- "Denying people equal protection under the law is bad."
- "Objectivity is not both sides in something that doesn't have two sides."
- "Treating it as an equal when it's not."
- "One party adopted a political platform and demonstrated through their actions that their route is objectively bad."

### Oneliner

Exploring objectivity in journalism and commentary, Beau challenges the notion of treating both parties equally when one adopts objectively bad principles.

### Audience

Journalists, commentators, readers

### On-the-ground actions from transcript

- Recognize and challenge objectively bad actions in political platforms (implied)
- Advocate for fairness and truth in reporting (suggested)

### Whats missing in summary

The full transcript delves into the nuances of objectivity in journalism and commentary, providing a critical perspective on treating objectively bad actions equally.

### Tags

#Journalism #Objectivity #Bias #PoliticalParties #Authoritarianism


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about five principles.
Impartiality,
objectivity,
and coverage.
You know, I've said before that I don't think true objectivity exists.
When you are talking about journalism, when you are talking about commentary,
I don't think
objective journalism is really a thing.
There are too many unconscious biases that come into play.
But it's something that you should at least strive for in some way.
Even if you're doing opinion pieces, it's important to try to remain objective.
But what does objectivity mean?
I think that's an important question as well.
I have five little concepts that I'm gonna put out there
and I want you to think about whether or not objectively
they are good or bad things.
Bigotry.
I'm willing to bet that
most people would agree with the statement
bigotry is objectively bad.
Like an overwhelming majority. I don't anticipate a whole bunch of people down in the
comments section
typing out, actually bigotry is a good thing.
Authoritarianism
objectively
is a bad thing.
Especially in the United States.
A country where
people are taught from a very young age
no taxation without representation.
the way our history is taught.
It reinforces the objective fact
that authoritarianism
is bad.
Denying people equal protection under the law
is bad.
Again, I'm not...
I don't anticipate a lot of pushback on these statements.
This is something that's
enshrined in the U.S. Constitution.
It's something the international community is trying to establish as a
world standard.
That's bad.
Denying people equal protection under the law is bad.
The government curtailing free speech is bad, right?
Again, human right enshrined in the Constitution, so on and so forth.
This is something that is objectively bad.
making laws that favor one religion over another.
Something that is objectively bad.
These are bad things.
Okay?
So with this in mind, because I'm willing to bet that 90% of people would agree with
all five of that, that they would be like, yeah, all those things, that's like bad stuff.
Okay?
With that in mind, is objectivity saying that both parties deserve equal treatment?
Because only one party made that their platform.
Only one party is out there scapegoating people based on immutable characteristics.
Bigotry.
Only one party is constantly reminding everybody that they don't care about the election results.
They do not care about that.
They're there to rule, not represent.
They don't care about the will of the people.
They don't care what people want.
They don't care how people voted, authoritarianism.
Only one party made that their platform, denying people equal protection under the law.
one party made that their platform, curtailing free speech, saying, you can't read that,
you can't wear that, you can't be in public if you look like this, if you are expressing
yourself in this way.
Only one party made that their platform, and only one party is attempting to legislate
religion. If these things are objectively bad and one political party has made
that their platform, objectivity dictates that you can't treat the two
parties the same because one of them has objectively chosen an authoritarian bent.
bent. One of them is violating basic concepts that society needs. Objectivity
is not both sides in something that doesn't have two sides. It's that
simple. You can say that I seem more favorable to the Democratic Party. I
remind you I'm not a Democrat. I try to be objective but being objective does
not mean that you say oh check out this relatable young fascist over here his
ideas have worth just the same as a normal person. A person who who wants
people to have freedom. Both sides in that treating that as equal, that's not
being objective. That's not being impartial. That is showing favoritism
to something that is objectively bad. Treating it as an equal when it's not.
Yeah, I am sure that my coverage does not seem neutral when it comes to the
political parties. But that wasn't my choice. One party adopted a political
platform and demonstrated through their actions that their route, the way they're
headed is objectively bad.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}