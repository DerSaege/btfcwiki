---
title: Let's talk about McConnell, Manchin, and leopards....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=YH6KS2Ca8SI) |
| Published | 2023/05/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Manchin, a Democrat from a predominantly red state, is facing an expected opponent in the next election who is incredibly popular.
- Senator McConnell, a Republican, has indicated his support for the Republican candidate in West Virginia, which has apparently deeply bothered Manchin.
- Manchin criticizes McConnell for going against him, stating that he has never campaigned against a sitting colleague and that McConnell represents everything wrong with Congress and the Senate.
- McConnell's support for Justice, the presumptive opponent of Manchin, could make it much harder for Manchin to win, although Manchin has previously won in a red state.
- Beau points out that the Republican Party prioritizes power above all else, even if it means compromising with authoritarians, as they are focused on gaining and maintaining control.
- McConnell's perspective is focused on gaining a majority of seats to become Senate Majority Leader or to ensure Republican control of the Senate.
- Beau warns about the dangers of compromise with politically shrewd individuals who thirst for power, as it can lead to being used as a means to an end.
- The situation in West Virginia exemplifies the power dynamics and strategic moves within the political landscape.

### Quotes

- "This is what's wrong with this place."
- "The compromise that a lot of people think is so important, it just sets you up to be surprised when people who are very politically shrewd and thirst for power decide to use you to get that power."

### Oneliner

Senator Manchin faces challenges as Senator McConnell supports his opponent, showcasing power struggles in politics.

### Audience

Politically aware citizens

### On-the-ground actions from transcript

- Analyze and understand the power dynamics at play in politics (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the power dynamics and strategic moves in the West Virginia political landscape.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Senator McConnell
and Senator Manchin and some friction that has developed
in the Senate over what might happen in West Virginia.
So to remind everybody, Senator Manchin
is kind of a Democrat.
He is a member of the Democratic Party
And he has done pretty well in West Virginia, which is a
predominantly red state.
However, now he has an expected opponent in the next
election who is incredibly popular.
Now, McConnell, he is, you know, McConnell.
Somebody who, for whatever reason, people continually
underestimate how shrewd he is.
Um, he's a Republican and he has indicated his support for the
Republican candidate, shock.
This has apparently really bothered Manchin who said, this is what's
wrong with this place, okay, and I have, I've said this, I have never ever
campaigned against a sitting colleague. And
if you want to know what's wrong with the process,
go talk to McConnell. That tells you everything that's wrong
with the Congress and the United States Senate, especially under his leadership.
Man,
I guess siding with the Republican Party
all those times really didn't benefit you very much.
I never thought the Leopards would come for my face, right? Yeah, so if McConnell
throws his weight behind Justice, who is the at this point the presumptive
opponent of Manchin, I mean I don't want to say it's a certainty that Justice is
going to win because Manchin has squeaked out wins in a very red state, but it is going
to be much harder for Manchin if McConnell throws his weight behind justice, which does
appear to be the case.
The Republican party is about power, and it doesn't matter if you compromise with
authoritarians.
They want power.
That never changes.
It doesn't matter if y'all are friends and have dinner and have lunch together all the
time.
At the end of the day, from McConnell's perspective, that's a seat.
seat is a seat, and he wants a majority of them so he can be Senate Majority Leader.
Or so at least the Republican Party can control the Senate.
The compromise that a lot of people think is so important, it just sets you up to be
surprised when people who are very politically shrewd and thirst for power decide to use
you to get that power.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}