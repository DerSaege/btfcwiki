---
title: Let's talk about Trump, CNN, and laughter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=O7LK7t0P_OE) |
| Published | 2023/05/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- CNN's soft interview with Trump backfired, leading to loss of control and negative coverage.
- Trump plans to campaign on the idea that the election was rigged, despite only 63% of his party supporting this notion.
- Trump's base, as seen in a rally where he mocked a victim, consists of individuals who find amusement in such behavior.
- CNN underestimated Trump's support at the interview, expecting a different crowd reaction.
- CNN's attempt to attract Fox News viewers by leaning right may lead to reporting biased towards right-wing talking points.
- Beau suggests that watching the part of the interview where Trump talks about E. Jean Carroll reveals insights into his base.

### Quotes

- "Trump plans to campaign on the idea that the election was rigged, despite only 63% of his party supporting this notion."
- "That's his base, the people who would laugh at the victim of that sort of crime."
- "CNN underestimated Trump's support at the interview, expecting a different crowd reaction."
- "Rather than reporting the news, they're gonna try to lean in to right-wing talking points a little bit."
- "Definitely watch the part where he is talking about E. Jean Carroll. Don't listen to him. Listen to the crowd."

### Oneliner

CNN's soft interview with Trump led to loss of control, revealing insights into his campaign strategies and base, while also shedding light on potential biases in reporting.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Question biased reporting and hold news outlets accountable (implied).
- Stay informed and critically analyze media coverage (implied).

### Whats missing in summary

Insights into media manipulation and audience influence during political events.

### Tags

#Media #CNN #Trump #ElectionRigging #BiasedReporting


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump on CNN.
To be honest, this wasn't something
I was gonna talk about because frankly,
I didn't see the need to cover a CNN-sponsored
infomercial for Trump, but lots of questions about it.
So before this happened, pretty much everybody was like,
yeah, this is a horrible idea.
CNN shouldn't be doing this.
They did it anyway.
yeah it was a horrible idea they shouldn't have done that. They lost
control of it almost immediately. They tried to run a soft interview with Trump
which was just a horrible idea and things went downhill. That's what the
coverage is on. I mean that is pretty much all of the coverage and I get it
that's what people are mad about. That's where the outrage is directed. They're
mad at CNN for doing this, but it's kind of skewing people, that's all they're
focused on. The thing is, there's something we can learn from this. We got
a glimpse of Trump's campaign, of what he actually plans on running on, and the new
New Trump, same as the old Trump, same as the old Trump, rigged.
He's going to lean into that.
He thinks that that's his ticket in, and it's not.
Only 63% of Republican and Republican leaning people believe that.
You can't win with that.
You cannot win with that.
If he campaigns on it was rigged, he is going to lose.
He's going to lose.
This is a very partisan topic.
It should be something that is near 100%.
he's hoping to win as polarized as this country is, running on it was rigged, it's
not gonna do it. It is absolutely not gonna do it. If you can't get all of your
side in a country as sharply divided as the US is right now, you can't win. And
keep in mind, this is him after he already lost. Remember, the majority of
Americans already rejected him once. If he leans into it was rigged, it's over.
He's done. But I don't think his ego is gonna let him do anything else. So you're
gonna see that. The other thing that that I find interesting, and I have a video
that I made in the immediate aftermath of the E. Jean Carroll case, talking about some
of the defenses I saw on social media, defenses of Trump.
And even after I made the video that just keeps getting bumped because of news, I was
wondering if that was just social media because you know there's that perceived
anonymity. Anonymity that doesn't really exist but people think people think it's
still there. Turns out it's not that. That's just his base. Trump made fun of
E.G. Carroll and the crowd laughed, man. That's his base, the people who would laugh at the
victim of that sort of crime. That's wild. That's the Republican Party today. I do not
think that is going to endear him to a lot of people. Ladies, I get those
questions all the time. Is this a red flag? If he's a Trump supporter, it's a
red flag factory. That's wild because it's not just online, it's in person too.
it's just who they are. Okay and that brings to the one thing about CNN that I
actually want to talk about. One of their things is we didn't know that the crowd
was going to be so on his side. Nobody at CNN thought that the crowd was going to
be on Trump's side. That seems odd. I mean did they think that like normal
people were going to give up their night, show up, go through the security, sit
there for that extended period to be lied to? I mean, that seems... that doesn't seem
like good investigative reporting. Of course they were gonna be on his side.
Nobody else is gonna show up for him. Trump is not your normal candidate. If
news outlets try to cover him like a normal candidate, it's gonna go bad every
single time. They will end up airing lies on their news network because they're
putting somebody on who's literally just held liable for defamation and he's just
going to talk. You can't run a soft interview with him. But overall yeah it was a
horrible idea but I think most people knew it was a horrible idea when it
happened. CNN is trying to kind of lean right a little bit. They're trying to
pick up some of Fox's viewers. It's not gonna work. I don't think it's gonna
work, but that seems to be the goal. Rather than reporting the news, they're
gonna try to lean in to right-wing talking points a little bit, which is
gonna lead them to both sides things that don't have two sides, and they're
going to get into trouble if they stick with this. I would hope that they learned
from this but we'll have to wait and see. The laughter, I don't suggest you watch
this. It'll be a waste of your time but if you are gonna watch a part of it,
definitely watch the part where he is talking about E. Jean Carroll. Don't
listen to him. Listen to the crowd. That's his base. Anyway, it's just a thought. Y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}