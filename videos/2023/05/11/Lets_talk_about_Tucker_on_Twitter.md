---
title: Let's talk about Tucker on Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=reVWWwDAZEg) |
| Published | 2023/05/11 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tucker Carlson is potentially moving to Twitter, a new home for him.
- Tucker faces the challenge of transferring his base from cable news to Twitter.
- Tucker may gain new followers on Twitter but will also face fact-checking and criticism.
- Elon Musk has not signed a deal with Tucker but has welcomed him aboard Twitter.
- Elon Musk faces the dilemma of allowing or disallowing fact-checking on Twitter.
- Democrats or some other group might pay people to fact-check Tucker in real-time.
- Elon Musk's main concern is Twitter's revenue and advertisers, especially after recent controversies.
- Advertisers may be hesitant to associate with Tucker, leading to revenue loss for Twitter.
- Tucker Carlson's extreme TV persona may not have the same impact on Twitter's right-wing audience.
- Tucker may have to compete with more extreme voices on Twitter, potentially leading to trouble with advertisers.

### Quotes

- "Tucker may have to compete with more extreme voices on Twitter, potentially leading to trouble with advertisers."
- "Elon Musk faces the dilemma of allowing or disallowing fact-checking on Twitter."
- "Tucker faces the challenge of transferring his base from cable news to Twitter."

### Oneliner

Tucker Carlson faces challenges transferring his base to Twitter, while Elon Musk grapples with fact-checking and revenue concerns.

### Audience

Twitter Users

### On-the-ground actions from transcript

- Monitor and fact-check accounts or information that may spread misinformation or extreme views on Twitter (implied).
- Support advertisers who choose not to associate with content you disagree with on social media (implied).

### Whats missing in summary

Analysis of the potential impact on Twitter's dynamics and culture by welcoming Tucker Carlson and the implications for the platform's future. 

### Tags

#TuckerCarlson #ElonMusk #Twitter #FactChecking #Advertisers #SocialMedia


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Tucker going to Twitter,
because that is apparently going to happen.
That's going to be his new home for at least a little while.
We're going to run through all of the various options,
because both Tucker and Elon, they
have very different concerns.
They have different issues that they're
have to deal with because of this new development. Elon has gone out of his way to say that they have
not signed a deal with Tucker, but has welcomed him aboard. So we'll see how that progresses.
But, what are the first concerns? We'll start with Tucker. Okay, so Tucker has to hope his base transfers.
Most of Tucker's base are people who watch cable news. They are not really...
most of them are not on Twitter. A lot of them are not going to follow him
because getting Twitter to your TV is going to be a little bit more difficult
than they want to put up with. It's really that simple, just ease of use type
stuff. Now, even though his base may not transfer, odds are he's gonna pick up
some from Twitter, so numbers-wise he should be fine. Now, you have to also
consider this. When Tucker's on Fox, he's in an echo chamber. When he's on Twitter,
he's not. So there will probably be instantaneous fact-checking of his shows
which might get really annoying for Tucker and people who are liberal or on
the left they're going to be incentivized to fact-check his shows
because of the way Twitter works, the engagement will benefit them. So it will
be a... it'll be like a freebie. Anytime their engagement starts to kind of slow
down, they can go and fact-check Twitter or go fact-check Tucker, make fun of him
and boost their engagement. So he's going to have to deal with that. The
The alternative to that is that people just decide, well, that's it for Twitter.
It's now truth social.
It has fallen to the right wing and they leave.
That's another option, in which case Tucker has his echo chamber back.
But that presents another problem later when we come back to Tucker.
For Elon, he has two options.
He can allow the fact-checking, which is certainly going to occur, or he cannot.
If he chooses not to, he is choosing to be truth social.
He also needs to understand that I would imagine if they have any sense at all, the Democratic
Party will literally be paying people to fact-checking in real time.
Maybe not the Democratic Party exactly, but a pack of some kind will probably be doing that.
So it will probably be pretty consistent.
Then you have to think about Elon's main issue, revenue at Twitter.
Advertisers. Advertisers are already nervous about Twitter, especially after the last week
with Elon going around and saying, hmm, that's interesting to every weird conspiracy theory out there.
So, you have to remember the position that Tucker's producers were in, in those leaked emails.
There was a whole lot of conversation about advertising.
In fact, they talked about bringing people on because they advertised with them.
They needed advertisers.
If Elon just lets him run wild, Elon's going to have that same issue.
So he may have to develop some kind of opt-in option for advertisers who don't want to
be associated with Tucker, which is probably what he's going to have to do because there's
going to be a lot of advertisers who are not going to want to be associated with that.
That would bring us back to Tucker.
If that happens, there's a loss in revenue.
And if there's a loss in revenue, well, he won't be promoted as much either because Twitter
won't be making as much money on it.
So there are a lot of options here as far as the impact and the financial side of it.
And then there's the one pitfall of Twitter that I don't know that Tucker's actually prepared
to deal with.
Tucker had a name for himself because he was the most extreme thing on TV.
He's not on TV anymore.
He's on Twitter.
It's Thunderdome.
His takes, the stories that he runs, the way he presents it, they're mild for a lot of
right-wing Twitter.
So he may have to start competing with them.
And when he does, if he chooses to have more and more extreme takes, he's going to end
up in trouble and in some ways he will probably push some people away and he will run into
trouble with advertisers.
Twitter has a habit of kind of pushing people into a situation where they feel the only
way they can compete on Twitter is to say things that are kind of outlandish.
Tucker had his base because he was extreme for TV.
He's kind of milk toast for right wing Twitter.
So I don't know that he's going to have the same cultural impact because there are things
on Twitter that are far beyond what Tucker puts out.
I don't know that this is actually a great move for either one of them, but at least
Because now we know where Tucker is headed, at least for the time being.
And it could be something that really energizes Twitter.
Or it could be something that is kind of the final nail in the birdhouse.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}