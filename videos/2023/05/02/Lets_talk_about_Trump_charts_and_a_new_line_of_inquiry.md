---
title: Let's talk about Trump, charts, and a new line of inquiry....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2CeG2uONCOI) |
| Published | 2023/05/02 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reports suggest special counsel is looking into wire fraud in relation to Trump diverting or inappropriately requesting funding during attempts to change election outcome.
- Uncertainty surrounds the specifics of what the special counsel is investigating beyond wire fraud.
- Mention of potential sentencing for wire fraud brings up misconceptions around maximum sentences in the federal system.
- Frustration expressed over media inaccuracies in reporting potential sentences, especially in civil rights cases involving law enforcement officers.
- Offense levels and points determine sentencing ranges in the federal system, not just maximum sentences.
- Severity in wire fraud cases is determined by the amount of money involved.
- Speculation on potential sentence length for Trump if found guilty of wire fraud involving $250 million, estimating between 150 and 210 months.
- Emphasis on the significant implications of a $250 million wire fraud case and the impact it could have on Trump, especially given his age.
- Mention of the importance of paying attention to this new avenue of inquiry, even though it may not be as sensational as other cases being looked into.
- Acknowledgment that if the allegations are true and the numbers are accurate, Trump could face serious consequences.

### Quotes

- "People familiar with the channel know that is something that just absolutely annoys me."
- "He's in trouble."
- "If he actually did what was alleged and those numbers are that high, he's in trouble."

### Oneliner

Reports suggest special counsel is investigating wire fraud allegations against Trump, potentially leading to significant consequences if the numbers hold true.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Pay close attention to developments in the investigation into wire fraud allegations against Trump (suggested).
- Educate others on the intricacies of federal sentencing guidelines to combat misinformation (exemplified).

### Whats missing in summary

Further insights on the potential implications of the wire fraud investigation on Trump's legal standing and public image.

### Tags

#Trump #WireFraud #SpecialCounsel #LegalSystem #FederalInvestigation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump charts
and what appears to be yet another avenue of inquiry.
There is a lot of reporting coming out
saying that the special counsel
looking into yet another avenue of things and it has led to some questions.
So first, what's the reporting? Reporting suggests they are now looking into wire
fraud. The guess at the time is that Trump might have diverted some funding
or inappropriately requested funding, that type of thing, during his attempt to overturn
things, to change the outcome.
So that's really all that's known at this point.
It's a lot of guessing other than the reporting says they're looking into wire fraud.
What exactly it is?
can guess because of the Jan 6 hearings, but nobody really knows. I want to be clear about
that. Nobody really knows exactly what they're looking into, at least that's not in the reporting.
So that led to a lot of pundits talking about what sentence he would be looking at
if he was convicted of wire fraud, and people kept throwing out that 20-year number because
that's the max sentence. People familiar with the channel know that is something
that just absolutely annoys me because in the federal system that's not how it
works. There's charts, there's points, levels, stuff like that, and that
determines the range that the judge is going to use. Most times people are
nowhere near the upper range of this stuff so that max number that gets
thrown out it doesn't it doesn't even apply and it annoys me when this happens
particularly in civil rights cases because the media sits there and tells
people you know this cop's looking at 30 years. No they're not. The charts say
they're looking at three to five and it builds this false expectation. So
somebody asked hey is this one of those things where they're not really looking
20 years. Yeah, I think based on what we heard at the January 6 committee, it's
definitely not going to be 20 years. That being said, it's not one of the things
where it's a slap on the wrist either. During the January 6 committee, they
threw out the number of 250 million dollars. That would be huge. The way this
This works is every crime, every federal crime has an offense level, a number that's assigned
to it.
That number has a range of months.
The more severe the action, the more levels get added.
Let's take substance, things that people like know about.
Let's say the base offense level is six, okay?
If you get caught with one to ten pounds, it might bump you up two levels.
That's nothing.
You're probably not even going to jail over that.
But instead of 10 pounds, let's say you had 10,000 pounds.
Well then you're going up a whole lot.
That's how it works.
The thing that determines the severity in wire fraud is how much money was taken.
$250 million?
That's huge.
My guess is that would put him in the range of 30, which is to convert it to months, somewhere
between 150 and 210 months.
Keep in mind, 120 months is 10 years.
If that number from the committee is right, and this really is what they're looking at
and they're going after him for this, it would be really high-ranging.
And this is coming from somebody who always is like, it's never going to be that much.
Two hundred and fifty million in a wire fraud case is huge.
He would be looking at a really long time, especially given his age.
So the news is there's reporting saying they're looking into wire fraud.
If that's really what they're doing and it's really about $250 million, yeah, he's in trouble.
Like more than for anything else.
Because again, all of these things that are at the federal level, they have those charts.
would be huge in comparison to pretty much everything else that's being looked
at and you can you can take from that what you will but it's definitely a new
avenue that people are going to have to pay attention to because realistically
even though it is nowhere near as interesting as you know a sedition case
or, you know, whatever, this may end up having the higher sentence. If he
actually did what was alleged and those numbers are that high, he's in trouble.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}