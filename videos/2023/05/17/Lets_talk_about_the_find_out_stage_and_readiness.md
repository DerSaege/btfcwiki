---
title: Let's talk about the find out stage and readiness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Kqi_wbk2YGw) |
| Published | 2023/05/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Legislation in red states negatively impacts military readiness by hindering the force's ability to do its job.
- Bases in red states are affected when the state targets certain demographics, leading to closures and economic losses.
- Space Force headquarters was supposed to be at Redstone in Alabama but is now likely to be in Colorado.
- The restrictive laws in Alabama regarding reproductive rights and targeting the LGBTQ community may have influenced the decision.
- Installations are selected based on a checklist that includes factors like room for growth, proximity to airports, schools, and healthcare access.
- Laws that were not in place when Huntsville was selected are now impacting decisions regarding military installations.
- Bases in states with restrictive laws may never be selected again due to the negative impact on readiness.
- Lack of investment in states with such restrictions will continue until the laws change, as it affects recruitment, retention, and quality of life.
- Politicians using wedge issues like these laws have consequences that hurt the military in the long run.
- The economic activity around military installations is at risk when bases close due to legislative impacts.

### Quotes

- "Legislation in red states negatively impacts military readiness."
- "Y'all did this. You're literally hurting the military."
- "It's not a political thing. It's because you're literally hurting the military."

### Oneliner

Legislation in red states negatively impacts military readiness, risking closures of bases and economic losses, hurting the military.

### Audience

Military advocates, policymakers

### On-the-ground actions from transcript

- Advocate for changes in restrictive laws impacting military installations (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of how legislation in red states affects military readiness and the implications for bases and communities.

### Tags

#Military #Legislation #Readiness #RedStates #Alabama #SpaceForce #EconomicImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about legislation
at the state level and a concept called readiness
and how sometimes legislation can impact readiness.
And we are also going to talk about reaching
the find out portion of the show.
For more than a year on this channel,
I have talked about how legislation that is being passed in a lot of red states is bad
for the military.
It hinders something called readiness, which is exactly what it sounds like.
The force's ability to do its job, because there are bases in these red states.
And let's say the state targets a certain demographic, and a soldier has a dependent
who's part of that demographic, well that soldier can't go there anymore, can't go
to that base.
That base starts to impact readiness, that base gets closed.
Talked about it for more than a year.
There will be links down in the description.
So the first step that occurs is these bases don't get selected to be expanded.
They don't want to invest somewhere that is hostile to their troops.
They don't want to send them somewhere where they can't get health care.
The next step is moving components of that base out of that base, out of the state.
Eventually the base closes.
And all of the economic activity that that base generates, well that's gone too.
There's a whole bunch of communities all over red states that are dependent on those military
installations.
Alabama, you apparently get to be the example. Currently, I think officially anyway, Space Force
headquarters was going to be at Redstone. Doesn't look like that's going to happen. Does not look
like that's gonna happen. The Biden administration is apparently on board
with it. I know there are some politicians saying that the Biden
administration did it. I don't think that's likely. I don't think that they
said don't put it there. I think that Space Force, which is under the
Department of the Air Force, which if you watch the videos that will be in the
description, you'll see they were the first branch, they were the first
department to come out and kind of fire a warning shot and say hey you can't do
this to our people. I feel like it was probably their decision and the Biden
administration is backing them up but it looks like Space Force headquarters will
be in Colorado now. I'm sure it's just a coincidence that Alabama has one of the
most restrictive laws when it comes to reproductive rights and they have
targeted the LGBTQ community. I'm sure that's just a coincidence, has nothing
to do with what's happening, right? It's worth noting that one of the officials
quoted in the reporting said, this is all about abortion politics. That's wrong.
It's also about targeting the LGBTQ community, but abortion politics has a
whole lot to do with it. When installations are selected it's the
military. Everything runs on charts and checklists. When you're looking for a
place to put a new headquarters you run through the checklist. You know, is there
room for growth? Is it close to airports? Does it have good schools? Is there good
access to health care. It's on the chart. It's on the checklist. No joke. All of
these laws, they were not on the books when Huntsville got selected. They are
now. That's what's changed. Now I'm sure the Republican politicians are going to
try to fight this and maybe, maybe they can structure some legal argument to
force Space Force to put something at Redstone because it was already selected.
maybe they can stand a chance. But make no mistake about it, this is step one.
And just to be clear, the only reason they might be able to force it is
because it was already selected. Installations that are in states with
laws like this, they will never be selected again. That opportunity for the
politicians to intervene and force it to happen, it's never going to happen again.
The bases don't complete the checklist anymore. The bases impact readiness.
They're not going to invest in them. They're not going to expand them.
Your politicians needed a wedge issue. They needed somebody to point at and say
look down at them so you don't pay attention to me. And it energized the
base. They got elected. This is the consequence. When those laws went through
this was pretty much guaranteed. And it doesn't stop. Understand the lack of
investment in states with these kinds of restrictions, the Department of Defense
is going to continue to do this until those restrictions go away. And
it's not because they have a political stance on it. It's because it impacts
readiness. It impacts recruitment. It impacts retention. It impacts where people
can be sent. It impacts the dependence. It impacts quality of life. Y'all did
this. The politicians who push this, those who supported them, y'all did this.
Y'all need to take a real hard look at the areas around military installations
that have closed. And what happens once that economic activity is gone? Because if you
don't change the laws, it's the future. And it's not a political thing. It's because
you're literally hurting the military.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}