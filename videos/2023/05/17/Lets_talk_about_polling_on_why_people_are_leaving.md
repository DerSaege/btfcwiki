---
title: Let's talk about polling on why people are leaving....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=FXhClIvx_ng) |
| Published | 2023/05/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains about polling data indicating a significant increase in the percentage of people changing their religious affiliation.
- Notes that 24% of individuals in the United States have changed their beliefs over their lifetime, a 50% increase from 2020.
- States that the primary reason for this change is losing faith, accounting for 56% of cases.
- Points out that negative teachings or treatment of the LGBTQ community is the second most common reason, at 30%.
- Mentions that 17% switched churches or congregations due to becoming too political.
- Stresses the importance of the separation of church and state for both institutions.
- Observes that many are leaving churches because they find it easier to preach hate than love.
- Suggests that the decline in church attendance and contributions is linked to this shift.
- Raises the idea that preaching hate rather than love is driving people away from religious institutions.
- Concludes by hinting that being a good person is ultimately more cost-effective than spreading hate.

### Quotes

- "The reason the pews are becoming more and more empty is because a lot of people have decided that it is easier to preach hate rather than preach love."
- "There's numbers to back it up."
- "It's cheaper to be a good person."

### Oneliner

Beau analyzes polling data on changing religious beliefs, attributing shifts to losing faith and negative treatment of the LGBTQ community, revealing a significant increase in affiliation changes.

### Audience

Religious communities, LGBTQ advocates

### On-the-ground actions from transcript

- Reassess church teachings and treatment of marginalized groups (suggested)
- Support inclusive religious communities (implied)

### Whats missing in summary

A deeper exploration of the emotional impact on individuals leaving faith communities. 

### Tags

#ReligiousBeliefs #PollingData #LGBTQCommunity #ChurchAndState #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about some polling
and we're gonna talk about change
and why people are changing what they believe.
We're gonna do this because this is something
that I have believed was the case for a long time,
but I never had any numbers to back it up.
It was an article of faith on my part,
but now there's some polling on it
and I think a lot of people might find it pretty enlightening so to start off
with the percentage of people who changed their religious affiliation they
changed what they believed over the course of their lifetime is now 24% in
United States. That's a wild number because in 2021 it was 16%. That means it's gone up 50%.
That's huge. So you have to ask why, right? I mean, that's the question everybody would want to know.
Why are people changing their religious affiliation? The number one reason
reason is they lost the faith. They stopped believing. That's 56%. The number two reason
is negative teachings or treatment of the LGBTQ community. 30%. That's pretty wild.
You have a massive increase in the number of people changing their religious affiliation,
and 30% of them are citing negative teachings or treatment of the LGBTQ community.
This is something I have believed for a really long time, but I never had the numbers.
Couldn't really find them.
I believed it because I mean it's a little hard to sit there and listen to somebody say
how Jesus is the perfect embodiment of love and then talk about hate and then preach hate.
Something else that's interesting, same study, same poll, is that 17% switched their church
or congregation because it became too political.
Separation of church and state, that's good for the church and the state.
This is a massive amount of people.
The reason the pews are becoming more and more empty is because a lot of people have
decided that it is easier to preach hate rather than preach love.
And now it's not just me saying that.
There's numbers to back it up.
are leaving churches, they are changing their religious affiliation because of
negative treatment or teachings about the LGBTQ community. That's the
main reason, second only to literally just not believing anymore. And I would
imagine that for a whole lot of people those two are interrelated. I would
imagine there's a lot of people who don't believe anymore because they saw
somebody who was a religious leader in their community preaching hate rather
than preaching love. The numbers are there. You want to know why attendance is
down? There's your answer. But since you probably don't care about that, may not
even keep track of those numbers if you want to know why there's less in the
collection plate. There's your answer. It's cheaper to be a good person. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}