---
title: Let's talk about Dems trying to oust Santos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AbPx3hCsiaA) |
| Published | 2023/05/17 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Democratic Party is attempting to expel Representative Santos from the House of Representatives.
- The Democrats believe it will be a tough vote for Republicans, revealing their naivety.
- The Republican base doesn't care about principles or loyalty to the country; they prioritize party loyalty.
- Voting to expel Santos won't impact Republicans in the general or primary elections.
- The current Republican Party prioritizes power and loyalty over principles and policy.
- Republican hypocrisy is futile to point out as they are focused on maintaining power.
- The Democratic Party needs to adjust its behavior accordingly in dealing with the current Republican Party.

### Quotes

- "The Republican base doesn't care about principle, about the country, stuff like that."
- "They care about loyalty. That's who runs their primaries now."
- "They literally don't care. The principles that that party claimed, they don't actually care about them anymore."
- "It's all about edgy comments, owning the libs and maintaining power."
- "The Democratic Party needs to acknowledge that. It needs to adjust their behavior accordingly."

### Oneliner

The Democratic Party's attempt to expel Santos reveals the futility of expecting Republican loyalty to principles over power.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Adjust behavior in dealing with the current Republican Party (suggested)
- Acknowledge the shift in Republican priorities and adjust accordingly (implied)

### Whats missing in summary

The full transcript delves into the changing dynamics within the Republican Party and the need for the Democratic Party to adapt its approach accordingly.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Democratic Party
attempting to expel
Representative Santas from the House of Representatives up there.
They're going through the steps to try to get Republicans on record
voting whether or not to expel him
or taking a vote to table it or something like that
because deep down
The Democratic Party still believes that this is going to be a tough vote for Republicans.
The Democratic Party is so naive at times, it is absolutely adorable.
For this vote to matter, the Republican base has to care.
They have to care about principle, about the country, stuff like that.
They have to have loyalty to more than their party.
isn't a tough vote for Republicans. It doesn't matter what they vote. It's not
going to impact them in the general and it's definitely not going to matter in
the primary. If anything, it's going to be a situation where those who voted to
expel are going to be damaged in the primary because the Republican base
doesn't care about policy, doesn't care about principle, doesn't care about the
country, they care about the party, they care about loyalty. That's who runs their
primaries now. It's not a hard vote. I get it. There was a time when something
like this would matter. That time has long since passed. And I do. I think it is
just absolutely adorable that there are people in the Democratic Party, in
Congress who think that this is something that the Republican base is
going to care about. Right now the right-wing talking point, the Republican
talking point, is that the FBI had absolutely no reason to investigate
Trump over Russian collusion. There's no way that he would have worked with Russia
to influence the outcome of an election. You shouldn't have investigated that.
That's just ridiculous. Russia, if you're listening, I hope you are able to find
the 30,000 emails that are missing. I think you will probably be rewarded
mightily by our press. Let's see if it happens. That wasn't said in secret. That
That was said to a bunch of reporters, but no, there's no reason to look into that even
though he's publicly making statements like that.
They don't care.
They do not care.
I don't think that any Republican is going to suffer if they vote to keep Santos in.
I get it.
understand where the Democratic Party is coming from. I understand what they
think matters. You're not dealing with the Republican Party of old. You're not
dealing with politicians. In many cases you are dealing with far right-wing
authoritarians who only hold loyalty to power and those who help them maintain
power. Getting rid of Santos, that reduces their number of seats. That'll be the
betrayal. They don't care about the institutions, they don't care about
policy, they don't care about any of that. Pointing out Republican hypocrisy is an
exercise in futility. They literally don't care. The principles that that
party claimed, they don't actually care about them anymore. It's all about edgy
comments, owning the libs and maintaining power and forcing those people who they view as lesser
than them to do their bidding. The Democratic Party needs to acknowledge that. It needs to
adjust their behavior accordingly.
Anyway, it's just a thought.
Y'all have a good day!
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}