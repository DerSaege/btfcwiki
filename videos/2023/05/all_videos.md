# All videos from May, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-05-31: Let's talk about the Texas impeachment trial scheduling.... (<a href="https://youtube.com/watch?v=D_JMAAXE_h0">watch</a> || <a href="/videos/2023/05/31/Lets_talk_about_the_Texas_impeachment_trial_scheduling">transcript &amp; editable summary</a>)

Texas Senate sets a distant August 28th date for Paxton's impeachment trial, potentially fostering animosity and political pressure within the Republican Party.

</summary>

"Texas Senate sets trial for Paxton's impeachment by August 28th, a distant date concerning the attorney general's removal."
"The prolonged delay in the impeachment trial could lead to increased anger, resentment, and fragile egos surfacing within the party."
"Trump's ego and potential embarrassment play a role in this situation, as he opposes Paxton's impeachment."
"Political pressure is likely to intensify as the trial date approaches, causing animosity within the Republican Party."
"The scandal's extended presence in the news cycle until August 28th could impact the political landscape and relationships within the Republican Party."

### AI summary (High error rate! Edit errors on video page)

Texas Senate sets trial for Paxton's impeachment by August 28th, a distant date concerning the attorney general's removal.
No word yet on Paxton's wife, a senator, about recusing herself from the proceedings.
Delaying impeachment trial seems strategic, possibly to muster votes with Trump's backing and pressure political decisions over impartiality.
Trump's vested interest in Paxton's case is evident; he may pressure political figures to support Paxton.
Trump's ego and potential embarrassment play a role in this situation, as he opposes Paxton's impeachment.
Abbott, the governor, faces criticism from Trump for not aiding Paxton and being "missing in action."
Political pressure is likely to intensify as the trial date approaches, causing animosity within the Republican Party.
The prolonged delay in the impeachment trial could lead to increased anger, resentment, and fragile egos surfacing within the party.
The extended timeline provides ample space for political maneuvering, polling assessments, and potential attack ads to emerge.
The scandal's extended presence in the news cycle until August 28th could impact the political landscape and relationships within the Republican Party.

Actions:

for political observers,
Contact your local representatives to express your views on the handling of political scandals (implied).
</details>
<details>
<summary>
2023-05-31: Let's talk about special months and a question.... (<a href="https://youtube.com/watch?v=4HJanN8xGFI">watch</a> || <a href="/videos/2023/05/31/Lets_talk_about_special_months_and_a_question">transcript &amp; editable summary</a>)

Beau addresses controversy over special months in the US, providing a thoughtful response to misinformation about Pride Month and Military Appreciation Month.

</summary>

"May is military appreciation month. If you post something like this today, go have fun with yourself. Signed, a combat veteran."
"They're asking for something that already exists, which means they really don't care."
"Maybe pointing out that they're literally using veterans as a prop, it might upset them."

### AI summary (High error rate! Edit errors on video page)

Addressing the controversy around special months in the United States, particularly regarding Pride Month and Military Appreciation Month.
Responding to a question from an individual whose dad criticized Pride Month, questioning why "the gays" have a month when veterans only have a day.
Advising the individual on how to respond to their dad's comments, suggesting pointing out that May is Military Appreciation Month.
Emphasizing that individuals parroting such statements online may not have accurate information and encouraging a common-sense approach to the topic.
Noting that veterans actually have several days and an entire month dedicated to appreciation.
Sharing a combat veteran's response to the issue, criticizing those who politicize veteran-related matters.
Suggesting ways to address the misinformation about Military Appreciation Month, either in May or when it circulates heavily in the following month.
Encouraging questioning the source of misinformation and prompting critical thinking about blindly believing such claims.
Pointing out that using veterans as a prop for political purposes is manipulative and misleading.
Urging individuals to challenge misleading sources and attempt to reach out to them with accurate information.

Actions:

for family members, allies,
Challenge misinformation about special months (suggested)
Educate others about Military Appreciation Month (implied)
Encourage critical thinking and fact-checking (suggested)
</details>
<details>
<summary>
2023-05-31: Let's talk about brains, reputation, and Elon Musk.... (<a href="https://youtube.com/watch?v=wVz_XuEw-Zs">watch</a> || <a href="/videos/2023/05/31/Lets_talk_about_brains_reputation_and_Elon_Musk">transcript &amp; editable summary</a>)

The Harris Poll rates Tesla's reputation drop, prompting speculation on Elon Musk's influence and Neuralink's FDA approval for brain chips.

</summary>

"There was a time not too long ago where he was kind of portrayed as somebody out to really better humanity."
"And now he has tweets that dabble in the wilder conspiracies."

### AI summary (High error rate! Edit errors on video page)

The Harris Poll rates companies based on their reputation, with Tesla dropping from 11th to 62nd place.
Speculation arises that Elon Musk's Twitter behavior may have influenced Tesla's reputation.
Neuralink, a company Musk is associated with, has received FDA approval for human studies on brain chips.
Despite controversy, Neuralink's technology has potential applications in healthcare and commercial sectors.
Concerns are raised about Musk's personal perception impacting companies he's associated with.
Musk's shift from being seen as a humanitarian to endorsing wild conspiracies may harm his reputation and affiliated companies.

Actions:

for business analysts, tech enthusiasts,
Research Neuralink's developments and controversies (suggested)
Stay informed about how public perception impacts companies (implied)
</details>
<details>
<summary>
2023-05-31: Let's talk about Trump's Day 1 promise.... (<a href="https://youtube.com/watch?v=z_il5VlcAP8">watch</a> || <a href="/videos/2023/05/31/Lets_talk_about_Trump_s_Day_1_promise">transcript &amp; editable summary</a>)

Donald Trump plans to undermine the US Constitution by signing an executive order on his first day in office, showing his disregard for the country's foundational values and his pursuit of power through scapegoating.

</summary>

"He's promising to undo the US Constitution with an executive order on day one."
"He absolutely would try because he doesn't care about the Constitution. He doesn't care about the country."
"He has promised on day one to undermine the U.S. Constitution."

### AI summary (High error rate! Edit errors on video page)

Analyzing Donald Trump's promise to sign an executive order on his first day in office to deny automatic US citizenship to the children of illegal aliens.
Trump's plan involves altering the US Constitution, specifically the 14th Amendment, through an executive order.
The oath of office Trump took to uphold and defend the Constitution seems contradictory to his plan.
Trump's disregard for the Constitution is evident in his promise to undermine it for his own power.
Trump's focus is on scapegoating people and gaining power for himself, rather than upholding constitutional values.

Actions:

for voters, constitution defenders,
Defend the US Constitution by advocating for its values and principles (implied).
</details>
<details>
<summary>
2023-05-30: Let's talk about why republicans did so bad at the negotiation.... (<a href="https://youtube.com/watch?v=8TRp7LzgWQ4">watch</a> || <a href="/videos/2023/05/30/Lets_talk_about_why_republicans_did_so_bad_at_the_negotiation">transcript &amp; editable summary</a>)

Republicans messed up in negotiations by choosing the wrong leverage, missing out on potential gains, while Biden emerged victorious despite expectations.

</summary>

"They chose the wrong thing for leverage."
"Government shutdown as leverage might have worked better."
"Biden definitely came out on top on this."

### AI summary (High error rate! Edit errors on video page)

Republicans messed up in negotiations.
Democrats unhappy with final product.
Republicans should have gotten more.
Most Republicans in the House are wealthy.
They didn't want to default.
They chose the wrong leverage.
Republicans should have passed a clean debt ceiling bill early on.
Government shutdown as leverage might have worked better.
Democrats understood Republicans probably wouldn't force a default.
Republicans walking away with less than they wanted.
Biden may have had the upper hand in negotiations.
Blame for the situation rests on the Republicans.
Republicans could have obtained more leverage by suggesting a government shutdown.
Despite Democratic concessions, Biden came out on top.

Actions:

for politically engaged individuals,
Contact your representatives and express your opinions on negotiation tactics (implied).
Stay informed about political negotiations and their implications (implied).
</details>
<details>
<summary>
2023-05-30: Let's talk about those opposed to the debt ceiling deal.... (<a href="https://youtube.com/watch?v=cnVKUIHq84o">watch</a> || <a href="/videos/2023/05/30/Lets_talk_about_those_opposed_to_the_debt_ceiling_deal">transcript &amp; editable summary</a>)

Beau explains the urgency of the debt ceiling timeline, warning of dire economic consequences if the bill fails, with no room for renegotiation, and potential political fallout for those who vote against it.

</summary>

"There's no time for renegotiation."
"Guess what? We default that day."
"It's really that simple."
"This is the deal. It is what it is."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the time constraints regarding voting on legislation, specifically related to the debt ceiling.
Points out that there is no time for renegotiation or delays in the process.
Emphasizes the importance of understanding the limited timeline for decision-making.
Warns against the consequences of voting against the bill and leading to default.
Stresses the potential negative impact on the economy and the likelihood of not getting re-elected if the bill fails.
Mentions the possibility of major companies withdrawing support if default occurs.
Suggests that the alternatives, like sending up a clean debt ceiling, might not be acceptable due to political reasons.
Concludes by stating that the situation is what it is, indicating a sense of finality and urgency.

Actions:

for politically engaged citizens,
Contact your representatives to urge them to vote responsibly on the debt ceiling bill (suggested)
Stay informed about the developments and implications of the debt ceiling issue (exemplified)
</details>
<details>
<summary>
2023-05-30: Let's talk about things over Moscow.... (<a href="https://youtube.com/watch?v=NCev03wbwQY">watch</a> || <a href="/videos/2023/05/30/Lets_talk_about_things_over_Moscow">transcript &amp; editable summary</a>)

Drones hit Moscow, Russia blames Ukraine, potential non-state actor involvement, shifting warfare dynamics with strategic implications.

</summary>

"Drones were over Moscow, hitting targets, with Russia blaming Ukraine."
"Non-state actors typically take general direction from sponsors, not direct orders."
"Increased drone attacks may provoke responses from both the Russian government and citizens."

### AI summary (High error rate! Edit errors on video page)

Drones were over Moscow, hitting targets, with Russia blaming Ukraine.
Ukraine has not acknowledged any involvement.
Possibility of a non-state actor claiming responsibility.
Russia likely to implicate Ukraine and its military.
Concerns over drones penetrating Russian air defenses or Ukrainian special operations within Russia.
Non-state actors operating in the conflict, not just a two-party conflict.
Social media reactions showcasing various perspectives.
Non-state actors typically take general direction from sponsors, not direct orders.
Increased drone attacks may provoke responses from both the Russian government and citizens.
Potential motivation from ongoing Russian bombardment of Ukraine's capital.
Shift towards low-intensity operations and unconventional warfare.
Strategic implications for future warfare due to the blending of state and non-state actors.
Anticipated headlines on the situation by morning.

Actions:

for global citizens, policymakers.,
Monitor news updates for developments on the situation (implied).
Stay informed about the evolving conflict dynamics (implied).
</details>
<details>
<summary>
2023-05-30: Let's talk about juries in the US.... (<a href="https://youtube.com/watch?v=NP603Pd3Tyw">watch</a> || <a href="/videos/2023/05/30/Lets_talk_about_juries_in_the_US">transcript &amp; editable summary</a>)

Beau explains flaws in the American legal system's jury duty culture, leading to wrongful convictions and lack of critical thinking jurors.

</summary>

"There's a number of issues with the culture surrounding our jury system and jury duty."
"Leads to a whole lot of people doing a whole bunch of time that maybe they shouldn't have."
"The standards of evidence in this country, it doesn't quite measure up to the way it gets portrayed in movies and on TV."

### AI summary (High error rate! Edit errors on video page)

Explains the American legal system and the issues surrounding jury duty.
Mentions how people try to get out of jury duty, leading to a less diverse and critical jury pool.
Notes that many on the jury may have poor critical thinking skills and tend to defer to law enforcement.
Describes how this dynamic can lead to wrongful convictions due to lack of skeptical jurors.
Points out that those most interested in fixing the criminal justice system often avoid jury duty.
Criticizes the standards of evidence in the American legal system, which differs from movie portrayals.
Suggests being more skeptical of police press releases instead of using excuses to avoid jury duty.

Actions:

for american citizens,
Question jury duty excuses (implied)
Advocate for diverse and critical jury pools (implied)
Educate others about the importance of jury duty (implied)
</details>
<details>
<summary>
2023-05-29: Let's talk about what's next for McCarthy.... (<a href="https://youtube.com/watch?v=XGQBAC_7_20">watch</a> || <a href="/videos/2023/05/29/Lets_talk_about_what_s_next_for_McCarthy">transcript &amp; editable summary</a>)

Beau dives into the budget deal drama within the Republican Party, exploring the challenges and potential outcomes faced by the hard-right faction.

</summary>

"The hard-right Republicans are faced with decisions on how to handle the situation."
"There will be significant political drama and infighting within the Republican Party over this issue."
"The Democratic Party might save McCarthy to weaken the far-right faction of the Republicans."

### AI summary (High error rate! Edit errors on video page)

McCarthy brought back a budget deal that doesn't satisfy the hard right of the Republican Party.
The hard-right Republicans are faced with decisions on how to handle the situation.
One promise they got from McCarthy is a motion to vacate, which they could use to remove him as speaker.
Options for the far-right Republicans include voting against the deal and filing the motion to remove McCarthy, or voting for the deal and justifying it to their base.
There's no guarantee the far-right Republicans will be successful in removing McCarthy if they choose that route.
Most representatives are likely to support McCarthy, making it challenging for the far-right group.
There will be significant political drama and infighting within the Republican Party over this issue.
Some Republicans in the Senate may oppose the deal, leading to further disruption within the party.
Despite internal conflicts, the Democratic Party might save McCarthy to weaken the far-right faction of the Republicans.
The situation is expected to escalate into more chaos and political maneuvering within the Republican Party.

Actions:

for political analysts, republican party members,
Contact your representatives to voice your opinion on the budget deal and the potential removal of McCarthy (suggested).
Organize community meetings to discuss the implications of internal conflicts within political parties (implied).
</details>
<details>
<summary>
2023-05-29: Let's talk about fault, the faultless, and blame.... (<a href="https://youtube.com/watch?v=_B_JhrJSLhQ">watch</a> || <a href="/videos/2023/05/29/Lets_talk_about_fault_the_faultless_and_blame">transcript &amp; editable summary</a>)

Beau sheds light on the GOP's push to end no-fault divorce, exposing it as a control tactic that reinforces patriarchy and jeopardizes relationships and safety.

</summary>

"It's not a red flag, it's a red flag factory."
"This is a horrible move by mediocre men."
"If you ever meet a guy and he says, you know, I'm in favor of ending no-fault divorce, run."
"Because if you probe and ask any questions, you're going to get so many more red flags."
"With everything that's going on, I have no idea how a woman can vote for any Republican at this point."

### AI summary (High error rate! Edit errors on video page)

Explains the movement within the GOP in Texas, Louisiana, and Nebraska to end no-fault divorce.
Points out that ending no-fault divorce is about control and compelling a partner to stay.
Shares personal experiences living in rural, conservative areas where men blamed no-fault divorce for their failed marriages.
Emphasizes that every man he met who complained about no-fault divorce was actually at fault in the relationship.
Clarifies that ending no-fault divorce doesn't mean the partner has to stay, but rather, it exposes the reasons for the divorce.
Notes the shift from the past when women were compelled to stay due to difficulties in proving issues like abuse, which technology now supports.
Criticizes the move to end no-fault divorce as a method to control and reinforce patriarchy without deserving privilege.
Advises women to be cautious of men who support ending no-fault divorce, as it signifies numerous red flags about their attitudes towards relationships.
Predicts that this issue may evolve into the next culture war due to societal pressures around marriage and divorce.
Condemns the potential impact of ending no-fault divorce, especially for women and in terms of domestic violence and suicide rates.

Actions:

for women,
Talk to friends and family about the importance of maintaining no-fault divorce laws (suggested).
Support organizations that advocate for women's rights and safety in relationships (implied).
</details>
<details>
<summary>
2023-05-29: Let's talk about Russia's nuke promise.... (<a href="https://youtube.com/watch?v=rVhzN3lnTcs">watch</a> || <a href="/videos/2023/05/29/Lets_talk_about_Russia_s_nuke_promise">transcript &amp; editable summary</a>)

Belarus-Russia nuke offer: strategic foreign policy or looming disaster? Spread of nukes sparks concerns, but implementation could be disastrous.

</summary>

"The spread of nuclear weapons is always something to worry about."
"Handing out nukes like candy, it's not a good move."
"If Russia is smart, this is a promise that takes a really long time to fulfill."

### AI summary (High error rate! Edit errors on video page)

Belarus hinted at joining Russia's union state in exchange for nukes, sparking concerns.
Explains the potential foreign policy advantages for Russia in offering tactical nuclear weapons.
Emphasizes that Russia won't actually hand out nukes but maintain control over them.
Warns against the negative implications of distributing nukes, citing security risks and conflict potential.
Notes that Russia's perceived military strength is largely due to its nuclear arsenal.
Asserts that handing out nukes could lead to a dangerous precedent and decreased prestige for Russia.
Suggests that the offer of nukes serves as a strategic foreign policy move, but implementing it fully could be disastrous.
Speculates that Russia may not transfer nukes to many countries, especially those not directly tied to them.
Considers the possibility of conditions delaying the actual transfer of nukes and maintaining Russia's control.
Stresses the ongoing concern about the spread of nuclear weapons, despite the current situation being more of a foreign policy move than a military one.

Actions:

for foreign policy analysts,
Monitor international developments and policies related to nuclear proliferation (implied)
Advocate for strong non-proliferation agreements and measures (implied)
</details>
<details>
<summary>
2023-05-28: Let's talk about why Republicans impeached Paxton.... (<a href="https://youtube.com/watch?v=uw38xM4JfWc">watch</a> || <a href="/videos/2023/05/28/Lets_talk_about_why_Republicans_impeached_Paxton">transcript &amp; editable summary</a>)

The Attorney General of Texas impeached and removed, sparking speculation on hidden motives within the Republican Party.

</summary>

"A Republican-led committee and a Republican House have impeached a Republican Attorney General in Texas."
"But I understand why people are asking this. I understand why people are curious. I get it."
"I don't know that this is something I'm gonna go looking really hard into."

### AI summary (High error rate! Edit errors on video page)

Attorney General of Texas impeached and removed, heading to Senate trial.
Calls made by Paxton's team to representatives warning against impeachment.
Trump and other Republican figures opposed impeachment, yet it happened.
Speculation on motives behind Republican Party's actions.
Potential motives: money, underestimation of Paxton's situation, undisclosed information.
Possibility of more allegations surfacing or legal entanglements catching up with Paxton.
Likelihood of Republicans knowing something undisclosed as motivation for impeachment.
Anticipation of litigation and trial in the Senate.
Trump's involvement and loyalty to Paxton post-2020 election.

Actions:

for political analysts, texas residents.,
Stay informed about the developments in Paxton's case (implied).
Monitor the litigation and trial proceedings in the Senate for transparency (implied).
Advocate for accountability and transparency within political parties (implied).
</details>
<details>
<summary>
2023-05-28: Let's talk about who won the debt ceiling negotiations.... (<a href="https://youtube.com/watch?v=4cJMKfB63X8">watch</a> || <a href="/videos/2023/05/28/Lets_talk_about_who_won_the_debt_ceiling_negotiations">transcript &amp; editable summary</a>)

Biden seems to have the upper hand in the deal with McCarthy, but the true impact will depend on Democratic support for passing the bill.

</summary>

"Biden got the better of McCarthy."
"The Republicans got their two core values fulfilled."
"The hardliners definitely did not get what they wanted."
"I'm going to say that Biden got the better of him."
"Your real gauge at the end of this is how much help the Republican Speaker of the House needs from the Democratic Party."

### AI summary (High error rate! Edit errors on video page)

Biden and McCarthy have a deal, but the details are still unknown.
It appears that Biden got the better of McCarthy based on leaks and reactions from Congress.
The Republican Party made promises but many of them are not reflected in the current deal.
Despite some changes to programs like SNAP and temporary assistance, Republicans seem to have achieved their core values of cutting the IRS budget and imposing work requirements.
The deal includes sunsets for certain programs, meaning they phase in over time and then cease to be a law.
McCarthy may need Democratic votes to pass the budget, indicating potential challenges within the Republican Party.
Tangible effects such as cuts to programs like the Inflation Reduction Act or infrastructure bill do not seem to be included in the deal.
While there are concerns about work requirements, the full impact will be clearer once the bill is seen.
Biden appears to have come out stronger in the deal, but the actual bill will provide more clarity.
The final assessment will depend on how much support McCarthy needs from the Democratic Party to pass the bill.

Actions:

for politically informed individuals,
Contact your representatives to express concerns about work requirements in social safety net programs (implied)
Stay informed about the details of the bill as it unfolds to understand its real effects (implied)
</details>
<details>
<summary>
2023-05-28: Let's talk about when it's time to leave.... (<a href="https://youtube.com/watch?v=O74huDYBPbI">watch</a> || <a href="/videos/2023/05/28/Lets_talk_about_when_it_s_time_to_leave">transcript &amp; editable summary</a>)

Knowing when it's time to leave a state is vital, with planning urgency tied to resources and circumstances—start early if targeted by legislation.

</summary>

"If you're considering it, if this is something that has worried you, if there is legislation that is specifically targeting you, now's the time to start making a plan."
"You have to plan and you have to put the pieces of the plan in motion that you can."
"And doing that, you'll relieve the pressure later."
"You need to start thinking about it now."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing the importance of knowing when it's time to leave a state due to increasing legislation targeting certain demographics.
The decision to leave varies based on individual circumstances and resources.
Two extremes presented: one with a tech job, remote work, and savings, requiring minimal pre-planning, and the other with kids, a home, paycheck to paycheck situation, needing detailed planning.
Emphasizing the need to start planning as soon as the thought of leaving arises, especially if legislation is targeting you.
Urging immediate action for those with limited resources, suggesting considerations like job prospects, schooling for kids, and financial requirements for moving.
Stressing the importance of being prepared and having a plan in motion, considering factors like security deposits and job opportunities in the new state.
Mentioning the potential challenges of moving to a state with an influx of people, like increased rent prices and tougher job markets.
Encouraging proactive planning for those who may be targeted or marginalized, to alleviate future pressure.
Advising on the necessity of making a decision based on personal circumstances, resources, and risk levels.
Concluding with a reminder to start thinking about these decisions promptly.

Actions:

for residents facing targeted legislation.,
Start planning for a potential move immediately, considering job prospects, schooling options, and financial requirements (implied).
Save up for first and last month's rent and security deposits in the new state (implied).
Make connections and gather resources in the area you plan to move to (implied).
</details>
<details>
<summary>
2023-05-28: Let's talk about when it's going to start in Ukraine.... (<a href="https://youtube.com/watch?v=j5UlA5SAfaI">watch</a> || <a href="/videos/2023/05/28/Lets_talk_about_when_it_s_going_to_start_in_Ukraine">transcript &amp; editable summary</a>)

Beau explains how continuous false alarms about the conflict starting in Ukraine degrade the effectiveness of the Ukrainian military's response.

</summary>

"Imagine being on the receiving end of it, and every other week it's starting today, it's starting today. Eventually you stop believing it, and once you stop believing it, well, that's when it happens."
"Because they've done it so many times, it becomes routine. Once it becomes routine, well, they're not really paying attention anymore."
"If you constantly expect something and it doesn't happen, eventually you expect it to not happen."

### AI summary (High error rate! Edit errors on video page)

Many messages in his inbox asking about when the conflict in Ukraine will start, with people expecting it to kick off any moment.
Russia has a history of running troops up to the border before invading, creating a pattern of false alarms to keep up the element of surprise.
Continuous rumors about the conflict starting prompt the Ukrainian military to make moves in response, causing the Russians to react and move troops around.
The repeated false alarms degrade the resolve and alertness of the Ukrainian military, making them routine and less effective over time.
Psychological delay tactics like this are common in military strategies, and they tend to work effectively by creating a pattern of expectations.
Even with good intelligence, constant false alarms can lead to decreased alertness and attention, akin to the story of "the boy who cried wolf."

Actions:

for military analysts, ukraine watchers,
Stay informed about the situation in Ukraine and be cautious of continuous rumors that may affect perceptions and responses (implied).
</details>
<details>
<summary>
2023-05-28: Let's talk about the roads to fact checking.... (<a href="https://youtube.com/watch?v=fHUosTiu6vU">watch</a> || <a href="/videos/2023/05/28/Lets_talk_about_the_roads_to_fact_checking">transcript &amp; editable summary</a>)

Beau provides techniques to avoid falling for misinformation, including verifying quotes, checking statistics, and viewing memes as propaganda.

</summary>

"Figures don't lie, but liars certainly figure."
"View every meme as propaganda."
"Make sure there's something down below saying, hey this is bad."
"Retrain your algorithm."
"A string of coincidences. Nothing to see here."

### AI summary (High error rate! Edit errors on video page)

Provides techniques on avoiding falling for bad information and fact-checking oneself.
Advises asking if information confirms existing beliefs and if it is sensationalized.
Talks about using reverse image search to verify images in memes.
Mentions the impact of AI on misinformation and fact-checking.
Emphasizes verifying quotes and their context through Google.
Warns about cherry-picking quotes and nut-picking to misrepresent groups.
Advises checking statistics for accuracy, methodology, and context.
Talks about linking unrelated events to create false narratives.
Mentions the difference between correlation and causation.
Encourages viewing memes as propaganda and retraining algorithms for quality information.

Actions:

for social media users,
Comment on misinformation to slow its spread (suggested)
Block outlets that consistently provide false information (exemplified)
</details>
<details>
<summary>
2023-05-27: Let's talk about the vote tomorrow on Texas AG's impeachment.... (<a href="https://youtube.com/watch?v=HR-24CYhuEI">watch</a> || <a href="/videos/2023/05/27/Lets_talk_about_the_vote_tomorrow_on_Texas_AG_s_impeachment">transcript &amp; editable summary</a>)

Texas Attorney General faces rapid impeachment process with potential legal challenges, signaling strong House support and urgency for removal.

</summary>

"He will be looking at 20 articles of impeachment."
"One of them said that to not move forward, they would be derelict in their duty after hearing the evidence."
"The impeachment is moving quickly."

### AI summary (High error rate! Edit errors on video page)

Texas Attorney General Ken Paxton faces impeachment with unprecedented speed, with the process happening on a weekend with 20 articles of impeachment.
Paxton claims his impeachment is illegal, citing the need for voters to be aware, but experts argue that the Texas state constitution supersedes any statute.
The rapid impeachment process suggests that the House likely already has the votes secured, possibly due to allegations against the Speaker of the House.
Paxton, an influential figure with insider knowledge, may be making calls to prevent potential impeachment threats.
Despite potential legal challenges, the House seems determined to remove Paxton from office, with some members seeing it as their duty to do so.

Actions:

for texas residents,
Contact local representatives to express support or opposition to the impeachment process (implied)
Stay informed about the developments in the impeachment process and potential legal battles (implied)
</details>
<details>
<summary>
2023-05-27: Let's talk about Trump vs DeSantis.... (<a href="https://youtube.com/watch?v=UpRCllEUS6A">watch</a> || <a href="/videos/2023/05/27/Lets_talk_about_Trump_vs_DeSantis">transcript &amp; editable summary</a>)

DeSantis attempts to emulate Trump without the drama, facing challenges in policies and national appeal, while Trump's base remains rooted in cruelty and drama.

</summary>

"DeSantis is trying to cast himself as Trump without the drama."
"The cruelty is the point. The drama is the point."
"I don't see how he can make it through the primaries."
"Let's make America Florida."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Contrasts Trump and DeSantis in their political approaches and personas.
Describes Trump as defined by grievance, rage, and drama, while DeSantis is trying to run as Trump without the drama.
Explains that DeSantis is focusing on being anti-woke and policy-oriented, which may not be a winning strategy.
Points out that DeSantis is trying to adopt Trump's policies without the anger and drama associated with them.
Criticizes Trump's policies, such as mishandling a pandemic and building a failed vanity project, as not appealing to the majority.
Suggests that DeSantis's efforts may work within a hard-right Republican base but could face challenges on a national level.
States that most people didn't vote for Trump based on policy but rather for the excuse he gave them to be their worst selves.
Expresses skepticism about DeSantis's chances of winning as long as Trump is in the race.
Speculates that if Trump exits the race, DeSantis might become a more significant contender but still faces obstacles in a general election due to his policies.
Concludes that DeSantis may struggle to gain broader acceptance beyond his hard-right base and favorable media coverage.

Actions:

for political analysts,
Analyze political candidates without bias (implied)
Stay informed about political strategies and policy implications (implied)
</details>
<details>
<summary>
2023-05-27: Let's talk about Biden, Tuberville, and the Joint Chiefs.... (<a href="https://youtube.com/watch?v=oivvIsXgwhc">watch</a> || <a href="/videos/2023/05/27/Lets_talk_about_Biden_Tuberville_and_the_Joint_Chiefs">transcript &amp; editable summary</a>)

President Biden's pick for the Joint Chiefs intersects with Senator Tuberville's damaging block on military promotions, posing a choice for Alabama between military readiness or a stronger economy.

</summary>

"It doesn't matter what cool little incentives you offer. If politicians in the state create an environment that damages military readiness, the military is not going to want to be there."
"Do you want to kick down at people? Or do you want a stronger economy?"
"People may not join a force if they think they're going to be sent to a place like that."
"The people of Alabama need to decide what they want and they need to start voting that way."

### AI summary (High error rate! Edit errors on video page)

President Biden's selection for the new chairman of the Joint Chiefs, General C.Q. Brown, is about to intersect with Senator Tuberville's roadblocking of military promotions.
Senator Tuberville has been blocking around 180 military promotions, damaging military readiness, including those of the current Air Force Chief of Staff and the incoming Chairman of the Joint Chiefs.
Huntsville, Alabama, is striving to support the military for the Space Command presence at Redstone to boost the economy.
Despite Huntsville's efforts, if politicians in a state create an environment that undermines military readiness, it deters the military from being there.
Senator Tuberville's actions show a lack of commitment to military readiness in Alabama, impacting the state's reputation.
Alabama faces a choice between damaging actions by politicians or striving for a stronger economy and better quality of life.
Senator Tuberville's remarks about inner city teachers' literacy, coming from a state with low education rankings, reveal a disconnect from reality and potentially harmful attitudes.
The goal behind such comments is to incite division and maintain a status quo of kicking down on others.
Outdated approaches and bigoted legislation can deter people from joining the military, affecting recruitment and retention.
Alabama must decide its priorities and voting patterns to shape its future and military investments.

Actions:

for alabama voters,
Vote in alignment with policies supporting military readiness and economic growth in Alabama (implied).
Advocate for inclusive and supportive legislation that enhances recruitment and retention in the military (implied).
</details>
<details>
<summary>
2023-05-27: Let's talk about Biden taking the 14th Amendment off the table.... (<a href="https://youtube.com/watch?v=mkhL9a8AAbc">watch</a> || <a href="/videos/2023/05/27/Lets_talk_about_Biden_taking_the_14th_Amendment_off_the_table">transcript &amp; editable summary</a>)

Biden's public withdrawal of the 14th amendment option in debt ceiling negotiations risks weakening leverage and echoes past mistakes in telegraphing moves, urging strategic silence in dealing with uncooperative Republicans.

</summary>

"You can't telegraph during negotiations."
"You have to treat them as if they are actively trying to damage the country."
"Telegraphing moves in negotiations is bad."
"Biden, if you notice, has been really quiet during all the negotiations."
"It wasn't smart."

### AI summary (High error rate! Edit errors on video page)

Biden administration taking the 14th amendment off the table regarding the debt ceiling sparks questions on the reasoning behind this decision.
Speculations arise on whether Biden's choice is to appease Republicans or to avoid economic damage through legal challenges.
Biden's announcement of not using the 14th amendment is criticized as a mistake in negotiations, akin to Trump telegraphing moves in Afghanistan.
Telegraphing moves in negotiations is seen as detrimental, as it diminishes leverage and allows opposition to prepare.
Dealing with Republicans who are not negotiating in good faith requires treating them as the opposition to protect the country's interests.
The Republican party's indifference to economic stability under Biden leads to the necessity of cautious and strategic negotiation tactics.
Public statements during negotiations are cautioned against, as they can weaken one's position.
Keeping quiet during negotiations is viewed as a prudent move, allowing for strategic advantage by not revealing intentions.
The importance of maintaining secrecy and strategic ambiguity during negotiations is emphasized to prevent undermining one's position.
Biden's decision to announce taking the 14th amendment off the table is deemed unwise and potentially damaging to negotiation outcomes.

Actions:

for negotiators, policymakers,
Maintain strategic silence during negotiations (suggested)
Treat uncooperative parties as opposition for protection of interests (implied)
</details>
<details>
<summary>
2023-05-26: Let's talk about when to pick up the phone and call.... (<a href="https://youtube.com/watch?v=4897lmwKH7s">watch</a> || <a href="/videos/2023/05/26/Lets_talk_about_when_to_pick_up_the_phone_and_call">transcript &amp; editable summary</a>)

Beau explains the risks of summoning lethal force by calling 911 and urges for thoughtful alternatives in non-violent situations to prevent unnecessary escalations involving armed authorities.

</summary>

"When you dial 911, you're summoning a lethal force."
"Every law is backed up by that. It's an inherent part of it in the US because of how we justify things."
"Using armed people who don't know what's going on, it's not normally a good idea."
"Not just might you save somebody's life, stop somebody from being killed who didn't need to be killed. The life you save might be your own."
"There are normally better ways to deal with things."

### AI summary (High error rate! Edit errors on video page)

Explains the appropriate time to contact law enforcement, especially in violent situations where force may be necessary.
Points out that calling 911 summons lethal force and there might be better alternatives in non-violent situations.
Illustrates how every law is backed by the potential for lethal force, not necessarily the death penalty, using examples like littering.
Emphasizes that non-violent actions can escalate to violence if not complied with by law enforcement.
Shares a real-life incident where a native man calling Border Patrol about trespassing led to his death, showcasing the risks of involving armed individuals who may not understand the situation.
Questions the effectiveness of law enforcement de-escalation when calling them might actually escalate the situation.
Encourages considering alternative ways to handle situations rather than immediately involving armed authorities.
Raises awareness about the potential consequences of involving law enforcement and urges for a more thoughtful approach to seeking help.

Actions:

for concerned citizens,
Question the necessity of involving law enforcement in non-violent situations (implied)
Advocate for exploring alternative ways to handle conflicts before resorting to calling authorities (implied)
</details>
<details>
<summary>
2023-05-26: Let's talk about the new Trump documents reporting.... (<a href="https://youtube.com/watch?v=qr4Dgl7n6pI">watch</a> || <a href="/videos/2023/05/26/Lets_talk_about_the_new_Trump_documents_reporting">transcript &amp; editable summary</a>)

New revelations suggest Trump may face severe charges beyond willful retention, possibly involving obstruction and conspiracy.

</summary>

"Employees allegedly moved documents the day before a search warrant was executed."
"The potential charges could look like the discord leaker's charges."
"Recent reporting suggests the possibility of severe charges beyond willful retention."

### AI summary (High error rate! Edit errors on video page)

New information on Trump's documents situation has escalated things to a new level.
Employees allegedly moved documents the day before a search warrant was executed.
In May 2020, Trump conducted dress rehearsals on how to hide and retain documents.
Trump showed documents to unauthorized individuals.
National Archives revealed Trump's awareness of the declassification process.
The notion of willful retention is significant.
Trump may face charges like obstruction, dissemination, and conspiracy.
There are implications that Trump coordinated with others to conceal documents.
The potential charges could be severe, akin to those faced by the Discord leaker.
Trump's team anticipates an imminent indictment, but uncertainties exist.
Recent reporting suggests the possibility of severe charges beyond willful retention.
The outcome may vary based on who accessed the documents, especially if they involve foreign nationals.
There is a likelihood of more severe charges if the reported information is accurate.

Actions:

for legal analysts,
Contact legal experts for analysis (suggested)
</details>
<details>
<summary>
2023-05-26: Let's talk about articles of impeachment in Texas being filed.... (<a href="https://youtube.com/watch?v=qH3zZmx51ks">watch</a> || <a href="/videos/2023/05/26/Lets_talk_about_articles_of_impeachment_in_Texas_being_filed">transcript &amp; editable summary</a>)

Texas is moving forward with impeaching Paxton, facing questions on voter awareness, potential quick proceedings, and escalating tensions within the Republican Party.

</summary>

"Texas is moving forward with the impeachment process against Paxton."
"Paxton could be temporarily relieved of duty if the House impeaches him."
"If you're in Texas, get ready for a show."

### AI summary (High error rate! Edit errors on video page)

Texas is moving forward with the impeachment process against Paxton.
The Republican-led committee in Texas unanimously voted to proceed with impeachment, filing 20 articles.
Impeachment in Texas requires a simple majority in the House and two-thirds in the Senate to convict.
Paxton could be temporarily relieved of duty if the House impeaches him.
Paxton claims voters already knew about the allegations against him.
Impeachment in Texas can only be based on actions taken after election to office.
Some allegations against Paxton might have been unknown to voters, raising questions about fulfilling obligations.
The process could move quickly, possibly within two weeks.
Tensions within the Republican Party are rising due to Paxton's responses.
Paxton's approach of attacking those involved may not be beneficial.
The Texas House's stance on the impeachment remains to be seen.

Actions:

for texans,
Stay informed on the impeachment proceedings and potential outcomes (exemplified)
Engage with local news sources for updates on the situation (exemplified)
</details>
<details>
<summary>
2023-05-26: Let's talk about Texas teachers and me missing something.... (<a href="https://youtube.com/watch?v=fJi81XafZT8">watch</a> || <a href="/videos/2023/05/26/Lets_talk_about_Texas_teachers_and_me_missing_something">transcript &amp; editable summary</a>)

Texas lawmakers push to arm teachers, while a science teacher's alleged violent act raises concerns about the risks involved, stressing the importance of keeping guns out of schools.

</summary>

"The solution here is not more guns. It's not bringing more guns into the building. It's keeping them out."
"Teachers are people. They're flawed, just like anybody else, and a percentage of them will have that history."
"The level of training it takes to get somebody to that point is immense."
"There is a huge link between DV histories and mass incidents."
"This is a bad move. This is going to go bad."

### AI summary (High error rate! Edit errors on video page)

Texas lawmakers are upset that schools are not arming teachers despite their efforts to push for it.
A science teacher in Texas is accused of shooting his sleeping son and stepdaughter during a domestic situation.
Beau stresses the immense training required for individuals to immediately respond to critical situations like the one involving the teacher.
He points out the significant link between domestic violence histories and potential future violent incidents.
Despite teachers being viewed as protectors of children, Beau acknowledges that they are also human and may have flawed histories.
Lawmakers advocating for arming teachers may inadvertently enable individuals with violent histories to bring weapons into schools.
Beau argues against the idea of introducing more guns into school buildings as a solution.
He advocates for keeping guns out of schools as a short-term solution to prevent potential tragedies.

Actions:

for texas residents, educators,
Advocate for policies that prioritize keeping guns out of schools (implied).
</details>
<details>
<summary>
2023-05-25: Let's talk about whether you're getting paid.... (<a href="https://youtube.com/watch?v=lfnabxTGKQ4">watch</a> || <a href="/videos/2023/05/25/Lets_talk_about_whether_you_re_getting_paid">transcript &amp; editable summary</a>)

The impending debt ceiling crisis looms, with Republicans blamed for holding up a resolution and jeopardizing vital payments.

</summary>

"If you get that [payment] check, you need to be as thrifty as possible with it until this is resolved."
"They could have sent up a clean debt ceiling. They could do it at any point in time."
"They wanted to use you as leverage. They wanted to damage your economic stability."
"All it takes is for the Republican party to actually care about the country for once."
"I'm still pretty hopeful that this gets resolved."

### AI summary (High error rate! Edit errors on video page)

Urgent talk about the impending debt ceiling crisis, with the possibility of the US defaulting on the 1st, just days away.
Major payments like Medicare, VA benefits, military pay, and Social Security are at risk if the debt ceiling issue isn't resolved.
Individuals receiving checks need to be cautious with spending until the situation is resolved.
Blames Republicans in the House for not raising the debt ceiling, using people's anxiety and fear as leverage against Biden.
Republicans could have easily sent a clean debt ceiling proposal, but they chose not to, preferring to make the public suffer.
Criticizes Republicans for not presenting a real budget and using citizens as leverage instead of negotiating in good faith.
Calls out McCarthy for deflecting blame, reminding that the House is responsible for the deadlock.
Encourages people to prepare for the possibility of payments not going out if the issue isn't resolved soon.
Stresses the importance of Republicans caring about the country to resolve the crisis.
Expresses hope for a resolution but warns about potential payment disruptions if action isn't taken.

Actions:

for us citizens,
Contact your representatives and urge them to prioritize resolving the debt ceiling issue (suggested).
Stay informed about the situation and its potential impacts on your payments and benefits (implied).
</details>
<details>
<summary>
2023-05-25: Let's talk about SCOTUS and checks and balances.... (<a href="https://youtube.com/watch?v=PMdt35vXpGM">watch</a> || <a href="/videos/2023/05/25/Lets_talk_about_SCOTUS_and_checks_and_balances">transcript &amp; editable summary</a>)

Beau dives into the Supreme Court, Congress's authority, ethical standards, and the need for checks and balances in a system of independent branches.

</summary>

"Independent doesn't mean immune from, the system was set up to have checks and balances."
"The whole idea is to have each branch checks the others."
"They absolutely have the power to impose ethical standards they have in the past, and I'm sure they will in the future."
"Congress can actually alter a lot of things about the Supreme Court."
"I definitely don't think this saga is over."

### AI summary (High error rate! Edit errors on video page)

Addressing the Supreme Court, branches of government, and Congress's power to legislate about other branches.
Chief Justice Roberts considering new ethical standards and Harlan Crowe dismissing Congress's legislative interest in his friendships.
Congress's authority to legislate and its impact on the court's independence.
Pelosi's subpoena power to call Justice Roberts for a hearing investigation.
The necessity for Congress to show a legislative interest to conduct a hearing investigation.
The concept of independent but not immune branches of government with checks and balances.
Comparing Congress enforcing ethics on the Supreme Court to the Presidential Records Act enforced after Nixon.
Congress's power to impose ethical standards and the need for Roberts to self-regulate to keep Congress out of the Supreme Court's internal affairs.
Congress's ability to alter aspects of the Supreme Court except those outlined in the U.S. Constitution.
Speculation on Roberts' intention to self-regulate and the ongoing saga regarding ethical standards and the relationship between Harlan Crow and Clarence Thomas.

Actions:

for civics enthusiasts,
Stay informed on the developments regarding Congress's legislative interest in ethical standards for the Supreme Court (implied).
</details>
<details>
<summary>
2023-05-25: Let's talk about Paxton's possible impeachment.... (<a href="https://youtube.com/watch?v=Miq8nJQKjco">watch</a> || <a href="/videos/2023/05/25/Lets_talk_about_Paxton_s_possible_impeachment">transcript &amp; editable summary</a>)

Recent events in Texas have brought forth the realistic possibility of impeaching the Attorney General, Paxton, following a Republican-led investigation uncovering numerous serious allegations against him.

</summary>

"A Republican-led investigation has uncovered numerous allegations against Paxton, including felonies."
"Allegations against Paxton include providing a sweetheart job for his partner and retaliating against whistleblowers."
"Paxton's response included calling the Republican Speaker of the House a liberal and accusing him of being drunk."

### AI summary (High error rate! Edit errors on video page)

Recent events in Texas have made the possibility of impeaching the Attorney General, Paxton, a realistic possibility.
A Republican-led investigation lasting four months has uncovered numerous allegations against Paxton, including felonies such as abuse of official capacity and misuse of public information.
Paxton is already under indictment for securities fraud and is being investigated by the FBI for bribery allegations stemming from whistleblowers' claims.
Allegations against Paxton also include providing a sweetheart job for his partner and retaliating against whistleblowers, leading to a significant settlement and subsequent investigation.
Despite previous scandals, this latest scandal involving Paxton's alleged wrongdoings may finally capture voters' attention in Texas.
Paxton responded by calling the Republican Speaker of the House a liberal and accusing him of being drunk.
Paxton's allies are faced with the options of ignoring the situation, censuring him, or impeaching him, but their response remains uncertain.
Lawmakers seem taken aback by the lengthy and detailed list of allegations, leaving them in a difficult position.
Paxton, a prominent figure in the GOP nationally, played a role in attempting to alter the outcome of the 2020 election for Trump.
Despite his influence, it is not inconceivable that other Republicans may take action against Paxton given the gravity of the allegations and his combative response.

Actions:

for texan voters,
Contact local representatives to express concerns about the allegations against Paxton (suggested)
Stay informed about the developments surrounding Paxton's situation and potential impeachment (implied)
</details>
<details>
<summary>
2023-05-25: Let's talk about DeSantis and a failure to launch.... (<a href="https://youtube.com/watch?v=9gYLTZceLj0">watch</a> || <a href="/videos/2023/05/25/Lets_talk_about_DeSantis_and_a_failure_to_launch">transcript &amp; editable summary</a>)

Governor DeSantis' lackluster presidential campaign launch hints at challenges in competing with Trump's shadow and style.

</summary>

"For an announcement of this size, that's a really bad sign."
"You can't out-Trump Trump."
"His only real hope is for Trump to no longer be in the race."
"The enthusiasm, 161,000 to 250,000 people. Those are not numbers that are going to be able to defeat Trump."
"This was his opening announcement. This was his launch."

### AI summary (High error rate! Edit errors on video page)

Governor of Florida launched his presidential campaign in a SpaceX-like event.
The launch faced technical difficulties and plummeted in listener numbers.
Despite Musk's promotion, the event had only 161,000 to 250,000 listeners.
DeSantis may try to spin the low numbers due to crashing Twitter servers.
Comparisons show DeSantis won't draw crowds like Trump did.
DeSantis is seen as a mini-Trump with a similar leadership style.
DeSantis' success depends on Trump not running or losing support.
DeSantis needs a significant shift in Trump's status for a chance at success.

Actions:

for political analysts,
Analyze campaign strategies realistically (suggested)
Monitor shifts in public sentiment towards political figures (suggested)
</details>
<details>
<summary>
2023-05-24: Let's talk about systems, deflections, and questions.... (<a href="https://youtube.com/watch?v=vhFLoklZ-TE">watch</a> || <a href="/videos/2023/05/24/Lets_talk_about_systems_deflections_and_questions">transcript &amp; editable summary</a>)

Addressing the question of why individuals with darker skin tones support fascist groups, Beau draws parallels between the acceptance of patriarchy and systems of oppression like white supremacy, illustrating how victims of oppressive systems can perpetuate them.

</summary>

"The system itself isn't about the system. The system is a means to an end, power, authority, authoritarianism."
"When it comes to systems of oppression, the goal isn't actually to keep a certain group of people down. That's not the end."
"It's not some historical anomaly. It occurs, and it occurs in other systems as well, all the time."

### AI summary (High error rate! Edit errors on video page)

Addressing a question that arises frequently: why do individuals with darker skin tones support fascist groups by displaying symbols like flags or tattoos?
Drawing parallels between the acceptance of patriarchy and the acceptance of systems of oppression like white supremacy.
Explaining how victims of oppressive systems can sometimes support and defend those systems due to fear of change and adherence to the status quo.
Mentioning individuals like Enrique Tario and Nick Fuentes as names associated with fascist movements in the US.
Noting that the concept of "whiteness" in the US is fluid and has evolved over time to include different ethnic groups for the benefit of maintaining power structures.
Emphasizing that systems of oppression are a means for certain groups to gain and maintain control by dividing people into in-groups and out-groups.
Stressing that the ultimate goal of oppressive systems is not to keep a specific group down but to consolidate power and authority for those in control.
Pointing out the link between individuals supporting oppressive systems and the perpetuation of authoritarianism.
Encouraging further exploration of racial hierarchies and systems of oppression beyond the US borders.
Concluding with the idea that individuals supporting oppressive systems, regardless of their background, are not anomalies but rather common occurrences.

Actions:

for social justice advocates,
Research racial hierarchies and systems of oppression beyond the US borders (suggested)
Have open, honest dialogues with individuals who may unknowingly support oppressive systems (implied)
</details>
<details>
<summary>
2023-05-24: Let's talk about phones, flags, fathers, and fact checks.... (<a href="https://youtube.com/watch?v=m206m4WOdo0">watch</a> || <a href="/videos/2023/05/24/Lets_talk_about_phones_flags_fathers_and_fact_checks">transcript &amp; editable summary</a>)

A fact-check on conspiracy theories involving satellite phones given to senators reveals routine security upgrades rather than a coup plot, urging early debunking requests.

</summary>

"If you are looking for a fact-check on some kind of wild theory that is spreading all over Facebook, send it to me right away."
"There's no big mystery behind the phones. No big mystery behind the flag. All of this stuff is super normal."
"It's dangerous to go along, here, take this."

### AI summary (High error rate! Edit errors on video page)

A viewer reached out about a conspiracy theory involving satellite phones given to senators for a coup attempt to keep Biden in office.
Beau spent 25 minutes fact-checking the claims and found the accurate information in just under 13 minutes.
Satellite phones were indeed given to some senators, but it was part of upgrading Senate security post-January 6th, not a nefarious plot.
The upgrade was voluntary, and only a limited number of senators opted for the phones.
The distribution of these phones is part of broader security upgrades and not connected to anything alarming.
Beau encourages sending wild theories for fact-checking to prevent spreading misinformation.
Addressing another issue, some believe a flag being laid out during an incident was staged, but it was likely done for documentation purposes by law enforcement.
Beau advises reading the full articles cited in such theories to understand the context and authority behind the information.

Actions:

for social media users,
Send wild theories for fact-checking promptly (suggested)
Read full articles cited in conspiracy theories for context (suggested)
</details>
<details>
<summary>
2023-05-24: Let's talk about Trump's chances of losing more cash.... (<a href="https://youtube.com/watch?v=OPr9swb7Wk4">watch</a> || <a href="/videos/2023/05/24/Lets_talk_about_Trump_s_chances_of_losing_more_cash">transcript &amp; editable summary</a>)

Beau clarifies legal entanglements in lawsuits against Trump, indicating potential significant financial repercussions for his actions.

</summary>

"Just another bad day for Trump."
"Every time he doubles down on it, the likelihood of him having to pay more increases."

### AI summary (High error rate! Edit errors on video page)

Explains the timeline involving a recent verdict in favor of E. Jean Carroll.
Mentions the first lawsuit tied up in court because Trump claims immunity as a former president.
Details the amendment to the first lawsuit to include information from the second lawsuit's verdict and a CNN town hall transcript.
Notes that Trump doubled down on his claims post-second lawsuit verdict, prompting Carroll's team to argue for an additional 10 million in damages.
Suggests Trump may end up paying between 15 and 25 million due to his actions.

Actions:

for legal analysts, political observers.,
Support organizations fighting for justice for victims of defamation and assault (implied).
</details>
<details>
<summary>
2023-05-24: Let's talk about Trump asking to speak to the regional manager.... (<a href="https://youtube.com/watch?v=ISWUZ2w_kD0">watch</a> || <a href="/videos/2023/05/24/Lets_talk_about_Trump_asking_to_speak_to_the_regional_manager">transcript &amp; editable summary</a>)

The special counsel's office nears a decision on Trump, who, in a panic-driven move, seeks a meeting with Garland amid mounting negative news and an impending trial date, with significant developments expected soon.

</summary>

"Is that justified? There has been a lot of news coming out little bits and pieces about evidence..."
"At the end of this, Jack Smith is a special counsel, okay."
"So he's been getting a whole lot of bad news lately."
"Undoubtedly we will have some interesting news when it comes to the documents case in the coming weeks."

### AI summary (High error rate! Edit errors on video page)

The special counsel's office is nearing a charging decision on the documents case involving Trump.
Trump requested a meeting with Attorney General Merrick Garland and representatives of Congress, a move often made when a client is anticipating indictment.
Recent news indicates evidence gathering related to potential trial rather than justifying charges in the documents case.
Trump's request for a meeting seems like a panic-driven move, given the mounting negative news and his upcoming trial date in New York.
Garland is unlikely to intervene in the special counsel's office's decision-making process, as seen in previous cases like Durham's investigation.
Trump's actions, like not heeding counsel's advice to stay quiet, are contributing to his current predicament.
Trump's belief that he will be indicted soon contrasts with public opinion on the matter.
The timing of Trump's trial date in the New York case during the primaries is unfavorable for him politically.
The recent developments suggest that there will be significant news regarding the documents case in the near future.

Actions:

for political analysts, legal experts,
Contact your representatives to advocate for transparency and accountability in legal proceedings (implied).
</details>
<details>
<summary>
2023-05-23: Let's talk about what happened in DC.... (<a href="https://youtube.com/watch?v=3C_m1vykNhM">watch</a> || <a href="/videos/2023/05/23/Lets_talk_about_what_happened_in_DC">transcript &amp; editable summary</a>)

Beau delves into the U-Haul incident near the White House, exposing right-wing deflection and the changing nature of political ideologies, urging critical self-reflection.

</summary>

"If you ever find yourself in a situation where the behavior of somebody who has one of those flags reflects poorly on you, there were a lot of poor decisions that were made along the way."
"The right wing in the United States has changed, and you can say that the left wing has changed too, that they've moved to more and more progressive positions."
"The people who fly that flag haven't changed."

### AI summary (High error rate! Edit errors on video page)

A U-Haul crashed into the barricades in Lafayette Park near the White House around 10 p.m.
Secret Service investigated the scene, and a robot was used to ensure the vehicle's safety.
Authorities found a flag in the U-Haul during the search.
Beau researched the incident but found right-wing commentators on Twitter spreading conspiracy theories.
Beau questions the shift in the right-wing's stance towards certain symbols and ideologies.
He recalls a time when politicians like George Bush Sr. distanced themselves from negative symbols and rhetoric.
The Republican Party's defense of certain symbols and ideologies has drastically changed over time.
Beau urges people to critically analyze their beliefs if they echo dangerous ideologies.
He points out the evolving nature of political ideologies, especially the right-wing becoming more authoritarian.
Beau concludes by noting that those defending certain symbols haven't changed amid political shifts.

Actions:

for activists, political observers,
Examine beliefs and ideologies to ensure alignment with values and ethics (implied).
</details>
<details>
<summary>
2023-05-23: Let's talk about running into Russia.... (<a href="https://youtube.com/watch?v=v3wFRTkH3qg">watch</a> || <a href="/videos/2023/05/23/Lets_talk_about_running_into_Russia">transcript &amp; editable summary</a>)

Beau explains recent events in Russia involving non-state actors, raising questions about sustainability and potential wider actions, stressing the internal security nature of the situation.

</summary>

"These were non-state actors. These were Russians."
"Welcome to the hard part."
"It's an internal security matter that sure, it's related to, spawned by, Russia's invasion of Ukraine, but it's not the Ukrainian military."

### AI summary (High error rate! Edit errors on video page)

Explains the recent events in Russia involving non-state actors moving into Russian territory and conducting raids.
Points out the potential downstream effects and uncertainties surrounding the situation.
Emphasizes that these non-state actors, although somewhat linked to Ukraine, are Russians or have Russian passports.
Compares the current situation in Russia to past instances where Russia took Ukrainian land with the involvement of non-state actors.
Raises questions about whether this is a sustained effort or just a diversion tactic.
Notes the risk of discontent in Russia leading to wider actions and the potential for similar movements in other regions.
Addresses the distinction between these non-state actors and the Ukrainian Armed Forces.
Comments on the military effectiveness of the situation, dependent on the level of discontent and potential organic actions.
Observes weaknesses in Russia's response and the expected increase in border security.
Mentions the demoralizing impact within Russia and the group's goal to depose Putin.
Emphasizes that these actions are not the same as Ukraine's military actions and are an internal security matter for Russia.
Acknowledges the uncertainties regarding the long-term implications of the events in Russia.

Actions:

for analysts, policymakers, activists,
Monitor developments in Russia and stay informed about the situation (suggested).
Engage in dialogues about the implications of non-state actors' actions in Russia (implied).
</details>
<details>
<summary>
2023-05-23: Let's talk about a different Republican Senator weighing in.... (<a href="https://youtube.com/watch?v=bf8mSrOK3z0">watch</a> || <a href="/videos/2023/05/23/Lets_talk_about_a_different_Republican_Senator_weighing_in">transcript &amp; editable summary</a>)

Republican Senator Cassidy and Senator Thune express concerns and potential resistance towards Trump and Trumpism within the Republican Party, citing Trump's challenges in a general election and his conduct post-acquittal.

</summary>

"Can't win a primary without Trump. Can't win a general with him."
"What former President Trump did to undermine faith in our election system and disrupt the peaceful transfer of power is inexcusable."
"Interesting developments and I feel like there might be more coming."

### AI summary (High error rate! Edit errors on video page)

Republican Senator Cassidy openly expressing concerns about Trump's ability to win a general election without winning swing states.
Cassidy, who has never been a fan of Trump, making a public statement about Trump's potential challenges in 2024.
Mention of Senator Thin's past statement criticizing Trump for undermining faith in the election system and disrupting the peaceful transfer of power.
Speculation about potential organized resistance against Trump and Trumpism within the Republican Party.
Senator Thune's odd behavior and similar views to Cassidy regarding Trump's conduct.
Thune's reasoning for voting to acquit Trump based on his belief that impeachment is primarily to remove the president, and since Trump was already gone, he voted to acquit.

Actions:

for political observers,
Stay informed on the evolving dynamics within the Republican Party (implied).
</details>
<details>
<summary>
2023-05-23: Let's talk about Biden's Colorado River deal.... (<a href="https://youtube.com/watch?v=nsNlOQKr6Bg">watch</a> || <a href="/videos/2023/05/23/Lets_talk_about_Biden_s_Colorado_River_deal">transcript &amp; editable summary</a>)

Beau explains the Colorado River deal, stressing state involvement for climate action and farmer support amidst political challenges.

</summary>

"All politics is local, right?"
"Your tradition will cost you your farm."
"They don't care about you. They never did."
"Make the environment a campaign issue."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Colorado River overuse necessitated a deal due to decreasing water levels.
Initially, states were reluctant to make cuts, leading to federal intervention.
Biden administration presented a plan, prompting states to negotiate amongst themselves.
Arizona, California, and Nevada agreed to significant cuts, conserving three million acre feet by 2026.
Importance of states handling the issue locally to drive climate change awareness and action.
Despite the temporary relief, deeper cuts are deemed necessary for long-term sustainability.
Farmers facing water cuts will receive compensation through the Inflation Reduction Act.
Emphasizes the impact of political decisions on individual livelihoods and traditional practices.
Urges people to recognize the role of legislation in supporting farmers during environmental transitions.
Encourages voters to prioritize environmental issues in local and state elections.

Actions:

for farmers, environmentalists, voters,
Contact local representatives to prioritize environmental issues in legislation (implied)
Support legislation that aids farmers in environmental transitions (implied)
Make environmental concerns a focal point in local and state elections (implied)
</details>
<details>
<summary>
2023-05-22: Let's talk about currency & companies and peas & carrots.... (<a href="https://youtube.com/watch?v=QRRuydIzSkY">watch</a> || <a href="/videos/2023/05/22/Lets_talk_about_currency_companies_and_peas_carrots">transcript &amp; editable summary</a>)

Beau explains the backing of the US dollar, supremacy of corporations, and soft colonialism, calling for a shift towards cooperation in economic systems.

</summary>

"We don't colonize with flags anymore. We colonize with corporate logos."
"The US dollar is backed up by that 800 billion dollar a year defense industry."
"It's dirty right? It's not really how we would want things."
"The wealth is extracted from the states that we colonize with corporate logos."
"It will stay this way until we move to a more cooperative rather than competitive."

### AI summary (High error rate! Edit errors on video page)

Explains the backing of the US dollar and terms like soft colonialism.
Addresses the supremacy of US corporations and how they outcompete others.
Talks about legal mechanisms and incentives that benefit US corporations.
Mentions the extraction of wealth through corporate colonization.
Provides insights into the US defense industry's role in maintaining the dollar's backing.
Acknowledges the not-so-ideal practices in the system and the need for understanding to bring change.
Emphasizes the prevalence of such practices in the West.
Calls for a shift towards cooperation over competition in economic systems.
Offers a critical perspective on global economic structures and their implications.
Encourages reflection on the current state of affairs and the potential for improvement.

Actions:

for economic justice advocates,
Challenge exploitative economic systems (implied)
Advocate for fair trade practices (implied)
Support policies that prioritize cooperation over competition (implied)
</details>
<details>
<summary>
2023-05-22: Let's talk about Senator Thune's unexpected endorsement.... (<a href="https://youtube.com/watch?v=FjospeAKCXw">watch</a> || <a href="/videos/2023/05/22/Lets_talk_about_Senator_Thune_s_unexpected_endorsement">transcript &amp; editable summary</a>)

Senator Thune's unusual endorsement of Tim Scott prompts speculation on early positioning or genuine belief, indicating broader implications for Republican leadership dynamics.

</summary>

"His choice is odd."
"Endorsements are political decisions, not principled ones."
"It's just a weird move but it's worth paying attention to."

### AI summary (High error rate! Edit errors on video page)

Senator Thune's endorsement choice is odd and notable.
Endorsements are typically political, not principled decisions.
Senator Thune is endorsing Tim Scott, a Black candidate.
Thune's endorsement of Scott may indicate early positioning or genuine belief in Scott's capabilities.
Thune's decision to not endorse Trump or other lead candidates is significant.
Watching the moves of Senate Republicans close to leadership for endorsements can reveal insights.
Thune's early and unusual endorsement suggests potential positioning.
Uncertainty exists whether Thune is unaware of the Republican Party's racism or truly supports Scott.
Thune's endorsement timing is peculiar and could be strategic.
Thune's endorsement may have broader implications and is worth observing.

Actions:

for political observers,
Watch the moves of Senate Republicans close to leadership for potential insights (suggested)
</details>
<details>
<summary>
2023-05-22: Let's talk about Biden warming to the 14th Amendment.... (<a href="https://youtube.com/watch?v=kqv8h8JsdgE">watch</a> || <a href="/videos/2023/05/22/Lets_talk_about_Biden_warming_to_the_14th_Amendment">transcript &amp; editable summary</a>)

Biden's potential shift towards using the 14th Amendment to address the debt ceiling crisis signals a willingness to take risks for the economy's sake, amid Republican budget proposals that could harm their own base.

</summary>

"He might be willing to take that risk."
"The longer the Republican Party holds this up, the less Biden has to worry about."
"The economy goes down because a bunch of judges that Republicans appointed couldn't read the Constitution."
"The current Republican budget would damage the economy, probably more than a default."
"They may end up catching the car on this one, too."

### AI summary (High error rate! Edit errors on video page)

Explains Biden's shift towards considering using the 14th Amendment to address the debt ceiling issue.
Outlines the potential consequences of invoking the 14th Amendment, including legal challenges and economic uncertainty.
Describes Biden's initial reluctance to take this action to maintain a sense of normalcy and stability.
Points out that with the looming threat of default, Biden may now be more willing to take bold actions.
Suggests a hypothetical scenario where Biden delegates authority under the 14th Amendment to ensure U.S. debts are paid.
Speculates on the potential reactions and consequences of such a move, including political and economic impacts.
Emphasizes the importance of honoring the Constitution and avoiding a default scenario.
Raises concerns about the current Republican budget proposal and its potential negative impact on the economy.
Criticizes the Republican Party for sticking to outdated talking points and policies that may harm their own base.
Concludes with a cautionary note about the consequences of pursuing certain policies without considering their real-world effects.

Actions:

for policy analysts, political commentators,
Contact political representatives to urge them to prioritize economic stability and responsible budgeting (implied).
Join advocacy groups focused on fiscal policy and government spending to stay informed and push for prudent financial decisions (implied).
</details>
<details>
<summary>
2023-05-22: Let's talk about 5 concepts and political commentary.... (<a href="https://youtube.com/watch?v=HvvudAZtv8I">watch</a> || <a href="/videos/2023/05/22/Lets_talk_about_5_concepts_and_political_commentary">transcript &amp; editable summary</a>)

Exploring objectivity in journalism and commentary, Beau challenges the notion of treating both parties equally when one adopts objectively bad principles.

</summary>

"Bigotry is objectively bad."
"Denying people equal protection under the law is bad."
"Objectivity is not both sides in something that doesn't have two sides."
"Treating it as an equal when it's not."
"One party adopted a political platform and demonstrated through their actions that their route is objectively bad."

### AI summary (High error rate! Edit errors on video page)

Exploring the concept of objectivity in journalism and commentary, acknowledging the challenge of true objectivity due to unconscious biases.
Proposing five concepts to ponder on whether they are objectively good or bad, such as bigotry and authoritarianism.
Asserting that denying people equal protection under the law, curtailing free speech, and favoring one religion over another are objectively bad.
Questioning if objectivity means treating both political parties equally when one has embraced objectively bad principles.
Arguing that objectivity does not equate to treating objectively bad actions as equal and showing favoritism to something detrimental.
Emphasizing the importance of recognizing when one party's actions are objectively bad and not treating both sides as equal for the sake of false impartiality.

Actions:

for journalists, commentators, readers,
Recognize and challenge objectively bad actions in political platforms (implied)
Advocate for fairness and truth in reporting (suggested)
</details>
<details>
<summary>
2023-05-21: The roads to a May 2023 Q&A.... (<a href="https://youtube.com/watch?v=h_U9x1c8MFE">watch</a> || <a href="/videos/2023/05/21/The_roads_to_a_May_2023_Q_A">transcript &amp; editable summary</a>)

Beau answers Patreon questions on a variety of topics, sharing personal experiences, advice, and insights in a relatable and engaging manner.

</summary>

"Allow yourself to experience the feelings."
"Every firearm is lethal."
"It gets easier."
"Do what you do best."
"Experience the feelings."

### AI summary (High error rate! Edit errors on video page)

Answers Patreon questions in a Q&A session for the channel.
Addresses various topics such as border situations, AI advancements, historical events, personal stories, and advice on diverse subjects.
Expresses views on political parties, military strategies, community networking, grief, time management, and team roles.
Shares anecdotes, opinions, and advice with a mix of seriousness and humor.
Provides insights, reflections, and personal experiences in response to the questions.
Shows a down-to-earth approach and willingness to connect with the audience.
Balances informative responses with engaging storytelling.
Offers practical and thoughtful advice on different situations and dilemmas.
Demonstrates a wide range of knowledge and experiences in his responses.
Engages with the audience in a conversational and relatable manner.

Actions:

for creators, learners, curious minds.,
Join or support existing mutual aid groups ( suggested ).
Get involved in disaster relief efforts ( exemplified ).
Take time to understand different perspectives and widen your view ( exemplified ).
</details>
<details>
<summary>
2023-05-21: Let's talk about what Republicans are saying about Trump's legal exposure.... (<a href="https://youtube.com/watch?v=RTwXkxJ9Iuk">watch</a> || <a href="/videos/2023/05/21/Lets_talk_about_what_Republicans_are_saying_about_Trump_s_legal_exposure">transcript &amp; editable summary</a>)

Beau explains Republican views on Trump's document situation, Bill Barr's warning of trouble, and vested interests in Trump's exoneration.

</summary>

"It's very clear that he had no business having those documents."
"Barr stood by Trump through a lot. A whole lot. A whole lot that I personally don't think he should have. Why is he saying this? Because it's true."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explaining what Republicans are saying about Trump's situation regarding the documents.
Bill Barr's comments on Trump's exposure in the documents case, indicating a bad sign.
Barr's assertion that Trump had no business keeping the documents and could be in trouble for not returning them.
Speculation on potential consequences if games were played after the documents were requested back.
Noting that Barr, who stood by Trump through a lot, is now speaking the truth about the situation.
Linking individuals supporting Trump's re-election and claiming he has no criminal liability to a vested interest in Trump returning to office.
Pointing out the risk of individuals denying Trump's wrongdoing being implicated in activities surrounding the sixth.
Mentioning that those not understanding or blindly supporting Trump may have a different perspective.
Observing that individuals trying to spin the situation to defend Trump often have their names linked to the events of the sixth.
Implying a motive for those trying to ensure Trump does not implicate them in any potential wrongdoing.

Actions:

for interested viewers,
Question individuals supporting Trump's exoneration and their potential vested interests (implied).
Stay informed about the unfolding events and statements made by key figures (exemplified).
</details>
<details>
<summary>
2023-05-21: Let's talk about how there are no red states.... (<a href="https://youtube.com/watch?v=J1VT-4t6s3Y">watch</a> || <a href="/videos/2023/05/21/Lets_talk_about_how_there_are_no_red_states">transcript &amp; editable summary</a>)

Beau breaks down the myth of red states, urging the Democratic Party to focus on voter turnout and enthusiasm to flip traditionally red states like Texas blue.

</summary>

"Red states don't exist the way people think they do."
"Friends don't let friends vote alone."
"You can't win here. Yeah, you can."

### AI summary (High error rate! Edit errors on video page)

Received two messages on messaging and red states in response to a video about the Democratic Party's messaging.
One message questioned the ability to win in red states like Texas.
Ran the numbers for Texas, Alabama, and West Virginia, finding no statistically red states.
Used Texas as an example: Abbott won with 4.4 million votes, O'Rourke had 3.5 million.
Democrats need to focus on getting their voters to show up and create enthusiasm.
There are roughly 10 million voters up for grabs in Texas.
Democratic Party needs to run candidates that people in the state want to vote for.
The idea of red states is self-reinforcing, causing people not to show up to vote.
Enthusiasm and voter turnout are key for Democrats to win in places like Texas.
Creating messaging to encourage people to vote and take others to the polls is vital.
If Democrats flip Texas blue, it can have a significant impact on national politics.
The Republican Party will take notice if Texas turns blue during a national election.
There are enough Democratic voters in Texas to beat a statewide Republican candidate.
Focus on progress and getting Democratic voters to show up is key.

Actions:

for voters, democratic party members,
Mobilize voters to show up at the polls (suggested)
Create messaging campaigns to encourage voter turnout (suggested)
Take friends and family to vote (suggested)
</details>
<details>
<summary>
2023-05-21: Let's talk about Santos from 3 views.... (<a href="https://youtube.com/watch?v=YDOPjVp4ol8">watch</a> || <a href="/videos/2023/05/21/Lets_talk_about_Santos_from_3_views">transcript &amp; editable summary</a>)

Democratic attempt to oust George Santos fails, Republicans use a motion to refer Santos to Ethics Committee, Independents find expulsion pointless, and Republicans defend their party's stance on indicted candidates, showcasing divides in perspectives.

</summary>

"They're leading candidate for the presidential nomination is currently under indictment."
"They will absolutely send a message saying that there's no way their party would support somebody who's being indicted."
"The messages reveal divides among those who are very idealistic, those who are very pragmatic, and those who have fallen into an echo chamber."

### AI summary (High error rate! Edit errors on video page)

Democratic attempt to oust George Santos failed.
Republicans turned it into a motion to refer Santos to the Ethics Committee.
Santos gets to stay in office during a long drawn-out process.
Republicans can claim they held one of their party members accountable.
Independent voice finds expelling Santos pointless.
Independent questions the concept of a "real independent."
Republican message defends their party against accusations of not caring about principles.
Republicans show support for a candidate under indictment, citing polling data.
Symbolic votes can backfire, as Democrats were outmaneuvered in this case.
Independents may be more easily swayed by criminal indictments.
Republicans may ignore facts and logic in favor of base emotions and feelings.
Calling out Republican hypocrisy may not be effective due to their disregard for new information.
The messages reveal divides among idealistic, pragmatic, and echo chamber individuals.
The dynamics show a failed attempt at accountability, differing perspectives on expelling Santos, and challenges in reaching Republicans effectively.

Actions:

for political observers,
Analyze polling data to better understand voter perspectives (implied).
Engage in meaningful dialogues with individuals holding different political views (implied).
</details>
<details>
<summary>
2023-05-20: Let's talk about TikTok, Montana, and Constitutions.... (<a href="https://youtube.com/watch?v=LT_-QVSEpSM">watch</a> || <a href="/videos/2023/05/20/Lets_talk_about_TikTok_Montana_and_Constitutions">transcript &amp; editable summary</a>)

Beau explains freedom of speech rights in relation to TikTok, citing constitutional amendments and a lawsuit brought by creators against Montana's state laws.

</summary>

"No law shall be passed impairing the freedom of speech or expression."
"States that state governments cannot abridge your freedom of speech."

### AI summary (High error rate! Edit errors on video page)

Explains a message received from someone in Montana criticizing his views on freedom of speech regarding TikTok.
Quotes the 14th Amendment to the US Constitution to support his argument that states are bound by the Bill of Rights.
Mentions Montana's state constitution also includes a freedom of speech clause.
Talks about a freedom of speech lawsuit filed by five TikTok creators against the state.
The creators argue that Montana lacks the authority to dictate foreign policy or national security interests.
The attorney general in Montana is ready to defend the legislation in court.
Beau predicts that federal government intervention might preempt some issues but court cases will still proceed.
States that state governments cannot limit freedom of speech.
Overall, the issue revolves around the conflict between state laws and individual freedom of speech rights.

Actions:

for constitution enthusiasts,
Contact legal aid organizations for guidance on constitutional rights and freedom of speech (implied).
</details>
<details>
<summary>
2023-05-20: Let's talk about Spartacus still giving lessons for those on the bottom.... (<a href="https://youtube.com/watch?v=g56jBSepS4I">watch</a> || <a href="/videos/2023/05/20/Lets_talk_about_Spartacus_still_giving_lessons_for_those_on_the_bottom">transcript &amp; editable summary</a>)

Beau talks about unity, economy, and Spartacus, showcasing how diversity doesn't hinder unification, but bigotry does, warning against falling for divisive tactics.

</summary>

"Bigotry is bad for the economy. Period. Full stop. It always is."
"Diversity doesn't stop people from unifying. Bigotry does."
"Unions are diverse. Spartacus's army was diverse."
"Those at the top will try to exploit bigotry to keep people divided."
"You have more in common with the black guy down the road than with your white representative up in DC."

### AI summary (High error rate! Edit errors on video page)

Addresses unity, economy, and Spartacus in response to a message received.
Mentions the relationship between losing money and attracting investment.
Talks about diverse workforce discouraging unions, referencing Jeff Bezos.
Draws parallels between Spartacus' slave revolt and modern-day diversity.
Argues that bigotry is bad for the economy in a capitalist society.
Emphasizes that diversity does not stop unification, but bigotry does.
Criticizes the exploitation of bigotry by those in power to keep people divided.
Encourages unity across diverse backgrounds rather than falling for divisive tactics.
Challenges the idea of having more in common with people of similar backgrounds rather than those in positions of power.
Concludes with a message against falling for tactics that exploit bigotry.

Actions:

for people seeking to understand the importance of unity and diversity in society.,
Challenge divisive rhetoric and encourage unity in your community (implied).
Support diverse workplaces and advocate against discrimination (implied).
</details>
<details>
<summary>
2023-05-20: Let's talk about Russian scientists and treason.... (<a href="https://youtube.com/watch?v=dNdQvfS7PAU">watch</a> || <a href="/videos/2023/05/20/Lets_talk_about_Russian_scientists_and_treason">transcript &amp; editable summary</a>)

Three Russian scientists were arrested, sparking rumors of intelligence leaks and potentially causing a chilling effect on the scientific community.

</summary>

"Russia is creating a situation, whether they realize it or not, where they're going to suffer even more brain drain."
"It's just something to be aware of in case there are more scientists who wind up getting arrested."
"That is not something that encourages people to go into scientific research."

### AI summary (High error rate! Edit errors on video page)

Three Russian scientists were arrested, reportedly due to issues with Russia's hypersonic missile system.
Rumors are circulating about the reasons behind the arrests, including potential intelligence leaks to other countries.
The closed nature of treason cases in Russia means the truth may never be fully revealed.
The situation may have a chilling effect on the Russian scientific community, deterring future researchers.
Russia's actions could lead to brain drain as researchers seek opportunities in other countries.
Other nations may benefit from Russia's missteps by attracting Russian scientists to work for them.

Actions:

for policy makers, scientists, researchers,
Contact organizations supporting scientists' rights (suggested)
Stay informed about developments in the Russian scientific community (implied)
Offer support and opportunities to Russian scientists seeking to work abroad (implied)
</details>
<details>
<summary>
2023-05-20: Let's talk about 1.5 degrees and the climate.... (<a href="https://youtube.com/watch?v=4OxdIgcbxS4">watch</a> || <a href="/videos/2023/05/20/Lets_talk_about_1_5_degrees_and_the_climate">transcript &amp; editable summary</a>)

Beau explains the urgency of addressing climate change as global temperatures approach 1.5 degrees Celsius, warning of severe impacts and the need for immediate action to prevent irreversible consequences.

</summary>

"The impacts from this, they're going to be severe."
"The climate is not a car. When you slam on the brakes, it's not going to stop."
"This isn't about being fair. It's not a board game."
"By the time you see it, it's too late."
"We really need to get the ball on this one."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of 1.5 degrees Celsius in relation to climate change.
Warns about the likelihood of the average global temperature exceeding 1.5 degrees Celsius by 2027.
Emphasizes the increased risks of wildfires, food shortages, droughts, heat waves, and flooding at 1.5 degrees Celsius.
Notes the international community's goal of limiting climate change to 1.5 degrees Celsius and the current lack of progress.
Calls for climate action to be a prominent campaign issue in elections.
Stresses the urgent need for aggressive action in transitioning to cleaner energy, altering food consumption, and preparing for future human migration.
Acknowledges the disproportionate impacts of climate change and the necessity for the United States to take significant action.
Compares addressing climate change to a semi-truck rather than a car, indicating the urgency of action.
Urges immediate and continuous attention to climate change as delays will have severe consequences.
Warns that waiting until the effects of climate change are apparent will be too late.

Actions:

for climate advocates, policymakers,
Advocate for climate action in local and national political campaigns (implied)
Support and push for aggressive transition to cleaner energy sources (implied)
Raise awareness about the urgent need for climate action in communities (implied)
</details>
<details>
<summary>
2023-05-19: Let's talk about planes, runways, and Ukraine.... (<a href="https://youtube.com/watch?v=TKEcs-q49Qs">watch</a> || <a href="/videos/2023/05/19/Lets_talk_about_planes_runways_and_Ukraine">transcript &amp; editable summary</a>)

Ukraine's potential acquisition of F-16s prompts questions about runway readiness and Ukrainian military's adaptability, challenging doubts on logistical capabilities.

</summary>

"The Ukrainian military has demonstrated time and again that they're adaptable."
"I see no reason to believe that we won't be wrong this time as well."
"I don't have any reason to believe that this time will be different."

### AI summary (High error rate! Edit errors on video page)

Ukraine expressed the need for air power, particularly the F-16, with the Biden administration showing some reluctance but not defiance.
European powers require U.S. approval to provide F-16s to Ukraine since it is a U.S. design, and this approval seems likely.
Getting F-16s does not guarantee air superiority for Ukraine, but it will help level the playing field.
Concerns have been raised about Ukrainian runways being inadequate for F-16s due to debris, length, and other issues.
There is uncertainty about whether the runway situation is a minor performance issue or a major problem.
Beau questions if the high standards set by the U.S. military are necessary or if they could adapt like the Ukrainian military.
Beau suggests hearing from specialized Air Force construction units like Red Horse and CVs to understand the runway preparation process.
He expresses confidence in the Ukrainian military's adaptability and problem-solving abilities based on past performance.
Beau recalls instances where commentators doubted Ukrainian capabilities but were proven wrong, indicating potential success in handling F-16 logistics.
He prompts the audience to think about the runway preparation process and its potential challenges compared to managing logistics for other military equipment.

Actions:

for military analysts, policymakers,
Contact specialized Air Force construction units like Red Horse and CVs for insights on runway preparation (suggested)
Support efforts to clear debris or resurface runways for potential F-16 deployment (implied)
Stay informed about the ongoing developments regarding Ukraine's airpower needs and potential F-16 acquisition (implied)
</details>
<details>
<summary>
2023-05-19: Let's talk about expanding confidence in SCOTUS.... (<a href="https://youtube.com/watch?v=2qP0uPb87r4">watch</a> || <a href="/videos/2023/05/19/Lets_talk_about_expanding_confidence_in_SCOTUS">transcript &amp; editable summary</a>)

Democratic lawmakers reintroduce legislation to expand the Supreme Court by four seats amidst low public confidence and concerns about the far-right majority's control until 2065, aiming to address ethical issues and unique rulings.

</summary>

"The justice system is broke."
"This is something that honestly the Democratic Party needs."
"A third of the country has hardly any confidence."
"Those people who want reproductive rights would get behind that."
"The odd thing is I don't think that's why it's being proposed."

### AI summary (High error rate! Edit errors on video page)

Democratic lawmakers reintroduced legislation to expand the Supreme Court by four seats, bringing the total number of justices up to 13, the same as the number of federal appellate courts.
Motivations for expansion include ethical questions, unique rulings not in line with history, and concerns about the current far-right majority retaining control until 2065.
Recent polling shows low confidence in the Supreme Court, with only 18% having a great deal of confidence, and a third of the country having hardly any confidence.
Altering the institution might be necessary to protect it, especially with repeated attacks on institutions by one political party.
While Beau doesn't foresee the expansion passing soon, he believes many running in 2024 will have to address how they plan to vote on it.
Beau suggests that candidates supporting expanding the Supreme Court may receive a positive response, especially considering the low confidence levels in the court.
Despite generally disliking messaging votes, Beau sees expanding the Supreme Court as something that needs to be done and likely wanted by the majority of Americans.
The issue could become a unifying campaign topic for the Democratic Party, energizing various demographics like supporters of gun control, reproductive rights, conservationists, and younger people whose rights are under attack.
Beau believes that while the proposal may fail initially, it could motivate people to give the Democratic Party a majority in the House and Senate to eventually pass it.
Beau concludes by mentioning the broken justice system and leaves with a thought for the day.

Actions:

for politically engaged individuals,
Mobilize support for expanding the Supreme Court through community outreach and education (implied)
Advocate for reforming the justice system to address issues of legitimacy and public confidence (implied)
</details>
<details>
<summary>
2023-05-19: Let's talk about SCOTUS and Naperville.... (<a href="https://youtube.com/watch?v=t4Fyzru8tJM">watch</a> || <a href="/videos/2023/05/19/Lets_talk_about_SCOTUS_and_Naperville">transcript &amp; editable summary</a>)

The Supreme Court's denial of an injunction on an assault weapons ban in Naperville prompts caution rather than celebration as ongoing legal battles loom.

</summary>

"The ban will be enforced as it makes its way through the court system but the court battles are nowhere near over."
"I wouldn't go around claiming victory on this just yet."
"So if you're supportive of this more of a breather than a victory."
"Anyway it's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the recent resolution of a case related to assault weapons and high-capacity magazines in Naperville.
Mentions the case reaching the emergency docket, also known as the shadow docket, of the Supreme Court.
Notes the expectation for the Supreme Court to issue an injunction to halt enforcement of the ban, but that did not happen.
Justice Barrett denied the application for a writ of injunction pending appeal without providing an explanation, common for the emergency docket.
Warns against prematurely claiming victory, as the case could still return to the Supreme Court as it progresses through the legal system.
Points out the Supreme Court's recent trend of avoiding major decisions via the emergency docket, possibly indicating a cautious approach.
Emphasizes that while many groups are celebrating the denial of the injunction, it's premature to declare a major victory.
Indicates the likelihood of continued pro-gun rulings from the Supreme Court, even if not through the emergency docket.
Advises against assuming the case is over, as the ban will still be enforced during the legal process, with potential ongoing court battles ahead.
Suggests that the general tendency of the court, based on previous decisions, leans towards supporting the gun owner, so any relief should be seen as temporary rather than definitive.

Actions:

for legal observers, gun control advocates,
Stay informed about the progress of the case and any future legal developments (implied).
</details>
<details>
<summary>
2023-05-19: Let's talk about Disney's non-move.... (<a href="https://youtube.com/watch?v=XDKKjwezOeo">watch</a> || <a href="/videos/2023/05/19/Lets_talk_about_Disney_s_non-move">transcript &amp; editable summary</a>)

Disney's decision not to proceed with a project in Florida showcases the massive economic repercussions and long-lasting impacts on regions, reflecting the broader consequences of political climates on business decisions.

</summary>

"It is cheaper to be a good person."
"When Disney puts something somewhere, it stays."
"The amount of money that is lost because of it is huge."
"These decisions have long-term impacts for an area that is economically disadvantaged."
"Companies that look towards the future, institutions that look towards the future, they're not going to want to be there."

### AI summary (High error rate! Edit errors on video page)

Disney decided not to move forward with a project that involved putting 2,000 Imagineers in Florida, leading to significant economic implications.
Speculation surrounds whether Disney's decision was influenced by political factors in Florida, but there is no concrete evidence to support this.
The project cancellation will result in a loss of $200 million annually from the economy due to the high salaries of the Imagineers.
The economic impact goes beyond just salaries, including the money spent by the company in the area and the ripple effects on local businesses.
The decision not only affects the employees but also has repercussions on various other aspects like landscapers, truckers, and overall economic activity in the region.
The long-lasting effects of this decision will result in billions of dollars not being pumped into Florida's economy.
Similar situations, with less attention, occur frequently, impacting regions without making headlines.
Beau mentions a personal connection to a situation where a project was diverted from Florida to Georgia, Minnesota, or Maine based on future political conditions.
These decisions have substantial long-term impacts on economically disadvantaged areas, causing significant financial losses.
Companies make decisions based on future-oriented policies, and regions that do not adapt risk losing out on profitable ventures.

Actions:

for economic policymakers,
Advocate for policies that support future-oriented companies and attract profitable ventures (implied)
Support economically disadvantaged areas facing substantial financial losses (implied)
Stay informed about the economic impacts of business decisions influenced by political climates (implied)
</details>
<details>
<summary>
2023-05-18: Let's talk about the other new Rudy suit.... (<a href="https://youtube.com/watch?v=eS_Dk0rocYA">watch</a> || <a href="/videos/2023/05/18/Lets_talk_about_the_other_new_Rudy_suit">transcript &amp; editable summary</a>)

Beau examines a new lawsuit against Rudy Giuliani, discussing false arrest, viral video evidence, and the ongoing legal challenges against Giuliani.

</summary>

"Our client merely patted Mr. Giuliani, who sustained nothing remotely resembling physical injuries."
"Giuliani apparently was unaware of the footage."
"It seems like there's a whole lot of finding out going on right now."

### AI summary (High error rate! Edit errors on video page)

Rudy Giuliani is facing a new lawsuit brought in federal court by Daniel Gill for false arrest, civil rights conspiracy, defamation, and emotional distress.
Gill's lawyer claims his client patted Giuliani on the back without malice to get his attention, as seen in viral video footage.
Giuliani described the incident as feeling like he'd been shot or hit by a boulder.
The lawsuit not only targets Giuliani but also the city and some police officers involved.
Giuliani filed charges against Gill after the incident, leading to Gill's arrest.
The lawsuit is likely to proceed swiftly due to the video evidence available.
Multiple lawsuits are being brought against Giuliani currently, indicating a trend of legal actions against him.
The outcome of these lawsuits will depend on court decisions and potentially jury rulings.
The situation involving Giuliani and these legal challenges is ongoing and may continue in the future.
Beau concludes by expressing his thoughts and wishing everyone a good day.

Actions:

for legal observers,
Watch the viral video footage to understand the context of the incident (suggested).
Stay informed about the developments in the legal cases involving Rudy Giuliani (suggested).
</details>
<details>
<summary>
2023-05-18: Let's talk about the new Trump evidence.... (<a href="https://youtube.com/watch?v=fFABLwBMaWY">watch</a> || <a href="/videos/2023/05/18/Lets_talk_about_the_new_Trump_evidence">transcript &amp; editable summary</a>)

The evidence handed over to the special counsel's office could point towards potential serious charges, not just investigating willful retention, with unclear motives behind it.

</summary>

"They're looking for a more severe charge, and that may be why it is taking a little bit longer than expected."
"There's not a real logical reason for them trying to get this."
"This seems like it's probably them looking at a more serious charge or they're getting ready for court."
"I don't think it will go anywhere."
"Sure, maybe they're just making it airtight, but I think the more likely answer is that they're looking for a more severe charge."

### AI summary (High error rate! Edit errors on video page)

The National Archives plans to hand over 16 records involving close presidential advisors to the special counsel's office, prompting potential serious charges or trial preparation.
Special Counsel's office may be looking at a more severe charge or preparing for court based on the evidence, not just investigating willful retention.
Willful retention involves national defense information, not just classified information, and evidence may not be necessary for the charges initially assumed.
Trump's claim of having a standing order to declassify anything he takes from a room is not valid and may not hold up in court.
Special Counsel's office may be trying to show Trump intentionally removed certain records and looking for a motive behind it.
The evidence appears to be unnecessary for the charges expected from the documents case, pointing towards potential serious charges.
Trump has the chance to fight this in court, but it may not be successful.
There might be more surprises in this case as the reason behind obtaining this evidence seems unclear.

Actions:

for legal analysts, political commentators,
Prepare for potential legal proceedings by staying informed on the developments (implied)
Stay updated on the case to understand the implications of the evidence being handed over (implied)
</details>
<details>
<summary>
2023-05-18: Let's talk about TikTok and bad information.... (<a href="https://youtube.com/watch?v=ToJck_yekKA">watch</a> || <a href="/videos/2023/05/18/Lets_talk_about_TikTok_and_bad_information">transcript &amp; editable summary</a>)

Montana's TikTok ban sparked misinformation through intentionally misleading screenshots, urging caution and verification.

</summary>

"The screenshot intentionally misrepresented the information."
"Be cautious with screenshots from mobile devices with banners."
"Verify information before reacting to misleading screenshots."

### AI summary (High error rate! Edit errors on video page)

Montana banned TikTok, sparking First Amendment arguments for residents and the company.
A screenshot shared falsely claimed Montana residents faced penalties for using TikTok.
The inaccurate information in the screenshot was attributed to Politico.
Beau verified the screenshot's text on Google and found a corresponding Politico article.
The actual article did not match the misleading screenshot.
The screenshot intentionally misrepresented the information, possibly to stir drama or alter perceptions.
Beau warns about being cautious with screenshots from mobile devices with banners.
The new technique involves altering screenshots to create misinformation.
Beau encourages people to verify information before reacting to misleading screenshots.
Despite the controversy, Beau plans to continue following the TikTok ban situation in Montana.

Actions:

for social media users,
Verify information before sharing screenshots (implied)
Be cautious with mobile device screenshots containing banners (implied)
</details>
<details>
<summary>
2023-05-18: Let's talk about Democratic messaging issues.... (<a href="https://youtube.com/watch?v=DSNdeBznWtU">watch</a> || <a href="/videos/2023/05/18/Lets_talk_about_Democratic_messaging_issues">transcript &amp; editable summary</a>)

Beau explains the Democratic Party's messaging struggles, attributing them to diverse subgroups, high-minded ideals, and a lack of local focus.

</summary>

"The Democratic Party struggles with messaging due to its diverse coalition of subgroups."
"Democrats tend to focus on high-minded ideas, which can backfire when attacked by the opposition."
"The Republican Party motivates through fear, projecting strength rather than engaging in high-minded debates."
"The Democratic Party struggles to explain popular policies in a simple, relatable manner."
"Beau points out the importance of understanding local politics and tailoring messaging accordingly."

### AI summary (High error rate! Edit errors on video page)

The Democratic Party struggles with messaging due to its diverse coalition of subgroups.
The party is filled with politicians who may be out of touch with average Americans.
The PR teams within the Democratic Party are often ideologically motivated and may lack relatability.
Newer figures like Katie Porter and AOC are successful in messaging due to their effective use of social media and simple explanations.
The Democratic Party sometimes fails to tailor its messaging to different regions and demographics.
Democrats tend to focus on high-minded ideas, which can backfire when attacked by the opposition.
The Republican Party motivates through fear, projecting strength rather than engaging in high-minded debates.
Unaffiliated PACs handle the negative attacks that the Democratic Party avoids due to their high-minded approach.
The Democratic Party struggles to explain popular policies in a simple, relatable manner.
Beau points out the importance of understanding local politics and tailoring messaging accordingly.

Actions:

for democratic strategists,
Break down policies in a simple, relatable manner (suggested)
Tailor messaging to different regions and demographics (suggested)
Utilize social media and simplified explanations for effective messaging (exemplified)
</details>
<details>
<summary>
2023-05-17: Let's talk about the find out stage and readiness.... (<a href="https://youtube.com/watch?v=Kqi_wbk2YGw">watch</a> || <a href="/videos/2023/05/17/Lets_talk_about_the_find_out_stage_and_readiness">transcript &amp; editable summary</a>)

Legislation in red states negatively impacts military readiness, risking closures of bases and economic losses, hurting the military.

</summary>

"Legislation in red states negatively impacts military readiness."
"Y'all did this. You're literally hurting the military."
"It's not a political thing. It's because you're literally hurting the military."

### AI summary (High error rate! Edit errors on video page)

Legislation in red states negatively impacts military readiness by hindering the force's ability to do its job.
Bases in red states are affected when the state targets certain demographics, leading to closures and economic losses.
Space Force headquarters was supposed to be at Redstone in Alabama but is now likely to be in Colorado.
The restrictive laws in Alabama regarding reproductive rights and targeting the LGBTQ community may have influenced the decision.
Installations are selected based on a checklist that includes factors like room for growth, proximity to airports, schools, and healthcare access.
Laws that were not in place when Huntsville was selected are now impacting decisions regarding military installations.
Bases in states with restrictive laws may never be selected again due to the negative impact on readiness.
Lack of investment in states with such restrictions will continue until the laws change, as it affects recruitment, retention, and quality of life.
Politicians using wedge issues like these laws have consequences that hurt the military in the long run.
The economic activity around military installations is at risk when bases close due to legislative impacts.

Actions:

for military advocates, policymakers,
Advocate for changes in restrictive laws impacting military installations (implied)
</details>
<details>
<summary>
2023-05-17: Let's talk about polling on why people are leaving.... (<a href="https://youtube.com/watch?v=FXhClIvx_ng">watch</a> || <a href="/videos/2023/05/17/Lets_talk_about_polling_on_why_people_are_leaving">transcript &amp; editable summary</a>)

Beau analyzes polling data on changing religious beliefs, attributing shifts to losing faith and negative treatment of the LGBTQ community, revealing a significant increase in affiliation changes.

</summary>

"The reason the pews are becoming more and more empty is because a lot of people have decided that it is easier to preach hate rather than preach love."
"There's numbers to back it up."
"It's cheaper to be a good person."

### AI summary (High error rate! Edit errors on video page)

Explains about polling data indicating a significant increase in the percentage of people changing their religious affiliation.
Notes that 24% of individuals in the United States have changed their beliefs over their lifetime, a 50% increase from 2020.
States that the primary reason for this change is losing faith, accounting for 56% of cases.
Points out that negative teachings or treatment of the LGBTQ community is the second most common reason, at 30%.
Mentions that 17% switched churches or congregations due to becoming too political.
Stresses the importance of the separation of church and state for both institutions.
Observes that many are leaving churches because they find it easier to preach hate than love.
Suggests that the decline in church attendance and contributions is linked to this shift.
Raises the idea that preaching hate rather than love is driving people away from religious institutions.
Concludes by hinting that being a good person is ultimately more cost-effective than spreading hate.

Actions:

for religious communities, lgbtq advocates,
Reassess church teachings and treatment of marginalized groups (suggested)
Support inclusive religious communities (implied)
</details>
<details>
<summary>
2023-05-17: Let's talk about North Carolina and the override.... (<a href="https://youtube.com/watch?v=aPr2woR2cJM">watch</a> || <a href="/videos/2023/05/17/Lets_talk_about_North_Carolina_and_the_override">transcript &amp; editable summary</a>)

Beau calls out North Carolina Republicans for prioritizing party interests over constituents, urging voters to take action and not tolerate blind loyalty.

</summary>

"They put the special interest groups above the people of North Carolina to include Republicans, to include their Republican voters."
"You need to set the tone right now that that's not acceptable, that they don't own you."
"None of them thought the people of North Carolina was more important than obeying the leadership."
"Their priority was doing what they're told. They don't sound like leaders to me."
"If you don't vote them out, they're going to keep doing it."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of the North Carolina veto override and its impact on the people and Republicans in the state.
Points out that despite overwhelming opposition from North Carolinians in polls, Republican legislators passed unpopular legislation.
Emphasizes that Republican lawmakers prioritized party loyalty and special interest groups over the will of the people.
Criticizes Republican legislators for choosing to obey party leadership instead of representing their constituents.
Urges Republican voters in North Carolina to take action by voting out those who prioritize party interests over the people's will.
Encourages Republican voters to primary their representatives and show that blind loyalty to the party is unacceptable.
Conveys the message that if voters don't hold their representatives accountable, they will continue to prioritize special interests over public opinion.

Actions:

for north carolina voters,
Primary Republican representatives (suggested)
Vote out representatives prioritizing party interests over constituents (implied)
</details>
<details>
<summary>
2023-05-17: Let's talk about Dems trying to oust Santos.... (<a href="https://youtube.com/watch?v=AbPx3hCsiaA">watch</a> || <a href="/videos/2023/05/17/Lets_talk_about_Dems_trying_to_oust_Santos">transcript &amp; editable summary</a>)

The Democratic Party's attempt to expel Santos reveals the futility of expecting Republican loyalty to principles over power.

</summary>

"The Republican base doesn't care about principle, about the country, stuff like that."
"They care about loyalty. That's who runs their primaries now."
"They literally don't care. The principles that that party claimed, they don't actually care about them anymore."
"It's all about edgy comments, owning the libs and maintaining power."
"The Democratic Party needs to acknowledge that. It needs to adjust their behavior accordingly."

### AI summary (High error rate! Edit errors on video page)

The Democratic Party is attempting to expel Representative Santos from the House of Representatives.
The Democrats believe it will be a tough vote for Republicans, revealing their naivety.
The Republican base doesn't care about principles or loyalty to the country; they prioritize party loyalty.
Voting to expel Santos won't impact Republicans in the general or primary elections.
The current Republican Party prioritizes power and loyalty over principles and policy.
Republican hypocrisy is futile to point out as they are focused on maintaining power.
The Democratic Party needs to adjust its behavior accordingly in dealing with the current Republican Party.

Actions:

for politically aware individuals,
Adjust behavior in dealing with the current Republican Party (suggested)
Acknowledge the shift in Republican priorities and adjust accordingly (implied)
</details>
<details>
<summary>
2023-05-16: Let's talk about why there's so much Turkish election coverage.... (<a href="https://youtube.com/watch?v=xytKKpGT0Bs">watch</a> || <a href="/videos/2023/05/16/Lets_talk_about_why_there_s_so_much_Turkish_election_coverage">transcript &amp; editable summary</a>)

Turkey's election and its implications for NATO, Russia, and the EU are closely watched globally, with the challenger likely to steer Turkey back towards Western alliances.

</summary>

"Turkey's election is significant on the international stage."
"The election outcome could impact not just Turkey but also other countries like Sweden's NATO membership."

### AI summary (High error rate! Edit errors on video page)

Turkey's election is significant on the international stage due to its implications for NATO, Russia, and the EU.
The current leadership in Turkey did not secure 50% of the votes, leading to a runoff election.
The challenger in the election aims to steer Turkey back towards the EU and NATO, contrasting with the current leadership's shift away from Western alliances.
The US likely favors the challenger, while Moscow is inclined towards the current leadership due to warmer relations.
The election outcome could impact not just Turkey but also other countries like Sweden's NATO membership.
The coverage of Turkey's election stems from the interconnected web of international interests and potential outcomes rather than a single major event.
The election is a culmination of various smaller factors that have piqued global interest.
The challenger is expected to have an advantage due to the anti-current leadership sentiment among other candidates' supporters.
Despite the challenger's probable lead, the runoff election outcome is anticipated to be closely contested.
The election result will be watched closely for its potential repercussions and effects on international relations.

Actions:

for global citizens,
Contact organizations supporting democratic values in Turkey to understand how to provide support (suggested)
Stay informed about the election runoff outcome and its potential impacts on international relations (implied)
</details>
<details>
<summary>
2023-05-16: Let's talk about the Durham report.... (<a href="https://youtube.com/watch?v=uiXDTWuKSlw">watch</a> || <a href="/videos/2023/05/16/Lets_talk_about_the_Durham_report">transcript &amp; editable summary</a>)

Beau breaks down the Durham Report, revealing its lack of significant findings and suggesting oversight to combat bias in investigations.

</summary>

"It's a burger we've already eaten."
"They relied too much on raw intelligence or information from sources that they really shouldn't have trusted."
"Throughout the entire thing I think they only brought charges against three people and two of them were acquitted."
"Because they have no clue what treason is."
"That's how you avoid confirmation bias. That's how you avoid groupthink."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of the Durham Report and its lack of groundbreaking information compared to the Inspector General's report.
Durham's conclusion that mistakes were made and the investigation shouldn't have proceeded due to reliance on questionable sources.
The report did not uncover any significant crimes or deep state plots, with very few individuals facing charges.
Criticizing those on social media who cry "treason" without understanding the legal definition.
Durham's main recommendation: creating oversight for politically sensitive investigations to challenge confirmation bias.
Beau's belief that this oversight should be extended to all investigations to combat groupthink.
Acknowledging that the report won't likely have a long-lasting impact, especially in right-wing media circles.

Actions:

for political analysts, policymakers,
Establish oversight for politically sensitive investigations to challenge confirmation bias (implied)
</details>
<details>
<summary>
2023-05-16: Let's talk about learning from Lauren Boebert.... (<a href="https://youtube.com/watch?v=p9-vNZl3FUQ">watch</a> || <a href="/videos/2023/05/16/Lets_talk_about_learning_from_Lauren_Boebert">transcript &amp; editable summary</a>)

Beau points out how a misleading graphic on US border encounters sparked criticism from Lauren Boebert, shedding light on the importance of processing information from graphics carefully to avoid falling into traps of misinformation.

</summary>

"How stupid do they think the American people are?"
"When you are looking at graphics, there's a bunch of things that can be done to alter the perception of something."
"Make sure you actually take the time to process the information."
"It's being done to reinforce a preconceived notion and that is the real danger."
"When you see the graphics as the talking heads are up there stating their position, make sure that you take the time to process it and read the days of the week."

### AI summary (High error rate! Edit errors on video page)

Lauren Boebert misinterpreted a graphic on CNN showing US border encounters since Title 42 ended.
Boebert criticized CNN for showing a surge on the graphic when the numbers actually dropped.
The graphic was in reverse chronological order, causing confusion for viewers used to reading left to right, top to bottom.
Beau suggests that the graphic may have been intentionally misleading by not following expected chronological order.
He mentions how truncated bar graphs can manipulate perception by showing only a portion of the data.
Beau points out that media outlets sometimes use graphics to stir up trouble or reinforce preconceived notions.
Despite Boebert's criticism, Beau acknowledges that misinterpreting such graphics is common.
He urges viewers to take the time to process information from graphics and not jump to conclusions.
Beau advises checking if the information is presented in context and reading the details carefully.
He warns about the danger of graphics being used to reinforce preconceived notions rather than present facts accurately.

Actions:

for media consumers,
Verify the information presented in graphics by checking multiple sources (implied).
Take time to process information from graphics and avoid jumping to immediate conclusions (implied).
Read details carefully and ensure the context is accurate before forming opinions (implied).
</details>
<details>
<summary>
2023-05-16: Let's talk about Rudy, at least parts of it.... (<a href="https://youtube.com/watch?v=2_8ybHpr96Y">watch</a> || <a href="/videos/2023/05/16/Lets_talk_about_Rudy_at_least_parts_of_it">transcript &amp; editable summary</a>)

Claims against Rudy Giuliani emerge, including selling pardons, with a lawsuit for $10 million, sparking potential media interest.

</summary>

"Allegations include being promised a million dollars annually but receiving significantly less."
"The suit filed against Giuliani is for $10 million."
"Media attention may resurface once there is movement on the case."

### AI summary (High error rate! Edit errors on video page)

Claims have surfaced about Rudy Giuliani, overshadowed by a recent report and media frenzy.
Allegations from a 70-page complaint filed by a former employee include being promised a million dollars annually but receiving significantly less.
An allegation suggests Giuliani and Trump were selling pardons for two million dollars.
A whistleblower named John Kariakou reached out through Giuliani to obtain a pardon, with an associate quoting a $2 million cost.
The complaint includes allegations of audio recordings of Giuliani using sexist, racist, and anti-Semitic language.
The suit filed against Giuliani is for $10 million.
Giuliani has denied all allegations and plans to seek legal remedy.
The wide range of allegations makes this case particularly interesting.
Media attention may resurface once there is movement on the case.
Beau hints at the need to be prepared for the allegations and suggests reading up on them rather than just listening.

Actions:

for media consumers,
Read up on the allegations against Giuliani (suggested)
Stay informed about the case developments (suggested)
</details>
<details>
<summary>
2023-05-15: Let's talk about the conventional and unconventional.... (<a href="https://youtube.com/watch?v=A4W8xtKXTzM">watch</a> || <a href="/videos/2023/05/15/Lets_talk_about_the_conventional_and_unconventional">transcript &amp; editable summary</a>)

Beau explains why unconventional forces pose a greater challenge for major powers due to technological advancements and information access, making them harder to combat effectively.

</summary>

"It's really, it kind of boils down to two things."
"The one thing that is a determining factor."
"The United States has a better chance of going toe-to-toe with China or Russia in achieving a decisive victory."
"The technology, the information awareness that the unconventional force has access to today."
"It's really hard for a major power to combat that."

### AI summary (High error rate! Edit errors on video page)

Explains the difference between conventional and unconventional forces in the context of the US military.
Points out that unconventional forces are more capable of degrading US capabilities in conflict.
Attributes the increased capability of unconventional forces to advancements in technology.
Gives an example of drone technology and its impact on battlefield awareness for unconventional forces.
Mentions the abundance of open-source intelligence available to the average person.
Stresses that technology has provided unconventional forces with tools for surprise attacks.
Talks about the increased lethality of individual soldiers, regardless of being conventional or unconventional.
States that unconventional forces can keep fighting until the political resolve of the major power breaks.
Notes that major powers often fail to adapt their military doctrine to effectively combat unconventional forces.
Concludes that the US military may struggle more against unconventional forces than against conventional forces like China or Russia.

Actions:

for military analysts, policymakers,
Analyze and adapt military strategies to effectively combat unconventional forces (implied)
Utilize open-source intelligence for informed decision-making in conflict situations (implied)
Build relationships with local populations to prevent sympathy towards unconventional forces (implied)
</details>
<details>
<summary>
2023-05-15: Let's talk about the Republican investigation into Biden.... (<a href="https://youtube.com/watch?v=J9AD88Z1wZE">watch</a> || <a href="/videos/2023/05/15/Lets_talk_about_the_Republican_investigation_into_Biden">transcript &amp; editable summary</a>)

Beau questions the lack of evidence in the Republican investigation into Biden while advocating for financial oversight of high-ranking officials' family members.

</summary>

"An informant in a Congressional investigation disappeared. They can't find him. They don't know what happened to him. Just gone with the wind."
"If you're actually concerned about this type of stuff, create some oversight. You're the legislative branch of government. legislate."

### AI summary (High error rate! Edit errors on video page)

Beau questions the ongoing investigation into Biden by Republicans and expresses skepticism due to lack of evidence.
Republicans are accused of rebranding similar claims with less evidence of influence peddling by Hunter and James.
The Fox summary acknowledges no illegal activity by Joe Biden and lack of profits for him.
A secret informant in the investigation has gone missing, raising concerns about their credibility.
Beau questions the lack of urgency in finding the missing informant if the allegations were believed.
International consulting firms linked to Biden's family received payments, which Beau sees as standard practice.
Beau recalls his past investigation into claims against Biden and how they fell apart with a timeline analysis.
Despite bank records not showing evidence and the missing informant, Beau predicts Republicans will drag out the investigation without producing substantial evidence.
He suggests oversight of immediate family members' finances in high-ranking positions to prevent conflicts of interest.
Beau proposes a bipartisan oversight committee including IRS and DOJ representatives to ensure financial transparency.

Actions:

for legislators, government officials,
Advocate for bipartisan oversight of immediate family members' finances in high-ranking positions (suggested)
Push for the creation of a committee including IRS and DOJ representatives to ensure financial transparency (suggested)
</details>
<details>
<summary>
2023-05-15: Let's talk about assumptions, perceptions, and silence.... (<a href="https://youtube.com/watch?v=rDjlXx0M12A">watch</a> || <a href="/videos/2023/05/15/Lets_talk_about_assumptions_perceptions_and_silence">transcript &amp; editable summary</a>)

Beau clarifies his stance on discussing incidents, explains the LVNR, and stresses the importance of intent while criticizing cheering for disturbing events.

</summary>

"I give everybody the benefit of the doubt on intent. That's super important."
"It doesn't matter what you think happened in your breakdown of it. It's not good."
"There's no way where this is good."

### AI summary (High error rate! Edit errors on video page)

Received a message prompting him to address perceptions and assumptions.
Clarifies his stance on not discussing certain incidents and why.
Differentiates between law enforcement versus private citizen incidents.
Explains his knowledge of a lateral vascular neck restraint (LVNR).
Stresses the importance of intent but doubts its relevance in a specific incident.
Expresses disapproval of cheering for a homeless person in distress being taken out on the subway.
Encourages critical thinking about what content creators cover.

Actions:

for content creators, viewers,
Re-examine beliefs on what content creators cover (implied).
</details>
<details>
<summary>
2023-05-15: Let's talk about Ukraine spy news.... (<a href="https://youtube.com/watch?v=1p-YZDerIUI">watch</a> || <a href="/videos/2023/05/15/Lets_talk_about_Ukraine_spy_news">transcript &amp; editable summary</a>)

Addressing leaked info from Ukraine, Beau analyzes the strategic implications and potential impact on Russian leadership, underscoring a possible win for Ukrainian intelligence.

</summary>

"It's an incredibly successful intelligence operation."
"Whether or not this actually occurred, this is a Ukrainian win."

### AI summary (High error rate! Edit errors on video page)

Addressing news from Ukraine about leaked information regarding Wagner, Russia's private military.
The leaked info suggests the boss of Wagner was offering to reveal troop locations to Ukrainian intelligence to protect his own people.
The move is seen as strategic since Ukrainian intelligence knows troop formations but not leadership locations.
Historical context supports the idea of contractors in military command becoming liabilities.
The success of the operation lies in either flipping a high-ranking Russian official or spreading believable information to sow discord.
Regardless of the truth, it serves as a win for Ukrainian intelligence by playing into existing splits and paranoia within the Russian command structure.
The outcome may not be known until after the war, but it's a significant development to monitor.

Actions:

for intelligence analysts, geopolitical enthusiasts.,
Monitor Russian leadership moves over the next 60 days (suggested).
Stay informed on developments in the region (suggested).
</details>
<details>
<summary>
2023-05-14: Let's talk about what Zelenskyy said.... (<a href="https://youtube.com/watch?v=AROotqrtG08">watch</a> || <a href="/videos/2023/05/14/Lets_talk_about_what_Zelenskyy_said">transcript &amp; editable summary</a>)

Zelensky's suggested aggressive responses towards Russia pose strategic dilemmas for Ukraine, balancing capability with caution, while leveraging the power of rhetoric to influence Russian actions and resource allocation.

</summary>

"You want them in the position they're in now begging for troops."
"The fear of it happening is probably more effective than actually doing it."
"He should say wilder stuff to be honest."

### AI summary (High error rate! Edit errors on video page)

Information surfaced about Zelensky suggesting aggressive responses towards Russia, like taking Russian towns and conducting missile strikes.
While Ukraine could take and hold Russian territories, the question is whether they should due to potential Russian responses.
Ukrainian generals have advised against extreme actions that could provoke wider mobilization in Russia.
Putin's possible reactions to Ukraine's aggressive moves include using strategic weapons like nukes.
Beau underscores the importance of maintaining Ukraine's current advantageous position in public opinion.
There's a distinction between taking and holding territory, with Russia historically demonstrating challenges in holding captured areas.
Beau believes it's not a good idea for Ukraine to pursue extreme actions and suggests Zelensky's generals likely share this viewpoint.
Beau argues that even discussing such aggressive responses can influence Russian leadership and divert resources from Ukraine.
Beau supports Zelensky continuing to vocalize bold strategies, as the fear of these actions can be more effective than actually executing them.

Actions:

for leaders, policymakers, strategists,
Analyze potential consequences of aggressive actions (implied)
Maintain current advantageous position in public opinion (implied)
Continue strategic discourse to influence opponents (implied)
</details>
<details>
<summary>
2023-05-14: Let's talk about a North Carolina veto and one vote.... (<a href="https://youtube.com/watch?v=vdeaGjEklSY">watch</a> || <a href="/videos/2023/05/14/Lets_talk_about_a_North_Carolina_veto_and_one_vote">transcript &amp; editable summary</a>)

Republicans in North Carolina push to override governor's veto on a bill curbing reproductive rights despite low public support, urging civic engagement for change.

</summary>

"One vote. People in North Carolina need to convince one to break ranks, one to honor their campaign promise, one to do what polling suggests they should do."
"Republicans deciding that they're going to rule rather than represent."
"Don't actually listen to the people who put you in office. Don't enact their will."

### AI summary (High error rate! Edit errors on video page)

North Carolina's governor vetoed a bill curbing reproductive rights, prompting Republicans to attempt an override.
Republicans in North Carolina need every vote for the override; losing one vote could sway the decision.
The bill in question is convoluted, with varying restrictions on reproductive rights at different stages of pregnancy.
Only 35% of North Carolinians support the bill, indicating widespread opposition to additional restrictions.
Majority in North Carolina either support expanding reproductive rights or maintaining the current status quo.
Republicans are accused of prioritizing rule over representation, going against the will of the people.
Despite low public support, Republicans plan to override the veto, disregarding constituents' desires.
People in North Carolina are urged to convince just one Republican representative to prevent the override.
Pressure from Republican Party leadership may influence representatives to vote against constituents' wishes.
Civic engagement is encouraged to influence representatives and uphold the will of the people.

Actions:

for north carolinians,
Convince one Republican representative to prevent the override (implied)
Engage in civic activities to influence representatives (implied)
</details>
<details>
<summary>
2023-05-14: Let's talk about Twitter's new CEO.... (<a href="https://youtube.com/watch?v=uUlLXxbu5W4">watch</a> || <a href="/videos/2023/05/14/Lets_talk_about_Twitter_s_new_CEO">transcript &amp; editable summary</a>)

Elon Musk's departure as Twitter CEO sparks concerns about the platform's future under new leadership and the impact on his cultivated user base.

</summary>

"Elon Musk stepping down as CEO of Twitter, with a new CEO, Linda Iaccarino, coming in to lead."
"The base of users Musk catered to is displeased with the new CEO selection."
"Concerns arise about the direction Twitter may take under the new CEO and the potential impact on Musk's influence."

### AI summary (High error rate! Edit errors on video page)

Elon Musk stepping down as CEO of Twitter, with a new CEO, Linda Iaccarino, coming in to lead.
Linda Iaccarino known for expertise in brand safe advertising, but has ties to the World Economic Forum (WEF).
The WEF is seen by some as a hub for conspiracy theories, which raises concerns among certain Twitter users.
A parody account attributed a fake tweet to Linda Iaccarino about controlling Elon Musk, causing confusion.
Concerns arise about the direction Twitter may take under the new CEO and the potential impact on Musk's influence.
The new CEO is believed to have right-wing ties but is also expected to address issues like content moderation and verification on Twitter.
Uncertainty looms over whether Musk will allow the new CEO to make necessary changes or if his ego will interfere.
The base of users Musk catered to is displeased with the new CEO selection, indicating potential discord ahead for Twitter.

Actions:

for twitter users,
Monitor and actively participate in the changes occurring at Twitter (implied)
Support efforts towards brand safe advertising and responsible content moderation on social media platforms (implied)
Stay informed about the developments within Twitter leadership and their potential implications (implied)
</details>
<details>
<summary>
2023-05-14: Let's talk about Facebook echoing in eternity.... (<a href="https://youtube.com/watch?v=UaycTJyPId4">watch</a> || <a href="/videos/2023/05/14/Lets_talk_about_Facebook_echoing_in_eternity">transcript &amp; editable summary</a>)

Beau talks about the intersection of social media, history, and future historical documentation, reflecting on the generational impact of online posts and the detailed records they create.

</summary>

"What you post on Facebook echoes in eternity."
"Every person is a public figure in that regard."
"It's wild when you think about it."

### AI summary (High error rate! Edit errors on video page)

Describes a realization about social media, history, and future history sparked by a diner incident and a Twitter trend.
Recounts a confusing interaction with his child about learning about lunch counters in school.
Mentions the lack of consequences for those who poured food during lunch counter protests due to police indifference and lack of social accountability.
Points out how future generations will have more detailed historical records due to social media documentation.
Talks about the potential for social media to be a valuable tool for future historians in recording social movements.
Raises the idea of future history books containing memes and detailed records of online posts.
Emphasizes the generational impact of social media posts and the importance of being on the right side of history.
Speculates on the challenges of creating an American mythology in the future with detailed historical records.
Concludes by reflecting on the public nature of online posts and the scrutiny they will face over time.

Actions:

for history enthusiasts, social media users.,
Preserve digital content for future generations by documenting significant social movements online (exemplified).
Encourage responsible online posting to ensure a positive generational legacy (implied).
</details>
<details>
<summary>
2023-05-13: Let's talk about defenses of Trump and a mistake.... (<a href="https://youtube.com/watch?v=0g4QUqbd9mY">watch</a> || <a href="/videos/2023/05/13/Lets_talk_about_defenses_of_Trump_and_a_mistake">transcript &amp; editable summary</a>)

Exploring defenses of Trump reveals disturbing attitudes towards power and relationships, even among fathers, with a caution about the impact of online comments.

</summary>

"That's not why that occurs."
"It's pathetic."
"Trump, even out of office, even with as little power as he has, he still has a hold over people."
"Not just your daughters, but other people in your life, they will never look at you the same way again."

### AI summary (High error rate! Edit errors on video page)

Exploring defenses of Trump after a decision in New York led to shocking revelations.
Sought out right-wing social media reactions to understand their defenses.
Shocked by two particular defenses focusing on Trump's ability to have any woman he wanted.
Emphasizes that the behavior is not about attractiveness but about power.
Both defenders happened to be fathers, which added a disturbing layer to their comments.
Expresses horror at the thought of daughters not being believed based on looks or the man's wealth.
Notes the impact of social media comments on relationships, especially with one's children.
Criticizes the idea of valuing money as a reason to be with someone, especially for right-wing individuals.
Acknowledges Trump's lingering influence even after leaving office.
Warns about the permanence and consequences of words spoken online.

Actions:

for parents, online commentators,
Monitor and reconsider the impact of one's comments on social media (implied).
Educate children on healthy values and relationships (implied).
Foster open communication with children to address potentially harmful beliefs (implied).
</details>
<details>
<summary>
2023-05-13: Let's talk about Ukraine, and whether it started.... (<a href="https://youtube.com/watch?v=o_eF2QaXO_Y">watch</a> || <a href="/videos/2023/05/13/Lets_talk_about_Ukraine_and_whether_it_started">transcript &amp; editable summary</a>)

Beau explains the ongoing military operations in Ukraine, focusing on shaping the battlefield for a future counter-offensive rather than the current actions being a full-fledged counter-offensive.

</summary>

"They're not supposed to be there. They're facing combat and they've decided they're not going to die for some sick old man in the Kremlin. Good for them."
"Conscripts, draftees, it doesn't go well. It very rarely produces the desired result."
"If things kind of lock up, don't let that impact morale."

### AI summary (High error rate! Edit errors on video page)

Explains the strategic, operational, and tactical levels of military operations in Ukraine.
Believes that the current actions are shaping the battlefield for a future, larger move rather than the actual counter-offensive.
Observes that most actions seem to be at the tactical level of securing certain areas.
Notes that the Russians are not engaging in a full counter-offensive yet, but testing and exploiting vulnerabilities.
Points out that Russians leaving Ukraine is not an orderly withdrawal, but more of an abrupt decision to depart.
Comments on Russia's conscripts and draftees not being suited for combat, leading to their departure.
Advises against mandatory military service based on the situation in Ukraine.
Suggests that the gains made by the Ukrainian side may not be part of the larger operation for significant gains.
Warns against morale dropping if progress slows down, as the current actions are setting the stage for a future counteroffensive.

Actions:

for military analysts,
Monitor the situation in Ukraine closely (implied)
Support efforts that aim to de-escalate the conflict (implied)
</details>
<details>
<summary>
2023-05-13: Let's talk about Trump's NY warning.... (<a href="https://youtube.com/watch?v=PBcLu3gk-ro">watch</a> || <a href="/videos/2023/05/13/Lets_talk_about_Trump_s_NY_warning">transcript &amp; editable summary</a>)

The judge in the New York case is setting up a Zoom call on May 23rd with Trump to explain restrictions on using information obtained via discovery, warning of contempt if violated, particularly regarding harassment.

</summary>

"Trump is prohibited from using any information obtained via discovery to harass or intimidate witnesses."
"The judge's call is a warning to Trump, indicating that he could be found in contempt if he violates the order."
"Trump has used social media to bully people in the past, but the judge is now making it clear that he cannot continue this behavior in the criminal proceeding in New York."

### AI summary (High error rate! Edit errors on video page)

The judge in the New York case is setting up a Zoom call with Trump on May 23rd to explain what he can and cannot do with the information he obtains through discovery from the state.
Trump is prohibited from using any information obtained via discovery to harass or intimidate witnesses, and he is not allowed to possess certain information, only to view it with his lawyers without taking notes or copying anything.
The judge's call is a warning to Trump, indicating that he could be found in contempt if he violates the order.
Trump's history suggests he may struggle to keep information confidential until February or March 2024.
Trump has used social media to bully people in the past, but the judge is now making it clear that he cannot continue this behavior in the criminal proceeding in New York.
The judge's intention seems to be holding Trump accountable if he breaches the restrictions set during the Zoom call.
It is likely that there will be further proceedings related to Trump's actions following this briefing.

Actions:

for legal watchers,
Stay updated on the developments in legal cases involving public figures (implied)
Support accountability in legal proceedings by advocating for fair treatment and adherence to restrictions (implied)
</details>
<details>
<summary>
2023-05-13: Let's talk about Missouri and a failed power play.... (<a href="https://youtube.com/watch?v=aFAkYXKNSzI">watch</a> || <a href="/videos/2023/05/13/Lets_talk_about_Missouri_and_a_failed_power_play">transcript &amp; editable summary</a>)

Missouri Republicans attempt to restrict citizens' ability to override legislature through Initiative Petition due to fear of reproductive rights, prioritizing ruling over representing the will of the people.

</summary>

"Republicans, they don't care about the Republic. They don't care about being your representative. They don't want to represent you. They want to rule you."
"They know their ban is unpopular. That they know it goes against the will of the people of Missouri. They just don't care because they're your betters."
"They were not able to change it so that process is still available and there will probably be an initiative petition put forward that will restore reproductive rights in Missouri."
"Rather than represent the will of the people, they want to override it."
"Republicans in Missouri, well, they caught the car."

### AI summary (High error rate! Edit errors on video page)

Missouri Republicans tried to make it harder for citizens to override legislature through the Initiative Petition by increasing the required support from a simple majority to 57%.
Their attempt to change the process was driven by their fear that citizens wanted reproductive rights, which goes against the restrictive ban they passed.
Despite knowing their ban was unpopular and against the will of the people, Republicans prioritized ruling over representing the citizens.
The Republican Party's attempt to limit citizens' ability to represent themselves failed, allowing for the potential restoration of reproductive rights in Missouri through an initiative petition.
Republicans displayed a clear desire to rule over the citizens rather than truly represent them, showing a disregard for the will of the people.

Actions:

for activists, missouri residents,
Support and participate in initiative petitions to restore reproductive rights in Missouri (implied).
Stay informed about legislative changes and initiatives in Missouri (implied).
</details>
<details>
<summary>
2023-05-12: Let's talk about the SCOTUS shadow docket case that could make waves.... (<a href="https://youtube.com/watch?v=QL9KI-fQKcs">watch</a> || <a href="/videos/2023/05/12/Lets_talk_about_the_SCOTUS_shadow_docket_case_that_could_make_waves">transcript &amp; editable summary</a>)

Beau warns about potential impacts of a Supreme Court case on gun control, urging readiness for change and consideration of alternative solutions.

</summary>

"It's probably time to begin looking at other options, even if they are more expensive, to try to mitigate some of the harm."
"Don't get so locked into one solution because that solution it may not be on the table much longer."
"Until the court changes, that's the way it is."

### AI summary (High error rate! Edit errors on video page)

Explains the potential impact of a Supreme Court case on gun control in the United States.
Mentions the National Association for Gun Rights versus the city of Naperville case on the shadow docket.
Describes the Supreme Court's options with the gun rights case, including ruling narrowly or widely.
Suggests that the Supreme Court might either fully do away with assault weapons bans or open the door for another case to challenge them.
Points out that the current balance of the court indicates a likelihood of reversing previous gun control legislation.
Urges people to be prepared for potential changes and start considering alternative options to mitigate harm.
States that the odds of the gun control side winning before this court are slim.
Encourages individuals to look at other solutions to reduce harm and save lives as the current solution may not be viable for long.
Emphasizes the importance of being aware and ready for the outcome of the court's decision.
Advises against getting too fixated on one solution as circumstances may change.
Indicates that people with a vested interest in a particular solution may need to acknowledge the current reality until the court changes.
Suggests that it's time to start exploring alternative strategies in light of the potential Supreme Court decision.

Actions:

for advocates for gun control,
Prepare for potential changes in gun control laws by staying informed and engaged (implied)
Advocate for alternative solutions to mitigate harm and save lives in the face of potential legal challenges (implied)
</details>
<details>
<summary>
2023-05-12: Let's talk about a Tuberville quote and readiness.... (<a href="https://youtube.com/watch?v=TyRKxq5N554">watch</a> || <a href="/videos/2023/05/12/Lets_talk_about_a_Tuberville_quote_and_readiness">transcript &amp; editable summary</a>)

Senator Tuberville's controversial views on military recruitment and white nationalists undermine unit cohesion and readiness, hindering the effectiveness of the military.

</summary>

"They call them that, I call them Americans."
"We cannot start putting rules in there for one type, one group, and make different factions in the military."
"This statement is just wild to me."
"He wants to help military readiness by allowing people who break down unit cohesion."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Senator Tuberville suggested that white nationalists should be allowed to serve in the military, referring to them as Americans.
Tuberville blames Democrats for the military's recruitment struggles, claiming they are attacking white extremists.
He believes that imposing rules for specific groups in the military weakens the institution.
Tuberville blocked promotions of around 180 generals, hindering military readiness.
Allowing white nationalists in the military can harm unit cohesion and readiness.
Tuberville's team clarified that he was skeptical of the presence of white nationalists, not supporting their inclusion.
His actions go against improving military readiness by promoting disunity within the ranks.
The Senator's stance on military recruitment and white nationalists has sparked controversy.
Tuberville's beliefs and actions are seen as detrimental to the military's effectiveness.
His approach contradicts the goal of maintaining a strong and united military force.

Actions:

for military personnel, policymakers,
Challenge discriminatory rhetoric within military recruitment (implied)
Advocate for inclusivity and unity within the military (implied)
Support efforts to maintain strong unit cohesion (implied)
</details>
<details>
<summary>
2023-05-12: Let's talk about Trump and debt ceiling 101.... (<a href="https://youtube.com/watch?v=pQIP-fvAc6o">watch</a> || <a href="/videos/2023/05/12/Lets_talk_about_Trump_and_debt_ceiling_101">transcript &amp; editable summary</a>)

Beau explains Trump's debt ceiling stance, clarifying it's Republicans jeopardizing stability, not negotiation, aiming to blame Biden for economic damage.

</summary>

"The Duke of debt, the Baron of bankruptcy doesn't want to pay his bills."
"If the country defaults, it is the Republican party's fault, nobody else."
"Make no mistake about this. If the country defaults, it wasn't an accident. It didn't fall. It was pushed into economic calamity and the people that pushed them, they're wearing red hats."

### AI summary (High error rate! Edit errors on video page)

Explains Trump's statements on the debt ceiling and provides a "default 101" to clarify misconceptions.
Trump indicated that without massive budget cuts, the country may face default, which is not surprising considering his history of not paying bills.
Trump owns a disproportionate $7.8 trillion of the debt, contradicting his claim of spending money like "drunken sailors."
Reveals that the debate is not about negotiating the debt ceiling but rather Republicans choosing not to raise it, endangering economic stability.
Points out that Biden is willing to either raise or eliminate the debt ceiling, contrary to the false narrative of negotiation.
Emphasizes that the Republican party, particularly McCarthy, Trump, and hardline Republicans, are to blame for risking economic damage by not raising the debt ceiling for leverage.
Warns against falling for misinformation that aims to shift blame onto Biden for potential economic suffering due to budget cuts.
Condemns the plan to make the public endure hardships to achieve political goals, stressing that any potential default is a deliberate push into economic calamity by certain Republicans.

Actions:

for voters, political activists,
Contact your representatives to urge them to prioritize raising the debt ceiling for economic stability (suggested).
Stay informed about the debt ceiling debate and hold accountable those responsible for risking economic hardship (implied).
</details>
<details>
<summary>
2023-05-12: Let's talk about Russia, Ukraine, and Garland.... (<a href="https://youtube.com/watch?v=TcXvrDzMN20">watch</a> || <a href="/videos/2023/05/12/Lets_talk_about_Russia_Ukraine_and_Garland">transcript &amp; editable summary</a>)

Russia is altering conscription laws, Ukraine makes modest gains, and support for Ukraine is critical in the ongoing conflict with Russia.

</summary>

"Russia is realizing that they're in a protracted fight."
"Ukraine paid an incredibly high price for those couple of kilometers."
"If their resolve breaks, it's not a political loss. It will lead to real loss there."

### AI summary (High error rate! Edit errors on video page)

Russia is altering legal mechanisms to make conscription easier, using Instagram messages to notify individuals.
The changes indicate Russia’s realization of a protracted fight and the need for more troops.
Ukraine has made modest gains near Bakhmut, causing Russian forces to suffer a significant morale blow.
Zelensky has hinted at delaying the expected counter-offensive due to potential high Ukrainian casualties.
The Department of Justice has decided to use funds from oligarchs to support Ukraine, a change from freezing the money.
Both sides in the conflict are gearing up for a heavy phase, with Ukraine relying on support from other countries.
The support Ukraine receives is vital for them to continue resisting Russian aggression.

Actions:

for global citizens,
Support Ukraine through donations or spreading awareness (implied)
Stay informed about the situation in Ukraine and Russia (implied)
</details>
<details>
<summary>
2023-05-11: Let's talk about Tucker on Twitter.... (<a href="https://youtube.com/watch?v=reVWWwDAZEg">watch</a> || <a href="/videos/2023/05/11/Lets_talk_about_Tucker_on_Twitter">transcript &amp; editable summary</a>)

Tucker Carlson faces challenges transferring his base to Twitter, while Elon Musk grapples with fact-checking and revenue concerns.

</summary>

"Tucker may have to compete with more extreme voices on Twitter, potentially leading to trouble with advertisers."
"Elon Musk faces the dilemma of allowing or disallowing fact-checking on Twitter."
"Tucker faces the challenge of transferring his base from cable news to Twitter."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson is potentially moving to Twitter, a new home for him.
Tucker faces the challenge of transferring his base from cable news to Twitter.
Tucker may gain new followers on Twitter but will also face fact-checking and criticism.
Elon Musk has not signed a deal with Tucker but has welcomed him aboard Twitter.
Elon Musk faces the dilemma of allowing or disallowing fact-checking on Twitter.
Democrats or some other group might pay people to fact-check Tucker in real-time.
Elon Musk's main concern is Twitter's revenue and advertisers, especially after recent controversies.
Advertisers may be hesitant to associate with Tucker, leading to revenue loss for Twitter.
Tucker Carlson's extreme TV persona may not have the same impact on Twitter's right-wing audience.
Tucker may have to compete with more extreme voices on Twitter, potentially leading to trouble with advertisers.

Actions:

for twitter users,
Monitor and fact-check accounts or information that may spread misinformation or extreme views on Twitter (implied).
Support advertisers who choose not to associate with content you disagree with on social media (implied).
</details>
<details>
<summary>
2023-05-11: Let's talk about Trump, CNN, and laughter.... (<a href="https://youtube.com/watch?v=O7LK7t0P_OE">watch</a> || <a href="/videos/2023/05/11/Lets_talk_about_Trump_CNN_and_laughter">transcript &amp; editable summary</a>)

CNN's soft interview with Trump led to loss of control, revealing insights into his campaign strategies and base, while also shedding light on potential biases in reporting.

</summary>

"Trump plans to campaign on the idea that the election was rigged, despite only 63% of his party supporting this notion."
"That's his base, the people who would laugh at the victim of that sort of crime."
"CNN underestimated Trump's support at the interview, expecting a different crowd reaction."
"Rather than reporting the news, they're gonna try to lean in to right-wing talking points a little bit."
"Definitely watch the part where he is talking about E. Jean Carroll. Don't listen to him. Listen to the crowd."

### AI summary (High error rate! Edit errors on video page)

CNN's soft interview with Trump backfired, leading to loss of control and negative coverage.
Trump plans to campaign on the idea that the election was rigged, despite only 63% of his party supporting this notion.
Trump's base, as seen in a rally where he mocked a victim, consists of individuals who find amusement in such behavior.
CNN underestimated Trump's support at the interview, expecting a different crowd reaction.
CNN's attempt to attract Fox News viewers by leaning right may lead to reporting biased towards right-wing talking points.
Beau suggests that watching the part of the interview where Trump talks about E. Jean Carroll reveals insights into his base.

Actions:

for media consumers,
Question biased reporting and hold news outlets accountable (implied).
Stay informed and critically analyze media coverage (implied).
</details>
<details>
<summary>
2023-05-11: Let's talk about McConnell, Manchin, and leopards.... (<a href="https://youtube.com/watch?v=YH6KS2Ca8SI">watch</a> || <a href="/videos/2023/05/11/Lets_talk_about_McConnell_Manchin_and_leopards">transcript &amp; editable summary</a>)

Senator Manchin faces challenges as Senator McConnell supports his opponent, showcasing power struggles in politics.

</summary>

"This is what's wrong with this place."
"The compromise that a lot of people think is so important, it just sets you up to be surprised when people who are very politically shrewd and thirst for power decide to use you to get that power."

### AI summary (High error rate! Edit errors on video page)

Senator Manchin, a Democrat from a predominantly red state, is facing an expected opponent in the next election who is incredibly popular.
Senator McConnell, a Republican, has indicated his support for the Republican candidate in West Virginia, which has apparently deeply bothered Manchin.
Manchin criticizes McConnell for going against him, stating that he has never campaigned against a sitting colleague and that McConnell represents everything wrong with Congress and the Senate.
McConnell's support for Justice, the presumptive opponent of Manchin, could make it much harder for Manchin to win, although Manchin has previously won in a red state.
Beau points out that the Republican Party prioritizes power above all else, even if it means compromising with authoritarians, as they are focused on gaining and maintaining control.
McConnell's perspective is focused on gaining a majority of seats to become Senate Majority Leader or to ensure Republican control of the Senate.
Beau warns about the dangers of compromise with politically shrewd individuals who thirst for power, as it can lead to being used as a means to an end.
The situation in West Virginia exemplifies the power dynamics and strategic moves within the political landscape.

Actions:

for politically aware citizens,
Analyze and understand the power dynamics at play in politics (implied)
</details>
<details>
<summary>
2023-05-11: Let's talk about Feinstein's return.... (<a href="https://youtube.com/watch?v=gIcjJxJffJQ">watch</a> || <a href="/videos/2023/05/11/Lets_talk_about_Feinstein_s_return">transcript &amp; editable summary</a>)

Senator Feinstein's return to DC gives Democrats a chance to take action on pressing matters and potentially impact the 2024 election by holding justices accountable.

</summary>

"Let's see if they act on it. They might."
"Either way, with the way the Republican Party is becoming more and more scandal-ridden, it's something that they can use if they actually act on their rhetoric."
"Let's hope they don't choose poorly."

### AI summary (High error rate! Edit errors on video page)

Senator Feinstein will be returning to DC this week for one last crusade, even though she will not seek re-election.
The return of Senator Feinstein means that the Democratic Party will have a functional majority on the Senate Judiciary Committee again.
Judicial confirmations have been slowed down, and the Committee couldn't act on Supreme Court matters due to lacking votes.
With Senator Feinstein back, Democrats on the Judiciary Committee can issue subpoenas and take action on pressing matters.
This could potentially lead to a shift in the Democratic Party's ability to hold justices accountable, potentially affecting the 2024 election.
It is currently impossible for the Democratic Party to remove certain justices without a big majority, making motivation for upcoming elections critical.
Democrats need to decide if their tough talk is genuine and if they will act on their rhetoric now that they have the votes.
The Republican Party's scandals provide an opening for Democrats to capitalize on if they act decisively.
Democrats must choose whether their recent tough talk was genuine or just for show, especially in light of ongoing reports.
It remains to be seen if Democrats will follow through with their promises now that they have the power back.

Actions:

for democrats, voters,
Mobilize voters for the 2024 election by pointing out the importance of Senate Judiciary Committee actions (implied).
</details>
<details>
<summary>
2023-05-10: Let's talk about the GOP chair in Georgia and a novel statement.... (<a href="https://youtube.com/watch?v=7mp07chsSec">watch</a> || <a href="/videos/2023/05/10/Lets_talk_about_the_GOP_chair_in_Georgia_and_a_novel_statement">transcript &amp; editable summary</a>)

Georgia's GOP chair attempts to evade accountability by claiming reliance on legal advice, but the defense's efficacy remains uncertain amid skepticism and potential complications.

</summary>

"I'm just not sure that it's ironclad."
"When it comes to far-right power grabs, generally speaking, 'I was just following orders' doesn't go over well."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Georgia's GOP chair, David Schaefer, is using a novel defense to avoid accountability for his actions related to the alternate electors in Georgia.
Schaefer's lawyers claim he shouldn't be charged because he was following detailed legal advice, including advice from former President Trump's attorneys.
The defense argues that Schaefer's actions were taken in conformity with legal counsel, eliminating the possibility of criminal intent or liability.
While such a defense might work in some cases, Beau questions its effectiveness in this specific situation of far-right power grabs.
The defense relies on the premise that intent is a key element in proving criminal actions, suggesting that if intent is lacking, it could be challenging to establish guilt.
Beau speculates on potential complications if evidence surfaces suggesting knowledge of wrongdoing despite legal advice.
Despite the defense's argument, Beau expresses skepticism about its ironclad nature due to the secretive nature of the events.
The effectiveness of the defense hinges on whether there is documented reassurance from legal counsel that Schaefer's actions were lawful.
Beau concludes by acknowledging that the defense might hold up under scrutiny if backed by substantial evidence but suggests awaiting further developments.

Actions:

for legal observers,
Question legal defenses (implied)
Await further developments (implied)
</details>
<details>
<summary>
2023-05-10: Let's talk about the GOP budget, veterans, and the diner.... (<a href="https://youtube.com/watch?v=mcq7W86qRYQ">watch</a> || <a href="/videos/2023/05/10/Lets_talk_about_the_GOP_budget_veterans_and_the_diner">transcript &amp; editable summary</a>)

Beau visits a small Southern town diner where veterans gather, revealing the disconnect between Republican budget cuts and their impact on their own supporters.

</summary>

"Republicans are keeping quiet about specific cuts because they target their own supporters."
"The Republican budget will target the Republican base."
"Don't argue with disabled vets telling them that politicians wouldn't use them as pawns."

### AI summary (High error rate! Edit errors on video page)

Beau visits a small Southern town diner, which also serves as a town hall and meeting place for veterans.
Most veterans at the diner are unable to work due to injuries.
The diner chatter revolves around a Republican budget with significant cuts to veterans' benefits.
A man at the diner insists Republicans wouldn't cut veterans' benefits, but he is corrected by another diner.
The Republican budget passed with a 22% cut and no exemption for the VA.
Veterans at the diner find it amusing when someone claims Republicans wouldn't use veterans as pawns.
Beau points out the disconnect between the Republican budget cuts and the impact on their own base.
Republicans are keeping quiet about specific cuts because they target their own supporters, including farm subsidies and veterans' benefits.
The Republican budget aims to negotiate how much to cut from their own base.
Beau warns against arguing with disabled veterans about politicians not using them as pawns.

Actions:

for community members, veterans,
Support local veterans' organizations (implied)
Advocate for transparent budget plans that prioritize community needs (implied)
</details>
<details>
<summary>
2023-05-10: Let's talk about Santos being indicted.... (<a href="https://youtube.com/watch?v=g-Rlo7s7IPo">watch</a> || <a href="/videos/2023/05/10/Lets_talk_about_Santos_being_indicted">transcript &amp; editable summary</a>)

Checking in on US Rep. George Santos' indictment reveals likely charges related to soliciting money from wealthy donors, impacting McCarthy's seat.

</summary>

"Lying isn't illegal. It's not under most circumstances."
"They will continue to manipulate you until you stop listening to them."

### AI summary (High error rate! Edit errors on video page)

Checking in on US Representative George Santos after three months of silence.
Previous rumors about Santos being a secret spy for Trump debunked.
Speculation arises as Santos is reportedly indicted, with charges under seal.
Likely charges relate to alleged solicitation of money from wealthy donors.
Impact on McCarthy's seat and Republican support discussed.
Evidence against the existence of a government "super-secret war" emphasized.
Warning against manipulation by false narratives and the importance of critical thinking.

Actions:

for political observers,
Stay informed and critically analyze political narratives (implied).
Remain vigilant against manipulation and false information (implied).
</details>
<details>
<summary>
2023-05-10: Let's about claims about the budget caps and cuts.... (<a href="https://youtube.com/watch?v=k1Y3yA5gwB0">watch</a> || <a href="/videos/2023/05/10/Lets_about_claims_about_the_budget_caps_and_cuts">transcript &amp; editable summary</a>)

Beau explains the implications of the 22% cut and 1% increase in the Republican budget plan, critiquing its impact on the economy and services.

</summary>

"The Republican budget plan, it's not a plan for a healthy economy."
"The 1% increase is still an effective budget cut."
"It's huge. It's huge."
"So the 22% reduction across the board. That's where the number comes from."
"It's not even close to a reasonable starting point for a negotiation."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of the numbers 22% and 1% in Republican and Democratic math regarding the budget.
Republicans focus on a 1% increase cap annually, not the 22% cut in non-defense spending.
Breaks down how the 22% cut in non-defense spending is derived from the budget plan.
Illustrates how a 1% increase can actually result in a 5% reduction in purchasing power due to inflation.
Emphasizes the real-life impact of budget cuts on services like Border Patrol with a reduction in purchasing power.
Criticizes the Republican budget plan as detrimental to the economy and unsustainable.

Actions:

for budget analysts, policymakers,
Analyze and understand the implications of budget cuts on services in your community (implied)
Advocate for sustainable budget plans that prioritize economic stability and public services (implied)
</details>
<details>
<summary>
2023-05-09: Let's talk about trusted news sources and media literacy.... (<a href="https://youtube.com/watch?v=o7vlyG1-q1g">watch</a> || <a href="/videos/2023/05/09/Lets_talk_about_trusted_news_sources_and_media_literacy">transcript &amp; editable summary</a>)

Insights from a poll on media trust levels show Republicans' distrust, preference for opinions, and the need to differentiate facts from feelings.

</summary>

"People care about themselves and people can tell the difference between objective news and opinion if they want to."
"Even Republicans, even those who pretend like they don't know the difference between opinion and objective news. They do."
"Stop treating their opinions as facts."
"It's pretty clear if you really sit down and look at this."
"The majority of Americans trust these outlets more than the AP."

### AI summary (High error rate! Edit errors on video page)

Shares insights from a YouGov poll on media organization trust levels, broken down by party affiliation.
Top 10 most trusted media organizations include The Weather Channel, PBS, BBC, Wall Street Journal, Forbes, AP, ABC, USA Today, CBS, and Reuters.
People trust media outlets based on how they impact them directly, such as money or accent.
Fox ranks below Newsmax and OAN, indicating unfamiliarity with the latter two's accuracy records.
Republicans generally distrust most media organizations unless they are clearly partisan or reinforcing their opinions.
Republicans value feelings over facts, as shown by their widespread net distrust of media outlets.
Beau suggests that even Republicans know the difference between opinion and objective news and should stop pretending otherwise.
Beau encourages not treating opinions as facts, especially on issues where there is no real debate.
Points out that Wall Street Journal and Forbes cater to specific groups and have a particular slant towards a competitive society.
Beau calls for understanding and working towards a more cooperative society rather than a competitive one based on media biases.

Actions:

for media consumers,
Verify information from multiple sources before forming opinions (implied)
Encourage critical thinking and fact-checking among peers (implied)
Support media literacy programs in schools and communities (implied)
</details>
<details>
<summary>
2023-05-09: Let's talk about the Trump NY verdict.... (<a href="https://youtube.com/watch?v=cLEwxyndbeA">watch</a> || <a href="/videos/2023/05/09/Lets_talk_about_the_Trump_NY_verdict">transcript &amp; editable summary</a>)

Former President Trump found liable for battery and defamation, exemplifying his pattern of lies and manipulation, eroding supporters' beliefs and loyalty.

</summary>

"He lied about that too. That's not true. He absolutely could have presented a defense he chose not to."
"He lied to you. He's still lying to you."
"They lie to you constantly. They keep you angry."

### AI summary (High error rate! Edit errors on video page)

A federal jury in New York found former President Trump liable for battery and defamation in the E. Jean Carroll case, with a potential $5 million payout to her.
The case stems from allegations of sexual assault in a dressing room, with Carroll also requesting a public retraction of Trump's statements.
Trump falsely claimed he was not allowed to speak or defend himself, which is contradicted by public records.
Trump's rhetoric, like claiming he couldn't present a defense, is designed to push others down to make his base believe they are moving forward.
Trump's lies have separated supporters from their families and eroded their belief systems, focusing solely on loyalty to him.
This situation should mark the end of the MAGA movement, revealing the constant lies and manipulation from Trump and his supporters.
Trump and those who mimic his leadership style keep supporters angry and looking down rather than up at how they are being exploited.
Trump's lies and manipulation have led supporters to abandon their previous beliefs little by little, prioritizing loyalty to him above all else.

Actions:

for supporters of the republican party,
Reassess your beliefs and loyalty towards leaders who manipulate and lie to you (implied)
Educate yourself on the facts of cases involving public figures before forming opinions (implied)
</details>
<details>
<summary>
2023-05-09: Let's talk about patches, socials, and Texas.... (<a href="https://youtube.com/watch?v=6SDWAjAWhtk">watch</a> || <a href="/videos/2023/05/09/Lets_talk_about_patches_socials_and_Texas">transcript &amp; editable summary</a>)

Beau explains his cautious approach in discussing a patch linked to a specific group and the shooter in Texas, along with the discovery of social media profiles indicating far-right views but no direct connection to the group, anticipating increased scrutiny on the organization.

</summary>

"The patch, you know, it is what it is."
"The socials, which do appear to be his, that pretty much confirms his ideological makeup."
"I like to be right, I don't like to jump the gun."

### AI summary (High error rate! Edit errors on video page)

Explaining the hesitation in discussing a patch linked to a specific group and the shooter in Texas.
Describing the commercial availability and widespread use of patches once they leave an in-group.
Noting hidden meanings of patches and how they can be misunderstood by many who wear them.
Mentioning the discovery of social media profiles that indicate far-right and white supremacist views of the shooter.
Speculating on the lack of direct links between the shooter and the group associated with the patch.
Pointing out the recent conviction of the leadership of a group associated with the patch, which could deter any potential connection to the shooter.
Anticipating intense scrutiny from domestic counter-terror efforts on the organization due to the proximity of the patch to the convicted leadership.
Stating the preference to wait for concrete evidence before discussing such sensitive topics to ensure accuracy.

Actions:

for community members,
Monitor and report extremist activity in your community (implied)
Support efforts combatting far-right and white supremacist groups (implied)
</details>
<details>
<summary>
2023-05-09: Let's talk about an update on Bryan Slaton in Texas.... (<a href="https://youtube.com/watch?v=TDjEZ5pXH0c">watch</a> || <a href="/videos/2023/05/09/Lets_talk_about_an_update_on_Bryan_Slaton_in_Texas">transcript &amp; editable summary</a>)

Texas State Representative Brian Slaton resigns after misconduct findings, faces potential expulsion from a Republican-majority body for providing drinks to underage workers.

</summary>

"It's one of those cases that don't happen very often where you have a Republican majority body actually moving forward to hold another Republican accountable for their actions and their behavior."
"There are questions about some of the activities because some of the things that the committee found were, in fact, crimes."
"For those who were wondering if anything was going to come of it, yes, something absolutely came of it, and apparently came of it pretty quickly."

### AI summary (High error rate! Edit errors on video page)

Providing an update on Texas State Representative Brian Slaton and the findings of the House Committee regarding his activities and behavior.
Slaton provided drinks to underage workers, leading to one needing Plan B with questions about consent.
Slaton resigned two days after the findings were released, citing the need for a new representative to meet expectations.
Despite the resignation, Representative Murr plans to call for a vote to expel Slaton from the Texas House of Representatives.
Slaton is considered an officer of the state until a successor is elected and takes office, with the expulsion vote scheduled for that day.
The expulsion vote is likely to succeed as Slaton has already resigned, making it the first expulsion in almost a hundred years in the Texas legislature.
This situation is unique as it involves a Republican majority body holding another Republican accountable, a rare occurrence.
Rumors suggest that Slaton was offered the chance to resign earlier but declined, leading to the investigation and subsequent events.
There are questions about potential criminal activities as some findings by the committee were misdemeanors, possibly leading to law enforcement involvement.
The outcome of the expulsion may cover these potential legal implications, but the exact course of action remains uncertain.

Actions:

for legislative observers, accountability advocates,
Contact Texas legislators to express support for holding accountable those who violate ethical standards (implied)
Stay informed about local political developments and hold elected officials accountable for their actions (implied)
</details>
<details>
<summary>
2023-05-08: Let's talk about everything on the table with SCOTUS.... (<a href="https://youtube.com/watch?v=gB7irKhGupo">watch</a> || <a href="/videos/2023/05/08/Lets_talk_about_everything_on_the_table_with_SCOTUS">transcript &amp; editable summary</a>)

Update on Supreme Court saga may impact 2024 election, with potential talks of removing justices and creating an election issue for the Democratic Party.

</summary>

"Everything is on the table."
"A lot of Republicans are saying, well, we don't care. Thomas is on our team."
"If they continue that attitude, they are creating an election issue for the Democratic Party."
"I didn't see this on the 2024 calendar, but I think it's going to become an issue."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Update on Supreme Court and Senate Judiciary Committee saga for 2024 election.
Flurry of reports on questionable transactions involving Supreme Court members.
Chief Justice declined Senate Judiciary Committee's invitation to testify.
Senate Judiciary Committee may have to decide on next steps without Chief Justice's voluntary cooperation.
Dick Durbin implies potential hearings and investigations into Justice Thomas.
Pressure on Senator Feinstein to return to committee or retire due to absence.
Potential for 2024 to involve talks of removing Supreme Court justices.
Republican stance on Thomas being on their team despite ongoing revelations.
Democratic Party may harness issue for 2024 election and energize voters.
Supreme Court's lack of action could create an election issue for Democrats.

Actions:

for political activists,
Contact local representatives to express opinions on Supreme Court issues (implied).
Organize or join community events discussing the role of Supreme Court justices (implied).
</details>
<details>
<summary>
2023-05-08: Let's talk about Tucker, Elon, Fox, and what to do.... (<a href="https://youtube.com/watch?v=BqdVK9T_Hqw">watch</a> || <a href="/videos/2023/05/08/Lets_talk_about_Tucker_Elon_Fox_and_what_to_do">transcript &amp; editable summary</a>)

Beau provides insights into the escalating conflict between Tucker Carlson, Fox News, and Elon Musk, suggesting potential repercussions for both parties involved.

</summary>

"The idea that anyone is going to silence Tucker and prevent him from speaking to his audience is beyond preposterous."
"At the end of this, either Tucker or Fox is going to take a huge hit."
"What do you do when and all the worst people you know are fighting."

### AI summary (High error rate! Edit errors on video page)

Beau provides insight into the situation involving Fox News, Tucker Carlson, and Elon Musk, suggesting that Tucker wants to free up his allies to go after Fox in hopes of being released from his contract.
Tucker retained a Hollywood lawyer known for being ruthless, indicating his seriousness in the matter.
The idea of silencing Tucker is deemed preposterous by his attorney, Brian Friedman.
If Tucker's allies speak out against Fox and engineer some outrage, it may impact the situation.
There are reports of Tucker meeting with Elon Musk to potentially collaborate on a new project, hinting at a possible Fox News rival.
Tucker's absence has already affected Fox's viewership, leading to a loss of trust and declining numbers.
The conflict seems to be more against Rupert Murdoch than Fox News itself, raising questions about how it will unfold.
Ultimately, either Tucker or Fox is likely to face significant consequences due to the escalating situation.

Actions:

for media consumers,
Support alternative media sources (suggested)
Stay informed about media dynamics and biases (implied)
</details>
<details>
<summary>
2023-05-08: Let's talk about Trump, deals, and Georgia.... (<a href="https://youtube.com/watch?v=1irrHJo-mmk">watch</a> || <a href="/videos/2023/05/08/Lets_talk_about_Trump_deals_and_Georgia">transcript &amp; editable summary</a>)

Recent developments in the Trump case in Georgia indicate potential indictments and a clear path from lower-level individuals to higher-ups, possibly household names or elected officials.

</summary>

"Either Elector E is going to get this immunity and he's going to answer the questions or we're gonna leave and if we leave we're ripping up his immunity agreement and he can be on the indictment."
"There are a lot of people who are involved in this but they are insulated directly from the electors."
"I think that this latest chain of events is strengthening a case that is already pretty strong."

### AI summary (High error rate! Edit errors on video page)

Talks about recent developments in the Trump case in Georgia involving electors and immunity deals.
Mentioned that eight or nine electors have taken immunity deals to provide information.
The prosecution is aiming to build a strong case against those who briefed the electors.
Revealed a quote from the prosecutor to one of the electors' attorneys regarding immunity and answering questions.
Indicates that the prosecution is moving towards indictments and has specific questions they want answered.
Suggests that the case involves individuals higher up, potentially household names or current/former elected officials.
Speculates on the likelihood of upcoming indictments based on the unfolding events.
Mentions a statement from the prosecutor in Georgia about being ready this summer, hinting at a timeline for action.

Actions:

for activists, legal observers,
Contact legal aid organizations for updates and involvement (suggested)
Stay informed and ready to take action based on developments (suggested)
</details>
<details>
<summary>
2023-05-08: Let's talk about Republican solutions to Texas.... (<a href="https://youtube.com/watch?v=hJwLZVampYY">watch</a> || <a href="/videos/2023/05/08/Lets_talk_about_Republican_solutions_to_Texas">transcript &amp; editable summary</a>)

Beau exposes Republican insincerity in advocating for mental health post-shootings, citing their lack of genuine commitment and using it as a facade for public image.

</summary>

"It's a lie, and I'm not going to help them flood the zone."
"They absolutely do not care about mental health, period."
"They have zero intention of doing anything about mental health."
"I don't believe that all of them had a sudden change of heart."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains why he doesn't support Republican solutions when advocating for mental health resources after a shooting.
Believes Republicans use mental health as a talking point without any intention of taking meaningful action.
Refuses to help Republicans spread lies about addressing mental health by discussing it right after a shooting.
Points out the lack of budget allocation and actual initiatives for mental health by the Republican Party despite controlling the House.
Questions the sincerity of public figures advocating for mental health who justify instances like Jordan Neely's killing.
Stresses that Republicans use mental health as a facade to appear caring, but their actions demonstrate otherwise.
Urges viewers to look into public figures' stances on mental health and compare them to their justifications for certain events to see the lack of genuine concern.
Beau advocates for discussing mental health reform at other times except immediately following a shooting.
Calls out the hypocrisy and lack of genuine concern from Republicans regarding mental health issues.
Encourages critical thinking about political rhetoric and actions related to mental health advocacy.

Actions:

for advocates for mental health,
Fact-check public figures' past statements on mental health and their justifications for certain incidents (implied).
Advocate for genuine mental health reform in your community (suggested).
</details>
<details>
<summary>
2023-05-07: The roads to tools for relief and aid (<a href="https://youtube.com/watch?v=Lu7iWMi_tck">watch</a> || <a href="/videos/2023/05/07/The_roads_to_tools_for_relief_and_aid">transcript &amp; editable summary</a>)

Beau shares his experience with Hart Tools, finding them worth trying out for relief work due to their affordability and functionality, despite not being commercial grade.

</summary>

"Hart Tools are definitely worth trying out."
"If you are looking to get the tools to help out, it's a brand that'll work."
"They are not commercial grade, but they will work."
"It's a good value. Just manage your expectations."
"Get this stuff, a different circular saw, get a bunch of batteries, and then get a solar generator."

### AI summary (High error rate! Edit errors on video page)

Many people who want to help with relief efforts lack the necessary tools, as they can be expensive and inaccessible for those not making a living with them.
There is a lot of classism surrounding tools, with certain brands being deemed superior and expensive.
Hart Tools gained attention in 2021 for being inexpensive and made by a major manufacturer, sparking rumors about their quality.
Beau personally tested and used Hart Tools, finding them to be worth trying out for disaster relief work.
The Hart chainsaw is described as mid-grade, adequate for cutting and performing its functions, but the batteries drain quickly and it is very light.
The reciprocating saw from Hart Tools is praised for being light and durable, even surviving being submerged once.
Beau expresses a dislike for the circular saw from Hart Tools due to its small size and limitations in cutting capacity.
The impact tool that came as part of a set is mentioned as not being extensively used by Beau, but it performed fine in the few instances it was used.
While the Hart drill is not considered commercial grade, it is still functional and not comparable to cheaper brands that may not last.
Overall, Beau finds that Hart Tools are a good value for the cost, especially for those looking for tools to have on hand for relief efforts or for hobbyists.

Actions:

for diy enthusiasts, disaster relief volunteers,
Purchase Hart Tools for disaster relief efforts (suggested)
Get a solar generator as part of your relief kit (suggested)
Manage your expectations when using the tools (implied)
</details>
<details>
<summary>
2023-05-07: Let's talk about races, rockets, and Ukraine.... (<a href="https://youtube.com/watch?v=vIagohTIomI">watch</a> || <a href="/videos/2023/05/07/Lets_talk_about_races_rockets_and_Ukraine">transcript &amp; editable summary</a>)

Beau explains how a Patriot system can intercept a Russian hypersonic missile by predicting its path, not matching its speed, debunking misconceptions and addressing concerns about defense and nuclear warfare.

</summary>

"The Patriot doesn't have to go as fast as the missile. It just has to calculate where the missile is going to be and then put its missile there at the same time."
"It's not a race. The Patriot is already where the missile is headed."
"If it hasn't happened, it will. These things aren't wonder weapons."
"The speed at which the warheads are delivered is the least of your worries because it means major powers are exchanging nukes."
"Because it's not a race. It's that simple. The Patriot is already where the missile is headed."

### AI summary (High error rate! Edit errors on video page)

Exploring the possibility of Ukraine shooting down a Russian hypersonic missile with a Patriot system.
Clarifying misconceptions about the speed dynamics involved in such a scenario.
Emphasizing that the Patriot system doesn't need to match the speed of the missile but accurately intercept its path.
Suggesting that repeated use of hypersonic missiles by Russia might diminish their effectiveness.
Pointing out that the hype around hypersonic missiles could be a tactic to increase defense spending.
Noting that the reputation of the Patriot system has evolved since the first Gulf War.
Expressing concerns about the implications of hypersonic missiles in nuclear warfare.
Mentioning ongoing developments of countermeasures against hypersonic missiles.
Summarizing that the Patriot intercepts by predicting the missile's path, not by matching its speed.

Actions:

for defense analysts,
Develop or support technologies for countering hypersonic missiles (implied)
Stay informed about advancements in defense systems and technologies (implied)
</details>
<details>
<summary>
2023-05-07: Let's talk about Ukraine, Russia, and estimates.... (<a href="https://youtube.com/watch?v=Jf46U0J0qsQ">watch</a> || <a href="/videos/2023/05/07/Lets_talk_about_Ukraine_Russia_and_estimates">transcript &amp; editable summary</a>)

US estimates Russia's limitations in Ukraine, foresee prolonged conflict with no speedy resolution in sight.

</summary>

"Putin understands he can't take the whole country."
"The conflict may not end before the end of the year."
"The conflict may not end before the end of the year."
"The troops will not be home by Christmas."
"Ukraine has a pretty good track record of surprising everybody."

### AI summary (High error rate! Edit errors on video page)

The US provided an overview of the situation in Ukraine and developments in Russia.
Putin understands he can't take the whole of Ukraine but aims to solidify control in occupied areas.
US intelligence suggests Russia will attempt to restock and renew its offensive later.
The boss of Wagner expressed frustration at Russian generals for failing to supply ammo.
Political infighting in Russia is increasing and becoming more public and dramatic.
Russia is unlikely to launch a major offensive this year without new partners for weapons.
US estimates indicate Ukraine cannot achieve its goals through military means alone.
The US has historically overestimated Russia and underestimated Ukraine.
Ukrainian forces' adaptability and integration of weapon systems are key strengths.
US estimates are based on lengthy timelines, suggesting a prolonged conflict.
Christmas is not a realistic timeline for the troops to return home.
The conflict may not end before the end of the year, with surprises possible from Ukraine.

Actions:

for foreign policy analysts,
Stay informed on developments in Ukraine and Russia (implied)
Support diplomatic efforts for a peaceful resolution (implied)
</details>
<details>
<summary>
2023-05-07: Let's talk about Bryan Slaton, Texas, expulsion, and a report (<a href="https://youtube.com/watch?v=MMKzqw26SZQ">watch</a> || <a href="/videos/2023/05/07/Lets_talk_about_Bryan_Slaton_Texas_expulsion_and_a_report">transcript &amp; editable summary</a>)

Texas State House committee investigates Rep. Slayton, likely to recommend expulsion for providing alcohol to young women.

</summary>

"The report is apparently going to recommend his expulsion."
"On top of all of that you know there's the alcohol thing, there's the abusive capacity, there's the official oppression."
"It's worth noting that the last time somebody was expelled was 1927, almost a hundred years ago."

### AI summary (High error rate! Edit errors on video page)

Texas State House committee is investigating Representative Brian Slayton.
The report likely recommends his expulsion due to assisting young women under 21 with alcohol.
Rumors suggest Slayton was encouraged to resign but did not.
The committee found the young woman could not effectively consent to the alcohol consumption.
Slayton allegedly tried to prevent his young staff from disclosing information.
The House is expected to vote on his expulsion soon, a rare occurrence since 1927.
The issue will likely dominate Texas headlines until resolved.

Actions:

for texans, lawmakers,
Contact local representatives or organizations to voice opinions on the expulsion (implied)
</details>
<details>
<summary>
2023-05-07: Let's talk about 2 question about Texas from Germany.... (<a href="https://youtube.com/watch?v=oMfdDRuOXN0">watch</a> || <a href="/videos/2023/05/07/Lets_talk_about_2_question_about_Texas_from_Germany">transcript &amp; editable summary</a>)

Beau addresses stereotypes about Texas, explains why people didn't shoot back in a recent incident, criticizes politicians for failing to act on gun control despite public support, and warns of potential unintended consequences of sharing disturbing footage.

</summary>

"Most people are not killers."
"It's not a matter of shifting thought when it comes to the things that at least I think would really be effective."
"Their intractability on that is probably going to lead to incredibly restrictive gun legislation."
"Their own rhetoric, buying into it, becoming so immovable on the subject, is probably going to lead to firearm restrictions."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing the stereotypes about Texas and guns, Beau points out that not everyone in Texas carries a gun and explains why people didn't shoot back during a recent incident.
Most people are not killers and not inclined to respond actively in a dangerous situation, even if they have firearms.
Beau mentions the high level of support in Texas for certain gun control measures, such as raising the age to buy firearms and allowing courts to take firearms from those deemed a threat.
Despite the public support for such measures, Beau criticizes politicians for not taking action and instead catering to a specific identity and party loyalty.
He questions whether the disturbing footage of incidents like the one in Texas will actually lead to meaningful change or just desensitize people, drawing a parallel with driver's education videos.
Beau ultimately leaves the decision of using such footage to the families involved, acknowledging the sensitivity and ethical considerations around its distribution.
He expresses uncertainty about how to bring about a shift in the Republican Party's stance on gun control, particularly in states like Texas.
Beau points out the irony that the current lack of reasonable gun regulations could eventually lead to stricter legislation, contrasting it with the potential positive impact of taking proactive measures.
He suggests that if politicians made reasonable efforts to address gun violence, it could potentially reduce incidents and prevent extreme restrictions on firearms in the future.

Actions:

for politically aware individuals,
Contact local representatives to advocate for reasonable gun control measures (implied)
Join community organizations working towards sensible gun regulations (implied)
</details>
<details>
<summary>
2023-05-06: Let's talk about the value of reaching out to the other side.... (<a href="https://youtube.com/watch?v=skgaaSAD9nc">watch</a> || <a href="/videos/2023/05/06/Lets_talk_about_the_value_of_reaching_out_to_the_other_side">transcript &amp; editable summary</a>)

Beau addresses skepticism towards reaching conservatives, advocating for outreach to independents with conservative leanings to shift societal perspectives.

</summary>

"Trying to reach people is a fundamental aspect of effecting change."
"To change society, you don't have to change the law, you have to change the way people think."
"I am not a Democrat."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the skepticism towards reaching out to conservatives due to their perceived unwavering support for MAGA and conservative beliefs.
He points out that trying to reach people is a fundamental aspect of effecting change and not doing so means accepting the status quo.
Beau presents data showing that a significant portion of the population identifies as independents, hinting at a conservative lean among them.
He notes that many conservatives may identify as independents rather than Republicans or MAGA supporters, making them a potential group for outreach.
Beau draws parallels between how rural people are overlooked by Democrats despite sharing left-leaning economic principles and suggests they can be reached through engagement.
By looking at voter registration numbers, Beau shows that there is a substantial number of independents who may lean conservative and are open to being influenced.
Beau clarifies that while his beliefs mostly resonate with the Democratic Party and differ from the MAGA version of Republicans, he does not identify as a Democrat.
He stresses the importance of reaching out to people to shift societal perspectives, stating that changing minds is more impactful than changing laws.

Actions:

for community members, activists, voters,
Reach out to independents with conservative leanings for constructive dialogues and engagement (suggested)
Engage with individuals holding differing political views to foster understanding and influence societal change (implied)
</details>
<details>
<summary>
2023-05-06: Let's talk about Trump being told to hush in the hush money case.... (<a href="https://youtube.com/watch?v=_uJJTSrzzy0">watch</a> || <a href="/videos/2023/05/06/Lets_talk_about_Trump_being_told_to_hush_in_the_hush_money_case">transcript &amp; editable summary</a>)

The district attorney prohibits Trump from discussing evidence, foreseeing trouble, while Trump navigates legal maneuvers for the 2024 trial.

</summary>

"Trump may end up in trouble by using evidence obtained via discovery to make statements."
"It is definitely a novel approach, and I think there may be some timeline errors as far as when Trump became president and when some of this happened."
"The trial is set to start during the 2024 primaries, which is really bad timing for Trump."

### AI summary (High error rate! Edit errors on video page)

The district attorney's office sought to prohibit Trump from speaking about any evidence obtained during the discovery process to prevent harassment.
The judge upheld the prohibition, but Trump can still talk about the evidence his side presents.
Trump may end up in trouble by using evidence obtained via discovery to make statements.
Trump is trying to have the case removed from New York State Court and sent to federal court, a unique approach.
There may be timeline errors regarding when events occurred in relation to Trump becoming president.
The trial is set to start during the 2024 primaries, creating bad timing for Trump.
Trump can't agree to any events during this time or delay the trial.
Trump might attempt to delay the trial until after the primary season and election if he makes it through the primary.
The hush money case is expected to be litigated over the next year, with slow progress and procedural developments.
We may not see significant results from the case until primary season is in full swing.

Actions:

for legal analysts,
Stay informed about the legal developments surrounding Trump's cases (implied)
Follow the progress of Trump's legal battles and how they unfold over time (implied)
</details>
<details>
<summary>
2023-05-06: Let's talk about Democrats going on the offensive.... (<a href="https://youtube.com/watch?v=UbXS2XcQdmk">watch</a> || <a href="/videos/2023/05/06/Lets_talk_about_Democrats_going_on_the_offensive">transcript &amp; editable summary</a>)

Democratic Party is strategically targeting vulnerable Republicans for the 2024 elections, aiming to leverage chaos within the Republican Party to push for stability and control under Biden's leadership.

</summary>

"Democratic Party going on offense for 2024 elections."
"Biden's campaign strategy is to showcase stability and control contrasted with Republican chaos."

### AI summary (High error rate! Edit errors on video page)

Democratic Party going on offense for 2024 elections.
Various PACs like League of Conservation Voters, Courage for America, and Protect Our Care targeting vulnerable Republicans.
Republican proposed budget not taken seriously, leading to chaos and uncertainty.
Biden's campaign strategy is to showcase stability and control contrasted with Republican chaos.
Impact of PACs may not heavily influence the 2024 outcome but could pressure vulnerable Republicans to distance from McCarthy.
Possibility of House Democrats having a secret strategy to gain Republican support for their agenda.
Democratic Party investing in billboards, polling, and ads to pressure vulnerable Republicans.
Goal is to push vulnerable Republicans to present a budget they truly support amidst the chaos.

Actions:

for political activists and voters.,
Contact vulnerable Republicans in your district and express concerns about the proposed budget and debt ceiling (implied).
Support PACs like League of Conservation Voters, Courage for America, and Protect Our Care in their efforts to pressure vulnerable Republicans (implied).
</details>
<details>
<summary>
2023-05-06: Let's talk about Biden just ordering the debt paid.... (<a href="https://youtube.com/watch?v=xqxB-3YKUqk">watch</a> || <a href="/videos/2023/05/06/Lets_talk_about_Biden_just_ordering_the_debt_paid">transcript &amp; editable summary</a>)

Beau explains why willingly defaulting on debts is unconstitutional in the U.S., citing key constitutional clauses and potential economic repercussions.

</summary>

"You can't willingly default."
"The logic that is presented by the people that are putting this out there. Yeah, it's it's solid."
"It's so close to the time when it would matter to an actual default that it shakes the economic system."
"U.S. economic system in many ways runs on faith."
"To me and I think to most people who have actually read the Constitution, you can't willingly default."

### AI summary (High error rate! Edit errors on video page)

Explains the constitutionality of default and the debt ceiling in the United States.
Cites Article 1, Section 8, and the 14th Amendment, Section 4 of the U.S. Constitution to argue against the legality of a debt ceiling.
Emphasizes that all debts discussed were authorized by law, making defaulting unconstitutional.
Mentions the importance of Congress's power to lay taxes and pay debts in relation to the debt ceiling issue.
Refers to Federalist Papers Number 30 by Hamilton, suggesting that willingly defaulting is not an option.
Raises the question of why President Biden doesn't just pay the debts, explaining that it could lead to legal challenges and doubts about payment.
Stresses that the U.S. economic system relies heavily on faith, which could be shaken by legal challenges to debt payments.
Expresses concern over the potential partisan decisions of the Supreme Court and the negative impacts on the economy.
Suggests that Biden should have sought a finding from the Supreme Court earlier to avoid economic instability.
Concludes with the belief that willingly defaulting is not permissible under the Constitution and criticizes those who support defaulting.

Actions:

for policy analysts, lawmakers, activists,
Seek legal guidance on constitutional aspects of default (implied)
Advocate for transparency and adherence to constitutional principles in debt payment (implied)
</details>
<details>
<summary>
2023-05-05: Let's talk about the right vs Disney.... (<a href="https://youtube.com/watch?v=Cfx-6KLcBPE">watch</a> || <a href="/videos/2023/05/05/Lets_talk_about_the_right_vs_Disney">transcript &amp; editable summary</a>)

The Republican Party's attacks on American icons like Disney risk alienating their working-class base and may lead to significant pushback.

</summary>

"Elephants are afraid of mice. They probably should be."
"This is going to be like the InBev thing, only worse."
"They might just better leave that mouse alone."
"They're going to hit traditions that are just, they're not worth giving up for the social media clicks."
"It's a unique thing but it shows how the Republican Party, you know they lost touch with the youth of the country."

### AI summary (High error rate! Edit errors on video page)

The right wing in the United States is becoming increasingly critical of Disney, a company that many working-class families cherish as a symbol of Americana.
The Republican Party, historically able to portray themselves as a party for the working class, is now attacking American staples like Disney, alienating their base.
Disney, through its ownership of various icons and symbols, has become intertwined with the very fabric of American culture.
Beau observes a truck with a bumper sticker saying "F Disney" while also featuring a mural with the Punisher skull, a character owned by Disney through Marvel.
The irony of attacking Disney while driving a vehicle adorned with symbols owned by the company is not lost on Beau.
The disconnect between the Republican Party and the traditional American values they claim to uphold is becoming increasingly apparent.
Beau predicts that the Republican Party's attacks on iconic symbols like Disney may lead to pushback from their working-class base.
The potential backlash from attacking cherished American symbols could prove detrimental for the Republican Party's image.
Beau humorously suggests that elephants, symbolizing the Republican Party, should perhaps fear the mouse (Disney) they are targeting.
The transcript ends with Beau signing off, leaving the audience to ponder the implications of attacking symbols deeply ingrained in American culture.

Actions:

for working-class americans,
Support initiatives that uphold American values and traditions in your community (implied).
Encourage political representatives to focus on issues that truly benefit working-class families (implied).
</details>
<details>
<summary>
2023-05-05: Let's talk about how a fake debt ceiling can hurt.... (<a href="https://youtube.com/watch?v=g-fUu8o1ygM">watch</a> || <a href="/videos/2023/05/05/Lets_talk_about_how_a_fake_debt_ceiling_can_hurt">transcript &amp; editable summary</a>)

Beau explains how the self-imposed debt ceiling, though not real, has significant real consequences on the economy and people's livelihoods, criticizing the political game-playing around it.

</summary>

"The debt ceiling is not real, but make no mistake, it has very real consequences."
"It's fake. They made it up. But if they stick to it, if they don't raise the debt ceiling, make no mistake about it, the average American will suffer for their talking point."
"Politicians, particularly Republican politicians, are real big about using a household analogy to describe the government, which doesn't make any sense."
"The ultimate irony being that the people that will suffer the most will probably be people in red states."
"It's all a game."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the debt ceiling and its real consequences despite being a self-imposed, arbitrary limit.
Compares the debt ceiling to a situation where you and your partner decide to spend only $1000 of a $15,000 credit limit, even though you have access to more.
Points out that politicians, particularly Republicans, like to use a household analogy to describe the government, which doesn't make sense but is commonly done.
Emphasizes that the debt ceiling is not real but has very real consequences on the economy and people's livelihoods.
States that a default due to the debt ceiling could lead to economic instability, stock market losses, and a significant number of job losses.
Mentions that the 8 million job loss scenario is a doomsday situation and even playing brinkmanship could result in around 200,000 job losses.
Criticizes the political game-playing around the debt ceiling and budget negotiations, which ultimately harm the average American.
Raises concern about the suffering caused by not raising the debt ceiling, especially for those in red states.
Concludes by expressing concern over the situation and its impact on the American people.

Actions:

for american citizens,
Contact your representatives to urge them to prioritize raising the debt ceiling and avoid harmful economic consequences (suggested).
Join advocacy groups or organizations working on economic policy issues to stay informed and take collective action (exemplified).
</details>
<details>
<summary>
2023-05-05: Let's talk about Biden, the FBI, and Republican pursuits.... (<a href="https://youtube.com/watch?v=IiJHncOJl5w">watch</a> || <a href="/videos/2023/05/05/Lets_talk_about_Biden_the_FBI_and_Republican_pursuits">transcript &amp; editable summary</a>)

Two Republicans seek dirt on Biden through requesting FD-1023 forms from the FBI, prompting Beau to advocate for full transparency to prevent political manipulation of the agency.

</summary>

"Release it all."
"Don't just release the FD-1023."
"Prohibits politicians from weaponizing the FBI."
"Another Republican Scooby-Doo mystery."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Two Republicans in Congress have requested all FD 1023 forms related to the term "Biden" from the FBI, aiming to dig up dirt on President Biden.
The FD-1023 forms are used by the FBI to record notes from meetings, even if the information is unverified.
Beau suspects that the Republicans may be trying to create a scandal, even though they might already know the allegations are false.
Beau suggests that all information, including the final disposition of the case, should be released to prevent political weaponization of the FBI.
He advocates for transparency to prevent future attempts at manipulating the FBI for political gain.
The speaker believes that this situation may turn out to be another Republican "Scooby-Doo mystery" with no substantial findings.
Regardless of the outcome, Beau calls for releasing all information to the public.

Actions:

for congressional watchdogs,
Contact representatives to advocate for full transparency and release of information (suggested)
Monitor the situation for updates and demand accountability from politicians (implied)
</details>
<details>
<summary>
2023-05-05: Let's talk about 3 trials, Trump, coincidence, reality, and Jaws.... (<a href="https://youtube.com/watch?v=P5eMU_QDW7Q">watch</a> || <a href="/videos/2023/05/05/Lets_talk_about_3_trials_Trump_coincidence_reality_and_Jaws">transcript &amp; editable summary</a>)

A jury's guilty verdict in a seditious conspiracy case involving the Proud Boys raises questions about organization, potential connections to Trump, and the strengthening case against those with White House access.

</summary>

"They were looking for a coincidence."
"Takes a lot of work to make a coincidence like that happen."
"It is worth noting these sentences, they're probably pretty lengthy."

### AI summary (High error rate! Edit errors on video page)

A jury has returned a guilty verdict in a seditious conspiracy case involving the Proud Boys, separate from previous cases involving the oath keepers.
Drawing parallels to the movie Jaws, where people were reluctant to accept a reality that could impact them economically.
Raises questions about the likelihood of multiple organizations coincidentally acting together for the same goal.
Speculates on whether there was a central organizer above these groups and potential connections to Trump.
Suggests that the special counsel's case against those with direct access to the White House may strengthen over time.
Points out that while details are still unclear, the pieces of the puzzle are starting to come together.

Actions:

for activists, investigators, concerned citizens,
Investigate potential connections between different groups and central organizers (implied)
Stay informed on updates related to ongoing investigations (implied)
</details>
<details>
<summary>
2023-05-04: Let's talk about what happens if we default.... (<a href="https://youtube.com/watch?v=rnkWq7QmKS0">watch</a> || <a href="/videos/2023/05/04/Lets_talk_about_what_happens_if_we_default">transcript &amp; editable summary</a>)

Beau explains the consequences of the Republican party not raising the debt ceiling and warns about job losses and economic recessions if action isn't taken quickly.

</summary>

"They have to make their move and they have to do it quick."
"Any hopes they have of going anywhere in 2024, they're gone."
"The blame will rest with the Republican Party."
"200,000 people lose their jobs because they can't get their act together."
"It's over now."

### AI summary (High error rate! Edit errors on video page)

Explains the potential consequences of the Republican party not raising the debt ceiling, with three different types of projections.
Points out that economic damage begins before an actual default occurs.
Criticizes the Republican party for using the debt ceiling as leverage and sending a budget to Biden that even Republicans don't want.
Describes the internal conflicts within the Republican party regarding budget cuts and their lack of seriousness in negotiations.
Warns about the economic impact: 200,000 job losses if it runs up to the brink, half a million in case of a short default, and eight million in a protracted default.
Notes that all scenarios lead to a recessionary period and that the government won't have resources to soften the blow without raising the debt ceiling.
Mentions McCarthy's failed attempt to use leverage and how time is running out for political games.
Emphasizes that blame will fall on the Republican party if the debt ceiling isn't raised and that they risk losing credibility for 2024.
States the necessity for the Republican party to act quickly to avoid Senate or House Democrats taking control of the situation.

Actions:

for politically engaged citizens,
Contact your representatives to urge them to prioritize raising the debt ceiling quickly (implied).
</details>
<details>
<summary>
2023-05-04: Let's talk about two Senate plans for the debt ceiling.... (<a href="https://youtube.com/watch?v=O4zZoHIbV8c">watch</a> || <a href="/videos/2023/05/04/Lets_talk_about_two_Senate_plans_for_the_debt_ceiling">transcript &amp; editable summary</a>)

Beau breaks down two Senate plans concerning the debt ceiling, from outright repeal to restructuring, with shifting bipartisan dynamics potentially favoring a new approach.

</summary>

"The debt ceiling isn't real."
"It is more like Visa gives you a credit limit of $100,000, and you and your partner decide, well, we're only going to spend $1,000 this month."
"Those people who are more concerned about their country than their party inside the Republican Party, they'd be more likely to go for it."
"The longer this goes on, the more leverage McCarthy loses."
"If he doesn't come up with something soon, Republicans may decide they want a different speaker."

### AI summary (High error rate! Edit errors on video page)

Explains two plans regarding the debt ceiling coming out of the Senate.
The first plan is to repeal the debt ceiling entirely, arguing it's not real and only leads to unnecessary brinksmanship.
Senator Brian Schatz from Hawaii is behind the idea of eliminating the debt ceiling to force Congress to handle the budget more responsibly.
The second plan, proposed by Senator Tim Kaine, suggests changing how the debt ceiling works by letting the president set it annually, with Congress having the power to block or alter it.
Kaine's plan aims to prevent the debt ceiling from being used for political leverage in budget negotiations.
Republicans might find Kaine's plan more appealing as it allows for party power retention while limiting the debt ceiling's manipulation.
The likelihood of Kaine's plan passing through the House seems higher due to potential bipartisan support from Republicans who prioritize national interests over party politics.
Both ideas are initial proposals, hinting at more developments to come in the future.
The longer the debt ceiling issue persists, the more leverage Republican leader McCarthy loses, potentially leading to pressure for a change in House leadership.
The Senate seems to be growing tired of the situation, indicating a shift in dynamics that may impact McCarthy's position as Speaker of the House.

Actions:

for politically engaged citizens,
Reach out to representatives to express support for a more responsible approach to handling the debt ceiling (suggested).
Stay informed about further developments regarding the debt ceiling proposals (implied).
</details>
<details>
<summary>
2023-05-04: Let's talk about some off Republican primary polling.... (<a href="https://youtube.com/watch?v=CR3xG6dE2Xg">watch</a> || <a href="/videos/2023/05/04/Lets_talk_about_some_off_Republican_primary_polling">transcript &amp; editable summary</a>)

Beau examines polling data on Republican primary voters' views on Trump and speculates on potential implications for his general election prospects.

</summary>

"85% said that they wanted a nominee who challenges woke ideas."
"Trump appears likely to win the primary, but his lack of support among the Republican Party at large could hinder his general election prospects."
"Trump may depress Republican voter turnout just by being Trump."

### AI summary (High error rate! Edit errors on video page)

Discussed polling data revealing primary voters' preferences.
Primary voters expressed wanting a nominee who challenges "woke" ideas, opposes gun regulations, claims Trump won in 2020, and angers liberals.
Trump has 24% definite support, 49% considering, and 27% not considering among Republican primary voters.
Notably, 27% of Republican primary voters are not considering voting for Trump.
Despite likely winning the primary, Trump faces decreasing support within the Republican Party at large.
A significant portion of Republicans not voting for Trump could lead to his inability to win the general election.
Speculation that Trump's association may prevent some Republicans from voting altogether.
Concerns about potential impact on Republican voter turnout if Trump remains in the race.
Acknowledges the need to monitor further polling data to see if the lack of enthusiasm for Trump persists.

Actions:

for political analysts, republican voters,
Watch for further polling data on Republican voter sentiments towards Trump (implied).
</details>
<details>
<summary>
2023-05-04: Let's talk about Twitter, free speech, and a new feature.... (<a href="https://youtube.com/watch?v=kFab_DqgU_U">watch</a> || <a href="/videos/2023/05/04/Lets_talk_about_Twitter_free_speech_and_a_new_feature">transcript &amp; editable summary</a>)

Under Musk's Twitter rule, compliance with government requests is 100%, and a new pay-per-article feature may worsen media literacy and hinder free speech.

</summary>

"Twitter has a 100% score of complying with surveillance and censorship requests."
"One of the real issues that we have in the US is media literacy and people being energized by a headline and not reading anymore."
"I don't think that's something that would foster a whole lot of free speech."

### AI summary (High error rate! Edit errors on video page)

Twitter, under Musk's rule, has a 100% compliance rate with surveillance and censorship requests from governments.
Musk aims to introduce a feature allowing major media companies to charge Twitter users per article read.
Beau is skeptical about major companies taking up this offer and believes it could worsen media literacy issues in the US.
The proposed pay-per-article feature could lead to people being misled by headlines without reading the full articles.
Beau questions whether this feature supports free speech and open discussion or if it hinders real debate.

Actions:

for social media users,
Stay informed about social media platform policies and how they impact free speech and information access (implied).
</details>
<details>
<summary>
2023-05-03: Let's talk about possible President Manchin.... (<a href="https://youtube.com/watch?v=mheVyeMLNso">watch</a> || <a href="/videos/2023/05/03/Lets_talk_about_possible_President_Manchin">transcript &amp; editable summary</a>)

Beau deciphers Senator Manchin's potential presidential run as a strategic move for more Democratic Party support in his Senate reelection bid, downplaying the likelihood of an actual third-party candidacy.

</summary>

"If he plays up the idea that he might run as a third-party candidate and somebody who might siphon off votes from Biden, he might have a little bit more leverage."
"I don't actually think that Manchin is going to launch a third-party candidacy. Even if he does, I don't think he's gonna pull any votes."
"He seems to be a pretty business-oriented person and he does seem to do what is in his own best interest."
"I think there's a big miscalculation going on with Manchin."
"At the end of the day, I think that this is all about trying to establish a position to get more campaign funding for the Senate race."

### AI summary (High error rate! Edit errors on video page)

Beau provides insights into the possibility of Senator Manchin running for president in 2024 and his ties to the centrist group No Labels.
Senator Manchin's potential strategic move to leverage the idea of running as a third-party candidate to gain more support from the Democratic Party for his reelection campaign is discussed.
Despite speculations, Beau believes that it is unlikely for Senator Manchin to launch a third-party campaign during a polarized election cycle dominated by the two major parties.
Beau points out that Senator Manchin's position as a Democrat, despite his Republican-leaning voting record, is valuable to the Democratic Party as it contributes to their Senate majority and legislative agenda setting.
The importance of campaign funding and support for Senator Manchin's Senate race, especially considering the popularity of his potential opponent, is emphasized.
Beau suggests that Senator Manchin may be overly focused on West Virginia's attitudes, potentially leading to a miscalculation regarding his political strategies.
The likelihood of Senator Manchin actually running for president and the impact of such a decision are questioned by Beau, who views it as more of a strategic positioning for campaign funding.
Overall, Beau sees Senator Manchin's actions as a means to secure support and funding for his Senate race rather than a serious presidential bid.

Actions:

for political observers,
Contact local Democratic Party chapters to inquire about their support for candidates (implied)
Stay informed about political developments and funding strategies in your state (implied)
</details>
<details>
<summary>
2023-05-03: Let's talk about Texas, training, and too much.... (<a href="https://youtube.com/watch?v=d_Px833tzfs">watch</a> || <a href="/videos/2023/05/03/Lets_talk_about_Texas_training_and_too_much">transcript &amp; editable summary</a>)

Proposed legislation in Texas aims to train third graders as combat medics, reflecting deeper societal issues and glorification of violence.

</summary>

"You're asking third graders to become combat medics. That's what this is for."
"If you vote for this, you never get to talk about a book in a library again."
"If you're a politician and you stand up there showing off your AR, you're a part of the issue even if you vote to teach them how to plug the holes."

### AI summary (High error rate! Edit errors on video page)

Explains the legislation HB 1147 in Texas that requires schools to provide bleeding control station training annually.
Criticizes the legislation for aiming to turn students into combat medics rather than teaching basic first aid.
Argues that the training proposed for third graders is unrealistic and too intense.
Shares his personal experience of teaching his son first aid skills due to living far from a hospital.
Challenges the effectiveness of the proposed training and suggests it cannot be the best solution.
Emphasizes the glorification of violence in society and how it contributes to the issues being faced.
Condemns the idea of teaching third graders combat medic skills while not addressing underlying societal problems.
Calls out politicians for glorifying violence and contributing to the issue.

Actions:

for educators, policymakers, parents,
Advocate for realistic and age-appropriate emergency training in schools (implied)
Challenge legislation that may not address the root causes of societal issues (implied)
Prioritize nurturing creativity and understanding over glorification of violence (implied)
</details>
<details>
<summary>
2023-05-03: Let's talk about Texas, the Constitution, and Pennsylvania.... (<a href="https://youtube.com/watch?v=j55HN1X3xfQ">watch</a> || <a href="/videos/2023/05/03/Lets_talk_about_Texas_the_Constitution_and_Pennsylvania">transcript &amp; editable summary</a>)

Beau dismantles misconceptions about religion in schools, citing historical treaties and legal precedents while addressing fear-mongering around Satanist literature.

</summary>

"If that legislation goes forward, they put the Ten Commandments into the classroom, I assure you Satanist literature will be in there alongside it."
"The United States was not founded as a Christian nation."
"The Constitution protects the free exercise of all religions, any religion, not just yours."
"Your law of unintended consequences is just a slippery slope fallacy. No, it's cause and effect."
"Y'all have a good day. Thank you."

### AI summary (High error rate! Edit errors on video page)

Explains the message he received regarding the Texas Constitution and Satanist literature in classrooms.
Clarifies that he did not insinuate but outright stated that if Texas allowed the Ten Commandments in class, Satanist literature will follow.
Disputes the claim that the United States was founded as a Christian nation, referencing the 1797 Treaty of Tripoli.
Addresses the misunderstanding about the Constitution protecting only Christian religions, outlining that it safeguards all religions.
Distinguishes between the law of unintended consequences and cause and effect regarding religion in schools.
Mentions a federal court decision in Pennsylvania allowing the after-school Satan Club alongside the Christian evangelical club.
Notes similar cases in Virginia where the Satanic Temple prevailed in gaining access to schools for their club.

Actions:

for students, educators, activists.,
Support and advocate for inclusive religious education in schools (suggested).
Stay informed about legal battles surrounding religious clubs in schools (suggested).
</details>
<details>
<summary>
2023-05-03: Let's talk about Montana, Missouri, and outcomes.... (<a href="https://youtube.com/watch?v=mC5eBX_4KOI">watch</a> || <a href="/videos/2023/05/03/Lets_talk_about_Montana_Missouri_and_outcomes">transcript &amp; editable summary</a>)

Missouri and Montana face battles over gender-affirming care bans and silencing of a trans representative, raising concerns about abuse of power and representation.

</summary>

"Missouri's ban on gender-affirming care has a temporary restraining order."
"The legislature is attempting to silence Zoe Zephyr because she hurt their feelings."
"Denying representation based on offensive speech is a problematic power play."
"More lawsuits against the legislature's actions targeting Zoe Zephyr are anticipated."
"The situation raises concerns about the abuse of power and suppression of representation."

### AI summary (High error rate! Edit errors on video page)

Missouri's ban on gender-affirming care has a temporary restraining order, set to expire on May 15th with a hearing on May 11th.
In Montana, trans representative Zoe Zephyr is being targeted by the legislature, similar to Tennessee, for speaking against a ban on gender-affirming care.
The legislature is attempting to silence Zoe Zephyr because she hurt their feelings by warning about the ban's detrimental effects.
There are protests and legal actions to ensure Zoe Zephyr can represent her constituents.
A lawsuit has been filed, claiming people are being denied adequate representation by silencing Zoe Zephyr.
The likelihood of Zoe Zephyr's lawsuit succeeding is high, but it may not be a quick process.
A historical case involving Julian Bond in Georgia shows legislators can't exclude representatives for expressing unpopular opinions.
The situation in Montana mirrors the Julian Bond case and may result in a similar legal outcome.
Judges in Montana may sympathize with legislators' hurt feelings, potentially affecting the case's progress.
Denying representation based on offensive speech is a problematic power play by the legislature.
Republicans in power likely aim to avoid negative commentary rather than being influenced by Zoe Zephyr's statements.
More lawsuits against the legislature's actions targeting Zoe Zephyr are anticipated.
Beau predicts further legal actions from constituents in addition to Zoe Zephyr.
The situation raises concerns about the abuse of power and suppression of representation.
Expect ongoing legal battles and challenges to ensure fair representation for marginalized communities.

Actions:

for advocates for marginalized communities,
Support legal actions challenging the legislature's suppression of Zoe Zephyr's representation (implied).
</details>
<details>
<summary>
2023-05-02: Let's talk about the CNN Trump town hall.... (<a href="https://youtube.com/watch?v=o97piktflaE">watch</a> || <a href="/videos/2023/05/02/Lets_talk_about_the_CNN_Trump_town_hall">transcript &amp; editable summary</a>)

CNN risks credibility by hosting Trump without rigorous fact-checking, potentially losing viewers in pursuit of ratings.

</summary>

"Trump is not somebody that you can put on stage without fact-checking."
"CNN risks losing credibility if they allow defamatory statements without fact-checking."
"This was a bad move by CNN, and if they don't play it just right, they'll be paying for it for a long time."

### AI summary (High error rate! Edit errors on video page)

CNN announced hosting Trump for a town hall, sparking controversy over their rightward lean.
Concerns raised over CNN potentially becoming like Fox News if they do not fact-check Trump's claims on the spot.
Fear that Trump will spread false claims and new inventions without pushback from CNN.
CNN risks losing credibility by allowing defamatory statements without fact-checking.
Suggests that if CNN lets Trump answer questions without pushback, their ratings could plummet.
Beau questions the wisdom of giving Trump a town hall platform, especially without adequate fact-checking.
Acknowledges Trump as the Republican frontrunner but insists on the necessity of guardrails during the town hall.
Warns CNN of the potential long-term consequences if they mishandle the town hall with Trump.
Beau believes hosting Trump without rigorous fact-checking was a bad move by CNN pursued for ratings.
Urges CNN to be incredibly careful in handling Trump's claims during the town hall to avoid negative repercussions.

Actions:

for media consumers,
Fact-check statements in real-time during media events (implied)
Hold media networks accountable for allowing defamatory statements (implied)
Share concerns with CNN about the potential consequences of hosting Trump without proper fact-checking (implied)
</details>
<details>
<summary>
2023-05-02: Let's talk about a signal from Fox.... (<a href="https://youtube.com/watch?v=JtjOQd3bRs8">watch</a> || <a href="/videos/2023/05/02/Lets_talk_about_a_signal_from_Fox">transcript &amp; editable summary</a>)

Fox News hints at potential limits on hosts' speech and increased fact-checking, possibly driven by financial concerns, as Tucker Carlson leaves the network.

</summary>

"So it certainly seems as though Fox is kind of signaling to everybody that they plan on instituting some limits."
"If Fox begins seeding itself more in the truth, that's probably good."
"Losing that kind of money isn't sustainable."
"I'm going to keep watching and as this expected shift occurs. If it does, I'll let you know."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson is no longer with Fox News, prompting speculation about changes at the network.
Fox News media reporter Howard Kurtz suggests that networks may start imposing limits on what hosts can say and insisting on fact-checking.
There is uncertainty about the extent of these potential limits and the effectiveness of fact-checking at Fox News.
Beau doesn't expect Fox News to suddenly become a paragon of journalistic ethics but hopes they will avoid spreading blatantly false information.
The potential changes at Fox News could be driven by financial considerations, as spreading false information could lead to significant legal costs.
Beau will continue to monitor the situation at Fox News and update viewers on any shifts that occur.

Actions:

for viewers,
Keep informed about the developments at Fox News and other media outlets (suggested).
Stay vigilant about the information spread by news networks and demand accuracy (implied).
</details>
<details>
<summary>
2023-05-02: Let's talk about Trump, charts, and a new line of inquiry.... (<a href="https://youtube.com/watch?v=2CeG2uONCOI">watch</a> || <a href="/videos/2023/05/02/Lets_talk_about_Trump_charts_and_a_new_line_of_inquiry">transcript &amp; editable summary</a>)

Reports suggest special counsel is investigating wire fraud allegations against Trump, potentially leading to significant consequences if the numbers hold true.

</summary>

"People familiar with the channel know that is something that just absolutely annoys me."
"He's in trouble."
"If he actually did what was alleged and those numbers are that high, he's in trouble."

### AI summary (High error rate! Edit errors on video page)

Reports suggest special counsel is looking into wire fraud in relation to Trump diverting or inappropriately requesting funding during attempts to change election outcome.
Uncertainty surrounds the specifics of what the special counsel is investigating beyond wire fraud.
Mention of potential sentencing for wire fraud brings up misconceptions around maximum sentences in the federal system.
Frustration expressed over media inaccuracies in reporting potential sentences, especially in civil rights cases involving law enforcement officers.
Offense levels and points determine sentencing ranges in the federal system, not just maximum sentences.
Severity in wire fraud cases is determined by the amount of money involved.
Speculation on potential sentence length for Trump if found guilty of wire fraud involving $250 million, estimating between 150 and 210 months.
Emphasis on the significant implications of a $250 million wire fraud case and the impact it could have on Trump, especially given his age.
Mention of the importance of paying attention to this new avenue of inquiry, even though it may not be as sensational as other cases being looked into.
Acknowledgment that if the allegations are true and the numbers are accurate, Trump could face serious consequences.

Actions:

for legal analysts,
Pay close attention to developments in the investigation into wire fraud allegations against Trump (suggested).
Educate others on the intricacies of federal sentencing guidelines to combat misinformation (exemplified).
</details>
<details>
<summary>
2023-05-02: Let's talk about McCarthy, McConnell, and Biden.... (<a href="https://youtube.com/watch?v=suPtQdyF9v0">watch</a> || <a href="/videos/2023/05/02/Lets_talk_about_McCarthy_McConnell_and_Biden">transcript &amp; editable summary</a>)

Beau dissects the debt ceiling issue, foreseeing a nail-biting scenario leading up to a potential clean bill passing as a likely resolution before June 1st.

</summary>

"A nail biter where everything is choreographed, it's going to come down to the wire, well at least it might."
"I think the most likely outcome is we're all gonna get real nervous as June 1st approaches and then a clean bill passes."

### AI summary (High error rate! Edit errors on video page)

Addressing the debt ceiling issue, the timeline change has moved up the debt ceiling hitting date to potentially June 1st.
President Biden has called for a meeting with congressional leaders on May 9th to navigate the situation.
McCarthy's proposed budget slash debt ceiling increase is widely unpopular, even within his own party.
McConnell appears uninterested in saving McCarthy, hinting at little inclination to intervene.
Possibilities of a deal between Biden and McCarthy seem slim due to challenges in garnering Republican support in the House.
The likely scenario involves a nail-biter situation leading up to a clean debt ceiling increase passing in the Senate.
McConnell's lack of involvement may signal enough Republican support for a clean debt ceiling bill in the Senate.
If the bill passes the Senate, it may face some delay in the House before eventual passage.
McCarthy's reputation and position as Speaker of the House could be at risk if a deal isn't reached swiftly.
The most probable outcome is heightened nerves approaching June 1st, followed by a clean bill passing and budget negotiations postponed.

Actions:

for us citizens, policymakers,
Contact your congressional leaders to express your concerns about the debt ceiling situation (implied)
Stay informed about updates on the debt ceiling issue and its potential impact on the economy (implied)
</details>
<details>
<summary>
2023-05-01: Let's talk about a message about terms and acceptance.... (<a href="https://youtube.com/watch?v=I6rSpB8SF78">watch</a> || <a href="/videos/2023/05/01/Lets_talk_about_a_message_about_terms_and_acceptance">transcript &amp; editable summary</a>)

Beau receives a message urging him to address terminology struggles among parents with trans kids, stressing the importance of support and respect, even through initial awkwardness.

</summary>

"All we really want is for the people who loved us yesterday to love us today and to get the basic amount of respect anybody else gets."
"People shouldn't be terrified at getting some term wrong. If you're trying, it's okay and we'll see it."
"You were always supportive even when you were awkward and you have a huge trans following."
"I hope you can come up with some way to deliver this well because I don't really know how to make this clear."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Receives a message thanking him for supporting a community in Georgia, mostly by city folks.
Urged to address terminology struggles among parents with trans kids, particularly older generations.
Requested to convey that it's okay to not be perfect with terminology immediately.
Acknowledged for being supportive despite past awkward moments and mistakes.
Emphasized the importance of love and basic respect for the trans community.
Encouraged to share the journey of improvement with terminology and support.
Stressed the need for loved ones to continue showing support and respect.
Expressed hope for a clear delivery of the message on understanding and acceptance.
Mentioned the idea of finding a way to spread the message effectively.
Ends with a message of positivity and a wish for a good day.

Actions:

for supportive individuals.,
Find ways to effectively deliver messages of understanding and acceptance (suggested).
</details>
<details>
<summary>
2023-05-01: Let's talk about Trump's silence on a certain topic.... (<a href="https://youtube.com/watch?v=nXtY6765KVU">watch</a> || <a href="/videos/2023/05/01/Lets_talk_about_Trump_s_silence_on_a_certain_topic">transcript &amp; editable summary</a>)

Beau reveals how the Trumps' investment interests outweighed their supposed belief in culture wars, exposing the hypocrisy within conservative circles.

</summary>

"They don't believe what they sell you."
"If it's between that culture war nonsense that has the Republican base super angry, yelling, screaming and running over cans and money, the money always is going to win."
"Those at the top, they don't actually care about that."

### AI summary (High error rate! Edit errors on video page)

Outlines a recent conservative uproar over a beverage company's novelty can.
Explains why Trump remained quiet during the uproar and why his son spoke out.
Mentions that Trump's financial disclosure revealed a significant investment in Anheuser-Busch InBev.
Notes that the Trumps did not actively participate in the culture war despite their investment in the company.
Suggests that culture wars are used to keep supporters busy, angry, and energized.
Emphasizes that those at the top do not genuinely care about the culture wars.
Points out the hypocrisy of Trump's investment in the company while calling for the boycott to stop.
Concludes that money always wins over culture war rhetoric for the Republican base.

Actions:

for political enthusiasts,
Divest from companies contradicting stated beliefs (suggested)
Stay informed on political motivations behind certain actions (suggested)
Support businesses that uphold values you believe in (suggested)
</details>
<details>
<summary>
2023-05-01: Let's talk about Tennessee, Missouri, and intervening.... (<a href="https://youtube.com/watch?v=gUjhRhMfdgY">watch</a> || <a href="/videos/2023/05/01/Lets_talk_about_Tennessee_Missouri_and_intervening">transcript &amp; editable summary</a>)

Tennessee and Missouri laws face federal scrutiny for violating constitutional rights, sparking proactive intervention and hope for marginalized communities.

</summary>

"The Department of Justice is saying people have a right to make their health care decisions with their family, with their doctors."
"It's gonna be really hard to say that DOJ is wrong."
"This is a good sign. They're not waiting for these laws to take effect."
"Hopefully help is on the way."
"I know that there are a whole lot of people genuinely worried about this."

### AI summary (High error rate! Edit errors on video page)

Tennessee and Missouri are facing potential federal government intervention due to exceeding their authority.
The Department of Justice is suing Tennessee over the ban on gender-affirming care, citing violation of the Equal Protection Clause of the US Constitution.
The law in Tennessee discriminates based on sex and trans status, denying equal rights to this demographic.
Missouri has a judge who has temporarily halted the enforcement of a similar law to gather more information.
The ACLU and federal government are expected to push back against these laws that violate constitutional rights.
There is concern and worry among many individuals about these laws taking effect.
The Department of Justice's arguments against the laws are strong, making it difficult for states to defend their positions.
Federal intervention in these cases signifies a potential path towards protecting the rights of marginalized communities.
The determination on the temporary restraining order in Missouri is expected by Monday at 5 p.m.
The actions taken by the federal government and ACLU show proactive measures to combat unconstitutional laws.

Actions:

for advocates, activists, citizens,
Contact local representatives to voice opposition to discriminatory laws (implied).
Stay informed about the legal proceedings and outcomes in Tennessee and Missouri (implied).
</details>
<details>
<summary>
2023-05-01: Let's talk about Abbott's wild west wanted poster.... (<a href="https://youtube.com/watch?v=g0te60cZIcg">watch</a> || <a href="/videos/2023/05/01/Lets_talk_about_Abbott_s_wild_west_wanted_poster">transcript &amp; editable summary</a>)

Governor Abbott's announcement of a reward for info on the deaths of five immigrants in Texas is criticized as a PR stunt lacking critical details or genuine intent for justice.

</summary>

"He's not trying to leverage his network, not to actually bring the criminal to justice."
"This whole thing is a PR stunt. Nothing more."
"The amount of the bounty. He's got that covered."
"No information whatsoever. No name, no picture, nothing."
"It's not just a way to signal to his base about that. This whole thing is a stunt."

### AI summary (High error rate! Edit errors on video page)

Governor Abbott announced a $50,000 reward for info on the criminal who killed five illegal immigrants in Texas.
The governor's statement unnecessarily emphasized the victims as "five illegal immigrants," sparking controversy.
The message was seen as a signal to the base, portraying the victims as different from Texans.
Beau criticizes the governor's PR stunt, pointing out the lack of key information in the message.
Despite trying to appear tough, the governor's message lacked critical details typically found on wanted posters.
The governor's communication strategy should include Spanish translations, considering Texas's demographics.
Beau suggests that the governor's approach seems more focused on a publicity stunt than actual justice.
The lack of information provided in the governor's message raises questions about the sincerity of the reward offer.
Beau implies that the governor's actions are more about optics and signaling rather than genuine concern for justice.
The entire ordeal is viewed as a performance rather than a genuine effort to apprehend the criminal responsible.

Actions:

for texans, advocates,
Advocate for inclusive and accurate communication strategies with diverse communities, suggested
Support efforts to seek justice for victims through genuine actions, exemplified
</details>
