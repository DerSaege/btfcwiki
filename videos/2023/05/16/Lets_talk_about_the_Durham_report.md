---
title: Let's talk about the Durham report....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uiXDTWuKSlw) |
| Published | 2023/05/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of the Durham Report and its lack of groundbreaking information compared to the Inspector General's report.
- Durham's conclusion that mistakes were made and the investigation shouldn't have proceeded due to reliance on questionable sources.
- The report did not uncover any significant crimes or deep state plots, with very few individuals facing charges.
- Criticizing those on social media who cry "treason" without understanding the legal definition.
- Durham's main recommendation: creating oversight for politically sensitive investigations to challenge confirmation bias.
- Beau's belief that this oversight should be extended to all investigations to combat groupthink.
- Acknowledging that the report won't likely have a long-lasting impact, especially in right-wing media circles.

### Quotes

- "It's a burger we've already eaten."
- "They relied too much on raw intelligence or information from sources that they really shouldn't have trusted."
- "Throughout the entire thing I think they only brought charges against three people and two of them were acquitted."
- "Because they have no clue what treason is."
- "That's how you avoid confirmation bias. That's how you avoid groupthink."

### Oneliner

Beau breaks down the Durham Report, revealing its lack of significant findings and suggesting oversight to combat bias in investigations.

### Audience

Political analysts, policymakers

### On-the-ground actions from transcript

- Establish oversight for politically sensitive investigations to challenge confirmation bias (implied)

### Whats missing in summary

Beau's detailed analysis and insights on the Durham Report's implications and potential for oversight in investigations.

### Tags

#DurhamReport #InspectorGeneral #Oversight #PoliticalBias #Investigations


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Durham Report.
We're gonna talk about what's in it.
Um, we're going to go over the recommendation.
We're going to just kind of go through it
in a little bit more calm manner
than I think most people are going through it.
The main question that people have is really, you know,
is this a nothing burger? No, it's not a nothing burger. It's a burger we've already eaten.
If you're familiar with what was in the Inspector General's report, it's basically the same thing.
Not a whole lot of, you know, ground breaking new information, kind of really is a rehash.
I mean, there's new little sidelines and stuff like that, but nothing major.
The difference between the two is, you know, the IG was like mistakes were made,
but the investigation should have gone forward, paraphrasing, simplifying.
simplifying. Durham, mistakes were made, shouldn't have gone for it. They relied
too much on raw intelligence or information from sources that they
really shouldn't have trusted. Maybe they should have vetted it more. That's
pretty much it if you're looking to simplify it. Was it the crime of the
century, quote, did it uncover the crime of the century? No, of course not. In fact,
given that, you know, after all of these years and 300 pages, there's not a
recommendation to indict anybody in the report. Not just did not uncover the
crime of the century, didn't actually uncover a crime. No deep state plot,
nothing like that. Throughout the entire thing I think they only brought charges
against three people and two of them were acquitted. So no giant plot, no
giant conspiracy. So why are people running around social media screaming
treason? Because they have no clue what treason is. How widespread are the
problems? Well given the fact that Durham didn't really recommend any massive
shifts in how the FBI conducts investigations, I mean you can you can
kind of look to that to see how widespread it is. There were a number of
little things that you know this could have stopped this, this could have stopped
that. Most of that was already addressed. In fact, I think all of it, but I'll say
most of it to cover myself. Most of that was already addressed from the IG report.
Now, Durham made one real recommendation. And I'm going to be honest, I think it's a
good one. In fact, I don't think it goes far enough, to be honest. But basically,
it's creating an office or a position that oversees politically sensitive
investigations and the job is to challenge the investigation the entire
time. And really it's just instituting a 10th man rule which if you're not
familiar with it, 10 people sitting at the table and going around giving their
opinions. If the first nine all agree, it's the duty of the tenth person to
disagree, even if they do agree. Yeah, I think that's a good idea. In fact, I think
that should be done with all investigations, not just politically
sensitive ones. That's how you avoid confirmation bias. That's how
you avoid groupthink. That's a good idea, and that's kind of the only
real recommendation in it. Most of the other stuff that's in it, we already knew that there
was no plot. Even if you side with Durham's version, basically it was bad investigating.
People were over eager. They were falling for confirmation bias, using bad judgment as an investigator, stuff like that.
But no giant conspiracy to get Trump. And that's Trump's guy. Well, not Trump's guy. That's not fair.
It's the person that Trump's guy picked.
So my guess is right-wing media will run with this for like a week and then it'll
fade away because they'll start to realize there really isn't as much in it
as they were hoping for. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}