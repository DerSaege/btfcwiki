---
title: Let's talk about learning from Lauren Boebert....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=p9-vNZl3FUQ) |
| Published | 2023/05/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lauren Boebert misinterpreted a graphic on CNN showing US border encounters since Title 42 ended.
- Boebert criticized CNN for showing a surge on the graphic when the numbers actually dropped.
- The graphic was in reverse chronological order, causing confusion for viewers used to reading left to right, top to bottom.
- Beau suggests that the graphic may have been intentionally misleading by not following expected chronological order.
- He mentions how truncated bar graphs can manipulate perception by showing only a portion of the data.
- Beau points out that media outlets sometimes use graphics to stir up trouble or reinforce preconceived notions.
- Despite Boebert's criticism, Beau acknowledges that misinterpreting such graphics is common.
- He urges viewers to take the time to process information from graphics and not jump to conclusions.
- Beau advises checking if the information is presented in context and reading the details carefully.
- He warns about the danger of graphics being used to reinforce preconceived notions rather than present facts accurately.

### Quotes

- "How stupid do they think the American people are?"
- "When you are looking at graphics, there's a bunch of things that can be done to alter the perception of something."
- "Make sure you actually take the time to process the information."
- "It's being done to reinforce a preconceived notion and that is the real danger."
- "When you see the graphics as the talking heads are up there stating their position, make sure that you take the time to process it and read the days of the week."

### Oneliner

Beau points out how a misleading graphic on US border encounters sparked criticism from Lauren Boebert, shedding light on the importance of processing information from graphics carefully to avoid falling into traps of misinformation.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Verify the information presented in graphics by checking multiple sources (implied).
- Take time to process information from graphics and avoid jumping to immediate conclusions (implied).
- Read details carefully and ensure the context is accurate before forming opinions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how graphics in media can manipulate perception and reinforce preconceived notions, urging viewers to be vigilant in processing information for accurate understanding.

### Tags

#Graphics #MediaLiteracy #Misinformation #CriticalThinking #USPolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about graphics
and how we process information
and what we can all learn from Lauren Boebert.
Okay, Boebert saw a graphic on CNN
and she misinterpreted the graphic
and she made a statement about it.
She was quickly corrected about her interpretation of it.
But the thing is, the graphic is bad.
The graphic was bad.
A lot of people might have made the mistake that she did.
So it's worth going over for once, really.
We can learn something from Lauren Boebert.
OK, so what did she say?
She has a picture of the graphic, which is showing US border encounters, and it's showing
the change since Title 42 ended.
And she says, still no surge, says CNN, as they show a surge on their graphic.
How stupid do they think the American people are?
Yeah.
Okay.
the graphic itself is in reverse chronological order. Most people, if you
grew up in the United States, you are English speaking, odds are when you are
reading something you read left to right, top to bottom. When you are looking at
something that is a graphic, you are expecting that information to be
organized and concise. You're expecting it to be in chronological order,
especially if it's detailing order of events, which this is, but that's not how
they did it. They didn't start before Title 42 ended. The graphic is
Saturday 4200, Friday 6300, Title 42 end, Thursday 9600, Wednesday 10,000 plus,
Tuesday, 10,400. So if you glance at this, what you see is the numbers
increasing. But the reality is it's in reverse. The numbers have dropped
substantially for whatever reason, despite all the fear-mongering. That
chaos, that surge, it has not appeared. So my guess is that she had a
preconceived notion that there was going to be a surge because there was
fear-mongering in the echo chamber that she's a part of. And she sees numbers
that confirm that because most people would expect this to be in chronological
order. And then I guess she had an issue discerning the days of the week when she
read it. But I think a lot of people could have made this mistake. When you
are looking at graphics, there's a bunch of things that can be done to alter the
perception of something. It's like a truncated bar graph. You see them all the
time and what they show is just the top of a bar graph so it makes it look like
there are these massive changes and things are just swinging back and forth
but in real life if you were to zoom out and show the entire graph at full
representation you'd realize it's running flat but that happens a lot too
you see these kind of graphics especially from outlets that are trying
to stir up trouble now it's CNN okay with them and their current issues this
could just be a bad graphic or it could be intentional we don't know they are
are trying to gather a wider audience. I would point out that it does not appear
to be going well. Primetime Friday they have dropped to below Newsmax as far as
viewership. So maybe the the swing right isn't actually going as according to
plan but while I understand it's Boebert and I understand the the statement that
was made how stupid do they think the American people are I understand that
that's that's just an invitation to be like well they expected him to know the
days of the week like I get it but this was a bad graphic and you see a lot of
this on the news. So when you are seeing any kind of graphic like this, make sure
you actually take the time to process the information. Read the days of the
week. Look to see if the bar graph actually does represent what's being
shown because there are a lot of times now, and you're seeing it a lot with some
breaking news today, where just elements of something are being presented or
they're being presented out of context or it's being done to reinforce a
preconceived notion and that is that's the real danger. So when you see the
graphics as the talking heads are up there stating their position, make sure
that you take the time to process it and read the days of the week. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}