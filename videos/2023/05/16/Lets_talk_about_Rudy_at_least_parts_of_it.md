---
title: Let's talk about Rudy, at least parts of it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=2_8ybHpr96Y) |
| Published | 2023/05/16 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Claims have surfaced about Rudy Giuliani, overshadowed by a recent report and media frenzy.
- Allegations from a 70-page complaint filed by a former employee include being promised a million dollars annually but receiving significantly less.
- An allegation suggests Giuliani and Trump were selling pardons for two million dollars.
- A whistleblower named John Kariakou reached out through Giuliani to obtain a pardon, with an associate quoting a $2 million cost.
- The complaint includes allegations of audio recordings of Giuliani using sexist, racist, and anti-Semitic language.
- The suit filed against Giuliani is for $10 million.
- Giuliani has denied all allegations and plans to seek legal remedy.
- The wide range of allegations makes this case particularly interesting.
- Media attention may resurface once there is movement on the case.
- Beau hints at the need to be prepared for the allegations and suggests reading up on them rather than just listening.


### Quotes

- "Allegations include being promised a million dollars annually but receiving significantly less."
- "The suit filed against Giuliani is for $10 million."
- "Media attention may resurface once there is movement on the case."


### Oneliner

Claims against Rudy Giuliani emerge, including selling pardons, with a lawsuit for $10 million, sparking potential media interest.


### Audience

Media consumers


### On-the-ground actions from transcript

- Read up on the allegations against Giuliani (suggested)
- Stay informed about the case developments (suggested)


### Whats missing in summary

Full details and nuances of the allegations against Rudy Giuliani can be best understood by watching the full video.


### Tags

#RudyGiuliani #Allegations #MediaAttention #LegalRemedy #Whistleblower


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some claims
that have arisen about Rudy, Rudy Giuliani.
The information came out yesterday,
but due to the release of the report
and the media frenzy that accompanied that,
it seems like it got overshadowed,
and I'm not sure if the media
is going to pick this story back up again.
But it is interesting, to say the least.
The claims arise from a 70-page complaint filed in New York by a woman who is allegedly
a former employee who says that she was promised a million dollars a year and got just a few
thousand of that. And she worked for him for two years. But that's not the wild
part. There's also the allegation that she is aware of him attempting to sell or
at least saying that he and Trump were selling pardons for two million dollars,
which is interesting because there's a whistleblower named John Kariakou who,
I guess, was trying to get a pardon and reached out through Giuliani, was trying
to go that route, and one of Giuliani's associates reportedly told him that it
would cost $2 million.
Even that's not the weirdest part.
Please keep in mind, I like to keep this channel
very family-friendly.
There's a whole bunch of allegations
that I will not be going over.
The person who filed the complaint
says that there are audio recordings of some of this
to include Giuliani using sexist, racist, anti-Semitic
language.
The lawyer seemed to indicate that there
were other methods of corroborating some of this
as well.
The allegations are wild.
So I feel like the media is probably
going to pick this back up again,
But I'm not sure if they're going to do it until there's
movement on the case.
This just seems like something people should be kind of prepped
for, because the allegations are definitely out there.
You might want to read them first, the part about him
wanting to be like Bill Clinton definitely caught my eye.
But even that isn't the strangest part of this.
The suit is for, I want to say, $10 million.
Giuliani has denied all of this and says
that he will be seeking legal remedy.
It's the wide range of allegations
that make this so interesting.
And I definitely can see this being a topic
as it moves forward.
And so this might be something you definitely
want to read up on rather than listen to,
especially if you have kids in the room.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}