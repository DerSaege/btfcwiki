---
title: Let's talk about fault, the faultless, and blame....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_B_JhrJSLhQ) |
| Published | 2023/05/29 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the movement within the GOP in Texas, Louisiana, and Nebraska to end no-fault divorce.
- Points out that ending no-fault divorce is about control and compelling a partner to stay.
- Shares personal experiences living in rural, conservative areas where men blamed no-fault divorce for their failed marriages.
- Emphasizes that every man he met who complained about no-fault divorce was actually at fault in the relationship.
- Clarifies that ending no-fault divorce doesn't mean the partner has to stay, but rather, it exposes the reasons for the divorce.
- Notes the shift from the past when women were compelled to stay due to difficulties in proving issues like abuse, which technology now supports.
- Criticizes the move to end no-fault divorce as a method to control and reinforce patriarchy without deserving privilege.
- Advises women to be cautious of men who support ending no-fault divorce, as it signifies numerous red flags about their attitudes towards relationships.
- Predicts that this issue may evolve into the next culture war due to societal pressures around marriage and divorce.
- Condemns the potential impact of ending no-fault divorce, especially for women and in terms of domestic violence and suicide rates.

### Quotes

- "It's not a red flag, it's a red flag factory."
- "This is a horrible move by mediocre men."
- "If you ever meet a guy and he says, you know, I'm in favor of ending no-fault divorce, run."
- "Because if you probe and ask any questions, you're going to get so many more red flags."
- "With everything that's going on, I have no idea how a woman can vote for any Republican at this point."

### Oneliner

Beau sheds light on the GOP's push to end no-fault divorce, exposing it as a control tactic that reinforces patriarchy and jeopardizes relationships and safety.

### Audience

Women

### On-the-ground actions from transcript

- Talk to friends and family about the importance of maintaining no-fault divorce laws (suggested).
- Support organizations that advocate for women's rights and safety in relationships (implied).

### Whats missing in summary

The full transcript provides detailed insights into the harmful implications of ending no-fault divorce laws, backed by personal experiences and societal observations.

### Tags

#NoFaultDivorce #GOP #Relationships #Patriarchy #Women'sRights


## Transcript
Well, howdy there, Internet people, Lidsbell again.
So today we're going to talk about fault,
and who's at fault, and who's not at fault,
and no fault, and reality,
because there are a bunch of men who,
I think they have the wrong impression of something.
We're gonna talk about no fault divorce,
because apparently the Republican Party,
Well, they're coming for that, too.
There are movements in Texas, Louisiana, and Nebraska,
at least, within the GOP, with the goal
of ending no-fault divorce.
For younger people, it's divorce, as you know it.
If no-fault divorce was ended, people
have to show cause to get divorced. Okay, so why do some Republicans want to end
no-fault divorce? I would guess that a large portion of them don't actually
understand what it would mean. And then the rest are just pathetic. This is
all about control. This is literally saying my wife wants to leave me. I need
the power of government to compel her to stay. Okay so first and foremost to the
guys okay because this is this is definitely men that want to end no fault
divorce. I am certain that there will be some women who come out in support of
this because it's good for clicks or whatever, but I have lived in super rural, super southern,
super conservative, super Christian places most of my life.
I've heard this conversation once or twice.
In my entire life, I have probably met, I don't know, somewhere between 12 and 15 men
have been divorced and who blame no fault divorce. Every single one of them
was at fault and I don't mean that in the way of oh well he wasn't trying hard
enough or anything like that. No fault divorce just means that she has to show
No cause.
Every single man I have ever met who complained and whined about no fault divorce provided
it.
No exceptions to this.
For those who think that this is something that is going to compel her to stay for a
long period of time, you're wrong.
That's not what it means.
What it means is rather than getting a divorce and splitting up and going your separate ways
and going on with your life, it means that everybody finds out about your girl on the
side.
Everybody finds out about your addiction, about your substance issue.
Everybody finds out about the time you hit her.
That's what it means.
doesn't mean that she has to stay. This isn't the 1950s and when I say that I
realize it could be taken in two different ways. I'm not talking about your
attitude, although that's from the 50s as well. I'm talking about technology. See
back then women were compelled to stay. They were forced to stay because it was
really hard to prove some of this stuff. Not anymore. Technology is on her side.
All it means is that everybody finds out why she left. That's what it means. And it's a court record. No lying about it.
Because that's the other thing with this. When I was younger, when I was single, I can remember a guy whining and
complaining about
about how his wife filed for divorce out of nowhere. This guy was out with us all
the time. I didn't even know he was married. But he was one of those who
complained about no-fault divorce, just letting her do it. And he complained
about that in front of his girlfriend.
This is just another method of control, of re-establishing patriarchy, of reinforcing
it, of trying to grant privilege towards men who don't deserve it.
I have never met a guy who complained about no fault divorce, who said, you know, I was
really trying.
I was trying to change, or we were really working on it.
Has never happened in my entire life.
It's always been one type of guy.
I am certain, because it's the internet, that there will be exceptions to that.
But in my entire life, I've never seen one.
Ladies, since this has been a very one-sided conversation here directed at one demographic,
if you ever meet a guy and he says, you know, I'm in favor of ending no-fault divorce, run.
Don't walk, run.
It's not a red flag, it's a red flag factory.
Because if you probe and ask any questions, you're going to get so many more red flags.
Why do you feel that way?
And then you're going to hear about everything that's wrong with women.
You're going to hear, oh, they just give up too easy.
They're really only after money.
And you're going to hear how he feels about you.
It's a huge red flag.
This will probably evolve into the next culture war because it's one of those things where
there aren't many people who will say, no, don't do that because they don't want to be
seen as somebody who is going to give up on their marriage. So it will be, they'll
use it as a way to other people and label them, particularly women.
This is, we talk about people voting against their interest all the time.
With everything that's going on, I have no idea how a woman can vote for any Republican
at this point.
This is going to be bad.
It's worth noting that once no fault divorce was instituted, domestic violence, suicide,
stuff like that, they all dropped.
This is a horrible move by mediocre men.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}