---
title: Let's talk about when to pick up the phone and call....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4897lmwKH7s) |
| Published | 2023/05/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the appropriate time to contact law enforcement, especially in violent situations where force may be necessary.
- Points out that calling 911 summons lethal force and there might be better alternatives in non-violent situations.
- Illustrates how every law is backed by the potential for lethal force, not necessarily the death penalty, using examples like littering.
- Emphasizes that non-violent actions can escalate to violence if not complied with by law enforcement.
- Shares a real-life incident where a native man calling Border Patrol about trespassing led to his death, showcasing the risks of involving armed individuals who may not understand the situation.
- Questions the effectiveness of law enforcement de-escalation when calling them might actually escalate the situation.
- Encourages considering alternative ways to handle situations rather than immediately involving armed authorities.
- Raises awareness about the potential consequences of involving law enforcement and urges for a more thoughtful approach to seeking help.

### Quotes

- "When you dial 911, you're summoning a lethal force."
- "Every law is backed up by that. It's an inherent part of it in the US because of how we justify things."
- "Using armed people who don't know what's going on, it's not normally a good idea."
- "Not just might you save somebody's life, stop somebody from being killed who didn't need to be killed. The life you save might be your own."
- "There are normally better ways to deal with things."

### Oneliner

Beau explains the risks of summoning lethal force by calling 911 and urges for thoughtful alternatives in non-violent situations to prevent unnecessary escalations involving armed authorities.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Question the necessity of involving law enforcement in non-violent situations (implied)
- Advocate for exploring alternative ways to handle conflicts before resorting to calling authorities (implied)

### Whats missing in summary

Beau's emotional delivery and real-life examples provide a compelling argument against immediately involving armed authorities in non-violent situations, urging for a more thoughtful and cautious approach.

### Tags

#LawEnforcement #LethalForce #De-escalation #CommunityPolicing #ArmedAuthorities


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about making calls
and when it's the appropriate time to make that call.
And what happens once that call is made,
and the chain of events that follows,
and we're going to respond to a message.
We're going to do this because over on the other channel,
The Roads with Beau, we did that Q&A recently.
And one of the questions in it was asking me
when I thought it was appropriate to contact law
enforcement.
And my basic answer was if it's a violent situation,
if it's a situation where force is something
that might be needed.
Other than that, it's probably handled a better way.
When you dial 911, you're summoning a lethal force.
There's probably a better alternative.
There aren't a whole lot of situations
that are actually helped by the introduction of armed people
who are typically ill-trained and don't actually
know what's going on.
So, I got this message.
You said that you thought the only time to dial 911 was if lethal force was justified.
Then said every law was backed up by the death penalty.
That's stupid.
Nobody's getting killed over littering or trespassing.
Okay.
I said every law is backed up by penalty of death, not the death penalty.
different things. It's funny because littering is normally one of the examples
that I use to explain this concept, littering or jaywalking. We'll use
littering. Okay, you litter. Okay, you drop something on the ground. A cop sees you.
What's he do? Writes you a ticket, right? What if you don't pay? If you do not pay
that ticket, what happens? There's a chain of dominoes that occurs through the
judicial system, but if you don't pay it, eventually there's going to be a warrant
for you, right? So at this point you have done no violent act, right? And they're
showing up to throw you in a cage. What if you don't go with them? What happens
next? Violence ensues. If you don't want to go with them bad enough, they're gonna
kill you. Make no mistake about it, there is not a law in this country that is so
trivial a cop won't kill you over. Every law is backed up by that. It's an
inherent part of it in the US because of how we justify things. Now, nobody is
getting killed over littering or trespassing. I don't know where you've
been the last few years but I mean there's a lot of cell phone footage that
would suggest otherwise. I happen to just type in cops kill and trespassing
because that was your example. This week a native man called Border Patrol
because he either saw or thought that maybe migrants were trespassing on his property.
The details on this really aren't clear yet, but Border Patrol showed up and by the details that are available,
not a whole lot of time passed before they killed him, the person who called.
using armed people who don't know what's going on, it's not normally a good idea.
There are normally better ways to deal with things.
You know, people, and this is especially true of liberals, they like to advocate for law
enforcement to de-escalate things, which, yeah, they should, absolutely.
a good idea, but if you're gonna call them knowing that they're probably not
going to de-escalate it, are you escalating it yourself? The situation
with the Border Patrol shooting, I'll follow it because it just seems wild to
me, but as far as this concept, it kind of takes it to a whole new level.
Not just might you save somebody's life, stop somebody from being killed who didn't need
to be killed.
The life you save might be your own.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}