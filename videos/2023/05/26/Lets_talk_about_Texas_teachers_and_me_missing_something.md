---
title: Let's talk about Texas teachers and me missing something....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fJi81XafZT8) |
| Published | 2023/05/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas lawmakers are upset that schools are not arming teachers despite their efforts to push for it.
- A science teacher in Texas is accused of shooting his sleeping son and stepdaughter during a domestic situation.
- Beau stresses the immense training required for individuals to immediately respond to critical situations like the one involving the teacher.
- He points out the significant link between domestic violence histories and potential future violent incidents.
- Despite teachers being viewed as protectors of children, Beau acknowledges that they are also human and may have flawed histories.
- Lawmakers advocating for arming teachers may inadvertently enable individuals with violent histories to bring weapons into schools.
- Beau argues against the idea of introducing more guns into school buildings as a solution.
- He advocates for keeping guns out of schools as a short-term solution to prevent potential tragedies.

### Quotes

- "The solution here is not more guns. It's not bringing more guns into the building. It's keeping them out."
- "Teachers are people. They're flawed, just like anybody else, and a percentage of them will have that history."
- "The level of training it takes to get somebody to that point is immense."
- "There is a huge link between DV histories and mass incidents."
- "This is a bad move. This is going to go bad."

### Oneliner

Texas lawmakers push to arm teachers, while a science teacher's alleged violent act raises concerns about the risks involved, stressing the importance of keeping guns out of schools.

### Audience

Texas Residents, Educators

### On-the-ground actions from transcript

- Advocate for policies that prioritize keeping guns out of schools (implied).

### Whats missing in summary

Importance of addressing domestic violence histories and the potential risks associated with arming teachers in schools.

### Tags

#Texas #ArmingTeachers #GunControl #DomesticViolence #SchoolSafety


## Transcript
Well, howdy there, internet people, it's Bob again.
So today, we are going to talk about Texas again
and something that is going on there
and me missing something
because I've provided commentary on this before,
but like anybody who provides commentary,
I did it through my lens, what I knew about.
Sometimes when you're doing that,
you can overlook something
that is just painfully obvious when you see it later.
And that's what's happened here.
So in Texas, you have some lawmakers
who are upset because they went through all the steps
and put out all the rhetoric, arming teachers
and all of that stuff.
And then they found out that a whole lot of schools,
they're not taking them up on it.
A majority aren't.
So they're kind of doubling down on it.
And as I'm reading about this,
I stumble across a different article, a different story.
And I read it because it's something else
that interests me.
It's about a domestic situation.
The suspect in it, he had an altercation with his wife.
Then he went into their sleeping son's room and shot him.
And then he shot their stepdaughter.
Those are the allegations.
He's a teacher.
He's a science teacher in Texas.
When I have talked about this before, I have always talked
about the lens that I know about, the training aspects,
and how hard it is to get somebody to the level that they
can immediately respond in a situation like this because it's not like when the
cops show up a few minutes later. It's right then in the moment. The level of
training it takes to get somebody to that point is immense. It's a lot.
Another thing that I have talked about a lot on this channel is how there is a
huge link between DV histories and mass incidents.
That kind of violence is a really good indicator of future violence.
But I think like a whole lot of people look at teachers as the people who in some cases
literally shield the kids.
really considered them a threat. But at the end of the day, teachers are people.
They're flawed, just like anybody else, and a percentage of them will have that
history. And with these policies in effect, there is nothing that will stop
them from bringing a weapon to work, right? In fact, the lawmakers want them
to. It's wild. It is wild. The solution here is not more guns. It's not bringing
more guns into the building. It's keeping them out. That's the solution, at least
the short-term one because we're not going to get a long-term one for a while.
This is a bad move. This is going to go bad. Anyway, it's just a thought. You'll have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}