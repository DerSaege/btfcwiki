---
title: Let's talk about articles of impeachment in Texas being filed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qH3zZmx51ks) |
| Published | 2023/05/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas is moving forward with the impeachment process against Paxton.
- The Republican-led committee in Texas unanimously voted to proceed with impeachment, filing 20 articles.
- Impeachment in Texas requires a simple majority in the House and two-thirds in the Senate to convict.
- Paxton could be temporarily relieved of duty if the House impeaches him.
- Paxton claims voters already knew about the allegations against him.
- Impeachment in Texas can only be based on actions taken after election to office.
- Some allegations against Paxton might have been unknown to voters, raising questions about fulfilling obligations.
- The process could move quickly, possibly within two weeks.
- Tensions within the Republican Party are rising due to Paxton's responses.
- Paxton's approach of attacking those involved may not be beneficial.
- The Texas House's stance on the impeachment remains to be seen.

### Quotes

- "Texas is moving forward with the impeachment process against Paxton."
- "Paxton could be temporarily relieved of duty if the House impeaches him."
- "If you're in Texas, get ready for a show."

### Oneliner

Texas is moving forward with impeaching Paxton, facing questions on voter awareness, potential quick proceedings, and escalating tensions within the Republican Party.

### Audience

Texans

### On-the-ground actions from transcript

- Stay informed on the impeachment proceedings and potential outcomes (exemplified)
- Engage with local news sources for updates on the situation (exemplified)

### Whats missing in summary

A deeper understanding of the specific allegations against Paxton and the potential consequences of his impeachment.

### Tags

#Texas #Impeachment #Paxton #RepublicanParty #VoterAwareness


## Transcript
Well, howdy there, internet people, it's Bill again.
So today we are going to talk about Texas and Paxton.
And yeah, they're moving forward with the impeachment.
Surprise, I kind of am.
So this is what happened.
That Republican-led committee in Texas,
they voted unanimously to move forward with impeachment.
They have filed 20 articles of impeachment.
Now, I am not an expert on impeachments in Texas and as far as I know they've only done
it like twice in their entire history.
So I would wait before you start quoting this, I would wait for like Texas Tribune or somebody
like that that really, really digs into this stuff because there might be some particulars
that I missed.
it appears that the impeachment can move forward with a simple majority in the House.
They need two-thirds in the Senate to convict and try. If they want to remove him, I think they also
need two-thirds in the House as well. Now, if the House impeaches, Paxton's out of office right then.
Basically, like temporarily relieved of duty type of thing until the Senate comes up with
the final determination on it.
Now Paxton said something to the effect of the voters already knew and that kind of got
me like what?
Okay.
You look in the Texas code and what you find is that somebody can only be impeached for actions that took place after
their election to office.
So I think what they're going to try to say is that these actions took place before the most recent election and that
the voters knew about it.  which if you really dig into it, you find this passage which is really in like a section
about
mayors, it's weird, anyway, that says the voters if the actions were matter of public record were
the voters otherwise knew about it. Now some of the allegations, yeah, the voters knew and they
elected him anyway for whatever reason. Some of them, I mean I followed this stuff pretty closely
and I didn't know about so I don't know that that would count as far as fulfilling that obligation.
But that's either way that's probably an argument they're going to make and there will probably be
that's something that will have to be kind of litigated and debated. Now how soon can this
process start. This could happen very, very quickly. For all I know, the vote is
going on as y'all watch this. This could happen very, very quickly. I would say
that at most two weeks, but it might happen a whole lot faster than that. The
tensions within the Republican Party are flaring and Paxton, in my opinion, is not
doing himself any favors by going out there and basically attacking everybody
involved in this. Calling them liberals or rhinos or whatever. Might not be the
best course of action, but we'll have to wait and see how the Texas house feels
about all of this. Anyway, if you're in Texas, get ready for a show. Anyway, it's
Just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}