---
title: Let's talk about the new Trump documents reporting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=qr4Dgl7n6pI) |
| Published | 2023/05/26 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New information on Trump's documents situation has escalated things to a new level.
- Employees allegedly moved documents the day before a search warrant was executed.
- In May 2020, Trump conducted dress rehearsals on how to hide and retain documents.
- Trump showed documents to unauthorized individuals.
- National Archives revealed Trump's awareness of the declassification process.
- The notion of willful retention is significant.
- Trump may face charges like obstruction, dissemination, and conspiracy.
- There are implications that Trump coordinated with others to conceal documents.
- The potential charges could be severe, akin to those faced by the Discord leaker.
- Trump's team anticipates an imminent indictment, but uncertainties exist.
- Recent reporting suggests the possibility of severe charges beyond willful retention.
- The outcome may vary based on who accessed the documents, especially if they involve foreign nationals.
- There is a likelihood of more severe charges if the reported information is accurate.

### Quotes

- "Employees allegedly moved documents the day before a search warrant was executed."
- "The potential charges could look like the discord leaker's charges."
- "Recent reporting suggests the possibility of severe charges beyond willful retention."

### Oneliner

New revelations suggest Trump may face severe charges beyond willful retention, possibly involving obstruction and conspiracy.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Contact legal experts for analysis (suggested)

### Whats missing in summary

Detailed context and analysis of the potential legal implications beyond willful document retention.

### Tags

#Trump #Documents #Charges #Obstruction #Conspiracy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the reporting that is coming out about the
new information regarding the documents situation with Trump, because it has
taken things to a new level.
In recent videos, I said it showed signs they were looking at more serious charges.
The new reporting says that employees moved the documents the day before the search warrant
was executed.
Also says that in May of 2020, Trump conducted dress rehearsals on how to hide the documents
and keep them.
also says that he showed them to people that weren't supposed to see him. Okay, all of
this combined with the information from the National Archives that says that he was fully
aware of the actual declassification process and that his presidential magic wand to declassify
things by thinking about him isn't real. All of this is leading up to a change in
charges. What we've been talking about on this channel for the longest time from
the very beginning is willful retention. He might also now, if this reporting is
accurate, be looking at things like obstruction, dissemination, and the
The reporting seems to suggest that he coordinated with other people to do this, which brings
it to conspiracy.
So to make this simple, I have this, I'm not supposed to.
How I wound up with it really doesn't matter.
The feds show up and say, hey, give me that back, no, I'm not going to do that.
full retention. I show it to you. Dissemination. I hide it and make it hard for them to get
it back. Go out of my way, make statements, stuff like that. Obstruction. Do this in conjunction
with other people. Conspiracy. If this reporting is accurate and it lines up with a lot of
other stuff that we talked about before that kind of hindered this way, he's in a world
of trouble.
Like well beyond what we were talking about in the beginning.
The potential charges could look like the discord leaker's charges.
It's very different.
If he showed those documents to people, which I mean from the beginning we said it was a
possibility and he actively attempted to conceal them while the feds were
attempting to get him back and he did it in conjunction with other people it's
well beyond woeful retention and they are probably looking at other charges
which are more severe. His team seems to think an indictment is imminent. I don't
know that. I don't know that. There are a lot of signs that say, Smith is done. But
there's also nothing to say that he doesn't want to get done with everything,
with all of the different investigations, all of the different routes he's going
down, before presenting everything. Or keep in mind it's a possibility that
there's already an indictment, but it's sealed. There's a lot of room for a lot of different variables here.
But this most recent reporting really does kind of put all the pieces together to look at those other charges.
This definitely, at this point, appears to be way more than I have these, I was told to give them back, and I didn't,
which is really what willful retention boils down to.  There are other avenues they could go with this as far as
charges.
To me, it seems unlikely that they would use those routes with a former president, but I think a lot of that is going to
depend on  who actually got to see them, who got to see the documents. If it's him showing
off and showing people that are people who aren't really a risk but they're
not cleared, they're not authorized to see it, that's one thing. If he's showing
them to foreign nationals, foreign nationals that are linked to adversarial
countries, stuff like that. It's an entirely new game. And we don't know that yet. We
don't have that information. But this reporting kind of confirms everything that we've been
talking about as far as it looks like they might be entertaining the idea of more severe
charges if what the post found is accurate. I mean that that looks like
where it's headed. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}