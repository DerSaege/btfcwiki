---
title: Let's talk about when it's going to start in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=j5UlA5SAfaI) |
| Published | 2023/05/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Many messages in his inbox asking about when the conflict in Ukraine will start, with people expecting it to kick off any moment.
- Russia has a history of running troops up to the border before invading, creating a pattern of false alarms to keep up the element of surprise.
- Continuous rumors about the conflict starting prompt the Ukrainian military to make moves in response, causing the Russians to react and move troops around.
- The repeated false alarms degrade the resolve and alertness of the Ukrainian military, making them routine and less effective over time.
- Psychological delay tactics like this are common in military strategies, and they tend to work effectively by creating a pattern of expectations.
- Even with good intelligence, constant false alarms can lead to decreased alertness and attention, akin to the story of "the boy who cried wolf."

### Quotes

- "Imagine being on the receiving end of it, and every other week it's starting today, it's starting today. Eventually you stop believing it, and once you stop believing it, well, that's when it happens."
- "Because they've done it so many times, it becomes routine. Once it becomes routine, well, they're not really paying attention anymore."
- "If you constantly expect something and it doesn't happen, eventually you expect it to not happen."

### Oneliner

Beau explains how continuous false alarms about the conflict starting in Ukraine degrade the effectiveness of the Ukrainian military's response.

### Audience

Military analysts, Ukraine watchers

### On-the-ground actions from transcript

- Stay informed about the situation in Ukraine and be cautious of continuous rumors that may affect perceptions and responses (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the psychological tactics used in military strategies and their impact on alertness and response effectiveness.


## Transcript
Well, howdy there, internet people.
Let's build again.
So today, we are going to talk about Ukraine
and when it's going to start.
Because there's a whole bunch of messages in my inbox asking.
And basically, they all have the same tone.
It's people saying, hey, every other week,
they're saying that it's going to start now,
that it's going to kick off any moment,
that this week will be the week that the counteroffensive begins.
And of course, it doesn't.
It hasn't yet.
OK, so what's going on?
The same thing the Russians did before they invaded.
If you go back before the invasion, before this most recent incursion,
you'll find Russia running troops up to the border
and then doing nothing.
Happened a couple of times.
In fact, I'm not sure if I released it or not,
but there's actually a video that I recorded
that says, hey, you know,
looks like they're pulling them off.
And then they sent them right back in.
It's to keep up the element of surprise.
Think about how annoyed you are for whatever reason.
You're waiting for the news.
Imagine being on the receiving end of it, and every other week it's starting today, it's starting today.
Eventually you stop believing it, and once you stop believing it, well that's when it happens.
It's relatively safe to assume that the Ukrainian military is making moves every time these
rumors start to surface.
That's what really kind of originates the rumors, right?
The Russians have to respond to those.
They have to get ready.
They have to get on guard.
Maybe they move troops around.
And every time they do it and don't do something, it degrades the resolve, their alertness.
Because they've done it so many times, it becomes routine.
Once it becomes routine, well, they're not really paying attention anymore.
So the delay, it's a psychological thing.
And pretty much every military does this at some point or another.
The thing is, it always works.
It's kind of a constant.
If you constantly expect something and it doesn't happen, eventually you expect it
to not happen.
So even with good intelligence, let's say Russia has good intelligence, and they're
like, oh, there's movement, there's movement, get ready.
the third or fourth time, you know, it becomes the boy who cried wolf. And they
stop paying attention. Just remember at the end of that story, there's actually a
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}