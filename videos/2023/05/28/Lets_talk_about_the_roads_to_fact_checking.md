---
title: Let's talk about the roads to fact checking....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=fHUosTiu6vU) |
| Published | 2023/05/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides techniques on avoiding falling for bad information and fact-checking oneself.
- Advises asking if information confirms existing beliefs and if it is sensationalized.
- Talks about using reverse image search to verify images in memes.
- Mentions the impact of AI on misinformation and fact-checking.
- Emphasizes verifying quotes and their context through Google.
- Warns about cherry-picking quotes and nut-picking to misrepresent groups.
- Advises checking statistics for accuracy, methodology, and context.
- Talks about linking unrelated events to create false narratives.
- Mentions the difference between correlation and causation.
- Encourages viewing memes as propaganda and retraining algorithms for quality information.

### Quotes

- "Figures don't lie, but liars certainly figure."
- "View every meme as propaganda."
- "Make sure there's something down below saying, hey this is bad."
- "Retrain your algorithm."
- "A string of coincidences. Nothing to see here."

### Oneliner

Beau provides techniques to avoid falling for misinformation, including verifying quotes, checking statistics, and viewing memes as propaganda.

### Audience

Social media users

### On-the-ground actions from transcript

- Comment on misinformation to slow its spread (suggested)
- Block outlets that consistently provide false information (exemplified)

### Whats missing in summary

In-depth series on fact-checking techniques and safeguarding against misinformation.

### Tags

#FactChecking #Misinformation #Propaganda #AvoidingFakeNews #DigitalLiteracy


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about the roads to
fact checking and how to kind of avoid falling for bad information. How to fact check yourself,
some techniques that I use, questions to ask yourself before you share something, stuff like
that. This was highly requested after a recent video over on the other channel.
Okay, so we're going to jump right into this. The first question you need to ask
yourself, you're scrolling on your feed and you see a meme, okay, and you want to
share it because there's information on it and it's just shocking, right? Does it
confirm what you already believe? Is it something you want to believe? If the
answer is yes, and it normally is, you have confirmation bias coming into play
and it's going to skew your detector because you want to believe it. The next
thing is to ask yourself whether or not it's sensationalized. Is it just a wild
claim about nukes or you know EMPs or whatever the odds are if the news is that
big you are not going to find out about it from an image on social media you'll
see either text or you'll see articles you'll see some kind of real reporting
not just an image with some word slapped onto it. Is the information presented in
a sensationalized way? Is it designed to be shocking? Now all memes pretty much
they have an image right? Prior to AI the way you would deal with this is
you would basically just take the image itself, cut it out of the meme, and then upload it
to a reverse image search.
I liked 10i, but there's a whole bunch of them out there.
And what you would find is that many times that image had been online for years, and
the meme says it happened this weekend.
They're lying.
They're obviously lying.
And they know they're lying.
or trust the source again.
Now, in a recent Q&A on this channel,
somebody asked how I thought AI was
going to impact misinformation and disinformation.
And I said eventually it was going
to lead to more fact checking because people
would get tired of being burned by bad images or bad videos.
A couple of days later an image circulated on Twitter of the Pentagon
and it looked like it'd been hit. Giant plume of smoke coming out of it and it got shared by a
whole bunch of people. Some who should have known better. When you have an AI-generated image you
can't use a reverse image search.
Because it's a brand new image, it won't be online.
So that is also true of breaking events.
If it just happened, there's no way
for the image to be on there.
But what makes it suspicious?
What makes it sus?
It's one image.
something that important, something that is that newsworthy, is not going to be
one image. It's not going to just have one image from one angle. That should
have been a sign. There was no corroborating messages, posts, tweets on
social media. Nobody talking about how, hey I just had to leave the Pentagon
because it was on fire.
It seemed like something somebody might mention.
There was nothing like that.
Just one image shared over and over again
with different commentary.
When people are using AI right now to create images,
what they're doing is they're typing in what they want to see
and it creates the image for them.
If they try to get it from a different angle, odds are,
The images won't look close enough to pass as the same event.
So for the time being, until we have better tools,
that's something to look for, because it'll probably just
be one angle on it.
OK.
Quote context verification.
You see little bits and pieces of quotes all the time
on memes? What's the context behind the quote? Google is your friend here. Take the quote,
whatever it is, and type it into Google in quotation marks. So it gives you, Google then
knows to search for those words in that order. And it will bring you the quote. It will bring
you the context of when it was set. Read it, see if it actually matches up. Most times
it doesn't. They're taking it out of context, they're cherry picking it. The opposite of
cherry picking is nut picking. Let's say there is a demonstration somewhere and there's
video of somebody saying, well, what we want is a return to trading with shells and we
want to get rid of the dollar. That isn't necessarily representative of the entire demonstration.
There are, quote, journalists who will go to these events and find people who might
have issues and talk to them and get quotes from them and use those and try to pass those
off as representative of the whole. They aren't. See, when you see that, you see a
wild claim coming out of a group or demonstration, see if it's more than one
person or more than one group because sometimes you'll have two people
together. See if it's really representative of the group because this
is how a bad actor can scapegoat somebody. Okay, obviously they lie about
statistics and stuff like that. You know, figures don't lie, but liars certainly
figure. So look to see if it's per capita or raw numbers. Look to see if the stats
are actually cited. Look at the methodology of how the statistics were
collected. Look at the bar graph, see if they're truncated. See the year it starts.
Like let's say you have a a line graph and it's showing how great the economy
was under Trump. But it starts the year Trump took office. See if you can pull
that same information for the four years before. I'm using this because Trump
supporters actually did this a whole lot.
What you will find out is that Trump's great economy, where
it starts to shoot up at like a 45 degree angle, that trend
actually started under Obama.
It had been going on like that for years.
OK.
So another good one, and you'll see examples of a lot of
these, because somebody sent me something, and we'll go
through and do it right now.
but linking unrelated events and trying to make them seem as
though they're connected.
So this could be timeline-wise, things
that aren't actually related in any way, shape, or form,
or it could be maps and graphs.
One that happened recently, there
was a job application for an IRS agent.
And this was one of the few armed agents.
At the same time, there was legislation pending
talking about hiring a whole bunch of new IRS agents.
The legislation was pending.
They certainly weren't hiring for that position already.
But bad actors put the two together
and made it seem like the IRS was going
to hire 50,000 armed agents, linking unrelated events
that way.
You can also do this with the whole correlation and causation
thing.
You'll see that a lot too.
That's normally in the form of a map.
A good one to show you how it's done
is pulling up the prevalence of mad cow in the UK
and support for Brexit.
The maps are almost identical.
So obviously, the only conclusion you can draw
is that Mad Cow causes support for withdrawing from the EU.
Of course, it doesn't.
Causation and correlation are not the same thing.
Another one, dated events that they try to make
seem like they just happened.
They'll write about something with a new article, sometimes a new dateline, but it happened
five years ago.
But they don't include that in their little article because they want the sensationalism.
Oftentimes they will try to connect this, this thing that happened years in the past,
to the present day.
And we have an example of that here in a second.
Mundane events seeming important.
We have an example of that here too that's pretty funny.
They ask a lot of questions in the memes.
Do you really think this is a coincidence?
Can this possibly be normal?
That type of thing.
They're not telling you that this is happening.
They're just asking a question.
They do this so their subscribers or their viewers take the blame upon themselves.
They say that, well, I drew the wrong conclusion, not that they were manipulated into it.
When you look at an outlet, if they have a history of just asking questions about stuff
that never happened, stop reading them.
If it's an account, stop following it.
Is the culprit actually identified?
They are out to get you, right?
Who is they?
Is it just something generic?
Liberals?
Some buzzword.
If that's the case, it's probably not true.
It's probably not real reporting.
Even in a recent video I was talking about no-fault marriage.
Now this is something that I personally think is probably going to become GOP platform,
trying to get rid of no fault marriage.
In that video, I point out the three states that have publicly said it.
So if you wanted to fact check it, you could go find it.
If it's all just very generic and it makes you mad, that's what it was designed to do.
you're mad, you'll share it, and it increases engagement, it increases revenue.
They're using you.
They're tricking you.
Does it scapegoat a group that doesn't have a lot of power?
Good example of this right now is pretty much any meme vilifying trans people.
This is not a group of people.
This isn't a demographic that has a strong lobby.
They don't have a lot of political power.
They represent a very small percentage of the United States.
But if you were to look at most right-wing outlets right now that peddle in disinformation,
you're going to find memes about them all over the place.
Because that's the current scapegoat.
who those outlets want to fear monger about because they don't have a lot of political
power so it's harder for them to fight back.
Any time somebody is trying to get you to blame somebody with less institutional power
than you have, they're playing you.
Check the comments underneath the meme or article.
are somebody already debunked it.
Somebody was already like, this isn't true and this is why.
Follow their link, see if it's true, check their work as well.
If they're right, make sure you like or share the debunking rather than the actual
post.
Don't give them the engagement.
sure that other people see that they lied.
And something you can also see is, this is a good sign on YouTube, if you correct them
and they block you, it's because they're going to lie again in the future.
If you correct them and they pin the comment, it's because they want their information
to be right.
That's a really good sign on YouTube.
Okay, the big thing to remember, view all memes as propaganda, because they are.
They're designed to be easily consumable, shareable, viral, and convey information that
is designed to make you feel a certain way.
It's pretty much every meme out there.
View every meme as propaganda.
Meaning there's no way you're going to get the whole story from a meme.
So don't allow yourself to think that it's in any way valid
as far as getting actual information.
Retrain your algorithm.
If you get outlet x and you have fact-checked them twice
and they were wrong both times, block them.
get rid of them. Eventually the algorithm will start showing you a higher quality
of stuff. Now when you catch people engaging in misinformation, disinformation,
sharing bad information, comment. Make sure that it's there. Make sure that
there's something down below saying, hey this is bad, because you can you can
and help slow the spread of it, and you can make it less profitable for people to lie
to the public.
Okay, so here's an example.
This is something that I was sent, and it has a whole lot of stuff in it that we're
going to kind of go back to because a lot of these are actually in this.
Okay, so it says, what do you think of all of this?
I really wish I knew where this came from.
30 tons of explosives are still missing. This is a reference to what's missing on
the train. Is it explosives? It's not. It's something you need. You can make stuff
out of it, but in the form that it was in, it's more akin to fertilizer. So off
top, sensationalized, right? $290 million worth of anti-radiation
meds purchased. That's true, that happened. I want to say two years ago. Dated event,
trying to tie it to something new and current. Senators given satellite phones for emergency
communications. But only 50 senators. Does that mean it's only Democrat senators? There's
the question. Trying to make it sensational. The information is not right either. It was
more than 50.
It just gets weirder from here.
High level U.S. politicians will be vacationing Memorial Day weekend with their families at
various undisclosed continuity of government locations.
There's that jargon.
We've talked about that in other videos.
A lot of times, to make something seem more official, they will include jargon, continuity
of government, cog plans, those are real.
But here's the question, high level US politicians, which ones?
Don't know, right?
Vacationing at undisclosed, then how do you know about it?
But more importantly, this is an example of the mundane, oh, the upper class are going
go on vacation on Memorial Day. Shock, right? But what they're trying to do is
tie that to the phones and the radiation meds to make it seem like something's
gonna happen in DC. Yeah, just a string of coincidences. Nothing to see here. Not a
question but falls into that same concept, trying to get you to
draw the conclusion. So if you were to actually look into it, at time of filming, the current
theory about the train with the stuff missing is that it actually fell out along the track.
They're little pellets in the stage that it's in and that they're scattered all over the
tracks. We'll have to see if that holds up but that's their current theory. So it's a kind of
a overview of how to look at this stuff and hopefully safeguard yourself and those who are
on your friends list or those who follow you on Twitter from bad information. So I hope that
helps I'm probably going to put together a more in-depth series on this because
there was a whole lot of interest in it so anyway it's just a thought y'all have
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}