---
title: Let's talk about who won the debt ceiling negotiations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4cJMKfB63X8) |
| Published | 2023/05/28 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden and McCarthy have a deal, but the details are still unknown.
- It appears that Biden got the better of McCarthy based on leaks and reactions from Congress.
- The Republican Party made promises but many of them are not reflected in the current deal.
- Despite some changes to programs like SNAP and temporary assistance, Republicans seem to have achieved their core values of cutting the IRS budget and imposing work requirements.
- The deal includes sunsets for certain programs, meaning they phase in over time and then cease to be a law.
- McCarthy may need Democratic votes to pass the budget, indicating potential challenges within the Republican Party.
- Tangible effects such as cuts to programs like the Inflation Reduction Act or infrastructure bill do not seem to be included in the deal.
- While there are concerns about work requirements, the full impact will be clearer once the bill is seen.
- Biden appears to have come out stronger in the deal, but the actual bill will provide more clarity.
- The final assessment will depend on how much support McCarthy needs from the Democratic Party to pass the bill.

### Quotes

- "Biden got the better of McCarthy."
- "The Republicans got their two core values fulfilled."
- "The hardliners definitely did not get what they wanted."
- "I'm going to say that Biden got the better of him."
- "Your real gauge at the end of this is how much help the Republican Speaker of the House needs from the Democratic Party."

### Oneliner

Biden seems to have the upper hand in the deal with McCarthy, but the true impact will depend on Democratic support for passing the bill.

### Audience

Politically informed individuals

### On-the-ground actions from transcript

- Contact your representatives to express concerns about work requirements in social safety net programs (implied)
- Stay informed about the details of the bill as it unfolds to understand its real effects (implied)

### Whats missing in summary

Insights on the potential repercussions of the deal for individuals and communities.

### Tags

#Biden #McCarthy #RepublicanParty #BudgetDeal #SocialSafetyNets #Legislation


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the deal.
Biden and McCarthy apparently have a deal.
So we're going to talk about what we know.
The questions that have come in have basically been who won the negotiations.
It's a negotiation.
Nobody won the negotiation.
More importantly, a time of filming, which is last night, we don't actually know.
We have to wait and see the legislation.
We actually have to see the text of the bill on some of it.
But if you're going by the talking points memos that went out, if you're going by the
leaks, the reactions to the deal by people in Congress, it seems as though Biden got
the better of McCarthy.
You have to remember the Republican Party, they put out all kinds of promises, promises
that for months we've been saying there's no way that they'll be able to actually get
what they're saying.
Big surprise, that return to the fiscal year 2022 part, that's not in there.
massive cuts really aren't in there. The Inflation Reduction Act, the CHIPS Act,
the PACT Act, the infrastructure, all of that stuff looks like it's untouched. So
all of that's a win. Now you have one Republican representative who is super
mad because McCarthy was able to get the IRS's budget reduced but it looks like
it's going down according to the representative by 1.9 billion dollars.
Keep in mind we're talking about out of 80, not exactly slashing it. Now the
interesting part that we're really gonna have to pay attention to is what's
going on with SNAP, the food stamp program, because it looks like it was both expanded
in some ways, but they're adding work requirements, which is, they don't work, just so you know.
So the expansion part sounds good.
The work requirements, of course, do not.
The Republicans pushed for the work requirements.
It was a big deal for them.
But to see the real effects of it, we're going to have to wait and actually see the
bill.
And then you have some changes to temporary assistance for needy families.
Again, for that, we're going to have to wait until we actually see the text.
But at the end of it, it looks like the Republicans got their two core values fulfilled, a cut
to the IRS so they can help wealthy people cheat on their taxes, and punish people who
need social safety nets.
That's the Republican Party today.
That appears to be it.
There are some caps that have been mentioned, but as we've talked about before, the caps
they're not real.
They're more guidelines than anything a future Congress can decide otherwise.
The other part about this that's interesting is the stuff with SNAP and the other program,
They appear to have sunsets, which means they go into effect, like they're phased in over
a period of time, and then they cease to be a law.
And it looks like it's like five years from now.
So any deficit reduction the Republicans were hoping to get out of it, that doesn't seem
to be likely either. But for the the parts that matter to most people
watching this channel, you know how it's gonna hit people on the bottom, we're
gonna have to wait to see the bill. As far as knowing who won, I think the
easiest way to gauge that is to see how many votes McCarthy needs to pass his
own budget because I got a feeling the Democratic Party's gonna have to come
across and help him pass it. There were a number of Republicans who said I will
not vote for a budget or a debt ceiling increase that doesn't have X. Okay, those
exes, they're not in there. The hardliners definitely did not get what they wanted.
That much is very clear from all of the the memos and reactions. The people who
really wanted to go after the IRS doesn't look like they're getting what
they wanted either. These were all promises that Republican
representatives made to their constituents. So in order to pass this
McCarthy is probably going to need members of the Democratic Party to vote
for it. If you want to know how bad it is for McCarthy, how bad it is for the
Republicans, we need to wait and see how many Republicans refuse to vote for
their own budget. But as far as the tangible effects, the stuff
that people were really, really worried about, as far as them
going after the Inflation Reduction Act, or the
infrastructure bill or any of that stuff, none of that appears
to be in it. The really deep cut going back to fiscal year 2022
doesn't look like it's in it. The stuff with the work
Work requirements to me is concerning, but I want to wait and see the bill because there
are different, I don't want to say they're leaks because they knew this stuff was going
to come out, but there are people talking about how in some ways the exemptions were
expanded, but there's more work requirements.
I want to see the fine print on this before I decide, you know, how bad it is.
Cause I don't want to say, you know, Biden totally, you know, just ran rough
shod over, uh, McCarthy and then find out that McCarthy got his way when it came
to snap, uh, but it's those two social safety nuts seem to be the only thing
that are really in jeopardy.
There's an IRS cut, but it's not big, so at this point, I'm going to say that Biden got the better of him.
But at the same time, I want to wait and see the actual bill.
But your real gauge at the end of this is how much help the Republican Speaker of the House needs from the Democratic
Party to get his bill through.  Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}