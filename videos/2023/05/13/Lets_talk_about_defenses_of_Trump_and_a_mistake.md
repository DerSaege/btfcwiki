---
title: Let's talk about defenses of Trump and a mistake....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=0g4QUqbd9mY) |
| Published | 2023/05/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring defenses of Trump after a decision in New York led to shocking revelations.
- Sought out right-wing social media reactions to understand their defenses.
- Shocked by two particular defenses focusing on Trump's ability to have any woman he wanted.
- Emphasizes that the behavior is not about attractiveness but about power.
- Both defenders happened to be fathers, which added a disturbing layer to their comments.
- Expresses horror at the thought of daughters not being believed based on looks or the man's wealth.
- Notes the impact of social media comments on relationships, especially with one's children.
- Criticizes the idea of valuing money as a reason to be with someone, especially for right-wing individuals.
- Acknowledges Trump's lingering influence even after leaving office.
- Warns about the permanence and consequences of words spoken online.

### Quotes

- "That's not why that occurs."
- "It's pathetic."
- "Trump, even out of office, even with as little power as he has, he still has a hold over people."
- "Not just your daughters, but other people in your life, they will never look at you the same way again."

### Oneliner

Exploring defenses of Trump reveals disturbing attitudes towards power and relationships, even among fathers, with a caution about the impact of online comments.

### Audience

Parents, online commentators

### On-the-ground actions from transcript

- Monitor and reconsider the impact of one's comments on social media (implied).
- Educate children on healthy values and relationships (implied).
- Foster open communication with children to address potentially harmful beliefs (implied).

### Whats missing in summary

The full transcript delves into the complex dynamics of defending Trump and the lasting effects of online commentary on personal relationships.


## Transcript
Well, howdy there Internet people, it's Bo again.
So today we are going to talk about defenses of Trump.
Things that people said in defense of Trump and a mistake that I made.
You know, after the decision in New York, I decided to scroll social media, right-wing social media,
and see how they were defending them, you know, see how they were reacting.
Man, I shouldn't have done that.
There were two in particular that caught my eye, and it wasn't that what they said was, you know, that different than
the other defenses that were being thrown out there.  It was a different characteristic about the person leveling the
defense.
One of them said that Trump didn't need to do that because back then he could have any woman he wanted.
The other compared the relative levels of attractiveness between her and the
other women that he had access to. He wouldn't do that, he could have hotter.
Now just in case it needs to be said, that particular act is not about attractiveness,
It's about power. That's why so many authoritarian goons engage in it. The
thing that struck me about these comments is that both the people that
said them were dads. That's got to be horrifying to know that as a daughter if
it happened to you, your dad wouldn't believe you if you weren't hot enough or
if the guy was rich. That is something else. Wouldn't believe you based on those
characteristics. Even if there was audio of him admitting that he does that type
of thing. Man, cannot imagine being that devoted to some politician or maybe they
just believe it about everybody I don't know but I think that a whole lot of
people need to remember that their kids have access to social media. They see
these comments. A lot of them you may not know it. You're saying it to be part of
some in-group, but it might forever alter the relationships you have with
your own kids for Twitter likes. It's pathetic.
That's not why that occurs and I would also point out that not everybody looks
money as, you know, the reason to to be with someone. It's probably not a value
that right-wing people would necessarily want to instill on their daughters, but
that's certainly the message that's out there.
Trump, even out of office, even with as little power as he has, he still has a hold over
people to the point where they are saying things that will forever alter their relationships
with their own kids. It is mind-blowing.
Remember that the Internet
is accessible. Remember that people can see your comments.
People you might not expect to see them. And
that there are some things that once you say them,
Not just your daughters, but other people in your life, they will never look at you
the same way again.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}