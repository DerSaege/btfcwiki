---
title: Let's talk about Ukraine, and whether it started....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=o_eF2QaXO_Y) |
| Published | 2023/05/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the strategic, operational, and tactical levels of military operations in Ukraine.
- Believes that the current actions are shaping the battlefield for a future, larger move rather than the actual counter-offensive.
- Observes that most actions seem to be at the tactical level of securing certain areas.
- Notes that the Russians are not engaging in a full counter-offensive yet, but testing and exploiting vulnerabilities.
- Points out that Russians leaving Ukraine is not an orderly withdrawal, but more of an abrupt decision to depart.
- Comments on Russia's conscripts and draftees not being suited for combat, leading to their departure.
- Advises against mandatory military service based on the situation in Ukraine.
- Suggests that the gains made by the Ukrainian side may not be part of the larger operation for significant gains.
- Warns against morale dropping if progress slows down, as the current actions are setting the stage for a future counteroffensive.

### Quotes

- "They're not supposed to be there. They're facing combat and they've decided they're not going to die for some sick old man in the Kremlin. Good for them."
- "Conscripts, draftees, it doesn't go well. It very rarely produces the desired result."
- "If things kind of lock up, don't let that impact morale."

### Oneliner

Beau explains the ongoing military operations in Ukraine, focusing on shaping the battlefield for a future counter-offensive rather than the current actions being a full-fledged counter-offensive.

### Audience

Military analysts

### On-the-ground actions from transcript

- Monitor the situation in Ukraine closely (implied)
- Support efforts that aim to de-escalate the conflict (implied)

### Whats missing in summary

Analysis of potential implications on the Ukrainian conflict

### Tags

#Ukraine #MilitaryOperations #CounterOffensive #RussianWithdrawal #Morale


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we are going to talk about Ukraine.
And we're going to talk about the strategic,
operational, and tactical, and what the difference is.
And we're gonna talk about what's going on and why it may
not be what a lot of people seem to think it is.
Almost as soon as that last video went out, got a number of
messages from people basically saying, hey it started, it started, it's underway,
you know the Russians are running and the counter-offensive is underway. I'm
gonna be honest I don't I don't I don't think it is. I don't think that's what
we're witnessing. Not in earnest yet. I think that most of what is happening
right now is operations designed to shape the battlefield for a future, larger
move. They just happen to be taking dirt in the process. You have three levels to
things. You have the strategic, big picture. Operational, how to make the
big picture happen. Tactical, sergeant go take that hill. Okay, most of this stuff
is happening at the sergeant go take that hill level. One of the clips I saw,
it honestly seemed like the Russians broke and ran from reconnaissance and
force. I don't think that this is the counter-offensive. I think these are the
operations designed to set the stage for the counter-offensive and the Russians
are running from from that. I don't think that this is actually the big push yet
which I mean they're they're making gains and it doesn't matter how you make
them but I don't actually think that that most of what is happening right now is
actually planned. It's more of they're testing things out, they're trying to
secure certain things, and when they see a vulnerability as they should, the
Ukrainians are exploiting it. That seems to be what's happening right now. This
could just steamroll, could thunder run right into a larger counter-offensive
that may not be exactly what they planned. At the same time, I have a bunch of
questions basically asking why Russia is withdrawing, why they are taking off the
way they are, and wondering if that is some, you know, 12-D chess. No. Now, when
you look at the footage of them leaving, that is not what would be deemed an
orderly withdrawal. When they decide to leave, they're just leaving. It moves to
the question of why. Because they never should have been there to begin with. And I'm
not talking about, you know, Russia shouldn't have been in Ukraine. That part,
I think people know what I think. They're conscripts. They're draftees. They're
workers not warriors. They're not supposed to be there. They're facing
combat and they've decided they're not going to die for some sick old man in the
Kremlin. Good for them. That's the right move. It's good that they're not letting
letting their egos get them killed.
They should leave.
You have a whole bunch of people that have no training and little equipment and they're
not up to it, they know they're not up to it, their heart's not in it, it's not what
they're really about and they're leaving.
It's the right move.
Americans there is something to learn from this. Every once in a while you have
some politician who wants to appeal to his nationalist base say something like
everybody should serve two years in the army or whatever. No, they shouldn't and
that's why. That's why. Conscripts, draftees, it doesn't go well. It very
rarely produces the desired result. So at the end of this, what do you have? You have
some decent gains for the Ukrainian side. I don't think that this is actually the larger
operation that is supposed to produce gains. These are more incidental. Due to the rate
at which they are making gains, they may decide to try to exploit it in a more organized manner.
But at this point, I don't think that this is actually the counter offensive.
And the reason I'm saying this is because I don't want people to become discouraged
if in like, you know, 48 hours, everything slows down and there's not gains anymore.
Because I don't actually think that's what this was supposed to do.
I think this was more setting the stage for a counteroffensive that hasn't launched yet.
So if things kind of lock up, don't let that impact morale.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}