---
title: Let's talk about Trump's NY warning....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=PBcLu3gk-ro) |
| Published | 2023/05/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- The judge in the New York case is setting up a Zoom call with Trump on May 23rd to explain what he can and cannot do with the information he obtains through discovery from the state.
- Trump is prohibited from using any information obtained via discovery to harass or intimidate witnesses, and he is not allowed to possess certain information, only to view it with his lawyers without taking notes or copying anything.
- The judge's call is a warning to Trump, indicating that he could be found in contempt if he violates the order.
- Trump's history suggests he may struggle to keep information confidential until February or March 2024.
- Trump has used social media to bully people in the past, but the judge is now making it clear that he cannot continue this behavior in the criminal proceeding in New York.
- The judge's intention seems to be holding Trump accountable if he breaches the restrictions set during the Zoom call.
- It is likely that there will be further proceedings related to Trump's actions following this briefing.

### Quotes

- "Trump is prohibited from using any information obtained via discovery to harass or intimidate witnesses."
- "The judge's call is a warning to Trump, indicating that he could be found in contempt if he violates the order."
- "Trump has used social media to bully people in the past, but the judge is now making it clear that he cannot continue this behavior in the criminal proceeding in New York."

### Oneliner

The judge in the New York case is setting up a Zoom call on May 23rd with Trump to explain restrictions on using information obtained via discovery, warning of contempt if violated, particularly regarding harassment.

### Audience

Legal Watchers

### On-the-ground actions from transcript

- Stay updated on the developments in legal cases involving public figures (implied)
- Support accountability in legal proceedings by advocating for fair treatment and adherence to restrictions (implied)

### Whats missing in summary

Insight into potential consequences and implications of Trump's actions in the ongoing legal case. 

### Tags

#Trump #LegalCase #Restrictions #Contempt #JudicialProceedings


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
in the New York case, and the little lecture
that he's going to be getting from the judge on May 23rd.
The judge in the New York case
is basically setting up a Zoom call
with the former president.
And during this call, the judge will explain to him
what he can do and what he cannot do
with the information that he obtains
via discovery from
the state. The judge has entered
an order that basically prohibits Trump from using any information he obtains
from the prosecution via discovery to
harass or intimidate any
witness or anything like that. Some of the information he's not allowed to even like have
possession of. He can only review it with his lawyers. He's not allowed to take notes, copy, anything.
Now, a lot of people are like, you know, normal folk wouldn't get this nice neat little briefing
from a judge. And that's true, but Trump's not normal folk, you know. He's a former president.
That being said, Trump can't keep a secret, can't properly handle a secret when it literally
has secret written on it. The idea that the former president is going to be able to keep
his mouth shut from the time discovery starts until February or March of 2024?
I don't think that's incredibly likely. The judge going out of their way to set up this little
Zoom call is putting the former president on notice. If you violate this stuff, if you use
this information to harass a witness, you could be found in contempt. That's the
purpose of this call. I have to believe that that means the judge intends on
holding Trump in contempt if he violates that. I am fairly certain that Trump is
going to violate that. Trump is not a man known for holding his tongue and he has
until February or March to mess this up. I feel like we're going to see another
proceeding sometime in I would assume the relatively near future dealing with
this. Trump for a long time has used social media to bully people and he's
gotten away with it. He is now in a criminal proceeding in New York. The
judge is telling him flat out, you can't do this via an order and then is setting
up a little briefing so Trump can ask any questions he needs to so Trump is
crystal clear on what he can do and what he can't. And then Trump will be left to
his own devices and the advice of his attorneys until February or March of
2024. So I would imagine that pretty soon I will be covering Trump being called in
of that judge to explain some comment on social media. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}