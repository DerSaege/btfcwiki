---
title: Let's talk about Missouri and a failed power play....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=aFAkYXKNSzI) |
| Published | 2023/05/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Missouri Republicans tried to make it harder for citizens to override legislature through the Initiative Petition by increasing the required support from a simple majority to 57%.
- Their attempt to change the process was driven by their fear that citizens wanted reproductive rights, which goes against the restrictive ban they passed.
- Despite knowing their ban was unpopular and against the will of the people, Republicans prioritized ruling over representing the citizens.
- The Republican Party's attempt to limit citizens' ability to represent themselves failed, allowing for the potential restoration of reproductive rights in Missouri through an initiative petition.
- Republicans displayed a clear desire to rule over the citizens rather than truly represent them, showing a disregard for the will of the people.

### Quotes

- "Republicans, they don't care about the Republic. They don't care about being your representative. They don't want to represent you. They want to rule you."
- "They know their ban is unpopular. That they know it goes against the will of the people of Missouri. They just don't care because they're your betters."
- "They were not able to change it so that process is still available and there will probably be an initiative petition put forward that will restore reproductive rights in Missouri."
- "Rather than represent the will of the people, they want to override it."
- "Republicans in Missouri, well, they caught the car."

### Oneliner

Missouri Republicans attempt to restrict citizens' ability to override legislature through Initiative Petition due to fear of reproductive rights, prioritizing ruling over representing the will of the people.

### Audience

Activists, Missouri residents

### On-the-ground actions from transcript

- Support and participate in initiative petitions to restore reproductive rights in Missouri (implied).
- Stay informed about legislative changes and initiatives in Missouri (implied).

### Whats missing in summary

The full transcript provides more context on the power struggle between Missouri Republicans and citizens advocating for reproductive rights.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Missouri
and the Initiative Petition and a last-minute attempt
to change something and an amazing admission.
So in Missouri, there's a process
called the Initiative Petition.
Basically, it's there to provide the citizens
with a way of avoiding legislative tyranny.
It's a check that allows the citizens to override
the legislature, if need be.
Requires 50% of people in Missouri to support something.
Now, Republicans in Missouri, at the last minute,
right as the session is ending, they
decide they want to change it.
And they want to make it harder for the citizens in Missouri
to override them.
I want you to think about it.
It's set.
You already need a majority.
But they wanted to bump it up a few points.
I think they wanted to take it to 57% or something like that.
So a simple majority wouldn't be enough.
You had to have more.
And the reason they wanted to do this
was because they were worried that the people in Missouri,
well, they wanted reproductive rights
and Republicans in Missouri, well, they caught the car.
They're the dog who caught the car.
They passed a very restrictive ban
and they know it's unpopular.
But rather than represent the will of the people,
they want to override it.
Going as far as to say that this is the Republican House Speaker, going as far as to say that the Senate should be held
accountable for allowing abortion to return.  Because they know that without that change, it will.
If they don't raise it and make it harder for the citizens to represent themselves,
reproductive rights will come back in Missouri.
Not just are they saying that they know the decision they made was unpopular, it didn't
represent the will of the people, they are also saying that they would rather rule than
represent.
Republicans, they don't care about the Republic. They don't care about being
your representative. They don't want to represent you. They want to rule you and
they've made that extremely clear by this move. It shows that they know their
ban is unpopular. That they know it goes against the will of the people of
Missouri. They just don't care because they're your betters. They get to tell
you what to do and you need to obey. That was the message they sent. It failed
just so you know. They were not able to change it so that process is still
available and there will probably be an initiative petition put forward that
will restore reproductive rights in Missouri, despite the Republican Party's
best efforts to rule over the citizens they're supposed to be representing.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}