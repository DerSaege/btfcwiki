---
title: Let's about claims about the budget caps and cuts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=k1Y3yA5gwB0) |
| Published | 2023/05/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of the numbers 22% and 1% in Republican and Democratic math regarding the budget.
- Republicans focus on a 1% increase cap annually, not the 22% cut in non-defense spending.
- Breaks down how the 22% cut in non-defense spending is derived from the budget plan.
- Illustrates how a 1% increase can actually result in a 5% reduction in purchasing power due to inflation.
- Emphasizes the real-life impact of budget cuts on services like Border Patrol with a reduction in purchasing power.
- Criticizes the Republican budget plan as detrimental to the economy and unsustainable.

### Quotes

- "The Republican budget plan, it's not a plan for a healthy economy."
- "The 1% increase is still an effective budget cut."
- "It's huge. It's huge."
- "So the 22% reduction across the board. That's where the number comes from."
- "It's not even close to a reasonable starting point for a negotiation."

### Oneliner

Beau explains the implications of the 22% cut and 1% increase in the Republican budget plan, critiquing its impact on the economy and services.

### Audience

Budget analysts, policymakers

### On-the-ground actions from transcript

- Analyze and understand the implications of budget cuts on services in your community (implied)
- Advocate for sustainable budget plans that prioritize economic stability and public services (implied)

### Whats missing in summary

Detailed breakdown of economic impacts and comparisons to everyday purchasing power.

### Tags

#Budget #Republican #Democratic #Math #Economy


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about math,
Republican math and Democratic math and the budget.
And we're gonna talk about two percentage numbers
that keep showing up, 22% and 1%.
And we're gonna talk about why, at this point,
Republicans and Democrats are two people
separated by common language, okay.
So I had a whole bunch of questions about the 22% number,
and I had somebody ask how a 1% increase is a cut when
they're talking about McCarthy's budget plan.
So what we're going to do is we're going to go through,
explain where the 22% number comes from,
and then we're going to talk about the Republican increase,
which if you don't know, if you exist in a right-wing echo
Chamber, you have probably never heard of the 22% cut. They're not talking about
that. What they're focusing on is a 1% increase every year, a cap at 1%. Okay,
so what is the plan? The first part of the plan is to set the budget for fiscal
year, 2024 at the same level as fiscal year, 2022, that is $1.471 trillion.
That's the plan.
Now they have promised repeatedly, they are not cutting defense spending at all.
Assume they're telling the truth.
Fiscal year, 2023 defense spending, $885 billion more or less.
You can get different numbers depending on how you factor this, but it doesn't change
the end result really.
So you take that $885 billion and you subtract it from the $1.471 trillion.
What do you have left for non-defense spending?
$586 billion.
Now you have to look at current non-defense spending, which is $756 billion.
So if you're spending $756 billion now and you're going to start spending $586 billion,
what do you have?
A 22% reduction across the board.
That's where the number comes from.
So now let's go to the 1% increase.
First it doesn't guarantee a 1% increase a year, it caps it at 1%.
And there are some people in the Democratic Party and liberal commentators who have said
well that's actually a cut.
And then they don't go on to explain it and normal people are sitting there going what
does that mean?
The Democratic Party is horrible at messaging.
can't get their information out. Okay, so if you increase the budget one percent,
all right, you have to take into account everything else. Let's just, we're gonna
stick with one thing. Now economists down in the comments will probably go into a
whole bunch of other things that just make the cut worse because there's
population growth and all kinds of other stuff that will factor into this, but
we're going to stick with the one thing that everybody's familiar with, inflation.
If you go up 1%, but your inflation rate is what it has been, let's look at the last three
years you have, right now I want to say it's around 5, last year it was 6.5, the previous
year it was 7.
It's going down, but the people sending in these questions are Republicans, you probably
don't think it'll go much lower.
Let's just take 6% right in the middle of that.
And then you get to knock down 1% of it because you're going to increase spending 1%.
You still have 5% left.
So it ends up being a reduction in purchasing power of 5%.
You're familiar with this at home.
You deal with this all the time.
Because nobody will raise the minimum wage, wages stay the same.
Everybody's wages do, right?
prices keep going up. You deal with this. The difference is scale. Since it was
mostly Republicans sending in these questions, let's take an agency y'all care
about, Border Patrol. Border Patrol, I want to say their base budget is like 16.5
billion, but then they get like an additional 1.5 billion for this and some
extra money for that. By the time it's all said and done, it's a little under 20
billion dollars. But we're just going to take 20 billion because it's an example
we're not actually figuring out the budget. If you have a five percent
reduction in the purchasing power of that 20 billion what does it equate to?
to, a billion dollars. It's huge. It's huge. So the 1% increase is still an
effective budget cut. To run flat, the budget increase has to be whatever
inflation is, and there's actually more things that it has to match up with, but
But for the purposes of this conversation, it has to at least be what the rate of inflation
is for them to have the same effective budget.
So a cap of 1% reduces their purchasing power, which reduces their services.
And I know a whole bunch of people right now watching this channel are like, okay, Cup Border
Patrol's budget by a billion dollars.
I don't care.
I get it.
I understand what you're saying.
same time this is across the board. This would be for everybody. This same math
would apply to whatever program you actually care about. The Republican
budget plan, it's not a plan for a healthy economy. It's if enacted as they
have written it out, it will crash the economy. There's no doubt it's gonna be
super painful. So I would also point out that the yearly cap that turns into a
budget cut, that occurs after the 22% reduction. Yeah, it's a bad budget. It's
not even close to a reasonable starting point for a negotiation, you know, using
everybody's pawns. But that's that's where the math comes from. That's where
the statements come from. I don't think that anybody who actually sat down and
thought about how this would impact things across the board would support
this or would support it as a starting point for any serious negotiations and
that's why it's being treated the way it is. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}