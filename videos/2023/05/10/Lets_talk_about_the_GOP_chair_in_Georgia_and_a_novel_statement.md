---
title: Let's talk about the GOP chair in Georgia and a novel statement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=7mp07chsSec) |
| Published | 2023/05/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Georgia's GOP chair, David Schaefer, is using a novel defense to avoid accountability for his actions related to the alternate electors in Georgia.
- Schaefer's lawyers claim he shouldn't be charged because he was following detailed legal advice, including advice from former President Trump's attorneys.
- The defense argues that Schaefer's actions were taken in conformity with legal counsel, eliminating the possibility of criminal intent or liability.
- While such a defense might work in some cases, Beau questions its effectiveness in this specific situation of far-right power grabs.
- The defense relies on the premise that intent is a key element in proving criminal actions, suggesting that if intent is lacking, it could be challenging to establish guilt.
- Beau speculates on potential complications if evidence surfaces suggesting knowledge of wrongdoing despite legal advice.
- Despite the defense's argument, Beau expresses skepticism about its ironclad nature due to the secretive nature of the events.
- The effectiveness of the defense hinges on whether there is documented reassurance from legal counsel that Schaefer's actions were lawful.
- Beau concludes by acknowledging that the defense might hold up under scrutiny if backed by substantial evidence but suggests awaiting further developments.

### Quotes

- "I'm just not sure that it's ironclad."
- "When it comes to far-right power grabs, generally speaking, 'I was just following orders' doesn't go over well."
- "It's just a thought. Y'all have a good day."

### Oneliner

Georgia's GOP chair attempts to evade accountability by claiming reliance on legal advice, but the defense's efficacy remains uncertain amid skepticism and potential complications.

### Audience

Legal observers

### On-the-ground actions from transcript

- Question legal defenses (implied)
- Await further developments (implied)

### Whats missing in summary

Context on the potential legal implications and developments that may arise from the defense strategy.

### Tags

#Georgia #GOP #LegalDefense #CriminalIntent #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Georgia
and the Republican Party chair in Georgia.
And a novel defense, one that nobody has ever heard before,
when it comes to why he's not really responsible
for what happened in Georgia
and why he can't be held accountable
for any actions that he might have taken. So David Schaefer, who is the GOP chair there,
he was one of the alternate electors and helped organize it. His current lawyers are saying
that he shouldn't be charged because he was relying on detailed advice at
counsel. Basically saying that former President Trump's attorneys told him
that this was okay and therefore he can't really have criminal intent
because he was trying to follow the law but the lawyers were wrong. Specifically
It says that every action by Mr. Schaefer as a presidential elector nominee or contingent
elector in 2020 was specifically undertaken in conformity with and reliance upon the repeated
and detailed advice of legal counsel, eliminating any possibility of criminal intent or liability.
I mean, I get it.
I get it.
from the intent side of it, it makes sense.
And I actually think that in a lot of situations,
this would matter.
Like this would go somewhere as far as providing
a pretty solid shield.
However, I'm not sure that it would work in this case.
I don't know how they're going to respond to this suggestion.
When it comes to far right
power grabs, generally speaking, I was just following orders.
Doesn't go over well.
I don't know because the argument itself
it makes sense. It makes sense.
Part of the elements of offense
in most crimes is that you intended to do something. You intended to
break the law. If they can actually show that he lacked intent, that's
going to be hard to overcome. Now it's important to remember that if you have
detailed advice of counsel, but let's say hypothetically speaking, and again this
is a real hypothetical, you talked to a different contingent elector and in
that conversation indicated that you knew it was shady and that contingent
elector accepted an immunity deal, you might be in trouble. Now I want to be
clear about this, I don't know that that's the situation. I'm just speculating
when it comes to the amount of secrecy that was involved in a lot of this,
while it is an interesting legal approach, I don't know that it's ironclad and a lot of people
seem to be pointing to the elements of offense and saying look there's no way that he could
have done it because he lacked intent and I get it but that's assuming that this defense that is
being put forward now saying don't charge him for this reason, holds up to
scrutiny. We don't know that it will. That being said, it very well might. If there
are a bunch of emails where he's asking questions and he's being reassured that
everything he's doing is legal and there are no other conversations, this might
actually be a pretty effective shield. We're gonna have to wait and see. Anyway,
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}