---
title: Let's talk about the GOP budget, veterans, and the diner....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=mcq7W86qRYQ) |
| Published | 2023/05/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau visits a small Southern town diner, which also serves as a town hall and meeting place for veterans.
- Most veterans at the diner are unable to work due to injuries.
- The diner chatter revolves around a Republican budget with significant cuts to veterans' benefits.
- A man at the diner insists Republicans wouldn't cut veterans' benefits, but he is corrected by another diner.
- The Republican budget passed with a 22% cut and no exemption for the VA.
- Veterans at the diner find it amusing when someone claims Republicans wouldn't use veterans as pawns.
- Beau points out the disconnect between the Republican budget cuts and the impact on their own base.
- Republicans are keeping quiet about specific cuts because they target their own supporters, including farm subsidies and veterans' benefits.
- The Republican budget aims to negotiate how much to cut from their own base.
- Beau warns against arguing with disabled veterans about politicians not using them as pawns.

### Quotes

- "Republicans are keeping quiet about specific cuts because they target their own supporters."
- "The Republican budget will target the Republican base."
- "Don't argue with disabled vets telling them that politicians wouldn't use them as pawns."

### Oneliner

Beau visits a small Southern town diner where veterans gather, revealing the disconnect between Republican budget cuts and their impact on their own supporters.

### Audience

Community members, veterans

### On-the-ground actions from transcript

- Support local veterans' organizations (implied)
- Advocate for transparent budget plans that prioritize community needs (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the impact of Republican budget cuts on veterans and the importance of transparency in political decisions.

### Tags

#RepublicanBudget #Veterans #Community #Transparency #Support


## Transcript
Well, howdy there Internet people, it's Bo again.
So today we are going to talk about the Republican budget
and veterans and my trip to the diner,
which turned out to be pretty illuminating.
So I'm hungry and I go up to the diner.
Now keep in mind, it's a small Southern town.
The diner is not just a diner,
it's also kind of a town hall and a meeting place.
I go in, I'm standing up by the counter.
counters where all the vets sit. All the veterans sit up there with, you know, black hats, yellow
lettering, says the name of their ship or has the ribbons to show where they were at, whatever. Most
of these guys are there pretty much every day because they're vets who got hurt and can't work
anymore. And because it is also the town hall, people are talking about the big news of the day,
which is a Republican getting really mad that the VA kind of gamed out how they would deal with these
massive cuts that are in the Republican budget. And there's a guy who is not sitting up at the
counter, mind you, who's sitting there in one of the booths, and he's like, Republicans are not
going to cut veterans benefits. And he's getting mad talking to whoever he's at the booth with.
One of the guys up the counter turns and he says there is no exemption for the VA in the
budget.
The guy yells, Republicans would not do that.
The guy at the counter turns back and looks at him.
In the budget, the Republican party passed with a 22% cut.
is no exemption for the VA. It's not would do that, they already did that. And
somebody else at the counter kind of chimes in and says they're trying to use
it to negotiate. And the guy that's sitting in the booth, he's like,
Republicans would not use veterans as pawns. The laughter that erupted from all
of those vets was possibly the funniest thing I have ever seen and somebody up
there kind of turned and looked at him he's like yeah politicians would never
use soldiers as pawns that's not a thing that would never occur. So there are a
couple of things that this kind of showcases. First, if you're going to try
to defend the Republican budget, again, a budget they passed that does not have an
exemption for the VA no matter what they say. They already voted on it. If you're
gonna try to defend that, I would suggest you not do it to a bunch of
disabled vets. That's probably not going to go over well because the
conversation got a little bit more heated from there. The other thing to
remember is that the budget that they have passed, the budget the Republicans
voted in favor of, it contains a 22% like top-line reduction, okay. The reason
they didn't go through and actually work out a real budget is because there is no
way for them to get the 22% reduction that they want without touching things
that their base needs. Understand if you're a Republican, you are part of the
Republican base and you are supporting the Republican Party on this, there's a
reason they're not telling you what they're going to cut. It's because it's
you. They're coming for you. They're coming for your benefits from government.
They're not going after what you think they're going after. It's going to impact you. That's
why they won't tell you. That's why they're keeping it quiet. That's why they're using vague
numbers and they won't tell you specifically what they're going to cut because it's your farm
subsidies, it's your veterans benefits, it's your pay. Keep in mind, a lot of
people who work in the trades work at places that are in some way funded or
subsidized by the federal government. They're coming after you with this
budget. That's why they're not telling you. They're saying that it's a
negotiating point using you as a pawn just like they did the vets at the
counter. But it's not to get to the point where they're not cutting you, where
they're not cutting stuff that's for you. They're just trying to negotiate how
much they're going to cut from you. The Republican budget will target the
Republican base. There's a reason they're not telling you. Don't forget that.
And please, do not argue with a bunch of disabled vets telling them that
politicians would not use them as pawns. That's how they wound up sitting at the
encounter in the first place.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}