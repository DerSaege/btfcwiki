---
title: Let's talk about Santos being indicted....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=g-Rlo7s7IPo) |
| Published | 2023/05/10 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Checking in on US Representative George Santos after three months of silence.
- Previous rumors about Santos being a secret spy for Trump debunked.
- Speculation arises as Santos is reportedly indicted, with charges under seal.
- Likely charges relate to alleged solicitation of money from wealthy donors.
- Impact on McCarthy's seat and Republican support discussed.
- Evidence against the existence of a government "super-secret war" emphasized.
- Warning against manipulation by false narratives and the importance of critical thinking.

### Quotes

- "Lying isn't illegal. It's not under most circumstances."
- "They will continue to manipulate you until you stop listening to them."

### Oneliner

Checking in on US Rep. George Santos' indictment reveals likely charges related to soliciting money from wealthy donors, impacting McCarthy's seat.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed and critically analyze political narratives (implied).
- Remain vigilant against manipulation and false information (implied).

### Whats missing in summary

The full transcript provides detailed insights into the speculation surrounding the indictment of US Representative George Santos and its potential impact on political dynamics.

### Tags

#USPolitics #GeorgeSantos #Indictment #McCarthy #CriticalThinking


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to check in on
US Representative George Santos.
It's been a while since we talked about him
and I figure it's a good time to check in
because it's been like three months
since we talked about him.
The last time we spoke about him,
It was when there was a rumor coming out of certain segments of the American
right that suggested he was actually a really good guy, that he was a super
secret spy working for Trump and Mago World.
And that's why his background was so sketchy, because he was like
undercover and stuff.
Their evidence of that was that the United States Department of Justice
stepped in and told the Federal Election Commission to back off. And I mean that
video outlined that sure that's a theory. He could be, you know, a super secret
double agent or something, but a more likely answer, more likely reason for DOJ
doing that is that he's under criminal investigation and they didn't want a
civil FEC investigation getting in the way of the criminal one. So in completely
unrelated news, reporting suggests that US Representative George Santos has been
indicted. The indictment is currently under seal and should remain so until
until he answers to it, meaning he turns himself in or he's picked up, one of the two.
Because it's under sealed, the exact nature of the charges is unknown.
There's a lot of speculation about it.
Most of the coverage is kind of rehashing everything that he has said, going through
of his various claims. That's not it. That's not what he's being charged over.
Lying isn't illegal. It's not under most circumstances. The video before the
last video, I have both of them down below, detailed an allegation and I said
that if he got in trouble this is what it was going to be about. It was an
allegation dealing with the idea that somebody from maybe his team posed as
somebody related to McCarthy's team and solicited money from wealthy donors. He
broke the big rule. He messed with rich people's money. That was the allegation
at the time was the time Republican outrage really started to kick up. You
know, they didn't care that he lied to voters, but when he started messing with
rich people money, oh man, they got mad. If I had to guess and I was going to
throw my hat into the ring on the speculation, probably has something to do
with that. At the end of the day, this will probably be a case dealing with
rich people money or alternatively dealing with how money was obtained to
to run his campaign. That's going to be my guess, but we're not going to know for
sure until he answers the indictment and it's unsealed. Either way, this is bad
news for McCarthy. McCarthy may end up a seat short now. He may lose one of his
votes on the Republican side of the aisle, but we'll have to wait and see how
it all plays out. But yeah, I will have those videos down below. Rest assured
that this is one more piece of evidence to show that there's probably not some
super-secret war going on inside the government. And if you were one of the
ones who fell into that and believed that, please remember this. It's been
three months. They lied to you. They told you that that's what was going on. Wasn't
true then, it's not true now. They will continue to manipulate you until you
not listening to them. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}