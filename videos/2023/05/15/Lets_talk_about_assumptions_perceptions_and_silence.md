---
title: Let's talk about assumptions, perceptions, and silence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=rDjlXx0M12A) |
| Published | 2023/05/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message prompting him to address perceptions and assumptions.
- Clarifies his stance on not discussing certain incidents and why.
- Differentiates between law enforcement versus private citizen incidents.
- Explains his knowledge of a lateral vascular neck restraint (LVNR).
- Stresses the importance of intent but doubts its relevance in a specific incident.
- Expresses disapproval of cheering for a homeless person in distress being taken out on the subway.
- Encourages critical thinking about what content creators cover.

### Quotes

- "I give everybody the benefit of the doubt on intent. That's super important."
- "It doesn't matter what you think happened in your breakdown of it. It's not good."
- "There's no way where this is good."

### Oneliner

Beau clarifies his stance on discussing incidents, explains the LVNR, and stresses the importance of intent while criticizing cheering for disturbing events.

### Audience

Content creators, viewers

### On-the-ground actions from transcript

- Re-examine beliefs on what content creators cover (implied).

### Whats missing in summary

Beau's personal reactions and emotional responses to the message and the incidents discussed.

### Tags

#Perceptions #Assumptions #LVNR #Intent #CriticalThinking


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about perceptions
and assumptions, and something I wasn't talking about,
something I had no intention of talking about,
to be honest, but I got a message
and it made me realize people might be misinterpreting
why I'm not talking about it because they're viewing
something through an incorrect lens,
and I just want to make sure that anybody else
who feels the same way, has that clarified for them.
So here's the message.
Hey, Bo, I've got to hand it to you
and call you out at the same time.
While most of the woke streamers are
calling for the Marines' head, you've been quiet.
I appreciate you not joining the woke mob on this.
Now I have to call you out.
Your silence says you only care about your narrative.
Even with all the views you could get,
you won't talk about it.
You are probably the only person on the left
knows an LVNR when you see one. Or maybe you know he had 42 charges and this was bound to happen.
Maybe you're just giving the Marine the benefit of the doubt on intent. Either way,
you explain these incidents in detail all the time and break them down for people unskilled
with tactics. You need to do it here. Do I, though? Do I break down these kinds of incidents all the
the time? Is this what I discuss in detail? See, I think what you see is an
altercation that is white person versus black person. That's not actually what I
break down in detail. I break down law enforcement versus private citizen. The
fact that the two are apparently indistinguishable? Well, I mean that's
commentary on law enforcement tactics, like all on its own, right? When you're
talking about private citizen versus private citizen, it's actually not
something I cover. It's not something I break down in detail. I may mention
something in passing, I may talk about an incident just as normal coverage, but I
do not break those incidents down. That's not something that happens.
The reason is that state violence, violence perpetrated by law enforcement, the jury system
doesn't really work very well there. And there are policies and standards and best
practices, things that can be applied. When you're talking about private citizen versus
private citizen, the jury system functions much better. And it's not based on any of
it's based on what's reasonable. Reasonable varies from jurisdiction to jurisdiction,
state to state, jury to jury. But, I will go through your points here.
You are probably the only person on the left that knows an LVNR when you see one. No, I'm not.
I am certain that there are other people. In fact, I know there are two people
on YouTube on the left that were actually trained in it.
For those that don't know, an LVNR
is a lateral vascular neck restraint.
Only person on the left that knows an LVNR when you see one.
I mean, yeah, I know one when I see one.
I know one when I don't.
I could even tell you if somebody attempted to apply one
and applied it improperly.
See, here's the thing.
LVNR, that's a super specific term.
It doesn't mean just any hold that looks vaguely similar.
LVNR is super specific.
So specific, in fact, I'm pretty sure
it's a registered trademark.
The reason people know that term and know that system
is because it has a wild claim to fame.
There's a reason everybody knows it.
It's because it's been in use, I want
say 40 years, decades for certain, and when it's properly applied, it's never
been shown to kill anybody. I've seen this term thrown around all over social
media. It's a very specific thing, and I would suggest that a system that has
a decades-long history of when it's used properly, not killing people. And what
happened on that subway? Probably a little different. So while I'm not going
to provide commentary on that incident and break everything down, it's
important to remember that an LVNR when properly applied has this has been used
for decades and nobody's ever been able to show that it killed someone. In fact,
I don't even think that they've had like a serious injury claim or maybe you know
he had 42 charges and know it was bound to happen. What's the difference between
42 and 34? I mean I know the answer is 8. I'm just wondering why one of them at
42 it seems like you're okay with somebody just being dispatched on a
subway and at 34 you want to put them in the White House again. Just wondering
what the difference is or maybe there's a lens you're looking at there as well.
And then the last bit, or maybe you're just giving the
Marine the benefit of the doubt on intent.
I give everybody the benefit of the doubt on intent.
That's super important.
That's really important.
But I'm going to be honest, I don't think intent is really
going to come into play here.
I don't think it's going to matter as much as people are
expecting it to.
So, there's that.
While I am not going to break this down in detail, what I will say is that I have absolutely
no problem saying that I don't think a homeless person under mental duress having an issue
should be taken out on the subway or if that happens, I don't think it's something that
people should be cheering about.
It doesn't matter what you think happened in your breakdown of it.
It's not good.
There's no way where this is good.
When you see somebody who covers something and they're not covering something that you
think is the same, maybe re-examine what it is you believe they cover.
And I do not see any situation in which people should be cheering or be happy about what
happened.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}