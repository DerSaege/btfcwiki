---
title: Let's talk about the Republican investigation into Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=J9AD88Z1wZE) |
| Published | 2023/05/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau questions the ongoing investigation into Biden by Republicans and expresses skepticism due to lack of evidence.
- Republicans are accused of rebranding similar claims with less evidence of influence peddling by Hunter and James.
- The Fox summary acknowledges no illegal activity by Joe Biden and lack of profits for him.
- A secret informant in the investigation has gone missing, raising concerns about their credibility.
- Beau questions the lack of urgency in finding the missing informant if the allegations were believed.
- International consulting firms linked to Biden's family received payments, which Beau sees as standard practice.
- Beau recalls his past investigation into claims against Biden and how they fell apart with a timeline analysis.
- Despite bank records not showing evidence and the missing informant, Beau predicts Republicans will drag out the investigation without producing substantial evidence.
- He suggests oversight of immediate family members' finances in high-ranking positions to prevent conflicts of interest.
- Beau proposes a bipartisan oversight committee including IRS and DOJ representatives to ensure financial transparency.

### Quotes

- "An informant in a Congressional investigation disappeared. They can't find him. They don't know what happened to him. Just gone with the wind."
- "If you're actually concerned about this type of stuff, create some oversight. You're the legislative branch of government. legislate."

### Oneliner

Beau questions the lack of evidence in the Republican investigation into Biden while advocating for financial oversight of high-ranking officials' family members.

### Audience

Legislators, government officials

### On-the-ground actions from transcript

- Advocate for bipartisan oversight of immediate family members' finances in high-ranking positions (suggested)
- Push for the creation of a committee including IRS and DOJ representatives to ensure financial transparency (suggested)

### Whats missing in summary

Detailed analysis and examples from Beau's past investigation into similar claims against Biden.

### Tags

#Biden #Republican #Investigation #FinancialOversight #Transparency


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Biden and the Republicans looking into
Biden and how that whole thing is going.
Um, about a week ago, we talked about it and I was less than believing of the
Republican claims primarily because when they made incredibly similar
claims about Biden's activities in Ukraine, I actually looked into them.
Um, and this honestly, it just seems like a rebranding of the exact
same claims with less evidence.
So the Republican House committee, the investigation that is looking into all
this. They came out and they gave their statement, a bunch of allegations and stuff like that.
This, what I'm about to read, is from Steve Ducey over on Fox.
He's a Fox News host, just to be clear, and this is how he summed up the evidence that the Republican
The Republican investigators say that that's proof of influence peddling by Hunter and James.
But that's just your suggestion. You don't actually have any facts to that point.
You have some circumstantial evidence.
Of all those names, the one person who didn't profit, there's no evidence that Joe Biden did anything illegally.
illegally. That's the Fox summary of this. So, needless to say, at this point the
investigation is not going well for the Republican Party. But wait, there's more.
One of the congresspeople was on Fox and had to disclose that their super
secret sort of spy informant that they claim to have, they lost him.
They don't know where he is or they are.
Um, we're hopeful that the informant is still there.
The whistleblower knows the informant.
The whistleblower is very credible.
Let's be real for a second.
Let's just pretend that this is all, like, even remotely true, like, that Republicans
actually believe what they're saying here.
An informant in a Congressional investigation disappeared.
They can't find him.
They don't know what happened to him.
Just gone with the wind.
And their response is to be like, oh, well, we hope we find them.
If they believed any of this, they'd have an FBI investigation looking for this person.
An informant in an international influence peddling operation goes missing and nobody
cares.
I find that hard to believe.
seems like somebody would want to locate them because it might lead to... I mean
that would suggest that maybe something happened to them, right? But they're not
making that allegation. My guess is they don't want this person found. Maybe they
found out that the informant wasn't very credible. But that's a guess. At the end
of this, what you have is some international consulting firms that were linked to family
members of Biden got international payments.
That seems to be the totality of the evidence thus far.
I would point out that that's how international consulting firms make their money.
Again, I am incredibly skeptical of this because I went through and actually did all of the
digging on the claims about Biden intervening over the investigation in Ukraine and all
of this stuff back when he was vice president.
And I have a video on it.
As soon as you put together a timeline of events, the whole narrative falls apart.
It doesn't make any sense.
The reality is that investigation that was allegedly into Hunter Biden started before
Hunter Biden ever had anything to do with the company.
And like that's the strongest part of the case.
It just falls apart from there.
Because of my experiences with that, I am very skeptical of these claims.
now that they've had the bank records and they actually couldn't find any evidence and their
informant has disappeared and they don't seem to care, I'm going to suggest that they're going
to continue to try to drag this out but at the end of it they'll never produce any real evidence
of any wrongdoing. That being said, maybe it's time to have some kind of
oversight of the finances of immediate family members, of people
in, I don't know, let's say the White House, Congress, and the Supreme Court.
And I understand that the people, their family members,
they didn't run for office.
So maybe do close doors, you know?
Have four people from each party
and a representative from the IRS and the DOJ,
and just make it a normal thing
where this stuff gets reviewed.
If you're actually concerned about this type of stuff,
create some oversight.
You're the legislative branch of government.
legislate.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}