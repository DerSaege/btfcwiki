---
title: Let's talk about the conventional and unconventional....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=A4W8xtKXTzM) |
| Published | 2023/05/15 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the difference between conventional and unconventional forces in the context of the US military.
- Points out that unconventional forces are more capable of degrading US capabilities in conflict.
- Attributes the increased capability of unconventional forces to advancements in technology.
- Gives an example of drone technology and its impact on battlefield awareness for unconventional forces.
- Mentions the abundance of open-source intelligence available to the average person.
- Stresses that technology has provided unconventional forces with tools for surprise attacks.
- Talks about the increased lethality of individual soldiers, regardless of being conventional or unconventional.
- States that unconventional forces can keep fighting until the political resolve of the major power breaks.
- Notes that major powers often fail to adapt their military doctrine to effectively combat unconventional forces.
- Concludes that the US military may struggle more against unconventional forces than against conventional forces like China or Russia.

### Quotes

- "It's really, it kind of boils down to two things."
- "The one thing that is a determining factor."
- "The United States has a better chance of going toe-to-toe with China or Russia in achieving a decisive victory."
- "The technology, the information awareness that the unconventional force has access to today."
- "It's really hard for a major power to combat that."

### Oneliner

Beau explains why unconventional forces pose a greater challenge for major powers due to technological advancements and information access, making them harder to combat effectively.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Analyze and adapt military strategies to effectively combat unconventional forces (implied)
- Utilize open-source intelligence for informed decision-making in conflict situations (implied)
- Build relationships with local populations to prevent sympathy towards unconventional forces (implied)

### Whats missing in summary

In-depth analysis and examples showcasing the impact of technological advancements on the capabilities of unconventional forces.

### Tags

#Military #Technology #UnconventionalWarfare #Intelligence #Conflict


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about
the conventional and unconventional.
And we're gonna do this because I got a question.
The question itself is self-explanatory.
The way it was phrased, the answer's already there.
But I think I know what the person
was actually trying to ask.
So we're gonna go over that because I think
other people have the same question.
When I'm talking about the US military I will often say something along the lines of,
there is not a conventional force on the planet that can go toe to toe with the US military
and win.
That's not American exceptionalism, that is statement of fact.
is not a conventional force on the planet that can go toe-to-toe with the
US military. The person asked why do I always specify conventional? I mean the
obvious answer to that is because unconventionally an unconventional force
is much more capable of degrading US capabilities in winning a conflict. But I
think that the person knows that. I think they're what they're really asking is
why is an unconventional force more capable? And the answer is pretty simple.
It's really, it kind of boils down to two things. The first is how technology
has changed and I'm going to give some specific examples but it's important to
remember that unconventional conflicts forever, okay, they're hard to win to
begin with but the one thing that the state actor had that the unconventional
force typically didn't was access to information, a real intelligence network.
work. Today, in fact right over there, I have a drone that we got for the Channel
for Bo on the Road and we got it because right around the beginning of the
pandemic a friend decided that flying drones was going to be their their stay
at-home hobby and they got a new one. We got their old one. It was obsolete, okay,
near the beginning of the pandemic. This person was like, ah, this isn't good
enough for me now. I can throw this thing in the air and get a complete view of
everything that's going on in my immediate area. That kind of battlefield
awareness, that is something that unconventional forces didn't have access
to not too long ago. They had to use human sources to get that. Now drones you
can buy at Walmart can do that. The one that he has now he had to get a license
from the FAA for and I mean this thing day night anything it's amazing but it
was something he was able to just buy. Those kind of resources didn't exist.
Right now, if you use the satellite that is used to track fires, you can tell what's being
shelled in Ukraine.
The amount of open source intelligence that is available to the average person is just
astounding.
makes an unconventional force a whole lot more capable because when you're
fighting unconventionally what is it? Hit, run, surprise, right? To surprise you have
to have information and technology has handed unconventional forces that tool,
A tool that they didn't have half a century ago.
The other thing is the lethality of the individual soldier.
Conventional or unconventional doesn't matter, but the individual soldier now packs a punch.
So there was a time when a conventional military going to engage in something that would prompt
unconventional responses, they'd be fighting older equipment.
They'd be fighting equipment that didn't really have the same punch.
times the unconventional force was outgunned. That's not really true anymore.
Those two things right there make it incredibly hard. And then you have the
the one thing that is a determining factor. Large powers, they don't do it right. They
try to put down the unconventional force through force and most times that makes the unconventional
force grow. Remember, they don't have to win. They just have to keep fighting until political
resolve breaks in the country that has the conventional force. All they have to
do is just keep going. The US or whatever major power is combating an
unconventional force, they have the watches. The unconventional force has the
time. They just have to wait them out. And military doctrine just refuses to adapt
to this fact. So that's why that caveat is in that statement. The United States
has a better chance of going toe-to-toe with China or Russia in achieving a
decisive victory than it does trying to achieve a decisive victory in a country
that most people in the United States have never heard the name of because the
force there isn't going to fight in the conventional sense. They're going to use
surprise. They're going to use the tools available to them and all they have to
do is keep going. Eventually the United States or whatever the power is will tap
out, the political resolve will fail. It's really that simple. It's not that the
United States is just somehow incapable. The U.S. actually knows how to fight
against an unconventional force, but they're not willing to take the
reorganizational steps to do it.
And the technology, the information awareness
that the unconventional force has access to today
is enough to tip the scales in their favor.
If they combine open source intelligence
with the normal human sources that unconventional networks
tend to have, it's really hard for a major power
to combat that, especially when the unconventional force has the sympathy of the locals or the
major power is there doing things to make sure that the local populace stays sympathetic
to the unconventional force.
It's just, it's different, and major powers, and it's not just the US, major powers do
not have a good track record of this.
They didn't have a good track record before the information age.
The information age just shifted it heavily in favor of the unconventional force.
So that's why that little caveat is always there, and it will be for the foreseeable
future. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}