---
title: Let's talk about a Tuberville quote and readiness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TyRKxq5N554) |
| Published | 2023/05/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Tuberville suggested that white nationalists should be allowed to serve in the military, referring to them as Americans.
- Tuberville blames Democrats for the military's recruitment struggles, claiming they are attacking white extremists.
- He believes that imposing rules for specific groups in the military weakens the institution.
- Tuberville blocked promotions of around 180 generals, hindering military readiness.
- Allowing white nationalists in the military can harm unit cohesion and readiness.
- Tuberville's team clarified that he was skeptical of the presence of white nationalists, not supporting their inclusion.
- His actions go against improving military readiness by promoting disunity within the ranks.
- The Senator's stance on military recruitment and white nationalists has sparked controversy.
- Tuberville's beliefs and actions are seen as detrimental to the military's effectiveness.
- His approach contradicts the goal of maintaining a strong and united military force.

### Quotes

- "They call them that, I call them Americans."
- "We cannot start putting rules in there for one type, one group, and make different factions in the military."
- "This statement is just wild to me."
- "He wants to help military readiness by allowing people who break down unit cohesion."
- "Anyway, it's just a thought."

### Oneliner

Senator Tuberville's controversial views on military recruitment and white nationalists undermine unit cohesion and readiness, hindering the effectiveness of the military.

### Audience

Military personnel, policymakers

### On-the-ground actions from transcript

- Challenge discriminatory rhetoric within military recruitment (implied)
- Advocate for inclusivity and unity within the military (implied)
- Support efforts to maintain strong unit cohesion (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of Senator Tuberville's concerning views on military readiness and recruitment, shedding light on the implications of his statements and actions.

### Tags

#SenatorTuberville #MilitaryReadiness #WhiteNationalists #UnitCohesion #Controversy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Senator Tuberville
and military readiness and some quotes
that I'm really not making up.
This was really said.
And we're going to talk about the overall readiness picture
in regards to this after the quote.
Okay, so the Senator was asked if he felt white nationalists should be
allowed to serve in the U.S.
military.
He said, they call them that, I call them Americans.
Yeah, go ahead and take your time with that.
He also said, we are losing in the military so fast, our readiness in terms of recruitment,
and why?
I'll tell you why.
Because the Democrats are attacking our military, saying we need to get out the white extremists,
the white nationalists, people that don't believe in our agenda as Joe Biden's agenda.
They're destroying it.
This year, we will not reach any recruiting goals in the military.
So if we want to talk about looking weak, that's where we're going to look weak.
We cannot start putting rules in there for one type, one group, and make different factions
in the military because that is the most important institution in the United States of America
and our allies is a strong, hard-nosed killing machine, which is called our military.
Okay, now for those who do not know who Senator Tuberville is, checks notes, the Senator currently
famous for impeding military readiness by blocking the promotions of a whole bunch,
I want to say like a hundred and eighty generals and admirals, command officers, big officers.
Yeah, so apparently he believes that by allowing these people in there won't be different factions
in the military.
The reason the military doesn't want these people in is because it breaks down unit cohesion.
If you have that type of person in a unit, well some people in the unit are
obviously not going to be accepted by them. That hurts unit cohesion, that hurts
military readiness. This statement is just wild to me. I feel like I
should note that his team tried to like clear it up and said, Senator
Tuberville's quote that is cited shows that he was being skeptical of the
notion that there are white nationalists in the military, not that he
believes they should be in the military. Okay, so he is skeptical of a fact. He's
skeptical of something that has been proven, that has been demonstrated, and
by arguing against their removal, he is once again, just like with the promotions,
impeding military readiness, which apparently is his talking point here. He
wants to help military readiness by allowing people who break down unit
cohesion and by not allowing promotions so people can take their commands.
Yeah, so that's the Republican Party today.
Anyway, it's just a thought.
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}