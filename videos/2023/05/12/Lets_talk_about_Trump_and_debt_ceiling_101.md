---
title: Let's talk about Trump and debt ceiling 101....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=pQIP-fvAc6o) |
| Published | 2023/05/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains Trump's statements on the debt ceiling and provides a "default 101" to clarify misconceptions.
- Trump indicated that without massive budget cuts, the country may face default, which is not surprising considering his history of not paying bills.
- Trump owns a disproportionate $7.8 trillion of the debt, contradicting his claim of spending money like "drunken sailors."
- Reveals that the debate is not about negotiating the debt ceiling but rather Republicans choosing not to raise it, endangering economic stability.
- Points out that Biden is willing to either raise or eliminate the debt ceiling, contrary to the false narrative of negotiation.
- Emphasizes that the Republican party, particularly McCarthy, Trump, and hardline Republicans, are to blame for risking economic damage by not raising the debt ceiling for leverage.
- Warns against falling for misinformation that aims to shift blame onto Biden for potential economic suffering due to budget cuts.
- Condemns the plan to make the public endure hardships to achieve political goals, stressing that any potential default is a deliberate push into economic calamity by certain Republicans.

### Quotes

- "The Duke of debt, the Baron of bankruptcy doesn't want to pay his bills."
- "If the country defaults, it is the Republican party's fault, nobody else."
- "Make no mistake about this. If the country defaults, it wasn't an accident. It didn't fall. It was pushed into economic calamity and the people that pushed them, they're wearing red hats."

### Oneliner

Beau explains Trump's debt ceiling stance, clarifying it's Republicans jeopardizing stability, not negotiation, aiming to blame Biden for economic damage.

### Audience

Voters, Political Activists

### On-the-ground actions from transcript

- Contact your representatives to urge them to prioritize raising the debt ceiling for economic stability (suggested).
- Stay informed about the debt ceiling debate and hold accountable those responsible for risking economic hardship (implied).

### Whats missing in summary

The detailed breakdown of Trump's statements on the debt ceiling and the Republican party's role in potentially causing economic calamity.

### Tags

#DebtCeiling #Trump #Republicans #EconomicStability #PoliticalResponsibility


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump
in his statements on the debt ceiling.
Um, and we're going to provide default 101,
so everybody understands something,
and we're going to address some terminology
that is being used that is not true.
Okay.
So we'll start off with what Trump said.
He said, quote, I say to the Republicans out there, congressmen, senators, if they don't
give you massive cuts, you're going to have to do a default.
Okay I mean, first let's start there.
I mean, is anybody surprised by that?
The Duke of debt, the Baron of bankruptcy doesn't want to pay his bills.
Shock.
gasping, I'm gasping. A guy known for not paying his bills doesn't want to pay
his bills and they are in fact his bills. 7.8 trillion dollars of it is his. A
disproportionate amount. Which is funny because he also said that we're spending
money like drunken sailors I mean we meaning him yeah that's true that
happened we the royal we no that's not the case 7.8 trillion of the debt is his
he owns it. Now there is an element in this that is kind of interesting
because he unintentionally told the truth. The Republican Party is trying to
frame this as a negotiation over the debt ceiling. That is not true. When it
comes to the debt ceiling there's two options and then from there there's
There's really only three ways to deal with it.
You either don't default or you do.
And as far as how you go about that, you either raise the debt ceiling or get rid of it or
you do nothing and you default, right?
Now Biden will sign a bill that says, Hey, we're raising the debt ceiling.
He will sign a bill that says, hey, there's no more debt ceiling.
There's no negotiating over the debt ceiling.
That's not happening.
The Republicans are choosing not to raise the debt ceiling, putting your economic stability
at risk.
There's no debate happening over that.
It's what Trump said.
I'm sure this was an accident and I didn't mean to say this clearly.
If they don't give you massive cuts, you're going to have to do a default.
The two things are unrelated.
The Republican party is using one to try to get leverage on the other.
They want a budget that has all of those cuts we talked about in the other video.
that would damage the economy.
What they are hoping is that you are too ill-informed to understand that if the country defaults,
it is the Republican party's fault, nobody else.
Nobody else shares any blame.
And it's not even all of the Republican party.
It's McCarthy, Trump, and the hardline Republicans in the House.
That's who's really to blame because there are moderates in the House who would totally
pass a normal debt ceiling bill. So they're hoping that you are too
ill-informed to understand that the debt ceiling and the budget are not
naturally related. That they're using one as leverage to get what they want in the
other. Those 22% cuts, they would severely damage the economy and they're hoping
that you're too ill-informed and will blame that on Biden. That's the point of
all of this. The goal is to make you suffer so you'll take whatever they
throw your way. That's the plan. Make no mistake about this. If the country
defaults, it wasn't an accident. It didn't fall. It was pushed into economic
calamity and the people that pushed them, they're wearing red hats. Anyway, it's
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}