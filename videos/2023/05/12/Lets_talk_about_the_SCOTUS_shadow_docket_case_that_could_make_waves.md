---
title: Let's talk about the SCOTUS shadow docket case that could make waves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=QL9KI-fQKcs) |
| Published | 2023/05/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the potential impact of a Supreme Court case on gun control in the United States.
- Mentions the National Association for Gun Rights versus the city of Naperville case on the shadow docket.
- Describes the Supreme Court's options with the gun rights case, including ruling narrowly or widely.
- Suggests that the Supreme Court might either fully do away with assault weapons bans or open the door for another case to challenge them.
- Points out that the current balance of the court indicates a likelihood of reversing previous gun control legislation.
- Urges people to be prepared for potential changes and start considering alternative options to mitigate harm.
- States that the odds of the gun control side winning before this court are slim.
- Encourages individuals to look at other solutions to reduce harm and save lives as the current solution may not be viable for long.
- Emphasizes the importance of being aware and ready for the outcome of the court's decision.
- Advises against getting too fixated on one solution as circumstances may change.
- Indicates that people with a vested interest in a particular solution may need to acknowledge the current reality until the court changes.
- Suggests that it's time to start exploring alternative strategies in light of the potential Supreme Court decision.

### Quotes

- "It's probably time to begin looking at other options, even if they are more expensive, to try to mitigate some of the harm."
- "Don't get so locked into one solution because that solution it may not be on the table much longer."
- "Until the court changes, that's the way it is."

### Oneliner

Beau warns about potential impacts of a Supreme Court case on gun control, urging readiness for change and consideration of alternative solutions.

### Audience

Advocates for gun control

### On-the-ground actions from transcript

- Prepare for potential changes in gun control laws by staying informed and engaged (implied)
- Advocate for alternative solutions to mitigate harm and save lives in the face of potential legal challenges (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the implications of a Supreme Court case on gun control laws, urging individuals to be proactive and adaptable in seeking solutions.

### Tags

#SupremeCourt #GunControl #ShadowDocket #LegalChallenge #CommunityAction


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the Supreme Court
and a case that it has on its docket, on the shadow docket
that could have some pretty wide-ranging impacts
that will probably cause a lot of tension
in the United States.
One of the questions that I get pretty often
in when people are asking about that video,
let's talk about a comprehensive gun control plan
or something like that,
people ask why I didn't say
that there should be an assault weapons ban.
Now there's actually a whole bunch of reasons for that,
but the main one in that video
is the criteria was that it had to be able
survive the Supreme Court. And the rebuttal to that from people has always
been, well, you know, they've upheld state ones. Previous courts did, but there was a
a lot of settled law that has been overturned. Sitting on the Supreme Court
shadow docket right now, if you don't know what the shadow docket is, it's a
docket for like emergency hearings and stuff like that. There's a case, National
Association for Gun Rights versus the city of Naperville, Naperville, I don't
know how to pronounce it, and the Supreme Court has a number of options with it.
They could decide very narrowly or they could just decide widely. Now the shadow
docket cases, they don't get like a full hearing, they don't get the full treatment, a lot of times
they don't even get mentioned in the press, but the Supreme Court has options. If they rule narrowly
on it, it will probably open the door to a full challenge where this court will review assault
weapons bans at the state and local level, or they could just decide to rule
widely on it right away, in which case they might strike down every assault
weapons ban and high capacity magazine, what gets termed high capacity
magazine ban in the country all at once. The case is already there, it's on
the docket. We don't know what they're going to do with it, but this is one that
it shouldn't be a surprise. When whatever happens when it happens, I feel like with
this court, the most likely outcome is going to be either fully doing away with
assault weapons bans or opening the door for another case to do away with them so they
can get the full hearing.
The balance of the court right now, it seems incredibly likely that this court would reverse
a lot of previous gun control legislation.
And this might be the start of it.
So it's one of those things that people just need to be ready for.
And it's probably time to begin looking at other options, even if they are more expensive,
to try to mitigate some of the harm.
Because the odds of the gun control side winning before this court is pretty slim.
It's possible, but it doesn't seem likely.
And that case has made it to their shadow docket, so this is something that we should
find out about in the next few months.
that when that decision happens, whatever it is, there are going to be a lot of people
who are heavily invested in that as a solution that are going to have to acknowledge that
until the court changes, that's the way it is.
So it's time to start looking at other options, other things that are on the table that can
be used to mitigate, reduce harm and save lives. Don't get so locked into one
solution because that solution it may not be on the table much longer. Anyway
It's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}