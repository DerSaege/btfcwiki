---
title: Let's talk about Russia, Ukraine, and Garland....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=TcXvrDzMN20) |
| Published | 2023/05/12 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia is altering legal mechanisms to make conscription easier, using Instagram messages to notify individuals.
- The changes indicate Russia’s realization of a protracted fight and the need for more troops.
- Ukraine has made modest gains near Bakhmut, causing Russian forces to suffer a significant morale blow.
- Zelensky has hinted at delaying the expected counter-offensive due to potential high Ukrainian casualties.
- The Department of Justice has decided to use funds from oligarchs to support Ukraine, a change from freezing the money.
- Both sides in the conflict are gearing up for a heavy phase, with Ukraine relying on support from other countries.
- The support Ukraine receives is vital for them to continue resisting Russian aggression.

### Quotes

- "Russia is realizing that they're in a protracted fight."
- "Ukraine paid an incredibly high price for those couple of kilometers."
- "If their resolve breaks, it's not a political loss. It will lead to real loss there."

### Oneliner

Russia is altering conscription laws, Ukraine makes modest gains, and support for Ukraine is critical in the ongoing conflict with Russia.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support Ukraine through donations or spreading awareness (implied)
- Stay informed about the situation in Ukraine and Russia (implied)

### Whats missing in summary

Insights on the potential ripple effects of global support for Ukraine

### Tags

#Ukraine #Russia #Conflict #Support #Conscription #GlobalAwareness


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Ukraine and Russia
and Merrick Garland, oddly enough,
and just kind of go through some of the decisions
and realizations that have been made
and give a little bit of a lay of the land
and what's happening over there.
We will start with Russia.
Now, recently, the authorities there, they've been altering the legal mechanisms by which
they can tell people, hey, you've got to go fight.
They've been making it easier to draft people, to conscript them.
One of the changes was they made it to where the authorities didn't have to show up with
papers that you had to sign.
They could just like message you over Instagram.
And if you didn't respond, you didn't show up, you didn't report, you faced penalties.
So that is a pretty clear indication that Russia is realizing that they're in a protracted
fight.
Troops won't be home by Christmas, and they need people to send to the grinder, plain
and simple.
They're making it easier to get those.
Another thing that is going on right now is it appears they are calling up some reservists.
Now the way it's currently being framed is, you know, this is your annual training one
week and a month, two weeks a year type of thing.
And this is normal.
Don't worry about it.
The expectation is that once those troops report, they will be pressured into volunteering
to go fought.
Since all this started, a number of people who have been accessing this channel have
asked questions about whether or not they should stay or go where they're at.
If you were one of those who was waiting for the right time to maybe make a move, make
a change in your life, sit just.
Okay, so let's go to Ukraine.
Around Bakhmut, Ukrainian forces have made some gains.
They're modest.
We're only talking about a couple of kilometers here.
But it's really important to remember that Russia paid an incredibly high price for those
couple of kilometers.
lose them is huge from a morale perspective. Those who understand what's
happening there and as that news travels through the Russian forces it's one of
those things where you start to feel like it's pointless because they paid a
A lot, a lot to get that, and they don't have it anymore, all for nothing.
Now Zelensky has kind of indicated that the counter-offensive that everybody's been expecting,
he's saying that right now it might cost too much, it might cost too many Ukrainian lives,
so it's on hold.
immediately there was conversation. Is that real? Is he, you know, trying to psych
out the Russians? You know, what's going on? I don't know. And even if I did, I wouldn't say.
But I really have no clue. It could be either. It could be either. There's a lot
of equipment pouring in, so they may want to get that positioned. It could be
something that simple. So on that, when it comes to when the
counteroffensive is going to start, we don't know. And that's
good. That's good. And now to Merrick Garland. So the
Department of Justice has decided funds that were
forfeited, liberated, whatever term you want to use, funds that
came from oligarchs, that money can now be used to support Ukraine. Now at time
of filming, I don't know whether or not that money is going to be sent to Ukraine
or it's going to be used from outside the country to support efforts there.
That part isn't really clear in what I have at time of filming, but that is,
that's a change. Up until now I think most of it has just kind of been frozen.
It looks like they're going to start accessing it and using it. And then I
think the other thing that is interesting is I heard something about
Poland renaming a city and Russians are getting really mad. So everything
that's happening there, both sides are aware of what's coming. Both sides
seem to be very aware that they're going to be entering a very heavy
face soon, and again, as most people watching this are Americans or British or Canadians,
the support that Ukraine currently has is what's enabling them to continue to try
to break the hammer.
It's going to be protracted.
So it's one of those things where if those in countries that are supporting Ukraine,
if their resolve breaks, it's not a political loss.
It will lead to real loss there.
So anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}