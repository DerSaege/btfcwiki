---
title: Let's talk about running into Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=v3wFRTkH3qg) |
| Published | 2023/05/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the recent events in Russia involving non-state actors moving into Russian territory and conducting raids.
- Points out the potential downstream effects and uncertainties surrounding the situation.
- Emphasizes that these non-state actors, although somewhat linked to Ukraine, are Russians or have Russian passports.
- Compares the current situation in Russia to past instances where Russia took Ukrainian land with the involvement of non-state actors.
- Raises questions about whether this is a sustained effort or just a diversion tactic.
- Notes the risk of discontent in Russia leading to wider actions and the potential for similar movements in other regions.
- Addresses the distinction between these non-state actors and the Ukrainian Armed Forces.
- Comments on the military effectiveness of the situation, dependent on the level of discontent and potential organic actions.
- Observes weaknesses in Russia's response and the expected increase in border security.
- Mentions the demoralizing impact within Russia and the group's goal to depose Putin.
- Emphasizes that these actions are not the same as Ukraine's military actions and are an internal security matter for Russia.
- Acknowledges the uncertainties regarding the long-term implications of the events in Russia.

### Quotes

- "These were non-state actors. These were Russians."
- "Welcome to the hard part."
- "It's an internal security matter that sure, it's related to, spawned by, Russia's invasion of Ukraine, but it's not the Ukrainian military."

### Oneliner

Beau explains recent events in Russia involving non-state actors, raising questions about sustainability and potential wider actions, stressing the internal security nature of the situation.

### Audience

Analysts, policymakers, activists

### On-the-ground actions from transcript

- Monitor developments in Russia and stay informed about the situation (suggested).
- Engage in dialogues about the implications of non-state actors' actions in Russia (implied).

### Whats missing in summary

Insights on the potential geopolitical ramifications and global responses.

### Tags

#Russia #NonStateActors #Ukraine #InternalSecurity #Geopolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about what happened in Russia.
And we're going to go over the importance of it, whether or
not it's going to be sustained, whether or not this is the same
as what Zelensky was talking about, whether or not this is
Russia's next mistake.
and we're just going to kind of run through everything because there are a
lot of downstream effects from this that may not be apparent at first. So if you
have absolutely no idea what I'm talking about right now, a group of non-state
actors moved into Russian territory and conducted some raids. Now, last bit of
information I got was that there are still some there conducting raids. Now,
these people are somewhat aligned in theory with Ukraine, but they're not the
Ukrainian Armed Forces. They are non-state actors. They're Russians, or at
least have Russian passports. Now, I know that some people are going to want to
draw a distinction and say that they're not really separate from the Ukrainian
Armed Forces. Yeah, can't do that now. I would point you back to the initial
invasion before this one, when Russia first started taking Ukrainian land, and all of
those people that were not really Russian military, the contractors, the special operations,
those folks.
You know the rules and so do I.
You can't change them now.
These are non-state actors.
They have claimed responsibility for them.
There's two separate groups.
Now, is this going to be sustained, or is it just a diversion?
Just something to get them looking somewhere else and kind of trick them a little bit?
We don't know.
We don't know if this is a full commitment, if they are just never going to give it up,
or this is something that is relatively short-lived.
could be gone tomorrow, or they could never desert it. But once something like this starts,
there's always the risk of it organically widening. There is a lot of discontent in Russia, a whole lot.
Once actions start being taken, they might be taken in other places, maybe even
deeper inside Russia, especially if, let's say, all of the Russian border
services were looking in one specific direction at one time, I mean that might
be the ideal time for other teams to slip across the border unnoticed, just
saying. So we don't know whether it's a full commitment, we don't know whether if
if it's just a diversion, but there will probably be more of some kind.
Is this the same as what Zelensky was talking about?
Does it carry the same risks?
No, these are Russians.
These are non-state actors.
Moscow can whine and complain all they want, but these are non-state actors.
Putin has denied involvement, the people involved have claimed it for themselves, they are Russian.
This is an internal matter for Russia to sort out.
So it doesn't carry the same risks, in theory.
I mean, Putin has kind of lost his edge, so he may make bad decisions he has before, but
shouldn't. Now, is this going to be militarily effective? Depends on how much
discontent there really is. We know there's some, but whether it crosses that
threshold to create organic action elsewhere, we're gonna have to wait and
There's no way of knowing that beforehand, but the movements that occurred on the Russian
side in response to this shows that they're much weaker than I think most anticipated.
people who have been very critical of Russia's performance expected them to at least have
some real presence along their borders in the middle of a war.
That would seem like kind of Warfare 101, but it doesn't appear that they really did.
Not anything that wasn't quickly overcome or bypassed.
So at bare minimum, I think that Russia is probably going to increase security along
their borders, which means, well, every troop that's on the border isn't in Ukraine.
So while this may just seem like a PR thing, and in many ways it is, it's going to have
a tangible effect.
I mean, even if you only pull 1,000 troops off, that's 1,000 troops.
So there's that.
There's also the PR factor within Russia, the demoralizing factor, because they are
going to view it as being linked to Ukraine, but they're Russians.
statements are out through videotape. So they will make the connection
eventually between their actions in Ukraine and this non-state actor, this
partisan group that is aimed at the Kremlin. They have been clear in that
their goal is to depose Putin. We'll see how popular that idea is. It could be
nothing or we could see Swan Lake soon. We don't know. But this is not the same
as Ukraine doing it and that's a big thing that everybody needs to keep in
mind. These were non-state actors. These were Russians. It's an internal security
matter that sure, it's related to, spawned by, Russia's invasion of Ukraine, but it's
not the Ukrainian military.
That's an important distinction that everybody needs to be making.
But as far as the long-term side of this, we don't know.
If it is a full commitment, all I can say to Russia is, welcome to the hard part.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}