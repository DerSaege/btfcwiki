---
title: Let's talk about what happened in DC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=3C_m1vykNhM) |
| Published | 2023/05/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A U-Haul crashed into the barricades in Lafayette Park near the White House around 10 p.m.
- Secret Service investigated the scene, and a robot was used to ensure the vehicle's safety.
- Authorities found a flag in the U-Haul during the search.
- Beau researched the incident but found right-wing commentators on Twitter spreading conspiracy theories.
- Beau questions the shift in the right-wing's stance towards certain symbols and ideologies.
- He recalls a time when politicians like George Bush Sr. distanced themselves from negative symbols and rhetoric.
- The Republican Party's defense of certain symbols and ideologies has drastically changed over time.
- Beau urges people to critically analyze their beliefs if they echo dangerous ideologies.
- He points out the evolving nature of political ideologies, especially the right-wing becoming more authoritarian.
- Beau concludes by noting that those defending certain symbols haven't changed amid political shifts.

### Quotes

- "If you ever find yourself in a situation where the behavior of somebody who has one of those flags reflects poorly on you, there were a lot of poor decisions that were made along the way."
- "The right wing in the United States has changed, and you can say that the left wing has changed too, that they've moved to more and more progressive positions."
- "The people who fly that flag haven't changed."

### Oneliner

Beau delves into the U-Haul incident near the White House, exposing right-wing deflection and the changing nature of political ideologies, urging critical self-reflection.

### Audience

Activists, Political Observers

### On-the-ground actions from transcript

- Examine beliefs and ideologies to ensure alignment with values and ethics (implied).

### Whats missing in summary

A deep dive into shifting political ideologies and how they impact societal perceptions and behaviors.

### Tags

#DC #RightWing #PoliticalIdeologies #ConspiracyTheories #SelfReflection


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about what happened up in DC.
And then we're going to talk about what happened while I
was looking into what happened up in DC, because that's a
wild ride in and of itself.
OK.
So if you have no idea what I'm talking about, at this
point, let's just say that around 10 p.m. or so there was a, let's call it a car
accident, a U-Haul slammed into the barricades in Lafayette Park, which is
right by the White House. And then apparently, like, tried to reverse and
tried to go around them but hit something else. What it seems like at this
point. Obviously this drew the attention of the authorities. It piqued my
curiosity because even though I haven't covered it on the channel I don't think
there is a large quantity of something that makes things go boom that's missing
right now. That fact and a U-Haul definitely had me interested. Secret
Service got involved and eventually a robot was brought out to check out the
vehicle and it was deemed safe. And last I heard it does look like there are
going to be charges filed against this person. What kind? I don't really know yet.
But during the search a flag was pulled out of the vehicle and I knew what it
looked like but I didn't get a clear view of the center from the footage that
I was looking at. It was a red flag, it had a white circle, and then it had some
black design in the middle of it. I mean you can probably guess what it was, and
that's what I thought it was, but I wasn't sure. So because I like to be
right and out of habit and forgetting about the whole Elon Musk takeover of
Twitter, I hopped on Twitter and typed in U-Haul into the search bar because there
was a time, pre-Musk, where if you did this, which you would get after, you know, some
event, you would get a whole bunch of journalists from the local area sharing their reporting.
You'd get photos, different angles, footage, stuff like that.
And I would be able to confirm that it was, in fact, what it looked like.
But that's not what you see.
That's not what you get anymore.
What I found was a whole bunch of right-wing commentators with blue check marks talking
about how it was a setup, how it was awfully convenient.
Obviously, Biden was behind it and doing this to make them look bad.
Why would somebody with that flag doing something make you look bad?
I mean, that's kind of a statement, isn't it?
If you ever find yourself in a situation where the behavior of somebody who has one of those
flags reflects poorly on you, there were a lot of poor decisions that were made along
the way.
When did it start?
Because the right wing in the United States, there was a time when they understood that
fasch was bad.
There really was, and it wasn't that long ago.
did it start? Because this isn't the first time. It's not the first time the
right wing went out of their way to deflect, deny, and downplay. Same thing
happened with that mall. When did all of this start? When did it become necessary
for the right wing in the United States to cover up for the people who have that
flag. About the time all the academics came out and said hey that person, their
rhetoric, their characteristics, I mean that's that's pretty flashy stuff right
there but you didn't believe them because he had an R after his name, right?
That's when this started. That's when it became incumbent on the Republican Party
to look out for people with that flag and downplay anything that they did.
It wasn't always like that. Who remembers when George Bush Sr. left the NRA and why?
He was a lifetime member, and he distanced himself from the NRA over something.
Just words.
The NRA referred to federal agents as jackbooted thugs.
I think it was in a newsletter or something like that, and it might be specific to one
agency or something along those lines.
But the end result of it was Bush Sr. viewing it as them calling federal agents out.
And one of the people who was on his detail was in Oklahoma City and didn't make it.
So Bush Sr. took that statement personally.
And he put out a statement basically saying, you know, I'm leaving the NRA even though
I'm a lifetime member, you know, it casts doubt on the honor of, you know, this agent.
He was not a Nazi.
There was a time when a politician would distance themselves from one of the most powerful political
groups because they wrongly used that and they cast that on somebody who didn't deserve
it and he withdrew over it.
He knew that fascia were bad.
The NRA did too.
That's why they used that term, to cast whoever their target was in a negative light.
And they knew that that imagery would make people think they were evil.
That wasn't that long ago, but that's how far the Republican Party has gone.
major rifts over that kind of accusation, just hinting at it, using the same kind of footwear
was enough.
But now, Republicans defend somebody who literally had one of their flags or tattoos, now it's
all made up, couldn't be true.
Yeah, because, you know, people who sport that symbol, they're known for being like
upstanding citizens and not doing anything bad, right?
Doesn't even make sense.
You don't have to manufacture an incident when you're talking about this group.
You just have to wait.
It's gonna happen.
If you have found yourself suddenly repeating talking points or believing in vast conspiracies
that are super similar to the people who flew that flag in Germany, you might want to examine
why and how it happened, when it happened, whether or not you would say these things,
these things or do these things. Before you were following the guy who all the
academics said, hey that guy's pretty flashy. It's probably worth examining
because remember the most dangerous one of them, it's not any of the names you
know was the good German, right? Was the one who didn't really believe it, but
echoed the talking points to fit in, because they were the ones that created
an environment in which that kind of ideology could grow.
The right wing in the United States has changed, and you can say that the left wing has changed
too, that they've moved to more and more progressive positions.
Yeah, I mean they have, they're progressives, that's what they do, they progress, right?
But the right wing has changed, too.
They've become more and more authoritarian.
In fact, in the entire story, there's only one group of people that hasn't changed, the
people who fly that flag.
And you're defending them.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}