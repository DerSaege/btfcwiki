---
title: Let's talk about a different Republican Senator weighing in....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bf8mSrOK3z0) |
| Published | 2023/05/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Senator Cassidy openly expressing concerns about Trump's ability to win a general election without winning swing states.
- Cassidy, who has never been a fan of Trump, making a public statement about Trump's potential challenges in 2024.
- Mention of Senator Thin's past statement criticizing Trump for undermining faith in the election system and disrupting the peaceful transfer of power.
- Speculation about potential organized resistance against Trump and Trumpism within the Republican Party.
- Senator Thune's odd behavior and similar views to Cassidy regarding Trump's conduct.
- Thune's reasoning for voting to acquit Trump based on his belief that impeachment is primarily to remove the president, and since Trump was already gone, he voted to acquit.

### Quotes

- "Can't win a primary without Trump. Can't win a general with him."
- "What former President Trump did to undermine faith in our election system and disrupt the peaceful transfer of power is inexcusable."
- "Interesting developments and I feel like there might be more coming."

### Oneliner

Republican Senator Cassidy and Senator Thune express concerns and potential resistance towards Trump and Trumpism within the Republican Party, citing Trump's challenges in a general election and his conduct post-acquittal.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed on the evolving dynamics within the Republican Party (implied).

### Whats missing in summary

Insights on the potential implications of internal resistance against Trump and Trumpism within the Republican Party. 

### Tags

#RepublicanParty #Trump #ElectionSystem #Resistance #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about a different Republican Senator having
something to say about the Republican presidential primary.
And this one is interesting as well.
So this comes from Louisiana Senator Cassidy.
Now it's worth noting, Cassidy has never been a fan of
Trumps. He was one of the few Republicans that voted against him during the
impeachments. This is what he had to say. We saw in all the swing states, almost
all, Georgia, Pennsylvania, Nevada, and Arizona, the candidates for Senate that
Trump endorsed all lost. If passed his prologue, that means President Trump is
going to have a hard time in the swing states, which means he cannot win a general election.
Can't win a primary without Trump. Can't win a general with him. And it certainly appears,
the way it's shaping up, it looks like in 2024, that saying applies to Trump himself,
assuming he gets the nomination. So this in and of itself, it's interesting to see
people in the Republican Party start to openly admit this. That being said, it's
Cassidy. Not a huge surprise that he doesn't like Trump, but that was
public. That wasn't a private statement. That was a public statement. There is
something that I find interesting given something else that's happened recently.
I'm going to read a statement from a while back. The impeachment trial is over
and former President Trump has been acquitted. My vote to acquit should not
be viewed as exoneration for his conduct on January 6, 2021, or in the days and weeks leading up to
it. What former President Trump did to undermine faith in our election system and disrupt the
peaceful transfer of power is inexcusable. That's not from Cassidy. That's from Senator Thin,
a while back. It is very possible that Trump, and perhaps even Trumpism, all of
the many Trumps, the Trump-lites, are going to face a little bit more
organized resistance from inside the Republican Party. We don't know that
But Thune doing what he's doing, which is odd, and this statement occurring right
around the same time, them having similar views, even though Thune voted to
acquit, his statement was that he did it because he believed the Constitution
says the primary reason for impeachment is to remove the president, and Trump was
already gone. He said his conduct was inexcusable. Interesting developments and
I feel like there might be more coming. Anyway, it's just a thought. Y'all have a
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}