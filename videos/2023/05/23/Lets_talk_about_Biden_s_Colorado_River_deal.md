---
title: Let's talk about Biden's Colorado River deal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nsNlOQKr6Bg) |
| Published | 2023/05/23 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Colorado River overuse necessitated a deal due to decreasing water levels.
- Initially, states were reluctant to make cuts, leading to federal intervention.
- Biden administration presented a plan, prompting states to negotiate amongst themselves.
- Arizona, California, and Nevada agreed to significant cuts, conserving three million acre feet by 2026.
- Importance of states handling the issue locally to drive climate change awareness and action.
- Despite the temporary relief, deeper cuts are deemed necessary for long-term sustainability.
- Farmers facing water cuts will receive compensation through the Inflation Reduction Act.
- Emphasizes the impact of political decisions on individual livelihoods and traditional practices.
- Urges people to recognize the role of legislation in supporting farmers during environmental transitions.
- Encourages voters to prioritize environmental issues in local and state elections.

### Quotes

- "All politics is local, right?"
- "Your tradition will cost you your farm."
- "They don't care about you. They never did."
- "Make the environment a campaign issue."
- "It's just a thought."

### Oneliner

Beau explains the Colorado River deal, stressing state involvement for climate action and farmer support amidst political challenges.

### Audience

Farmers, environmentalists, voters

### On-the-ground actions from transcript

- Contact local representatives to prioritize environmental issues in legislation (implied)
- Support legislation that aids farmers in environmental transitions (implied)
- Make environmental concerns a focal point in local and state elections (implied)

### Whats missing in summary

Further insights on the intricacies of water management policies and political influences.

### Tags

#ColoradoRiver #WaterManagement #ClimateChange #StatePolitics #EnvironmentalAction


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
the Colorado water deal, the river out there.
If you are a farmer that gets water from the Colorado River,
watch the whole video,
because there's something really important
for you at the end.
Okay, so this is something we have been talking about
on the channel for a while, but a quick recap.
The Colorado River is being overused, a lot.
especially given the fact that there's less and less water in it.
The states needed to work out a deal,
but nobody wanted to take the cuts.
So they were bickering back and forth. The feds
have the authority to step in and just be like, yeah, here's the deal.
But that's not best for a whole bunch of reasons.
And we'll go over some of them here in a little bit.
In this case, what finally happened was a Biden brokered deal, but it's not the
fed stepping in, my understanding is that the Biden administration kind of
pulled the laptop trick, they were like, Hey, here, take a look at the federal
plan, this is how we would handle it, you know, something we have full
authority and capability to do.
All of a sudden the States got super interested in working it out on their
own. And they did. Arizona, California, and Nevada are going to take pretty big
cuts. It will conserve three million acre feet by 2026. For people outside of the
United States, an acre foot is an acre covered one foot deep with water, you
know, because we will use anything other than the metric system. An acre foot is
1,233 meters cubed more or less. Okay, so the deal has been made. So it is temporarily
safeguarding the drinking water of about 40 million people. So the big question is why
Why is it important for the states to have done this on their own rather than the feds?
Because now it's state politics.
All politics is local, right?
If you want somebody to care about something, it has to impact them.
If you want climate change to be discussed at the local and state level, they've got
to be involved.
because of this deal and the tone that is being set of it being the state's
responsibility to work this stuff out, climate change will become a state and
local campaign issue. Now for people who live in you know very blue states, it
already is for you, right? Yeah, it's not, it's not anywhere else and it
should be. So this is a big step in that direction. What's the bad news? This isn't
the best possible outcome. The 3 million acre feet, it's good and it's going to
provide relief, but it's not a solution. Most people who have really looked into
this say that the cuts need to be much deeper. This drought has been going on a
long time and there's not really a there's not really a whole lot of signs
of it slowing down. Okay so for the farmers you got to take water cuts right
water cuts well that's it you're gonna lose your farm right? No you're not you
are not. The federal government in the process of brokering this has agreed to
make a lot of payments to compensate people. I want to point out that the
money is coming from the Inflation Reduction Act. If you're a farmer you are
probably in a very red area. Understand that budget that they passed, budget in
in quotes,
you wouldn't be getting this money.
You would lose your farm.
It would be gone
because it wouldn't exist. This money would not exist.
The money that is going to float you,
pardon the pun,
only exists
because Republicans haven't gotten their way yet.
I understand
tradition
and I understand how important it is to a whole lot of people.
your tradition will cost you your farm.
They don't care about you.
They never did.
The legislation that is coming up with the money that is going to help you is legislation
that is being targeted by the Republican Party.
All of that environmental money that they're spending, this is the type of stuff it pays
for. It pays for you to be able to transition. It pays for you to be able
to keep your farm. It doesn't go to the liberal elites. I think it's really
important for people to understand that and maybe next time you're voting at the
local and state level, make the environment a campaign issue. Anyway, it's
It's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}