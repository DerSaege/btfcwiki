---
date: 2023-08-06 06:07:42.841000+00:00
dateCreated: 2023-05-30 15:08:00.179000+00:00
description: null
editor: markdown
published: true
tags: null
title: Let's talk about things over Moscow....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NCev03wbwQY) |
| Published | 2023/05/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Drones were over Moscow, hitting targets, with Russia blaming Ukraine.
- Ukraine has not acknowledged any involvement.
- Possibility of a non-state actor claiming responsibility.
- Russia likely to implicate Ukraine and its military.
- Concerns over drones penetrating Russian air defenses or Ukrainian special operations within Russia.
- Non-state actors operating in the conflict, not just a two-party conflict.
- Social media reactions showcasing various perspectives.
- Non-state actors typically take general direction from sponsors, not direct orders.
- Increased drone attacks may provoke responses from both the Russian government and citizens.
- Potential motivation from ongoing Russian bombardment of Ukraine's capital.
- Shift towards low-intensity operations and unconventional warfare.
- Strategic implications for future warfare due to the blending of state and non-state actors.
- Anticipated headlines on the situation by morning.

### Quotes

- "Drones were over Moscow, hitting targets, with Russia blaming Ukraine."
- "Non-state actors typically take general direction from sponsors, not direct orders."
- "Increased drone attacks may provoke responses from both the Russian government and citizens."

### Oneliner

Drones hit Moscow, Russia blames Ukraine, potential non-state actor involvement, shifting warfare dynamics with strategic implications.

### Audience

Global citizens, policymakers.

### On-the-ground actions from transcript

- Monitor news updates for developments on the situation (implied).
- Stay informed about the evolving conflict dynamics (implied).

### Whats missing in summary

Detailed analysis of potential repercussions and diplomatic efforts.

### Tags

#Russia #Ukraine #Drones #Conflict #Warfare


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Russia, and Ukraine, and Moscow, and the likely pointing
of fingers that is about to occur.
If you missed it, for most viewers this occurred while you were asleep, a not small number
of drones were over Moscow and some of them hit targets in Moscow.
Russia immediately blamed this on Ukraine.
That time of filming, Ukraine has not acknowledged any part in it.
My guess is that they are going to deny responsibility and that it is kind of likely that a non-state
actor claim responsibility for it.
Russia will of course try to implicate Ukraine and the Ukrainian military.
I don't know that that's actually the best move for Russia to be honest because if that's
the case, that means those drones, dozens of drones penetrated Russian air defenses to
To a pretty amazing degree, that or Ukrainian special operations are operating within Russia
and basically going unchallenged.
Neither one of those would, you know, fill me with like a warm and fuzzy feeling if I
was a Russian citizen.
I think that the most likely outcome is going to be a non-state actor claiming credit.
When you look at social media right now, people obviously have their various takes.
Most of them are kind of forgetting that this is no longer a two-party conflict.
You have non-state actors that are operating.
Now, you may want to make the case that they're connected to the Ukrainian military or Ukrainian
intelligence or whatever, and you're free to do that.
But generally, non-state actors, they at most take general direction from a sponsor.
They don't actually follow orders.
But the sheer number that hit Moscow, that is probably going to elicit a response of
some kind, not just from the Russian government, but also from the people.
It's worth noting that I want to say this is the third, I think, night of Russia bombarding
Ukraine's capital. So there might be a motivation there that may have something to do with this.
This turning into a drone war and this turning into a conflict that has low intensity operations
inside Russia is probably going to further alter the way we look at what is considered conventional
warfare. One thing that has been going on this entire time is people who study strategy and
doctrine, they've been paying attention. There's going to be a lot of shifts, not just on the
the battlefield there, but across the face of warfare. Stuff like what we're
seeing here with this hybrid conventional unconventional conflict is
something that may become the norm. The blending of state and non-state actors,
things that move in a transnational fashion, and a whole lot more activity on
the information space, but I would imagine that this is going to be all over the headlines
by morning.
So that's what we know at time of filming, dozens of drones were over Moscow.
Some were intercepted by air defenses at Moscow.
they arrived there right now I don't think anybody knows anyway it's just a
thought. Y'all have a good day.
## Beau's Shirt 
Black shirt with red block text Breaking News with a globe behind the text.
## Easter Eggs on Shelf
Drone at curious george's feet