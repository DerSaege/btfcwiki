---
title: Let's talk about juries in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=NP603Pd3Tyw) |
| Published | 2023/05/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the American legal system and the issues surrounding jury duty.
- Mentions how people try to get out of jury duty, leading to a less diverse and critical jury pool.
- Notes that many on the jury may have poor critical thinking skills and tend to defer to law enforcement.
- Describes how this dynamic can lead to wrongful convictions due to lack of skeptical jurors.
- Points out that those most interested in fixing the criminal justice system often avoid jury duty.
- Criticizes the standards of evidence in the American legal system, which differs from movie portrayals.
- Suggests being more skeptical of police press releases instead of using excuses to avoid jury duty.

### Quotes

- "There's a number of issues with the culture surrounding our jury system and jury duty."
- "Leads to a whole lot of people doing a whole bunch of time that maybe they shouldn't have."
- "The standards of evidence in this country, it doesn't quite measure up to the way it gets portrayed in movies and on TV."

### Oneliner

Beau explains flaws in the American legal system's jury duty culture, leading to wrongful convictions and lack of critical thinking jurors.

### Audience

American citizens

### On-the-ground actions from transcript

- Question jury duty excuses (implied)
- Advocate for diverse and critical jury pools (implied)
- Educate others about the importance of jury duty (implied)

### Whats missing in summary

Importance of educating oneself on the jury duty process and advocating for fair representation in juries.

### Tags

#JuryDuty #AmericanLegalSystem #WrongfulConvictions #CriminalJustice #PolicePressRelease


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about jury duty
in the American legal system and how something happens
because I got a message from Germany
and it's one of those things that is just
apparently uniquely American that it needs to be explained.
So the person was like, hey, you know,
I've started following more and more American news
and I've started seeing more and more stories
about people who were convicted,
went to prison, and then years later
are exonerated by some piece of physical evidence.
How did this person end up in prison?
Don't you have a jury system?
We do.
We do have a jury system.
There's a number of issues
with the culture surrounding our jury system and jury duty in the way people look at it.
For a long time, it was a running gag to talk about how you would get out of jury duty,
on sitcoms, on stand-up comedians, specials, everything.
So people started trying to do it more and more.
Which leaves you with a whole bunch of people on the jury that weren't quick-witted enough
to come up with a method of getting out of it.
You also have an issue where those people who are most skeptical of law enforcement,
those people who would listen to the cop's testimony and be like, yeah, I don't believe
this person, they have a bad view of the criminal justice system. Therefore, they
don't want to participate in it and they try to get out of it or when they're
being asked questions they say something that gets them disqualified. By the time
this is all said and done, the bulk of people who end up on a jury are people
who may not have the best critical thinking skills and people who defer to law enforcement.
People who basically would trust the cops hunch and this leads to a lot of wrongful
convictions.
It's not everybody on the jury, but I would say most probably fall into that, which means
the two or three that are left, they have nine people in the room, or however many people in
the room, depending on the state, I guess, saying guilty, and eventually they get worn down. And
those people who are most interested in correcting the criminal justice system
since they don't want to participate in it, they're not there. They're not in the room.
Leads to a whole lot of people doing a whole bunch of time that maybe they shouldn't have.
The standards of evidence in this country, it doesn't quite measure up to the way it gets portrayed in movies and on TV
a lot.  So that's where the system arises.
So if you are one of those people who is skeptical of, you know, a police press release, maybe next time don't, you
know, talk about your grandma being sick.  Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}