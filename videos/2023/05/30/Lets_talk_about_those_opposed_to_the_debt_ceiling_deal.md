---
title: Let's talk about those opposed to the debt ceiling deal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=cnVKUIHq84o) |
| Published | 2023/05/30 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the time constraints regarding voting on legislation, specifically related to the debt ceiling.
- Points out that there is no time for renegotiation or delays in the process.
- Emphasizes the importance of understanding the limited timeline for decision-making.
- Warns against the consequences of voting against the bill and leading to default.
- Stresses the potential negative impact on the economy and the likelihood of not getting re-elected if the bill fails.
- Mentions the possibility of major companies withdrawing support if default occurs.
- Suggests that the alternatives, like sending up a clean debt ceiling, might not be acceptable due to political reasons.
- Concludes by stating that the situation is what it is, indicating a sense of finality and urgency.

### Quotes

- "There's no time for renegotiation."
- "Guess what? We default that day."
- "It's really that simple."
- "This is the deal. It is what it is."
- "Y'all have a good day."

### Oneliner

Beau explains the urgency of the debt ceiling timeline, warning of dire economic consequences if the bill fails, with no room for renegotiation, and potential political fallout for those who vote against it.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to urge them to vote responsibly on the debt ceiling bill (suggested)
- Stay informed about the developments and implications of the debt ceiling issue (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the time constraints surrounding the debt ceiling legislation and the potential ramifications of a failure to pass the bill.


## Transcript
Well, howdy there internet people, let's bow again.
So today we're going to talk about timelines
and the debt ceiling and the legislation
and those people who are opposed to it.
You know, there's a lot of people saying
that they're not happy with the bill
and they may vote against it and so on and so forth.
I don't know that they understand that they're late.
they're late, they're late for an important date. They don't have time to
vote against it. There's no time for renegotiation. Okay, so the House rules
say you need 72 hours to review legislation. The current bill, the one
that people are less than happy with, that's going to be voted on on Wednesday,
Hopefully. That's the earliest. Let's say people vote against it and it fails.
Renegotiation starts. Let's say it's a world of make-believe and magic where
everything gets hammered out in 10 minutes. The whole negotiation is
redone and everybody has something that they'll vote for in 10 minutes. 72 hours
starts. It gets, you know, turned into legislation and it gets distributed to
people, okay, on Thursday. 72 hours starts. That means the earliest voting day for
the House is Monday. Guess what? We default that day. Still has to go through
the Senate. The time for negotiation is over. This is the deal. Either they're
going to vote for it or they are going to actively vote to default even if they
were to do away with the rules okay and say okay well we don't need 72 hours to
to review the legislation we'll just vote on it now that's fine but it still
has to go to the Senate it's still there's a whole lot at play here and they
don't have time. The people who are saying that they are going to vote
against this, you better hope that you have your vote count math right because
if you vote against this and it fails and we default, I'm fairly certain you
won't get re-elected. It's really that simple. So if you're
to vote against it for some kind of show for your base to show that you're a true hardliner
or whatever, I mean that's fine, but you better be right on the money on your vote count and
you better hope that nobody hits the wrong button or changes their mind at the last minute.
Because if this fails, if this doesn't go through, the US is in for a whole lot of economic
turmoil. And I don't think that the American people will be very forgiving
when it comes to to casting their votes for the people who voluntarily decided
to cause a default when there's a deal on the table. Seems super unlikely that
there's going to be a whole lot of campaign contributions coming your way
from, you know, the major companies who are all going to take a massive hit. In
fact, they might donate to your opposition. It just seems very unlikely
that this fails because if it does, there's a whole lot of problems. The
other alternatives include withdrawing it and sending up a clean debt
ceiling, which I don't think Republicans are going to go for because that's going
to be even more upsetting to their base, to the people they promised that
they would fight forever for and all of that stuff. And keep in mind, that
timeline that I gave, that includes getting the negotiation and turning it
into legislation done in a night. Doesn't really seem likely. This is the deal. It
It is what it is.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}