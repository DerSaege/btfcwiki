---
title: Let's talk about the value of reaching out to the other side....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=skgaaSAD9nc) |
| Published | 2023/05/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the skepticism towards reaching out to conservatives due to their perceived unwavering support for MAGA and conservative beliefs.
- He points out that trying to reach people is a fundamental aspect of effecting change and not doing so means accepting the status quo.
- Beau presents data showing that a significant portion of the population identifies as independents, hinting at a conservative lean among them.
- He notes that many conservatives may identify as independents rather than Republicans or MAGA supporters, making them a potential group for outreach.
- Beau draws parallels between how rural people are overlooked by Democrats despite sharing left-leaning economic principles and suggests they can be reached through engagement.
- By looking at voter registration numbers, Beau shows that there is a substantial number of independents who may lean conservative and are open to being influenced.
- Beau clarifies that while his beliefs mostly resonate with the Democratic Party and differ from the MAGA version of Republicans, he does not identify as a Democrat.
- He stresses the importance of reaching out to people to shift societal perspectives, stating that changing minds is more impactful than changing laws.

### Quotes

- "Trying to reach people is a fundamental aspect of effecting change."
- "To change society, you don't have to change the law, you have to change the way people think."
- "I am not a Democrat."

### Oneliner

Beau addresses skepticism towards reaching conservatives, advocating for outreach to independents with conservative leanings to shift societal perspectives.

### Audience

Community members, activists, voters

### On-the-ground actions from transcript

- Reach out to independents with conservative leanings for constructive dialogues and engagement (suggested)
- Engage with individuals holding differing political views to foster understanding and influence societal change (implied)

### Whats missing in summary

Beau's genuine and reasoned approach to engaging with conservatives and independents to broaden perspectives and effect societal change.

### Tags

#PoliticalOutreach #Conservatives #Independents #SocietalChange #CommunityEngagement


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about Republicans
and Democrats and independence and outreach.
We're gonna do this because I get a lot of questions.
Well, sometimes they're questions.
Sometimes they're comments, sometimes they're commands.
But the general idea is,
why are you wasting so much time
trying to reach out to conservatives. MAGA will never change. You can't be
reasoned out of a position that you weren't reasoned into, so on and so forth.
And I get the sentiment behind them. I really do. Believe me, I understand where
you're coming from. But there's a couple of things to note about that. First and
foremost, trying to reach people, that's kind of part of the reason the
exists. The second part is that if you aren't trying to reach people, you are by
default accepting the status quo. I don't think anybody watching this channel
wants the status quo. And then there's the other thing, and it's the big thing.
I understand the sentiment because everything seems so polarized. I get it
on a gut emotional level. Where I don't get it is all data. Okay, so in 2020, 31%
of people identified as members of the Democratic Party. They viewed
themselves as Democrats. 25% viewed themselves as Republicans. 41% viewed
themselves as independents. Judging by the outcome of elections, what does that
mean? It means that independents probably lean conservative more often, right?
Because if you just split it down the middle, the Democratic Party would win
pretty much every national election. If you took that 41, divided it amongst the two parties,
the Democratic Party would always come out on top. But that's not what happens. My guess is it's
because there are more conservatives who consider themselves independents. They lean more conservatively
at this point in time. This obviously changes throughout history and I imagine
it's about to change big time in the future and it will probably lean the
other way. But right now over the last few years this is what you have. That's a
lot of people that are leaning conservative but don't consider
themselves Republicans. Certainly don't consider themselves MAGA. Somebody's got
to talk to them. It's like it's like the way the Democratic Party views a lot of
rural people. Said it over and over again. Rural people live their lives by left
economic principles pretty much every day but nobody comes and talks to them
because they are viewed and yeah I will admit that the majority have very
socially conservative positions but philosophically most times even there
they kind of lean left. I think that they can be reached and then when you get
away from the way people identify and you look at the way they register. In 2021, 48 million Democrats,
36 million Republicans, 35 million independents, and then there's two or three million that are
registered with a third party. Again, by the way people vote, by the vote totals, it kind of leans
to the idea that there are a bunch of independents, people who do not want to consider themselves
republican, who lean conservative, who are up for grabs.
So I do spend a lot of time trying to reach those people, I think that's a big part of
the battle.
And then there's the big thing that everybody needs to remember, I am not a Democrat.
Yeah, for me most of my beliefs align more with the Democratic Party than the Republican
Party and certainly more than with the MAGA, FASH version of it that exists today.
But I'm not a Democrat.
I think that's important to remember as well.
If you want societal change, you have to reach people, you have to shift thought.
And there's a whole lot of people that are up for grabs that aren't necessarily being
let around by party officials.
out to them is important. To change society, you don't have to change the law,
you have to change the way people think. And that's a big part of why I do this.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}