---
title: Let's talk about Trump being told to hush in the hush money case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=_uJJTSrzzy0) |
| Published | 2023/05/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The district attorney's office sought to prohibit Trump from speaking about any evidence obtained during the discovery process to prevent harassment.
- The judge upheld the prohibition, but Trump can still talk about the evidence his side presents.
- Trump may end up in trouble by using evidence obtained via discovery to make statements.
- Trump is trying to have the case removed from New York State Court and sent to federal court, a unique approach.
- There may be timeline errors regarding when events occurred in relation to Trump becoming president.
- The trial is set to start during the 2024 primaries, creating bad timing for Trump.
- Trump can't agree to any events during this time or delay the trial.
- Trump might attempt to delay the trial until after the primary season and election if he makes it through the primary.
- The hush money case is expected to be litigated over the next year, with slow progress and procedural developments.
- We may not see significant results from the case until primary season is in full swing.

### Quotes

- "Trump may end up in trouble by using evidence obtained via discovery to make statements."
- "It is definitely a novel approach, and I think there may be some timeline errors as far as when Trump became president and when some of this happened."
- "The trial is set to start during the 2024 primaries, which is really bad timing for Trump."

### Oneliner

The district attorney prohibits Trump from discussing evidence, foreseeing trouble, while Trump navigates legal maneuvers for the 2024 trial.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Stay informed about the legal developments surrounding Trump's cases (implied)
- Follow the progress of Trump's legal battles and how they unfold over time (implied)

### Whats missing in summary

Insight into the potential consequences of these legal developments and their implications on Trump's future.

### Tags

#Trump #LegalIssues #HushMoneyCase #2024Election #DiscoveryProcess


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump, New York,
and a couple of different developments
in the case up there.
So first, the district attorney's office up there
sought to have Trump prohibited from speaking
about any evidence that was obtained during the discovery process, particularly any information
that he might put on social media that would lead people to be harassed.
Trump has a long history of making inflammatory statements towards and about various people
involved in any of his legal proceedings.
people often come under harassment and threats.
The judge decided to uphold that and prohibit Trump from talking about anything that was
obtained during the discovery process like that.
However, Trump can still talk about the evidence that his side is presenting.
My guess is that Trump will probably end up in trouble over this.
He might obtain something via discovery, see it from his lawyers, and then use a different
piece of evidence that they have to make statements.
And I have a feeling that this will end up back in court.
Now, in the same case, and to be clear, we are talking about the, I guess it's mainly
being called the hush money case, Trump is also attempting to have the case removed from
New York State Court and sent to federal court, which is unique.
I'm not sure that the argument that was being put forward, at least not what was explained
in the quotes that have appeared in the coverage, I don't know that that's going to go anywhere.
It is definitely a novel approach, and I think there may be some timeline errors as far as
when Trump became president and when some of this happened and when people were hired,
but we'll have to wait and see.
Those are the two main developments.
Now, the interesting thing is that these things will have to be litigated over the next year
because the trial is set to start, well, during the 2024 primaries is what it looks like,
which is really bad timing for Trump. Once the dates are set, it's worth remembering that
that Trump can't agree to any event during these periods and can't delay the trial.
So I would expect to hear more about that.
My guess would be that Trump is going to try to delay the entire trial until after the
primary season and then after the election, assuming he makes it through
the primary. So the the hush money case as it has become known, my guess is that
we're not going to get a lot out of it for like a year, that there's not going
to be much that occurs other than procedural stuff.
You might get some occasional news breaking when it comes to Trump saying something he
shouldn't.
But other than that, this case seems to be one that's going to move very, very slowly.
And we won't really see any results from it, or really even start to see any results from
until primary season. It is in full swing. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}