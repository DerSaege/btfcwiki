---
title: Let's talk about Biden just ordering the debt paid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=xqxB-3YKUqk) |
| Published | 2023/05/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the constitutionality of default and the debt ceiling in the United States.
- Cites Article 1, Section 8, and the 14th Amendment, Section 4 of the U.S. Constitution to argue against the legality of a debt ceiling.
- Emphasizes that all debts discussed were authorized by law, making defaulting unconstitutional.
- Mentions the importance of Congress's power to lay taxes and pay debts in relation to the debt ceiling issue.
- Refers to Federalist Papers Number 30 by Hamilton, suggesting that willingly defaulting is not an option.
- Raises the question of why President Biden doesn't just pay the debts, explaining that it could lead to legal challenges and doubts about payment.
- Stresses that the U.S. economic system relies heavily on faith, which could be shaken by legal challenges to debt payments.
- Expresses concern over the potential partisan decisions of the Supreme Court and the negative impacts on the economy.
- Suggests that Biden should have sought a finding from the Supreme Court earlier to avoid economic instability.
- Concludes with the belief that willingly defaulting is not permissible under the Constitution and criticizes those who support defaulting.

### Quotes

- "You can't willingly default."
- "The logic that is presented by the people that are putting this out there. Yeah, it's it's solid."
- "It's so close to the time when it would matter to an actual default that it shakes the economic system."
- "U.S. economic system in many ways runs on faith."
- "To me and I think to most people who have actually read the Constitution, you can't willingly default."

### Oneliner

Beau explains why willingly defaulting on debts is unconstitutional in the U.S., citing key constitutional clauses and potential economic repercussions.

### Audience

Policy analysts, lawmakers, activists

### On-the-ground actions from transcript

- Seek legal guidance on constitutional aspects of default (implied)
- Advocate for transparency and adherence to constitutional principles in debt payment (implied)

### Whats missing in summary

Analysis of potential solutions or alternative approaches to address the debt ceiling issue.

### Tags

#Constitutionality #DebtCeiling #USConstitution #Biden #EconomicImpact


## Transcript
Well, howdy there Internet people, it's Bo again.
So today we are going to talk about the constitutionality
of default, the debt ceiling, whether or not
it's even something that's allowed
by the framework of the United States.
There have been some big names
that have brought this up recently,
and we're going to talk about the suggestion
and why it isn't being used the way people are suggesting.
First, let's get into the constitutionality of it.
Short version, when you look at Article 1, Section 8
of the Constitution of the United States,
and then you look at the 14th Amendment, Section 4,
which says, the validity of the public debt
of the United States authorized by law,
including debts incurred for payments of pensions and bounties for services in suppressing
insurrection or rebellion shall not be questioned." The important part to note here is it says
including debts incurred for payment, meaning not just the debts incurred that way. So if you take
out that clause, which you end up with plain reading, the validity of the public debt of
the United States, authorized by law, shall not be questioned. Period. Full stop. That's it.
So, all of the debt that they're talking about, it was all authorized by law.
So, by the 14th Amendment and combining that with Congress's power to lay taxes and pay debts,
which is Article 1, Section 8, it shouldn't be even allowed to have a debt ceiling when it comes
to payment of debts. It's not something we should be able to actually have. That's especially true
if you take that and look at the Federalist Papers Number 30, where Hamilton talks about debt.
and these papers are something that are often used by the Supreme Court to frame the Constitution
and to really think about what it means.
There's a lot of information there that makes it pretty clear we can't willingly default.
It's something that is just, it's not something within question.
Okay, so with that in mind, why doesn't Biden just say, pay it?
Because then it goes to court.
Even though this is a plain reading, and I think most people would agree with this, it
hasn't gone to court.
If Biden's payments are challenged, it creates doubt that those debts are actually going
to be paid.
The U.S. economic system in many ways runs on faith.
When it's in court, people lose faith.
What if the Supreme Court decides to be partisan?
What if a court that has certainly gone against a whole lot of precedent decides to do the
same thing here?
It shakes the foundation of the economy.
Which is why I suggested Biden get a finding from the Supreme Court three months ago.
apparently did not happen, but you know it is what it is. To me and I think to
most people who have actually read the Constitution, you can't willingly default
and I know that's going to bother a whole lot of people who want to support
the Republican Party in this, but also you know really like pretending they're
a constitutionalist. The logic that is presented by the people that are
putting this out there. Yeah, it's it's solid. That's what the documents say. But
it's a little late in the game to try to do that now. It's so close to the time
when it would matter to an actual default that it would it would shake the
economic system and we would start seeing negative economic impacts like
now, which is what the Biden administration is actually trying to avoid.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}