---
title: Let's talk about Bryan Slaton, Texas, expulsion, and a report
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=MMKzqw26SZQ) |
| Published | 2023/05/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas State House committee is investigating Representative Brian Slayton.
- The report likely recommends his expulsion due to assisting young women under 21 with alcohol.
- Rumors suggest Slayton was encouraged to resign but did not.
- The committee found the young woman could not effectively consent to the alcohol consumption.
- Slayton allegedly tried to prevent his young staff from disclosing information.
- The House is expected to vote on his expulsion soon, a rare occurrence since 1927.
- The issue will likely dominate Texas headlines until resolved.

### Quotes

- "The report is apparently going to recommend his expulsion."
- "On top of all of that you know there's the alcohol thing, there's the abusive capacity, there's the official oppression."
- "It's worth noting that the last time somebody was expelled was 1927, almost a hundred years ago."

### Oneliner

Texas State House committee investigates Rep. Slayton, likely to recommend expulsion for providing alcohol to young women.

### Audience

Texans, lawmakers

### On-the-ground actions from transcript

- Contact local representatives or organizations to voice opinions on the expulsion (implied)

### Whats missing in summary

Full details on the specific allegations and evidence presented in the report.

### Tags

#Texas #StateHouse #BrianSlayton #Investigation #Expulsion


## Transcript
Well, howdy there, you don't know people, let's bow again.
So today, we are going to talk about Texas and a report,
what is likely to come of that report
and a representative in Texas named Brian Slayton.
So, a committee in the Texas State House there
was looking into this representative.
If you're wondering who this person is, think of all the wild legislation that has come
out of Texas dealing with everything from shows to reproductive rights.
Odds are he had something to do with it.
The report is apparently going to recommend his expulsion.
Among other things in it says that he assisted in providing young women with alcohol, two
of which were under the age of 21.
He himself is 45.
One of them was 19 years old and according to the report drank quote a lot and felt quote
really dizzy.
She did not go on to talk more about what happened after that, but it does appear that
she utilized Plan B. And the committee, according to the reporting, I have not seen the full
16-page report from the committee yet, but according to the reporting from news outlets,
committee concluded that she quote could not effectively consent. On top of all of
that you know there's the alcohol thing, there's the abusive capacity, there's
the official oppression, which are kind of things that are mentioned in the
report. These are crimes in Texas. The rumor mill suggests that he was
actually encouraged when allegations first started surfacing. He was
encouraged to resign and he elected not to do so. The committee then went about
its business and started looking into it. There are reports that he attempted to
to, I don't want to use the G word, he attempted to convince some of the
younger people working for him to not disclose what they knew and to keep
everything to themselves. The expectation here is that the House will expel him.
Uh, the vote should occur relatively soon.
It's worth noting that the last time somebody was expelled was 1927, almost
a hundred years ago.
This is not something that happens very often.
Um, so this is something that will probably dominate headlines, at least in Texas
for the next few days, uh, until it is resolved.
My guess is they're going to try to get this report answered and they'll take the vote
on the resolution to expel him pretty quickly. I would be surprised if they put a whole lot of
effort into trying to delay this or cover for him. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}