---
title: Let's talk about races, rockets, and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vIagohTIomI) |
| Published | 2023/05/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the possibility of Ukraine shooting down a Russian hypersonic missile with a Patriot system.
- Clarifying misconceptions about the speed dynamics involved in such a scenario.
- Emphasizing that the Patriot system doesn't need to match the speed of the missile but accurately intercept its path.
- Suggesting that repeated use of hypersonic missiles by Russia might diminish their effectiveness.
- Pointing out that the hype around hypersonic missiles could be a tactic to increase defense spending.
- Noting that the reputation of the Patriot system has evolved since the first Gulf War.
- Expressing concerns about the implications of hypersonic missiles in nuclear warfare.
- Mentioning ongoing developments of countermeasures against hypersonic missiles.
- Summarizing that the Patriot intercepts by predicting the missile's path, not by matching its speed.

### Quotes

- "The Patriot doesn't have to go as fast as the missile. It just has to calculate where the missile is going to be and then put its missile there at the same time."
- "It's not a race. The Patriot is already where the missile is headed."
- "If it hasn't happened, it will. These things aren't wonder weapons."
- "The speed at which the warheads are delivered is the least of your worries because it means major powers are exchanging nukes."
- "Because it's not a race. It's that simple. The Patriot is already where the missile is headed."

### Oneliner

Beau explains how a Patriot system can intercept a Russian hypersonic missile by predicting its path, not matching its speed, debunking misconceptions and addressing concerns about defense and nuclear warfare.

### Audience

Defense Analysts

### On-the-ground actions from transcript

- Develop or support technologies for countering hypersonic missiles (implied)
- Stay informed about advancements in defense systems and technologies (implied)

### Whats missing in summary

Beau's detailed analysis and breakdown of the dynamics involved in intercepting a hypersonic missile with a Patriot system.

### Tags

#Defense #MilitaryTechnology #HypersonicMissiles #PatriotSystem #NuclearWarfare


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about something
that was said to have happened in Ukraine.
And we're going to talk about whether or not
it's actually possible for it to have occurred.
And we'll just kind of run through some of the skepticism
that has arisen.
I will point back to some earlier coverage of this topic. So if you don't
know what happened, Ukraine says that it shot down one of Russia's hypersonic
missiles with a Patriot system. It's an air defense system the United States
provided to Ukraine. Now I do want to point out that at time of filming I have
not personally reviewed the evidence to see if this actually occurred. However,
if it didn't, it will soon. But we're going to go through one question in
particular and just kind of explain where some of the confusion may lie. I
I can't wait to hear you explain how a patriot could shoot down a Russian hypersonic missile
when the missile is going so much faster.
This seems like an outright lie or really bad propaganda, but undoubtedly you'll just
repeat Ukrainian propaganda.
Okay.
It's at this moment that I realize why people are skeptical.
don't know how it works. I don't know what people are actually picturing, but the comparative speeds
made me realize they're not picturing what actually happens. It's not a race. This is what I want you
to picture. A Lamborghini, Ferrari, whatever car, doesn't matter. The ultimate when it comes to
speed for you. It is going down the road as fast as it possibly can, and then a Ford Escort
pulls out at the intersection and they collide. Does the Ford Escort have to be going as fast as
the sports car? No, of course not. That's silly. That's the comparison that's being made.
The Russian hypersonic missile is fired from a pretty great distance. The Patriot
is already near the target. The Patriot doesn't have to go as fast as the
missile. It just has to calculate where the missile is going to be and then put
its missile there at the same time. That's it. The Patriot has to do exactly
what it was designed to do. It's not a race. The speeds really don't
matter as much as people might believe, especially given this message. The more
Russia uses them, the less effective they are going to be. Every time one is fired
the United States is collecting data. That information is absolutely going to
be shared with Ukraine. Every time they're used they're less effective. I
have been of the opinion that the whole hypersonic missile thing is the
mineshaft gap. It's it's hyped up and it is a a method of getting people behind
a bunch of extra defense spending. I will put a whole bunch of videos down below. I
would also like to point out that when you are talking about the category of
hypersonic missile, the thing Russia's been using is like the worst of all of
them. It's not even a good one. It is completely possible that this occurred.
Now, again, I haven't seen the evidence, but I assure you that if it hasn't
happened, it will. It will. These things aren't wonder weapons. I think a lot of
this stems from the initial reputation of the Patriot from the first Gulf War. I
would point out that a lot of that reputation stems from things falling
apart that weren't part of the Patriot system and I would also point out that
it has been upgraded a lot since then. This is completely possible. When people
talk about this, most people are truly concerned with nukes. That's where
their apprehension really is because it reduces response time.
Me personally, I learned to love the hypersonic missile because realistically
if it is ever being used for that, the speed at which the warheads are
delivered is the least of your worries because it means major powers are
exchanging nukes. I know that there are some concerns about it being used against
ships, I would point out that there are already countermeasures being developed.
Some may have already been fielded and we not know it yet. So to answer the
question, how a Patriot could shoot down a Russian hypersonic missile when the
missile is going so much faster, because it's not a race. It's that simple. The
The Patriot is already where the missile is headed.
It just has to collide with it.
Anyway, it's just a thought.
y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}