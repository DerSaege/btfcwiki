---
title: The roads to tools for relief and aid
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Lu7iWMi_tck) |
| Published | 2023/05/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Many people who want to help with relief efforts lack the necessary tools, as they can be expensive and inaccessible for those not making a living with them.
- There is a lot of classism surrounding tools, with certain brands being deemed superior and expensive.
- Hart Tools gained attention in 2021 for being inexpensive and made by a major manufacturer, sparking rumors about their quality.
- Beau personally tested and used Hart Tools, finding them to be worth trying out for disaster relief work.
- The Hart chainsaw is described as mid-grade, adequate for cutting and performing its functions, but the batteries drain quickly and it is very light.
- The reciprocating saw from Hart Tools is praised for being light and durable, even surviving being submerged once.
- Beau expresses a dislike for the circular saw from Hart Tools due to its small size and limitations in cutting capacity.
- The impact tool that came as part of a set is mentioned as not being extensively used by Beau, but it performed fine in the few instances it was used.
- While the Hart drill is not considered commercial grade, it is still functional and not comparable to cheaper brands that may not last.
- Overall, Beau finds that Hart Tools are a good value for the cost, especially for those looking for tools to have on hand for relief efforts or for hobbyists.

### Quotes

- "Hart Tools are definitely worth trying out."
- "If you are looking to get the tools to help out, it's a brand that'll work."
- "They are not commercial grade, but they will work."
- "It's a good value. Just manage your expectations."
- "Get this stuff, a different circular saw, get a bunch of batteries, and then get a solar generator."

### Oneliner

Beau shares his experience with Hart Tools, finding them worth trying out for relief work due to their affordability and functionality, despite not being commercial grade.

### Audience

DIY Enthusiasts, Disaster Relief Volunteers

### On-the-ground actions from transcript

- Purchase Hart Tools for disaster relief efforts (suggested)
- Get a solar generator as part of your relief kit (suggested)
- Manage your expectations when using the tools (implied)

### Whats missing in summary

Beau's detailed insights and hands-on experience with using Hart Tools for disaster relief efforts can best be understood by watching the full transcript.

### Tags

#Tools #DisasterRelief #HartTools #Affordable #Functionality #CommunityAid


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the roads to tools for
relief, assistance, aid, whatever.
So for those people who want to help, a lot of times they
don't have the tools, the literal tools, to do it.
And the price point to get the tools you need to do like
hurricane relief or something like that, it's not cheap.
And if you're not making your living with those tools,
you're probably not going to buy them.
Especially given the way people talk about tools and how you
can only use certain brands, there's a lot of tool
classism, for lack of a better term.
If you go to get a DeWalt drill, reciprocating saw,
and circular saw, you're out $600, like out the door.
Before you even get a chainsaw, which
is something you have to have.
So for those first getting started
and doing this type of stuff, there's
been a lot of questions about what's the cheapest you
can use to get started.
And then for people who do have a lot of expensive tools,
when you go down to do relief work,
you know what happens to them.
I have never gone to help after a hurricane
and brought back everything I went down there with.
Sometimes it just gets destroyed.
Sometimes somebody walks off with it.
Sometimes you leave it intentionally
because people need it.
So, people look for inexpensive items to fill those roles.
In 2021, a company called Hart, this is not a sponsored video, a company called Hart started
getting a lot of attention because they're really inexpensive and they're made by a major
manufacturer and there were a bunch of rumors that went out like right away during this
period.
absolute garbage, their commercial grade back and forth. About this time I was
getting a whole bunch of questions and there was a sale on Hart Tools right by
me. So I said the most I was going to spend was the cost of my chainsaw. I have
a Husqvarna Rancher and I went and I bought a bunch of them. I have been using
and abusing them for the last more than a year and I'll tell you what I learned
about them. Short version if you don't want to watch the whole video and you're
gonna experiment with them yourself yeah they're definitely worth trying out. Okay
so their chainsaw. The overall first caveat is I had never used a battery
chainsaw before. If you're gonna make the switch I'm going to tell you now
this doesn't do anything on this one. It doesn't stop anything. Overall it's a
mid-grade chainsaw. It is obviously not a rancher but it's not garbage. It's not
bad. The oil, get the Hart brand biodegradable oil or whatever that they
sell. We used what I'd normally used and it seemed to gum it up. So you use their
stuff. The bar on it is short. It's only 14 inches. An additional four inches
would make this a whole lot better.
Overall, yeah it's mid-grade. There's nothing wrong with it. It performs, it
It cuts, it does everything it's supposed to.
It is very, very light, alarmingly so, if you're used to a heavier chainsaw.
The batteries, they seem to drain pretty quickly.
So if you're going to go do a disaster relief, make sure you have a couple of them.
When we go down in the future and we take a couple extra because we know somebody's
going to need them, this is what we'll be bringing with extra batteries.
And it's helpful after hurricanes because you can't get gas.
Having something that you can charge is wonderful.
And as far as cost, which is a huge factor in this conversation, that costs less than
a bar and chain for mine.
definitely worth it. The other thing you absolutely have to have to do Hurricane
Relief is a reciprocating saw. Same thing, super light, unnervingly so. It
holds up. I use this a whole lot. I actually dropped it once and it got
submerged. Still works. I wouldn't count on that, but overall it's good.
Obviously you will always wear proper safety equipment. Gloves and eye
protection and all of that stuff. And you will always use a proper grip. I will
say due to the design, if you were ever holding it in a weird way, that little
notch right there. It's gonna drive you nuts. Make sure you have your gloves on.
Okay, the circular saw. I hate it. I hate it. It's absolutely horrible. There's
nothing wrong with it mechanically, but it's small. It's small. The blade is small.
If you are sitting there cutting boards all day to hold a tarp on a roof,
something like that, this isn't gonna cut it. I mean literally it will cut it,
but it's going to be annoying. This is the only thing I wouldn't buy again, to be
honest. Sometimes size matters. Okay, the impact. I don't know. It came as part of a
set. This isn't something I really use. The few times I used it, it did fine, but
this did not get it didn't get the wear and tear and abuse that the other tools
got so it seems fine but I don't know their drill this thing has has seen a
lot this is one of the things that people said was commercial grade it is
absolutely not commercial grade that being said it's not bad it's not like
it's, you know, black and decker or something that's gonna burn up. The bits
that come with it, just go ahead and throw them away. Don't set yourself up
for that though, you're in that annoyance. Go ahead and throw them away, they're
not worth anything. Let's see, they come with a little bag, the bags fall apart
and this is for all the tools, but you're not buying it for that reason.
The hand tools, the normal hand tools, this kind of stuff, the coating flakes off pretty
quickly and it rusts.
That being said, in full disclosure, I am notoriously hard on tools and I live in a
very humid part of the South, so some of that was going to happen anyway.
I got the giant socket set.
The big one that they offer comes in the black case.
The sockets are good.
Oddly enough, the bits in the socket set are fine.
But the ones that come with the drills are not.
The wrench itself has an issue.
If you, there we go.
Nope.
Now that I want it to do it, it's not going to.
When you're flipping it back and forth, sometimes it snags,
and it goes into just a free spin thing.
And it's not part of the design that's all the way over.
But that's the only real mechanical thing that's
wrong with any of this stuff.
I also got their staple gun and stuff like that.
It's fine.
Overall, this stuff is on par with Porter Cable.
It's not DeWalt.
It's nothing like that.
But it's a fraction of the price.
Buying all of this from a major company
is going to run, I don't know, $1,500, $2,000.
And all of this was like $450.
used extensively for more than a year in less than ideal conditions and to be honest for the
last three months or so I've been kind of trying to burn them up and not babying them in any way
shape or form. They're fine. If you are looking to get the tools to help out it's a brand that'll
work. Yeah some people are gonna probably joke about them being Walmart brand or
whatever, it's fine, it'll work. If you are looking for stuff to have on hand to
loan out, definitely the right stuff because it functions. You're not gonna
give a you know a Solzall to somebody and then leave and then three days later at
break. It will continue working. The battery drain on the chainsaw, that
doesn't happen with the other tools. They're two different battery systems as
well. That may have something to do with it, but the smaller batteries didn't seem
to suffer that same drain. I think that's pretty much it.
Oh, the hand tools come with a little bag that actually held up, but so if you're looking
for stuff to distribute, it's the right stuff because it doesn't break your bank.
If you're just getting started, if you're a hobbyist and you're just looking for some
tools to get started doing stuff, first time homeowner type of thing, yeah.
I mean, again, the only thing that I really, really
didn't like was the circular saw.
And maybe by now, they have a bigger one.
If they do, I would definitely get the larger one.
But that's it.
Yeah, there are no major issues with any of it.
And I can't think of anything that is comparable in cost
that is also comparable in function.
I mean, these things are like, let's say,
most of the individual pieces are like $30 to $50.
It's a good value.
Just manage your expectations.
They are not commercial grade, no matter
whatever your friend's at.
But they will work.
So there you go.
If you were putting your kit together,
I would get this stuff, a different circular saw,
get a bunch of batteries, and then get a solar generator.
And that should keep you up and running when the hurricanes
start coming.
Anyway, it's just a thought.
You all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}