---
title: Let's talk about 2 question about Texas from Germany....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=oMfdDRuOXN0) |
| Published | 2023/05/07 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the stereotypes about Texas and guns, Beau points out that not everyone in Texas carries a gun and explains why people didn't shoot back during a recent incident.
- Most people are not killers and not inclined to respond actively in a dangerous situation, even if they have firearms.
- Beau mentions the high level of support in Texas for certain gun control measures, such as raising the age to buy firearms and allowing courts to take firearms from those deemed a threat.
- Despite the public support for such measures, Beau criticizes politicians for not taking action and instead catering to a specific identity and party loyalty.
- He questions whether the disturbing footage of incidents like the one in Texas will actually lead to meaningful change or just desensitize people, drawing a parallel with driver's education videos.
- Beau ultimately leaves the decision of using such footage to the families involved, acknowledging the sensitivity and ethical considerations around its distribution.
- He expresses uncertainty about how to bring about a shift in the Republican Party's stance on gun control, particularly in states like Texas.
- Beau points out the irony that the current lack of reasonable gun regulations could eventually lead to stricter legislation, contrasting it with the potential positive impact of taking proactive measures.
- He suggests that if politicians made reasonable efforts to address gun violence, it could potentially reduce incidents and prevent extreme restrictions on firearms in the future.

### Quotes

- "Most people are not killers."
- "It's not a matter of shifting thought when it comes to the things that at least I think would really be effective."
- "Their intractability on that is probably going to lead to incredibly restrictive gun legislation."
- "Their own rhetoric, buying into it, becoming so immovable on the subject, is probably going to lead to firearm restrictions."
- "Y'all have a good day."

### Oneliner

Beau addresses stereotypes about Texas, explains why people didn't shoot back in a recent incident, criticizes politicians for failing to act on gun control despite public support, and warns of potential unintended consequences of sharing disturbing footage.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Contact local representatives to advocate for reasonable gun control measures (implied)
- Join community organizations working towards sensible gun regulations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges surrounding gun control in Texas and the potential consequences of political inaction.

### Tags

#Texas #GunControl #PoliticalAction #PublicOpinion #Ethics


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about what happened in Texas.
And I'm going to answer two questions that came in from
somebody who is from Germany.
And it deals with stereotypes and the way a lot of people
see themselves and the reality.
And then it deals with perceptions and sings something.
I'm from Germany.
I saw your news from Texas.
I'm so very sorry.
I have two questions.
We view Texas as the place where everybody carries a gun.
Is this wrong?
Why didn't somebody shoot back?
There were so many people around to stop it.
The second question, do you think the horrible footage will help make people more likely
to make their politicians act or at least shift thought as you like to say?
First question, okay, Texas is a place where everybody carries a gun, a whole lot of people,
a lot of people. Undoubtedly there were people in that crowd that had a firearm.
Why didn't they shoot back? Because contrary to a lot of people's self
perception, most people aren't killers. Most people are not killers. Even an even
smaller amount is the sort that would rush towards it and actively respond if
they thought they could get away. That's the reality of it. Most people
are not up to that. Do you think the horrible footage will make people
more likely to make their politicians act or at least shift thought. When it
comes to Texas, the people there in Texas are actually incredibly supportive of
action. A lot of thought has already shifted. I think it would surprise most
people to know that more than three-quarters, 76 percent, of people in
Texas support the idea of raising the age from 18 to 21 in Texas. When it comes
to the idea of allowing courts to make people surrender their firearms if they
are found to pose a risk to themselves or others? 72%. It isn't really a matter
of shifting thought when it comes to the things that at least I think would
really be effective. It's not a matter of that. It's a matter of politicians
catering to an echo chamber and creating that identity and making sure
that those who are within the Republican Party don't dare vote for the other
party. I mean let's let's be real 76 and 72 percent there aren't a whole lot of
things in the United States that have that kind of support. You don't get
polling numbers that high, but odds are that's not going to happen, not in Texas,
because the politicians are very entrenched. Their rhetoric is too
heavily linked to the idea that they need to cast themselves as somebody who
would respond. Those politicians, had they been there, well you're right they
absolutely would have acted. It's marketing. They market the tough guy
image so they can't they can't break away from it even when it is a
ridiculously popular opinion. I don't know. I don't know what it's going to
take to get to get a shift from the Republican Party particularly those in
super red states. When it comes to Texas I mean they've had they've
had a pretty rough year when it comes to stuff like this. So maybe, I don't know. As
far as the footage itself, I don't know. I mean when I talk about, when I talk
about footage, if it's bad I will tell y'all, maybe y'all don't watch this, you
I will put that advice out there because I don't think that's something that
everybody needs in their psyche. I think that it might desensitize people. I don't
know, I could be wrong. There's a debate about this when it comes to
driver's ed videos as well. There's reporting that suggests that driver's ed
videos that show, that show things in detail as far as accidents, that they
actually aren't more effective. In fact, they may be less effective than just
encouraging positive behavior. I don't know that things being gruesome actually
shifts thought. I know that in this case, because it was such a public place and
there were so many people, there is a lot of footage of this and I will tell you,
I would not suggest you look at it, but as far as using it to that end, I think
that that is a decision that is 100% up to the families involved. I would not, I
would not take it upon myself to use footage like that for memes or
advertising in an attempt to shift thought. I think that would definitely
need to be up to the people who are family members. But we don't, you know,
from Europe, from other places, it's got to be so weird. We don't have a plan.
We really don't. At this point it seems like the current plan is to let those
who are younger who are dealing with this now get to the point where they can
vote and vote in numbers large enough to override the the crowd that grew up
with a bunch of firearms and the thing is if that's the way it goes it will go
from everything's legal to everything being banned. The ultimate irony in all
of this is that the Republican politicians that are so steadfast in, you
we're not going to have any regulations whatsoever, their intractability on that
is probably going to lead to incredibly restrictive gun legislation, I would say
within my lifetime. Whereas if they actually took steps, reasonable steps, 18
to 21, you know, taking firearms from people deemed a threat after due
process. That's even involving the courts in it. I think if they did that it would
reduce a lot of these incidents which would ultimately probably lead to less
of a desire to just get rid of them altogether. That's the funny part. There
is something funny. Their own rhetoric, buying into it, becoming so immovable on
the subject, is probably going to lead to firearm restrictions that are far
beyond anything that would happen if they made even the slightest efforts to mitigate
this issue.
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}