---
title: Let's talk about a North Carolina veto and one vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=vdeaGjEklSY) |
| Published | 2023/05/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- North Carolina's governor vetoed a bill curbing reproductive rights, prompting Republicans to attempt an override.
- Republicans in North Carolina need every vote for the override; losing one vote could sway the decision.
- The bill in question is convoluted, with varying restrictions on reproductive rights at different stages of pregnancy.
- Only 35% of North Carolinians support the bill, indicating widespread opposition to additional restrictions.
- Majority in North Carolina either support expanding reproductive rights or maintaining the current status quo.
- Republicans are accused of prioritizing rule over representation, going against the will of the people.
- Despite low public support, Republicans plan to override the veto, disregarding constituents' desires.
- People in North Carolina are urged to convince just one Republican representative to prevent the override.
- Pressure from Republican Party leadership may influence representatives to vote against constituents' wishes.
- Civic engagement is encouraged to influence representatives and uphold the will of the people.

### Quotes

- "One vote. People in North Carolina need to convince one to break ranks, one to honor their campaign promise, one to do what polling suggests they should do."
- "Republicans deciding that they're going to rule rather than represent."
- "Don't actually listen to the people who put you in office. Don't enact their will."

### Oneliner

Republicans in North Carolina push to override governor's veto on a bill curbing reproductive rights despite low public support, urging civic engagement for change.

### Audience

North Carolinians

### On-the-ground actions from transcript

- Convince one Republican representative to prevent the override (implied)
- Engage in civic activities to influence representatives (implied)

### Whats missing in summary

The full transcript provides detailed insight into the political dynamics and public sentiment surrounding the vetoed bill in North Carolina.


## Transcript
Well, howdy there internet people, it's Bo again.
So today we are going to talk about North Carolina and the governor and a
veto and a possible override and how likely that is because the governor in
North Carolina made good on their promise, said that if that bill went forward,
it was going to get vetoed. They did it in kind of a public ceremony, which was odd.
But Republicans in North Carolina, they can override it. They need every vote, every single
vote, every vote. They lose one. It's over. They don't have the votes anymore. That's
how close it is. Now there are multiple people who are counted in the Republican
caucus who made a campaign promise saying that they would protect reproductive
rights. This bill that was vetoed curtails them and it's a mess. It is a
mess of a bill. It's 12 weeks, it's 20 weeks here, it's 24 weeks here, it's all
over the place. The goal appears to be to make it so complicated that it's
impossible to get access. It is a mess. One vote. People in North Carolina need
to convince one to break ranks, one to honor their campaign promise, one to do
what polling suggests they should do. Because here's the reality. Polling says
that only 35% of people in the state support the bill. That's not even
close. 35%. What is interesting is that the overwhelming majority of people in
North Carolina don't want additional restrictions at all. Forget about this
mess of a bill. They don't want any new restrictions. 57% of people in North
Carolina support expanding or keeping it as it is. It's not popular. It's another
example of Republicans deciding that they're going to rule rather than
represent. It was vetoed by a governor that's trying to enact the will of the
people, but Republicans at the state legislature in North Carolina are
determined to say, no, you don't know how to run your own life. That's our job. We
know what you think you want. We've seen the polling and we know that's what you
want. That's why we lied to you during our campaigns. But you don't really know
what you want. We know what's better for you. Only 35% of North Carolinans support
this and yet they're going to override a veto. They're going to attempt to. One
vote. People in North Carolina, those who are politically active, those who call
and write and email and tweet and all of that stuff, you have to convince one of
them. That's it. They lose one vote and they can't override the veto. If you are
somebody who gets active and this is a cause you care about, this is probably
the time because they have the votes if everybody, everybody in the Republican
Party sticks together. One. One vote gets flipped and it's over. There will be a
lot of pressure from the Republican Party leadership to the representatives
saying don't do what your constituents want. Don't do what the polling says. Do
what we tell you to do. Don't actually listen to the people who put you in
office. Don't enact their will. Forget about the Republic. Forget about
representative democracy. Just do what you're told and then they'll do what
they're told. If you're active, now's the time to engage in whatever form of civic
engagement that you prefer. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}