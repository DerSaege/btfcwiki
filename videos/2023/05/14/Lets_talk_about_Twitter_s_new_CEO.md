---
title: Let's talk about Twitter's new CEO....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=uUlLXxbu5W4) |
| Published | 2023/05/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk stepping down as CEO of Twitter, with a new CEO, Linda Iaccarino, coming in to lead.
- Linda Iaccarino known for expertise in brand safe advertising, but has ties to the World Economic Forum (WEF).
- The WEF is seen by some as a hub for conspiracy theories, which raises concerns among certain Twitter users.
- A parody account attributed a fake tweet to Linda Iaccarino about controlling Elon Musk, causing confusion.
- Concerns arise about the direction Twitter may take under the new CEO and the potential impact on Musk's influence.
- The new CEO is believed to have right-wing ties but is also expected to address issues like content moderation and verification on Twitter.
- Uncertainty looms over whether Musk will allow the new CEO to make necessary changes or if his ego will interfere.
- The base of users Musk catered to is displeased with the new CEO selection, indicating potential discord ahead for Twitter.

### Quotes

- "Elon Musk stepping down as CEO of Twitter, with a new CEO, Linda Iaccarino, coming in to lead."
- "The base of users Musk catered to is displeased with the new CEO selection."
- "Concerns arise about the direction Twitter may take under the new CEO and the potential impact on Musk's influence."

### Oneliner

Elon Musk's departure as Twitter CEO sparks concerns about the platform's future under new leadership and the impact on his cultivated user base.

### Audience

Twitter users

### On-the-ground actions from transcript

- Monitor and actively participate in the changes occurring at Twitter (implied)
- Support efforts towards brand safe advertising and responsible content moderation on social media platforms (implied)
- Stay informed about the developments within Twitter leadership and their potential implications (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of Elon Musk stepping down as Twitter CEO and the potential shifts in leadership and direction for the platform. Viewing the full transcript can offer a comprehensive understanding of these dynamics. 

### Tags

#ElonMusk #Twitter #LindaIaccarino #SocialMedia #BrandSafety


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about Twitter
and the new boss over at Twitter,
because Elon has decided to finally step down.
And a new CEO will be taking his place.
It is worth noting that Elon will still have something
to do with product development and design and stuff like that.
But the new CEO will be kind of running the show.
So we're going to talk about that and the reactions
to that news from the base that Elon has spent
a lot of time courting.
And just kind of take a look at how things are shaping up.
OK, so who is the new CEO?
Linda Iaccarino, NBC Universal, advertising like Demi-God, a person who is just kind of
renowned for understanding brand safe advertising. So she'll be coming on to clean up his mess.
I mean, that's really what it is.
But there's an issue.
So Yakarino has a tie to something called the WEF, the World Economic Forum.
What is that?
In real life, it's like a thing where rich people get together and talk to each other.
It has become kind of a nexus for conspiracy theories.
The reaction from the right wing that Musk has spent so much time amplifying and courting
has been, hey, Elon Musk, will Tucker Carlson still be able to talk about the WEF like this
with Twitter's new lizard overlord lady running the show?
I wouldn't have seen this under normal circumstances, but this person has a, they bought one of
those blue checks. They're a subscriber to Twitter Blue, so it was amplified. I hope
it's a really bad joke, but this is reportedly the new Twitter CEO, ProMask,
Provax, and a participant in the World Economic Forum. Again, I don't follow this
person, but they're subscribed to Twitter Blue. So that might be a lesson in
Brandsafe advertising. There's also this. There's a tweet from Linda Giaccarino.
It's what it says. It has her picture right there. I'm looking at it. And it
says, my job as Twitter CEO will be to ensure Elon follows the WEF script. He
wasn't given all that DARPA money for nothing. Man, if only there was some
verification system for public figures that would let me know that this isn't
a real one. It's not, just so everybody knows. This is a parody account. But it doesn't really matter
because it's been seen by 60,000 people. Now, why would a whole bunch of people on Twitter have this
conspiratorial view of the WEF? Here's one of my favorite quotes. WEF is increasingly becoming an
an unelected world government that the people never asked for and don't want, Elon Musk.
So he spent all of this time courting the far right, courting this base that continually
reaches for more extreme, more conspiratorial views.
This is who the Twitter base has become.
This is who is signing up for Twitter Blue.
And because of the connection between the new CEO and the WEF, there might actually
be an issue, another one, for Musk.
The reality is, if you look at her previous statements, now it is worth noting that she
She has some pretty right-wing ties, which is funny, but all they saw was WEF and it
was all over.
They didn't actually investigate her.
She's pretty right-wing, but she is also somebody that understands what makes advertising profitable.
So she would probably clean up most of his mistakes if this continues to go forward.
she would establish some rules for BrandSafe advertising
and probably do something about the verification system
on Twitter, which is where a lot of the issues first started.
Now, here's the question.
Is Musk going to be able to let this go?
If he just lets it go, steps away,
and allows her to clean up his mess,
she can probably do it.
But we don't know that he's actually going to do that.
We don't know that he's going to allow her the free rein
that she's gonna need to fix it.
If you look at some of her clips,
when she's talking about stuff like this,
She talks about how content moderation is necessary.
She has spoke about this exact topic, this kind of platform.
It might have even been in reference to Twitter
and talked about how to clean it up.
The thing is, it's basically reverting it back
to what it was before Musk made all of his changes.
I don't know that his ego is going to allow
him to let that happen.
But there are definitely some more changes
occurring at Twitter.
And the base that he courted is very upset by his selection
of the new CEO.
And we'll have to see what happens with the platform,
because it's probably going to continue to be a bumpy ride.
Anyway, it's just a thought.
Y'all have a good day..
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}