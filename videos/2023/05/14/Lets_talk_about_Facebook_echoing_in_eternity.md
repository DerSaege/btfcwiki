---
title: Let's talk about Facebook echoing in eternity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=UaycTJyPId4) |
| Published | 2023/05/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a realization about social media, history, and future history sparked by a diner incident and a Twitter trend.
- Recounts a confusing interaction with his child about learning about lunch counters in school.
- Mentions the lack of consequences for those who poured food during lunch counter protests due to police indifference and lack of social accountability.
- Points out how future generations will have more detailed historical records due to social media documentation.
- Talks about the potential for social media to be a valuable tool for future historians in recording social movements.
- Raises the idea of future history books containing memes and detailed records of online posts.
- Emphasizes the generational impact of social media posts and the importance of being on the right side of history.
- Speculates on the challenges of creating an American mythology in the future with detailed historical records.
- Concludes by reflecting on the public nature of online posts and the scrutiny they will face over time.

### Quotes

- "What you post on Facebook echoes in eternity."
- "Every person is a public figure in that regard."
- "It's wild when you think about it."

### Oneliner

Beau talks about the intersection of social media, history, and future historical documentation, reflecting on the generational impact of online posts and the detailed records they create.

### Audience

History enthusiasts, social media users.

### On-the-ground actions from transcript

- Preserve digital content for future generations by documenting significant social movements online (exemplified).
- Encourage responsible online posting to ensure a positive generational legacy (implied).

### Whats missing in summary

The emotional weight of realizing the long-lasting impact of online posts and the detailed scrutiny they may face in the future.

### Tags

#SocialMedia #History #GenerationalImpact #DocumentingHistory #OnlineLegacy


## Transcript
Well, howdy there, internet people. It's Bo again.
So today we're going to talk about how two videos came together in a really weird way.
And combined with something that's happening on Twitter to, I don't know, kind of give me a
realization. So we're gonna talk about social media and history and future
history. So I'm talking to a friend and I am telling him the story of what
happened at the diner, thing I made the video about. And I use the term lunch
counter to describe where the vets were sitting. And my kid, like we learned
about that. Learned about what? Lunch counters? Yeah, yeah, they showed us pictures. They
showed you pictures of lunch counters. It's early, I have no idea where this is going
if you've already figured it out. And he's like, yeah, yeah, we learned at school. They
showed you pictures and you learned about lunch counters at school. Yeah. Okay, you
I have no idea what to say to this.
Then he's like, what happened to him?
What happened to who?
The people who poured food on him.
Oh, those lunch counters, the lunch counter protests
from the Civil Rights Movement.
So we wound up talking about it.
And I'm sitting there explaining, well,
what happened to the people who poured food on him?
Not much, to be honest, because the cops didn't care.
and there wasn't the same social accountability that there was back then.
And as time moved on, nobody really looked into what grandma and grandpa did.
All got swept under the rug.
And then I started thinking about the video of the people defending Trump.
going to be different in the future. Those old grainy black-and-white photos,
is that really grandma? You'd have to look into it to know who the people were,
especially if you were their grandkids. But in the future, as society progresses,
as it will, as more social movements succeed and more people are accepted,
those struggles will be recorded in history books. The photos will not be
grainy black-and-white photos. No insult to the work of Fred Blackwell, but
technology's changed. They'll be 4k and they will probably have the name of the
people because of social media. Because of people documenting everything, when
you think about it social media is going to be an amazing tool for future
historians. And all of this came up because Twitter is removing inactive
accounts, which a lot of them are accounts of people who are no longer
with us. Now on other social media platforms you have the option of a
like memorializing the account and that's happened a lot and it shows their
family connections. So grandkids will be able to click on mom, click on grandma
and see them say horrible things about their friends because society will
change. The importance of being on the right side of things, it's generational
now. It's not something that's going to be forgotten. What you post on Facebook
Facebook echoes in eternity.
And undoubtedly, when there are books about social movements, just like they contained
editorial cartoons in the past, they will contain memes.
it will probably include the people who posted them. It's just wild to think
about. History in the future can become much more detailed. It's going to be a
whole lot harder to have that American mythology that we talk about on this
channel so often where things just get swept under the rug if they're
inconvenient because people signed their name to this stuff. It's out there. It's
gonna be wild. It's wild when you think about it. Every person is a public figure
in that regard. All of these posts become scrutinized by time. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}