---
title: Let's talk about what Zelenskyy said....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=AROotqrtG08) |
| Published | 2023/05/14 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Information surfaced about Zelensky suggesting aggressive responses towards Russia, like taking Russian towns and conducting missile strikes.
- While Ukraine could take and hold Russian territories, the question is whether they should due to potential Russian responses.
- Ukrainian generals have advised against extreme actions that could provoke wider mobilization in Russia.
- Putin's possible reactions to Ukraine's aggressive moves include using strategic weapons like nukes.
- Beau underscores the importance of maintaining Ukraine's current advantageous position in public opinion.
- There's a distinction between taking and holding territory, with Russia historically demonstrating challenges in holding captured areas.
- Beau believes it's not a good idea for Ukraine to pursue extreme actions and suggests Zelensky's generals likely share this viewpoint.
- Beau argues that even discussing such aggressive responses can influence Russian leadership and divert resources from Ukraine.
- Beau supports Zelensky continuing to vocalize bold strategies, as the fear of these actions can be more effective than actually executing them.

### Quotes

- "You want them in the position they're in now begging for troops."
- "The fear of it happening is probably more effective than actually doing it."
- "He should say wilder stuff to be honest."

### Oneliner

Zelensky's suggested aggressive responses towards Russia pose strategic dilemmas for Ukraine, balancing capability with caution, while leveraging the power of rhetoric to influence Russian actions and resource allocation.

### Audience

Leaders, policymakers, strategists

### On-the-ground actions from transcript

- Analyze potential consequences of aggressive actions (implied)
- Maintain current advantageous position in public opinion (implied)
- Continue strategic discourse to influence opponents (implied)

### Whats missing in summary

Deeper insights on the nuances of geopolitical strategy and rhetoric in influencing adversary actions.

### Tags

#Zelensky #Ukraine #Russia #Geopolitics #NationalSecurity


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Zelensky and some of
the information that came out about some of the things that
he might have suggested.
Looks like he did.
And whether or not it's a good idea.
And the two questions that have come in are could Ukraine
really do this?
And should Ukraine really do this?
Those are two very different questions. And then I'm gonna add a third one that
nobody asked but I think it's really important.
Okay, so if you have no idea what I'm talking about, information came out that
basically Zelensky, in private, has suggested some aggressive responses such
as taking and holding Russian dirt, taking some of their small towns along
the border and occupying them or using or conducting missile strikes into Russian territory,
stuff like this.
Okay so we will first, well we'll just talk about the more extreme, taking and holding
Russian dirt in response.
Okay, so could they, yeah, yeah, Ukraine could absolutely do that.
Should they?
That's a very different question.
You have to think about what the Russian response would be to it, and this is why his generals
have probably advised against this, apparently multiple times.
So if a Russian town gets taken, the Russian populace might respond by saying, hey, we're
going to get invaded, and it provide a better justification for a wider mobilization.
That's not good for Ukraine.
You want them in the position they're in now begging for troops.
That's where you want them.
We don't really want to give them a pretext to get people to sign up to be patriotic
and all of that stuff, to free name of Russian city.
Another risk is that Putin is not the Putin of old.
He is slipping.
That is obvious.
What if he views it as an existential threat to Russia and he decides to use strategic
weapons, nukes. That's their doctrine. So you have that, but you also have the
possibility that they decide to go full bore with it. Like rather than use nukes,
use something conventional, but a level of town in Ukraine. If you're a Ukrainian
soldier holding a Russian town and that happens, what might you do?
Ukraine is winning on the PR front, the public opinion side of things.
Ukraine is winning.
I would not want to risk putting troops in a position where they might act on their own
and do things that would undermine the public support that Ukraine has.
And that's a consideration.
You know, by all accounts, Ukrainian troops have, they have applied the laws of armed
conflict pretty well.
But they're not, they're not robots.
And an extreme Russian response might provoke an extreme Ukrainian response, even if it
wasn't ordered.
And then you have the third thing.
Taking it isn't holding it.
Russia has demonstrated this over and over and over again.
It's not the same thing.
Ukraine could take it.
There's no doubt in my mind.
In fact, there's actually some, I don't want to say large, but big enough towns and cities
that I think are within grasp, holding it as something else.
And you don't know how Russia would respond to that kind of move.
So I personally don't think it's a good idea.
From the actions that have actually happened, I have a feeling like Zelensky's generals
are telling him the same thing.
Then there's the third question.
Should he say stuff like this, even in private?
Yeah, absolutely.
Absolutely because let me tell you something.
Right now, Putin is getting that same report.
Right now Putin is sitting there thinking, wow, this guy's out there, he's thinking
about taking Russian towns.
It might influence leadership.
The fear of it happening is probably more effective than actually doing it.
So I don't think that he should curtail what he's saying.
I think it would be very valuable for Russian leadership to view him as being held back
by the generals, basically saying, yeah, do it, but his generals won't let him because
they have to worry about their actions at that point.
What would make the general stop holding him back?
That's a much better position to be in, much more valuable.
If they can induce that fear enough, resources will be diverted to protecting Russian border
towns.
And every troop that's doing that isn't in Ukraine.
So can they do it?
Most of it, yeah, I think they can.
Should they?
I don't think so.
he stop saying stuff like this? Nah. He should say wilder stuff to be honest. Anyway, it's
It's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}