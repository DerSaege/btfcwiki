---
title: Let's talk about TikTok, Montana, and Constitutions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=LT_-QVSEpSM) |
| Published | 2023/05/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a message received from someone in Montana criticizing his views on freedom of speech regarding TikTok.
- Quotes the 14th Amendment to the US Constitution to support his argument that states are bound by the Bill of Rights.
- Mentions Montana's state constitution also includes a freedom of speech clause.
- Talks about a freedom of speech lawsuit filed by five TikTok creators against the state.
- The creators argue that Montana lacks the authority to dictate foreign policy or national security interests.
- The attorney general in Montana is ready to defend the legislation in court.
- Beau predicts that federal government intervention might preempt some issues but court cases will still proceed.
- States that state governments cannot limit freedom of speech.
- Overall, the issue revolves around the conflict between state laws and individual freedom of speech rights.

### Quotes

- "No law shall be passed impairing the freedom of speech or expression."
- "States that state governments cannot abridge your freedom of speech."

### Oneliner

Beau explains freedom of speech rights in relation to TikTok, citing constitutional amendments and a lawsuit brought by creators against Montana's state laws.

### Audience

Constitution enthusiasts

### On-the-ground actions from transcript

- Contact legal aid organizations for guidance on constitutional rights and freedom of speech (implied).

### Whats missing in summary

Explanation on the potential impact of federal government intervention on the state's freedom of speech laws.

### Tags

#FreedomOfSpeech #Constitution #TikTok #Montana #Lawsuits


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about TikTok
and the constitutions, plural,
and a message that I received
and how things are shaping up
when it comes to Montana's attempt
to stop TikTok from being in their state.
Okay, here's the message.
I'm in Montana
and you have no idea what you're talking about, you Moran.
Freedom of speech arguments against Montana?
Wrong, all caps, exclamation point.
You're a liar.
The Bill of Rights, which is where the freedom of speech
is found, applies to the federal government, not states.
Read the Constitution, all caps, exclamation point.
OK, so I'm going to read this real quick.
All persons born or naturalized in the United States
and subject to the jurisdiction thereof
are citizens of the United States and of the state wherein
they reside.
No state shall make or enforce any law which
shall abridge the privileges or immunities of citizens
of the United States, nor shall any state
deprive any person of life, liberty, or property
without due process of law, nor deny to any person
within its jurisdiction the equal protection of the laws."
That's the 14th Amendment to the US Constitution.
This has been interpreted through something
called the Incorporation Doctrine
to basically mean that the Bill of Rights applies to states.
Just saying, but even if it didn't,
I would like to read something else.
No law shall be passed impairing the freedom of speech
or expression.
Every person shall be free to speak or publish
whatever he will on any subject being responsible for all
abuse of that liberty.
That's also in the Constitution, Montana's.
Montana's state constitution has a freedom of speech clause.
I think it's Article II, Section 7.
Okay, now on to the news that is completely unrelated to that message, a freedom of speech
lawsuit has been filed.
What I'm assuming is going to be the first of many dealing with this.
I want to say this one was brought by five TikTok creators against the state.
Now in it, they claim, of course, a violation of their free speech rights, but they also
make the argument that Montana really doesn't have the authority to engage in foreign policy
or further what they believe is in the interest of national security. That's that's a pretty good
argument. Now the attorney general there has stated that they are fully prepared to defend
this legislation in court, which is good because I got a feeling they're going to have to a lot.
Generally speaking, if I had to guess at how this will end up playing out,
I feel as though the federal government will probably preempt a lot of this,
but the court cases will still end up moving forward. But yes, the state
governments cannot abridge your freedom of speech or any of the other, you know,
Anyway, it's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}