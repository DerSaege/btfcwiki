---
title: Let's talk about Spartacus still giving lessons for those on the bottom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=g56jBSepS4I) |
| Published | 2023/05/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses unity, economy, and Spartacus in response to a message received.
- Mentions the relationship between losing money and attracting investment.
- Talks about diverse workforce discouraging unions, referencing Jeff Bezos.
- Draws parallels between Spartacus' slave revolt and modern-day diversity.
- Argues that bigotry is bad for the economy in a capitalist society.
- Emphasizes that diversity does not stop unification, but bigotry does.
- Criticizes the exploitation of bigotry by those in power to keep people divided.
- Encourages unity across diverse backgrounds rather than falling for divisive tactics.
- Challenges the idea of having more in common with people of similar backgrounds rather than those in positions of power.
- Concludes with a message against falling for tactics that exploit bigotry.

### Quotes

- "Bigotry is bad for the economy. Period. Full stop. It always is."
- "Diversity doesn't stop people from unifying. Bigotry does."
- "Unions are diverse. Spartacus's army was diverse."
- "Those at the top will try to exploit bigotry to keep people divided."
- "You have more in common with the black guy down the road than with your white representative up in DC."

### Oneliner

Beau talks about unity, economy, and Spartacus, showcasing how diversity doesn't hinder unification, but bigotry does, warning against falling for divisive tactics.

### Audience

People seeking to understand the importance of unity and diversity in society.

### On-the-ground actions from transcript

- Challenge divisive rhetoric and encourage unity in your community (implied).
- Support diverse workplaces and advocate against discrimination (implied).

### Whats missing in summary

Importance of recognizing and rejecting attempts to exploit bigotry for political or social control.

### Tags

#Unity #Diversity #Bigotry #Economy #Exploitation


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today we are going to talk about
unity and the economy, briefly,
but mostly about Spartacus.
We're gonna do this because I got a message.
And this is one of those things where,
despite the tone of the message,
I think the person is like really close to getting it and I feel like it's important
to, to kind of talk about this.
Okay so this is about the whole Disney thing.
Just because you lose money on one end doesn't mean you won't gain it on another.
You will attract with the same policies.
There are people who will invest there regardless.
There are people who will move there and bring their skills there regardless because that
as the way some people want to live their lives. Some people don't want to be
surrounded by you with illegal aliens or have their kids brainwashed. Jeff Bezos
said the reason he likes having a diverse workforce is because it
discourages unions. Now I should point out I did not fact-check this. I have no
idea whether or not Bezos ever actually said that because it discourages unions.
After Spartacus' slave revolt was put down, the first thing the Romans did was diversify the slave pits
so the slaves could no longer unify.
Okay, briefly about the economy. We're going to do this really, really quickly.
Bigotry is bad for the economy. Period. Full stop. It always is.
Okay? If you are in a capitalist society and you choose to exclude potential customers, partners,
employees, it's bad for you. It is bad for the economy. All the people at the top,
they know this. They know that. It is bad for the economy. But that's not what I
want to talk about. That's not what I think is important. It's this last bit.
Jeff Bezos said the reason he likes having a diverse workforce is because it
discourages unions. Again, don't know if that's true. After Spartacus' slave revolt
was put down, the first thing the Romans did was diversify the slave pits so the
slaves could no longer unify. This is in here and it's kind of just really pushed
in. It doesn't really fit with everything else that was being said unless the
point is to show that diversity is bad, and it makes it impossible to unify or
have unions, right? Those words are in both, so I have to assume that's the
focus. Okay, let's talk about Spartacus first. The great Roman general, Sporadicus,
the general who became a slave, the slave who defied an empire, whatever.
Okay, so in real life Spartacus wasn't Roman, he was Thracian. Do you think the
tens of thousands of people that joined his army were Thracian? They weren't.
weren't. Castus, one of his commanders, was Golic. Gannicus was a Celt. They did
unify. They did unify. The difference in ethnicity or race or orientation or
gender, that doesn't stop people from unifying. Diversity doesn't stop people
from unifying. Bigotry does. Bigotry does. That's what stops people. What you have
here in your examples, okay, unions, okay. Unions are not, they're not segregated.
they're generally pretty diverse.
Spartacus
had a diverse army.
His commanders,
like even up at the top,
it was diverse.
They unified.
What you have here
isn't proof
that diversity stops
unification.
Your two examples, true or not,
demonstrate that
People at the top try to use bigotry to keep people divided.
That's what it shows.
They're your examples.
And in both of them, the point is those at the top try to
encourage the bigotry to keep people divided, to keep them
easier to control. Do you think that there might be a political party in the
United States doing the exact same thing, othering people, saying that they're
different, encouraging that bigotry? You don't want to be around those people. You
what your kids brainwashed, right? Keeping people divided, keeping them easier to control.
That's what your examples are. Unions are diverse. Spartacus's army was diverse. What
What you're saying, your examples, don't showcase that people can't unify over diversity.
It shows that those people at the top will try to exploit bigotry to keep people divided
so they are easier to control.
I assure you, you have more in common with the black guy down the road, with the Latino
guy up the street with the trans person next door than you are ever going to
have in common with your white representative up in DC.
Your view here, you picked the examples and in both of them it's that
owner class, those people up at the top exploiting bigotry to keep the people on
the bottom in their place. Don't fall for that. Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}