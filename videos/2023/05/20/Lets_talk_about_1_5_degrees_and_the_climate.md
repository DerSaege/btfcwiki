---
title: Let's talk about 1.5 degrees and the climate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=4OxdIgcbxS4) |
| Published | 2023/05/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of 1.5 degrees Celsius in relation to climate change.
- Warns about the likelihood of the average global temperature exceeding 1.5 degrees Celsius by 2027.
- Emphasizes the increased risks of wildfires, food shortages, droughts, heat waves, and flooding at 1.5 degrees Celsius.
- Notes the international community's goal of limiting climate change to 1.5 degrees Celsius and the current lack of progress.
- Calls for climate action to be a prominent campaign issue in elections.
- Stresses the urgent need for aggressive action in transitioning to cleaner energy, altering food consumption, and preparing for future human migration.
- Acknowledges the disproportionate impacts of climate change and the necessity for the United States to take significant action.
- Compares addressing climate change to a semi-truck rather than a car, indicating the urgency of action.
- Urges immediate and continuous attention to climate change as delays will have severe consequences.
- Warns that waiting until the effects of climate change are apparent will be too late.

### Quotes

- "The impacts from this, they're going to be severe."
- "The climate is not a car. When you slam on the brakes, it's not going to stop."
- "This isn't about being fair. It's not a board game."
- "By the time you see it, it's too late."
- "We really need to get the ball on this one."

### Oneliner

Beau explains the urgency of addressing climate change as global temperatures approach 1.5 degrees Celsius, warning of severe impacts and the need for immediate action to prevent irreversible consequences.

### Audience

Climate advocates, policymakers

### On-the-ground actions from transcript

- Advocate for climate action in local and national political campaigns (implied)
- Support and push for aggressive transition to cleaner energy sources (implied)
- Raise awareness about the urgent need for climate action in communities (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the urgency and severity of climate change, stressing the need for immediate action to mitigate its impacts.

### Tags

#ClimateChange #GlobalWarming #Urgency #PoliticalAction #CleanEnergy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the climate.
And 1.5 degrees celsius and why that number matters,
because you're gonna start hearing it a lot.
Um, the newest information says that sometime
between now and 2027, we will have a year
where the average temperature,
where we break that threshold.
And we go over 1.5 degrees Celsius warmer
than pre-industrial levels.
Right now, there is a 66% chance of that happening
between now and 2027.
So it is more likely than not.
There is a 98% chance that within the next five years,
the world will experience the hottest year on record,
at least one of those years.
There's a decent chance that we're gonna have more than one.
So,
crossing that threshold, and it is worth noting that
by this information, at least right now,
they think that it will be a temporary thing.
We'll cross that for a year and then it'll cool down a little bit.
But the reason that number is important
is because right now we're at about 1.2.
When we get to 1.5, things like wildfires,
food shortages, droughts, heat waves,
flooding in other places become more likely.
Back in 2015, the international community set the goal
of holding the climate at 1.5 degrees
doesn't look like we're meeting those objectives, doesn't look like we're
doing too well on this front.
This is a topic that needs to be a campaign issue
in every election
until we have
things mitigated.
We need much more aggressive action
when it comes to transitioning
away from dirty energy into cleaner energy, to shortening our supply chains, to altering
food consumption, all kinds of things to getting ready for what at this point to me seems to
be the inevitable future human migration that is going to occur.
The United States has to get on the ball and I know somebody is going to say, well you
know China or India or some other country, they're not going to do their part.
And we have to do twice as much.
It's not fair.
This isn't about being fair.
It's not a board game.
If it was something that was going to be fair, the impacts would be most heavily felt in
industrial nations.
But that's not what's going to happen.
It's not fair.
This is something that has to be done.
The climate is not a car.
When you slam on the brakes, it's not going to stop.
The warming isn't going to stop.
It's more like a semi-truck.
It takes time.
Time that we don't have.
This needs to be a campaign issue constantly.
have to start working on this much more than we are now and I know people don't
want to hear that because you know we just made the largest investment in
green energy you know ever yeah that's not enough. It's not enough. The impacts
from this, they're going to be severe. I mean that much at this point is
inevitable. We need to begin a process of transitioning a whole lot of things
in this country to make sure that all of those things that people talk about, them
wanting to give their grandkids are still possible. If you ignore this, if you wait
until it becomes very apparent, it will be too late. This isn't a topic like a lot of
political issues where people see it in front of them and then they start screaming at the
government and in a year or two the government does something about it right
that won't work by the time you see it it's too late we we really need to get
the ball on this one. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}