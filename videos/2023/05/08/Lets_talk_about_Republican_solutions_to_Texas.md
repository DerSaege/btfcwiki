---
title: Let's talk about Republican solutions to Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=hJwLZVampYY) |
| Published | 2023/05/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he doesn't support Republican solutions when advocating for mental health resources after a shooting.
- Believes Republicans use mental health as a talking point without any intention of taking meaningful action.
- Refuses to help Republicans spread lies about addressing mental health by discussing it right after a shooting.
- Points out the lack of budget allocation and actual initiatives for mental health by the Republican Party despite controlling the House.
- Questions the sincerity of public figures advocating for mental health who justify instances like Jordan Neely's killing.
- Stresses that Republicans use mental health as a facade to appear caring, but their actions demonstrate otherwise.
- Urges viewers to look into public figures' stances on mental health and compare them to their justifications for certain events to see the lack of genuine concern.
- Beau advocates for discussing mental health reform at other times except immediately following a shooting.
- Calls out the hypocrisy and lack of genuine concern from Republicans regarding mental health issues.
- Encourages critical thinking about political rhetoric and actions related to mental health advocacy.

### Quotes

- "It's a lie, and I'm not going to help them flood the zone."
- "They absolutely do not care about mental health, period."
- "They have zero intention of doing anything about mental health."
- "I don't believe that all of them had a sudden change of heart."
- "Y'all have a good day."

### Oneliner

Beau exposes Republican insincerity in advocating for mental health post-shootings, citing their lack of genuine commitment and using it as a facade for public image.

### Audience

Advocates for mental health

### On-the-ground actions from transcript

- Fact-check public figures' past statements on mental health and their justifications for certain incidents (implied).
- Advocate for genuine mental health reform in your community (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the insincerity behind Republican advocacy for mental health, urging viewers to question political rhetoric and actions in this regard.

### Tags

#MentalHealth #RepublicanSolutions #PoliticalRhetoric #Advocacy #Insincerity


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about talking points
and Republican solutions.
And why I don't back them up when they suggest something,
even though any other time, it's a topic that I advocate for.
And somebody asked why, in certain circumstances, I don't support Republicans when they say it.
And it's a fair question.
So here's the message.
You often talk about the need for greater mental health resources in your episodes.
I've noticed that you don't advocate for mental health after a shooting when you could
get Republicans to agree with you.
I followed you since 2019 and know that this isn't an accident, but for the life of me I
couldn't guess why. It seems like getting some mental health reform would be in your interests.
Not that you're crazy, but that you've talked about it before.
Why not encourage them at the same time they're advocating for it?
As I respond to this, I want you to think about the soft language that I normally use to describe
some of this. I don't want to help them lie. They're liars. They don't believe a
word of it. It's a lie. A total lie. Not being economical with the truth. Not
less than accurate. It's a lie. They have zero intention of doing anything about
mental health. It's a talking point that they they drag out every time something
happens, but they don't believe that they're going to do anything about it
any more than they believe they should love their neighbor. They don't believe
it. It's a lie, and I'm not going to help them flood the zone, which is what they
do after every one of these incidents. They find something about the assailant
to throw out into the media. They advocate for mental health by just
saying we need mental health, they talk about parents and they offer thoughts
and prayers, but they never actually do anything about it. They're lying and I
don't want to help them do it. That's why I don't talk about it right after a
shooting, but we'll talk about it at any other point in time. I would imagine
after me saying that you want some evidence, right? I mean I would. I give
you two really good pieces. First and foremost, the Republican Party controls
the House. They control the budget right now. Where's the money for it? Multiple
incidents. Where's the money? That's right, it's not there. In fact, they're
cutting what little programs that we actually have. What about at the state
level? Are red states known as like a bastion of good mental health care?
There are a lot of access in a red states where they control everything.
There's not right because they have absolutely no intention of doing
anything with it. It's just something to throw out to make it seem like they care.
They don't. They don't. And now for a little bit more of a dramatic piece of
evidence that you can look up yourself. I want you to look at the talking heads,
those with public profiles, that have advocated for mental health over the
last couple of days and then I want you to scroll back and go back a few days
and see how they justified Jordan Neely being killed. I don't believe that all of
them had a sudden change of heart. To them, being in mental distress, that's
justification for you to be put in the forever box. They absolutely do not care
about mental health, period. And I'm not going to pretend like they do and
entertain those talking points. It's really that simple. Anyway, it's just a
thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}