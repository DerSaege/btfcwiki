---
title: Let's talk about Tucker, Elon, Fox, and what to do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=BqdVK9T_Hqw) |
| Published | 2023/05/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides insight into the situation involving Fox News, Tucker Carlson, and Elon Musk, suggesting that Tucker wants to free up his allies to go after Fox in hopes of being released from his contract.
- Tucker retained a Hollywood lawyer known for being ruthless, indicating his seriousness in the matter.
- The idea of silencing Tucker is deemed preposterous by his attorney, Brian Friedman.
- If Tucker's allies speak out against Fox and engineer some outrage, it may impact the situation.
- There are reports of Tucker meeting with Elon Musk to potentially collaborate on a new project, hinting at a possible Fox News rival.
- Tucker's absence has already affected Fox's viewership, leading to a loss of trust and declining numbers.
- The conflict seems to be more against Rupert Murdoch than Fox News itself, raising questions about how it will unfold.
- Ultimately, either Tucker or Fox is likely to face significant consequences due to the escalating situation.

### Quotes

- "The idea that anyone is going to silence Tucker and prevent him from speaking to his audience is beyond preposterous."
- "At the end of this, either Tucker or Fox is going to take a huge hit."
- "What do you do when and all the worst people you know are fighting."

### Oneliner

Beau provides insights into the escalating conflict between Tucker Carlson, Fox News, and Elon Musk, suggesting potential repercussions for both parties involved.

### Audience

Media consumers

### On-the-ground actions from transcript

- Support alternative media sources (suggested)
- Stay informed about media dynamics and biases (implied)

### Whats missing in summary

Insights on the potential impacts of media conflicts and power struggles.

### Tags

#FoxNews #TuckerCarlson #ElonMusk #MediaConflict #AlternativeMedia


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about Fox News
and Tucker Carlson and even Elon Musk makes a cameo
in this coverage.
It is a very relatable story.
I know that most people watching this channel
can completely empathize with Tucker Carlson here.
The reporting suggests that Tucker wants to free up his allies, get his allies to
go after Fox so they will let him out of his contract so he can go start another
network or go to work for another network, something like that. I think most
people at some point in their life were forced, maybe they had to leave a job
that they liked and the company was gonna pay him like 20 million dollars a
year to not work and I think that most people can understand the amount of
anger you would have in that kind of situation. So I think most people would
be able to to really root for Tucker on this one as he goes to war with Fox. All
All jokes aside, it does appear that Tucker's taking this pretty seriously.
The reporting suggests he retained Brian Friedman, who is like a big-time Hollywood
lawyer.
Like the kind, you know, the kind they depict in the movies is just being utterly ruthless.
That's this person's reputation.
I don't know, never met him, but that's how they are described by people.
Very motivated in the pursuits once they have been retained by somebody.
He said, the idea that anyone is going to silence Tucker and prevent him from speaking
to his audience is beyond preposterous.
So there is some on the record stuff about this.
Now if Tucker frees up his allies, because Tucker himself can't really do anything because
of the contract, he can't go out and attack Fox.
But if his allies do and talk about how poorly Fox treated Tucker and kind of go after maybe
engineer some issue. I mean I don't know that his friends would have a whole lot
of you know experience creating like an outrage of the day or anything like that
but if they did they might do something like that. There's also reporting that
during this period Tucker Carlson met with Elon Musk to discuss working
together that they have talked about it. There aren't any details on that but
let's be real, if you wanted to create a rival to Fox, who do you know that has
the money and has the ideology? It's a pretty short list, so that makes
sense too. At the end of the day somebody's gonna lose here. Fox is
already in trouble. They're losing viewership, partially because their
viewers have lost trust and partially because Tucker's not there. Tucker,
despite what like 98% of the people watching this channel think about him, he
was a draw. He brought in viewers. So they're already kind of down. Now the
flip side to this is that he's not attacking Fox if he does this. He's not
going up against the News Network. He's going up against Rupert Murdoch. I don't
know how that's going to play out. I mean who do you root for there? At the end of
this, either Tucker or Fox is going to take a huge hit. If this reporting is
accurate and Tucker's crew is really gearing up to just go to war to try to
get him out of his contract, somebody is going to take a hit. What do you do when
and all the worst people you know are fighting.
Let them fight.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}