---
title: Let's talk about everything on the table with SCOTUS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gB7irKhGupo) |
| Published | 2023/05/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Update on Supreme Court and Senate Judiciary Committee saga for 2024 election.
- Flurry of reports on questionable transactions involving Supreme Court members.
- Chief Justice declined Senate Judiciary Committee's invitation to testify.
- Senate Judiciary Committee may have to decide on next steps without Chief Justice's voluntary cooperation.
- Dick Durbin implies potential hearings and investigations into Justice Thomas.
- Pressure on Senator Feinstein to return to committee or retire due to absence.
- Potential for 2024 to involve talks of removing Supreme Court justices.
- Republican stance on Thomas being on their team despite ongoing revelations.
- Democratic Party may harness issue for 2024 election and energize voters.
- Supreme Court's lack of action could create an election issue for Democrats.

### Quotes

- "Everything is on the table."
- "A lot of Republicans are saying, well, we don't care. Thomas is on our team."
- "If they continue that attitude, they are creating an election issue for the Democratic Party."
- "I didn't see this on the 2024 calendar, but I think it's going to become an issue."
- "Y'all have a good day."

### Oneliner

Update on Supreme Court saga may impact 2024 election, with potential talks of removing justices and creating an election issue for the Democratic Party.

### Audience

Political activists

### On-the-ground actions from transcript

- Contact local representatives to express opinions on Supreme Court issues (implied).
- Organize or join community events discussing the role of Supreme Court justices (implied).

### Whats missing in summary

Insights on the potential long-term implications of the Supreme Court saga.

### Tags

#SupremeCourt #SenateJudiciaryCommittee #2024Election #PoliticalPressure #DemocraticParty


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are gonna provide a little bit of an update
on the ongoing saga of the Supreme Court
and the Senate Judiciary Committee,
because a chain of events may push this to be
one of the top issues for 2024
in a way that it normally isn't.
So if you have missed it,
there has just been an absolute flurry
reporting, dealing with, let's just refer to them as questionable transactions and
arrangements, dealing with members of the Supreme Court. The Senate Judiciary
Committee requested the Chief Justice come testify and explain what's going on
and talk about ethics there. The Chief Justice said, I respectfully decline
your invitation. Those words, they may have drastically altered the 2024 races
in a whole lot of ways that may not be entirely evident at this point. Without a
voluntary, you know, come in let's talk about it type of thing, the Senate
The Judiciary Committee has to make a decision on what they're going to do.
There are people on the committee that are, let's just say, enthusiastic about the prospect
of speaking directly to these issues.
Dick Durbin, for one, said, the bottom line is this, everything is on the table, day after
day, week after week, more and more disclosures about Justice Thomas. We
cannot ignore them. That probably means hearings. That probably means an
investigation. He went on to say that they wanted to gather the information.
Now, this puts Feinstein in a situation. She's got a lot of people asking her to
resign now, because she is on this committee and because she is
currently absent, it makes it really hard for them to do
anything. So there will be even more pressure on her to either
get back to work or decide it's time to retire. Now, how does
this play for 2024? If these hearings start heating up,
You're going to see
not just the normal
conversations about Supreme Court appointments and stuff like that.
You're going to see conversations about the possibility of removing
Supreme Court justices. Everything is on the table.
That's what he was saying. Currently
that seems really unlikely. A lot of Republicans
are saying, well, we don't care. Thomas is on our team.
And I get it. I mean, I understand what they're saying.
But at the same time, these
revelations continue to come out. I would imagine there's probably more.
People are going to want some kind of accountability.
This is something that the Democratic Party will be able to harness
for 2024. This is an issue that might actually energize people. If they start
talking about actually removing Supreme Court justices and keeping everything on
the table, there might be a lot of people who are incredibly unhappy about previous
rulings from the Supreme Court who might get out and vote, even if they don't
necessarily care about the the ethical issues being presented, it might be a
situation where they just see an opening. I would give that pretty good
odds that that happens, assuming that the Supreme Court continues to just, well
we're not going to do anything about it. We're not going to talk about it. We're
not going to disclose anything. If they continue that attitude, they are creating
an election issue for the Democratic Party because it can easily be spun into
if they will engage in questionable transactions and then continue to rule,
they'll do it to you too. And that is a tried and true electoral method. I didn't
see this on the 2024 calendar, to be honest. I didn't see this as becoming an
issue but I think it's going to because I don't think the Republican Party is
going to break ranks here and I think they're handing the Democratic Party an
issue that they can really use assuming that they still desire to go like
Dark Brandon and actually go on the offensive. You know there are there are a
lot of times when the Democratic Party was handed a tool and decided to
snatch defeat from the jaws of victory type of thing. But we'll have to wait and
see. I would expect to continue to hear more and more about this until it
reaches a pitch. Anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}