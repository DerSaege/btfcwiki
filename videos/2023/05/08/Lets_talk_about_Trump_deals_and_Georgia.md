---
title: Let's talk about Trump, deals, and Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=1irrHJo-mmk) |
| Published | 2023/05/08 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about recent developments in the Trump case in Georgia involving electors and immunity deals.
- Mentioned that eight or nine electors have taken immunity deals to provide information.
- The prosecution is aiming to build a strong case against those who briefed the electors.
- Revealed a quote from the prosecutor to one of the electors' attorneys regarding immunity and answering questions.
- Indicates that the prosecution is moving towards indictments and has specific questions they want answered.
- Suggests that the case involves individuals higher up, potentially household names or current/former elected officials.
- Speculates on the likelihood of upcoming indictments based on the unfolding events.
- Mentions a statement from the prosecutor in Georgia about being ready this summer, hinting at a timeline for action.

### Quotes

- "Either Elector E is going to get this immunity and he's going to answer the questions or we're gonna leave and if we leave we're ripping up his immunity agreement and he can be on the indictment."
- "There are a lot of people who are involved in this but they are insulated directly from the electors."
- "I think that this latest chain of events is strengthening a case that is already pretty strong."

### Oneliner

Recent developments in the Trump case in Georgia indicate potential indictments and a clear path from lower-level individuals to higher-ups, possibly household names or elected officials.

### Audience

Activists, Legal Observers

### On-the-ground actions from transcript

- Contact legal aid organizations for updates and involvement (suggested)
- Stay informed and ready to take action based on developments (suggested)

### Whats missing in summary

Details on the potential implications of the unfolding events in Georgia related to the Trump case.

### Tags

#Trump #Georgia #Electors #ImmunityDeals #Prosecution #Indictments


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and Georgia and the electors
and some new information that has come out
and what we can kind of surmise is going on
because there have been a few developments
since the last time we talked about it.
The last time we were talking
this particular Trump case, the news had come out that it appeared there were
some of the electors who the prosecution was willing to give immunity deals to.
But somehow that deal didn't really get conveyed.
Somehow those who were being offered deals,
they seemed to not know that they were being offered them.
Since that issue have been clarified, eight or nine of the electors have
taken the deal. This is a lot of people who can go in
and say, this is what happened, this is what we were told by people
higher up. Odds are the prosecution is trying to develop
an incredibly strong case against those people who directly briefed the electors and then they will
offer them some kind of deal as well. A very interesting quote has surfaced and this is coming
from the prosecutor to, I think, one of the electors' attorneys. Here's the deal. Here's the deal.
Either Elector E is going to get this immunity and he's going to answer the
questions or we're gonna leave and if we leave we're ripping up his immunity
agreement and he can be on the indictment. So there are two really big
pieces of information there. They are definitely going for the indictment.
That's happening. The other piece is that they have questions that they want
answered. This isn't a case where they're looking at some little fish who isn't
worth prosecuting. This is a case where somebody has, at least in the
prosecution's view, committed some crime and they're willing to kind of brush
that aside if they talk. Eight or nine, depending on the reporting, have agreed
to take that deal. If they start talking about those who briefed them and that
case develops further than what we already know, there's probably a pretty
good likelihood that that person will look even higher up. There are a lot of
people who are involved in this but they are insulated directly from the
electors. This chain of events it looks like it's building the prosecution a
very clear path from the fake electors who are at the bottom to people much
higher up, household names, people you you've heard of, people who were or may
still be in elected office. I would imagine that there is an indictment
coming. And I think that this latest chain of events is strengthening a case
that is already pretty strong. Now we have the statement from the prosecutor
there in Georgia that basically they told the cops be ready this summer. So
So that time frame, it's drawing closer.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}