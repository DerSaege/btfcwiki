---
title: Let's talk about a message about terms and acceptance....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=I6rSpB8SF78) |
| Published | 2023/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a message thanking him for supporting a community in Georgia, mostly by city folks.
- Urged to address terminology struggles among parents with trans kids, particularly older generations.
- Requested to convey that it's okay to not be perfect with terminology immediately.
- Acknowledged for being supportive despite past awkward moments and mistakes.
- Emphasized the importance of love and basic respect for the trans community.
- Encouraged to share the journey of improvement with terminology and support.
- Stressed the need for loved ones to continue showing support and respect.
- Expressed hope for a clear delivery of the message on understanding and acceptance.
- Mentioned the idea of finding a way to spread the message effectively.
- Ends with a message of positivity and a wish for a good day.

### Quotes

- "All we really want is for the people who loved us yesterday to love us today and to get the basic amount of respect anybody else gets."
- "People shouldn't be terrified at getting some term wrong. If you're trying, it's okay and we'll see it."
- "You were always supportive even when you were awkward and you have a huge trans following."
- "I hope you can come up with some way to deliver this well because I don't really know how to make this clear."
- "Y'all have a good day."

### Oneliner

Beau receives a message urging him to address terminology struggles among parents with trans kids, stressing the importance of support and respect, even through initial awkwardness.

### Audience

Supportive individuals.

### On-the-ground actions from transcript

- Find ways to effectively deliver messages of understanding and acceptance (suggested).

### Whats missing in summary

The emotional journey of learning and growing in support and understanding. 

### Tags

#Support #TransgenderCommunity #Acceptance #Terminology #Respect


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about terminology and a
message I was sent.
This might be particularly interesting for people who are
my age or older.
So what I'm going to do is I'm going to go through and read
the message, and then we'll go from there.
Beau, I've watched you for years, and I want to start by saying I can't thank you enough
for the support you've given my community.
I live in Georgia and we have support here, but it's mostly from people from the cities
and who are obviously from the cities.
Subtext, you look like a redneck.
You've always been nothing but supportive, so I'd hate to ask you to do something else
but this is really important and like you say sometimes it's the messenger. You
have a lot of literal boomers in Gen X watching you and that's who needs to hear
this. I was at a group event for parents and they all talked about how they had a
hard time with the terminology. These are parents who are willing to show up to a
meeting with their adult trans kids and they all said they were constantly
afraid they'd get some word wrong. You are a very awkward ally sometimes. You pause and
I can see you actually searching for the right word. I laugh out loud every time you are
forced to say the word cis. You get awkward around the terminology and you've made some
bad analogies in your early videos. You actually, you once actually compared me to a lamp for
heck's sake. And you referred to non-binary people as whatever else wants.
I remember that. But nobody has ever doubted you were anything but supportive.
And over the years you've gotten so much better, but you still have to think about
it. I think it would be really helpful for people to understand that none of us
expect them to get everything right or to be completely comfortable with it
right away or even years later. We just want to be treated with love or at least basic
respect. I have no idea how you'd make this a video, but people really need to hear about
how you were quote, less than ideal in the beginning, but you were always supportive
even when you were awkward and you have a huge trans following. All we really want is
for the people who loved us yesterday to love us today and to get the basic amount of respect
anybody else gets. People shouldn't be terrified at getting some term wrong. If
you're trying, it's okay and we'll see it. I hope you can come up with some way
to deliver this well because I don't really know how to make this clear other
than pointing to you and saying look at some of the things this donkey has said
but since they were always said in love and support, we love him. I will try to
come up with some way to get this message out to people.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}