---
title: Let's talk about Trump's silence on a certain topic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=nXtY6765KVU) |
| Published | 2023/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Outlines a recent conservative uproar over a beverage company's novelty can.
- Explains why Trump remained quiet during the uproar and why his son spoke out.
- Mentions that Trump's financial disclosure revealed a significant investment in Anheuser-Busch InBev.
- Notes that the Trumps did not actively participate in the culture war despite their investment in the company.
- Suggests that culture wars are used to keep supporters busy, angry, and energized.
- Emphasizes that those at the top do not genuinely care about the culture wars.
- Points out the hypocrisy of Trump's investment in the company while calling for the boycott to stop.
- Concludes that money always wins over culture war rhetoric for the Republican base.

### Quotes

- "They don't believe what they sell you."
- "If it's between that culture war nonsense that has the Republican base super angry, yelling, screaming and running over cans and money, the money always is going to win."
- "Those at the top, they don't actually care about that."

### Oneliner

Beau reveals how the Trumps' investment interests outweighed their supposed belief in culture wars, exposing the hypocrisy within conservative circles.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Divest from companies contradicting stated beliefs (suggested)
- Stay informed on political motivations behind certain actions (suggested)
- Support businesses that uphold values you believe in (suggested)

### Whats missing in summary

Exploration of the manipulation tactics used by political figures in perpetuating culture wars for personal gain. 

### Tags

#ConservativeUproar #Trump #CultureWars #Hypocrisy #Investments


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a recent uproar from conservative circles.
You know, some of their culture nonsense that, you know, went wild.
Um, and we're going to talk about why Trump might have been quiet during that
period and what might have led to his silence or perhaps even his son
speaking out and what this can really tell us about Trump's relationship to
the culture wars. Okay so as you you might remember a large group of
conservatives were upset with a certain beverage company. They made a novelty
can that really upset them and there was just all kind of drama about it. And they
started to stop buying Anheuser-Busch InBev products, right? And as that
outrage really got going, Trump's kid came out and said, whoa, whoa, hang on a
a second. Y'all, I don't know if y'all really need, this is an American icon here.
This is an iconic company in there and they're kind of conservative and kind of
called for everything to calm down. So some recent reporting suggests that the
reason behind that might lie in the former president's most recent financial
Disclosure, which in what was listed as DJT Trust Investment II, somewhere
between one and five million dollars worth of Anheuser-Busch InBev.
Yeah, so that's the most recent financial disclosure, sure things could have changed,
but it's definitely interesting to say the least.
It is noteworthy that the Trumps did not partake in this particular culture war.
And are at least were recently invested in the company somewhere between one and five
million dollars.
And they called for the boycott to stop.
All of the culture war nonsense, it is simply to keep the low information supporters busy,
to keep them angry, to keep them energized.
Those at the top, they don't actually care about that.
They tell their followers to be angry about something.
Sometimes lines get crossed, I guess.
But make no mistake about it.
If it's between that culture war nonsense
that has the Republican base super angry, yelling,
screaming and running over cans and money, the money always is going to win.
They don't believe what they sell you.
They just know it's a good way to keep people motivated and angry, to keep you scared, to
keep you afraid, to keep you listening to them and under their control.
most recent financial disclosures, one to five million dollars in that company, you
would think that if the Trumps were going to say, hey stop that boycott, that they
might want to mention that. I mean if they're being upfront, I guess, or maybe
if they truly believed in the culture wars that got declared, maybe they'd pull
their investment. But I mean they wouldn't want to do that you know after
the boy got started because stocks might be down and that might hurt them
personally and that's not what it's about. Anyway it's just a thought y'all
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}