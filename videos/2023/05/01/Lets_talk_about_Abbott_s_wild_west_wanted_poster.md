---
title: Let's talk about Abbott's wild west wanted poster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=g0te60cZIcg) |
| Published | 2023/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor Abbott announced a $50,000 reward for info on the criminal who killed five illegal immigrants in Texas.
- The governor's statement unnecessarily emphasized the victims as "five illegal immigrants," sparking controversy.
- The message was seen as a signal to the base, portraying the victims as different from Texans.
- Beau criticizes the governor's PR stunt, pointing out the lack of key information in the message.
- Despite trying to appear tough, the governor's message lacked critical details typically found on wanted posters.
- The governor's communication strategy should include Spanish translations, considering Texas's demographics.
- Beau suggests that the governor's approach seems more focused on a publicity stunt than actual justice.
- The lack of information provided in the governor's message raises questions about the sincerity of the reward offer.
- Beau implies that the governor's actions are more about optics and signaling rather than genuine concern for justice.
- The entire ordeal is viewed as a performance rather than a genuine effort to apprehend the criminal responsible.

### Quotes

- "He's not trying to leverage his network, not to actually bring the criminal to justice."
- "This whole thing is a PR stunt. Nothing more."
- "The amount of the bounty. He's got that covered."
- "No information whatsoever. No name, no picture, nothing."
- "It's not just a way to signal to his base about that. This whole thing is a stunt."

### Oneliner

Governor Abbott's announcement of a reward for info on the deaths of five immigrants in Texas is criticized as a PR stunt lacking critical details or genuine intent for justice.

### Audience

Texans, Advocates

### On-the-ground actions from transcript

- Advocate for inclusive and accurate communication strategies with diverse communities, suggested
- Support efforts to seek justice for victims through genuine actions, exemplified

### Whats missing in summary

The full transcript provides a detailed analysis of Governor Abbott's messaging, shedding light on the performative nature of his actions and the lack of genuine intent behind his statements.

### Tags

#GovernorAbbott #Texas #Immigration #PRStunt #Justice #CommunityPolicing


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Texas
and a statement put out by the governor there,
Governor Abbott, and we're gonna talk about what's in it
and we will briefly talk about what everybody's
kind of talking about that's there.
And then we're gonna talk about what isn't in it.
And what isn't in it is actually more telling than what is,
what is, which is really kind of surprising given what's in
there. Okay, so the governor sent out a message to his
million followers. And it says, I've announced a $50,000 reward
for info on the criminal who killed five illegal immigrants
Friday. Also directed Operation Lone Star to be on the lookout.
I continue working with state and local officials to
ensure all available resources are deployed to respond. And then there's a
photo of a press release that basically has the same information repeated. He
did manage to work the phrase, you know, five illegal immigrants into the
press release as well. And that's what people are talking about because that's
completely unnecessary statement. They're five people, right?
And people are pointing out that that's a signal,
signaling to the base. You know, those people were different, that kind of thing.
Not five Texans. Not five people living in Texas.
Nothing like that. Five illegal immigrants.
But here's the thing. This whole thing's a signal.
This whole thing is a PR stunt. Nothing more.
He wants to cast the image
of, you know, super tough governor from the Old West
putting a cash bounty on the bad guy.
I got it. I understand.
But he kind of falls short, doesn't he?
I want you to think about
What three things are always on a wanted poster? Whether you're talking
about like, modern ones or ones from back in the day,
the era that he's trying to capture and the image that he wants to put out
to those people who can't see through it.
Three things. The amount of the bounty. He's got that covered.
A picture of the person? No. And their name? No.
sent this out to a million people, gave them absolutely no information whatsoever.
He's not trying to leverage his network, not to actually
bring the criminal to justice.
He's trying to leverage it
for some PR stunt, nothing more.
I would also suggest
that given the nature of this,
it should also probably be translated into Spanish.
My guess is that there's going to be more information coming from that
community
than the English-speaking community. I mean, but to be honest given the fact
that he's governor of Texas and like a third of the state
speak Spanish at home, I would suggest that pretty much all of his official
communication should go out that way.
but he wouldn't want to do that.
Doesn't cast the right image, I guess.
I understand
what people are talking about.
I get what you're saying
when you are talking about
the way he categorized
those five people.
Yeah, that's definitely worth talking about.
But I think it should also be mentioned
that he doesn't care at all.
It's not just a way to signal to his base about that. This whole thing is a
stunt. No information whatsoever. No name, no picture, nothing. I'll have some links
down below to information about the person that they're looking for. Anyway,
It's just a thought, y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}