---
title: Let's talk about Tennessee, Missouri, and intervening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=gUjhRhMfdgY) |
| Published | 2023/05/01 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tennessee and Missouri are facing potential federal government intervention due to exceeding their authority.
- The Department of Justice is suing Tennessee over the ban on gender-affirming care, citing violation of the Equal Protection Clause of the US Constitution.
- The law in Tennessee discriminates based on sex and trans status, denying equal rights to this demographic.
- Missouri has a judge who has temporarily halted the enforcement of a similar law to gather more information.
- The ACLU and federal government are expected to push back against these laws that violate constitutional rights.
- There is concern and worry among many individuals about these laws taking effect.
- The Department of Justice's arguments against the laws are strong, making it difficult for states to defend their positions.
- Federal intervention in these cases signifies a potential path towards protecting the rights of marginalized communities.
- The determination on the temporary restraining order in Missouri is expected by Monday at 5 p.m.
- The actions taken by the federal government and ACLU show proactive measures to combat unconstitutional laws.

### Quotes

- "The Department of Justice is saying people have a right to make their health care decisions with their family, with their doctors."
- "It's gonna be really hard to say that DOJ is wrong."
- "This is a good sign. They're not waiting for these laws to take effect."
- "Hopefully help is on the way."
- "I know that there are a whole lot of people genuinely worried about this."

### Oneliner

Tennessee and Missouri laws face federal scrutiny for violating constitutional rights, sparking proactive intervention and hope for marginalized communities.

### Audience

Advocates, Activists, Citizens

### On-the-ground actions from transcript

- Contact local representatives to voice opposition to discriminatory laws (implied).
- Stay informed about the legal proceedings and outcomes in Tennessee and Missouri (implied).

### Whats missing in summary

The full transcript provides more in-depth analysis and background information on the legal actions being taken against discriminatory laws in Tennessee and Missouri.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Tennessee
and how the federal government has decided
it needs to step in there.
And we are going to also talk about Missouri
because a judge decided they may need
to step in there as well.
Basically, the general tone is that these states
are exceeding their authority.
And it may need to go to court to settle some things.
In Tennessee, the Department of Justice
has filed suit over the ban on gender-affirming care.
The Department of Justice says that that law violates
the Equal Protection Clause of the US Constitution.
they're saying it does it because it discriminates on the grounds of both sex
and trans status. I cannot wait to see the lawyers for Tennessee have to argue
both of those. So the Department of Justice is basically saying people have
a right to make their health care decisions with their family, with their
doctors, and that specifically targeting this particular demographic is a
violation of the US Constitution by not granting them the same equal
protections, the equal rights that cis people have. It's going to be really hard
for Tennessee to argue against this. They're going to have a hard time
saying that's not the case. Now in Missouri it's a little different. You
have a judge there, a state-level judge, Missouri judge, who has put a temporary
hold on the enforcement of the law while they kind of get more information and
try to... I think the person is trying to educate themselves on the topic and they
are expected to issue a determination on whether or not they're going to provide
a temporary restraining order stopping enforcement of the law just completely
until it goes through legislation or until it goes through litigation, and
they'll let it play out from there. That determination is supposed to occur on
Monday by 5 p.m. So you are starting to see the completely predictable
pushback that is going to come from the ACLU, from the federal government, from a
whole bunch of different places to these laws that many people who fashion
themselves constitutionalists and those who really love we the people and have
that as their profile picture support it. And the main grounds is that it violates
the document that they pretend that they've read. And again, really early in
all of this, but the framing that is being used by the Department of Justice,
yeah, I mean it's gonna be really hard to say that DOJ is wrong. So that's where
we're at. I know that there are a whole lot of people genuinely worried about
this and a whole lot of people who are concerned for their friends. This is, this
is a good sign. They're not waiting for these laws to take effect. I think the
one in Tennessee, I didn't think it was supposed to go into effect until July,
but they've already started the process. So hopefully help is on the way. Anyway,
It's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}