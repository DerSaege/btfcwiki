---
title: The Roads Not Taken EP1
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=P8IIL-5ACj8) |
| Published | 2023/08/20 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing the new format of "The Road's Not Taken," where they recap news that didn't make it into videos on the main channel.
- Team discovered an abundance of topics on Beau's whiteboard, leading to this recap episode.
- US economy improving, but polling shows disapproval of Biden's handling of it.
- Minnesota sending out income tax rebates due to surplus.
- Controversy in Kansas over police searches related to a newspaper.
- Civil rights investigation expected regarding the questionable searches.
- Documents released regarding alleged misdeeds of Texas Attorney General Ken Paxton.
- Georgia investigating threats against the grand jury in the Trump case.
- Federal judge criticizing Trump's efforts to delay defamation suit.
- Former FBI official pleads guilty to working for a sanctioned Russian oligarch.
- Department of Justice preparing sentencing for Proud Boys convicted of seditious conspiracy.
- Environmental study linking health problems to living near natural gas wells.
- New gray wolf pack found in California, bringing hope.
- Odd news includes a pig kidney transplant and Britney Spears' divorce.

### Quotes

- "The U.S. economy is built on faith and perceptions."
- "There was a lot of concern about it having a chilling effect on journalism."
- "I don't see this as a huge threat."
- "The fall of the ruble is showing that the sanctions are working."
- "Many of the stuff that he's facing, many of the charges he's facing, they're zone D offenses."

### Oneliner

Beau recaps news not covered in main channel videos, from economic disapproval to controversial police searches and international developments, with insights on various topics.

### Audience

News enthusiasts

### On-the-ground actions from transcript

- Contact local representatives to voice concerns about controversial police searches (implied).
- Stay informed and advocate for environmental regulations near natural gas wells (implied).

### Whats missing in summary

Insights on the potential implications and consequences of the various news items discussed.

### Tags

#NewsRecap #CurrentEvents #Economy #PoliceSearches #InternationalRelations


## Transcript
Well, howdy there, internet people.
Let's bow again.
So welcome to the first episode of The Road's Not Taken.
We're trying out a new format today,
and you'll get to see it.
Feel free to drop any suggestions, comments,
complaints, concerns, down below.
The basic premise of this.
It kind of stemmed from about a week ago, maybe two weeks ago.
Pretty much the entire team was here for, I don't know,
four or five days in a row.
I've talked before about how I'm very fiercely independent
when it comes to content.
Most of the team had never actually seen
the giant whiteboard I used to like board up topics
and do research and all of that stuff.
They realized there was way more topics on the board
than there were spots in a week.
They asked what happened to the rest of them.
And when I said nothing, they suggested this.
So what you're getting is basically a recap of news that did not get turned into videos
on the main channel, but it's still something that you're probably going to see or hear
about again.
In fact, I would imagine that at least a couple of these topics will end up being videos next
week and be a little bit more in depth.
I think at the end of this episode, we're also going to do a quick Q&A with some questions
it came in. All right, so we will start off with U.S. news. First, while the U.S. economy is
improving in a whole bunch of ways, polling shows that people still disapprove of Biden's handling
of the economy. This goes to what we have talked about in the past. The U.S. economy is built on
faith and perceptions of that economy impact everything else. It's also worth
noting that while the economic data is improving a lot of it hasn't a lot of it
really hasn't been felt by the people who are likely answering the polling.
it's going to take a couple more months for that. So that may change. Minnesota
is going to be sending out income tax rebates to about 2 million people
because they had a giant budget surplus. I think some of it is paper checks and
some of it's just showing up in your account. There were some cops in Kansas.
They conducted a search, multiple searches I guess, related to a newspaper
out there. This got a lot of coverage. There were some computers and phones
that were taken. Let's just say that the search itself was questionable, legally
speaking. I fully, fully expect a civil rights investigation on this one. There
There was a lot of concern about it having a chilling effect on journalism, and that's
a legitimate concern.
I don't think that story is anywhere near the end of its tale.
Those who are overseeing the impeachment of the Attorney General in Texas, Ken Paxton,
They released 4,000 pages of documents related to his alleged misdeeds.
I have not gone through them all yet.
That I would expect, I would imagine that's going to be a video next week.
Georgia is investigating the threats against the grand jury in the Trump case.
There's an active investigation going on with that.
Trump news here, the federal judge that is overseeing the defamation suit in New
York has kind of run out of patience, it seems, and has openly kind of criticized
Trump's, quote, repeated efforts to delay. They're probably not going to be
successful. See a former high-ranking FBI counterintelligence official has
pleaded guilty to working for a sanctioned Russian oligarch. We talked
about this when it first happened, when the arrests were made. As more
information came out, it's bad but it's not as bad as it could have been, one of
those things. People in counterintelligence positions, they have a whole lot of information.
Based on what we have seen so far, he didn't disclose anything he wasn't supposed to,
not to any major degree. There wasn't a lot of stuff that got out into the wild. It looks
like he really did opposition research and tried to get the oligarch off the sanctions
Don't get me wrong, this is all still illegal, but this is one of those things that it could have been a whole lot
worse.
More DOJ news. The Department of Justice is setting up for the sentencing of the Proud Boys who were convicted of
seditious conspiracy.  And I guess not all of them were convicted of that, so those who were convicted recently
due to their activities related to January 6th, their sentencing recommendations are
lengthy, decades long in some cases.
It's also worth noting that apparently one of them is on the run, decided that they weren't
going to, they didn't want to hear what the sentence was. I would imagine that
the Marshals will probably find him pretty quickly. Okay, the New York Times
is reportedly looking into a lawsuit against, what is it, Open AI I think is the
name of the company? Fact check that. The the AI company that is producing all of
these tools. Now according to the reporting it looks like the New York
Times is viewing it not as a tool but as a direct competitor and they're reviewing
their options. I'm going to say that I think news agencies are looking at this
all wrong. The AI is not really that intelligent. It is scraping data that
exists. If it is scraping data that exists, that means your story was out
first. Your story was out first and it's going to have your information. I think
this may be a case where news agencies are seeing a tool that isn't really
going to be super helpful or they are seeing a competitor that is not really
going to be much of a competitor because it can only produce content based on
information that was put out by human sources. I don't see this as a
huge threat. Hawaii's governor has pledged to keep local land in
local people's hands. I have questions about how that's going to happen and not
that it's not a bad idea. I'm just wondering what beyond a slogan is going
to occur. Let's see, Aldi looks like they're buying 400 Windixis and Harveys
across the southern United States. I don't really have any like poignant
thought about that, I just found it interesting. Okay, so we're gonna move on
to foreign policy now. We talked about how the ruble took a dive. It has
lost around a third of its value since the beginning of the year. Russia's
central bank made a massive interest rate hike. They are trying to stabilize
it. They're trying to kind of beef up the ruble because it is incredibly
important. It is viewed by Russians as an overall economic health indicator. So
they're gonna put a lot of effort into this. They're still trying to cook the
books. This, the fall of the ruble, is showing that the sanctions are working.
It's showing that Russia is facing challenges, but this does not indicate an
immediate demise of the Russian economy, which is kind of how it's being played
up in some places. But I've noticed, since this note was put in here to talk
about. I've noticed that the reporting's got a lot better. Okay, Poland is
deploying thousands of troops to its border with Belarus. It's a deterrent,
nothing to panic about. Around 300 Russian Orthodox Church priests signed
on to an open letter calling for peace in Ukraine. There were repercussions to
to that statement. Some have been relieved of their duties, some have been defrocked.
It will probably be an ongoing thing, and it is showing not just a split in the church itself,
but among Russian people. It's more of an indicator of something more to come.
Researchers in the Netherlands have reportedly developed a drone that can stay aloft a very,
very long time using very little energy by mimicking what birds do when it comes
to the winds. This is still early on but this is going to have military
applications later. You will see this used in that way just so nobody has an
Oppenheimer moment later. This is exactly what militaries want when they're
are looking for a drone that can loiter.
OK, so a US troop crossed over into North Korea.
Very low-level troop, as far as rank,
didn't have access to a lot of sensitive information.
So the North Koreans are likely going
to use him for propaganda purposes.
And there's already some statements coming out.
and they're being cast as total propaganda.
They're probably not.
The North Koreans are saying that his reason
for coming to North Korea and fleeing
was inequality and racism.
I mean, the soldiers black, I am willing to bet
that during the conversation,
there were things that were said about inequality
in the United States and racism.
So is it being capitalized on by the North Koreans?
Probably, but I don't think that this is something
they've like manufactured out of whole cloth.
I believe this was actually said.
So satellite imagery appears to show China
constructing an airfield on a disputed island.
This is an island that is claimed by China,
Taiwan, and Vietnam.
It might cause some tensions.
I don't see this becoming a flashpoint of any kind.
The UN Special Envoy says that Afghan officials
should be prosecuted internationally
due to denying women, education, and work opportunities.
Iran's foreign minister made a trip to Saudi Arabia.
Not really a big deal, except it shows another step
towards that multipolar Middle East
with strong regional players that Biden wanted to create.
It's worth noting that the Chinese government actually
did a lot of the work to make this happen.
The idea, and this will probably turn into a video too,
but short form, the idea here is to create a Middle East that
is kind of balanced with Iran, Saudi Arabia, and Israel
the main players. Let's see. The US approved F-16s for Ukraine. The Netherlands
wanted to provide them. They had to get US approval because it's a US aircraft.
The US provided that approval so some will be going to Ukraine. They will not
get there. It is not going to have an immediate impact on the conflict like
any time soon. The fact that this is happening shows that Western powers are
still looking at this as a conflict that is going to be going on for quite some
time. Okay, some environmental news. A study out of the University of Pittsburgh
suggests that living near natural gas wells causes increased health problems
basically. Children are reportedly more likely to develop a rare kind of cancer
and basically everybody has an increased chance of asthma. In good news, there is
a new gray wolf pack in California. One was found and identified. So that is good
news. As far as just some odd things that hit the news cycle, a kidney from a pig
was transplanted into a human and it has been working for more than a month
which is kind of opening the door to more research on using animal organs in
people and Britney Spears is getting divorced. Again I have no idea why that's
on there like that's it. I found it interesting thought maybe it could be
used for now an analogy or something. Okay so now on to the Q&A portion. These
are from y'all. Okay earlier in the year I went through a blizzard here in
California and that was quite the ordeal. This week we have our first I
think ever hurricane making landfall this Sunday. Can you give us some advice
please? Even the basics especially since this will be our first but probably not
or less.
OK, so I have an entire playlist on the main channel
about emergency preparedness.
I also, on this channel, I have a number
of videos that talk about how to deal with emergencies like this.
I would imagine this video came in before I
gave the little tips.
One thing that I think is really important out there
due to the risk of flooding, remember if you're going into your attic, take an axe, take something
to get through the roof.
Could the DA in Felton be dismissed under this law, and if so, do you think it's likely?
So there is a law that would allow partisan officials to step in and kind of deal with
DAs they don't like.
Is it possible that they use it?
Sure.
Is it likely?
I actually don't think so.
I think there are enough people in the Republican Party now who want Trump gone that they're
not going to.
Even if that was to happen, it wouldn't do anything to the case.
They may be able to do something about the DA, but the grand jury's already returned
that indictment.
Anything that impeded it would cause serious issues for the political figures behind it.
I've caught a few of your videos recently about the idea of the US being the world's
EMT instead of the world's policeman.
How if at all would the Peace Corps fit into that?
I'm from Canada and don't know much about them.
So the Peace Corps has the same general idea, but it's not as big and it doesn't have teeth.
If you want this explained in geopolitical terms, on YouTube there is a TED Talk from
a guy named Thomas Barnett.
Please understand, he is somebody who worked for the Pentagon.
His presentation of this idea is very much from a military point of view and it kind
of stresses all of that.
But if you listen to it and read between the lines, you'll get a real clear picture about
how it can be used not just during a reconstruction after a war, which is what he's primarily
focused on. But also being used to prevent the war from happening. Let's see. I'm not
asking a question here. I'm just writing to ask if you do another video on the West Coast
hurricane. Can you please mention the part about not being in somebody's nightmares?
Yeah, make sure you take an axe up into an attic.
You got to have a way out if you're going to go up there during the flooding.
Okay, Bo, I don't understand how there could be so much confusion about if Trump is convicted
how they will handle his incarceration.
We can fly around the world destroying cities with a bomb and land a man on the moon, land
rover on Mars, create stealth planes, invent technologies that allow instant communication
with anyone on the planet, yet we can't figure out what to do with an ex-president.
There are a whole bunch of options, and it really depends, at this point, it's going
to heavily depend on, obviously, if he is convicted, but what jurisdiction he is convicted
in, whether it is state or federal, and what the guidelines are for that particular charge.
Many of the stuff that he's facing, many of the charges he's facing, they are, they're
zone D offenses, which means he's going to be confined.
Now will he get special treatment because he's a former president?
Certainly, because as many Republicans have been saying, we definitely have a two-tier justice system, it's just not the
way they think.
Why do you think so many Republicans believe that it is more honorable to be a loyal Republican than it is to be a
decent American?  For those who actually hold this opinion, it's because they don't believe you're an American.
believe you're an American. Those that hold this type of opinion and are this far down that echo
chamber, it's a love it or leave it thing and you don't love this country so your opinion doesn't
matter. You're not really an American and it's one of the signs that that faction of the Republican
party is actually really headed towards unauthoritarian creep that is kind of too much and is dangerous.
Hello Mr. Bo, you forgot to mention the eye of the hurricane and that more winds are coming
after it passes.
Yeah, I didn't even think about that.
Man, I hope that people understand that.
So if you're ever in a hurricane and you haven't been in one before, there's a break.
You can just be battered by winds and rain for hours and then it clears, but you're in
the eye and then you get battered for hours again with winds and rain.
Now, hopefully, the storm continues to break up.
At time of filming last time I checked,
it was already down to a Category 1, which, again,
I don't want to downplay it, but it's manageable.
I'm really starting to think the real risk here
is the rain and the floods that are likely to occur.
This is an interesting thing.
So there is apparently somebody called Ryan Hall
here on YouTube.
And I guess the name of the channel is Ryan Hall Y'all.
I have not followed them very closely.
But it's a channel that a lot of people
who I know from doing disaster relief really follow
and put a lot of stock in what is said.
And my understanding, and it looks
like this is mentioned in this message, which
is kind of a long one, is that they also, they don't just
say, hey, bad weather's coming.
Get ready.
They also help afterward, which is always something
that I kind of view as a bonus when
it comes to channels on YouTube.
Okay, looking for your thoughts on a couple of scenarios.
Trump doesn't get the Republican nomination for president and runs as an independent instead.
Totally think it's possible.
If he doesn't get the nomination, I do believe he will continue to run because his whole
framing right now is that this is to stop him from running.
So he has to continue to run no matter what.
So if he doesn't get the nomination, he's going to run third party.
I am fairly certain of that.
The other scenario is Trump wins the Republican nomination for president, but there's also
a viable independent third party candidate.
I don't know.
Like when it comes to the third parties, I don't see them having a lot of likelihood
of success.
I think I would need a definition as to what viable is.
Viable to carry a message, that's what most third party candidates are good for, is to
get a specific message out.
As far as winning, I just don't see that.
Now, who they take more votes from, I don't know.
I know that the Republican Party, or at least, let me say this correctly, I know that people
who are typically supporters of the Republican Party are supporting some third party candidates.
And I think that there is a pretty wide belief that they're doing so because they think
They think it will hurt Biden.
I'm not so sure that that's how it's going to play out.
If Trump is the nominee, there may be a lot of disaffected Republicans that may vote for
that third party.
Will the Republican Party throw Mitch McConnell under the bus because of his age as a point
of attack for Biden's age?
I don't think so.
Generally speaking, the Republican Party does not care about hypocrisy, so I don't think
they'll care. Okay, what can the average citizen do to contribute to whatever
process needs to be done to disqualify Mr. Trump for the presidency? I don't
think there is anything you can do. That's gonna be made at a different
level. Can you talk about Biden? No, that's a video, so that one's not getting
answered. Okay, so that's it for the day. Really haven't figured out a sign off for
these yet. So anyway, it's just a thought. Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}