---
title: The roads to Idalia recovery and the mythical orange buckets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=WXq2l0lrJ8g) |
| Published | 2023/08/31 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- References magical orange buckets used for relief after Hurricane Michael.
- Illustrates the importance of community network involvement.
- Describes two different versions of relief buckets.
- Details the contents of the buckets, including cleaning supplies.
- Explains the significance of these supplies for people affected by disasters.
- Emphasizes the value of providing simple items like cleaning supplies and toys for children.
- Notes that small toys can be beneficial to keep kids occupied during recovery.
- Mentions the efficiency of National Guard in providing food and water.
- Encourages sending items like cleaning supplies and toys for children to aid recovery efforts.
- Acknowledges the challenges faced by a Florida community unaccustomed to hurricanes.

### Quotes

- "It meant the world to the people who got it."
- "Even though it may not seem like a lot, it gave them the ability to try to move forward."
- "You're probably talking about a bucket with, I don't know, $40, maybe $50 worth of stuff."
- "This is what I recommend."
- "If you are looking for something to do to help out, this is what I recommend."

### Oneliner

Beau illustrates the impact of simple relief supplies on disaster-affected communities, stressing the significance of community support and basic items for recovery efforts in unaccustomed regions like Florida.

### Audience

Community members, aid organizations

### On-the-ground actions from transcript

- Prepare and send relief buckets with cleaning supplies and toys for children (suggested).
- Support recovery efforts by providing basic items like cleaning supplies and toys (suggested).
- Coordinate with organizations to send aid to disaster-affected areas (implied).

### Whats missing in summary

The full transcript provides a detailed account of the contents and significance of relief buckets, showcasing the impact of basic supplies on disaster recovery efforts.

### Tags

#DisasterRelief #CommunitySupport #RecoveryEfforts #HurricaneMichael #CommunityAid


## Transcript
Well, howdy there, internet people, it's Beau again.
So today is definitely going to be a little bit different.
For years on the channel, I have referenced
these magical, mythical orange buckets
that we handed out when we were doing relief after Michael.
I used them to illustrate a couple of different things.
One is even if you are somebody
who doesn't quote people well,
you can still be involved with a community network
because these buckets, they got packed up somewhere, right?
And then they got delivered to us
and then we delivered them to the people that needed them.
The other thing I have used it to illustrate
is how you don't always know what people need
because when we were going around after Michael,
everywhere we went, people were asking if we had them.
I used this as an illustration a number of times.
The last time I talked about it, I had a whole bunch of people ask specifically what was
in them.
I couldn't remember.
I knew it was like cleaning stuff, but I really didn't have a good memory of exactly what
was in them.
So I put out a call to see if anybody had any left.
have two which is good because come to find out something I didn't know was
that there were two different types of bucket. So if you are somebody who packs
relief supplies and you are preparing multiple versions of something and they
come in the same package make sure that you you label them because the people
who distribute them they're probably not going to open them. I handed out I don't
know dozens upon dozens of these buckets. Never had opened them, so I had no clue that there were
two different types. But we actually have two of them. So they came like this and inside
the first version of the bucket, N95s. Again, this is for recovery, cleanup and then a bunch
of rags. Just cheap rags. Remember after Hurricane everything's wet. If your
place was really damaged, it got flooded, everything's wet and you can't wash
anything because the power's out. You can't wash anything. Clothesline.
I've had gloves, clothespins, just want you to know this just smells like, you know, a
bunch of chemicals have been in a bucket for years, work gloves, scrubbies, trash bags
with some kind of chemical all over them.
Okay, it's just baking soda.
Sponges, brushes, your dishwasher doesn't work when your power's out.
And then Mr. Clean, and then there's some more sponges, and what is that?
Oh, insect repellent.
one version. Again, most of this, aside from insect repellent, probably not something you would think
would really be sought after after an emergency, but people have to clean up their places when
it's all said and done, right? So that was one version. The other one
had still scrubbies, clothesline, bunch of rags, sponges, brushes, that looks like
dish washing gloves, a couple of sets of those, trash bags, Lysol all-purpose
cleaner, looks like the bag busted, clothespins, work gloves, the other half
of the clothespins, dish washing detergent, insect repellent, laundry
detergent, more trash bags, and a whole bunch of sponges down at the bottom.
It's cleaning supplies. That's it. The clothesline and the clothespins were
probably what one of the things that people were like, yeah we really need
that right now. But the cleaning supplies themselves, if the place just got a
little bit of water damage, if a little bit of water came in and people were
trying to salvage their place. It's not like the stores were open. You couldn't go out and get
the scrubbies or any cleansers really. So this, it came in incredibly handy. Everywhere that we went
people were asking for this. I didn't know that there was like a heavy cleanup, which is what I'm
I'm guessing the first one is with the masks, and then the lighter one.
But this, these materials, this same type of stuff, after Michael, everybody wanted
it.
I am certain that after I Delia, people are going to want it.
In fact, we will pack these back up and we'll take them over there because I don't think
any of this stuff goes bad.
We'll check C. But this illustrates those points.
When you really look at this stuff, it's nothing major.
You're probably talking about a bucket with, I don't know, $40, maybe $50 worth of stuff.
It meant the world to the people who got it.
not just is it the monetary value, it lets them get started. You know, after one
of these things, people are sitting around waiting. They're waiting for the
National Guard. They're waiting for FEMA to get them a check or to tell them
where to go. They're waiting for local responders to even get to them.
They're waiting for the electric company to get power restored or they're
waiting to get their road cleared. There's a lot of just downtime and it's
not like they're going to work, you know. So this gives them something to do and
it allows them to affect self-help. Even though it may not seem like a lot, it
It gave them the ability to try to move forward, to start the actual recovery process instead
of just waiting around.
These things, I don't know, none of them are labeled.
It looks like there was a sticker on this one of where it might have come from, but
I don't know who made them.
But I know somewhere out of Tennessee or Kentucky, and they sent them down by a semi-truck full.
But the people that were impacted by this...
This is a part of Florida that is not accustomed to hurricanes.
They may have a harder time than normal kind of getting back together because they haven't
done it before.
So I am certain that this type of stuff would come in handy.
And the other thing to think about is small like kids toys, like think Dollar General.
There's a whole bunch of kids, young kids, with nothing to do, no electronics, nothing
to keep them occupied.
Those little packs of toys that like have like Play-Doh and stuff like that, just something
to keep them occupied.
parents are already as stressed as they can possibly be. So if you are looking to send
stuff down, realistically, the National Guard is going to take care of the food, the water,
stuff like that. In Florida, they're really good at that. I've been in other states where
they're not quite as on the ball, but my guess is before this video is actually seen by anybody,
they already have all that stuff up and running. So while replenishing people's pantries as time
goes on, it's a worthwhile goal. It's not an immediate need because the National Guard's
going to take care of it. This stuff, or stuff to keep young kids occupied, you're going
to save a lot of people, a lot of idle time just sitting there waiting, looking around,
thinking about everything that they've lost. So, if you're looking to do something and
get it down this is this is what I would recommend and I feel like I should put
it out kind of one of those things where I was asking for this for a video and
they showed up right before people would need to know how to make them. So this is
what was in it. I don't think anybody would care if it wasn't brand name
stuff like if it came from Dollar General or Dollar Tree or something like
that. I don't think they'd mind but my guess is it's gonna be a tougher time
than normal to get the recovery part of this underway. The relief it's gonna get
going. The recovery may take a little bit longer because they're just not accustomed
to it there. This is a part of Florida that just does not get hit. It's in this
weird little protected area, and that protection didn't hold this time. So, if
you are looking for something to do, if you, your organization, somewhere up north,
you're looking for something to do to help out, this is what I would recommend.
Anyway, it's just a thought.
Y'all have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}