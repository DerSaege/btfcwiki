---
title: The Roads Not Taken EP2
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=Y8cHKZcB2-w) |
| Published | 2023/08/27 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing "The Road's Not Taken," a series diving into underreported topics with future relevance.
- BRICS extends invitations, potentially rivaling the G7.
- Wagner troops respond to boss's fall, while Putin demands loyalty.
- Speculation on Russia's potential charges against Finland.
- Tension over North Korea's failed satellite launch.
- U.S., Australia, Filipino forces drills in the South China Sea signal to China.
- Updates on Trump-related hearings and motions.
- Biden administration allocates $700 million for rural high-speed internet.
- House Judiciary Committee to probe Fulton DA, sparking outrage.
- Wisconsin Republicans request Justice Protasewicz recusal in redistricting case.
- Georgia Governor Kemp urged to support Fulton DA against Republicans.
- Recap of incident involving Mr. Rose and police dog, with new developments.
- DC Attorney General investigates Leonard Leo's network.
- New York plans to build world's tallest jail in Chinatown amid opposition.
- Environmental news on cargo ship with steel sails, forest issues, Panama Canal drought.

### Quotes

- "BRICS extends invitations, potentially rivaling the G7."
- "Tension over North Korea's failed satellite launch."
- "Wisconsin Republicans request Justice recusal in redistricting case."
- "DC AG investigates Leonard Leo's network."
- "New York plans world's tallest jail in Chinatown."

### Oneliner

Beau dives into underreported global and US news, Trump-related hearings, rural internet funding, and environmental updates, urging vigilance on rising authoritarianism.

### Audience

Citizens, activists, analysts.

### On-the-ground actions from transcript

- Contact local representatives or organizations to stay informed and engaged with global and US news (implied).
- Support initiatives for rural high-speed internet access in your community (implied).
- Stay alert to authoritarian trends and push back against divisive politics (implied).

### What's missing in summary

Insights on the importance of remaining vigilant against authoritarianism and the need for electoral defeat of divisive politics for long-term stability.

### Tags

#UnderreportedTopics #BRICS #GlobalNews #TrumpHearings #RuralInternet #EnvironmentalNews #Authoritarianism #CommunityEngagement


## Transcript
Well, howdy there, internet people, it's Beau again.
So welcome to the second episode of The Road's Not Taken,
a weekly series where we will be diving into topics
that didn't get covered on the main channel,
but are probably going to be relevant in the future.
So we'll just kind of run through.
And basically we're hitting highlights of news
that's a little bit under-reported
that will probably come back
and you'll end up seeing that material again.
And then we will close out with a few questions in a Q&A.
OK, so starting off, foreign policy news.
BRICS, which is a Chinese-led economic bloc,
they met and extended invitations to other countries.
Originally, the idea behind this,
it seemed to be kind of trying to create something that
would compete with the G7.
Now later, this topic and BRICS in general
might become important.
Right now, they're having a hard time getting their act together.
I know there's a lot of speculation about it.
And I know there's the idea of moving away from the dollar
and all of that stuff.
In response to those questions, I
would simply ask, which currency would they want to move to?
I know that that's a big topic, but at this time, it just,
they don't seem to be the competitor that a lot of people
seem to think they are.
Okay, now, so Wagner troops are currently pledging a response to their boss's fall from grace,
and we'll kind of have to wait and see how that plays out. Meanwhile, at the same time, Putin is
demanding that they swear loyalty to him, or to Russia. I mean, you know, because it deals
with Putin, they always work out.
In a kind of related note, there's a lot of unconfirmed chatter that Russia might trump
up some charges on somebody from Finland in an attempt to get some leverage when it comes
to a possible prisoner exchange.
So that's something to watch for.
But again, at this time, that's pretty unconfirmed.
There is a lot of international tension over a failed satellite launch in North Korea.
Some countries are mad because it's kind of using tech that's banned.
North Korea is saying that they have a sovereign right to launch a satellite, if they can.
My understanding is they will try again in November.
You'll probably see this kind of start to spin up between now and then.
Try again in October or November.
The United States, Australia, and Filipino forces conducted some drills.
They were kind of practicing how to retake an island along the South China Sea.
Normally when the U.S. military is conducting these drills, my standard thing is these are
super common, don't worry about this, this is very, very normal.
This is still something that you don't need to worry about, but it isn't entirely normal.
There definitely seems to be an element of signaling going on here, probably directed
at China, just a little bit of, hey, we still know how to take an island back.
Lots of posturing, it's Cold War stuff.
Okay, on to U.S. news.
So Monday is going to be a big day for hearings in the world of Trump.
There's going to be a hearing in Navarro's Contempt of Congress case.
There will be a trial date hearing on Trump's D.C. case and a hearing for Meadows on moving
his Fulton County case and having that removed to federal court.
I feel like there might be some surprises in the Meadows case, particularly who might
be called to testify.
Okay, I don't think anybody missed this, but Trump allies, they all surrendered in Fulton.
An interesting development is that motions have been filed, a bunch of them, some for
speedy trials, some for removal to federal court, and some are about severing trials.
You're going to see a lot of speculation about this all week.
The Biden administration announced nearly $700 million to bring people in rural areas
like me and get them high-speed internet.
That would be super cool.
The House Judiciary Committee will reportedly launch a probe called for by Republicans into
the Fulton DA.
We've seen a bunch of these committees.
It's an outrage farm.
That's all it is.
I don't expect much to come of this other than a bunch of social media engagement, people
People angry and probably some claims being taken seriously when they probably shouldn't.
Team Trump was just ruthlessly mocked for reporting impressions as views when it came
to the Tucker interview.
If you missed this, the Republican candidates for the nomination for president, they had
their debate.
because he totally didn't need to be there, and wasn't worried about it at all.
He engaged in a little bit of counter-programming.
He did this interview with Tucker Carlson.
When it went out, it went out on Twitter,
and the reporting shows them counting impressions as views.
A view means you watched it, right?
An impression is, let's say you're scrolling along on Twitter and you catch two seconds
here because it auto plays and you just scroll right past it and you do that four times,
that's four views.
It grossly inflated the numbers on that.
Wisconsin Republicans are asking Justice Protasewicz to not hear a redistricting case.
The Justice just got elected recently.
Wisconsin is consistently rated as an extremely gerrymandered state.
They're asking her to not be involved in this case.
We'll see how that plays out.
probably be a video about that later. Let's see, Georgia Governor Kemp is facing a whole lot of
pressure to publicly tell Republicans to back off the Fulton DA. It's probably a really good idea
for him to do that. If he doesn't, there's a pretty high likelihood that the Republican Party
accidentally, completely undermines him.
Recently, on the main channel, we covered the incident involving Mr. Rose, who was a truck driver who had a police dog
let loose on him by a local cop who is now no longer with the department.
And that happened while state troopers, the state police, were basically saying don't let that dog loose.
In the immediate aftermath of that, Rose, the truck driver, was charged with a felony.
The prosecution requested that it be dismissed.
There is a possibility that it comes back with like a misdemeanor charge, like a low-level one.
but we don't we don't know yet. It's also worth noting that some additional body
camera footage came out and the cop who released the dog is asking somebody that
he works with I guess why the state troopers are so mad at him and he said
quote, he wasn't complying. I mean, am I wrong? I have a feeling like if this ends
up in a courtroom that phrase is going to be... it's gonna get a lot of focus. Okay,
the DC Attorney General, so the DC, not the Attorney General of the United States
it's in DC, but the DC attorney general,
appears to be looking into Leonard Leo's network.
Very prominent conservative.
I haven't dug into this a whole lot yet.
One, I feel like there's time.
It looks like this is the beginning.
And two, if there is any major news that comes out of this,
I feel like it'll be big news and it'll probably be everywhere.
This is an extremely influential person.
I also feel like the odds are not much is going to come of it
because he doesn't strike me as the type to leave himself
legally exposed.
So that's something that's just now starting to wind up.
don't really know a whole lot about it yet.
New York wants to build the world's tallest jail in Chinatown.
Obviously, there's going to be a lot of opposition to this.
Don't know how it's going to shape up at the end.
There's going to be a lot of vested interests who want this,
even if the community does not.
It is pretty fitting that the United States
is going to have the world's tallest jail.
That's quite an indictment right there.
On to environmental news.
A large cargo ship has started its maiden voyage.
What makes this noteworthy?
It's using giant steel sails for power.
It's an attempt to make shipping a little greener.
Let's see.
Part of some of the tropical forests in South America,
and maybe in Southeast Asia, the reporting's
a little conflicting on that, are the trees
are having issues with photosynthesis because it's,
well, because of the heat.
That seems noteworthy.
That seems like something that we'll probably
have to look into.
That doesn't seem like a good thing.
Drought is causing issues for the Panama Canal.
That means that if that persists,
there's going to be supply chain issues, which
will disrupt the holiday shopping season coming up.
This will also probably end up getting a video
the main channel. Oh and there might be some hurricanes interrupting production here this
week and I will keep you updated on that. As far as some odd bits of news Yosemite is asking people
to knock over rock towers or more specifically don't build them in the first place. If you don't
know the towers are used for, like, navigation for hikers.
The problem is they also damage the ecosystem, particularly if the rocks are taken from rivers
or, well, anywhere that's moist.
Hikers will come along and they'll stack them up, and it basically gets used as a waypoint.
But they're asking people to not do that, and if you see it, to knock them over and
leave no trace.
And in what might be the strangest news, the largest monster hunt in recent memory has
started.
Scotland invited amateur monster hunters from pretty much everywhere to bring their
drones and their cameras and attempt to find the Loch Ness Monster.
That's probably going to be a reality show, I would guess.
Okay, so moving on to the Q&A here.
So if you don't know, if you haven't seen this before, these are questions picked by
the team.
I have not read them yet, so you will get an off-the-cuff answer.
Oh, what's up with the Destro figure on the shelves?
It doesn't seem to relate to the videos the way other Easter eggs on the shelves do.
Ah, yeah, so, I don't know if you can see it, yeah, up there by the coffee cup, there
is a small G.I.
Joe figure, a character named Destro.
My dogs are famously named after G.I. Joe characters, Baroness, Destro, and Zanya.
Destro is no longer with us.
That's for me.
That's just something I put up there.
He got cancer, and we did what we could.
in the end he was already kind of old and just didn't didn't work okay you say
you don't sleep much while you're awake how do you avoid feeling overwhelmed
with too much information hobbies or projects how do you balance life with
your interests. I think it's funny you think I have balance. Now most of my
hobbies and projects and everything they they intersect. I try to incorporate
everything together that helps me. When it comes to things like work-life balance
and that type of stuff don't take my advice. I am NOT the right person to ask
ask about that. I know where my shortcomings are and that's definitely
one of them. I very, when it comes to you know my interests and work in general, I
would like to say that I'm dedicated. Some might call it obsessive, like
overwhelmed with too much information. That's not a thing in my
world. Yeah, I'm definitely not the right person to ask about this. Generally
speaking, especially if you do have a similar personality type, set aside time
for important things. You know, set aside time so you have time every day with
your family and stuff like that. And then try to blend everything else together.
That, that to me has always been a challenge.
I knew, I knew you've got black friends when you used shook instead of scared.
You use black words in your videos a lot, I'm wondering what your favorite black phrase
is and how you use it. Yeah, I do. Okay. My favorite black phrase, caucasity. The
word caucasity. How do I use it to signify super white? Could you use the
word in a sentence please. Yeah I do have black friends I also have a lot of
friends that are white like white white you know like two finger guns saying
buddy kind of white. That sometimes leads to some very interesting dynamics which
is why this word I like using it. A good example recently a whole bunch of us
were sitting around and we were talking about the whole Tupac search and all of that stuff.
And some of the people there in this conversation, they were listening, they were interested,
but they didn't know any of the lead up.
So people are trying to explain that to them.
And at one point, one of the people who was there was like, okay, so this this sugar knight
person. Did you just say sugar night? Oh the caucasity. That's probably my
favorite and sometimes it can also be used as a you really don't understand
the amount of privilege you have type of thing. You know there's a lot of
caucasity in that statement right there buddy that type of thing okay if a
Republican other than Trump wins the general election what are the chances
the MAGA Republicans work with him I have a feeling they'll be unruly and
resistant to anyone but Trump oh it's gonna be a glorious disaster if Trump
Trump doesn't get the nomination and like a mainstream Republican actually winds up
getting the nomination and then winning the general, it's going to just be a giant mess.
There are some candidates that aren't Trump that are close enough to MAGA or at least
would pretend to be to manipulate those people in MAGA that they would probably work with.
But if it's Pence or Christie or Hutchinson or something like that, they are going to
be very obstructionist and complain a lot at first.
Eventually they'll fall in line because they won't have a choice.
But in the beginning, it's going to be a mess worth watching.
Like popcorn worthy mess.
Okay.
Last one.
What do you think will happen to MAGA Republicans if Trump doesn't win the presidency?
The same thing that happened to Tea Party Republicans.
Who are they exactly?
If Trump or somebody who is like him, if all of the candidates who align that way, if they
all fail and they don't reach the executive branch. You will see an even
further and sharper decline of the MAGA factions. Please remember that a lot of
the MAGA faction, they don't actually believe what they say. So they will
either try to camouflage themselves, kind of take a low profile for a little
bit and try to rebrand as a mainstream Republican, or they will write a book,
start a podcast, something like that.
I don't see, there are people who can take up Trump's position, but they're not
Trump and I think that if that faction is defeated at the polls again, I think they
might start to realize it's not a winning electoral strategy and they might give it
up and it will slowly fade away the same way the Tea Party did.
But what's important to remember is that that energized base that was the Tea Party,
that's a big part of what gave us MAGA.
So this particular brand of authoritarianism that has a lot of risk, the U.S. is going
to have to be on guard for it for a pretty long time.
The time to defeat this in one, you know, big swoop would have been the last
election and Joe Biden winning in a landslide.
That's what, that's what needs to happen.
If you want to put that particular brand of authoritarianism
to rust, they have to be defeated at the polls in a big way.
It can't be close.
They have to know that they can't succeed electorally that way.
They have to understand that.
And by that, I'm not talking about the rank and file, I'm talking about the politicians.
The politicians, a whole lot of them at this point, they're just echoing back what they
think the rank-and-file want to hear.
That leads to kind of a feedback loop.
It's much easier to break that loop if you do it with the politician side.
If they don't have leaders in office, if they don't have people who can command the news
cycle, they're going to, they'll fade away, but they don't disappear. That hunger to
be ruled or to think that you're going to be one of the rulers, that's probably
something that the United States is going to have to, is going to have to
contend with for a while okay so that looks like it and I still haven't
figured out the sign-off for this I saw all the suggestions but for right now I
I think we're still going with it's just a thought y'all have a good day
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}