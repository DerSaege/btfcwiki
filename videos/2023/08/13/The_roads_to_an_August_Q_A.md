---
title: The roads to an August Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=G0j7hsfs8q8) |
| Published | 2023/08/13 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is considering featuring unreleased topics on his channel as the "roads not taken" to cover under-reported news that provides context to bigger news later.
- Beau appreciates Bailey Sarian and Stephanie Harlow's merchandise without needing an advertising fee.
- Maintaining a positive attitude is non-negotiable for Beau; he sees it as a form of commitment and self-help.
- Beau believes that some Republicans might move away from MAGA, while others may not, depending on their beliefs.
- Age might not be a significant factor in the 2024 presidential election, according to Beau, who focuses on the economy's impact.
- Beau clarifies his accent pronunciations in response to audience comments on specific words.
- Beau expresses concern over the implications of certain amendments in Ohio and upcoming videos on related issues.
- Beau shares thoughts on the cute fox videos in Ukraine, advising viewers to rethink the cuteness in context.
- Beau anticipates consequences for members of Congress who supported Trump and encourages nuanced thinking on political dynamics.
- Beau values reaching people with his content over channel size, focusing on impact rather than numbers.

### Quotes

- "Don't have a choice."
- "It doesn't do any good."
- "I think some will."

### Oneliner

Beau addresses various topics in a Q&A session, discussing under-reported news, maintaining positivity, political dynamics, and content impact over channel size.

### Audience

Content creators

### On-the-ground actions from transcript

- Reach out to Beau for advice on starting a podcast or YouTube channel (suggested).
- Watch Beau's videos on starting a YouTube channel for guidance (exemplified).

### Whats missing in summary

Insights into media analysis and political commentary from Beau's perspective can be best understood by watching the full transcript.

### Tags

#ContentCreation #PositiveAttitude #PoliticalAnalysis #UnderreportedNews #CommunityBuilding


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to do another Q&A,
but we also have a little bit of channel news
before we get into that.
Recently, we kind of had everybody here together
and one of the things that has happened
since we all started like having a designated place to work
is people have seen the whiteboard that I use
keep track of topics.
And it was pointed out that basically I will board up and research, I don't know, 60 topics
a week and like only 28 get turned into videos.
And I was asked about those that don't make it and don't actually get turned into videos
and what happens to them.
and I said that nothing becomes of them, somebody suggested putting those into basically a headline
real quick, this is stuff that happened, it's information you're going to see again, and
putting it on this channel as the road's not taken.
So you will probably see something like that soon, and it will just cover what is likely
to be under-reported news that will help provide context to bigger news later.
So just a heads up on that.
Okay, so getting into the questions.
Bailey Sarian and Stephanie Harlow should be giving you an advertising fee for their
merchandise.
If you don't know, a lot of the shirts that I wear frequently, like the one that says
allegedly, the one that says say it with me, the justice system is broken, suspish, nay
nay, I say all of those are from them.
I don't think they need to give me an advertising fee.
I just appreciate that they put out shirts that have been oh so useful very often lately
dealing with all of the legal entanglements that politicians are getting involved in.
Okay, your approach, you approach your stories with deliberation and critical thinking.
You're careful to not state your personal opinions on most things and that's so freaking important
for a journalist to do. When you do personalize your videos, your attitude is always a positive one
And I guess that's why I'm writing.
How do you maintain positivity?
Don't have a choice.
I mean, what's the alternative to not
maintain a positive attitude?
To me, that's just surrender.
It doesn't do any good.
It's a lot like the real definition of panic,
the inability to affect self-help.
If you get to where you can't maintain a positive attitude,
you fall into the same category.
You can't.
You are not fully committed if you
believe that there's no use.
So I just don't view it as an option.
I'm sure there's probably a psychologist out there writing
paper on that. It may not be the healthiest way to look at it but I
really don't see an alternative. Okay, will this carve Republicans away from
MAGA? And it's a link to the article about the Federalist Society members
arguing that Trump is disqualified. Some, like normal Republicans, yeah, it will
probably help them move away and come to a different view on things. The MAGA
Republicans, probably not. They're just gonna say that they're part of
There's some grand plot to get Trump.
How much do you think age will be a factor in the 2024 presidential election given Dianne
Feinstein's age and health problems and Mitch McConnell's recent brain freeze on camera?
I wonder if this will influence people's willingness to support Joe Biden.
I think Joe Biden is a really great president.
I think it's going to hinge on who his opposition is.
You have to remember, it's not like Trump is young, and there is still a high probability
that Trump wins the primary.
I don't know, and if the economy continues to improve, and people begin to perceive
Believe the improvements because you have to remember the data is one thing.
People filling it and believing that it's improving, that's something else.
If the economy starts to improve, I don't think it's going to matter.
I think the economy is going to play a huge part in the selection.
For the Hawaii video, I have to ask, do you have a shirt for every state?
I do not, but I am actively working on trying to get one.
In the YouTube video about the Montgomery docks, it sounds very much like you said hollowed
ground several times, which is what the captioning picked up.
The term is hollowed ground.
Yeah, that's just my accent.
Hallowed ground, I guess.
It hallowed.
Hallowed.
Yeah.
That's just my accent, I know it's an A, it just doesn't make that sound for me.
Had somebody telling me the same thing about the word program recently because I guess
I say program.
Okay, to put your message in perspective, 41% of people in Ohio wanted to be ruled.
Ouch.
Ah, that hurts.
If it required a 60% majority, 59% of the population would have had their will to be
represented taken away by the 41% that wanted to be ruled.
That's actually a really good point.
If that amendment, if that change was made in Ohio with issue one, the landslide that
we saw there wouldn't have been enough to get anything else passed.
That's how severe it is.
That's a really good point, and I wish I had seen this before making a recent video,
because there's another video coming out about that, and you are not going to believe
what's happening there now.
Can you talk about the cute fox videos in Ukraine?
I'm trying to decide whether or not I want to ruin this for people.
If you are unaware, there have been a bunch of videos featuring foxes.
A bunch.
I've seen three.
There may be more.
And they just have foxes that are like coming up to either Russian or Ukrainian troops and
just like hanging out.
And they are cute as long as you don't think about them.
If you have seen them and you would like to maintain the cuteness of those videos, cover
your ears for a few seconds until I look back down at the screen down here, until I look
away from the camera.
You need to think about why an animal that is an opportunistic eater is hanging around
the trenches.
It's not as cute when you put it in perspective.
Okay, now that I have ruined that for everybody.
Trump has been on the block for a while now relating to his allegedly criminal actions.
I'm going to edit that a little bit.
A network has been sued relating to Trump's lies.
Will the members of Congress who have encouraged him slash covered for him ever face consequences
other than those that are instigated by the voters?
I think some will.
I think some will, and I think we may actually see that relatively soon.
I do believe that there are some people in Congress who went beyond the protections afforded
to them by the speech and debate clause, and we may see that relatively quickly.
Do you think the result of all the back and forth with Trump will be your maxim at large,
can't win a primary without him, can't win the general with him?
Yeah, I do.
I do.
Barring something like really wild, that saying, it applies to him too.
talk about the Fitch Downgrade of the U.S. credit from AAA to AA plus.
Okay, I will, but that's going to be in a separate video.
When I get into economics, I like to be very, very deliberate in what I say and not just
come off the cuff with it.
So that will be a separate video over on the main channel.
I go to UT, and four or five of us who grew up without dads say you're our surrogate
dad.
And I doubt we're alone.
Can you maybe do a video with advice for 20-something women and their 20-somethings
with daddy issues?
By the way, we used to call you Daddy Bo, but that led to a misunderstanding, and I'm
not going to read any more of this.
OK, so I have a video titled, Let's Talk About the Advice I Give My Sons.
And it doesn't actually look like one of mine because it was filmed by a campfire,
and it actually kind of doesn't even sound like me because it's very different.
Watch that and watch it to the end, to the very end.
If there are any specific questions that you have, question for F-O-R-BO-B-E-A-U at Gmail.
Just send them over.
Given the number of Russian flags and slogans we see in the pro-coup protests, and given
in the timing of the coups happening right after the African leaders' delegation to
see Putin, but without jumping to conclusions, is it reasonable to suspect Wagner Group had
a direct role?
And what open source can be used to determine that?
I have a theory on this, so I'm going to pass on this.
I'm not going to answer this question.
What I will say is that if I thought you were completely off bass, I would say now.
I think it's a little bit more nuanced than that, but I don't think that what you're
saying here is completely out of the realm of possibility.
Let's see.
You're approaching 100k.
Will that play button mean as much to you as the first?
More?
Does channel size really matter to you?
Love your content.
Glad to be a supporter.
Yeah, this channel is getting close to that.
I don't know as far as the button itself.
I don't know about that.
size. Sometimes size does matter when it comes to reach. That's what matters to me. If I could
reach the same number of people with a subscriber base of three, I would be just as happy. But
the number of subscribers definitely impacts how many people you can reach. And that's important
to me. And as this channel eventually transitions into what it was intended to be before the
pandemic reach is really gonna matter for it to be effective. So, yes, hitting that
is important to me, but I don't know that the button itself is. I think it's more just
knowing that the effects are gonna be there.
I would be interested on your take on the shooting of Shaver in L.A.
Is the anniversary coming?
So this is asking about a specific incident a while back, years, years ago.
I actually have a couple of videos early on that mention this or make comparisons to it.
As far as just a general take that you are going to get from most people who look at
situations like that, well, let's do it this way.
You have probably heard people who talk about police misconduct use the phrase, and the
cop challenged him to a life or death version of Simon Says.
This is where that term originated.
I was there when the person said it and it stuck.
You are not going to find people who are critical, who look at those situations, you're not
going to find a lot of them who say that that was an appropriate outcome.
What do you think about Biden and Trump being in a dead heat in polling despite the former
POTUS' crazy indictments?
Is Biden really that weak or is this a case of Teflon Don's voters not caring about
his legal troubles?
When it comes to polling this early on, it doesn't matter.
It doesn't matter.
I would imagine that around now, actually it would probably be a couple of months ago,
But if you wanted to draw a comparison, I would point out that around the same timeframe
away from the election, Obama and Clinton were both trailing and the first George Bush
had just overwhelming support.
The two that were trailing won, the one with overwhelming support lost.
At this point, polling about the general election, it doesn't really matter.
Pay attention to more of the enthusiasm, the primary polling and how much support and enthusiasm
they have within their own party, and the policy questions.
Right now, people are saying, well, X percentage of Republicans think he did something seriously
wrong, but you know, some of them are still apparently going to vote for him.
That may change as time moves on.
Hey, Bo, why wasn't Trump charged with seditious conspiracy as well like his underlings and
list some of them?
Can you, I can only guess it's because DOJ wanted to only use the most easily proven
conservative charges.
Yes, that, I think that definitely played into it.
NASCAR indictment. Nothing extra and bill to go fast. That is what that
election interference indictment is. It is worth remembering we may not have seen
everything yet. There may be more to come. Okay, I was wondering if you had any
Any advice for two women who are big fans of yours who want to start a podcast here
on YouTube?
We would talk about a bunch of things like health, mental health, and have some political
things thrown in there as well.
I know that you don't really do interviews, but would you be willing to sit down?
Yeah.
I have videos on this.
I think they're mostly titled, like, let's talk about starting your YouTube channel.
There's one on, I think this channel, On the Roads with Bo, that's called like something
like a career day or something like that, and that was a whole bunch of questions from
both kids and adults about being on YouTube.
This would be useful and feel free to send an email and we can go from there.
Wow that was quick, or at least it seemed quick, I don't know if it was.
But that was all of them and it seemed like a much longer list.
Okay, so we will probably have a new format coming that will be on this channel.
It'll probably be weekly and you may see it as soon as next week.
This, the Q&A's are going to become a little bit more polished and there will be some extra stuff thrown
before and after the Q&A's to provide a little bit more
a little bit more structure to them and
those those types of episodes will be on this channel, so I
I guess that's it.
Anyway, it's just a thought.
have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}