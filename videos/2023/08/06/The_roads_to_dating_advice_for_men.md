---
title: The roads to dating advice for men....
---

| Input     | Output |
| --------- | ------ |
| Link      | [Youtube](https://www.youtube.com/watch?v=bv-jvrfrRJk) |
| Published | 2023/08/06 |
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is doing a Q&A session specifically for guys about dating, based on a request he received.
- He plans to refine the Q&A format over the next few weeks and include extra content beyond just the Q&A.
- Beau's advice for men in dating is to not make any single thing, like a hobby or belief, their entire personality as it can overwhelm their identity.
- He suggests that women are individuals with diverse preferences, so there isn't a one-size-fits-all answer to what women want.
- Beau explains the concept of core values in relationships, focusing on shared values rather than surface-level interests.
- He advises against checking a partner's phone as a way to monitor their interactions and stresses the importance of respecting individual boundaries.
- Beau addresses the issue of attracting partners based on wealth or status, encouraging authenticity and seeking connections beyond materialistic interests.
- He rejects the notion of "the wall," a concept that suggests women decline in value with age based on appearance.
- Beau warns against using gimmicky tactics to attract shallow partners and recommends engaging with individuals authentically.
- Beau provides advice on disclosing personal information, navigating workplace flirting, and avoiding gimmicky dating advice.

### Quotes

- "Don't make any single thing your personality."
- "Women don't all want the same thing."
- "Share core values for easier relationships."
- "Ignore gimmicky dating advice."
- "Engage with individuals authentically."

### Oneliner

Beau advises men to prioritize authenticity, shared core values, and individual connections over gimmicky dating advice to foster meaningful relationships.

### Audience

Men seeking authentic and meaningful relationships.

### On-the-ground actions from transcript

- Seek relationships based on shared core values, not surface-level interests (exemplified).
- Respect individual boundaries and avoid monitoring partners' interactions (implied).
- Embrace authenticity and avoid gimmicky tactics in dating (exemplified).
- Prioritize engaging with individuals authentically over following online dating trends (implied).

### Whats missing in summary

The full transcript provides in-depth insights into navigating dating as a man, focusing on authenticity, shared values, and individual connections.

### Tags

#Dating #Relationships #Authenticity #CoreValues #GenderRoles #Communication #WorkplaceFlirting #Gimmicks #Individuality


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to do a little Q&A.
This one is about dating, but this one is for guys.
Recently we did a Q&A that had to do with dating,
and afterward got a message saying,
hey, you know, that was great and all,
but most of those questions were obviously from women.
Can we do one for guys?
And the answer is yes, and here it is.
This format that we're doing right now,
there's a lot of people who seem to like it.
So we're going to kind of refine it a little bit
over the next couple of weeks.
And you'll see, you'll definitely see more of it.
And there will be some slight changes.
And there will be a little bit of extra stuff
beyond just the Q&A in it.
OK, so what do we have here?
Just give me a single piece of advice that you think most men my age, 21, need to know.
In relationship to dating, don't make any single thing your personality.
There is a habit right now where people will, and this is in particular to men, will find
a hobby or something that they enjoy, which is fine, that's good.
But then that becomes everything they're about.
becomes their entire personality, whether it be an ideology, political belief, a hobby
of some kind.
It's no longer a hobby, it's their identity.
It overwhelms their personality.
I will tell you right now, I have never in my entire life met a woman who liked that
ever.
That would be my advice.
Okay, I'm a 28 year old automotive mechanic, not a high status job, but I make good money,
not ugly, I've done the PUA scene, and that's probably why I saw your last dating video.
My dating life is still a disaster.
None of them knew what women want.
That's my question.
What do women want?
that don't know the PUA scene is like the forerunner to what you have on
YouTube now. Basically like this is how you get women type of videos. The
question is what do women want? The easiest way to illustrate this is to ask
your friends, your friends, your your male friends, get them all together and ask
them what kind of woman they want. Ask them to describe their ideal woman. And
when you realize that if you have six friends you're gonna get seven
different answers, that that should be your a sign. Women don't all want the
same thing. That's why nobody is going to be able to tell you what women
collectively want. They are individuals. I know that's not, you know, that is
probably not advice that you heard before but if you treat them as
individuals you will definitely get get further along in the dating process. What
What did you mean by core values?
You said you don't like taking care of the horses, but it fits the core values.
I don't understand.
So that's a reference to the first video.
And it had to do with opposites of tract, I think was the question it was based on.
But how people can have different interests as long as their core values are the same.
I may not have done a good job of explaining that.
So we have horses.
I like to play with the horses, like they're, as I have said
before, to me they're big dogs.
And to the person who sent the message about that, no, that
is not where I got the term, but that totally tracks.
They're big dogs.
My wife enjoys taking care of them as well.
The hobby itself is not just confined to horses.
The horses that are here are horses that were in some way
broke or misplaced or retired or something like that.
The core value at play there is doing what you can.
that rule 303 type of thing, if you have the means, you have the responsibility, yeah,
I would probably not choose to have a home for wayward horses, but I understand the drive
and I can understand why it is something that is exciting to her and it's something that
she's passionate about because that same core value of helping in a way that you can is
there.
That's what I mean.
You have to find core values that are the same.
And they don't have to be something altruistic.
You know, I have a friend that is very much motivated by money.
It is what it is.
That's one of his core values.
He grew up poor and he's never going to be poor again.
His wife, very much motivated by money.
Those are individual core values that if you share them, it makes your life a whole lot
easier.
Because when he makes a decision that he's going to, he's got to go to work for three
months in some other city, she's supportive of it because they share that core value.
That's way more important for long-term relationships than I think most people understand.
The fact that you both like a certain movie or music may not be as important as sharing
those core values.
If you don't check her phone, how do you know if she's starting to talk to somebody
else?
A shoulder to cry on becomes a… something I'm not saying on the channel.
can you date a woman if you don't know what she's doing? This is obviously in
response to me saying I found the idea of checking people's phones, somebody
that you're dating. I found that weird. Okay, so you're in the dating phase. Why
Why do you want to stop her?
If you are doing your partner relationship and the other person decides to go the other
way, why do you want them around?
I've never understood that.
when you were talking about early on in the relationship,
I would much rather find out that that's the type of person
that you were hooked up with.
Like, I would rather find that out early and let them go,
than try to play goalie your entire life.
If that's the case, she's just not that into you, don't...
Don't, trying to force it, it's probably not gonna be great.
All you're doing is setting yourself up
to feed a bunch of really, really questionable behavior
that over time will probably become incredibly toxic.
Just let it go.
before you say oh poor you read the whole thing man this is gonna be a good
one I can already tell I'm rich I'm and I'm above average looking because of
that I can get lots of dates I'm self-made and came from nothing I worked
my rear off to be where I am I was a scholarship kid at the rich schools and
never got dates. How do I know women I'm meeting today aren't interested in me,
or are interested in me and not with me for the bins, the boat, and money? My
friends basically laugh when I express my feelings on this. I don't want a
trophy wife. I want a good woman. The richest man I ever met in my life, and by
rich I mean rich rich not not like I'm talking about absurdly rich he drove a
beat-up Ford Explorer worth more money than most people can even fathom and
that's what he drove why do you have the Benz I mean why are you driving a
flashy car to attract people right I mean more than likely to show off that
status if you don't want to have to question whether or not somebody is
interested in you for that status don't drive that car I mean that that's the
easy answer but I'm gonna go back to the core value thing because you actually
actually sound a lot like my friend. Like I said, he came from nothing. Just because
somebody is interested in the security that comes with having a lot of money that doesn't
necessarily make them a trophy wife, and it doesn't necessarily mean they're a bad woman,
There is a habit, particularly because of things on Instagram and TikTok and social
media in general, there is a very overriding theme that women fit into certain classes
and they are this stereotype.
It's not true.
Again, you have to address them as individuals.
If you are concerned that every woman you meet is interested in you just for your money,
A, there's probably a little bit of projection going on there.
Because if you did grow up and you came from nothing, it's probably really important to
you.
So you might be placing the fact that it's important to you, placing that emotion on
other people.
I mean, I don't know, that gets into some deep psychological stuff I may not be qualified
to talk about, but I may not, I'm certainly not, but that's just how I would read it.
If it's a big concern, conceal how much money you have.
How can you not believe in the wall?
There has to be an age where women decline in value, jeez, cringe, like, ow, god, okay,
so if you're not familiar with the concept of the wall, you're lucky.
It also, basically, again, this is an internet thing that has taken hold, the idea that women
at a certain age just like drop off and apparently decline in value. That's a
horrible way to phrase that. Why do I not believe in the wall? And for context
this is almost always about appearance. It's never about anything else, which I
I mean, you can probably read a lot into that and the type of people that are promoting this idea
based on the fact that this concept is entirely about appearance.
Why do I not believe that, believe in that?
Because I just watched a Selma Hayek movie.
Okay, have you seen the interview with the woman who says she would date someone making
$75,000 per year?
Why are so many people calling her a liar?
Is $75,000 not a lot of money?
Oddly enough, yeah, I did see this.
I actually saw this like two hours ago and I was, I watched people, I looked at the comments.
That was definitely enlightening.
It's a clip of a woman who stopped in New York, I think she's from New Jersey, and
the person asks, hey, would you date a person that makes $75,000 a year?
And she's an attractive woman, and she's like, yes, and you can tell by the look on
her face that this seems like a really weird question.
The comments are very much full of people who don't believe she's telling the truth,
that she's a unicorn because women don't date poor guys like that's the the general tone
first yeah is 75k not a lot of money okay yeah where i live if you make $75,000 a year you're
rich. There's definitely a cost-of-living thing going on that when this conversation
occurs online, the cost of living in the various geographic areas, that really doesn't seem to come
into play. So that's definitely part of it. The reason so many people are saying that she's lying
or treating her like she is something incredibly rare is because the incredibly online scene
of men who engage in these conversations, they're skewed, they fell into an information
siloed and it was all reinforcing and then the general idea is that all women are only
after money and if you don't make six figures you're just gonna be forever alone is the
general tone.
And I'm seeing in these questions, I'm seeing a whole lot of little terms that come from
that world, like the high value, high status, like there's a lot of terminology that is
definitely from that online community but okay I'm a lefty I'm in college and
want to find someone with the same core values so I've tried to date within my
groups but always end up friend-zoned obviously once I feel that's the
direction it's going I stop but help okay I'm going to assume because you
started with I'm a lefty, when you're saying my groups, you don't mean your friend group,
you mean your activist groups.
Don't do that.
I know it's completely logical, but generally speaking, the women in those organizations,
you're talking about campus organizations and stuff like that.
date your comrades type of thing. If you are somebody who is very much, you have decided
you want to find somebody of the exact same ideology as you and you're looking for that
now and it's important to you, or that is one of those core values that you just have
to have. When y'all do, I don't know what y'all would call it, when y'all assist
groups at other campuses or assist groups that you're not in, maybe look
there, not in the groups that you are actually in. That causes just a lot of
issues for those organizations and the women who are friend-zoning you, they're probably
doing it for an ideological reason.
It may have absolutely nothing to do with you.
In this one instance, it may have nothing to do with you.
It may be like kind of an unspoken rule.
I am familiar with groups that have rules like that, so maybe look at groups that are
aligned that you're not in and maybe start trying to meet them and go there.
I followed the advice of Manosphere YouTubers, I became a high value male.
And it worked, in a way.
I have way more women interested in me, and they're all attractive.
They're also all shallow and dumb.
What do I do now, WTF?
I am shocked.
I am shocked.
You telling me that women who fell for shallow gimmicky tricks are not necessarily women
of a lot of intelligence and depth.
You specifically went out of your way and used these gimmicks, these tactics to attract
this exact type of woman... I'm just saying. That should not be a surprise. If a woman
is going to be attracted solely based on your posture, your language, and how you carry
yourself on that level, the level that is discussed in that world, they're
probably not the brightest. I mean, you are not going to get a Nobel Prize
winner that way. This should not be a surprise. Put any thought into it at
all and that should be the expected outcome.
If they're going to fall for that kind of gimmicky stuff, it is what it is.
My advice?
Forget everything that you learned and again engage them as individuals.
As an enby, non-binary, male, presenting but certainly not drawn to those ideals, how does
one approach your potential soulmate and communicate to them you don't quite fit into the norms?
okay so I have absolutely no experience with this particular this particular
piece of information that you're wanting to disclose okay but everybody has
secrets of some kind everybody has things that they don't really know how
out to tell to a potential partner or whatever.
My, and this is gonna sound super cliche,
but it's second date, gently, but also directly.
That's when I would bring that information out.
If there is, because you don't wanna overwhelm somebody
when you first meet them, when you first start going out,
There's no reason to because it may not, there may be other reasons that you all don't continue
to see each other.
You get to the second or third time you're hanging out, you want to get that information
out because in some cases, it may be something they're entitled to know.
In other cases, it may be something that you don't want to waste a lot of time going out
with somebody if they're going to reject you when they find out like a key piece of
information.
So it should be early and just honestly and directly as much as you can.
And I'm sure that there are other people with questions that are similar.
It doesn't necessarily have to be about something like that.
But any information that you're wanting to disclose, get it out early but not like right
away.
I have a friend who I know has been with half our women friends on a casual one or two time
basis and stays friends with them after.
I've asked him for advice on how he does it and he just lies and says he's never been
with them.
I know he has any ideas on how he does it.
He's not answering you, he's showing you.
This is another thing, and yeah, I'm going to totally sound like, you know, get off my
lawn right now, but I think a lot of young men could maybe re-embrace the idea of
of don't kiss and tell, there's a whole bunch of reasons
that that rule existed for a really long time.
If a woman in your friend group is going
through a phase of some kind, and she
is wanting to hang out with somebody on a very limited basis or something like that.
Do you think if there was a person available who would not disclose it, do you think that
that person might be an attractive option?
He's not answering you, but he is demonstrating what I would imagine is a key part of this.
The fact that he's like, nah, it didn't happen.
Can you explain how we are supposed to navigate flirting at work?
It really is like that meme.
If she finds you attractive, it's fine.
If she doesn't, it's harassment.
Okay, yeah, I have seen that meme with the really attractive guy and yeah, and then the
next block is the less attractive guy and she calls HR on the second one when they say
the exact same thing.
Yeah, okay, so a big part of this is the word unwanted.
Yes, there is a difference between something that is wanted, somebody that they might find
attractive, and something that isn't wanted, unwanted, is a key part of this.
As an example, I want you to picture the most attractive woman you have at your workplace.
Somebody that you really like, you are very attracted to, and she walks up and kisses
you dead on the face and I get hired tomorrow and do the exact same thing I'm willing to
bet one of those you would have a problem with it's the same thing yes it is it is
subjective in a lot of ways because the unwanted part is, you know, critical to the whole conversation.
The other thing that I would say is that in my entire life, I have never known anybody
to get in trouble for flirting. Flirting is supposed to be subtle. And I have never seen
that.
What I have seen is people that are crude or people who do not take no for an answer
after it is clearly unwanted if they continue.
As far as navigating this at work, I can help a whole lot with that very simply.
Don't, don't.
co-workers are not necessarily your dating pool.
That's a common theme here.
I would like to kind of alter that first piece of advice after seeing the questions.
That first question, just give me a single piece of advice that you think most men my
age need to know, you know, I said don't make anything your entire personality.
Don't take any advice on dating or relationships from memes or any online method that is designed
to be gimmicky or says this is what women want and clumps it into demographics and tries
to play that game.
You'll never be happy with the outcome.
That's my advice.
Ignore all of that.
Watch some romantic comedies or something.
Those are the questions.
Those are the questions, which is unique in and of itself.
I cannot wait to see the comments from women on this one.
Okay, anyway, it's just a thought.
Have a good day.
## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}