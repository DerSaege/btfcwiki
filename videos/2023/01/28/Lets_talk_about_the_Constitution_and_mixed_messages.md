---
title: Let's talk about the Constitution and mixed messages....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RqAR1i8xCy4) |
| Published | 2023/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the mixed messages about the Constitution he's been accused of giving.
- Talks about how he loves the Constitution's concise and precise nature, mentioning that it's the reason why he might make a short video just reading it.
- Points out that the Constitution repeats only one command, which is about not depriving any person of life, liberty, or property without due process, emphasized in both the fifth and 14th amendments.
- States that this command is the foundation of everything in the Constitution, making it the glue that holds the nation together as a nation of laws.
- Argues that supporting the Constitution means following this command twice given, and failure to do so means not truly supporting any part of it.
- Stresses the importance of evaluating behavior through the lens of necessity to prevent deprivation of life, liberty, or property without due process.
- Concludes by reiterating that failure to support this fundamental command means not genuinely supporting the Constitution in any aspect.

### Quotes

- "No person shall be deprived of life, liberty, or property without due process."
- "If you do not support the one command that the Constitution gave the governments twice, you cannot pretend that you support the Constitution."

### Oneliner

Beau explains mixed messages about the Constitution, focusing on the fundamental command of due process and its critical role in upholding the nation of laws.

### Audience

Constitutional enthusiasts

### On-the-ground actions from transcript

- Ensure understanding and support for the fundamental command of due process (emphasized)
- Advocate for the necessity of due process in all actions (implied)

### Whats missing in summary

The full video provides a deep dive into the importance of upholding the fundamental command of due process in the Constitution and its impact on supporting the nation of laws.

### Tags

#Constitution #DueProcess #Support #FundamentalCommand #NationOfLaws


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the Constitution
in mixed messages, and you're gonna get some trivia
that is very applicable today.
I have been accused of giving mixed messages
about the Constitution because I say things
like the Constitution either expressly permitted
or was powerless to stop everything
that has happened in this country.
And it's true.
And incidentally, that's not me.
I stole that from someone else.
And while I'm saying that, I will simultaneously
talk about how much I love it, the language,
the document itself.
And at some point I'll make a video
and it's just gonna be me reading it.
And it wouldn't be a long video.
That's one of the things that makes
the Constitution so beautiful.
It doesn't waste language.
It doesn't waste words.
It's incredibly concise.
In fact, there's only one thing that's repeated.
There's only one repetition.
There's only one command that is said twice
in the entire document, one command for the government.
And it's in there twice because it's that important.
Everybody had to get the memo, federal, state.
Everybody had to understand this
because it's the building block.
It's the thing on which everything else rests,
your second amendment, worthless without it.
My first amendment means nothing
because it can be overridden without this one thing.
No person shall be deprived of life, liberty,
or property without due process.
It's in the fifth amendment
and it's repeated in the 14th amendment
to make sure everybody understood this
because it's the glue.
It's what holds it together.
It's the thing that, in theory, makes us a nation of laws.
If you are attempting to justify somebody being deprived
of their life, liberty, or property without due process,
you do not support the Constitution.
You cannot abide by the one command it gives twice.
Oftentimes, people are trying to justify behavior,
and they come up with all kinds of excuses,
but there's only one lens that it can really be looked at through.
Did they have to?
Was it absolutely necessary in order to stop themselves
from being deprived of their life, liberty, or property?
That's it.
That seems very applicable today.
If you do not support the one command
that the Constitution gave the governments twice,
you cannot pretend that you support the Constitution,
any part of it, because without that,
the rest of it means nothing.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}