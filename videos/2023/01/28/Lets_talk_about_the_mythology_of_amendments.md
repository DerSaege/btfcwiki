---
title: Let's talk about the mythology of amendments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lkJXKIsBbNk) |
| Published | 2023/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the mythology surrounding the amendments, particularly the Second Amendment and its historical context.
- Emphasizing the founders' original intent behind the amendments and how it differs from modern interpretations.
- Exploring the idea that being pro-Second Amendment requires being alert to injustice and using peaceful civic actions first.
- Drawing parallels between the spirit of the First Amendment (freedom of speech) and the Second Amendment (right to bear arms).
- Arguing that being anti-Woke contradicts the spirit of the Second Amendment, as being "woke" involves being alert to injustice.
- Questioning why some leaders advocate for supporting the Second Amendment while being anti-Woke, which goes against the founding principles.
- Encouraging individuals to fulfill their duty under the Second Amendment by remaining vigilant against injustice and utilizing civic actions.

### Quotes

- "It's like the founders put up a piece of glass, you know, break glass only in the event of tyranny."
- "Tyranny is too late in the game. Things don't just flip and become totalitarian overnight."
- "By definition, you have to be alert to injustice."
- "You have to fulfill it. And you have to be alert to injustice, because that's what's going to precede tyranny."
- "It is a duty. You have to wonder why they don't want you alert to injustice."

### Oneliner

Beau questions the modern interpretation of the Second Amendment, advocating for vigilance against injustice as a core duty underlying its spirit.

### Audience

Citizens, Activists, Gun Rights Advocates

### On-the-ground actions from transcript

- Talk to your second amendment friends about the true spirit and duty behind it (suggested).
- Be alert to injustice in your community and surroundings (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on the historical context and original intent behind the Second Amendment, urging individuals to embody civic responsibility and vigilance against injustice.

### Tags

#SecondAmendment #FoundersIntent #Injustice #CivicDuty #Vigilance


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today I'm gonna talk about an idea that I have,
and we're going to talk about the amendments
and the mythology that follows them.
And we're going to follow what people believe
is the founders' intent behind the amendments.
And we're gonna see if it lands where people think it does.
And I have my questions about this.
I am not a second guy.
I'm a first guy.
I have a press, a modern-day equivalent of one.
But the mythology around the second, it's fascinating.
It's fascinating.
And it's not untrue.
It's not untrue, the thread that's
followed by those who see the second
as part of their identity, their contribution to the country,
their way of being a patriot, it's not untrue.
They may view it the same way I view this.
It's not the whole story, but it's not untrue.
If you ask them, those people who are just
incorporating the second into their identity,
if you ask them what it's for, why the second exists,
what do they say?
You ask them.
And you know, at first you say they'll
give you some like soft answer.
But then when you ask them, why do you need parity?
But then when you ask them, why do you need parity
with the military?
They'll tell you the idea is to be
able to fight back in the event of tyranny.
That's cool.
I mean, it's dangerous to go alone.
Take this.
It's like the founders put up a piece of glass,
you know, break glass only in the event of tyranny.
It's cool.
It's cool rhetoric.
It's cool mythology.
And again, not untrue either.
And they know that it's more than that,
because that's not how the founders did it.
They didn't start by breaking the glass.
They started with assembly, speech, pamphlets, petitions,
First Amendment stuff.
And if you ask them, and seriously,
talk to your second friends, the second is there to back up.
They will finish the sentence.
And they'll say the first.
That's the order that the founders used it in.
They didn't start off thinking they
were fighting a revolution.
They didn't even know they were fighting one until Thomas Paine
told them they were.
They started with the peaceful stuff,
the stuff that doesn't get the action movies made about it.
They sat there and they used the press and their voices
to talk about how intolerable things were.
History joke, intolerable acts.
Anyway, that's the mythology.
And it's not untrue.
Again, there's more to it, but it's not untrue.
What they're saying isn't false.
But here's the thing.
That means that the spirit of the second,
because of the founders and the way they follow it,
the spirit of the second requires that you're on alert.
That's their duty under the second,
is to be alert to tyranny.
The same way my duty under the first
is to be alert to current events.
It's the same thing.
But tyranny is too late in the game.
That's not when the founders started.
They started when things were intolerable.
And they used all the civic stuff, the peaceful stuff,
first.
That's where they started.
So you can't wait for tyranny, not
if you want to embody the meaning and the spirit
of the Second Amendment.
You have to be on guard a little bit earlier.
Tyranny is too late in the game.
Things don't just flip and become totalitarian overnight.
The face of tyranny is mild at first.
So you have to be on guard.
You have to be alert to the things that precede tyranny.
Injustice.
Part of the Second Amendment, part of the spirit of it,
is to remain on alert to injustice,
because that's what's going to precede the tyranny.
And you should use all of the civic stuff first.
That's the idea behind it.
And because of that, I suggest that you cannot be
pro-Second Amendment and anti-Woke.
By definition, you have to be alert to injustice.
Google it.
Define woke, second version, second definition.
It will be some version of the phrase alert to injustice.
The two things go together.
So you have to ask yourself why you
have a whole bunch of leaders dressing themselves up
in the language of the founders, in the language of the patriots,
and telling you to shirk your responsibility under the Second.
If you view that as your contribution to this country,
your duty, then you have to fulfill it.
And you have to be alert to injustice,
because that's what's going to precede tyranny.
And you have to use the other amendments first.
It's all there.
And it's there in their own mythology, their own rhetoric.
But the leaders, they're now saying,
support the Second Amendment and be anti-Woke.
What is that doing?
Is that what the founders wanted?
Would they have stood on idea and principle, opposed injustice?
Or would they have followed a man, a person, who just told them
what to think, told them what to do?
If you think about it, following the orders of your betters
and just doing what you're told, is that more like the colonists?
Or is it more like the British?
I might be wrong, but when you listen to the mythology of the Second,
from the people who view it as part of their identity,
that's what they tell you.
But they have these people now telling them
to shirk part of their responsibility, part of their duty.
Because it is the whole people, right?
It is a duty.
You have to wonder why they don't want you alert to injustice.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}