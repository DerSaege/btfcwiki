---
title: Let's talk about ledges, New Mexico, and a GOP candidate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=if03fIeUwVc) |
| Published | 2023/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing recent events in New Mexico involving drive-by shootings at homes of Democratic Party members.
- Law enforcement linking ballistics evidence to a Republican candidate, Solomon Pina, in the attacks.
- Pina believed baseless election claims and allegedly paid people to carry out the attacks.
- Reports suggest Pina may be advised to seek a plea deal due to incriminating evidence.
- The media focusing on Pina's landslide loss in the election and his motivations.
- One in four voters supported Pina, raising concerns about electorate judgment.
- Beau criticizes the Republican Party's shift and warns of increasing political violence.
- Mention of tweets indicating support for Pina and Trump, hinting at ongoing divisive rhetoric.
- Beau stresses the importance of countering divisive rhetoric and not underestimating its impact.
- The need for continued vigilance to prevent further violence and address underlying issues.

### Quotes

- "In one of them, rounds wound up in the bedroom of a child."
- "It is becoming more common. It is happening with greater frequency."
- "Ignoring his statements that later end up as Republican policy."
- "The echo chambers that brought about what occurred on the 6th, they still exist."
- "We're headed to our own troubles."

### Oneliner

Analyzing political violence in New Mexico, linking a Republican candidate to drive-by shootings, Beau warns of increasing frequency and the dangers of divisive rhetoric.

### Audience

Community members, voters

### On-the-ground actions from transcript

- Contact local representatives or party officials to express concerns about political violence (suggested)
- Attend community forums or events focused on promoting unity and understanding (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of recent political violence, urging vigilance and action to address the underlying issues fueling such incidents.

### Tags

#NewMexico #PoliticalViolence #RepublicanParty #DivisiveRhetoric #CommunitySafety


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about what is
going on out in New Mexico. What has happened. What is happening. And we're
going to delve a little bit deeper than the headlines and see if there's
anything that we should be paying attention to that maybe we're not. And if
there's anything that we can take away from it. If there's any real lessons
learned. And in particular, if there's anything that will answer a question
that shows up a lot here in the comments. If you don't know what occurred, a losing
candidate for the Republican Party out there for a state legislature position,
they have been implicated in a series of attacks. And by attacks I mean drive-by
shootings. And the attacks were conducted at the homes of prominent members of the
Democratic Party. Now, law enforcement is now saying that they have ballistics
evidence that links a shell casing from the scene to a shell casing from the
weapon linked to the candidate whose name is Solomon Pina or Pina. Reporting
varies on the pronunciation. I don't know which is correct. In the realm of
ballistics evidence, I mean that's about as close to a smoking gun as you can get.
That is pretty hard evidence. If the reports law enforcement is suggesting
exist, actually exist, the former candidate is already in an area where
his attorney is probably advising him to find a plea deal. The information
suggests that he paid people to engage in the attacks and then in at least one
instance participated himself. In one of them, rounds wound up in the bedroom of a
child. Now, one of the things that keeps getting brought up is his motivations. It
certainly appears that this is a person who believed all of the baseless claims.
They believe the lies about the election and he believed that it applied to him
as well. The media seems to be focusing on the outcome of that election a lot.
And yeah, I mean it's notable. They're framing it as he lost in a landslide. And
I mean, yeah, it's like 50 points or something. I want to say he had 26% and
the opposing candidate had 74%. I mean, yeah, that qualifies as a landslide. That seems
to be used in an attempt to exonerate the electorate. And sure, a landslide
against him, that's one way to say it. Another way to say it is that one in
four people who voted in this election voted to put this person in charge of
the levers of state violence in that area. That seems like a really bad move.
It is definitely an indictment of the people who voted there, who voted that
way. The Republican Party has a lot of soul-searching to do and they need to
understand that maybe you shouldn't just vote for somebody just because they have
an R after their name. What occurred is evident in a lot of rhetoric, in a lot of
talking points. It is what the Republican Party is becoming. And the average voter,
you need to begin to understand this. This is not the Republican Party that
your dad or granddad voted for. It has changed a lot and it's not for the
better. Another thing that I want to point out is that this type of activity,
this type of political violence, is becoming more common. It is happening
with greater frequency. If the United States, and again in particular the
Republican Party, does not change course, we're headed to our own troubles. We
really are. I want to read some tweets. I dissent. I am the MAGA king. This is the
candidate. Trump just announced for 2024. I stand with him. I never conceded my HD
14 race. Now researching my options. Guessing he didn't like his options. A
common question that is coming from mainly centrists and independents is why
do we have to keep talking about Trump? This is why. This is why. The echo
chambers that brought about what occurred on the 6th, they still exist.
They still exist. Ignoring his statements that later end up as Republican policy.
Ignoring his claims when they go out. Ignoring their direction. He is taking
that highly energized base is a mistake. It has to be covered and it has to be
countered. We can't stop paying attention. There will be more news about this. My
guess is that over time more and more links will emerge and the echo chambers
that helped facilitate this will end up being revealed. The US was in a position
where it was very obvious that it was on a latch and I think a lot of people saw
it as you know the those who support democracy with their back to the cliff
and the the far-right faction of the Republican Party you know has them by
little lapel pins and they're trying to push them over the cliff and they feel
like that's ended. All that has occurred is the two figures switched positions
and it's worth remembering that all that far-right side of the Republican Party
has to do is grab on and fall back. It's not over. We haven't walked away from the
cliff's edge here. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}