---
title: Let's talk about my shirts, the art, the artist, and cancel culture....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VTNZaaFdxM8) |
| Published | 2023/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of separating the art from the artist and applies it to his choice of shirts.
- Mentions wearing shirts from Rick and Morty, but decides to stop wearing them due to allegations against a key player in the show.
- Talks about wearing Hunter S. Thompson shirts and how it wouldn't be a good fit for his channel if it routinely raised money for AA or NA.
- Provides an example of a Harry Potter shirt he couldn't wear on his channel due to the artist using her platform to attack people supportive of causes he raises money for.
- Describes how cancel culture isn't about targeting conservatives but about companies protecting their brand image and assets.
- Explains that cancel culture is essentially capitalism in action, removing things that might upset certain demographics that use the product.
- Concludes by stating that just separating the art from the artist doesn't guarantee acceptance everywhere, especially if the ideas associated with them cause harm to communities.

### Quotes

- "Just because you separate the art from the artist, that doesn't mean that the art is still going to be welcomed everywhere."
- "Cancel culture is literally capitalism in action."
- "It's not about being a conservative, it's about the ideas that are so aggressive that they're causing active harm to the communities that these companies are catering to."

### Oneliner

Beau explains the nuances of separating art from the artist and why it matters in the context of cancel culture and brand image protection.

### Audience

Content creators, Conservatives

### On-the-ground actions from transcript

- Support domestic violence shelters (implied)
- Be mindful of the impact of the content you support or associate with (implied)

### Whats missing in summary

Deeper insights into the impact of cancel culture on brand image and the importance of considering the associated ideas rather than just separating art from the artist.

### Tags

#CancelCulture #Artists #BrandImage #Conservatives #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about my shirts
and separating the art from the artist.
And we're gonna talk about Hunter S. Thompson
and Rick and Morty and Harry Potter.
We're gonna do all of this primarily for the Republicans
who watch this channel,
especially those who are concerned about cancel culture and hopefully by
the end of it you'll understand how it works and why it's not what you think it
is. So there is a show, there's a TV show called Rick and Morty. I have worn shirts
from this show on this channel a lot. It's a sci-fi show and I wear them when
I'm talking about science, particularly if it's something very very futuristic
or you know really out there because it fits. Recently one of the key players in
the show, well some allegations surfaced and they're wild and there's beyond
what's being reported in the news, there's a whole flood of other rumor and
innuendo and all kinds of other stuff. What's true and what's not, I don't
really know, okay? But the allegations are there. And I made a comment saying that,
well, I will be taking those shirts out of, you know, the lineup. You're not gonna
see me wearing those anymore. And somebody said, something to the effect of, what
about separating the art from the artist? Which is something I totally agree with.
You should do that in most cases. In those cases you should do that. If you
went through and removed every movie, TV show, song, book that was written by
somebody that had something problematic, you wouldn't have a whole lot left. You
have to separate the art from the artist. However, there are some times that just
because you make that distinction, it doesn't mean that the art is still a
good fit. I wear Hunter S Thompson shirts a lot as well. He was a journalist if you
don't know. Fear and Loathing in Las Vegas is where most people would
recognize it. It wouldn't be a good fit if this channel routinely raised money
for AA or NA right? That would be a really weird pairing. It wouldn't make
sense. It would be very counterintuitive and it would seem really off. This
channel raises money routinely for domestic violence shelters. Rick and
already has to go. It's that simple based on that. Recently I saw this shirt and it
was perfect because I had been getting all of those questions about we have to
stop talking about Trump and all of that stuff and it it was a shirt that said
you know talking about he who shall not be named from the Harry Potter series
would have been perfect because I knew I was going to be making those videos. But
there's no way I can wear a Harry Potter shirt on this channel. Separating the art
from the artist in that case becomes really hard because the artist used the
platform that the art gave her to attack people that, well, this channel is very
supportive of. So, can't be here. It's not a good fit. Even if you, even if you do
separate the art from the artist. In fact, one of the people, in fact the only
The only person I know from that demographic that gets attacked by her, that has commented on this, that I've seen was
like super forgiving about it and was like well, you know, if you still enjoy them, you know, maybe you still watch
them, enjoy that, have that fun, maybe you just don't promote it, you know.
Very lukewarm, much better person than I am about the whole thing.
it still it just doesn't sit well and it goes back to what we're talking about
when we're talking about brand safe content on Twitter. When you're talking
about cancel culture, we can finally stop talking about my shirts now that you see
a little example of how it works and how these decisions get made, when you're
talking about these major multi-billion dollar corporations. What kind of
safeguards do you think they have in place to protect their assets from being
associated with something that might upset one of their key demographics?
They're pretty substantial. That's where cancel culture is coming from.
That's how that's how it is actually impacting Republicans and conservatives.
They're not being canceled because they're Republican or because they're
conservative. They're being canceled because of the imagery and a rhetoric
they use and how it others and marginalizes and attacks demographics
that the major companies view as their own. Because of that, even if somebody on
the board agrees with whatever is being said, well, they're not a good fit. That's
how cancel culture works. It isn't some plot to go after conservatives. It's the
rhetoric and imagery that they use being reprehensible to large segments of the
American society. So large that major companies who try to cater to as many
people as possible can't associate with those figures. That's that's how it
works. Cancel culture is literally capitalism in action. It is removing
things that might upset demographics that might use that product.
The shirts, it's kind of the same thing, I mean, not really, but the same mechanism
is there.
These things aren't a good fit on the channel because of their association, because of how
they would make the channel look. It's the same thing. Just because you separate
the art from the artist, that doesn't mean that the art is still going to be
welcomed everywhere. Just because you separate the the rhetoric from the
person, that doesn't mean that person is going to be welcome everywhere. It's not
about being a conservative, it's about the ideas that are so aggressive that
they're causing active harm to the communities that these companies are
catering to. And this is why, well, while go-woke, go-broke sounded cute, the
reality is, and people are learning it, go-fash, no cash. Anyway, it's just a
With that, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}