---
title: Let's talk about when Biden should've disclosed the documents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9VzwiyeaX2k) |
| Published | 2023/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the talking point about whether Biden should have disclosed a document's story earlier before the election.
- Exploring the future implications and path of this talking point.
- Challenging the notion that Biden's failure to disclose equates to a stolen election.
- Emphasizing the importance of responsible disclosure and the earliest possible moment for it.
- Illustrating a scenario where Biden's team discovers a classified document and the steps they should take.
- Pointing out the flaws in the argument that Biden should have immediately disclosed the information.
- Mentioning ongoing searches for potential additional documents to prevent leaks.
- Criticizing the Biden administration's statement on not interfering with the investigation.
- Arguing that immediate disclosure could have compromised national security.
- Asserting that the Republican talking point is not about the timing of disclosure but about undermining the country's principles.

### Quotes

- "There are miles of difference between the Trump case and the Biden case, but what about this part?"
- "They care about the ability to use that to cast doubt on the founding principles of this country."
- "You don't get to know everything."
- "It was released too soon."
- "No, they really should not have disclosed it as soon as they found out about it."

### Oneliner

Addressing whether Biden should have disclosed a document earlier, Beau argues for responsible disclosure and challenges the Republican narrative aiming to undermine the country's principles.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Support responsible and strategic disclosure of sensitive information to protect national security (implied).

### Whats missing in summary

Insights into the nuances of responsible disclosure and the implications of weaponizing disclosure timing for political gain.

### Tags

#Biden #Disclosure #NationalSecurity #ResponsibleDisclosure #PoliticalNarratives


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
whether Biden should have disclosed
the document's story earlier.
We're gonna do this because this has become a talking point
and this talking point has a future and it's not a good one.
So we're gonna talk about it.
We're going to talk about the future path of this.
And then we're gonna answer the question.
And we're gonna do this because I got a message
from somebody that was like,
hey, I understand that there are miles of difference
between the Trump case and the Biden case,
but what about this part?
Shouldn't Biden have disclosed this earlier
and told the press about it
because it was before the election?
Okay, so that's the talking point right now.
Biden should have disclosed it earlier
because it was before the election.
The next phase of this is if Biden had disclosed it earlier,
it would have influenced the election.
And then the next phase of it is
since Biden didn't disclose it earlier,
it's another stolen election.
That's where they're gonna go with it.
And now we're going to show why that's just not true.
Okay, so first the question's wrong.
Should Biden have disclosed it earlier?
Earlier than what?
That's not a real question.
The real question is what is the earliest possible moment
that the Biden administration
could have responsibly disclosed this?
That's the real question.
Earlier than what?
You know, just this random phrasing doesn't mean anything.
What we have to determine is the earliest possible moment
they could have disclosed it responsibly
because responsibly matters
as long as you're not part of a party that believes,
you know, it's cool to leave classified documents
knowing they're there at a club
or put the space laser lady
on the Homeland Security Committee.
The responsible part comes into play.
Okay, so you're Biden's lawyer.
That's how we're gonna do this.
You are Biden's lawyer.
And you're doing whatever the lawyer was doing there.
You're moving boxes or you're looking for something,
whatever, and you see a document
with that very, very distinctive set of markings.
What's the first call you make?
I know what you're thinking, but that's probably not it.
The first call you're gonna make is to your assistant
to bring you a new pair of pants
because you're gonna remember all of the coverage
about Trump's case, right?
It's gonna make you a little nervous.
So after that, then yes,
you're gonna call the proper authorities and say,
hey, I've got this thing and I don't wanna have this thing.
Come get it.
And that's what they did, right?
And then of course, the very next call you should make
is to the press to inform them about this story
that will become international news
and inform every foreign intelligence service on the planet
that there are more classified documents out in the wild.
But when you say it aloud, it doesn't sound like such a good idea, right?
So you probably don't do that.
What's the next call you make?
To search everywhere.
Search everywhere.
If one document made it out,
if there was one mistake, there might've been more.
So you search everywhere that Biden might have possibly put one
or his staff might have possibly put one
or they might have sent one by accident.
You search everywhere, which is again, what they did, right?
Once that's completed, that is the earliest point
you could responsibly disclose it.
Now, keep in mind, that's your team.
Your team, Biden administration people or Biden's personal lawyers,
those people completing that search,
that is the earliest possible moment
that it could be responsibly disclosed.
The smart move would be to wait
and see if the feds wanted to double check your work.
And then you would wait for that, for the smart move.
But that's not the earliest possible moment.
So when would that be?
Probably sometime next week.
No, they didn't fail to disclose it in a timely manner.
It still shouldn't be disclosed.
My understanding is that they found a document set like four days ago.
They're still searching.
They're still looking, which means this news got out
and there are probably people checking places
that we wouldn't want to get them.
Now, that leaves us to the obvious answer.
No, they shouldn't have disclosed it as soon as they found out about it.
It doesn't matter because a few days afterward,
it's after the election and the Republican talking point goes away
because they don't care anymore,
because they don't actually care about the time frame it was disclosed.
They care about the ability to use that to cast doubt
on the founding principles of this country.
Now, the obvious inbox message that I'm going to get,
even though I'm going to say this, is that, you know,
you wouldn't be saying this if it was Trump.
Yeah, I would.
In fact, I did multiple times.
I'll put some videos down below, but I absolutely said
that if he had done what he was supposed to and turned them back in
or even returned six months later, nothing would have happened.
And I also talked about the fact that if he had said nothing,
there'd have been nothing after the search.
Keep in mind, it wasn't the FBI who put out a press release.
It was Trump that started talking about it.
I absolutely would have said that because I did.
People have an expectation to know everything.
They're secrets.
You don't get to know everything.
And now, if searches are still going on to see if there are still documents
in the wild, it was released too soon.
The idea that it wasn't released soon enough is ludicrous.
The Biden administration put out a statement about this,
and it was less than well thought out from my perspective,
because they said, well, we didn't want to put out, you know,
part of the information, and we didn't want to interfere with the investigation.
That last part, yeah, you don't want to interfere with the investigation.
You don't want to interfere with the counterintelligence stuff that's going to go on.
That first part, I mean, it was a clunky response in my opinion,
and it didn't do them any favors.
But no, they really should not have disclosed it as soon as they found out about it.
That would actually be a bad idea if you care about the national security aspects of this.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}