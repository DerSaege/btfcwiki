---
title: Let's talk about the US and Russia cooking the books....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=i77bVhs9mv4) |
| Published | 2023/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparing economic situations of the United States and Russia, focusing on book cooking methods.
- Russia cooking the books to project strength internationally while the U.S. is doing it to avoid default.
- Russia artificially keeping the ruble strong and manipulating unemployment statistics to appear strong.
- U.S. engaging in internal book cookery to keep paying bills and avoid defaulting.
- Differences lie in intent: Russia for international image and U.S. to maintain domestic situation.
- Sanctions as a tool of war rather than diplomacy; Republican Party causing economic crisis over debt ceiling.
- Republican Party actions can be compared to a nation imposing sanctions on the U.S., actively working against economic stability.

### Quotes

- "Sanctions are often viewed as a tool of diplomacy, they're more often than not used as a tool of war."
- "The Republican Party manufacturing a crisis over the debt ceiling is having roughly the same effects that another country imposing sanctions on the U.S. would have."
- "It's performative for social media. It's not actual policy."
- "The Republican Party is actively working against the United States economic stability."
- "If a nation imposed sanctions on the United States, that's pretty interesting and pretty telling."

### Oneliner

Comparing economic strategies of Russia and the U.S., revealing how book cooking serves different intents, with sanctions being likened to a tool of war.

### Audience

Economic analysts, policymakers

### On-the-ground actions from transcript

- Analyze economic policies (suggested)
- Stay informed on political actions (suggested)

### Whats missing in summary

Insight into the broader impacts of economic manipulation in global politics.

### Tags

#Economy #Russia #US #Sanctions #RepublicanParty


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
similarities and differences between the United States and Russia and their current economic
situations and how they're going about something. We're going to compare them. We'll start off
with talking about the differences because that's the actual question, and then we'll go to
a unique similarity that I think Americans might should notice. And we're talking about
how the U.S. is cooking the books because of the manufactured debt ceiling thing,
and how Russia is cooking the books because of the sanctions. So here's the actual question,
and I think that this was meant to be kind of snarky, but the idea is so good that I'm
actually going to overlook the snark. Beau, please outline the differences between Russia
cooking its books after supposedly crippling sanctions versus what you touched on here.
In a recent video, I talked about how the U.S. was going to start cooking the books,
what they are calling extraordinary measures or something like that.
Okay, so outline the differences. Mainly intent. Russia is cooking the books to paint a rosy
economic picture on the international stage. The U.S. is now cooking the books to maintain
a domestic situation, and because of differing intent, they're going about it in different ways.
Russia is engaging in a lot of, I don't want to say currency manipulation,
currency controls. How about that? They're keeping the ruble strong artificially. Why?
Because of the historical trends with the ruble weakening, Russians tend to look at the ruble
the way Americans look at the stock market. It's kind of a generalist view of the world.
The stock market is kind of a general overall economic indicator. So it's really important
for them to keep the ruble strong, even if it's not real. Another thing they're doing is
engaging in some creative accounting when they're talking about classifications of things.
A good example, unemployment is something that is viewed and looked at all over the world in
countries. It helps to determine economic health. Right now, Russia officially has
record low unemployment. I can't remember the exact number, but I want to say it's like 3.7%.
That's all fine and good, but that doesn't include the literal millions of people who are on
quote unpaid leave. What's the difference between unpaid leave and being unemployed?
Not much if you're the person without the paycheck. They're trying to project an image of
strength to get people to think that the sanctions aren't working and therefore relax them. That's
their intent. If you want to know a little bit more about this, there's an article over on
foreign affairs and I think the title of it is the sanctions on Russia are working or something like
that. It's very direct. There will be some people who will say foreign affairs is very sympathetic
to the West. It is. At the same time, it's worth noting that all of that data that's in that
article, it checks out. So even if you want to disregard the analysis, the data's there,
the information's there. The U.S. isn't doing that. The U.S. is mostly internal within the
government and they're like basically like writing a check knowing it's not going to be cashed.
It's kind of what they're doing to keep paying the bills so we don't default.
But they're engaged in the same type of book cookery to keep things going.
Those are the differences. That's the question. The reason I found it so interesting is that while
sanctions are often viewed as a tool of diplomacy, they're more often than not used as a tool of war.
The Republican Party manufacturing a crisis over the debt ceiling is having
roughly the same effects that another country imposing sanctions on the U.S. would have.
The Republican Party is intentionally damaging the U.S. economy over a talking point.
They're not going to send up a balanced budget. They're not going to try to give Biden this huge
win by giving him a budget surplus. It's not going to occur. It's all a show. It's all a facade.
It's all made up. It's performative for social media. It's not actual policy.
That's why I find it interesting. The Republican Party, this question, does kind of highlight
that the Republican Party is actively working against the United States economic stability.
So much so that their activities can be compared with the activities of a nation at war with the United States.
If a nation imposed sanctions on the United States, that's pretty interesting and pretty telling.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}