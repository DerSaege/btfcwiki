---
title: Let's talk about what men can learn about masculinity from women....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0fK_KdcaCN8) |
| Published | 2023/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the concept of masculinity and what women from the pre-women's liberation era can teach modern men about it.
- Dismisses the idea of "alpha" and "traditional masculinity," stating they are not real concepts.
- Describes the advice given to women in the past on how to attract a man, including posture, topics to talk about, and appearance.
- Talks about the shifting views of masculinity and the confusion it causes for young men.
- Criticizes the advice given to men today on how to be masculine, likening it to teaching them to be women in the 1800s.
- Mentions the importance of heroes exhibiting universal qualities like problem-solving and community protection.
- Shares a Japanese concept of masculinity related to achieving perfection without effort and helping the community.
- Criticizes those pushing consumerism as a form of masculinity and encourages helping the community instead.
- Emphasizes that masculinity is not unified and individuals should focus on genuine actions to make the world better.
- Urges men to think for themselves and to prioritize helping others as a true representation of masculinity.

### Quotes

- "They're not teaching you to be a man. They're teaching you to be a woman in the 1800s."
- "Your masculinity will be perfect if you just get out there and help."
- "If you want to achieve that good masculinity, that kind that doesn't get made fun of in razor commercials, you have to think for yourself."

### Oneliner

Beau dismantles traditional ideas of masculinity, urging men to prioritize community service over superficial traits to embody true manhood.

### Audience

Men, Young Adults

### On-the-ground actions from transcript

- Help your community by volunteering, supporting others, and making the world better (implied).

### Whats missing in summary

In-depth exploration of the impact of consumerism on modern masculinity and the importance of critical thinking in defining true manhood.

### Tags

#Masculinity #CommunityService #GenderRoles #TraditionalIdeals #CriticalThinking


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about masculinity a little bit.
And we're going to talk more specifically about what women from the pre-women's lib era
can teach modern men about masculinity.
Because there's a trend that really can't be overlooked, or it shouldn't be.
Now, before we get into this, for those who are just tuning in for this video,
a couple of caveats. First things first.
Alpha, that's not a thing. That's not real.
That's based off bad science studying an animal in captivity.
This is how an animal who is contained by a system it doesn't understand behaves.
That's not really a thing.
The next is get rid of the idea of traditional masculinity.
That's not really a thing.
If you look, you will have varying cultures, ethnicities, subcultures,
all with their own idea of what masculinity is.
And it shifts over time. Are there general themes?
Yeah, there are.
There are some that you can find that are pretty universal for the most part.
But they're not really the things that are being discussed
when people talk about traditional masculinity.
Just get rid of that idea for a little bit as well.
You can bring it back later if you want.
So what could women possibly teach men about how to be men?
How to be masculine? Particularly women from before women's lib.
First, we have to talk about the world they existed in
and what their purpose was and what their limitations were.
At that point in time, there was basically the goal of getting a man to say,
I pick you. You're the one for me.
And because of that, they got a whole bunch of advice on how to do that.
There were advice columns. There were articles in magazines.
There were books. There were schools that taught women how to project a feminine air.
Taught them how to walk, how to stand,
what topics they could discuss and still be viewed as feminine.
What they could eat, how they should eat,
how they should dress to attract to the masculine gaze.
And it was all with the goal of creating this facade to attract a man.
Everything. This is what it was about.
Their posture, their form, how they had to stay in shape.
All of this stuff. This is what it was about.
And that's the world they existed in until they were able to break free of that system.
So today, because of shifting views of masculinity, right?
We've already talked about it. They do shift. They change a little bit.
And because of that, there's a whole bunch of young men today who are questioning how to be masculine,
how to be a man. And because we exist in a system that a whole lot of people don't understand,
there's a lot of people who are willing to step up for the sake of ad revenue or whatever,
to provide advice on how to be a man.
What's the advice they're giving you?
How to stand, how to walk, what activities you can enjoy and still be viewed as masculine,
what foods you should eat to be viewed as masculine, how to project that image, right?
What clothing you should wear to attract the feminine gaze.
These how to be a man things, that's...
They're not teaching you to be a man.
They're teaching you to be a woman in the 1800s. That's what they're doing.
They're not encouraging masculinity.
They are not giving you the skills necessary to be a man in today's world.
They're giving you the backstory to pretty much every Disney princess.
Because they tap in to that base desire and they say,
hey, this is how you can get a woman to say, I pick you.
And if it's successful, then you have to try to maintain that facade for the rest of your life.
That's not masculinity.
When you think about those people who are viewed as masculine heroes,
sure, they may have some of those qualities and they may exhibit some of those traits,
but that's not why you know their name.
That's not why they're in a history book.
That's not why somebody wrote a book about them.
They used those few things that are universal, the problem solving, the assertiveness,
the desire to protect your community, stuff like that.
They used that to help people.
Now, you have to view that through a relativist framework,
because a lot of them, they were people, they were flawed.
So they thought they were doing good when they weren't,
but they were motivated, those truly masculine, aggressive males that people want to emulate.
They were motivated out of a desire to help most times,
even if they were just objectively wrong.
That's what their goal was.
When you think about the concept of masculinity,
I think the best thing that I've ever heard about it comes from a Japanese concept.
It has to do with art, and it's the concept of perfection without effort.
You can achieve perfection, but if you're trying, you fail automatically.
And it has to do with being a part of nature,
but also separate from it and attempting to tame it.
That to me is more the general essence of masculinity if you want to get into it.
But that's not what people want to know.
People want to know how to be a man.
Help. Help your community. Do that.
Everything else will fall into place. Do that.
Right now, you've got a whole bunch of these people who are pushing this idea.
A lot of them live out there in California where there were floods,
where people need help.
But what are they doing? Telling you you need to buy a specific kind of watch.
They're not teaching you to be men. They're not teaching you to be alphas.
They're teaching you to be cattle in a capitalist system that a lot of people don't understand.
They're teaching you to be a consumer.
Masculinity isn't a team sport, and it's not unified.
Yours, your friends, mine, they're all going to be different.
If you are trying to fit into some role of masculinity that somebody has told you this is how you need to be,
you're not. You're failing because you're trying.
Your masculinity will be perfect if you just get out there and help.
If you try to make the world better.
If you do a lot of the stuff that those people that you view as masculine did.
They tried to make the world better.
Now, in order to do that, you need to make sure that you end up on the right side of things.
You don't want to be one of those people who thought they were helping and is viewed today as a villain.
Which means you have to think, and you have to think on your own.
If somebody is telling you that this is what you need to believe,
and you need to follow this peer pressure from dead people, this tradition, and this is how it's supposed to be,
they might as well be selling you a bridge.
They're not trying to help you be a man.
If you want to achieve that good masculinity, that kind that doesn't get made fun of in razor commercials,
you have to think for yourself, and you have to generally try to help those people around you.
That's masculinity. That's being a man.
It is not about your posture.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}