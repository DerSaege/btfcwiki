---
title: Let's talk about political ideas limiting earning potential....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MX4Qyb2i2q8) |
| Published | 2023/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Conservatives instilling ideas in children may limit future earning potential.
- Concepts already in motion, not widely recognized.
- Example: person with outdated speech patterns unlikely to get hired.
- Illustration: high-pressure office job search lacking suitable candidates.
- Example of adaptability: fast food worker impresses potential employer.
- Employer overlooks worker's felony history but dismisses him for inappropriate comment.
- A single comment changes the worker's future.
- Conservative values like bigotry may harm future job prospects.
- Bumper stickers or symbols indicating intolerance can impact job interviews.
- These outdated ideas are not marketable and cause economic harm.

### Quotes

- "It's just capitalism. These ideas don't sell. These ideas cause economic damage. Therefore, they have to go."
- "The hate, the bigotry, the intolerance, they're not marketable."
- "Conservative values like bigotry may harm future job prospects."

### Oneliner

Conservative ideas instilled in children can limit future earning potential, as outdated notions like bigotry are not marketable in the job market.

### Audience

Parents, educators, employers

### On-the-ground actions from transcript

- Challenge outdated beliefs and prejudices within your community (implied)
- Encourage open-mindedness and acceptance in children (implied)
- Advocate for diversity and inclusion in the workplace (implied)

### Whats missing in summary

The full transcript delves deeper into the impact of conservative ideologies on future earning potential and job opportunities, urging a reevaluation of harmful beliefs to secure better prospects.

### Tags

#ConservativeIdeologies #FutureEarningPotential #JobOpportunities #Bigotry #Inclusion


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk a little bit more
about how the ideas that conservatives are instilling
in their children will likely limit
their future earning potential.
It's a concept that I mentioned recently
in that video about Pink Floyd,
and it prompted a few questions about how it would work,
how it would start, and so on and so forth.
The reality is it's already happening.
it's just not occurring on a level that is so widespread that people
notice or identify it.
This is what I want you to picture.
I want you to picture
a person walking into
apply for a job today
that talks
the way
white southerners talked in the nineteen sixties.
They getting hired?
Of course not.
That's not going to happen.
No business is going to expose themselves
to that kind of liability.
That employee, not going to make it,
at least not for the good jobs, not for those high-paying jobs.
And this is something that I can attest to.
I know that it's happened.
I know a person, this guy, he's looking to hire someone
for an office job.
Admittedly, it's a high pressure office job, but it is really good pay and it's with a
good company.
He can't find anybody who's a good fit for the company.
He can find people who want the job, but at this time, he didn't even have anybody that
he was really considering because they just weren't up to it for whatever reason.
They had to be adaptable.
One day he's in a fast food restaurant and the place is just in utter chaos, just total
disarray and there is one person that is working the drive through the front counter and bagging
up the food.
They're obviously short staffed and it's the rush, it's lunch time.
And this person who is working there really impressed him because he was adaptable.
minute, he's yelling back into the kitchen and bagging up food, then he
turns around, he's at the front desk, immediately slips into that customer
service voice, hi, how are you today? And it just really resonated with the guy. So
he identifies the guy's name, gets it off of his tag, and he goes back and kind of
eats there a few more times just to watch this guy, make sure it's not a fluke.
And in the meantime, he finds out a little bit about him. And what he finds
out is that realistically this is this guy's future. He got in trouble like
when he was 19. Nothing like super objectionable but it was a felony so his
options are already limited. The guy doing the hiring, totally willing to
overlook this. I've known this guy a long time. It's probably because this guy did
lot of stupid stuff when he was a teenager as well. But when he's sitting
there one day, he hears the guy make a comment about women. And it's a comment
that I can't repeat on this channel. And that was it. That was it. That was enough.
He's like, well, can't have him working for me.
That comment changed this guy's future and he doesn't even know it.
We are talking about a life-changing pay difference and he has no idea that this even occurred.
And the reason he didn't get talked to or get brought in or even offered anything is
because of something he said. It's already happening. It's just not super overt. It's
not out in the open and people haven't identified it yet, but it's definitely occurring. These
old ideas, they're not marketable. The bigotry that is being instilled in a lot of conservative
kids is going to harm them. If that becomes part of their personality, if they internalize
and that becomes who they are when they're 25 years old, 30 years old, they're
they're gonna have a hard time getting a job. I guarantee you that there are
people who have bumper stickers that they think are super cute or a Confederate
flag bumper sticker or something mocking different groups on their car and when
the interviewer walked in and saw that, when they're walking through the parking
they see that car and they realize that's the person they're interviewing,
that's really the end of the interview. Because no company can accept that kind of risk.
No company can accept that liability. It's not cancel culture from the libs.
It's nothing like that. It's just capitalism. These ideas don't sell. These
ideas cause economic damage. Therefore, they have to go. It's that simple. If you
want to look at it from a right-wing standpoint, there you go. That's how to
look at it. Forget about, you know, the morality of it and the egalitarian
nature of a good society and all of this stuff. Forget about all that. If you
want to be a good right-winger, you want to support capitalism, this is just the
free market. These ideas, the hate, the bigotry, the intolerance, they're not
marketable. They are going to limit the earning potential of the children who
are being indoctrinated into those ideas today. It's going to happen. Anyway, it's
If it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}