---
title: Let's talk about M&Ms....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xJ-LOvp-Y5I) |
| Published | 2023/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- M&Ms updated their cartoon candies, sparking controversy like Tucker Carlson upset about the candy not being "hot" anymore.
- M&Ms made moves to be more inclusive, leading to backlash from the right due to their support for causes considered left-leaning.
- A recent campaign focused on women's charities upset conservative thought leaders, resulting in a known alpha male figure rejecting the feminine M&Ms.
- The company announced an indefinite pause on the spokes candies and introduced Maya Rudolph as a spokesperson, seemingly using satire.
- Conservatives misunderstood the indefinite pause, declaring victory over the candies, despite indications that they will return, possibly for the Super Bowl.
- The situation led to confusion and backlash, with conservatives struggling to understand the satire behind M&Ms' actions.
- Beau suggests that the war against M&Ms may be unwinnable and predicts further conservative backlash that could ironically boost M&Ms sales.
- He humorously suggests targeting Hershey's next due to the pronouns "her" and "she," poking fun at conservative outrage.

### Quotes

- "The war against M&Ms may be unwinnable."
- "Pronouns are bad. Y'all should go after them."
- "They're trying to train your kids early and stuff."

### Oneliner

M&Ms' inclusive moves spark conservative backlash, leading to misunderstandings and declarations of victory, but the war against the candies may be unwinnable.

### Audience

Social media users

### On-the-ground actions from transcript

- Support women's charities (implied)
- Stay informed on corporate actions and responses (suggested)

### Whats missing in summary

The full transcript provides more context and humor regarding the M&M controversy, enhancing understanding and entertainment.

### Tags

#M&Ms #ConservativeBacklash #Inclusivity #Satire #CorporateActions


## Transcript
Well, howdy there, Internet people. It's Beau again. So today, we're gonna talk about M&Ms.
M&Ms. The candy. Because wow. We've talked about it on the channel before,
but there have been a lot of developments since then, and it apparently needs clarification.
So, to provide a quick recap of events, not too long ago, M&Ms altered their cartoon candies,
the fake candy, the spokes candies. You know, they changed the shoes on one,
and that really upset Tucker Carlson because the candy wasn't hot anymore.
I think most people remember that happening. Along the way, M&Ms has made a lot of other
moves that were designed to be more inclusive, you know, trying to capture that market.
They're a favorite target of the right because they do, despite a lot of problems with the company,
they do give a lot of money to causes that the right wing of the United States would consider
left, and they do support Democratic candidates far more than Republican candidates.
So, they're an easy target.
One of the more recent developments was a campaign that was just supposed to give money to some
charities and help people out, but it was focused on women's charities, and this upset some
conservative thought leaders, and it led to a well-known campaign called the Women's Campaign.
And it led to a well-known alpha male figure declaring that he didn't want the feminine M&Ms,
and he would not let them pass his lips, which, I mean, that is a sentence. Those are words.
So, M&Ms, the company, put out a corporate statement.
I'm going to read this statement now and then continue, but y'all need to hear this to
understand everything that has occurred since.
America, let's talk. In the last year, we've made some changes to our beloved spokes candies.
We weren't sure if anyone would even notice, and we definitely didn't think it would break
the internet, but now we get it. Even a candy's shoes can be polarizing, which was the last
thing M&Ms wanted since we're all about bringing people together. Therefore, we have decided to
take an indefinite pause. Remember the word indefinite. From the spokes candies. In their
place, we are proud to introduce a spokesperson America can agree on, the beloved Maya Rudolph.
We are confident Ms. Rudolph will champion the power of fun to create a world where everyone
feels they belong. Now, if you're watching this channel, you have at some point in time
been exposed to the concept of satire. This, to me, definitely seems like if the M&Ms had
patches on their hats, they would be upside down. It's just dripping with satire, and
nothing screams, you know, take me seriously. This is definitely not a bit like bringing
somebody from Saturday Night Live on. Conservatives took this at face value, and they declared
victory over the candies. If you were to type in M&Ms woke on YouTube or wherever, you will
come across major conservative outlets and personalities talking about how they won,
and they have defeated the fictional cartoon candies. Most of them do point to that phrase,
we have decided to take an indefinite pause from the spokes candies. You keep using this
word. I do not think it means what you think it means. Indefinite means for an unknown
or unstated amount of time. It does not mean forever. It could mean forever, but it is
not definite. It's an indefinite amount of time, not really stated. I'll go ahead and
state it. The Super Bowl, they're going to be back for the Super Bowl. And the company
has almost confirmed this at this point because so many people did not understand what was
happening, which again just goes to state for certain that conservatives are incapable
of understanding satire, which kind of explains a lot of my inbox, to be honest. So the candies
are not going away, despite the many declarations of victory, at least at this point in time.
However, now M&Ms has gotten itself into such a bind because people did not understand that
it was a publicity stunt that I have no idea what they're going to do now. It will definitely
be interesting. I would like it noted that fictional cartoon characters appear to have
more intellectual depth than a lot of conservative thought leaders. So there is that. But rest
assured that at least at this point in time, it does appear that the candies have won.
The war against M&Ms may be unwinnable. Now, this, of course, is going to lead to yet another
round of conservative backlash, which will again probably help M&Ms sales. And it will
lead to conservatives needing a new company to be mad at for trying to be inclusive because
it's good for their sales. I would like to suggest Hershey's. I mean, look at this. It
has pronouns in her, she, just like the pronouns. Pronouns are bad. Y'all should go after them.
They're the real problem here. Hershey's. Her, she. They're trying to train your kids
early and stuff. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}