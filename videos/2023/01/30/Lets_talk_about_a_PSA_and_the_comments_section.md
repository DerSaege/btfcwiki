---
title: Let's talk about a PSA and the comments section....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aDnznf64DWo) |
| Published | 2023/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There are impersonators on YouTube leaving comments claiming you won a prize or asking for personal information and money.
- Impersonators try to gather personal information and trick people into sending them money.
- If a YouTuber has something special to share, they will make a video about it on their platform.
- Impersonators create fake accounts that look similar to the real channel but have small differences in the name and profile picture.
- Always verify the profile sending you a message by checking if it redirects you to the actual channel.
- Pay attention to web addresses in emails or messages to detect scams. 
- Most YouTubers do not direct people off-platform or ask for personal information.
- Scams are becoming more common as the internet expands, targeting a wider audience.
- Beau expected older individuals to fall for scams but found that younger people were also targeted.
- Use links in the about section to reach out to YouTubers on their official social media accounts.

### Quotes

- "If a person on YouTube has some kind of special deal or something like that or some kind of information or whatever, they're going to make a video about it."
- "Most of us, we don't even allow that. It will direct you to Patreon or something like that if you're wanting to support the channel."
- "So just kind of be on guard."
- "Y'all have a good day."
- "Thanks for watching!"

### Oneliner

Be on guard against YouTube impersonators asking for personal information and money; verify profiles, avoid off-platform interactions, and use official channels to reach out.

### Audience

YouTube Viewers

### On-the-ground actions from transcript

- Verify the profile of anyone reaching out to you on YouTube (suggested)
- Use official social media links to contact YouTubers (implied)

### Whats missing in summary

The full transcript provides detailed insights into how YouTube impersonators operate and offers practical tips to avoid falling for scams online.

### Tags

#YouTube #Scams #Impersonation #OnlineSafety #FactChecking


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we have a little bit of a PSA.
It's about something that is happening here on the channel
and a whole bunch of other channels too.
And hopefully we can use it to illustrate how to kind of fact
check something and maybe up everybody's game a little bit
on this.
So a lot of you've probably seen them.
There are comments down in the comments section
that appear to come from me, saying like, hey,
you've won a prize.
Or contact me at this number or something like that.
That's not me.
There are people who are impersonating the channel
that you're watching, a bunch of different channels.
And they try to gather personal information,
try to get you to send them money, stuff like that.
Here's the thing that you need to know.
If a person on YouTube has some kind of special deal or something like that or some kind of
information or whatever, they're going to make a video about it.
That's what keeps the lights on.
They are not going to direct you off platform to tell you about it.
I don't even use the services that are being promoted down there.
The reason this came up is because somebody started talking to one of them and once they
started talking to him was like oh yeah I have this prize for you or something
like that and I just need you to send me a hundred bucks and they're like yeah
this isn't Bo. So they kind of poked at him and the the people on the other end
actually sent them a photoshopped driver's license from the wrong state.
Also I have a CDL by the way if y'all are watching this. I am also taller than
Ben Shapiro definitely do not weigh 125 pounds, and my eyes aren't brown.
Obviously the person saw through this, and they sent it to me over Twitter.
So what can you do?
These kinds of scams, they're pretty common.
How can you make sure that you don't fall for one?
The easiest thing to do is to always check the profile that is actually sending you the
message. The account is made to look like this channel as far as name, like it'll
say Boa the Fifth Column or something like that, but there'll be underscores
and or dashes separating it and the image will be the the profile picture
from the YouTube channel. But if you click on it, it takes you to a different
channel completely with no content or anything like that. It doesn't redirect
to you, back to the channel that you're watching.
And this is true on every channel this is happening to.
Most scams, if you pay attention to the web address, you can see that it's not really
coming from where they say it's coming from.
Like it'll say www.v.epa.whatever and you owe us a fine for spilling oil or something.
It's not coming from epa.gov.
So that same principle can help you check out pretty much anything.
The same thing is true of emails that may come in.
It may look like an official message,
but if you go to the return sender thing, the reply,
you'll see that the email address doesn't
go where it says it's going.
So just so you know, as far as I know,
there's not a single YouTuber who engages
in that kind of behavior and sends people off platform,
tries to get personal information from them.
Most of us, we don't even allow that.
It will direct you to Patreon or something like that
if you're wanting to support the channel.
Because frankly, we don't want your personal information
because we don't want to have to safeguard it.
But that little game of cat and mouse, it's going to continue.
And as the web expands, there's going
more and more people that are that are targeted. The thing that I found
interesting and this may be my own bias showing was that I expected everybody
who like in the beginning saw it was like oh well I'll reply. I expected
everybody to be older. That wasn't the case. Some of the people were younger so
So I thought I would put this information out there.
And again, it's not just this channel.
There's a bunch of them that it's happening to.
So just kind of be on guard.
If you're going to reach out to somebody on YouTube, use one of the links in the about
section that directs you to a social media that is linked to the page.
That way you know for sure you are talking to, at the bare minimum, you're talking to
is somebody who at least works for the channel.
For some of the bigger channels, it may not
be the person you think it is.
But you're at least talking to somebody
who is authorized to speak on behalf of the channel.
Don't go to WhatsApp or Telegram or anything like that.
I don't think those would ever be real.
Anyway, it's just a thought.
Y'all have a good day.
Thanks for watching!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}