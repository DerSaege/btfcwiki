---
title: Let's talk about journalists and press releases....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NXa60Ku1B2k) |
| Published | 2023/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces an interactive video discussing journalism practices and a specific case to illustrate a point.
- Mentions the intersection of Rains Road and Ross Road as a focal point for the demonstration.
- Instructs viewers to search for a specific phrase related to an incident at the intersection.
- Reads a police department press release regarding an incident at the mentioned intersection.
- Criticizes journalists for accepting police press releases as factual accounts without questioning.
- Raises concerns about police departments using ambiguous and misleading language in press releases.
- Urges journalists to scrutinize police narratives and compare them with actual footage.
- Questions the motives behind police press releases and the lack of accountability in reporting.
- Encourages journalists to stop treating police press releases as unquestionable truth.
- Calls for a critical examination of police narratives by journalists and a shift away from blind acceptance.

### Quotes

- "Stop taking police department press releases as gospel."
- "Make 2023 the year that journalists stop accepting police press releases as if they're fact."

### Oneliner

Beau introduces an interactive lesson on journalism, urging reporters to scrutinize police narratives instead of accepting them blindly as factual accounts. He criticizes the use of ambiguous and misleading police press releases.

### Audience

Journalists, News Outlets

### On-the-ground actions from transcript

- Scrutinize police narratives for accuracy and question ambiguous or misleading statements (implied).
- Stop accepting police department press releases as unquestionable truth (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how journalists should approach and verify information from police press releases, advocating for critical examination and transparency in reporting.

### Tags

#Journalism #PoliceNarratives #MediaAccountability #PressReleases #CommunityPolicing


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today is going to be a little bit different,
a little bit more interactive than usual.
I will give you a task to complete during the video.
If you don't want to do it, it's fine.
The video is still going to make sense.
But I think it might help illustrate something.
Over the last week or so, I have talked a lot
about why journalists do things the way they do,
even when it doesn't seem ideal,
why they phrase things certain ways.
And I have been very sympathetic to journalists.
Today, we are going to talk about something
they have to stop doing.
And we're going to engage in a little field trip,
digital field trip, to demonstrate the point.
We are going to go to the intersection
of Rains Road and Ross Road.
So open up your favorite search engine.
And I'm going to give you a phrase to type in.
Rains Road, R-A-I-N-E-S,
Rains Road and Ross Road attempted to make a.
Hit Enter.
Now you are going to get, depending on the search engine,
you're using three to five articles dated January 8th.
Choose your own adventure, open one.
Scroll down to where it lists that intersection.
Now I'm going to read something,
and it's not from the news article
that you're looking at right now.
You may argue with me, but trust me, it's not.
You're going to see a lot of the same language though.
In some cases, it's verbatim.
In some cases, it's verbatim.
Okay, on January 7th, 2023, at approximately 8.30 p.m.,
officers in the area of Rains Road and Ross Road
attempted to make a traffic stop for reckless driving.
As officers approached the driver of the vehicle,
a confrontation occurred.
Please remember that phrasing.
And the suspect fled the scene on foot.
Officers pursued the suspect and again attempted
to take the suspect into custody.
While attempting to take the suspect into custody,
another confrontation occurred.
Remember that phrasing.
However, the suspect was ultimately apprehended.
Afterward, the suspect complained
of having a shortness of breath,
at which time an ambulance was called to the scene.
The suspect was transported to St. Francis Hospital
in critical condition.
Due to the suspect's condition,
the District Attorney General's Office was contacted
and it was determined that the Tennessee Bureau
of Investigation would handle this investigation.
The officers involved will be routinely relieved of duty
pending the outcome of this investigation.
All additional information regarding this incident
will be provided by TBI, Tennessee Bureau of Investigation.
Now, odds are the article that you're reading,
that's pretty much what it says.
In some cases, it is exactly what it says.
What I read was the press release
from Memphis PD the night before.
Stop taking police department press releases as gospel.
They're not.
For those who aren't looking at an article
to explain exactly what we're talking about,
in case you haven't guessed,
that's the first draft of the story
of the footage that we all just saw.
That's what the cops said happened.
A confrontation occurred.
Okay, so if you are a journalist, there's your sign.
Think back to every other press release
you have gotten from a police department.
Does it say a confrontation occurred?
Does it use ambiguous language like that?
Or does it say the suspect attempted to strike an officer
and then reached for their waistband?
When you see that language that leaves room for doubt,
doubt it.
That's why it's there.
It's them attempting to not lie,
but also not wanting to say what happened.
You will see multiple articles that have almost all of this,
and they reported it.
Some of them did actually at the end put like police say,
but I mean, when you report it as fact,
adding that little bit at the bottom doesn't really help.
You're putting out that story.
You're helping to manufacture that story
to cover up that footage.
Now, when you're looking at this press release,
you really only have a couple of options.
Either they're intentionally trying to mislead the press,
or they literally didn't know.
And if they didn't know, I have questions
about every single officer that was on scene.
If the administration did not know what happened,
that means nobody told them.
And after what we saw, it seems like somebody should have.
And then I guess the third option
is that they're just really bad at their jobs
and have no idea what their officers are doing.
There are no good options that explain this press release.
There are no good options to explain
why it was carried almost verbatim by multiple outlets.
In case the last few years hasn't taught
the journalistic community this, police administrations,
they'll totally lie to you.
Make 2023 the year that journalists
stop accepting police press releases as if they're fact.
Compare this statement to that footage.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}