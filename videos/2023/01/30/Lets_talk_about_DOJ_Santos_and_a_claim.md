---
title: Let's talk about DOJ, Santos, and a claim....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rFHB6gd1WW4) |
| Published | 2023/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring a conspiracy theory about Santos being a secret double agent on Team Trump.
- Claim circulating among a group known for inaccurate claims, leaving many questions.
- Theory involves Santos fighting against evil Democrats as a super secret double agent.
- Belief that FBI, no longer run by Democrats, is proof of Santos being on the good side.
- Department of Justice intervened to protect Santos from FEC investigation, according to Washington Post reporting.
- DOJ's intervention not to protect Santos but likely due to a criminal investigation on him taking precedence.
- DOJ telling FEC to back off is bad news for Santos, indicating criminal investigation involvement.
- Likelihood of Santos being under criminal investigation, not being protected by the DOJ.
- Theory's lack of accuracy, coming from a group that previously waited for Kennedy to return in Dallas.
- Most likely explanation: DOJ doesn't want FEC compromising criminal investigation accidentally.
- Possibility of resurgence of similar conspiracy theories in the future.

### Quotes

- "I do not think that anything else in the versions of this theory that I have been given is even remotely accurate."
- "Please remember, these are the people, this is coming from the same set of people who showed up in Dallas waiting for Kennedy to return."
- "That seems more likely that the FBI or the Department of Justice doesn't want the FEC compromising a criminal investigation by accident."

### Oneliner

Beau examines a conspiracy theory about Santos being a secret double agent for Team Trump, debunking it with the reality of a criminal investigation involving the Department of Justice.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed about current events and political developments (implied)

### Whats missing in summary

Full understanding of the conspiracy theory and its debunking with the reality of a criminal investigation involving Santos and the Department of Justice.

### Tags

#ConspiracyTheory #Santos #DepartmentOfJustice #FBI #CriminalInvestigation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about world of nonsense.
We're going to talk about a claim that has been sent to me
a couple of times.
I have yet to see this in the wild yet.
I can't wait to.
But it is circulating among a group of people
that have become pretty famous for claims that
are less than accurate and leaving people with a lot of questions.
So we'll go through the claim and then we'll get to the reality of it, which makes the
claim even funnier.
Okay, so the theory is that Santos is one of the good guys.
He's on Team Trump fighting against the evil Democrats who are deep inside of everything.
That's the reason for his colorful history and how some of it can't be proven.
It's because he's like a super secret double agent on probation or whatever.
The Trump team is winning because the FBI is now no longer run by the Democrats.
I would like it noted that the FBI has literally never been run by a Democrat, but their proof,
their evidence of the FBI now being on the good guy team is that the Department of Justice
has stepped in to protect Santos from the FEC, the Federal Elections Commission.
And this is all based in that world where I guess, I mean, it's the nonsense.
It's the conspiracy theory nonsense stuff, okay?
I had somebody send this in with a question, not just telling me about it as well.
And the question was, is any of this even remotely true?
I mean, one part of it is the Department of Justice did, in fact, according to reporting,
step in to stop the FEC from investigating Santos.
That part is accurate, according to reporting from the Washington Post.
But it's not because they're protecting him, it's most likely because they're running a
criminal investigation on him and the FEC investigation was civil.
The criminal one takes precedence.
So they basically told the FEC to kind of back off, get out of the way, and let the
criminal investigation proceed.
They can deal with the civil stuff later if they need to.
The Department of Justice telling the FEC to back off of Santos, that's not good news
for Santos.
That's actually really bad news for Santos.
The only scenario I can think of is that he's under criminal investigation.
There might be another really bizarre one.
the Department of Justice has something that's, it's the only thing I can think of.
I can't think of anything else that's even remotely plausible.
It is bad news.
I do not think that anything else in the versions of this theory that I have been given is even
remotely accurate.
Please remember, these are the people, this is coming from the same set of people who
showed up in Dallas waiting for Kennedy to return.
So yeah, there is an element of truth which is always necessary for somebody to manufacture
a really wild theory.
But if you are not down the rabbit hole and through the looking glass, here in a world
where things do make sense, the most likely answer is not that secret forces within the
government have been battling that out and now the good guys control the Department of
Justice so they're protecting Santos who is really like some super spy or whatever.
That seems super unlikely.
It seems more likely that the FBI or the Department of Justice doesn't want the FEC compromising
a criminal investigation by accident and having the investigators step on each other's toes.
That seems way more plausible to me.
Given the fact that this came in a couple of times and the theories presented, the claims
presented in the different messages were very similar, this is probably something that's
gaining ground.
We might need to be ready for a resurgence of these kinds of theories.
We've had a good run where they were way in the background.
They weren't even really making headlines.
But they might be coming back.
So prepare yourself for that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}