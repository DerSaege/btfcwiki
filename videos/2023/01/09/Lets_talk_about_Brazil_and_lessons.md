---
title: Let's talk about Brazil and lessons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IY98n0V9Jfs) |
| Published | 2023/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Drawing parallels between recent events in Brazil and the United States where supporters of a far-right authoritarian leader stormed key buildings.
- Noting the transferability of nationalist rhetoric and imagery used to incite far-right supporters.
- Emphasizing that the threat is not over, cautioning against underestimating the persistence of this style of leadership.
- Warning that without a resounding defeat, this authoritarian playbook will continue to be replicated with potential success.
- Stating that the fight against far-right authoritarianism is ongoing and needs to be acknowledged and addressed.
- Stressing the importance of recognizing the pattern and not dismissing current events as a thing of the past.
- Mentioning the ease with which false patriotism can be manufactured to manipulate aggressive individuals reminiscing about a mythic past.
- Noting that this leadership style can be easily mimicked and that the fight against it must persist.
- Calling attention to the need to take seriously the lessons from recent events and not downplay the ongoing threat.
- Urging vigilance and acknowledging that until there is a clear defeat, this cycle may continue.

### Quotes
- "It's not over. It's not over in the United States. It's not over in Brazil. And it can happen anywhere."
- "That style of leadership, it will transfer to the next Xeroxed copy of Trump because there wasn't that resounding defeat."
- "We can't just pretend that it's stopped."
- "This far right authoritarian style of leadership is easily mimicked."
- "This is one of those times when the polls matter."

### Oneliner
Recent events in Brazil mirror past events in the United States, warning against underestimating the ongoing threat of far-right authoritarianism and stressing the need for continued vigilance.

### Audience
Activists, concerned citizens

### On-the-ground actions from transcript
- Stay informed and engaged with current events (suggested)
- Advocate for policies and leaders that prioritize democracy and human rights (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of the parallels between recent events in Brazil and the United States, urging vigilance and action against the ongoing threat of far-right authoritarianism.

### Tags
#FarRight #Authoritarianism #Nationalism #Brazil #UnitedStates


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a little bit of d??j?? vu
that I'm sure a lot of people are feeling.
I mean, the imagery even looks pretty similar.
We're going to talk about Brazil.
And we're going to talk about what the events down there
can teach us in the United States,
and pretty much everywhere else as well.
Because something happened.
If you missed it, we got a replay.
The supporters of a losing far-right authoritarian
politician, a politician who claimed he didn't lose,
who riled up his supporters on social media, and then fled.
Those supporters stormed the Capitol in Brazil
where Congress meets.
And the Supreme Court.
And the presidential palace.
Hundreds were arrested.
Chaos ensued.
The authorities are saying that everybody
will be brought to justice.
It all sounds really familiar, right?
And it really did.
It looked pretty similar.
Different colors.
But it looked pretty much the same.
Because it was pretty much the same.
There are a couple of lessons that
are readily apparent from this.
The first, and maybe the most important for some people,
is that the rhetoric and imagery that
gets used in the United States to stir up the far right,
it's not special.
It's just nationalism.
It's transferable to countries all over the world.
That stirring of false patriotism,
it's easy to manufacture.
If you have people who are aggressive,
those people who look back to a myth of what
yesteryear looked like.
It's easy to rile those people up
and get them to do your bidding while you're safe in Florida.
The other thing is it's not over.
This is for the rest of us.
It's not over.
There are people in the United States
who see Trump's diminishing power
and see it as something that's over.
It was this bad chapter in American history,
and now our long national nightmare
is over and all of this stuff.
It's not.
It's not over.
It's not over in the United States.
It's not over in Brazil.
And it can happen anywhere.
This far right authoritarian style of leadership
is easily mimicked.
They all got their plays from the same people
back in the 30s and 40s.
Those who stormed the buildings down there,
they were called fascist by a lot of people.
It's all the same.
And it's not over.
This fight continues.
We can't just pretend that it's stopped.
With everything that we've seen in the United States recently,
it should be abundantly clear that it is not over.
That style of leadership,
it will transfer to the next Xeroxed copy of Trump
because there wasn't that resounding defeat,
and this will go on and on until they win
or there is that resounding defeat.
This is one of those times when the polls matter.
It will keep happening.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}