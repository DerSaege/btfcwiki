---
title: Let's talk about rivers in Alaska....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jSYhcvoxEdo) |
| Published | 2023/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Alaska rivers turning orange, resembling areas around mines, but no mines present.
- Scientists link the orange color to climate change, specifically thawing permafrost.
- Thawing permafrost releases trapped soils with sediment and iron, causing rust when exposed to air and water.
- The water's acidity is also increasing in some areas.
- Researchers are uncertain about the exact cause but are aware of the changing water.
- Rivers on protected land in Alaska serve as drinking water for native communities.
- Downstream impacts on the food web, fish, and other animals are significant considerations.
- The changing rivers serve as an obvious indicator of the effects of climate change.
- Urges the United States to prioritize significant mitigation efforts.
- Calls for a real transition and shift to address climate change adequately.
- Emphasizes the lack of support and pressure on elected leaders to address climate change.
- Suggests that people need to be awakened to the imminent threats posed by climate change.

### Quotes

- "It's another orange flag that's kind of pretty obvious."
- "The planet is definitely signaling to us that things are changing."
- "Climate change is happening. It's happening all around us."

### Oneliner

Alaska rivers turning orange signal the impacts of climate change, urging real action to mitigate the environmental threats ahead.

### Audience

Climate advocates, environmental activists

### On-the-ground actions from transcript

- Advocate for legislative priorities on climate change (implied)
- Raise awareness about the impacts of climate change in local communities (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the changing rivers in Alaska due to climate change and the urgent need for significant mitigation efforts to address environmental threats.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Alaska and rivers
and what's happening up there because they're turning orange.
You have rivers that are turning orange in Alaska.
The... when you look at the footage or the photos,
it looks like what you find around mines.
But there are no mines. That's not what's doing it.
Scientists out there believe that it is related to climate change.
The current theory is that the increase in temperature
is causing the permafrost to thaw,
which releases the soils trapped underneath,
which has sediment, lots of iron.
When it hits the air and water, it rusts, creates that color,
which also, at least in some areas, is raising the acidity of the water.
There's one story about a researcher who put the water in
with their powdered milk and it curdled it.
So right now they don't exactly know what's going on.
They have theories, but they don't know.
What they do know is that the water is changing.
The rivers are changing.
These rivers, a lot of them are on protected land,
and in some cases they serve as the drinking water for native communities.
You also have to consider the downstream impacts
when it comes to, you know, the entire food web.
When you think about how it might impact fish,
which then impact other animals that live on the land,
and how everything kind of snowballs from there.
It's another orange flag that's kind of pretty obvious,
and it's letting us know where we're headed as far as climate change.
You know, the planet is definitely signaling to us that things are changing.
It seems like the United States would want to make it a priority
to mitigate in large ways.
You know, we've talked about how a whole lot is being done,
but it's not enough.
It isn't enough.
We have to begin a real transition, a real shift,
and when the support isn't really there
because people just want to look the other way,
we have to do more to wake people up to the real threats that are on the horizon.
I mean, when they have to sneak through
one of the most effective climate packages in US history,
when they have to kind of slide it into an inflation measure,
it shows that the US population isn't putting enough pressure
on their elected leaders to make this a legislative priority.
Climate change is happening.
It's happening all around us.
It's coming, and eventually it will be coming to a neighborhood near you.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}