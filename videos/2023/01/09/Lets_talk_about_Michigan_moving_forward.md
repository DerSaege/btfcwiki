---
title: Let's talk about Michigan moving forward....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1nZB3C5n6oA) |
| Published | 2023/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Michigan Attorney General Dana Nessel reopens an investigation into individuals involved in the electors' plot after a year of inaction.
- Nessel's decision to proceed with charges is met with criticism from the GOP, who label it a political stunt.
- Despite GOP claims, the Attorney General's office indicates a swift move towards charging decisions.
- Nessel asserts there is clear evidence supporting charges against the false electors.
- State-level investigations, like the one in Michigan, showcase frustration with the slow pace of federal investigations.
- The Attorney General's office signals a non-political intent and a rapid progression towards charges.
- Michigan has laws addressing the behavior in question, indicating a quick movement towards legal action.
- The GOP's dismissive attitude may backfire as the investigation progresses swiftly.
- Nessel's actions challenge the narrative of a broken justice system by demonstrating proactive state-level prosecution.
- State attorneys general are growing impatient with the federal government's pace, leading to independent investigations and prosecutions.

### Quotes

- "There is clear evidence to support charges against those 16 false electors."
- "I don't think that this is going to be a political show."
- "There are a whole lot of people saying that the justice system is broken."

### Oneliner

Michigan Attorney General Dana Nessel reopens an investigation into the electors' plot, signaling swift legal action against those involved, challenging the narrative of a broken justice system.

### Audience

Legal activists and concerned citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for efficient and fair legal processes (implied)
- Support efforts by state-level attorneys general pushing for timely investigations and prosecutions (implied)

### Whats missing in summary

Insights on the potential implications of state-level prosecutions and their impact on the justice system.

### Tags

#Michigan #AttorneyGeneral #StateInvestigations #JusticeSystem #LegalAction


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Michigan
and Attorney General up there, Dana Nessel,
and her plans to proceed.
Because, I mean, not to put too fine a point on it,
but it seems like she got tired of waiting.
In Michigan, there are a number of allegations
surrounding people who may have participated
in the electors' plot, how it's come to be known.
The Attorney General submitted kind of a referral
for federal prosecution.
It's been like a year.
And there hasn't been any public movement on it.
So she has decided to reopen the investigation.
And move forward with it.
There are complaints from the GOP up there,
basically saying that it's a political stunt
and that the people really shouldn't want somebody
who's going to use tax dollars
to constantly go after their political enemies.
That's how they're trying to frame it.
I don't know that that framing is accurate
or that it's going to hold for very long.
Some of the quotes coming out of the Attorney General's office
really makes it seem like they're headed
to a charging decision like now.
They're saying they're reopening the investigation.
But some interesting quotes include,
there is clear evidence to support charges
against those 16 false electors.
That's straight from the Attorney General.
That's from Nussel.
I don't think that this is going to be a political show.
I think that this is an Attorney General at the state level
who doesn't want to wait anymore.
Who isn't willing to see what Garland is up to.
And plans to proceed pretty quickly.
I have a feeling that the kind of flippant response
from the GOP up there is going to come back to bite them.
It doesn't appear that they're taking it seriously.
And based on some of the public statements
from the Attorney General's office
and the information coming out,
I don't think that this is a political thing.
It certainly appears that they plan on moving forward
with charges very quickly.
And in Michigan, there are charges
that kind of directly address this sort of thing.
There are laws on the books about kind of what happened.
So I don't think it's a stretch to say
that this will move forward pretty quickly.
But it also should serve as a reminder
that while many of us are growing frustrated
with the Department of Justice's slow pace,
methodical pace, however you want to frame it,
there are state investigations that either are open
or might be reopened at any moment.
There are a whole lot of people saying
that the justice system is broken.
And this is kind of a sign
that you're seeing state-level attorneys general
kind of feel the same way about the speed
at which the federal government is moving.
Now, there are a whole bunch of reasons
for the federal government to be moving as slowly as it is,
but it doesn't look like the states
are willing to wait much longer.
So this is something that we'll have to wait
and see how it plays out,
but I'm pretty sure you'll see this material again.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}