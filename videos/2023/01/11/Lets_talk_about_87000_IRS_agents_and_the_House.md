---
title: Let's talk about 87000 IRS agents and the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gkCMVNldvrM) |
| Published | 2023/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the addition of 87,000 new employees at the IRS over 10 years.
- Clarifies that more than 50,000 of the new hires are to replace retiring IRS employees.
- Notes a net gain of around 27,000 employees, mainly customer service and IT staff.
- Debunks the myth of hiring 87,000 armed agents, stating they are specifically targeting tax evaders.
- Mentions that individuals making under $400,000 annually won't face increased audit chances.
- Points out that despite the bill passing in the house, it's unlikely to advance in the Senate.
- Criticizes the creation of false issues by certain political factions for perceived victories.
- Emphasizes that the reported changes are still set to occur with increased hiring and audits.
- Condemns the misleading narrative created around the IRS issue for political gains.
- Concludes that the core situation remains unchanged, despite the political spectacle.

### Quotes

- "87,000 armed agents, okay? That was never going to happen."
- "They just made that up and it got reported on by outlets like Fox."
- "They will hire 87,000 more people and there will be more audits of people who make more than $400,000 a year."
- "Literally nothing changed, except they got a cool little vote and talking point in."
- "So all of this is still happening, just so everybody's clear on that."

### Oneliner

Beau clarifies the hiring of 87,000 IRS employees and debunks myths surrounding the bill, exposing political manipulations for false victories while asserting that the core changes are still set to happen.

### Audience

Taxpayers, Political Activists

### On-the-ground actions from transcript

- Contact your representatives to stay informed and hold them accountable (exemplified)
- Stay updated on political developments related to IRS funding and hiring (suggested)

### Whats missing in summary

Detailed analysis of the potential implications of increased IRS staffing on tax enforcement and compliance.

### Tags

#IRS #Taxation #PoliticalManipulation #GovernmentFunding #Accountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about 87,000 new employees
at the IRS and what happened in the house.
And we're gonna kind of put everything into focus
because there's a lot of confusion about it.
First, let's talk about what the bill does.
What has already been passed?
Let's talk about that part.
So there's 87,000 new employees to be hired at the IRS
over 10 years.
And more than 50,000 of those
are to replace retiring IRS employees.
The net gain, I wanna say it's like 27,000.
Those are gonna be mostly customer service and IT people.
The idea that they're hiring 87,000 armed agents,
that's just not true.
Again, this is over 10 years
and the new resources are specifically directed
to go after quote, high income and corporate tax evaders.
The threshold is 400,000.
If you make less than $400,000 a year,
you will not see your chances of an audit increase.
So I know there's probably a lot of people
who watch Fox News who are wondering
why I'm still talking about this in the present tense,
like it's gonna happen because, you know,
the plucky upstarts in the house,
they stopped it and they took that funding away.
No, they didn't.
No, they didn't.
They passed it in the house, that's true.
But based on the reporting,
it's not even gonna be taken up in the Senate.
If it was taken up, it wouldn't pass.
If it did pass, it wouldn't be signed by Biden.
They accomplished absolutely nothing
except for creating a perceived solution
to a problem that they made up.
It's the new Republican mantra
because they have come to the conclusion
that not only is their base easily manipulated,
they also don't understand basic civics.
Their new method is to create an issue out of whole cloth.
87,000 armed agents, okay?
That was never going to happen.
That wasn't a thing.
They just made that up and it got reported on
by outlets like Fox.
And now they've done this thing
and made a big show about it in the house.
So when 87,000 agents don't show up,
they're gonna look like they did something.
They won, they stopped the IRS from coming after you,
except none of that's true.
They just made it up.
So all of this is still happening,
just so everybody's clear on that.
The funding's already there.
They're going to start the hires.
Over the next 10 years, they will hire 87,000 more people
and there will be more audits of people
who make more than $400,000 a year.
So literally nothing changed,
except they got a cool little vote and talking point in.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}