---
title: Let's talk about Utah and the Great Salt Lake....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=B1ru0e4tBNU) |
| Published | 2023/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Utah's Great Salt Lake is facing a critical situation with a projected disappearance in five years due to a 73% water loss and 60% of its lake bed exposed.
- To start replenishing the lake, drastic cuts of about 50% in water usage in the Great Salt Lake watershed are necessary, a scenario deemed unlikely.
- Despite the economic and health risks associated with a dry salt lake, people are hesitant to make the required water consumption cuts.
- Utah has an above-average snowpack this year at 170%, providing a unique chance to address the crisis by diverting the excess water to the lake.
- Acting now can't fix the issue entirely but will buy time for mitigation efforts, yet failure to seize this chance might lead to irreversible consequences.
- Urges Utah residents to contact their representatives and state officials promptly to implement a plan before the snow melts.
- Proposals like pipelines from other sources are unrealistic; the focus should be on reducing water consumption across multiple areas, a tough but necessary decision.
- Emphasizes the urgency of the situation, as once the lake dries up, there's no turning back.
- Calls for action from Utah residents to raise awareness and push for immediate measures to save the Great Salt Lake before it's too late.

### Quotes

- "The time to fix this is now, not five years from now."
- "If they miss it, it's probably done."
- "This is their chance."
- "It isn't part of the culture war."
- "Y'all have a good day."

### Oneliner

Utah faces a critical five-year deadline to save the Great Salt Lake by making drastic water usage cuts amidst a unique snowpack bonus, requiring immediate action from residents and officials.

### Audience

Residents of Utah

### On-the-ground actions from transcript

- Contact your representatives and state officials to push for immediate action to address the Great Salt Lake crisis (suggested).
- Raise awareness in your community about the urgent need to save the lake and the importance of making water consumption cuts (implied).

### Whats missing in summary

The urgency and critical nature of the situation regarding the Great Salt Lake crisis and the potential for irreversible consequences without immediate action.

### Tags

#Utah #GreatSaltLake #WaterCrisis #EnvironmentalAction #CommunityInvolvement


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Utah and the Great Salt Lake.
Most of what we're going to go over is like much of the news
that we've been covering about water supplies in the United States.
It is not good news.
However, in this case, there is a little bit of good news
if it's acted upon, if the information is applied
and Utah acts, there's some good news here.
But before we get to that, what's the situation?
The new survey suggests that the Great Salt Lake
will be here for about five more years.
That's it.
After that, it's gone.
The time to fix this is now, not five years from now.
Currently, the lake has lost 73% of its water,
and 60% of its lake bed is exposed.
These are really, really bad numbers.
In order to get to where the lake starts to replenish itself,
the cuts that would have to be made to water usage in the Great Salt Lake
watershed are about 50%, about half.
I'm going to go ahead and call this now.
It's not going to happen.
That won't happen.
People still have not accepted the realities yet.
So I find it incredibly unlikely that cuts of that nature will be made.
So that's even more bad news.
People just aren't ready for it yet.
It doesn't matter.
These things are still happening.
Even though a dry salt lake would lead to about $1.7 to $2.2 billion
per year in economic damage, they won't be willing to make the cuts.
And that's just the money.
The lake bed, the particles that would get blown around
in the dust from the wind, that's not good stuff either.
Really bad for the respiratory system.
OK, so what's the good news?
There's a chance.
For once, there's actually the resources
to correct this issue if they act and act now.
This year, Utah has had an above average snowpack,
like wildly above average.
Right now, they're sitting at 170%.
They have to figure out a way to get the stuff from point A to point B.
That's what matters.
And they can help shore up the water in the lake.
Is it going to fix it all?
No, but it will buy more time to mitigate.
This is the chance.
This is the chance.
If they miss it, it's probably done.
So if you're in Utah, now's the time
to be calling your representatives.
Now's the time to be calling state officials,
because they're going to have to do this before the snow melts.
They're going to have to figure out a plan,
and they're going to have to do it quickly,
and they're going to have to get it in action.
Other than that, the other ideas that have been floated around
include pipelines.
One I saw was so old that the idea
was to run a pipeline from the Colorado River.
That's not going to happen.
They don't have it to give up.
We're going to have to make those water consumption cuts.
We're going to have to make them in a lot of places.
I know that's not what people want to hear,
but it's one of those things that is going to happen.
The snowpack gives Utah just an amazing opportunity
to start to correct this issue.
And they don't have a lot of time.
I mean, once it's dry, it's dry.
So again, not a lot of calls to action on this channel.
If you're in Utah, yeah, you should probably
be sending emails and calling people,
because this is one of those things.
It isn't part of the culture war.
It's not headline grabbing.
And in Utah, that's what's going to matter.
This is going to be one of those things that is far more
devastating than anything this state is looking at right now,
and they're probably going to ignore it.
This is their chance.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}