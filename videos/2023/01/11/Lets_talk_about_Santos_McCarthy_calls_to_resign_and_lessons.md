---
title: Let's talk about Santos, McCarthy, calls to resign, and lessons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HV6F8E4jl5o) |
| Published | 2023/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing ongoing developments involving Santos and the new representative.
- Local GOP calls for the representative to resign and not be seated on committees.
- Outrage emerged after news that the representative or his aide impersonated McCarthy's chief of staff for funds.
- Republican Party leaders and committees showed they can tolerate lies, except when it comes to money.
- Lesson learned: Republicans can lie to constituents, voters, and supporters as long as they don't "mess with the money."

### Quotes

- "They can totally lie to you. They can lie to their constituents. They can lie to their voters. They can lie to the people who support them. Just don't mess with the money."
- "It's totally okay with them if your representative, your senator, your elected official lies to you and misrepresents himself to you, but they better not mess with the money."

### Oneliner

The Republican Party tolerates lies as long as money isn't compromised, revealing a stark truth about GOP priorities.

### Audience

Republicans

### On-the-ground actions from transcript

- Confront establishment Republicans about their tolerance for lies from representatives (implied).

### Whats missing in summary

The full transcript provides a detailed insight into the Republican Party's priorities and the consequences of lying, especially in relation to financial interests.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk a little bit more about Santos
and the ongoing developments involving
that new representative.
His local GOP has called for him to resign.
McCorthy is apparently under the belief
that he shouldn't be seated on any committees.
The fallout from the recent revelations
do appear to be heading in the direction of him
no longer being a representative.
Now, there's an important lesson for everybody to notice here,
and this is incredibly important when it comes to members
of the Republican Party.
It is something that they really need to notice
and pay attention to.
This occurred only after news broke
that he broke the real rule, scamming rich people.
The news about his less than accurate statements,
those have been out for...
that's been out for a while.
The calls to resign weren't there.
The outcry, the outrage that is just pouring out of the GOP,
that didn't exist until news broke
that he or his aide impersonated McCarthy's chief of staff
in order to get funds from wealthy Republican donors.
That is when the outrage emerged.
That's when it became a real problem.
So from the GOP standpoint, from the leadership,
from every level, what it showed is that they can totally...
a representative, somebody in the Republican Party,
they can totally lie to you.
They can lie to their constituents.
They can lie to their voters.
They can lie to the people who support them.
Just don't mess with the money.
That's the lesson that should be learned by everybody here.
That's what should be coming out.
It's not that the Republican Party has finally decided that,
oh, this is enough.
It's that he broke a different rule.
He messed with the money.
He messed with those wealthy donors
that support a whole bunch of Republicans.
That's what caused the outrage.
That's what caused the calls for him to resign.
That's what caused everything.
Make no mistake about it.
If you're a Republican, what this incident says
is that the establishment Republicans,
those people in power, those politicians,
those people who control the GOP committees,
all of these people, it's totally okay with them
if your representative, your senator,
your elected official lies to you
and misrepresents himself to you,
but they better not mess with the money.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}