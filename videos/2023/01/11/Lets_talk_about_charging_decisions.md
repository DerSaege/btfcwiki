---
title: Let's talk about charging decisions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5kafl0rZ8WQ) |
| Published | 2023/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the anticipation around charging decisions involving Smith in the coming weeks.
- Emphasizing the uncertainty regarding the timeline for these decisions.
- Mentioning the federal government's typical approach to prosecuting conspiracy cases.
- Pointing out the lack of substantial information in current reporting on this matter.
- Noting Smith's proactive nature and experience in handling such cases.
- Speculating on the potential speed of progress based on Smith's actions.
- Drawing attention to the significance of Smith bringing on two experienced prosecutors.
- Stating that the speculation until the decision is made is just that – speculation.

### Quotes

- "Possible is a key part."
- "Weeks can mean a lot of different things."
- "He brought on two other prosecutors."
- "All the talk until that decision is made, well, it's just a thought."
- "I don't have much to go on with it."

### Oneliner

Beau addresses the uncertainty surrounding charging decisions involving Smith, stressing the importance of not relying too heavily on speculation until a decision is made.

### Audience

Observers, Analysts, Reporters

### On-the-ground actions from transcript

- Monitor updates on the situation involving Smith (implied)

### Whats missing in summary

Insight into the potential consequences of the charging decisions.

### Tags

#Decisions #ChargingDecisions #Prosecution #Speculation #LegalSystem


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about decisions and Smith
and a question that I got.
And it basically said, hey, I'm really
surprised you haven't talked about the fact
that Smith is going to make charging decisions
in the next couple of weeks.
Yeah.
I mean, that tone and that reporting is widely circulating.
But here's the thing.
Based on the information I have, what occurred
was people familiar with the matter
said that Smith was working quickly
and that it was possible there would be charging decisions
coming in the coming weeks.
Possible is a key part.
And weeks, is it two?
Is it six?
Is it 16?
We don't know.
I know that this is getting a lot of press
right now, this reporting.
But there's not really much to it.
There's a lot of information we already know.
We knew Smith was working quickly.
I mean, he was engaged in filings before he even
made it back to the states.
So we knew he was high initiative.
We knew he was taking it seriously.
But this reporting that's coming weeks,
there's not really much there to go on.
And then there's the other side to this,
which is normally when you're talking about a large conspiracy
like this, the federal government
prosecutes in rings, smaller and smaller rings.
And they start on the outside.
So even if there was a charging decision to come next week,
it could be about people far removed from the person
that you're actually concerned about,
the decision that everybody's waiting on.
I just don't have a lot to go on based on that reporting,
because there's the qualifier of possible.
And then weeks is not a couple weeks.
That is not how it was phrased.
I know as the reporting moved on,
that's how they made it seem.
But it just said weeks.
And weeks can mean a lot of different things.
But as I rain on everybody's parade there,
I do want to point something out.
And this is something that I've said before,
but it's worth reminding everybody.
Smith had a pretty amazing assignment
that he left to come do this.
He is known as somebody with a lot of initiative
and very, very much a go-getter type of person.
A lot of experience in this type of thing.
So I don't think he would leave the gig that he had
and come to the US to take this position to sit on his hands.
I expect it to move quickly,
but I don't know what's going to happen.
I think a much more telling sign
is not the information that came out in that report,
is not the information that came out in that reporting,
but the fact that he brought on two other prosecutors,
both from the...
My understanding is that both of them
are from the public integrity section.
So these are people who have experience
prosecuting the exact type of cases
that would start this off.
And they were brought on, I want to say, within the last week.
That to me is a far greater sign
that things are progressing than the quotes.
I just don't see much there.
Not to say they're wrong.
I just don't have much to go on with it.
There's a lot of speculation that is going to occur
until that decision is made.
But it's important to keep in mind
that all the talk that goes on until that decision is made,
well, it's just a thought. Anyway, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}