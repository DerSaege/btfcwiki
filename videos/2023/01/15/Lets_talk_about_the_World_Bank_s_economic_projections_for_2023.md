---
title: Let's talk about the World Bank's economic projections for 2023....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6U4W7fxkoQM) |
| Published | 2023/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of the projections for the global economy in 2023 by the World Bank.
- Global economy expected to expand by 1.7%, barely dodging recession.
- Lowest projections in 20-30 years, except for the 2008 financial crisis and the worst of the 2020 global pandemic.
- The US economy also expected to barely dodge recession with a growth projection of around 0.5%.
- Potential threats to the economy include a resurgence of global public health issues impacting supply chains and Russia's invasion of Ukraine.
- If supply chains are affected, it could erase the projected growth in the US.
- Russia's victory in Ukraine could lead to economic consequences, such as increased costs, especially for food.
- Despite potential risks, current projections indicate that the US and global economy will avoid recession for now.
- Uncertainty remains due to various unpredictable factors.
- The projections are based on the assumption that things continue as they are now.

### Quotes

- "The global economy will expand by 1.7%, that's really low."
- "The US will grow by roughly half a percent, that's low."
- "If Russia wins, your pocketbook, your checking account, it takes a hit."
- "Y'all have a good day."

### Oneliner

Beau provides an overview of the World Bank's projections for the global economy in 2023, showing minimal growth but potential risks ahead.

### Audience

Economic analysts, policymakers

### On-the-ground actions from transcript

- Monitor global public health issues and their impact on supply chains (implied)
- Stay informed about the situation between Russia and Ukraine (implied)

### Whats missing in summary

Insights on potential strategies for mitigating economic risks and preparing for possible downturns.

### Tags

#WorldBank #Economy #GlobalOutlook #Projections #RiskFactors


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about the economy for 2023,
the world economy, and what the World Bank is projecting
is going to happen.
We'll start with the good news.
The good news is it appears that the global economy will
dodge recession, but barely, barely.
The projection for 2023 shows a 1.7% increase.
So the economy will expand by 1.7%.
That's really low.
I want to say these are the lowest projections
in the last 20 or 30 years.
The exceptions to that would be the 2008 financial crisis
and the absolute worst part of the global pandemic
back in 2020.
1.7% is not good, but it is dodging a recession.
So there won't be a contraction.
The United States will also dodge recession, but barely.
The World Bank is suggesting that the US economy
will grow by roughly half a percent.
That's low.
But given the fact that there are a lot of estimates
suggesting that we're going to run flat or actually go
into recession territory, it's better than a lot
of the other ones out there.
So this is relatively good news when it comes to the economy.
What's the bad news?
There's a lot of wild cards still.
These estimates are based on the status quo and everything
going the way it is right now.
Things that can upset that would be a resurgence
of our global public health issue.
It doesn't really matter where it happens.
If it impacts the supply chains, it
will lower the global economy.
And if it hits supply chains impacting the United States,
that half a percent increase, well, it could go away.
The other big wild card is Russia's invasion of Ukraine.
If that conflict was to widen, that would obviously be bad.
If Russia was to win, that would be bad.
If Russia wins, your pocketbook, your checking account,
it takes a hit.
Things start to cost more because of that.
Particularly food.
So there are a few things that can eat into that half a percent
increase.
But as it stands right now at this moment,
it appears, according to the World Bank,
that the US will dodge recession.
And that overall, the global economy
will dodge recession, which there were a lot of people
worried about this, and with good reason.
You can see how close the numbers are.
So the wild cards are still yet to be determined.
But starting off the year, it looks like growth.
Just not a lot of it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}