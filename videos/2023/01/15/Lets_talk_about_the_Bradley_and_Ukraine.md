---
title: Let's talk about the Bradley and Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-4tDezCRUhI) |
| Published | 2023/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is providing Bradleys, infantry fighting vehicles, not tanks, to Ukraine.
- Older individuals may have negative opinions about the Bradley due to its design process.
- Despite having a cannon and being armored and tracked, Bradleys are not tanks.
- Americans may expect Ukraine to use the Bradleys similar to how the US does, but Beau doesn't foresee that happening.
- The Bradleys give Ukraine an advantage at night with superior sensors compared to Russia.
- There are plenty of Bradleys available, so providing them to Ukraine won't harm US readiness.
- The next step after Bradleys could be tanks, but many are hesitant due to concerns about advanced equipment integration.
- Ukraine has proven to be adaptable in integrating new military equipment.
- Logistics, especially fuel requirements for tanks like Abrams, could pose challenges for Ukraine.
- While the Bradley can help Ukraine reclaim territory, keeping it running may be a challenge.
- Artillery can cause devastation, but the Bradley can help Ukraine take and hold ground in a semi-unconventional manner.

### Quotes

- "These aren't tanks and this will come up again later."
- "The sensors on this thing are well beyond what Russia has."
- "I don't buy that. I think they'll be able to integrate them pretty quickly."
- "The Bradley can take ground."
- "It's just a thought."

### Oneliner

The US providing Bradleys, not tanks, to Ukraine gives them an edge, but challenges remain in integrating advanced equipment and logistics.

### Audience

Military policy analysts

### On-the-ground actions from transcript

- Support organizations providing aid to Ukraine (exemplified)
- Volunteer with organizations assisting Ukraine (implied)
- Advocate for peaceful solutions in the conflict (suggested)

### Whats missing in summary

The full transcript provides detailed insights on the military assistance provided to Ukraine and the potential challenges and benefits associated with using Bradleys in the conflict.

### Tags

#Military #Ukraine #Bradleys #Logistics #Integration


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the Bradley and Ukraine and what we can expect.
So let's start with this.
The United States is providing Bradleys to Ukraine.
If you were to type in Bradley and pull up the images,
you're going to get something that looks like a tank.
It's not a tank.
This is important to some politicians, diplomats, and military Twitter.
It's not a tank.
It's an infantry fighting vehicle or an armored fighting vehicle.
I know it looks just like a tank.
It's not.
It's technically a troop transport.
So that's one aspect to keep in mind.
These aren't tanks and this will come up again later.
The next part of this is if you are older or the people you're talking to are older,
you may have people say pretty negative things about the Bradley.
Most of that has to do with its design process.
If you ever see like a sketch of what the Bradley was supposed to be and then compare
it to what exists, you will completely understand every Pentagon project ever.
There are some slight differences.
So they're not tanks even though they have a cannon.
They're armored.
They're tracked.
I think that most Americans, especially those who are familiar with military affairs, are
expecting these to be used the way Americans would use them.
I don't foresee that happening.
This gives Ukraine a pretty heavy edge at night and it gives them a speedy armored vehicle.
I wouldn't expect them to be used the way the US might.
Did y'all hear that?
Thunder ran from one side to the other.
The sensors on this thing are well beyond what Russia has.
So it's going to be a pretty formidable vehicle.
Now one of the big questions that's going to come up, do we have enough of these to
give away?
Yeah, tons.
There are tons, hundreds sitting around collecting dust.
This is not going to harm US readiness.
In fact, there are some people asking why we gave them so few.
The next progression in this when it comes to providing assistance to Ukraine, the next
step would be tanks because this is not a tank.
It's like a tank, but it's not.
The next step would be tanks.
This is kind of a line that a lot of politicians and diplomats don't want to cross.
Those who are familiar with military logistics, they have reservations as well because traditionally
when you're supplying equipment to a country, you don't want to give them something that
is so advanced.
They have a hard time integrating it into their way of fighting.
Ukraine has proven itself to be wildly adaptable and basically can kind of take anything and
fit it into what they're doing.
So that one is probably gone.
Now as soon as this happens, as soon as the Bradley starts getting used in a widespread
manner there, there are going to be people pushing for Abrams.
Just remember the logistical train that has to go along with that.
That is wild.
The part about it being advanced, yeah, I do not have concerns about that anymore.
Ukraine has shown that it can integrate anything really.
But the fuel, because remember the Abrams doesn't run on normal fuel, it runs on jet
fuel.
So the fuel will be an issue and there's a lot of logistics with that thing.
So I know that that's the next logical step is to provide them some of those.
You'd have to figure out the logistics first.
The people who are going to talk about Ukraine not being up to running them and all of that
stuff, I don't buy that.
I think they'll be able to integrate them pretty quickly.
Keeping them running, that's something else.
I don't know about that.
But this is a vehicle that will definitely help and it's something that they can use
to take back some dirt.
Really can.
You know, the equipment that has been provided, particularly the artillery, stuff like that,
it can cause a lot of devastation on the Russian side.
But artillery doesn't take ground.
The Bradley can.
And I have a feeling that Ukraine will use these in a semi-unconventional sense and replicate
some successes they've had in other places.
So anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}