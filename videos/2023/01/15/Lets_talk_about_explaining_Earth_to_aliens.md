---
title: Let's talk about explaining Earth to aliens....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VURKY-447dk) |
| Published | 2023/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces himself as Beau and explains that he was chosen to answer questions about Earth for interstellar visitors.
- Describes society as being organized around the accumulation of paper which serves as a token for exchanging goods and services.
- Mentions that some people live very comfortably while others have dirt floors, with wealth distribution determined by luck and birth circumstances.
- Talks about technological advancement being primarily driven by war against our own species for resources, books, and historically, skin color.
- Touches on how diseases are more prevalent in areas with less paper and how wealthier areas often ignore new diseases due to better infrastructure.
- Expresses a need for cleaner energy methods to prevent the planet from being destroyed by current energy production practices.
- Declines the responsibility of explaining human civilization to a more advanced species following jokes about him being nominated for first contact.

### Quotes

- "We have kind of decided that we as a society want some people to live very comfortably."
- "What drives our technological advancement? War, war mostly, war?"
- "So if you all could distribute that. Where are you all going? I'm sorry, you nuke from orbit?"
- "I do not want to have to explain the nature of human civilization to a species that is capable of actually getting somewhere."
- "Y'all have a good day."

### Oneliner

Beau explains Earth's society organized around paper, driven by war for advancement, and pleads for cleaner energy distribution while declining the responsibility of explaining civilization to advanced beings.

### Audience

Interstellar visitors

### On-the-ground actions from transcript

- Distribute cleaner energy methods for Earth's sustainability (suggested)

### Whats missing in summary

The full transcript provides a humorous yet thought-provoking look at Earth's societal structures and behaviors, urging reflection on the consequences of our actions and the need for change.

### Tags

#Society #Earth #War #CleanEnergy #Responsibility #InterstellarVisitors


## Transcript
Well, howdy there, interstellar people.
It's Bo again.
So I was chosen to answer any questions
you might have about Earth now that you're here
and try to familiarize you
with the planet that you are visiting.
Yeah.
So why don't we just go ahead and start off with questions
and we'll go from there.
The general structure of our society.
So generally speaking, we have organized our society
around the accumulation of paper.
And it's like a token that you can exchange
for goods and services.
Sometimes the paper doesn't actually even exist.
It's like digital.
But that is how we engage in commerce.
And no, no, no, no.
The paper doesn't actually have any,
I mean, it has like pretty drawings on it.
And the drawings vary from one administrative section
to the next.
And that's kind of cool.
And they have different values
based on which administrative section printed them.
No, no, they're not actually, there's nothing.
It's yeah, just paper.
Well, I mean, sometimes there's foil.
No, the foil isn't used for anything either.
It's just paper, high quality paper.
Okay, so why?
Yeah, that's a good question.
So we have kind of decided that we as a society
want some people to live very comfortably.
Some people even get to live like very luxurious lifestyles
and other people, they have dirt floors.
No, no, no, no.
Being part of the poor group of people,
that's not a punishment for non-participation
in society or anything.
Okay, so it's kind of determined by luck.
Do you have this concept?
So it's determined based on which administrative section
you're born in and who you're born to.
There are some people who advance via merit,
but there's not a lot of those.
Those just kind of get held up as examples
to kind of keep everybody else in line.
Okay, so the administrative sections,
some of them have a lot of the paper
and they use that to keep the other areas without paper
and therefore they have a lower standard of their life.
Yeah, yeah, I know.
Weird is a good word to describe it.
Yeah, that's strange.
What drives our technological advancement?
War, war mostly, war?
No, no, not against other species.
It's against ourselves and as I say that,
I realize it's not against us.
It's against our species, it's against ourselves.
And as I say that, I realize it's not actually better, is it?
Yeah, okay.
No, yeah, so we fight wars amongst ourselves.
Why?
So yeah, three reasons mainly.
One is paper, you know, resources around the planet
have a value based on the paper
and we fight over those a lot.
And then, what?
No, no, no, we actually have plenty of resources.
If we were to cooperate, yeah,
we could totally take care of everybody.
But we choose not to do that.
Yeah, so that's one reason.
Another reason would be books.
Yeah, yeah, books.
The books form the basis for a religion,
a set of spiritual beliefs.
No, no, you would think that the values would be drastically different.
More or less, they pretty much all kind of teach the same thing.
So why do we fight?
Different characters and symbols?
And then the last one is historically,
this isn't as much of a problem today,
but historically, pigment, our tone, our skin, the way we look.
No, no, y'all are pale.
Don't worry about that.
And again, that's becoming less of an issue.
But yeah.
Disease.
Yeah, so in the areas, the administrative sections,
where there's a lot of paper,
in those sections, diseases are less common.
Yes, that's true.
No, eradicated is not a thing.
In fact, it's weird.
Even in the areas that don't have a lot of disease,
the wealthier areas, because of having better access to infrastructure,
so if a new disease emerges, a lot of people just pretend like it doesn't exist.
No, of course that doesn't.
That's not productive.
It does not help.
I understand that.
Yeah, so that's ??? I'm sorry.
We're not ready.
Yeah, I mean, I kind of felt our resume was a little light walking in here today.
Yeah, yeah, no.
We're not ready.
Yeah, I got that.
You're going to check back in a thousand years.
Yeah, yeah.
Okay, wait, wait, wait.
About that, about that.
So we create a lot of our energy by burning stuff that releases ???
look, Morbo, even through your visor, I can feel your look.
Yeah, I know.
It doesn't make any sense.
It would be great if you guys could, like, give us some cleaner methods,
not even methods because we actually have cleaner methods of producing energy.
We just don't.
Could you all build them and distribute them for us?
Because otherwise a thousand years from now, we very well may not be here.
Yeah, we totally know methods of producing cleaner energy.
So the people with the most paper, a lot of them make their paper.
They get more paper based on producing energy using methods that kill the planet.
Yeah.
Yeah, yeah.
So if you all could distribute that.
Where are you all going?
I'm sorry, you nuke from orbit?
So after that recent video talking about first contact,
a whole bunch of people made little jokes about nominating me to be the person to do that.
I don't think that's a good idea.
I don't want that responsibility.
I do not want to have to explain the nature of human civilization
to a species that is capable of actually getting somewhere.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}