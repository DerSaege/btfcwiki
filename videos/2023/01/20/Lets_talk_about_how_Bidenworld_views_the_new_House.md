---
title: Let's talk about how Bidenworld views the new House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=laZc-qO_xSU) |
| Published | 2023/01/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party's focus on social media engagement has led to underperformance in elections.
- Beau predicts real problems for the Republican Party in 2024 due to extreme positions taken by House members like Gosser, Green, and Boebert.
- Team Biden reportedly views the new House appointments with glee, considering it a political gift.
- The White House and Biden allies are gearing up to handle the Republican Party's planned hearings, which they see as baseless attacks.
- Beau believes that the Republican Party's focus on hearings about Biden's son and the Afghanistan withdrawal will backfire.
- The hearings are expected to set the tone for the 2024 campaign and may lead to members not getting reelected.
- Beau suggests keeping an eye on the House of Representatives for upcoming events.

### Quotes

- "The Republican Party seems to have fallen into the idea that social media engagement will lead to electoral success, despite it leading to underperformance."
- "The White House views the new House appointments with glee, considering it a political gift."
- "The hearings are expected to set the tone for the 2024 campaign."
- "It's going to be a wild couple of years in the House."
- "Y'all have a good day."

### Oneliner

Beau predicts problems for the Republican Party in 2024 due to extreme House members, while Team Biden sees their appointments as a political gift, gearing up for baseless hearings that may backfire and set the tone for the upcoming campaign.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Watch the House of Representatives for upcoming events (suggested)
- Stay informed about the political landscape and upcoming hearings (suggested)

### Whats missing in summary

The full transcript provides additional insights into the dynamics between the Republican Party and Team Biden, offering a comprehensive view of the potential challenges and strategies leading up to the 2024 campaign.

### Tags

#RepublicanParty #HouseAppointments #BidenAdministration #PoliticalStrategy #2024Elections


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Biden
and the White House and how the White House views
the new makeup of the House of Representatives,
particularly in relation to the oversight committees.
We have talked on this channel at length
about how the Republican Party seems to have fallen
into the idea that social media engagement
among their most energized base
will somehow lead to electoral success,
despite it actually leading to them
consistently underperforming.
I have expressed my opinion that the House of Representatives
and a lot of the people that have been given prominent roles
very well may lead to the Republican Party
having real problems in 2024, because they're
going to double down on their more, let's say,
flamboyant takes.
And they're going to become more extreme.
And they're going to try to outdo each other.
And it's going to lead to this vicious cycle.
And this is going to alienate independent voters.
That's going to cause problems.
Now there is reporting detailing how Team Biden,
Biden world, feels about these new appointments,
particularly those of like Gosser and Green and Boebert
and people like that.
The reporting suggests that they weren't happy.
They were giddy.
That was the term used.
Full of glee.
They texted each other, calling it a political gift,
because the White House seems to agree.
And the Republicans aren't doing themselves any favors
as they talk about the hearings they plan to hold.
Congressional hearings, they can really cause problems
for an administration.
But not if they're baseless.
It's really easy to defend against something
that is baseless.
And as these committees run amok, amok, amok,
just drawing up whatever they can come up with
or whatever they heard on, you know, FreedomEagle.ru
or whatever website they're getting their information
from now, it's going to be easy for the Biden administration
to deal with it.
And with the people that have been given
relatively prominent assignments,
it's pretty much a guarantee that the Republican Party
is going to go down these roads with these hearings,
which means the moderates, those in moderate areas,
they're going to have to go on the record
about these hearings, which is going to,
well, it's going to hurt them one way or another.
Either they're not going to be loyal to,
you know, the more extreme members
and it will hurt them among Republicans,
or they agree with them
and it hurts them among the independents.
It doesn't put the Republican Party in a good position.
And the topics that the Republican Party
is currently floating
when they're talking about these hearings,
looking into, you know, Biden's son,
somebody who's not in elected office,
they're going to waste a bunch of money on a hearing,
looking into that while they try not to raise the debt ceiling
and crash the economy because they need to spend less.
It's not going to go over.
Another thing they're reported to be gearing up to look into
is the Afghanistan withdrawal.
Yeah, let me tell you what isn't going to go the way they think.
Now, with the benefit of everything that has occurred
and finding out more and more information,
unless this is an attempt to basically boot Trump
out of the Republican Party,
that hearing is not going to go as planned for them.
They have fallen into their own echo chamber
and they believe the reporting that they hear,
even though for a lot of them,
they may have actually helped create the false narratives,
it's now turned into a loop where they're hearing it
so they believe that's what actually happened.
Divorced from reality is a term that was used
to describe how the hearings are going to run.
And these hearings will set the tone for the 2024 campaign.
The White House apparently already recognizes this.
Biden allies already recognize this,
which means they're going to gear up for it.
They're going to be ready for it.
It should make for, while slightly depressing,
also kind of entertaining hearings in the House.
And if the Republican Party goes the route
that it apparently is just dead set on going,
there may be people who view themselves
as very secure in their positions
who don't get reelected.
And there's also, I'm of the belief that
because of the relationship that exists
between some of the more extreme members
of the Republican Party and former President Trump,
that they may decide that they won't be attending
all of the hearings that they initiate.
I would watch the House of Representatives.
It's going to be a wild couple of years.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}