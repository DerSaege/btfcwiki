---
title: Let's talk about the debt ceiling and credit cards....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Rongpo2TbqQ) |
| Published | 2023/01/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the debt ceiling and how it's being inaccurately framed by Republicans.
- Compares the debt ceiling to a self-imposed limit, not a credit card limit imposed by a lender.
- Points out that the US is nowhere near its actual credit limit and can borrow more.
- Analogy: Debt ceiling is like limiting spending on an already accumulated debt, not new spending.
- Criticizes Republicans for not caring about the debt despite creating a significant portion of it.
- Mentions economic instability and the risk of default due to hitting the debt ceiling.
- States that not raising the debt ceiling could lead to economic calamity and instability.
- Questions why the Republican Party is obstructing and risking economic stability.
- Emphasizes that the Republican Party's actions are about leverage and obstruction, not fiscal responsibility.
- Concludes that Republican concerns about the debt are not genuine and it's all a show for political gain.

### Quotes

- "It's not a credit limit. It's a self-imposed thing."
- "The idea of a fiscal conservative, that's not a thing anymore."
- "All of this is true. But it doesn't matter."
- "They're using your economic stability, they're putting that at risk."
- "It's all an act, it's all a show to manipulate their more easily manipulated base."

### Oneliner

Beau explains the debt ceiling reality, Republican hypocrisy, and the looming economic instability due to political games.

### Audience

Policy Analysts, Voters

### On-the-ground actions from transcript

- Contact elected officials to demand a resolution to the debt ceiling issue (implied).
- Educate others on the true implications of hitting the debt ceiling and the risks involved (implied).
- Stay informed on economic policies and their potential impacts on the national economy (implied).

### Whats missing in summary

Deeper insights into how the debt ceiling issue could affect everyday Americans financially, especially in terms of inflation and job stability.

### Tags

#DebtCeiling #RepublicanHypocrisy #EconomicInstability #PoliticalGames #FiscalResponsibility


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the debt ceiling.
We're going to talk about how it's being framed, what it is,
what's going on with the whole show that's occurring,
and just kind of go over all of it.
We'll do this for two reasons.
First, we hit the debt ceiling.
Second, it's being framed in a certain way
that I just think is wildly inaccurate,
And I think people need to know that.
You have Republicans right now saying
that the debt ceiling is like a credit limit on a credit card.
No, that's not what it's like at all.
A credit limit is imposed by the lender.
The debt ceiling is self-imposed.
The US credit, it's fine.
The US is nowhere near its actual credit limit.
Can borrow a whole bunch more.
It's not a credit limit.
It's a self-imposed thing.
It's more like if you were to, I don't know,
have a credit card that has $100 balance on it,
and a $10,000 limit, and you're walking into the store
because you want to buy a new computer.
And as you walk in, you say, I'm only going to spend $600.
That $600 limit, that's what the debt ceiling is.
It's self-imposed.
It's not real.
It's been raised, I don't know, 80 times or so since the 1960s.
But even that analogy with the credit card,
that's not really accurate either.
It's closer, but it's not really what it is,
because this isn't new spending.
It's debt.
old spending. It would be more like if you had a credit card for four years and
you ran it up, I don't know, 7.8 trillion dollars. You added 7.8
trillion dollars to the debt on the credit card and then when the bill came
in you're like, no I don't want to pay that. That's more what it's like because
that's what happened. The national debt increased 7.8 trillion under Trump and
now they don't want to increase the artificial limit once they ran it up.
Big surprise, the person who describes himself as the king of debt ran up a
bunch of debt. The idea of a fiscal conservative, that's not a thing anymore.
They don't care about the debt. They don't care about the spending. It's a
facade. Now normally when you bring up the 7.8 trillion, the next thing that
happens is somebody's like well you know Obama. Obama ran up the debt eight point
whatever I can't remember what it is we're just gonna say nine trillion
because it doesn't actually matter and they try to illustrate that Obama ran
the debt up more the national debt increased more under Obama. I mean yeah
Yeah, it's true, but what's the difference?
And right now people are probably thinking, you know, Obama had two hot wars going on,
the Great Recession, there was a lot of emergency spending, all of that stuff.
And yeah, that's true, but Trump, even though his leadership wasn't there, had the pandemic.
And there was a lot of spending associated with that.
So that's not really fair.
What's the real difference?
wasn't a one-term loser. He got reelected. Trump ran up almost the same amount in half
the time. That's the difference. The total national debt is 31 point something trillion.
7.8% of that was from Trump's four years.
A whole lot of it was due to the tax breaks, you know, for the billionaires that will be
at your expense later. And some of it was legitimate emergency spending due to COVID.
So, it's not a credit limit, the US is nowhere near its limit, it's artificially imposed,
there's no such thing as a fiscal conservative, that's not a thing that they actually care
about anymore, and the debt, a whole big chunk of it, was ran up by Trump.
All of this is true.
But it doesn't matter.
We hit the debt ceiling.
So what does that mean?
Right now, the US is cooking the books, shuffling stuff around, putting IOUs and retirement
accounts and stuff like that.
That will probably work until June is the estimate, at which point the US might default.
If the US defaults, economic calamity.
But even before then, there will be economic instability because the US economic system
is based on faith. Not raising the debt ceiling, having to put in IOUs and cook the books,
it shakes that faith. Could have implications for the stock market. Could have implications
for a lot of the economic situation in the United States.
The Republican Party has been up there not long and is already damaging the economy.
Big surprise.
Now one of the other things that tends to follow when you explain this is then why are
they doing it?
Why is the Republican Party doing this?
Because let's be real.
Do you think that the Republican Party is going to hand Biden a balanced budget?
Do you think they're going to create a budget surplus?
Think that's what they're going to send up there to him to sign and give him a massive
political win and pretty much ensure his reelection?
Of course not.
They're not going to do that.
That's going to continue to go up because the budget that Congress is going to send
is going to have a deficit.
So the debt will continue to rise.
They're not going to send up a balanced budget and give him the first surplus since, what,
Clinton?
Clinton's budgets?
Seems super unlikely.
It's not about that.
It's about a show.
It's about obstructing.
It's about trying to get their projects into the budget, it's about leverage.
They're using your economic stability, they're putting that at risk, they're putting the
US economy at risk for themselves.
And to paint the picture that they care about fiscal responsibility, when it is them saying
not to pay the bill, doesn't make sense.
That's just not true.
About the only thing they're right about here and that they're being honest about is that
yeah, there's a bunch of spending that could be cut.
That's true.
That is absolutely true.
But they're not really concerned about the national debt because they're not going to
send up a balanced budget, which means they're not actually going to try to reduce the debt,
Which means the debt ceiling has to be lifted, no matter what.
It's all an act, it's all a show to manipulate their more easily manipulated base.
That's what it is.
It's all about obstruction, trying to cause a little bit of economic instability, hope
that people blame Biden, get some leverage for their own programs that they're going
to spend, which will add to the national debt that they're pretending they care about right
now.
It's all an act.
fall for it.
Anyway, it's just a thought.
It's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}