---
title: Let's talk about what liberals get wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3N8MWVg7u84) |
| Published | 2023/01/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Emphasizes the importance of understanding vocabulary terms when discussing a subject to be taken seriously.
- Points out how not knowing basic terms can derail a conversation, especially when discussing firearms.
- Mentions the confusion around the term AR and how it's commonly misinterpreted as automatic rifle or assault rifle.
- Explains that AR actually stands for Armalite Rifle, derived from "A" for Armalite and "R" for rifle.
- Criticizes those who get the AR acronym wrong, stating it's not just limited to Armalite Rifle.
- Challenges the notion that AR designates civilian use, citing examples like AR-22 and 23 designed for training purposes in a Mark 19.
- Notes the historical context of Armalite selling the AR-15 to Colt and subsequently naming their own release as M-15.
- Condemns gun enthusiasts who argue over acronyms instead of engaging in meaningful dialogues on pressing issues like school shootings.
- Urges individuals to focus on substantive policy actions rather than just "owning the libs" on social media.
- Encourages constructive engagement in real-world issues rather than getting caught up in trivial online debates.

### Quotes

- "If you don't know the vocabulary, you have no business talking about this subject."
- "Owning the libs on social media is not actually policy."
- "Engage with real-world, substantive policy actions."

### Oneliner

Understanding basic vocabulary is key to being taken seriously in debates, particularly on contentious topics like firearms; focus on meaningful policy actions over trivial social media debates.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Correct misinformation on firearms terminology (implied)
- Engage in constructive dialogues on gun control and school safety measures (implied)

### Whats missing in summary

Deeper insights on the impact of misinformation in firearms debates and the urgency of addressing real issues like school shootings.

### Tags

#Vocabulary #Firearms #Misinformation #Policy #Debates


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about what words mean
and vocabulary terms and why it's
important to understand the vocabulary when you're
discussing a subject.
Because oftentimes, if you don't know the vocabulary, well,
then people may not take you seriously.
They may not accept anything else you say,
because you didn't know the basic terms, right?
And this is something that happens a lot with liberals
when they're talking about firearms.
It's a big thing.
I mean, you see the Second Amendment people show up
every time, because what happens is somebody says AR,
and then they use the term automatic rifle or assault
rifle, and that's not what that means.
And you get an entire comment section
full of people who are saying it really means armalite rifle.
A for armalite, R for rifle.
That's what it means.
And if you don't know that, you have no business
talking about this subject.
And it ends up derailing any other conversation,
because, I mean, let's be real.
If you don't know the vocabulary,
you have no business talking about it.
And I'm certain that it's the marketing that
has a lot to do with this.
You've heard these terms associated,
so you just buy into it, right?
And right now, there's a whole bunch
of right-wingers nodding their heads
and laughing, because they know that this is true,
and they know that's how it works.
They show up.
They see that comment.
They correct it, and they never engage further
in the discussion, because they know what it stands for,
armalite rifle.
That's not what it stands for either, by the way.
The AR-24, made by Armalite, is a pistol.
AR-9, AR-17, shotguns.
It doesn't stand for armalite rifle, guys.
Y'all are wrong.
If you don't know the vocabulary,
I guess nobody needs to take anything you say seriously,
right?
And I know right now somebody's going to say,
well, see, the pistol and maybe those shotguns,
maybe they have rifling.
And that's why they're calling it that.
No, no.
I mean, sure, we can pretend that's true.
But then you have to explain the AR-22 and 23.
Those are devices that are designed
to be inserted into a Mark 19 for training purposes.
They're not even weapons.
And that also throws out the other thing
that is commonly believed, that the AR designates
that it's civilian, because the AR-22 and 23 are
designed to go into a Mark 19.
I'm fairly certain there aren't a lot of automatic grenade
launchers on the civilian market.
Also worth noting that when Armalite sold the AR-15 to Colt,
when they re-released their own, they designated it the M-15.
That's not what it means.
Y'all are just making stuff up.
You fell for the marketing, just like they did, right?
But based on that, it doesn't matter.
If you're literally a gunsmith, you
don't know what you're talking about.
There's nothing you can add to the conversation
because you got this acronym wrong.
You tried to force an acronym.
Probably just stands for the first two letters in Armalite,
in case you're curious.
It's just a product code.
Your takes don't matter, because you didn't know this.
You didn't know this piece of trivia.
You didn't know about the other AR platforms.
So you can go over there and sit with people
who think it means assault rifle or automatic rifle.
See how silly this sounds?
This is you.
This is you for the last 10 years.
This is your contribution to the conversation.
And it's worth noting that the actual conversation
is counting the number of kids that didn't
get to walk out of a school.
And you're over here arguing about an acronym
and getting it wrong.
The gun crowd is its own worst enemy.
Engage with conversations.
Owning the libs on social media is not actually policy.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}