---
title: Let's talk about the T-14's prospects....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_f4GJxRceyE) |
| Published | 2023/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the discrepancy between the T-14 tank's potential on paper versus its real-world limitations.
- Points out the lack of numbers and production delays for the T-14 tank in Russia.
- Mentions the potential design flaws and issues with the T-14 tank that may only become apparent in combat.
- Talks about the logistical challenges of maintaining and repairing the T-14 tank in Ukraine.
- Raises concerns about the T-14 tank's untested design and limited production numbers affecting its effectiveness in combat.
- Mentions the rumor about a major issue with the turret of the T-14 tank.
- Addresses the unique design features of the T-14 tank, such as the unmanned gun and reliance on electronics.
- Indicates that even if the T-14 tanks were deployed, they lack the necessary support and resources to be effective on the battlefield.

### Quotes

- "On paper, this tank is amazing. It is amazing. It can compete."
- "It's not a competitor, because as far as it being a real force on the battlefield, it doesn't really exist yet."
- "The crew is down in like a capsule, an armored capsule inside to improve survivability, which is great."

### Oneliner

Beau breaks down the potential versus reality of Russia's T-14 tank, from production delays to combat readiness, revealing significant limitations.

### Audience

Military analysts

### On-the-ground actions from transcript

- Train mechanics on how to work on new equipment (suggested)
- Ensure availability of spare parts for military equipment (suggested)

### Whats missing in summary

Deeper insights on the implications of untested military equipment in combat scenarios.

### Tags

#Military #Russia #T-14 #Tank #Combat


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the T-14
and
numbers
and logistics
and how things look on paper versus real life and a whole bunch of things.
Because
in a recent video
I mentioned
the idea that Russia really didn't have anything
that could compete with the modern equipment that might be headed to Ukraine from the West.
Can't compete with the Challenger, the Abrams, the Leopard.
They don't have tanks that can compete with these vehicles.
My inbox filled up with people asking about the T-14.
favorite was this will not stand. This is T-14 Erasure. I mean it is T-14 Erasure,
but I didn't erase it. The Russians did. On paper, this tank is amazing. It is
amazing. It can compete. It can outperform Western tanks in a lot of ways. The
The problem is if you want to use it in Ukraine, you have to build it and filled it.
Two things the Russian military has not done.
In 2020, the Russian army was supposed to take possession of 2300 of these monsters.
Currently estimates suggest numbers in the low tens.
number, the 2300, that got pushed back. Supposed to be delivered by 2025. That's
not going to happen either. Last thing I saw is that they were hoping to produce
40 through the year of 2023. I don't think they're going to make that goal
either if they even still maintain it. So they don't have the numbers. They don't
have enough to make a difference. But let's pretend they do. Let's say that
they do. They built them in secret. They have them in a hollowed out mountain
somewhere and they're just sitting there. They've been waiting to put them in for
whatever reason. They're a new vehicle. They haven't been filled it. When a new
piece of equipment comes along, it has bugs, it has flaws, it has problems
inherent with the design, and a lot of them aren't noticed until they're
actually out in the field, once they leave the testing thing and they're
actually out there. And this isn't something that is just a Russian problem.
Every military has this issue. There are things that the engineers overlook, there
There are production issues that they didn't know about.
There's all kinds of stuff.
These types of things will show themselves if these tanks are used.
Then you have the additional issue of it never having been in combat before.
There are big problems that they're not aware of because it's never been battle tested.
In fact, I'll go ahead and tell you there is a rumor, an urban legend, that says one
of the reasons that Russia doesn't seem to be very interested in producing a lot of these
is because they have a major issue with the turret.
That gun up top doesn't really move as well as it should.
I don't know if that's true, but it would explain a lot if it was.
So there will be design flaws.
There will be issues with it.
Then you have another problem, the logistics train.
Everything that we talked about with the Abrams is going to be true of this.
Right now, a tank mechanic, a Russian tank mechanic in Ukraine can fix the tanks.
All the variants, all the variants, because they've worked on them.
The T-14 is new.
They don't know how to work on it.
They don't know how to fix it.
They don't know cool ways to improvise fixes in a pinch.
But let's solve that problem for them, too.
Let's say that they've distributed manuals and trained everybody in secret.
Then they have to have the parts, and they don't have them prior to the war.
to the sanctions. The main Russian tank production facility was capable of
upgrading or turning out 20 to 30 tanks a month. High-end estimate was 20 to 40.
That's less than they're losing. They don't have it. They don't have it. On
On paper, this thing is amazing, but it doesn't really
exist in tangible form yet, outside of a few examples.
Now, the British intelligence believes
that the T-14 will make an appearance in Ukraine
for propaganda purposes, but they won't actually
put it into the field for all of the reasons I just said.
But aside from that, it is brand new, brand new.
Western intelligence would want one.
If they're deployed in any meaningful way, they will become a target.
And then you have one issue that is specific to Ukraine.
The T-14 is a monster.
It's a monster and it's heavy.
When all this started, you remember all those tanks stuck in the mud?
That's going to be a problem for it, too.
The design is innovative, but it hasn't been tested.
Hasn't really been built in significant numbers, either.
Another thing that seems to be an issue, and again this part is rumor, is it has a very
unique design as far as that gun up top.
That's unmanned.
The crew is down in like a capsule, an armored capsule inside to improve survivability, which
is great.
That's a cool design.
But that means it has to rely on electronics and optics.
A lot of the stuff that they need for that, they can't get right now.
So they don't have enough.
Even if they filled the ones they have, they're going to have issues.
They don't have people to fix them.
Even if they did have people to fix them, they don't have the parts.
It's not a competitor, because as far as it being a real force on the battlefield, it
doesn't really exist yet.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}