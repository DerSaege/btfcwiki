---
title: Let's talk about eggs, supply, and demand....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RSRdIMI54Ig) |
| Published | 2023/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The price of eggs in the United States has dramatically increased, with prices rising anywhere from 62% to 104%.
- The industry attributes the price increase to increased demand and avian flu, leading to a decrease in egg production.
- Some individuals suspect price fixing among larger suppliers, artificially inflating egg prices.
- Republicans are trying to connect the increased egg prices to issues at the southern border, sparking outrage.
- Beau points out that demand drives people to seek alternatives like buying cheaper insulin or eggs from across the border.
- He suggests that instead of focusing on increasing enforcement and militarization, efforts should be made to reduce demand for certain goods.
- Beau advocates for shifting strategies to reduce demand and address root causes of issues rather than perpetuating failed approaches.

### Quotes

- "Reduce demand. Apply that strategy elsewhere."
- "Rather than continuing failure after failure after failure, shift the strategy."
- "Reduce demand rather than increase militarization."
- "Demand. There aren't enough people who have to deal with that."
- "The government's doing something wrong, and we should eliminate the need for people to do this."

### Oneliner

The price of eggs in the US rises dramatically due to increased demand and avian flu, sparking debates on supply, demand, and potential price fixing, with a call to shift strategies by reducing demand across various sectors.

### Audience

Policy advocates, activists, consumers

### On-the-ground actions from transcript

- Advocate for policies that aim to reduce demand for goods rather than focus solely on enforcement and militarization (implied)

### Whats missing in summary

The full transcript further expands on the implications of supply, demand, and pricing dynamics beyond the egg industry, urging for a shift in strategy towards reducing demand to address underlying issues effectively.

### Tags

#EggPrices #SupplyAndDemand #PriceFixing #ReducingDemand #PolicyAdvocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about eggs and supply
and demand.
And we're going to kind of branch out from eggs
and talk about other forms of supply and demand as well.
So if you don't know, the price of eggs
has gone up dramatically in the United States,
depending on the source you use, anywhere from,
I think it's 62% to like 104%.
Big jump.
Big jump in the price of eggs.
Now, the industry line, their narrative,
their story on why this is occurring
is increased demand and avian flu.
Does this check out?
Yeah.
On the surface, yes.
Avian flu, it's a thing.
It's happening.
They've lost tens of millions of birds,
which means less eggs at a time of more demand.
Prices go up.
Yeah, that tracks.
Now, at the same time, there is a group
of people who thinks it's a little bit more than that.
And they have sent a letter or a complaint to the FTC.
And it's kind of alleging price fixing
among some of the larger suppliers,
artificially inflating the price.
Does that check out?
I looked.
And yeah, there is kind of a discrepancy
between the amount of birds lost versus the price increases.
Looking at it, it seems a little bit odd.
But to be honest, I don't know enough about the egg industry
to make a real judgment on it.
That's something that the FTC is going to have to do.
Don't expect them to make any kind of comment on it,
because they generally don't comment on anything.
OK, so you have a price increase.
People don't like those, generally.
So there is a ready source of people
to be tapped into to inflame, to provoke, to outrage.
And if you can tie it to one of your key issues,
that's good politics.
Is that happening?
Of course.
Of course.
You have a lot of Republicans right now
trying to tie the price of eggs in the United States
to the southern border.
There is just shock and dismay and outrage from Republicans
right now that Americans are going south of the border
to buy eggs and then bring them back.
Now, something I want to point out before we go any further,
Avian Flu is down there as well, and there
are some pretty stiff penalties for bringing eggs back,
especially if they're not declared.
And they're way more than a carton of eggs.
So just maybe that's not the best course of action.
But the question is, why is the Republican Party
suddenly focusing in on this?
This type of thing has been happening for a while
with like, I don't know, insulin, people's meds.
People will go south of the border
to get them because they're cheaper.
Why wasn't it brought up then?
This isn't a whataboutism.
The answer is simple, demand.
Demand.
There aren't enough people who have to deal with that.
There aren't enough people who need insulin,
who live close enough to the border, who see the impacts.
It's not true with eggs.
More people buy eggs.
So this is something that they can tap into.
They don't actually care that Americans
are in these kinds of financial straits
because they don't care about something that is literally
required to live.
But there's a demand for it.
And because there's a demand for it,
somebody's going to provide it.
And that's what's happening.
And then they're also talking about how there are people who
are apparently not doing this for themselves,
but like doing it for a profit, bringing eggs
across the border for a profit.
Again, just a reminder, the penalties for that
are even more severe, and that brings up something else.
Why would people do it?
Because there's a demand.
If there's a demand, the supply will eventually emerge.
Maybe since they're kind of acknowledging this
with this part, with eggs, maybe it's
time to transfer that logic, which is sound, to other things.
Rather than advocating for billions and trillions
of dollars to be spent on a 60-year failed war,
maybe it would be better trying to reduce demand,
especially if you're also currently talking
about how much money the US spends on worthless things.
The demand exists in the United States.
Somebody's going to supply it.
Somebody will supply it.
When you're talking about drugs, somebody's
going to supply it because there's a demand.
It's just like eggs.
Somebody is going to supply it.
If you want to reduce that, you have to reduce the demand.
The militarization, all of the stuff that they're advocating,
the just draconian sentences, this isn't going to work.
It hasn't.
We've been doing this for decades, and it's a failure.
You have to reduce demand.
When we're talking about eggs and people doing it with eggs,
there's not an outcry to severely punish them.
That's not the Republican stance.
The Republican stance is that the government
is doing something wrong because they've
got to blame this on Biden.
The government's doing something wrong,
and we should eliminate the need for people to do this.
Reduce demand.
Apply that strategy elsewhere.
Apply that strategy to places that have just
caused immense amounts of suffering over the years.
Rather than continuing failure after failure after failure,
shift the strategy.
Reduce demand rather than increase militarization.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}