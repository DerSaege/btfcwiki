---
title: Let's talk about an FBI CI agent getting arrested in New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xKfEV45KmKM) |
| Published | 2023/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Breaking news about a former top FBI agent's arrest on charges related to working with a Russian oligarch with ties to Russian intelligence.
- The FBI did not cover up the arrest, and the charges are serious.
- Inferences are being made that the former agent's involvement in the Trump probe may have impacted the investigations, but there's no solid evidence yet.
- Jumping to conclusions without evidence can be detrimental and worse for Trump.
- While there may be a thorough investigation into the agent's past cases, it's too early to draw definite conclusions.
- The situation could have significant implications but requires more evidence before making definitive statements.
- The story is likely to receive extensive coverage and remain in the news cycle for a while.

### Quotes

- "This is a big deal. Expect a lot of coverage."
- "Jumping to conclusions without evidence can be detrimental and worse for Trump."
- "It's too early to draw definite conclusions."
- "The story is likely to receive extensive coverage and remain in the news cycle for a while."

### Oneliner

Former top FBI agent's arrest on charges related to working with a Russian oligarch sparks inferences about Trump probe involvement, caution urged against premature conclusions.

### Audience

News consumers

### On-the-ground actions from transcript

- Stay informed about developments in the case and critically analyze information provided (implied).
- Avoid jumping to conclusions without substantial evidence (implied).

### Whats missing in summary

Detailed insight into the potential implications of the former agent's arrest and its impact on ongoing investigations. 

### Tags

#FBI #Arrest #RussianTies #Implications #Inferences


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the breaking
news of today.
And we're going to talk about what we know,
what we don't know, inferences, implications,
and jumping to conclusions, and how
if you're going to jump to conclusions,
you should at least jump the right way.
For those who have missed it, who are unaware,
news has broke that a former FBI agent, a top, top FBI agent
who was in charge of counterintelligence in New York
has been arrested.
They have been arrested on charges
of basically working with a Russian oligarch who is,
I don't want to say known, but who
is believed to have some pretty significant ties
to Russian intelligence.
What we know, this is a big deal.
This is a big deal.
Expect a lot of coverage.
This is yet another giant gaping hole
that has been shown in US counterintelligence.
So this is definitely going to get a lot of airtime.
It also comes at a time when the FBI would probably rather not
have a story like this breaking, that they probably
don't want something else that can
be viewed as political happening right now.
It is worth noting that the FBI did what they
were supposed to with this.
They didn't try to cover it up.
So they have that going for them.
The charges, pretty serious, and expect a lot of coverage.
That's really what we know.
It looks like the former FBI officer or agent
tried to help deal with the sanctions
that the oligarch was under and stuff like that.
OK.
Now, from here, people are making inferences
that we really don't have the evidence to make yet.
The former FBI person was involved in the Trump probe
dealing with Russian collusion.
There are a lot of people who are pointing to this
and saying, ha ha, look, it wasn't Trump.
It was this guy.
OK, you're making an inference without a lot of evidence
to support it.
At this point, I've seen nothing to suggest that this actually
impacted the person's investigations.
But if you are going to jump to a wild conclusion,
understand this is bad for Trump.
You get that, right?
This is worse.
This isn't good.
Pointing out that a person who had something
to do with a probe of Trump, pointing out
that they might have been colluding with Russia,
that doesn't help Trump.
That's worse.
That's worse.
Because if that was true, and again, there's actually
no evidence to support this, but this is the line
that's being drawn.
If that was true, it would mean that that person maybe
helped Trump during that investigation.
Again, I want to stress that while this is certainly
going to become a talking point, there's not evidence yet
to make the jump that they're making.
I am certain that a review of his cases, a review of stuff
that he was involved with, either has taken place
or is going to take place.
That's pretty standard if something like this happens.
But I think that's a good point.
It's hard to always tell whether this is true or if it is.
But if that was shown to be true,
it would be bad for Trump.
Not good.
So I find it entertaining that the people
who are pointing to this don't understand that aspect of it.
Again, it's not actually a thing where if somebody else does it,
your guy is OK.
That's not how it works.
The logical chain of events would be that he would assist Trump.
It's not good for Trump if that was happening.
Just throwing that out there.
But once more, there's not actually evidence to support this yet.
It looks like it was relatively isolated, but there will certainly be a review of that.
We're going to have to wait.
It's really early in this process.
There's going to be a lot of coverage.
There's going to be a lot of news about it.
I think it's unlikely with the current political climate that this story just kind of fades
from the headlines.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}