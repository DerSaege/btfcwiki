---
title: Let's talk about learning about institutions from board games....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dKV8yXqoKiM) |
| Published | 2023/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A woman in her twenties shares a tradition of playing board games with her parents every Tuesday night, but it has recently become difficult for her to attend.
- The woman's parents, who recently retired, have become consumed by watching Fox News, leading to negative and hateful behavior during their game nights.
- An incident occurred where the woman's parents reacted negatively to learning that her black friend received a full scholarship, assuming it was solely based on her skin color.
- Despite the woman's efforts to explain her friend's achievements, her parents refused to believe her, leading to a heated argument.
- The woman is struggling with the decision to continue attending these game nights due to the toxicity and hate that has tainted the once enjoyable tradition.
- Beau suggests introducing new rules for Monopoly that simulate systemic disadvantages to help the woman's parents understand the concept of generational wealth.
- He acknowledges the difficulty of the situation and the common issue of Fox News influencing destructive behavior in families.
- Beau encourages the woman to try educating her parents using the Monopoly analogy or by showing them his video to help them understand the impact of their behavior on her.

### Quotes

- "You did it fine in the message."
- "Connect all of the dots for them."
- "If you want to make the effort and try to educate them, go this route."
- "It's put you in a situation where this is how you feel."
- "Realistically though, from what you've described, you're going to end up going to dinner."

### Oneliner

A woman struggles with attending weekly game nights with her parents due to their consumption of Fox News leading to hateful behavior, prompting Beau to suggest using Monopoly to explain generational wealth.

### Audience

Children of radicalized parents

### On-the-ground actions from transcript

- Show the video to parents and explain the impact of their behavior (suggested)
- Introduce new Monopoly rules to simulate systemic disadvantages (suggested)

### Whats missing in summary

The full transcript provides additional details on the woman's struggle with her parents' behavior, offering a creative solution through a Monopoly analogy to address generational wealth issues and systemic disadvantages. 

### Tags

#FamilyConflict #GenerationalWealth #Radicalization #BoardGames #FoxNews


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
board games and what we can learn from board games in the right situations. And
we're going to talk about institutions and a tradition, I guess. So I received this
message from a woman who is in college, she's in her twenties, she lives in the
same city with her parents, but she doesn't live with her parents. But they
carry on a tradition that started when she was a little girl. Every Tuesday
night, I don't know which night it is, but once a week they get together, they have
dinner, and then they play a board game. And she says it's something she's loved
her entire life. I mean that is some super wholesome stuff right there. But over the
last few months it's been harder and harder for her to go, and recently she
didn't for the first time. She said the only other time she missed it was when
she was in high school and went somewhere as part of a class trip. So what
changed? Her parents retired. Her parents retired recently, they are at home and
they spend all of their time watching Fox News. So what used to be a pleasant
dinner and then a game has turned into them telling her about who they were
told to hate that day. And they're constantly angry and upset. It all came
to a head recently when her parents found out that one of her friends was on
a free ride. On a full scholarship. And they just didn't expect that from her
because they thought she was quote one of the good ones. Her friend is black. Now
it is important to note that her daughter, the daughter here, did try to
explain that no that's it's not a skin thing, she's actually like amazing at
particular sport X. And that she also has a whole lot of academic achievements, but
her parents did not believe this. It was obviously given to her due to her skin
tone. The daughter then went on and tried to explain like even if that was the
case that's still okay and these are the reasons why. It turned into a giant fight
and now she's at the point of like I just don't even want to be around these
people. But she watched the live stream the other night there was a question
about somebody trying to remain friends with somebody who is going down a rabbit
hole. And she sent me this giant message explaining all of this and everything
that's going on. And really just to say hey I'm kind of in the same situation I
don't know what to do. But I just I can't be around that much hate and it's
completely ruined this activity for me. I would rather have the pleasant memories
of before then just watch it all fall apart. I have a suggestion. Next time you
go, because you will, you've been doing this a long time, you're gonna go again.
Parents have a way of guilting you into it. Next time you go tell them that you
have discovered online some new rules for expert monopoly. Rules are simple.
Before everybody starts, everybody rolls the dice. If you get an even number you
are part of group A. If you get an odd number you are part of group B. Those who
get an even number, they play by the normal rules of monopoly. Nothing is
changed specifically to help them. They just play the normal rules. They play
the game normally. Those in group B, they don't get to move for the first eight
turns. They don't get to go anywhere for the first eight turns. They don't get to
do anything. They just have to wait. After those eight turns, they can move, but they
can only buy properties on that front row between go and jail. You know, the
cheap ones up front. And they have to play by those rules for ten turns. And
after those ten turns, then they can play as normal. They can follow the same
rules as everybody else. Ask them if they think that realistically there's a
significant chance of the people in group B winning the game. Not really. I
mean, every once in a while you will have somebody who plays expertly and gets a
little bit of luck. You know, one in a hundred thousand that breaks through. But
it's not going to be a common occurrence, because so much time has
passed and those rules, even though they weren't set out to help this group of
people, group A, they severely impacted group B. And because they started so far
behind in the game of capitalism and in the game of monopoly, there's
only so much and so far they can go without maybe a rule change in their
favor. I'm willing to bet that if you did this, that concept that you tried to
explain to them, generational wealth, I'm willing to bet they would begin to
understand what generational wealth is. I mean, check the math to the terms for
the number of turns. Say 1780 to 1860, so eight years, eight turns. And then 1865 to
1964, use those, so another ten. But you can adjust that as you see fit. I
understand. I get it. And you're right, you have every reason to be angry. And yeah, this is
not anywhere near the first message I've gotten from somebody about how Fox News
has destroyed their parents. It's not even in the...I doubt it's even in the first
hundred. You can make the effort if you want, but it's up to you. Realistically
though, from what you've described, you're going to end up going to dinner. You're
going to end up playing a game. If you want to make the effort and try to educate them,
go this route. Or show them this video. Show them this video and explain that this is
this is what their retirement, their choice of retirement leisure activity
has done. It's put you in a situation where this is how you feel. And connect
all of the dots for them. You did it fine in the message. The rule changes to help
individuals who are striving and trying to get ahead. After the institutional
policies of this country kept them down for a couple hundred years, it doesn't
seem like scholarship or a little bit of assistance here or there or maybe, I don't
know, paying them for the work their ancestors did. That doesn't seem like a
whole lot to ask to me. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}