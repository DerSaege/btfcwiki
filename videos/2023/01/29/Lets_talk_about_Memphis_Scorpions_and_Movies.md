---
title: Let's talk about Memphis, Scorpions, and Movies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sqUJlSCtF2Y) |
| Published | 2023/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Memphis has decided to disband the Scorpion team responsible for troubling actions.
- The strategy behind the Scorpion team, with plainclothes officers having a lot of leeway, raises concerns.
- Despite the team's disbandment, there hasn't been a disavowal of the problematic strategy.
- A significant percentage of officers from the team are currently facing legal consequences.
- The family's attorney has been uncovering more troubling information about the team's actions.
- The community gets tragedy while the department boasts seized assets.
- Hollywood movies and TV shows often draw inspiration from real-life events involving unchecked police authority.
- Officer Rafael Perez, associated with the Rampart scandal, was mimicked in Denzel Washington's character in "Training Day."
- Perez cooperated and exposed misconduct involving over 50 cops.
- Beau expresses concern about potentially losing records after the Scorpion team's closure.

### Quotes

- "The community gets tragedy while the department boasts seized assets."
- "Hollywood understands what happens when you provide that much unchecked authority."
- "Teams like this lead to problems."
- "Do I believe it's in good faith when they're not disavowing that strategy?"
- "They knew it was a Scorpion when they picked it up."

### Oneliner

Memphis disbands the problematic Scorpion team, but the concerning strategy remains unaddressed, reflecting the broader issue of unchecked police authority in popular culture.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Contact local representatives to advocate for the disavowal of problematic police strategies (suggested)
- Join community organizations focused on police accountability and reform (implied)

### Whats missing in summary

Exploration of the long-standing impact of unchecked police authority in real-life events and its portrayal in popular culture.

### Tags

#PoliceReform #PoliceAccountability #CommunityAction #MemphisPD #UncheckedAuthority


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about things going away
and whether or not it can be accepted at face value
and whether or not it's a hopeful sign.
And we will talk about movies,
and you'll get a little bit of trivia about a movie
that I think a lot of people have seen.
If you've missed the news up there in Memphis,
they have decided to get rid of the Scorpion team.
This is the team that was responsible.
And I've had a lot of people ask
if I thought that was in good faith.
No, no, definitely not. Don't.
No, I do not believe it's in good faith.
They're talking about disbanding the team permanently.
They're talking about disbanding the team permanently.
Yeah, that's fine. I haven't heard a disavowal of the strategy behind it.
You know, the idea of creating a plainclothes force within a force
that has a whole lot of leeway and a very vague mission
that, you know, kind of does what it wants to
via saturation and wolfpack type stuff.
Haven't heard a disavowal of that.
I mean, let's be real, when you have 13 to 12 percent...
That's a coincidence.
It really is 13 to 12 percent of the officers on that team
that are currently in cuffs.
Wow.
You kind of have to get rid of the team.
You have a scheduling issue.
But I haven't heard a disavowal of that strategy.
Even though Kromp, the family's attorney,
by the way, if you're concerned, they are in very good hands,
he's asked for that.
And it seems like he keeps getting more and more information
about other things that the team has done.
Closing it down?
I mean, okay. That makes sense, I guess.
But you have to disavow the strategy.
When the Scorpion program first started up there,
Memphis PD was putting out press releases
talking about how much money and cars they had seized,
stuff like that.
That's what the department gets out of it.
What's the community get out of it?
Tragedy. Tragedy.
What's the American people get out of it?
Movies. TV shows.
Movies and TV shows.
We get training day.
Denzel Washington mimicked the style of Officer Rafael Perez
to make his cop more authentic.
Perez was part of the crash team out there,
not Scorpion.
Different acronym.
And there are some people who think that the vehicle plate,
ORP 967, in the movie,
is a reference to Officer Rafael Perez,
who was born incidentally in 1967.
Perez was a pretty big figure in the Rampart scandal.
The Shield. Vic Mackey. That show.
The fact that these type of teams lead to problems,
it's not a secret.
It's a trope. It's a movie trope.
First one I can think of that is loosely based on real events
is Extreme Justice.
It had Lou Diamond Phillips in it.
That's how long this has been going on.
Hollywood figured it out.
Hollywood understands what happens when you provide that much unchecked authority.
I guess police departments are just slower to learn this fact.
Another interesting thing about Perez,
when it comes to all of those allegations that Crump keeps finding out about,
Perez flipped.
He cooperated.
And I want to say, flipped on more than 50 cops.
I really hope that during the move,
after closing down the Scorpion team,
I really hope the records aren't lost.
I have a feeling that some people may want to look at those in the future.
The idea of these type of teams,
it's been around for a really long time,
and there are all types of things that have entered popular culture
based on real events that you would know.
Teams like this lead to problems.
Do I believe it's in good faith when they're not disavowing that strategy?
No.
They knew it was a Scorpion when they picked it up.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}