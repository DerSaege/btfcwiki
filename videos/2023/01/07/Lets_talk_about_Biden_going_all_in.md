---
title: Let's talk about Biden going all in....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VbjT08D7cSI) |
| Published | 2023/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration has launched the "All in Federal Strategies for Reducing and Ending Homelessness" program with an ambitious goal of reducing homelessness by 25% in the next two years.
- The program involves input from people who have experienced homelessness, providers, advocates, developers, and multiple federal agencies.
- Lessons learned from successful initiatives like tackling veteran homelessness are being applied to this program.
- The approach includes a focus on housing first and engaging state and local governments to set equally ambitious goals.
- Web-based seminars for this program start on January 10th, indicating a fast-moving initiative.
- While achieving a 25% reduction in homelessness in two years seems challenging, setting such an ambitious goal is more impactful than settling for minimal targets.
- Even if the program falls slightly short of the 25% reduction, any progress will be a significant win.
- The timeline for achieving these goals is tight, with 2025 approaching quickly.
- The resources and planning put behind this initiative make even a partial success beneficial.
- The outcome of this program will be interesting to observe given its ambitious objectives and comprehensive planning.

### Quotes

- "The Biden administration has launched the 'All in Federal Strategies for Reducing and Ending Homelessness' program with an ambitious goal of reducing homelessness by 25% in the next two years."
- "Even if the program falls slightly short of the 25% reduction, any progress will be a significant win."
- "The outcome of this program will be interesting to observe given its ambitious objectives and comprehensive planning."

### Oneliner

The Biden administration's ambitious program aims to reduce homelessness by 25% in two years, drawing on past successes and involving various stakeholders and agencies.

### Audience

Advocates, policymakers, communities

### On-the-ground actions from transcript

- Attend the web-based seminars starting on January 10th to learn more about the program and how to get involved (suggested).
- Support local initiatives to reduce homelessness by engaging with state and local governments (implied).

### What's missing in summary

The full transcript provides additional insights into the detailed planning and stakeholder involvement in the program, offering a comprehensive view of the initiative's potential impact.

### Tags

#Homelessness #BidenAdministration #FederalProgram #CommunityAction #AmbitiousGoals


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about some good news.
Something that probably got missed and overshadowed with all of the drama this week.
The Biden administration is going all in.
That's the name of the program. All in.
The full name is something like All in Federal Strategies for Reducing and Ending Homelessness
or something like that.
It's an incredibly ambitious program.
The goal is to reduce the number of people experiencing homelessness by 25% in the next
two years.
By 2025.
That's huge.
That's huge.
They did this unique thing when they came up with these strategies that are outlined
in this.
They talked to people who had experienced homelessness.
They probably have some insight.
And they talked to providers.
They talked to advocates.
They talked to developers.
They talked to people who really do have a stake and an understanding in it.
It is a combination of, I want to say, 19 federal agencies, something like that.
It looks like they are drawing heavily on lessons learned from when the United States
government tackled veteran homelessness.
If you don't know, 10, 12 years ago, veteran homelessness was twice what it is today.
That initiative was very successful.
It looks like they're pulling a lot of lessons learned from there.
They're trying to replicate that whole of government approach, focusing on housing first,
all of that stuff.
At the same time, incorporating state and local governments to get in on the act and
set their own goals, hopefully just as ambitious.
The seminars, the web-based seminars, webinars, whatever, those start on January 10th.
This is going to be fast moving.
And it will be successful.
Now whether or not it's going to hit 25% in two years, I mean, I have questions about
that.
But at the same time, when you're talking about a topic like this, I would much rather
an administration set a goal of 25% and miss by two or three points than commit to doing
the bare minimum and say we're going to reduce it by 5% and hit the goal.
This is one of those moments where the goal is so ambitious that even a failure is a win.
It's something to kind of keep your eye on.
I'm really interested to see how this plays out and how far they can get and how quickly
they can do it.
That is a very tight timeline to hope for that kind of cut.
Now a lot went into it.
Like I said, I've been talking to a lot of people and working out the plans and getting
everything together.
But I mean 2025 is right around the corner.
So it'll be interesting to see how this plays out.
But with the goals that they have set and the resources that they have put behind it,
with the planning they've put behind it, I mean, even a failure is going to be good for
10%.
So this is a win before it even starts, to be honest.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}