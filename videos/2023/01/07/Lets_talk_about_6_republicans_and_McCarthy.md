---
title: Let's talk about 6 republicans and McCarthy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A40k-nZ60bU) |
| Published | 2023/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why the media approached a certain topic in a particular way concerning the mess up in the House of Representatives.
- Questions why it is expected of the Democratic Party to make power moves or find a unity candidate instead of suggesting Republicans cross over.
- Compares blaming Democratic Party for not catching Santos to the current situation, pointing out the focus on Democrats rather than Republicans.
- Expresses uncertainty about finding six Republicans willing to prioritize country over party in a significant vote.
- Suggests possible incentives for Republicans to cross party lines, but acknowledges the challenge in finding willing candidates.
- Criticizes the Republican Party for prioritizing party over country, citing examples of authoritarian behavior.
- States lack of faith in Republicans to act in the country's best interest and the need to appeal to their ambition in negotiations.
- Comments on the impasse between parties and the decision the Republican Party faces in defining itself as conservative or fascist.
- Notes the media's lack of expectation from Republican candidates to do the right thing.

### Quotes

- "You have to appeal to ambition, to being a self-serving politician."
- "The Republican party has to decide whether it is going to be a conservative party or a fascist party."
- "The party is what matters. They don't care about the country."
- "It's the Republican Party, count on them to do the wrong thing."
- "I stopped asking myself what a Republican president do and started asking myself what a fascist will do."

### Oneliner

Beau delves into media portrayal, Republican reluctance, and the party's priority over country in House matters.

### Audience

Media consumers, political observers, activists

### On-the-ground actions from transcript

- Engage in critical media consumption to understand biases and narratives (suggested)
- Advocate for accountability and country-first actions from elected officials (implied)
- Support unity candidates who are not beholden to extreme factions (suggested)

### Whats missing in summary

Insights on the evolving political landscape and challenges in bipartisan cooperation.

### Tags

#Media #RepublicanParty #Bipartisanship #PoliticalAnalysis #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again. So today we are going to talk about why
the media
approached a certain topic the way it did when it comes to the mess up in the House of Representatives.
Because I got a bunch of messages and they're all pretty much asking the same thing, different ways,
ways, but it's all pretty much the same question.
Which is, why is it up to the Democratic Party to either pull some power move, or find a
unity candidate?
Why isn't the media suggesting that six Republicans cross over and vote for Jeffries?
And I get the question on a theoretical level.
It makes sense, it's a worthwhile question to ask, but think about it.
This is a lot like why everybody was blaming the Democratic Party or the media for not
catching Santos earlier for not finding out about all of his embellishments.
He's a Republican, but they were looking at the Democratic Party or the media.
They weren't looking at the Republican Party.
Nobody was looking to affix blame there.
It's the same thing at play.
I could be wrong about this, and that's not a sarcastic, I could be wrong about this.
I really could be wrong about this.
I don't have workups on all of the Republicans in the House of Representatives.
So I'll just ask you, can you name six Republicans in the House that would put their country
over their party, particularly on a vote that significant?
That's why.
That is why.
I can think of three that might be worth approaching, but I don't know where the other three are.
Some people have suggested, give them committee chairs, give them something.
And yeah, I mean sure, but they would have to be candidates who were in areas that voted
for Biden, so blue areas, and preferably kind of near the time they would want to
retire anyway, because this would come up over and over again throughout their
political career.
This isn't crossing the aisle on a bipartisan measure.
This is crossing the aisle and putting the Democratic Party in control of the
House.
That's huge.
That's huge. I don't know that there are six Republicans who would do that. I
can't think of six. They might exist, but it wouldn't even be surprising. It would
be outright shocking if there were six Republicans who were willing to cross
the aisle on this.
And I think that for a lot of the media, there wasn't that much thought put into it.
It's just, it's the Republican Party, count on them to do the wrong thing.
Questions I got all the time during the Trump administration.
How do you always know what he's going to do?
About a year ago, I stopped asking myself what would a Republican president do and started
asking myself what a fascist would do.
I wasn't wrong from that point forward.
The Republican Party has become incredibly authoritarian and it is insulated to itself.
The party is what matters.
They don't care about the country.
I mean, you literally have semi-major figures within the Republican Party talking about
intentionally sending the country into default.
They don't care about the country.
They care about the party.
Finding six Republicans, I mean, that's an Easter egg hunt that I don't want to be a
part of.
You'd have to turn over a lot of rocks to find those six.
Is it worth the attempt?
Yeah, and I'm sure that the Democratic Party is doing that.
I don't know how much luck they're going to have.
A lot of times when you see the media in general, and me in particular, put something on the
Democratic Party, y'all need to fix this, when it's really the Republicans' fault.
It's because I have zero faith in the Republican Party doing anything that would benefit the
United States unless it benefits them more. That's why when you're talking about
striking deals, cutting deals with them, you can't appeal to anything like that.
You have to appeal to ambition, to being a self-serving politician. That's how you
have to frame any deal. The idea that they're going to find morality or
conscience at this point in the game seems pretty slim. I think they could
probably get the numbers for a unity candidate to find a moderate who would
not be beholden to the Sedition Caucus and wouldn't be McCarthy, I think you could
find the numbers for that.
But that's not really that helpful.
The impasse that has existed, and even though this probably isn't going out until tomorrow
night, it will probably still exist then, it's part of what we've been talking about.
The Republican party has to decide whether it is going to be a conservative party or
a fascist party.
And that's really part of the battle that's going on right now.
I mean, understand that the Republican party is so off its base that McCarthy isn't far
enough to the right.
That should tell you something.
reason the media didn't bring it up is because nobody expects the Republican
candidates to do the right thing. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}