---
title: Let's talk about being displaced by disaster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J3JxkopAw1o) |
| Published | 2023/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares insights from a Census Bureau survey on natural disaster displacement.
- 3.3 million Americans were displaced last year, with one out of six never returning home.
- The survey provides perspective on scope and time regarding natural disasters.
- Points out that these numbers are expected to increase with climate change.
- Indiana, Maine, North Dakota, Ohio, and Oklahoma are the least likely to experience displacement, while Florida is the most likely.
- Emphasizes the importance of emergency preparedness, especially considering the increasing frequency of natural disasters.
- Mentions the vastness and diversity of the United States in accepting displaced people.
- Encourages individuals to take steps to prepare for natural disasters.
- Raises awareness about the commonality of natural disaster displacement.
- Urges families to create emergency kits and plans.

### Quotes

- "At some point in your life, you will be displaced by a natural disaster."
- "The most important takeaway here is for those who need some information to get their family members to put together a kit or a plan."
- "Y'all have a good day."

### Oneliner

Beau shares insights from a Census Bureau survey on natural disaster displacement, urging individuals to prepare as such occurrences are becoming more common.

### Audience

Community members

### On-the-ground actions from transcript

- Prepare an emergency kit for your family members to deal with natural disasters (suggested).
- Create a plan to address potential natural disaster situations in your area (suggested).

### Whats missing in summary

The full transcript provides detailed statistics and insights on natural disaster displacement, reinforcing the importance of preparedness in the face of increasing occurrences.

### Tags

#NaturalDisasters #EmergencyPreparedness #ClimateChange #CommunityAction #Awareness


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a survey
conducted by the Census Bureau,
and it's a survey that might help you
convince some of your family members to take some action.
And it also gives some perspective on scope and time
and all kinds of things.
Last year, 3.3 million Americans were displaced
from their homes by a natural disaster.
It's a little more than one out of a hundred,
1.3% of the population.
Now, a third of those were only out of their home
for a week or less, but one out of six
never returned to their home.
One out of six never returned to their home.
So this is the first time a survey like this has been done.
These numbers are much larger than I would have expected.
We can expect these numbers to go up.
As climate change increases
and the more extreme weather becomes more common,
we can expect these numbers to decline.
Currently, according to, again, first year of data,
it looks like Indiana, Maine, North Dakota,
Ohio, and Oklahoma are the places
where you are least likely to be displaced.
Most likely is Florida.
Go figure.
So one of the things when I've talked about
emergency preparedness,
I always get questions from people,
well, my family won't take this seriously.
They don't wanna do anything like this.
They'll be fine.
They ignore it.
According to this, there's, I mean,
if you figure it out over a lifespan and everything,
it's pretty much guaranteed that at some point in your life,
you will be displaced by a natural disaster.
So it seems like something people might want to
take seriously, especially given the fact
that it's probably going to become a whole lot more common.
Another thing that I wanna point out
is when people talk about
those who are displaced from other countries
and end up in the United States,
and they talk about the hundreds of thousands,
it sounds like a lot.
3.3 million last year,
Americans internally displaced.
Did you notice?
I mean, if you lived in Atlanta,
yeah, I'm sure when Ian hit,
you had a whole bunch of Floridians up there
driving you crazy.
But overall, is this something you noticed?
The United States is an incredibly large country.
It's very diverse, and it is very capable
of accepting large amounts of people.
But I think the most important takeaway here
is for those who need some information
to get their family members to put together a kit
or a plan to deal with whatever natural disaster
is common in your area.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}