---
title: Let's talk about next year....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ltBL9xsat7A) |
| Published | 2023/01/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Plans for next year's content on the channels are being discussed.
- The main channel will not see significant changes, just a workflow change and a new building for a more productive workspace.
- There will be a surprise rebuild of the shop in the new building.
- On the second channel, expect a community garden video, Jeep build phases, and content related to hurricane relief.
- The summer might bring travel content on the channel "The Roads with Bo."
- Two books are almost ready for release this year.
- Expect at least one live stream a month, possibly two.
- More interviews are planned on the second channel, including in-person interviews for deeper insights.
- Beau hints at bigger projects in the works but isn't ready to announce them yet.

### Quotes

- "So on the main channel, not much is going to change."
- "Expect at least one live stream a month, maybe two."
- "There are a couple of other bigger projects that we've been working on that I'm not quite ready to announce yet."

### Oneliner

Beau plans for upcoming content, including workspace upgrades, books, live streams, and in-depth interviews, with hints of bigger projects on the horizon.

### Audience

Content Creators

### On-the-ground actions from transcript

- Start planning and creating content for your channels (implied)
- Stay updated with community garden video, Jeep build phases, and hurricane relief content (implied)
- Tune in for live streams and interact with the content (implied)
- Support upcoming book releases and participate in possible motivational events (implied)
- Stay engaged for future announcements of bigger projects (implied)

### Whats missing in summary

Details on the specific topics of the two upcoming books and further elaboration on the surprise rebuild in the new building.

### Tags

#ContentCreation #ChannelUpdates #LiveStreams #Interviews #FutureProjects


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about what's
going to happen next year on the channels
and kind of give everybody a heads up
as to what is being planned.
And kind of go from there because we have a lot going on.
So on the main channel, not much is going to change.
I talked about a workflow change and a new building.
If you don't know, we're converting a still building
into a more productive workspace because this entire time,
we've been doing this from laptops
and just driving everybody around us crazy.
So we will have a designated workspace, which will
be much, much more productive.
A lot of people were very concerned about what
it was going to look like.
It's going to look exactly the same.
We are actually going to rebuild the shop in the new building.
And there will actually be some.
Well, I'll keep that part as a surprise.
But you all won't see a difference on the main channel
other than it should be easier for me, which is nice.
On the second channel, you will have the community garden
video coming out before spring.
And then you will have the first phase of the Jeep build
and the stuff about what we did during Ian, the hurricane
relief.
That'll be coming out real soon as well.
This summer, we hope to actually go on the road with that
channel, The Roads with Bo.
And for those that haven't been around a long time,
that channel started with the goal of actually going out
on the road.
And we did a couple of little things.
But then the pandemic hit, and everything just stopped.
We didn't really feel comfortable going out.
Then once things cleared up, we had other projects going
and couldn't break free.
So once we wrap up the projects we have,
we'll be free to go out on the road.
Something else that will happen this year is a book
will come out.
There are actually two.
At this point, it's competing.
We wanted to have one of them done by Christmas.
It didn't happen.
There are two of them that are almost done.
Like, we're in the final editing phases of all of this.
So you will get one, maybe two books this year.
There will be at least one live stream a month, maybe two,
because there's something I want to do at the end of this year
that I think it will be a good motivating thing,
but I haven't figured out exactly how to do it yet.
So there may be a second live stream a month.
I'm not sure.
Expect more interviews on the second channel,
and then expect in-person interviews,
bringing people here or me going there
and interviewing people in person.
Those will be way more in-depth than the ones
you've seen via the video conferencing.
Those we will start with people we all know,
and those should be really fun.
That's all we can disclose now.
For those who've been around a long time,
y'all know we talked about the second channel for a year
before it showed up.
There are a couple of other bigger projects
that we've been working on that I'm not
quite ready to announce yet, but we'll be around as long
as y'all are.
So that's pretty much it.
Y'all have a happy new year.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}