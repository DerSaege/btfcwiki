---
title: Let's talk about cops and that thing I didn't read....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sNekAAwhfQA) |
| Published | 2023/01/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau received a message questioning why he didn't read a specific piece of information on a police website related to high crime rates in the South, particularly affecting poor and Black individuals.
- The information not read by Beau revealed that all the 10 most dangerous counties listed had an African American plurality or majority.
- Beau questions the causal link between skin tone and high crime rates, pointing out the lack of evidence and providing examples like all locations having a Walmart, majority with brown eyes, and a river – none of which directly lead to crime.
- Institutional racism in policing is brought to light through the inclusion of irrelevant information that perpetuates harmful stereotypes.
- Beau stresses that treating demographic identifiers as causal factors without evidence is a form of prevalent racism within law enforcement, leading to dangerous assumptions and actions.
- The danger lies in perpetuating the idea that Black people are more dangerous, ultimately contributing to excessive force by law enforcement officers.
- The misinformation included in crime statistics can lead to viewing skin tone as a threat, despite it not correlating with an increased propensity for violent crime.

### Quotes

- "Skin tone does not increase somebody's propensity to engage in violent crime."
- "Treating demographic identifiers as causal things without information to back it up is a type of racism."
- "Institutional racism in policing continues to persist through the inclusion of irrelevant information that perpetuates harmful stereotypes."

### Oneliner

Beau received criticism for not discussing high crime rates affecting poor and Black individuals, pointing out the lack of evidence linking skin tone to crime and the dangers of perpetuating harmful stereotypes.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Challenge institutional racism in policing through advocacy and awareness (implied)
- Educate others on the dangers of perpetuating harmful stereotypes based on demographic identifiers (implied)

### What's missing in summary

The full transcript provides a detailed insight into the prevalence of institutional racism in law enforcement and the dangers of perpetuating harmful stereotypes based on skin tone in crime statistics.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to,
we're going to talk about something I didn't say
and taking the bait.
And we're going to go over a piece of information
that was included in one of the sources
that I used in the recent video
talking about rates in rural areas versus cities
and the methodology that gets used
and so on and so forth.
Because there were three items listed in one,
three things that the authors of the article noticed,
and I only read two of them.
And then I put a link down below, something I never do.
Okay, so this is the message I received.
Why didn't you read the other thing on the police website?
In the South, poor and black.
You just exclude the information about it being black people's fault.
And this is why you're a, a couple of words I'm not going to read, hack.
Give me one reason why you exclude it to get this message
so we could talk about it.
Because it deserves a video all of its own.
So for those who didn't watch that video,
there's a, an article that I used,
and it's talking about the 10 most dangerous counties,
and it's put out by cops.
The first thing that they kind of put in their notes
was that there's a regional divide.
This, the most dangerous counties, they're all in the South.
And that makes sense.
There's a causal link there between a strong gun culture,
weak gun laws, and more gun violence.
I mean, that just kind of tracks.
And then they note that they're low income areas,
which also has a causal link.
People without money often commit crimes of desperation,
and sometimes that turns into violent crime.
And then it says this, those two I read, this I didn't.
All 10 of the cities and counties on this list
have an African American plurality or majority.
What's the causal link?
I've asked this question for years
when people say stuff like this.
I have never gotten an answer, not once.
What is the causal link between skin tone and high crime rates?
Fun fact, all 10 places on that list,
they all have a Walmart.
They all have a plurality or majority
of people with brown eyes.
They all have a stream or river running through them.
Do these things lead to more violent crime?
No, there's no causal relationship.
The same thing applies.
Nobody has ever been able to provide one
because it doesn't exist.
Now, you have to ask the question of why it was included
and more importantly, what the impact was.
But you know what the impact was because you said,
you just exclude the information
about it being black people's fault.
That is not what this says,
but that's what you took away from it.
And that's what every cop that read it took away from it.
And it was put there by, I'm sure,
other cops who had similar views.
Welcome to the world of institutional racism
in policing and how it just continues to go.
I run across this all the time with crime stats.
People wanting to treat a demographic identifier
as a causal thing.
But there's literally no information to back that up.
This type of racism, there's no other word for it.
It's incredibly prevalent in law enforcement
and you've seen it now.
And this is how it ends up there.
Information included like this that has no actual bearing on it.
I find it interesting and I'm willing to bet
that they didn't actually check to see
if the perpetrators or victims were black.
They just looked at demographic information
and rolled with that.
The...
This exists.
It's going to continue to exist for a while
until people stop looking at it as if there is a causal link.
It does not exist.
If you ask that question of anybody who uses these stats,
they can't give you an answer
because that's not how it works.
It's a way to scapegoat and it's dangerous
because it perpetuates the idea
that black people are more dangerous,
which then leads to excessive force.
Skin tone does not increase somebody's propensity
to engage in violent crime,
but information like this definitely increases
an officer's propensity to view skin tone as a weapon.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}