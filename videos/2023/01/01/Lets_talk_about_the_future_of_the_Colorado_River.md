---
title: Let's talk about the future of the Colorado River....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q6JjhhD9HiQ) |
| Published | 2023/01/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nevada called on upper basin states to cut water usage in response to the Colorado River drought.
- States are approaching the issue as a numbers game, focusing on cutting water usage rather than developing a sustainable solution.
- Beau argues that simply managing cuts is not a long-term or short-term solution to the water crisis.
- He stresses the need for states to collaborate and create a sustainable water management plan with more reuse and less reliance on the Colorado River.
- Beau believes that states must understand the severity of the situation and prioritize maintaining existing resources over unlimited growth.
- The current focus on maintaining growth in Arizona and Nevada shows a lack of understanding of the water crisis's severity.
- He warns that if states fail to address the issue, the federal government may intervene and take control of water management.
- Beau underscores the importance of states setting a precedent for managing scarce resources as similar crises may arise in the future.
- The lower states are currently bearing the brunt of the water crisis, but Beau predicts that its impact will spread to more regions.
- States need to develop a comprehensive, long-term plan rather than short-term fixes to address the dwindling water resources.

### Quotes

- "Cutting usage is not a solution. The states have to come together and come up with a sustainable water management plan."
- "You can't have just unlimited growth when you have finite resources. You certainly can't have unlimited growth when you have decreasing resources."
- "The states get to set the tone for the rest of the country. And right now, the tone they're setting is one of denial."

### Oneliner

Beau stresses the need for a sustainable water management plan as states tackle the Colorado River drought by focusing on cuts rather than long-term solutions.

### Audience

State policymakers

### On-the-ground actions from transcript

- Develop a sustainable water management plan with more reuse and less reliance on the Colorado River (implied)
- Prioritize maintaining existing resources over unlimited growth (implied)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's call for states to address the water crisis comprehensively and sustainably.

### Tags

#ColoradoRiver #WaterCrisis #Sustainability #ResourceManagement #StatePolicymakers


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk a little bit more
about the Colorado River and how that is impacting things
out West with the droughts.
One of the most recent developments
has been Nevada calling on states
in the upper basin of the Colorado River
to cut like half a million acre feet of water usage.
And that really, therein lies the problem, really,
the way of thinking about the current situation,
because that's how states are viewing it.
They're viewing it as a numbers game.
And well, if we can just cut usage here and cut usage here
and cut usage here, we'll get a little bit further with it.
That's not a solution.
That is not a solution.
The states have to come together and come up
with a sustainable water management plan,
a lot more reuse, a lot more reclamation, a lot less
reliance on the Colorado River.
Right now, their focus is how to divide up the cuts
and just make it through in the short term,
because they're still operating under the assumption
that this isn't going to be a new normal.
I'm pretty sure they're wrong.
There are measures being taken to make sure
that certain areas, like the tribes, have water.
But for the most part, the states,
they're not getting anywhere.
All they're doing is trying to manage the cuts.
That's it.
That's not a long-term solution.
That's not even a short-term solution, really.
This is going to be something that has to occur.
And it's this river basin that gets
to set the tone for what happens,
because this isn't going to be the last time resources
like this become scarce.
The states have to develop a system
for working these issues out.
And it can't be ad hoc.
It can't be on the fly.
If the states don't work this out,
the federal government absolutely
will step in and take control over all of this management.
It'll occur.
There are articles talking about how
people in Arizona and Nevada are worried about how they're
going to maintain growth.
That going on, that being where their minds are at,
shows that they probably don't understand
the severity of the situation.
I don't even know that they should be worried about growth
at all.
They should be more concerned about how to maintain
what's currently there.
And this issue right now is hitting those lower states
the most.
But it doesn't stop there.
It's going to spread.
It is going to impact more and more places.
These states get to set the template.
They get to set the tone and decide
how these things are going to be managed,
because there will be more of them.
Right now, they're failing.
They're not getting anywhere.
They aren't coming up with anything comprehensive.
It's all very short term, trying to manage things.
Right now, this second, to maintain growth.
You can't have just unlimited growth
when you have finite resources.
You certainly can't have unlimited growth
when you have decreasing resources.
These states get to set the tone for the rest of the country.
And right now, the tone they're setting is one of denial.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}