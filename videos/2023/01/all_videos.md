# All videos from January, 2023
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2023-01-31: Let's talk about police pension policy problems.... (<a href="https://youtube.com/watch?v=u6AZoHi2cvo">watch</a> || <a href="/videos/2023/01/31/Lets_talk_about_police_pension_policy_problems">transcript &amp; editable summary</a>)

Slogans like "end qualified immunity and take payments from the police pension fund" draw attention to accountability but may have unintended consequences, confusing slogans with solutions when lives are at stake.

</summary>

"Slogans are not solutions."
"We can't confuse slogans with solutions, especially with something like this."
"Lives are literally on the line."
"It's a great slogan and it's a great way to draw attention to the fact that officers aren't held accountable enough."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Qualified immunity is not what many people think it is; it's a civil protection, not a shield against criminal charges.
There are calls to alter or end qualified immunity, but it's not a cure-all solution.
The slogan "end qualified immunity and take the payments out of the police pension fund" is not a policy suggestion but a statement to show accountability.
Shifting payments to the police pension fund could create unintended consequences by providing a financial incentive for officers to cover up misconduct.
Changing statutes for criminal liability may be more effective in changing the culture within law enforcement than altering the pension fund system.
Confusing slogans with solutions can have serious consequences, especially when lives are at stake.

Actions:

for advocates, policymakers, activists,
Advocate for altering statutes for criminal liability to incentivize reporting misconduct (implied)
</details>
<details>
<summary>
2023-01-31: Let's talk about original intent and the Constitution.... (<a href="https://youtube.com/watch?v=Lspu9WYHeoA">watch</a> || <a href="/videos/2023/01/31/Lets_talk_about_original_intent_and_the_Constitution">transcript &amp; editable summary</a>)

Originalists paradoxically push for the original intent of the Constitution while not knowing it, failing to recognize its revolutionary machinery for change.

</summary>

"The Constitution is not set in stone and was never intended to be."
"They expected it to be changed pretty often."
"One of the most revolutionary parts of the Constitution is the part that those people who call themselves patriots, call themselves constitutionalists, are trying to make sure you forget."
"When you remove that section, the beauty of the document disappears."
"The machinery for change in the Constitution is one of its most revolutionary aspects."

### AI summary (High error rate! Edit errors on video page)

Originalists push for following the original intent of the Constitution while admitting they don't know what the original intent was, creating a paradox.
Some believe the Constitution can't be changed, but it has amendments, which are changes themselves.
The Constitution is not set in stone and was never intended to be unchangeable.
The US Constitution has seven articles, with Article 5 covering amendments, indicating the founders' intent for it to be changed.
Article 5 gives clues on how frequently and quickly changes were expected.
Certain parts of the Constitution were protected from changes only until 1808, showing the expectation for frequent changes.
People believing the Constitution shouldn't be changed have been misled to maintain the status quo.
The founders understood that society and thoughts change over time, necessitating a mechanism to update the Constitution.
Removing the ability to change the Constitution removes its beauty and original intent.
The machinery for change in the Constitution is one of its most revolutionary aspects.

Actions:

for constitution advocates,
Educate others on the original intent and purpose of the US Constitution (implied)
Advocate for the importance of being able to change the Constitution when necessary (implied)
</details>
<details>
<summary>
2023-01-31: Let's talk about Trump, New York, and cold water.... (<a href="https://youtube.com/watch?v=9qTGc3MuxK4">watch</a> || <a href="/videos/2023/01/31/Lets_talk_about_Trump_New_York_and_cold_water">transcript &amp; editable summary</a>)

New York grand jury looking into Trump's alleged payments might not lead to jail time, managing expectations is key.

</summary>

"This case is not like a lot of the other ones where the charges are much more serious."
"I don't really see jail as something that's going to happen."
"Even if he's convicted, I don't really see jail as something that's going to happen."

### AI summary (High error rate! Edit errors on video page)

New York has impaneled a grand jury to look into Trump's alleged payments to Ms. Daniels and possible criminal activity.
The most likely scenario involves falsification of business records.
People are excited, thinking Trump might be arrested, but there are factors to manage expectations.
Potential hearings on statute of limitations could cause delays if Trump is charged and arrested.
Even if convicted, the chance of Trump going to jail over this low-level felony seems unlikely.
Being a former president might influence the outcome, possibly shielding him from jail time.
Beau admits to not knowing enough about New York law to form an opinion on the situation.
Despite tax evasion history, managing expectations is key as this case may not lead to imprisonment for Trump.

Actions:

for legal observers,
Stay informed on the legal proceedings (implied)
Manage expectations and avoid speculation (implied)
</details>
<details>
<summary>
2023-01-31: Let's talk about Social Security and why the GOP is going after it.... (<a href="https://youtube.com/watch?v=QCr06LXSJ5Y">watch</a> || <a href="/videos/2023/01/31/Lets_talk_about_Social_Security_and_why_the_GOP_is_going_after_it">transcript &amp; editable summary</a>)

A conservative dissects the Republican Party's shift from individual responsibility to grievance, warning of the consequences of their actions on Social Security.

</summary>

"The slogans are still there. Those policy planks are still there."
"It's a party of blaming everybody else."
"They want to rule."

### AI summary (High error rate! Edit errors on video page)

A conservative who has been a lifelong Republican crossed party lines in 2018 due to Trump's influence but still identifies as conservative.
The conservative values individual responsibility and believes in planning for retirement independently rather than relying on the government, even though they disagree with Social Security.
Despite Social Security's popularity, the Republican Party's stance remains against it, with no majority support for their ideas.
Beau questions whether the cruelty is the point behind the Republican Party's actions, suggesting they are targeting and blaming certain groups to appeal to their base.
The Republican Party's focus has shifted from individual responsibility to grievance and blaming others, even though slogans and policies against Social Security persist.
Beau criticizes the party for promoting candidates who refuse to acknowledge election losses and describes them as no longer being advocates of personal responsibility.
He points out that many Republicans, particularly in the House, prioritize social media engagement over actual voter support, banking on slogans but lacking substance.
Beau warns that if the Republican Party succeeds in altering Social Security benefits, it will have severe consequences for those relying on it, as many members of the party are unaware of the program's broader implications.
The shift towards authoritarianism within the party is noted, with an emphasis on consolidating power at the expense of understanding the consequences of their actions.
Beau concludes by stating that the Republican Party has deviated from its original principles of individualism and anti-authoritarianism, now embodying entitlement and grievance while shunning representation in favor of rule.

Actions:

for conservative voters,
Contact local representatives to express support for Social Security and advocate against cuts or alterations (implied)
Educate fellow conservatives on the importance and impact of Social Security to debunk misconceptions (implied)
Organize community forums or events to raise awareness about Social Security and encourage informed decision-making (implied)
</details>
<details>
<summary>
2023-01-30: Let's talk about journalists and press releases.... (<a href="https://youtube.com/watch?v=NXa60Ku1B2k">watch</a> || <a href="/videos/2023/01/30/Lets_talk_about_journalists_and_press_releases">transcript &amp; editable summary</a>)

Beau introduces an interactive lesson on journalism, urging reporters to scrutinize police narratives instead of accepting them blindly as factual accounts. He criticizes the use of ambiguous and misleading police press releases.

</summary>

"Stop taking police department press releases as gospel."
"Make 2023 the year that journalists stop accepting police press releases as if they're fact."

### AI summary (High error rate! Edit errors on video page)

Introduces an interactive video discussing journalism practices and a specific case to illustrate a point.
Mentions the intersection of Rains Road and Ross Road as a focal point for the demonstration.
Instructs viewers to search for a specific phrase related to an incident at the intersection.
Reads a police department press release regarding an incident at the mentioned intersection.
Criticizes journalists for accepting police press releases as factual accounts without questioning.
Raises concerns about police departments using ambiguous and misleading language in press releases.
Urges journalists to scrutinize police narratives and compare them with actual footage.
Questions the motives behind police press releases and the lack of accountability in reporting.
Encourages journalists to stop treating police press releases as unquestionable truth.
Calls for a critical examination of police narratives by journalists and a shift away from blind acceptance.

Actions:

for journalists, news outlets,
Scrutinize police narratives for accuracy and question ambiguous or misleading statements (implied).
Stop accepting police department press releases as unquestionable truth (implied).
</details>
<details>
<summary>
2023-01-30: Let's talk about a PSA and the comments section.... (<a href="https://youtube.com/watch?v=aDnznf64DWo">watch</a> || <a href="/videos/2023/01/30/Lets_talk_about_a_PSA_and_the_comments_section">transcript &amp; editable summary</a>)

Be on guard against YouTube impersonators asking for personal information and money; verify profiles, avoid off-platform interactions, and use official channels to reach out.

</summary>

"If a person on YouTube has some kind of special deal or something like that or some kind of information or whatever, they're going to make a video about it."
"Most of us, we don't even allow that. It will direct you to Patreon or something like that if you're wanting to support the channel."
"So just kind of be on guard."
"Y'all have a good day."
"Thanks for watching!"

### AI summary (High error rate! Edit errors on video page)

There are impersonators on YouTube leaving comments claiming you won a prize or asking for personal information and money.
Impersonators try to gather personal information and trick people into sending them money.
If a YouTuber has something special to share, they will make a video about it on their platform.
Impersonators create fake accounts that look similar to the real channel but have small differences in the name and profile picture.
Always verify the profile sending you a message by checking if it redirects you to the actual channel.
Pay attention to web addresses in emails or messages to detect scams.
Most YouTubers do not direct people off-platform or ask for personal information.
Scams are becoming more common as the internet expands, targeting a wider audience.
Beau expected older individuals to fall for scams but found that younger people were also targeted.
Use links in the about section to reach out to YouTubers on their official social media accounts.

Actions:

for youtube viewers,
Verify the profile of anyone reaching out to you on YouTube (suggested)
Use official social media links to contact YouTubers (implied)
</details>
<details>
<summary>
2023-01-30: Let's talk about M&Ms.... (<a href="https://youtube.com/watch?v=xJ-LOvp-Y5I">watch</a> || <a href="/videos/2023/01/30/Lets_talk_about_M_Ms">transcript &amp; editable summary</a>)

M&Ms' inclusive moves spark conservative backlash, leading to misunderstandings and declarations of victory, but the war against the candies may be unwinnable.

</summary>

"The war against M&Ms may be unwinnable."
"Pronouns are bad. Y'all should go after them."
"They're trying to train your kids early and stuff."

### AI summary (High error rate! Edit errors on video page)

M&Ms updated their cartoon candies, sparking controversy like Tucker Carlson upset about the candy not being "hot" anymore.
M&Ms made moves to be more inclusive, leading to backlash from the right due to their support for causes considered left-leaning.
A recent campaign focused on women's charities upset conservative thought leaders, resulting in a known alpha male figure rejecting the feminine M&Ms.
The company announced an indefinite pause on the spokes candies and introduced Maya Rudolph as a spokesperson, seemingly using satire.
Conservatives misunderstood the indefinite pause, declaring victory over the candies, despite indications that they will return, possibly for the Super Bowl.
The situation led to confusion and backlash, with conservatives struggling to understand the satire behind M&Ms' actions.
Beau suggests that the war against M&Ms may be unwinnable and predicts further conservative backlash that could ironically boost M&Ms sales.
He humorously suggests targeting Hershey's next due to the pronouns "her" and "she," poking fun at conservative outrage.

Actions:

for social media users,
Support women's charities (implied)
Stay informed on corporate actions and responses (suggested)
</details>
<details>
<summary>
2023-01-30: Let's talk about DOJ, Santos, and a claim.... (<a href="https://youtube.com/watch?v=rFHB6gd1WW4">watch</a> || <a href="/videos/2023/01/30/Lets_talk_about_DOJ_Santos_and_a_claim">transcript &amp; editable summary</a>)

Beau examines a conspiracy theory about Santos being a secret double agent for Team Trump, debunking it with the reality of a criminal investigation involving the Department of Justice.

</summary>

"I do not think that anything else in the versions of this theory that I have been given is even remotely accurate."
"Please remember, these are the people, this is coming from the same set of people who showed up in Dallas waiting for Kennedy to return."
"That seems more likely that the FBI or the Department of Justice doesn't want the FEC compromising a criminal investigation by accident."

### AI summary (High error rate! Edit errors on video page)

Exploring a conspiracy theory about Santos being a secret double agent on Team Trump.
Claim circulating among a group known for inaccurate claims, leaving many questions.
Theory involves Santos fighting against evil Democrats as a super secret double agent.
Belief that FBI, no longer run by Democrats, is proof of Santos being on the good side.
Department of Justice intervened to protect Santos from FEC investigation, according to Washington Post reporting.
DOJ's intervention not to protect Santos but likely due to a criminal investigation on him taking precedence.
DOJ telling FEC to back off is bad news for Santos, indicating criminal investigation involvement.
Likelihood of Santos being under criminal investigation, not being protected by the DOJ.
Theory's lack of accuracy, coming from a group that previously waited for Kennedy to return in Dallas.
Most likely explanation: DOJ doesn't want FEC compromising criminal investigation accidentally.
Possibility of resurgence of similar conspiracy theories in the future.

Actions:

for political observers,
Stay informed about current events and political developments (implied)
</details>
<details>
<summary>
2023-01-29: Let's talk about the House GOP, gas prices, and promises.... (<a href="https://youtube.com/watch?v=IKv0zVJHzlc">watch</a> || <a href="/videos/2023/01/29/Lets_talk_about_the_House_GOP_gas_prices_and_promises">transcript &amp; editable summary</a>)

House Republicans focus on gas prices reveals their lack of genuine concern for average people, aiming to limit Biden's power rather than address real issues.

</summary>

"Promises made are definitely not promises kept."
"They don't actually care about the commoners."
"More talk designed to manipulate those who don't actually follow through."

### AI summary (High error rate! Edit errors on video page)

House Republicans focused on gas prices during midterms, criticizing Biden's efforts.
News of oil companies making significant profits led to House Republicans passing new regulations.
Regulations aim to increase refining capacity and infrastructure maintenance for oil companies.
House Republicans aim to prohibit Biden from drawing from the strategic oil reserve.
Beau questions the effectiveness of Biden's previous actions in lowering gas prices.
House Republicans' actions reveal their lack of genuine concern for average people and gas prices.
Promises made by House Republicans are not being kept, showing a disregard for helping the common people.
Their focus seems to be on potential political gains for the Republican Party in 2024.
House Republicans' actions are seen as more for social media engagement than actual legislative progress.
Passing regulations in the House is unlikely to succeed in the Senate or be approved by Biden.
The move by House Republicans showcases their lack of genuine interest in regulating oil companies.
Beau suggests discussing this political tactic with relatives.
The House Republicans' strategy appears to be limiting Biden's power rather than addressing actual issues.

Actions:

for voters, political analysts,
Engage in informed political discussions with relatives about the House Republicans' focus on gas prices (suggested).
Stay updated on political strategies and motivations to make informed voting decisions (implied).
</details>
<details>
<summary>
2023-01-29: Let's talk about learning about institutions from board games.... (<a href="https://youtube.com/watch?v=dKV8yXqoKiM">watch</a> || <a href="/videos/2023/01/29/Lets_talk_about_learning_about_institutions_from_board_games">transcript &amp; editable summary</a>)

A woman struggles with attending weekly game nights with her parents due to their consumption of Fox News leading to hateful behavior, prompting Beau to suggest using Monopoly to explain generational wealth.

</summary>

"You did it fine in the message."
"Connect all of the dots for them."
"If you want to make the effort and try to educate them, go this route."
"It's put you in a situation where this is how you feel."
"Realistically though, from what you've described, you're going to end up going to dinner."

### AI summary (High error rate! Edit errors on video page)

A woman in her twenties shares a tradition of playing board games with her parents every Tuesday night, but it has recently become difficult for her to attend.
The woman's parents, who recently retired, have become consumed by watching Fox News, leading to negative and hateful behavior during their game nights.
An incident occurred where the woman's parents reacted negatively to learning that her black friend received a full scholarship, assuming it was solely based on her skin color.
Despite the woman's efforts to explain her friend's achievements, her parents refused to believe her, leading to a heated argument.
The woman is struggling with the decision to continue attending these game nights due to the toxicity and hate that has tainted the once enjoyable tradition.
Beau suggests introducing new rules for Monopoly that simulate systemic disadvantages to help the woman's parents understand the concept of generational wealth.
He acknowledges the difficulty of the situation and the common issue of Fox News influencing destructive behavior in families.
Beau encourages the woman to try educating her parents using the Monopoly analogy or by showing them his video to help them understand the impact of their behavior on her.

Actions:

for children of radicalized parents,
Show the video to parents and explain the impact of their behavior (suggested)
Introduce new Monopoly rules to simulate systemic disadvantages (suggested)
</details>
<details>
<summary>
2023-01-29: Let's talk about Memphis, Scorpions, and Movies.... (<a href="https://youtube.com/watch?v=sqUJlSCtF2Y">watch</a> || <a href="/videos/2023/01/29/Lets_talk_about_Memphis_Scorpions_and_Movies">transcript &amp; editable summary</a>)

Memphis disbands the problematic Scorpion team, but the concerning strategy remains unaddressed, reflecting the broader issue of unchecked police authority in popular culture.

</summary>

"The community gets tragedy while the department boasts seized assets."
"Hollywood understands what happens when you provide that much unchecked authority."
"Teams like this lead to problems."
"Do I believe it's in good faith when they're not disavowing that strategy?"
"They knew it was a Scorpion when they picked it up."

### AI summary (High error rate! Edit errors on video page)

Memphis has decided to disband the Scorpion team responsible for troubling actions.
The strategy behind the Scorpion team, with plainclothes officers having a lot of leeway, raises concerns.
Despite the team's disbandment, there hasn't been a disavowal of the problematic strategy.
A significant percentage of officers from the team are currently facing legal consequences.
The family's attorney has been uncovering more troubling information about the team's actions.
The community gets tragedy while the department boasts seized assets.
Hollywood movies and TV shows often draw inspiration from real-life events involving unchecked police authority.
Officer Rafael Perez, associated with the Rampart scandal, was mimicked in Denzel Washington's character in "Training Day."
Perez cooperated and exposed misconduct involving over 50 cops.
Beau expresses concern about potentially losing records after the Scorpion team's closure.

Actions:

for community members, activists,
Contact local representatives to advocate for the disavowal of problematic police strategies (suggested)
Join community organizations focused on police accountability and reform (implied)
</details>
<details>
<summary>
2023-01-28: Let's talk about the mythology of amendments.... (<a href="https://youtube.com/watch?v=lkJXKIsBbNk">watch</a> || <a href="/videos/2023/01/28/Lets_talk_about_the_mythology_of_amendments">transcript &amp; editable summary</a>)

Beau questions the modern interpretation of the Second Amendment, advocating for vigilance against injustice as a core duty underlying its spirit.

</summary>

"It's like the founders put up a piece of glass, you know, break glass only in the event of tyranny."
"Tyranny is too late in the game. Things don't just flip and become totalitarian overnight."
"By definition, you have to be alert to injustice."
"You have to fulfill it. And you have to be alert to injustice, because that's what's going to precede tyranny."
"It is a duty. You have to wonder why they don't want you alert to injustice."

### AI summary (High error rate! Edit errors on video page)

Analyzing the mythology surrounding the amendments, particularly the Second Amendment and its historical context.
Emphasizing the founders' original intent behind the amendments and how it differs from modern interpretations.
Exploring the idea that being pro-Second Amendment requires being alert to injustice and using peaceful civic actions first.
Drawing parallels between the spirit of the First Amendment (freedom of speech) and the Second Amendment (right to bear arms).
Arguing that being anti-Woke contradicts the spirit of the Second Amendment, as being "woke" involves being alert to injustice.
Questioning why some leaders advocate for supporting the Second Amendment while being anti-Woke, which goes against the founding principles.
Encouraging individuals to fulfill their duty under the Second Amendment by remaining vigilant against injustice and utilizing civic actions.

Actions:

for citizens, activists, gun rights advocates,
Talk to your second amendment friends about the true spirit and duty behind it (suggested).
Be alert to injustice in your community and surroundings (implied).
</details>
<details>
<summary>
2023-01-28: Let's talk about the Constitution and mixed messages.... (<a href="https://youtube.com/watch?v=RqAR1i8xCy4">watch</a> || <a href="/videos/2023/01/28/Lets_talk_about_the_Constitution_and_mixed_messages">transcript &amp; editable summary</a>)

Beau explains mixed messages about the Constitution, focusing on the fundamental command of due process and its critical role in upholding the nation of laws.

</summary>

"No person shall be deprived of life, liberty, or property without due process."
"If you do not support the one command that the Constitution gave the governments twice, you cannot pretend that you support the Constitution."

### AI summary (High error rate! Edit errors on video page)

Explains the mixed messages about the Constitution he's been accused of giving.
Talks about how he loves the Constitution's concise and precise nature, mentioning that it's the reason why he might make a short video just reading it.
Points out that the Constitution repeats only one command, which is about not depriving any person of life, liberty, or property without due process, emphasized in both the fifth and 14th amendments.
States that this command is the foundation of everything in the Constitution, making it the glue that holds the nation together as a nation of laws.
Argues that supporting the Constitution means following this command twice given, and failure to do so means not truly supporting any part of it.
Stresses the importance of evaluating behavior through the lens of necessity to prevent deprivation of life, liberty, or property without due process.
Concludes by reiterating that failure to support this fundamental command means not genuinely supporting the Constitution in any aspect.

Actions:

for constitutional enthusiasts,
Ensure understanding and support for the fundamental command of due process (emphasized)
Advocate for the necessity of due process in all actions (implied)
</details>
<details>
<summary>
2023-01-27: Let's talk about what escalation is.... (<a href="https://youtube.com/watch?v=h5LyoQ6cXQw">watch</a> || <a href="/videos/2023/01/27/Lets_talk_about_what_escalation_is">transcript &amp; editable summary</a>)

Beau explains the recent decision to supply Ukraine as an escalation in equipment quality, not conflict, and underscores the importance of empowering Ukraine as a major power in Europe to deter aggression.

</summary>

"It's not an escalation in the conflict. It's not broadening the conflict. It isn't deepening it."
"Russia cannot go toe-to-toe with NATO."
"It's better for Ukraine to come out of this a major power in Europe."
"Putin understands that if that happens, he's number one."
"Nobody wants Russia to be destroyed."

### AI summary (High error rate! Edit errors on video page)

Explains the recent decision to supply Ukraine as not an escalation in the conflict itself, but rather an escalation in the quality of equipment being provided.
Emphasizes that the equipment is intended for mounting a counteroffensive and enhancing Ukraine's capabilities.
Points out that the conflict started with Russia launching an unprovoked surprise invasion in violation of international law.
Mentions that true escalations from this point could involve NATO getting directly involved or strategic arms, which seem unlikely.
Notes that Ukraine holding its own against Russia makes a direct conflict with NATO unrealistic for Russia.
Raises the question of how Putin might view foreign aid packages and whether he may see them as direct engagement.
Compares the situation to a street fight where someone hands a lead pipe to your opponent, causing discomfort but not necessarily leading to direct conflict with others nearby.
Indicates that NATO's decision not to directly get involved is influenced by factors such as the situation on the ground, political considerations, and future stability in Europe.
Stresses the long-term strategy of empowering Ukraine to emerge as a major power in Europe to deter further Russian aggression.
Mentions the implications of a nuclear response and the devastating consequences it could bring.

Actions:

for foreign policy analysts,
Support efforts to empower Ukraine as a major power in Europe (exemplified).
Advocate for strategic responses that prioritize stability and deter aggression (exemplified).
</details>
<details>
<summary>
2023-01-27: Let's talk about a trend and ideological consistency.... (<a href="https://youtube.com/watch?v=0B5iGRzwdeg">watch</a> || <a href="/videos/2023/01/27/Lets_talk_about_a_trend_and_ideological_consistency">transcript &amp; editable summary</a>)

Beau explains how Switzerland and Finland's actions reveal insights on Ukraine and Russia, challenging beliefs and justifications for Russian aggression and imperialism.

</summary>

"NATO cannot exist on Russia's borders."
"Just because something is in a nation's national interest doesn't mean that it's the correct moral or lawful thing to do."
"If you are saying that one country's interests do not matter unless they can be approved by another country, you're condoning imperialism."

### AI summary (High error rate! Edit errors on video page)

Explains how Switzerland and Finland can teach us about Ukraine and Russia.
Points out the trend of people believing in something off and not liking the answers when exploring the roots of their beliefs.
Addresses the idea of Russia having a national interest in invading Ukraine due to NATO's influence.
Talks about the impact of Russia's aggression on countries like Finland and Switzerland.
Mentions the issue of Russia's actions not being within international law.
Challenges the pretext of NATO's presence on Russia's borders being a valid reason for Russia's invasion of Ukraine.
Criticizes arguments defending Russia's imperialism based on historical factors and racial hierarchies.
Raises questions about defending imperialism and supporting Russian influence over sovereign countries.
Dismisses claims of Red Scare propaganda as a justification for opposing Russia.
Condemns the idea of justifying military action based on another country's approval as condoning imperialism.

Actions:

for global citizens,
Support efforts to counter misinformation about international relations (suggested).
Advocate for diplomacy and respect for sovereign countries' interests (suggested).
</details>
<details>
<summary>
2023-01-27: Let's talk about Social Security, liberals and leftists.... (<a href="https://youtube.com/watch?v=OAPm9j9GG8g">watch</a> || <a href="/videos/2023/01/27/Lets_talk_about_Social_Security_liberals_and_leftists">transcript &amp; editable summary</a>)

Republicans target Social Security, urging liberals and leftists to unite in defending its importance and promoting that everyone deserves a decent life.

</summary>

"Leftists will be upset with liberals."
"Everybody deserves a decent life."
"Everybody's entitled to that."
"Say yes, and everybody, regardless of situation, is entitled to a decent life."
"This is a moment where liberals and leftists, you have to work together."

### AI summary (High error rate! Edit errors on video page)

Republicans will inevitably target Social Security, wanting to make changes like cutting it or raising the age, as part of a long process to chip away at it.
Liberals and the left must be unified when defending Social Security, despite potential disagreements over phrasing and motivations.
Leftists may be upset with liberals not for what they want to do, but for why they want to do it.
Liberals tend to argue that Social Security is not welfare, but something they paid into and are entitled to, while leftists stress that everyone deserves a decent life, regardless of contributions.
Leftists aim to prevent the conditioning of viewing a difference between welfare and earned benefits, advocating for the idea that everybody deserves a decent life.
Social Security is not solely for those who worked their entire lives; it serves broader purposes, reinforcing the notion that everybody is entitled to a decent life.
Leftists urge for unity in defending all social safety nets and promoting the concept that everyone deserves a decent life.
Advocates should approach the defense of Social Security positively, focusing on the idea that regardless of circumstances, everyone is entitled to a decent life.
Referencing the Constitution's goal to "promote the general welfare" supports the argument for protecting Social Security as a vital safety net.
Collaboration between liberals and leftists is vital in facing potential Republican attacks on Social Security.

Actions:

for advocates, activists, citizens,
Unite liberals and leftists in defending Social Security (implied)
Advocate for the idea that everyone deserves a decent life (exemplified)
</details>
<details>
<summary>
2023-01-27: Let's talk about Senate predictions.... (<a href="https://youtube.com/watch?v=173kt8sO1Qk">watch</a> || <a href="/videos/2023/01/27/Lets_talk_about_Senate_predictions">transcript &amp; editable summary</a>)

Beau advises ignoring early polls and focusing on policy and candidate quality, citing the unpredictability of future political dynamics.

</summary>

"Ignore the polls, they do not matter, they're worthless."
"It is way too early to even begin to start to assess what is going to happen in 2024."
"We're not in normal political times."
"Do the job. You do that, you don't have to worry about the polling."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Recent polls show the Democratic Party is not in a good position for the Senate in 2024.
Advises the Democratic Party to ignore the polls as they are worthless and too early to predict.
Points out that predicting outcomes in 2024 is premature due to many potential changes in political dynamics.
Mentions factors like the economy, situation in Ukraine, and Supreme Court decisions that could impact voter preferences.
Emphasizes that current political times are far from normal and unprecedented.
Cites an incident where a high-ranking Democrat's child was involved in assaulting a cop, overshadowed by other news.
Notes that some politicians predicted to hold their seats are under investigation, potentially affecting polling numbers.
Stresses the importance for any party to focus on enacting good policy and candidate quality.
Suggests that if polls were accurate, the focus wouldn't be on Senate defense but on retaking it.
Criticizes polling models for not accurately reflecting shifts in voter turnout and behavior.
Urges parties to concentrate on doing their job well instead of worrying about early polling data.

Actions:

for politically engaged individuals,
Focus on enacting good policy and supporting candidates (suggested)
Disregard early polling data and concentrate on fulfilling responsibilities (suggested)
</details>
<details>
<summary>
2023-01-26: Let's talk about Soledar and Bunker Hill.... (<a href="https://youtube.com/watch?v=MT4xLOSPd7E">watch</a> || <a href="/videos/2023/01/26/Lets_talk_about_Soledar_and_Bunker_Hill">transcript &amp; editable summary</a>)

Beau addresses the Russian capture of Solidar, drawing parallels to historic battles and cautioning about potential consequences for Russia's forces in the ongoing conflict in Ukraine.

</summary>

"Y'all know we lost that battle, right?"
"It had devastating consequences for one of the only effective Russian units."
"If Russia has a few more victories like that, they're not going to have many people left to withdraw."

### AI summary (High error rate! Edit errors on video page)

Responding to a message about not providing proper coverage of events, Beau addresses the Russian capture of Solidar, Ukraine, after a lengthy fight.
Draws a parallel to the Battle of Bunker Hill during the American Revolution, where the Americans lost but it is celebrated in U.S. history.
Points out that the British victory at Bunker Hill was costly, and they paid a high price for capturing the hill.
Suggests that there may be lessons from history, particularly the Battle of Bunker Hill, that relate to the situation in Solidar.
Raises concerns about the potential devastating consequences for Russia despite their capture of the town.
Notes that the conflict in Ukraine is in a protracted stage with lines moving back and forth until a breakthrough occurs.
Warns that if Russia continues to have victories like Solidar, they may deplete their forces rapidly.

Actions:

for history enthusiasts,
Analyze historical battles for potential lessons (implied)
</details>
<details>
<summary>
2023-01-26: Let's talk about Newport News and remembering your training.... (<a href="https://youtube.com/watch?v=mtdb1kscZ1U">watch</a> || <a href="/videos/2023/01/26/Lets_talk_about_Newport_News_and_remembering_your_training">transcript &amp; editable summary</a>)

Lawyers allege school administrators ignored warnings in Newport News, underscoring the need for proactive safety measures and adult accountability.

</summary>

"The children remembered their training. Maybe it's time for everybody else to start remembering theirs."
"We teach these kids, if you see something, say something. Drill it into them."
"It could have been way, way worse."
"This is probably a moment for school administrators to realize the idea of, oh, it can't happen here. That's not a thing."
"There was no guarantee that that's how it was going to turn out."

### AI summary (High error rate! Edit errors on video page)

Lawyers in Newport News allege school administrators were warned three times on the day of the shooting involving a six-year-old.
A child reported being threatened by another student with a gun, but no action was taken by the administrators.
Despite being warned, the administrators failed to act and even discouraged someone from checking on the situation.
The story initially made headlines but faded, likely to resurface due to these allegations.
The incident underscores the reliance on children to follow safety protocols when adults fail to act.
There was no guarantee of a safe outcome, and the situation could have been much worse.
Similar to past incidents, there may be public outcry over the lack of preventive action by authorities.
This case serves as a reminder that school safety is not guaranteed and can happen anywhere.
It's time for school administrators to recognize the potential risks and take proactive measures.
Beau concludes with a call for everyone to be vigilant and prepared in such situations.

Actions:

for school administrators,
Educate school staff on responding promptly to safety concerns (implied)
Implement regular drills and training for staff and students (implied)
</details>
<details>
<summary>
2023-01-26: Let's talk about McCarthy's deals and the Tax.... (<a href="https://youtube.com/watch?v=pney-5Pf_eE">watch</a> || <a href="/videos/2023/01/26/Lets_talk_about_McCarthy_s_deals_and_the_Tax">transcript &amp; editable summary</a>)

The Republican Party faces tension over the Fair Tax Act, revealing rifts and potential division within the party.

</summary>

"It's going to be an abject failure for the holdouts."
"This was never getting anywhere."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is facing issues with deals made by McCarthy to become Speaker, causing tension within the party.
One major point of contention is the Fair Tax Act, aiming to eliminate the IRS and implement a 30% sales tax, disproportionately affecting lower-income individuals.
The Act benefits the ultra-wealthy while burdening the working and upper middle class.
McCarthy is under scrutiny for promising a vote on the Fair Tax Act, which some Republicans interpret differently.
McCarthy plans to oppose the bill, signaling its likely failure even in the House.
The Act was primarily a political stunt for social media engagement, not a realistic legislative endeavor.
Less extreme Republicans understood the negative impact of the Act on their constituents.
McCarthy's opposition and other Republicans unwilling to vote for it indicate the Act may not pass.
The failure of this legislation may alienate far-right Republicans from their base and push them towards less extreme factions.
The situation may reveal rifts within the Republican Party and challenge their unity.

Actions:

for politically engaged individuals,
Contact your representatives to voice opposition to the Fair Tax Act (implied)
Join advocacy groups working against regressive tax policies (implied)
</details>
<details>
<summary>
2023-01-26: Let's talk about Biden's impeachment and the GOP Senate.... (<a href="https://youtube.com/watch?v=nWPxNl5Ue2U">watch</a> || <a href="/videos/2023/01/26/Lets_talk_about_Biden_s_impeachment_and_the_GOP_Senate">transcript &amp; editable summary</a>)

Senate Republicans prioritize political capital and astuteness over drama, avoiding risky battles and focusing on maintaining public perception.

</summary>

"Senate Republicans prioritize political capital and avoid losing battles in the media."
"Senate Republicans are politically astute rather than moderate."
"Republicans in the Senate avoid taking stances that may harm their political careers."
"The Senate Republicans prioritize always appearing to win to maintain public perception."

### AI summary (High error rate! Edit errors on video page)

Senate Republicans declined to impeach Biden over the documents case, showing political savvy.
Republicans in the House want to impeach Biden, but the Senate is not on board.
Senate Republicans prioritize political capital and avoid losing battles in the media.
They won't risk convicting Biden during an impeachment with insufficient votes.
The Senate Republicans are politically astute rather than moderate.
They won't support initiatives like the Fair Tax Act that could backfire with voters.
Republicans in the Senate avoid taking stances that may harm their political careers.
Senate Republicans differ from the more dramatic House Republicans in their approach.
House Republicans focus on social media engagement but may struggle to deliver on promises.
The Senate Republicans prioritize always appearing to win to maintain public perception.

Actions:

for politically-aware citizens,
Support politicians who prioritize practicality over drama (suggested)
Engage in constructive political discourse based on realistic outcomes (suggested)
</details>
<details>
<summary>
2023-01-25: Let's talk about the language of journalism.... (<a href="https://youtube.com/watch?v=t5C3NaNDO6E">watch</a> || <a href="/videos/2023/01/25/Lets_talk_about_the_language_of_journalism">transcript &amp; editable summary</a>)

The importance of journalistic terminology like "alleged" and the impact of legal perspective on reporting, with a warning about an upcoming disturbing footage.

</summary>

"If somewhere else in that passage of that article, whatever, blame seems to be implied, oh, that's alleged."
"That's why you hear that word so much."
"Until they're convicted, they're an alleged armed robber."
"Be ready for it. When the footage comes out, just kind of brace yourself."
"The city government up there is already trying to distance itself from the media."

### AI summary (High error rate! Edit errors on video page)

Explains why the word "alleged" is frequently used in journalism to protect outlets from being sued.
Mentions that the absence of certain words like "alleged" can signify commentary rather than straight reporting.
Shares a personal experience of using the term "alleged" in reporting and the editor's explanation on its necessity.
Talks about passive language in journalism, such as using "death of" instead of "murder of," until legally confirmed.
Recounts a friend getting criticized for using "death of" in reporting due to legal and journalistic considerations.
Advises to think from a legal perspective when critiquing journalists' choice of terminology like "murder" or "killed."
Mentions a case in Memphis that will likely stir strong emotions once the footage is released, warning viewers to brace themselves.

Actions:

for journalists, reporters, activists,
Brace yourself for the disturbing footage release (implied)
</details>
<details>
<summary>
2023-01-25: Let's talk about the Pence documents case.... (<a href="https://youtube.com/watch?v=JuqaSQoH7s8">watch</a> || <a href="/videos/2023/01/25/Lets_talk_about_the_Pence_documents_case">transcript &amp; editable summary</a>)

Pence's document mishap underscores national security risks, stressing the need for accountability and preventive measures like counterintelligence filter teams.

</summary>

"None of this is good."
"Counterintelligence filter teams, they need to be a thing."
"It cannot continue to happen."
"That's the difference, and that's what's going to matter."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Pence's lawyer found documents with classification markings in boxes that had been moved from the White House to temporary and then permanent residence.
The documents were secured by contacting the National Archives, who then involved the National Security Division of the FBI.
Pence's actions didn't seem to indicate any wrongdoing, and proper steps were taken once the documents were discovered.
Comparisons are made between Pence's case and Biden's, with emphasis on Pence's cooperation and lack of criminal aspects.
The issue revolves around national security implications rather than criminal liability.
The importance of "willful retention" in potential criminal aspects is emphasized, contrasting with national security concerns.
The broadcast of this incident to foreign intelligence agencies poses significant national security risks.
Suggestions are made for implementing a counterintelligence filter team during transitions to prevent such incidents.
Support for this initiative is deemed bipartisan as it serves both national security interests and prevents potential scandals.
Emphasis is placed on the need for cooperation, speed of document return, and awareness by individuals in handling such sensitive documents.

Actions:

for government officials, policymakers,
Establish counterintelligence filter teams during transitions (suggested)
Ensure proper handling and awareness of sensitive documents (implied)
</details>
<details>
<summary>
2023-01-25: Let's talk about insight into Trump's Georgia situation.... (<a href="https://youtube.com/watch?v=Hqm_H7UFEyM">watch</a> || <a href="/videos/2023/01/25/Lets_talk_about_insight_into_Trump_s_Georgia_situation">transcript &amp; editable summary</a>)

Beau analyzes a Georgia event involving a sealed grand jury report, hinting at impending charges with careful wording and uncertainty about who will be charged.

</summary>

"The state understands the media's inquiry in the world's interest, but we have to be mindful of protecting future defendants' rights."
"A decision could be made to not charge, but based on that statement, I don't think that's what's happening."

### AI summary (High error rate! Edit errors on video page)

Analyzing a recent event in Georgia involving a special purpose grand jury report.
District attorney received a sealed report a couple of weeks ago, sparking media interest.
Media was trying to access the report to gain insight into the DA's intentions.
The statement made by the state attorney during the hearing stands out, focusing on protecting future defendants' rights.
Speculation that the district attorney is moving towards attempting to gain indictments based on the sealed report.
Uncertainty remains about who will be charged and for what specific reasons.
Georgia's laws provide various options for charging individuals involved in the case.
The famous "Find Me the Votes" phone call is part of the investigation, but details are scarce.
Despite public anticipation, the charging decisions are imminent, with a possibility of not charging.
The district attorney's careful choice of words suggests a potential move towards pursuing charges.

Actions:

for legal analysts,
Stay informed about updates on the Georgia case and legal proceedings (implied)
</details>
<details>
<summary>
2023-01-25: Let's talk about Abrams approval and timelines.... (<a href="https://youtube.com/watch?v=BRZbDwuhw64">watch</a> || <a href="/videos/2023/01/25/Lets_talk_about_Abrams_approval_and_timelines">transcript &amp; editable summary</a>)

Unofficial reports suggest the US may send Abrams tanks to Ukraine through a slower drawdown process, aiming to bolster logistics and maintain pressure on Russia until withdrawal.

</summary>

"The wider war, it's done. Russia lost."
"Hopefully, with this equipment coming in, it demonstrates pretty clearly that NATO is not going to stop."
"Wars are fought to achieve other geopolitical goals. Almost immediately, they failed or backfired."

### AI summary (High error rate! Edit errors on video page)

Unofficial reports suggest the United States is considering sending Abrams tanks to Ukraine.
The delivery mechanism for the tanks is rumored to be through a slower process called a drawdown.
This method of delivery could provide time to build a beefier logistical network.
The decision to opt for a slower delivery method could be influenced by policymakers' views on logistics.
The slower delivery could also allow for Abrams tanks to arrive in a second wave, considering potential destruction in combat.
The combination of challengers, leopards, Bradleys, and strikers for Ukraine presents a formidable force against Russia.
The equipment provided by NATO demonstrates resolve in supporting Ukraine.
The wider war is deemed over, with Russia losing and facing continued pressure until they leave.
The hope is that the equipment's arrival convinces Russia to withdraw without conflict.
The ultimate goal is for the equipment to be shipped to Ukraine but never used.

Actions:

for foreign policy analysts,
Support policies that strengthen logistical networks for military aid delivery to conflict zones (implied).
Advocate for sustained international support to provide necessary equipment to conflict-affected regions (exemplified).
</details>
<details>
<summary>
2023-01-24: Let's talk about the T-14's prospects.... (<a href="https://youtube.com/watch?v=_f4GJxRceyE">watch</a> || <a href="/videos/2023/01/24/Lets_talk_about_the_T-14_s_prospects">transcript &amp; editable summary</a>)

Beau breaks down the potential versus reality of Russia's T-14 tank, from production delays to combat readiness, revealing significant limitations.

</summary>

"On paper, this tank is amazing. It is amazing. It can compete."
"It's not a competitor, because as far as it being a real force on the battlefield, it doesn't really exist yet."
"The crew is down in like a capsule, an armored capsule inside to improve survivability, which is great."

### AI summary (High error rate! Edit errors on video page)

Explains the discrepancy between the T-14 tank's potential on paper versus its real-world limitations.
Points out the lack of numbers and production delays for the T-14 tank in Russia.
Mentions the potential design flaws and issues with the T-14 tank that may only become apparent in combat.
Talks about the logistical challenges of maintaining and repairing the T-14 tank in Ukraine.
Raises concerns about the T-14 tank's untested design and limited production numbers affecting its effectiveness in combat.
Mentions the rumor about a major issue with the turret of the T-14 tank.
Addresses the unique design features of the T-14 tank, such as the unmanned gun and reliance on electronics.
Indicates that even if the T-14 tanks were deployed, they lack the necessary support and resources to be effective on the battlefield.

Actions:

for military analysts,
Train mechanics on how to work on new equipment (suggested)
Ensure availability of spare parts for military equipment (suggested)
</details>
<details>
<summary>
2023-01-24: Let's talk about eggs, supply, and demand.... (<a href="https://youtube.com/watch?v=RSRdIMI54Ig">watch</a> || <a href="/videos/2023/01/24/Lets_talk_about_eggs_supply_and_demand">transcript &amp; editable summary</a>)

The price of eggs in the US rises dramatically due to increased demand and avian flu, sparking debates on supply, demand, and potential price fixing, with a call to shift strategies by reducing demand across various sectors.

</summary>

"Reduce demand. Apply that strategy elsewhere."
"Rather than continuing failure after failure after failure, shift the strategy."
"Reduce demand rather than increase militarization."
"Demand. There aren't enough people who have to deal with that."
"The government's doing something wrong, and we should eliminate the need for people to do this."

### AI summary (High error rate! Edit errors on video page)

The price of eggs in the United States has dramatically increased, with prices rising anywhere from 62% to 104%.
The industry attributes the price increase to increased demand and avian flu, leading to a decrease in egg production.
Some individuals suspect price fixing among larger suppliers, artificially inflating egg prices.
Republicans are trying to connect the increased egg prices to issues at the southern border, sparking outrage.
Beau points out that demand drives people to seek alternatives like buying cheaper insulin or eggs from across the border.
He suggests that instead of focusing on increasing enforcement and militarization, efforts should be made to reduce demand for certain goods.
Beau advocates for shifting strategies to reduce demand and address root causes of issues rather than perpetuating failed approaches.

Actions:

for policy advocates, activists, consumers,
Advocate for policies that aim to reduce demand for goods rather than focus solely on enforcement and militarization (implied)
</details>
<details>
<summary>
2023-01-24: Let's talk about an FBI CI agent getting arrested in New York.... (<a href="https://youtube.com/watch?v=xKfEV45KmKM">watch</a> || <a href="/videos/2023/01/24/Lets_talk_about_an_FBI_CI_agent_getting_arrested_in_New_York">transcript &amp; editable summary</a>)

Former top FBI agent's arrest on charges related to working with a Russian oligarch sparks inferences about Trump probe involvement, caution urged against premature conclusions.

</summary>

"This is a big deal. Expect a lot of coverage."
"Jumping to conclusions without evidence can be detrimental and worse for Trump."
"It's too early to draw definite conclusions."
"The story is likely to receive extensive coverage and remain in the news cycle for a while."

### AI summary (High error rate! Edit errors on video page)

Breaking news about a former top FBI agent's arrest on charges related to working with a Russian oligarch with ties to Russian intelligence.
The FBI did not cover up the arrest, and the charges are serious.
Inferences are being made that the former agent's involvement in the Trump probe may have impacted the investigations, but there's no solid evidence yet.
Jumping to conclusions without evidence can be detrimental and worse for Trump.
While there may be a thorough investigation into the agent's past cases, it's too early to draw definite conclusions.
The situation could have significant implications but requires more evidence before making definitive statements.
The story is likely to receive extensive coverage and remain in the news cycle for a while.

Actions:

for news consumers,
Stay informed about developments in the case and critically analyze information provided (implied).
Avoid jumping to conclusions without substantial evidence (implied).
</details>
<details>
<summary>
2023-01-24: Let's talk about Tuesday in Georgia, Trump, and what's next.... (<a href="https://youtube.com/watch?v=STvmiBKfzsU">watch</a> || <a href="/videos/2023/01/24/Lets_talk_about_Tuesday_in_Georgia_Trump_and_what_s_next">transcript &amp; editable summary</a>)

Georgia's pivotal investigation into alleged election interference by Trump and team nears a critical juncture as the public awaits the DA's statements and potential outcomes, anticipating significant political and legal ramifications.

</summary>

"The Georgia case has been quietly moving along, and we're nearing the end of it."
"If Trump is indicted, it'll be the first time that a former president's been indicted."

### AI summary (High error rate! Edit errors on video page)

Georgia is in the spotlight due to a proceeding related to the investigation into alleged election interference by Trump and his team.
The hearing will decide if the special grand jury report becomes public, providing insight into the case's direction.
The DA's upcoming statement during the proceeding is expected to offer clarity on potential outcomes.
The investigation's findings, including any exculpatory evidence or damning revelations, remain undisclosed.
Trump's team will not attend the hearing, claiming they weren't invited to participate.
The future post-proceeding could lead to either significant developments or no further action.
Potential outcomes include the case being dropped or resulting in indictments.
The proceedings mark a critical phase for revealing the investigation's findings.
Political implications are substantial, especially if Trump or his team face indictments.
Indicting a former president, if it happens, will have far-reaching effects within the GOP and beyond.
The state-level nature of the case increases the likelihood of significant actions being taken.
The DA's office has been secretive, making it uncertain if the report will be released or if new information will emerge.
Despite seeming stagnant, the Georgia case has been progressing quietly and is now reaching a conclusion.

Actions:

for legal analysts, political commentators, interested citizens,
Monitor updates on the Georgia case (suggested)
Stay informed about the legal and political implications of the proceedings (suggested)
</details>
<details>
<summary>
2023-01-23: Let's talk about proposed detrumpification laws.... (<a href="https://youtube.com/watch?v=lkacky3MUJo">watch</a> || <a href="/videos/2023/01/23/Lets_talk_about_proposed_detrumpification_laws">transcript &amp; editable summary</a>)

Beau examines state-level laws aimed at detrumpification but raises concerns about their vague definitions and potential misuse, stressing the need for more specific legislation.

</summary>

"The laws appear to be structured with the intent of detrumpification, of making sure that those people who participated in the Sixth can't hold public office."
"We have to acknowledge that what happened on the 6th was special. It was different."
"Creating a statute that basically bars them from future employment is a little much when it's this vague."
"While I like the idea in theory, the application seems like it needs a little work to me."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Exploring proposed laws at the state level aimed at detrumpification, preventing those involved in the Sixth from holding public office or positions of public trust.
Laws are surfacing in places like Connecticut, New York, and Virginia, designed around words like insurrection, rebellion, and sedition, which aren't commonly charged.
Concerns about the vague definitions within the laws, particularly regarding crimes related to the specified offenses and how they could be misused in the future.
Beau expresses a lack of opposition to the idea behind the laws but is worried about their potential selective application and unforeseen consequences.
Acknowledging the uniqueness of the events of the Sixth and the need for specific legislation that doesn't overly restrict future employment opportunities based on vague criteria.
Emphasizing the importance of ensuring that the laws are well-defined to prevent potential misuse or unjust applications down the line.

Actions:

for legislative observers,
Contact your state representatives to express concerns about the vague definitions in proposed laws (suggested).
Advocate for clear and specific legislation to prevent potential misuse and unjust applications (implied).
</details>
<details>
<summary>
2023-01-23: Let's talk about Pete Buttigieg, language, and consistency.... (<a href="https://youtube.com/watch?v=C53FHXqq2wM">watch</a> || <a href="/videos/2023/01/23/Lets_talk_about_Pete_Buttigieg_language_and_consistency">transcript &amp; editable summary</a>)

Beau explains the resistance to using gender-neutral terms and addresses the underlying bigotry in social conservatives' response to Pete Buttigieg's marriage.

</summary>

"They want to be addressed in a way that reflects them."
"It's not that you didn't understand. It's that you didn't want to."
"People go through title changes all the time, and it normally marks the growth of that person."
"You'll feel better about your lot in life if you can kick down at somebody."
"It's that somebody had told them, you can other somebody this way."

### AI summary (High error rate! Edit errors on video page)

Explains the request for basic courtesy and respect in addressing individuals based on their title, name, and pronouns.
Describes the common response of "I just don't get it" when discussing gender identity and pronouns.
Notes a shift in social conservatives' response towards Pete Buttigieg and his husband regarding the term "husband."
Questions the inconsistency in social conservatives' argument for using gender-neutral terms like "partner" instead of "husband."
Points out the underlying motivation of bigotry and discrimination rather than genuine lack of understanding.
Emphasizes the celebration of title changes as reflective of personal growth and identity.

Actions:

for all individuals,
Support and respect individuals by addressing them using their preferred title, name, and pronouns (implied)
Challenge discriminatory attitudes and behaviors towards marginalized communities (implied)
</details>
<details>
<summary>
2023-01-23: Let's talk about Germany's change of heart.... (<a href="https://youtube.com/watch?v=sS2pJwzCjJM">watch</a> || <a href="/videos/2023/01/23/Lets_talk_about_Germany_s_change_of_heart">transcript &amp; editable summary</a>)

Germany signals approval for exporting tanks to Ukraine, intertwining national interests of multiple countries in the ongoing situation.

</summary>

"Germany signals approval for the export of leopard tanks to Ukraine."
"Poland's national interests intersect with supporting Ukraine due to safety concerns."
"Ukraine likely to receive tanks with hopes for Germany's commitment."

### AI summary (High error rate! Edit errors on video page)

Germany signals approval for the export of leopard tanks to Ukraine.
US involvement in potentially sending Abrams tanks discussed.
Poland's national interests in supporting Ukraine due to safety concerns.
Germany's interests in wealth and safety intersect with selling military equipment to Poland.
Ukraine's situation and potential impact on Poland's borders.
Ukraine likely to receive tanks with hopes for Germany's commitment.
Multiple countries have vested interests in Ukraine's success.

Actions:

for foreign policy analysts,
Support military aid to countries in need, like Ukraine (implied)
Advocate for international cooperation and support for Ukraine (implied)
</details>
<details>
<summary>
2023-01-22: Let's talk about what men can learn about masculinity from women.... (<a href="https://youtube.com/watch?v=0fK_KdcaCN8">watch</a> || <a href="/videos/2023/01/22/Lets_talk_about_what_men_can_learn_about_masculinity_from_women">transcript &amp; editable summary</a>)

Beau dismantles traditional ideas of masculinity, urging men to prioritize community service over superficial traits to embody true manhood.

</summary>

"They're not teaching you to be a man. They're teaching you to be a woman in the 1800s."
"Your masculinity will be perfect if you just get out there and help."
"If you want to achieve that good masculinity, that kind that doesn't get made fun of in razor commercials, you have to think for yourself."

### AI summary (High error rate! Edit errors on video page)

Addresses the concept of masculinity and what women from the pre-women's liberation era can teach modern men about it.
Dismisses the idea of "alpha" and "traditional masculinity," stating they are not real concepts.
Describes the advice given to women in the past on how to attract a man, including posture, topics to talk about, and appearance.
Talks about the shifting views of masculinity and the confusion it causes for young men.
Criticizes the advice given to men today on how to be masculine, likening it to teaching them to be women in the 1800s.
Mentions the importance of heroes exhibiting universal qualities like problem-solving and community protection.
Shares a Japanese concept of masculinity related to achieving perfection without effort and helping the community.
Criticizes those pushing consumerism as a form of masculinity and encourages helping the community instead.
Emphasizes that masculinity is not unified and individuals should focus on genuine actions to make the world better.
Urges men to think for themselves and to prioritize helping others as a true representation of masculinity.

Actions:

for men, young adults,
Help your community by volunteering, supporting others, and making the world better (implied).
</details>
<details>
<summary>
2023-01-22: Let's talk about the US and Russia cooking the books.... (<a href="https://youtube.com/watch?v=i77bVhs9mv4">watch</a> || <a href="/videos/2023/01/22/Lets_talk_about_the_US_and_Russia_cooking_the_books">transcript &amp; editable summary</a>)

Comparing economic strategies of Russia and the U.S., revealing how book cooking serves different intents, with sanctions being likened to a tool of war.

</summary>

"Sanctions are often viewed as a tool of diplomacy, they're more often than not used as a tool of war."
"The Republican Party manufacturing a crisis over the debt ceiling is having roughly the same effects that another country imposing sanctions on the U.S. would have."
"It's performative for social media. It's not actual policy."
"The Republican Party is actively working against the United States economic stability."
"If a nation imposed sanctions on the United States, that's pretty interesting and pretty telling."

### AI summary (High error rate! Edit errors on video page)

Comparing economic situations of the United States and Russia, focusing on book cooking methods.
Russia cooking the books to project strength internationally while the U.S. is doing it to avoid default.
Russia artificially keeping the ruble strong and manipulating unemployment statistics to appear strong.
U.S. engaging in internal book cookery to keep paying bills and avoid defaulting.
Differences lie in intent: Russia for international image and U.S. to maintain domestic situation.
Sanctions as a tool of war rather than diplomacy; Republican Party causing economic crisis over debt ceiling.
Republican Party actions can be compared to a nation imposing sanctions on the U.S., actively working against economic stability.

Actions:

for economic analysts, policymakers,
Analyze economic policies (suggested)
Stay informed on political actions (suggested)
</details>
<details>
<summary>
2023-01-22: Let's talk about political ideas limiting earning potential.... (<a href="https://youtube.com/watch?v=MX4Qyb2i2q8">watch</a> || <a href="/videos/2023/01/22/Lets_talk_about_political_ideas_limiting_earning_potential">transcript &amp; editable summary</a>)

Conservative ideas instilled in children can limit future earning potential, as outdated notions like bigotry are not marketable in the job market.

</summary>

"It's just capitalism. These ideas don't sell. These ideas cause economic damage. Therefore, they have to go."
"The hate, the bigotry, the intolerance, they're not marketable."
"Conservative values like bigotry may harm future job prospects."

### AI summary (High error rate! Edit errors on video page)

Conservatives instilling ideas in children may limit future earning potential.
Concepts already in motion, not widely recognized.
Example: person with outdated speech patterns unlikely to get hired.
Illustration: high-pressure office job search lacking suitable candidates.
Example of adaptability: fast food worker impresses potential employer.
Employer overlooks worker's felony history but dismisses him for inappropriate comment.
A single comment changes the worker's future.
Conservative values like bigotry may harm future job prospects.
Bumper stickers or symbols indicating intolerance can impact job interviews.
These outdated ideas are not marketable and cause economic harm.

Actions:

for parents, educators, employers,
Challenge outdated beliefs and prejudices within your community (implied)
Encourage open-mindedness and acceptance in children (implied)
Advocate for diversity and inclusion in the workplace (implied)
</details>
<details>
<summary>
2023-01-21: Let's talk about Sinema, Gallego, and Arizona.... (<a href="https://youtube.com/watch?v=yaeY4j77Pm4">watch</a> || <a href="/videos/2023/01/21/Lets_talk_about_Sinema_Gallego_and_Arizona">transcript &amp; editable summary</a>)

Sinema's strategic move, Gallego's candidacy, and the urgent call for Democratic Party unity in Arizona's Senate race for 2024.

</summary>

"The Democratic Party has to overcome her and whatever Republican challengers put up."
"They have to start pushing right now, this minute."
"The Democratic Party has to start organizing now, today, for 2024."

### AI summary (High error rate! Edit errors on video page)

Sinema's move to become an independent in Arizona is perceived as a strategic political calculation to avoid a challenging Democratic primary.
Gallego, identified as a progressive Democrat, has announced his intention to run for the Senate race in 2024.
The Democratic Party needs to act swiftly and unite behind a candidate to overcome Sinema and potential Republican challengers.
Gallego, a Marine combat vet, is known for his outspokenness and colorful rhetoric, particularly regarding the events of January 6.
The importance of the Democratic Party starting early to build momentum and support for the upcoming election is emphasized to prevent a far-right Republican from winning the seat.
There is speculation about whether Sinema will run for re-election, with the possibility of her not running if a strong Democratic candidate emerges.

Actions:

for democratic party members in arizona,
Start organizing for the 2024 Senate race today (suggested)
Overcome potential Republican challengers by uniting behind a candidate (implied)
</details>
<details>
<summary>
2023-01-21: Let's talk about Pink Floyd, rainbows, and social media.... (<a href="https://youtube.com/watch?v=mp_wnxdVLZI">watch</a> || <a href="/videos/2023/01/21/Lets_talk_about_Pink_Floyd_rainbows_and_social_media">transcript &amp; editable summary</a>)

Pink Floyd's social media tribute sparks backlash, revealing deeper fears of manipulation and division rooted in societal power dynamics.

</summary>

"You're making yourself look stupid. What is that, Pink Floyd? What a disgrace. From this moment, I don't listen to this band."
"If you don't stop letting these people control your every thought by giving you somebody to blame, you're gonna end up another brick in the wall."

### AI summary (High error rate! Edit errors on video page)

Pink Floyd changed their social media profile pictures to commemorate the 50th anniversary of Dark Side of the Moon, featuring a design related to the album's iconic cover.
Some individuals reacted negatively to the rainbow element in the profile picture, questioning its relevance and accusing Pink Floyd of being "woke."
The album cover of Dark Side of the Moon features light hitting a prism, creating a rainbow, unrelated to the current association with LGBTQ+ pride.
Beau notes Pink Floyd's history of being politically engaged and mentions the album's 1973 release date, five years before the rainbow flag became a symbol of Pride.
The backlash against the rainbow in Pink Floyd's profile picture stems from a desire for "equal representation," but Beau argues that default representation already favors the majority.
Beau criticizes the insecurity and anger fueling such comments, suggesting they are manipulated by those in power to maintain control through fear and division.
Authoritarians historically use scapegoating and fear-mongering to control populations, directing anger towards specific groups to manipulate behavior.
Beau warns against falling into a pattern of blaming and fearing others, stressing the importance of breaking free from manipulation to avoid generational consequences.
Not addressing the manipulation and fear tactics perpetuated by certain groups could lead to a perpetuation of ignorance and timidity among future generations, creating an underclass.
Beau urges listeners to resist being controlled by divisive narratives and avoid becoming complicit in perpetuating harmful systems.

Actions:

for social media users,
Resist manipulation and fear tactics by critically analyzing information and narratives spread on social media (implied).
Foster understanding and empathy towards diverse perspectives to combat divisive narratives (implied).
</details>
<details>
<summary>
2023-01-21: Let's talk about DOJ telling House Judiciary to take a hike.... (<a href="https://youtube.com/watch?v=ZuJLTcDTX7A">watch</a> || <a href="/videos/2023/01/21/Lets_talk_about_DOJ_telling_House_Judiciary_to_take_a_hike">transcript &amp; editable summary</a>)

The Department of Justice's refusal to disclose ongoing investigations to the House Judiciary Committee may hinder the effectiveness of future hearings, rooted in a long-standing policy of non-disclosure.

</summary>

"The Department of Justice does not comment on active investigations when it comes to congressional requests."
"It's not a thing where it's the Democrats stonewalling the Republican Party."
"Concerns expressed about ongoing investigations are unrealistic because department policy prohibits 99% of what people are worried about."

### AI summary (High error rate! Edit errors on video page)

The Department of Justice declined to provide the House Judiciary Committee with information on ongoing investigations, citing long-standing policy.
The DOJ stated that they will not confirm or deny the existence of pending investigations or provide non-public information in response to congressional requests.
This decision means that upcoming House Judiciary hearings may lack substance, as many topics are related to active investigations.
The separation of powers between the executive branch, where the DOJ resides, and Congress is emphasized as the reason behind this practice.
Members of the House Judiciary Committee may face challenges in fulfilling their claims and promises made on social media due to the lack of progress in investigations.
The DOJ's stance prevents the committee from obtaining non-public information about ongoing criminal investigations.
Concerns raised about this issue are deemed unrealistic because DOJ policy restricts most of what people are worried about.
Beau was criticized for not discussing this topic earlier, but he explains that the DOJ's policy makes it unlikely for the information sought by the committee to be provided.

Actions:

for legislative aides, policymakers,
Contact your representatives to advocate for transparency and accountability in government practices. (implied)
</details>
<details>
<summary>
2023-01-20: Let's talk about what liberals get wrong.... (<a href="https://youtube.com/watch?v=3N8MWVg7u84">watch</a> || <a href="/videos/2023/01/20/Lets_talk_about_what_liberals_get_wrong">transcript &amp; editable summary</a>)

Understanding basic vocabulary is key to being taken seriously in debates, particularly on contentious topics like firearms; focus on meaningful policy actions over trivial social media debates.

</summary>

"If you don't know the vocabulary, you have no business talking about this subject."
"Owning the libs on social media is not actually policy."
"Engage with real-world, substantive policy actions."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of understanding vocabulary terms when discussing a subject to be taken seriously.
Points out how not knowing basic terms can derail a conversation, especially when discussing firearms.
Mentions the confusion around the term AR and how it's commonly misinterpreted as automatic rifle or assault rifle.
Explains that AR actually stands for Armalite Rifle, derived from "A" for Armalite and "R" for rifle.
Criticizes those who get the AR acronym wrong, stating it's not just limited to Armalite Rifle.
Challenges the notion that AR designates civilian use, citing examples like AR-22 and 23 designed for training purposes in a Mark 19.
Notes the historical context of Armalite selling the AR-15 to Colt and subsequently naming their own release as M-15.
Condemns gun enthusiasts who argue over acronyms instead of engaging in meaningful dialogues on pressing issues like school shootings.
Urges individuals to focus on substantive policy actions rather than just "owning the libs" on social media.
Encourages constructive engagement in real-world issues rather than getting caught up in trivial online debates.

Actions:

for policy advocates,
Correct misinformation on firearms terminology (implied)
Engage in constructive dialogues on gun control and school safety measures (implied)
</details>
<details>
<summary>
2023-01-20: Let's talk about the debt ceiling and credit cards.... (<a href="https://youtube.com/watch?v=Rongpo2TbqQ">watch</a> || <a href="/videos/2023/01/20/Lets_talk_about_the_debt_ceiling_and_credit_cards">transcript &amp; editable summary</a>)

Beau explains the debt ceiling reality, Republican hypocrisy, and the looming economic instability due to political games.

</summary>

"It's not a credit limit. It's a self-imposed thing."
"The idea of a fiscal conservative, that's not a thing anymore."
"All of this is true. But it doesn't matter."
"They're using your economic stability, they're putting that at risk."
"It's all an act, it's all a show to manipulate their more easily manipulated base."

### AI summary (High error rate! Edit errors on video page)

Explains the debt ceiling and how it's being inaccurately framed by Republicans.
Compares the debt ceiling to a self-imposed limit, not a credit card limit imposed by a lender.
Points out that the US is nowhere near its actual credit limit and can borrow more.
Analogy: Debt ceiling is like limiting spending on an already accumulated debt, not new spending.
Criticizes Republicans for not caring about the debt despite creating a significant portion of it.
Mentions economic instability and the risk of default due to hitting the debt ceiling.
States that not raising the debt ceiling could lead to economic calamity and instability.
Questions why the Republican Party is obstructing and risking economic stability.
Emphasizes that the Republican Party's actions are about leverage and obstruction, not fiscal responsibility.
Concludes that Republican concerns about the debt are not genuine and it's all a show for political gain.

Actions:

for policy analysts, voters,
Contact elected officials to demand a resolution to the debt ceiling issue (implied).
Educate others on the true implications of hitting the debt ceiling and the risks involved (implied).
Stay informed on economic policies and their potential impacts on the national economy (implied).
</details>
<details>
<summary>
2023-01-20: Let's talk about how Bidenworld views the new House.... (<a href="https://youtube.com/watch?v=laZc-qO_xSU">watch</a> || <a href="/videos/2023/01/20/Lets_talk_about_how_Bidenworld_views_the_new_House">transcript &amp; editable summary</a>)

Beau predicts problems for the Republican Party in 2024 due to extreme House members, while Team Biden sees their appointments as a political gift, gearing up for baseless hearings that may backfire and set the tone for the upcoming campaign.

</summary>

"The Republican Party seems to have fallen into the idea that social media engagement will lead to electoral success, despite it leading to underperformance."
"The White House views the new House appointments with glee, considering it a political gift."
"The hearings are expected to set the tone for the 2024 campaign."
"It's going to be a wild couple of years in the House."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The Republican Party's focus on social media engagement has led to underperformance in elections.
Beau predicts real problems for the Republican Party in 2024 due to extreme positions taken by House members like Gosser, Green, and Boebert.
Team Biden reportedly views the new House appointments with glee, considering it a political gift.
The White House and Biden allies are gearing up to handle the Republican Party's planned hearings, which they see as baseless attacks.
Beau believes that the Republican Party's focus on hearings about Biden's son and the Afghanistan withdrawal will backfire.
The hearings are expected to set the tone for the 2024 campaign and may lead to members not getting reelected.
Beau suggests keeping an eye on the House of Representatives for upcoming events.

Actions:

for politically engaged individuals,
Watch the House of Representatives for upcoming events (suggested)
Stay informed about the political landscape and upcoming hearings (suggested)
</details>
<details>
<summary>
2023-01-20: Let's talk about Trump paying Clinton.... (<a href="https://youtube.com/watch?v=H4sck56YQxg">watch</a> || <a href="/videos/2023/01/20/Lets_talk_about_Trump_paying_Clinton">transcript &amp; editable summary</a>)

Trump's dismissed lawsuit leads to sanctions, with Hillary Clinton receiving a significant sum, showcasing judicial disapproval of Trump's legal antics.

</summary>

"Trump was characterized as a mastermind of strategic abuse of the judicial process."
"This is a deliberate attempt to harass, to tell a story without regard to facts."
"Hillary Clinton will receive the largest portion of the sanctions."

### AI summary (High error rate! Edit errors on video page)

Trump filed a lawsuit against multiple people, claiming a conspiracy to rig the 2016 election against him.
The lawsuit was dismissed and now the judge is issuing sanctions against Trump and his lawyer.
The judge described the lawsuit as frivolous, brought in bad faith, and a deliberate attempt to harass.
Trump was characterized as a mastermind of strategic abuse of the judicial process by seeking revenge on political adversaries.
The judge imposed sanctions of almost a million dollars, with the money going to the defendants accused by Trump.
Hillary Clinton will receive the largest portion of the sanctions, around $170,000.
The judge's actions suggest a growing frustration with Trump's behavior in using lawsuits for improper purposes.
More sanctions against Trump may be on the way as this judge is handling multiple cases.
Trump received legal news he probably didn't want, and Hillary Clinton will receive a financial compensation as a result of the sanctions imposed.

Actions:

for legal observers, political analysts,
Support organizations working to improve the legal system integrity (suggested)
Stay informed about legal cases involving public figures (suggested)
</details>
<details>
<summary>
2023-01-19: Let's talk about the differences between the Biden and Trump cases.... (<a href="https://youtube.com/watch?v=ioNkslE70SA">watch</a> || <a href="/videos/2023/01/19/Lets_talk_about_the_differences_between_the_Biden_and_Trump_cases">transcript &amp; editable summary</a>)

Beau breaks down the critical difference between the Biden and Trump document cases: Team Biden returned documents, Team Trump tried to retain control, potentially harming national security.

</summary>

"Team Biden used their lawyers to try to get the documents back where they're supposed to be and limit harm to national security."
"Team Trump used their resources to try to retain control and leave them out in the wild, increasing damage to national security."

### AI summary (High error rate! Edit errors on video page)

Explains differences between the Biden and Trump document cases, criticizing the lack of context in most media explainers.
Emphasizes that the crux of the matter is not the numerical comparisons of documents but the context and actions taken by each team.
Contrasts the moments when both sides became aware of the documents: National Archives informing Trump's team vs. a lawyer finding a document for Biden's team.
Details Team Biden's actions to ensure proper authorities received the document, initiate a search for more documents, and preserve national security.
Points out the lack of information on what Team Trump did after they were aware of the documents.
Notes that Team Trump attempted to keep the documents out longer, potentially increasing damage to national security.
Stresses that the attempt to retain documents after awareness is the critical factor, not the number of documents involved.
Suggests that some explainers may be prolonging the scandal to sensationalize and boost ratings.
Summarizes the difference as Team Biden working to return documents while Team Trump sought to keep control, potentially harming national security.

Actions:

for concerned citizens, political observers,
Contact your representatives to demand accountability for any mishandling of sensitive documents (implied)
Support transparency and accountability in government actions (implied)
</details>
<details>
<summary>
2023-01-19: Let's talk about Ukraine, tanks, and sliding cards.... (<a href="https://youtube.com/watch?v=Q8jItrLlIS8">watch</a> || <a href="/videos/2023/01/19/Lets_talk_about_Ukraine_tanks_and_sliding_cards">transcript &amp; editable summary</a>)

Beau provides insights on the international poker game of providing tanks to Ukraine amidst concerns about restarting invasions and logistical challenges, with debates on Western assistance and Russian strategies.

</summary>

"Providing tanks is a big deal. We're all holding hands and crossing the street together."
"No inspection-ready unit has ever made it through combat. And no combat-ready unit ever makes it through inspection."
"We'll have to wait and see how it plays out."
"There's definitely a debate that is occurring right now."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Providing an update on Ukraine and discussing developments involving tanks, especially the Abrams.
International dynamics in providing tanks to Ukraine, diplomatic reservations, and the involvement of countries like the United Kingdom, Poland, Finland, Germany, and the United States.
The significance of tanks in the conflict with Russia potentially restarting the invasion.
Concerns about the effectiveness and logistical challenges of using Abrams tanks in Ukraine.
Debates and considerations around providing military equipment to Ukraine and its implications on Western resolve and Russian strategies.
Logistics concerns for Americans regarding ongoing support for the tanks.
Chatter about an upcoming offensive in Ukraine and leadership changes in the region.
Speculation about a new order from Russian leadership to shave and its potential reasons.
Assumptions and concerns about Russia's military capabilities and preparations in the conflict.
Anticipation for further intelligence on developments in Ukraine.

Actions:

for military analysts, policymakers, activists,
Contact organizations supporting Ukraine for ways to provide assistance (suggested)
Stay informed about developments in Ukraine and advocate for diplomatic resolutions (implied)
</details>
<details>
<summary>
2023-01-19: Let's talk about Elvis helping us see the future.... (<a href="https://youtube.com/watch?v=-ecj3UD5zZY">watch</a> || <a href="/videos/2023/01/19/Lets_talk_about_Elvis_helping_us_see_the_future">transcript &amp; editable summary</a>)

Elvis faced backlash for challenging societal roles, warning against opposing modern performers based on tradition.

</summary>

"Elvis, Elvis, leave me be. Keep that pelvis far from me."
"Your concern is based on tradition. Your concern is based on peer pressure from dead people."
"At some point in the future, there will probably be one of those people on a stamp."
"Don't be cruel."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Elvis faced a significant backlash when he first emerged, with demonstrations and laws attempting to ban his performances.
The backlash against Elvis was due to his provocative dance moves and challenging of societal roles, particularly as a white Southern man.
People were upset not because Elvis may have been profiting off black music, but because he was introducing it to white kids, challenging existing roles.
Despite the backlash, society did not collapse, and instead evolved and moved forward.
The opposition to Elvis was rooted in perception, tradition, and resistance to change.
Beau questions if there are modern performers challenging societal roles, and warns against standing against them based on outdated traditions.
He suggests that those opposing current performers may be ridiculed in the future, just like those who protested Elvis.
Beau ends with a message inspired by Elvis and Jesus: "Don't be cruel."

Actions:

for cultural critics, music enthusiasts,
Support and appreciate modern performers who challenge societal norms (exemplified)
Embrace change and evolution in society (exemplified)
</details>
<details>
<summary>
2023-01-19: Let's talk about Alec Baldwin, charges, and lessons.... (<a href="https://youtube.com/watch?v=k_724XKGA9o">watch</a> || <a href="/videos/2023/01/19/Lets_talk_about_Alec_Baldwin_charges_and_lessons">transcript &amp; editable summary</a>)

Beau clarifies differences in involuntary manslaughter charges, raises concerns about negligence in unintentional shootings, and urges the gun crowd to take proactive steps in teaching gun safety.

</summary>

"There are no accidents, only negligence."
"The most dangerous gun in the world is the one that you're pretty sure is unloaded."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of the Alec Baldwin case, pointing out that both Baldwin and the armorer from the set of Rust are facing charges.
Contrasting the differences in how involuntary manslaughter charges are applied in New Mexico versus Georgia, leading to different perceptions.
Explaining that involuntary manslaughter charges in New Mexico carry a maximum sentence of 18 months, while in Georgia, it could result in a decade of imprisonment.
Mentioning that the prosecution believes Baldwin and the armorer didn't exercise enough caution during a lawful act, resulting in a death, which will be decided by a jury.
Not forming an opinion on guilt or innocence until more details emerge and leaving it up to the jury to decide.
Anticipating that the gun crowd will heavily criticize Baldwin and stress the importance of negligence over accidents in such incidents.
Emphasizing that unintentional shootings resulting in death are unfortunately common in the United States due to negligence and lack of caution.
Calling for the gun crowd to take proactive steps in teaching gun safety to prevent such incidents from occurring.
Expressing doubt in the gun crowd's willingness to take action but hoping for a change in response to such incidents.
Concluding with a thought on the dangers of assuming a gun is unloaded, urging caution and awareness.

Actions:

for gun owners, activists, advocates,
Advocate for proactive gun safety education within your community (implied)
Take steps to ensure firearms are handled with caution and proper safety measures (implied)
</details>
<details>
<summary>
2023-01-18: Let's talk about when Biden should've disclosed the documents.... (<a href="https://youtube.com/watch?v=9VzwiyeaX2k">watch</a> || <a href="/videos/2023/01/18/Lets_talk_about_when_Biden_should_ve_disclosed_the_documents">transcript &amp; editable summary</a>)

Addressing whether Biden should have disclosed a document earlier, Beau argues for responsible disclosure and challenges the Republican narrative aiming to undermine the country's principles.

</summary>

"There are miles of difference between the Trump case and the Biden case, but what about this part?"
"They care about the ability to use that to cast doubt on the founding principles of this country."
"You don't get to know everything."
"It was released too soon."
"No, they really should not have disclosed it as soon as they found out about it."

### AI summary (High error rate! Edit errors on video page)

Addressing the talking point about whether Biden should have disclosed a document's story earlier before the election.
Exploring the future implications and path of this talking point.
Challenging the notion that Biden's failure to disclose equates to a stolen election.
Emphasizing the importance of responsible disclosure and the earliest possible moment for it.
Illustrating a scenario where Biden's team discovers a classified document and the steps they should take.
Pointing out the flaws in the argument that Biden should have immediately disclosed the information.
Mentioning ongoing searches for potential additional documents to prevent leaks.
Criticizing the Biden administration's statement on not interfering with the investigation.
Arguing that immediate disclosure could have compromised national security.
Asserting that the Republican talking point is not about the timing of disclosure but about undermining the country's principles.

Actions:

for political analysts, concerned citizens,
Support responsible and strategic disclosure of sensitive information to protect national security (implied).
</details>
<details>
<summary>
2023-01-18: Let's talk about what you can do about House Republicans.... (<a href="https://youtube.com/watch?v=9BaFaDgbTH4">watch</a> || <a href="/videos/2023/01/18/Lets_talk_about_what_you_can_do_about_House_Republicans">transcript &amp; editable summary</a>)

House Republicans continue to equate social media engagement with electoral success despite repeated failures, leading to alienation of centrists and independents, while individuals focus on countering harmful rhetoric and planning for the future.

</summary>

"Have some milk and M&Ms."
"Stay in the fight."
"Be ready to lend a hand."
"Defeat the rhetoric."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

House Republicans equate social media engagement with electoral success despite repeated failures.
They plan to continue this strategy, even promoting extreme and absurd voices.
Reacting with anger and posting may play into their narrative of "owning the libs."
The key is not to interrupt them but focus on countering their rhetoric and planning for the future.
House Republicans risk alienating centrists and independents with their extreme rhetoric.
Mitigating harm caused by their rhetoric and planning legislative agendas for the future are critical.
The extreme strategy of House Republicans may lead to a significant loss in the next election.
Supporting marginalized communities targeted by harmful rhetoric is vital.
While you may not be able to stop them, you can work to counter their harmful narratives.
The Republican Party's current strategy seems to be setting them up for failure in the upcoming elections.
Mitigating the harm caused by their rhetoric is a key action individuals can take.

Actions:

for progressive activists,
Counter the harmful rhetoric spread by House Republicans by engaging in meaningful discourse and sharing factual information (implied).
Plan and prepare for the legislative agenda in 2025 to offer alternatives to harmful narratives (implied).
Support marginalized communities targeted by harmful rhetoric through advocacy and allyship (implied).
</details>
<details>
<summary>
2023-01-18: Let's talk about my shirts, the art, the artist, and cancel culture.... (<a href="https://youtube.com/watch?v=VTNZaaFdxM8">watch</a> || <a href="/videos/2023/01/18/Lets_talk_about_my_shirts_the_art_the_artist_and_cancel_culture">transcript &amp; editable summary</a>)

Beau explains the nuances of separating art from the artist and why it matters in the context of cancel culture and brand image protection.

</summary>

"Just because you separate the art from the artist, that doesn't mean that the art is still going to be welcomed everywhere."
"Cancel culture is literally capitalism in action."
"It's not about being a conservative, it's about the ideas that are so aggressive that they're causing active harm to the communities that these companies are catering to."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of separating the art from the artist and applies it to his choice of shirts.
Mentions wearing shirts from Rick and Morty, but decides to stop wearing them due to allegations against a key player in the show.
Talks about wearing Hunter S. Thompson shirts and how it wouldn't be a good fit for his channel if it routinely raised money for AA or NA.
Provides an example of a Harry Potter shirt he couldn't wear on his channel due to the artist using her platform to attack people supportive of causes he raises money for.
Describes how cancel culture isn't about targeting conservatives but about companies protecting their brand image and assets.
Explains that cancel culture is essentially capitalism in action, removing things that might upset certain demographics that use the product.
Concludes by stating that just separating the art from the artist doesn't guarantee acceptance everywhere, especially if the ideas associated with them cause harm to communities.

Actions:

for content creators, conservatives,
Support domestic violence shelters (implied)
Be mindful of the impact of the content you support or associate with (implied)
</details>
<details>
<summary>
2023-01-18: Let's talk about ledges, New Mexico, and a GOP candidate.... (<a href="https://youtube.com/watch?v=if03fIeUwVc">watch</a> || <a href="/videos/2023/01/18/Lets_talk_about_ledges_New_Mexico_and_a_GOP_candidate">transcript &amp; editable summary</a>)

Analyzing political violence in New Mexico, linking a Republican candidate to drive-by shootings, Beau warns of increasing frequency and the dangers of divisive rhetoric.

</summary>

"In one of them, rounds wound up in the bedroom of a child."
"It is becoming more common. It is happening with greater frequency."
"Ignoring his statements that later end up as Republican policy."
"The echo chambers that brought about what occurred on the 6th, they still exist."
"We're headed to our own troubles."

### AI summary (High error rate! Edit errors on video page)

Analyzing recent events in New Mexico involving drive-by shootings at homes of Democratic Party members.
Law enforcement linking ballistics evidence to a Republican candidate, Solomon Pina, in the attacks.
Pina believed baseless election claims and allegedly paid people to carry out the attacks.
Reports suggest Pina may be advised to seek a plea deal due to incriminating evidence.
The media focusing on Pina's landslide loss in the election and his motivations.
One in four voters supported Pina, raising concerns about electorate judgment.
Beau criticizes the Republican Party's shift and warns of increasing political violence.
Mention of tweets indicating support for Pina and Trump, hinting at ongoing divisive rhetoric.
Beau stresses the importance of countering divisive rhetoric and not underestimating its impact.
The need for continued vigilance to prevent further violence and address underlying issues.

Actions:

for community members, voters,
Contact local representatives or party officials to express concerns about political violence (suggested)
Attend community forums or events focused on promoting unity and understanding (implied)
</details>
<details>
<summary>
2023-01-17: Let's talk about school lunches.... (<a href="https://youtube.com/watch?v=mreoKmGCFzU">watch</a> || <a href="/videos/2023/01/17/Lets_talk_about_school_lunches">transcript &amp; editable summary</a>)

Participation in school lunch programs dropped by 23%, accumulating $19.2 million in debt, exposing ongoing issues of child hunger amid political distractions.

</summary>

"There are kids at the school in your area that are going hungry."
"Everything I know I learned when I wasn't hungry."
"There are kids going hungry. And it's not even a talking point."

### AI summary (High error rate! Edit errors on video page)

Participation in school lunch programs has declined by 23% since June, a significant decrease.
Students have accumulated $19.2 million in school lunch debt during the same period, indicating financial struggles.
The areas most affected by this decline are the Midwest and the Mountain Plains region.
The federal government allowed a program that supported school lunches to expire, leading to the current situation.
Despite the pandemic, food insecurity and kids going hungry are still prevalent issues.
Politicians focus on trivial matters like stoves instead of addressing real problems like child hunger.
Beau criticizes the lack of priority given to tackling food insecurity and hunger among students.
The $19.2 million school lunch debt is a massive burden for working-class families but a small amount for the federal government.
Beau stresses the importance of addressing real issues like child hunger rather than getting distracted by insignificant matters.

Actions:

for community members, parents, advocates,
Assist families in need with school lunch payments (exemplified)
Support local programs combating child hunger (exemplified)
</details>
<details>
<summary>
2023-01-17: Let's talk about both parties being the same and the overton window.... (<a href="https://youtube.com/watch?v=yjKcVvmIo5w">watch</a> || <a href="/videos/2023/01/17/Lets_talk_about_both_parties_being_the_same_and_the_overton_window">transcript &amp; editable summary</a>)

Beau explains the nuances between political parties in the US, addressing criticisms of similarity and underlying ideological differences.

</summary>

"Both parties do, in fact, use the power of the state, the violence of the state, to maintain order."
"The United States is authoritarian right."
"People who are anti-authoritarian left, they didn't get to that position through bumper sticker mentality."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the political spectrum with left, right, authoritarian, and anti-authoritarian.
Addresses the statement that both political parties in the US are the same and where it originates.
Points out that both parties use violence to maintain order but are not necessarily identical.
Contrasts the Democratic and Republican parties regarding consent-based policing, demilitarizing the police, and responses to maintaining order.
Analyzes the perspectives of anti-authoritarian right, authoritarian right, authoritarian left, and anti-authoritarian left on corporate and capitalist influence in politics.
Emphasizes that while the criticisms are valid, there are distinctions between the parties in terms of policy and ideology.
Acknowledges the challenges of debating with the extreme authoritarian right and anti-authoritarian left.
Argues that anti-authoritarian left individuals have deeply rooted beliefs, are well-informed, and challenging to debate due to their strong convictions.
Compares historical contexts to illustrate the evolution of political ideologies in the US.
Encourages engaging in political discourse and mentions the Tea Party's impact on shaping Republican Party policies.

Actions:

for politically engaged individuals.,
Debunk anti-Semitic conspiracy theories through education and awareness (implied).
Engage in civil discourse with individuals holding different political beliefs to foster understanding and constructive debate (implied).
</details>
<details>
<summary>
2023-01-17: Let's talk about "just asking questions".... (<a href="https://youtube.com/watch?v=TI9-YY5O51k">watch</a> || <a href="/videos/2023/01/17/Lets_talk_about_just_asking_questions">transcript &amp; editable summary</a>)

Beau challenges the misconception about Fox News, revealing their manipulative tactics of asking leading questions and painting false narratives.

</summary>

"They ask questions in a leading manner to get somebody to the right conclusion."
"Watching Fox doesn't make you special. It makes you average, below average, really."
"They are not informing some band of plucky upstart patriots. They are the mainstream media."
"When I see people watching it, they're nodding along with the questions because they know the answer they've been conditioned to believe."
"Information for real patriots would be accurate."

### AI summary (High error rate! Edit errors on video page)

Expresses dislike for Fox News due to their manipulation of truth by asking leading questions to push viewers towards false conclusions.
Points out Fox News' tactic of asking questions in a leading manner to make viewers believe something without directly stating it.
Challenges the misconception that Fox News is not part of the mainstream media and instead argues that they are the mainstream media.
Encourages viewers to be critical when watching Fox News, especially when they avoid making definitive statements and instead ask questions.
Criticizes Fox News for painting false narratives that are objectively untrue and conditioning viewers towards conspiratorial thinking.
Calls out Fox News for not providing accurate information but rather asking questions and hoping viewers get the wrong answers.

Actions:

for viewers,
Fact-check news sources to verify information (implied)
Encourage critical thinking and media literacy among peers (implied)
</details>
<details>
<summary>
2023-01-16: Let's talk about the dreams and realities of change.... (<a href="https://youtube.com/watch?v=wIJIrR8nk2A">watch</a> || <a href="/videos/2023/01/16/Lets_talk_about_the_dreams_and_realities_of_change">transcript &amp; editable summary</a>)

Beau encourages understanding the realities behind dreams, especially in foreign policy, to transition from mere dreaming to impactful action.

</summary>

"My dream of the world's EMT. I don't want it to stay a dream."
"The dream has to be tempered with the reality."
"You have to be able to describe it in waking terms."

### AI summary (High error rate! Edit errors on video page)

Encourages those seeking systemic change to watch the video.
Acknowledges the importance of understanding the realities behind dreams.
Expresses his dream of a world's EMT to improve foreign policy and help those in need.
Outlines the complex process of actualizing the world's EMT concept.
Emphasizes the need to transition from dreaming to understanding realities to taking action.
Explains that his foreign policy videos aim to provide clarity on how things work, moving past moral judgments.
Shares a personal example of a flawed implementation of a dream without considering the practicalities.
Stresses the importance of tempering dreams with reality and educating others on effective solutions.
Urges individuals with causes to understand and communicate the practical applications of their dreams.
Advocates for a shift from mere dreams to actionable plans grounded in reality.

Actions:

for advocates for systemic change,
Educate others on the practical applications of dreams and causes (implied)
Advocate for solutions grounded in reality (implied)
Encourage understanding of the realities behind dreams (implied)
</details>
<details>
<summary>
2023-01-16: Let's talk about how to understand the House Republicans.... (<a href="https://youtube.com/watch?v=BKiTZjJ8mLA">watch</a> || <a href="/videos/2023/01/16/Lets_talk_about_how_to_understand_the_House_Republicans">transcript &amp; editable summary</a>)

Beau analyzes the House majority's actions, urging viewers to see Republicans as social media commentators prioritizing engagement over governance.

</summary>

"View them as social media commentators."
"They're trying to get those likes, those shares on social media."
"Don't view this House as an entity interested in governing."
"The Republican Party is definitely going to do themselves a giant disservice."
"It's about getting engagement on social media because they think that that's going to propel them to victory."

### AI summary (High error rate! Edit errors on video page)

Analyzing the actions of the new House majority, focusing on the Republicans' post-takeover behavior.
Republicans are doubling down on unpopular decisions, raising questions about their rationale.
Beau draws a parallel from his ability to predict Trump's moves by viewing him as a fascist rather than a traditional Republican.
He suggests looking at the current House majority as edgy social media commentators rather than typical lawmakers.
The Republicans seem to prioritize social media feedback over governance, aiming to generate controversy for fundraising.
The party's echo chamber mentality leads them to cater to their vocal social media base, despite it not representing their wider support.
Beau notes that not all Republicans in the House follow this trend, with some genuine politicians questioning extreme family planning measures.
The House's focus seems more on generating social media engagement than effective governance or popularity.
Beau predicts that the House's controversial actions may benefit the Democratic Party in the long run by alienating voters.
Despite potential drawbacks, the Republican Party appears committed to their current strategy due to their deep-rooted echo chamber beliefs.

Actions:

for political observers,
Contact local representatives to express concerns about prioritizing social media engagement over effective governance (suggested)
Engage with community members to raise awareness about the implications of echo chamber politics and its impact on decision-making (exemplified)
</details>
<details>
<summary>
2023-01-16: Let's talk about China, the US, the economy, and American exceptionalism.... (<a href="https://youtube.com/watch?v=1cWti0QOme8">watch</a> || <a href="/videos/2023/01/16/Lets_talk_about_China_the_US_the_economy_and_American_exceptionalism">transcript &amp; editable summary</a>)

Beau challenges American exceptionalism, pointing out the inevitability of China's economic rise and advocating for cooperation over isolationism.

</summary>

"The idea that the United States will remain the economic superpower of the world while other countries with much larger populations modernize and become economic powers of their own, it's rooted in American mythology."
"The answer here is not to become more isolationist. The answer here is more cooperation, more economic trade."
"You mean to tell me that a country more than four times the size, as far as population goes, is going to have more goods and services exchanged? I'm shocked."

### AI summary (High error rate! Edit errors on video page)

Explains the concern over China's growing economic power and potential to outstrip the United States.
Notes that China's GDP is increasing at a faster rate compared to the United States.
Points out that China having more goods and services exchanged due to its larger population should not be surprising.
Criticizes American exceptionalism for leading people to believe the US will always lead the world economically.
Suggests increasing the US population through immigration or reducing income inequality to boost economic power.
Emphasizes the need to dispel American mythology and embrace critical history.
Advocates for more cooperation and economic trade rather than isolationism.

Actions:

for economic analysts, policymakers.,
Increase cooperation and economic trade (implied).
Advocate for reducing income inequality to boost population growth (implied).
</details>
<details>
<summary>
2023-01-15: Let's talk about the World Bank's economic projections for 2023.... (<a href="https://youtube.com/watch?v=6U4W7fxkoQM">watch</a> || <a href="/videos/2023/01/15/Lets_talk_about_the_World_Bank_s_economic_projections_for_2023">transcript &amp; editable summary</a>)

Beau provides an overview of the World Bank's projections for the global economy in 2023, showing minimal growth but potential risks ahead.

</summary>

"The global economy will expand by 1.7%, that's really low."
"The US will grow by roughly half a percent, that's low."
"If Russia wins, your pocketbook, your checking account, it takes a hit."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Overview of the projections for the global economy in 2023 by the World Bank.
Global economy expected to expand by 1.7%, barely dodging recession.
Lowest projections in 20-30 years, except for the 2008 financial crisis and the worst of the 2020 global pandemic.
The US economy also expected to barely dodge recession with a growth projection of around 0.5%.
Potential threats to the economy include a resurgence of global public health issues impacting supply chains and Russia's invasion of Ukraine.
If supply chains are affected, it could erase the projected growth in the US.
Russia's victory in Ukraine could lead to economic consequences, such as increased costs, especially for food.
Despite potential risks, current projections indicate that the US and global economy will avoid recession for now.
Uncertainty remains due to various unpredictable factors.
The projections are based on the assumption that things continue as they are now.

Actions:

for economic analysts, policymakers,
Monitor global public health issues and their impact on supply chains (implied)
Stay informed about the situation between Russia and Ukraine (implied)
</details>
<details>
<summary>
2023-01-15: Let's talk about the Bradley and Ukraine.... (<a href="https://youtube.com/watch?v=-4tDezCRUhI">watch</a> || <a href="/videos/2023/01/15/Lets_talk_about_the_Bradley_and_Ukraine">transcript &amp; editable summary</a>)

The US providing Bradleys, not tanks, to Ukraine gives them an edge, but challenges remain in integrating advanced equipment and logistics.

</summary>

"These aren't tanks and this will come up again later."
"The sensors on this thing are well beyond what Russia has."
"I don't buy that. I think they'll be able to integrate them pretty quickly."
"The Bradley can take ground."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The United States is providing Bradleys, infantry fighting vehicles, not tanks, to Ukraine.
Older individuals may have negative opinions about the Bradley due to its design process.
Despite having a cannon and being armored and tracked, Bradleys are not tanks.
Americans may expect Ukraine to use the Bradleys similar to how the US does, but Beau doesn't foresee that happening.
The Bradleys give Ukraine an advantage at night with superior sensors compared to Russia.
There are plenty of Bradleys available, so providing them to Ukraine won't harm US readiness.
The next step after Bradleys could be tanks, but many are hesitant due to concerns about advanced equipment integration.
Ukraine has proven to be adaptable in integrating new military equipment.
Logistics, especially fuel requirements for tanks like Abrams, could pose challenges for Ukraine.
While the Bradley can help Ukraine reclaim territory, keeping it running may be a challenge.
Artillery can cause devastation, but the Bradley can help Ukraine take and hold ground in a semi-unconventional manner.

Actions:

for military policy analysts,
Support organizations providing aid to Ukraine (exemplified)
Volunteer with organizations assisting Ukraine (implied)
Advocate for peaceful solutions in the conflict (suggested)
</details>
<details>
<summary>
2023-01-15: Let's talk about explaining Earth to aliens.... (<a href="https://youtube.com/watch?v=VURKY-447dk">watch</a> || <a href="/videos/2023/01/15/Lets_talk_about_explaining_Earth_to_aliens">transcript &amp; editable summary</a>)

Beau explains Earth's society organized around paper, driven by war for advancement, and pleads for cleaner energy distribution while declining the responsibility of explaining civilization to advanced beings.

</summary>

"We have kind of decided that we as a society want some people to live very comfortably."
"What drives our technological advancement? War, war mostly, war?"
"So if you all could distribute that. Where are you all going? I'm sorry, you nuke from orbit?"
"I do not want to have to explain the nature of human civilization to a species that is capable of actually getting somewhere."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces himself as Beau and explains that he was chosen to answer questions about Earth for interstellar visitors.
Describes society as being organized around the accumulation of paper which serves as a token for exchanging goods and services.
Mentions that some people live very comfortably while others have dirt floors, with wealth distribution determined by luck and birth circumstances.
Talks about technological advancement being primarily driven by war against our own species for resources, books, and historically, skin color.
Touches on how diseases are more prevalent in areas with less paper and how wealthier areas often ignore new diseases due to better infrastructure.
Expresses a need for cleaner energy methods to prevent the planet from being destroyed by current energy production practices.
Declines the responsibility of explaining human civilization to a more advanced species following jokes about him being nominated for first contact.

Actions:

for interstellar visitors,
Distribute cleaner energy methods for Earth's sustainability (suggested)
</details>
<details>
<summary>
2023-01-14: Let's talk about two types of nationalism.... (<a href="https://youtube.com/watch?v=wI-BjA9StsA">watch</a> || <a href="/videos/2023/01/14/Lets_talk_about_two_types_of_nationalism">transcript &amp; editable summary</a>)

Beau explains the dangers of nationalism as an ideology leading to perpetual war and the prevalence of nationalism in the US.

</summary>

"Nationalism is good for war, which is useful if you're trying to achieve a nation."
"Nationalism is politics for basic people. People who really require a leader."
"Nationalism in service of throwing off a colonial yoke. Yeah, I get it. It's a tool."

### AI summary (High error rate! Edit errors on video page)

Talks about nationalism and the different kinds of nationalism.
Mentions how people have pointed to Ireland as an example of nationalism used for good.
Explains the distinction between nationalism as a tool and nationalism as an ideology.
Describes how nationalism was used by Connolly and Pearse in Ireland in 1916.
Points out that nationalism becomes an ideology when the nation already exists.
Addresses the negative impacts of nationalism on the economy, environment, diplomacy, and domestic situations.
Emphasizes that nationalism is good for motivating people for war.
Explains how nationalism can morph into fascism when internal enemies are sought.
Criticizes nationalism as politics for people who require a leader to tell them what to do.
Warns about the dangers of nationalism as an ideology leading to perpetual war.
Talks about the prevalence of nationalism in the US and its potential dangers.
Connects the push for nationalism in the US to political movements like Trump's.

Actions:

for activists, policymakers, educators.,
Analyze and challenge nationalist rhetoric in your community (suggested).
Educate others on the dangers of nationalism turning into fascism (suggested).
</details>
<details>
<summary>
2023-01-14: Let's talk about the FTC's new Non-compete rule.... (<a href="https://youtube.com/watch?v=VJ9YqgFTMCc">watch</a> || <a href="/videos/2023/01/14/Lets_talk_about_the_FTC_s_new_Non-compete_rule">transcript &amp; editable summary</a>)

FTC's rule eliminating non-compete clauses faces legal challenges and questions of authority, with predictions of narrowing but still a win for labor.

</summary>

"Non-compete clauses have never made sense to me."
"I don't think an employer should be able to limit where you work in the future."
"Expect to hear a lot of news about challenges to this."

### AI summary (High error rate! Edit errors on video page)

FTC rule eliminates non-compete clauses, a win for labor.
Expect legal challenges to narrow the rule's scope.
Less than 1% chance the rule will stand as is.
Possible argument: FTC overstepped its authority by creating a new law.
Administration likely has a response to the separation of powers issue.
Predicts large portions of the rule will stand, still a win for labor.
Deep-pocketed interests will challenge the rule in court.
Anticipates a lot of news coverage on challenges to the rule.

Actions:

for labor advocates, policymakers.,
Stay informed on developments regarding the FTC rule and legal challenges (implied).
Support organizations advocating for labor rights and fair employment practices (implied).
</details>
<details>
<summary>
2023-01-14: Let's talk about an opportunity for detrumpification.... (<a href="https://youtube.com/watch?v=YxVXzV4c0a8">watch</a> || <a href="/videos/2023/01/14/Lets_talk_about_an_opportunity_for_detrumpification">transcript &amp; editable summary</a>)

The Republican Party has a chance for de-Trumpification by replacing McDaniel and potentially distancing from Trump's influence, amidst a shift in party dynamics post-midterms.

</summary>

"This is a golden opportunity for them."
"This is a moment where they can start to take their party back."

### AI summary (High error rate! Edit errors on video page)

Republican Party faces a golden chance for de-Trumpification by selecting a new head at the end of the month.
Current head, McDaniel, perceived as closely allied with Trump due to being handpicked by him.
Only 6% of normal voters want McDaniel to retain her position.
Removing McDaniel can sever one of Trump's levers of power.
The average Republican might not be fully informed about McDaniel's connection to Trump.
The entry of the MyPillow guy as a contender may complicate the process.
Overwhelming majority of Republicans are ready for new leadership, potentially aiding those seeking to distance from Trump.
The desire for change within the party may stem from disappointment in the midterm performance.
This is an opportune moment for traditional conservatives to reclaim the party if they so wish.

Actions:

for republicans,
Vote to remove McDaniel as head of the Republican Party (exemplified)
</details>
<details>
<summary>
2023-01-13: Let's talk about default and the Constitution.... (<a href="https://youtube.com/watch?v=id5hxCtWmwo">watch</a> || <a href="/videos/2023/01/13/Lets_talk_about_default_and_the_Constitution">transcript &amp; editable summary</a>)

Beau outlines the severe consequences of a U.S. default, challenges its constitutionality, and urges action against ill-informed or ill-intended individuals to prevent harm to economic stability and standards of living.

</summary>

"They're holding your financial stability hostage."
"This isn't some thing where some plucky upstarts are sticking it to the man up in D.C. Their leverage is you."
"I do not believe that the majority of the Republican Party is going to be willing to allow the United States to default."
"Either they don't understand or they are literally bad actors."
"Those are the options."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the United States defaulting on its loan payments and addresses questions and concerns about it.
Outlines the potential consequences of a U.S. default, including increased interest rates, decreased dollar value, inflation, and a decline in standard of living.
Emphasizes that holding the budget hostage means holding financial stability, way of life, and standard of living hostage.
Analyzes the constitutionality of a U.S. default, mentioning the 14th Amendment, Article 1, Section 8 of the U.S. Constitution, and Federalist Papers number 30.
Suggests that defaulting intentionally is unconstitutional and advocates for the Supreme Court to rule on it.
Contrasts the Obama administration's stance on defaulting with the belief that it wasn't constitutional but not a good strategy.
Urges to call the bluff of those considering default, especially because it impacts the wealthy individuals in Congress.
Asserts that the lack of case law on default is due to the historical adherence to economic stability and the absence of individuals willing to risk such a detrimental outcome.
Concludes by stating that those advocating for default are either ill-informed or ill-intended and calls for action to prevent a U.S. default.

Actions:

for us citizens,
Contact your representatives and urge them to prevent a U.S. default by supporting financial stability and economic well-being (implied).
Stay informed about the potential impacts of a U.S. default and advocate for responsible decision-making in Congress (implied).
</details>
<details>
<summary>
2023-01-13: Let's talk about Trump campaigning on military action.... (<a href="https://youtube.com/watch?v=ZobppeCGV5I">watch</a> || <a href="/videos/2023/01/13/Lets_talk_about_Trump_campaigning_on_military_action">transcript &amp; editable summary</a>)

Beau criticizes Trump's promise to use the US military in Mexico, foreseeing unnecessary destruction, chaos, and more refugees due to flawed foreign interventions.

</summary>

"His promise is not to win. His promise is to create a bunch of refugees."
"It might be time for the media to stop accepting campaign promises just as talking points."
"Talk about what is actually going to occur if this campaign promise is fulfilled."
"A whole bunch of unnecessary destruction and chaos, a whole bunch more refugees."
"He won't win, because this war, the plants won."

### AI summary (High error rate! Edit errors on video page)

Criticizes Trump's campaign promise to use the US military to go after organizations in Mexico without questioning the cause and effect or looking at the consequences.
Trump plans to utilize the US military against organizations in Mexico, similar to how he took down IS, despite ongoing hostilities with IS and recent incidents in the US.
Trump's promise involves ordering the Department of Defense to utilize special forces, cyber warfare, and covert actions to target cartel leadership, infrastructure, and operations.
Beau points out the potential mission creep and spread of military operations if cartels are targeted, leading to a situation spiraling out of control and triggering a civil war.
The promise is not to defeat the cartels but rather to create refugees and unnecessary destruction.
Criticizes the media for not questioning campaign promises and focusing on the potential negative outcomes of such actions.
Mentions past US military interventions leading to refugees and destabilization in other countries, suggesting a similar outcome if Trump's promise is fulfilled.
Emphasizes the cycle of mistakes in US foreign interventions and the inability to fix other countries while causing harm.
Concludes that Trump's promise will result in unnecessary destruction, chaos, and more refugees without achieving victory.

Actions:

for activists, policy makers,
Advocate against harmful foreign interventions (implied)
Support policies that prioritize diplomacy over military actions (implied)
</details>
<details>
<summary>
2023-01-13: Let's talk about Republicans still following Trump.... (<a href="https://youtube.com/watch?v=M7L5LvqV4gI">watch</a> || <a href="/videos/2023/01/13/Lets_talk_about_Republicans_still_following_Trump">transcript &amp; editable summary</a>)

Explaining how Trump's influence still shapes Republican Party policy-making and candidate engagements despite his weakened position.

</summary>

"Trump controls that energized base. That vocal minority, they are Trump's people."
"Trump is a symbol for the Republican Party. He is still a thought leader."
"As much as we all want to, Trump's not out of the game yet."

### AI summary (High error rate! Edit errors on video page)

Explains how policymaking occurs within the Republican Party, focusing on the influence of Trump's campaign promises despite his weakened position.
Describes the multiple factions within the Republican Party, particularly those in red states and deep red areas.
Points out the trap the Republican Party has fallen into by believing vocal supporters on social media represent all voters.
Notes that Trump controls an energized base that influences policy decisions within the party.
Outlines the game of red state Republicans to be the most extreme in order to gain social media engagement and win elections.
Emphasizes that Trump remains a symbol and thought leader for the Republican Party, influencing policy decisions and candidate engagements.
Mentions that Trump's influence will continue until criminal prosecutions, voluntary exit from political life, or a shift within the Republican Party.
Stresses the importance of addressing Trump's campaign promises to prevent them from becoming Republican Party policy.

Actions:

for political analysts, republican voters,
Engage in political discourse and analysis to understand the impact of Trump's continued influence (implied).
Support candidates who prioritize policy decisions based on broader voter interests rather than extreme positions for social media engagement (implied).
</details>
<details>
<summary>
2023-01-12: Let's talk about the Biden docs and charging Trump.... (<a href="https://youtube.com/watch?v=n91vUi5H80w">watch</a> || <a href="/videos/2023/01/12/Lets_talk_about_the_Biden_docs_and_charging_Trump">transcript &amp; editable summary</a>)

Beau clarifies the differences between the coverage of Biden and Trump documents, urging for a fair investigation and dismissal of attempts to protect Trump from charges.

</summary>

"The correct course of action, if there was enough evidence to charge, it wouldn't be to let Trump go. It would be to charge them both."
"That's just something they've made up to muddy the waters."
"If there was enough evidence to charge, it wouldn't be to let Trump go."
"They're trying to convince you to let Trump out of this."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Comparing coverage of Biden documents to Trump documents
Emphasizing that based on available evidence, they are not the same
Calls for an investigation to determine what happened and prevent a recurrence
Mentions the handover of the case to a Trump-nominated US attorney
Contrasts willful retention in Trump case with immediate action in Biden case
Asserts that charging Trump should not be harder due to Biden case
Criticizes the idea that Biden case makes charging Trump more difficult
States that the proper response, if evidence is enough, should be to charge both
Argues against those trying to protect Trump from charges
Concludes by stressing the importance of charging based on evidence

Actions:

for citizens, activists, justice advocates,
Contact your representatives to demand transparency and accountability in investigations (suggested)
Support organizations advocating for fair legal processes and holding officials accountable (suggested)
</details>
<details>
<summary>
2023-01-12: Let's talk about Special Counsel updates.... (<a href="https://youtube.com/watch?v=34NYzjhYZxk">watch</a> || <a href="/videos/2023/01/12/Lets_talk_about_Special_Counsel_updates">transcript &amp; editable summary</a>)

Updates on the special counsel's office reveal an expanding investigation into potential financial crimes and intense actions aiming to secure a conviction, contradicting the idea of winding down the probe or protecting Trump from accountability.

</summary>

"This reporting matches the idea of a special counsel's office looking to secure a conviction."
"The actions are not in line with letting Trump go."
"It's worth noting that if Trump World knew their claims were false and were raising money off them, that might not be legal."
"It's more like Trump's thumbprint on a shell casing."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Updates on the special counsel's office are being provided due to new reporting.
A flood of subpoenas went out in December, targeting Trump-affiliated PACs and communications related to voting machine companies.
The special counsel's office seems to be branching into new lines of investigation, potentially related to financial crimes.
People are being called before the grand jury at an accelerating pace, with some called back for intense second appearances.
The actions of the special counsel's office suggest an expanding investigation, not one winding down.
There's speculation that the investigation is looking into possible illegal activities by Trump World, such as false claims for fundraising.
Testimony involving Trump and Eastman requesting help for alternate electors is seen as significant but not a smoking gun.
The new information indicates that the special counsel's office is actively seeking to prosecute rather than wind down the investigation.
The reporting doesn't support the idea of protecting Trump from accountability but rather focuses on securing a conviction.
The overall picture painted is of an intensifying investigation into various aspects related to Trump and potential financial crimes.

Actions:

for legal analysts, political commentators,
Contact legal experts to understand the implications of the special counsel's actions (suggested)
Stay informed about the developments in the investigation and share accurate information with others (exemplified)
</details>
<details>
<summary>
2023-01-12: Let's talk about Russian leadership changes.... (<a href="https://youtube.com/watch?v=NDjRM3MhrP4">watch</a> || <a href="/videos/2023/01/12/Lets_talk_about_Russian_leadership_changes">transcript &amp; editable summary</a>)

Leadership changes in the Russian military reveal political maneuvering and a potential crack in resolve, impacting morale and indicating challenges ahead.

</summary>

"Erratic leadership changes within 90 days can damage morale and may signify a crack in Russian resolve."
"Putin's impatience for results and erratic decision-making may indicate a lack of resolve for a prolonged occupation."
"Ukrainian leadership appears upbeat, setting timelines for resolving the conflict based on the support they receive."

### AI summary (High error rate! Edit errors on video page)

Leadership changes in the Russian military at high levels indicate a division and political maneuvering rather than boosting the war effort.
Reputation of the new commander, Gerasimov, is good, but previous commanders lacked necessary resources.
Erratic leadership changes within 90 days can damage morale and may signify a crack in Russian resolve.
Russian military bloggers are aware of these moves and are spreading information, impacting troop morale.
Putin's impatience for results and erratic decision-making may indicate a lack of resolve for a prolonged occupation.
Ukrainian leadership appears upbeat, setting timelines for resolving the conflict based on the support they receive.
Erratic leadership changes suggest a weakening resolve within the Russian military, acknowledging the challenges they face.
The reality of the situation is starting to wear on the patience of Russian military leadership, potentially benefiting Ukraine.

Actions:

for military analysts, policymakers,
Monitor and analyze the leadership changes in the Russian military to understand potential shifts in strategy and resolve (implied).
Stay informed about the situation in Ukraine and the impact of these changes on troop morale and military operations (implied).
</details>
<details>
<summary>
2023-01-11: Let's talk about charging decisions.... (<a href="https://youtube.com/watch?v=5kafl0rZ8WQ">watch</a> || <a href="/videos/2023/01/11/Lets_talk_about_charging_decisions">transcript &amp; editable summary</a>)

Beau addresses the uncertainty surrounding charging decisions involving Smith, stressing the importance of not relying too heavily on speculation until a decision is made.

</summary>

"Possible is a key part."
"Weeks can mean a lot of different things."
"He brought on two other prosecutors."
"All the talk until that decision is made, well, it's just a thought."
"I don't have much to go on with it."

### AI summary (High error rate! Edit errors on video page)

Addressing the anticipation around charging decisions involving Smith in the coming weeks.
Emphasizing the uncertainty regarding the timeline for these decisions.
Mentioning the federal government's typical approach to prosecuting conspiracy cases.
Pointing out the lack of substantial information in current reporting on this matter.
Noting Smith's proactive nature and experience in handling such cases.
Speculating on the potential speed of progress based on Smith's actions.
Drawing attention to the significance of Smith bringing on two experienced prosecutors.
Stating that the speculation until the decision is made is just that – speculation.

Actions:

for observers, analysts, reporters,
Monitor updates on the situation involving Smith (implied)
</details>
<details>
<summary>
2023-01-11: Let's talk about Utah and the Great Salt Lake.... (<a href="https://youtube.com/watch?v=B1ru0e4tBNU">watch</a> || <a href="/videos/2023/01/11/Lets_talk_about_Utah_and_the_Great_Salt_Lake">transcript &amp; editable summary</a>)

Utah faces a critical five-year deadline to save the Great Salt Lake by making drastic water usage cuts amidst a unique snowpack bonus, requiring immediate action from residents and officials.

</summary>

"The time to fix this is now, not five years from now."
"If they miss it, it's probably done."
"This is their chance."
"It isn't part of the culture war."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Utah's Great Salt Lake is facing a critical situation with a projected disappearance in five years due to a 73% water loss and 60% of its lake bed exposed.
To start replenishing the lake, drastic cuts of about 50% in water usage in the Great Salt Lake watershed are necessary, a scenario deemed unlikely.
Despite the economic and health risks associated with a dry salt lake, people are hesitant to make the required water consumption cuts.
Utah has an above-average snowpack this year at 170%, providing a unique chance to address the crisis by diverting the excess water to the lake.
Acting now can't fix the issue entirely but will buy time for mitigation efforts, yet failure to seize this chance might lead to irreversible consequences.
Urges Utah residents to contact their representatives and state officials promptly to implement a plan before the snow melts.
Proposals like pipelines from other sources are unrealistic; the focus should be on reducing water consumption across multiple areas, a tough but necessary decision.
Emphasizes the urgency of the situation, as once the lake dries up, there's no turning back.
Calls for action from Utah residents to raise awareness and push for immediate measures to save the Great Salt Lake before it's too late.

Actions:

for residents of utah,
Contact your representatives and state officials to push for immediate action to address the Great Salt Lake crisis (suggested).
Raise awareness in your community about the urgent need to save the lake and the importance of making water consumption cuts (implied).
</details>
<details>
<summary>
2023-01-11: Let's talk about Santos, McCarthy, calls to resign, and lessons.... (<a href="https://youtube.com/watch?v=HV6F8E4jl5o">watch</a> || <a href="/videos/2023/01/11/Lets_talk_about_Santos_McCarthy_calls_to_resign_and_lessons">transcript &amp; editable summary</a>)

The Republican Party tolerates lies as long as money isn't compromised, revealing a stark truth about GOP priorities.

</summary>

"They can totally lie to you. They can lie to their constituents. They can lie to their voters. They can lie to the people who support them. Just don't mess with the money."
"It's totally okay with them if your representative, your senator, your elected official lies to you and misrepresents himself to you, but they better not mess with the money."

### AI summary (High error rate! Edit errors on video page)

Addressing ongoing developments involving Santos and the new representative.
Local GOP calls for the representative to resign and not be seated on committees.
Outrage emerged after news that the representative or his aide impersonated McCarthy's chief of staff for funds.
Republican Party leaders and committees showed they can tolerate lies, except when it comes to money.
Lesson learned: Republicans can lie to constituents, voters, and supporters as long as they don't "mess with the money."

Actions:

for republicans,
Confront establishment Republicans about their tolerance for lies from representatives (implied).
</details>
<details>
<summary>
2023-01-11: Let's talk about 87000 IRS agents and the House.... (<a href="https://youtube.com/watch?v=gkCMVNldvrM">watch</a> || <a href="/videos/2023/01/11/Lets_talk_about_87000_IRS_agents_and_the_House">transcript &amp; editable summary</a>)

Beau clarifies the hiring of 87,000 IRS employees and debunks myths surrounding the bill, exposing political manipulations for false victories while asserting that the core changes are still set to happen.

</summary>

"87,000 armed agents, okay? That was never going to happen."
"They just made that up and it got reported on by outlets like Fox."
"They will hire 87,000 more people and there will be more audits of people who make more than $400,000 a year."
"Literally nothing changed, except they got a cool little vote and talking point in."
"So all of this is still happening, just so everybody's clear on that."

### AI summary (High error rate! Edit errors on video page)

Explains the addition of 87,000 new employees at the IRS over 10 years.
Clarifies that more than 50,000 of the new hires are to replace retiring IRS employees.
Notes a net gain of around 27,000 employees, mainly customer service and IT staff.
Debunks the myth of hiring 87,000 armed agents, stating they are specifically targeting tax evaders.
Mentions that individuals making under $400,000 annually won't face increased audit chances.
Points out that despite the bill passing in the house, it's unlikely to advance in the Senate.
Criticizes the creation of false issues by certain political factions for perceived victories.
Emphasizes that the reported changes are still set to occur with increased hiring and audits.
Condemns the misleading narrative created around the IRS issue for political gains.
Concludes that the core situation remains unchanged, despite the political spectacle.

Actions:

for taxpayers, political activists,
Contact your representatives to stay informed and hold them accountable (exemplified)
Stay updated on political developments related to IRS funding and hiring (suggested)
</details>
<details>
<summary>
2023-01-10: Let's talk about Santos, McCarthy, and the new chapter.... (<a href="https://youtube.com/watch?v=_qtHMVI0Yng">watch</a> || <a href="/videos/2023/01/10/Lets_talk_about_Santos_McCarthy_and_the_new_chapter">transcript &amp; editable summary</a>)

Beau talks about the embattled Representative Santos, his aide's alleged actions, and potential consequences from messing with wealthy donors, speculating on how the truth may have surfaced and hinting at further legal implications.

</summary>

"Don't scam rich people."
"I'm not even mad. That's impressive."
"I don't know that's how this came out, how these allegations were generated. But I mean, that seems pretty likely to me."
"There are already suggestions that some of these actions might have violated the law."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talking about embattled Representative Santos and new developments
Representative's credibility questioned in various ways
Aide accused of posing as McCarthy's chief of staff to solicit donations for Santos' campaign
Potential trouble for Santos if he knew about his aide's actions
Potential consequences from messing with wealthy Republican donors
Speculation on possible reactions from McCarthy and the Republican Party
Imagining scenarios of how the truth may have come to light
Likelihood of further developments and legal implications
Ending with a casual sign-off and well wishes

Actions:

for political watchers,
Contact local representatives or organizations to advocate for accountability and transparency in political campaigns (suggested)
Stay informed about the unfolding story and share relevant updates with your community (suggested)
</details>
<details>
<summary>
2023-01-10: Let's talk about Republican priorities and ethics.... (<a href="https://youtube.com/watch?v=KhA-_0R2LO4">watch</a> || <a href="/videos/2023/01/10/Lets_talk_about_Republican_priorities_and_ethics">transcript &amp; editable summary</a>)

Republicans in control of the House majority prioritize dismantling the Office of Congressional Ethics, raising doubts about their transparency and accountability promises while potentially betraying voter trust.

</summary>

"What's more valuable than going after them? Protecting themselves, right?"
"It seems like they lied. They just made it up."
"They talk about it all the time. The corrupt Democrats. They throw that word out."
"I think there are a whole bunch of people in the Republican Party who are about to find out they were duped."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Republicans, now in control of the House majority, prioritize going after the Office of Congressional Ethics to render it ineffective and destroy its ability to operate.
Questions arise regarding promises made by Republicans to their voter base, such as investigating Pelosi, AOC, and the squad, which now seem unlikely to be fulfilled.
The move to dismantle the Office of Congressional Ethics casts doubt on Republicans' accountability and transparency promises.
Despite talking about targeting the Democratic Party during their campaign, Republicans appear hesitant to follow through on those intentions.
Republicans' reluctance to allow the Office of Congressional Ethics to function may lead to repercussions from the Department of Justice, which previously took a hands-off approach.
By undermining the office designed to keep promises of holding corrupt individuals accountable, Republicans risk alienating their voter base and betraying their trust.

Actions:

for house constituents,
Reach out to your representatives and demand transparency and accountability in government (implied).
Stay informed and hold elected officials accountable for their actions (implied).
</details>
<details>
<summary>
2023-01-10: Let's talk about Georgia and the Special Grand Jury.... (<a href="https://youtube.com/watch?v=QXB7ItP_nYw">watch</a> || <a href="/videos/2023/01/10/Lets_talk_about_Georgia_and_the_Special_Grand_Jury">transcript &amp; editable summary</a>)

Beau provides insights on the conclusion of the special grand jury investigation in Georgia and speculates on potential charges, hinting at significant developments by mid-February.

</summary>

"The show that most people are waiting for, I have a feeling it's about to start."
"And given how quickly things tend to work in Georgia, middle of February."

### AI summary (High error rate! Edit errors on video page)

Explains the conclusion of the special grand jury investigation into the aftermath of the 2020 election in Georgia, including the famous phone call about "find me the votes."
Notes that the special grand jury in Georgia doesn't issue indictments but makes recommendations to the DA, who can then seek indictments through normal processes.
Speculates that based on publicly available evidence, the recommendation from the grand jury is likely to be to charge.
Mentions the possibility of exculpatory evidence being presented behind closed doors, which could affect the outcome.
Draws a distinction between the Georgia and Michigan cases, with the potential for the Michigan case to be a significant development.
Suggests that the first indictments could mark the beginning of significant movement in response to the allegations.
Anticipates that by the middle of February, there may be more clarity on the situation in Georgia.
Indicates that for many Americans, regardless of political affiliation, the focus is on whether charges will be brought, rather than the details leading up to that point.

Actions:

for legal analysts,
Stay informed on updates regarding the special grand jury investigation in Georgia (suggested)
Follow legal proceedings closely for potential indictments and developments (suggested)
</details>
<details>
<summary>
2023-01-10: Let's talk about Biden and classified documents.... (<a href="https://youtube.com/watch?v=0KjxTZwj9YQ">watch</a> || <a href="/videos/2023/01/10/Lets_talk_about_Biden_and_classified_documents">transcript &amp; editable summary</a>)

Beau explains the differences between the Biden and Trump document cases, stressing the importance of the classification level and suggesting the need for a counterintelligence filter team during transitions.

</summary>

"Comparable is a relative term."
"If they are SCI documents, the counterintelligence teams need to go in and find out exactly what happened."
"It's probably a good idea to have what amounts to a counterintelligence filter team there."

### AI summary (High error rate! Edit errors on video page)

Explains the discovery of documents at the Penn Biden Center, some of which are classified and from Biden's time as vice president.
Compares the Biden document case to the Trump document case regarding classified documents.
Notes the immediate action taken by Biden's lawyers to hand over the documents within 24 hours, contrasting it with the Trump case.
Emphasizes that the classification level of the documents is key to determining the seriousness of the situation.
Mentions the appointment of a US attorney by Garland to oversee the investigation into the Biden documents.
Suggests that there may have been a lapse in the transition period between administrations, calling for a counterintelligence filter team.
Advocates for a more rigorous process during transitions to prevent mishandling of classified documents by former presidents or vice presidents.

Actions:

for policymakers, government officials.,
Establish a counterintelligence filter team during transitions (suggested).
Ensure rigorous handling of classified documents by former presidents or vice presidents (suggested).
</details>
<details>
<summary>
2023-01-09: Let's talk about rivers in Alaska.... (<a href="https://youtube.com/watch?v=jSYhcvoxEdo">watch</a> || <a href="/videos/2023/01/09/Lets_talk_about_rivers_in_Alaska">transcript &amp; editable summary</a>)

Alaska rivers turning orange signal the impacts of climate change, urging real action to mitigate the environmental threats ahead.

</summary>

"It's another orange flag that's kind of pretty obvious."
"The planet is definitely signaling to us that things are changing."
"Climate change is happening. It's happening all around us."

### AI summary (High error rate! Edit errors on video page)

Alaska rivers turning orange, resembling areas around mines, but no mines present.
Scientists link the orange color to climate change, specifically thawing permafrost.
Thawing permafrost releases trapped soils with sediment and iron, causing rust when exposed to air and water.
The water's acidity is also increasing in some areas.
Researchers are uncertain about the exact cause but are aware of the changing water.
Rivers on protected land in Alaska serve as drinking water for native communities.
Downstream impacts on the food web, fish, and other animals are significant considerations.
The changing rivers serve as an obvious indicator of the effects of climate change.
Urges the United States to prioritize significant mitigation efforts.
Calls for a real transition and shift to address climate change adequately.
Emphasizes the lack of support and pressure on elected leaders to address climate change.
Suggests that people need to be awakened to the imminent threats posed by climate change.

Actions:

for climate advocates, environmental activists,
Advocate for legislative priorities on climate change (implied)
Raise awareness about the impacts of climate change in local communities (implied)
</details>
<details>
<summary>
2023-01-09: Let's talk about Michigan moving forward.... (<a href="https://youtube.com/watch?v=1nZB3C5n6oA">watch</a> || <a href="/videos/2023/01/09/Lets_talk_about_Michigan_moving_forward">transcript &amp; editable summary</a>)

Michigan Attorney General Dana Nessel reopens an investigation into the electors' plot, signaling swift legal action against those involved, challenging the narrative of a broken justice system.

</summary>

"There is clear evidence to support charges against those 16 false electors."
"I don't think that this is going to be a political show."
"There are a whole lot of people saying that the justice system is broken."

### AI summary (High error rate! Edit errors on video page)

Michigan Attorney General Dana Nessel reopens an investigation into individuals involved in the electors' plot after a year of inaction.
Nessel's decision to proceed with charges is met with criticism from the GOP, who label it a political stunt.
Despite GOP claims, the Attorney General's office indicates a swift move towards charging decisions.
Nessel asserts there is clear evidence supporting charges against the false electors.
State-level investigations, like the one in Michigan, showcase frustration with the slow pace of federal investigations.
The Attorney General's office signals a non-political intent and a rapid progression towards charges.
Michigan has laws addressing the behavior in question, indicating a quick movement towards legal action.
The GOP's dismissive attitude may backfire as the investigation progresses swiftly.
Nessel's actions challenge the narrative of a broken justice system by demonstrating proactive state-level prosecution.
State attorneys general are growing impatient with the federal government's pace, leading to independent investigations and prosecutions.

Actions:

for legal activists and concerned citizens,
Contact local representatives to advocate for efficient and fair legal processes (implied)
Support efforts by state-level attorneys general pushing for timely investigations and prosecutions (implied)
</details>
<details>
<summary>
2023-01-09: Let's talk about Brazil and lessons.... (<a href="https://youtube.com/watch?v=IY98n0V9Jfs">watch</a> || <a href="/videos/2023/01/09/Lets_talk_about_Brazil_and_lessons">transcript &amp; editable summary</a>)

Recent events in Brazil mirror past events in the United States, warning against underestimating the ongoing threat of far-right authoritarianism and stressing the need for continued vigilance.

</summary>

"It's not over. It's not over in the United States. It's not over in Brazil. And it can happen anywhere."
"That style of leadership, it will transfer to the next Xeroxed copy of Trump because there wasn't that resounding defeat."
"We can't just pretend that it's stopped."
"This far right authoritarian style of leadership is easily mimicked."
"This is one of those times when the polls matter."

### AI summary (High error rate! Edit errors on video page)

Drawing parallels between recent events in Brazil and the United States where supporters of a far-right authoritarian leader stormed key buildings.
Noting the transferability of nationalist rhetoric and imagery used to incite far-right supporters.
Emphasizing that the threat is not over, cautioning against underestimating the persistence of this style of leadership.
Warning that without a resounding defeat, this authoritarian playbook will continue to be replicated with potential success.
Stating that the fight against far-right authoritarianism is ongoing and needs to be acknowledged and addressed.
Stressing the importance of recognizing the pattern and not dismissing current events as a thing of the past.
Mentioning the ease with which false patriotism can be manufactured to manipulate aggressive individuals reminiscing about a mythic past.
Noting that this leadership style can be easily mimicked and that the fight against it must persist.
Calling attention to the need to take seriously the lessons from recent events and not downplay the ongoing threat.
Urging vigilance and acknowledging that until there is a clear defeat, this cycle may continue.

Actions:

for activists, concerned citizens,
Stay informed and engaged with current events (suggested)
Advocate for policies and leaders that prioritize democracy and human rights (implied)
</details>
<details>
<summary>
2023-01-08: Let's talk about utopia, philosophy, and consistency.... (<a href="https://youtube.com/watch?v=02IkL4qjgv0">watch</a> || <a href="/videos/2023/01/08/Lets_talk_about_utopia_philosophy_and_consistency">transcript &amp; editable summary</a>)

Beau delves into philosophy, utopias, and the challenges of advocating for societal change while navigating contradictions and realities of poverty.

</summary>

"You're not going to get very far telling people who are barely getting by that they have to go further."
"When you're thinking about your ideologically pure utopia that's going to emerge generations from now, you can be pretty much anything in service of that except for ideologically pure."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the concepts of philosophy, consistency, utopias, and contradictions, sparked by a message he received.
The message questions advocating for certain government interventions while maintaining a philosophy of anti-authoritarianism.
Beau shares his vision of a cooperative society where people live harmoniously without a president.
He presents two answers to the question posed, a Twitter answer and a real answer.
The Twitter answer focuses on being a leftist for harm reduction and helping people through advocating for egalitarian society.
The real answer involves a scene from the movie Platoon that impacted Beau, showcasing the idea of fairness in combat roles for rich and poor individuals.
Beau contrasts the lifestyle of those in gated communities with homeowners associations to those in trailer parks and government housing, who already practice cooperation out of necessity.
He recounts an experience in government housing where individuals exemplified community support despite societal stereotypes.
Beau stresses the importance of providing resources and support to individuals in poverty to expand the reach of anti-authoritarian philosophies.
He concludes by suggesting that to achieve a utopia, one must be willing to adapt and be pragmatic in their approach.

Actions:

for philosophy enthusiasts, activists,
Support community initiatives to aid individuals living in poverty (implied)
Advocate for resources and assistance for those struggling financially (implied)
</details>
<details>
<summary>
2023-01-08: Let's talk about post contact.... (<a href="https://youtube.com/watch?v=p7QaPUKH4yo">watch</a> || <a href="/videos/2023/01/08/Lets_talk_about_post_contact">transcript &amp; editable summary</a>)

Beau introduces the SETI Post-Detection Hub, drawing parallels between humanity's response to COVID and potential extraterrestrial contact, questioning if we are prepared for such an event beyond scientific benefits.

</summary>

"If that was a dress rehearsal, let's just say we failed."
"They're trying to come up with established methods and a strategy for keeping everybody calm if ET shows up."
"Is it something that we're ready for as we strive to unlock the skies?"
"We have to figure out if we should even be told."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces the SETI Post-Detection Hub for processing information on potential contact with extraterrestrial beings.
Compares the hub's strategies to humanity's response to COVID, considering it a failed dress rehearsal.
Focuses on the impacts of disinformation on first contact and potential societal upheaval.
Considers whether the public should be initially informed about contact due to humanity's tendency towards panic.
Current protocol involves confirming contact's authenticity and then informing the UN, yet this may not suffice based on past experiences.
Aims to establish methods to keep calm if extraterrestrial contact occurs, acknowledging humanity's capability to handle such an event.
Raises concerns about potential fallout on Earth and exploitation of the situation if contact is made.
Questions if humanity is prepared for such contact as we venture into space exploration.
Urges reflection on whether the public should be informed about extraterrestrial contact, focusing on people's ability to accept new information.
Encourages pondering on humanity's readiness for contact beyond scientific benefits.

Actions:

for space enthusiasts,
Join organizations researching extraterrestrial intelligence (implied)
Stay informed about developments in space exploration and potential first contact scenarios (implied)
</details>
<details>
<summary>
2023-01-08: Let's talk about planting ourselves.... (<a href="https://youtube.com/watch?v=XzXdDV38zjA">watch</a> || <a href="/videos/2023/01/08/Lets_talk_about_planting_ourselves">transcript &amp; editable summary</a>)

New York's new composting law sparks backlash and potential shifts in burial traditions, challenging business interests and individual rights posthumously.

</summary>

"I mean, I wouldn't mind literally becoming part of the ranch."
"And this is definitely going to cut into some business interests, because it's fundamentally altering the way things are done."
"But make no mistake about it, the big business and government will argue over what you can do with yourself, even once you're gone."

### AI summary (High error rate! Edit errors on video page)

New York has recently allowed a unique form of composting where individuals can be composted after they pass away.
The process involves being placed in a still box with organic material to help compost the body, taking about a month.
Family members then collect the resulting dirt in an urn-like container.
This new form of composting has faced backlash from certain religious communities and traditionalists.
Despite the initial resistance, Beau predicts that rural people, particularly those who value land passed down through generations, may find this method appealing.
As more states adopt this disposal method, there may be increased opposition from religious groups and cemeteries, as it challenges established practices and business interests.
Beau believes that ultimately, the demand for this composting method may prevail due to its appeal to those seeking a return to older burial practices.
He envisions a broad demographic, including rural and conservative individuals, embracing this alternative to traditional burials.
Beau points out the potential conflict between personal choices after death and the interests of big business and government.
He concludes by suggesting that this composting method could spark debates over individual rights even posthumously.

Actions:

for environment enthusiasts, rural communities,
Advocate for eco-friendly burial options in your community (exemplified)
Research and support legislation promoting alternative burial practices (exemplified)
</details>
<details>
<summary>
2023-01-07: Let's talk about being displaced by disaster.... (<a href="https://youtube.com/watch?v=J3JxkopAw1o">watch</a> || <a href="/videos/2023/01/07/Lets_talk_about_being_displaced_by_disaster">transcript &amp; editable summary</a>)

Beau shares insights from a Census Bureau survey on natural disaster displacement, urging individuals to prepare as such occurrences are becoming more common.

</summary>

"At some point in your life, you will be displaced by a natural disaster."
"The most important takeaway here is for those who need some information to get their family members to put together a kit or a plan."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Shares insights from a Census Bureau survey on natural disaster displacement.
3.3 million Americans were displaced last year, with one out of six never returning home.
The survey provides perspective on scope and time regarding natural disasters.
Points out that these numbers are expected to increase with climate change.
Indiana, Maine, North Dakota, Ohio, and Oklahoma are the least likely to experience displacement, while Florida is the most likely.
Emphasizes the importance of emergency preparedness, especially considering the increasing frequency of natural disasters.
Mentions the vastness and diversity of the United States in accepting displaced people.
Encourages individuals to take steps to prepare for natural disasters.
Raises awareness about the commonality of natural disaster displacement.
Urges families to create emergency kits and plans.

Actions:

for community members,
Prepare an emergency kit for your family members to deal with natural disasters (suggested).
Create a plan to address potential natural disaster situations in your area (suggested).
</details>
<details>
<summary>
2023-01-07: Let's talk about Biden going all in.... (<a href="https://youtube.com/watch?v=VbjT08D7cSI">watch</a> || <a href="/videos/2023/01/07/Lets_talk_about_Biden_going_all_in">transcript &amp; editable summary</a>)

The Biden administration's ambitious program aims to reduce homelessness by 25% in two years, drawing on past successes and involving various stakeholders and agencies.

</summary>

"The Biden administration has launched the 'All in Federal Strategies for Reducing and Ending Homelessness' program with an ambitious goal of reducing homelessness by 25% in the next two years."
"Even if the program falls slightly short of the 25% reduction, any progress will be a significant win."
"The outcome of this program will be interesting to observe given its ambitious objectives and comprehensive planning."

### AI summary (High error rate! Edit errors on video page)

The Biden administration has launched the "All in Federal Strategies for Reducing and Ending Homelessness" program with an ambitious goal of reducing homelessness by 25% in the next two years.
The program involves input from people who have experienced homelessness, providers, advocates, developers, and multiple federal agencies.
Lessons learned from successful initiatives like tackling veteran homelessness are being applied to this program.
The approach includes a focus on housing first and engaging state and local governments to set equally ambitious goals.
Web-based seminars for this program start on January 10th, indicating a fast-moving initiative.
While achieving a 25% reduction in homelessness in two years seems challenging, setting such an ambitious goal is more impactful than settling for minimal targets.
Even if the program falls slightly short of the 25% reduction, any progress will be a significant win.
The timeline for achieving these goals is tight, with 2025 approaching quickly.
The resources and planning put behind this initiative make even a partial success beneficial.
The outcome of this program will be interesting to observe given its ambitious objectives and comprehensive planning.

Actions:

for advocates, policymakers, communities,
Attend the web-based seminars starting on January 10th to learn more about the program and how to get involved (suggested).
Support local initiatives to reduce homelessness by engaging with state and local governments (implied).
</details>
<details>
<summary>
2023-01-07: Let's talk about 6 republicans and McCarthy.... (<a href="https://youtube.com/watch?v=A40k-nZ60bU">watch</a> || <a href="/videos/2023/01/07/Lets_talk_about_6_republicans_and_McCarthy">transcript &amp; editable summary</a>)

Beau delves into media portrayal, Republican reluctance, and the party's priority over country in House matters.

</summary>

"You have to appeal to ambition, to being a self-serving politician."
"The Republican party has to decide whether it is going to be a conservative party or a fascist party."
"The party is what matters. They don't care about the country."
"It's the Republican Party, count on them to do the wrong thing."
"I stopped asking myself what a Republican president do and started asking myself what a fascist will do."

### AI summary (High error rate! Edit errors on video page)

Explains why the media approached a certain topic in a particular way concerning the mess up in the House of Representatives.
Questions why it is expected of the Democratic Party to make power moves or find a unity candidate instead of suggesting Republicans cross over.
Compares blaming Democratic Party for not catching Santos to the current situation, pointing out the focus on Democrats rather than Republicans.
Expresses uncertainty about finding six Republicans willing to prioritize country over party in a significant vote.
Suggests possible incentives for Republicans to cross party lines, but acknowledges the challenge in finding willing candidates.
Criticizes the Republican Party for prioritizing party over country, citing examples of authoritarian behavior.
States lack of faith in Republicans to act in the country's best interest and the need to appeal to their ambition in negotiations.
Comments on the impasse between parties and the decision the Republican Party faces in defining itself as conservative or fascist.
Notes the media's lack of expectation from Republican candidates to do the right thing.

Actions:

for media consumers, political observers, activists,
Engage in critical media consumption to understand biases and narratives (suggested)
Advocate for accountability and country-first actions from elected officials (implied)
Support unity candidates who are not beholden to extreme factions (suggested)
</details>
<details>
<summary>
2023-01-06: Let's talk about why the McCarthy show is happening.... (<a href="https://youtube.com/watch?v=F8pZMlU7rKk">watch</a> || <a href="/videos/2023/01/06/Lets_talk_about_why_the_McCarthy_show_is_happening">transcript &amp; editable summary</a>)

A prolonged political standoff on Capitol Hill reveals a power struggle driven by self-serving desires, not ideology or policy.

</summary>

"It's about power."
"It's about feathering their own nests, not fulfilling some MAGA dream or policy idea."
"It's about serving themselves, not serving the people."

### AI summary (High error rate! Edit errors on video page)

A prolonged political standoff on Capitol Hill is ongoing until a specific outcome is reached.
The framing of the situation by extremist Republicans is questioned, focusing on power struggles rather than ideology.
Republicans' desire for better committee assignments and influence drives their actions.
The political struggle aims to gain relevance without Trump and secure positions in the House of Representatives.
The extremist Republicans' requests and actions are primarily self-serving.
The Republicans facing a lack of policy are resorting to extreme measures to remain relevant.
The situation mirrors the Democratic Party's progressive members, the squad, in being unable to push through their beliefs due to lack of votes.
The extremist Republicans are considering allowing the United States to default, impacting the economy and blaming Biden.
The Republicans' threats of economic collapse are seen as disingenuous tactics to energize their base.
Ultimately, the political maneuvers are about personal gain and attention rather than fulfilling any meaningful policy goals.

Actions:

for political observers,
Contact local representatives to express concerns about prioritizing political power struggles over serving the people (implied).
</details>
<details>
<summary>
2023-01-06: Let's talk about a skeptical ceasefire in Ukraine.... (<a href="https://youtube.com/watch?v=_wvcbbeWDDw">watch</a> || <a href="/videos/2023/01/06/Lets_talk_about_a_skeptical_ceasefire_in_Ukraine">transcript &amp; editable summary</a>)

Beau explains the skepticism surrounding the ceasefire declared by Russia for Christmas, clarifying Ukraine's position and international obligations.

</summary>

"One party cannot just suddenly declare it. That's not how this works."
"It's probably not going to be followed, and any complaints about Ukraine not following it, they're not grounded in international law."

### AI summary (High error rate! Edit errors on video page)

Explains the ceasefire declared by Russia for Christmas and the skepticism surrounding it.
Russia and the Russian Orthodox Church called for a 36-hour ceasefire, but Ukraine did not agree to it.
Ukrainian side believes Russia will use this time to refit, rearm, and regroup their troops.
Beau questions Ukraine's behavior during the ceasefire and whether they will push against Russia's declaration of no fighting.
States that Ukraine has zero obligation to follow the ceasefire as one party cannot unilaterally declare it.
Advises Ukraine to be cautious about targeting to avoid a propaganda disaster.
Mentions the possibility of Ukraine launching an offensive during the ceasefire but doubts their readiness.
Talks about the United States sending Bradleys (infantry fighting vehicles) to Ukraine, which may impact any future offensive plans.

Actions:

for foreign policy analysts,
Monitor the situation between Ukraine and Russia (implied).
Stay informed about international laws regarding ceasefires (implied).
Advocate for peaceful resolutions and diplomacy in conflicts (implied).
</details>
<details>
<summary>
2023-01-06: Let's talk about 2 years, Trump's lawsuit, and medals.... (<a href="https://youtube.com/watch?v=b85XHWwYIvs">watch</a> || <a href="/videos/2023/01/06/Lets_talk_about_2_years_Trump_s_lawsuit_and_medals">transcript &amp; editable summary</a>)

Two-year anniversary of failed coup attempt marked by slow progress in federal investigations and crafting of American mythology.

</summary>

"American history vs. American mythology being crafted around the incident."
"Reminder that authoritarian rhetoric and Trumpism still present."
"Slow progress in federal investigations causing frustration."

### AI summary (High error rate! Edit errors on video page)

Two-year anniversary of failed coup attempt marked by news breaking today.
Attorney General's office statement on holding criminally responsible.
Lawsuit against former President Trump for wrongful death.
Biden to hand out citizen medals, framing incident as coup plotters vs. law enforcement.
Michigan Secretary of State and others to receive medals.
Republicans and Democrats both receiving medals for standing up to Trump's campaign.
American history vs. American mythology being crafted around the incident.
Reminder that authoritarian rhetoric and Trumpism still present.
Slow progress in federal investigations causing frustration.
Jack Smith's return may indicate movement in investigations.

Actions:

for american citizens,
Watch for updates on investigations and political developments (implied)
</details>
<details>
<summary>
2023-01-05: Let's talk about what national interests are.... (<a href="https://youtube.com/watch?v=PmFHANpmpVI">watch</a> || <a href="/videos/2023/01/05/Lets_talk_about_what_national_interests_are">transcript &amp; editable summary</a>)

Beau defines national interests as the pursuit of power in foreign policy, involving safety and wealth to achieve power, and criticizes the current system, advocating for a shift towards being the world's EMT.

</summary>

"Foreign policy is the pursuit of power, plain and simple."
"If you want to get more in depth, it is safety and wealth being used to achieve power."
"It's about achieving, building, storing power, and then using it theoretically to benefit the population."
"Foreign policy decisions are always going to lead you to why that decision was made."
"Anything that benefits the country in the realm of safety, wealth, and ultimately power, is a national interest."

### AI summary (High error rate! Edit errors on video page)

Defines national interests as the pursuit of power in foreign policy.
National interests involve safety and wealth to achieve power.
Foreign policy decisions are not transactional but part of an international poker game.
Gives an example of releasing oil to control global oil prices.
Explains how countries like China strategize based on their national interests.
US foreign policy often aims to maintain the status quo as the world's top power.
Foreign policy decisions are ultimately about power, safety, and wealth for the country as a whole.
Criticizes the current foreign policy system, suggesting a shift from being the world's policeman to the world's EMT.
Stresses that anything benefiting a country in terms of safety, wealth, and power is considered a national interest.
Urges to view foreign policy decisions through the lens of power, safety, and wealth.

Actions:

for foreign policy analysts,
Analyze foreign policy decisions through the lens of power, safety, and wealth (suggested)
Advocate for a shift in foreign policy towards prioritizing emergency response over policing (implied)
</details>
<details>
<summary>
2023-01-05: Let's talk about Musk and losing a fortune.... (<a href="https://youtube.com/watch?v=EcFe-ozVF_c">watch</a> || <a href="/videos/2023/01/05/Lets_talk_about_Musk_and_losing_a_fortune">transcript &amp; editable summary</a>)

Elon Musk's wealth loss is primarily tied to Tesla's valuation and marketing, not just his actions on Twitter, alienating customers along the way.

</summary>

"Somebody can lose a couple hundred billion dollars and still be one of the richest people on the planet."
"It's at this point, I would say most of it is just the marketing exceeding the performance."
"He is a genius. Just not the kind that people think."
"Is this go-fash, no cash? Yes, but also no."
"Elon Musk becoming the biggest loser of wealth ever."

### AI summary (High error rate! Edit errors on video page)

Elon Musk has reportedly lost a couple hundred billion dollars, surpassing others as the biggest loser of wealth.
Musk's alignment with the far right since acquiring Twitter has led to questions about the impact of "go-fash-no-cash" on his wealth.
The majority of Musk's wealth loss is attributed to Tesla's valuation and marketing rather than his actions on Twitter.
Musk's marketing genius led to Tesla being valued at around a trillion dollars, surpassing all other major car manufacturers combined.
Doubts about Tesla's valuation and Musk's selling of Tesla stock prior to buying Twitter contributed to the decline in stock prices.
Musk's alignment with the far right through pushing conspiracy theories on Twitter has alienated many, including his core demographic of left-wing supporters.
The perception that Musk is now associating with the right wing has led to alienation of customers and potential customers.
Musk's failure to maintain Tesla's marketing momentum and valuation has affected the company's stock performance.
Current Tesla customers are being lost due to Musk's actions and marketing strategies.
The decline in Tesla's stock prices is primarily due to the company's inability to sustain the valuation driven by Musk's marketing efforts.

Actions:

for investors, tesla enthusiasts,
Support alternative electric vehicle companies to reduce reliance on Tesla (implied).
Engage in critical analysis of company valuations and marketing strategies to make informed investment decisions (implied).
Advocate for ethical business practices and alignment with values when supporting companies (implied).
</details>
<details>
<summary>
2023-01-05: Let's talk about Dem leverage, McCarthy, and the Speaker.... (<a href="https://youtube.com/watch?v=DA2tPK4-7Ok">watch</a> || <a href="/videos/2023/01/05/Lets_talk_about_Dem_leverage_McCarthy_and_the_Speaker">transcript &amp; editable summary</a>)

Beau suggests leveraging votes to make McCarthy Speaker, pitting him against the Sedition Caucus for potential Democratic Party benefit in 2024.

</summary>

"They're not conservative. They're regressive."
"They're extremists. They are people who have fallen prey to the idea that social media engagement translates to votes."
"The Republican Party is in disarray. It is the Democratic Party's opportunity to create some red-on-red conflict there."

### AI summary (High error rate! Edit errors on video page)

Beau criticizes the Democratic Party for not playing hardball and missing an opportunity to exercise leverage during the McCarthy circus on Capitol Hill.
He points out that regardless of who the Speaker in the House is, they will obstruct and hold investigations that lead to nothing.
Beau suggests a strategy where the Democratic Party could leverage their votes to make McCarthy the Speaker by pulling concessions off the table from the Sedition Caucus.
He explains the catch involved in this strategy, where McCarthy must render the Sedition Caucus irrelevant once he becomes Speaker.
Beau outlines how pitting the extreme wing of the Republican Party against McCarthy could create conflict and chaos, potentially benefiting the Democratic Party in 2024.

Actions:

for political strategists,
Reach out to elected representatives to advocate for strategic political moves (implied)
Stay informed about political dynamics and potential leverage points (implied)
</details>
<details>
<summary>
2023-01-04: Let's talk about why Trump wants a more progressive GOP.... (<a href="https://youtube.com/watch?v=vvrkGp9kw_s">watch</a> || <a href="/videos/2023/01/04/Lets_talk_about_why_Trump_wants_a_more_progressive_GOP">transcript &amp; editable summary</a>)

Former President Trump shows signs of pushing for a more progressive Republican Party, urging a shift from regressive stances to adapt to societal evolution.

</summary>

"If the Republican Party wants to stand a chance in the future, they have to become more progressive."
"It's about maintaining a group of people to kick down at."
"You have to stand up to it. You can't look the other way."
"He just wants to capitalize on it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Former President Donald J. Trump is showing signs of wanting the Republican Party to become more progressive, distancing himself from causes of midterm losses and blaming the party.
Trump acknowledges mishandling issues like abortion that led to losing voters, signaling a shift towards a more progressive stance within the Republican Party.
He points out that the Republican Party's static platform hasn't kept up with societal progress, becoming regressive and outdated.
Trump's supporters, who have been socially regressive, may find it challenging to adapt to a more progressive direction the party might be heading towards.
Beau suggests that for the Republican Party to have a chance in the future, they must embrace progressiveness as society evolves.
While Trump seems to be recognizing the need for change, his followers may resist moving towards a more progressive stance on various issues.
The Republican Party's outdated platform, offensive to many Americans, could lead to continued electoral losses unless they adapt to a changing society.
Beau warns that as the party shifts its focus to different demographics, it's vital for individuals to stand up against discriminatory practices and policies.
He stresses the importance of not turning a blind eye to harmful actions and policies targeting marginalized groups and urges people to be informed voters.
Beau concludes by questioning whether individuals want leaders who resort to bullying and scapegoating, implying that the Republican Party has devolved into such behavior.

Actions:

for republican party members,
Stand up against discriminatory practices and policies targeting marginalized groups (implied)
Be informed voters and look at policies rather than blindly supporting a party (implied)
</details>
<details>
<summary>
2023-01-04: Let's talk about the numbers to defend the US.... (<a href="https://youtube.com/watch?v=ZkzRz35xYVY">watch</a> || <a href="/videos/2023/01/04/Lets_talk_about_the_numbers_to_defend_the_US">transcript &amp; editable summary</a>)

Beau explains why foreign invasion of the US is unlikely due to troop numbers and civilian firearms, affirming strong defense capabilities without needing to occupy the country.

</summary>

"The US is not at risk of invasion. There isn't a single country that has the troops."
"The US military doesn't need to occupy all of the dirt. It just has to create a line and trust that the Americans behind them are going to stay on their side."
"Be glad we're not spending enough to occupy the US because that, I mean, if you think the budget is bloated now."
"No country, no military anywhere wants to invade a country that has more guns than people."
"The US military lacks the ability to defend the United States."

### AI summary (High error rate! Edit errors on video page)

Many people are interested in troop numbers, especially in the United States, following recent events in Ukraine.
The US military does not have enough troops to occupy the country, which has raised questions about defense strategies.
The US relies on a doctrine of a strong offense and intelligence gathering for defense.
Occupying the US is not feasible due to the vast number of troops it would require, even surpassing combined forces of major countries like Russia, China, and India.
The idea of a foreign invasion of the US is highly unlikely due to the sheer number of firearms in civilian hands.
The US military doesn't need to occupy all land but rather create a defensive line and trust in American citizens' support.
The budget for defense is significant, but it's necessary to ensure national security against potential threats.
Any potential invasion of the US would have to cross oceans and air, making it extremely unlikely.
There is no need to worry about invasion threats to the US; the country is well-prepared to defend itself.
The level of spending on defense is not aimed at preparing for an occupation but rather ensuring strong national defense capabilities.

Actions:

for national security analysts,
Trust in the existing defense strategies and capabilities of the US military (implied)
Advocate for responsible firearm ownership and safety measures within communities (implied)
</details>
<details>
<summary>
2023-01-04: Let's talk about preaching to the choir.... (<a href="https://youtube.com/watch?v=exnNpEhMdSs">watch</a> || <a href="/videos/2023/01/04/Lets_talk_about_preaching_to_the_choir">transcript &amp; editable summary</a>)

Beau shares insights on the value of speaking to like-minded individuals and how it can contribute to progress and growth, challenging the notion of "preaching to the choir."

</summary>

"I don't say this to convince somebody, I say it so those who already think like me know they're not alone."
"It keeps people thinking about the larger issues. It keeps people engaged. It makes them push further, hopefully beyond you."
"Maybe your sermon helps them to keep the faith. Helps keep them moving forward and spurs their ideological growth."

### AI summary (High error rate! Edit errors on video page)

Talks about a person on YouTube who feels like they are "preaching to the choir" due to constant attacks if they express self-doubt.
Beau shares his experience of receiving hate mail to show that the person is not only reaching like-minded people.
Questions the notion that preaching to the choir is pointless, mentioning pastors preaching to choirs even though they are already aware of the information.
Emphasizes that sometimes the goal is not to convince others but to let those who think similarly know they are not alone.
Suggests that reinforcing beliefs and keeping people engaged has value in moving towards a better world.
Encourages the idea that even if you are speaking to like-minded individuals, your words can still inspire growth and progress.

Actions:

for creators, activists, influencers,
Start a community group centered around shared beliefs and growth (suggested)
Encourage ideological growth through engaging content creation (implied)
</details>
<details>
<summary>
2023-01-03: Let's talk about what McCarthy knows that you should too.... (<a href="https://youtube.com/watch?v=cTlYWa1Zn-o">watch</a> || <a href="/videos/2023/01/03/Lets_talk_about_what_McCarthy_knows_that_you_should_too">transcript &amp; editable summary</a>)

Representative McCarthy's promises reveal the Republican Party's prioritization of loyalty over ethical oversight, putting party interests ahead of accountability and truth.

</summary>

"Destroying the oversight, the ethical oversight for the US House of Representatives is not a deal breaker for the rest of the Republican Party."
"When you want to talk about putting party over country, this is the clearest example you're ever going to get."
"The truth was never told during office hours."
"They'll look the other way when the people in their party do."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Representative McCarthy made promises and concessions to become Speaker of the House, including agreeing to a vote of no confidence by five Republicans.
McCarthy also promised to effectively destroy the Office of Congressional Ethics by implementing hiring rules that hinder the office's functioning.
By making these promises, McCarthy aimed to cater to unethical individuals who might come under investigation by the Office of Congressional Ethics.
This move reveals how some Republicans in the US House are willing to undermine ethical oversight to protect themselves.
McCarthy understood that destroying oversight was not a deal breaker for many Republicans and wouldn't cost him votes.
This showcases the Republican Party's prioritization of party loyalty over accountability and ethical oversight.
The willingness of some Republicans to look the other way regarding accountability issues is a clear example of putting party over country.
McCarthy's promise to the extreme wing of the Republican Party did not deter those trying to appear as moderates to the media.
The situation exemplifies how some politicians may not directly lie but choose to ignore or overlook unethical actions within their party.
This reveals a deep-rooted issue within the Republican Party regarding accountability and ethical standards.

Actions:

for us voters,
Hold elected officials accountable for prioritizing party loyalty over ethical oversight (implied).
Support candidates who prioritize accountability and transparency within their party (implied).
</details>
<details>
<summary>
2023-01-03: Let's talk about a storm PSA.... (<a href="https://youtube.com/watch?v=QLz72jlZDPQ">watch</a> || <a href="/videos/2023/01/03/Lets_talk_about_a_storm_PSA">transcript &amp; editable summary</a>)

Beau stresses the importance of being prepared for severe weather conditions, advising against seeking shelter in the attic and ensuring you have a way out to avoid being trapped.

</summary>

"Your attic isn't a good place to go."
"Don't be somebody's nightmares for the next five years."
"Make sure you have a way out."

### AI summary (High error rate! Edit errors on video page)

Half the country is facing rough weather, be prepared for tornadoes.
Areas in California experiencing flooding should prepare for more with another atmospheric river coming.
Concerns about flooding around the Russian River are high.
Avoid going to your attic during flooding as it's not a safe place to seek shelter.
Have a plan in place and know what to do if your area gets flooded.
If you must go to the attic, ensure you have a way out by creating an exit on the roof.
Don't be someone's nightmare in the aftermath of a disaster, make sure you have a way out.
People often end up in the attic due to someone in the home unable to achieve an alternative.
Ensure you have necessary supplies like batteries and water ready for severe weather conditions.
Utilize advanced weather warnings to be prepared and not caught off guard.

Actions:

for community members in areas prone to severe weather.,
Prepare an emergency kit with batteries, water, and essentials in case of severe weather (implied).
Familiarize yourself with your area's evacuation routes and shelters (implied).
Stay informed about weather updates and warnings to take necessary precautions (implied).
</details>
<details>
<summary>
2023-01-03: Let's talk about Biden, negotiations, and the briar patch.... (<a href="https://youtube.com/watch?v=CMwWxL3jJD4">watch</a> || <a href="/videos/2023/01/03/Lets_talk_about_Biden_negotiations_and_the_briar_patch">transcript &amp; editable summary</a>)

Beau explains the key healthcare win in the Inflation Reduction Act involving Medicare's negotiation power with pharmaceutical companies, likening it to dealing with organized crime.

</summary>

"But to me, that's not the biggest."
"I mean, yeah, it's a negotiation, the same way you negotiate with Vito Corleone."
"Oh no, don't throw me into the briar patch."
"To change society, you have to change thought first, not the law."
"This is what it actually looks like."

### AI summary (High error rate! Edit errors on video page)

Explains the Inflation Reduction Act, pointing out significant wins.
Criticizes Biden administration for not adequately promoting the biggest win in healthcare.
Emphasizes that the major healthcare win involves Medicare negotiating with pharmaceutical companies.
Compares Medicare negotiation to dealing with Vito Corleone from "The Godfather."
Describes the process where Health and Human Services sets prices, and pharmaceutical companies can counteroffer but may face hefty taxes if they don't comply.
Addresses concerns about pharmaceutical companies shifting costs onto others.
Analyzes the strategy behind changing societal thought regarding Medicare for all.
Comments on the lack of legislative priority for expansive healthcare despite widespread support.
Speculates on the potential shift in public perception if pharmaceutical companies raise prices.
Advises not to negotiate with "Dark Brandon" and makes a reference to 4D chess in politics.

Actions:

for healthcare advocates,
Support legislative efforts for Medicare negotiation with pharmaceutical companies (implied)
Advocate for healthcare reform and increased support for Medicare for all (implied)
</details>
<details>
<summary>
2023-01-02: Let's talk about whether it's time to stop covering Trump.... (<a href="https://youtube.com/watch?v=a-VlzYtEVqQ">watch</a> || <a href="/videos/2023/01/02/Lets_talk_about_whether_it_s_time_to_stop_covering_Trump">transcript &amp; editable summary</a>)

Addressing the need to continue covering Trump and those who enabled him, as Trumpism will outlive him if coverage stops prematurely.

</summary>

"The coverage has to continue until there's a conclusion."
"They may rehabilitate their image, but they showed you who they are."
"Trumpism will outlive Trump."
"It's not over. Make no mistake about it."
"If the coverage stops, Trump becomes this bygone thing."

### AI summary (High error rate! Edit errors on video page)

Addressing whether 2023 is the year to stop covering Trump due to recent events, including the coup attempt.
Asserting that historic events like the coup attempt should continue to be talked about and not forgotten.
Noting that Trump's attempts to hold a press conference with no cable news outlets showing up indicates his dwindling influence.
Drawing a parallel to a 1924 article about Hitler being "tamed by prison" and the importance of continuing coverage until there's a conclusion.
Emphasizing that those who enabled Trump and similar figures are still in power, indicating a need to keep covering them.
Stating that Trumpism will outlive Trump and continue under new names or figures if coverage stops prematurely.

Actions:

for journalists, activists, citizens,
Continue staying informed about political events and figures (implied)
Stay engaged in political discourse and hold those in power accountable (implied)
</details>
<details>
<summary>
2023-01-02: Let's talk about Russia in the East and numbers.... (<a href="https://youtube.com/watch?v=cAMqsaPGsg0">watch</a> || <a href="/videos/2023/01/02/Lets_talk_about_Russia_in_the_East_and_numbers">transcript &amp; editable summary</a>)

Beau disputes assumptions about Russia's intentions and lack of support in Ukraine, questioning the feasibility of successful occupation given troop numbers and ongoing fighting.

</summary>

"They absolutely tried to take the capital and they failed."
"The support that they're pretending they have among the people there, it doesn't exist."
"Even with those numbers, they don't have it because the fighting is going to continue."
"It's not post-conflict yet."
"So anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the assumptions about Russia's intentions in Ukraine, pointing out flaws.
Russia initially aimed to take the whole country, not just east of the river.
Disputes the claim that Russia had support from the people east of the river.
Mentions partisan activity in Russian-occupied areas as evidence of lack of support.
Raises concerns about Russia's troop numbers for successful occupation.
Notes that even with altered victory conditions, Russia lacks the troops to occupy successfully.
Emphasizes that ongoing fighting prevents troops from being utilized for occupation.

Actions:

for international observers,
Contact organizations supporting Ukraine (implied)
Monitor the situation in Ukraine and Eastern Europe (implied)
</details>
<details>
<summary>
2023-01-02: Let's talk about McCarthy as Speaker.... (<a href="https://youtube.com/watch?v=00Bkf-f9Nrg">watch</a> || <a href="/videos/2023/01/02/Lets_talk_about_McCarthy_as_Speaker">transcript &amp; editable summary</a>)

Kevin McCarthy's potential speakership may leave him as a powerless figurehead, with the House dominated by self-interested members chasing headlines.

</summary>

"He won't be able to lead. He will be basically the clerk for the Republican Party."
"The House will be dominated by those who are most out for themselves."
"Even if McCarthy becomes speaker, he's speaker in name only."

### AI summary (High error rate! Edit errors on video page)

Kevin McCarthy, presumptive speaker of the House, may be the weakest in American history.
McCarthy made a major concession that weakens his ability to lead.
Any five Republicans in the House can call for a vote of no confidence against McCarthy.
The Speaker of the House doesn't have real power; representatives hold power over him.
There are five MAGA-style Republicans actively opposed to McCarthy as speaker.
These Republicans want to establish themselves as mega candidates by going against Trump's influence.
McCarthy is facing division within the Republican Party and potential circus-like behavior in the House.
By making concessions, McCarthy handed leverage and power to the rank and file of the Republican Party.
If McCarthy becomes Speaker, he won't have real power and will act as a clerk for the party.
McCarthy's potential speakership could lead to a House dominated by self-interested individuals chasing headlines.

Actions:

for political analysts, republican party members,
Reach out to Republican representatives to express opinions on leadership (implied)
</details>
<details>
<summary>
2023-01-01: Let's talk about the future of the Colorado River.... (<a href="https://youtube.com/watch?v=Q6JjhhD9HiQ">watch</a> || <a href="/videos/2023/01/01/Lets_talk_about_the_future_of_the_Colorado_River">transcript &amp; editable summary</a>)

Beau stresses the need for a sustainable water management plan as states tackle the Colorado River drought by focusing on cuts rather than long-term solutions.

</summary>

"Cutting usage is not a solution. The states have to come together and come up with a sustainable water management plan."
"You can't have just unlimited growth when you have finite resources. You certainly can't have unlimited growth when you have decreasing resources."
"The states get to set the tone for the rest of the country. And right now, the tone they're setting is one of denial."

### AI summary (High error rate! Edit errors on video page)

Nevada called on upper basin states to cut water usage in response to the Colorado River drought.
States are approaching the issue as a numbers game, focusing on cutting water usage rather than developing a sustainable solution.
Beau argues that simply managing cuts is not a long-term or short-term solution to the water crisis.
He stresses the need for states to collaborate and create a sustainable water management plan with more reuse and less reliance on the Colorado River.
Beau believes that states must understand the severity of the situation and prioritize maintaining existing resources over unlimited growth.
The current focus on maintaining growth in Arizona and Nevada shows a lack of understanding of the water crisis's severity.
He warns that if states fail to address the issue, the federal government may intervene and take control of water management.
Beau underscores the importance of states setting a precedent for managing scarce resources as similar crises may arise in the future.
The lower states are currently bearing the brunt of the water crisis, but Beau predicts that its impact will spread to more regions.
States need to develop a comprehensive, long-term plan rather than short-term fixes to address the dwindling water resources.

Actions:

for state policymakers,
Develop a sustainable water management plan with more reuse and less reliance on the Colorado River (implied)
Prioritize maintaining existing resources over unlimited growth (implied)
</details>
<details>
<summary>
2023-01-01: Let's talk about next year.... (<a href="https://youtube.com/watch?v=ltBL9xsat7A">watch</a> || <a href="/videos/2023/01/01/Lets_talk_about_next_year">transcript &amp; editable summary</a>)

Beau plans for upcoming content, including workspace upgrades, books, live streams, and in-depth interviews, with hints of bigger projects on the horizon.

</summary>

"So on the main channel, not much is going to change."
"Expect at least one live stream a month, maybe two."
"There are a couple of other bigger projects that we've been working on that I'm not quite ready to announce yet."

### AI summary (High error rate! Edit errors on video page)

Plans for next year's content on the channels are being discussed.
The main channel will not see significant changes, just a workflow change and a new building for a more productive workspace.
There will be a surprise rebuild of the shop in the new building.
On the second channel, expect a community garden video, Jeep build phases, and content related to hurricane relief.
The summer might bring travel content on the channel "The Roads with Bo."
Two books are almost ready for release this year.
Expect at least one live stream a month, possibly two.
More interviews are planned on the second channel, including in-person interviews for deeper insights.
Beau hints at bigger projects in the works but isn't ready to announce them yet.

Actions:

for content creators,
Start planning and creating content for your channels (implied)
Stay updated with community garden video, Jeep build phases, and hurricane relief content (implied)
Tune in for live streams and interact with the content (implied)
Support upcoming book releases and participate in possible motivational events (implied)
Stay engaged for future announcements of bigger projects (implied)
</details>
<details>
<summary>
2023-01-01: Let's talk about cops and that thing I didn't read.... (<a href="https://youtube.com/watch?v=sNekAAwhfQA">watch</a> || <a href="/videos/2023/01/01/Lets_talk_about_cops_and_that_thing_I_didn_t_read">transcript &amp; editable summary</a>)

Beau received criticism for not discussing high crime rates affecting poor and Black individuals, pointing out the lack of evidence linking skin tone to crime and the dangers of perpetuating harmful stereotypes.

</summary>

"Skin tone does not increase somebody's propensity to engage in violent crime."
"Treating demographic identifiers as causal things without information to back it up is a type of racism."
"Institutional racism in policing continues to persist through the inclusion of irrelevant information that perpetuates harmful stereotypes."

### AI summary (High error rate! Edit errors on video page)

Beau received a message questioning why he didn't read a specific piece of information on a police website related to high crime rates in the South, particularly affecting poor and Black individuals.
The information not read by Beau revealed that all the 10 most dangerous counties listed had an African American plurality or majority.
Beau questions the causal link between skin tone and high crime rates, pointing out the lack of evidence and providing examples like all locations having a Walmart, majority with brown eyes, and a river – none of which directly lead to crime.
Institutional racism in policing is brought to light through the inclusion of irrelevant information that perpetuates harmful stereotypes.
Beau stresses that treating demographic identifiers as causal factors without evidence is a form of prevalent racism within law enforcement, leading to dangerous assumptions and actions.
The danger lies in perpetuating the idea that Black people are more dangerous, ultimately contributing to excessive force by law enforcement officers.
The misinformation included in crime statistics can lead to viewing skin tone as a threat, despite it not correlating with an increased propensity for violent crime.

Actions:

for community members, activists,
Challenge institutional racism in policing through advocacy and awareness (implied)
Educate others on the dangers of perpetuating harmful stereotypes based on demographic identifiers (implied)
</details>
