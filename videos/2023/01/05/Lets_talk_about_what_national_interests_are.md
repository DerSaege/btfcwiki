---
title: Let's talk about what national interests are....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PmFHANpmpVI) |
| Published | 2023/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines national interests as the pursuit of power in foreign policy.
- National interests involve safety and wealth to achieve power.
- Foreign policy decisions are not transactional but part of an international poker game.
- Gives an example of releasing oil to control global oil prices.
- Explains how countries like China strategize based on their national interests.
- US foreign policy often aims to maintain the status quo as the world's top power.
- Foreign policy decisions are ultimately about power, safety, and wealth for the country as a whole.
- Criticizes the current foreign policy system, suggesting a shift from being the world's policeman to the world's EMT.
- Stresses that anything benefiting a country in terms of safety, wealth, and power is considered a national interest.
- Urges to view foreign policy decisions through the lens of power, safety, and wealth.

### Quotes

- "Foreign policy is the pursuit of power, plain and simple."
- "If you want to get more in depth, it is safety and wealth being used to achieve power."
- "It's about achieving, building, storing power, and then using it theoretically to benefit the population."
- "Foreign policy decisions are always going to lead you to why that decision was made."
- "Anything that benefits the country in the realm of safety, wealth, and ultimately power, is a national interest."

### Oneliner

Beau defines national interests as the pursuit of power in foreign policy, involving safety and wealth to achieve power, and criticizes the current system, advocating for a shift towards being the world's EMT.

### Audience
Foreign policy analysts

### On-the-ground actions from transcript

- Analyze foreign policy decisions through the lens of power, safety, and wealth (suggested)
- Advocate for a shift in foreign policy towards prioritizing emergency response over policing (implied)

### Whats missing in summary

In-depth examples and analysis of how national interests influence foreign policy decisions.

### Tags
#ForeignPolicy #NationalInterests #Power #Safety #Wealth


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about national interests
and what they are and how they impact
the people in the countries that are engaged in foreign policy
in pursuit of their national interests.
We're going to do this because somebody sent me a message.
It was like, what are national interests
and how do they impact the people living in those countries?
So basically how it was phrased.
And when I first saw the question,
I was kind of taken aback, because I was like,
what do you mean, what are national interests?
And then I thought about it from the perspective of somebody
who doesn't intensely follow foreign policy.
And yeah, that makes total sense.
That's actually a really valuable question.
And when you try to define what national interests are,
it gets shaky.
When we talk about foreign policy,
I always say the same thing, foreign policy
is the pursuit of power, plain and simple.
That's what foreign policy is.
But when you try to define national interest,
it starts to get kind of murky because every country has
its own culture, ideology, economic system,
system of government.
And then they have the framing that they
use to tie all these little pieces together.
So, it seems like there would be different national interests for different countries.
There's really not.
The main goal is power of one kind or another.
It's power.
If you want to get more in depth, it is safety and wealth being used to achieve power.
It's all about power.
This is why there are a lot of people who look at moves that get made on the foreign
policy scene and they don't seem to make any sense because they view it as transactional.
It's not.
International poker game, everybody's cheating.
Each hand, each game feeds into the next.
A good example of this, do y'all remember when the Biden administration started releasing
oil from the strategic reserve and selling it, right?
There were people that just lost their minds, he's selling oil to China while the gas prices
here in the United States are really high, right?
That doesn't seem like it's in pursuit of power, safety, and wealth, right?
But it totally was.
Oil is a global market.
You put more oil on the market, the prices go down.
sense, right? That's what happened. You can't view it as transactional. It's
about achieving, building, storing power, and then using it theoretically to
benefit the population. In that case, the goal was achieving power through the
pursuit of wealth and safety, because if the United States doesn't have a ready
supply of fuel, it stops. So that applies there too. So it's safety and wealth in the
pursuit of power. When that was achieved, what happened? Your gas prices went down.
That's how it works. You know, there were a lot of people during the Iraq War, like,
why aren't the Chinese helping? Because it's their oil. Like, I mean, when you really look
at it it is it would benefit China and has benefited China a lot so why didn't
they help and the simple answer is they didn't need to the US was gonna do it so
they reserved they held on to that power they didn't expend it they knew that it
was going to be taken care of at the same time if it was really going to
benefit China why would the United States engage in it because it's a
global market. This is why the United States gets referred to as the
world's policeman. A lot of times US foreign policy is about maintaining the
status quo. Why? Because the US is at the top. Since it's at the top, it tends to
want to keep things the way they are. Other countries, they may view power
differently, they may view safety differently, so their foreign policy
reflects that. But when you're looking at foreign policy decisions and you look
at them through the lens of safety, wealth, and power, that's always going to
lead you to why that decision was made. Even the absurdly bad ones like, I don't
know, invading Ukraine, the goal for Putin, it was misread and everything's
gone wrong, but the goal was safety, wealth, and power, those three things. But
mainly it's about power, it's about being able to exert influence to at least
in theory benefit the country as a whole. The problem is there are a whole lot of
people in the upper classes of the various countries who believe they are
the entire country, so it doesn't always help everybody. But ideally that's how
foreign policy works in its current system. Again, I've pointed out I think
that this mentality, I think the way our foreign policy and world foreign policy
works is wrong. I think it leads to a lot of unnecessary waste. Again, going back to
being the world's EMT rather than the world's policeman would make more sense.
But that's a general overview of how it works, what national interests are,
anything that benefits the country in the realm of safety, wealth, and
ultimately power, is a national interest.
What it really boils down to is scale.
A lot of times, the exact same behavior would be condemned if it was done on a smaller scale.
If the United States went in and seized a single oil field and was like, this is ours
Now that would be condemned universally, realigning an entire nation over oil, or at least with
a huge benefit of oil, well that's just foreign policy.
Pirates and emperors type of stuff.
If you're going to do something that is questionable, you've got to do it on a big enough scale.
the way foreign policy works. And theoretically it is supposed to help the
people of that country. But start looking at all foreign policy decisions through
the lens of power and then try to decide whether that decision is rooted in safety
or wealth, the pursuit of wealth. Because, you know, money is power coupons. You have
enough power, you have safety. So picture it like a triangle. Anyway, it's just a
thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}