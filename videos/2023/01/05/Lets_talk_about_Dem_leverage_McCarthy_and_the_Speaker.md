---
title: Let's talk about Dem leverage, McCarthy, and the Speaker....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DA2tPK4-7Ok) |
| Published | 2023/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau criticizes the Democratic Party for not playing hardball and missing an opportunity to exercise leverage during the McCarthy circus on Capitol Hill.
- He points out that regardless of who the Speaker in the House is, they will obstruct and hold investigations that lead to nothing.
- Beau suggests a strategy where the Democratic Party could leverage their votes to make McCarthy the Speaker by pulling concessions off the table from the Sedition Caucus.
- He explains the catch involved in this strategy, where McCarthy must render the Sedition Caucus irrelevant once he becomes Speaker.
- Beau outlines how pitting the extreme wing of the Republican Party against McCarthy could create conflict and chaos, potentially benefiting the Democratic Party in 2024.

### Quotes

- "They're not conservative. They're regressive."
- "They're extremists. They are people who have fallen prey to the idea that social media engagement translates to votes."
- "The Republican Party is in disarray. It is the Democratic Party's opportunity to create some red-on-red conflict there."

### Oneliner

Beau suggests leveraging votes to make McCarthy Speaker, pitting him against the Sedition Caucus for potential Democratic Party benefit in 2024.

### Audience

Political strategists

### On-the-ground actions from transcript

- Reach out to elected representatives to advocate for strategic political moves (implied)
- Stay informed about political dynamics and potential leverage points (implied)

### Whats missing in summary

The full transcript provides detailed insights into political maneuvering and potential strategies for leveraging power dynamics in the current political landscape.

### Tags

#Politics #DemocraticParty #RepublicanParty #Leverage #CapitolHill


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about McCarthy
and that circus that is occurring up on Capitol Hill.
Now at time of filming,
I believe they have just finished the seventh, I think, vote
and there's still no outcome,
but that may change in between the time this is filmed
and the time it goes public.
I feel like the Democratic Party is missing an opportunity
to truly exercise leverage and a lot of it.
And this is one of the main issues
with the Democratic Party is they don't play hardball
when they have the chance.
There are a lot of ideas out there
about how things can go well for the Democratic Party here.
You know, talking about peeling off some Republicans
to vote for a Democratic candidate, all kinds of stuff.
Most of it is incredibly unlikely
and even if it was successful,
it wouldn't really accomplish a whole lot.
It'd be a great symbolic victory,
but I don't know that it would really do anything.
So what leverage do they have?
We have to be honest about what the Speaker in the House
is gonna do over the next couple of years.
Nothing.
They're gonna obstruct and they're going to
hold investigations that are going to amount to nothing.
That's what they're gonna do.
Doesn't matter who it is.
That's what's going to happen.
The idea that the right Speaker is going to somehow
further some kind of Republican policy, that's silly.
There is no keynote Republican policy
that is going to make it through the House
and then make it through the Senate
and then get signed by Biden.
The Republican Party, they're not interested in governing.
They want to obstruct.
They want to make things as bad as possible
for the average American so they stand
a better chance in 2024.
That's gonna be their goal.
It doesn't matter who the Speaker is.
So what can the Democratic Party do about that?
Nothing.
But they can look to 2024
the same way the Republican Party is.
Somebody in the Democratic Party walks in,
hi, Representative McCarthy, how are you doing today?
You know the Sedition Caucus,
that group of people who have humiliated you
and undercut your power and your authority
and painted you as somebody
who can't unite the Republican Party,
therefore putting a giant dent
into any future presidential aspirations,
and they did all of this
while making you a national laughingstock, that group?
Would you like to get even, sir?
Because you don't have the votes.
We do.
So if you were to publicly announce
that you are taking all of the concessions
that you have given to them off the table,
we can snap our fingers and make you Speaker.
Now understand, you can't go back on your word here
because if you do, you look even weaker.
You're somebody who couldn't unite Republicans,
and then once you got help
from the Democratic Party to become Speaker,
you still caved.
That's how it will appear.
You will be in even a worse position than you are right now.
Then there's the catch.
If you do this, we will do our best
to paint you as a bipartisan person.
We'll make sure it's our more conservative members
that vote for you,
and we'll help you craft that image,
but if you do this, the Sedition Caucus,
they're coming for you.
They are going to try to destroy you.
So once you get that gavel,
you have to render them completely irrelevant.
You don't have a choice.
It's them or you.
You have two hours to make this announcement,
and if you don't, we'll take that as a no,
and we'll give the votes
to whoever wants to make Lauren Boebert Speaker
because she will not hesitate to use the gavel on you.
And walk out.
That's it.
What you have done,
what the Democratic Party would have done by doing this
is pit those two sides against each other
for the next two years.
They would be more interested in combating each other
than obstructing,
and on the off chance that Boebert ends up in that position,
let chaos reign.
Nothing's going to happen in the House
in the next two years anyway.
Let the Republican Party see what it's become.
Let the voters see what's happened up there.
See the type of people they have in office.
Let all of those sound bites become national news.
The extreme wing of the Republican Party,
you know, the media is currently calling
the Sedition Caucus the conservatives.
They're not conservative.
Stop pretending that they are.
They're the extreme wing.
They are far right.
They're not conservative.
They're regressive.
That crew likes to pretend that they have America on their side,
and that's how they operate.
These votes show that they don't.
They don't even have the majority
of the Republican Party establishment.
Those people inside the Republican Party,
they don't even have them on their side.
They're not mavericks.
They're not outsiders in the sense of
they're trying to get something done.
They're extremists.
They are people who have fallen prey to the idea
that social media engagement translates to votes.
They've had three election cycles to learn
that's not necessarily true and that they need to stop,
but they haven't.
They haven't learned the lesson now.
That's why they're doing this.
Give them the speaker gavel and watch them propose
more and more extreme legislation that will go nowhere
because it can't get through the Senate and won't get signed,
but it will alienate millions of Americans
and make 2024 that much easier for the Democratic Party.
The Republican Party is in disarray.
It is the Democratic Party's opportunity
to create some red-on-red conflict there.
As I said at time of filming, they're still voting.
I was hoping that this is what was going to happen today.
It doesn't seem likely.
I feel like that if the Democratic Party
was actually going to do this, they already would have done it.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}