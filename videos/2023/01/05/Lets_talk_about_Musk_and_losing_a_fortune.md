---
title: Let's talk about Musk and losing a fortune....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EcFe-ozVF_c) |
| Published | 2023/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk has reportedly lost a couple hundred billion dollars, surpassing others as the biggest loser of wealth.
- Musk's alignment with the far right since acquiring Twitter has led to questions about the impact of "go-fash-no-cash" on his wealth.
- The majority of Musk's wealth loss is attributed to Tesla's valuation and marketing rather than his actions on Twitter.
- Musk's marketing genius led to Tesla being valued at around a trillion dollars, surpassing all other major car manufacturers combined.
- Doubts about Tesla's valuation and Musk's selling of Tesla stock prior to buying Twitter contributed to the decline in stock prices.
- Musk's alignment with the far right through pushing conspiracy theories on Twitter has alienated many, including his core demographic of left-wing supporters.
- The perception that Musk is now associating with the right wing has led to alienation of customers and potential customers.
- Musk's failure to maintain Tesla's marketing momentum and valuation has affected the company's stock performance.
- Current Tesla customers are being lost due to Musk's actions and marketing strategies.
- The decline in Tesla's stock prices is primarily due to the company's inability to sustain the valuation driven by Musk's marketing efforts.

### Quotes

- "Somebody can lose a couple hundred billion dollars and still be one of the richest people on the planet."
- "It's at this point, I would say most of it is just the marketing exceeding the performance."
- "He is a genius. Just not the kind that people think."
- "Is this go-fash, no cash? Yes, but also no."
- "Elon Musk becoming the biggest loser of wealth ever."

### Oneliner

Elon Musk's wealth loss is primarily tied to Tesla's valuation and marketing, not just his actions on Twitter, alienating customers along the way.

### Audience

Investors, Tesla enthusiasts

### On-the-ground actions from transcript

- Support alternative electric vehicle companies to reduce reliance on Tesla (implied).
- Engage in critical analysis of company valuations and marketing strategies to make informed investment decisions (implied).
- Advocate for ethical business practices and alignment with values when supporting companies (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the factors contributing to Elon Musk's wealth loss, including his marketing strategies, Tesla's valuation, and alienation of customers.

### Tags

#ElonMusk #Tesla #WealthLoss #MarketingGenius #FarRight


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Elon Musk becoming the biggest loser of wealth ever.
Reporting has come out that suggests he's lost a couple hundred billion dollars and
that it has crossed the point and he has now lost more wealth than any other person.
Now that in and of itself, it is what it is.
That alone, the only real commentary there is somebody can lose a couple hundred billion
dollars and still be one of the richest people on the planet.
That might indicate a systemic wide failure, but this news has prompted a couple of people
to ask a question and that's, is this the effect of go-fash-no-cash?
Because since he bought Twitter, there has been the perception that he is openly aligning
with the far right.
So is this go-fash-no-cash?
Yes but also no.
It's not entirely that.
In fact I don't even know that that's the majority of it yet.
So part of it, a big part, maybe the majority, I didn't actually do the numbers, but I would
guess that this is most of it, has to do with the fact that he is a genius.
Just not the kind that people think.
Mark is a marketing genius.
He really is.
It doesn't matter if you don't like him.
He's really good at that.
This led to Tesla being valued at like around a trillion dollars.
There was a point in time where Tesla was valued at more than all the other big car
makers combined.
At some point people started to question whether or not that valuation was the right amount.
Whether or not the company itself really was worth that much.
And when they started to question that, the stock started to decline.
This happened before the Twitter buy.
So it started going down prior.
And that's just a normal correction.
Then you have what happened with Twitter.
He bought Twitter.
In the process of this he winds up selling a bunch of Tesla stock, which didn't help
its stock price, which further deflated the value and his wealth.
When he did that, when he bought Twitter and started to push conspiracy theories, and very
much the perception is he's aligning with the right wing, he alienated people.
But most importantly, he alienated his people.
The people who care about space travel and the environment and electric cars, they're
not the far right.
They're generally left-wing people.
And this is the demographic that has become alienated.
You know, I'm not going to stand here and say, I was going to buy a Tesla, but now I never will.
Because I was never going to buy a Tesla.
You know, when the time comes for my next vehicle, it'll probably be an electric Jeep.
But I do have a story related to that.
And it's anecdotal, but it's worth mentioning.
I know somebody who owns a Tesla.
And I watched them give static to somebody who uses Starlink, another product.
And their logic was that, you know, it would be really hard for me to trade in my car right
now, but you can stop Starlink at any time.
So not just has Musk's actions in relationship to Twitter alienated potential customers,
there are current customers that they're losing.
So part of it is, you know, go fash, no cash.
But a lot of this deals with the fact that he is so good at marketing, generally speaking.
And the company didn't keep up with the marketing.
The marketing drove the stock prices, the buzz, everything that Musk did, that whole
real life Tony Stark thing, it drove the value of Tesla up.
And the company didn't keep up with that valuation.
And that's why the stock is currently doing what it's doing.
And it is definitely being helped along by alienating a lot of their customers.
So is this go fash, no cash?
Yes, but also no.
And I would guess if I sat down and did the numbers, the majority of what was lost is
really more about the valuation of Tesla, not what's happened on Twitter.
Don't get me wrong, that didn't help.
And it's probably going to continue to hurt for quite some time.
But it's at this point, I would say most of it is just the marketing exceeding the performance.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}