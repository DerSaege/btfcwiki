---
title: Let's talk about "just asking questions"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TI9-YY5O51k) |
| Published | 2023/01/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses dislike for Fox News due to their manipulation of truth by asking leading questions to push viewers towards false conclusions.
- Points out Fox News' tactic of asking questions in a leading manner to make viewers believe something without directly stating it.
- Challenges the misconception that Fox News is not part of the mainstream media and instead argues that they are the mainstream media.
- Encourages viewers to be critical when watching Fox News, especially when they avoid making definitive statements and instead ask questions.
- Criticizes Fox News for painting false narratives that are objectively untrue and conditioning viewers towards conspiratorial thinking.
- Calls out Fox News for not providing accurate information but rather asking questions and hoping viewers get the wrong answers.

### Quotes

- "They ask questions in a leading manner to get somebody to the right conclusion."
- "Watching Fox doesn't make you special. It makes you average, below average, really."
- "They are not informing some band of plucky upstart patriots. They are the mainstream media."
- "When I see people watching it, they're nodding along with the questions because they know the answer they've been conditioned to believe."
- "Information for real patriots would be accurate."

### Oneliner

Beau challenges the misconception about Fox News, revealing their manipulative tactics of asking leading questions and painting false narratives.

### Audience

Viewers

### On-the-ground actions from transcript

- Fact-check news sources to verify information (implied)
- Encourage critical thinking and media literacy among peers (implied)
  
### Whats missing in summary

The full transcript provides a detailed analysis of Fox News' manipulative tactics and the importance of being critical of media sources to avoid falling into false narratives and conspiratorial thinking.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about the news.
And we're going to talk about the process of just
asking questions.
And we're going to talk about a misconception
that a lot of people have about a particular news outlet
and how that misconception can actually
illustrate why it's not really an outlet you
be consuming. It's not a secret that I'm not a fan of Fox News. That's not
like, you know, something I hide. I'm very open about that. But I have this message.
You've been pushing against Fox a lot in your videos lately. The only reason your
blank masters have you attacking them is because Fox is the only outlet willing
to report on things the mainstream media wants to ignore. They're the only outlet
in forming real patriots, all caps. Okay. My masters, huh? Yeah, it's totally
unsurprising that the person who sent me a message trying to defend Fox News
managed to work in a slur. That's not a surprise. Okay, so the reason I
don't like Fox is because they are very economical with the truth. More
importantly, they just ask questions. They go through a process and it's called
just asking questions. And the purpose of doing this is to ask questions in a way
that leads the viewer to believe something that the outlet can't just
come out and say because it's just overtly false and they get called out
on it. So what they do is they they ask questions in a leading manner to get
somebody to the right conclusion. Are you aware of anybody that does that on Fox
News? Can you think of why they would do that? Do you know what they look like?
Does he have brown hair? Does he wear a bow tie? Does his voice go up and he
always looks confused? That's why. And this is how they get you to believe things
that aren't true. They ask questions of the viewer. If there were a news outlet
they should be informing, not asking questions, all right?
Okay, so here's the part of this that is really important because I see it a lot with Fox.
They report on things the mainstream media won't report on, sure, okay, I mean that
makes sense.
But do me a favor real quick.
See what the highest rated cable TV shows are.
They're on Fox, right?
Fox is the mainstream media genius.
That's just something else they've lied to you about.
They've tricked you with.
They are not informing some band of plucky upstart patriots.
They are the mainstream media.
You are not special for watching Fox News.
That's not a thing.
That's what they try to appeal to.
They try to cast that image, but they are the mainstream media.
They are the big outlet.
Watching Fox doesn't make you special.
It makes you average, below average, really.
So when you're watching Fox, just keep that in mind.
And look for them to make definitive statements, things that a news outlet would typically
do. This is what happened. Who, what, when, where, right? And then when you get to the
why, do they really tell you the why or do they ask questions? Which happens? And who's
the biggest offender? And are you picturing somebody right now? Because it's what they
always do. I don't like Fox because they routinely paint a false narrative. Something
that is something that is objectively false.
We're not talking about the way every outlet, every commentator, myself included, we all
have spin.
We all have our own bias that comes through our reporting no matter how hard we try to
be objective.
I'm not talking about that.
I'm talking about things that are easily debunked, things that are easily disproven.
And they try to cast that image, they don't come out and say the falsehood, but they present
information in a selective manner that leads people to the wrong conclusion.
Like them being somehow outside the mainstream media, that would be a good example.
This is, in my opinion, it's a gateway to conspiratorial thinking that is not grounded
in reality because it conditions you.
When I see people watching it, they're nodding along with the questions because they know
the answer they've been conditioned to believe, so they just nod along.
scary if you ever actually sit down and watch it when it's playing in a restaurant or something.
And watch those people at the bar and watch them really consume it. You see it in action.
It's not information for real patriots. Information for real patriots would be accurate.
Many times they're not even providing information, they're just asking questions.
and hoping that you get the answer wrong. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}