---
title: Let's talk about both parties being the same and the overton window....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yjKcVvmIo5w) |
| Published | 2023/01/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the political spectrum with left, right, authoritarian, and anti-authoritarian.
- Addresses the statement that both political parties in the US are the same and where it originates.
- Points out that both parties use violence to maintain order but are not necessarily identical.
- Contrasts the Democratic and Republican parties regarding consent-based policing, demilitarizing the police, and responses to maintaining order.
- Analyzes the perspectives of anti-authoritarian right, authoritarian right, authoritarian left, and anti-authoritarian left on corporate and capitalist influence in politics.
- Emphasizes that while the criticisms are valid, there are distinctions between the parties in terms of policy and ideology.
- Acknowledges the challenges of debating with the extreme authoritarian right and anti-authoritarian left.
- Argues that anti-authoritarian left individuals have deeply rooted beliefs, are well-informed, and challenging to debate due to their strong convictions.
- Compares historical contexts to illustrate the evolution of political ideologies in the US.
- Encourages engaging in political discourse and mentions the Tea Party's impact on shaping Republican Party policies.

### Quotes

- "Both parties do, in fact, use the power of the state, the violence of the state, to maintain order."
- "The United States is authoritarian right."
- "People who are anti-authoritarian left, they didn't get to that position through bumper sticker mentality."

### Oneliner

Beau explains the nuances between political parties in the US, addressing criticisms of similarity and underlying ideological differences.

### Audience

Politically engaged individuals.

### On-the-ground actions from transcript

- Debunk anti-Semitic conspiracy theories through education and awareness (implied).
- Engage in civil discourse with individuals holding different political beliefs to foster understanding and constructive debate (implied).

### Whats missing in summary

In-depth analysis of the historical context shaping political ideologies and the significance of engaging in political discourse for societal progress.

### Tags

#PoliticalParties #USPolitics #IdeologicalDifferences #Debate #Authoritarianism


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the parties
and the Overton window and a common statement
that you hear from people who are outside the Overton window
when it comes to the political parties in the United States.
We're going to do this because over the last couple of weeks,
I've gotten messages from people asking how to respond to that statement
from three of the four corners of the political spectrum.
So we're going to go through and talk about it,
talk about where their statement comes from that both parties are the same,
and how to basically talk to them about it.
Okay, so to start with, you have the political spectrum, right?
So you have left and right, and then you have authoritarian,
and for the sake of this conversation, anti-authoritarian.
So left and right, and then up and down is represented by authoritarian on the upside
and anti-authoritarian on the downside.
The United States is authoritarian right.
That is what the country is.
Okay.
All right, so anti-authoritarian right.
They will often say both parties are the same.
And this comes, when they're talking about it,
it's coming from a place of internalizing a key belief of the anti-authoritarian right.
And that is that using violence to achieve state power is bad.
That's where it comes from.
So that's where that comparison gets drawn.
So the question is, are both parties willing to use violence to maintain order?
Yes.
They're not wrong.
They're not wrong in their statement, but it's also not particularly insightful.
We're an authoritarian right state.
Yes, both parties in the authoritarian right state will be authoritarian,
but that doesn't mean that they're the same.
More importantly, it doesn't mean that they're the same in relationship to the stated cause.
Which party is more likely to be interested in consent-based policing?
Which, it's still authoritarian, but it's a whole lot less violent.
The Democratic Party, right?
Which party is, at least in theory, more interested in demilitarizing the police?
It's the Democratic Party.
Which party is more likely to say,
this wouldn't have happened if he had just complied?
The Republican Party, in support of authoritarian violence to maintain order.
Which party is more likely to say, back the blue?
Well, I guess now it's back the blue unless it's a coup.
The Republican Party, right?
So while their critique is accurate, it's true.
You can't debate that.
Both parties do, in fact, use the power of the state,
the violence of the state, to maintain order.
They're not actually the same.
There's a difference.
Is it a big difference to somebody who's anti-authoritarian right?
It's not huge, but it's a step in the direction they want to go.
Okay, so what about the authoritarian right?
This is the extreme authoritarian right.
Honestly, skip it.
This is rooted in anti-Semitic conspiracy theories.
It would be a better use of your time to try to debunk the theories
than try to argue this with them, because they're not going to go anywhere.
Unless you're really good at deradicalization, and you're, I mean,
to the point that you wouldn't be sending me a message.
Your real use is to try to debunk those conspiracy theories.
So now we move to the left.
You have the authoritarian left and the anti-authoritarian left,
and they both share the same critique.
Just the way you're going to find out that the anti-authoritarian left
shares the violence critique with the anti-authoritarian right.
Okay, so what's the critique?
Both parties serve their corporate masters.
Both parties serve their capitalist masters.
Are they wrong?
They're not.
It's a valid critique, but also not particularly insightful.
I mean, let's be real.
It's a capitalist society.
Authoritarian right.
Right wing is capitalist.
The fact that both parties in a capitalist country are capitalist,
that's not actually like a big surprise.
So what are the actual problems when it comes to that statement?
When they're talking about that, and they say that,
both parties serve their capitalist masters.
Money in politics.
Which party is against Citizens United?
Which party generally wants more social safety nets?
Which party generally is for labor?
And right now when you bring that up, be prepared to hear about railroads.
Because again, they're not wrong.
They're just taking it to the extreme.
And they're saying that because both of them are on this side of the spectrum,
well, there's no difference.
And that's not accurate.
There is a difference.
It just may not be as big as they want it to be.
And then you get to the anti-authoritarian left,
which I didn't get a question about how to talk to them.
But I did get a question asking why they're
so insufferable about their beliefs.
Thanks, by the way.
OK, so they share both of these critiques.
The US government is willing to use violence to maintain order.
This is bad for the anti-authoritarian left.
It's also a capitalist entity.
This is bad for the anti-authoritarian left.
You have to use both arguments.
The thing is, as that other message pointed out,
arguing with somebody who is anti-authoritarian left,
well, they are insufferable because they're fanatics.
They won't change their mind, and they are not changing the subject.
So the question is why?
The answer is simple.
The United States is authoritarian right.
If you become anti-authoritarian right, you realized violence is bad.
Seems like that should be pretty obvious.
If you became authoritarian left, you realize that capitalism
isn't really working out for everybody.
It's benefiting a few people.
If you became anti-authoritarian left, you rejected everything
that you were born into.
People who are anti-authoritarian left,
they didn't get to that position through bumper sticker mentality.
They have put a lot of thought into their position.
They know where they want to go.
Generally speaking, they are very steeped in political ideology
and political theory.
And just like the two bumper sticker statements that we've gone over,
they're right.
What they're saying is true.
You can't actually debate their ideas because they're correct.
The only thing you have is we're not ready for that yet,
or it's too hard to get there, or something about human nature,
or something like that.
That's why it's just such a pain to argue with them.
A lot of them are so convinced that they're correct
that they don't even talk about their political ideology.
They just provide people with information
and assume that if they get enough information,
they will come to the same conclusion on their own.
They don't really feel the need to argue this.
In fact, I'm willing to bet that every one of those arguments
in which you found them unsufferable, you've starred it,
because most of them don't feel the need to try to push it.
Okay, so what occurs when people say both parties are the same?
They're taking something because they share similar views of authority
and similar economic system.
And they say, because of these two things, they're the same.
It means they're similar.
History and fiction are just full of stories
where an authoritarian capitalist entity battles
another authoritarian capitalist entity,
and one's the good guy and one's the bad guy.
It doesn't mean that they're exactly the same.
I think history may be better to go to than fiction.
The United States is authoritarian right.
It was founded as an authoritarian right country.
So 1790, the United States is authoritarian right.
1867, it's also authoritarian right.
Is it the same?
1867 would have been shortly after the Civil War.
It's not the same.
When you are talking about what's best,
just because two things share a few characteristics
doesn't mean they're identical.
And they will immediately go to,
you know, well, this is just the lesser of two evils.
I mean, yeah, sure, if that's part of your...
if you're that far outside the Overton window,
that is how you're going to look at it.
But I mean, that's the value judgment that they're making.
And when they say that, that provides you the opening
to suggest, no, they're not actually the same.
And if people were to look to the Tea Party as an example,
those views, when they were first presented,
they were way outside the Overton window.
But they put in the work, they built the infrastructure,
that infrastructure converted.
And now, I mean, a lot of that is Republican Party policy.
To me, that's not a good thing,
but it shows that it can be done.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}