---
title: Let's talk about school lunches....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mreoKmGCFzU) |
| Published | 2023/01/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Participation in school lunch programs has declined by 23% since June, a significant decrease.
- Students have accumulated $19.2 million in school lunch debt during the same period, indicating financial struggles.
- The areas most affected by this decline are the Midwest and the Mountain Plains region.
- The federal government allowed a program that supported school lunches to expire, leading to the current situation.
- Despite the pandemic, food insecurity and kids going hungry are still prevalent issues.
- Politicians focus on trivial matters like stoves instead of addressing real problems like child hunger.
- Beau criticizes the lack of priority given to tackling food insecurity and hunger among students.
- The $19.2 million school lunch debt is a massive burden for working-class families but a small amount for the federal government.
- Beau stresses the importance of addressing real issues like child hunger rather than getting distracted by insignificant matters.

### Quotes

- "There are kids at the school in your area that are going hungry."
- "Everything I know I learned when I wasn't hungry."
- "There are kids going hungry. And it's not even a talking point."

### Oneliner

Participation in school lunch programs dropped by 23%, accumulating $19.2 million in debt, exposing ongoing issues of child hunger amid political distractions.

### Audience

Community members, parents, advocates

### On-the-ground actions from transcript

- Assist families in need with school lunch payments (exemplified)
- Support local programs combating child hunger (exemplified)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's message can be best understood by watching the full transcript.

### Tags

#ChildHunger #SchoolLunchDebt #FoodInsecurity #CommunityAction #PoliticalPriorities


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about school lunches
and some statistics
and some information that I think is pretty important.
Because
since June,
participation in school lunch meals,
programs,
has declined
by 23%.
That's huge.
That's almost a quarter.
Now, your knee-jerk reaction to this,
when you hear this, oh, well, they're bringing their
food from home.
I mean, maybe some of them, but one in four?
I don't think that's what's going on. That seems pretty unlikely.
The idea that that's unlikely is backed up by the fact
that during that same period
students have racked up $19.2 million
in school lunch debt.
Because, you know, that's a thing.
Decline of 23%.
$19.2 million
in school lunch debt.
The areas that are hardest hit,
according to the survey,
are the Midwest
and the Mountain Plains area.
See, you have to ask why this is happening.
The federal government allowed a program to expire.
That's why it's happening.
During the pandemic, there was a program to assist with this, and it was
apparently working very well.
But it expired.
Because I guess
if there's not a
pandemic, kids don't need to eat?
I guess that's what it is.
That's the logic.
What this tells us, what this information tells us,
is that there are kids at school going hungry.
That's what it tells us.
Food insecurity is still a problem.
Public health crisis or not,
it's still an issue.
This might be something to keep in mind.
When you look up there at the House,
or you look at
your favorite politician on social media,
and they're ratcheting up the anger
at some mythical task force that the Biden administration is going to put together
to come after your gas stove,
and they're getting you all worked up and angry
about something like that,
it might be worth remembering that there are real problems.
There are actually issues. There are kids
at the school
in your area
that are going hungry.
But they're going to
talk about stoves.
They're going to do anything other than try to address this issue
because it's not a priority for them.
They don't know any of these kids
because they don't go to private school with theirs.
I think that when you're talking about
$19.2 million
for working-class families, that's just an insurmountable figure. It's huge.
It's huge.
When you think about how
that number came to be,
when you're talking about
the federal government, that's nothing.
It's nothing.
It's a drop in the bucket.
But there are more important things
to get people angry about.
Everything I know
I learned
when I wasn't hungry.
This is something that shouldn't be going on in the United States.
You want to talk about America first.
You want to talk about how much you love kids, and you create mythical scenarios
and vote against those.
There are kids going hungry.
And it's not even a talking point.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}