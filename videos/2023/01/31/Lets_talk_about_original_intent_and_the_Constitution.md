---
title: Let's talk about original intent and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Lspu9WYHeoA) |
| Published | 2023/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Originalists push for following the original intent of the Constitution while admitting they don't know what the original intent was, creating a paradox.
- Some believe the Constitution can't be changed, but it has amendments, which are changes themselves.
- The Constitution is not set in stone and was never intended to be unchangeable.
- The US Constitution has seven articles, with Article 5 covering amendments, indicating the founders' intent for it to be changed.
- Article 5 gives clues on how frequently and quickly changes were expected.
- Certain parts of the Constitution were protected from changes only until 1808, showing the expectation for frequent changes.
- People believing the Constitution shouldn't be changed have been misled to maintain the status quo.
- The founders understood that society and thoughts change over time, necessitating a mechanism to update the Constitution.
- Removing the ability to change the Constitution removes its beauty and original intent.
- The machinery for change in the Constitution is one of its most revolutionary aspects.

### Quotes

- "The Constitution is not set in stone and was never intended to be."
- "They expected it to be changed pretty often."
- "One of the most revolutionary parts of the Constitution is the part that those people who call themselves patriots, call themselves constitutionalists, are trying to make sure you forget."
- "When you remove that section, the beauty of the document disappears."
- "The machinery for change in the Constitution is one of its most revolutionary aspects."

### Oneliner

Originalists paradoxically push for the original intent of the Constitution while not knowing it, failing to recognize its revolutionary machinery for change.

### Audience

Constitution advocates

### On-the-ground actions from transcript

- Educate others on the original intent and purpose of the US Constitution (implied)
- Advocate for the importance of being able to change the Constitution when necessary (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of why the US Constitution was designed to be changed and adapted over time based on the founders' intentions. Watching the full transcript can provide a deeper understanding of the importance of flexibility in constitutional interpretation.

### Tags

#USConstitution #OriginalIntent #Amendments #Change #Founders


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the US Constitution
a little bit more and its original intent.
Originalists, that's a thing now.
And many of them,
as they try to tell everyone
that we must follow the original intent of the founders,
the original intent of the Constitution,
they simultaneously broadcast
that they don't know what the original intent was.
It's a unique little paradox.
In a recent video,
I think it was called,
let's talk about mythology and the amendments,
something like that,
there were a lot of people who suggested
that the Constitution can't be changed,
which is funny.
I mean, in a video with amendments in the title,
amendment is something that was changed.
There were a whole bunch of comments.
I'm gonna read one.
I'm not picking on this person in particular.
It's literally just the shortest.
It's not a myth.
They're set in stone
and they're what this country is supposed to be built on.
They are not set in stone.
The Constitution is not set in stone
and it was never intended to be.
When you say that,
and when those who argue
that the Constitution is some document
that just cannot be altered,
you undermine one of the most beautiful parts
about the US Constitution.
The US Constitution has seven articles,
seven main sections, right?
First three are real easy.
You know, they're covering the legislative,
the executive, the judicial.
There's an article covering how the states
should interact with each other.
There's an article covering the Supreme Law.
There's an article covering ratification.
And the fifth article, Article 5, covers amendments.
The machinery for change included in the Constitution
was so important, it got its whole own article.
The Constitution was definitely meant to be changed.
That is the original intent.
It's supposed to be changed.
More importantly, in Article 5,
it actually gives you a clue, a little hint,
as to how frequently and how quickly
they expected it to be changed.
Because the last few sentences,
it prohibits changes of certain parts of the Constitution
and one overriding principle.
The overriding principle being you
couldn't put in an amendment that removed
the voting power of a state without that state's consent.
But there were a couple of individual clauses
that were protected.
Only until 1808, though, they were there to protect slavery.
But only until 1808.
20 years.
The founders, the people who put the thing together,
expected it to be changed pretty frequently.
Because when they wrote it, they put together
a prohibition on changing certain parts of it for 20
years.
If you didn't expect it to be changed frequently,
you probably wouldn't need that, right?
They expected it to be changed pretty often.
So why do so many people believe that it shouldn't be changed,
that it can't be changed?
Because they were lied to.
They were lied to.
If you believe this, you were lied
to somebody who wanted to keep you in your place,
wanted to maintain the status quo,
creating this image that this document shouldn't be changed.
That's against the original intent.
As it stands today, with the exception
of removing a state's ability to kind of represent itself,
everything in the Constitution is fair game to be changed.
And that's how it was written.
Got its whole own article.
That's the original intent.
And for those who say that they support
would defend the Constitution, those
who look at the founders as geniuses,
when you remove that section, the beauty of the document
disappears.
That section showed that they understood that over time,
thought shifted, that society would change.
And if they wanted to create a document that
was going to be able to keep up with people,
they had to create the machinery to change the document itself.
One of the most revolutionary parts of the Constitution
is the part that those people who call themselves patriots,
call themselves constitutionalists,
are trying to make sure you forget because they
enjoy the status quo.
They want you where you're at.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}