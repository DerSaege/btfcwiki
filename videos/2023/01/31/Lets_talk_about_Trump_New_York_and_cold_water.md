---
title: Let's talk about Trump, New York, and cold water....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9qTGc3MuxK4) |
| Published | 2023/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New York has impaneled a grand jury to look into Trump's alleged payments to Ms. Daniels and possible criminal activity.
- The most likely scenario involves falsification of business records.
- People are excited, thinking Trump might be arrested, but there are factors to manage expectations.
- Potential hearings on statute of limitations could cause delays if Trump is charged and arrested.
- Even if convicted, the chance of Trump going to jail over this low-level felony seems unlikely.
- Being a former president might influence the outcome, possibly shielding him from jail time.
- Beau admits to not knowing enough about New York law to form an opinion on the situation.
- Despite tax evasion history, managing expectations is key as this case may not lead to imprisonment for Trump.

### Quotes

- "This case is not like a lot of the other ones where the charges are much more serious."
- "I don't really see jail as something that's going to happen."
- "Even if he's convicted, I don't really see jail as something that's going to happen."

### Oneliner

New York grand jury looking into Trump's alleged payments might not lead to jail time, managing expectations is key.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed on the legal proceedings (implied)
- Manage expectations and avoid speculation (implied)

### Whats missing in summary

Insight on the potential implications of the investigation and legal proceedings

### Tags

#Trump #NewYork #LegalSystem #CriminalActivity #GrandJury


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about New York and Trump
and cold water, cold water.
There are a lot of people who are very excited
about the news that has come out of New York.
And I think it might be time to go ahead
and start managing expectations on this one a little bit.
So if you don't know, if you've missed the news,
New York, there's a grand jury, like a real one,
not one that's like gonna do a little investigation
type thing, no, a real grand jury has been impaneled.
And it is reportedly looking into
Trump's alleged payments to Ms. Daniels
and the possibility that there may be
some criminal activity that went along with that.
The most likely scenario, if you listen to the legal analyst
has to do with the falsification of business records.
Okay.
So for a lot of people,
they understand this is the step before like he's arrested.
And that's true.
That is true.
Because of that, they're very excited,
but we have to kind of run through some things.
Let's assume for a second that Trump is charged, arrested.
Okay.
Right off the top, right away,
there are going to be hearings
about the statute of limitations.
There is a weird part of New York law
that seems to allow them to like stop the clock
if he leaves the state continuously,
such as moving into the White House
or down to Florida.
And I'm sure that's how the prosecution is going to argue it.
For those that don't know,
the statute of limitations is basically a time limit
that they have to bring the charges.
That is going to be argued.
So you're going to have a bunch of delays over that.
Then let's just take it to the next step.
Let's assume he's arrested,
you know, charged, goes to trial.
They go through the statute of limitations hearing,
it sides with the prosecution.
He gets convicted.
The idea that he's going to jail over this is really unlikely.
This is a low level crime.
I want to say it is the lowest level felony
that New York has.
We like to pretend
that we don't have a two-tier justice system.
The reality is that he's a former president.
I know that people don't always like to acknowledge that,
but he is.
Even if he's convicted,
I don't really see jail as something that's going to happen.
That seems very unlikely to me.
But I do want to kind of go back to one little thing
when it comes to the statute of limitations thing.
I'm sure people are going to ask what my opinion is.
I don't know enough about New York law
to even have an opinion on that.
I have no idea, can't even guess how that would play out.
And that's kind of the same thing with all of this.
From outside looking in, it's pretty low level.
And he's a celebrity with a lot of resources
and a former president.
This doesn't seem like a case
that's actually going to put him behind bars.
That being said, they did get componed for tax evasion.
So, I don't know.
Tax evasion, so.
I would just manage your expectations on this one.
This case is not like a lot of the other ones
where the charges are much more serious.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}