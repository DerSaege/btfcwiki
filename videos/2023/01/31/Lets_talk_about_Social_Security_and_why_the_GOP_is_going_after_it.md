---
title: Let's talk about Social Security and why the GOP is going after it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QCr06LXSJ5Y) |
| Published | 2023/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A conservative who has been a lifelong Republican crossed party lines in 2018 due to Trump's influence but still identifies as conservative.
- The conservative values individual responsibility and believes in planning for retirement independently rather than relying on the government, even though they disagree with Social Security.
- Despite Social Security's popularity, the Republican Party's stance remains against it, with no majority support for their ideas.
- Beau questions whether the cruelty is the point behind the Republican Party's actions, suggesting they are targeting and blaming certain groups to appeal to their base.
- The Republican Party's focus has shifted from individual responsibility to grievance and blaming others, even though slogans and policies against Social Security persist.
- Beau criticizes the party for promoting candidates who refuse to acknowledge election losses and describes them as no longer being advocates of personal responsibility.
- He points out that many Republicans, particularly in the House, prioritize social media engagement over actual voter support, banking on slogans but lacking substance.
- Beau warns that if the Republican Party succeeds in altering Social Security benefits, it will have severe consequences for those relying on it, as many members of the party are unaware of the program's broader implications.
- The shift towards authoritarianism within the party is noted, with an emphasis on consolidating power at the expense of understanding the consequences of their actions.
- Beau concludes by stating that the Republican Party has deviated from its original principles of individualism and anti-authoritarianism, now embodying entitlement and grievance while shunning representation in favor of rule.

### Quotes

- "The slogans are still there. Those policy planks are still there."
- "It's a party of blaming everybody else."
- "They want to rule."

### Oneliner

A conservative dissects the Republican Party's shift from individual responsibility to grievance, warning of the consequences of their actions on Social Security.

### Audience

Conservative voters

### On-the-ground actions from transcript

- Contact local representatives to express support for Social Security and advocate against cuts or alterations (implied)
- Educate fellow conservatives on the importance and impact of Social Security to debunk misconceptions (implied)
- Organize community forums or events to raise awareness about Social Security and encourage informed decision-making (implied)

### Whats missing in summary

Insights on the historical context and evolution of the Republican Party's stance on Social Security could be further explored in the full transcript.

### Tags

#RepublicanParty #SocialSecurity #Conservatism #Entitlement #Grievance


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the Republican Party
and Social Security.
And we are going to answer a question from a conservative
about the Republican Party's current stance
and whether it really is what a lot of people
on the other side of the aisle think it is.
Okay, so here we go.
I'm a conservative.
I've been a conservative my whole life.
I was a staunch Republican.
The first time I crossed party lines was in 2018
when the party fell under Trump's spell.
But I'm still a conservative.
I started watching you during the pandemic
when conservatives I follow started telling people
to not protect themselves.
I'm a conservative in the sense of individual responsibility.
That means I support protecting yourself
and I believe I can plan for my retirement
better than the government can.
Social Security has never been a program I agree with,
but people like it, so I'm outvoted.
And I'm okay with that.
All of that to ask you this.
In your comments, people often say,
the cruelty is the point.
Is that really what it is?
Why are Republicans going after a program that is so popular?
I feel like they'll never come back to normal.
Okay, so first, if you don't know,
the Republican Party is doing its normal stuff
with Social Security,
the normal rhetoric that comes from them.
Second, yes, Social Security is wildly popular.
90% of voters want Social Security to exist 10 years from now.
76% of voters would rather pay more into Social Security
than have its benefits reduced.
The numbers are wild.
There's not even a majority of Republicans
who support the current ideas.
It's just there is no support for what they're doing.
Is the cruelty the point?
With the Republican Party, sometimes it is.
Sometimes it really is,
in the sense of they're othering a group of people,
they're targeting them, they're blaming everything on them
to give their base a way to feel better about themselves.
So in those cases, yeah, the cruelty is the point.
Is that what's happening here?
I don't think so.
I don't think so.
I think this is more akin to their stance on family planning
and them catching the car after that Supreme Court decision.
Just like their stance on family planning,
the stance against Social Security,
it's been there a long time.
It's a major piece of the Republican Party platform.
It's kind of always been there.
So it remains, even though ideas have shifted,
thought has shifted over the years,
that policy plank is still there.
And what's happened is the Republican Party
has become more interested in slogans,
the average Republican.
And they know from the time they were little,
they heard people talking about exactly what you said.
I can plan better for my retirement
than the government can.
So those slogans, they still resonate.
There are conservatives and Republicans in particular
who want to get rid of Social Security,
but the individual responsibility thing,
the part that motivates you,
that's not the Republican Party anymore.
That's not there.
That is not the Republican Party.
They are not the party of individual responsibility.
They're the party of grievance.
They're the party of nothing's fair.
I mean, you're talking about a party
that really does support candidates
who can't even admit they lost an election.
This is not a party of personal responsibility.
It's a party of blaming everybody else.
That's what they've turned into,
but the slogans are still there.
Those policy planks are still there.
So a lot of Republicans, particularly those in the House,
know that they can get social media engagement,
which they then believe translates into votes,
although you're living proof that it doesn't.
I would notice you said 2018, not 2016,
which means in the beginning,
it sounds to me like you voted for Trump,
and then after two years of seeing what it really was,
after the campaign, you were like,
yeah, no, I'm not down with this,
which is something that happened to a whole lot of Republicans
who actually believe in the idea of individual responsibility.
The party pretends that that's still who they are.
They're not that.
They haven't been that for a while.
So they still adopt those policies,
and they still try to push them through
because that's always been part of the Republican Party.
If this happens, if the Republican Party
is somehow successful in taking benefits away
or adjusting the way it actually gets paid out to people,
it's going to be way worse than after that Supreme Court ruling
because while you say you can plan for your retirement
better than the government can,
remember that's not all that Social Security is used for.
There are a whole lot of Republicans
who depend on the checks that they're saying
they're okay with them getting rid of
because they don't understand how everything works
because it's become a party of slogans.
It's become a party of memes,
of social media engagement, and nothing more.
In this case, I don't believe the cruelty is the point.
I believe that they literally don't know any better.
They do not understand what's going to happen,
and because they have become more authoritarian,
again, not the party of individual responsibility,
it's more and more about governors
doing everything they can
and then having the state courts say,
no, you can't do that.
They're trying to take as much power
for themselves as they can.
I think if they do this,
they're going to lose just drastically,
and it's not who the Republican Party is anymore.
It's not who they've been for a very, very long time.
They're not the individualist, anti-authoritarian party
that they once cast themselves as.
You're going to see people down in the comments saying
if they were never actually that party,
they were just better at pretending to be that party,
and there's an element of truth to that,
but at one point in time,
the majority of Republican voters
actually believed in those ideals.
Today, they don't.
They're the party of entitlement and grievance,
and because the party has embraced authoritarianism,
the representatives, they don't want to represent.
They don't want to look at what their party thinks,
what us commoners think.
They don't want to represent.
They want to rule.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}