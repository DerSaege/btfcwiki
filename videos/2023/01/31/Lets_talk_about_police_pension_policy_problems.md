---
title: Let's talk about police pension policy problems....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=u6AZoHi2cvo) |
| Published | 2023/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Qualified immunity is not what many people think it is; it's a civil protection, not a shield against criminal charges.
- There are calls to alter or end qualified immunity, but it's not a cure-all solution.
- The slogan "end qualified immunity and take the payments out of the police pension fund" is not a policy suggestion but a statement to show accountability.
- Shifting payments to the police pension fund could create unintended consequences by providing a financial incentive for officers to cover up misconduct.
- Changing statutes for criminal liability may be more effective in changing the culture within law enforcement than altering the pension fund system.
- Confusing slogans with solutions can have serious consequences, especially when lives are at stake.

### Quotes

- "Slogans are not solutions."
- "We can't confuse slogans with solutions, especially with something like this."
- "Lives are literally on the line."
- "It's a great slogan and it's a great way to draw attention to the fact that officers aren't held accountable enough."
- "Anyway, it's just a thought."

### Oneliner

Slogans like "end qualified immunity and take payments from the police pension fund" draw attention to accountability but may have unintended consequences, confusing slogans with solutions when lives are at stake.

### Audience

Advocates, policymakers, activists

### On-the-ground actions from transcript

- Advocate for altering statutes for criminal liability to incentivize reporting misconduct (implied)

### Whats missing in summary

The full transcript provides a comprehensive breakdown of the slogan "end qualified immunity and take payments from the police pension fund" and its potential implications, urging caution in confusing slogans with actual policy solutions.

### Tags

#QualifiedImmunity #PoliceReform #Accountability #PolicySlogans #CommunityPolicing


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about a slogan that is,
we need to remind everybody it is a slogan.
It's something meant to highlight something.
It's not an actual solution.
Slogans are not solutions.
Facebook posts are not policy.
And we need to go through this
because I'm now seeing this tweeted to members of Congress
as an actual suggestion.
And it's one of those things
that sounds good at first glance,
but there's a whole lot more to it.
We don't want to become the right wing here
and fall for our own slogans to the point
that we actually expect that to become policy.
Okay, so we're going to talk about qualified immunity.
If you have passionate opinions about qualified immunity,
please watch the whole video.
Starting off for a whole lot of people,
qualified immunity is not what you think it is.
This is not the thing that stops cops
from getting charged criminally.
This is a civil thing.
It stops them from being sued in a lot of cases.
It's a shield.
There are widespread calls to alter or end qualified immunity.
That is probably part of the solution,
part of the solution.
It's not a cure-all.
It is not a cure-all.
It doesn't fix everything,
but that's probably part of the solution.
Now, a slogan that was used for a long time
was end qualified immunity
and take the payments out of the police pension fund.
It was a slogan.
It was designed to show that, you know,
if you really think about it,
the cop engages in misconduct against a quote, taxpayer,
and then the taxpayers quote, end up paying for it.
That's what it was designed to show.
And then the idea is to shift it to the police pension fund
to show who is responsible for it.
There are people now who take this
as a real policy suggestion.
And it makes sense because it's been said a lot.
I would imagine that if you go through my videos,
at some point, I've probably said it, but it's a slogan.
So when you say this and you think about it seriously,
what are you imagining happening?
You're imagining that an officer engages in misconduct,
they get sued,
and because it's coming out of the police pension fund,
well, then they get fired
and don't get rehired somewhere else.
This is what you're imagining.
This is, it's just a few bad apples
with extra steps to get to that conclusion.
You're assuming that their response
is going to be a positive one.
What's the reality?
Look at that footage.
Look at the footage from Memphis.
You don't have to look at the actual incident,
but look at the aftermath.
All of them standing around.
They do that for free.
I think most people would acknowledge
that there is a culture within law enforcement
that encourages them to cover for each other.
It's been the subject of hundreds of books,
articles, movies, TV shows, everything.
This is a widespread and widely acknowledged
part of law enforcement culture within the United States.
They do it for free.
Now, because of that institutional culture,
because of camaraderie,
because they don't want to be labeled
as being a rat or whatever,
they do it for free.
Now, if you start taking the money out of the pension fund,
you are giving them a financial incentive to cover it up.
Officer Good Apple rounds a corner
and witnesses misconduct.
If he reports it, that person on the ground,
they're winning their lawsuit.
They're going to win their lawsuit
and his retirement is going to be reduced.
They'll do the math.
I'm going to lose $100 out of my retirement every month.
I'll be retired for 30 years.
That's 30 years of my life.
I'm going to lose $100.
I'll be retired for 30 years.
That's $36,000 out of my pocket.
That's not really a solution.
It doesn't work that way.
It will not.
I do not believe that they will overcome their cultural,
the institutional culture that's developed
to give them a financial incentive not to.
They are definitely, if we find out that Officer Bruno
is out there beating people up,
they are not talking about Bruno
because it's money out of their pocket.
It's not actually a solution.
It's a great slogan
and it's a great way to draw attention
to the fact that officers aren't held accountable enough.
But we can't confuse slogans with solutions,
especially with something like this.
Lives are literally on the line.
Again, for everybody,
altering drastically or ending qualified immunity,
yeah, that's probably part of the solution.
But it's not a total fix.
And taking it out of the pension fund
may have unintended consequences.
Dealing with the civil aspects
through qualified immunity, all right.
Adjusting policy,
changing statutes for the criminal liability,
now that is probably going to be more
of an incentive to change the culture.
Because if laws start being put on the books
that say, well, you knew about it and you didn't report it,
yeah, that's a minimum three years.
It might alter things way more
than creating a situation where they are paid more
if they don't report it.
They are paid more during their retirement
if they cover it up.
Anyway, it's just a thought.
All right.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}