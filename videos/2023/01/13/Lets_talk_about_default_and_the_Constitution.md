---
title: Let's talk about default and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=id5hxCtWmwo) |
| Published | 2023/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the United States defaulting on its loan payments and addresses questions and concerns about it.
- Outlines the potential consequences of a U.S. default, including increased interest rates, decreased dollar value, inflation, and a decline in standard of living.
- Emphasizes that holding the budget hostage means holding financial stability, way of life, and standard of living hostage.
- Analyzes the constitutionality of a U.S. default, mentioning the 14th Amendment, Article 1, Section 8 of the U.S. Constitution, and Federalist Papers number 30.
- Suggests that defaulting intentionally is unconstitutional and advocates for the Supreme Court to rule on it.
- Contrasts the Obama administration's stance on defaulting with the belief that it wasn't constitutional but not a good strategy.
- Urges to call the bluff of those considering default, especially because it impacts the wealthy individuals in Congress.
- Asserts that the lack of case law on default is due to the historical adherence to economic stability and the absence of individuals willing to risk such a detrimental outcome.
- Concludes by stating that those advocating for default are either ill-informed or ill-intended and calls for action to prevent a U.S. default.

### Quotes

- "They're holding your financial stability hostage."
- "This isn't some thing where some plucky upstarts are sticking it to the man up in D.C. Their leverage is you."
- "I do not believe that the majority of the Republican Party is going to be willing to allow the United States to default."
- "Either they don't understand or they are literally bad actors."
- "Those are the options."

### Oneliner

Beau outlines the severe consequences of a U.S. default, challenges its constitutionality, and urges action against ill-informed or ill-intended individuals to prevent harm to economic stability and standards of living.

### Audience

US Citizens

### On-the-ground actions from transcript

- Contact your representatives and urge them to prevent a U.S. default by supporting financial stability and economic well-being (implied).
- Stay informed about the potential impacts of a U.S. default and advocate for responsible decision-making in Congress (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of a U.S. default, the constitutional aspects surrounding it, and the necessity of taking action to prevent detrimental effects on financial stability and living standards.

### Tags

#USDefault #Constitutionality #FinancialStability #EconomicImpact #Congress


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about default.
We're going to talk about the idea of the United States
defaulting on its loan payments.
We're going to do this
because there's a bunch of questions about it
because there are people in Congress
who seem to think it's a good thing.
So we're going to kind of go through a bunch of the questions.
We're going to talk about the constitutionality of it,
a whole bunch of stuff.
But before we do any of that,
I want to talk about what happens
if the United States defaults,
because I think we need some perspective
on what we're talking about here.
We're going to use a very middle-of-the-road chain of events.
There are doomsday scenarios associated with the U.S. default.
I think they're alarmist,
but a middle-of-the-road estimation of what will occur
if the United States defaults
and then shortly thereafter makes good on its loans.
What you will see happen is interest rates go up.
The value of the dollar go down,
which means your purchasing power goes down,
which means inflation goes up,
which means you find it harder to make ends meet.
Your standard of living declines substantially.
That's a middle-of-the-road estimate.
Trillions of dollars in economic damage.
Okay, so that's what we're talking about.
That's what we're dealing with as far as outcomes here.
When you hear them say,
oh, they're holding the budget hostage.
No, they're holding your financial stability hostage.
They're holding you, your way of life,
your standard of living.
This isn't some thing where some plucky upstarts
are sticking it to the man up in D.C.
Their leverage is you.
Do not forget that.
Okay, so one of the big, big questions is,
isn't it unconstitutional for the U.S. to default?
And there are a lot of soundbite slogan answers to this.
You know, most of them revolve around
pointing to the 14th Amendment.
And basically it says national debts,
they can't even be questioned, is what it says.
So most of what I have seen,
they are not complete.
They're not inaccurate, but they're not complete,
and they don't give a good understanding.
My belief is that if you combine Article 1,
Section 8 of the U.S. Constitution
with the 14th Amendment and Federalist Papers number 30,
which is where Alexander Hamilton just goes off
about the concept of paying our debts on time,
that if the Supreme Court looked at it,
they would say, yes, defaulting is unconstitutional,
especially if you're doing it on purpose.
I definitely believe that's the case.
To me, it is unconstitutional.
However, the little tidbits you see on social media
seem incomplete to make that case.
And most importantly, that case has never been made in court.
So maybe the Biden administration
should kind of get on that and send it to the Supreme Court,
because I am of the opinion that this court,
being at least in theory,
one that goes off of original intent,
would look at the Federalist Papers and be like,
yeah, this is unconstitutional.
You can't do this.
And then the whole concept of a debt ceiling goes away.
But that hasn't happened.
On the flip side, you see little bits on social media
suggesting that the Obama administration
thought that it was constitutional to default.
That's not the case.
Basically, Obama said it wasn't a good strategy
to just do it on their own through the executive branch.
The reason he believed that was because
if they just did it on their own,
ignored the wishes of Congress, and just did it,
it would be challenged in court,
which would cast doubt
on the economic system of the United States.
It would shake that faith.
And the economic system here is built on faith.
If that faith is shaken, all those bad things happen.
This was a case of the Obama administration
putting country above party or political ambition,
something that a whole lot of people today
should probably take notes from.
He did not believe that it was constitutional.
I haven't seen anything to suggest that,
just that it wasn't a good strategy.
Those two things aren't the same.
Okay, so we've covered what the stakes are.
We've covered the constitutionality of it.
Now we go to the big question.
What do you do?
My opinion, call their bluff.
Call their bluff.
This is not the vote for the Speaker.
Another reason I'm fairly certain
that the Supreme Court was side
with the idea of paying the debts
is because they're rich people.
They are rich people.
So are most people in Congress, to include the Republicans.
If the United States economy takes a hit
like the kind of hit that is likely to occur
after a US default, they're heavily impacted.
I'm willing to bet that they will put their money
over any belief that they have.
That tends to be the trend.
Just in this case, it also works out
for the people who don't have tons of money.
We're not talking about the Speaker vote.
All of the Democratic Party would vote
in favor of paying the bill,
and you would only need a few Republicans to break away.
Now, with all of this in mind,
you have to ask why there isn't case law on this.
Why hasn't this ever been challenged?
Because until very recently,
you didn't have people who were so ignorant
of the US economic system
that they would risk something like this in Congress.
There was no reason to do this.
The US has not defaulted in its modern economy
since the current economy has been developed.
It hasn't defaulted, and a default would be devastating.
It's never had to go to the courts to be tested
because nobody's that bad.
Like, we haven't had people who would look at that
and say, oh, that's a good thing.
Let's do that or let's risk that.
It is safe to say that anybody who believes
that holding the US kind of in limbo
and threatening default is so ill-informed
about the US economic system
that they should be nowhere near
any decision-making process whatsoever.
Either they're ill-informed or they're ill-intended,
and they literally, literally want to harm the United States.
They want to lower your standard of living.
They want to weaken your economic stability.
Those are the options.
Either they don't understand or they are literally bad actors.
I do not believe that the majority of the Republican Party
is going to be willing to allow the United States to default,
not because I think they have the good of the people at heart,
but because it's going to hurt them a whole lot, too.
I would call their bluff.
In the meantime, maybe get a ruling from the Supreme Court.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}