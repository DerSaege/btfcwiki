---
title: Let's talk about Trump campaigning on military action....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZobppeCGV5I) |
| Published | 2023/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes Trump's campaign promise to use the US military to go after organizations in Mexico without questioning the cause and effect or looking at the consequences.
- Trump plans to utilize the US military against organizations in Mexico, similar to how he took down IS, despite ongoing hostilities with IS and recent incidents in the US.
- Trump's promise involves ordering the Department of Defense to utilize special forces, cyber warfare, and covert actions to target cartel leadership, infrastructure, and operations.
- Beau points out the potential mission creep and spread of military operations if cartels are targeted, leading to a situation spiraling out of control and triggering a civil war.
- The promise is not to defeat the cartels but rather to create refugees and unnecessary destruction.
- Criticizes the media for not questioning campaign promises and focusing on the potential negative outcomes of such actions.
- Mentions past US military interventions leading to refugees and destabilization in other countries, suggesting a similar outcome if Trump's promise is fulfilled.
- Emphasizes the cycle of mistakes in US foreign interventions and the inability to fix other countries while causing harm.
- Concludes that Trump's promise will result in unnecessary destruction, chaos, and more refugees without achieving victory.

### Quotes

- "His promise is not to win. His promise is to create a bunch of refugees."
- "It might be time for the media to stop accepting campaign promises just as talking points."
- "Talk about what is actually going to occur if this campaign promise is fulfilled."
- "A whole bunch of unnecessary destruction and chaos, a whole bunch more refugees."
- "He won't win, because this war, the plants won."

### Oneliner

Beau criticizes Trump's promise to use the US military in Mexico, foreseeing unnecessary destruction, chaos, and more refugees due to flawed foreign interventions.

### Audience

Activists, Policy Makers

### On-the-ground actions from transcript

- Advocate against harmful foreign interventions (implied)
- Support policies that prioritize diplomacy over military actions (implied)

### Whats missing in summary

Full understanding of the negative impact of Trump's proposed military actions and the cyclical nature of US interventions leading to destabilization in other countries.

### Tags

#Trump #USMilitary #ForeignIntervention #Refugees #MediaCoverage


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
one of Trump's campaign promises.
What he promises to do if he gets elected
and becomes president again.
It's made some headlines,
and I have problems with the way it's being framed
because it's not being questioned at all.
The cause and effect that comes from this promise
isn't being looked at in any way.
So what is his promise?
He says that he will use the United States military
to go after organizations inside Mexico.
That's what he is talking about doing.
He's saying that he will use the US military
to take them down the same way he took down IS,
which is entertaining that the former president
still believes that he won that,
given the fact that US forces
are actively involved in hostilities
with that organization, like now,
and that very recently there was an incident
inside the United States
and that prosecutors contend the reason for the incident
was so the assailant could join the organization
he claims he defeated.
But what he is directly saying, his quote,
is that he will order the Department of Defense
to make appropriate use of special forces,
cyber warfare, and other overt and covert actions
to inflict maximum damage on cartel leadership,
infrastructure, and operations.
Overt and covert actions.
It is worth noting that he entertained the idea
of using missiles against labs when he was in office.
Now, I want to set aside the fact
that he's talking about using the US military
in another sovereign nation for a moment.
I want to talk about the way it's being framed
and the headlines, promises to go after the cartels.
That's not the promise.
That isn't the promise.
He's not going to defeat the cartels like this.
That's not what's going to happen.
Capitalists, right-wingers, there's a demand, right?
Inside the United States, there is a demand for this product.
So what happens?
Somebody's going to make it.
If one organization is shut down, another one will pop up.
If that occurs, this military operation
will suffer from mission creep.
It will spread all over the country,
spiraling out of control, triggering the Civil War,
and creating a whole bunch of refugees.
That is what is going to occur.
His promise is not to win.
His promise is to create a bunch of refugees.
And that's it.
And if I ended this video now, undoubtedly
within the next 24 hours, I would
get a message from somebody saying,
Beau, there's no way you could know
how this is going to play out.
Yes, it can, because sometimes past performance does
predict future results.
I'll have a video down below, and it
will detail US military interventions in the countries
where most of the refugees are coming from today.
And what you're going to find out
is that it's not that they can't fix their country.
It's that we can't fix ours, because we
keep making the same mistakes over and over again,
and going down there and messing up their countries.
So this promise and the way it is being framed is ridiculous.
It might be time for the media to stop accepting campaign
promises just as talking points and use that as the headline.
Talk about what is actually going
to occur if this campaign promise is fulfilled.
And the answer is a whole bunch of unnecessary destruction
and chaos, a whole bunch more refugees.
And it won't work.
He won't win, because this war, the plants won.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}