---
title: Let's talk about Republicans still following Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M7L5LvqV4gI) |
| Published | 2023/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how policymaking occurs within the Republican Party, focusing on the influence of Trump's campaign promises despite his weakened position.
- Describes the multiple factions within the Republican Party, particularly those in red states and deep red areas.
- Points out the trap the Republican Party has fallen into by believing vocal supporters on social media represent all voters.
- Notes that Trump controls an energized base that influences policy decisions within the party.
- Outlines the game of red state Republicans to be the most extreme in order to gain social media engagement and win elections.
- Emphasizes that Trump remains a symbol and thought leader for the Republican Party, influencing policy decisions and candidate engagements.
- Mentions that Trump's influence will continue until criminal prosecutions, voluntary exit from political life, or a shift within the Republican Party.
- Stresses the importance of addressing Trump's campaign promises to prevent them from becoming Republican Party policy.

### Quotes

- "Trump controls that energized base. That vocal minority, they are Trump's people."
- "Trump is a symbol for the Republican Party. He is still a thought leader."
- "As much as we all want to, Trump's not out of the game yet."

### Oneliner

Explaining how Trump's influence still shapes Republican Party policy-making and candidate engagements despite his weakened position.

### Audience

Political analysts, Republican voters

### On-the-ground actions from transcript

- Engage in political discourse and analysis to understand the impact of Trump's continued influence (implied).
- Support candidates who prioritize policy decisions based on broader voter interests rather than extreme positions for social media engagement (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the ongoing influence of Trump within the Republican Party and the potential consequences of his campaign promises spreading throughout the party.

### Tags

#RepublicanParty #TrumpInfluence #PolicyMaking #PoliticalAnalysis #CampaignPromises


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about how policymaking
within the Republican Party actually occurs.
Because I had a question,
and I've touched on this subject before this year,
but this was a little bit more specific.
It asked why I was even talking about Trump's campaign promises
when I'm standing here saying constantly that he's weakened,
it's unlikely he's going to hold office again.
And, yeah, that might seem contradictory,
but we have to look at what the Republican Party is right now
and how it gets the rhetoric and policy ideas that shape it.
A lot of it still comes from Trump.
Right now you have multiple factions
within the Republican Party.
To keep it simple right now,
we're just going to talk about those who are in states
where they actually have to run for office
and those that are in deep red areas.
Those that are in deep red areas,
they are very supported on their social media,
and we have talked about it on the channel.
The Republican Party has fallen into this trap
where they believe their most vocal supporters
are reflective of the voters.
They should have learned this after three failed elections,
but they haven't.
They still think that that vocal minority on social media
cheering on the most extreme rhetoric
is representative of the whole.
Trump controls that energized base.
That vocal minority, they are Trump's people.
They love him.
So when he comes out and says,
yeah, we're going to launch military operations into Mexico,
him saying it, it doesn't really matter
because he's not going to be in office again.
That's incredibly unlikely.
So it goes down to them.
They hear it.
They start talking about it.
It shows up in the social media profiles of other candidates,
and those in very red areas know that it resonated.
So they pick up those policy decisions.
Right now, the game of red state Republicans
and Republicans in very secure red areas
is to try to be the most extreme.
That's why you have the governor of South Dakota
taking shots at the governor of Florida right now,
even though they're on the same team.
They are both far-right Republicans.
But being the most extreme,
well, that's how you get that social media engagement.
And that's how, to their way of thinking, you win elections,
even though it damages those Republicans
who aren't in those very safe districts.
So like it or not,
Trump is a symbol for the Republican Party.
He is still a thought leader.
That's why, when that fiasco happened up on Capitol Hill,
there at the House of Representatives,
even though the people who were holding out
and blocking McCarthy,
even though they were doing that against Trump's endorsement,
they threw up a vote for Trump to let everybody know
they're still on his side.
They're still Trump's people
because he's a symbol.
He's the thought leader.
He is that guiding light for the Republican Party.
And he will continue to be that until something changes,
until the criminal prosecutions take hold,
until he decides of his own free will
to leave political life,
until the Republican Party just says,
we've had enough, he's still very influential,
even if he never holds office again.
So when it comes to his vows on campaign issues
and what he's promising to do,
they have to be addressed because they will spread.
It will infect his energized base,
which will infect the red state Republican base.
And over social media, it will infect the red state candidates.
And then you run the risk of it actually becoming
a Republican Party policy.
So as much as we all want to,
Trump's not out of the game yet.
We can't ignore him.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}