---
title: Let's talk about China, the US, the economy, and American exceptionalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1cWti0QOme8) |
| Published | 2023/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concern over China's growing economic power and potential to outstrip the United States.
- Notes that China's GDP is increasing at a faster rate compared to the United States.
- Points out that China having more goods and services exchanged due to its larger population should not be surprising.
- Criticizes American exceptionalism for leading people to believe the US will always lead the world economically.
- Suggests increasing the US population through immigration or reducing income inequality to boost economic power.
- Emphasizes the need to dispel American mythology and embrace critical history.
- Advocates for more cooperation and economic trade rather than isolationism.

### Quotes

- "The idea that the United States will remain the economic superpower of the world while other countries with much larger populations modernize and become economic powers of their own, it's rooted in American mythology."
- "The answer here is not to become more isolationist. The answer here is more cooperation, more economic trade."
- "You mean to tell me that a country more than four times the size, as far as population goes, is going to have more goods and services exchanged? I'm shocked."

### Oneliner

Beau challenges American exceptionalism, pointing out the inevitability of China's economic rise and advocating for cooperation over isolationism.

### Audience

Economic analysts, policymakers.

### On-the-ground actions from transcript

- Increase cooperation and economic trade (implied).
- Advocate for reducing income inequality to boost population growth (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of China's economic rise and challenges American exceptionalism, advocating for critical thinking and cooperation over isolationism.

### Tags

#China #US #EconomicPower #AmericanExceptionalism #Cooperation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about China
in the United States and American exceptionalism
and how something that has become surprising,
some might even say shocking or concerning,
to a lot of people isn't surprising at all.
It shouldn't be.
But because of American exceptionalism, it is.
So there is a lot of talk about China's
growing economic power.
The concern is that someday soon,
China will outstrip the United States
and become that leading economic power.
A good gauge of economic power is the GDP.
Currently, the United States has about $23 trillion
and China is sitting around $17.
But China is increasing at a faster rate.
That's the general trend.
What is GDP?
I mean, other than a really good indicator of economic power.
It is the final number when it comes to goods and services
exchanged.
The idea that China, as it modernizes,
is going to have a higher number,
I don't know how that's surprising.
I don't know why it's shocking.
And most importantly, one of the reasons it's happening
is the policies of the people who
are terrified of it the most.
China's growing economic power is a concern
for all of the US government, but it is particularly scary
for those on the right.
Here's the thing.
Final goods and services, right?
What's the US population?
331 million, something like that.
What's China's?
1.4 billion.
You mean to tell me that a country more than four times
the size, as far as population goes,
is going to have more goods and services exchanged?
I'm shocked.
The only people that should be surprised by this
are people who have bought into the idea
that the United States is somehow special,
that it will always lead the world because it always
has during their lifetime.
They bought into the mythology.
China has a much larger population base.
As it modernizes, yeah, they're going
to have more goods and services exchanged.
That's not a surprise in any way, shape, or form.
And the irony here is that the United States and its GDP
expansion being slower is the fault
of the policies of the people who are scared the most.
Off the top of your head, come up
with two ways to increase the US population real quick.
First is probably immigration, right?
Just let people come.
We'll have more people.
There will be more goods and services exchanged.
Shocking.
Got a feeling the right wing isn't
going to be cool with that.
What's the other option?
They rant about how the US, we're
not having enough babies, right?
You know a real easy fix for that?
Reduce income inequality.
Make sure that those people who right now may not
be having kids because they don't have the money
have more money because then they can have kids.
Incidentally, if they have more disposable income,
they will spend it, which will increase the GDP, which
will increase the United States' economic power.
The idea that the United States will
remain the economic superpower of the world
while other countries with much larger populations
modernize and become economic powers of their own,
it's rooted in American mythology.
It's rooted in American exceptionalism.
And this is why it's dangerous.
This is why you need to get rid of the mythology.
This is why you need critical history so you
can understand stuff like this.
The message that I got that prompted this, I mean,
this person was absolutely terrified, and of course,
blaming me.
But the answer here is not to become more isolationist.
The answer here is more cooperation,
more economic trade.
It's lowering the barriers, not putting them up higher.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}