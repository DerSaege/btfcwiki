---
title: Let's talk about the dreams and realities of change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wIJIrR8nk2A) |
| Published | 2023/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Encourages those seeking systemic change to watch the video.
- Acknowledges the importance of understanding the realities behind dreams.
- Expresses his dream of a world's EMT to improve foreign policy and help those in need.
- Outlines the complex process of actualizing the world's EMT concept.
- Emphasizes the need to transition from dreaming to understanding realities to taking action.
- Explains that his foreign policy videos aim to provide clarity on how things work, moving past moral judgments.
- Shares a personal example of a flawed implementation of a dream without considering the practicalities.
- Stresses the importance of tempering dreams with reality and educating others on effective solutions.
- Urges individuals with causes to understand and communicate the practical applications of their dreams.
- Advocates for a shift from mere dreams to actionable plans grounded in reality.

### Quotes

- "My dream of the world's EMT. I don't want it to stay a dream."
- "The dream has to be tempered with the reality."
- "You have to be able to describe it in waking terms."

### Oneliner

Beau encourages understanding the realities behind dreams, especially in foreign policy, to transition from mere dreaming to impactful action.

### Audience

Advocates for systemic change

### On-the-ground actions from transcript

- Educate others on the practical applications of dreams and causes (implied)
- Advocate for solutions grounded in reality (implied)
- Encourage understanding of the realities behind dreams (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of transitioning from dreams to action through understanding realities, particularly in foreign policy. For a deeper dive, watch the full video. 

### Tags

#SystemicChange #ForeignPolicy #DreamsToAction #RealityVsDreams #Activism


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about dreams
and realities and a question I got. And I think that this is a video that if you are one of those
people who is actively trying to make the world better, if you're one of those people who wants
that deep systemic change and you are out there trying to make it happen, maybe watch this one.
I think that even though this is going to be specific to one topic, I think that it might help
a lot. Beau, I really wish you'd talk more often about your dream of how foreign policy should
work. Your videos about how foreign policy works are very informative and have helped me a lot,
but you're so matter-of-fact about it that it seems like you endorse the current status quo.
I can't imagine how many people saw one of those videos and then never watched another because you
don't condemn it in the only video they watched. They'd only know you don't want that if they see
one of the few that talks about your dream of the world's EMT. Yeah. And this makes sense. It does.
There's a lot of truth in it. My dream of the world's EMT. I don't want it to stay a dream.
I don't want it to stay a dream. I want that to happen. That concept is something I think would
fix a lot of foreign policy and would help a lot of people and it would curtail imperialism. It
would help people who are suffering from famine. It would just be a wonderful thing. It also requires
seeding a force inside the U.S. military, building it, taking that force, establishing a record of
being able to accomplish something, then taking that force, civilianizing it, and building it out
even further and then internationalizing it. There's a lot of moving parts to that and I
want it to happen. I don't want it to stay a dream. When do you dream? I'm gonna get a little
metaphorical here. Most times when you're in the dark, when you are in the dark, slogans,
world's EMT instead of world's policeman, they can get people interested. But if you want people
to be able to advocate for your dream, whatever it is, for your cause, they really have to understand
the realities of it and how it works. You have to take them from dreaming to daydreaming and then to
acting and then you can get somewhere. The foreign policy, this is the way it is videos.
They help people understand those concepts and the way things actually work because most people don't.
Most people try to assign morality and ideology and all of this stuff to foreign policy.
It's not there, not really. You have to get people out of the dream and then you can get them to
daydreaming and then you can get them to actually getting to the cause. And that's why I do it that
way. I'm sure you're right. I'm sure there are people who would love the rest of this channel,
but the first video they watched was one of those foreign policy ones where I'm like,
this is how it is. And they're like, this guy's out there. He's not on my team. Yeah, I mean,
but it's a cost of getting that information out. And I think getting that information out is more
important. I have a real life example of jumping straight from dream to trying to put it into
action. I have a friend, he called me the other day, furious, just absolutely irate because across
the road from his farm, this county road, they're going to put in low income housing. And this is
not like a not in my backyard kind of thing. He's remote. He's more remote than I am. He is way out
there. There is his place. There's two other farms. And there's like a, I think they make the
trailers that people haul motorcycles on. It's a metal fabricating place. Between those places
of employment, there might be 20 jobs if every single person that they hire comes from that low
income housing development. Where's everybody going to work? The nearest like large grouping
of jobs, probably 40 miles in one direction, 50 in another. The dream was they wanted to get low
income housing for people. But they didn't daydream about it. They didn't have enough
information. They didn't really think it through. So they went out and bought the cheapest piece of
dirt they could find, which was out in the middle of nowhere. I don't see how it's really going to
help anybody. I mean, there's probably 80% of the people there are going to have to spend $320,
$400 a month in gas just to get to work. Generally speaking, if you can afford $400 a month in gas,
you probably don't need to live in low income housing. The dream has to be tempered with the
reality. And you've got to get the information out about how it actually works so people
understand how your dream, your solution, whatever it is, no matter how radical it is,
how it can be applied and fix things. So if you are one of those people and you have a pet cause
or two or three, getting the information out about how it works today and letting people see all of
the faults in it, it makes it, it makes them better at carrying that message out to other
people. They don't just have the slogan. They can say, hey, if we did this, this would help here.
This would help in this situation that is happening right now. This politically bankrupt
country that wants help. They don't want the UN because the UN never works and the US could send
in troops, but that's not going to work. This is where we could use that force, but we don't have
it built. This is how we can actually help people and we could change the nature of foreign policy
and how countries interact with each other. But it can't just all be the dream. You have to be
able to describe it in waking terms. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}