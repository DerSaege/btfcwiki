---
title: Let's talk about Abrams approval and timelines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BRZbDwuhw64) |
| Published | 2023/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unofficial reports suggest the United States is considering sending Abrams tanks to Ukraine.
- The delivery mechanism for the tanks is rumored to be through a slower process called a drawdown.
- This method of delivery could provide time to build a beefier logistical network.
- The decision to opt for a slower delivery method could be influenced by policymakers' views on logistics.
- The slower delivery could also allow for Abrams tanks to arrive in a second wave, considering potential destruction in combat.
- The combination of challengers, leopards, Bradleys, and strikers for Ukraine presents a formidable force against Russia.
- The equipment provided by NATO demonstrates resolve in supporting Ukraine.
- The wider war is deemed over, with Russia losing and facing continued pressure until they leave.
- The hope is that the equipment's arrival convinces Russia to withdraw without conflict.
- The ultimate goal is for the equipment to be shipped to Ukraine but never used.

### Quotes

- "The wider war, it's done. Russia lost."
- "Hopefully, with this equipment coming in, it demonstrates pretty clearly that NATO is not going to stop."
- "Wars are fought to achieve other geopolitical goals. Almost immediately, they failed or backfired."

### Oneliner

Unofficial reports suggest the US may send Abrams tanks to Ukraine through a slower drawdown process, aiming to bolster logistics and maintain pressure on Russia until withdrawal.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Support policies that strengthen logistical networks for military aid delivery to conflict zones (implied).
- Advocate for sustained international support to provide necessary equipment to conflict-affected regions (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential delivery of Abrams tanks to Ukraine and the geopolitical implications, offering insights into the ongoing conflict dynamics and potential outcomes.

### Tags

#ForeignPolicy #Geopolitics #MilitaryAid #Russia #Ukraine


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about challengers and leopards
and Abrams.
Oh, my.
We're going to talk about the unofficial news.
So far, it's unofficial.
And we will talk about why it may not
mean what a lot of people think it means as far as speed.
And we're going to talk about what it likely means.
So if you missed it, officials, speaking unofficially
because they're not allowed to officially comment,
have said that the United States, well,
it's kind of opened up to the idea of sending Abrams.
Numbers being tossed around range anywhere
from 30 to 40 of them.
Now, that much is being reported.
What I haven't seen a whole lot about
is the mechanism being used to deliver.
The rumor mill, again, stressing this part's rumor,
is that they're going to use the Ukraine.
If we do it this way, we're going to end up in the weeds.
There are multiple methods of transferring equipment
to Ukraine.
One is called a drawdown.
The United States has the stuff sitting there in a parking lot,
as they do with the Abrams.
And they do a drawdown and just ship it over.
It's a very quick process.
There's another process that takes a little bit more time
to put it together.
Rumor mill says that's the one they're going to use.
So it will take a little bit for the equipment to get there.
Now, why would they do that?
Because there are going to be some people that
are not happy with that news, if that is indeed what comes out.
There are multiple reasons.
First, this gives Germany what it wants.
The United States, Germany, the entire alliance,
holding hands, crossing the street together, and saying,
we're going to provide tanks.
It's just that the United States is also
pulling a wagon behind them.
Tanks are going to take a little bit to get there.
But it provides Germany that reassurance.
It gets the leopards out and gets them moving.
The next reason is all of the logistical stuff
that we've talked about.
And I understand that there is a giant and intense debate going
on right now among tankers, saying,
you know, it's not really that difficult to run
the logistics, to train people to use them.
You know, there's a debate that's occurring.
I don't really want to get into that.
But the one thing I do want to point out
is that people who are having the debate, generally speaking,
they are the tools or former tools of policy.
The people who believe that it needs a beefier
logistical network are the policymakers.
As a former tool of policy, you know what that means.
Your opinion does not matter.
They believe it needs a beefier logistical network.
Therefore, that's how they're going to make their decision.
If they do it this way in the slower delivery time,
it provides time to build that.
So that might have something to do with it.
Then there is the third thing.
Right now, there's a whole bunch of people that are very excited
about the challengers heading over
and the leopards heading over, particularly the leopard,
because, I mean, it was kind of specifically designed
to take out the type of tanks that
are in widespread use there.
But here's the thing.
It's not a defense expo.
When these things get there, they're going to get used.
Some of them will be destroyed.
Having Abrams arriving in a second wave kind of makes sense.
So those are the reasons they might go that route.
They might go the slower delivery route.
They're pretty good reasons, to be honest.
Again, that's a rumor.
It hasn't been officially stated yet, at least not
to my knowledge.
Now, does this mean that Ukraine is
going to be shorthanded in the meantime?
No, no.
The leopards and the challengers will do just fine.
Again, my opinion is that the leopard
is best cut out for this.
So that's the one that it appears they're
going to be getting the most of.
Then you have to take into consideration everything else
they're getting.
And you have to view it all at once.
They're getting challengers, leopards, Bradleys, strikers.
That is a pretty formidable combination of equipment
right there.
And if used in even a halfway decent manner,
it will produce effects that it is incredibly unlikely
the Russian army can stand up to.
Now, from Russia's point of view, what happens now?
Hopefully, they understand.
Hopefully, they understand.
Hopefully, they also see the resolve
that was demonstrated by NATO when it comes
to providing this equipment.
It would be great if Ukraine didn't even
have to use this stuff.
But we don't know that Russia is going to see that.
The war, we talked about it at length on this channel.
The war's over.
Russia lost.
Wars are not fought for the fighting.
Wars are fought to achieve other geopolitical goals.
Almost immediately, they failed or backfired.
The war's over.
Russia lost.
That wider war, it's done.
And it's been done for a while.
Hopefully, with this equipment coming in,
it demonstrates pretty clearly that NATO is not going to stop.
They're not going to stop giving Ukraine what it needs,
which means this will just go on and on and on until Russia
leaves.
Hopefully, that's acknowledged and they just go home.
It would be better for everybody if all of this stuff
got shipped over there and then never used.
But that seems pretty unlikely.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}