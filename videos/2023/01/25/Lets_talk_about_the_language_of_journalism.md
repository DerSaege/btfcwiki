---
title: Let's talk about the language of journalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t5C3NaNDO6E) |
| Published | 2023/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why the word "alleged" is frequently used in journalism to protect outlets from being sued.
- Mentions that the absence of certain words like "alleged" can signify commentary rather than straight reporting.
- Shares a personal experience of using the term "alleged" in reporting and the editor's explanation on its necessity.
- Talks about passive language in journalism, such as using "death of" instead of "murder of," until legally confirmed.
- Recounts a friend getting criticized for using "death of" in reporting due to legal and journalistic considerations.
- Advises to think from a legal perspective when critiquing journalists' choice of terminology like "murder" or "killed."
- Mentions a case in Memphis that will likely stir strong emotions once the footage is released, warning viewers to brace themselves.

### Quotes

- "If somewhere else in that passage of that article, whatever, blame seems to be implied, oh, that's alleged."
- "That's why you hear that word so much."
- "Until they're convicted, they're an alleged armed robber."
- "Be ready for it. When the footage comes out, just kind of brace yourself."
- "The city government up there is already trying to distance itself from the media."

### Oneliner

The importance of journalistic terminology like "alleged" and the impact of legal perspective on reporting, with a warning about an upcoming disturbing footage.

### Audience

Journalists, Reporters, Activists

### On-the-ground actions from transcript

- Brace yourself for the disturbing footage release (implied)

### Whats missing in summary

Importance of understanding journalistic terminology and legal perspectives in reporting.

### Tags

#Journalism #Media #LegalPerspective #Ethics #Memphis


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the language of journalism
and why some words get used a whole bunch, some would say overused,
and why some words are completely avoided and almost never get used.
And we're going to do this because there have been a couple of questions about it,
but I feel like this is going to become really important as a story out of Memphis
really enters the news cycle because you're going to see a lot of this.
So the first word we're going to talk about is the word alleged.
It shows up a lot and sometimes it doesn't even make sense.
You get weird sentences, the alleged shooting.
Now, I mean, somebody's got a bullet in them.
We know there was a shooting.
But if somewhere else in that passage of that article, whatever,
blame seems to be implied, oh, that's alleged.
It's alleged. It's a legal thing.
It protects the outlet from getting sued.
Now, we are talking about straight reporting right now.
We're not talking about commentators.
In fact, when you don't hear these words,
that's a clear indication that you are listening to a commentator,
an opinion piece and something that should be treated as opinion rather than fact.
Those words are really important.
When I was a baby journalist first starting out,
I wound up having one of those sentences, the alleged shooting.
And I had a back and forth with my editor about it.
And I'm like, it's not alleged. It's on film.
And they went through and explained the whole thing to me.
And the part that will always stick in my mind is something to the effect of,
I don't care if the person is robbing a bank on film without a mask,
holding their driver's license and then signs a confession.
Until they're convicted, they're an alleged armed robber.
And that's pretty much the standard.
And then there was a follow on joke about an appeal
where things that weren't alleged become alleged again
if it goes back through an appeal process.
So that's why you hear that word so much.
And there are other terms that can be used.
Appears. It appears to be this.
It is something that indicates this is what the journalist saw on the film.
And you may hear that phrase as well.
The person is visible in the film doing X.
It's another way to kind of shield the outlet.
Now, another thing is passive language and headlines in particular.
First thing to know is that oftentimes the journalist,
the person that wrote the article, they may not have written the headline.
That may be an entirely different person.
But you'll often see the death of somebody
rather than the murder of somebody.
Until there is a death certificate with homicide,
you really shouldn't even say killing.
To go to murder, oh, no, no, no.
That's a legal thing.
Again, that has to be found in a court before you say that.
Or you have to have an official statement from a cop saying
they're investigating it as a murder before you can use that word.
I, not too recently, but not too long ago,
I had a friend that I just watched get trashed on social media
over the phrasing of the death of.
And to me, it was funny because I know this person.
We've had lengthy conversations about this topic,
about law enforcement through the lens of race
and how different departments treat people.
Had huge conversations about it.
If there was audio of it, somebody had taped it,
it would be incredibly lengthy, like 13 minutes and 12 seconds long.
And it would be nothing but both of us trashing certain departments.
And this is a person who is very aware and actually
holds kind of radical opinions when it comes to this topic.
But they work for an outlet that is straight reporting.
So they can't let it come through in what they write.
There are other situations that are similar,
where they will use alleged or they'll use terminology that seems very passive.
Sometimes it is an attempt to downplay.
Sometimes it's a legal thing.
So before you trash somebody over it,
think about it from a legal standpoint.
Like the word murder, even the word killed,
those are terms that you've got to be very careful with.
And you have to think of it from the legal perspective of not what you see
and what you feel morally, but what happens in a courtroom.
Because if it's justified, well, it can't be murder.
And then I want you to think about some of the things that have been deemed justified
by juries in this country.
That's what people have to go on.
That's what journalists have to use as kind of a marker of when they can use language.
So take it easy on them.
There's a case out of Memphis that I have not made a video about,
but I've been following about it and I have talked about it on social media.
Be ready.
Be ready for it.
When the footage comes out, just kind of brace yourself.
I'll wait to see the footage to put out a video on it,
but based solely on the actions of agencies within the government there,
this footage is going to be really bad.
It is going to be really bad.
There are going to be a lot of emotions that are going to be running high.
And this is just based prior to nationwide outrage,
prior to the video being released.
The city government up there is already trying to distance itself from the media.
Distance themselves, cover bases.
It's going to be bad and they know it.
So just be ready for it.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}