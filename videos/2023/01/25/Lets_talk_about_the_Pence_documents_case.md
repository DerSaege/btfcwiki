---
title: Let's talk about the Pence documents case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JuqaSQoH7s8) |
| Published | 2023/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pence's lawyer found documents with classification markings in boxes that had been moved from the White House to temporary and then permanent residence.
- The documents were secured by contacting the National Archives, who then involved the National Security Division of the FBI.
- Pence's actions didn't seem to indicate any wrongdoing, and proper steps were taken once the documents were discovered.
- Comparisons are made between Pence's case and Biden's, with emphasis on Pence's cooperation and lack of criminal aspects.
- The issue revolves around national security implications rather than criminal liability.
- The importance of "willful retention" in potential criminal aspects is emphasized, contrasting with national security concerns.
- The broadcast of this incident to foreign intelligence agencies poses significant national security risks.
- Suggestions are made for implementing a counterintelligence filter team during transitions to prevent such incidents.
- Support for this initiative is deemed bipartisan as it serves both national security interests and prevents potential scandals.
- Emphasis is placed on the need for cooperation, speed of document return, and awareness by individuals in handling such sensitive documents.

### Quotes

- "None of this is good."
- "Counterintelligence filter teams, they need to be a thing."
- "It cannot continue to happen."
- "That's the difference, and that's what's going to matter."
- "Y'all have a good day."

### Oneliner

Pence's document mishap underscores national security risks, stressing the need for accountability and preventive measures like counterintelligence filter teams.

### Audience

Government officials, policymakers

### On-the-ground actions from transcript

- Establish counterintelligence filter teams during transitions (suggested)
- Ensure proper handling and awareness of sensitive documents (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the Pence document case, offering insights into national security implications and the need for proactive measures to prevent similar incidents in the future.

### Tags

#NationalSecurity #PenceDocumentCase #Counterintelligence #PreventiveMeasures #GovernmentOfficials


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the Pence document case.
Andre, you're telling me you've lost another set of documents?
I feel like the guy from the Hunt for Red October at this point.
OK, so what happened?
It appears that Pence's lawyer, probably
doing like a proactive, preemptive check
to make sure that Pence didn't have any documents,
you know, attempting to preserve national security
and protect the interests of their clients.
They did a search.
They're looking through Pence's stuff.
They find a couple of boxes.
Boxes are taped up.
They have been moved twice, once from the White House
to a temporary residence, and then
from the temporary residence to his permanent residence.
They open them up.
They find documents bearing classification markings.
This is what happens now is what matters.
They contacted the National Archives, said,
hey, we've got this stuff and don't want to have it,
and then they secured it.
The archives then contacted the National Security Division
of the FBI.
Documents are no longer loose.
OK, so Pence is a Republican.
He obviously did something wrong.
Nope, doesn't look like it.
Doesn't look like it.
What matters is what they do once they
become aware of the documents.
This is pretty comparable to the Biden case.
Now, personally, I would prefer that if something
like this happens, they just jump right to the end
and contact the National Security Division of the FBI.
But I don't think that's a rule.
I think that's just it just makes more sense to me.
It doesn't look like Pence did anything wrong.
It doesn't look like there was any real issue
with how that was handled.
So what happens next for him?
Odds are his lawyers are going to conduct their own search
and try to make sure that there's nothing else out there.
And then probably there will be a checking your work search
by the Department of Justice.
They'll go in and look with the full cooperation of Pence.
And this is where you can see the lines, the cooperation
part of it.
That's the difference between Biden and Pence and Trump,
because you need to get ready for more false equivalencies.
The media is going to try to say, look, they all have it.
Trump did nothing wrong.
Ending up with classified documents accidentally
isn't actually the issue when it comes to criminal liability.
It's a national security issue, and we'll get to that.
But that's not a criminal aspect of it.
If you go back to the original videos
where we're talking about the Trump case,
prior to any of the news about Biden or Pence breaking,
you continually hear the phrase willful retention.
I don't feel like I did a great job of explaining
how important the difference is back then,
because I didn't know that documents were going
to start showing up everywhere.
But that phrase shows up over and over again,
willful retention.
And I even start making jokes about it,
where if they say, hey, you've got these documents,
and then you say you don't, or you say they're mine,
or you say you looked and they don't exist,
that might be considered willfully retaining them.
That's actually the problem.
It's literally what the law is called.
So they are not the same as far as the criminal aspects,
potential criminal aspects.
When it comes to the national security aspects,
this is all bad.
This is all bad.
None of this is good.
What has been broadcast to every foreign intelligence agency
on the planet is that an office or former home
or whatever of a former president or vice president
is someplace you might find stuff that you want to see.
That's not good.
That's not good.
So you're going to see those comparisons made
between Pence and Biden.
Those comparisons are pretty good.
Comparisons between Pence, Biden, and Trump, not so much.
It doesn't hold up.
And you can go back to when before news broke
and see the difference.
So what needs to happen next?
There needs to be a counterintelligence filter team
that is at the White House during the transitions.
That much is evident at this point.
This is bad.
This shouldn't be happening.
It shouldn't happen at all.
It certainly shouldn't happen on this scale.
The counterintelligence community
needs to think about it like this.
You have a division HQ in the Army,
division headquarters,
and every time there's a change of command,
a bunch of documents end up out in the wild.
Are you going to continue to do things the same way?
No, of course not.
That's what needs to happen here.
And for those who heard it,
there's a podcast, Opening Arguments.
I'll try to find a link and put it down below,
where the experts on this,
like the people who this is what they live, eat,
and breathe is national security law,
totally co-signed to that idea.
They're like, this is how we need to do it.
It would prevent issues like this from occurring.
Now, there should be support for this
on a bipartisan nature at this point,
because we do know that a lot of politicians
don't actually care about national security,
but they do care about scandals.
And while it might be annoying
to have everything that you designate as personal
as a president or vice president,
it might be annoying to have some counterintelligence person
go through all of it,
it would stop this in the future.
It would stop your embarrassment,
your potential embarrassment,
your party's embarrassment.
And that's on top of maybe saving lives.
So this is going to continue.
You're going to hear more about it.
My guess is that every former president
and vice president's legal team right now
is going through all of their stuff.
So you're probably gonna hear about more showing up.
The key element here, the key difference,
and the part you have to remember
is the level of cooperation
and the speed at which the documents were returned
and how they were returned.
It's also really important to remember
that in the Pence and Biden cases,
it doesn't look like the government
was actually aware these documents were missing.
So there isn't a call from the government saying,
hey, give us this stuff back.
They just did it on their own.
That's the difference,
and that's what's going to matter.
Counterintelligence filter teams,
they need to be a thing.
This cannot continue to happen.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}