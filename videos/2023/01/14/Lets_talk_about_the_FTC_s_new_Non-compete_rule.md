---
title: Let's talk about the FTC's new Non-compete rule....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VJ9YqgFTMCc) |
| Published | 2023/01/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- FTC rule eliminates non-compete clauses, a win for labor.
- Expect legal challenges to narrow the rule's scope.
- Less than 1% chance the rule will stand as is.
- Possible argument: FTC overstepped its authority by creating a new law.
- Administration likely has a response to the separation of powers issue.
- Predicts large portions of the rule will stand, still a win for labor.
- Deep-pocketed interests will challenge the rule in court.
- Anticipates a lot of news coverage on challenges to the rule.

### Quotes

- "Non-compete clauses have never made sense to me."
- "I don't think an employer should be able to limit where you work in the future."
- "Expect to hear a lot of news about challenges to this."

### Oneliner

FTC's rule eliminating non-compete clauses faces legal challenges and questions of authority, with predictions of narrowing but still a win for labor.

### Audience

Labor advocates, policymakers.

### On-the-ground actions from transcript

- Stay informed on developments regarding the FTC rule and legal challenges (implied).
- Support organizations advocating for labor rights and fair employment practices (implied).

### Whats missing in summary

Deeper analysis on the potential impacts of the FTC rule changes and the long-term effects on labor rights and employment practices. 

### Tags

#FTC #NonCompeteClauses #LaborRights #LegalChallenges #EmploymentPractices


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the FTC
and non-compete clauses and expectations for the future.
I should probably start off by saying, I think this is great.
The news, I think the news is great.
The FTC kind of put out a rule that basically got rid of,
I mean, pretty much all non-compete clauses.
Non-compete clauses have never made sense to me.
I don't think an employer should be able to limit
where you work in the future.
There are very, very few situations
in which that makes sense.
So it's a big win for labor.
That being said, don't expect this rule
to stand the way that it does.
It is super broad, which is good.
But it's also incredibly broad to the point
that there will definitely be challenges, and some of them
are going to be successful.
This rule will be narrowed in scope
as the legal challenges move forward.
That's a guarantee.
I give it less than a 1% chance of standing the way it is.
It's, they tried to hand labor a big win,
but the courts are definitely going to have their say here.
And then there's also another issue
when it comes to a constitutional aspect
of the FTC doing this.
It is probably going to be argued that the FTC created a law,
that they don't really have the authority to do this,
that they've overstepped their bounds.
And since it is a new law, well, it should have originated
with Congress.
And since Congress didn't do it,
it happened in the executive branch under the FTC.
Well, it's a violation of the separation of powers
and will be struck down, should be struck down.
That's how they're going to argue it.
I have to believe that the administration has an answer
to that argument.
I don't know what it is,
but I've got to believe it's there somewhere.
I don't think that they would just create,
attempt to create a new law out of whole cloth
the way it's definitely going to be argued in the future.
I expect large portions of it to stand,
so it's still a win for labor,
but I don't see it standing the way it is.
It is very broad and it's very vague.
And there are a lot of deep pockets
who are very interested in making sure
that their employees feel like they don't have a choice.
They have to stay with them because they can't go somewhere else
and work in their chosen field.
And those moneyed interests will definitely be taken this to court.
So expect to hear a lot of news about challenges to this.
Anyway, it's just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}