---
title: Let's talk about two types of nationalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wI-BjA9StsA) |
| Published | 2023/01/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about nationalism and the different kinds of nationalism.
- Mentions how people have pointed to Ireland as an example of nationalism used for good.
- Explains the distinction between nationalism as a tool and nationalism as an ideology.
- Describes how nationalism was used by Connolly and Pearse in Ireland in 1916.
- Points out that nationalism becomes an ideology when the nation already exists.
- Addresses the negative impacts of nationalism on the economy, environment, diplomacy, and domestic situations.
- Emphasizes that nationalism is good for motivating people for war.
- Explains how nationalism can morph into fascism when internal enemies are sought.
- Criticizes nationalism as politics for people who require a leader to tell them what to do.
- Warns about the dangers of nationalism as an ideology leading to perpetual war.
- Talks about the prevalence of nationalism in the US and its potential dangers.
- Connects the push for nationalism in the US to political movements like Trump's.

### Quotes

- "Nationalism is good for war, which is useful if you're trying to achieve a nation."
- "Nationalism is politics for basic people. People who really require a leader."
- "Nationalism in service of throwing off a colonial yoke. Yeah, I get it. It's a tool."

### Oneliner

Beau explains the dangers of nationalism as an ideology leading to perpetual war and the prevalence of nationalism in the US.

### Audience

Activists, policymakers, educators.

### On-the-ground actions from transcript

- Analyze and challenge nationalist rhetoric in your community (suggested).
- Educate others on the dangers of nationalism turning into fascism (suggested).

### Whats missing in summary

In-depth analysis of the historical and contemporary implications of nationalism and its potential for both positive and negative outcomes.

### Tags

#Nationalism #Ireland #Fascism #US #PoliticalMovements


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about nationalism.
What it is and the different kinds of nationalism.
I think most people have noticed that when I talk about nationalism,
I have a very discernible disdain in my tone.
And lately, over the last couple of weeks,
I've had a number of people send messages talking about how nationalism can be used for good.
Most pointed to Ireland as an example.
And don't think for a second that I don't understand.
Y'all are just, you know, playing on my heritage and trying to get my feelings and all of that.
But it's okay. It's fair play.
And it also makes this video easier because I don't have to do any research.
It's a good example to talk about the two kinds of nationalism.
When you are talking about Ireland in 1916, yeah, the people who signed the proclamation,
they were nationalists, yes.
They wanted to achieve a nation and therefore they were nationalists.
But was that their ideology?
Or was it just simply a tool they were using to achieve nationhood?
When you look at the proclamation and you read through it, what do you see?
You see the word equality a lot.
You see a lot of talk about equality, universal suffrage.
You see language about how the Irish people will own everything.
There's a lot of fingerprints of a different ideology in that proclamation.
And that makes sense because Connolly and Pearse, they were socialists.
Nationalism wasn't the ideology.
It was a tool that they used to motivate people to want to achieve nationhood.
You can see this play out with colonized people all over the world.
It was a tool, but not the ideology.
It wasn't the philosophy that the government was going to be founded on.
Nationalism as an ideology occurs when the nation already exists.
The nation has already been achieved.
And now people just want everything in service of the state.
The nation becomes supreme.
Is it particularly good for the economy?
Nationalism as an ideology?
No.
The environment?
No.
Diplomacy?
No.
The domestic situation inside the country?
Not historically.
Nationalism, what is it good for?
War.
Absolutely nothing.
War. It's good for war.
Motivates people.
Creates that imagery and that attachment to flags and songs and paintings of men marching in step toward slaughter.
It creates that imagery and that bond that motivates people to give themselves or their children up to the grinder.
Nationalism is good for war, which is useful if you're trying to achieve a nation.
But if that nation already exists, what does it become when nationalism becomes the ideology?
It becomes a warmongering state.
So what if there aren't any external enemies to fight?
What happens then?
You have to carry on the fight somehow.
So you find those internal enemies.
You find those scapegoats.
And this is where nationalism morphs very quickly into fascism.
Because the nationalist wants to make their nation strong and get rid of the weak and all of that stuff.
All the stuff you see in the rhetoric.
Nationalism is politics for basic people.
People who really require a leader.
Somebody to tell them what to do and what to believe.
That's what nationalism is.
Its use is war.
It doesn't really have any use outside of that.
A nation that accepts nationalism as its ideology is doomed to near perpetual war.
Because it's the only way that ideology can be satisfied.
Nationalism in the sense of a desire to achieve a nation, well that's something else.
Because you can have nationalism that wants to achieve a very equal society.
You can have nationalism that wants to create a peaceful society with a different ideology.
That kind of nationalism is a tool to achieve something.
Not a philosophy or an ideology in and of itself.
There's two kinds.
The thing about this in the United States is that nationalism is very prevalent in the US.
It always has been.
But as it becomes more and more refined and more and more pervasive throughout all of society,
we become that war mongering state.
We become that entity that is always looking for enemies.
And if the US does not find them outside, it will find them inside.
And that is what people, Trump and his ilk, that's what they tapped into.
That push for nationalism that has been an undercurrent of American philosophy for so long.
They just drilled down until they found it.
Once it broke through, well it was political gold.
And it motivated their entire movement.
It's nationalism.
And as they search for more and more internal enemies, it morphs.
It becomes even more dangerous.
Nationalism in service of throwing off a colonial yoke.
Yeah, I get it.
It's a tool.
Nationalism as an ideology, a philosophy, everything in service of the state,
to comply and always do what your betters tell you.
No, no.
That's a recipe for tyranny.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}