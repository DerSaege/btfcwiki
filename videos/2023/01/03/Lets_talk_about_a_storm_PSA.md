---
title: Let's talk about a storm PSA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QLz72jlZDPQ) |
| Published | 2023/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Half the country is facing rough weather, be prepared for tornadoes.
- Areas in California experiencing flooding should prepare for more with another atmospheric river coming.
- Concerns about flooding around the Russian River are high.
- Avoid going to your attic during flooding as it's not a safe place to seek shelter.
- Have a plan in place and know what to do if your area gets flooded.
- If you must go to the attic, ensure you have a way out by creating an exit on the roof.
- Don't be someone's nightmare in the aftermath of a disaster, make sure you have a way out.
- People often end up in the attic due to someone in the home unable to achieve an alternative.
- Ensure you have necessary supplies like batteries and water ready for severe weather conditions.
- Utilize advanced weather warnings to be prepared and not caught off guard.

### Quotes

- "Your attic isn't a good place to go."
- "Don't be somebody's nightmares for the next five years."
- "Make sure you have a way out."

### Oneliner

Beau stresses the importance of being prepared for severe weather conditions, advising against seeking shelter in the attic and ensuring you have a way out to avoid being trapped.

### Audience

Community members in areas prone to severe weather.

### On-the-ground actions from transcript

- Prepare an emergency kit with batteries, water, and essentials in case of severe weather (implied).
- Familiarize yourself with your area's evacuation routes and shelters (implied).
- Stay informed about weather updates and warnings to take necessary precautions (implied).

### Whats missing in summary

Detailed instructions on creating an exit from the attic in case of flooding.

### Tags

#WeatherPreparedness #EmergencyPreparedness #FloodSafety #SevereWeather #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about the weather.
Not something we normally talk about,
but it seems appropriate right now.
Basically like half the country is in for rough weather.
So if you are in tornado country, be ready for tornadoes.
Get your stuff together, have your plan,
put together what you normally would do in these situations
because it appears that there are going to be some coming.
In California, in the areas where you're already
starting to see flooding, it appears that there's going
to be another atmospheric river coming
and kind of hitting the same place.
There is the concern of more flooding,
particularly around the Russian River.
So just something to bear in mind.
Get your plan together, figure out what you're going to do
if your area gets flooded.
You know, this is hitting areas that aren't really used
to it in a lot of places.
One thing I want to stress,
your attic isn't a good place to go.
If you have basically any other alternative,
use the alternative.
There are a lot of people who when water start to rise,
they go into their attic to stay dry.
Once you get up there, there's not really a way out.
So if you go, if you cannot find any other alternative,
before you head up into your attic, take a pry bar,
an ax, a chainsaw, a sawzall, a sledgehammer,
whatever it's going to take for you to make a hole
in your roof so you can get out.
On behalf of a whole bunch of people
who have done relief and recovery work
after hurricanes and flooding down here on the Gulf Coast,
don't be somebody's nightmares for the next five years.
Make sure you have a way out.
Again, I would strongly advise
doing pretty much anything else.
You don't want to get trapped up there.
And generally speaking, the reason people elect to go there
has to do with somebody in the house or in the home
that can't do whatever it would take
to achieve the alternative.
And most times it just makes the eventual discovery
even more tragic.
So make sure if you have to go into your attic,
you have a way out.
So for everybody else, get your plans together.
You hopefully know what to do.
Make sure you have your batteries,
make sure you have your water, everything that you need,
and be ready for it.
You know, one of the amazing things
about all of our weather monitoring
is that we get advanced warning that this could happen.
Take advantage of it.
Don't let it surprise you
and don't let it catch you off guard.
So just a friendly PSA this morning,
get your stuff together and be ready for it.
It looks like it's impacting,
basically there's bad weather from Georgia to California.
So take a look at the weather maps today
if it's not something you normally look at.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}