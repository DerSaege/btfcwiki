---
title: Let's talk about what McCarthy knows that you should too....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cTlYWa1Zn-o) |
| Published | 2023/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Representative McCarthy made promises and concessions to become Speaker of the House, including agreeing to a vote of no confidence by five Republicans.
- McCarthy also promised to effectively destroy the Office of Congressional Ethics by implementing hiring rules that hinder the office's functioning.
- By making these promises, McCarthy aimed to cater to unethical individuals who might come under investigation by the Office of Congressional Ethics.
- This move reveals how some Republicans in the US House are willing to undermine ethical oversight to protect themselves.
- McCarthy understood that destroying oversight was not a deal breaker for many Republicans and wouldn't cost him votes.
- This showcases the Republican Party's prioritization of party loyalty over accountability and ethical oversight.
- The willingness of some Republicans to look the other way regarding accountability issues is a clear example of putting party over country.
- McCarthy's promise to the extreme wing of the Republican Party did not deter those trying to appear as moderates to the media.
- The situation exemplifies how some politicians may not directly lie but choose to ignore or overlook unethical actions within their party.
- This reveals a deep-rooted issue within the Republican Party regarding accountability and ethical standards.

### Quotes

- "Destroying the oversight, the ethical oversight for the US House of Representatives is not a deal breaker for the rest of the Republican Party."
- "When you want to talk about putting party over country, this is the clearest example you're ever going to get."
- "The truth was never told during office hours."
- "They'll look the other way when the people in their party do."
- "It's just a thought. Y'all have a good day."

### Oneliner

Representative McCarthy's promises reveal the Republican Party's prioritization of loyalty over ethical oversight, putting party interests ahead of accountability and truth.

### Audience

US voters

### On-the-ground actions from transcript

- Hold elected officials accountable for prioritizing party loyalty over ethical oversight (implied).
- Support candidates who prioritize accountability and transparency within their party (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how Representative McCarthy's promises expose a concerning trend within the Republican Party, showcasing the prioritization of loyalty over accountability and ethical standards.

### Tags

#RepublicanParty #EthicalOversight #Accountability #PartyLoyalty #USPolitics


## Transcript
Well, howdy there, internet people.
It's Beau Young.
So today we are going to talk about something
that Representative McCarthy knows about Republicans
in the US House of Representatives
that I think the rest of the United States
needs to know as well.
In his attempt to get the votes
to become Speaker of the House,
he made a lot of promises.
He made a lot of concessions.
And we've talked about some of them on the channel,
the big one being that he agreed
that any five Republicans could come together
and call for a vote of no confidence in him.
But I think one that is more revealing
as far as to the character of Republicans in the US House
is his promise to basically destroy
the Office of Congressional Ethics.
He promised to institute rules
that would kind of make it impossible
for them to hire anybody.
They had to do all of their hiring
within the first 30 days,
and anybody who has ever worked for the federal government
will be able to tell you,
it takes a long time to get hired.
So basically guts it,
but it doesn't look like he's destroying the office.
It's a smart move.
If you want to cater to those who are unethical,
who might be investigated by this office,
if you want their votes,
this is a way to get them.
There are a number of Republicans in the US House
who are more than likely would have been investigated
by the Office of Congressional Ethics
for their alleged role in what happened on January 6th.
So it seems like that's McCarthy catering to them,
and it is, no doubt.
That is definitely the appearance.
It makes sense,
but he knows something about the other Republicans
that everybody needs to understand.
Destroying the oversight,
the ethical oversight for the US House of Representatives
is not a deal breaker for the rest of the Republican Party.
He knew he could make this promise
to those in the MAGA faithful,
and it wouldn't upset the so-called moderates.
It shows the extent to which Republicans in the US House,
to this day, are still willing to attempt to hide
what really occurred on January 6th.
It's the attempt to stonewall any investigation into it,
and he knew the rest of the Republican Party
would go along with it.
He wasn't worried about losing votes over this.
He needed every vote he could get,
and he knew that by making this promise,
he wouldn't lose one.
That should tell you a whole lot about who the Republican Party is,
who they have in the US House,
people who would gladly look the other way
when it comes to removing accountability,
removing even the prospect of any kind of oversight
from their colleagues.
When you want to talk about putting party over country,
this is the clearest example you're ever going to get.
McCarthy knew this promise to the extreme wing
of the Republican Party would not cost him votes
among those that like to portray themselves
as moderates to the press.
And remember that portray themselves part.
Hunter S. Thompson believed that the truth was never told
during office hours.
I have to agree.
And those who may not lie,
they'll look the other way when the people in their party do.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}