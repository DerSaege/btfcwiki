---
title: Let's talk about Biden, negotiations, and the briar patch....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CMwWxL3jJD4) |
| Published | 2023/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Inflation Reduction Act, pointing out significant wins.
- Criticizes Biden administration for not adequately promoting the biggest win in healthcare.
- Emphasizes that the major healthcare win involves Medicare negotiating with pharmaceutical companies.
- Compares Medicare negotiation to dealing with Vito Corleone from "The Godfather."
- Describes the process where Health and Human Services sets prices, and pharmaceutical companies can counteroffer but may face hefty taxes if they don't comply.
- Addresses concerns about pharmaceutical companies shifting costs onto others.
- Analyzes the strategy behind changing societal thought regarding Medicare for all.
- Comments on the lack of legislative priority for expansive healthcare despite widespread support.
- Speculates on the potential shift in public perception if pharmaceutical companies raise prices.
- Advises not to negotiate with "Dark Brandon" and makes a reference to 4D chess in politics.

### Quotes

- "But to me, that's not the biggest."
- "I mean, yeah, it's a negotiation, the same way you negotiate with Vito Corleone."
- "Oh no, don't throw me into the briar patch."
- "To change society, you have to change thought first, not the law."
- "This is what it actually looks like."

### Oneliner

Beau explains the key healthcare win in the Inflation Reduction Act involving Medicare's negotiation power with pharmaceutical companies, likening it to dealing with organized crime.

### Audience

Healthcare advocates

### On-the-ground actions from transcript

- Support legislative efforts for Medicare negotiation with pharmaceutical companies (implied)
- Advocate for healthcare reform and increased support for Medicare for all (implied)

### Whats missing in summary

Detailed analysis of the potential impact of Medicare negotiation on healthcare costs and access.

### Tags

#Healthcare #Medicare #Negotiation #PharmaceuticalCompanies #InflationReductionAct


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Biden
and negotiations and the Breyer patch.
So inside the Inflation Reduction Act,
there are a lot of wins, there really are.
But by my way of thinking,
most people don't know about the biggest win.
And that's kind of the Biden administration's fault,
to be honest, because they are out there
touting that the cap on insulin,
the cap on the cost on insulin,
they are out there talking about that constantly,
and I get it, it's a huge deal for a lot of people,
and it's immediate.
And they're talking about how the pharmaceutical companies
can now basically only raise their prices
like in accordance with inflation and all of that stuff.
And yeah, that's good too.
But to me, that's not the biggest.
These aren't the real wins as far as healthcare
in that act.
The big one has to do with Medicare being able to negotiate
with the pharmaceutical companies over the cost of meds.
You've probably heard it mentioned offhand,
be able to negotiate prices.
I mean, yeah, it's a negotiation,
the same way you negotiate with Vito Corleone.
They're gonna make you an offer,
and if you don't like it,
well, you're gonna meet my friend over here.
And that's kind of how it works.
The dynamics of this, as it was explained to me,
was Health and Human Services running Medicare,
they say, I'll pay this much for these meds.
And the pharmaceutical company can make a counteroffer,
but Health and Human Services doesn't have to go for it.
At which point, the pharmaceutical company
just kind of has to play ball.
And if they don't, their friend, Erwin Roger Stevens,
the IRS, shows up and hits them with a baseball bat
made of up to 1,900% excise tax.
I mean, that's not exactly a negotiation.
For those who've been waiting for dark Brandon
to come to life, this is as close as you're gonna get.
And don't get me wrong, I am not about to start,
talking about the poor, poor, pitiful
pharmaceutical companies.
I wanted to lay out the dynamics
because I got a message about this.
Somebody's dad said that if this went into effect,
that the pharmaceutical companies
are just gonna charge Medicare one price
and jack up the price on everybody else.
Oh no, don't throw me into the briar patch.
I'm pretty sure that's actually the plan.
To change society, you have to change thought first,
not the law.
To change society, you have to change thought.
Why don't we have Medicare for all?
And right now the lefties are down there
just typing furiously, saying that it's,
you know, expansive healthcare is widely supported
and all of that stuff.
And it's true, it's true.
The problem is people support it
the way they support schools.
They sit there and they say, yeah,
I want the best schools, you know,
anywhere in my county, in my jurisdiction, whatever.
Half a cent increase on sales tax?
No way, I'm not paying for it.
Show up voluntarily to the fundraiser?
Nope.
That's how they support it.
Because the support for it isn't just overwhelming,
it doesn't become a legislative priority.
So it doesn't really move, doesn't go anywhere.
So what happens if a capitalist entity,
like a pharmaceutical company,
decides to foist the cost onto everybody anyway?
I would imagine there would be a shift in thought
because it would change from,
I'm not paying for that,
to I'm already paying for that
and the pharmaceutical company's profit.
Yeah, I actually think that might be part
of why they're doing it this way.
I don't know that, that's a guess.
But that's how I read it.
So a couple of things for everybody to take away from this.
While the Republican Party screams
there's nothing in the Inflation Reduction Act
dealing with inflation,
just understand they're wrong.
Another thing to take away from this is
do not negotiate with with Dark Brandon.
A third thing to take away from this,
if you are one of those people who supports Trump
but watches this channel,
y'all remember when y'all were talking about 4D chess?
This is what it actually looks like.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}