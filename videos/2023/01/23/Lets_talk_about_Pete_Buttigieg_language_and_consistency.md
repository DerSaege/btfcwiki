---
title: Let's talk about Pete Buttigieg, language, and consistency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C53FHXqq2wM) |
| Published | 2023/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the request for basic courtesy and respect in addressing individuals based on their title, name, and pronouns.
- Describes the common response of "I just don't get it" when discussing gender identity and pronouns.
- Notes a shift in social conservatives' response towards Pete Buttigieg and his husband regarding the term "husband."
- Questions the inconsistency in social conservatives' argument for using gender-neutral terms like "partner" instead of "husband."
- Points out the underlying motivation of bigotry and discrimination rather than genuine lack of understanding.
- Emphasizes the celebration of title changes as reflective of personal growth and identity.

### Quotes

- "They want to be addressed in a way that reflects them."
- "It's not that you didn't understand. It's that you didn't want to."
- "People go through title changes all the time, and it normally marks the growth of that person."
- "You'll feel better about your lot in life if you can kick down at somebody."
- "It's that somebody had told them, you can other somebody this way."

### Oneliner

Beau explains the resistance to using gender-neutral terms and addresses the underlying bigotry in social conservatives' response to Pete Buttigieg's marriage.

### Audience

All individuals

### On-the-ground actions from transcript

- Support and respect individuals by addressing them using their preferred title, name, and pronouns (implied)
- Challenge discriminatory attitudes and behaviors towards marginalized communities (implied)

### Whats missing in summary

The full transcript expands on the importance of respecting individuals' identities and the negative impact of bigotry on personal growth.

### Tags

#GenderIdentity #Respect #Bigotry #SocialConservatives #TitleChanges


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about language
and the use of language and how language changes
and tradition and failing to understand a concept
in Pete Buttigieg because something has changed
and I think it's worth noting.
For a long time, there's been a group of people
who have basically been asking for a bare minimum amount
of courtesy and respect, which in today's age
is just a giant ask, I guess.
They want to be addressed in a way that reflects them.
That's it.
When it comes to their title, their name, their pronouns,
that's all they're asking for, just this bare minimum amount
of courtesy and respect.
And I have a whole bunch of videos explaining
the difference between biology and gender
and explaining how easy this is.
The thing is, at the end of it, with a lot of people
that you're trying to talk to about this subject,
what you end up with is somebody saying,
well, I just don't get it.
And that's it.
They sit there and they say, I don't understand.
I just don't get it.
And when you're approaching something
from an academic standpoint, when the other person says,
I'm just not smart enough to understand this,
you're kind of at a loss.
You can't really go anywhere from there.
Now, I think that, well, I know that I believed
that they were pretending to not get it.
And I think that most people in their heart knew that.
Anybody who's had one of those conversations,
I think they knew that, but there's no way
to really prove it because on this subject,
social conservatives have remained remarkably consistent.
They don't alter from this position until now.
Enter Pete Buttigieg and the person he is married to.
If you missed it, over the last few days,
a group of social conservatives has decided
that he shouldn't call the person he's married to,
his husband.
They would like him instead to use the term partner
or spouse.
A gender neutral term, huh?
Okay.
Why?
I just don't get it.
I don't know how I'm supposed to accommodate
all of these new terms you want us to use
and the way you want us to use them.
I mean, that's not tradition.
That is not tradition right there.
I just don't think, I don't understand.
Can you explain to me why Pete shouldn't call
the man he's married to his husband?
They don't have an answer
and they can't go anywhere with it.
They were told to be mad about this and they are,
but now it conflicts with their earlier routine.
That moment has finally arrived
where the consistency broke down.
With social conservatives,
this normally happens really early on.
The fact that they maintained consistency with this
for as long as they did is nothing short of a miracle.
But they can't say it's tradition.
Traditionally, what does husband mean?
A married man in relation to his spouse.
And since they want him to use the term spouse,
they obviously know what that means.
That's what husband means.
They want to shift it to say that husband and wife
have to go together.
That's not what the definition says.
I don't understand.
You're gonna have to explain it to me
because that is not in the dictionary.
I don't get it.
What this shows is that what a lot of us felt
from the very beginning is true.
It wasn't that they didn't understand.
It wasn't that they didn't get it.
It wasn't that they actually cared.
It was that somebody had told them,
you can other somebody this way.
You can kick down at somebody this way
and feel better about yourself.
More importantly, if you're looking down at them,
well, you're not gonna be looking up at your betters
seeing what they're doing.
You'll feel better about your lot in life
if you can kick down at somebody,
if you can take something away from someone else.
Because the reality is people go through title,
people go through title changes all the time,
all the time.
And it normally reflects
them becoming something new in themselves.
Miss to misses, misses to doctor,
a promotion in the military, change of title.
These are all things that are celebrated
because it marks the growth of that person.
For years, you've wanted to take that away from a group.
It's not that you didn't get it.
It's not that you didn't understand.
It's not that you don't understand gender neutral terms.
It's that you didn't want to
because somebody told you not to
and it made you feel better
to be bigoted against somebody else.
I hope Pete Buttigieg and his husband
have a very nice time.
Anyway, it's just a thought.
Y'all have a good day.
Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}