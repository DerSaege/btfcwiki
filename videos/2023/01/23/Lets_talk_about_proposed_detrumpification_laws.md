---
title: Let's talk about proposed detrumpification laws....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lkacky3MUJo) |
| Published | 2023/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring proposed laws at the state level aimed at detrumpification, preventing those involved in the Sixth from holding public office or positions of public trust.
- Laws are surfacing in places like Connecticut, New York, and Virginia, designed around words like insurrection, rebellion, and sedition, which aren't commonly charged.
- Concerns about the vague definitions within the laws, particularly regarding crimes related to the specified offenses and how they could be misused in the future.
- Beau expresses a lack of opposition to the idea behind the laws but is worried about their potential selective application and unforeseen consequences.
- Acknowledging the uniqueness of the events of the Sixth and the need for specific legislation that doesn't overly restrict future employment opportunities based on vague criteria.
- Emphasizing the importance of ensuring that the laws are well-defined to prevent potential misuse or unjust applications down the line.

### Quotes

- "The laws appear to be structured with the intent of detrumpification, of making sure that those people who participated in the Sixth can't hold public office."
- "We have to acknowledge that what happened on the 6th was special. It was different."
- "Creating a statute that basically bars them from future employment is a little much when it's this vague."
- "While I like the idea in theory, the application seems like it needs a little work to me."
- "Y'all have a good day."

### Oneliner

Beau examines state-level laws aimed at detrumpification but raises concerns about their vague definitions and potential misuse, stressing the need for more specific legislation.

### Audience

Legislative observers

### On-the-ground actions from transcript

- Contact your state representatives to express concerns about the vague definitions in proposed laws (suggested).
- Advocate for clear and specific legislation to prevent potential misuse and unjust applications (implied).

### Whats missing in summary

Deeper insights into the potential consequences of vaguely defined laws and how they could impact individuals in the future.

### Tags

#StateLaws #Detrumpification #Legislation #VagueDefinitions #PotentialMisuse


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some laws
that are in the works in different places
around the country at the state level.
Had some questions about them, about their effectiveness,
and we're just gonna kind of go over the intent of them,
how they're kind of structured,
and what may actually end up happening,
because those things aren't always the same.
The laws appear to be structured with the intent of detrumpification, of making sure
that those people who participated in the Sixth can't hold public office, or in some
cases even a position of public trust, like a teacher would fall under that.
Now they're being proposed around the country.
I know there's some in Connecticut, New York, Virginia.
And there are some in the works in other places.
On the surface, this kind of seems like a decent idea.
These are people who knowingly, unknowingly tricked into it, wandered into it, got out
out of control, whatever, they attempted to literally overturn an election.
That was the goal.
So that kind of makes sense.
But the laws are structured and focused on words like insurrection, rebellion, sedition,
things that aren't really being charged.
Yes, we have had a few seditious conspiracy cases, but not a lot of them.
So the application is a little weak.
It doesn't look like it's actually going to apply much.
And then in some of them it says, and crimes related to those.
In most cases, I think they have to be a felony related to those.
Again, okay, but how are you going to define related to those?
How are you going to define what these things are?
More importantly, how's it going to be used in the future?
If you end up with an authoritarian governor somewhere who declares a teacher's strike
rebellion? What are the charges and how does that impact them? What I have seen
so far these are very vague and vague when it comes to legislation is is not
good because it leads to the statute being used selectively and causing
downstream effects that you might not imagine.
I'm not opposed to the idea of this, it's just the way they're structured, it makes
me a little concerned.
I understand that the goal was to create a statute that included the sixth, but didn't
look like it was singling out those who participated in the sixth.
I get that. That makes sense on some level. However, we have to acknowledge that what
happened on the 6th was special. It was different. This wasn't the same thing as a union strike.
This wasn't the same thing as a bunch of people angry about a public health mandate. Even
And for those people who hold positions I don't agree with, creating a statute that
basically bars them from future employment is a little much when it's this vague.
It needs to be more specific because best case, it doesn't really do much.
case, it gets used in the future, 10, 15, 20 years from now, in a very unjust way.
So while I like the idea in theory, the application seems to, seems like it needs a little work
to me.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}