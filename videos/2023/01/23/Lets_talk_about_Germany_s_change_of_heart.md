---
title: Let's talk about Germany's change of heart....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sS2pJwzCjJM) |
| Published | 2023/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Germany signals approval for the export of leopard tanks to Ukraine.
- US involvement in potentially sending Abrams tanks discussed.
- Poland's national interests in supporting Ukraine due to safety concerns.
- Germany's interests in wealth and safety intersect with selling military equipment to Poland.
- Ukraine's situation and potential impact on Poland's borders.
- Ukraine likely to receive tanks with hopes for Germany's commitment.
- Multiple countries have vested interests in Ukraine's success.

### Quotes

- "Germany signals approval for the export of leopard tanks to Ukraine."
- "Poland's national interests intersect with supporting Ukraine due to safety concerns."
- "Ukraine likely to receive tanks with hopes for Germany's commitment."

### Oneliner

Germany signals approval for exporting tanks to Ukraine, intertwining national interests of multiple countries in the ongoing situation.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Support military aid to countries in need, like Ukraine (implied)
- Advocate for international cooperation and support for Ukraine (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the geopolitical implications of tank exports to Ukraine, offering insights into the intersecting national interests of various countries involved.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Germany, and signals,
and the table, and national interests,
and developing stories.
So last week we talked about the situation
when it came to supplying Ukraine.
And I said we'd hear more about it
the end of this week or early next. It is early next. Germany has signaled that
they won't block the export of leopard tanks to Ukraine. To catch everybody up,
Poland in particular is a country that has leopard tanks that is willing to
to give them to Ukraine. However, these tanks are of German production and
Germany retains kind of veto power over export. And there was a concern that
Germany wasn't going to allow it. They kind of indicated that they wanted the
United States to commit Abrams. Now they're signaling that they wouldn't
stand in the way.
Now, this is all signaling and messaging publicly.
It isn't an actual deal yet, but it's all there.
It looks like things are moving in that direction.
So we're not going to talk about the actual impact
of the tanks here.
And I will get a video out about the T-14.
sometime soon stop sending me messages about that. We're going to talk about
that international poker table where everybody's cheating because something
happened, right? There was a change of heart with Germany. They went from
signaling one thing to something else. So what happened? When we were talking
about it, we said it was playing out in a semi-public fashion. This part was
private. We don't really know, but there are only a few options, so we're going to
kind of go over those and we're going to talk about the different national
interests at play. The first option is that we don't know it yet, but the US has
agreed to send Abrams. Seems unlikely, but it's possible. Some kind of deal got
worked out there. That's option 1. Option 2 is that the United States slid some
other card that Germany wants to them quietly. We don't know what it is. These
are the two options that are gonna show up in the media the most. Why? Because
we're the United States and everybody's gonna focus on us, you know, because we
run the world, masters of the universe, and all that type stuff. There's a third
option and it doesn't have anything to do with DC, it has to do with Warsaw.
We're going to look at it through Polish national interests for a moment. When we
talked about national interests, wealth, safety, leading to power, kind of a
triangle. That's what makes up national interests. Poland is kind of a frontline
country when it comes to NATO. They're sitting there looking at Ukraine and it
is in their national interests due to safety for Ukraine to win and not just
win but for NATO to ingratiate itself with Ukraine in the process. That helps
Poland. So Poland wants to give them the tanks. Germany interfering with that
upsets their national interests. Make sense? Now to the next part. Poland is
looking at Ukraine. They are concerned about further Russian aggression and
they're modernizing their military. This is one of those things when we talk
about how the war in Ukraine is already lost for Russia, regardless of how the
fighting plays out, this is one of those things. Poland is modernizing their
military because of what happened in Ukraine. They're building it up,
they're expanding it, and they're building big. So, if we say we are going
to modernize our military, we're going to build it up, what does that mean we're
about to do? We're about to spend some money, right? We're about to buy some
stuff. If Germany wouldn't allow you to export tanks and it infringed on your
interests, would you buy more equipment from them? Probably not, right? Now let's
switch to Germany. Wealth, safety, leads to power. Selling more equipment to Poland?
Wealth. Having other countries use your military equipment is safety. Countries
that are using your equipment generally don't want to go to war with you because
you can cut off the supply chain. To be clear, Poland is not going to go to
World War Germany, but it's generally considered good foreign policy to have
other countries using your equipment. This is why the United States engages
in so much military aid. So now national interests converge. Wealth and safety for
Germany, safety for Poland. What brings them together? Leopard tanks going into
Ukraine. I don't think that this aspect of it is going to be covered much but I
think it has a whole lot to do with it. Ukraine is in a situation that it needs
help and if it falls it puts Poland right there. Poland is now border to
border in in a big way in a bigger way with the opposition. So they don't want
that obviously but at the same time dealing with preparations and dealing
with the borders that they do share with Russian ally, they're preparing, they're
expanding, they're building, they're gonna spend a lot of money. It looks like
they're adopting a saturation strategy which that means enough to put stuff
everywhere. Meaning if you're gonna come through here you're gonna pay for every
inch. That appears to be the strategy they've adopted and that's an expensive
strategy. That's a lot of equipment and Germany probably wants to make some
money on it. So national interests converge. Now, do I know that it is option
three? No. In fact, more than likely it's a combination of option three and one
of the others, but we don't know that.
It appears right now that Ukraine is going to get their tanks and some good ones.
It's worth noting that the signal is almost hopeful in the sense that maybe Germany is
willing to commit some as well. Maybe Germany is starting to read that the
rest of the West is not going to bail on Ukraine, and maybe they want Ukraine as a
client in the future. Germany has, I want to say, 200 Leopard tanks in storage. Not
the active ones that their military is currently using, 200 extra ones.
If Germany was to commit those, in addition to the challengers from the UK and the Leopard
tanks from elsewhere in Europe, that would give Ukraine what they need.
So there is a little bit of hope there, but again, we don't know this yet.
The card game is playing out and we get to watch it.
But when you're watching the news on this, be aware that it's probably going to center
on the United States, but there are a lot of countries, Poland in particular, who have
very vested interests in Ukraine succeeding. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}