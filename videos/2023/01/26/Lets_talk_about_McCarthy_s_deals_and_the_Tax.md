---
title: Let's talk about McCarthy's deals and the Tax....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pney-5Pf_eE) |
| Published | 2023/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party is facing issues with deals made by McCarthy to become Speaker, causing tension within the party.
- One major point of contention is the Fair Tax Act, aiming to eliminate the IRS and implement a 30% sales tax, disproportionately affecting lower-income individuals.
- The Act benefits the ultra-wealthy while burdening the working and upper middle class.
- McCarthy is under scrutiny for promising a vote on the Fair Tax Act, which some Republicans interpret differently.
- McCarthy plans to oppose the bill, signaling its likely failure even in the House.
- The Act was primarily a political stunt for social media engagement, not a realistic legislative endeavor.
- Less extreme Republicans understood the negative impact of the Act on their constituents.
- McCarthy's opposition and other Republicans unwilling to vote for it indicate the Act may not pass.
- The failure of this legislation may alienate far-right Republicans from their base and push them towards less extreme factions.
- The situation may reveal rifts within the Republican Party and challenge their unity.

### Quotes

- "It's going to be an abject failure for the holdouts."
- "This was never getting anywhere."
- "It's just a thought."

### Oneliner

The Republican Party faces tension over the Fair Tax Act, revealing rifts and potential division within the party.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to voice opposition to the Fair Tax Act (implied)
- Join advocacy groups working against regressive tax policies (implied)

### Whats missing in summary

Insights on potential long-term implications and strategies for resolving internal party conflicts.

### Tags

#RepublicanParty #FairTaxAct #TaxPolicy #McCarthy #PoliticalDivision


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about the taxman
and how the Republican Party is already running into issues with the deals that McCarthy made
to become Speaker. There were a number of little side deals that were alleged to have been made.
Some of them are already starting to surface and cause tension within the Republican Party.
One of the biggest at the moment is something called the Fair Tax Act.
In short, this is getting rid of the IRS completely and replacing it with a sales tax of about 30%.
Obviously, this is going to hit lower income people the most because they have to spend more
of their checks just to get by. So it's going to hurt them. It would really benefit the ultra-wealthy.
This is something that most Republicans are aware of and they understand that this is going to hit
the working class hard and it's going to hit their portion of the working class and the upper middle
class hard. Your suburban, you know, that big SUV you're going to buy, yeah, that thing's now,
it's going to be like 120 grand. It will price a lot of the upper middle class out of things that
they feel entitled to. Now, McCarthy is said to have promised a vote on this. That is how the
far-right Republicans view it. And we're talking about the holdouts who made him go through all of
those embarrassing, you know, votes. They view it as a promise on a vote. Now, McCarthy is saying
that isn't what he promised. He promised that it would go to committee or whatever. And the
Republican Party apparently plans to get rid of it in committee. McCarthy has signaled that he's
going to oppose this bill. It's worth noting this thing was never getting through the Senate.
It was never getting signed. It was a giant political stunt designed for social media
engagement because they believe that translates to votes. In this case, the less extreme Republicans
clearly understood the problem with it and what was going to happen to the people that they
cater to. The only people that would be happy about this would be the ultra-wealthy. Now,
with McCarthy signaling his opposition and there are a few other Republicans who have already said
they're not going to vote in favor of it, I don't even think it has the votes to pass the House
now. Even if it gets out of committee, I don't think it's going to go anywhere. So, in one of
the first real pieces of legislation that is going to arise from all of these deals and all of this
grandstanding that occurred during the battle for Speaker, it's going to be an abject failure
for the holdouts. It's going to cast them in a situation where they made these promises,
but they can't get anywhere with it. Now, they will probably try to spin this and say that
anybody who votes against it, well, they're not really MAGA. They're not really a Republican,
and you're going to see that. The thing is, this might be one of those moments because it hits
upper middle class people in their pockets. It might be one of the things that actually starts
to drive a wedge between that far-right group and that base that they've counted on. It may
start to push them back towards the less extreme Republicans. But if you've heard about this,
the reason we didn't cover it is because obvious outcome is obvious. This was never getting
anywhere. But now, it doesn't even look like it's going to get out of the House.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}