---
title: Let's talk about Soledar and Bunker Hill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MT4xLOSPd7E) |
| Published | 2023/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to a message about not providing proper coverage of events, Beau addresses the Russian capture of Solidar, Ukraine, after a lengthy fight.
- Draws a parallel to the Battle of Bunker Hill during the American Revolution, where the Americans lost but it is celebrated in U.S. history.
- Points out that the British victory at Bunker Hill was costly, and they paid a high price for capturing the hill.
- Suggests that there may be lessons from history, particularly the Battle of Bunker Hill, that relate to the situation in Solidar.
- Raises concerns about the potential devastating consequences for Russia despite their capture of the town.
- Notes that the conflict in Ukraine is in a protracted stage with lines moving back and forth until a breakthrough occurs.
- Warns that if Russia continues to have victories like Solidar, they may deplete their forces rapidly.

### Quotes

- "Y'all know we lost that battle, right?"
- "It had devastating consequences for one of the only effective Russian units."
- "If Russia has a few more victories like that, they're not going to have many people left to withdraw."

### Oneliner

Beau addresses the Russian capture of Solidar, drawing parallels to historic battles and cautioning about potential consequences for Russia's forces in the ongoing conflict in Ukraine.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Analyze historical battles for potential lessons (implied)

### Whats missing in summary

The emotional impact and historical significance of battles like Bunker Hill and their relevance to current events in Solidar may best be understood by watching the full transcript. 

### Tags

#History #Ukraine #Russia #Conflict #Lessons


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about something
that I didn't talk about that I guess I should have.
So we will remedy that today.
I got a message and it said
that I was doing y'all a disservice.
I wasn't providing full coverage, proper coverage
of everything that occurred
and that it would lead people to the wrong conclusion.
Said that I covered the Ukrainian wins,
but I didn't cover the Russian wins of the same way.
So it's just bad for y'all.
And that in particular,
I have an obligation to talk about Solidar.
Fair enough.
I wouldn't want to shirk my obligations.
So for those that don't know,
it's a town in Ukraine
and the Russians have captured it
after a very, very, very lengthy fight.
So that's what happened.
I'm gonna talk about it
the way I would normally talk about something like this.
In 1775, during the siege of Boston,
there was the Battle of Bunker Hill.
History nerds, just let it go.
That's what everybody knows it as.
This battle is seen as formative to the United States.
Incredibly important.
It was one of those moments
where during the American Revolution,
the Americans proved themselves, right?
Big deal.
It's in every American history book,
monuments, songs even were written about it.
And I don't mean way back then,
even though there were some way back then.
I mean, relatively recently.
It's a big deal.
And why shouldn't it be?
It's the place where Prescott proved he was wise
because when outnumbered and low on ammunition
as the British stormed his position,
he said, hold your fire
until you see the whites of their eyes.
Great moment in American history.
Y'all know we lost that battle, right?
The British took the hill.
The British took the hill, but it's seen as formative.
It's something that is celebrated
in the United States today.
After the battle, Nathaniel Green said,
I wish we could sell him another hill at the same price.
General Clinton, American general,
said something to the effect of
if the British had a few more victories like that,
their rule in the colonies would come to an end.
Their dominion would come to an end.
The British general said that the loss we have sustained
is greater than we can bear, even though they won.
See, what happened was they didn't really want the hill.
What they wanted was a town near the hill, right?
But they just, for whatever reason,
obsessed with capturing that piece of Boston dirt,
and they paid a devastating cost for it.
I think there's probably some lessons there.
There's probably some things that relate to Soledad.
It's hard to tell at this point in time.
You know, history has a sense of humor.
But if I had to guess what the legacy of that fight
ends up being, Soledad will be known as the anvil
on which Wagner and Ukraine was broke.
I don't know that this is quite the win
that some may want it to be.
It had devastating consequences
for one of the only effective Russian units,
one of the only effective groups.
Desertions, I don't see it the way
I think some people might want me to.
Yeah, the Russians, they have the town, for now.
But as discussed in other videos,
we are in that protracted stage.
Lines are going to move back and forth
until there's a breakthrough.
It's what's going to happen.
If Russia has a few more victories like that,
they're not going to have many people left to withdraw.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}