---
title: Let's talk about Newport News and remembering your training....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mtdb1kscZ1U) |
| Published | 2023/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lawyers in Newport News allege school administrators were warned three times on the day of the shooting involving a six-year-old.
- A child reported being threatened by another student with a gun, but no action was taken by the administrators.
- Despite being warned, the administrators failed to act and even discouraged someone from checking on the situation.
- The story initially made headlines but faded, likely to resurface due to these allegations.
- The incident underscores the reliance on children to follow safety protocols when adults fail to act.
- There was no guarantee of a safe outcome, and the situation could have been much worse.
- Similar to past incidents, there may be public outcry over the lack of preventive action by authorities.
- This case serves as a reminder that school safety is not guaranteed and can happen anywhere.
- It's time for school administrators to recognize the potential risks and take proactive measures.
- Beau concludes with a call for everyone to be vigilant and prepared in such situations.

### Quotes

- "The children remembered their training. Maybe it's time for everybody else to start remembering theirs."
- "We teach these kids, if you see something, say something. Drill it into them."
- "It could have been way, way worse."
- "This is probably a moment for school administrators to realize the idea of, oh, it can't happen here. That's not a thing."
- "There was no guarantee that that's how it was going to turn out."

### Oneliner

Lawyers allege school administrators ignored warnings in Newport News, underscoring the need for proactive safety measures and adult accountability.

### Audience

School administrators

### On-the-ground actions from transcript

- Educate school staff on responding promptly to safety concerns (implied)
- Implement regular drills and training for staff and students (implied)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau in urging accountability and proactive measures for school safety. 

### Tags

#SchoolSafety #Accountability #PreventiveAction #ChildProtection #CommunityPolicing


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about some news coming out
of Newport News, Virginia.
And we're going to go over what is being said,
what it might mean, and how a story that
has sort of faded from the headlines
is probably about to come back in a very, very big way.
So what's being said?
Lawyers in Newport News are alleging
that school administrators were warned three times
the day of the shooting.
I know it's hard to keep track.
This is the one involving the six-year-old.
The lawyers say that the administrators were warned
at around 11.15, then about an hour later, and then at 1 PM.
In one of the instances, a child told a teacher
that the student had showed them the gun
and said that if they told anybody, well,
they'd be shot.
We teach these kids, if you see something, say something.
Drill it into them.
Tell them that over and over and over again.
According to these allegations, the administrators
at this school were warned three times and did nothing.
According to the allegations, they even had somebody
volunteer and say, well, I'll go check it out,
and were told not to worry about it because the school
day was almost over.
This story, it was very front of the news cycle
when it happened, and then it started to fade.
It's going to come back because of this.
This is another instance where we sit there
and we count on one person to say,
where we sit there and we count on literal schoolchildren
to remember their training and do what they're supposed to,
but the adults fail to act.
Those are the allegations.
In this case, we were relatively lucky,
but it didn't have to be that way.
There was no guarantee that that's
how it was going to turn out.
It could have been way, way worse.
So this will probably generate outcry
much like what happened in Texas,
where something could have been done, but it wasn't.
This is something that is occurring all over the country.
This is probably a moment for school administrators
to realize the idea of, oh, it can't happen here.
That's not a thing.
It can happen anywhere.
The children remembered their training.
Maybe it's time for everybody else
to start remembering theirs.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}