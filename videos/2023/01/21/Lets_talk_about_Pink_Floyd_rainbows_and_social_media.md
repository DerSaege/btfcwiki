---
title: Let's talk about Pink Floyd, rainbows, and social media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mp_wnxdVLZI) |
| Published | 2023/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pink Floyd changed their social media profile pictures to commemorate the 50th anniversary of Dark Side of the Moon, featuring a design related to the album's iconic cover.
- Some individuals reacted negatively to the rainbow element in the profile picture, questioning its relevance and accusing Pink Floyd of being "woke."
- The album cover of Dark Side of the Moon features light hitting a prism, creating a rainbow, unrelated to the current association with LGBTQ+ pride.
- Beau notes Pink Floyd's history of being politically engaged and mentions the album's 1973 release date, five years before the rainbow flag became a symbol of Pride.
- The backlash against the rainbow in Pink Floyd's profile picture stems from a desire for "equal representation," but Beau argues that default representation already favors the majority.
- Beau criticizes the insecurity and anger fueling such comments, suggesting they are manipulated by those in power to maintain control through fear and division.
- Authoritarians historically use scapegoating and fear-mongering to control populations, directing anger towards specific groups to manipulate behavior.
- Beau warns against falling into a pattern of blaming and fearing others, stressing the importance of breaking free from manipulation to avoid generational consequences.
- Not addressing the manipulation and fear tactics perpetuated by certain groups could lead to a perpetuation of ignorance and timidity among future generations, creating an underclass.
- Beau urges listeners to resist being controlled by divisive narratives and avoid becoming complicit in perpetuating harmful systems.

### Quotes

- "You're making yourself look stupid. What is that, Pink Floyd? What a disgrace. From this moment, I don't listen to this band."
- "If you don't stop letting these people control your every thought by giving you somebody to blame, you're gonna end up another brick in the wall."

### Oneliner

Pink Floyd's social media tribute sparks backlash, revealing deeper fears of manipulation and division rooted in societal power dynamics.

### Audience

Social Media Users

### On-the-ground actions from transcript

- Resist manipulation and fear tactics by critically analyzing information and narratives spread on social media (implied).
- Foster understanding and empathy towards diverse perspectives to combat divisive narratives (implied).

### Whats missing in summary

The full transcript provides a comprehensive examination of how societal power dynamics, manipulation, fear, and division intersect in responses to seemingly innocuous events like Pink Floyd's social media tribute.

### Tags

#PinkFloyd #SocialMedia #Manipulation #Fear #Division


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Pink Floyd
and the 50th anniversary of Dark Side of the Moon
and social media.
And it just goes on from there, but you'll see.
So yes, celebrating the 50th anniversary
of Dark Side of the Moon, Pink Floyd changed
their profile pictures on social media.
They changed it to a, like a 50 in the background
with like a triangle and a rainbow inside.
This upsets some people.
What's up with the rainbow?
Lose the rainbow.
You're making yourself look stupid.
What is that, Pink Floyd?
What a disgrace.
From this moment, I don't listen to this band.
Are you going woke with rainbows?
Is there a straight flag?
I want equal representation.
Now, those old enough to have ever seen an album cover
in real life, probably already laughing.
The Dark Side of the Moon, the album cover,
is light hitting a prism, a triangle,
and breaking apart into a rainbow of colors.
That's what it's representing.
Doesn't have anything to do
with what they're associating it with.
At the same time, I would like to point out
that Pink Floyd isn't exactly known for being apolitical.
It's also, I mean, worth noting,
little trivia fact here,
the 50th anniversary would indicate
that the album came out in 1973.
Five years before the rainbow flag
became associated with Pride.
All of this is funny, right?
This is funny on some level,
but what can we take away from it?
What can we learn from it?
More importantly, what causes it?
I want equal representation.
Okay, here's the thing.
If you're the default, if you are the norm, the average,
norm not meaning normal,
meaning the thing that appears the most,
you don't need special representation.
You get it by default.
It's already there.
I mean, this is literally that meme,
won't somebody think of the straight white men
when you think of the straight white men.
Aside from the anxiety,
there's also the anger, the fear, the insecurity
that actually causes these comments.
Where does that come from?
It comes from the same people
that are telling you to be alpha and masculine,
trying to, well, keep you angry, keep you scared,
because they know that if you are angry, if you are scared,
you'll do whatever your betters tell you to do.
You will lash out on social media.
You'll lash out at the Capitol.
This is how they manipulate you,
by pushing this anger, this hate, this division.
Find somebody to other, blame them.
There's a reason that they continue to push this.
And it's hate, sure,
but more importantly,
it's control of those people they can convince to hate.
Because if they can convince them to hate some other group,
to lash out just randomly at any rainbow,
they can control them.
They can get them to do anything,
because they're so scared, they're so insecure,
that they will do whatever their betters tell them to do.
They farm out everything.
It's not a stretch.
It's how it works.
It's why authoritarians throughout history
always try to find a scapegoat.
They always try to find somebody to blame,
to focus that anger on,
and to scare people about that group.
What are you actually scared of?
If the right wing in the United States
doesn't wake up to this,
this ignorance, this fear, this timidity,
it's gonna become a generational thing.
If you don't wake up and stop listening to these people
who are telling you to blame everybody
and be afraid of anything,
literally have y'all swinging at rainbows now,
your kids are gonna be the same way,
and they're gonna get left behind.
They'll end up an underclass.
If you don't stop letting these people
control your every thought by giving you somebody to blame,
you're gonna end up another brick in the wall.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}