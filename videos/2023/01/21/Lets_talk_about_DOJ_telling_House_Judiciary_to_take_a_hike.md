---
title: Let's talk about DOJ telling House Judiciary to take a hike....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZuJLTcDTX7A) |
| Published | 2023/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Justice declined to provide the House Judiciary Committee with information on ongoing investigations, citing long-standing policy. 
- The DOJ stated that they will not confirm or deny the existence of pending investigations or provide non-public information in response to congressional requests. 
- This decision means that upcoming House Judiciary hearings may lack substance, as many topics are related to active investigations. 
- The separation of powers between the executive branch, where the DOJ resides, and Congress is emphasized as the reason behind this practice.
- Members of the House Judiciary Committee may face challenges in fulfilling their claims and promises made on social media due to the lack of progress in investigations.
- The DOJ's stance prevents the committee from obtaining non-public information about ongoing criminal investigations.
- Concerns raised about this issue are deemed unrealistic because DOJ policy restricts most of what people are worried about.
- Beau was criticized for not discussing this topic earlier, but he explains that the DOJ's policy makes it unlikely for the information sought by the committee to be provided.

### Quotes

- "The Department of Justice does not comment on active investigations when it comes to congressional requests."
- "It's not a thing where it's the Democrats stonewalling the Republican Party."
- "Concerns expressed about ongoing investigations are unrealistic because department policy prohibits 99% of what people are worried about."

### Oneliner

The Department of Justice's refusal to disclose ongoing investigations to the House Judiciary Committee may hinder the effectiveness of future hearings, rooted in a long-standing policy of non-disclosure.

### Audience

Legislative aides, policymakers

### On-the-ground actions from transcript

- Contact your representatives to advocate for transparency and accountability in government practices. (implied)

### Whats missing in summary

The nuances and detailed explanations provided by Beau in the full transcript. 

### Tags

#DepartmentOfJustice #HouseJudiciaryCommittee #OngoingInvestigations #SeparationOfPowers #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Department of Justice and what they
told the House Judiciary when the committee assignments were made and
people began to see the makeup of the new House Judiciary Committee, there
were concerns raised because some people believe that some of those on the
Judiciary Committee might actually be at least in some way related to some of the
ongoing investigations. There was concern expressed a few times I didn't talk about
it and you're about to find out why. So the Department of Justice sent a letter
to the House Judiciary Committee that if I was to paraphrase it, it would be take
a walk, you're gonna find out more information about active investigations
from CNN than you're gonna find out from us. It's not exactly what was said, but
that was kind of the meaning. Why? Because that's how it's always been. That is how
it's always been. The Department of Justice does not comment on active
investigations when it comes to congressional requests. That's not a
a thing. People qualified to be on that committee would know that. What did it
actually say? Consistent with long-standing policy and practice, any
oversight requests must be weighed against the department's interests in
protecting the integrity of its work. Long-standing department policy prevents
us from confirming or denying the existence of pending investigations in
response to congressional requests or providing non-public information about
our investigations. In short, we're not even going to tell you whether or not
we're investigating something or somebody and we're only going to give
you public information. Yeah, those hearings, they don't look like they're
going to have much teeth now because a lot of them related to active
investigations. So the hearings in many cases will turn into the House
judiciary saying, hey, tell us about this. And the DOJ saying, yeah, we can't comment on that.
This isn't a thing where it's the Democrats stonewalling the Republican Party. This is
part of the separation of powers. The Department of Justice is in the executive branch.
This is a long-standing thing. It's been this way for a really, really long time.
So the ability of some of the members on the House Judiciary to fulfill all of those claims and all of those things they
said they were going to do on social media,
it's going to get really hard because the investigations won't go anywhere. Their hearings won't go anywhere.
won't go anywhere. They're not going to get non-public information about ongoing
criminal investigations, which is what they really want, at least what it's what
they appear to want. So if you were one of those who expressed those concerns,
and especially one of those, if you were one of those who sent me mean messages
for not talking about it, this is why. It's not going to happen. It's not, it's
It's not a realistic concern because department policy from DOJ prohibits 99% of what people
are actually worried about.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}