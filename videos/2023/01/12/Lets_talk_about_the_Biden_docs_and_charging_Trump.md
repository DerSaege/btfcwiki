---
title: Let's talk about the Biden docs and charging Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=n91vUi5H80w) |
| Published | 2023/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparing coverage of Biden documents to Trump documents
- Emphasizing that based on available evidence, they are not the same
- Calls for an investigation to determine what happened and prevent a recurrence
- Mentions the handover of the case to a Trump-nominated US attorney
- Contrasts willful retention in Trump case with immediate action in Biden case
- Asserts that charging Trump should not be harder due to Biden case
- Criticizes the idea that Biden case makes charging Trump more difficult
- States that the proper response, if evidence is enough, should be to charge both
- Argues against those trying to protect Trump from charges
- Concludes by stressing the importance of charging based on evidence

### Quotes

- "The correct course of action, if there was enough evidence to charge, it wouldn't be to let Trump go. It would be to charge them both."
- "That's just something they've made up to muddy the waters."
- "If there was enough evidence to charge, it wouldn't be to let Trump go."
- "They're trying to convince you to let Trump out of this."
- "Y'all have a good day."

### Oneliner

Beau clarifies the differences between the coverage of Biden and Trump documents, urging for a fair investigation and dismissal of attempts to protect Trump from charges.

### Audience

Citizens, Activists, Justice Advocates

### On-the-ground actions from transcript

- Contact your representatives to demand transparency and accountability in investigations (suggested)
- Support organizations advocating for fair legal processes and holding officials accountable (suggested)

### Whats missing in summary

Detailed breakdown of the specific points of comparison between Biden and Trump document coverage.

### Tags

#Investigation #Transparency #Justice #Accountability #ChargingDecisions


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk a little bit
about the coverage that has come out
since the Biden documents thing began
and its comparison to the Trump documents thing.
We're going to go over some of the statements
that have shown up in the media and the general idea.
The first thing is that it's the same.
We've talked about it already.
Based on all available evidence, it is not the same.
It really isn't.
Now, should there be an investigation
to determine what happened
and make sure it doesn't happen again?
The hot wash aspect of it?
Yes, absolutely.
That definitely needs to occur.
Based on some of the reporting,
some of this stuff was the type of documents
that I have always been worried about
during the Trump coverage, which means, yes,
they have to find out what happened.
The thing is, that's occurring.
They handed, Garland handed off this case
to a Trump-nominated US attorney out of Chicago.
So that's occurring, that's happening.
Now, as far as whether or not they're really the same,
if you go back to the Trump coverage,
you will hear the phrase willful retention
over and over and over again,
because that's kind of actually the crime.
Willfully retaining the documents, that's the issue,
but that's not what happened in the Biden case.
By all available evidence, what occurred was,
hey, they found it and they immediately called.
That's the exact opposite of willful retention.
It's not the same.
As far as it, making a comparison,
the comparison is, look,
this is how it's supposed to be handled.
This is what Trump did.
The Biden administration didn't fight subpoenas,
didn't say the stuff had been returned,
didn't constantly obstruct.
That didn't occur.
They didn't willfully retain.
Which is the crime.
Now, another piece of rhetoric,
another talking point that has come out
that's kind of gotten under my skin a little bit,
to be honest, is the idea that this
would make charging Trump harder.
In what world?
How does that work?
I'm super curious, because that doesn't make sense.
The way a charging decision is supposed to be reached
is they evaluate the evidence
and they make the charging decision based on the evidence.
Never, never has it occurred
where the FBI is looking into a bank robbery
and they're like, oh, we were gonna charge him,
but we found out somebody else robbed a bank too.
That's not how it works.
It doesn't make the charging decision more difficult.
That is something that apologists and lawyers
and that is something that apologists
and those people who want to protect the institution
of the presidency made up.
That's not how this functions.
Let's say that the Biden case and the Trump case
are identical in every respect.
Everything is the same.
It's not, but let's say that it is.
The correct course of action,
if there was enough evidence to charge,
it wouldn't be to let Trump go.
It would be to charge them both.
That's how it's supposed to work.
It doesn't make the charging decision more difficult.
Anybody who is telling you that is selling you something.
They're trying to convince you to let Trump out of this.
The charge is willful retention.
That's the most likely charge for this,
the willful retention of national defense information.
That would be the real issue.
By all the evidence,
and it's being investigated by a Trump-nominated US attorney,
that didn't occur.
So it's not the same, but even if it was the same,
the proper response wouldn't be to let Trump go,
to look the other way.
Set aside the scale, the scope, all of the actual facts.
If the two instances were identical,
that still wouldn't justify not charging Trump.
That's just something they've made up to muddy the waters.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}