---
title: Let's talk about Special Counsel updates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=34NYzjhYZxk) |
| Published | 2023/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on the special counsel's office are being provided due to new reporting.
- A flood of subpoenas went out in December, targeting Trump-affiliated PACs and communications related to voting machine companies.
- The special counsel's office seems to be branching into new lines of investigation, potentially related to financial crimes.
- People are being called before the grand jury at an accelerating pace, with some called back for intense second appearances.
- The actions of the special counsel's office suggest an expanding investigation, not one winding down.
- There's speculation that the investigation is looking into possible illegal activities by Trump World, such as false claims for fundraising.
- Testimony involving Trump and Eastman requesting help for alternate electors is seen as significant but not a smoking gun.
- The new information indicates that the special counsel's office is actively seeking to prosecute rather than wind down the investigation.
- The reporting doesn't support the idea of protecting Trump from accountability but rather focuses on securing a conviction.
- The overall picture painted is of an intensifying investigation into various aspects related to Trump and potential financial crimes.

### Quotes

- "This reporting matches the idea of a special counsel's office looking to secure a conviction."
- "The actions are not in line with letting Trump go."
- "It's worth noting that if Trump World knew their claims were false and were raising money off them, that might not be legal."
- "It's more like Trump's thumbprint on a shell casing."
- "Y'all have a good day."

### Oneliner

Updates on the special counsel's office reveal an expanding investigation into potential financial crimes and intense actions aiming to secure a conviction, contradicting the idea of winding down the probe or protecting Trump from accountability.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact legal experts to understand the implications of the special counsel's actions (suggested)
- Stay informed about the developments in the investigation and share accurate information with others (exemplified)

### Whats missing in summary

In-depth analysis of the implications and potential outcomes of the expanding investigation into Trump and related financial crimes.

### Tags

#SpecialCounsel #InvestigationUpdate #PotentialFinancialCrimes #TrumpWorld #LegalImplications


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the special counsel's
office, provide some updates on that,
because there's some new reporting.
And we're going to talk about a question
that somebody sent to me.
And we're going to kind of throw it all together,
because there's a lot of new information that
has come out over the last 24 hours or so.
And it's interesting.
So when we talked about the special counsel's office
before, we said we weren't going to find out anything really
until stuff started leaking out about new subpoenas or whatever.
That is now happening.
In December, a flood of subpoenas went out.
A whole bunch of subpoenas went out.
What's interesting about them is that some
the people that had already been subpoenaed before and some of the
questions and some of the stuff that it the special counsel's office is looking
at. In particular, they're looking for information related to Trump affiliated
PACs, the money, and they're looking for communications related to the voting
machine companies. Those two things in particular are interesting because they
don't necessarily have much to do with the cases that we are aware of. It may
indicate that Smith has opened an entirely new line of investigation into
possible financial crimes on top of everything else.
We also have reporting saying that people are being called
before the grand jury at an accelerating pace,
and that some people are being called back
to talk to the grand jury a second time.
And the word that was used to describe
those second appearances before the grand jury was intense.
When the special counsel came on board,
there were a lot of people who viewed it as a sign
that it was gonna become political
and Trump would avoid accountability.
There are also people who thought Smith was being brought in
kind of wind everything down.
That is not what this information says.
That's not what this looks like.
To the contrary, it looks like the investigation is actually expanding.
These actions are not in line with letting Trump go.
These actions are in line with actually trying to prosecute him.
So we're going to have to continue to wait, but the information that came out is painting
a picture of a special counsel's office that is looking into every aspect of it and might
have even opened up a whole new line of investigation.
It's worth noting that if Trump World, if there are a bunch of communications that suggest
they knew that their claims were false and they were raising money off of those claims
and saying it was going to an election defense fund that didn't actually exist, that might
not be legal, and that may be what Smith is looking into. We don't know that though.
So that's the news. There's more people going before the grand jury. Some people
are being called back. It is intense when you're in front of the grand jury,
particularly the second time, and there are a whole bunch of new subpoenas. And
the questions in the subpoenas kind of paint the picture that they might be
looking in a entirely new direction.
Now, going along with this, I got a message from somebody asking me,
Hey, is this a smoking gun?
Because I guess MSNBC called it that.
And it's testimony from McDaniel, no, it's testimony from the January 6th
committee, but it's McDaniel talking about a phone call that started with Trump.
Trump called her and then put Eastman on the phone and then according to the testimony Eastman then
asked for her help in facilitating the alternate electors. Is it a smoking gun? No, no. It's
testimony. It's not a smoking gun. It's more like Trump's thumbprint on a
shell casing though. It is very important testimony, but a smoking gun is generally
physical evidence. It's a big deal because this would directly connect
Trump to that to that plot but I don't know that I would go as far as to call
it a smoking gun super important if this goes to court you will definitely see
that material again but it's not something that 100% makes the case it's
It's a big building block in the case though.
So that's the new information, that's the new reporting about this.
All of this leads me to believe that Smith is actually looking to prosecute, that this
isn't going to be something that is kind of wound down or drug out so long that nobody
cares anymore, or they're looking for some other way for Trump to avoid accountability,
protecting the institution of the presidency, all of that stuff.
None of this, none of this reporting matches that idea.
This reporting matches the idea of a special counsel's office looking to secure a conviction.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}