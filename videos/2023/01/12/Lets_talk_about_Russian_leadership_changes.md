---
title: Let's talk about Russian leadership changes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NDjRM3MhrP4) |
| Published | 2023/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Leadership changes in the Russian military at high levels indicate a division and political maneuvering rather than boosting the war effort.
- Reputation of the new commander, Gerasimov, is good, but previous commanders lacked necessary resources.
- Erratic leadership changes within 90 days can damage morale and may signify a crack in Russian resolve.
- Russian military bloggers are aware of these moves and are spreading information, impacting troop morale.
- Putin's impatience for results and erratic decision-making may indicate a lack of resolve for a prolonged occupation.
- Ukrainian leadership appears upbeat, setting timelines for resolving the conflict based on the support they receive.
- Erratic leadership changes suggest a weakening resolve within the Russian military, acknowledging the challenges they face.
- The reality of the situation is starting to wear on the patience of Russian military leadership, potentially benefiting Ukraine.

### Quotes

- "Erratic leadership changes within 90 days can damage morale and may signify a crack in Russian resolve."
- "Putin's impatience for results and erratic decision-making may indicate a lack of resolve for a prolonged occupation."
- "Ukrainian leadership appears upbeat, setting timelines for resolving the conflict based on the support they receive."

### Oneliner

Leadership changes in the Russian military reveal political maneuvering and a potential crack in resolve, impacting morale and indicating challenges ahead.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Monitor and analyze the leadership changes in the Russian military to understand potential shifts in strategy and resolve (implied).
- Stay informed about the situation in Ukraine and the impact of these changes on troop morale and military operations (implied).

### Whats missing in summary

Analysis of the long-term implications and potential outcomes of the leadership changes in the Russian military.

### Tags

#Russia #Ukraine #Military #LeadershipChanges #PoliticalManeuvering


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about some leadership changes
in Russia.
Well, actually, I guess in Ukraine,
but on the Russian side.
And what they mean, because they're interesting.
And they may reveal something.
What we're going to do is we're going to go over what we know,
and then we will go over the possibilities of what it means.
There are leadership changes occurring in the Russian
military at very high levels.
In fact, the overall commander of the invasion,
of the operation in Ukraine, has been yanked,
is being replaced with somebody named Gerasimov.
The previous commander was on the job, I don't know,
90 days, not long, not long.
Now, Gerasimov has a reputation, and it's a good one.
The thing is, the previous commanders,
they weren't tactically or operationally incompetent.
They just don't have what they need.
So rather than boosting the Russian war effort,
this may actually just serve to damage his reputation.
And it seems unlikely that this is going to have a massive effect
on the battlefield.
Now, that's what we know.
What can we kind of infer?
First, one of the interesting things about this
is that it appears as though there is a division occurring,
a lot of division.
There is a division occurring.
A lot of these moves seem political.
They don't appear to be related to military achievement.
They appear to be political.
And that kind of maneuvering, especially
when it's that erratic, 90 days, that's bad for morale.
That is bad for morale for the troops.
The other thing is that it may point to a crack.
I don't want to say a break in Russian resolve.
I don't think it's that serious yet.
But there appears to be a division that's developing
within the Russian military.
And to put it into US terms, it's
the US Special Operations community on one side
and the regular army on the other.
There's this division, and it seems
like they're trying to jockey for political power.
And these leadership changes may have something to do with that.
The other thing that comes of this
is that these moves are well known.
The Russian military bloggers, they're aware of this,
and they all have their own takes,
and they're all putting out their own information.
And this information is making its way to the troops.
And this is, again, it's bad for their morale.
It's not a good move for the Russian military.
I don't know that Grasmoff having direct control
is going to create a massive shift.
The other thing that's at play is that 90 days.
That 90 days.
Putin is getting impatient.
Putin is getting impatient.
That is an erratic move.
He wants results.
The reality is they don't have what they need
to get lightning fast results.
They don't have it.
And if Putin is already to the point
where he's making decisions like this
or being manipulated by factions in the military,
that doesn't say a lot for their resolve
and how long they're going to be willing to put it out there.
The other piece of this that is really interesting
is that Putin knows what we've talked about on this channel.
They haven't gotten to the hard part yet.
If he's losing his patience now, he does not
have the resolve to stick it out through an actual occupation.
He does not have the resolve to deal with the partisan activity
that will occur if Russia is successful.
That is especially true when the Ukrainian leadership is,
I don't even know, they may have said this just to poke at him.
But the Ukrainian leadership is very upbeat.
They're coming out saying, you keep giving us what we need.
We're going to be done with this this year.
And it's not a troops will be home by Christmas type of thing.
They have their estimates and they think that they can achieve.
And then on the flip side, they're saying that, you know,
if it doesn't work, well, then this is going to go on for years and years and years.
And that's true because Russia hasn't gotten to the hard part yet.
The erratic leadership changes as quickly as they're occurring.
And with as little military sense as they make, to me,
it does look like a division has occurred within the Russian military.
And there is an overall weakening of resolve.
Now, it's not enough to say that they're going to break soon.
It's not that.
But you're starting to see them come to terms with the reality of what they???
that they bit off more than they could chew.
That their intelligence assessments in the beginning were not good.
They were based on faulty information.
Their estimates of what they had in stockpiles were not accurate.
All of this is starting to come up.
And it's starting to take its toll on the leadership at the very top.
Those on the lines, they've known this.
But it's really starting to wear on their patience.
That's good news for Ukraine.
It's not enough???it's not enough yet to say that they're really starting to weaken
their political position and that they may be more willing to
pull back to lines they already had or something like that.
It's not there yet, but it's starting.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}