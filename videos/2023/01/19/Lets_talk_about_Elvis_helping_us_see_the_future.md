---
title: Let's talk about Elvis helping us see the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-ecj3UD5zZY) |
| Published | 2023/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elvis faced a significant backlash when he first emerged, with demonstrations and laws attempting to ban his performances.
- The backlash against Elvis was due to his provocative dance moves and challenging of societal roles, particularly as a white Southern man.
- People were upset not because Elvis may have been profiting off black music, but because he was introducing it to white kids, challenging existing roles.
- Despite the backlash, society did not collapse, and instead evolved and moved forward.
- The opposition to Elvis was rooted in perception, tradition, and resistance to change.
- Beau questions if there are modern performers challenging societal roles, and warns against standing against them based on outdated traditions.
- He suggests that those opposing current performers may be ridiculed in the future, just like those who protested Elvis.
- Beau ends with a message inspired by Elvis and Jesus: "Don't be cruel."

### Quotes

- "Elvis, Elvis, leave me be. Keep that pelvis far from me."
- "Your concern is based on tradition. Your concern is based on peer pressure from dead people."
- "At some point in the future, there will probably be one of those people on a stamp."
- "Don't be cruel."
- "Y'all have a good day."

### Oneliner

Elvis faced backlash for challenging societal roles, warning against opposing modern performers based on tradition.

### Audience

Cultural critics, music enthusiasts

### On-the-ground actions from transcript

- Support and appreciate modern performers who challenge societal norms (exemplified)
- Embrace change and evolution in society (exemplified)

### Whats missing in summary

The full transcript delves into the historical backlash against Elvis and draws parallels with modern-day resistance to change and challenging of traditional roles.

### Tags

#Elvis #SocietalChange #CulturalEvolution #ChallengingNorms #MusicHistory


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Elvis
and how Elvis can, well, still teach the country a whole lot.
In fact, he can help us see the future.
Today, Elvis is a national icon in a lot of ways.
But what about when he first came on scene?
Was he universally loved?
No, no.
In fact, there was a pretty big backlash against him.
Really big.
There were demonstrations.
Some states, they either did or tried
to pass laws banning his type of performance.
Some cities wouldn't let him play when he was on TV.
They would only show him from certain angles.
There's commentary in newspapers about him.
And they didn't like him.
In fact, you can find articles where they
used his name in the headline.
But then throughout the article, they just
referred to him as the pelvis.
Elvis, Elvis, leave me be.
Keep that pelvis far from me.
Why?
Why was there this backlash?
There's two reasons.
And part of it was the charged nature
when it came to the way he danced, which was, I mean,
today it seems kind of quaint.
But at the time, people were screaming,
oh, think of the children.
But there was something else that had a lot more
to do with the backlash.
And it's because he was breaking down a wall.
He was breaking down a wall and challenging roles.
And for a white Southern man to be doing it,
oh, it infuriated people.
Now, there's a whole video that could
be made about how Elvis could be perceived to have basically
just ripped off black music and profited off of it.
That's a whole video that can be made.
But that's not why people were mad at the time.
Nobody in the South in that period of time
cared if somebody ripped off black folk.
That wasn't a concern of theirs.
The concern was that he was taking black music
and getting it to white kids.
That was the problem.
It challenged those roles, those roles that existed.
And people are reluctant to change.
Now, we know that the backlash wasn't successful, right?
I mean, he's on a stamp today.
The backlash was not successful.
Did society collapse?
No, right?
We all just evolved.
We got better.
We moved on.
But why were people so opposed to it?
We know now that society didn't collapse.
So what was the opposition?
It wasn't rooted in reality, right?
It wasn't rooted in any real tangible effect
that was going to occur.
It's just perception, roles, tradition,
peer pressure from dead people.
That's it.
That's what it was rooted in.
When he challenged those roles, maybe even unintentionally,
he upset those people who want everything to remain the same.
And they created these doomsday scenarios
about what was going to happen in the future.
And none of it happened.
We have the benefit of hindsight.
None of that occurred.
So how does this help us see the future?
Time moved on.
We changed.
But the more things change, the more they stay the same.
OK, can you think of any performers today
that are challenging roles, that have people screaming,
oh, think about the children, that
are just doing their thing?
If you are one of those standing outside of those performances,
understand you're going to look as silly as the people who
protested Elvis.
That's what the future has in store for you.
You will be ridiculed by somebody in the future
because your concern is based on tradition.
Your concern is based on peer pressure from dead people,
these outdated roles that you want people to fit in.
That's what it's based on.
It's not based on anything like real life.
At some point in the future, there
will probably be one of those people on a stamp.
Something that that group of people
who are out there protesting and raising a moral panic,
something that they might want to take to heart that
came from both Elvis and Jesus.
Don't be cruel.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}