---
title: Let's talk about Ukraine, tanks, and sliding cards....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q8jItrLlIS8) |
| Published | 2023/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on Ukraine and discussing developments involving tanks, especially the Abrams.
- International dynamics in providing tanks to Ukraine, diplomatic reservations, and the involvement of countries like the United Kingdom, Poland, Finland, Germany, and the United States.
- The significance of tanks in the conflict with Russia potentially restarting the invasion.
- Concerns about the effectiveness and logistical challenges of using Abrams tanks in Ukraine.
- Debates and considerations around providing military equipment to Ukraine and its implications on Western resolve and Russian strategies.
- Logistics concerns for Americans regarding ongoing support for the tanks.
- Chatter about an upcoming offensive in Ukraine and leadership changes in the region.
- Speculation about a new order from Russian leadership to shave and its potential reasons.
- Assumptions and concerns about Russia's military capabilities and preparations in the conflict.
- Anticipation for further intelligence on developments in Ukraine.

### Quotes

- "Providing tanks is a big deal. We're all holding hands and crossing the street together."
- "No inspection-ready unit has ever made it through combat. And no combat-ready unit ever makes it through inspection."
- "We'll have to wait and see how it plays out."
- "There's definitely a debate that is occurring right now."
- "It's just a thought."

### Oneliner

Beau provides insights on the international poker game of providing tanks to Ukraine amidst concerns about restarting invasions and logistical challenges, with debates on Western assistance and Russian strategies.

### Audience

Military analysts, policymakers, activists

### On-the-ground actions from transcript

- Contact organizations supporting Ukraine for ways to provide assistance (suggested)
- Stay informed about developments in Ukraine and advocate for diplomatic resolutions (implied)

### Whats missing in summary

Detailed analysis and in-depth context on the ongoing conflict in Ukraine and the strategic implications of providing military support.

### Tags

#Ukraine #MilitaryAid #Tanks #InternationalRelations #Conflict #Russia #NATO


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to provide a pretty big update
on Ukraine and what's happening there and the developments.
And we're going to talk about tanks.
In a recent video about the Bradley,
the one with all the jokes that fell flat,
I mentioned how once those were underway,
we would end up talking about the Abrams.
So today, we are going to talk about the Abrams.
One of the interesting things about the developments
over there is that we get a really good look
at that international poker game where everybody's cheating.
Because it's playing out, the sliding of cards and the deals
that are being made.
And it's semi-public.
So we'll kind of go through with it.
Remember that tanks are, there's a diplomatic hesitation
to provide them to Ukraine.
There's a lot of reservations about it.
It's why it was so important that the Bradley wasn't a tank,
even though it looks like one.
So who started us off?
Who started us off as far as this next hand?
The United Kingdom.
The United Kingdom was like, hey,
we'll give up some challengers, give them to Ukraine.
Those are their modern tanks.
Then Poland and Finland were like, hey,
we've got some Leopards.
And Leopards are their modern tanks.
And probably out of all of them, those
would probably be the best for use in Ukraine.
But this is where the poker game starts.
Even though they are Polish or Finnish tanks,
they're of German production.
And Germany gets kind of like veto power on export.
So Poland and Finland can really only give them to Ukraine
if Germany says it's OK.
And Germany has kind of looked over at the United States
and basically said, yeah, we're all playing this hand.
Because again, providing tanks is,
there's a lot of reservation.
So the reporting suggests that Germany is willing to say,
yeah, let's do it.
But the US has to send Abrams.
Now, do these tanks matter?
Yeah.
There's a lot of chatter about the possibility of Russia
basically trying to get a do-over and restart
the invasion.
They realize that they've kind of hit a wall.
And they're looking to basically re-energize the war.
They're looking to basically re-energize the invasion.
When they do that, they'd be using a lot of tanks,
assuming they have some.
Now, one of the things we have learned
throughout this conflict is that the Russian tanks
aren't that great.
The tanks that would be sent, they're really good.
So it would provide Ukraine an edge.
Now, as far as the hand, where does the United States sit?
The Pentagon's policy advisor here says,
I just don't think we're there yet.
The Abrams tank is a very complicated piece of equipment.
It's expensive.
It's hard to train on.
It has a jet engine, all the stuff
I said in that last video.
That's where they're at.
I don't know that that's going to hold,
because I don't think Germany is looking
for a huge commitment.
I think this is really more of, this is a line,
providing this kind of equipment is a big deal.
We're all holding hands and crossing the street together.
And I would imagine that a platoon would actually
satisfy Germany's requirement, the condition
that they want fulfilled.
I don't know what the US is going to do,
but this is one of those things that has multiple schools
of thought with it, because there are some people who
are incredibly worried, not just about the effectiveness
of an Abrams there and the logistical train that
has to follow, but also what happens
if things go poorly for Ukraine.
Then Russia may end up with their hands
all over the better stuff.
The flip side to that, the competing argument,
is that at that poker game, this is kind of like NATO saying,
OK, well, we're going to raise by $50,
knowing that Russia only has $20.
Russia doesn't really have anything
that can compete with this stuff, and they know it.
I would imagine that inside the Kremlin,
there has been a desire to basically wait out
Western resolve as far as assisting Ukraine,
and just wait until the West gets tired of it.
If this type of equipment is committed,
it kind of sends a signal that that's not going to happen.
So there's definitely a debate that is occurring right now.
Which side wins?
Which side is right?
I don't really know.
Now, the only other concern, particularly for Americans,
is the logistics train.
The Abrams is complicated.
This is one of those things that might require
ongoing logistical support, and that's one of those things
that the Biden administration probably wants to avoid,
because that's a real quick step to having people there,
which is something the administration has made it clear
that they don't want to take that step.
So there's all of that.
That is what is going on with the tanks.
Now, there have been some other developments.
One is the chatter about the offensive.
We'll have to wait and see how it plays out.
It's worth noting that a lot of the equipment that is being sent
will be there before this is supposed to happen,
by the rumor mill.
Now, another piece of this is we recently talked about
the leadership changes there.
And there's some concern dealing with a new order
that has come down from the new Russian leadership,
which is to shave.
Now, there are two logical reasons to give this order.
The first is maintaining a professional appearance
in a war zone.
OK, Patton.
And the other is you need to be able to seal your mask.
Now, of course, because sensationalism drives clicks,
you are hearing a whole lot more about the possibility of them
needing to use masks and the use of chemicals
or something like that.
Based on some of the other orders,
my gut tells me it's the other thing.
It's a desire to have a professional-looking military
in a combat zone, which, if that's the case,
don't interrupt them.
No inspection-ready unit has ever made it through combat.
And no combat-ready unit ever makes it through inspection.
So I'm fairly certain that that's the way it would go.
I don't think that they're readying for anything involving
chemicals, mainly because we've seen their equipment.
Their masks, they're like GP.
They're really old gas masks.
I wouldn't trust them.
A lot of the equipment that they have that's
supposed to be sealed, it's not.
It wasn't maintained.
I don't know that they're up for it.
At the same time, I'm a little concerned about thinking that
because the times I have been wrong in my assessments
in this conflict are because I thought Russia
was too smart to do something.
So it's a concern, but I don't think that's where it's headed.
We'll have to wait and see.
There will be intelligence about newer equipment moving forward.
So that's kind of the whole lay of the land right now.
Now, Ukraine is probably also preparing for a newer offensive
to renew things.
But we're going to have to wait, see what equipment is coming in.
We should find out probably end of this week or the week
we should find out probably end of this week, early next.
And that will shape their plans.
So we have a lot of developments,
but there's not a lot of final reports on this yet.
So we'll hear more about it next week.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}