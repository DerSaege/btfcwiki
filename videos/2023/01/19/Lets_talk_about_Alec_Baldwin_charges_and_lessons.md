---
title: Let's talk about Alec Baldwin, charges, and lessons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=k_724XKGA9o) |
| Published | 2023/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of the Alec Baldwin case, pointing out that both Baldwin and the armorer from the set of Rust are facing charges.
- Contrasting the differences in how involuntary manslaughter charges are applied in New Mexico versus Georgia, leading to different perceptions.
- Explaining that involuntary manslaughter charges in New Mexico carry a maximum sentence of 18 months, while in Georgia, it could result in a decade of imprisonment.
- Mentioning that the prosecution believes Baldwin and the armorer didn't exercise enough caution during a lawful act, resulting in a death, which will be decided by a jury.
- Not forming an opinion on guilt or innocence until more details emerge and leaving it up to the jury to decide.
- Anticipating that the gun crowd will heavily criticize Baldwin and stress the importance of negligence over accidents in such incidents.
- Emphasizing that unintentional shootings resulting in death are unfortunately common in the United States due to negligence and lack of caution.
- Calling for the gun crowd to take proactive steps in teaching gun safety to prevent such incidents from occurring.
- Expressing doubt in the gun crowd's willingness to take action but hoping for a change in response to such incidents.
- Concluding with a thought on the dangers of assuming a gun is unloaded, urging caution and awareness.

### Quotes

- "There are no accidents, only negligence."
- "The most dangerous gun in the world is the one that you're pretty sure is unloaded."

### Oneliner

Beau clarifies differences in involuntary manslaughter charges, raises concerns about negligence in unintentional shootings, and urges the gun crowd to take proactive steps in teaching gun safety.

### Audience

Gun owners, activists, advocates

### On-the-ground actions from transcript

- Advocate for proactive gun safety education within your community (implied)
- Take steps to ensure firearms are handled with caution and proper safety measures (implied)

### Whats missing in summary

Deeper insights into the implications of negligence in firearm handling and the importance of proactive gun safety education.

### Tags

#AlecBaldwin #InvoluntaryManslaughter #GunSafety #Negligence #CommunityPolicing


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Alec Baldwin and what to expect with all of this,
some differences that people are noticing, and we'll clarify that.
And we're going to talk about the discussion that is likely to take place
and how people are likely to view this, and a teachable moment,
and something that's really worth going over when something like this happens,
because hopefully it can stop it from happening again.
Okay.
So if you don't know, Alec Baldwin and the armorer or weapon specialist
from the set of Rust are going to be charged.
An assistant director has kind of already pled out to a lesser charge.
The charge that Baldwin and the armorer are looking at is involuntary man,
involuntary manslaughter.
Now this led to a whole bunch of questions from people from the South,
particularly those in Georgia.
It's not the same.
And I don't even know that the laws are really that different.
It's more how they get used, so it creates a very different perception.
So in New Mexico, involuntary manslaughter in the way it is being charged here,
the max sentence is 18 months on the base offense.
It's a low-level felony.
I'm sure there are enhancements that could be applied,
and it may be at play in this case because a firearm was in use.
But it's not the same as the way it gets used in Georgia.
For those outside of the Southeast, involuntary manslaughter in Georgia,
I don't know, you're looking at a decade.
Alec Baldwin is not going away for 10 years.
That's not what's occurring here.
They're different.
Okay.
So what's next?
The prosecution believes they can show that during a legal, a lawful act,
something they were allowed to do, they didn't exercise enough caution
because the lawful act was dangerous, and somebody died.
That seems like a really low bar.
It seems kind of obvious that that occurred on the set.
The question is, is it Baldwin and the armorer who are responsible for that?
That's going to be decided by a jury.
The details of that, based on what we know, there's a whole lot of questions that I have.
So as far as guilt or innocence, I'm leaving that up to the jury,
or will not form an opinion until at least a whole bunch more comes out.
Now, one of the things that you are likely to hear in the discussion about this is the gun crowd.
The gun crowd is going to just throw Baldwin to the wolves on this one.
And you're going to hear the phrase,
there are no accidents, only negligence, over and over and over again.
You're going to hear this for two reasons.
One, it's an incredibly common saying in the gun crowd.
Two, the gun crowd doesn't like Alec Baldwin.
So they're not going to defend him in any way, shape, or form.
Now, you will hear a bunch of,
this is what should have happened on set to prevent this from the gun crowd.
They're going to talk about it at length, about all of the things that could have been done
to stop this unintentional shooting from occurring.
Okay, here's the thing, and this is where it's a very teachable moment.
There will be an unintentional shooting resulting in death today, and tomorrow, and the next day, and the day after.
In the United States, they occur at a rate of a little more than one per day.
There's like 480 a year.
The same advice that the gun crowd is going to give,
at least 480 people a year didn't use it.
The unintentional shootings that occur, they occur for all of the same reasons.
Because proper caution wasn't exercised.
Because people didn't clear a weapon.
Because weapons weren't secured.
All of the advice that the gun crowd is going to say those Hollywood liberals need to take, they don't take.
And it carries a cost of a person a day that doesn't make it.
There's more unintentional shootings than that.
These are just the ones where somebody doesn't make it.
It's way more common than it should be, or it has to be, because there are no accidents, only negligence.
That's a conversation that if the gun crowd had any real leadership, would be occurring right now.
All of those people who talk about, you know, we need to teach gun safety.
This is where you talk about it. This is your opening.
It happens all the time. It just normally doesn't have a celebrity involved.
So hopefully that'll occur.
It seems unlikely. I don't have a lot of faith in the gun crowd taking proactive steps.
But maybe they will this time.
If they don't, just remember, the most dangerous gun in the world is the one that you're pretty sure is unloaded.
Anyway, it's just a thought. You know, have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}