---
title: Let's talk about the differences between the Biden and Trump cases....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ioNkslE70SA) |
| Published | 2023/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains differences between the Biden and Trump document cases, criticizing the lack of context in most media explainers.
- Emphasizes that the crux of the matter is not the numerical comparisons of documents but the context and actions taken by each team.
- Contrasts the moments when both sides became aware of the documents: National Archives informing Trump's team vs. a lawyer finding a document for Biden's team.
- Details Team Biden's actions to ensure proper authorities received the document, initiate a search for more documents, and preserve national security.
- Points out the lack of information on what Team Trump did after they were aware of the documents.
- Notes that Team Trump attempted to keep the documents out longer, potentially increasing damage to national security.
- Stresses that the attempt to retain documents after awareness is the critical factor, not the number of documents involved.
- Suggests that some explainers may be prolonging the scandal to sensationalize and boost ratings.
- Summarizes the difference as Team Biden working to return documents while Team Trump sought to keep control, potentially harming national security.

### Quotes

- "Team Biden used their lawyers to try to get the documents back where they're supposed to be and limit harm to national security."
- "Team Trump used their resources to try to retain control and leave them out in the wild, increasing damage to national security."

### Oneliner

Beau breaks down the critical difference between the Biden and Trump document cases: Team Biden returned documents, Team Trump tried to retain control, potentially harming national security.

### Audience

Concerned citizens, political observers

### On-the-ground actions from transcript

- Contact your representatives to demand accountability for any mishandling of sensitive documents (implied)
- Support transparency and accountability in government actions (implied)

### Whats missing in summary

Insights on the potential legal implications and consequences for mishandling government documents.

### Tags

#DocumentCases #Biden #Trump #NationalSecurity #GovernmentAccountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the differences between the Biden document case and the Trump
document case.
We're going to do this because I've seen some explainers from a lot of outlets on it, but
they're kind of lacking in context, most of them.
They're making numerical comparisons.
You know, Trump is worse because he had more documents type of thing.
Meh.
That's not really the crux of the matter, to be honest.
And then I got a message saying, hey, I'm going to visit my super Republican parents
this weekend.
I know this is going to come up.
Can you give me a break it down into crayons version of what the biggest difference is?
The biggest difference is the context that a lot of those explainers are missing.
So this is what you do.
Start with the moment that both sides became aware of the documents.
And be generous when you're talking about Team Trump.
Don't assume that they knew they took them from the White House.
Just pretend like it was an accident.
They found out when the National Archives was like, hey, you've got stuff that belongs
to us.
That's the starting point for Trump.
The starting point for Team Biden is when the lawyer was looking for the boxes or whatever
and found one.
What did Team Biden do from that point?
By all accounts, Team Biden made sure that document found its way into the hands of the
proper authorities and then used personal resources to initiate a search of everywhere
to find out if there were more documents in the wild and preserve national security and
mitigate damage.
That is what Team Biden did.
All accounts, every account that we have says that.
What did Team Trump do?
We don't have that.
We looked, couldn't find it.
It's all been returned.
Those are mine.
They used their resources in an attempt to keep the documents out in the wild longer,
reducing damage to national security, not mitigating the harm.
That's the difference.
That's the big difference.
That's the one that matters.
That's the one that might lead to criminal liability.
That's the difference, not the number of documents.
Make no mistake about it.
Let's say Trump had 10,000 documents, and the National Archives says, hey, you have
some stuff that belongs to us, and some of it is SCI.
At this point, if Trump and his team are like, come get it, come get it right now, we will
lock everything down.
You come and find whatever it is you think is missing.
And Team Biden had one document, and they found out about it, and they're like, no,
this is ours.
We're not giving this up.
Team Biden would be in trouble.
The number of documents isn't really the deciding factor, in which case is worse.
It's the attempt to retain the documents after they became aware of them.
That's the problem.
That's what has to be addressed.
Most of the explainers that I've seen, they don't delve into this.
It appears like they're trying to, it almost appears like they're trying to keep the scandal
alive for the Biden side of things, so they can both sides it and get the ratings.
I mean, that's how I read it.
I have no evidence to say that's true, but that's the only reason I can think of to
not actually explain that it's the willful retention part that's the problem.
So you want a meme-style answer for your parents?
There it is.
Team Biden used their lawyers to try to get the documents back where they're supposed
to be and limit harm to national security.
Team Trump used their resources to try to retain control and leave them out in the wild,
increasing damage to national security.
That's the difference.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}