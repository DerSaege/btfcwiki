---
title: Let's talk about McCarthy as Speaker....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=00Bkf-f9Nrg) |
| Published | 2023/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Kevin McCarthy, presumptive speaker of the House, may be the weakest in American history.
- McCarthy made a major concession that weakens his ability to lead.
- Any five Republicans in the House can call for a vote of no confidence against McCarthy.
- The Speaker of the House doesn't have real power; representatives hold power over him.
- There are five MAGA-style Republicans actively opposed to McCarthy as speaker.
- These Republicans want to establish themselves as mega candidates by going against Trump's influence.
- McCarthy is facing division within the Republican Party and potential circus-like behavior in the House.
- By making concessions, McCarthy handed leverage and power to the rank and file of the Republican Party.
- If McCarthy becomes Speaker, he won't have real power and will act as a clerk for the party.
- McCarthy's potential speakership could lead to a House dominated by self-interested individuals chasing headlines.

### Quotes

- "He won't be able to lead. He will be basically the clerk for the Republican Party."
- "The House will be dominated by those who are most out for themselves."
- "Even if McCarthy becomes speaker, he's speaker in name only."

### Oneliner

Kevin McCarthy's potential speakership may leave him as a powerless figurehead, with the House dominated by self-interested members chasing headlines.

### Audience

Political analysts, Republican Party members

### On-the-ground actions from transcript

- Reach out to Republican representatives to express opinions on leadership (implied)

### Whats missing in summary

Insights on the potential impacts of McCarthy's speakership on the Republican Party and the House dynamics.

### Tags

#HouseSpeaker #RepublicanParty #KevinMcCarthy #AmericanPolitics #HouseLeadership


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the presumptive speaker
of the House, Kevin McCarthy, and the concession
that he made, and why, even if he wins,
he will probably be the weakest speaker of the House
in American history, at least for this term.
So the vote is tomorrow, and by my count,
it's a scene from Hamilton.
He doesn't have the votes.
Now, he has a day to try to kind of get his party in position,
but right now, he does not have the votes.
He made a major concession, and it
will be that concession that contributes
to his inability to lead.
Any five Republicans can call for a vote of no confidence.
Five, five Republicans in the House
can call for a vote of no confidence,
which, if successful, would lead to him being removed.
So that means that the Speaker of the House doesn't actually have any power.
The representatives have power over him in that position because any even remotely influential
representative can get five votes, especially given the fact that there are five MAGA-style
Republicans who are actively opposed to McCarthy as speaker, which really doesn't
make a whole lot of sense because Trump told them to support him. But they've
realized that Trump is losing influence and they know that if this goes in, if
if the ability to have that vote of no confidence goes in, that they can use
that to get headlines. They can't ride on Trump's coattails anymore, so they have
to make waves of their own, and this will be a way for them to establish
themselves as the mega-mega candidate. Kevin McCarthy, he's the leftist, it's
coming. Or a rhino. You've already got a number of them saying that he
doesn't believe in anything. He has no ideological basis, which I mean, maybe
that's true. I've never met the guy. But with all of this in play and dozens of
Republicans in the House unwilling to say how they're going to vote, it seems
unlikely right now that he has to vote. Maybe he can pull him in, but
But even if he does, with the division within the Republican Party, with the habit of Republicans
in the House wanting to create a circus, to get headlines, to get social media clicks,
which they for whatever reason, after all of these failures, still believe translate
into votes, he's going to be a disaster.
The House is going to be just a giant mess.
He handed, by making this concession, he handed the rank and file of the Republican Party
all the leverage, all the power, because they don't actually have to win the vote of no
confidence.
They just have to get one.
And every time they do, his political future falls a little bit further.
So it appears that even if McCarthy does somehow manage to pull this off and become Speaker
of the House, there's no power.
He won't be able to lead.
He will be basically the clerk for the Republican Party.
He will be trying to run their errands so he can have that title.
This is probably one of the last things the Republican Party needs because it creates
a situation where the House will be dominated by those who are most out for themselves,
most interested in grabbing headlines
and creating sensationalist scenarios
that they think translate into votes,
despite all of the evidence to the contrary.
But that's where we're at.
So we'll have to wait and see how the vote goes.
But even if McCarthy becomes speaker,
he's speaker in name only.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}