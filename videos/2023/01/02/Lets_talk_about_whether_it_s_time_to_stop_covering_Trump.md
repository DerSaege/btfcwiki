---
title: Let's talk about whether it's time to stop covering Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a-VlzYtEVqQ) |
| Published | 2023/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing whether 2023 is the year to stop covering Trump due to recent events, including the coup attempt.
- Asserting that historic events like the coup attempt should continue to be talked about and not forgotten.
- Noting that Trump's attempts to hold a press conference with no cable news outlets showing up indicates his dwindling influence.
- Drawing a parallel to a 1924 article about Hitler being "tamed by prison" and the importance of continuing coverage until there's a conclusion.
- Emphasizing that those who enabled Trump and similar figures are still in power, indicating a need to keep covering them.
- Stating that Trumpism will outlive Trump and continue under new names or figures if coverage stops prematurely.

### Quotes

- "The coverage has to continue until there's a conclusion."
- "They may rehabilitate their image, but they showed you who they are."
- "Trumpism will outlive Trump."
- "It's not over. Make no mistake about it."
- "If the coverage stops, Trump becomes this bygone thing."

### Oneliner

Addressing the need to continue covering Trump and those who enabled him, as Trumpism will outlive him if coverage stops prematurely.

### Audience

Journalists, activists, citizens

### On-the-ground actions from transcript

- Continue staying informed about political events and figures (implied)
- Stay engaged in political discourse and hold those in power accountable (implied)

### Whats missing in summary

The full transcript includes historical parallels and a call to action to stay vigilant against authoritarian figures and their enablers.

### Tags

#Trump #Coverage #PoliticalAnalysis #Authoritarianism #MediaCoverage


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about whether or not 2023,
whether or not this is the year
that we can finally stop covering Trump.
It's a comment and a question that I'm seeing a lot.
And I'm gonna give you the reasons why,
and there's multiple, that the answer's no.
The answer is definitely no.
First is allegedly, you know,
there was that whole, you know, coup attempt thing.
Eventually there will be proceedings about that.
There will be more.
So that's gonna be in the news.
The next part is that,
I think that was a pretty historic event.
It will never stop being talked about.
It shouldn't be anyway.
That'll be in history books.
Those people who participated,
those people who enabled it,
they will be in history books.
And then there's the reason that people think
that it's time to stop covering him.
And it's worth noting, he tried to hold a press conference
and not a single cable news outlet showed up.
Like not even Fox or whatever that other one is,
Newsmax or whatever, they didn't even show up.
I mean, that's gotta hurt.
And when you hear stuff like that,
and you hear about his dwindling influence,
and you hear about it on this channel,
I understand why people are like,
well, then why are we still covering him?
Because he does look weak, diminished, tamed.
I get it.
I get it.
To answer that, I wanna read you an article.
Trump tamed.
Donald Trump, once the demigod
of the reactionary extremists, flew home today
and immediately left in a limo for Mar-a-Lago.
He looked a much sadder and wiser man today
than on January 6th, when he and other radical extremists
appeared to engage in an attempt
to overthrow the government.
His behavior since has convinced people
that he, like his political organization,
are no longer to be feared.
It is believed he will retire to private life
and return to Austria, the country of his birth.
Oh, sorry, that's not about Trump.
That's about the guy who led Germany during World War II.
After his failed attempt, not to be feared, tamed.
So actually the headline, Hitler tamed by prison.
Copyright 1924, The New York Times Company.
It's December 20th, if you wanna look it up.
Obviously, I changed a lot of the wording,
but read the article.
The fact that he is diminished, that he's weak,
that he appears tamed and without influence
doesn't mean that we can stop covering.
The coverage has to continue until there's a conclusion.
If the coverage stops and he stops being baggage
for those who enabled him,
they begin their rehabilitation process,
not of like their actual character or behavior,
but of their image, and they get ready for next time.
It is important to remember that it certainly appears
there are people who enabled and encouraged this
who are still in power today.
I know it seems like it's been forever.
It's been a while, but these things don't stop.
Trumpism will outlive Trump.
It will continue under a new name,
a new authoritarian figurehead, some Xerox copy of Trump,
maybe a little bit more polished,
but it's still going to be there.
And it is much more likely that they're successful
if Trump doesn't become baggage,
if his actions aren't linked to those who would do it.
They may rehabilitate their image,
but they showed you who they are.
They were down.
So, no, I'll still keep covering him and those like him,
all of those people who enabled him.
It's not over.
Make no mistake about it.
They're scrambling, trying to figure out what to do,
but many of those people who enabled this
are still in positions of power today.
If the coverage stops, Trump becomes,
you know, this bygone thing.
He's no longer linked to the people in power today
who were willing to do his bidding.
So when the next authoritarian comes along,
they might just do their bidding as well.
And they've had practice.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}