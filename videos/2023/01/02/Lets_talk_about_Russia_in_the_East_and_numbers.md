---
title: Let's talk about Russia in the East and numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cAMqsaPGsg0) |
| Published | 2023/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the assumptions about Russia's intentions in Ukraine, pointing out flaws.
- Russia initially aimed to take the whole country, not just east of the river.
- Disputes the claim that Russia had support from the people east of the river.
- Mentions partisan activity in Russian-occupied areas as evidence of lack of support.
- Raises concerns about Russia's troop numbers for successful occupation.
- Notes that even with altered victory conditions, Russia lacks the troops to occupy successfully.
- Emphasizes that ongoing fighting prevents troops from being utilized for occupation.

### Quotes

- "They absolutely tried to take the capital and they failed."
- "The support that they're pretending they have among the people there, it doesn't exist."
- "Even with those numbers, they don't have it because the fighting is going to continue."
- "It's not post-conflict yet."
- "So anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau disputes assumptions about Russia's intentions and lack of support in Ukraine, questioning the feasibility of successful occupation given troop numbers and ongoing fighting.

### Audience

International observers

### On-the-ground actions from transcript

- Contact organizations supporting Ukraine (implied)
- Monitor the situation in Ukraine and Eastern Europe (implied)

### Whats missing in summary

Insights on the potential implications of Russia's actions in Ukraine and the ongoing conflict.

### Tags

#Russia #Ukraine #Occupation #TroopNumbers #PartisanActivity


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about another question that
has come up regarding Ukraine and numbers
and how many people are there and all of that stuff.
It's come up a couple of times.
And it's based on two assumptions.
Now, I have problems with both of these assumptions.
don't think they're valid and we'll go over the problems with the assumptions
but then we'll pretend like those problems don't exist and we'll go from
there. Okay so the general idea is that Russia didn't want the whole
country that's why they didn't send in that many troops they just wanted east
of the river and they you know they have the support of the people there. Okay so
So the two assumptions are that that's what Russia wanted at the outset.
They just wanted east of the river and that they have support.
I don't believe either of those are true.
I would point out that Russia took land west of the river.
It's just been kicked out of it now.
The Russian plan when this started was to take the capital.
They were trying to take the whole country.
They failed.
Now could they have altered victory conditions and now they're saying we're just going to
take east of the river?
Sure, sure, that's true.
But the idea that that was their plan at the outset and that's why they used so few troops,
that's just not true.
That's not real.
They absolutely tried to take the capital and they failed.
tried and they succeeded in taking some dirt west of the river, and then got pushed out.
So the idea that that's what they were originally intending, it's false.
That's 100% objectively false, all right?
Now let's go to the idea that they have support, they have the support of the people east of
the river.
I don't believe that.
I do not believe that.
During this conflict, we have talked on this channel
a couple of times about the partisan activity
in Russian occupied areas, areas that Russia was holding,
and partisans, irregular forces, allied with Ukraine
were still fighting behind the lines.
That's what happens if you have support in an area.
A lot of that was in the East, by the way.
But more importantly, there are sections that Russia took and then got pushed out of that
are east of the river.
Do you know what we don't see there?
Partisan activity loyal to Russia.
They don't have that support.
The support that they're pretending they have among the people there, it doesn't exist.
not real. So even though I don't believe either of the assumptions that this
statement is based on, let's roll with it. Let's just say, okay, we're gonna
pretend that they do have some support and that that was really all they were
trying to do. Do they have the numbers? In real life, you know, when we did the
troop number requirement estimate before. We pretended that they hadn't suffered any losses,
nobody was wounded, nobody was captured, and we were using the whole force that they committed.
In real life, knowing what we know about their losses, about who's been captured, and all of
that they probably do they have the numbers to occupy that area successfully
by that ratio and that ratio there were a couple of people who were like well
what about this conflict everyone that was thrown up the ratio determined
whether or not it worked that ratio is very accurate so they probably do have
the troop numbers currently to occupy that area successfully. There's a problem
though. That ratio is based on a post-conflict scenario after the
fighting is over. If they are not going to take the country, the fighting
continues. So the troops they have, they can't occupy and be on the line at the
same time. So even with the altered victory conditions of it just being the
East, they still don't have the troops. It would take Ukraine saying sure you
can have that and no longer fighting and nobody in the Ukrainian side outside of
the Russian occupied area assisting those in the occupied area. They still
don't have the troops. Even with those numbers, they don't have it because
the fighting is going to continue. So it's not post-conflict yet. So those troops
can't be pulled off the line to engage in normal occupation duties and even in
the event of a peace deal of some kind any partisans in the new Russia colony or
whatever you want to call it the area east of the river they would be getting
support from Ukrainians on the other side it's even the altered victory
The conditions do not create a scenario where they have the troop numbers they need to successfully
occupy after the fighting.
So anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}