---
title: Let's talk about Social Security, liberals and leftists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OAPm9j9GG8g) |
| Published | 2023/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans will inevitably target Social Security, wanting to make changes like cutting it or raising the age, as part of a long process to chip away at it.
- Liberals and the left must be unified when defending Social Security, despite potential disagreements over phrasing and motivations.
- Leftists may be upset with liberals not for what they want to do, but for why they want to do it.
- Liberals tend to argue that Social Security is not welfare, but something they paid into and are entitled to, while leftists stress that everyone deserves a decent life, regardless of contributions.
- Leftists aim to prevent the conditioning of viewing a difference between welfare and earned benefits, advocating for the idea that everybody deserves a decent life.
- Social Security is not solely for those who worked their entire lives; it serves broader purposes, reinforcing the notion that everybody is entitled to a decent life.
- Leftists urge for unity in defending all social safety nets and promoting the concept that everyone deserves a decent life.
- Advocates should approach the defense of Social Security positively, focusing on the idea that regardless of circumstances, everyone is entitled to a decent life.
- Referencing the Constitution's goal to "promote the general welfare" supports the argument for protecting Social Security as a vital safety net.
- Collaboration between liberals and leftists is vital in facing potential Republican attacks on Social Security.

### Quotes

- "Leftists will be upset with liberals."
- "Everybody deserves a decent life."
- "Everybody's entitled to that."
- "Say yes, and everybody, regardless of situation, is entitled to a decent life."
- "This is a moment where liberals and leftists, you have to work together."

### Oneliner

Republicans target Social Security, urging liberals and leftists to unite in defending its importance and promoting that everyone deserves a decent life.

### Audience

Advocates, activists, citizens

### On-the-ground actions from transcript

- Unite liberals and leftists in defending Social Security (implied)
- Advocate for the idea that everyone deserves a decent life (exemplified)

### Whats missing in summary

In-depth examples and analysis on the potential consequences of failing to defend Social Security fully.

### Tags

#SocialSecurity #Unity #Liberals #Leftists #GeneralWelfare


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about what to do
when the Republicans inevitably come for social security.
They're gonna wanna do something.
Cut it, raise the age.
They're going to want to make changes
because it's a years-long process
for them to just chip away at it.
But when that occurs,
liberals and the left have to be unified.
liberals and the left have to be unified.
The problem is because of phrasing and motivations,
The problem is because of phrasing and motivations,
people are gonna wanna argue.
people are gonna wanna argue.
People are gonna wanna argue.
Leftists will be upset with liberals.
And if you haven't seen it,
we have videos on the difference between liberals and leftists.
Leftists are going to be upset with liberals,
not for what they wanna do,
but why they want to do it.
We've talked about the left's habit of saying,
We've talked about the left's habit of saying,
no, you're wrong.
This is the right take.
This is one of those moments where improv rules apply.
You don't say no, but you say yes and.
The instinctive reaction for a liberal
is going to be,
this is not welfare.
We paid for this.
This is our money.
We're entitled to it.
Give it back.
This is where the leftist is gonna come out of the woodwork
and be like, don't say that.
And they're going to talk about
how everybody deserves a decent life.
And they're gonna go all ideological
and get utopian on you.
And you're gonna lose it, right?
Why are they doing that?
Because the leftist doesn't want you to become conditioned
to see a difference between welfare
and something that was earned.
Because it's a very short step from that
to, well, it's okay to get rid of welfare programs,
stuff that wasn't earned.
When the reality is,
everybody deserves a decent life.
Everybody deserves the ability to get by,
whether they worked for 65 years or not.
Everybody's entitled to that.
And it's worth remembering
that not everybody on Social Security
worked their entire life and then started collecting it.
Social Security is used for other stuff.
So that's what the leftist is trying to get across.
They don't want you to become indoctrinated
by a Republican talking point
that starts to differentiate
between the types of social safety nets.
They want you on board with the idea
that everybody deserves a decent life.
For leftists, say it like that, please.
Don't start with an attack.
Say yes, and everybody, regardless of situation,
is entitled to a decent life.
Now, when you run into those people
who talk about the Constitution,
please remember it is a goal of the Constitution
to promote the general welfare.
And this definitely falls into this.
It seems an almost certainty
that the Republican Party
is going to come for Social Security.
This is a moment where liberals and leftists,
you have to work together.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}