---
title: Let's talk about a trend and ideological consistency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0B5iGRzwdeg) |
| Published | 2023/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how Switzerland and Finland can teach us about Ukraine and Russia.
- Points out the trend of people believing in something off and not liking the answers when exploring the roots of their beliefs.
- Addresses the idea of Russia having a national interest in invading Ukraine due to NATO's influence.
- Talks about the impact of Russia's aggression on countries like Finland and Switzerland.
- Mentions the issue of Russia's actions not being within international law.
- Challenges the pretext of NATO's presence on Russia's borders being a valid reason for Russia's invasion of Ukraine.
- Criticizes arguments defending Russia's imperialism based on historical factors and racial hierarchies.
- Raises questions about defending imperialism and supporting Russian influence over sovereign countries.
- Dismisses claims of Red Scare propaganda as a justification for opposing Russia.
- Condemns the idea of justifying military action based on another country's approval as condoning imperialism.

### Quotes

- "NATO cannot exist on Russia's borders."
- "Just because something is in a nation's national interest doesn't mean that it's the correct moral or lawful thing to do."
- "If you are saying that one country's interests do not matter unless they can be approved by another country, you're condoning imperialism."

### Oneliner

Beau explains how Switzerland and Finland's actions reveal insights on Ukraine and Russia, challenging beliefs and justifications for Russian aggression and imperialism.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support efforts to counter misinformation about international relations (suggested).
- Advocate for diplomacy and respect for sovereign countries' interests (suggested).

### Whats missing in summary

In-depth analysis of Russia's wider geopolitical losses beyond the provided pretext.

### Tags

#Ukraine #Russia #NATO #Imperialism #InternationalRelations


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Switzerland and Finland
and what they can teach us about Ukraine and Russia
because there is a trend that people seem to be
kind of falling into.
A lot of people genuinely may believe
something that's a little off.
And when they explore the roots of why they believe that,
they may not like the answers.
So here's a message.
You say the war is lost for Russia
and act like it's a good thing.
By your own definitions,
Russia had a national interest in invading Ukraine.
Ukraine was becoming too friendly with NATO and Russia
and Russia had to act to protect their interests.
NATO cannot exist on Russia's borders.
You also say Russia has lost some wider geological,
I'm assuming that means geopolitical, war,
but would never dare to specify how.
Okay, so that wider thing, I have entire videos on this,
but just to recap, as stated in your message,
the pretext that Russia used was too close to NATO.
Can't have NATO on our borders, right?
When the invasion happened, a bunch of countries,
particularly Finland, for the sake of this conversation,
are wanting to join NATO.
Finland shares a huge border with Russia.
It backfired.
It backfired.
The Russian aggression caused countries
that are close to it in like miles
to warm to NATO,
countries that would never have considered it before.
That's losing the wider geopolitical war.
Another example would be Switzerland.
Now, they're not joining NATO, but they just took a vote.
It was 14 to 11,
stating that if a country had Swiss military gear
and they wanted to export it to Ukraine,
well, that would be okay.
That wouldn't violate their famed neutrality.
Switzerland, a country that is known for being neutral,
even in cases where it really shouldn't have been,
it turned the world against Russia.
That's what I wouldn't dare to explain.
I have much more in-depth videos about this, though.
And then we have to go to the other thing.
By your own definitions, it's in Russia's national interest.
Yeah, okay, but in that video,
kind of allude to the fact that just because something
is in a nation's national interest doesn't mean
that it's the correct moral or lawful thing to do,
which is the case here.
Russia acting on those national interests,
it wasn't within international law.
And this is one of those situations
where it does come down to pirates and emperors,
and Russia is becoming the pirate here
because they weren't able to actually pull it off
and pull it off quickly,
that they're suffering the international backlash.
And then that pretext,
Ukraine was becoming too friendly with NATO,
and Russia had to act to protect their interest.
NATO cannot exist on Russia's borders.
Okay, and that's a cool talking point,
but NATO has existed on Russia's borders a really long time.
It doesn't make any sense.
If you actually put any real thought into it,
you realize it's a pretext.
Latvia, Estonia, Norway, since like the 40s,
since NATO's been on Russia's borders,
it's not what it's about.
That's a, it's a manufactured pretext,
but it's easy to sell.
It is easy to sell,
and people don't like the answers
as to why they fall for it.
If you believe this is true,
if you believe that Russia has a legitimate,
a legitimate national interest in invading any country
that is warming up to NATO,
where are the calls to justify Russia's invasion of Finland,
who's actually trying to join NATO?
It's not there, right?
Why?
Why?
I've asked this question numerous times.
You get two answers if you get anything at all.
One is this really creepy thing where it's like,
well, that's a Nordic country and Ukraine is Slavic.
I mean, generally speaking,
making like a hierarchical racial argument
is not something that an enlightened person should be doing,
but that's what it falls down to.
The idea that somehow Russia is destined
to lead the Slavic people or whatever,
and that's kind of odd.
The other one is this historical precedent idea
that Russia has say because Ukraine
used to be part of the Soviet Union.
That's odd because that's indicating a sphere of influence.
If you are saying that Ukraine has to do what Russia says
and that its own national interests
and its own political interests are below those of Russia,
then you're indicating that Russia has Ukraine
in a sphere of influence.
It's literally imperialism.
You're actually defending imperialism with that reasoning.
And if you were to apply it to anything else,
you would, most people who make this argument
fall on one side of the political spectrum,
and they wouldn't make it in any other situation.
Would you suggest that the British, as an example,
should still have control of the United States or India?
It's not how it works.
It's a sovereign country.
It has its own national interests.
You can back this idea if you want to.
If you're coming at it from the perspective
of you want a strong Russia
and a nationalist approach to Russia,
you can remain ideologically consistent and do that.
If you are backing this idea
under the idea of anti-imperialism,
you've got to do some work
in going through what you're actually supporting.
Even in the arguments that get made,
it's this historical thing.
Well, they used to rule them, so they should still rule them.
I mean, would you say that about any other empire?
No.
This is one of those things.
It's propaganda.
It's a holdover.
It's the propaganda from the Cold War,
where all of this area is Russia's sphere of influence.
The Soviet Union's sphere of influence
somehow transferred to Russia,
and they should still run all of that.
It's just not true.
And then you have the other argument
that shows up very, very rarely,
like so rarely that it kind of seems...
It's almost like a straw man.
It's not made often,
so we're not going to devote a lot of time to it.
But the idea that the push against Russia
is due to Red Scare propaganda.
I think that most people know Russia's not red.
Russia is not red anymore.
It is not communist.
It's a capitalist oligarchy.
So that's not really valid either.
If you are saying that one country's interests
do not matter unless they can be approved by another country,
you're condoning imperialism.
And once you cross that line into not just
are you going to condone it in theory and in diplomacy,
but you justify military action based on that,
you're on some ground that, ideologically speaking,
most people who make these statements don't want to be on.
So there are other videos that are way more in-depth.
There are a whole lot of other ways
that Russia lost the wider geopolitical war.
I just used the pretext that you provided,
saying that NATO can't exist on the borders.
Well, now, because of this, there's going to be even more.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}