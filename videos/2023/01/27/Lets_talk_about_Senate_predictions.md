---
title: Let's talk about Senate predictions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=173kt8sO1Qk) |
| Published | 2023/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent polls show the Democratic Party is not in a good position for the Senate in 2024.
- Advises the Democratic Party to ignore the polls as they are worthless and too early to predict.
- Points out that predicting outcomes in 2024 is premature due to many potential changes in political dynamics.
- Mentions factors like the economy, situation in Ukraine, and Supreme Court decisions that could impact voter preferences.
- Emphasizes that current political times are far from normal and unprecedented.
- Cites an incident where a high-ranking Democrat's child was involved in assaulting a cop, overshadowed by other news.
- Notes that some politicians predicted to hold their seats are under investigation, potentially affecting polling numbers.
- Stresses the importance for any party to focus on enacting good policy and candidate quality.
- Suggests that if polls were accurate, the focus wouldn't be on Senate defense but on retaking it.
- Criticizes polling models for not accurately reflecting shifts in voter turnout and behavior.
- Urges parties to concentrate on doing their job well instead of worrying about early polling data.

### Quotes

- "Ignore the polls, they do not matter, they're worthless."
- "It is way too early to even begin to start to assess what is going to happen in 2024."
- "We're not in normal political times."
- "Do the job. You do that, you don't have to worry about the polling."
- "Anyway, it's just a thought."

### Oneliner

Beau advises ignoring early polls and focusing on policy and candidate quality, citing the unpredictability of future political dynamics.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Focus on enacting good policy and supporting candidates (suggested)
- Disregard early polling data and concentrate on fulfilling responsibilities (suggested)

### Whats missing in summary

The importance of staying grounded in policy actions and candidate quality amidst political uncertainty.

### Tags

#Polls #2024Election #PoliticalStrategy #DemocraticParty #PolicyMaking


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about polling.
We're going to talk about polling for 2024 already.
We're going to do this because I got a message.
The polls that just came out show that the Democratic Party is not in a good position
for the Senate in 2024.
What advice do you have for the Democratic Party as far as formulating strategy?
Okay.
First piece, and this is the most important part, ignore the polls, they do not matter, they're worthless.
It is way too early, way too early to even begin to start to assess what is going to happen in 2024.
2024. There are a couple of reasons for this. First, I would just point out that a lot is
going to change in a normal political cycle this far out. It's just, it's wild to me that
there is even polling or predictions being made right now during normal political times.
There's the economy, the situation in Ukraine, all of this stuff will have an impact on voters,
Supreme Court decisions.
There's a lot of stuff that can change any prediction that is being made right now.
Number two, we're not in normal political times.
I hate to use the word, but we are in an unprecedented situation.
Just a couple of days ago, the number two in the House, in the Democratic Party, their
kid got picked up for assault on an LEO, assaulting a cop.
It barely made the news.
Why?
Because there's a lot of other stuff going on.
I would point out that some of the people that are predicted to hold their seats in
In some of this polling, they're currently under state or federal investigation.
Some of them we are waiting for a quote, imminent charging decision on.
I would imagine if they get indicted, that might change the polling numbers a little
bit.
We are, we're not in normal political times.
So these polls are not worth anything.
They really aren't.
Ignore them.
I would also kind of point out that the strategy at this point for any party should be to do
the job the American people sent you there to do, enact good policy, and focus on candidate
quality. That's what matters. Aside from that, there's nothing that you can really
do, but there is a third reason to ignore these polls and not try to formulate
strategy around them. If these polls were any good, you wouldn't be trying to figure
out how to defend the Senate, you'd be trying to figure out how to retake it.
That was just a few months ago, remember the whole red wave thing?
This is not information that I would base anything on.
The polling models aren't good.
I seriously doubt that over the last couple of months they figured out how to account
for the unlikely voter.
They figured out how to account for the younger generation showing up in numbers that they're
not predicting.
It is way too early to start looking at polling, and we're certainly not going to cover every
one that comes out.
Do the job.
Do the job.
You do that, you don't have to worry about the polling.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}