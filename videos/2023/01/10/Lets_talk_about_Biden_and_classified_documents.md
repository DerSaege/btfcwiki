---
title: Let's talk about Biden and classified documents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0KjxTZwj9YQ) |
| Published | 2023/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the discovery of documents at the Penn Biden Center, some of which are classified and from Biden's time as vice president.
- Compares the Biden document case to the Trump document case regarding classified documents.
- Notes the immediate action taken by Biden's lawyers to hand over the documents within 24 hours, contrasting it with the Trump case.
- Emphasizes that the classification level of the documents is key to determining the seriousness of the situation.
- Mentions the appointment of a US attorney by Garland to oversee the investigation into the Biden documents.
- Suggests that there may have been a lapse in the transition period between administrations, calling for a counterintelligence filter team.
- Advocates for a more rigorous process during transitions to prevent mishandling of classified documents by former presidents or vice presidents.

### Quotes

- "Comparable is a relative term."
- "If they are SCI documents, the counterintelligence teams need to go in and find out exactly what happened."
- "It's probably a good idea to have what amounts to a counterintelligence filter team there."

### Oneliner

Beau explains the differences between the Biden and Trump document cases, stressing the importance of the classification level and suggesting the need for a counterintelligence filter team during transitions.

### Audience

Policymakers, government officials.

### On-the-ground actions from transcript

- Establish a counterintelligence filter team during transitions (suggested).
- Ensure rigorous handling of classified documents by former presidents or vice presidents (suggested).

### Whats missing in summary

Importance of proper handling and oversight of classified documents during transition periods.

### Tags

#Biden #Trump #ClassifiedDocuments #Counterintelligence #TransitionPeriod


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the Biden documents,
because that's a thing now.
We're going to talk about that
and go over the one question that has flooded my inbox
since news of this broke.
If you don't know what happened,
some documents were discovered at the Penn Biden Center.
It's a think tank.
And they are from Biden's time as vice president,
and some of them are classified.
The question that has just flooded me is,
is this comparable to the Trump document case?
Comparable is a relative term.
Even though we use it as an example of two things
that shouldn't be compared,
you can compare apples to oranges.
I mean, they're both about classified documents,
but that's kind of where the similarities end.
Based on reporting,
it appears that Biden's lawyers found the documents,
picked up the phone, called like right then,
and were like, hey, you need to come get this
and get it out of my possession,
which is what's supposed to happen.
Literally immediately, within 24 hours,
literally immediately, within 24 hours,
and everything was handed over immediately.
So that in and of itself is a massive departure
from what happened in the Trump case.
There's also the matter of scale.
You're talking about what appears to be
an errant document set in the Biden case,
I mean, hundreds of documents in the Trump case.
It's a little different.
That being said, that isn't what matters.
The media, if there's one thing I've learned
over the last four or five months,
it's that the media doesn't understand classification levels.
Some of the reporting is saying
that they were marked classified.
Okay, then this doesn't matter.
This is a nothing burger.
However, just because that's what the reporting says,
that doesn't actually mean that they were marked classified.
They could have been marked something else.
I'm going to go back to what I've said
the entire time through the Trump case.
If you go back and look at those videos,
like the first one in the series is something like,
let's talk about the search at Trump's house
or something like that.
The next one is let's talk about Trump and TSSCI documents.
That's what matters.
To know whether or not this is even remotely comparable,
we have to know the classification level.
Throughout the thing with Trump,
I have focused on that particular set of documents
because that's what matters.
Those that were labeled TSSCI,
the compartmented information,
those are the big secrets.
The other stuff under Trump,
when we're talking about that case,
I've been like, yeah, it's not good, but I mean, okay.
The SCI stuff is what matters.
The same thing holds true here.
There's no difference.
If they are SCI documents,
if this set of documents from the Biden case,
if this is SCI, then yeah, yes,
the counterintelligence teams need to go in
and find out exactly what happened.
Yeah, there's no difference there.
The reporting suggests that this all occurred
back in November, early November,
and that Garland appointed a US attorney
who was nominated by Trump to oversee all of it.
Based on the length of time that has passed,
it's a safe assumption
that the decision about whether or not
to pursue a full investigation has already been made.
So there was no attempt to cover it up.
The documents were handed over immediately,
which kind of precludes the idea of an investigation
under willful retention.
It's not the same other than they involve classified documents.
Does that matter when it comes to the way Fox News
and places like that are going to try to frame it?
No, it absolutely doesn't.
I am fairly certain, based on the tone of the reporting
and how this didn't become a big news story for a long time,
that these aren't SCI documents.
So my gut here says investigate it.
There's not going to be anything there.
I don't think when it all shakes out
that it's going to be even remotely comparable.
Just on the surface, it's apples and oranges.
All of that being said, this is probably a clear indication
that there is a lapse
when it comes to the period of transition
between administrations.
It's probably a good idea to have what amounts
to a counterintelligence filter team there
to go through the documents and make sure
that a former president or vice president
isn't leaving with something that they shouldn't.
That seems like maybe it should start happening
between every administration.
Because even if they are the lower levels of classification,
it's still not good.
It's just not at the same level.
I have a bunch of videos from Trump's time
where I'm talking about SCI documents
and how much they matter, because that is what he had.
So I don't foresee the comparisons being valid.
I definitely see them being made, though.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}