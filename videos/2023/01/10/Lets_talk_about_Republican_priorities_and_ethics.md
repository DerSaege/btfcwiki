---
title: Let's talk about Republican priorities and ethics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KhA-_0R2LO4) |
| Published | 2023/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans, now in control of the House majority, prioritize going after the Office of Congressional Ethics to render it ineffective and destroy its ability to operate.
- Questions arise regarding promises made by Republicans to their voter base, such as investigating Pelosi, AOC, and the squad, which now seem unlikely to be fulfilled.
- The move to dismantle the Office of Congressional Ethics casts doubt on Republicans' accountability and transparency promises.
- Despite talking about targeting the Democratic Party during their campaign, Republicans appear hesitant to follow through on those intentions.
- Republicans' reluctance to allow the Office of Congressional Ethics to function may lead to repercussions from the Department of Justice, which previously took a hands-off approach.
- By undermining the office designed to keep promises of holding corrupt individuals accountable, Republicans risk alienating their voter base and betraying their trust.

### Quotes

- "What's more valuable than going after them? Protecting themselves, right?"
- "It seems like they lied. They just made it up."
- "They talk about it all the time. The corrupt Democrats. They throw that word out."
- "I think there are a whole bunch of people in the Republican Party who are about to find out they were duped."
- "Y'all have a good day."

### Oneliner

Republicans in control of the House majority prioritize dismantling the Office of Congressional Ethics, raising doubts about their transparency and accountability promises while potentially betraying voter trust.

### Audience

House constituents

### On-the-ground actions from transcript

- Reach out to your representatives and demand transparency and accountability in government (implied).
- Stay informed and hold elected officials accountable for their actions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of Republican priorities in the House majority and the potential consequences of dismantling the Office of Congressional Ethics.


## Transcript
Well, howdy there, Internet people. It's Bob again. So today we are going to talk about
day one priorities for the new House majority. The Republicans, that they've got control
of the House and they're going to move forward to help the American people. And what's their
first priority? What's the first thing they want to do? They want to go after the Office
of Congressional Ethics. They want to make it ineffective. They want to destroy its ability
to do its job. They want to gut it. I have questions. I have questions. And they're not
about ethics. It's a Republican majority. I've given up on that. But I have questions
about stuff they told their own base, promises that they made to their voters. What happened
to going after Pelosi, AOC, the squad as a whole? I thought the new majority was going
to look into all that. You know how they do it, right? All of those claims about Pelosi,
you know, using her position corruptly and all of that stuff and the claims about the
squad's backgrounds and all of that. They'd use the Office of Congressional Ethics to
look into all of that. Doesn't look like they have any intention of doing that. Seems like
they lied. They just made it up. It was just something to feed to the more easily manipulated
among their voter base. I mean, that's how it appears. I don't know any other way to
take it. When on day one, you put that office in a position where it won't be able to do
its job. I mean, that just, that seems like it kind of casts doubt on all of their promises
when it comes to accountability and transparency. Now, they were talking about going after the
Democratic Party when they were running, but doesn't look like they intend on doing that,
which is weird. You have to ask why? What's more valuable? If they believe this stuff
is true, what's more valuable than going after them? Protecting themselves, right? It's the
only option. Even if they don't believe it's true, it sure would make a good show, right?
But they're not going to do that. Because if that office exists, it is a threat to them.
Because they know some of the behavior that they've engaged in. So their only option is
to get rid of it. I think they're misreading the outcome of this, by the way. Because outside
looking in, it has always appeared to me that the Department of Justice kind of allowed
the Office of Congressional Ethics to handle its own house, so to speak. If that office
isn't functional, I don't know that the Department of Justice is going to be as accommodating
as it has been in the past. I don't know that, but that has always been my view of it. That's
why DOJ tended to kind of be hands-off, that they let Congress police themselves. Without
that office, I don't know that they're going to do that. But whether or not they misread
it doesn't matter. Because they certainly misrepresented their intentions to their own
voters. Because they told them that they were going to go after these people who did these
corrupt things. They talk about it all the time. The corrupt Democrats. They throw that
word out. But now they have the chance, and the first thing they do is go after the body
that they would use to keep their promises. I think there are a whole bunch of people
in the Republican Party who are about to find out they were duped when it comes to a whole
lot of promises that came from those candidates trying to get into that house. They're going
to realize that those politicians did not believe what they said, and they're certainly
not going to act on it. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}