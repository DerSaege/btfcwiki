---
title: Let's talk about Santos, McCarthy, and the new chapter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_qtHMVI0Yng) |
| Published | 2023/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about embattled Representative Santos and new developments
- Representative's credibility questioned in various ways
- Aide accused of posing as McCarthy's chief of staff to solicit donations for Santos' campaign
- Potential trouble for Santos if he knew about his aide's actions
- Potential consequences from messing with wealthy Republican donors
- Speculation on possible reactions from McCarthy and the Republican Party
- Imagining scenarios of how the truth may have come to light
- Likelihood of further developments and legal implications
- Ending with a casual sign-off and well wishes

### Quotes

- "Don't scam rich people."
- "I'm not even mad. That's impressive."
- "I don't know that's how this came out, how these allegations were generated. But I mean, that seems pretty likely to me."
- "There are already suggestions that some of these actions might have violated the law."
- "Y'all have a good day."

### Oneliner

Beau talks about the embattled Representative Santos, his aide's alleged actions, and potential consequences from messing with wealthy donors, speculating on how the truth may have surfaced and hinting at further legal implications.

### Audience

Political watchers

### On-the-ground actions from transcript

- Contact local representatives or organizations to advocate for accountability and transparency in political campaigns (suggested)
- Stay informed about the unfolding story and share relevant updates with your community (suggested)

### Whats missing in summary

Full details and nuances of the unfolding story and implications.

### Tags

#RepresentativeSantos #PoliticalScandal #WealthyDonors #LegalImplications #RepublicanParty


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk a little bit more about embattled Representative Santos
and the new developments with that.
If you're not familiar with this representative,
this is a representative whose credibility has come into question in a number of ways.
New reporting suggests that his aide may have broken the one rule in DC that matters.
Don't scam rich people.
The allegation is that the aide posed as McCarthy's chief of staff via email and phone
while talking to wealthy donors in an attempt to get them to give money to Santos' campaign.
That's... I mean, I'm not even mad. That's impressive.
Now, this is probably going to cause even more problems for Santos
if it is demonstrated that he knew about the alleged actions of his aide.
That would be really bad.
But even lacking that sort of evidence,
I would imagine that the Republican Party does not look kindly on messing with their wealthy donors.
I would imagine that's something that might prompt McCarthy to take some kind of action.
It might even generate the votes needed to expel him from the House.
So there's that.
But honestly, all I could think about when I'm hearing this coverage and I'm reading about it,
is all the different ways it could have been discovered.
Because I just have this image of one of these wealthy donors
seeing all the coverage about Santos and being just furious with McCarthy's chief of staff
for convincing him to give money to Santos and trying to call him.
And then when he doesn't answer the cell phone number that he has,
calls his old cell phone number, calls the office, and finally gets a hold of him
and just lays into him, talking about, you know,
I can't believe you put me in this position and blah, blah, blah, blah, blah.
And McCarthy's chief of staff has no idea what the guy is talking about.
He's like, I didn't tell you to give that guy money.
And then it all starts to surface.
I don't know that that's how this came out, how these allegations were generated.
But I mean, that seems pretty likely to me.
I would imagine that this is not the end to this story.
There are already suggestions that some of these actions might have violated the law.
There will almost certainly be more coverage of this unfolding story.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}