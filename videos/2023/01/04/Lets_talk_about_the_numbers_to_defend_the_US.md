---
title: Let's talk about the numbers to defend the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZkzRz35xYVY) |
| Published | 2023/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Many people are interested in troop numbers, especially in the United States, following recent events in Ukraine.
- The US military does not have enough troops to occupy the country, which has raised questions about defense strategies.
- The US relies on a doctrine of a strong offense and intelligence gathering for defense.
- Occupying the US is not feasible due to the vast number of troops it would require, even surpassing combined forces of major countries like Russia, China, and India.
- The idea of a foreign invasion of the US is highly unlikely due to the sheer number of firearms in civilian hands.
- The US military doesn't need to occupy all land but rather create a defensive line and trust in American citizens' support.
- The budget for defense is significant, but it's necessary to ensure national security against potential threats.
- Any potential invasion of the US would have to cross oceans and air, making it extremely unlikely.
- There is no need to worry about invasion threats to the US; the country is well-prepared to defend itself.
- The level of spending on defense is not aimed at preparing for an occupation but rather ensuring strong national defense capabilities.

### Quotes

- "The US is not at risk of invasion. There isn't a single country that has the troops."
- "The US military doesn't need to occupy all of the dirt. It just has to create a line and trust that the Americans behind them are going to stay on their side."
- "Be glad we're not spending enough to occupy the US because that, I mean, if you think the budget is bloated now."
- "No country, no military anywhere wants to invade a country that has more guns than people."
- "The US military lacks the ability to defend the United States."

### Oneliner

Beau explains why foreign invasion of the US is unlikely due to troop numbers and civilian firearms, affirming strong defense capabilities without needing to occupy the country.

### Audience

National Security Analysts

### On-the-ground actions from transcript

- Trust in the existing defense strategies and capabilities of the US military (implied)
- Advocate for responsible firearm ownership and safety measures within communities (implied)
  
### Whats missing in summary

The full transcript provides a detailed breakdown of why the US is well-prepared to defend itself against potential foreign invasions.


## Transcript
Well, howdy there, Internet people.
It's Bobby Young.
So we have yet another question about troop numbers.
I didn't think people would be this interested in this.
This one is related to the United States.
And I guess something I've said before on the channel,
but this time, I think because of what happened in Ukraine,
it kind of made people a little nervous.
The United States, the US military,
does not have the troops necessary to occupy
the United States.
The numbers just aren't there.
This led a number of people to ask, well,
how can the US military defend the US then?
Defending and occupation are very, very different.
Now, with the US, the United States
relies on a doctrine that suggests the best
defense is a strong offense and a lot of intelligence gathering.
The number of troops necessary to occupy the United States,
if being raised by a foreign power,
oh, it would be noticed.
You're talking about millions of troops.
It would be spotted.
You can't really do that in secret.
To put it into perspective, if you
took the militaries of Russia and China and India
and combined them, they still don't have the numbers.
Occupying the United States is like occupying China.
It's just not going to happen.
It's not a real thing.
I know that there's a whole lot of people
who have like Red Dawn fantasies who are really upset right now,
but it's just not a thing.
The number of troops necessary is staggering.
So if somebody was to attempt that,
the odds are the invasion force would be destroyed on or out.
It wouldn't even reach US shores.
But if it did, the US military doesn't
have to occupy all of the dirt.
It just has to create a line and trust
that the Americans behind them are
going to stay on their side.
So they don't need that full force to defend the country.
The other thing that is unique to the United States
when it comes to the worry that another country might invade
the US is something that is just purely American.
No country, no military anywhere wants
to invade a country that has more guns than people.
This is the one piece of like ridiculous pro-gun propaganda
that is actually true.
The just ridiculous amount of firearms
in civilian hands in the United States
kind of precludes any occupation really being successful.
Even though a lot of those arms are civilian,
you only have to use it once before you obtain something
better.
It's just the US is not at risk of invasion.
There isn't a single country that has the troops.
And even if you combine some of the largest
militaries on the planet, they still don't have the troops.
So the inability of the US to occupy the United States
does not mean that the US military lacks the ability
to defend the United States.
This question came in a number of ways.
Like one was like, well, why are we spending so much?
Be glad we're not spending enough to occupy the US
because that would, I mean, if you think the budget is bloated
now.
And then I feel like the other questions came
from a place of genuine concern, like the US is not
up to the task.
The US would meet any possible invasion
that would have to cross the oceans in the air.
This is not something that people should worry about.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}