---
title: Let's talk about preaching to the choir....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=exnNpEhMdSs) |
| Published | 2023/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a person on YouTube who feels like they are "preaching to the choir" due to constant attacks if they express self-doubt.
- Beau shares his experience of receiving hate mail to show that the person is not only reaching like-minded people.
- Questions the notion that preaching to the choir is pointless, mentioning pastors preaching to choirs even though they are already aware of the information.
- Emphasizes that sometimes the goal is not to convince others but to let those who think similarly know they are not alone.
- Suggests that reinforcing beliefs and keeping people engaged has value in moving towards a better world.
- Encourages the idea that even if you are speaking to like-minded individuals, your words can still inspire growth and progress.

### Quotes

- "I don't say this to convince somebody, I say it so those who already think like me know they're not alone."
- "It keeps people thinking about the larger issues. It keeps people engaged. It makes them push further, hopefully beyond you."
- "Maybe your sermon helps them to keep the faith. Helps keep them moving forward and spurs their ideological growth."

### Oneliner

Beau shares insights on the value of speaking to like-minded individuals and how it can contribute to progress and growth, challenging the notion of "preaching to the choir."

### Audience

Creators, Activists, Influencers

### On-the-ground actions from transcript

- Start a community group centered around shared beliefs and growth (suggested)
- Encourage ideological growth through engaging content creation (implied)

### Whats missing in summary

Exploration of the importance of solidarity, community building, and empowerment in shared beliefs.


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about another
person on YouTube and preaching to the choir because this person who has asked me not to
reveal who they are because, well, it makes sense. They exist in an ecosystem on YouTube that
any expression of self-doubt would just lead to constant attacks. So it tracks, it makes sense.
But they said they felt like they were preaching to the choir.
I was like, yeah, you know, I get that sometimes too. People tell me that. But I always just look
at my inbox and, you know, see the volume of hate mail I get and it lets me know that that's not
the case. Do you get hate mail? Well, yeah. You're obviously not only preaching to the choir.
And that seemed to make the person feel a little bit better. But then I started thinking about
that phrase and the way it gets used. It gets used to suggest that you're basically doing
something that's pointless. That's got to be a phrase that originated with people who aren't
familiar with the gossip inside churches about choirs or anybody, really. It's worth remembering
that the pastor does, in fact, preach to the choir. When they start preaching, they don't tell
the choir to leave. Even if they are aware of all of the information, it's worth hearing again.
A lot of times, you're not saying what you're saying in an attempt to convince people.
Convince people who feel differently. I mean, yeah, I think most people on YouTube, most people
who are political commentators at times will tailor their message to reach out. Some people
do it a lot more than others. But it's something that happens occasionally.
But even if that's not something that somebody does often, it doesn't mean it's pointless.
Because even if somebody is aware of that information, it's probably really good to hear it.
It's that old adage, I don't say this to convince somebody, I say it so those who already think like
me know they're not alone. I'm going to suggest that has a whole lot of value. It keeps people
thinking about the larger issues. It keeps people engaged. It makes them push further, hopefully
beyond you. And that's how you end up moving that Overton window. That's how you end up creating
that better world. That is why on a long enough timeline, we win. So when you hear that, and when
you hear people say you're preaching to the choir, it may be true. You may be preaching to people who
are predominantly already of your faith, ideologically speaking. But maybe you're a sermon
helps them to keep the faith. Helps keep them moving forward and spurs their ideological growth.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}