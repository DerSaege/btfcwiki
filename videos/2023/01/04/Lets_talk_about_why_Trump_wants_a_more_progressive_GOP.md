---
title: Let's talk about why Trump wants a more progressive GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vvrkGp9kw_s) |
| Published | 2023/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Donald J. Trump is showing signs of wanting the Republican Party to become more progressive, distancing himself from causes of midterm losses and blaming the party.
- Trump acknowledges mishandling issues like abortion that led to losing voters, signaling a shift towards a more progressive stance within the Republican Party.
- He points out that the Republican Party's static platform hasn't kept up with societal progress, becoming regressive and outdated.
- Trump's supporters, who have been socially regressive, may find it challenging to adapt to a more progressive direction the party might be heading towards.
- Beau suggests that for the Republican Party to have a chance in the future, they must embrace progressiveness as society evolves.
- While Trump seems to be recognizing the need for change, his followers may resist moving towards a more progressive stance on various issues.
- The Republican Party's outdated platform, offensive to many Americans, could lead to continued electoral losses unless they adapt to a changing society.
- Beau warns that as the party shifts its focus to different demographics, it's vital for individuals to stand up against discriminatory practices and policies.
- He stresses the importance of not turning a blind eye to harmful actions and policies targeting marginalized groups and urges people to be informed voters.
- Beau concludes by questioning whether individuals want leaders who resort to bullying and scapegoating, implying that the Republican Party has devolved into such behavior.

### Quotes

- "If the Republican Party wants to stand a chance in the future, they have to become more progressive."
- "It's about maintaining a group of people to kick down at."
- "You have to stand up to it. You can't look the other way."
- "He just wants to capitalize on it."
- "Y'all have a good day."

### Oneliner

Former President Trump shows signs of pushing for a more progressive Republican Party, urging a shift from regressive stances to adapt to societal evolution.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Stand up against discriminatory practices and policies targeting marginalized groups (implied)
- Be informed voters and look at policies rather than blindly supporting a party (implied)

### What's missing in summary

Importance of individuals actively participating in shaping the future of political parties through informed decision-making and standing against regressive practices.

### Tags

#RepublicanParty #Progressiveness #Trump #SocietalChange #Discrimination


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about former president,
Donald J. Trump,
indicating that he wants the Republican Party
to become more progressive.
I'm not joking.
It appears that the former president
is beginning to understand what happened
during the midterms.
And he is going out of his way to distance himself
from the causes of what happened during the midterms
and to try to affix blame within the Republican Party.
He's realizing portions of what we've been talking about
on this channel for months.
So what did he say?
He tweeted this out on his little make-believe Twitter.
It wasn't my fault that the Republicans
didn't live up to expectations in the midterms.
I was 233 to 20.
Okay, I'm gonna stop for a second.
The purpose of an endorsement is not to pick the winner.
The purpose of an endorsement is to swing votes
towards the candidate you want to win.
When you endorse a candidate that is already slated to win,
that really doesn't mean anything.
And that's what the bulk of his wins are,
picking Republicans in heavy red areas.
That's where his wins are.
When it comes to those hotly contested elections
where his endorsement was supposed to swing votes
towards the Republican candidate, it really didn't work.
So his record actually isn't that impressive
when it comes to what matters.
It's not a ticket at a racetrack.
You're not trying to pick those who are going to win.
You're trying to change the outcome.
And he didn't do well with that.
Anyway, back to what he said.
It was the abortion issue, in quotation marks,
poorly handled by many Republicans,
especially those that firmly insisted on no exceptions.
Blah, blah, blah, blah.
He goes on.
That lost large numbers of voters.
Also, the people that pushed so hard for decades
against abortion got their wish from the US Supreme Court
and just plain disappeared, not to be seen again.
Yeah, I mean, and then it also says,
plus Mitch stupid dollar signs.
I don't know what that means.
Okay, so the people who were adamant for this policy change,
they pushed for it for decades, and then they disappeared.
I mean, that's partially true.
There were a lot of people who were very active in this regard
who disappeared.
You know when they disappeared?
During the pandemic because of his faulty leadership.
But aside from that, it was never popular.
It wasn't popular.
The Republican Party adopted that platform decades ago,
and they didn't keep up with the times.
They didn't keep up with the times.
Society moved forward.
The Republican Party stayed static.
So now it's regressive.
It's backwards.
It's outdated.
Trump is beginning to understand this with this issue.
I wonder if he understands the same thing is going to happen
with all of the scapegoating of the LGBTQ community
or immigration.
It's the entire Republican Party.
It isn't just one plank in the platform.
He certainly appears to be calling
for a more progressive Republican Party.
There are some that are suggesting
this is him kind of teasing out what his third party platform
would be.
And I have to say that if he goes that route,
there have been people historically
who used a combination of nationalism and social ideas,
blending them together.
I mean, who saw that coming, really?
He's always been what everybody said he was.
So you have Trump starting to recognize this.
Now, one of the things that's kind of contrary to it
is that it's his adherence.
It's his followers who are most adamant about staying
socially regressive.
Maybe not on this particular issue,
but on the other ones that are going to have the same outcome.
The fact that the Republican Party
is apparently unaware of something
that even Trump figured out is mind boggling to me.
If the Republican Party wants to stand a chance in the future,
they have to become more progressive.
They don't have any options.
Society has moved on.
The Republican Party platform is antiquated.
It is outdated.
It is offensive to most Americans.
They will continue to lose elections
by larger and larger margins.
Because as time moves on, more and more people
will disappear, to use his term, and they
will be replaced by younger, more forward-thinking people.
If Trump has figured this out, I would
imagine that the rest of the Republican Party
isn't far behind.
It's important to remember, as positions evolve
from the politicians here, it's important to remember
that for them, the positions don't matter.
It's about maintaining a group of people to kick down at.
If they find out that kicking down at women
isn't as inspiring as it used to be to their base,
they will move on to another target.
If you want society to progress, if you
want to build that better world where everybody is getting
a fair shake, when they move to more heavily targeting
other demographics, you have to stand up to it.
You can't look the other way.
You can't continue to vote for an R
without looking at the policy.
They're going to come after your friends next.
They're going to lean even more heavily
into going after the LGBTQ community,
because while they have recognized this problem when
it comes to family planning, it doesn't
look like they've recognized it when it comes to other issues.
So they're going to continue to scapegoat.
The question you have to ask yourself
is whether you want leaders whose sole purpose is acting
like a playground bully and giving you somebody to pick on,
because that's what the Republican Party has devolved
into.
And even Trump is starting to see that.
He just wants to capitalize on it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}