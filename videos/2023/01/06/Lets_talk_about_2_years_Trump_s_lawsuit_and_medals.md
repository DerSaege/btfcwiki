---
title: Let's talk about 2 years, Trump's lawsuit, and medals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b85XHWwYIvs) |
| Published | 2023/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two-year anniversary of failed coup attempt marked by news breaking today.
- Attorney General's office statement on holding criminally responsible.
- Lawsuit against former President Trump for wrongful death.
- Biden to hand out citizen medals, framing incident as coup plotters vs. law enforcement.
- Michigan Secretary of State and others to receive medals.
- Republicans and Democrats both receiving medals for standing up to Trump's campaign.
- American history vs. American mythology being crafted around the incident.
- Reminder that authoritarian rhetoric and Trumpism still present.
- Slow progress in federal investigations causing frustration.
- Jack Smith's return may indicate movement in investigations.

### Quotes

- "American history vs. American mythology being crafted around the incident."
- "Reminder that authoritarian rhetoric and Trumpism still present."
- "Slow progress in federal investigations causing frustration."

### Oneliner

Two-year anniversary of failed coup attempt marked by slow progress in federal investigations and crafting of American mythology.

### Audience

American citizens

### On-the-ground actions from transcript

- Watch for updates on investigations and political developments (implied)

### Whats missing in summary

Insights on the impact of framing historical events as part of American mythology.

### Tags

#FailedCoupAttempt #FederalInvestigations #AmericanMythology #Authoritarianism #Trumpism


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about medals, and suits,
and statements, and history, and mythology,
and everything associated with the anniversary that's
occurring today.
There's a lot of news breaking today that relates to the failed
coup attempt two years ago.
So the Attorney General's office put out a statement.
And it basically said, hey, it's been two years.
We remain committed to making sure
that everybody who is criminally responsible
is held accountable.
And it had some stats and figures
about the number of people arrested,
and so on and so forth.
Not much there, to be honest.
No sign of any real forward movement,
but that kind of tracks with Garland.
There is a lawsuit against former President Donald J.
Trump for wrongful death.
It is being brought by the longtime partner,
I want to say fiancee, of Sicknick, one of the officers
that was there on the 6th.
It is a $10 million suit.
There are a couple of other people on it.
Today, Biden should be handing out about a dozen medals.
They are citizen medals.
So they are the second highest-ranking
medal for civilians.
Seven of them are going to cops, to law enforcement.
And then the Michigan Secretary of State, Jocelyn Benson,
Rusty Bowers, Ruby Freeman, and her daughter
are all going to get one.
And this is the beginning of crafting the mythology
behind this incident.
You know, there's American history,
and then there's American mythology,
and those two things aren't normally the same.
This is how it's going to be framed.
It will be the coup plotters and those
who stormed the Capitol versus law enforcement
and select public servants and citizens.
Who withstood the pressure campaign.
That's how it's being framed.
And that's how it will go down.
Oh, Al Schmidt is the other one.
It's worth noting there are a number of Republicans
receiving medals.
So this isn't a, you know, Democrats awarding Democrats
medals type of thing.
These are people who are going to be
receiving Congress medals type of thing.
These are people who in one way or another
stood up to Trump's campaign to overturn
the will of the people.
Now, if you're looking at it from a historical standpoint,
there are a couple of people I would
ask why they're not there, like Ravensburger as an example.
And there are some people who would probably be here
because they did what they were supposed to on that day.
But then afterward, they didn't cooperate.
They didn't provide information.
They didn't condemn or speak out.
There are a number of people who did the right thing
in the moment and who are effectively
riding themselves out of the American mythology, which
is what people are going to know.
And all of this happening now, while you
have what you have going on in the US House of Representatives,
should be a stark reminder that this type of authoritarian
rhetoric, it's not over.
Trumpism is not gone just because Trump is.
That type, that style of leadership, of governance,
it's still on the horizon.
It wasn't beaten.
The United States just got a reprieve from it.
That fight goes on.
I know that there are going to be a whole lot of people,
and you're going to see a whole lot of posts today
on social media saying, hey, it's been two years, Garland.
Yeah, yeah, it's been two years.
It's a complex case, and federal investigations
take a long time.
That being said, I mean, at this point, even I'm getting annoyed.
I think there should be some resolution pretty quickly,
but it's also worth noting that Jack Smith just recently got
back to the United States.
So we should be able to gauge how quickly he's
going to move by the little tidbits of information
that start to surface from his moves.
Just as we were able to tell that the investigation was
way beyond what people kind of thought it was
and what was being publicly disclosed based on cell phones
and who they were talking to, we'll
get that same kind of hint with this, I would imagine.
So while the House is presumably still locked
in a never-ending cycle of groundhog day votes,
the rest of the government is crafting America's story.
Not its history, but the story that's going to be told.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}