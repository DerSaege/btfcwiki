---
title: Let's talk about why the McCarthy show is happening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F8pZMlU7rKk) |
| Published | 2023/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A prolonged political standoff on Capitol Hill is ongoing until a specific outcome is reached.
- The framing of the situation by extremist Republicans is questioned, focusing on power struggles rather than ideology.
- Republicans' desire for better committee assignments and influence drives their actions.
- The political struggle aims to gain relevance without Trump and secure positions in the House of Representatives.
- The extremist Republicans' requests and actions are primarily self-serving.
- The Republicans facing a lack of policy are resorting to extreme measures to remain relevant.
- The situation mirrors the Democratic Party's progressive members, the squad, in being unable to push through their beliefs due to lack of votes.
- The extremist Republicans are considering allowing the United States to default, impacting the economy and blaming Biden.
- The Republicans' threats of economic collapse are seen as disingenuous tactics to energize their base.
- Ultimately, the political maneuvers are about personal gain and attention rather than fulfilling any meaningful policy goals.

### Quotes

- "It's about power."
- "It's about feathering their own nests, not fulfilling some MAGA dream or policy idea."
- "It's about serving themselves, not serving the people."

### Oneliner

A prolonged political standoff on Capitol Hill reveals a power struggle driven by self-serving desires, not ideology or policy.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact local representatives to express concerns about prioritizing political power struggles over serving the people (implied).

### Whats missing in summary

Insight into the potential consequences of prioritizing personal gain over public service.

### Tags

#CapitolHill #PowerStruggle #PoliticalStandoff #RepublicanParty #SelfServing


## Transcript
Well, howdy there internet people, Lidsbo again. So today we're going to talk a little bit more about that show going on
up on Capitol Hill. We're gonna answer a couple of questions
that have come in and
because that's not really what it's about. So first, the the most
common question came from people overseas. How long is this going to go on? As long as
it needs to. This will go on until somebody reaches that magic number. It could go on
a really, really long time. It could be resolved tomorrow. But it goes on as long as it needs
to. And while this is going on, the United States does not have a functioning House of
representatives. Now one of the things that people are kind of accepting is the
framing that is being provided to them by the extremist Republicans about why
they're doing it. That's not really being questioned very much and it should be.
There are some people who are bringing it up but it's not getting the airplay
that it should because this is not an ideological thing. This is not about
policy. It's about power. A good example, one of their prominent talking points is
that McCarthy didn't deliver the red wave therefore he's bad leadership so
he needs to go. Right, sure. And then one of them sat there and voted for Trump to
be Speaker of the House, the person that has basically led the Republican Party to loss
after loss after loss.
It's not about that.
It's not about that.
Don't buy the framing.
Think about it like this.
Why would the deal that I suggested, why would that work when it comes to McCarthy?
There were some people who were like, well, you can't trust him to do what he says.
You don't have to trust him to do what he says, you have to trust him to be who he is.
A self-serving, ambitious, power-hungry politician.
That's all you're asking him to do.
Assume that anybody that is in a political power struggle up on Capitol Hill has those
same characteristics.
That's what it's about.
better committee assignments and more influence.
They want to kind of cut ahead in the line, get those more prominent positions so they
can make waves so they can become more prominent on the national scene.
We talked about how this was going to happen back when Trump was still in office.
We talked about how people who were in heavy red areas right now, all they have to do is
echo Trump, but once Trump's gone, well then they have to have policy to stand on and they
don't have any so that they'll lash out.
And here we are.
So it's not about any of the ideological or political considerations they're bringing
up.
It's about them trying to stay relevant without Trump.
about them trying to get into a position in the House of Representatives that they haven't
really earned, that they don't belong on a lot of the committees they're asking to be
on because they don't have the experience and they're trying to cut in line.
Their requests of McCarthy, they're all pretty self-serving.
When you get to the political things, the ideological things that that group of extreme
Republicans might actually try to do, they're going to find out they're in a situation a
lot like the squad on the other side of the aisle.
For those that don't know, the progressive members of the Democratic Party, there's a
group of them known as the squad.
And they actually hold some pretty progressive beliefs, but they can't really do anything
about it because they don't have the votes.
The same is going to be true on the other side.
One of the big ones, they're basically talking about allowing the United States to default.
When I talked about them wanting to make it as hard as possible on the average American
And so that average American then blames Biden.
This would, well, this would make it horrible.
It would destroy the US economy and quite a few economies overseas.
But at the same time, they can talk about that, but they're a long way from being able
to do that.
There's a reason that the bills that stop that from happening are called must pass bills.
They're not going to be able to convince almost the entirety, or yeah, the entirety of the
Republican Party to allow the US economy to collapse.
It's a talking point for their energized base, and I think they're being pretty disingenuous
about it to be honest, but if you look, you have a whole bunch of people who are from
deep red areas so they know they're not going to suffer for this next time they go up for
election who are trying to grandstand, to cut in line, to get ahead when it comes to
committee assignments, to gain more national attention because they don't have Trump holding
them up anymore.
That's what this really has to do with.
It's about feathering their own nests, not fulfilling some MAGA dream or policy idea,
because there aren't any.
There is no major Republican policy that is going to go through in the next two years
in the House that is going to get past the Senate and the presidency.
It's not going to happen.
So they know that.
It's not about that.
It's about them serving themselves, not serving the people.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}