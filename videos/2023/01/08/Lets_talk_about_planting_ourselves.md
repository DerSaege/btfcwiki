---
title: Let's talk about planting ourselves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XzXdDV38zjA) |
| Published | 2023/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New York has recently allowed a unique form of composting where individuals can be composted after they pass away.
- The process involves being placed in a still box with organic material to help compost the body, taking about a month.
- Family members then collect the resulting dirt in an urn-like container.
- This new form of composting has faced backlash from certain religious communities and traditionalists.
- Despite the initial resistance, Beau predicts that rural people, particularly those who value land passed down through generations, may find this method appealing.
- As more states adopt this disposal method, there may be increased opposition from religious groups and cemeteries, as it challenges established practices and business interests.
- Beau believes that ultimately, the demand for this composting method may prevail due to its appeal to those seeking a return to older burial practices.
- He envisions a broad demographic, including rural and conservative individuals, embracing this alternative to traditional burials.
- Beau points out the potential conflict between personal choices after death and the interests of big business and government.
- He concludes by suggesting that this composting method could spark debates over individual rights even posthumously.

### Quotes

- "I mean, I wouldn't mind literally becoming part of the ranch."
- "And this is definitely going to cut into some business interests, because it's fundamentally altering the way things are done."
- "But make no mistake about it, the big business and government will argue over what you can do with yourself, even once you're gone."

### Oneliner

New York's new composting law sparks backlash and potential shifts in burial traditions, challenging business interests and individual rights posthumously.

### Audience

Environment enthusiasts, rural communities

### On-the-ground actions from transcript

- Advocate for eco-friendly burial options in your community (exemplified)
- Research and support legislation promoting alternative burial practices (exemplified)

### Whats missing in summary

The full transcript provides a deeper exploration of the societal implications and potential conflicts arising from the introduction of composting as a burial alternative.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about a unique form
of composting that is starting to catch on.
New York has just joined a few other states
in allowing you to be composted when you're done.
So the way it works is you are placed in basically a still
box with a bunch of stuff, organic material,
that will help compost you.
It takes about a month.
And then from there, your family picks up your dirt
in like a bucket.
It's an urn type thing.
Now, this is new.
It has caused a little bit of a backlash,
particularly among certain religious communities
and those who are very much into tradition.
This is how we've always done it kind of thing.
I don't know how long that backlash is going to last,
because I have a feeling this is going to be super popular,
particularly among rural people, people
who hold the same dirt, the same land,
in a family for generations.
I can see this really catching on.
I mean, I wouldn't mind literally
becoming part of the ranch.
I mean, that's actually kind of cool to me.
But as more and more states start
to accept this form of disposal, I
would imagine that you're going to get more and more outcry
from religious organizations and eventually cemeteries,
because you've got to remember everything's
a business in the United States.
And this is definitely going to cut in to some business
interests, because it's fundamentally altering
the way things are done.
At the same time, I really do.
I think that demand for this is probably
going to win out.
I think there will be a lot of people who
would welcome that opportunity, because rural people,
everybody used to just stay there when they were done.
Hole, you go in it.
Family cemeteries were a thing.
And that's not really done anymore.
This would be kind of a return to that.
And I think that this is probably
going to catch on with groups of people,
with demographics of people that you might not
envision at first.
Like, this sounds like something that would be kind of a left
wing, hippie, eco-conscious type of selling point.
I think you might end up with a whole lot of rural,
conservative people who look at this as a ready alternative
to something that has been done for generations.
But make no mistake about it, the big business
and government will argue over what
you can do with yourself, even once you're gone.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}