---
title: Let's talk about utopia, philosophy, and consistency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=02IkL4qjgv0) |
| Published | 2023/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the concepts of philosophy, consistency, utopias, and contradictions, sparked by a message he received.
- The message questions advocating for certain government interventions while maintaining a philosophy of anti-authoritarianism.
- Beau shares his vision of a cooperative society where people live harmoniously without a president.
- He presents two answers to the question posed, a Twitter answer and a real answer.
- The Twitter answer focuses on being a leftist for harm reduction and helping people through advocating for egalitarian society.
- The real answer involves a scene from the movie Platoon that impacted Beau, showcasing the idea of fairness in combat roles for rich and poor individuals.
- Beau contrasts the lifestyle of those in gated communities with homeowners associations to those in trailer parks and government housing, who already practice cooperation out of necessity.
- He recounts an experience in government housing where individuals exemplified community support despite societal stereotypes.
- Beau stresses the importance of providing resources and support to individuals in poverty to expand the reach of anti-authoritarian philosophies.
- He concludes by suggesting that to achieve a utopia, one must be willing to adapt and be pragmatic in their approach.

### Quotes

- "You're not going to get very far telling people who are barely getting by that they have to go further."
- "When you're thinking about your ideologically pure utopia that's going to emerge generations from now, you can be pretty much anything in service of that except for ideologically pure."

### Oneliner

Beau delves into philosophy, utopias, and the challenges of advocating for societal change while navigating contradictions and realities of poverty.

### Audience

Philosophy enthusiasts, activists

### On-the-ground actions from transcript

- Support community initiatives to aid individuals living in poverty (implied)
- Advocate for resources and assistance for those struggling financially (implied)

### Whats missing in summary

Beau's engaging storytelling and nuanced perspective on societal issues can best be appreciated by watching the full video. 

### Tags

#Philosophy #Consistency #Utopias #Contradictions #AntiAuthoritarianism #CommunitySupport #Poverty #Advocacy #SocietalChange


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about
philosophy and consistency and utopias and contradictions that may seem more pronounced
than they really are. I'm going to do this because I got a message, and this is a theme
I have seen in a couple of different places from different people, and it seems like it's
worth going over. So here's the message. I have a question, and it's not really a criticism
because I do the same thing, but I feel like a hypocrite. I think I know what your philosophy
is, and it's mine too. I'm also a left-leaning, extreme anti-authoritarian. Nice way to say
that. So by definition, I don't want big government, but I find myself advocating for health care
and minimum wage increases and government-funded help for the homeless. If there's one thing
I know about you from your channel, Setting Aside Parasocial Feelings, it's that you're
consistent. I'd like to know how you advocate for these things, or at least don't attack
them while maintaining your philosophy. And yeah, I mean, I do hope one day to have a
much more cooperative society. And so cooperative, in fact, that our great-great-grandkids one
day look at each other and they're like, what if we decide not to have a president and we
all just promise to be like super cool? Yeah, that's the ideal. That's my vision of Utopia.
And there's two answers to your question. And I don't know whether to give you the Twitter
answer or the real one. So we'll do both. The Twitter answer is, why'd you become a leftist?
For me, I wasn't moved by any writing or some great theory or anything like that. I just
got tired of watching people suffer. That's where it started. That's where it started.
So in that vein, if you are a leftist, you want some form of egalitarian society. It's
harm reduction. You can advocate or not attack these things because it's harm reduction.
You're helping people. That's the Twitter answer. Short, sweet, concise, really hard
to take out of context. The real answer? Platoon. The movie Platoon. There's a scene in it.
And it really kind of stuck with me. This is not a movie that you would expect to find
a whole lot of philosophical guidance in, but it's there in a throwaway comment. These
three soldiers, they got in trouble. I think they were, I think they took something from
the sergeant or something like that. But they're cleaning out latrines as punishment. And they're
talking about how long they've been in country. And they turn to the new guy. One of them's
like, how'd you even end up here? You seem pretty educated. And he says, I volunteered.
Dropped out of college. Asked for infantry, combat, Vietnam. And they look at him like
he has lost his mind. And one of them asks him why. And he said he didn't think it was
fair that the poor kids always got shipped off to fight and the rich kids got out of
it. And he's saying this to two draftees. And they make fun of him for a second. And
then one of them looks at him and says, man, you gotta be rich to even think like that.
And it's true. It's true. That offhand comment really stuck with me because it is true. You
don't have to be rich, but you can't be in poverty. When you think about that more cooperative
society, who today lives it? Already? Is it people who live in gated communities with
homeowners associations? Or is it the people who live in the trailer parks and government
housing all over this country? Who are the people that save their baby clothes and bottles
in trash bags because they know soon another family in the neighborhood is going to need
it? When I was doing relief after Michael pulled into government housing and a group
of guys that most people watching this channel would probably cross the street not to walk
past, they knew the diaper size of every kid in that neighborhood. They already live that
more cooperative society, but they do it out of necessity, not out of some vision of utopia.
They do it to survive. Imagine telling them that they have to extend that obligation to
the entire community, not just their neighborhood. They can't really envision that when they're
spending most of their time trying to figure out how they're going to get food, which
bill they cannot pay based on which service takes the longest to cut off. If you want
to expand the amount of people who want that extreme anti-authoritarian left-leaning philosophy,
that utopia, they have to be able to think about more than just survival. They have to
have the resources to put it into action. They can't be in poverty. You're not going
to get very far telling people who are barely getting by that they have to go further. When
you're thinking about your ideologically pure utopia that's going to emerge generations
from now, you can be pretty much anything in service of that except for ideologically
pure. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}