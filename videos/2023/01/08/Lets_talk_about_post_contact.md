---
title: Let's talk about post contact....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p7QaPUKH4yo) |
| Published | 2023/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the SETI Post-Detection Hub for processing information on potential contact with extraterrestrial beings.
- Compares the hub's strategies to humanity's response to COVID, considering it a failed dress rehearsal.
- Focuses on the impacts of disinformation on first contact and potential societal upheaval.
- Considers whether the public should be initially informed about contact due to humanity's tendency towards panic.
- Current protocol involves confirming contact's authenticity and then informing the UN, yet this may not suffice based on past experiences.
- Aims to establish methods to keep calm if extraterrestrial contact occurs, acknowledging humanity's capability to handle such an event.
- Raises concerns about potential fallout on Earth and exploitation of the situation if contact is made.
- Questions if humanity is prepared for such contact as we venture into space exploration.
- Urges reflection on whether the public should be informed about extraterrestrial contact, focusing on people's ability to accept new information.
- Encourages pondering on humanity's readiness for contact beyond scientific benefits.

### Quotes

- "If that was a dress rehearsal, let's just say we failed."
- "They're trying to come up with established methods and a strategy for keeping everybody calm if ET shows up."
- "Is it something that we're ready for as we strive to unlock the skies?"
- "We have to figure out if we should even be told."
- "Y'all have a good day."

### Oneliner

Beau introduces the SETI Post-Detection Hub, drawing parallels between humanity's response to COVID and potential extraterrestrial contact, questioning if we are prepared for such an event beyond scientific benefits.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Join organizations researching extraterrestrial intelligence (implied)
- Stay informed about developments in space exploration and potential first contact scenarios (implied)

### Whats missing in summary

The full transcript provides a deeper reflection on humanity's readiness for potential extraterrestrial contact and the societal implications beyond scientific curiosity.

### Tags

#ExtraterrestrialContact #SETI #SpaceExploration #HumanResponse #SocietalImpact


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a new field of study, in a way,
and a new hub for processing information and coming up with strategies and protocols
to deal with humanity's first contact with people from other worlds,
with entities from outside of Earth.
It's called the SETI Post-Detection Hub,
SETI, Search for Extraterrestrial Intelligence, Post-Detection.
So after contact, in some way, has been made,
and they're trying to figure out what to do.
What I find incredibly interesting about this is that they're looking heavily
at our response to COVID.
They're looking at how humanity responded to COVID to kind of guide their discussions,
because, I mean, if that was a dress rehearsal, let's just say we failed.
It didn't go well.
They are looking at the impacts of disinformation on first contact.
They're looking at how social and religious beliefs might be shaken
and how to best deal with it.
It appears that some of the lines of thought and some of the debates are about
whether or not they even release the message to the public initially,
because we have proven ourselves to be panicky animals.
The current protocol is to basically make sure it's real and then inform the UN.
But as we saw with COVID, that may not be enough.
So they're trying to come up with established methods
and a strategy for keeping everybody calm if ET shows up.
The fact that it exists, the fact that these discussions are taking place,
I mean, that provides a whole lot of commentary all on its own
when it comes to humanity as a whole.
Are we even capable of managing that kind of contact?
Is it something that we're ready for as we strive to unlock the skies?
And many people hope that contact is made in the near future.
We have to worry about the fallout here on Earth
and how it would upset all of the various systems that we have come to rely on
and how people might seek to exploit the situation for personal gain.
So rather than focusing on the scientific aspects of it
and how much a contact like that might offer to humanity,
we have to figure out if we should even be told.
That's quite the remark on how capable the average person is of processing new information
and accepting things that are outside of their current norms.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}