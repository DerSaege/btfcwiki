---
title: Let's talk about congressional comments on the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LHDpmYPWnx0) |
| Published | 2022/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses comments made by a sitting member of Congress, referring to them as the "space laser lady" and discussing her statement about organizing to win with Steve Bannon on January 6th.
- Questions the appropriateness of a Congress member joking about armed resistance to the US government.
- Criticizes the notion of winning through armed entry and capturing symbolic buildings like Walmart, pointing out the impracticality of such actions.
- Raises the question of who "we" refers to in the context of the Capitol storming, suggesting it includes the Congress member herself.
- Examines the persistence of false claims and desires for victory despite debunked narratives and court losses, indicating a disregard for democracy and the Constitution.

### Quotes

- "That is not how you win."
- "Who is we?"
- "They're telling you who they are."
- "Still suggesting that we would have won indicates that she still wished they had."
- "Whoever we is would have done that."

### Oneliner

Beau questions the seriousness of a Congress member's comments and examines the implications of armed resistance fantasies on January 6th, prompting reflection on the true intentions behind such statements.

### Audience

Congressional accountability advocates

### On-the-ground actions from transcript

- Contact elected representatives to express concerns about inappropriate statements made by officials (implied)
- Stand against narratives that undermine democracy and the Constitution (implied)

### Whats missing in summary

Deeper analysis and context on the political climate surrounding false claims and desires for power post-January 6th.

### Tags

#Congress #PoliticalCommentary #Accountability #Democracy #Constitution


## Transcript
Well, howdy there internet people it's Bo again
So today we are going to talk about some comments that came from a sitting member of Congress
I think it's worth going through the comments even though now
She is saying. Oh, it was just a joke
I believe it's still worth going through them
so we're gonna go through the comment and then we're going to provide the commentary that is it is just
directly begging for. And then I'm going to ask a question about the comment that
I think is far more important than any of the commentary, even though the
commentary needs to be made. So of course we are talking about the space laser
lady and her statement of if Steve Bannon and I had organized that we would
have won, not to mention, it would have been armed, okay?
And if you don't know, she's talking about the 6th.
She's talking about the 6th.
Okay, so yeah, it certainly appears that a sitting member of Congress is talking about
resistance to the US government. Some might call that levying war against the
US government. I'm pretty sure that there's a term for that. I don't
think that's a statement that somebody in Congress should be making. Now walking
it back and saying it was a joke, that's all fine and good, but I'm gonna suggest
that I don't think that's something a sitting member of Congress should be
joking about either. And then there's the obvious would have won if we'd been
armed. Okay, I'll play your silly little game. So what happens? They make
entry, okay, a whole bunch of innocents get lost, and then they they have
succeeded. They have captured a building as strategically important as a
Walmart, that is a symbolic building. It's not actually, it's not a strategic
objective. You don't win that way. What happens is you have now placed yourself
in a situation where you are in a building surrounded and outnumbered in
the middle of one of the most heavily surveilled cities on the planet. Yes, I
I can see the brilliance of your strategic mind at work.
No, that's not how you win.
That is not how you win.
But here's my question.
Who is we?
It says here, if Steve Bannon and I had organized that, we would have won.
Who is we?
Certainly sounds like a sitting member of Congress just identified we as the people
who stormed the Capitol, including herself in that group.
That's kind of telling.
We.
I'm just wondering, is it just her faction of the Republican Party or all of the Republican
Party that is we here. They're telling you who they are. The other part of this, when
it comes to we would have won, is you have to remember that at the time the Republican
Party had manufactured a facade that had created this outrage, this fiction that gave them
cover. Remember it was about the steel and all of that. The thing is, since then
and all the time that's passed, we found out that none of that was true. They made
it up. So still suggesting that we would have won indicates that she still wished
they had. She still wants that side to win even though all of those claims have
since been debunked, even though they lost in court, even though it was all made
up, which could really be rephrased as we would have succeeded in disregarding the
voice of the American people and shredding the Constitution and installing
a government of our own choosing. Whoever we is would have done that. Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}