---
title: Let's talk about Trump's major announcement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t6ysC86SvE8) |
| Published | 2022/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's major announcement is a distraction from his plummeting polling numbers.
- Speculation about the announcement ranges from returning to Twitter to running for Speaker of the House.
- Trump's favorability ratings have hit an all-time low among registered voters and independents.
- Only 20% of Republicans view Trump favorably, a significant drop from his prior popularity in the party.
- Trump's numbers are even worse in primary polling, where he loses to DeSantis by a large margin.
- Biden's favorability ratings are rising, adding to Trump's discomfort.
- Trump's announcement aims to re-energize his image and create buzz around him as a product.
- Trump may be realizing the legal jeopardy he faces and the unlikelihood of returning to the White House.
- To change his odds, Trump plans to double down on being erratic and a maverick, believing it resonates with people.
- Beau doubts the effectiveness of Trump's strategy but acknowledges it may guide his behavior.

### Quotes

- "His announcement is the event, not whatever he's going to announce."
- "Among registered voters, he has a 31% favorability rating. 59% view him unfavorably."
- "He cannot win with those numbers and he knows it."
- "His only option to change the odds is to just out-Trump himself and go all in."
- "I'm fairly certain that's what he believes."

### Oneliner

Trump's announcement serves as a distraction from his plummeting polling numbers, prompting a desperate attempt to re-energize his image and create buzz around himself as a product in the face of unfavorable ratings and primary losses.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor political developments closely (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's announcement as a diversion tactic from his declining favorability ratings and primary losses, shedding light on his potential strategies and motivations.

### Tags

#Trump #Announcement #PollingNumbers #Distraction #ElectionPolitics


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Trump's major announcement, whatever it may be, except
we're not really going to talk about his announcement because it doesn't matter.
We're going to talk about what he is trying to change the story from.
If you don't know what I'm talking about, the former president of the United States
indicated that today he would be making a major announcement and speculation has run
wild.
Everything from he's returning to Twitter to he's going to run for Speaker of the House
to he's launching a third party.
Thing is, it doesn't really matter because the announcement is the event, not whatever
he's going to announce.
The former president is a numbers person.
He likes numbers.
He likes polls.
He looks at them.
And he's gotten a lot of really bad news lately.
And I am fairly certain that this whole thing, whatever it is, is just him trying to distract
from the numbers or he has a heads up that something even worse is coming.
Ever recently we talked about the fact that he is breaking with his norms and to expect
erratic behavior.
Here we are.
Okay, so what do I think he's trying to change the story from?
Polling numbers.
Among registered voters, he has a 31% favorability rating.
59% view him unfavorably.
These are his worst numbers in like seven years.
If you look at independents, it's even worse.
Only 25% view him favorably.
62% view him unfavorably.
He cannot win with those numbers and he knows it.
Among Republicans, just Republicans, 20%, one out of five, view him unfavorably in his
own party.
This is a person that had just total command of the Republican Party not too long ago and
now one out of five view him poorly.
That is, again, his lowest number since 2016.
Meanwhile, Biden's favorability ratings are going up.
You know, that's got to hurt.
But to me, I think the biggest sting has to do with the numbers related to the primary.
In polling of likely primary voters for the Republican Party, Trump loses to DeSantis.
Big.
Double ditches.
Like, just, he gets stomped.
And while he always tells people not to believe the polls, he certainly does.
And he knows that those numbers, they're probably going to continue the trend that they're on.
So his announcement is really just about re-energizing and trying to create buzz about him.
He's the product and he's trying to sell it.
He may finally be beginning to understand the amount of legal jeopardy he is in and
that his return to the White House is incredibly unlikely.
His only option to change the odds is to just out-Trump himself and go all in and be erratic
and be a maverick because he thinks that's what really resonated with people.
I don't think he was right.
I don't think he's right about that.
But I'm fairly certain that's what he believes.
And so that will probably govern his behavior.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}