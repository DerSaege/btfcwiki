---
title: Let's talk about McCarthy vs McConnell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iFSq77qnLO0) |
| Published | 2022/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- McCarthy, in pursuit of becoming Speaker of the House, has sacrificed his power by pandering to different factions within the Republican Party.
- This has made McCarthy more of an errand boy for Republicans with strong social media followings, rather than a leader.
- McCarthy has opposed reaching a budget agreement with the Democratic Party and has clashed with McConnell, the Republican Senate leader.
- McConnell is known for his political acumen and understands the importance of US assistance to Ukraine in countering Russia.
- McConnell is likely to push for more aid to Ukraine in any spending package, a stance that may create tension between him and McCarthy.
- The conflict between McCarthy and McConnell resembles a showdown between Freddie versus Jason – a difficult choice.
- McCarthy's allegiance to hardline Republicans, especially those with a strong social media presence, influences his positions.
- In contrast, McConnell prioritizes power and is not swayed by social media dynamics.
- Beau offers political advice to McCarthy, warning against picking a fight with McConnell if he wants to maintain his position.
- He predicts that McConnell may publicly praise McCarthy while working behind the scenes to undermine him.

### Quotes

- "It's like Freddie versus Jason. I mean, who do you really root for there?"
- "McConnell will come out and say something very personable. He will come out and say, hey, you know, I've always liked young McCarthy. He's a good man. And then quietly work to destroy him."

### Oneliner

Beau breaks down the power struggle between McCarthy and McConnell within the Republican Party, cautioning against underestimating McConnell's political prowess.

### Audience

Political observers

### On-the-ground actions from transcript

- Support political candidates who prioritize policies over social media presence (implied)

### Whats missing in summary

Insights into the potential consequences of the power struggle between McCarthy and McConnell within the Republican Party.

### Tags

#RepublicanParty #McCarthy #McConnell #PowerStruggle #USPolitics


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about some surprising developments
within the Republican Party and the dynamics that are shaping up
between the House and the Senate for 2022.
About two weeks ago, we talked about how McCarthy,
in his quest to become Speaker of the House, has basically given up all of the power
that comes with that position. In order to get the votes to become Speaker,
he's put himself in a position where he's not leading.
He's so beholden to so many different factions within the Republican Party
that he's really just kind of turned into the errand boy
for Republicans with strong social media presences.
So because of that, McCarthy has come out against working out an end-of-the-year deal
for the budget with the Democratic Party.
He doesn't want to do that and took a swing at McConnell in the Senate,
the Republican leader in the Senate.
McConnell is McConnell. I've said repeatedly on this channel,
you may not like him, but don't underestimate him.
He is a very, very savvy person.
He understands foreign policy a little bit.
He knows that the United States and the assistance that the US has provided to Ukraine
has put Russia on the ropes. It is important to McConnell to keep Russia on the ropes.
Therefore, he's going to want to get a spending package through
that includes more assistance for Ukraine.
I don't think McConnell's going to want to give that up.
So you now have a fight brewing between the Republican leader in the House
and the Republican leader in the Senate.
You have to choose between McCarthy and McConnell.
It's like Freddie versus Jason. I mean, who do you really root for there?
This dynamic will probably continue for quite some time
because the reality is that McCarthy is beholden to a bunch of people
who see themselves as hardline Republicans,
as they cater to a very vocal but small base on social media.
Because McCarthy is beholden to those people,
he is going to adopt a lot of those positions.
McConnell does not care about those games.
McConnell has been up there forever.
So he's not going to...
It is unlikely that he gives a social media presence any thought whatsoever.
That's not what he cares about. He cares about power.
And just some free political advice for McCarthy.
If you're in a situation where you are just making all kinds of deals
to get the votes to become Speaker,
I'm going to suggest that picking a fight with Mitch McConnell
is probably not a good idea.
I have a feeling that McConnell will come out and say something very personable.
He will come out and say,
hey, you know, I've always liked young McCarthy.
He's a good man. And then quietly work to destroy him.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}