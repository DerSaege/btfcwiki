---
title: Let's talk about me helping Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nF2aFX4CRe0) |
| Published | 2022/12/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Encouraging Republicans to become more progressive to win elections.
- Explains three reasons for helping Republicans: a factual statement, belief it will work, and the potential for progress.
- Raises a question to a viewer: why are you a Democrat?
- Democrat or Republican affiliation should be based on holding progressive ideals.
- Loyalty to a party can lead individuals to shift positions based on party stances.
- Shifting the Overton window by moving the Republican Party to more progressive positions.
- By making Republicans more progressive, it allows for more progressive ideas to be advocated for within the Democratic Party.
- Views the party as a tool for beliefs, not something that should shape beliefs.
- The cycle of progress where Republicans becoming more progressive can lead to further progress.
- Emphasizes the importance of getting past stagnant arguments and pushing for progress through shifting political positions.

### Quotes

- "The party should be a tool for your beliefs. Shouldn't shape them."
- "All of those people will shift if the Republican Party becomes more progressive."
- "We have to shift to the Republican Party, get them to catch up, so progressives can move forward."

### Oneliner

Encouraging Republicans to become more progressive is key to shifting the Overton window and advancing progressive ideas, leading to a cycle of progress.

### Audience

Progressive individuals

### On-the-ground actions from transcript

- Advocate for progressive ideas within political parties (implied)

### Whats missing in summary

The full transcript provides a thorough explanation of how shifting the Republican Party towards progressiveness can benefit the overall political landscape and advance progressive ideals.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about some of my recent videos
where I'm giving advice to Republicans and Republican politicians
and a question I got about why I am helping Republicans win all of a sudden.
In a lot of your recent videos, you're encouraging the Republican Party to become more progressive
so they can win elections.
Can you please explain why you would want to help them win?
I don't understand and I've followed you a long time. Frankly, it's infuriating.
I do it for three reasons.
One, it's a statement of fact. If they want to win, they have to become more progressive.
Two, I think it'll work. I think it will work.
And the reason I think it will work is the third thing.
It's the reason you're mad.
Why are you a Democrat?
Are you a Democrat because you like the color blue and donkeys?
No. You hold more progressive beliefs.
At least theoretically, that's why you would align with the Democratic Party.
If the Republican Party shifts and becomes more progressive, particularly on social issues,
they will stand a better chance of winning elections.
That is true.
But why are you a Democrat?
Because you hold progressive ideals.
The party is a tool to get your ideas out there.
The problem is, and this isn't just you, this is a whole lot of people,
a lot of people become so loyal to the party that if the party shifts positions on something,
well, so do they.
That would happen with the Republican Party.
If the Republican Party suddenly came out and was in favor of amnesty for undocumented workers,
the rank-and-file Republicans would support it.
Why? Because they did. That's happened before.
Not too long ago.
So if the Republican Party becomes more progressive,
now they're not going to become truly progressive,
but they won't be regressive.
They won't be going the other way.
If they become more progressive, it shifts not just the Republican Party's position,
but it shifts the Overton window, the range of acceptable political positions within a group,
the range of those ideas that are viewed as acceptable.
If they move to the left a little bit, the whole window moves to the left,
which means there are even more progressive ideas that the Democratic Party can then effectively advocate for.
Right now, some are outside of the Overton window, so they can't advocate for them.
Bringing the Republican Party to more progressive positions shifts that Overton window
and allows progress to occur.
That's a good thing.
That is how, on a long enough timeline, we win.
The party should be a tool for your beliefs.
Shouldn't shape them.
But most people hold a loyalty to the party, especially those that are partisans.
Those people who are incredibly, incredibly adamant about their loyalty to a party
rather than any particular position.
All of those people will shift if the Republican Party becomes more progressive.
And it helps achieve those progressive ideas rather than just talk about them.
So that's why I do it.
I think it'll work.
And it's this weird cycle.
If Republicans start to become more progressive on social issues
and then start winning elections because of that, what will occur?
They will become even more progressive, which will shift the Overton window even more.
It's how we get to where we want to go.
We can't just keep arguing over the same topics.
We have to shift to the Republican Party, get them to catch up, so progressives can move forward.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}