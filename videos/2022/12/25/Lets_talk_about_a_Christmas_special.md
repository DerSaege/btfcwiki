---
title: Let's talk about a Christmas special....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yd0yfSJBvmQ) |
| Published | 2022/12/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing funny Q&A questions selected by the team, including being mistaken for others and working dynamics with the team.
- Acknowledging misconceptions about his appearance and public image, such as being misidentified in public.
- Admitting to having no concept of time while working and questioning the rationale behind tasks.
- Expressing the importance of adjusting viewpoints when proven wrong and the ongoing effort to combat ableism.
- Responding to criticism about addressing race issues and his audience, encouraging support for Black creators.
- Sharing insights on collaborations, production workflow changes, and upcoming behind-the-scenes content.
- Describing past injuries, including a life-threatening head injury and a painful incident involving his feet.
- Touching on the decision not to heavily market his second channel on the main channel due to its impact on video quality.
- Wishing viewers Merry Christmas and discussing the political landscape in the United States for the upcoming year.

### Quotes

- "Don't idolize anybody."
- "If you want to take away that portion of my videos and make me stop making them, it's really simple. All you have to do is start watching black creators on YouTube."
- "Hopefully it will be a bit more calm though."

### Oneliner

Beau addresses funny Q&A questions, misconceptions about his appearance, working dynamics, adapting viewpoints, collaborations, production changes, injuries, marketing decisions, and upcoming political landscape.

### Audience

Content creators and supporters

### On-the-ground actions from transcript

- Support Black creators by actively watching and engaging with their content (suggested)
- Encourage collaborations with diverse creators on YouTube (exemplified)

### Whats missing in summary

Insights on Beau's personal growth, humor, and approachability may best be experienced by watching the full transcript. 

### Tags

#ContentCreator #Q&A #Misconceptions #Collaborations #PoliticalLandscape


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to do a Christmas special.
This is a Q&A and the questions were picked by members of the team.
The people who help put this out everywhere.
Um, they're supposed to be funny or at least give me the opportunity to have a funny response.
We'll see how that works out.
Alright, so, how does it feel to be a reverse Tony Hawk meme?
Nobody recognizes him, everybody recognizes you, but they recognize you as someone else.
Nice.
I haven't put much thought into it.
For those who only watch on YouTube and aren't aware of things that happen on other formats
of social media, it's relatively often that a photo surfaces and somebody's like, hey,
it's Beau.
No, it's not.
It's just some other redneck-y looking guy.
I think my favorite is getting just a gushing piece of fan mail for a song that I performed
called Bootstraps that I obviously did not write, but the person who put it together,
Duke Osler, basically worked the concept of consent into this very southern masculine
song and yeah, they apparently thought it was me.
And the weird part about that is that I have been mistaken for him twice, which is not
a lot, but it's weird that it's happened twice.
And then not exactly the same, but a very similar vibe was me walking into a gas station
and somebody being like, I love that YouTube channel.
And as I say, well, thanks, they say, she's so great.
And it's then that I realized I'm wearing a Bailey Sarian sweatshirt.
Yeah.
So anyway, what would your team say is the worst thing about working with you?
Wow.
This is like one of those games you play with your significant other, where you try to guess
what they're going to say.
Whether that I have absolutely no concept of time when I'm working and sometimes have
to remind me of that, or my eternal question of why, you know, hey, we should do this.
Why?
And even if it's something that I think immediately is a great idea, I still want to know the
why.
And I know that it probably feels like they have to convince me of just anything.
And I say I know that it probably feels that way because sometimes they have actually told
me that.
But it's just it.
It's the way I am.
That's that's what I'm going to guess they will say.
Shave your beard, at least trim it occasionally.
You look homeless.
Fun fact, you can't tell whether or not people are homeless based on how they look.
Have you publicly eaten your words about Zelensky not being a wartime president yet?
Yes, multiple times, Keith.
He called this early on and said that he would actually do very well once the fighting started.
Initially I was saying he was doing great at the diplomatic stuff, but didn't think
he would do well once things got loud.
And I was very wrong.
What do you think of the rumors that say you're trans?
I saw that.
I saw that.
I don't care.
I do not care at all.
Your use of gifs tells me that Emma Stone or Kristen Davis is your hall pass.
Comments.
Kristen Davis, that is Charlotte from Sex and the City.
She is an amazing actress.
She's also an amazing activist when it comes to animal conservation.
A whole bunch of stuff.
Refugees.
And she has my respect.
So I use her gifs a lot.
As far as Emma Stone, she is very emotive and has...
We share a lot of the same facial expressions.
What is your most problematic take or thing you really have to work on?
I don't have problematic takes.
Not because I'm always right, but because when I find out I'm wrong, I change my take.
If I find out something is... there's a problem with my view on something, I tend to alter
my view.
So currently I'm not aware of any.
I'm sure they exist.
I just don't know it yet.
Thing I really have to work on?
Ableism.
And it's not even...
I was very much formed by the idea, you can do anything.
And that's very common among a certain group of people, and it's very hard to break that.
And there are times, and we're not even talking about physical things here, just...
Have to constantly acknowledge that not everybody is brainwashed that way.
That healthy people don't actually put themselves to those extremes.
I saw you and Train talking on Twitter about your appeal to young women.
What do you really think about weaponized daddy issues and your following among that
group?
Yeah.
Wow.
Don't idolize anybody.
Yeah, I mean, I am aware of this phenomenon.
It's when you watch somebody often, you develop an idea of what they're like.
And most times, if what you're watching is positive, if you have a positive reaction
to what you're watching, the attributes that you assign to the person you're watching will
also be positive.
But nobody's perfect.
Don't idolize anybody.
And yeah, okay.
Your biggest videos are just you saying things black people have said for years.
All you're doing is repackaging it with your redneck appearance so white people listen
to it.
When are you going to speak to white people's issues?
What about us?
When are you going to stop just being a black being mouthpiece?
Yeah I know.
Won't somebody think of the poor white guys?
I tell you what, because it's in here.
So white people will listen to it.
If you want to take away that portion of my videos and make me stop making them, it's
really simple.
All you have to do is start watching black creators on YouTube.
If you listen to black people, then you've completely eliminated my ability to do that.
So if you really want to just shut me down because you hate me, then the best thing you
can do is listen to black people talk about race issues.
I noticed an icon in making movies for adults follows you on Twitter.
No shade, she got me through my last deployment.
That led me to noticing you are at least online friends with lots of women in that industry.
How does your wife feel about that?
So if you actually talk to the person above, please tell her she's a goddess.
I will pass that along.
I don't think my wife feels anything about that.
She has never brought that up at any point in time.
Does that guy you called Cletus have a girlfriend and will we see him in more videos?
So on the channels, you haven't seen him yet, I don't think.
But he helped when we went to do relief work with Hurricane Ian.
Yes, he has a significant other.
In fact, I would not be surprised if she was very happy about this question being asked.
When will we get to see more behind the scenes?
In a recent video, you said we'd see more.
So the workflow is changing soon.
For all the time this channel has existed, we have done this out of the shop and kitchen
tables and couches and stuff like that.
We are currently in the process of turning a steel building into a, I want to call it
an office because it's not really an office, a more productive workspace.
How about that?
And once that happens, there will be more behind the scenes stuff.
I can't really say when that's going to happen because we're doing that in the background
as we do our normal videos and all of that stuff.
Soon, I would hope.
The goal is to have it up and running by spring.
Who is the hottest woman on all of LeftTube?
They're all smart and smart women are hot.
When Corey's page got taken down, I assumed it was a scam.
I'm glad it wasn't, but am wondering what you would have done if it was.
For those who aren't familiar with this, a young man in the UK was on palliative care
and his big dream was to be a YouTuber and to get a play button.
A whole bunch of people went through to make that happen.
Right as he got to 100,000 subscribers, the channel disappeared.
It led to a lot of speculation.
It turned out it was something very simple having to do with basically the age of the
account associated with it and all of this stuff.
It all got worked out, but it being a scam was one of the things that came up.
If it had turned out to be a scam, I would alter my fact checking a little bit.
I checked out the story before I got involved with it.
When you're doing something like that, if somebody succeeds in taking advantage of you,
that's on them.
That's not on you.
I don't know that I would have changed much.
We confirmed the important parts of that story before we ever got involved with it as best
we could.
If they found a way to get through that process in order to scam me in some way, that's on
them.
I wouldn't want to make it so hard to reach out that it becomes basically a massive task
to get involved in something like that.
Trust that most people are good.
You need to do more collaborations with other people on YouTube.
I like to do them when something good can happen because of the audiences coming together.
That's when I like to do them.
For me, it's not a matter of choosing not to do them with people.
It's a matter of the right people being free at the right time to have the most impact.
But hopefully that will get easier soon because a lot of that has to do with basically production
speed and how quickly we can get stuff together.
And hopefully that will be changing soon.
What was your worst injury?
That's a list.
It depends on how you view worst.
Most life-threatening.
I got hit in the head with something once.
It was really bad.
Most broken bones.
I didn't fall.
I had to get down from a rappelling tower very quickly.
The people belaying me did not really understand that those ropes stretch, so as they dropped
me incredibly quickly and stopped me inches before the ground, the rope stretched and
I hit the ground anyway and basically broke all of my ribs.
Most painful was actually something pretty minor.
I had to get out of a car very quickly and the car was turning and it was moving.
That's an important part of it.
And as I got out, as it made its turn, it caught my ankle and my feet went under the
tires.
That was absolutely the most painful thing in the world because we had no way to really
do anything about it.
I just had to deal with my feet being messed up.
Can you trim your beard?
Seeing your mouth helps my significant other hear you better.
I can try to trim around my mouth a little bit more frequently, so I guess my lips are
easier to see.
Compare that to the original question about trimming his beard and that's how you have
to convince him of anything.
Well played.
Nice.
So yeah, the eternal question of why.
Why.
And if you have an answer, then yeah.
Okay.
Tell people to like and subscribe to the second channel and say it's called The Roads with
Bo.
Maybe put a link down in the comments.
We have a lot of cool stuff being released soon.
There is an ongoing conversation behind the scenes here about on the main channel after
the video, me saying, hey, we have this other channel, The Roads with Bo, blah, blah, blah,
blah.
And they have been pushing me to do it because basically it's like marketing 101, but I think
it kills the videos on the main channel.
So I don't do it.
And even though they have a good why, I don't accept it because I don't like the way it
makes the videos.
So that's what that's about.
If he looks weird right now, it's because he doesn't know we're uploading this to the
main channel.
Well, there we go.
Merry Christmas from us.
Your support of this channel not only helps get a positive message out, but gives us jobs
where we get to help a bunch of people or a bunch of great causes all the time.
Happy holidays to everyone.
And there we go.
So that's our Christmas Q&A.
It's been an interesting year.
Next year is going to be even more interesting when it comes to the political situation in
the United States because the Republican Party has to kind of decide which direction it's
going to head.
And there will be the lead up to the 2024 election.
There's a lot coming next year.
It will be even more interesting.
Hopefully it will be a bit more calm though.
We'll see.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}