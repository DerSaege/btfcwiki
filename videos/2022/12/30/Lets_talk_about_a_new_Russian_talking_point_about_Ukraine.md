---
title: Let's talk about a new Russian talking point about Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=smWaOz5r6CE) |
| Published | 2022/12/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a new talking point regarding Ukraine and Western assistance.
- Mention of inorganic talking points with similar typos being pushed.
- Exploring the idea that stopping Western assistance to Ukraine might lead to Russia taking over.
- Expresses skepticism about Russia making significant gains if assistance stops.
- Pointing out that if Russia were to take over, the difficult part lies in the occupation phase.
- Russia's lack of adequate troops for occupying Ukraine despite initial numbers.
- Explaining the need for a much larger number of troops for successful occupation.
- Detailing the duration and challenges of the occupation phase.
- Asserting that without the required troop numbers, any Russian occupation attempt will prolong the war.
- Criticizing the talking point against Western assistance as Russian propaganda.

### Quotes

- "Even if you want Russia to complete its imperialist endeavors for the sake of their capitalist oligarchs, understand that it's worse for the people in Ukraine."
- "It is absolutely Russian propaganda."
- "The Western assistance to Ukraine is speeding the end of the war."

### Oneliner

Beau addresses the flawed idea that stopping Western assistance to Ukraine will benefit peace, debunking it by explaining Russia's lack of troops for a successful occupation and how continued assistance actually speeds up the end of the war.

### Audience

Global citizens, activists

### On-the-ground actions from transcript

- Support organizations providing aid to Ukraine (suggested)
- Stay informed on the conflict in Ukraine and counter misinformation (suggested)
- Advocate for diplomatic solutions and peace in Ukraine (implied)

### Whats missing in summary

In-depth analysis of the geopolitical implications and historical context of the conflict in Ukraine.

### Tags

#Ukraine #WesternAssistance #Russia #Propaganda #Occupation #Peace


## Transcript
Well, howdy there, Internet of People.
It's Beau again.
So today, we are going to talk about a new talking point
when it comes to Ukraine.
I'm seeing it surface.
A lot of it is clearly inorganic.
It's a script, and some of them include the same typos.
But the general idea that's being pushed
is that the West, by providing assistance to Ukraine,
is just prolonging the inevitable.
That if the West stopped providing Ukraine
with assistance, then Russia would just roll right in,
and the war would be over, and therefore, it's best for peace,
and it would save lives.
OK, first, I'm not actually sure that's true.
I don't know that if the West stopped assisting,
that Russia would make any significant gains,
to be honest.
But let's say they do.
Let's say, let's follow this little train.
Let's say the West stops providing assistance,
and Russia is capable of, at that point,
actually winning the conflict.
OK, congratulations.
They have now made it to the hard part.
Something I have repeatedly said throughout this 300-something
days of this three-day excursion is
that Russia hasn't gotten to the hard part yet, the occupation.
That's the difficult part.
If the West stops supplying assistance
and Russia takes over the country,
that's when the resistance starts,
the unconventional stuff, the dirty stuff.
Russia won't be able to subdue it.
And that's not a subjective statement.
It's math.
It is pure math.
At the outset, most estimates say
that Russia rolled in with around 180,000 troops.
We're going to go ahead and round that up to 200,000.
And we're going to say, despite evidence to the contrary,
that they actually did recruit, draft another 300,000,
and that all of them went to Ukraine,
and that they have not had anybody wounded,
and nobody's been lost, and they have all 500,000.
Yeah, they have about, I don't know,
maybe 60% of what they would actually
need to stand a chance of occupying that country.
They don't have the troops.
A standard rule on this is that you
need 20 soldiers, or allied, key phrase,
cops, to subdue a post-conflict society.
And you need 20 per 1,000.
Ukraine is a country of 43 million, something like that.
So they need around 860,000 troops.
Even if they haven't had anybody wounded,
nobody lost, nobody captured, they don't have the numbers.
They have 500,000.
And that's, you know, assuming that the 100,000 that everybody
is pretty sure aren't here anymore,
we're letting their ghosts fight.
And then you have to consider the wounded.
The reality is, if you want to be even remotely realistic,
Russia doesn't have half the troops
that it actually needs to pull this off.
They don't have the numbers.
So if the West was to stop supplying assistance,
and Russia decides that somehow now it can win,
and it takes over the country, and it starts the occupation,
all it does is prolong the war.
Because then it gets to the nasty stuff, the dirty stuff.
Generally, that lasts six to seven years.
Some countries are so stubborn, they will hold on for 20.
If they don't have that formula, 20 per 1,000, they lose.
There aren't really any exceptions to this.
So, no, it's a bad talking point.
Whether you like it or not, the Western assistance to Ukraine
is speeding the end of the war.
Even if you want Russia to, you know,
complete its imperialist endeavors
for the sake of their capitalist oligarchs,
even if you want that to happen,
understand that it's worse for the people in Ukraine,
because the war will last longer.
More workers will be hurt.
More workers will be yanked out of factories in Russia
and sent in to fight.
This is a bad talking point,
and it is absolutely Russian propaganda.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}