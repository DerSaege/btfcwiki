---
title: Let's talk about Santos shenanigans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xaOzV3qZ2jo) |
| Published | 2022/12/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about controversies in New York and DC, originating from New York, particularly regarding a person named Santos who recently won a Congressional election.
- Questions have been raised about Santos' biographical data, including education, work experience, and heritage.
- Calls for Santos to step down have emerged, and prosecutors are reportedly looking into the situation.
- Congressperson Richie Torres is introducing an act called the Stop Another Non-Truthful Office Seeker Act in response to the Santos controversy.
- The act aims to require candidates to provide accurate information about their educational background, military service, and employment history under oath when filing for candidacy.
- There are doubts about the act passing due to potentially limiting Congresspeople's ability to lie to the public.
- Speculation that prosecutors might be investigating more than just misrepresentations about Santos' background, potentially related to campaign finance.
- Uncertainty about the outcome of the situation and the potential implications beyond the misrepresented biographical data.

### Quotes

- "Stop Another Non-Truthful Office Seeker."
- "You're limiting Congresspeople's ability to lie to the American public."
- "There's probably more to it."

### Oneliner

Beau dives into controversies surrounding a recent Congressional election winner, Santos, prompting calls for accountability and introducing potential legislation to address misrepresentations in candidacy filings.

### Audience

Voters, legislators

### On-the-ground actions from transcript

- Contact Congressperson Richie Torres to express support for the Stop Another Non-Truthful Office Seeker Act (suggested).
- Stay informed about the developments in the Santos situation and potential legislative changes (implied).

### Whats missing in summary

Detailed analysis and context surrounding the controversies in New York and DC, as well as the potential impacts of introducing legislation like the Stop Another Non-Truthful Office Seeker Act.

### Tags

#NewYork #WashingtonDC #Congress #Legislation #Accountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about some shenanigans
going on in New York and in DC, originating in New York.
I know earlier this week, we talked about Santos.
And if you aren't aware of the story,
a person ran for Congress and they won their election.
After they won, a lot of questions have been raised
about their biographical data in pretty much every regard.
It's just everywhere, from education to work experience
to heritage, I mean everything is in question.
There definitely seems to have been some embellishments
along the way.
Now there are a lot of people calling for him to step down,
obviously, and there have been statements saying
that prosecutors are looking into it.
The thing is, as far as the misrepresentations,
I don't know that it's actually illegal.
But Congressperson Richie Torres is introducing an act
to fix that.
The act is called the Stop Another Non-Truthful Office
Seeker Act.
Stop Another Non-Truthful Office Seeker.
So the Santos Act, that's got to hurt.
It would require people to provide information
about their educational background, military service,
and employment history.
And it would be included with their filing
a statement of candidacy, which means it would be under oath.
So if you're going to run for Congress,
you have to fill out this form.
This form, if this was to pass, would now
include basically a work history and an educational history.
And you would have to fill it out accurately.
If you lied then, it would be a violation of federal law.
So that is something that is currently underway.
I don't know whether or not it's going to pass, to be honest.
It seems like something that should.
But at the same time, you're limiting Congresspeople's
ability to lie to the American public.
So I don't know if they would vote in favor of that.
But the proposal is being made.
As far as what's going on with the Santos situation,
I would imagine that the prosecutors might
be looking into something other than just
the misrepresentations about his work history and education
and stuff like that.
There's probably more to it.
We'll have to wait and see.
My guess is that this may have something
to do with campaign finance or something like that.
So anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}