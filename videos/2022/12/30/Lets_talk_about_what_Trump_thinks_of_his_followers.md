---
title: Let's talk about what Trump thinks of his followers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_q6y4jfCWpo) |
| Published | 2022/12/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's recent statement illustrates his low opinion of his supporters.
- The January 6th committee withdrew the subpoena for Trump.
- Trump twisted the narrative, claiming the withdrawal indicates he did nothing wrong.
- In reality, the committee recommended charges against Trump.
- Trump believes his supporters are not intelligent enough to see through his deception.
- Trump's tactic aims to manipulate and mislead his followers.
- Beau suggests using this as a talking point to raise doubt among Trump supporters.
- Trump's statement reveals his disregard for the intelligence of his followers.
- This moment exposes Trump's condescending view of his base.
- Beau hints at using this incident to challenge the blind loyalty of Trump supporters.

### Quotes

- "He openly indicates that he believes his supporters are very unintelligent."
- "My supporters are so dumb, they will believe this."

### Oneliner

Trump's deceptive narrative on the withdrawn subpoena reveals his low opinion of his supporters, aiming to manipulate their perception and loyalty.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Challenge misinformation among Trump supporters (suggested)
- Engage in constructive dialogues with family members (suggested)

### Whats missing in summary

The full transcript provides additional context on Trump's manipulation of his supporters and the importance of addressing misinformation within political discourse.

### Tags

#Trump #Supporters #Deception #Misinformation #PoliticalManipulation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk a little bit
about how Trump views his supporters
and how he thinks of them.
Because there was a recent statement
that illustrates how little he thinks of them.
And I'm sure he didn't mean for it to do this,
but it is what it is, to quote the man himself.
If you don't know, the committee, the January 6th
committee, withdrew the subpoena for it.
I mean, that stands to reason, because the committee's ending
and the report's out.
This was Trump's response.
This was his statement.
Was just advised that the unselect committee
of political thugs has withdrawn the subpoena of me
concerning the January 6th protest of the,
crooked, all caps, 2020 presidential election.
They probably did so because they
knew I did nothing wrong or they were about to lose in court.
Perhaps the FBI's involvement in rigging, all caps,
the election played into their decision.
In any event, the subpoena is dead, all caps.
OK, I don't think I need to say there was nothing
wrong with the election.
And the FBI didn't rig the election.
I don't feel like I need to say that, but it's there anyway.
The part that matters here, they probably
did so because they knew I did nothing wrong.
He thinks his supporters will believe that.
He thinks that they will see the withdrawn subpoena
and believe that that means the committee came
to the conclusion that he didn't do anything wrong.
What's the reality?
What actually happened?
The subpoena was withdrawn because the committee
is winding everything up and they've put out their report
in which they recommended charges against Trump.
They absolutely did not decide that he did nothing wrong.
But that's a statement he's putting out to his followers
because he doesn't believe they're
smart enough to understand what actually happened.
This is one of the more blatant moments where he just,
he openly indicates that he believes his supporters are
very unintelligent and can't figure out
anything for themselves.
Moments like this are probably pretty useful
for reaching out to those who are still under his spell.
I would imagine that most people are aware of the fact
that the committee recommended charges.
The idea that the withdrawn subpoena somehow
indicates his innocence, which is the line he's feeding them,
should be enough to kind of at least create a little bit
of doubt in the minds of his supporters.
Because, I mean, honestly, this is Trump walking out and saying,
my supporters are so dumb, they will believe this.
It might be a useful tool if you're running into any family
over New Year's.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}