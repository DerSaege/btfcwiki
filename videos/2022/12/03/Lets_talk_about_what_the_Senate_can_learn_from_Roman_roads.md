---
title: Let's talk about what the Senate can learn from Roman roads....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yyC2DJO7vQ4) |
| Published | 2022/12/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Roman roads built thousands of years ago are linked to economic prosperity today based on a study using satellite images taken at night.
- The study found that living near Roman roads meant living in a more economically prosperous area.
- There's a unique link between Roman roads from ancient times and economic activity today, although a causal relationship hasn't been established yet.
- The causal relationship is unclear - it's unknown whether Roman roads caused prosperity or just connected areas already prospering.
- Infrastructure is a key driver of economic activity and growth, as shown by the impact of Roman roads on economic prosperity.
- The survey found that areas in the US with less infrastructure tend to have lower economic prosperity.
- Infrastructure, whether it's roads, railroads, or broadband, plays a vital role in creating prosperity.
- Maintaining infrastructure is not just about the ability to move; it significantly impacts economic activity.
- The US needs to invest in sustainable infrastructure to avoid falling behind and causing damage to the country and the world.
- Making the decision to invest in viable and sustainable infrastructure is critical for long-term impact.

### Quotes

- "Infrastructure causes economic activity. It causes economic growth."
- "Infrastructure means money and it doesn't matter what kind you're talking about."
- "We're at a crossroads and we need to make the turn to start building infrastructure that is not only economically viable, but also sustainable."

### Oneliner

Roman roads from ancient times are linked to modern economic prosperity, stressing the critical role of infrastructure in driving economic growth and the urgent need for sustainable investments in the US.

### Audience

Policy makers, community planners

### On-the-ground actions from transcript

- Invest in local infrastructure projects (implied)
- Advocate for sustainable infrastructure development (implied)
- Support policies promoting economic growth through infrastructure investments (implied)

### Whats missing in summary

The full transcript provides a detailed historical context and a broader analysis of the impact of infrastructure on economic prosperity.


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about how
all roads lead to Rome. I mean at least most of them. And what we can all learn,
particularly those in the U.S. Senate, from old Roman road builders.
There was an interesting study, survey, that came out. And what they did was they took
satellite images taken at night and overlaid maps of old Roman roads.
Roads built thousands of years ago. And what they found, based on the light, which is a
way of approximating economic activity, was that if you lived near Roman roads,
you lived in a more economically prosperous area. Today there is a link
between Roman roads, built way back then, and economic prosperity today.
That's unique. There's definitely calls for further study here.
Now there's not a causal relationship established yet. They haven't been able
to determine the causal relationship because they don't know whether or not
the Roman roads caused the prosperity to begin with or the Roman roads linked
areas that were already beginning to prosper and therefore just allowed it
to continue to prosper. But either way, it doesn't matter which causal
relationship it is. There is a link between Roman roads from way back then
and economic activity today. So why does this matter? With all the discussion that
takes place in this country about infrastructure, it might be worth
remembering. Infrastructure causes economic activity. It causes economic growth.
When you hear people in the Senate complain about spending on
infrastructure, it's important to remember this. When you look at states in
the US, you'll find that. You will find that areas that don't have a lot of
infrastructure, well, they don't make a lot of money. It's common sense, but this
survey really kind of drives it home. And it was remarkably consistent. The only
places that it varied were outside of Europe. And the simple answer to that is
willed vehicles weren't used as much. They started using caravans and
camels. So the roads didn't need to be maintained. So that link doesn't exist as
much because the infrastructure wasn't maintained. It's pretty simple and reinforces
the rest of the concept. When we talk about roads or railroads and those who
maintain them, it's important to understand that it's not just the simple
ability to move. It is the infrastructure that drives American economic activity.
It's what creates prosperity. Infrastructure means money and it
doesn't matter what kind you're talking about, whether it be broadband or
railroads. This topic is going to continue to come up because the United
States will eventually start to fall further and further behind. And it will
continue to do so unless the US ups its infrastructure and transitions away from
the kind of infrastructure that damages the country and the world. We're at a
crossroads, so to speak, and we need to make the decision and we need to
make the turn to start building infrastructure that is not only
economically viable, but also sustainable. Something that will have an impact
thousands of years from now. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}