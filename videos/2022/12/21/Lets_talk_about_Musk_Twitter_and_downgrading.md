---
title: Let's talk about Musk, Twitter, and downgrading....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tunPcpPU6b8) |
| Published | 2022/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk conducted a poll on Twitter asking if he should resign, with a majority voting yes, including about 10 million users.
- Musk engaged in a Twitter exchange where the suggestion of only allowing verified users (blue checks) to vote was made, which Musk seemed to entertain, hinting at voter suppression.
- Tesla, the company, was downgraded by Oppenheimer & Company due to concerns about banning journalists without clear standards, potentially affecting consumers who support climate change mitigation.
- There are worries about a negative feedback loop for Tesla stemming from the controversial content some unbanned users create on Twitter, leading to brands pulling out and users leaving the platform.
- Oppenheimer believes that Tesla's value drop, around 50%, could be influenced by these Twitter-related issues.
- Beau suggests that Musk stepping away from the company and bringing on a board with content guidelines could be a way out of the situation.
- Beau challenges the idea that going woke leads to going broke, citing the real issue as going fast without cash as the downfall for companies.
- The situation raises concerns about the impact of Twitter controversies on Tesla's market value and reputation.

### Quotes

- "The reality is, go fast, no cash."
- "In what is just a hilarious development after that, Musk was seen in a conversational Twitter with somebody who suggested that he only allows those people with blue checks to vote."
- "One of the reasons here, we believe banning journalists without consistent defensible standards or clear communication in an environment where many people believe free speech is at risk is too much for a majority of consumers to continue supporting Mr. Musk slash Tesla."

### Oneliner

Elon Musk's Twitter antics and potential voter suppression raise concerns about Tesla's market value and reputation, prompting suggestions for a way out.

### Audience

Social media users

### On-the-ground actions from transcript

- Contact Tesla and express concerns about the company's handling of controversies on Twitter (suggested)
- Join or support organizations advocating for responsible social media conduct by companies like Tesla (implied)

### Whats missing in summary

The full transcript provides deeper insights into the potential consequences of social media controversies on corporate entities like Tesla.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Twitter and polls
and being downgraded and Tesla and how it all ties together.
For those who are not on Twitter and who are not
aware of just the wild goings on,
recently Elon Musk tweeted out something to the effect of,
Should I resign in the form of a poll?
A very clear majority of users who voted said yes.
Something like 10 million users said
that they wanted him to resign.
In what is just a hilarious development after that,
Musk was seen in a conversation on Twitter
with somebody who suggested that he only
allows those people with blue checks to those
are people who are paying to vote.
And in true voter suppression style,
it does appear that Musk found that to be a good idea.
And what I'm sure is unrelated news,
Tesla, the company, has been downgraded
by Oppenheimer & Company.
One of the reasons here, we believe
banning journalists without consistent defensible standards
or clear communication in an environment where many people
believe free speech is at risk is
too much for a majority of consumers
to continue supporting Mr. Musk slash Tesla, particularly
people ideologically aligned with climate change
mitigation, who would be the people buying those cars.
There's also concern about a negative feedback loop, which
is something we discussed on this channel initially, where
because of the content that is created by some of the people
he has unbanned, some of which are literal fascists,
Brands won't want to advertise on Twitter.
When the brands leave, they don't
want to be associated with it.
Users leave, which causes more brands to leave,
and it sends the company into a death spiral.
It appears that Oppenheimer believes
this will influence Tesla as well, which
is an understandable belief because my understanding is
that Tesla has dropped like 50% in value, the stock price has.
The only way out for Musk that I can see
is to step away from the company and bring on a woke board that
will institute some content guidelines
and hopefully have some idea of how the creator economy works.
I know that many people will say, go woke, go broke.
But time and time again, we have seen that that's not true.
The reality is, go fast, no cash.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}