---
title: Let's talk about encryption, tech, and cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PZYJnXAveww) |
| Published | 2022/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Apple is enhancing encryption to better protect user information, upsetting the FBI who are concerned about the difficulty in accessing information. 
- The debate revolves around the balance between personal freedom and the ability of law enforcement to do their job effectively. 
- This conflict mirrors the ongoing battle between social media companies and politicians over control of the narrative.
- Encryption debates are expected to increase globally, leading to proposed legislation and significant arguments on the topic.
- Apple’s goal is to enhance product security, not cater to criminals, showcasing capitalism in action.
- The underlying question is how much inconvenience society is willing to accept to maintain personal freedom.
- The clash between tech companies and law enforcement regarding encryption is anticipated to intensify.
- The encryption issue signifies broader philosophical debates on safety, security, and freedom in the digital age.
- The scenario with Apple and the FBI represents a microcosm of the larger issue of balancing security and liberty.
- The conflict between protecting privacy and enabling law enforcement access is a complex and ongoing dilemma.

### Quotes

- "The debate is about the balance between personal freedom and law enforcement’s ability to do their job."
- "This conflict mirrors the ongoing battle between social media companies and politicians over control of the narrative."
- "Apple’s goal is to enhance product security, not cater to criminals."
- "The clash between tech companies and law enforcement regarding encryption is anticipated to intensify."
- "The conflict between protecting privacy and enabling law enforcement access is a complex and ongoing dilemma."

### Oneliner

Apple enhances encryption, sparking debate on balancing privacy and law enforcement access, a microcosm of broader philosophical conflicts in the digital age.

### Audience

Tech Users, Privacy Advocates

### On-the-ground actions from transcript

- Contact local representatives to voice opinions on privacy and encryption (suggested)
- Join tech advocacy groups promoting user privacy rights (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the ongoing encryption debate and the potential implications for privacy rights and law enforcement access.


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
Apple trying to make their products better and who they wound up upsetting in the process.
And this is a little bit of foreshadowing to something we can expect to see a lot of in the
future. So Apple has introduced or is introducing a better, more encompassing encryption.
Basically right now there's a vulnerability when it comes to stuff in storage and on the cloud.
Apple is planning to close that gap and better protect their users. The goal of this
is to stop criminals from obtaining your personal information if you're using an Apple product.
The goal is to stop criminals. Guess who's super mad about it? The FBI.
The FBI is incredibly upset. I believe they use the words very concerned about this because it's
apparently just wall-to-wall encryption with only user access. So it would make it very difficult
for them to search something. So obviously it's your personal device. You should be able to
encrypt it. It's not like the FBI has regulations on how good your locks can be.
And that's the physical world comparison. But we live in a very different age now and that's not
the way people like to look at things. There are trade-offs that occur when it comes to safety,
security, true freedom, liberty, all of these philosophical concepts. And there are trade-offs
that occur. This is going to be something we see a lot of in the news.
The same way that you are seeing social media companies pitted against the government because
the government doesn't like, and in this case when I'm saying government I'm talking about
politicians, politicians don't like losing control of the narrative. And social media allows that to
happen. So you're seeing this fight begin to brew between social media and politicians. That one is
well underway. A follow-up will be a fight between tech companies and law enforcement and state
security apparatuses all over the world about encryption. This isn't an argument that's going
to go away. Eventually there will be legislation that gets proposed. It's going to be a thing.
As everybody presents their doomsday scenarios, keep in mind the goal of Apple is literally to
create a better product and keep your information safe. That's what they're doing. Believe me,
Apple didn't sit down and say, hey, let's cater to criminals who want to hide from the FBI. It's
not what they're doing. They're trying to make a better product for you. It's capitalism in action
right there. The FBI does have cause to be bothered by this because it is going to make their job
infinitely harder. The question that the American people have, and eventually this is going to be
an argument all over the world, is how much inconvenience are we going to foist onto law
enforcement to maintain personal freedom? It's a debate that is going to rage. I'm sure that
everybody has their opinion. I have mine. But at the end of the day, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}