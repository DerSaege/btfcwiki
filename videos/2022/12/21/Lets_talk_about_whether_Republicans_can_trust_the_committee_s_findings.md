---
title: Let's talk about whether Republicans can trust the committee's findings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KNgxoYQK6Wc) |
| Published | 2022/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing whether Republicans can trust the committee's findings and report.
- Addressing concerns about the committee being Democrat-run with no Trump supporters.
- Explaining that the committee did not testify or generate evidence; Trump's team did.
- Emphasizing that the most damaging information came from Trump's own team.
- Arguing that if you can't trust Trump's team's testimony, you can't trust the Trump administration.
- Debunking the idea of Republicans on the committee as RINOs, citing Liz Cheney's Republican history.
- Stating that lifelong Republicans are not RINOs but Trump, who manipulated a susceptible base.
- Asserting that closer to Trump, individuals were more likely to ignore subpoenas or plead the fifth.
- Noting that the most damaging testimony came from Trump's White House team.
- Leaving it to individuals to decide whether to trust Trump administration.

### Quotes

- "If you can't trust that testimony, I mean, that kind of says you can't trust the Trump administration, right?"
- "The lifelong members of the Republican Party, they're not the RINOs. That's Trump."
- "The most damaging information that came forward came from the testimony of Trump's team."
- "As far as the most damaging testimony, it wasn't brought forth by the Democratic party."
- "It says a whole lot about whether or not you can trust any Trump administration."

### Oneliner

Republicans question committee's findings, but most damaging info came from Trump's team, not the committee; Trust in Trump administration at stake.

### Audience

Republicans

### On-the-ground actions from transcript

- Examine the evidence presented by Trump's team and decide whether to trust the Trump administration (implied).
- Challenge preconceived notions about Republicans on the committee and understand their history (implied).

### Whats missing in summary

Detailed breakdown of Liz Cheney's Republican history and examples of Trump's manipulation of a susceptible base.

### Tags

#Republicans #Trust #CommitteeFindings #TrumpAdministration #LizCheney


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we are going to talk about whether or not
Republicans can really trust the committee's findings,
whether or not they can trust the report
and the representations that the committee has made.
And we're going to do this because in a recent video,
I talked about how representatives from all three branches of government would have kind
of co-signed the idea before Trump ever wound up in court over it.
And I got a message about that.
Don't portray the committee as representing Congress.
It was a Democrat-run committee with some RINOs thrown in for good measure.
You didn't have anybody who supported Trump on the committee.
We can't trust their report when it's nothing but anti-Trumpers.
We're going to get to the idea of the RINOs on the committee here in a minute, but I wanted
to talk about something else first because I have a feeling people are going to tune
out when I get to that.
So let's say you're a Republican and you truly believe in your heart of hearts that
the people on that committee were nothing but Democratic Party members and rhinos.
Okay, that's fine, but see, here's the thing.
The committee didn't testify.
The committee didn't generate the evidence.
That was Trump's team.
The most damaging information that came forward came from the testimony of Trump's team, people
on his side, literally his team, his staff.
That's where the most damaging information came from.
That has nothing to do with the committee.
The makeup of the committee doesn't play into their answers at all.
Now, you can bring up the idea that maybe the committee asked questions in a way that
precluded any exculpatory evidence or something like that.
Sure, I've talked about it on the channel.
This hasn't been tested in court and there was no cross, fine.
But the thing is, you're looking at a series of questions that were very direct, and the
answers were very direct, and those answers came from people who were on Trump's team.
They weren't never Trumpers.
They were literally on his staff.
That's where the really damaging stuff came from.
So if you're saying that you can't trust that testimony, I mean, that kind of says
you can't trust the Trump administration, right?
Now let's get to the idea of the Republicans on the committee being rhinos.
Republicans in name only.
Because that's getting pushed out now, right?
That's the big thing.
really bipartisan, even though both parties were represented, because those, they don't
really count.
They're not really Republicans.
Okay, so when you really think about it, let's take Liz Cheney.
Let's take her, because she's getting the bulk of this criticism.
When Trump was leaving the reform party and becoming a Democrat, what was Cheney doing?
She was being vetted to become a Republican official at State Department.
That's what she was doing.
When Trump was kind of deciding to leave the Democratic party, what was Cheney doing?
I think she had just wrapped up being a co-chair for a Republican presidential campaign.
And then when Trump had finished up being an independent in 2012, what was Cheney doing?
She was a Republican contributor to Fox News filling in for Hannity.
I hate to be the one to break it to you, but the lifelong members of the Republican Party,
they're not the RINOs.
They're not the Republicans in name only.
That's Trump.
Trump shopped around until he found a group within a party that was easily manipulated.
And then he played to that base.
That's what he did.
of the Rhino, not the people that have been Republicans since George Bush Jr. was in office.
When you're looking at that evidence, the committee makeup, sure, there might be something
that casts a different view of Trump and that would be brought up in court.
But as far as what was presented, either you believe that testimony or you don't.
And I would point out that the closer it got to Trump himself, the more likely somebody
was to ignore subpoena or plead the fifth.
As far as the most damaging testimony, it wasn't brought forth by the Democratic party.
It wasn't brought forth by somebody you might think is a rhino.
It was brought forth by Trump's own White House team.
Whether or not you trust that, well, that's up to you.
But it says a whole lot about whether or not you can trust any Trump administration.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}