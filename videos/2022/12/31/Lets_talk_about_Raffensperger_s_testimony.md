---
title: Let's talk about Raffensperger's testimony....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_L0ogdRLk-w) |
| Published | 2022/12/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the testimony from Raffensperger regarding Trump's phone call asking to find votes.
- Raffensperger felt that Trump's message of "no criminality" was dangerous and could imply consequences.
- Raffensperger believed Trump's message was a threat of violence, as stated in his book and under oath.
- Testimony suggests the White House Counsel's office shared concerns about the phone call's implications.
- The link between Trump's actions and the events of January 6th is drawn, with people being influenced by lies.
- Georgia's investigation is ongoing, indicating potential future actions based on the testimony.
- Raffensperger's feeling personally threatened could lead to legal implications in Georgia.
- The Department of Justice's actions will determine if history repeats itself or rhymes regarding the events.
- The investigation in Georgia may lead to significant developments, despite limited information disclosure.
- Beau concludes with the expectation of Raffensperger's testimony playing a prominent role in upcoming proceedings.

### Quotes

- "He felt it was a threat of violence. That's big. That's huge for Georgia."
- "People were spun up to just believing the lies that were told to them."
- "Georgia is still moving ahead with their investigation."
- "I'm fairly certain that the passage I just read and very similar testimony will be featuring pretty prominently."
- "It absolutely was one of those hinge points in American history."

### Oneliner

Raffensperger's testimony on Trump's alleged threats and the DOJ's actions determine Georgia's future actions amidst ongoing investigations and historical significance.

### Audience

Georgia investigators

### On-the-ground actions from transcript

- Support ongoing investigations in Georgia by cooperating with authorities (implied).
- Stay informed about the developments in the case and advocate for justice (implied).

### Whats missing in summary

Insights on the potential legal and political ramifications arising from Raffensperger's testimony.

### Tags

#Georgia #Trump #Raffensperger #DOJ #Investigation


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk a little bit
about Raffensperger and that now very famous home call
in which Trump was asking him to find votes.
We're gonna kinda go through a little bit of the testimony
that came out on the 6th,
that came from the committee, the January 6th committee,
and we're gonna go through it
and then talk about why you're going to see it again.
OK.
So he was asked, and in the next paragraph, he says,
but I mean all this stuff is very dangerous stuff.
When you talk about no criminality,
I think it's very dangerous for you to say that.
I know this is similar to what I asked you about before,
but when he says this is dangerous stuff
and I think it's very dangerous for you to say that,
meaning no criminality, what message do you think he was trying to send you?
He is Trump, send you is Raffensperger.
And Raffensperger said, I guess that pressure, I wouldn't use the word subtle pressure,
that there could be consequences.
The follow-up question is, in your book on page 194, your observation regarding that
same passage that I just read to you, you wrote observation I felt then and still believe
today that this was a threat.
Others obviously thought so too, because some of Trump's more radical followers have responded
as if it was their duty to carry out this threat.
Do you still believe that today?
And he said, yes.
So you have Raffensperger under oath saying that he felt it was a threat of violence.
That's big.
That's huge for Georgia.
That is huge for Georgia.
You will see this again.
He said this in his book, so this testimony isn't really a surprise, but there's a big
difference between writing it in a book and testifying to it under oath.
The investigators in Georgia, they have a clear picture of what he's going to say.
This is the person who received that call saying that he felt that it was the threat
of violence that's huge.
So it's going to come up again.
I do want to point out a couple of other things.
is that you have testimony from Hutchinson that suggests the White House
Counsel's office kind of felt the same way about it and was very concerned
about this phone call and what would happen if it leaked like it did and the
appearance that it was a pressure campaign which is definitely how it was
received on the other end of the call. So they they certainly had reason to worry.
Um, then there's also this bit, and this is back to Raffensperger.
Um, and so when you say people did die, it sounds like you're drawing a connection
between something president Trump did or failed to do and what happened on January 6th.
Can you explain that?
Well, I think that people were spun up to just believing the lies that were told to
them, and things got out of control.
and it's just one of those hinge points in American history, yeah, that's right.
It absolutely was one of those hinge points in American history. Whether or
not history repeats itself or rhymes depends on what the Department of Justice
justice does next.
Now Georgia is still moving ahead with their investigation, which this information leads
me to believe that we will see action in Georgia, because I'm certain that they have this as
well.
Secretary of State of Georgia saying that he felt personally threatened, yeah
that that's gonna fall under under their statutes I'm pretty sure and I think a
reasonable person would believe that as well which might be the the test in that
state. With the special counsel things have become very they've become a lot
more secretive. There's not as much information coming out about what's
happening. I don't think people should take that to mean that this is over
because I don't think it is. I'm fairly certain that the passage I just read
and very similar testimony will be featuring pretty prominently over the
Anyway, it's just a thought.
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}