---
title: Let's talk about what the US is getting out of Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kdcf8g9gBvM) |
| Published | 2022/12/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the US benefits from supporting Ukraine and the questions arising from right-wing talking points about the money spent.
- Nations prioritize interests over friendships, morality, or ideology.
- The economic power Ukraine could become after the war and its alignment with Europe benefiting the US and NATO.
- Investing in Ukraine to prevent it from becoming a Russian colony and helping keep Russia in check.
- The US benefits from buying a degraded Russian military, simplifying its international poker game with China now as the main competitor.
- The $50 billion investment in Ukraine is effective in knocking Russia out of the high stakes poker table.
- This support helps rebuild the US's reputation on the international scene and frame the contest as democracy versus authoritarianism.
- Ukrainians want to remain Ukrainian, not become Russian, which is often overlooked in foreign policy questions.
- Despite the strategic benefits, it's vital to always bring back the human aspect of conflicts and the impact on people.
- Overall, supporting Ukraine comes with multiple strategic and geopolitical advantages for the US.

### Quotes

- "Nations don't have friends. They don't have morality. They don't have ideology. They have interests."
- "The last time the United States engaged in a Cold War with Russia and engaged in the military expenditures that go along with this, it was $13 trillion."
- "When somebody is asking this question, 'what are we getting out of it?' I understand it from a two sizes, two small heart."
- "In real life, people do matter."
- "All of this money is spent on weapons because there are people who are having rockets rained down on their heads."

### Oneliner

Exploring the strategic benefits for the US in supporting Ukraine while acknowledging the human cost.

### Audience

Foreign policy enthusiasts

### On-the-ground actions from transcript

- Support organizations aiding Ukrainian civilians (suggested)
- Advocate for diplomatic solutions to conflicts (implied)

### Whats missing in summary

The emotional toll and human suffering amidst geopolitical strategies.

### Tags

#US #Ukraine #Geopolitics #ForeignPolicy #HumanitarianAid


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about what
the US is getting out of it.
And it's a fair question.
I guess over the holidays, a lot of people
talked with family members.
And their parents or whomever had things
to say about the amount of money the United States was
spending on Ukraine and wanted to know what the US was getting
out of it.
Because they're right-wing talking points.
It's about money, not people and everything
that we've seen in that footage, just about the money.
But at the same time, it's not an unfair question, something
that we've talked about on this channel repeatedly.
Nations don't have friends.
They don't have morality.
They don't have ideology.
They have interests.
So the $50 or so billion, what's the US getting for that?
OK, first, on the other side of this,
when Ukraine emerges from the other side of this war,
it is going to be an economic power, a big one.
If at the end of the war, it is aligned with Europe,
that's good for the United States.
It's good for NATO.
We're talking about economic alignment right now.
If it falls and becomes a Russian colony,
that economic power flows to Moscow.
It gets extracted out of Ukraine and sent to Moscow.
So that's one thing.
And undoubtedly, the billions that have been spent,
that investment would pay off just
from the economic benefits.
The other thing that NATO gets, and therefore the United States,
is a Russia in check.
If Ukraine succeeds on the other side of this,
they're going to be an economic power that just went
through a war that had a whole lot of help from the West.
Even if they don't join NATO, they're
going to be very much aligned with NATO,
and they're going to have a military,
and they're going to be right up against Russia's border.
That limits Russia's ability to make aggressive moves
on the international stage.
That is worth $50 billion.
So now we have two things.
And then you have the big one.
The United States is buying a degraded Russian military.
We talk on this channel about foreign policy,
the international poker game where everybody's cheating.
There are different tables.
And for a long time, Russia was at the high stakes table.
They were the big spender table because they
were perceived to have a lot of military capability.
When the invasion failed the way it failed,
they had to go sit at a lower stakes table.
The damage that has been done to Russian military reputation
and Russian military infrastructure and equipment
is just immense.
If you go back to before this war started,
you will see foreign policy people
talking about the future of a multipolar near-peer contest.
Multipolar, China, Russia, United States.
Those are the three poles near-peer.
After the degradation of Russia's military,
they're not a near-peer, and they're not
going to be a pole.
So it simplifies the high stakes poker game
for the United States.
China is now the main competitor nation.
Russia is, they're powerful because they have nukes,
but they're not going to sit at this table anymore.
And it prevents the United States
from having to engage in two Cold Wars
at once with Russia and China.
The last time the United States engaged in a Cold War
with Russia and engaged in the military expenditures
that go along with this, it was $13 trillion.
The $50 billion that has been given to Ukraine,
that $50 billion was far more effective at knocking Russia
out of that high stakes poker table.
It's a bargain for the United States.
When you strip away the morality and the humanity
of what is going on, this is a huge win for the US.
When you look at it, war as politics by other means,
and you forget about all of the horrible things
that accompany this, and you're just looking at the math,
it's a big win for the US.
I would like to remind everybody as we talk about the numbers here,
these are people.
When somebody is asking this question,
what are we getting out of it?
I understand it from a two sizes, two small heart.
I get the question, and it's a legitimate form
policy question, but I would suggest that one of the things
the United States is getting out of it
is kind of rebuilding its reputation
on the international scene.
You can't really put a price on that,
but helping a country defend itself
against imperialist aggression is something
the United States isn't really known for,
and it helps rebuild that perception
of moral authority.
This also helps frame the coming near-peer contest
as democracy versus authoritarianism.
People who are very much into foreign policy right now
are laughing.
Yes, not all of the United States allies
are anti-authoritarian.
There are a whole lot that are authoritarian,
but we're not talking about the United States
being authoritarian, but we're not talking about reality.
We're talking about framing here.
It also gives them that.
I would point out that the people in Ukraine
want to be Ukrainian.
They don't want to be Russian.
I think that for a lot of people,
it's kind of being lost.
I mean, and again, I'm not slamming the questions.
They are legitimate questions.
When you're looking at this from a foreign policy standpoint,
people don't matter.
In real life, they do.
Just always bring it back to the fact
that all of this money is spent on weapons
because there are people who are having rockets
rained down on their heads.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}