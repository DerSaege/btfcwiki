---
title: Let's talk about Sinema leaving the Democratic party and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9WNpAfTK1wc) |
| Published | 2022/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Sinema is leaving the Democratic Party in Arizona to become an independent, not planning to caucus with Republicans.
- She hopes to maintain her committee positions and caucus with Democrats even after leaving the party.
- Sinema's goal appears to be trying to regain the power she had before Warnock's win shifted the Senate majority to the Democrats.
- Beau criticizes Sinema for not truly representing Democratic values and being self-serving in her actions.
- He believes that Sinema's move may not be successful in the long run, especially in polarized election cycles.
- Beau predicts that the Democratic Party may either placate Sinema or actively work against her to secure their majority.
- There is a possibility of a progressive Democrat challenging Sinema in Arizona, depending on grassroots efforts.
- Sinema's votes on major Democratic agenda items may come at a cost, with more focus on serving her contributors.
- Beau suggests that the future outcomes depend on how the average Democrat in Arizona responds and organizes for a replacement candidate pursuing a progressive agenda.

### Quotes
- "Most politicians, to some degree, are self-serving."
- "There may be a chance that the Democratic Party does not run somebody against her."
- "A grassroots effort to elevate other candidates starting now probably be pretty successful."
- "Most of it depends on what the average Democrat in Arizona decides to do."
- "Her votes for anything that is a major Democratic agenda piece, they're going to come at a cost."

### Oneliner
Sinema's move to become an independent in Arizona raises questions about her true motives and potential impact on Democratic dynamics, prompting considerations on the future political landscape.

### Audience
Arizona Democrats

### On-the-ground actions from transcript
- Organize grassroots efforts to elevate progressive candidates in Arizona (implied)
- Respond actively and organize for a replacement candidate pursuing a progressive agenda (implied)

### Whats missing in summary
Analysis of the potential long-term effects of Sinema's decision and the implications for the Democratic Party's strategies and unity.

### Tags
#Arizona #DemocraticParty #Sinema #Independent #Elections


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about Sinema
leaving the Democratic Party and what it means.
We're going to talk about what she's probably hoping
to accomplish, whether or not it will work.
We'll talk about the Democratic Party and their options
and what they're probably going to do.
And we're gonna go from there.
Okay, so Sinema out there in Arizona
is leaving the Democratic Party, becoming an independent.
However, she has said she doesn't plan
on caucusing with the Republicans.
So what that means is she is hoping
that she'll be able to maintain her committee positions
and she'll caucus with the Democrats.
Okay, so if she's not really switching parties,
what's the goal?
She has probably become accustomed
to the amount of sway and power that she has.
With Warnock winning and it becoming a 51 Senate majority
for the Democratic Party, she doesn't have that anymore.
This is her way of trying to obtain it.
Some might see it as incredibly self-serving.
The standard line here is that this is very surprising.
No, it's not.
No, it isn't.
That's if you look at her voting record and her moves.
She is not what the Democratic Party stands for.
She's not for the working class.
She is not for any of the things
you would typically associate with the Democratic Party,
even when she has voted that way.
Most politicians, to some degree, are self-serving.
To me, looking at her record,
this is incredibly true of her.
And this is her trying to maintain power.
She probably thinks that this is her gateway to keep it.
And she's going to try to style herself
as an independent thinker in Arizona.
And that's how she plans to maintain power
because she thinks that if the Democratic Party
runs somebody against her with her as the independent,
that she'll act as a spoiler candidate.
Therefore, the Democratic Party won't really run somebody
and she'll be able to beat whoever the Republican is.
I don't think that's true.
I think that's a miscalculation on her part.
We will be going into election cycles
that are incredibly polarizing and partisan.
The Democratic Party nominee will get the Democratic vote.
It is unlikely, because some of her past votes,
that she gets the Republican vote in Arizona.
That's not a normal Republican vote.
That's not an average when you're talking about the country.
You've got to be out there
to get a Republican win in Arizona.
So they're not going to go for her.
And the Democratic Party will probably be loyal.
Personally, I think she ended her political career with this.
We'll have to wait and see.
There may be a chance that the Democratic Party
does feel the way that she does and not
run somebody against her.
I think that would be a mistake for the Democratic Party,
but we'll see.
Now, as far as the Democratic Party goes in D.C.,
they have two options.
They can placate her or they can go after her to destroy her.
Placating her means she gets to keep her committee positions
and she will caucus with them.
So really not much changes.
The Democratic Party really couldn't count on her vote
to begin with if they go that route.
Going to the other extreme and them actually actively
starting to work against her and lay the groundwork
for a Democratic candidate out in Arizona,
she might decide to caucus with Republicans.
And that would put it back to a 50-50 split.
So again, not a whole lot changes other
than the true majority aspect.
The Democratic Party, if they played hardball, could win.
As far as going after her and trying
to set the groundwork for a real progressive Democrat out there,
I think they could do it.
I don't think they will.
I think they'll probably placate her.
And they'll give her what she wants.
And she will be a spoiler vote, but with less ability to spoil.
Now, to those people in Arizona, what the DC Democrats
decide to do, that doesn't necessarily
mean that's what you have to do.
When you're talking about a state like Arizona,
a grassroots effort to elevate other candidates starting now
probably be pretty successful.
So unless she just totally goes off
the rails, which isn't impossible,
there's not going to be a whole lot of practical changes
to this.
Because even though she was a Democrat before,
she was often, like Manchin, that vote
that they really couldn't count on because she was trying
to serve her own interests.
Or I'm sure she would frame it as serve the interests of,
I don't know, her campaign contributors.
I'm sure there would be some nice way
that she would try to frame it.
But outside looking in, most of her stuff
appeared very self-serving to begin with.
The progressive Democrats, they are
going to want to go after her.
And the rank and file of the Democratic candidates
rank and file the non-politician progressive Democrats,
they're definitely going to.
Depends on how hard and how effective
they are as to whether or not them going after her
will influence the Democratic establishment
and their position on running somebody against her later.
So yeah, I mean, there's all kind of names
that can be thrown out right now.
It's politics.
You have to remember that a whole lot of people in politics
are not there to represent you.
Most aren't.
They're there to represent themselves.
And this is a case where it appears very out in the open
that that's what it is.
But I would count on the Democratic Party
playing ball with her for a little bit at least.
If there is enough backlash against her,
especially in Arizona, the Democratic establishment
may take a different view to it.
But at this point, one thing is for certain.
Her votes for anything that is a major Democratic agenda piece,
they're going to come at a cost.
She's going to want something for her contributors for,
I don't want to say herself.
She is going to want something that's
going to make her look good to those people who
can keep her in power.
And now she cannot count on the Democratic establishment
to help her do that.
There's going to be a lot more wheeling and dealing with her
now if the Democratic Party decides to placate her.
So as far as what it means long term,
we don't actually know yet.
There are multiple options for the future.
And most of it depends on what the average Democrat
in Arizona decides to do and how they respond
and whether or not they organize to provide a replacement that
is actually going to pursue a progressive agenda rather
than stand in the way of one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}