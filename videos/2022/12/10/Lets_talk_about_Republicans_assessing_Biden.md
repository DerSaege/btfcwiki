---
title: Let's talk about Republicans assessing Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=y14G0QArX24) |
| Published | 2022/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are underestimating President Joe Biden by focusing on his flaws and not recognizing his successes.
- Biden has effectively handled the situation in Ukraine without deploying American troops.
- Newt Gingrich suggests that Republicans need to rethink their approach to defeating socialism.
- Opposition evaluation is more accurate than self-evaluation, according to Beau.
- The Republican Party struggles to understand that their policies, not just their framing, contribute to their losses.
- Leftists should push Biden to be more socialist, contrary to the Republican narrative.
- Educating people on the true meaning of socialism can be beneficial for the left.

### Quotes

"Opposition evaluation is always going to be more accurate than a self-evaluation."

"The Republican Party struggles to understand that their policies, not just their framing, contribute to their losses."

"If you're on the left, now might be the time to really start educating people on what that word means..."

### Oneliner

Republicans underestimate Biden; Newt Gingrich calls for rethinking defeating socialism; Leftists urged to push Biden for more socialism.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Educate people on the true meaning of socialism (implied)
- Push Biden to adopt more socialist policies (implied)

### Whats missing in summary

The full transcript provides in-depth analysis and commentary on Republican attitudes towards Biden and the concept of socialism, offering insights into political strategies and messaging.

### Tags

#Politics #JoeBiden #RepublicanParty #Socialism #OppositionEvaluation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to do things a little bit different.
I'm going to read sections of something,
and I'll tell you who it is at the end,
and I'll have the link to the whole thing,
because I'm only going to be reading passages down below.
Then we're going to go over what they're kind of indicating
in this, what it means, and what certain groups might be able
to do to further their cause.
Okay, so it starts off.
Republicans must learn to quit underestimating
President Joe Biden.
The clarity of winning and losing
creates a clarity of analysis about who is doing well
and who isn't.
If you apply that simple model to Biden,
you realize how well he is doing
by his own definition of success.
However, conservatives' hostility
to the Biden administration on our terms
tends to blind us to just how effective Biden
has been on his terms.
We dislike Biden so much,
we pettily focus on his speaking difficulties,
sometimes strange behavior, clear lapses of memory,
and personal flaws.
Our aversion to him and his policies
make us underestimate him and the Democrats.
Biden has carefully and cautiously waged war
in Ukraine with no American troops.
Although poorly timed and slowly delivered,
U.S. weapons and financial aid have helped cripple
what most thought would have been an easy victory
for Russian President Vladimir Putin.
The Biden team had one of the best first-term
off-year elections in history.
And then a little bit of foreshadowing
on what they want to do.
Today, there is not nearly enough understanding
or acknowledgement among leading Republicans
that our system and approach failed.
We need to rethink from the ground up
how we are going to defeat big government socialism.
That's Newt Gingrich.
That is Newt Gingrich, a Republican extraordinaire.
When I want to know what I'm doing wrong,
I tend to not ask my close friends.
I ask people that really kind of don't like me.
Because your opposition's evaluation
is always going to be more accurate than a self-evaluation.
The old guard in the Republican Party
has kind of finally started to wise up
to Biden's low-key approach to stuff
and how much he's been able to get through.
They also, Gingrich in this case,
is pretty heavily pushing to the idea of shifting
from their normal, what they've been doing the last few years,
and moving back to the idea of defeating socialism
and using that word again.
OK.
So here's the thing.
Opposition evaluation of the Republican thought process,
they don't understand how comfortable younger voters
are with the word socialism.
They're going to make Biden way cooler than he is in this.
That's what's going to happen.
If they follow that road and that template,
they will help Biden, not hurt him.
The Republican Party, the leadership there,
is just apparently unable to understand or acknowledge
that it's not their framing.
It's their policies.
Their policies are easy to defeat
because they're from the 1960s.
They went back rather than progressing
with the rest of the country.
It makes them an easy target.
That's why they're on the losing streak they're on.
Now, for leftists, when I say this,
I'm talking about leftists, not liberals.
The most important thing that leftists
can do as far as civic engagement
with the electoral process right now
is constantly hammer Biden for not being socialist enough.
I know that's going to be really hard for y'all,
like telling a bunch of sharks to swim.
That would be incredibly helpful, not just to you,
but to Biden as well.
The Republican Party appears to be poised
to constantly be throwing that word out there again.
If you're on the left, now might be the time
to really start educating people on what that word means
because they're going to be using it
and pointing out that that's not what Biden is.
And for those that don't know,
Biden is not a socialist in any way, shape, or form.
I think the Republican Party is going to have a real hard time
casting him as that.
And I think as they try,
they may end up taking a large segment
of the younger voting bloc
and kind of making them wish that Biden
was the Biden from the Republican memes.
They're going to be wanting dark Brandon.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}