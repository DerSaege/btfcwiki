---
title: Let's talk about the most important climate issue....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UBYugVY4F3w) |
| Published | 2022/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the urgency of climate change and the need for immediate action.
- Explains that there is no one magic solution but rather a need for widespread systemic changes.
- Emphasizes that everything, from energy sources to agriculture practices, must change to combat climate change.
- Talks about the importance of celebrating small wins in transitioning to sustainable practices.
- Acknowledges that time is running out to mitigate the worst effects of climate change.
- Asserts that systems will change one way or another, voluntarily now or abruptly later due to crisis.
- Describes the inevitable transition away from fossil fuels, whether voluntarily or due to supply chain disruptions.
- Stresses the necessity for humanity to address climate change now to ensure a livable planet for future generations.

### Quotes

- "It's not that one thing has to change. It's that everything has to change."
- "Either we come together and treat this as the global emergency that it is, or we don't."
- "Humanity has to deal with climate change. There's not a choice here."
- "If we don't act on climate change, they won't be there."

### Oneliner

Beau addresses the urgency of climate change, stressing the need for widespread systemic changes to ensure a livable planet for future generations.

### Audience

Climate activists and concerned citizens.

### On-the-ground actions from transcript

- Start celebrating small wins in transitioning to sustainable practices (exemplified).
- Advocate for systemic changes in energy, waste management, and agriculture practices (suggested).
- Join or support organizations working towards climate action (implied).

### Whats missing in summary

Importance of taking immediate action to combat climate change and transition to sustainable practices.

### Tags

#ClimateChange #SystemicChange #Urgency #Transition #Activism


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about the climate.
And the one thing we have to do to get it right.
And how I am certain that we will transition.
I got a message.
And it's not an uncommon feeling.
But it's basically somebody that was becoming overwhelmed.
Because they were looking at all of the little things
that people talk about as something
that has to change to mitigate the effects of climate change.
And they're looking for that one thing, that magic bullet that
will stave off the worst effects.
And they asked how I can be so certain that we will transition.
Because I guess in the videos I talk like it's an inevitability.
So let's talk about that one thing that has to happen first.
The magic bullet to solve the climate issue.
There's not one.
It's not a thing.
There isn't one.
It's not that one thing has to change.
It's that everything has to change.
Most systems that people are aware of, that they know about,
that they think that they belong to, they have to shift.
It's not just a matter of changing
the source of our energy.
It's a matter of shortening supply chains,
about generating less waste, about being more economical
when it comes to our agriculture.
It's a lot.
Everything has to change.
There is no one item that's going to solve it or mitigate
the most.
It's a system-wide failure.
It needs a system-wide solution.
And when people hear that, they often
become even more overwhelmed because they
don't see it as likely.
They look at it as a pizza that they have to eat whole,
rather than a snowball.
It starts off small and builds upon itself.
You have to celebrate the wins, the small wins.
Every time some company switches its fleet of vehicles
to electric, that's a win.
Sure, it may only be 10 vans, but that's a win.
That's not going to solve climate change.
No, it's not.
But once that occurs and then the source for the energy that
charges those vans, transitions, and then
transitions, all of this starts to add up.
It's a snowball.
And there are people who say we don't have that much time.
I mean, yeah, we're coming up against a wire.
That's true.
We're definitely going to feel effects as it is.
But we have a little bit of time to deal with the worst effects
as long as we move speedily.
Most systems will have to change.
But here's the good news in a really weird way.
They're going to.
How can I be so certain we're going to transition?
Because it's going to happen one way or another.
Either we come together and treat this
as the global emergency that it is, or we don't.
If we come together, develop a real plan, and enact it,
we can speedily adjust the system
and mitigate the effects.
We can do it voluntarily that way.
Be quick and relatively smooth.
There will be some issues.
That's option one.
Option two is that we don't do that.
But we will still transition.
It'll just be abruptly later.
The people who look the other way on this topic,
who don't actually want to address it,
what they don't understand is we're going
to stop using oil for fuel.
It will occur.
The question is whether we do it voluntarily now,
or it happens abruptly later because the supply
chains become so unstable.
Because the areas where it is pulled out of the ground
become uninhabitable.
Because the demand is too high and the supply is too low.
Because it's too hard to travel.
It's going to occur.
If climate change isn't mitigated,
the worst effects of it, it's a disaster flick.
All of the systems that caused it will come crumbling down.
So I never noticed that I always spoke with the idea
that it was a certainty that it was going to occur.
But I guess it wouldn't surprise me
if I went back through the videos
and found that because I believe that.
One way or another, it is going to occur.
I believe that one way or another, it is going to happen.
It's just whether or not we get that snowball rolling
and do it voluntarily, or we wait for the snowball
or lack of snowballs to crush us.
Humanity has to deal with climate change.
There's not a choice here.
It's something that we have to do.
Either we do it now, we start putting in the work
to change the systems that have failed us, or we wait.
And the planet will make those systems cease to exist.
The creature comforts that most people
want to preserve for their children.
If we don't act on climate change, they won't be there.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}