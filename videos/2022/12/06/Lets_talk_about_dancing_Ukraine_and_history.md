---
title: Let's talk about dancing, Ukraine, and history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f-jDiyrT-RA) |
| Published | 2022/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party is upset about a Ukrainian soldier dancing, comparing them to the village elders from Footloose.
- Some criticize the soldier's dance as unprofessional and question her role because she didn't have ammo pouches.
- Beau defends the soldier's dance, pointing out that military traditions of chanting and dancing date back centuries.
- Stress relief through dancing is common in high-stress jobs like the military or nursing.
- Beau mentions how dancing, singing, and laughter have been activities before combat since ancient times.
- He debunks the hyper-masculine myth by sharing a surprising history of drag shows among US troops in World War II.
- Drag shows were so common that manuals with dress patterns were distributed for soldiers to perform in them.
- Beau explains that stress relief activities like dancing are vital for maintaining morale and coping with difficult situations.
- He contrasts the videos of Ukrainian soldiers dancing with those of another military displaying toughness through shirtless backflips and axe-throwing.
- Beau challenges the myth of the hyper-masculine soldier from 1980s action films, stating it's a fabricated concept.

### Quotes

- "I personally prefer the Pikachu dance."
- "It's normal. It's good. It is a good thing to relieve stress in that way."
- "The hyper-masculine soldier from the 1980s action flicks, that's a myth. It's not real."
- "It's a myth."

### Oneliner

Beau explains the history of military dances and stress relief, debunking hyper-masculine myths by sharing the surprising tradition of drag shows among US troops in World War II.

### Audience

Military personnel, history enthusiasts, social media users

### On-the-ground actions from transcript

- Support stress-relief activities in high-stress jobs (suggested)
- Challenge hyper-masculine myths in your community (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the history of stress relief activities in the military and challenges common misconceptions about soldiers' behaviors during times of conflict.

### Tags

#Military #StressRelief #HyperMasculinity #History #DebunkingMyths


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about one thing
so we can talk about another.
We're gonna talk about dancing in certain situations
and we're gonna get to some really weird history
and hopefully maybe dispel some myths.
So, the Republican Party has apparently turned into
the village elders from Footloose.
They are once again upset about somebody dancing.
In this case, a Ukrainian soldier.
Yes, I know, somebody's gonna say,
well, we don't know that she's a soldier
because she didn't have ammo.
Right, she didn't have ammo pouches either,
she didn't have those shears in her vest,
maybe she's a medic.
I'm just saying. I don't know.
Maybe she borrowed the uniform just to do this.
Maybe. I have no evidence one way or the other.
I will take it at face value.
Ukrainian soldier dancing, doing something called
the Pikachu dance,
and this upset right-wing pundit to the United States.
Some saying they, you know,
can't believe we're paying for that
or how unprofessional it is for the military to dance.
Yeah, tell me you've never been around enlisted soldiers
without telling me.
The US pays for that every day.
It happens constantly with US forces.
And as far as professionalism, chanting, barking,
dancing, singing, this kind of stuff,
when it comes to military tradition,
dates back to before writing.
This has been around a long time.
I have talked about it before.
The last time the Republican Party was mad
about somebody dancing, when it was the nurses.
Another high-stress job.
It's normal. It's good.
It is a good thing to relieve stress in that way.
When you're in a situation like that,
you can either embrace what little bit of light you can find,
or you can be consumed by the dark that is all around you.
And if that happens, you'll be getting other videos
and they have warnings before them.
I personally prefer the Pikachu dance.
So this is going on right now.
This argument is occurring.
And it is laced with the hyper-masculine myth
that is just so pervasive today.
And of course, somebody comes out of nowhere
and says, you know, that doesn't look like a war dance.
Talking about the tradition of dancing.
Looks more like, you know, drag story hour or whatever.
You know, to me, it just looks like a woman.
But I mean, I don't know.
Let's say that it is.
Because the idea there is that those, you know,
real tough American men,
that they wouldn't do anything like this.
Those men who stormed the beaches of Normandy,
fought at Guadalcanal or Midway,
they wouldn't dance and they certainly
wouldn't enjoy a drag show.
That's just unbelievable.
Except it's not, because they did.
Because they did.
Here's your weird history.
Drag shows were so common among US troops during World War II
that special services in the army put out,
what do they call them, blueprint specials.
Basically manuals on how to conduct shows
for the soldiers to be performed by soldiers.
And in them, they had dress patterns.
They had dress patterns in the manuals,
in the little packets that went out.
And instructions on how to perform girly shows.
So not just did the men who stormed Normandy
dance and sing and laugh and do all of the stuff
that people had been doing before combat
since prehistory.
They not only enjoyed the occasional drag show,
they performed in them.
It's important to remember that
it's getting rid of that stress.
That's why it's there.
And that's why you find people in high stress jobs doing it.
And it's also about the people around you.
It's a way of chanting that you're still brave enough
to go do whatever stupid thing they are sending you to do.
The hyper-masculine soldier from the 1980s action flicks,
that's a myth.
It's not real.
It's made up.
It's not a real thing.
I would point out that in the conflict in Ukraine,
there are two state militaries involved.
One putting out videos with soldiers
doing the Pikachu dance.
The other put out videos where, you know,
their super tough looking guy was shirtless
with his beret super glued to his head
as he back flipped off a log and threw an axe at a target.
Those were the two types of videos kind of put out.
Which one of those militaries has been retreating constantly?
It's a myth.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}