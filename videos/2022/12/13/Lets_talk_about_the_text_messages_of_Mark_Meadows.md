---
title: Let's talk about the text messages of Mark Meadows....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GRJ1zUecFuo) |
| Published | 2022/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mark Meadows' text messages have been made public, revealing his contact with over 30 members of Congress before the events of January 6th.
- Meadows and others were discussing ways to keep Trump in office, disregarding the voice of the people and subverting democracy.
- Representative Ralph Norman's message urging the invocation of martial law to save the Republic has gained attention, despite a misspelling.
- The message was sent on January 17th, three days before Biden's inauguration, indicating ongoing attempts to overturn the election results.
- Advocating for martial law to save the Republic goes against democratic principles and actually subverts them.
- Meadows was being urged by a member of Congress to convince Trump to declare martial law after the events of January 6th.
- Despite court rulings and the truth coming out, some individuals were still willing to use military force to suppress the will of the people.
- The actions and intentions of those pushing for extreme measures after the election raise serious questions about their fitness for office.

### Quotes

- "We are at a point of no return in saving our Republic. Our last hope is invoking Marshall, misspelled, law."
- "Creating a dictatorship declaring martial law does not save a republic, it subverts it."
- "Even after everything that happened on the 6th, there were still at least a few who were entertaining the idea of using the United States military to suppress the people."

### Oneliner

Mark Meadows and members of Congress discussed extreme measures to keep Trump in office, including advocating for martial law, despite court rulings against their claims.

### Audience

Concerned citizens, democracy advocates.

### On-the-ground actions from transcript

- Contact your representatives to ensure they uphold democratic principles (suggested).
- Stay informed about political developments and hold elected officials accountable (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the alarming messages exchanged by Mark Meadows and members of Congress, shedding light on attempts to subvert democracy and overturn election results.

### Tags

#MarkMeadows #TextMessages #January6th #ElectionSubversion #Democracy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Mark Meadows and text
messages.
His text messages, quite a number of them,
have become public.
Now, the committee's had them for quite some time,
but now a lot of them are available to us.
I don't think anybody's going to be surprised
the general content of them, I think people might be surprised at how expansive it was.
In the days leading up to the 6th, Meadows was in contact with more than 30 members of Congress
as they came up with different ways to keep Trump in office.
Another way to say that would be as they schemed to disregard the voice of the
people, throw out the Constitution, and subvert an election. 30 of them. More than
30 of them. There's one message in particular that is getting a lot of
attention, partially because of the content and partially because of a typo.
typo. It's not a typo, a misspelling. There's something more important that needs to be
applied when we're talking about the context of this. This is the message. It is from Representative
Ralph Norman of South Carolina. Mark, in seeing what's happening so quickly, and then it goes
on. The key part that everybody's focused on is we are at a point of no return in saving our
Republic. Our last hope, all caps, is invoking Marshall, misspelled, law. Please urge to
president to do so. All caps. I mean, yeah, you've got a person in Congress asking
somebody to urge the president to declare martial law. I mean, yeah, that's going to
get headlines. And the misspelling, a very common misspelling among people who want
to invoke it for some reason, that has gotten a lot of attention. The thing
that I want to point out is that this wasn't sent on January 5th.
It wasn't sent on January 4th.
It wasn't sent on January 6th.
It was sent on January 17th.
The reporting says this was sent on January 17th after the events of the 6th, three days
before Biden's inauguration, and you have a person in Congress that certainly
appears to have been urging Mark Meadows to go to then president Trump and get
him to declare martial law to save the Republic, pro tip, creating a
The dictatorship declaring martial law does not save a republic, it subverts it.
You cannot have a representative democracy, a republic of our type, when you subvert elections.
Or you use the military to suppress the people when you don't like the outcome of it.
That's the exact opposite of preserving a republic.
I would think that the Department of Justice already has these.
Some of the members of Congress, they probably just saw it all as political, and they probably
will it'll just kind of go away for them. Some of them it may not. These messages
coming out now I'm willing to bet that there are things beyond this that we
don't know and we will find out about them later. But the important part I
I want everybody to remember is that even after the 6th, even after everything that
happened that day, there were still at least a few who were entertaining the idea of using
the United States military to suppress the people of this country because they didn't
like the outcome of the election.
All of their claims, all of the stuff that they said, it all went to court.
all been to court since then. Everything. And they lost. It was all made up. It was
all made up. You're left with two options. Either those who advocated for those
kinds of extreme measures, those who pushed that idea, those who thought it
was okay. Either they, like most of us, knew it was false, or they didn't. If they didn't,
if they didn't know those claims were false, I have to ask how anybody can believe that
that they're fit to be in the office they're in.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}