---
title: Let's talk about a possible change in the air over Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ngn7rMk0z5Y) |
| Published | 2022/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States Air Force and the Pentagon are in disagreement over providing older versions of Reapers to the Ukrainian military, which could have a significant impact on the conditions in Ukraine.
- Concerns from the Pentagon include the potential for these drones to be shot down, leading to Russia gaining access to the technology within them and potentially sharing it with other countries like Iran and North Korea.
- The Air Force is willing to provide these drones because they are not top-tier technology anymore but could still be a game-changer for Ukraine if deployed effectively.
- Ukraine sees these drones as a way to shift the balance of power in the conflict and has been pushing to acquire them.
- There are concerns about the technology falling into the wrong hands, but Ukraine has offered assurances that they won't target Russia with these drones and even provide targeting packages.
- The Pentagon may be hesitant about having final approval on targeting packages in Ukraine and getting more involved in the conflict.
- The use of drones in this manner, as close air support, is not how they were originally intended but could provide valuable insights for future drone development.
- The outcome of this debate between the Air Force and the Pentagon could have a significant impact on the conflict in Ukraine and potentially change the dynamics of the war.

### Quotes

- "If it transfers, if this kind of transfer goes through, expect the entire face of that war to change very, very quickly."
- "All of those static positions that the Russian military has been relying on, those are no longer obstacles for the Ukrainian military to overcome."
- "Using them as close air support, using them in the role that Ukraine is probably going to use them in, there are probably some in the Pentagon who are willing to send the drones over just to see how it plays out."

### Oneliner

The United States Air Force and the Pentagon are at odds over providing older drone technology to Ukraine, potentially altering the conflict dynamics significantly.

### Audience

Policy makers, military analysts

### On-the-ground actions from transcript

- Monitor developments in the debate between the United States Air Force and the Pentagon over providing drone technology to Ukraine (implied).
- Stay informed about potential shifts in the conflict dynamics in Ukraine due to the outcome of this technology transfer (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the debate between the US Air Force and the Pentagon regarding providing drone technology to Ukraine, exploring the potential impacts on the conflict dynamics and international relations.


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
the skies. And we're going to talk about a discussion occurring between the United States
Air Force and the Pentagon because they have differing opinions of something. And
depending on which school of thought wins out, it could end up drastically altering the conditions
in Ukraine. Since very early on in this conflict, the United States Air Force has wanted to provide
the Ukrainian military with Reapers. Older versions of Reapers, but Reapers nonetheless.
The Pentagon has been hesitant to do this. Initially, there were two reasons. The Pentagon
didn't want to provide them because these things definitely had the capability to hit inside of
Russia, which NATO is doctrinally against. That has become less of a concern as the Ukrainians
have developed their own methods of hitting deeper into Russia. And the Pentagon has been
reluctant to do this. The other is the concern that some of them would be shot down and therefore
Russia would gain access to the technology in them. Once Russia gains access to the technology
in them, that means that technology then goes to Iran, North Korea, so on and so forth.
So, yes, the Reapers, the versions that they are talking about sending are well beyond anything
that any opposition military has in their arsenal. They're not even close, to be honest.
That being said, the reason the Air Force is so willing to give them up is because by the United
States Air Force standards, they're not obsolete, but they certainly are not tier one technology
anymore. So, the Air Force is willing to risk it. They're like, yeah, give it to them and allow them
allow them this capability. The Pentagon is concerned about them being shot down and then
reverse engineered. And yeah, it's a risk. That being said, it doesn't bring it to, it would not
bring an opposition nation to the level that the United States is at because these are
their older technology. Now, how important is this to Ukraine? It's a game changer.
If the Ukrainian military got their hands on these things and deployed them even halfway decently,
it would be a game changer. All of those static positions that the Russian military has been
relying on, those are no longer obstacles for the Ukrainian military to overcome. Those are targets
of opportunity for the Reavers. Those are things that they just hit on their way to go do what they
were really trying to do. It would be a huge, huge shift in the balance of power if Ukraine got their
hands on these. Obviously, Ukraine has been pressing to get these since the very beginning
of the war. And the Air Force has been willing because they have a bunch they're trying to get
rid of anyway. I think they even offered them to like customs or something at one point.
So, it's the Pentagon's fear of losing that technology that's holding this up.
Is it a realistic fear? Yeah, odds are if they're sent, one will be shot down, one will be captured,
it will be reverse engineered, and that technology will get out. Is the technology contained in it
so much of an advantage that it should preclude it being deployed? I don't know about that.
What they're saying could happen, I'm not going to say it could happen, I'm saying it's almost
certainly going to happen. If the older versions of the Reapers find their way to Ukraine, odds are
it's an almost certainty that Russia will end up with that technology, but they're older versions.
I don't know that that's a good enough reason to say no, don't send them. Ukraine, for its part,
has said, hey, we won't hit inside Russia with these and we'll even give you targeting packages.
That may also be a concern for the Pentagon. The fact that
the idea of the United States having final approval over a targeting package in Ukraine
is probably a step they don't want to take. They don't want to be in the position of them saying,
yes, do it, when they're doing everything they can to not be active in this conflict.
So that may have
an additional complicating factor that could easily be resolved by saying,
yeah, we don't want the target packages. Ukraine would be using the drones the way
drones were actually supposed to be used, which is to replace normal aircraft, normal manned
aircraft. They weren't really supposed to be used in the way that they were supposed to be used.
They weren't really supposed to be used the way they have been. That wasn't
how they were originally envisioned. Using them as close air support, using them in the role that
Ukraine is probably going to use them in, there are probably some in the Pentagon who are willing to
send the drones over just to see how it plays out. And I know there's a whole bunch of people
that are going to be just shocked by that statement, but it's the reality of it.
They haven't been used in a conventional sense enough to really know how effective they are.
And this would be a situation in which Ukraine gets a huge boost in capability,
and the United States sits back and kind of watches. And from there, it would give it a
good baseline into how much to invest in drones that are specifically designed for conventional
combat. So what's going to happen? We don't know. But what we are aware of is that this conversation
is heating up between the Air Force and the Pentagon, and there is undoubtedly pressure
from Ukraine to try to get these. And there's probably a motivating factor of the U.S. drone
policy changing. So theoretically, these older drones that are not tier one technology,
they're not going to be used at all because there won't be cause to use them. So there's a lot going
on, but this is something to kind of put on your radar. You will see this material again. This is
going to come up. If it transfers, if this kind of transfer goes through, expect the entire
face of that war to change very, very quickly. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}