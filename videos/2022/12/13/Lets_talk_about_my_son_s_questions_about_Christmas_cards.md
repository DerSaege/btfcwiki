---
title: Let's talk about my son's questions about Christmas cards....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=007h2thmT5U) |
| Published | 2022/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- His son questioned the tradition of sending Christmas cards, opting for texts instead.
- The son challenged the purpose of Christmas cards beyond tradition.
- Beau realized his mistake in the convo but continued.
- His son called tradition "peer pressure from dead people."
- The environmental impact of Christmas cards was brought up by the son.
- About 3,000 Christmas cards come from one tree, leading to half a million trees used per year in the U.S.
- Beau predicts that the younger generation will move away from the tradition of sending Christmas cards.
- He anticipates backlash, particularly from the Republican Party, labeling it as a "war on Christmas."

### Quotes

- "Tradition is just peer pressure from dead people."
- "Just because we always did something one way isn't a reason to continue doing it that way if a better option presents itself."

### Oneliner

Beau's son challenges the tradition of Christmas cards, pointing out their environmental impact, leading to a prediction of its eventual fade and potential backlash.

### Audience

Parents, environmentalists

### On-the-ground actions from transcript

- Opt for environmentally friendly alternatives to traditional practices (implied)

### Whats missing in summary

The full transcript provides a humorous and thought-provoking take on generational perspectives and traditions, prompting reflection on societal norms and environmental impact.

### Tags

#ChristmasCards #Traditions #GenerationalPerspectives #EnvironmentalImpact #Parenting


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a conversation I had
with my son and him throwing my own words back at me
and where it leads, because it actually touches
on a couple of interesting points.
My son is eight.
He has seen a Christmas card before.
I'm sure of it.
I know he has.
But for whatever reason, a Christmas card that just
showed up at my house, he asked what it was.
And I told him it was a Christmas card.
And he's like, yeah, but what is it?
And he already has this tone.
OK, it's a Christmas card.
And I hand it to him.
And he opens it up.
He looks at it, looks at both sides.
He's like, why do people send these?
I'm like, well, it's a way of saying hello
around the holidays, wishing people a Merry Christmas.
He's like, yeah, why don't they send a text?
I'm like, I don't know.
It's tradition.
The fill of the paper, the imagery on the cardstock,
I don't know.
And when I say tradition, he gets this look on his face.
And I know that I have messed up at this point
in the conversation.
But he just skips right over that for the moment.
And he says, he's like, you know there
are pictures of Christmas on the internet, right?
Yeah, yeah, I'm aware of that, man.
He's like, do we send Christmas cards to people?
In an incredibly accusatory tone.
And I kind of like, you know, just
stand there with this look on my face
because I honestly don't know the answer.
And he's like, I'll ask mom later.
I'm like, yeah, that's probably a good move, kid.
And then he comes back to, is there any reason
that we send these other than tradition?
And the tone in his voice was something to be remembered.
And I'm like, not that I know of.
And he's like, yeah, you know that tradition is just
peer pressure from dead people, right?
Like, yeah, I'm aware of that.
And he's like, and you know that just because we always
did something one way isn't a reason
to continue doing it that way if a better option presents
itself.
I'm like, yeah, I remember telling you all of this.
But at this point, I still don't actually know why he's mad.
I don't know what his conflict with Christmas cards
is until he says, how many Christmas cards
does one tree make?
Got it.
Very environmentally conscious kid, by the way.
I didn't know the answer offhand.
So this is napkin math based on estimates I found online.
One tree produces about 3,000 Christmas cards.
In the United States, we send about 1.3 billion per year.
When you include envelopes and stuff like that,
you're looking at about half a million trees per year.
So the next part to this is that I'm certain that my older son
and basically everybody of that generation,
they're going to do away with Christmas cards.
That tradition will fade.
They will not succumb to that peer pressure.
And it's probably a good thing.
So one thing that everybody needs to prepare themselves
for is the Republican Party calling this part
of the war on Christmas, because you know it's coming.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}