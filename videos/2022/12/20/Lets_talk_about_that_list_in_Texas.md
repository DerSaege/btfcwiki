---
title: Let's talk about that list in Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wZXgZB9-yEs) |
| Published | 2022/12/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reports suggested the Texas attorney general's office wanted a list of people who changed their gender on their driver's license.
- More than 16,000 entries were involved, but the information didn't change hands due to the exhausting manual process.
- Beau usually investigates stories but couldn't find any context or information on this matter.
- Major news outlets like Washington Post, NBC, and New York Times haven't been able to get answers either.
- The Attorney General's office has remained silent on the matter, even after stories were published.
- Beau expresses concern about authorities making lists of marginalized groups.
- Due to the holidays, the story is unlikely to progress until after New Year's unless someone has inside information.
- Beau urges for transparency from the Attorney General's office regarding the motive behind seeking this list.
- He stresses that this story shouldn't be forgotten and requires further investigation.
- Beau hopes that reputable news outlets will continue to pursue this story despite the holiday season.

### Quotes

- "Reports suggested the Texas attorney general's office wanted a list of people who changed their gender on their driver's license."
- "The Attorney General's office certainly appears to have requested a list of trans people in the state with no explanation as to why."
- "When a group of authoritarians starts attempting to make lists of people that they have actively othered, it's not a good thing."
- "This isn't a story that should be forgot, though."
- "No, this doesn't need to be forgotten."

### Oneliner

Beau raises concerns about the Texas attorney general's office seeking a list of people who changed their gender on driver's licenses, stressing the need for transparency and continued investigation despite the holiday season.

### Audience

News Outlets, Activists

### On-the-ground actions from transcript

- Investigate the situation further to uncover the motives behind the request for the list (implied).
- Stay informed and keep the story alive by sharing information on social media and with relevant organizations (implied).

### Whats missing in summary

The full transcript provides more context on the concerning request for a list of trans people in Texas and the lack of transparency from the Attorney General's office.

### Tags

#Texas #AttorneyGeneral #TransgenderRights #Investigation #Transparency #NewsCoverage


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Texas and the list.
And it's going to be an unusual video that is
going to end in a unique way.
So about five or six days ago at time of filming,
which is Monday night, I had a bunch of people
send me information and links to other reporting
about a report that suggested that the attorney general's
office in Texas had reached out to basically the driver's
license bureau there and tried to get
the numbers of people who had changed their gender
on their driver's license and said, well,
eventually we might need a list of who these people are.
Now, there were more than 16,000 entries.
So the information, as far as anybody can tell,
never actually changed hands because doing
a manual check of why that information was changed
would just be too exhausting.
So it didn't go anywhere.
But the request was apparently out there.
Now, normally when I get something that is this wild,
one of two things happens.
I start to look into it, and I get the context,
and I make a video providing that context.
The other option is I start to look into it,
realize that there's absolutely nothing to it, no big deal,
don't worry about it, so I don't make a video.
I'm making this one because I don't want anybody to think
that's what's happening.
The reason I haven't made a video about it
is because I don't know anything.
I can't find anything out.
I started looking into it, but I can't get anywhere.
I can't even get an off-the-record answer.
But at the same time, I don't really
have great sourcing in the Attorney General's office
in Texas or at the driver's license place.
So it wasn't totally surprising that I couldn't get anywhere.
The reason I'm making this video is because nobody else
can get anywhere either.
Washington Post, NBC, the New York Times,
NBC, Texas Tribune, nobody has an answer.
So right now, what we know is that the Attorney General's
office certainly appears to have requested
a list of trans people in the state
with no explanation as to why.
Them requesting it is concerning.
Having absolutely no idea why they wanted it,
that's alarming.
After this much time with stories
being published in the Washington Post,
you would think the Attorney General's office
would say something.
But so far, we have nothing.
Generally speaking, the Attorney General's office
is not speaking.
When a group of authoritarians starts
attempting to make lists of people
that they have actively othered, it's not a good thing.
This is a story that shouldn't go away.
The thing is, anybody who's trying to look into this,
nobody is going to get anywhere from now until New Year's
with it.
The story is effectively not going to go anywhere
as basically with the holidays and places being shut down,
and then the few days they're going to be open,
they're going to be trying to catch up
with normal routine stuff.
There's no way that this is going to really get anywhere
until after New Year's unless somebody has a really
good source of their own.
So if you happen to have that, now
might be the time to get to work.
This isn't a story that should be forgot, though.
The Attorney General's office needs
to explain why it was trying to gather this list, what
this list is for, why they were trying to get this information
to begin with, what they planned to do with it once they had it,
why they thought they might go from just needing the numbers
to needing an actual list, why they
would want to verify documents, which
was another part of one of the email exchanges.
There's grounds to be concerned here,
and I just don't want the people who sent me this
to think it's one of those where I looked into it
and there's nothing to it.
So just don't worry about it.
No, this doesn't need to be forgotten.
I don't have the answer, but I do
have the answer, but the answer needs to be found.
I'm hoping, and I'm fairly certain,
that the Washington Post isn't going to let this go.
I'm sure they understand that this is a unique development
that needs attention, even though we are
going into the holiday season.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}