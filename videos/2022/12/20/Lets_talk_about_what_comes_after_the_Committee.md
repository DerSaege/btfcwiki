---
title: Let's talk about what comes after the Committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AKp5LfjKSNM) |
| Published | 2022/12/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about what comes after the committee hearings, even though they are over and the show has theoretically ended.
- Mentions that the released summary doesn't offer much new information for those who watched the hearings, spanning about 150 pages.
- Points out that the next phase will involve the release of transcripts, where different political factions will try to manipulate discrepancies to create scandal and divert attention.
- Anticipates a phase where inconsistencies in statements will be magnified to cast doubt on testimonies.
- Emphasizes the importance of recognizing that if someone was lying under oath, it implicates others too.
- Notes the upcoming DOJ decision and the historic significance of the committee's actions regarding January 6th.
- Concludes by mentioning the illusion some hold about social media's impact on electoral strategies.

### Quotes

- "They are still under the illusion that social media shares and likes somehow are a valid electoral strategy."
- "The key thing you have to remember is if they were lying under oath, that just means that a whole bunch of people that were handpicked by Trump had no problem lying under oath."
- "Make no mistake, the January 6th event and the hearings afterward, they will be in history books."

### Oneliner

Beau talks about the aftermath of committee hearings, warns of upcoming political manipulations, and underlines the historic significance of recent events, including January 6th.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact local representatives to express support for transparency and accountability in political processes (suggested)
- Engage in informed political discourse with friends and family to counter misinformation and manipulation (suggested)

### Whats missing in summary

The emotional weight and nuance in Beau's delivery and analysis can be best experienced in watching the full segment.

### Tags

#CommitteeHearings #PoliticalManipulation #DOJ #HistoricEvents #SocialMediaImpacts


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about what comes after the committee,
because while the hearings are over, the show is theoretically wound up,
and it has made its referrals, it's really not over.
There's more to come.
The summary's been released. I've read, skimmed,
and there's not a lot in it that's new for those people who actually watched the hearings.
It's about 150 pages.
There's a lot of language in it that is very direct, very straightforward.
You know, Trump did this, and there's already been questions about that.
Don't infer too much.
They're not journalists. They are in Congress.
They are protected by the whole speech and debate thing.
He allegedly did those things, but their evidence is pretty strong.
So that's where the real fun begins.
The next phase of this will be all of the transcripts being released,
at which point those in the Republican Party trying to downplay it
and those loyal to Trump trying to justify it will go through
and try to find any discrepancy they can and zero in on any inconsistency,
any discrepancy, any misstatement that they can find,
and they will try to create a giant story and scandal out of that
to divert attention from the content of the testimony.
A good example of this from the hearing would be the whole debate
over what really happened in the limo.
Keep in mind the key pieces of information that he wanted to go,
none of that was disputed.
What actually mattered wasn't really there for debate.
It was this one thing that they could kind of zero in on
and try to use that to cast doubt on everything.
Expect that. It's coming.
And that portion of the show will probably last weeks once it starts
because they'll find some inconsistency between two people's statements
and try to frame it as if they're lying
rather than just seeing it from two different perspectives.
And then once it starts, they'll just keep pulling out little baby things
to try to convince the American people
that there's something wrong with the testimony.
The key thing you have to remember is if they were lying under oath,
that just means that a whole bunch of people that were handpicked by Trump
had no problem lying under oath.
There's really not a winning strategy here,
but make no mistake about it,
they are still under the illusion that social media shares and likes
somehow are a valid electoral strategy,
despite three consecutive pretty devastating losses at the polls.
So that's what's coming up next.
From there, we'll have to see what DOJ does.
Everything really is in their court now.
They really get to decide how this proceeds from here.
Regardless of what DOJ does, this committee was historic,
not just for referring the charges.
That's what people are pointing to as it being historic.
Make no mistake, the January 6th event and the hearings afterward,
they will be in history books.
Your kids will learn about it in school.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}