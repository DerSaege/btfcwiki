---
title: Let's talk about Trump's move with McCarthy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=roSe51-Ds3s) |
| Published | 2022/12/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's public address was a strategic move to gain influence through McCarthy, the presumptive Speaker of the House.
- McCarthy currently lacks the votes needed to become Speaker, and Trump's public support puts McCarthy in a position where he must follow Trump's lead.
- Trump's public endorsement of McCarthy essentially gives him de facto control of the House if McCarthy becomes Speaker.
- Despite potential backlash, McCarthy has tied his political fortunes to Trump by accepting his support.
- Trump's control over the House through McCarthy could lead to a chaotic political environment and keep his grievances at the forefront.
- The Republican Party's continued loyalty to Trump allows him to maintain significant power and influence.

### Quotes

- "Trump owns McCarthy."
- "Trump still controls the Republican Party."
- "Trump will use that influence, that leverage, to turn the House into a circus."

### Oneliner

Former President Trump strategically gains influence by publicly endorsing McCarthy for Speaker, potentially leading to de facto control over the House and a chaotic political environment.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor the political developments closely and stay informed about the power dynamics within the Republican Party (implied).

### Whats missing in summary

Analysis of potential long-term implications and consequences of Trump's influence on the Republican Party and the political landscape.

### Tags

#Trump #RepublicanParty #HouseSpeaker #PoliticalInfluence #PowerDynamics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk about the little show
that former President Trump put on
that gained him a lot of influence.
It's not apparent right now,
but it was a smart political move.
I doubt he came up with it on his own.
And it has to do with his new relationship with McCarthy,
McCarthy, the presumptive Speaker of the House.
People keep saying that, but the reality is right now, McCarthy doesn't have the votes.
He does not have the votes.
He needs more Republicans to sign on to him becoming Speaker.
That's where Trump comes in.
He gave a little address, addressing those people in Congress, those holdouts, and said
that they needed to vote for McCarthy.
Now here's the thing, is that statement going to carry a lot of weight?
We don't know, but it doesn't matter because now Trump owns McCarthy.
Even if McCarthy was capable of wheeling and dealing and getting the votes himself at this
point, it doesn't matter.
Because of the way Trump did it so publicly, it puts McCarthy in a position that basically
means he has to do whatever Trump says.
Because if he is disloyal to Trump, if he doesn't obey Trump, if he doesn't do everything
exactly the way Trump wants him to do, Trump will walk out and say, you know, I thought
McCarthy was going to be a good speaker.
I told everybody to give him the shot.
He didn't have the votes.
He came to me on his knees down at my resort.
He was begging for my help.
I gave him my help and then he didn't do what he was supposed to.
He turned on us.
And the Republican Party will follow Trump's lead and they will disavow McCarthy.
McCarthy has tied himself, his political fortunes, to Trump right now, which doesn't seem to
be like a good idea at all.
But from Trump's perspective, this was a smart move.
I mean, it was smarter than anything politically that, I mean, he's kind of ever done.
That's why I don't think it was his idea, or it was just an accident.
But it basically gives him de facto control of the House.
If McCarthy becomes Speaker now, he has to take his marching orders from Trump, which
means the House is his.
The House is his.
And by extension, because it's the only branch of the federal government that the Republican
Party controls, Trump still controls the Republican Party.
He still has that power, that influence, because the Republican Party, after everything he's
done, after everything he did in office, after everything he did after he left office, after
everything he has said regarding the Constitution, regarding how everything is shaping up because
of his activities, the Republican Party still won't break from him.
And now, through just a statement, he's going to end up having de facto control of the House
if Kevin McCarthy becomes the Speaker.
And make no mistake about it, Trump will use that influence, that leverage, to turn the
House into a circus.
It will be a show the entire time in an attempt to keep his grievances front and center, which
means come 2024, the Republican Party will be seen as ineffective as it has become.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}