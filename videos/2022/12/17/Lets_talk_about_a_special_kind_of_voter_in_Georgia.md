---
title: Let's talk about a special kind of voter in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2PopspP_DbI) |
| Published | 2022/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the phenomenon of "blank ballot voters" who didn't vote for either Senate candidate in Georgia's runoffs.
- Points out that these voters are predominantly Republicans or Libertarians who couldn't support either candidate due to ideological differences.
- Emphasizes that it's not a sign of something being wrong but rather a reflection of political beliefs.
- Notes that the issue lies in the Republican Party running a candidate that didn't appeal to a significant portion of its base.
- Suggests that the growing number of conservatives and Libertarians rejecting the MAGA movement indicates a shift within the Republican Party.

### Quotes

- "They're not supposed to be, the candidates aren't supposed to be so close together that if you don't like one, you just vote for the other and you're going to get the same result."
- "It shows that there are a growing number of people within the Republican side of things, or let's just say conservative."
- "They have seen the other side and they realize that the MAGA movement, the Sedition Caucus, is bad for the Republican Party."
- "It isn't that both parties ran candidates that were just unappealing to a whole bunch of Georgia voters."
- "It's Libertarians who are unwilling to compromise with an authoritarian, or it's conservatives who just cannot continue to bend the knee to the MAGA movement."

### Oneliner

Blank ballot voters in Georgia's runoffs reflected ideological differences, not flaws, within the Republican Party.

### Audience

Conservatives, Libertarians, Republicans

### On-the-ground actions from transcript

- Reach out to disillusioned voters in your community and have open dialogues about their concerns (suggested).
- Advocate for better candidate selection within political parties to avoid alienating voters (implied).
- Support and amplify voices calling for change within political parties (implied).

### Whats missing in summary

Exploration of the impact of blank ballot voters on electoral dynamics and party politics.

### Tags

#Georgia #RepublicanParty #Conservatives #Libertarians #MAGAMovement


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to, we're going to
talk about a special kind of voter during the runoffs. A voter that appeared during the runoffs
in Georgia that is getting some attention from a couple of different places, and we're just going
to kind of go over who this voter is and why they did what they did, because one of the places it's
getting attention is people who see this as a sign that there was something wrong. That's not what it
is. And another issue with it is that for the most part, the news is reporting this in a way that is
factually accurate, but it's lacking a lot of truth because it's missing a bunch of context.
So we're gonna go through it. We're going to talk about blank ballot voters. There are people
who showed up to vote during the runoffs who didn't vote for either of the candidates for senator.
They were blank. Okay, so what does this mean? Generally, it means none of the above was a
protest vote. It means none of the above. It's not a sign that something's wrong. People who did this
have been interviewed and talked to, and they flat out say, I didn't want to vote for either candidate.
And that's the way the news is framing it. The candidates were so bad that
a whole bunch of people in Georgia couldn't bring themselves to vote for either one of them.
I mean, that sounds good, and that sounds like a logical conclusion from what is said,
but here's the thing. All of the people who have been interviewed, who have been talked to
about this, they're all either Republicans or Libertarians. That's who they are.
The fact that they couldn't bring themselves to vote for a liberal isn't surprising.
When you're talking about making the decision on an ideological level, it's supposed to work that
way. They're not supposed to be, the candidates aren't supposed to be so close together that if
you don't like one, you just vote for the other and you're going to get the same result. These
are people who are conservative or they're Libertarians. And they're viewing it in a way
where when they look at Warnock, they're like, I can't vote for this person. They're completely
against what I believe. So it's not that both parties ran bad candidates. One party ran a good
candidate that appealed to their voters. Surprise, that one won. It shows that there are a growing
number of people within the Republican side of things, or let's just say conservative,
and we're lumping Libertarians in with them. Yeah, just go ahead and send the messages.
For the purposes of this conversation, you're all the same. I know you're not,
but for the purposes of this conversation, they didn't have a candidate they could vote for.
That's what happened. If you look at those interviews, particularly those who were high
profile who did it, there were a number of very well-known people in Georgia who did this and
made it public. They're Republicans or they're Libertarians. They didn't have a candidate they
could get behind. That means there is a growing number of people in Georgia who have gone through
the looking glass. They have seen the other side and they realize that the MAGA movement,
the Sedition Caucus, is bad for the Republican Party. It's not something that aligns with their
beliefs because remember, Libertarians and real conservatives are somewhat anti-authoritarian.
They're a different kind of... Anyway, the authoritarian bent of the MAGA movement doesn't
appeal to them. And you have a growing number of people on that side of things who are starting to
see it. And if the Republican Party doesn't want this to become a trend, because it will,
somebody is going to have to scream change places and y'all are going to have to get better
candidates. It isn't that both parties ran candidates that were just unappealing to a
whole bunch of Georgia voters. No, the Republican Party ran a bad candidate and people wouldn't vote
for him. It's a whole bunch of people who looked at werewolf Walker and were like, no, not doing it.
Now, given the fact that it is Georgia, I would assume that a few of those, probably not many,
but a few of those were just people who wouldn't vote for either candidate due to immutable
characteristics. But I don't think that's the bulk of them by any means. It's Libertarians
who are unwilling to compromise with an authoritarian, or it's conservatives who
just cannot continue to bend the knee to the MAGA movement. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}