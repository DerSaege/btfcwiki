---
title: Let's talk about House Republicans wanting referrals
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MeH-4wEuvZs) |
| Published | 2022/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- House Republicans plan to launch an investigation into the president's son, indicating an authoritarian nature embraced by the Republican Party.
- Republicans are compared to the Democratic Party's committee hearings into the January 6th events, a crime investigation, not an individual.
- The proposed Republican hearing focuses on Hunter Biden's laptop without an identified crime, seeking the person first and then the crime.
- Allegations against Hunter Biden in Ukraine fell apart when the timeline and narrative were examined.
- Despite ethical concerns, Hunter Biden's actions do not appear to be illegal.
- The Republican Party's approach seems to mirror Lavrentiy Beria's "show me the person, I'll show you the crime" tactic from Soviet state security, lacking evidence for alleged crimes.

### Quotes

- "They have the person and they're looking for a crime."
- "They don't have an identified crime that they're looking into."
- "Show me the crime and then you go find the person."
- "I think that's a little unethical, but it's not illegal."
- "It will just continue to tie the entire Republican Party to his failure of an administration."

### Oneliner

House Republicans plan to launch an investigation into Hunter Biden without an identified crime, mirroring an authoritarian approach and risking further Republican Party damage.

### Audience

Political observers

### On-the-ground actions from transcript

- Examine political investigations critically (suggested)
- Stay informed about political developments (suggested)

### Whats missing in summary

Exploration of the potential impacts on the Republican Party's reputation and future political standing. 

### Tags

#Politics #Investigation #RepublicanParty #HunterBiden #Authoritarianism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the future
of House Republicans and any committees they may develop
because they have indicated that they plan
on launching an investigation into the president's son.
And basically they're sitting there
and they are looking at the Biden administration.
They're saying, we're gonna bury you,
bury you, bury you, bury you.
And it's an interesting development
because it's showing the authoritarian nature
that the Republican Party has embraced.
And I don't know that they actually are aware
of where what they're doing came from,
or at least the modern incarnation of it.
They're trying to compare any investigation they launch
to the investigations that were launched
by the Democratic Party, like the committee hearings.
But here's the thing.
What was the committee hearing about?
What was it called?
January 6th hearings, right?
They were looking into the events of January 6th.
There were crimes that took place on video.
They had a crime and they were looking
for the people responsible for it.
That's how it's supposed to work.
You have a crime, you identify the crime.
In this case, there were all of the acts
that were live streamed.
And then the very real possibility
of what appears to be a failed self-coup attempt.
That's what that committee is about.
The crime, not investigating an individual person, the crime.
What about what's going on down there in Trump's room closet
down south?
What's that called when people talk about it?
The documents case, right?
That's what they're talking about.
That's what we call it on this channel.
Why?
Because I think one of the likely charges
that the Department of Justice is looking into
is the willful retention of national defense information.
Documents.
The crime first, then the person.
Show me the crime and then you go find the person.
What's the proposed hearing about from the Republican Party?
Hunter Biden's laptop.
Is it illegal for him to have a laptop?
What is the specific crime that they're looking for?
And when you ask that question,
you realize they have the person
and they're looking for a crime.
They don't have an identified crime
that they're looking into.
If you press them on it,
they will normally launch into something
that becomes very conspiratorial
about business dealings in Ukraine or China.
Now, I haven't looked into the ones in China,
but I did look into the ones in Ukraine,
the allegations there.
Have a whole video on it.
And all you have to do is lay out the timeline
of what occurred and the order it occurred in.
And their narrative, it falls apart.
It falls apart.
Now, Hunter Biden certainly appears to have been engaged
in like a do-nothing job,
trading on his name, stuff like that.
Me, personally, I think that's a little unethical,
but it's not illegal.
It's not a crime.
You're supposed to have the crime first,
and they don't have it.
Interestingly enough, that saying is,
show me the person, I'll show you the crime.
It came from a guy, Lavrentiy Beria.
He was head of Soviet state security, worked for Stalin.
And they're taking a page right out of his book.
That's what they're trying to do.
I don't know if they're aware of that,
but that is how they are operating.
They're not operating under a rule-of-law principle
because they have yet to allege a specific law
that was broken with any kind of evidence,
certainly not anything that brings it
to the level of congressional hearings.
But I guess that doesn't matter much anymore.
The Republican Party is not the Republican Party
of 20 years ago.
It has changed dramatically,
and more and more people will wake up to it.
I am of the firm opinion that any hearings of this sort
will really just serve to further undermine
the Republican Party.
They will be such a circus and so unprofessional
and so reminiscent of a certain former president
that it will just continue to tie the entire Republican Party
to his failure of an administration.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}