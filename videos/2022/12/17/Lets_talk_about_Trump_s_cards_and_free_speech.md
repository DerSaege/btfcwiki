---
title: Let's talk about Trump's cards and free speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Intc_pcyD3I) |
| Published | 2022/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's major announcement about hawking NFTs turned out ridiculous and did not change the story.
- Many believe Trump's reign in the GOP is coming to an end, despite claims that he was only pretending.
- Trump's speech about a free speech platform is viewed as a con and a lie, as he has a history of threatening journalists and dissent.
- Trump plans to sign an executive order barring federal employees from labeling anything as misinformation or disinformation, raising questions about his own use of such terms.
- People who mimic Trump's style are seen as manipulative, aiming to maintain relevance and deceive the public.
- Those in the Republican Party who follow Trump or mimic his style might eventually realize they have been deceived.
- Trump's attempts to monetize everything, from businesses to government positions, showcase his true character.
- Researching Trump's history reveals numerous instances where he sought to silence dissent, contradicting his image as a free speech advocate.

### Quotes

- "It's a con. It's a lie."
- "He is not a free speech advocate. He never has been."
- "He is the antithesis of free speech."

### Oneliner

Beau exposes Trump's facade of a free speech advocate, revealing his history of silencing dissent and manipulation in politics.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Research Trump's history of silencing dissent (suggested)
- Stay informed on political manipulations and tactics (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's actions and rhetoric, shedding light on his deceptive tactics and lack of commitment to free speech.

### Tags

#Trump #NFTs #FreeSpeech #Manipulation #RepublicanParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and NFTs and coping with the reality.
Um, you know, I said in that video that Trump's major announcement wasn't going
to matter, I did not expect it to be quite as ridiculous as it was if you missed
his major announcement was that he has started hawking NFTs. Some of the illustrations of him
are definitely worth seeing. I don't know that they're worth buying. His major announcement was,
of course, a flop. And it did not change the story. In fact, it made it worse. Now
there are more and more people basically saying that Trump's over. That his
reign in the GOP is coming to a close. In an amazing amount of cope, there are a
lot of people suggesting that, oh, he was just pretending he was playing.
That really wasn't his major announcement.
His major announcement was his speech about censorship and how, if he was to become the
president again, that he would enact this free speech platform.
Yeah, okay.
Because when I think free speech, I think Trump.
It's a con.
It's a lie.
He doesn't care about free speech.
This is a man who recently threatened to or expressed his desire to lock up journalists
who wouldn't name their sources and keep them there until they were assaulted.
He's not a free speech advocate.
And in the most hilarious part of his free speech platform, he plans to sign an executive
order, barring federal employees from labeling anything as misinformation or disinformation?
I mean think about it for a second.
Does he realize that he would be a federal employee?
And if so, is he going to write it so it would have to specifically use the words misinformation
or disinformation, or is he going to stop using the phrase fake news?
It's a con.
It's a con.
The former president is not an advocate for free speech.
He never has been.
It's just a hot topic now, and he is struggling desperately to remain relevant as his poll
numbers continue to plummet because people are finally seeing through him.
Understand that all of the people who mimicked his style, they're the same way.
I have a feeling that there are going to be a lot of people in the Republican Party who
jump from Trump to somebody who just copied his style, and then four years from now or
six years from now, they're going to find out that once again they were had.
Those Republicans who are taking these hard-line stances that their very public records stand
against, it's just a trick.
They're just going after the most easily manipulated people they can find.
And if you're falling for it, I mean, they're succeeding.
I don't imagine that Trump's baseball card collection is going to do very well.
It did kind of showcase him as who he is.
who tries to monetize everything from his businesses to his position in government.
He is not a free speech advocate.
He never has been.
If you were to actually research and look into this, you will find dozens, dozens of
instances in which he attempted to use or talked about using the force of
government to silence and squelch dissent. He is the antithesis of free
speech.
And that is not fake news.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}