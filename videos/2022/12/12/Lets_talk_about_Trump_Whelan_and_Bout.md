---
title: Let's talk about Trump, Whelan, and Bout....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MvAQQ9kYnsY) |
| Published | 2022/12/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing former President Trump's statements on various issues.
- Questioning Trump's attitude towards justice and leadership.
- Examining Trump's claim of turning down a deal with Russia.
- Criticizing Trump's prioritization of punishment over justice.
- Pointing out the lack of moral framework in Trump's statements.
- Refuting Trump's justification for not making a deal involving Paul Whelan.
- Providing insights from Fiona Hill and Whelan's family on Trump's lack of interest in the case.
- Mentioning Trump's past dealings with Saudi Arabia in comparison to his stance on Paul Whelan.
- Dismissing Trump's statement as mere ranting without a moral philosophy.
- Exploring the implications of Trump's claims and how the situation has evolved since 2018.
- Accusing Trump of using Whelan for his own political agenda.
- Condemning Trump as a foreign policy disaster even after leaving office.

### Quotes

- "Being more concerned about punishment rather than justice, that's not really a sign of good leadership right there."
- "Trump was a foreign policy disaster. There is no other way to say it."
- "His statements, they're not worth much."
- "It's just a thought."
- "Y'all have a good day."

### Oneliner

Analyzing former President Trump's questionable statements, lack of moral framework, and attempts to use political agendas to paint himself as better at foreign policy, Beau criticizes Trump's foreign policy disaster.

### Audience

Critically-minded individuals

### On-the-ground actions from transcript

- Contact organizations working on foreign policy issues (implied)
- Stay informed about political agendas and statements (implied)

### Whats missing in summary

Deeper insights into the implications of foreign policy decisions and the importance of holding leaders accountable.

### Tags

#ForeignPolicy #Trump #PoliticalAnalysis #Leadership #Justice


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about former President Trump
and his statements on certain goings on.
We'll talk about Whelan.
We'll talk about Bout.
And we're going to talk about a Trump quote.
And we're going to go through it.
In the beginning, we're just going to act like it's true.
We're going to discuss the philosophical aspects of what
he said.
Then we'll go through and see if it actually is true.
And we'll talk about that.
And then we'll go over the implication of what he said.
OK, so what did he say?
I turned down a deal with Russia for a one-on-one swap
of the so-called merchant of death for Paul Whelan.
I wouldn't have made the deal for 100 people
in exchange for someone that has killed untold numbers of people
with his arms deals.
And this attitude is why former President Trump
is former President Trump.
Being more concerned about punishment rather than justice,
that's not really a sign of good leadership right there.
I have a lot of questions about the idea of just letting
100 people go for one person who at the time
had been out of circulation for like 10 years.
Now there's also this bit that just I really couldn't get over
for a second.
Someone that has killed untold numbers of people
with his arms deals.
That makes him a bad person, right?
What about Trump's, if I'm not mistaken,
in 2017 just with Saudi Arabia, he signed letters of intent
for them for deals amounting to $350 billion.
I'm pretty sure Bout hadn't done anything of that scale.
The philosophy here, it doesn't exist.
There's no moral framework for his statements.
It's just him ranting.
Now let's talk about whether or not it's true.
Because according to Fiona Hill, who
was on his national security team,
he was not particularly interested in the case.
Didn't really care.
That sentiment is echoed by Whalen's family.
If I'm not mistaken, he was the one
who was the most concerned about the case.
If I'm not mistaken, Trump has talked about it
more in the aftermath of the recent deal
than he did during his entire presidency.
And he didn't really seem to care,
is the general consensus of people
who are familiar with the matter.
So I don't actually think that he put any thought into it.
Now let's go over the implication.
What he's trying to do is kind of insinuate
that because he would have been able to get a bout for Whalen
Dill back in 2018, that that would still
be on the table today.
It's not how it works.
That is not how it works.
International poker game where everybody is cheating.
Every card that is dealt changes everybody's hand.
So what happened between 2018 and today,
let's try August of 2019, that might
have changed Russia's intent?
One of their hitters, an FSB hitter by the name of Vadim
Kasikov, got picked up in Berlin.
Somebody that's actually important to Russian
operations.
That's who they wanted, incidentally.
Reporting suggests that's who they would trade him for today.
The math has changed.
The card game has changed.
It has moved on.
The implication that Trump is trying to make,
according to people familiar with the matter,
he didn't feel that way.
And the downstream effect of being able to do it today,
it's not true because the situation
has changed in four years.
So that's just a general overview
of Trump's statements on this.
He is trying to use Whelan for his own political ends.
He didn't care enough to try to work to get him out
during his presidency.
But now he's willing to make promises and insinuate
that Biden's doing something wrong
and try to tear down anything that's good
because he didn't do it.
And he knows he didn't do it.
And his end goal here is to try to paint himself
as somebody who is better at foreign policy than Biden.
Trump was a foreign policy disaster.
Trump was a foreign policy disaster.
There is no other way to say it.
And he'll continue to be.
He's not even in office and he's still a foreign policy disaster.
His statements, they're not worth much.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}