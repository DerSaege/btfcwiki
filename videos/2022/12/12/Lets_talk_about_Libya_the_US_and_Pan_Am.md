---
title: Let's talk about Libya, the US, and Pan Am....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5NRMQWX4oh4) |
| Published | 2022/12/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pan Am flight 103 incident, an airliner exploded over Lockerbie, Scotland in 1988, killing 259 in the air and 11 on the ground.
- One of the key figures sought after in the Pan Am flight 103 case, Massoud, is now in US custody.
- Speculation surrounds how Massoud ended up in US custody, with rumors suggesting Libyan involvement.
- Libya, with competing administrations in Tobruk and Tripoli, may have assisted in extracting Massoud due to international pressure and financial struggles.
- The National Oil Corporation in Libya recently sought foreign investors, hinting at economic desperation.
- The motive behind Libya's possible involvement in handing over Massoud to the US could be to attract European investment.
- Despite rumors implicating the Tripoli administration, the exact details of how Massoud was transferred to the US remain undisclosed.
- The US government remains vague about the circumstances of Massoud's custody, fueling speculation.
- Massoud's trial in the United States is anticipated to reignite interest in the Pan Am flight 103 incident.
- The case is expected to resurface in headlines, bringing awareness to those unfamiliar with the event.

### Quotes

- "For a lot of younger people, the only comparison is what happened in September."
- "Is there a reason for the government there to suddenly want to help the US. Yeah, there actually is."
- "Everything's going to be brought back up again."

### Oneliner

Massoud from Pan Am flight 103 case now in US custody, sparking speculation on Libyan involvement and potential motives amid economic struggles.

### Audience

International observers

### On-the-ground actions from transcript

- Monitor updates on the Pan Am flight 103 case to stay informed and aware of developments (implied).

### Whats missing in summary

Detailed information on the Pan Am flight 103 incident and the potential implications of Massoud's trial in the United States.

### Tags

#PanAmFlight103 #Lockerbie #Libya #USCustody #Speculation


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about a story more than 30
years in the making.
And we're going to talk about what we know.
We're going to talk about a lot of speculation
that is occurring and whether or not that speculation makes
any sense because there are a lot of rumors flying around
about this particular event.
So what we know is that one of the most sought after people
when it comes to Pan Am flight 103 is now in US custody.
Now if you don't know what this is, in 1988 an airliner exploded over Lockerbie, Scotland.
Two hundred and fifty-nine were taken out in the air, another eleven on the ground when
it crashed.
This was huge.
For a lot of younger people, the only comparison is what happened in September.
This was a big deal.
To this day, I'm fairly certain it's the largest attack on British soil, the largest
terror attack.
The person who is now in U.S. custody is named Massoud.
Masoud is allegedly the device maker. That information came out a few years ago
and been pretty sought after ever since. That's what we know and really that's
all we know. How Masoud wound up in US custody? Well, they're not really saying.
Which adds a lot of credibility to some of the speculation. There is a belief
that one of the administrations in Libya assisted. It's worth noting that Libya
has what's called them competing administrations. One based out of
Tobruk, one based out of Tripoli. And then there are a lot of local power
structures as well. The rumor is that the administration in Tripoli arranged to
forcibly extract Massoud. That's a really polite way to say snatch in the middle
of the night. And then he wound up in US custody. That's quite a turn of
events. Is there a reason for the government there to suddenly want to
help the US. Yeah, there actually is. As far as motive goes on that international
poker game where everybody's cheating, Libya is losing out right now. They
should be making a dump truck full of cash, an oil tanker full of cash to be
more precise, Libya has everything that isn't being supplied by Russia right now.
They should be making a ton of money.
And just last week, the National Oil Corporation in Libya asked foreign investors to come back.
This might be a signal, a way of saying, hey, we're trying here and hoping that there is
more investment from Europe. Do we know any of that? No. But it all kind of lines up.
It makes sense. I don't know that the administration in Tripoli would actually come out and say,
yeah, we did this. We snatched him and turned him over to the US. Or put him in a position
where U.S. officials could get him.
So I don't know that we'll ever know for certain.
But the U.S. government is being pretty withholding about exactly how they wound up with him.
I mean, when you listen to what they're saying, it's like, oh, he just showed up in the truck,
like they're not talking about it.
And there is the rumor that the administration and Tripoli did this, and that rumor is older
than the knowledge that he was turned over to the US.
So it tracks, and they do have motive to do it.
For those who are younger and are not familiar with this event, you're going to become familiar
with this event.
Everything's going to be brought back up again.
My understanding is that they're looking to put him on trial in the United States.
This will certainly dominate headlines.
Anyway, it's just a thought.
Y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}