---
title: Let's talk about Trump starting his own party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4EOlhYJIQuY) |
| Published | 2022/12/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Predicts Trump will likely lose the Republican nomination based on current polling and trends.
- Assumes Trump won't get indicted between now and then.
- Suggests that Trump may start a third party if he loses the nomination to make money off his base.
- Notes that Trump's base is loyal and may continue to support him financially.
- Speculates that while Trump's third party won't win the White House, it could be more successful than other third parties in the US.
- Proposes that a Trump party could attract current political officeholders to switch parties and join him.
- Suggests that a Trump party might help the Republican party return to a more moderate conservative stance by drawing away far-right members.
- Envisions the possibility of a Trump party winning House seats in deeply conservative areas.
- Emphasizes that Trump's primary goal will be financial gain rather than political success.
- Warns that even if Trump leaves the Republican Party, the ideology of Trumpism will likely persist with another similar candidate.
- Concludes that Trump may not care about the long-term success of his potential third party as much as extracting money from his supporters.

### Quotes

- "It's not about winning. If he loses the Republican nomination, he knows he's going to lose the race."
- "He's not going to win the White House running as a third party, but he might establish a third party that actually is around for longer than he is."
- "It's all about the money with him."

### Oneliner

Beau predicts Trump's potential third party may not win the White House but could outlast him, driven by financial motivations and loyalty of his base.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Monitor political developments and trends for potential impacts on the upcoming election (implied).

### Whats missing in summary

Analysis of the potential impact of a Trump third party on the overall political landscape.

### Tags

#Trump #ThirdParty #ElectionPrediction #PoliticalAnalysis #BaseLoyalty


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a question I got.
Recently we talked about the likelihood of Trump losing the Republican primary.
And I got a question that said, hey, do you think he'll run third party if that happens
and would it be successful?
So we have to make some assumptions here.
Given current polling and trends, yeah, Trump will almost certainly lose the Republican
nomination.
If everything that's happening right now continues on the course that it's on, that's what's
going to occur.
We also have to assume that between now and then he doesn't get indicted.
So if you assume those two things, he doesn't win the nomination, yeah, he would probably
start a third party.
For him, it's not about winning.
If he loses the Republican nomination, he knows he's going to lose the race.
But that doesn't mean he can't make a whole bunch of money off of it in the meantime.
And that's what Trump is going to need.
His base is very, let's just say, trusting of him.
They might continue to fund his endeavors for quite some time.
Now if this happens, the next question is, is it going to be successful?
Depends on what you mean.
Is he going to win the White House?
No.
Will the party be successful?
Probably more than other third parties in the United States.
They have a very, they would have a very amped up base and they would probably have people
in political office change their party to join the Trump party.
In some ways, if this occurs, it might actually be good for the Republican party.
It might help them clean house and reform and go back to being a normal conservative
party rather than a far-right, ultra-nationalist party.
Because many of the more flamboyant characters in the Republican party, those that are causing
the real issues for their fellow Republicans, they are Trump-aligned.
So they would probably go to a party that Trump establishes.
And then as far as successful, I could see a Trump party actually winning House seats
in incredibly red areas.
I could see that occurring.
So when you're asking that question, you have to judge and kind of gauge success.
You have to determine what you mean by that.
He's not going to win the White House running as a third party, but he might establish a
third party that actually is around for longer than he is.
You know, Trump is older.
Eventually, he will not be here.
That party, it might remain.
Because if it can bring in that base and keep it in that echo chamber that develops when
you fall into the Trump world, yeah, it could be around for quite some time and cater to
that far-right position.
It's realistically possible.
Now as far as Trump's concerned, he's not going to care about any of this.
He's going to be caring about the money.
He's going to be trying to get as much as humanly possible out of his base who trusts him.
That's going to be his goal.
So is it possible?
Yeah.
And it's an interesting thought exercise.
But there's a lot of things that have to happen between now and then to be thinking about
Trump as a third party candidate.
It's realistically possible.
It's within the realm of something that really could happen.
But everything has to fall in a certain way.
There are some things that you kind of have to assume occur or don't occur that I'm not
sure that's how it's really going to play out.
I would also kind of remind everybody that even if Trump is gone from the Republican
Party, Trumpism is still there.
There will be a candidate like Trump with that same authoritarian style, with that same
brashness and with that same willingness to lean into conspiracy theories.
There will be somebody else like that in the primary.
And those who are disaffected with Trump might just move to that person.
And if that's the case, they will probably maintain that fanaticism that they have.
And they'll be very enthusiastic supporters of, you know, the Xerox copy of Trump, which
would cut into his profits, which would make it less interesting for him.
It's all about the money with him.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}