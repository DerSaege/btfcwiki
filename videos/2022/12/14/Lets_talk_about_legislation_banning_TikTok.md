---
title: Let's talk about legislation banning TikTok....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MBbPw-fTQDk) |
| Published | 2022/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Legislation introduced aims to ban TikTok in the United States and any network substantially controlled by an opposition nation like China, North Korea, Russia, Iran.
- Concern is that an opposition nation could use platforms like TikTok for coordinated influence operations targeting the US population.
- The focus is on the Chinese government potentially controlling what appears in people's feeds to influence large segments of the US population.
- Espionage concerns are secondary; TikTok is already banned in defense agencies for such reasons.
- Likelihood of legislation passing in the current Congress is low due to limited time, but it may pass when the new Congress convenes.
- Republicans are likely to support the legislation, viewing it as a way to combat the influence of China, especially on younger generations.
- If the legislation passes the House and Senate, it's expected that President Biden will sign it.
- If TikTok is impacted, it may be spun off as a public company based in the US or be replaced by a similar network.
- Ultimately, the focus is more on maintaining power and control rather than protecting individuals.

### Quotes

- "It's really about the ability of the Chinese government to potentially influence large segments of the US population by controlling what's in their feed."
- "It's not really about protecting you. It's about protecting power and control up at the top."
- "Make no mistake about it, this isn't really about protecting you. It's about protecting power and control up at the top."

### Oneliner

Legislation targeting TikTok and similar platforms is more about influence operations than espionage, focusing on protecting power and control at the top rather than individuals.

### Audience

Legislative observers

### On-the-ground actions from transcript

- Monitor the progress of the legislation and advocate for transparency and accountability (suggested).
- Stay informed about the potential impacts on social media platforms and influence operations (suggested).

### Whats missing in summary

Insights on the potential implications for data privacy and free speech rights.

### Tags

#Legislation #TikTok #InfluenceOperations #China #USPopulation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about some legislation that was recently introduced,
what it means, whether or not it's going to be successful,
and if it really is about going after TikTok,
and why it might have support at a later point in time.
Okay, so if you have missed the news,
legislation has been introduced that is set to,
the way it's being framed is it's going to ban TikTok in the United States.
And yeah, I mean, that's what it's going to do.
And that's kind of the goal.
But it's a little bit wider than that,
in the sense that it's not just going after TikTok.
It's going after basically any network that is of any substantial size
that is based in or under the substantial control of an opposition nation,
meaning China, North Korea, Russia, Iran,
the list of countries that are just always show up as being portrayed as the bad guys.
So TikTok definitely falls under that category.
It does.
The list of reasons they have for this,
most of them are kind of, meh, they're not really reasons, they're add-ons.
The real concern is an opposition nation like China would actually have the capability to do it.
Using a platform like TikTok to run a really coordinated influence operation,
and it would probably be successful.
So that's their concern.
There are other things that you'll hear about it being used for espionage or whatever.
Yeah, sure, it could be, but that's not really what this is about.
That's just added-on stuff.
When it comes to the espionage aspects, TikTok's already banned inside defense agencies.
So that's just, it's extra window dressing.
It's really about the ability of the Chinese government in this case
to potentially influence large segments of the US population by controlling what's in their feed.
And yeah, there's data harvesting and stuff like that.
But again, that's not anything that can't be obtained by a thousand other sources.
So it's really about the influence operations.
Okay, so is this going to be pushed through in this Congress?
Probably not. There's not a lot of time left.
It is unlikely that this goes through right now.
However, when the new Congress comes back, I'm pretty sure it'll make it through the House.
I don't know about the Senate yet.
For the Republican Party, the conspiratorial thinking and the talking points and the rhetoric,
when it comes to China, they're going to go for this.
And these are already, the target of the legislation is already to them exemplary of a political issue they have.
They see younger generations and TikTok as synonymous.
And maybe the reason the younger generations don't like them is because of TikTok.
It's the evil Chinese people who have tricked them.
You know, I can see that being fed into and I can see it working.
So in the House, when the Republicans get a majority, it very well might pass.
Because they will view it as it's China's fault the young people have turned against us.
It's not that, you know, we're completely out of touch and yelling at clouds.
Now, from there, does it get through the Senate? I don't know.
If it goes through the House and the Senate, does Biden sign it? I think so. I think he would.
This seems like something that he would actually sign.
So what happens to TikTok if this goes through?
My guess is the company itself would be spun off and become a public company in the United States.
So they would basically base it in the U.S., spin off the U.S. division of it, and make it a public company here.
That's probably what would happen.
I don't think TikTok would go away.
If it does, make no mistake about it, it will be replaced immediately by another network that is very, very similar.
So whoever controls that one can run influence operations.
Make no mistake about it, this isn't really about protecting you.
It's about protecting power and control up at the top.
It's foreign policy. It's about power.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}