---
title: Let's talk about the US, Ukraine, and Patriots....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5iIJ-69yefg) |
| Published | 2022/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Breaking news about the Department of Defense delivering the Patriot missile system to Ukraine to alleviate issues.
- The system is an air defense system with a decent range to disrupt incoming threats in Ukraine.
- The delivery of the system still needs to be signed off by the Secretary of Defense and Biden.
- Recent events in Poland, where something fell, are being used as a pretext to get air defense closer to the Russian border.
- Putin's actions have led to updated air defense being positioned against their border as a response to his invasion of Ukraine.
- This move signals a shift towards potentially putting more technologically advanced equipment into play in Ukraine.
- Beau speculates on the possibility of the United States introducing Reaper drones into the region as well.

### Quotes

- "Breaking news about the Department of Defense delivering the Patriot missile system to Ukraine."
- "Putin's actions have led to updated air defense being positioned against their border."
- "This may signal a willingness of the United States to put more technologically advanced stuff into play in Ukraine."

### Oneliner

Beau reports on the Department of Defense delivering the Patriot missile system to Ukraine, potentially signaling a shift towards more advanced equipment, in response to recent events near the Russian border.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact organizations supporting peace efforts in Ukraine (implied)
- Stay informed about international relations and conflicts (implied)

### Whats missing in summary

More in-depth analysis of the potential impacts of introducing advanced technology in conflict zones.

### Tags

#DepartmentofDefense #PatriotMissileSystem #Ukraine #Russia #InternationalRelations


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we have some breaking news.
Well, it's breaking at the time I'm filming it, but with all the news coming out right
now, I have no idea when this is going to go out.
We have some news dealing with the United States and Ukraine that shouldn't really come
as a surprise, but it might.
It appears that the Department of Defense is poised to deliver a new system to Ukraine.
It is the Patriot missile system.
They'll probably be older stock, but they're still relatively advanced, and they will certainly
alleviate a lot of the issues that have been occurring in Ukraine with stuff raining down
on that country.
It's worth noting that at least at time of filming, it still has to be signed off on
by the Secretary of Defense and Biden.
It appears as though that will happen.
So if you're not familiar with this system, it is an air defense system, and it's got
a pretty decent range, so it should be capable of disrupting a lot of the incoming that is
hitting civilian areas in Ukraine right now.
Now there are a couple of other pieces of information to go along with this.
One, about four weeks ago something fell in Poland.
I put out a video talking about what occurred.
At the five minute mark, I said that no matter what happened, it would be used as a pretext
to get air defense closer to the Russian border, which is what's happening.
It was one of Putin's large complaints.
He was very against NATO air defense being close to the Russian border, and because of
his actions, because of the invasion, basically everything he was trying to bully Europe into
doing, the opposite is happening.
When it comes to the list of foreign policy failures that can be attributed to his invasion
of Ukraine, you can now add updated air defense right up against their border.
The other thing that makes this interesting is that this isn't ancient tech here.
In the older stock, it's going to be pretty good.
This may signal a willingness of the United States to put more technologically advanced
stuff into play in Ukraine.
If that is the case, and that seems reasonable to infer, I don't think that those Reapers
are too far behind.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}