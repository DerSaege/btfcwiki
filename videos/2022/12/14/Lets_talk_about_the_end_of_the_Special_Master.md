---
title: Let's talk about the end of the Special Master....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VuHleUQ4Bs0) |
| Published | 2022/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The special master position, tasked with going through Trump's documents, no longer exists after an appeal in the 11th circuit.
- Department of Justice prosecutors now have direct access to tens of thousands of documents to build their case.
- Trump's legal strategy of delay tactics is evident in requesting a special master, now proven to lack a legal basis.
- Trump's team chose not to take the case to the Supreme Court, indicating a potential understanding of the gravity of the situation.
- There are uncertainties regarding Trump's potential defense strategy and his realization that Fox News talking points won't suffice in court.
- The documents case presents objective charges, leaving Trump in a precarious legal position.
- Trump may be coming to terms with the legal jeopardy he faces, leading to potential erratic behavior as consequences sink in.

### Quotes

- "Delay, delay, delay, delay, delay."
- "Fox News talking points are not going to work in a federal courtroom."
- "He might be at the point where he's beginning to understand the legal jeopardy that he's in."

### Oneliner

The special master position is gone, leaving Trump exposed to direct access for building a case with potential erratic behavior ahead as he grapples with legal jeopardy.

### Audience

Legal observers, political analysts.

### On-the-ground actions from transcript

- Stay informed on the legal developments surrounding the case (exemplified).
- Monitor Trump's behavior and responses for potential insights into his understanding of the situation (exemplified).

### Whats missing in summary

Insights into the potential implications of Trump's legal predicament on broader political narratives.

### Tags

#LegalJeopardy #Trump #SpecialMaster #DepartmentOfJustice #DelayTactics


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about the special master
and the special master no longer existing.
That position is officially gone.
There was an appeal in the 11th circuit,
basically very directly said that this position
that this position never should have been created anyway. So in the documents
case, for Trump, the person who was tasked with going through all of the
documents, that's no longer there. The Department of Justice filter team still
exists, but what this means is that the Department of Justice prosecutors now
have direct access to tens of thousands of documents to build their case.
They're now at the point where they will be able to move forward as if it was a
normal case. Now there are some interesting little side notes. When all
this started, as soon as Trump started asking for a special master, everybody,
this channel and pretty much every other channel that didn't have their eyes
closed due to ideology was saying, yeah that's not gonna work. That has nothing
to do with going with what's going on right now. This is just a delay tactic
which is a common Trump legal strategy, delay, delay, delay, delay, delay.
It's, in this case, I think his goal was to delay it long enough so either he himself or one of his
allies would be in the Oval Office and be able to pardon him. That's what I think but we don't
know. There's an interesting development that goes along with this. Trump's team could have taken
this to the Supreme Court. They decided not to. Now here's the thing, they would have lost.
The special master didn't have real legal basis to exist in this case.
So they would have lost.
But it's more of that delay tactic and that appeal not being filed and that's what reporting
suggests is that Trump is not going to appeal this.
That might be the first sign that we see from Trump directly that he's beginning to understand
the gravity of the situation.
It might mean that his defenses have cracked and that somebody is at least explaining to
him that Fox News talking points are not going to work in a federal courtroom.
And he might be starting to take that to heart and maybe start focusing on preparing a real
defense, which to be honest, I don't even know what it would be.
Based on the publicly available information, some of the likely charges, like I have no
idea how he would mount a defense against them.
To me, his best bet is that either A, the crowd that wants to protect the institution
of the presidency wins out and he doesn't get charged, or the process takes so long
that he has an ally who can pardon him.
When it comes to the documents case, it's very different than any of the other stuff
we're talking about. It's not really subjective when it comes to some of the
charges that are likely to be filed. I'm starting to think that he might be
at the point where he's beginning to understand he is in the find out portion
of the show and I'm not sure what we can expect from him anymore. I've been pretty
good about predicting his moves. If he's actually at the point where he's
beginning to understand the legal jeopardy that he's in and he understands
the consequences that might go along with that, his behavior is a is likely to
get very erratic. So it's just something to watch out for. Anyway, it's just a
thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}