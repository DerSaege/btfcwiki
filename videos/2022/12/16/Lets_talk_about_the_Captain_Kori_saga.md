---
title: Let's talk about the Captain Kori saga....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=489B7RNudAQ) |
| Published | 2022/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of a well-known pirate and directs viewers to a young man's YouTube channel aiming for a silver play button.
- The young man's channel reached and surpassed its goal of 100,000 subscribers with the help of various channels, including Beau's.
- The channel was later taken down by YouTube due to the age of the person associated with the email on the account.
- After YouTube realized the young man's mom was actively involved, they reinstated the channel.
- There is an Amazon wish list for the young man to create unboxing videos, which also includes items for the hospital.
- Beau notes that the Amazon wishlist was taken down before filming but mentions a link to donate to Rainbow's Hospice.
- Despite some issues with the Amazon account, the channel is back online, and there is a new goal mentioned.
- Beau sends a message of encouragement to Captain Corey, assuring him he will recover from the setback and mentions the negativity that comes with being a YouTuber.
- The young man is likely to face unkind comments but is reminded that people are aware of his channel.
- Beau wraps up with a heartwarming message for the holiday season and signs off.

### Quotes

- "all pirates end up in jail."
- "You'll be able to recover from this. You will do fine."
- "There are a lot of unkind comments that will come your way."

### Oneliner

Beau updates on a young YouTuber's journey to a silver play button, facing setbacks and support along the way.

### Audience

YouTube viewers

### On-the-ground actions from transcript

- Support Captain Corey's YouTube channel by subscribing and engaging positively (exemplified).
- Donate to Rainbow's Hospice (exemplified).

### Whats missing in summary

The full transcript provides a detailed account of the challenges faced by a young YouTuber in reaching his subscriber goal, the temporary takedown of his channel, and the subsequent reinstatement, along with heartwarming community support. 

### Tags

#YouTube #Support #Community #Encouragement #Donation


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk a little bit more
about a very well-known pirate at this point.
If you missed it, earlier this week, we did a video.
I was dressed like a pirate, and we were directing people over
to another channel, asking them to check out
another channel of a young man whose big goal is
to get a silver play button from YouTube.
So he needed 100,000 subscribers.
That was his initial goal.
We were not the only channel to profile him by any means.
His channel definitely met that goal and exceeded it.
There were periods where he was picking up
thousands of subscribers a minute.
And I know that y'all played a big part in that.
I see y'all in the comments sections of the videos.
It's very heartwarming.
So he succeeded.
He got to 100,000.
Everything was going great.
And then his channel disappeared.
It was yanked by YouTube.
Now, the reason it was yanked, there's
a whole bunch of speculation about this.
The reason it was yanked had to do
with the age of the person whose email was
associated with the account.
There are rules about that.
YouTube, after people bringing it up and being like, hey,
you yanked this channel, YouTube went in and was like,
oh, wow, his mom is actively involved with the channel.
And everything was adjusted.
His channel is back.
And you can go there now if you want to check it out.
It's crack in the box.
And so from there, his channel is back online.
So everything is going well.
One of the things that I would like to point out
is that there is an Amazon wish list
so he could get stuff to do on boxing videos.
I do just kind of want to note that on that wish list
is a whole bunch of stuff for the hospital.
And I'm very reluctant to say this next part,
because I'm afraid it's just going to cause even more
of a continuation of this saga.
But when I clicked on the Amazon wishlist
right before I started filming, it had been taken down.
There is also a link to donate to Rainbow's Hospice as well.
I'm sure that the Amazon thing will be worked out at some
point, but that's what is going on there.
There's a new goal, if you want to hear about it,
you can go over there and find out about it.
But it's all going very, very well.
Now, a personal message to Captain Corey.
Because I am certain it was a little disheartening
to see your channel gone after you met your goal.
You wound up in YouTube jail.
all pirates end up in jail.
It's all right, you'll be okay.
You'll be able to recover from this.
You will do fine.
And there will be a whole lot of people who remember it
as the day they almost got Captain Cory.
Aside from that, you are now a YouTuber.
You're going to be putting out videos.
And in the comments section,
be told all kinds of things. There are a lot of unkind comments that will come
your way. You will hear people tell you that you are the worst YouTuber that
they have ever heard of, but they have heard of you. So at time of filming, his
channel's back, everything's fine with the exception of whatever's going on
with the Amazon account.
And you have a little bit of a heartwarming event
for the holiday season.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}