---
title: Let's talk about the last committee hearing and what to expect....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q32BGQmVLJ0) |
| Published | 2022/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Previewing the upcoming final committee hearing regarding January 6th.
- The last hearing will be shorter and serve as a closing argument.
- Expect new evidence presentation heavily relying on multimedia.
- It will also be a working hearing with votes on legislative recommendations, referrals, and adopting the report.
- Legislative recommendations aim to prevent similar events in the future.
- Referrals may include criminal referrals, election commission referrals, and house ethics referrals.
- The committee will vote on these referrals and adopting the report.
- The hearing is scheduled for December 21st.
- The outcome of the votes is likely predetermined.
- The effects of the committee's work will depend on the Department of Justice's actions.

### Quotes

- "This is the final tell them what you told them phase of things."
- "Legislative recommendations aim to help mitigate the possibility of this happening in the future."
- "The ball is in DOJ's court and what they decide to do at this point."

### Oneliner

Beau previews the final committee hearing on January 6th, where new evidence will be presented, and votes will be cast on legislative recommendations, referrals, and adopting the report, with the ball ultimately in DOJ's court.

### Audience

Committee members, concerned citizens

### On-the-ground actions from transcript

- Attend or follow the updates from the upcoming final committee hearing (exemplified)
- Stay informed about the legislative recommendations and referrals made during the hearing (exemplified)

### Whats missing in summary

Insights into the potential long-term impacts of the committee's findings and actions.

### Tags

#CommitteeHearing #January6th #LegislativeRecommendations #Referrals #DOJ


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about the last committee
hearing that is upcoming.
They have decided to have one more hearing.
And this is going to be a little bit different.
We've talked about it over and over again.
Tell them what you're going to tell them.
Tell them.
Tell them what you told them.
This is the final tell them what you told them
phase of things.
So what can you expect from this hearing, from the last hearing, looking into January
6th?
First it's going to be shorter.
Picture this as a closing argument.
It's going to be shorter, it is going to cover everything, and it's probably going to introduce
a little bit of new evidence that they have gathered since the last committee hearing.
It will be heavily reliant on multimedia presentation, trying to get as much information across as
quickly as possible.
So that's the presentation side of things.
This hearing is a little bit different because it's also going to be what they're calling
a working hearing and there will be votes.
There will be votes on three things, legislative recommendations, referrals, and adopting the
report because there's going to be a report that's released covering all of this.
Legislative recommendations, what does that mean?
Plain and simple.
They apparently have recommendations that they believe can help mitigate the possibility
of this happening in the future.
So they will be recommending those and there will be a vote on those.
The vote will be of the committee itself, not of the entire House right now.
When it comes to the referrals, something interesting has happened.
They said that there are five categories, five or six, quote, categories of referrals.
Now, most attention has been paid to the possibility of criminal referrals, but understand, those
aren't the only ones they can level.
They can level other ones.
The thing is, I can only think of three categories.
I don't know what the other two are.
One would be criminal referrals to DOJ.
Those are the ones that everybody's been talking about since this whole thing started.
Very symbolic in nature.
Another type of referral would be to the election commission and maybe pointing them in the
direction of people they believe violated election laws.
And then the last one that I can think of is to house ethics.
And there will be votes on the referrals.
What the other two categories are, I have no idea.
I've been trying to think of what they could be.
I don't know.
Maybe I'm misreading what they mean by category.
Maybe they're talking about people on the ground, people in Congress, staffers at the
White House.
I honestly can't figure out what would be five or six categories, though.
And then adopting the report.
There is a report that has been prepared.
I think they're still doing a little bit of proofing on it.
they will vote on whether or not to adopt it. Understand they're gonna vote
yes, that's they wouldn't be holding this if they weren't ready to do that. So I
would say that most of these votes are already predetermined that the outcome
is already known to the people on the committee. Not a lot of tension there, but
that's what you can expect. This is expected to occur on December 21st, so
So Merry Christmas.
And then that should be it for the House committee.
There shouldn't be anything more coming out of it.
They were able to get everything done in the time that they had.
And they did a good job.
Now let's see what the effects of it are over the long term.
At this point, the ball is in DOJ's court and what they decide to do at this point.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}