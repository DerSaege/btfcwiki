---
title: Let's talk about Louisiana, the AP, and movement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZOZyo6YOAu4) |
| Published | 2022/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Associated Press uncovered a possible cover-up within law enforcement in Louisiana related to the death of Ronald Green in May 2019.
- Initially, Green's family was told he died in a car crash, but the footage revealed a brutal incident.
- Federal charges against the officers involved were considered but not pursued due to concerns about proving willful misconduct in court.
- The state has decided to pursue charges, resulting in the indictment of five officers for offenses ranging from negligent homicide to malfeasance in office.
- The footage was so disturbing that even the state police's use of force instructor had nothing positive to say and viewed it as warranting charges.
- The journalists at the Associated Press played a significant role in bringing about justice in this case by pursuing the story relentlessly.
- Despite initial allegations of a cover-up, progress in the case has led to cracks in the "thin blue line" in Louisiana.
- Rebuilding trust with the community will be challenging after the release of the incriminating body camera footage and the disturbing actions it captured.
- The footage, while recently made public in May 2021, has been around for some time and showcases actions that no one could deem as normal in law enforcement.
- Law enforcement officials in the state, who typically support officers, are now openly condemning their actions due to the severity of the footage.
- The story will continue to gain widespread coverage, especially as more people view the footage, which is among the most disturbing seen by Beau.

### Quotes

- "The journalists at the Associated Press played a significant role in bringing about justice in this case by pursuing the story relentlessly."
- "Rebuilding trust with the community will be challenging after the release of the incriminating body camera footage."
- "The story will continue to gain widespread coverage, especially as more people view the footage."

### Oneliner

The Associated Press uncovered a possible cover-up within Louisiana law enforcement, leading to charges against officers for the death of Ronald Green and revealing the challenges of rebuilding community trust post-disturbing footage release.

### Audience

Louisiana residents, journalists, activists.

### On-the-ground actions from transcript

- Support investigative journalism by following and promoting the work of organizations like the Associated Press (exemplified).
- Advocate for transparency and accountability within law enforcement by engaging with local officials and demanding justice for victims (exemplified).
- Join community efforts to rebuild trust and foster positive relationships between law enforcement and residents (exemplified).

### Whats missing in summary

The emotional impact and urgency conveyed by witnessing the disturbing footage firsthand can best be understood by viewing the full transcript.

### Tags

#Louisiana #AssociatedPress #LawEnforcement #Justice #CommunityTrust


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about Louisiana
and the Associated Press and law enforcement
and a story that began way back in May of 2019,
a story that appears to be entering its final chapter
finally.
So back in May of 2021, the Associated Press had some investigative reporting going on,
and it appeared to expose a cover-up within law enforcement in Louisiana, and there was
some footage of the death of Ronald Green.
Now his death occurred back in May of 2019, and if I'm not mistaken, his family was initially
told he died in a car crash.
That is not what the footage showed.
The footage was brutal.
It was very bad.
One of the delays that has occurred was that the federal government was looking at bringing
federal charges against the officers involved.
But as time went on, they became concerned that they weren't going to be able to prove
the willful part that they needed to prove in court.
So they kind of nodded to the state, and the state has decided to pursue charges.
Five officers have been indicted.
There likely would have been a sixth, but he died in a car accident.
The charges range from negligent homicide to malfeasance in office.
It's pretty wide ranging.
The footage was so bad that even the state police's own use of force instructor, a position
that you will often find defending officers in court, had nothing positive to say and
categorized it in a way that, I mean honestly, can only lead to charges.
Now the Associated Press, and this is going to be overlooked because it's the way the
news works, it is unlikely that these proceedings would be happening, that these charges would
have been filed if it weren't for the journalists at the Associated Press really pursuing this
story. I truly believe that without those journalists there there would not be
justice in this case. It is worth noting that at this point now that things have
progressed despite any allegations of cover-up early on the the thin blue line
in Louisiana, oh it has cracked. It has definitely cracked. The head of the state
police there said, these actions are inexcusable and have no place in
professional public safety services. And then of course the line that has become
all too familiar to Americans about rebuilding trust with the community.
That's gonna be hard because the reality is that footage, yeah, it was released to
the public in May of 2021, but it was around the whole time. It was body camera
footage from the officers and the actions there, the taunting that occurred,
There is nobody who looked at that and was like, oh that's normal. So everybody
who saw that, who chose to remain silent about it, did so knowing that what they
were witnessing was wrong. It's going to be incredibly hard to rebuild trust
with the community after that. Regardless of how things proceed in court, there is
no way to look at what is on that footage and say, yes, that's normal law enforcement
activity because it's not.
It is so blatant that even law enforcement officials in the state who would normally
be doing everything they could to downplay and deflect from the officers are outright
condemning them in statements to the press. It's that bad. This will be an
ongoing story. The coverage of this will will be pretty widespread, especially
after people start viewing the footage. The footage has been out, but because
there wasn't a lot of forward movement on it, it didn't gain traction. But I
I personally, I will say that I viewed a lot of this footage
over the years.
This is easily in the top 10, as far as just the absolute worst.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}