---
title: Let's talk about Griner, Whelan, and Bout....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GMldJ2oXZqw) |
| Published | 2022/12/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing a recent trade deal involving Bout, Greiner, and Whalen.
- The Biden administration's deal with Putin to exchange Bout for Greiner.
- Some argue that Whalen should have been part of the deal.
- Victor Bout's value questioned due to outdated skill set and high visibility.
- Doubts about Bout's future usefulness to Russia.
- Addressing Trump's claims and actions regarding the trade deal.
- Criticizing Trump's approach to handling international prisoner exchanges.
- Warning against telegraphing intentions in negotiations.
- Suggestions for how Trump could actually help in bringing Whalen home.
- Critiquing the politicization of the trade deal and its impact on Whalen.

### Quotes

- "If anybody could have worked out that deal, Bout for Whelan, it would have been Trump."
- "You can't telegraph your wishes to the opposition. It doesn't work."
- "Stop being a national security risk, stop tying up the FBI's counterintelligence teams."
- "This is a good trade. The people who don't were stirred up by somebody saying, 'oh, this is what I would have done,' when he literally didn't do it."
- "In the process of turning Paul Whelan into a political pawn to make Biden look bad, you kept him away from his family longer."

### Oneliner

Beau breaks down a recent trade deal involving Bout, Greiner, and Whalen, criticizing Trump's role and warning against telegraphing intentions in negotiations.

### Audience

International Relations Watchers

### On-the-ground actions from transcript

- Contact your representatives to advocate for responsible and strategic approaches to international negotiations (implied).
- Stay informed about international relations to better understand complex trade deals and diplomatic actions (implied).

### Whats missing in summary

Insights on the potential repercussions of politicizing diplomatic negotiations and the importance of strategic communication in international relations.

### Tags

#Diplomacy #InternationalRelations #TradeDeal #Critique #PoliticalManipulation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Greiner and Whalen
and Bout and that trade, that deal.
We're going to talk about what happened.
We're going to go through the talking points that
are circulating everywhere.
And we're going to talk about who messed up,
because somebody did.
Somebody did.
And it's not the first time they made this exact mistake.
So it is definitely worth discussing.
There is a problem here, but it's not
the one that's getting all the coverage.
The idea that's getting the coverage
isn't really that valid.
So let's talk about what happened first.
The Biden administration worked out a deal with Putin,
exchanging Bout, Victor Bout, for Greiner, bringing her home.
OK?
There are some people who believe that Victor Bout is
super valuable, and therefore, Paul Whalen, who
is also in Russia, he should have been part of the deal.
They both should have came home in exchange for Victor Bout.
OK.
Victor Bout is super valuable.
Why?
What makes him valuable?
You saw a Nicolas Cage movie?
What exactly makes Victor Bout valuable to Russia?
So he was low key on the run since, I want to say,
2004, somewhere around there, behind bars since 2008,
spent 10 years in Marion.
He's kind of been out of the game a while.
His skill set really isn't useful anymore.
Think about how much has changed.
Forget about the time he was on the run.
Just the time he was locked up, those 14 years.
What made him so valuable way back then
was that he understood the systems.
Those systems have changed.
Yeah, 20 years ago, he was the lord of war.
Today, he's been out of the game for a decade and a half.
What, do you think they're going to put him back to work?
You think the Russians are going to use him?
Seems unlikely.
The US and Interpol have his name, his face, his prints,
his DNA, his biometrics, his voice.
What do you think he's going to do?
Odds are he won't leave Russia.
If he does, for the rest of his life,
he will be one of the most watched men on the planet.
Half the countries he might think about going to,
he'd have a giant bull's eye on him today.
A lot has changed in 14 years.
His skill set isn't there anymore.
And he's just incredibly hot.
The most likely scenario is that he becomes
a low-key celebrity in Russia.
Maybe somebody in that similar export industry
taps into him to use his name for clout.
But that's about it.
He's pretty much useless other than as a shield,
as a name to associate with.
That's it.
The deal as it stands, what actually occurred,
a relatively innocent person on a massively trumped-up sentence
was freed before they had to do a whole bunch of time.
In exchange, somebody who already
did a whole bunch of time, whose skill set is kind of useless
at this point, was sent home.
That's what happened.
Paul Whelan, that's a little bit of a different case.
He's there on espionage charges.
He's there on espionage charges.
Now, Trump said that he was just there for the asking for,
making it seem like it would be easy to get Greiner and Whelan
in exchange for Bout.
Here's the weird part about that.
Why didn't Trump do it?
Whelan's been over there since 2018.
He had Bout, and he didn't have the complicating factor
Greiner.
So why didn't he work the deal out?
He knows that it can't be done because he
knows they're different.
He's just putting this out there to stir up
his more easily manipulated base.
And they bought it.
They bought it.
Here's a pro tip.
If you're looking for insight into an international prisoner
exchange involving an espionage case,
perhaps don't take advice from somebody being investigated
under the Espionage Act.
If anybody could have worked out that deal,
Bout for Whelan, it would have been Trump.
Think about all the contacts he has there.
Here's the thing.
All of these pundits who got out there and said,
it's super important, we have to bring Whelan home, that's true.
It's absolutely true.
But you shouldn't have said that.
You shouldn't have done that.
You shouldn't have got on the air and said that,
because now you've made him valuable to Russia.
This is the exact same mistake Trump
made in Afghanistan that caused that giant mess.
You can't telegraph your wishes to the opposition.
It doesn't work.
You would think somebody with their name
on a book titled The Art of the Deal would know that.
If you got out there and you made him trend,
and you talked about how he was so much more important
to bring home, and that's what should have happened,
all you did was lengthen his stay,
because now they know he's important.
They're not going to give him up.
It's going to be a lot harder now.
If Trump actually wants to help Paul Whelan,
the most effective thing he could do
is stop being a national security risk,
stop tying up the FBI's counterintelligence teams,
because then they could go out there
and snatch up a Russian case officer,
because that's who Whelan's going to be traded for.
A relatively innocent person
is going to regain their freedom from this.
That's what happened.
This is a good trade.
The people who don't were stirred up by somebody saying,
oh, this is what I would have done,
when he literally didn't do it.
He couldn't do it.
And in the process of turning Paul Whelan
into a political pawn to make Biden look bad,
you kept him away from his family longer.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}