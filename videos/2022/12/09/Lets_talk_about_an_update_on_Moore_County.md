---
title: Let's talk about an update on Moore County....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ytw-q142Wn4) |
| Published | 2022/12/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on Moore County and its progress after an incident.
- Law enforcement found casings from the rounds, potentially linking the weapon to the incident.
- The significance of the casings lies in leading to the location the shots were fired from.
- Law enforcement is offering a reward for information leading to arrest and conviction.
- They have applied for search warrants without disclosing the details to the media.
- Beau predicts a potential twist in the search warrant scenario.
- The investigation is ongoing, and there may be quick progress due to the search warrants.
- The blackout in Moore County resulted in casualties.
- If any casualties are linked to the incident, it escalates the situation significantly.
- Expect more information soon on what happened and who's responsible.

### Quotes

- "Things are reportedly starting to come back on and everything's starting to move along nicely."
- "I have a feeling that we may see one of those, you know, America's most incompetent criminal things happening here."
- "If they show any relationship whatsoever to that being a result in any way to this, this goes to a whole new level."

### Oneliner

Beau provides updates on Moore County, including casings found by law enforcement and upcoming search warrants, hinting at potential significant developments soon.

### Audience

Law enforcement officials

### On-the-ground actions from transcript

- Provide any information related to the incident to law enforcement (suggested)
- Stay informed and aware of updates regarding the investigation (implied)

### Whats missing in summary

Full context and detailed insights from Beau's analysis can be best understood by watching the full transcript.

### Tags

#MooreCounty #LawEnforcement #Investigation #Casings #SearchWarrants


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk a little
bit more about Moore County, provide an update on what's going on, what's been happening,
and how everything is progressing there. Things are reportedly starting to come back on and
everything's starting to move along nicely. There have been some developments as far as
trying to figure out exactly what has occurred. Okay, so as far as that goes, it appears that
law enforcement has found some casings from the rounds. That in and of itself, the casings
can be used to link the weapon to the incident. But realistically, that information isn't going
to be what solves this. Normally by the time that is valuable, they kind of already know who it was.
So that in and of itself, not huge. However, because they found the casings, that means
they also found the location the person fired from. Odds are there's other evidence there
that isn't being disclosed. The law enforcement in the area has also publicized the fact that
there is, I want to say, 75 grand or so that is up for grabs for information leading to the arrest
and conviction and all of that stuff. It's coming from three different sources. And then there's
an interesting tidbit. Law enforcement has said that they have applied for search warrants,
but they're not telling the media what it's for. They're not disclosing that yet. The media is
reporting it, and as they report it, they're saying that law enforcement isn't telling them
the address or the property to be searched. Yeah, I'm going to go ahead and call this now.
That's not what the search warrant's for. That'll become entertaining later. I have a feeling that
we may see one of those, you know, America's most incompetent criminal things happening here.
So, the investigation is proceeding. I have a feeling if the search warrants yield what they
will probably yield, they're going to know who it was pretty quick. So, things are moving along.
There were people who did not make it through the blackout, and if you don't know what I'm
talking about, in North Carolina, in Moore County, there was an attack on the electrical system there,
and it caused a blackout. I'm sorry, just to catch anybody up who doesn't know.
So, given the fact that there was at least one person who did not make it,
if they show any relationship whatsoever to that being a result in any way to this,
this goes to a whole new level. I'm going to say that I would assume that
we're not looking at much longer before more tangible information comes out about what
happened and who's responsible for it. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}