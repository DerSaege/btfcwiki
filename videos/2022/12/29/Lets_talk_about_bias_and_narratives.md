---
title: Let's talk about bias and narratives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wyphBDP-PSc) |
| Published | 2022/12/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses information consumption and recognizing cherry-picked information shaping narratives.
- Believes biases can lead to bigotry when assigning crimes to specific demographics.
- Critiques a message suggesting discussing looting, carjackings, and gun crimes in democratic-controlled cities.
- Breaks down the flaws in cherry-picked crime statistics and biases related to demographics.
- Compares smash and grabs to construction site theft, pointing out selective reporting.
- Analyzes the demographics behind catalytic converter thefts and the areas with the highest increases.
- Debunks the idea of gun crimes being solely a problem in Democrat-run cities.
- Argues that gun crime is more prevalent in red states, contrary to certain narratives.
- Attributes the root cause of high gun crime rates to cultural factors and misinformation.
- Encourages punching up against systemic issues rather than kicking down at marginalized groups.

### Quotes

- "Stop kicking down. Punch up."
- "You really think it's not white people that are out there ripping off catalytic converters? I have a hard time believing that."
- "It's a red state problem."

### Oneliner

Beau breaks down biases, cherry-picked crime stats, and misconceptions about gun crimes, urging to punch up against systemic issues instead of kicking down at marginalized communities.

### Audience

Information Consumers

### On-the-ground actions from transcript

- Challenge biases and stereotypes in your community (implied).
- Counter misinformation and cherry-picked data with facts and logic (implied).

### Whats missing in summary

In-depth analysis and examples of how biases and cherry-picked information can lead to harmful narratives and stereotypes.

### Tags

#InformationConsumption #BiasRecognition #CherryPickedStats #SystemicIssues #CommunityAction


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about information consumption,
how to see through cherry picked information that is
trying to shape a narrative, what to do if you encounter
that, and it also illustrates something that I have believed
for a long time, which is if you encounter somebody with a
bias, if you let them talk long enough,
they will show you where that bias kind of transfers over
and leaves normal bias and turns into straight out bigotry.
And normally, it occurs when they
start assigning things to different demographics
that they're not really responsible for.
OK, so here's this message.
We're going to read the whole message,
And then we're going to try to complete the task assigned here.
Let's do a video on who's doing the looting in Buffalo
with over 30 people dead during one of the worst winter storms
ever.
Let's hone in on exactly who is doing the looting
during Christmas.
Matter of fact, why don't we talk about all the smashing grabs
there are going on across the country?
Or we can talk about carjackings and armed robberies.
or who is responsible for stealing
people's catalytic converters in the middle of the night?"
While we're at it, let's break down
why gun crimes are so high in democratic-controlled cities.
It's not happening here where I live.
I wonder why.
Let's break down all that bow.
Or are you not too confident in breaking that down,
because it might not fit the narrative?
Whatever my narrative is.
OK.
So what do you have here?
The obvious implication here is that this person believes
that there is one demographic responsible for all of this.
And the thing is, they're probably right.
It's just not the demographic they think it is.
So what you have here are a whole bunch
of cherry-picked crimes that, in their mind,
are associated with the demographic
that they want to demonize.
When you encounter this, the easy thing to do
is kind of separate it out and find one
where there is a parallel crime that is intentionally excluded
because it doesn't fit the narrative they're
trying to paint.
Smashing grabs, right?
OK.
So does that include construction site theft?
I mean, it's basically the same crime.
But when you call it a smash and grab,
you're limiting it to smashing the window at a retail store
and taking the stuff, right?
Even though it's basically the same thing
when you're talking about construction site theft.
What happens with construction site theft?
Some guy in a 2012 Dodge Avenger that's missing both
of its side view mirrors has bubbling tint
and a picture of Calvin on the back, rolls up to the gate,
smashes, cuts the lock on the gate, pulls out of the way.
And that's when the F-150s, the Chevy Silverados,
and the Ford Econoline vans pull into the construction site
and start robbing the place blind.
Why isn't that included?
Because let's be honest, when I started naming the vehicles,
who did you picture?
Not the demographic he wants to villainize.
You pictured, I mean, me.
It's me.
That's who they look like, right?
Well, maybe they don't include that because it's not as
serious an issue.
Maybe that's what it is.
OK, so the whole wave of coverage with the smash and
grab stuff started because of a two week
period in California.
very intense wave of it happened.
And it was, well, we'll get to that in a minute.
The timing of this occurring is interesting.
So two weeks in California, there were 11 incidents,
and it was $330,000.
That's how much merchandise was taken
in those smash and grabs that prompted all of this coverage.
So what we're going to do is we're
going to say that it is that intense year round, year round. Every two weeks it is that
bad for an entire year. That's $8.5 million. And we're not just going to limit it to California.
We're going to say it's nationwide that bad, which brings us up to around $429 million.
Now, we're also using the inflated number.
Those during that two week period, they were incredibly well organized and they were hitting
high end stores.
Each incident, each of the 11 incidents averaged around $30,000 a hit.
Now, if you were to Google and find out what the average take from a smash and grab is,
it is $7,594.48.
So that $429 million number should really be closer to $100 million, if we're being
even remotely accurate, even with that same intensity, if we're just being honest about
the averages.
But it doesn't matter, $429 million will roll with that.
Construction site theft, about a billion dollars a year, much more serious problem, but that
That doesn't get included because it can't be used to demonize the target demographic.
It's cherry-picked information.
But they made that other mistake, transferred from bias to bigotry.
You really think it's not white people that are out there ripping off catalytic converters?
I have a hard time believing that.
OK, so in absence of good information about this
as far as arrests, because most of them go unsolved,
we have to use a little bit of just reasoning here.
The easiest things to look at would be the type of cars
that get hit and what they can tell us
about the area in which they exist
and the areas that saw the largest increases.
And the reason you're looking at the type of cars
is because it's a relatively safe assumption
to suggest that those people who are out there ripping off
catalytic converters don't have a whole lot of gas money
to their name.
They're going to hit things that are relatively close.
So what kinds of vehicles are targeted the most?
have their converters stolen the most often.
I already gave you the list.
It's the ones that made you think of white people.
F-series trucks,
Ford trucks, Avengers,
Econoline vans,
Chevy Silverados, all in the top ten.
I'm willing to bet
that there aren't a whole lot of people from the inner city
rolling out to an area where there's a whole bunch of F-150s parked
with their Sawzall to rip off the converters.
Odds are, this is people who might have been portrayed
by extras on Breaking Bad.
Now, what areas have the greatest increase?
Washington, Oregon, and Texas.
Texas, Washington saw in 2020 saw a 1,324% jump, and then in 2021 saw an additional 617%.
Oregon, 1,117%, and then in 2021 an additional 645%.
Texas, 741%, and then an additional increase of 817%.
Incidentally, this started around the same time as the Smashing Gramps.
Weird.
I wonder why.
I mean, I don't think I need to say that when you picture Oregon, you probably don't
picture a bunch of black people.
I mean, that's a pretty white state.
The vehicles targeted would be those around them.
They're white people cars.
And to be clear, the rest of the top 10, it's like the Jeep Patriot and stuff like that.
They are cars that are stereotypically associated with people that look like me.
Trying to associate this crime with the demographic that's being targeted by this narrative is
where it transfers from bias into bigotry, because it's just not accurate.
So what would be the demographic here?
It's not going to be a race demographic, it's not going to be a racial demographic.
My guess is what you're dealing with, if you look at the timing of when it started, you're
You're dealing with a whole bunch of people who experienced the economic downturn at the
beginning of the pandemic who probably also already had a criminal record who, when they
got arrested the first time, they weren't rehabilitated, they were warehoused, they
weren't given any educational opportunities so they could overcome that felony stamp that's
going to haunt them for the rest of their life.
So when the downturn happened, they went to crying.
That's your demographic.
That's who's responsible for pretty much everything you're talking about right now.
not a racial demographic as much as you might want it to be. Okay, now let's
break down why crimes are so high in Democrat controlled cities. It's not
happening where I live. I wonder why. Okay, sure, sure. Obviously this is somebody who
lives in a rural area. Yeah, you're right. There's not a lot of murders that happen
in rural areas, but man, there's a lot of people that go missing.
It's just different.
Don't claim moral supremacy on this one.
OK, here's the thing about the whole gun crime,
Democrat-run cities, it's blue cities doing it.
The places with the most gun problems are red states.
They are red states.
And it really doesn't matter what metric you're using for that.
It's a red state.
Now, what people who use this talking point
have been conditioned to parrot by their betters is that, well,
it's the blue cities and the red states
that drag our numbers up.
No, that doesn't even make sense.
Do you think that in California, the major cities are red cities?
No, they're blue cities too.
So you have areas with blue cities and blue rural areas, and they have lower rates.
So it's not a blue city problem.
It's a red state problem.
Additionally, where are gun laws made?
They're not made at the city level.
Maybe when you're talking about permitting and stuff like that, but generally gun laws
Those are made at the state level, and it's a red state problem.
The outlets that push this narrative, they use raw numbers rather than rates.
Look at the rates.
Population to incident type of thing.
It's a red state problem.
tricked you, they lied to you, they duped you, they made you look like a fool. That's
what happened. What causes it? I actually don't believe it's the laws. I don't think
that's the primary reason for it. I think it's a cultural thing. I think it's people
who don't know how to process information, who are quick to get angry and want to blame
body, like you did in this message, who also then have a gun on their hip when
it happens, and they don't have time to think it through. And that has a whole
lot to do with it. But make no mistake about it, it's a red state issue, not a
a city one. So there you go. Quick attempt to break it down. You're going to encounter
more and more talking points like this, because at this point, a lot of people in the conservative
movement that are thought leaders, they've realized that following Trump and his
culture war nonsense, it's not going to win them elections. So they're going to
revert back to the thing that has always worked for them. Getting middle-class
white folk to kick down at people of other colors so they're distracted so
those people above them can continue to fleece them and trick them and
and manipulate them.
Stop kicking down.
Punch up.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}