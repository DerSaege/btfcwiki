---
title: Let's talk about infrastructure troubles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=178D4tP5kHM) |
| Published | 2022/12/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Infrastructure attacks in the US are increasing, but conventional wisdom suggests they won't succeed.
- Right-wing groups are suspected, but the true intent is still unclear.
- The US isn't under occupation, making typical attack objectives unlikely to succeed.
- Attacks aim to provoke a security clampdown, create disillusionment with the ruling party, or cause a reset.
- The US's robust control systems and lack of popular support make these objectives unachievable.
- Despite potential chaos and disruption, the attacks are unlikely to achieve their goals.
- Individuals should prepare for potential infrastructure disruptions in their areas.
- Suggestions include getting a generator, water purification tools, battery backups, and power inverters.
- Law enforcement's response will determine the duration of these attacks.
- Despite potential chaos, the attacks are not likely to succeed in their objectives.

### Quotes

- "It won't work."
- "The conditions necessary for this to work don't exist in the United States."
- "The chaos, the inconvenience, all of that will be there. But the goals, the conditions are not set for that."

### Oneliner

Infrastructure attacks in the US are on the rise, but the unique conditions make success unlikely; prepare for potential disruptions, but know that the chaos won't achieve its goals.

### Audience

Community members

### On-the-ground actions from transcript

- Stock up on essentials like water purification tools and battery backups (suggested)
- Get a generator or a power inverter for your car to prepare for potential infrastructure disruptions (suggested)
- Be prepared for chaos but understand that the attacks are unlikely to succeed in their goals (exemplified)

### Whats missing in summary

The full transcript provides a detailed explanation of why typical infrastructure attack objectives are unlikely to succeed in the United States.

### Tags

#Infrastructure #US #Security #Preparedness #CommunitySafety


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today we're going to talk about infrastructure
in the United States and what's going on,
what you can expect in the future.
This is basically going to be a good news sandwich, which
means a piece of bad news, a piece of good news,
and then a piece of bad news.
But we're going to kind of go through it,
answer questions that have popped up,
and talk about what's going on, why they're doing it,
and what you can do.
OK, so starting with the base of the sandwich,
the bad news here.
Hits on US infrastructure or attempts on US infrastructure
are through the roof.
There's a lot of them.
And it can be expected to continue.
It's probably going to continue.
Now, conventional wisdom says that these are
right-wing groups doing this.
We don't know that yet.
But at the time, right now, conventional wisdom
does actually appear to be real wisdom.
That is the way things look.
Now, there are questions about whether or not
the T word is applicable here.
And we'll find that out later.
That's a matter of intent.
And that won't really be decided until we find out
exactly what's going on.
But given the number of attempts,
we can expect it to continue.
That's bad news.
Good news, it won't work.
It won't work.
One of the questions that keeps popping up
is, why are they doing this?
And the answer is one that leftists watching this channel
will love.
They need to read some theory.
What they're doing is mimicking something they've seen overseas.
They're mimicking that without understanding
why it's done overseas in those situations
and how the dynamics work.
The good news here is that the conditions necessary for this
to work don't exist in the United States.
There are three main reasons this occurs overseas.
There are three objectives that people
are attempting to accomplish when they do this overseas,
typically in a country that is under occupation.
The first reason is to provoke a security clampdown.
This is something that we've talked about on the channel
before, but it's been a while.
It may seem counterintuitive, but irregular forces
will often hit infrastructure in an attempt
to get the occupation force to engage in a security clampdown,
clampdown on everybody to keep things safe and secure.
During that process, the civilian populace
gets caught up in the middle of it,
and their movements are restricted.
There's no way to get out of it.
Their movements are restricted.
There's all kind of checkpoints, and they
see the face of the occupation, and they don't like it.
This often generates sympathy for the irregular forces.
The problem with this right now is that, I mean,
the obvious one, the US isn't under occupation.
So that doesn't really work.
Another one is that the United States
has amazing control systems as far as keeping order,
keeping the status quo.
The United States has control systems
unlike any other country on the planet.
The other thing to keep in mind is that the US literally
wrote the manual on not engaging in a security clampdown.
It was the US that really determined how this worked
and developed an entire doctrine to make sure it didn't work.
Even if they don't read the manual,
like a previous administration, in this instance,
there's no unified opposition.
So the security clampdown happens,
but it's conducted by agency security apparatus that
is spread across local, state, federal.
There's no unified opposition to rally people against.
So they're just annoyed.
That identifiable agency, like the face of the occupation,
the occupation forces, it just doesn't
exist in the United States because the US
isn't under occupation.
So that objective, that reason for doing it,
it's not going to work.
It will not work.
The second reason for doing it is
to cause disillusionment with the political party that's
in power.
Now, for this to work, the political wing
of the irregular forces has to constantly blame
the party in power for the actions of the irregular forces
that they're allied with.
Say it's their fault this is happening.
It's their fault this is happening.
The problem with that in the US, assuming
that the information that we have about it being right wing
groups is accurate, the right wing political party
is so intertwined with what would be considered
the irregular forces, it won't work.
There's no way for these actions to be blamed on the party
in power because at a glance, it's
going to look like the right wing party in the United States
is a party to the hits on the infrastructure.
So that one won't work either.
Again, this is also something that typically occurs
in a country that is under occupation.
The third reason is a reset.
That's where everything gets knocked out.
And at that point, the irregular forces mount an offensive.
For that to work, they need popular support,
which they don't have.
They also probably can't do it because this
can't be done on a local level or even a regional level.
Because the United States is so large, doing it nationwide,
that takes coordination that they don't have.
And that's not, I mean, that takes coordination.
I don't know that the US Army could do it.
The US is huge.
Doing it on the scale that would be required for the reset
theory to be at play, that's not going to happen.
So at least now you understand why they might be doing it.
The good news is these theories can't
be applied in the United States.
The conditions are not set for any of this to work.
So there's the good news.
The top piece of bread of the bad news
there is that this is a lot like January 6th.
If you were watching back then or following me on Twitter
when I was live tweeting, I don't know,
within the first hour, I was saying,
this isn't going to work.
This is not going to work.
This is going to be an unmitigated failure,
so on and so forth.
And that's what happened.
But it didn't stop the chaos and destruction.
That still happened.
It just didn't achieve their goal.
They can still cause a lot of chaos by doing this.
It just won't achieve their goal.
So what does that mean for you?
It means be prepared for the infrastructure
to go down in your area.
Do what you can.
And everybody has different levels of means.
So everybody's preparations for that
will be a little bit different.
If you have the money, yeah, get a generator.
Get the good water purification stuff, so on and so forth.
If you don't, do what you can.
Battery backups for your phone, a sawyer.
That's a water purification thing.
If you have a little bit of money,
maybe look at those things that they used to jump cars.
A lot of those have outlets on them now.
Those aren't incredibly expensive,
but they have the power to run a space heater for a bit.
So there are things that you can do to try to get ready.
Because right now, it does appear
that there will probably be more.
And we don't know where they're going to happen or when.
So those little battery backups for your phone,
they're like $6 at big box stores.
Maybe get one of those so you can contact people.
What you can do to supplement and have
your own source of electricity, probably useful.
I would also look into some water purification stuff.
And that's not because I think they would
go after the water supply.
It has to do with if power goes out for a long time,
water also doesn't work.
Those would be my main ideas.
Another thing, relatively inexpensive,
is to get a power inverter for your car.
So basically, you can turn your car into a generator.
It plugs into the lighter socket.
So that's where we're at with that.
What happens from here depends on how quickly
the security apparatus in the United States
really swings into action on this.
Because it does appear that we're hitting a spike.
And it's going to be law enforcement's actions that
determine how long this lasts.
All you can do is be ready for it.
And remember that it's not going to work.
It won't work.
The chaos, the inconvenience, all of that will be there.
But the goals, the conditions are not set for that.
It's not going to succeed.
It is a lot like January 6th in that regard.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}