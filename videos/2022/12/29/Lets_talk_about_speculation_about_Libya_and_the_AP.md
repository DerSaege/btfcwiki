---
title: Let's talk about speculation about Libya and the AP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YH3Q2v-SuG4) |
| Published | 2022/12/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Released a video about events in Libya involving speculation about the government in Tripoli capturing someone and turning them over to the United States for oil investment.
- Received criticism for the speculation in the video.
- Associated Press confirmed that the government in Tripoli did snatch the individual and turn them over to the United States to curry favor, although the motive related to oil investment is not confirmed.
- People are upset about the situation due to concerns about the legality of the extradition, given Libya's two competing governments.
- US intelligence appears indifferent to the questionable nature of the extradition as long as they got the person they wanted.
- The individual was snatched by a group akin to a neighborhood watch before being in US custody.
- Analysts agree that the extradition was to curry favor with Western governments.
- Anticipates the story gaining significant traction in the headlines, especially once a trial commences.

### Quotes

- "US intelligence doesn't care how the sausage was made."
- "He was snatched quite some time before he was in US custody."
- "This will be a huge thing, especially once a trial starts."

### Oneliner

Beau clarifies speculation about Libya events, confirms government involvement, and anticipates a significant story gaining headlines.

### Audience

Journalists, Activists, World Citizens

### On-the-ground actions from transcript

- Contact local news outlets to raise awareness about the situation (suggested).
- Join organizations advocating for transparency in international dealings (implied).

### Whats missing in summary

Insight into the potential implications of this story on international relations and human rights.

### Tags

#Libya #Government #USIntelligence #Extradition #InternationalRelations


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to provide an update on a video that took place a while back.
I'm not sure when this video is going to go out.
At time of filming, it has been five days since I released a video about some events
in North Africa, in Libya.
I took a little bit of heat for that video because I included speculation that it was
the government in Tripoli who had captured Masoud and then turned him over to the United
States and that they were doing this in exchange for kind of currying favor with Western powers
in hopes of getting more oil investment.
Because right now Libya should be making a ton of money and they're not because they
have two competing governments and it's just not working out for them.
I took a lot of heat over that speculation.
Mainly some people suggesting I shouldn't make it at all and also a whole lot of people
suggesting that there's no way that a government in Libya would turn somebody over to the United
States.
Okay, so it's been five days, the Associated Press has provided more coverage.
They have done some investigation and there are some things that I would like to clarify.
So what we're going to do is we're going to separate the speculation that's in that video
and compare it to what the Associated Press has confirmed.
The AP has confirmed that it was the government in Tripoli who snatched him and turned him
over to the United States in order to curry favor.
The only thing that the AP hasn't confirmed yet is that it had to specifically do with
oil investment.
That's it.
Everything else has now been confirmed by the Associated Press.
So now that we know that that's the case, there are people who are upset about how it
was done because there is the sentiment that it wasn't a lawful extradition because of
the situation Libya is in right now.
There's two competing governments basically.
And the statement has been made that US intelligence doesn't care how the sausage was made.
They don't understand that it was bad and all they care about is that they got their
person they don't care how about how the sausage was made.
Yeah I'll go ahead and tell you they don't.
They do not care how the sausage was made.
They care about how that thing on that plane was made.
And by this guy's own statements, he made it.
That's what they care about.
Was this a lawful extradition?
I mean yeah it's questionable.
Make no mistake about it.
But it was done through state actors.
Now the Libyan government may have used, I don't want to use the M word, let's just say
it's a neighborhood watch that might have snatched him.
But that is exactly what occurred.
He was snatched quite some time before he was in US custody.
He was handed over to US officials who flew him back and all of the analysts that they
have spoken to agree it is to curry favor with Western governments and just kind of
show hey we're the reasonable ones.
That's what's going on and that is what occurred.
And this is going to be a story that is eventually going to be all over the headlines.
This will be a huge thing, especially once a trial starts.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}