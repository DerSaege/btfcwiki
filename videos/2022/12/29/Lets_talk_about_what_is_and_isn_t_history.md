---
title: Let's talk about what is and isn't history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1ZGjjq-DmWE) |
| Published | 2022/12/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the difference between rewriting US history taught in schools, specifically about race relations and slavery, and removing Confederate statues.
- He points out that statues are not historically relevant; historians do not trust the inscriptions on them but use them to understand the values and messages conveyed when they were made.
- The statues honoring Confederate figures were mainly erected during the 1920s and 1960s to convey a message of subjugation and oppression, not historical accuracy.
- Beau explains that the Confederates themselves emphasized slavery as a central issue, not states' rights or other myths associated with the Civil War.
- He gives examples of historically inaccurate statues, like one commemorating General Lee's actions at Antietam when Lee was actually injured and not on horseback.
- Beau stresses that the message conveyed by Civil War statues is one of continued oppression, not historical accuracy or heroism.
- He contrasts the mythological elements associated with statues like the Statue of Liberty, which symbolize aspirational ideals rather than oppressive histories.

### Quotes

- "Statues aren't history. They're rocks meant to mythologize something."
- "They're not part of history in the way that people look at them. They weren't made by people right after the Civil War. They're not history, they're mythology."
- "The Confederates themselves said that. Anything else is just a lie."

### Oneliner

Beau explains the inaccurate historical representation of Confederate statues and their role in mythologizing oppression, contrasting them with aspirational symbols like the Statue of Liberty.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Educate others on the historical inaccuracies and mythological nature of Confederate statues (implied).
- Support initiatives to remove or relocate Confederate statues to more appropriate spaces like museums (implied).

### Whats missing in summary

Beau's passionate delivery and detailed explanations on the mythological aspects of statues and the importance of understanding their historical context could be best experienced by watching the full video.

### Tags

#History #ConfederateStatues #CivilWar #Oppression #Mythology #HistoricalAccuracy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about history
and what is and is not part of history.
And we're gonna talk about mythology.
And we're going to talk about statues and a message.
And when I read this,
understand that kind of the first time,
I think this argument is actually being made in good faith
this time.
Normally when I get a message like this,
it's very clearly in bad faith.
In this case, I think it's somebody that doesn't have
the bits of pieces of information that they need.
And I think that for once,
this video may actually change somebody's mind.
Normally when I'm doing these videos,
it's more about giving everybody else the tools
to deal with this type of argument when they run into it.
But this one's a little different.
Okay.
Beau, I'm confused.
What is the difference between rewriting US history
as taught in schools,
specifically race relations and slavery,
and removing Confederate statues?
Both are fundamentally revising history,
though one is opposed and the other not.
Why can we not accept slavery happened,
it was a mistake and use the lesson of it being wrong
to guide relations with other groups of people
who tend to be treated as less than equal.
And this is what I mean by thinking it's in good faith.
Like the person is admitting that even today,
things aren't right.
Yes, the Confederate statues honor those who fought
for the South, yet that was only a small portion
of their lives.
They too fought for their beliefs and their way of life.
Some were brilliant tacticians
and had their knowledge and military skills been used
to fight a foreign foe, they would be considered heroes.
Why can we not look at them as men who,
though they were on the wrong side
of the conflict, still had worth?
It seems that by not accepting the past
and learning from it, we will be doomed to repeat it.
Okay.
So the fundamental piece of information that's missing here
is that statues aren't history.
They're not.
They're not historically relevant.
Not in the way that a lot of people believe.
Historians do not find a statue
and trust the inscription on it.
To a historian, the historic value of a statue
is not what's on there.
They don't believe the story.
It's used to understand the values
and the message that was trying to be conveyed
at the time the statue was made,
which is way after the actual events,
particularly with the Civil War.
When it comes to the Civil War,
these statues came in two waves, really.
The plaques and statues, they tend to have surfaced
in the 1920s and then again in the 1960s.
The idea that was being conveyed at that time,
well, it was pretty simple.
The 1920s saw a resurgence of the people
who wear white sheets,
and the 1960s was the Civil Rights Movement.
Both times, the idea being conveyed by these statues
was you uppity folk better stay in your place.
That's why people don't want them around,
because they don't have actual historic value.
They're not part of history
in the way that people look at them.
They weren't made by people right after the Civil War.
They're not history, they're mythology.
They're trying to change the perception of the war
away from what it really was.
They want to include the mythology of,
oh, it was about states' rights and all of that.
No, it wasn't.
And you don't have to argue this.
People will often respond with, right to do what?
And the answer, of course, is slavery,
but you don't even have to go that far.
The Confederates thought they were founding a new country.
They have founding documents.
If you read them, generally speaking,
you find the word slavery in the first paragraph.
When they talked about interacting with each other,
they said other slave-holding states,
because that was the uniting thing about them.
You have the Cornerstone speech.
Mississippi went all in on it
and said that black people had to be slaves
because white people couldn't handle the heat.
I'm not joking.
That's actually part of what they perceived
to be the founding documents of their country.
I have a whole video on this somewhere.
I'll try to find it and put it down below.
It's trying to reshape that history.
That's what was going on in the 1920s and the 1960s
when these statues were made.
It's mythology.
They don't have historical relevance
in the way that people think they do.
They're not history.
They're rocks meant to mythologize something.
And then when you're talking about the little bits of history
that they actually convey, they're often wrong.
They are often wrong.
I'm not sure if it's still there.
I would hope it's been removed by now.
But there's a statue of Lee on the road to Antietam.
And there he is on traveler, holding the reins
and his filled glasses in one hand and all of this stuff.
And it's to commemorate what he did at that battle.
The problem with that, of course,
is that Lee really didn't do anything at the place
where the statue exists.
He did pass by it, but he wasn't on his horse
because the horse had thrown him.
And both of his wrists were busted.
He was carried there in a wagon.
And that just goes to show that it's about mythologizing it.
It's not about history.
And that is not the only instance
of something like that.
It's incredibly, incredibly common.
Another prime example is a plaque
that reads, General Stonewall Jackson,
by General Lee's request, on this corner,
planned the Battle of Fredericksburg, November 27,
1862.
OK?
Literally nothing on that is right.
None of it is accurate.
On that date, I want to say Jackson was in Gordonsville
encamped there.
And he wasn't planning anything.
He was celebrating the birth of his daughter.
It's just made up.
It's not real.
So even the little bit of history
that is included in a lot of these monuments,
it's not even accurate.
There's a whole other one I want to say.
I want to say it's also at Antietam that
deals with crossing a bridge with pontoons.
There's tons of it.
They're not accurate historically.
It's about mythologizing something.
There's a whole genre of commentary
by battlefield historians making fun
of the monuments on battlefields based on where they were placed
or what they said happened in comparison to what actually
occurred.
Because the statues aren't history.
They're mythology.
They're not even designed to be historic.
It's all about trying to downplay what the Civil War was
really about, which was it was slavery.
There's no other way to look at it.
The Confederates themselves said that.
Anything else is just a lie.
And it's not just true for statues
related to the Civil War.
It's all mythology.
What's the most famous statue in the United States?
A statue that if you showed it to somebody from overseas,
they would be like, America.
The Statue of Liberty, right?
Yeah.
The idea behind it is that the United States
was going to be a bastion of freedom, land of the free
and home of the chain.
Even today in this message that was sent, it's not true.
The other element of mythology associated with it
is that the United States is just so welcoming to immigrants.
It's not.
Historically, it hasn't been.
The anti-immigrant sentiment that is so pervasive today,
it's been here.
It's just different groups have been targeted with it
the entire time.
That statue is the mythology, the mythology
we're supposed to live up to.
That's why it's viewed as so important.
That's why it's a symbol of the United States.
It's something to aspire to, like most statues.
The message conveyed by the Civil War statues
isn't one that people should be aspiring to.
It's one of subjugation and oppression.
It's not history.
That's why the two things aren't seen as the same.
It's because they're different.
They're historically inaccurate.
They aren't historic in the sense
that they were made during the period of the Civil War,
and they were made to convey the idea that continued oppression
was good.
That's why.
And believe me, the few examples I cited of the statues
being historically inaccurate.
And keep in mind, most inscriptions
are only like a paragraph long, and they still
manage to be wildly inaccurate just in that short space.
So they don't have historic value the way
that people like to pretend they do.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}