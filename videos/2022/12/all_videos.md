# All videos from December, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-12-31: Let's talk about what the US is getting out of Ukraine.... (<a href="https://youtube.com/watch?v=kdcf8g9gBvM">watch</a> || <a href="/videos/2022/12/31/Lets_talk_about_what_the_US_is_getting_out_of_Ukraine">transcript &amp; editable summary</a>)

Exploring the strategic benefits for the US in supporting Ukraine while acknowledging the human cost.

</summary>

"Nations don't have friends. They don't have morality. They don't have ideology. They have interests."
"The last time the United States engaged in a Cold War with Russia and engaged in the military expenditures that go along with this, it was $13 trillion."
"When somebody is asking this question, 'what are we getting out of it?' I understand it from a two sizes, two small heart."
"In real life, people do matter."
"All of this money is spent on weapons because there are people who are having rockets rained down on their heads."

### AI summary (High error rate! Edit errors on video page)

Addressing the US benefits from supporting Ukraine and the questions arising from right-wing talking points about the money spent.
Nations prioritize interests over friendships, morality, or ideology.
The economic power Ukraine could become after the war and its alignment with Europe benefiting the US and NATO.
Investing in Ukraine to prevent it from becoming a Russian colony and helping keep Russia in check.
The US benefits from buying a degraded Russian military, simplifying its international poker game with China now as the main competitor.
The $50 billion investment in Ukraine is effective in knocking Russia out of the high stakes poker table.
This support helps rebuild the US's reputation on the international scene and frame the contest as democracy versus authoritarianism.
Ukrainians want to remain Ukrainian, not become Russian, which is often overlooked in foreign policy questions.
Despite the strategic benefits, it's vital to always bring back the human aspect of conflicts and the impact on people.
Overall, supporting Ukraine comes with multiple strategic and geopolitical advantages for the US.

Actions:

for foreign policy enthusiasts,
Support organizations aiding Ukrainian civilians (suggested)
Advocate for diplomatic solutions to conflicts (implied)
</details>
<details>
<summary>
2022-12-31: Let's talk about cities, counties, and rates.... (<a href="https://youtube.com/watch?v=LCEqjXI1SLk">watch</a> || <a href="/videos/2022/12/31/Lets_talk_about_cities_counties_and_rates">transcript &amp; editable summary</a>)

Beau explains the importance of methodology in analyzing violent crime rates, advocating for per capita rates and county-level data over raw numbers to gain a nuanced understanding.

</summary>

"You need to look at per capita rates, and you need to look at it at the county level."
"Understand what matters is the methodology."
"Here in Florida, as an example, the most violent county, it's not Miami-Dade, it's Levy."
"It's also better to look at a grouping of years, rather than just one year in particular."
"When you're looking at those lists, most dangerous cities, understand what matters is the methodology."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of processing information and understanding methodology by examining three lists from different years.
He explains that the purpose is not to establish facts but to reveal a pattern.
Beau uses an analogy of a man with a map in a movie to illustrate the importance of having the right information.
He challenges the common belief about the most dangerous cities by presenting a list with a lower population threshold, showing different results.
Beau further contrasts lists of cities with age-adjusted county data to provide a more accurate representation of violent crime rates.
Different lists from liberal and police organizations show similarities in rankings but also regional divides and emphasis on low-income areas.
Beau stresses the significance of methodology in determining which areas are labeled as the most dangerous and the impact of population thresholds.
He encourages looking at data at the county level, mentioning examples like Levy County in Florida, to gain a more nuanced understanding.
Beau criticizes the use of raw numbers in reporting violent crimes, advocating for per capita rates and county-level analysis for better insights.
In conclusion, Beau suggests considering multiple years of data and focusing on rural areas or small towns to truly understand violent crime rates.

Actions:

for data analysts, crime researchers,
Analyze crime data at the county level to understand regional patterns and prioritize interventions (suggested).
Advocate for the use of per capita rates in reporting violent crime statistics for more accurate assessments (implied).
</details>
<details>
<summary>
2022-12-31: Let's talk about Raffensperger's testimony.... (<a href="https://youtube.com/watch?v=_L0ogdRLk-w">watch</a> || <a href="/videos/2022/12/31/Lets_talk_about_Raffensperger_s_testimony">transcript &amp; editable summary</a>)

Raffensperger's testimony on Trump's alleged threats and the DOJ's actions determine Georgia's future actions amidst ongoing investigations and historical significance.

</summary>

"He felt it was a threat of violence. That's big. That's huge for Georgia."
"People were spun up to just believing the lies that were told to them."
"Georgia is still moving ahead with their investigation."
"I'm fairly certain that the passage I just read and very similar testimony will be featuring pretty prominently."
"It absolutely was one of those hinge points in American history."

### AI summary (High error rate! Edit errors on video page)

Explaining the testimony from Raffensperger regarding Trump's phone call asking to find votes.
Raffensperger felt that Trump's message of "no criminality" was dangerous and could imply consequences.
Raffensperger believed Trump's message was a threat of violence, as stated in his book and under oath.
Testimony suggests the White House Counsel's office shared concerns about the phone call's implications.
The link between Trump's actions and the events of January 6th is drawn, with people being influenced by lies.
Georgia's investigation is ongoing, indicating potential future actions based on the testimony.
Raffensperger's feeling personally threatened could lead to legal implications in Georgia.
The Department of Justice's actions will determine if history repeats itself or rhymes regarding the events.
The investigation in Georgia may lead to significant developments, despite limited information disclosure.
Beau concludes with the expectation of Raffensperger's testimony playing a prominent role in upcoming proceedings.

Actions:

for georgia investigators,
Support ongoing investigations in Georgia by cooperating with authorities (implied).
Stay informed about the developments in the case and advocate for justice (implied).
</details>
<details>
<summary>
2022-12-30: Let's talk about what Trump thinks of his followers.... (<a href="https://youtube.com/watch?v=_q6y4jfCWpo">watch</a> || <a href="/videos/2022/12/30/Lets_talk_about_what_Trump_thinks_of_his_followers">transcript &amp; editable summary</a>)

Trump's deceptive narrative on the withdrawn subpoena reveals his low opinion of his supporters, aiming to manipulate their perception and loyalty.

</summary>

"He openly indicates that he believes his supporters are very unintelligent."
"My supporters are so dumb, they will believe this."

### AI summary (High error rate! Edit errors on video page)

Trump's recent statement illustrates his low opinion of his supporters.
The January 6th committee withdrew the subpoena for Trump.
Trump twisted the narrative, claiming the withdrawal indicates he did nothing wrong.
In reality, the committee recommended charges against Trump.
Trump believes his supporters are not intelligent enough to see through his deception.
Trump's tactic aims to manipulate and mislead his followers.
Beau suggests using this as a talking point to raise doubt among Trump supporters.
Trump's statement reveals his disregard for the intelligence of his followers.
This moment exposes Trump's condescending view of his base.
Beau hints at using this incident to challenge the blind loyalty of Trump supporters.

Actions:

for concerned citizens,
Challenge misinformation among Trump supporters (suggested)
Engage in constructive dialogues with family members (suggested)
</details>
<details>
<summary>
2022-12-30: Let's talk about a new Russian talking point about Ukraine.... (<a href="https://youtube.com/watch?v=smWaOz5r6CE">watch</a> || <a href="/videos/2022/12/30/Lets_talk_about_a_new_Russian_talking_point_about_Ukraine">transcript &amp; editable summary</a>)

Beau addresses the flawed idea that stopping Western assistance to Ukraine will benefit peace, debunking it by explaining Russia's lack of troops for a successful occupation and how continued assistance actually speeds up the end of the war.

</summary>

"Even if you want Russia to complete its imperialist endeavors for the sake of their capitalist oligarchs, understand that it's worse for the people in Ukraine."
"It is absolutely Russian propaganda."
"The Western assistance to Ukraine is speeding the end of the war."

### AI summary (High error rate! Edit errors on video page)

Addressing a new talking point regarding Ukraine and Western assistance.
Mention of inorganic talking points with similar typos being pushed.
Exploring the idea that stopping Western assistance to Ukraine might lead to Russia taking over.
Expresses skepticism about Russia making significant gains if assistance stops.
Pointing out that if Russia were to take over, the difficult part lies in the occupation phase.
Russia's lack of adequate troops for occupying Ukraine despite initial numbers.
Explaining the need for a much larger number of troops for successful occupation.
Detailing the duration and challenges of the occupation phase.
Asserting that without the required troop numbers, any Russian occupation attempt will prolong the war.
Criticizing the talking point against Western assistance as Russian propaganda.

Actions:

for global citizens, activists,
Support organizations providing aid to Ukraine (suggested)
Stay informed on the conflict in Ukraine and counter misinformation (suggested)
Advocate for diplomatic solutions and peace in Ukraine (implied)
</details>
<details>
<summary>
2022-12-30: Let's talk about Santos shenanigans.... (<a href="https://youtube.com/watch?v=xaOzV3qZ2jo">watch</a> || <a href="/videos/2022/12/30/Lets_talk_about_Santos_shenanigans">transcript &amp; editable summary</a>)

Beau dives into controversies surrounding a recent Congressional election winner, Santos, prompting calls for accountability and introducing potential legislation to address misrepresentations in candidacy filings.

</summary>

"Stop Another Non-Truthful Office Seeker."
"You're limiting Congresspeople's ability to lie to the American public."
"There's probably more to it."

### AI summary (High error rate! Edit errors on video page)

Talking about controversies in New York and DC, originating from New York, particularly regarding a person named Santos who recently won a Congressional election.
Questions have been raised about Santos' biographical data, including education, work experience, and heritage.
Calls for Santos to step down have emerged, and prosecutors are reportedly looking into the situation.
Congressperson Richie Torres is introducing an act called the Stop Another Non-Truthful Office Seeker Act in response to the Santos controversy.
The act aims to require candidates to provide accurate information about their educational background, military service, and employment history under oath when filing for candidacy.
There are doubts about the act passing due to potentially limiting Congresspeople's ability to lie to the public.
Speculation that prosecutors might be investigating more than just misrepresentations about Santos' background, potentially related to campaign finance.
Uncertainty about the outcome of the situation and the potential implications beyond the misrepresented biographical data.

Actions:

for voters, legislators,
Contact Congressperson Richie Torres to express support for the Stop Another Non-Truthful Office Seeker Act (suggested).
Stay informed about the developments in the Santos situation and potential legislative changes (implied).
</details>
<details>
<summary>
2022-12-29: Let's talk about what is and isn't history.... (<a href="https://youtube.com/watch?v=1ZGjjq-DmWE">watch</a> || <a href="/videos/2022/12/29/Lets_talk_about_what_is_and_isn_t_history">transcript &amp; editable summary</a>)

Beau explains the inaccurate historical representation of Confederate statues and their role in mythologizing oppression, contrasting them with aspirational symbols like the Statue of Liberty.

</summary>

"Statues aren't history. They're rocks meant to mythologize something."
"They're not part of history in the way that people look at them. They weren't made by people right after the Civil War. They're not history, they're mythology."
"The Confederates themselves said that. Anything else is just a lie."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the difference between rewriting US history taught in schools, specifically about race relations and slavery, and removing Confederate statues.
He points out that statues are not historically relevant; historians do not trust the inscriptions on them but use them to understand the values and messages conveyed when they were made.
The statues honoring Confederate figures were mainly erected during the 1920s and 1960s to convey a message of subjugation and oppression, not historical accuracy.
Beau explains that the Confederates themselves emphasized slavery as a central issue, not states' rights or other myths associated with the Civil War.
He gives examples of historically inaccurate statues, like one commemorating General Lee's actions at Antietam when Lee was actually injured and not on horseback.
Beau stresses that the message conveyed by Civil War statues is one of continued oppression, not historical accuracy or heroism.
He contrasts the mythological elements associated with statues like the Statue of Liberty, which symbolize aspirational ideals rather than oppressive histories.

Actions:

for history enthusiasts, activists,
Educate others on the historical inaccuracies and mythological nature of Confederate statues (implied).
Support initiatives to remove or relocate Confederate statues to more appropriate spaces like museums (implied).
</details>
<details>
<summary>
2022-12-29: Let's talk about speculation about Libya and the AP.... (<a href="https://youtube.com/watch?v=YH3Q2v-SuG4">watch</a> || <a href="/videos/2022/12/29/Lets_talk_about_speculation_about_Libya_and_the_AP">transcript &amp; editable summary</a>)

Beau clarifies speculation about Libya events, confirms government involvement, and anticipates a significant story gaining headlines.

</summary>

"US intelligence doesn't care how the sausage was made."
"He was snatched quite some time before he was in US custody."
"This will be a huge thing, especially once a trial starts."

### AI summary (High error rate! Edit errors on video page)

Released a video about events in Libya involving speculation about the government in Tripoli capturing someone and turning them over to the United States for oil investment.
Received criticism for the speculation in the video.
Associated Press confirmed that the government in Tripoli did snatch the individual and turn them over to the United States to curry favor, although the motive related to oil investment is not confirmed.
People are upset about the situation due to concerns about the legality of the extradition, given Libya's two competing governments.
US intelligence appears indifferent to the questionable nature of the extradition as long as they got the person they wanted.
The individual was snatched by a group akin to a neighborhood watch before being in US custody.
Analysts agree that the extradition was to curry favor with Western governments.
Anticipates the story gaining significant traction in the headlines, especially once a trial commences.

Actions:

for journalists, activists, world citizens,
Contact local news outlets to raise awareness about the situation (suggested).
Join organizations advocating for transparency in international dealings (implied).
</details>
<details>
<summary>
2022-12-29: Let's talk about infrastructure troubles.... (<a href="https://youtube.com/watch?v=178D4tP5kHM">watch</a> || <a href="/videos/2022/12/29/Lets_talk_about_infrastructure_troubles">transcript &amp; editable summary</a>)

Infrastructure attacks in the US are on the rise, but the unique conditions make success unlikely; prepare for potential disruptions, but know that the chaos won't achieve its goals.

</summary>

"It won't work."
"The conditions necessary for this to work don't exist in the United States."
"The chaos, the inconvenience, all of that will be there. But the goals, the conditions are not set for that."

### AI summary (High error rate! Edit errors on video page)

Infrastructure attacks in the US are increasing, but conventional wisdom suggests they won't succeed.
Right-wing groups are suspected, but the true intent is still unclear.
The US isn't under occupation, making typical attack objectives unlikely to succeed.
Attacks aim to provoke a security clampdown, create disillusionment with the ruling party, or cause a reset.
The US's robust control systems and lack of popular support make these objectives unachievable.
Despite potential chaos and disruption, the attacks are unlikely to achieve their goals.
Individuals should prepare for potential infrastructure disruptions in their areas.
Suggestions include getting a generator, water purification tools, battery backups, and power inverters.
Law enforcement's response will determine the duration of these attacks.
Despite potential chaos, the attacks are not likely to succeed in their objectives.

Actions:

for community members,
Stock up on essentials like water purification tools and battery backups (suggested)
Get a generator or a power inverter for your car to prepare for potential infrastructure disruptions (suggested)
Be prepared for chaos but understand that the attacks are unlikely to succeed in their goals (exemplified)
</details>
<details>
<summary>
2022-12-29: Let's talk about bias and narratives.... (<a href="https://youtube.com/watch?v=wyphBDP-PSc">watch</a> || <a href="/videos/2022/12/29/Lets_talk_about_bias_and_narratives">transcript &amp; editable summary</a>)

Beau breaks down biases, cherry-picked crime stats, and misconceptions about gun crimes, urging to punch up against systemic issues instead of kicking down at marginalized communities.

</summary>

"Stop kicking down. Punch up."
"You really think it's not white people that are out there ripping off catalytic converters? I have a hard time believing that."
"It's a red state problem."

### AI summary (High error rate! Edit errors on video page)

Addresses information consumption and recognizing cherry-picked information shaping narratives.
Believes biases can lead to bigotry when assigning crimes to specific demographics.
Critiques a message suggesting discussing looting, carjackings, and gun crimes in democratic-controlled cities.
Breaks down the flaws in cherry-picked crime statistics and biases related to demographics.
Compares smash and grabs to construction site theft, pointing out selective reporting.
Analyzes the demographics behind catalytic converter thefts and the areas with the highest increases.
Debunks the idea of gun crimes being solely a problem in Democrat-run cities.
Argues that gun crime is more prevalent in red states, contrary to certain narratives.
Attributes the root cause of high gun crime rates to cultural factors and misinformation.
Encourages punching up against systemic issues rather than kicking down at marginalized groups.

Actions:

for information consumers,
Challenge biases and stereotypes in your community (implied).
Counter misinformation and cherry-picked data with facts and logic (implied).
</details>
<details>
<summary>
2022-12-28: Let's talk about what happens if nothing happens.... (<a href="https://youtube.com/watch?v=Mo9ikbQ0drI">watch</a> || <a href="/videos/2022/12/28/Lets_talk_about_what_happens_if_nothing_happens">transcript &amp; editable summary</a>)

Beau explains the long-term risks of DOJ inaction, warning about emboldened officials undermining elections at local and state levels, ultimately posing a threat to democracy in the future.

</summary>

"The real danger here isn't somebody attempting to duplicate what Trump did."
"A newer crop of politicians emboldened by a total lack of accountability that start doing it themselves at a local level."
"The United States is not coup-proof."
"It just can't be amateur hour, which is what the sixth was."
"When it comes to the downstream effects from a lack of accountability, it might be so far removed that those who are tasked with providing accountability today, don't see it."

### AI summary (High error rate! Edit errors on video page)

Explains the potential consequences if the Department of Justice (DOJ) does not take any action following referrals.
Suggests that the referrals may not impact DOJ's decision-making process and could potentially cause delays.
Addresses concerns about whether inaction by the DOJ may embolden future Republican presidents to attempt similar actions.
Points out that the real danger lies at the local and state levels, where officials could become emboldened to undermine elections.
Warns about the risks of local and state officials casting doubt on elections, which could eventually lead to similar actions at the federal level.
Emphasizes that the true threat is not immediate but could manifest over the next 10 to 15 years.
Stresses the importance of accountability and the potential for networks of like-minded individuals to carry out undemocratic actions.
Concludes by raising concerns about the downstream effects of a lack of accountability in the political system.

Actions:

for politically engaged citizens,
Monitor local and state officials for any attempts to undermine election integrity (implied)
Advocate for accountability measures in political systems (implied)
</details>
<details>
<summary>
2022-12-28: Let's talk about Hutchinson, theories, and fireplaces.... (<a href="https://youtube.com/watch?v=1nwPapcUcH4">watch</a> || <a href="/videos/2022/12/28/Lets_talk_about_Hutchinson_theories_and_fireplaces">transcript &amp; editable summary</a>)

Hutchinson revealed Meadows burning sensitive documents, while conspiracy theories were briefed in the White House, debunking their credibility and hinting at a manipulative agenda.

</summary>

"Those briefings might be the best evidence to help break people free from it and make them really start to question what they have come to believe."
"Trump White House possibly entertained conspiracy theories to energize their base, as suggested by the need for briefings."

### AI summary (High error rate! Edit errors on video page)

Hutchinson revealed Meadows was burning documents in the White House fireplace a couple of times a week.
The White House has burn bags for incinerating sensitive documents, but Meadows chose to burn them himself.
Trump administration staff used to piece together torn documents and burn the rest to comply with federal law.
Meadows likely burned documents that couldn't be seen by White House staffers going through classified information.
The documents burned were probably so sensitive that even attempting to piece them together for retention was risky.
Meadows burning documents multiple times a week indicates extreme secrecy and importance attached to them.
Conspiracy theories were brought into the White House by external sources like the "space laser lady" and Peter Navarro.
The fact that higher-ups needed to be briefed on conspiracy theories debunks the credibility of those theories.
Trump White House possibly entertained conspiracy theories to energize their base, as suggested by the need for briefings.
Briefings on conspiracy theories could serve as evidence to help individuals break free from harmful beliefs.

Actions:

for media consumers,
Contact media outlets to ensure coverage of the revelations from Hutchinson's testimony (suggested)
Engage in critical discourse to debunk conspiracy theories and encourage questioning of beliefs (implied)
</details>
<details>
<summary>
2022-12-28: Let's talk about George Santos and blame.... (<a href="https://youtube.com/watch?v=QU_vg5Uwx4k">watch</a> || <a href="/videos/2022/12/28/Lets_talk_about_George_Santos_and_blame">transcript &amp; editable summary</a>)

Exploring accountability for a deceitful candidate in New York reveals a concerning lack of trust in the Republican Party, as Democrats and the media face blame while the real issue lies within the GOP.

</summary>

"Both entities dropped the ball on this one."
"The Republican Party has lost the trust of the American voter to such an extreme."
"Trust me, there are a whole bunch of independent first-time voters who will never vote for the Republican Party again after this."
"The damage that the Republican Party is doing to itself right now will last decades."
"The Republican Party has become synonymous with trying to undermine democracy in any way."

### AI summary (High error rate! Edit errors on video page)

Exploring the accountability for George Santos' election win in New York, where he provided false information about his background.
Allegations range from padding his resume to misrepresenting his educational background, work history, and heritage.
Debate centers on blaming either the Democratic Party or the media for not uncovering Santos' fabrications during the election.
Beau argues that both the Democratic Party and the media are responsible for failing to thoroughly vet Santos.
The Republican Party running a deceitful candidate like Santos should be the focus of blame, but the blame game shifts between Democrats and the media.
Independents and Democrats question who to blame, with the Republican Party escaping scrutiny despite running the deceptive candidate.
Beau warns that the Republican Party's lack of accountability for running dishonest candidates could alienate independent and first-time voters.
The Republican Party's pattern of deception harms its credibility and trust with voters, especially independents.
The ongoing damage caused by the Republican Party's deceit may have long-lasting consequences for their reputation and voter base.
Beau underscores the importance of accountability and transparency in political parties to uphold democracy.

Actions:

for voters, independents,
Hold political parties accountable for the candidates they endorse (implied)
Encourage thorough vetting processes for all candidates, regardless of party affiliation (implied)
Support transparency and honesty in political campaigns (implied)
</details>
<details>
<summary>
2022-12-27: Let's talk about understanding social media influence operations.... (<a href="https://youtube.com/watch?v=cQcxHvAxrzQ">watch</a> || <a href="/videos/2022/12/27/Lets_talk_about_understanding_social_media_influence_operations">transcript &amp; editable summary</a>)

Beau explains influence operations as marketing on social media, showing how amplifying existing movements can disrupt stability in a target country, and advocates for addressing inequality as a solution to prevent exploitation.

</summary>

"It's marketing, it's advertising, nothing more."
"Yes, racial justice in the United States is a matter of national security."
"The easy fix here is to fix the inequality."
"To me, the solution is fix the problem, fix the thing that can be exploited."
"When people talk about influence operations, think of it in terms of Coca-Cola and Pepsi, not in terms of cameras and trench coats."

### AI summary (High error rate! Edit errors on video page)

Explains influence operations on social media as marketing, advertising, and not just spy stuff.
Describes how amplifying existing movements on social media can disrupt the status quo in a target country.
Points out that stability is the ultimate goal in foreign policy.
Mentions the role of amplifying opposition to create disunity and weaken a country's ability to respond internationally.
Uses the example of the Black Lives Matter movement to illustrate how influence operations can work.
Talks about how the US conducts influence operations in other countries and how the Kurdish people's decentralized movement hinders US intervention.
Suggests that addressing inequality is the key to preventing exploitation of marginalized groups.
Emphasizes that racial justice in the US is a matter of national security.
Raises concerns about platforms like TikTok being used for influence operations due to their ability to control algorithms and spread messages.
Advocates for addressing underlying issues rather than just trying to stop the exploitation.

Actions:

for internet users,
Address inequality to prevent exploitation of marginalized groups (suggested)
Advocate for racial justice as a matter of national security (suggested)
</details>
<details>
<summary>
2022-12-27: Let's talk about fusion power and climate change.... (<a href="https://youtube.com/watch?v=_ko2aK8vifI">watch</a> || <a href="/videos/2022/12/27/Lets_talk_about_fusion_power_and_climate_change">transcript &amp; editable summary</a>)

Beau explains fusion energy's breakthrough, its potential, and the need for immediate climate action despite future technological promises.

</summary>

"Fusion is way cleaner. There's no radioactive byproducts from it."
"We have to act as if this isn't there. We have to act as if this isn't on the horizon."
"It's a ways off for that technology to really be of use and of impact."
"Everything is impossible. Until it isn't."
"We have to do a whole lot better than we've been doing."

### AI summary (High error rate! Edit errors on video page)

Explains fusion energy breakthrough and its potential impact on climate change.
Differentiates between fusion and fission energy processes.
Acknowledges the limitations of current fusion technology despite the recent breakthrough.
Emphasizes the need for continued action on climate change regardless of future technological advancements.
Underlines the time it will take for fusion energy to become commercially viable.
Shares optimistic estimates of 10 years and pessimistic estimates of 20+ years for fusion energy development.
Urges for immediate action rather than relying solely on future technology.
Stresses the importance of improving current practices to pave the way for fusion as a viable solution in the future.
Notes the historical perspective on fusion technology once being deemed impossible.
Encourages viewers to maintain hope and strive for better despite challenges.

Actions:

for climate activists, energy enthusiasts,
Actively participate in climate action initiatives (implied)
Advocate for sustainable energy practices in communities (implied)
</details>
<details>
<summary>
2022-12-27: Let's talk about dropping people in DC and a song from the 1990s.... (<a href="https://youtube.com/watch?v=gBliegqIoiA">watch</a> || <a href="/videos/2022/12/27/Lets_talk_about_dropping_people_in_DC_and_a_song_from_the_1990s">transcript &amp; editable summary</a>)

Beau talks about a 90s Christian song, contrasts it with a governor's inhumane actions, praises community aid efforts, and criticizes politicization of conservative Christianity.

</summary>

"Y'all need Jesus."
"There is absolutely no way to defend this."

### AI summary (High error rate! Edit errors on video page)

Talks about a song from the 90s that spent 22 weeks on the country music charts, not really a country song, but a Christian song.
Mentions people being dropped off in DC in freezing temperatures on Christmas by a governor for a political point.
Acknowledges the justified anger towards the governor's actions.
Praises the Migrant Solidarity Mutual Aid Network for providing shelter, food, clothes, and toys to those dropped off, showcasing the power of community networks.
Points out the irony that leftists teamed up with religious institutions to help those in need, contrasting with the governor's actions.
Criticizes the governor for using people as props and not following the values of his supposed Christian beliefs.
Expresses concerns about the conservative Christian movement being consumed by hate and politicization.
Raises the question of what if Jesus comes back as one of the city folk mentioned in the Christian song.
Concludes by urging those who support the governor's actions to "need Jesus" and states there is no defense for the governor's actions.

Actions:

for community members, activists,
Support and volunteer with organizations like the Migrant Solidarity Mutual Aid Network (exemplified)
</details>
<details>
<summary>
2022-12-26: Let's talk about numbers, headlines, and cops.... (<a href="https://youtube.com/watch?v=h8hTI83susc">watch</a> || <a href="/videos/2022/12/26/Lets_talk_about_numbers_headlines_and_cops">transcript &amp; editable summary</a>)

Beau challenges misconceptions about police deaths, revealing COVID as the primary cause and urging critical analysis of headlines and information.

</summary>

"You were provided information that, while accurate, led you to the wrong conclusion."
"The person responsible for it, who's responsible for it, COVID."

### AI summary (High error rate! Edit errors on video page)

Accepting a challenge to analyze numbers and headlines, particularly regarding police-related information.
Responding to a message about 650 cops being gunned down this year, Beau clarifies that 655 cops died in the line of duty in 2021.
Points out that COVID was the leading cause of death for cops, not gunfire, with 472 deaths attributed to COVID in 2020 and 214 deaths from gunfire in 2022.
Emphasizes the importance of fact-checking and getting information from reliable sources like the Officer Down Memorial page.
Criticizes the media for framing information in a way that generates emotional reactions rather than providing accurate data.
Concludes by stating that COVID, not criminal activity, is responsible for the majority of cop deaths, urging people to critically analyze the information they receive.

Actions:

for critical thinkers, fact-checkers.,
Fact-check statistics from reliable sources like the Officer Down Memorial page (suggested).
Encourage others to verify information before accepting it as truth (implied).
</details>
<details>
<summary>
2022-12-26: Let's talk about accidentally left Republicans.... (<a href="https://youtube.com/watch?v=78v_RetwCdE">watch</a> || <a href="/videos/2022/12/26/Lets_talk_about_accidentally_left_Republicans">transcript &amp; editable summary</a>)

Hard-right Trump supporters express leftist economic ideas but lack social progressiveness, not allies in the fight against inequality.

</summary>

"Rural people live our lives by leftist economic principles every day."
"They're not looking for equality."
"We have to win the cultural battles."
"They're not allies."
"Playing into the economic stuff isn't going to help."

### AI summary (High error rate! Edit errors on video page)

Videos circulating show hard-right Trump supporters expressing leftist economic ideas like getting rid of banks and questioning the need for money.
Rural Americans live by leftist economic principles in daily life through co-ops, unions, farm aid, and resource redistribution.
Despite appearing leftist economically, the MAGA crowd is socially conservative, regressive, and sometimes racist.
Their desire to eliminate banks and landlords stems from seeking a reset of the current system to benefit themselves.
Their economic beliefs are often rooted in conspiracy theories and blaming specific groups.
They are not seeking equality but rather hoping for a shift in power dynamics.
Winning cultural and moral battles is key to reaching these individuals, as appealing to economic theories may reinforce negative stereotypes.
The goal is to lead them to seek real freedom for everyone and reject the concept of oppressive power structures.
Until they become socially progressive, they are not allies in the fight against inequality and racism.
To create a society based on economic equality, it's vital to address cultural and moral aspects rather than solely focusing on economic theories.

Actions:

for progressive activists,
Challenge conspiracy theories and negative stereotypes through community education and engagement (implied).
Engage in cultural and moral dialogues to foster social progressiveness among those with differing economic beliefs (implied).
</details>
<details>
<summary>
2022-12-26: Let's talk about a question on heroes.... (<a href="https://youtube.com/watch?v=E3d2OtAGpJs">watch</a> || <a href="/videos/2022/12/26/Lets_talk_about_a_question_on_heroes">transcript &amp; editable summary</a>)

In today's flawed world, seeking heroes is natural, but the true measure of growth lies in self-improvement, not idolizing perfection.

</summary>

"Nobody is perfect like that."
"Everybody is flawed."
"The only person that you should measure yourself against is you."
"Your childhood hero should kind of be irrelevant at this point."
"If you have lived five, ten, 15 years and you haven't changed your beliefs your opinions you've wasted your life."

### AI summary (High error rate! Edit errors on video page)

People still seek heroes to look up to, desiring to measure themselves against someone who has lived the life they aspire to.
Musk suspended the Jet Tracker account despite declaring he was leaving it up on his new Twitter account, raising questions about heroism in today's world.
History's great personalities, like Gandhi and Mandela, are not without flaws, illustrating the impossibility of finding a perfect hero.
Beau admires Teddy Roosevelt but acknowledges his flaws, underscoring that everyone, including heroes, is flawed.
Heroic people do not exist, only heroic actions, as heroism can often mask underlying frustrations.
It's possible to admire individuals for specific characteristics, but expecting perfection leads to disappointment.
The pursuit of an idolized, perfect figure to always look up to is futile; the only person to measure yourself against is your past self.
Continuous self-improvement should be the focus, rather than searching for a flawless hero to guide your life.

Actions:

for aspiring individuals seeking guidance.,
Measure your growth by comparing yourself to your past self (implied).
</details>
<details>
<summary>
2022-12-25: Let's talk about me helping Republicans.... (<a href="https://youtube.com/watch?v=nF2aFX4CRe0">watch</a> || <a href="/videos/2022/12/25/Lets_talk_about_me_helping_Republicans">transcript &amp; editable summary</a>)

Encouraging Republicans to become more progressive is key to shifting the Overton window and advancing progressive ideas, leading to a cycle of progress.

</summary>

"The party should be a tool for your beliefs. Shouldn't shape them."
"All of those people will shift if the Republican Party becomes more progressive."
"We have to shift to the Republican Party, get them to catch up, so progressives can move forward."

### AI summary (High error rate! Edit errors on video page)

Encouraging Republicans to become more progressive to win elections.
Explains three reasons for helping Republicans: a factual statement, belief it will work, and the potential for progress.
Raises a question to a viewer: why are you a Democrat?
Democrat or Republican affiliation should be based on holding progressive ideals.
Loyalty to a party can lead individuals to shift positions based on party stances.
Shifting the Overton window by moving the Republican Party to more progressive positions.
By making Republicans more progressive, it allows for more progressive ideas to be advocated for within the Democratic Party.
Views the party as a tool for beliefs, not something that should shape beliefs.
The cycle of progress where Republicans becoming more progressive can lead to further progress.
Emphasizes the importance of getting past stagnant arguments and pushing for progress through shifting political positions.

Actions:

for progressive individuals,
Advocate for progressive ideas within political parties (implied)
</details>
<details>
<summary>
2022-12-25: Let's talk about a Christmas special.... (<a href="https://youtube.com/watch?v=Yd0yfSJBvmQ">watch</a> || <a href="/videos/2022/12/25/Lets_talk_about_a_Christmas_special">transcript &amp; editable summary</a>)

Beau addresses funny Q&A questions, misconceptions about his appearance, working dynamics, adapting viewpoints, collaborations, production changes, injuries, marketing decisions, and upcoming political landscape.

</summary>

"Don't idolize anybody."
"If you want to take away that portion of my videos and make me stop making them, it's really simple. All you have to do is start watching black creators on YouTube."
"Hopefully it will be a bit more calm though."

### AI summary (High error rate! Edit errors on video page)

Addressing funny Q&A questions selected by the team, including being mistaken for others and working dynamics with the team.
Acknowledging misconceptions about his appearance and public image, such as being misidentified in public.
Admitting to having no concept of time while working and questioning the rationale behind tasks.
Expressing the importance of adjusting viewpoints when proven wrong and the ongoing effort to combat ableism.
Responding to criticism about addressing race issues and his audience, encouraging support for Black creators.
Sharing insights on collaborations, production workflow changes, and upcoming behind-the-scenes content.
Describing past injuries, including a life-threatening head injury and a painful incident involving his feet.
Touching on the decision not to heavily market his second channel on the main channel due to its impact on video quality.
Wishing viewers Merry Christmas and discussing the political landscape in the United States for the upcoming year.

Actions:

for content creators and supporters,
Support Black creators by actively watching and engaging with their content (suggested)
Encourage collaborations with diverse creators on YouTube (exemplified)
</details>
<details>
<summary>
2022-12-25: Let's talk about a Christmas message about you.... (<a href="https://youtube.com/watch?v=2wN-wNE_a_Y">watch</a> || <a href="/videos/2022/12/25/Lets_talk_about_a_Christmas_message_about_you">transcript &amp; editable summary</a>)

Over 250 kids received Christmas gifts and $10,000 will aid shelters, all from the community's generous support, bringing smiles and safety during tough times.

</summary>

"More than 250 kids are going to wake up to a Christmas because of y'all."
"Because of y'all, this morning, 250 kids had a smile on their face because of you."
"With all of the problems in the world, all of the division, and all of the news that just doesn't seem to be getting any better, it's one of those moments to celebrate the win."

### AI summary (High error rate! Edit errors on video page)

Over 250 kids received Christmas gifts through the community's support, including tablets, earbuds, and gift cards.
Fundraisers were held for children of striking minors in Alabama and teens in domestic violence shelters.
$10,000 will be distributed to three shelters, thanks to donations from the community.
The gifts provided include bags with various items and an Xbox for one of the shelters.
Even those who couldn't attend live streams still contributed through YouTube ad revenue.
Despite the world's problems and divisions, celebrating the community's positive impact is vital.
$10,000 can significantly benefit domestic violence shelters and enhance safety for many individuals.
The community's collective efforts brought smiles to over 250 kids during Christmas.
Beau expresses gratitude for the community's support in making a difference in these children's lives.
The act of giving and supporting others, especially during tough times, is a powerful gesture.

Actions:

for community members,
Support local shelters with donations (exemplified)
Attend fundraisers for children in need (exemplified)
Contribute to community initiatives for positive impact (implied)
</details>
<details>
<summary>
2022-12-24: Let's talk about the what-ifs of the 6th.... (<a href="https://youtube.com/watch?v=yYVD3w0XfoA">watch</a> || <a href="/videos/2022/12/24/Lets_talk_about_the_what-ifs_of_the_6th">transcript &amp; editable summary</a>)

Beau explains why a self-coup through seizing Congress wouldn't work, detailing the necessary factors and structural resilience of the U.S. government.

</summary>

"You can't initiate a self-coup in the United States by the seizure of Congress. It will not work."
"The outrage of trying to initiate something like this through force that is very public, it won't work."
"Even if they had taken the building, they wouldn't have won."
"The numbers just don't exist."
"The U.S. government is structured to survive a nuclear attack."

### AI summary (High error rate! Edit errors on video page)

Explains why a self-coup in the United States through the seizure of Congress wouldn't work.
Describes the key factors needed for a self-coup to occur successfully.
Mentions the structural resilience of the U.S. government against such attempts.
Lists the chain of command required for a self-coup to be successful, starting from the president down to Homeland Security.
Emphasizes the importance of all individuals in the chain being complicit for the coup to succeed.
Points out that even if the military were used to impose rule, the numbers required for occupation don't exist.
States that a self-coup initiated in a public and forceful manner in the U.S. wouldn't be successful due to the outrage and potential conflicts it could spark.

Actions:

for citizens, political observers,
Understand the structural and procedural safeguards in place in the U.S. government to prevent a self-coup (implied).
</details>
<details>
<summary>
2022-12-24: Let's talk about a video game habit.... (<a href="https://youtube.com/watch?v=krlfp5BtZjA">watch</a> || <a href="/videos/2022/12/24/Lets_talk_about_a_video_game_habit">transcript &amp; editable summary</a>)

A single mom seeks advice on her daughter's gaming obsession with running a business, prompting Beau to suggest embracing the interest and providing a real-world challenge.

</summary>

"Our kids are doing something and we just don't get it."
"They just need the challenge. They need that passion for something."
"You can be the facilitator of her dream rather than the mom who wants you to put down the phone."

### AI summary (High error rate! Edit errors on video page)

A single mom is torn about getting Google Play cards for her daughter, who plays a game where she builds a city and runs a business.
The mom worries that the game gives her daughter an unrealistic idea of running a business and may be taking her away from real life.
Beau suggests getting the cards and also opening accounts on various platforms together to challenge her daughter.
He mentions underestimating kids' abilities and the importance of providing challenges for them to shine.
Beau encourages the mom to support her daughter's interest in running a business, as it could be a valuable learning experience.
He points out that kids today have a good understanding of marketing and brand management due to social media influences.
Beau recommends embracing the daughter's interest in business and letting her experience the learning curve early on.

Actions:

for single parents,
Open accounts on platforms like Teespring, Redbubble, Spreadshirt, Fine Art America together to challenge and support the daughter's interest in running a business (implied).
</details>
<details>
<summary>
2022-12-24: Let's talk about Trump calling out the committee.... (<a href="https://youtube.com/watch?v=VRt88sFIx2o">watch</a> || <a href="/videos/2022/12/24/Lets_talk_about_Trump_calling_out_the_committee">transcript &amp; editable summary</a>)

Beau breaks down the manipulation behind calling committee members Marxists, exposing the exploitation of ignorance for political gain and profit.

</summary>

"They stand there at those rallies. They give those speeches. They throw out that word. They get everybody angry. Then they walk behind that curtain and they laugh because they know what it means."
"They know that the American education system isn't great. They know their base doesn't know what Marxism is. Because quote, I love the uneducated because they're easy to manipulate."
"They played you about the election. They lied to you about that. And now they've moved on. A new term. They're lying to you again."
"One of the hardest things in the world is to admit that you have been tricked."
"They're tricking you."

### AI summary (High error rate! Edit errors on video page)

Critiques Trump's response to the committee calling him out, particularly focusing on Trump labeling the committee members as Marxists.
Beau breaks down the definition of Marxism as a belief in a stateless, classless, moneyless society where workers control the means of production.
Questions whether members of Congress, who are often wealthy and receive campaign contributions from big businesses, truly advocate for the working class as Marxists do.
Suggests that Trump and others who use the term "Marxist" as a scare tactic are fully aware of its meaning but exploit the ignorance of their base for manipulation.
Points out how certain media outlets profit from spreading misinformation and manipulating their audience's lack of knowledge.
Emphasizes the manipulation and trickery employed by certain political figures and media to control their supporters and advance their agendas.
Calls out the Republican Party for deceiving its supporters about various issues, including the election, and warns about ongoing manipulation tactics.
Urges people to recognize when they have been deceived and to be critical of the information they receive from political figures and media.

Actions:

for voters, media consumers,
Educate yourself on political terminologies and ideologies to be less susceptible to manipulation (implied).
Stay informed from diverse and credible sources to combat misinformation (implied).
</details>
<details>
<summary>
2022-12-23: The roads to Ukraine with Philip Ittner.... (<a href="https://youtube.com/watch?v=3ZNIRzPQLCk">watch</a> || <a href="/videos/2022/12/23/The_roads_to_Ukraine_with_Philip_Ittner">transcript &amp; editable summary</a>)

Beau and Phil dive deep into the resilience and unity of the Ukrainian people amidst war, showcasing their resourcefulness, resolve, and unwavering spirit against Russian aggression.

</summary>

"There is still joy. There is still, there's almost, there's a determined, it's almost a way of fighting back, is to keep joy and happiness and laughter in your life."
"You can't crush the human spirit. And Ukrainians are showing that."
"But the Ukrainian cause is just. And we should support them."

### AI summary (High error rate! Edit errors on video page)

Introduction of guest, Phil Itner, a journalist with decades of experience working for various broadcasters.
Phil's background as a war correspondent embedded with military divisions during significant events.
Phil's love for Ukraine, its people, and his hope for lasting peace after this war.
Phil's decision to provide context and depth in his reporting from Ukraine, focusing on understanding and living with the Ukrainian people.
Challenges faced in Ukraine, including lack of heating and spotty water supply.
Ukrainians' resolve to endure hardships rather than return to being under Moscow's control.
Anecdote of a woman's breakdown in a grocery store due to power outage, showcasing the collective resilience and support among Ukrainians.
Ukrainians' resourcefulness in combating the cold, including using space heaters, candles, stocking up on essentials, and utilizing underground structures for warmth.
Phil's struggle to maintain objectivity while emotionally invested in the war and the Ukrainian cause.
Phil's coping mechanism of humor and resolve in the face of war-related trauma.
Morale boost from President Zelensky's visit to Bakhmut and his role as a rallying point for the Ukrainian people.
Ukrainians' creativity and resilience in maintaining joy and normalcy during the conflict, such as painting tank traps for the holidays and embracing music and art.
Ukrainian people's unity and determination to fight back against Russian aggression, exemplified by their diverse contributions to the war effort.
Call to support Ukraine's just cause and stand against autocracy.

Actions:

for supporters of ukraine,
Support Ukrainian craftsmen by purchasing Ukrainian-made products to aid the local economy (suggested).
Share stories of Ukrainian resilience and creativity in the face of war to raise awareness and support for Ukraine (exemplified).
</details>
<details>
<summary>
2022-12-23: Let's talk about the missing Republican committee.... (<a href="https://youtube.com/watch?v=ft8kBLYcN9E">watch</a> || <a href="/videos/2022/12/23/Lets_talk_about_the_missing_Republican_committee">transcript &amp; editable summary</a>)

The missing committee on 2020 election allegations reveals the Republican Party's baseless claims and lack of accountability, exposing their propaganda.

</summary>

"Their propaganda is bad and they should feel bad."
"The missing committee is the one that was supposed to look into the allegations from the 2020 election."
"They know it's a lie. They know the allegations were baseless. They know they were made up."
"The American people are more interested in seeing those who made the allegations being held accountable."
"If the party truly believed in the claims they made, they should be eager to bring in those involved and have them provide evidence under oath."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is set to take over the House of Representatives soon and has promised numerous committees, but there is one missing committee.
The missing committee is the one that was supposed to look into the allegations from the 2020 election.
Instead of focusing on investigating the election allegations, they plan to look at Fauci, a laptop, and "investigate the investigators."
Despite claiming the election was bad, there seems to be no interest from the Republican Party in getting to the bottom of the allegations.
Beau questions whether the lack of interest in investigating the election claims implies that the party knows they were baseless all along.
He suggests that if the party truly believed in the claims they made, they should be eager to bring in those involved and have them provide evidence under oath.
Beau believes that the American people are more interested in seeing those who made the allegations being held accountable rather than investigating other issues.
The lack of interest in pursuing this investigation suggests that the Republican Party is aware that the allegations were false and baseless.
Bringing people in to talk about the election claims could reveal their complicity in spreading misinformation and undermining democratic institutions.
Beau criticizes the Republican Party for their propaganda and lack of interest in addressing the baseless election allegations.

Actions:

for voters, activists, concerned citizens,
Hold elected officials accountable for spreading misinformation (implied)
Support fact-checking initiatives to combat propaganda (implied)
</details>
<details>
<summary>
2022-12-23: Let's talk about how Biden's new policy is working.... (<a href="https://youtube.com/watch?v=PyalYfO_-jI">watch</a> || <a href="/videos/2022/12/23/Lets_talk_about_how_Biden_s_new_policy_is_working">transcript &amp; editable summary</a>)

The Biden administration's cautious approach to special operations shows promise in limiting civilian loss and developing a sensible drone policy.

</summary>

"The United States has finally developed a drone policy that makes sense."
"It is more difficult to do it this way. It is safer."
"The Biden administration's policy appears to be holding up and accomplishing its goals."

### AI summary (High error rate! Edit errors on video page)

The Biden administration formalized policy changes on the use of special operations or drone forces, making decisions at the White House to limit civilian loss.
Beau expresses skepticism about the effectiveness of the policy due to the temptation drones pose and past unsuccessful policies.
A recent raid in Syria, carried out through a special operations helicopter-borne raid, demonstrated a cautious approach by the White House to limit civilian loss.
Despite the ideal use of a drone in the raid, no civilian casualties were reported, indicating the administration's seriousness in adhering to the policy.
The Biden administration aims for almost zero civilian loss in such operations.
The raid provided an option to surrender, but the individuals chose a different route, resulting in a successful mission with no civilian casualties.
The US military may be shifting focus towards capturing individuals in operations and rebuilding intelligence-gathering capabilities.
This early implementation of the policy is a hopeful sign in US drone use, reflecting a more sensible approach after 20 years.
The Biden administration's policy appears to be achieving its goals, though it requires political will and commitment to maintain safety and effectiveness.
The success of the policy depends on the administration's dedication to keeping special operations prepared and in the necessary locations.

Actions:

for policy analysts, activists,
Support organizations advocating for transparency and accountability in special operations and drone policies (suggested).
Stay informed about updates and developments in US military strategies and policies (implied).
</details>
<details>
<summary>
2022-12-23: Let's talk about another statue being removed.... (<a href="https://youtube.com/watch?v=GtvuMLwzInc">watch</a> || <a href="/videos/2022/12/23/Lets_talk_about_another_statue_being_removed">transcript &amp; editable summary</a>)

Beau talks about the removal of a statue of Roger Brooke Taney, known for the infamous Dred Scott decision, and the importance of moving from symbolic gestures towards concrete actions to address historical injustices.

</summary>

"Removing a statue because it is embarrassing, yeah, I get it. Righting the wrongs, that's a whole lot better."
"The promises laid out in those founding documents are still not being lived up to."
"But each symbolic act carries us closer to real action, tangible action, to right the many things that this country has done."

### AI summary (High error rate! Edit errors on video page)

Introduction about discussing the removal of a statue.
The statue in question is of Roger Brooke Taney, overshadowed by the infamous Dred Scott decision he wrote.
The Dred Scott decision held that black Americans couldn't be citizens and that black people had no rights white people were bound to respect.
Taney viewed the founding documents as an agreement to keep a permanent underclass, not as a promise of equality.
Despite objections to Taney's views then and now, his statue is being removed by vote.
The statue will be replaced by Thurgood Marshall, the first black associate justice of the Supreme Court.
Beau sees this as a symbolic act, acknowledging that the promises of the founding documents are still unfulfilled.
Symbolic acts like statue removals move towards real and tangible actions to address past wrongs.
Beau expresses the importance of not just removing statues for embarrassment's sake but taking concrete steps to right historical injustices.
Beau concludes with a call to aim for real action beyond symbolic gestures.

Actions:

for history buffs, activists, voters,
Advocate for the removal of statues glorifying figures linked to systemic racism (exemplified)
Support initiatives that address historical injustices and work towards real action (exemplified)
</details>
<details>
<summary>
2022-12-22: Let's talk about responses to Minnesota's proposed regulations.... (<a href="https://youtube.com/watch?v=GIajeWHZTow">watch</a> || <a href="/videos/2022/12/22/Lets_talk_about_responses_to_Minnesota_s_proposed_regulations">transcript &amp; editable summary</a>)

Beau explains why law enforcement officers participating in hate speech should face consequences, linking it to public trust and safety, and challenges the argument against regulating such speech.

</summary>

"Not being a bigot is kind of a bona fide job qualification for law enforcement."
"There is no reason for an officer to oppose getting rid of bigots on police forces, except for one."
"Bigots on police departments. They are a danger to the department."
"I personally cannot think of anything that makes that kind of speech more powerful than giving it a badge and a gun."
"Making it more powerful."

### AI summary (High error rate! Edit errors on video page)

Addresses proposed regulations in Minnesota regarding law enforcement officers participating in online hate speech.
Explains the purpose of the First Amendment and distinguishes it from the concept of free speech.
Argues that law enforcement officers, as agents of the government, should be held to a higher standard.
Suggests that being a bigot contradicts the job qualifications for law enforcement officers.
Draws parallels with other professions like nursing and fast food service to illustrate the impact of biases on job performance.
Counters the argument that regulating hate speech makes it more powerful, especially in the context of law enforcement.
Points out the danger of bigoted officers within police departments and their potential impact on public trust and safety.

Actions:

for law enforcement reform advocates,
Advocate for regulations that hold law enforcement officers accountable for hate speech (suggested)
Support initiatives to remove bigoted officers from police forces (suggested)
</details>
<details>
<summary>
2022-12-22: Let's talk about Russia's nuclear rhetoric.... (<a href="https://youtube.com/watch?v=wpXN2a1393g">watch</a> || <a href="/videos/2022/12/22/Lets_talk_about_Russia_s_nuclear_rhetoric">transcript &amp; editable summary</a>)

Beau addresses Russian nuclear rhetoric, stressing it as saber rattling to unsettle the West and downplaying genuine concerns about strategic arms.

</summary>

"It is saber rattling. It is rhetoric."
"If you are going to attempt a first strike, do you know the number one thing you need for it to be a quote success? Understand there is no success when you're talking about nuclear weapons."
"I wouldn't worry about that too much."

### AI summary (High error rate! Edit errors on video page)

Talks about the Russian government engaging in unsettling rhetoric, making people nervous, especially the younger generation unfamiliar with Cold War tensions.
Mentions Putin discussing the possibility of removing their policy against a first strike and commanders expressing a desire to use nuclear weapons.
Explains the concept of Mutually Assured Destruction (MAD) in the context of nuclear weapons.
Emphasizes that the key element for a successful first strike with nuclear weapons is surprise, which is why they wouldn't openly telegraph their intentions.
States that the saber rattling and posturing from Russia are intended to unsettle the West and question their resolve.
Notes that for Putin, resorting to nuclear saber rattling may inadvertently embolden the West by implying weakness in Russia's conventional forces.
Predicts a similar pattern of rhetoric when tensions escalate with China in the future.
Assures viewers that if there were genuine concerns about strategic arms, he'd make a video expressing those concerns.

Actions:

for global citizens,
Stay informed on international relations and nuclear policies (implied)
</details>
<details>
<summary>
2022-12-22: Let's talk about Hannity not believing it for a second.... (<a href="https://youtube.com/watch?v=y5YQlHaNTkM">watch</a> || <a href="/videos/2022/12/22/Lets_talk_about_Hannity_not_believing_it_for_a_second">transcript &amp; editable summary</a>)

Beau questions the lack of accountability in news reporting and speculates on the potential impact of early truth-telling by key personalities on conservative networks.

</summary>

"I did not believe it for one second."
"I think this country would be a very, very different place if a whole lot of people who are saying that under oath right now had said it publicly on the air back then."

### AI summary (High error rate! Edit errors on video page)

Mentioning Sean Hannity's denial of believing in claims about the elections and machines from a deposition suit.
Noting that lawyers claim none at Fox believed the unsubstantiated claims broadcasted.
Questioning the lack of follow-up reporting from Fox News on incorrect narratives pushed on their network.
Expressing the impact if key personalities like Hannity or Tucker had publicly disavowed the false claims earlier.
Stating the ethical obligation for news outlets to provide accurate information, especially when influencing millions of people.
Speculating on the potential difference in outcomes if key conservative personalities had been honest with their audience.
Remarking on the responsibility of those disseminating misinformation for the consequences it can lead to.
Contemplating how many individuals might not be in trouble if truthful statements were made earlier.
Acknowledging the role of misinformation in people's decision-making processes.
Suggesting that a significant number of people publicly retracting false claims could have changed the country's trajectory.

Actions:

for media consumers,
Hold news outlets accountable for the accuracy of information they disseminate (implied).
Demand transparency and honesty from media personalities, especially when correcting false narratives (implied).
</details>
<details>
<summary>
2022-12-21: Let's talk about whether Republicans can trust the committee's findings.... (<a href="https://youtube.com/watch?v=KNgxoYQK6Wc">watch</a> || <a href="/videos/2022/12/21/Lets_talk_about_whether_Republicans_can_trust_the_committee_s_findings">transcript &amp; editable summary</a>)

Republicans question committee's findings, but most damaging info came from Trump's team, not the committee; Trust in Trump administration at stake.

</summary>

"If you can't trust that testimony, I mean, that kind of says you can't trust the Trump administration, right?"
"The lifelong members of the Republican Party, they're not the RINOs. That's Trump."
"The most damaging information that came forward came from the testimony of Trump's team."
"As far as the most damaging testimony, it wasn't brought forth by the Democratic party."
"It says a whole lot about whether or not you can trust any Trump administration."

### AI summary (High error rate! Edit errors on video page)

Analyzing whether Republicans can trust the committee's findings and report.
Addressing concerns about the committee being Democrat-run with no Trump supporters.
Explaining that the committee did not testify or generate evidence; Trump's team did.
Emphasizing that the most damaging information came from Trump's own team.
Arguing that if you can't trust Trump's team's testimony, you can't trust the Trump administration.
Debunking the idea of Republicans on the committee as RINOs, citing Liz Cheney's Republican history.
Stating that lifelong Republicans are not RINOs but Trump, who manipulated a susceptible base.
Asserting that closer to Trump, individuals were more likely to ignore subpoenas or plead the fifth.
Noting that the most damaging testimony came from Trump's White House team.
Leaving it to individuals to decide whether to trust Trump administration.

Actions:

for republicans,
Examine the evidence presented by Trump's team and decide whether to trust the Trump administration (implied).
Challenge preconceived notions about Republicans on the committee and understand their history (implied).
</details>
<details>
<summary>
2022-12-21: Let's talk about encryption, tech, and cops.... (<a href="https://youtube.com/watch?v=PZYJnXAveww">watch</a> || <a href="/videos/2022/12/21/Lets_talk_about_encryption_tech_and_cops">transcript &amp; editable summary</a>)

Apple enhances encryption, sparking debate on balancing privacy and law enforcement access, a microcosm of broader philosophical conflicts in the digital age.

</summary>

"The debate is about the balance between personal freedom and law enforcement’s ability to do their job."
"This conflict mirrors the ongoing battle between social media companies and politicians over control of the narrative."
"Apple’s goal is to enhance product security, not cater to criminals."
"The clash between tech companies and law enforcement regarding encryption is anticipated to intensify."
"The conflict between protecting privacy and enabling law enforcement access is a complex and ongoing dilemma."

### AI summary (High error rate! Edit errors on video page)

Apple is enhancing encryption to better protect user information, upsetting the FBI who are concerned about the difficulty in accessing information.
The debate revolves around the balance between personal freedom and the ability of law enforcement to do their job effectively.
This conflict mirrors the ongoing battle between social media companies and politicians over control of the narrative.
Encryption debates are expected to increase globally, leading to proposed legislation and significant arguments on the topic.
Apple’s goal is to enhance product security, not cater to criminals, showcasing capitalism in action.
The underlying question is how much inconvenience society is willing to accept to maintain personal freedom.
The clash between tech companies and law enforcement regarding encryption is anticipated to intensify.
The encryption issue signifies broader philosophical debates on safety, security, and freedom in the digital age.
The scenario with Apple and the FBI represents a microcosm of the larger issue of balancing security and liberty.
The conflict between protecting privacy and enabling law enforcement access is a complex and ongoing dilemma.

Actions:

for tech users, privacy advocates,
Contact local representatives to voice opinions on privacy and encryption (suggested)
Join tech advocacy groups promoting user privacy rights (implied)
</details>
<details>
<summary>
2022-12-21: Let's talk about Musk, Twitter, and downgrading.... (<a href="https://youtube.com/watch?v=tunPcpPU6b8">watch</a> || <a href="/videos/2022/12/21/Lets_talk_about_Musk_Twitter_and_downgrading">transcript &amp; editable summary</a>)

Elon Musk's Twitter antics and potential voter suppression raise concerns about Tesla's market value and reputation, prompting suggestions for a way out.

</summary>

"The reality is, go fast, no cash."
"In what is just a hilarious development after that, Musk was seen in a conversational Twitter with somebody who suggested that he only allows those people with blue checks to vote."
"One of the reasons here, we believe banning journalists without consistent defensible standards or clear communication in an environment where many people believe free speech is at risk is too much for a majority of consumers to continue supporting Mr. Musk slash Tesla."

### AI summary (High error rate! Edit errors on video page)

Elon Musk conducted a poll on Twitter asking if he should resign, with a majority voting yes, including about 10 million users.
Musk engaged in a Twitter exchange where the suggestion of only allowing verified users (blue checks) to vote was made, which Musk seemed to entertain, hinting at voter suppression.
Tesla, the company, was downgraded by Oppenheimer & Company due to concerns about banning journalists without clear standards, potentially affecting consumers who support climate change mitigation.
There are worries about a negative feedback loop for Tesla stemming from the controversial content some unbanned users create on Twitter, leading to brands pulling out and users leaving the platform.
Oppenheimer believes that Tesla's value drop, around 50%, could be influenced by these Twitter-related issues.
Beau suggests that Musk stepping away from the company and bringing on a board with content guidelines could be a way out of the situation.
Beau challenges the idea that going woke leads to going broke, citing the real issue as going fast without cash as the downfall for companies.
The situation raises concerns about the impact of Twitter controversies on Tesla's market value and reputation.

Actions:

for social media users,
Contact Tesla and express concerns about the company's handling of controversies on Twitter (suggested)
Join or support organizations advocating for responsible social media conduct by companies like Tesla (implied)
</details>
<details>
<summary>
2022-12-20: Let's talk about what comes after the Committee.... (<a href="https://youtube.com/watch?v=AKp5LfjKSNM">watch</a> || <a href="/videos/2022/12/20/Lets_talk_about_what_comes_after_the_Committee">transcript &amp; editable summary</a>)

Beau talks about the aftermath of committee hearings, warns of upcoming political manipulations, and underlines the historic significance of recent events, including January 6th.

</summary>

"They are still under the illusion that social media shares and likes somehow are a valid electoral strategy."
"The key thing you have to remember is if they were lying under oath, that just means that a whole bunch of people that were handpicked by Trump had no problem lying under oath."
"Make no mistake, the January 6th event and the hearings afterward, they will be in history books."

### AI summary (High error rate! Edit errors on video page)

Talks about what comes after the committee hearings, even though they are over and the show has theoretically ended.
Mentions that the released summary doesn't offer much new information for those who watched the hearings, spanning about 150 pages.
Points out that the next phase will involve the release of transcripts, where different political factions will try to manipulate discrepancies to create scandal and divert attention.
Anticipates a phase where inconsistencies in statements will be magnified to cast doubt on testimonies.
Emphasizes the importance of recognizing that if someone was lying under oath, it implicates others too.
Notes the upcoming DOJ decision and the historic significance of the committee's actions regarding January 6th.
Concludes by mentioning the illusion some hold about social media's impact on electoral strategies.

Actions:

for politically engaged citizens,
Contact local representatives to express support for transparency and accountability in political processes (suggested)
Engage in informed political discourse with friends and family to counter misinformation and manipulation (suggested)
</details>
<details>
<summary>
2022-12-20: Let's talk about that list in Texas.... (<a href="https://youtube.com/watch?v=wZXgZB9-yEs">watch</a> || <a href="/videos/2022/12/20/Lets_talk_about_that_list_in_Texas">transcript &amp; editable summary</a>)

Beau raises concerns about the Texas attorney general's office seeking a list of people who changed their gender on driver's licenses, stressing the need for transparency and continued investigation despite the holiday season.

</summary>

"Reports suggested the Texas attorney general's office wanted a list of people who changed their gender on their driver's license."
"The Attorney General's office certainly appears to have requested a list of trans people in the state with no explanation as to why."
"When a group of authoritarians starts attempting to make lists of people that they have actively othered, it's not a good thing."
"This isn't a story that should be forgot, though."
"No, this doesn't need to be forgotten."

### AI summary (High error rate! Edit errors on video page)

Reports suggested the Texas attorney general's office wanted a list of people who changed their gender on their driver's license.
More than 16,000 entries were involved, but the information didn't change hands due to the exhausting manual process.
Beau usually investigates stories but couldn't find any context or information on this matter.
Major news outlets like Washington Post, NBC, and New York Times haven't been able to get answers either.
The Attorney General's office has remained silent on the matter, even after stories were published.
Beau expresses concern about authorities making lists of marginalized groups.
Due to the holidays, the story is unlikely to progress until after New Year's unless someone has inside information.
Beau urges for transparency from the Attorney General's office regarding the motive behind seeking this list.
He stresses that this story shouldn't be forgotten and requires further investigation.
Beau hopes that reputable news outlets will continue to pursue this story despite the holiday season.

Actions:

for news outlets, activists,
Investigate the situation further to uncover the motives behind the request for the list (implied).
Stay informed and keep the story alive by sharing information on social media and with relevant organizations (implied).
</details>
<details>
<summary>
2022-12-20: Let's talk about Trump's move with McCarthy.... (<a href="https://youtube.com/watch?v=roSe51-Ds3s">watch</a> || <a href="/videos/2022/12/20/Lets_talk_about_Trump_s_move_with_McCarthy">transcript &amp; editable summary</a>)

Former President Trump strategically gains influence by publicly endorsing McCarthy for Speaker, potentially leading to de facto control over the House and a chaotic political environment.

</summary>

"Trump owns McCarthy."
"Trump still controls the Republican Party."
"Trump will use that influence, that leverage, to turn the House into a circus."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's public address was a strategic move to gain influence through McCarthy, the presumptive Speaker of the House.
McCarthy currently lacks the votes needed to become Speaker, and Trump's public support puts McCarthy in a position where he must follow Trump's lead.
Trump's public endorsement of McCarthy essentially gives him de facto control of the House if McCarthy becomes Speaker.
Despite potential backlash, McCarthy has tied his political fortunes to Trump by accepting his support.
Trump's control over the House through McCarthy could lead to a chaotic political environment and keep his grievances at the forefront.
The Republican Party's continued loyalty to Trump allows him to maintain significant power and influence.

Actions:

for political observers,
Monitor the political developments closely and stay informed about the power dynamics within the Republican Party (implied).
</details>
<details>
<summary>
2022-12-19: Let's talk about the committee, the symbolic, and civics.... (<a href="https://youtube.com/watch?v=pBDODYrsxvM">watch</a> || <a href="/videos/2022/12/19/Lets_talk_about_the_committee_the_symbolic_and_civics">transcript &amp; editable summary</a>)

Beau explains the importance of symbolism in government actions and how it carries messages to the public.

</summary>

"Symbolism matters."
"It's showing that the system as a whole, the government as a whole, representatives of all three branches have made the same determination."
"The symbolic matters a lot."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of symbolism in the context of criminal referrals against the former president.
Emphasizes that symbolism is significant because it conveys messages beyond the normal activist community.
Describes the process of different branches of government signing off on determinations related to the case.
Illustrates how the system functions with the legislative, judicial, and executive branches making determinations.
Acknowledges that while symbolic, the referrals are not irrelevant and play a vital role in showcasing a unified decision by all branches of government.
Advocates for understanding the symbolic importance and not letting commentators downplay its significance.
Stresses that symbolism carries messages to the public and is a key aspect of activism and government operations.

Actions:

for government observers,
<!-- Skip this section if there aren't physical actions described or suggested. -->
</details>
<details>
<summary>
2022-12-19: Let's talk about Trump's pardon possibilities.... (<a href="https://youtube.com/watch?v=KKXa7FrQO_8">watch</a> || <a href="/videos/2022/12/19/Lets_talk_about_Trump_s_pardon_possibilities">transcript &amp; editable summary</a>)

Beau stresses the importance of questioning Republican candidates on pardoning Trump to distinguish between factions and urges the party to embrace progressiveness to avoid irrelevance.

</summary>

"They have to become more progressive so they can at least stay within the Overton window of the United States."
"At this point, the normal conservatives within the Republican Party, they have to be acting on self-defense for their own positions of power."
"That question about whether or not a particular candidate would pardon Trump needs to be asked over and over and over again."

### AI summary (High error rate! Edit errors on video page)

Speculates on the possibility of former President Trump receiving a pardon in the future, potentially from an ally.
Suggests that delaying tactics might be linked to securing a pardon even if Trump doesn't return to the White House.
Believes that there are individuals within the Republican Party who might pardon Trump if he faces convictions or preemptively to avoid investigations.
Urges for candidates in the Republican Party, including moderators, to be questioned directly about their stance on pardoning Trump to distinguish between different factions within the party.
Indicates that those willing to pardon Trump may hinder the progression of the Republican Party and push it further into irrelevance.
Stresses the importance of the Republican Party moving towards a more progressive stance to remain relevant and attract voters, particularly on social issues.
Warns that failure to adapt and become more progressive may lead to irrelevance and reputational damage, especially from the "Sedition Caucus."
Points out that many within the Republican Party are motivated by maintaining power rather than ideology, making self-defense a key factor in their decisions.
Emphasizes the need to repeatedly question candidates about their willingness to pardon Trump as a critical issue within the party.

Actions:

for republican party members,
Question Republican candidates on their stance regarding pardoning Trump repeatedly (suggested)
Advocate for progressive policies within the Republican Party (implied)
</details>
<details>
<summary>
2022-12-19: Let's talk about Texas wanting to remove teens from social media.... (<a href="https://youtube.com/watch?v=ThhJu4t1_U8">watch</a> || <a href="/videos/2022/12/19/Lets_talk_about_Texas_wanting_to_remove_teens_from_social_media">transcript &amp; editable summary</a>)

Texas proposed a law to ban under-18s from social media under the guise of child protection, aiming to control information flow and limit young people's access to education and free speech.

</summary>

"The goal is to control the flow of information."
"They want to keep younger people ignorant. They want to remove their ability to really engage in any kind of free speech."
"All it will do is make those people in Texas less informed, less able to make decisions on their own."

### AI summary (High error rate! Edit errors on video page)

Texas proposed a law to ban under-18s from social media and require photo ID for account opening.
The rationale behind the law is framed as protecting children, but Beau doubts Texas's interest in child protection.
The true goal behind the law appears to be controlling the flow of information.
The Republican Party equates social media with their decline in power, leading to this proposed law.
Beau suggests the simple solution for the Republican Party is to stop lying.
The proposed law aims to keep younger people ignorant, limit free speech, and control information access.
Banning under-18s from social media could cut off educational opportunities, especially on platforms like YouTube.
The law could unintentionally create a surveillance apparatus by linking photo IDs to accounts.
Many do not realize YouTube is considered social media due to its user-generated content.
Beau criticizes the law for potentially making Texans less informed and limiting their decision-making abilities.

Actions:

for texans, activists, parents,
Rally against the proposed law in Texas (suggested)
Contact state legislators to voice opposition to the law (suggested)
</details>
<details>
<summary>
2022-12-18: Let's talk about where Artemis goes from here.... (<a href="https://youtube.com/watch?v=jqUjgSHIW0M">watch</a> || <a href="/videos/2022/12/18/Lets_talk_about_where_Artemis_goes_from_here">transcript &amp; editable summary</a>)

Artemis program faces budget delays in lunar missions, aiming to establish a base on the moon within the next few years.

</summary>

"Artemis 2 will fly astronauts around the moon sometime in 2024."
"It had to do with trying to fill a budget hole a few years ago. This is literally all about money."
"The delays in the Artemis missions are primarily due to budget constraints rather than technical upgrades."
"The successful completion of Artemis 1's mission involved deep space maneuvers and system tests on the Orion spacecraft."
"Despite budget challenges, NASA is continuing its efforts to advance towards establishing a base on the moon."

### AI summary (High error rate! Edit errors on video page)

Artemis 2 will fly astronauts around the moon in 2024, with Artemis 3 following to put people back on the moon.
NASA's Artemis program aims to establish a research base near the southern pole of the moon and a lunar space station for refueling missions.
The delays in the Artemis missions are primarily due to budget constraints rather than technical upgrades.
The successful completion of Artemis 1's mission involved deep space maneuvers and system tests on the Orion spacecraft.
The spacecraft completed a 240,000-mile journey and was retrieved by the Navy on December 11.
Following splashdown, the craft is being transported back to NASA for thorough examination.
The current timeline for the Artemis missions includes establishing a base on the moon within the next few years.
Financial constraints have caused significant delays in the Artemis program, impacting the timing of future missions.
Despite budget challenges, NASA is continuing its efforts to advance towards establishing a base on the moon.
The success of Artemis 1 marks a significant step towards the United States' goal of lunar exploration.

Actions:

for space enthusiasts,
Support funding for NASA's Artemis program to ensure timely progress (implied).
</details>
<details>
<summary>
2022-12-18: Let's talk about a McCarthy quote you should remember.... (<a href="https://youtube.com/watch?v=ydwyQTJR1pE">watch</a> || <a href="/videos/2022/12/18/Lets_talk_about_a_McCarthy_quote_you_should_remember">transcript &amp; editable summary</a>)

Beau stresses the importance of remembering McCarthy's claim that the committee is the least legitimate in American history during future investigations and hearings.

</summary>

"The reason it's important to remember this as you are watching that final closing."
"That statement, about it being the least legitimate in American history, needs to be at the front of your mind."
"That statement, the least legitimate in American history, that needs to be remembered by the American people."

### AI summary (High error rate! Edit errors on video page)

The final committee hearing was rescheduled from Wednesday to Monday.
McCarthy called the committee the least legitimate in American history.
The committee provided vital information to the American people about attempts to overturn an election.
The American people gained insight into the plot to prevent a lawful transfer of power.
Despite McCarthy's statement, the committee may be viewed as one of the most significant in the last half century.
McCarthy's statement should be remembered during future investigations by the Republican Party.
McCarthy's immediate dismissal of the committee's legitimacy raises questions about his involvement or knowledge of the events.
The statement about the committee's legitimacy should be a prominent consideration during all future hearings and investigations.
McCarthy's preemptive judgment on the committee's legitimacy may suggest a desire to deflect from the real issues.
The American people should not overlook McCarthy's statement and should keep it in mind moving forward.

Actions:

for american citizens,
Keep McCarthy's statement in mind during future hearings and investigations by the Republican Party (implied).
</details>
<details>
<summary>
2022-12-18: Let's talk about Republicans addressing the elephant in the room.... (<a href="https://youtube.com/watch?v=2NvQvjZS30o">watch</a> || <a href="/videos/2022/12/18/Lets_talk_about_Republicans_addressing_the_elephant_in_the_room">transcript &amp; editable summary</a>)

Former House members demand accountability for lawmakers' roles in the January 6th insurrection, stressing the importance of upholding democracy through legal investigations and disciplinary actions.

</summary>

"The Constitution becomes just another antiquated cutesy document."
"Supporting a coup attempt will not be tolerated in Congress."
"Accountability is fundamental to the integrity of the legislative branch and democracy."

### AI summary (High error rate! Edit errors on video page)

Republicans addressed the January 6th insurrection with an open letter from former House members.
The campaign to overturn the election involved alarming actions by lawmakers, including advocating for martial law and interference with the electoral vote counting.
Lawmakers sought presidential pardons and shared the goal of preventing the lawful transfer of power.
The letter stresses the need for legal accountability for lawmakers' actions outside of their legislative duties.
The Office of Congressional Ethics is urged to thoroughly investigate members involved in the events leading up to January 6th.
Accountability is seen as fundamental to the integrity of the legislative branch and democracy.
The letter calls for lessons to be learned from accepting defeat at the ballot box.
The failure to hold those involved accountable risks history repeating itself.
A bipartisan effort is appreciated since the majority party in the House seems reluctant to address the issue.
The resilience of the U.S. system is emphasized as being dependent on the faith and belief of its citizens in the democratic process.

Actions:

for former lawmakers, concerned citizens,
Demand thorough investigation by the Office of Congressional Ethics into members involved in the events leading up to January 6th (suggested)
Support disciplinary actions by the House if appropriate (suggested)
Hold accountable those who participated in the insurrection to uphold democracy (implied)
</details>
<details>
<summary>
2022-12-17: Let's talk about a special kind of voter in Georgia.... (<a href="https://youtube.com/watch?v=2PopspP_DbI">watch</a> || <a href="/videos/2022/12/17/Lets_talk_about_a_special_kind_of_voter_in_Georgia">transcript &amp; editable summary</a>)

Blank ballot voters in Georgia's runoffs reflected ideological differences, not flaws, within the Republican Party.

</summary>

"They're not supposed to be, the candidates aren't supposed to be so close together that if you don't like one, you just vote for the other and you're going to get the same result."
"It shows that there are a growing number of people within the Republican side of things, or let's just say conservative."
"They have seen the other side and they realize that the MAGA movement, the Sedition Caucus, is bad for the Republican Party."
"It isn't that both parties ran candidates that were just unappealing to a whole bunch of Georgia voters."
"It's Libertarians who are unwilling to compromise with an authoritarian, or it's conservatives who just cannot continue to bend the knee to the MAGA movement."

### AI summary (High error rate! Edit errors on video page)

Explains the phenomenon of "blank ballot voters" who didn't vote for either Senate candidate in Georgia's runoffs.
Points out that these voters are predominantly Republicans or Libertarians who couldn't support either candidate due to ideological differences.
Emphasizes that it's not a sign of something being wrong but rather a reflection of political beliefs.
Notes that the issue lies in the Republican Party running a candidate that didn't appeal to a significant portion of its base.
Suggests that the growing number of conservatives and Libertarians rejecting the MAGA movement indicates a shift within the Republican Party.

Actions:

for conservatives, libertarians, republicans,
Reach out to disillusioned voters in your community and have open dialogues about their concerns (suggested).
Advocate for better candidate selection within political parties to avoid alienating voters (implied).
Support and amplify voices calling for change within political parties (implied).
</details>
<details>
<summary>
2022-12-17: Let's talk about Trump's cards and free speech.... (<a href="https://youtube.com/watch?v=Intc_pcyD3I">watch</a> || <a href="/videos/2022/12/17/Lets_talk_about_Trump_s_cards_and_free_speech">transcript &amp; editable summary</a>)

Beau exposes Trump's facade of a free speech advocate, revealing his history of silencing dissent and manipulation in politics.

</summary>

"It's a con. It's a lie."
"He is not a free speech advocate. He never has been."
"He is the antithesis of free speech."

### AI summary (High error rate! Edit errors on video page)

Trump's major announcement about hawking NFTs turned out ridiculous and did not change the story.
Many believe Trump's reign in the GOP is coming to an end, despite claims that he was only pretending.
Trump's speech about a free speech platform is viewed as a con and a lie, as he has a history of threatening journalists and dissent.
Trump plans to sign an executive order barring federal employees from labeling anything as misinformation or disinformation, raising questions about his own use of such terms.
People who mimic Trump's style are seen as manipulative, aiming to maintain relevance and deceive the public.
Those in the Republican Party who follow Trump or mimic his style might eventually realize they have been deceived.
Trump's attempts to monetize everything, from businesses to government positions, showcase his true character.
Researching Trump's history reveals numerous instances where he sought to silence dissent, contradicting his image as a free speech advocate.

Actions:

for politically aware individuals,
Research Trump's history of silencing dissent (suggested)
Stay informed on political manipulations and tactics (exemplified)
</details>
<details>
<summary>
2022-12-17: Let's talk about House Republicans wanting referrals (<a href="https://youtube.com/watch?v=MeH-4wEuvZs">watch</a> || <a href="/videos/2022/12/17/Lets_talk_about_House_Republicans_wanting_referrals">transcript &amp; editable summary</a>)

House Republicans plan to launch an investigation into Hunter Biden without an identified crime, mirroring an authoritarian approach and risking further Republican Party damage.

</summary>

"They have the person and they're looking for a crime."
"They don't have an identified crime that they're looking into."
"Show me the crime and then you go find the person."
"I think that's a little unethical, but it's not illegal."
"It will just continue to tie the entire Republican Party to his failure of an administration."

### AI summary (High error rate! Edit errors on video page)

House Republicans plan to launch an investigation into the president's son, indicating an authoritarian nature embraced by the Republican Party.
Republicans are compared to the Democratic Party's committee hearings into the January 6th events, a crime investigation, not an individual.
The proposed Republican hearing focuses on Hunter Biden's laptop without an identified crime, seeking the person first and then the crime.
Allegations against Hunter Biden in Ukraine fell apart when the timeline and narrative were examined.
Despite ethical concerns, Hunter Biden's actions do not appear to be illegal.
The Republican Party's approach seems to mirror Lavrentiy Beria's "show me the person, I'll show you the crime" tactic from Soviet state security, lacking evidence for alleged crimes.

Actions:

for political observers,
Examine political investigations critically (suggested)
Stay informed about political developments (suggested)
</details>
<details>
<summary>
2022-12-16: Let's talk about the last committee hearing and what to expect.... (<a href="https://youtube.com/watch?v=Q32BGQmVLJ0">watch</a> || <a href="/videos/2022/12/16/Lets_talk_about_the_last_committee_hearing_and_what_to_expect">transcript &amp; editable summary</a>)

Beau previews the final committee hearing on January 6th, where new evidence will be presented, and votes will be cast on legislative recommendations, referrals, and adopting the report, with the ball ultimately in DOJ's court.

</summary>

"This is the final tell them what you told them phase of things."
"Legislative recommendations aim to help mitigate the possibility of this happening in the future."
"The ball is in DOJ's court and what they decide to do at this point."

### AI summary (High error rate! Edit errors on video page)

Previewing the upcoming final committee hearing regarding January 6th.
The last hearing will be shorter and serve as a closing argument.
Expect new evidence presentation heavily relying on multimedia.
It will also be a working hearing with votes on legislative recommendations, referrals, and adopting the report.
Legislative recommendations aim to prevent similar events in the future.
Referrals may include criminal referrals, election commission referrals, and house ethics referrals.
The committee will vote on these referrals and adopting the report.
The hearing is scheduled for December 21st.
The outcome of the votes is likely predetermined.
The effects of the committee's work will depend on the Department of Justice's actions.

Actions:

for committee members, concerned citizens,
Attend or follow the updates from the upcoming final committee hearing (exemplified)
Stay informed about the legislative recommendations and referrals made during the hearing (exemplified)
</details>
<details>
<summary>
2022-12-16: Let's talk about the Captain Kori saga.... (<a href="https://youtube.com/watch?v=489B7RNudAQ">watch</a> || <a href="/videos/2022/12/16/Lets_talk_about_the_Captain_Kori_saga">transcript &amp; editable summary</a>)

Beau updates on a young YouTuber's journey to a silver play button, facing setbacks and support along the way.

</summary>

"all pirates end up in jail."
"You'll be able to recover from this. You will do fine."
"There are a lot of unkind comments that will come your way."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of a well-known pirate and directs viewers to a young man's YouTube channel aiming for a silver play button.
The young man's channel reached and surpassed its goal of 100,000 subscribers with the help of various channels, including Beau's.
The channel was later taken down by YouTube due to the age of the person associated with the email on the account.
After YouTube realized the young man's mom was actively involved, they reinstated the channel.
There is an Amazon wish list for the young man to create unboxing videos, which also includes items for the hospital.
Beau notes that the Amazon wishlist was taken down before filming but mentions a link to donate to Rainbow's Hospice.
Despite some issues with the Amazon account, the channel is back online, and there is a new goal mentioned.
Beau sends a message of encouragement to Captain Corey, assuring him he will recover from the setback and mentions the negativity that comes with being a YouTuber.
The young man is likely to face unkind comments but is reminded that people are aware of his channel.
Beau wraps up with a heartwarming message for the holiday season and signs off.

Actions:

for youtube viewers,
Support Captain Corey's YouTube channel by subscribing and engaging positively (exemplified).
Donate to Rainbow's Hospice (exemplified).
</details>
<details>
<summary>
2022-12-16: Let's talk about Louisiana, the AP, and movement.... (<a href="https://youtube.com/watch?v=ZOZyo6YOAu4">watch</a> || <a href="/videos/2022/12/16/Lets_talk_about_Louisiana_the_AP_and_movement">transcript &amp; editable summary</a>)

The Associated Press uncovered a possible cover-up within Louisiana law enforcement, leading to charges against officers for the death of Ronald Green and revealing the challenges of rebuilding community trust post-disturbing footage release.

</summary>

"The journalists at the Associated Press played a significant role in bringing about justice in this case by pursuing the story relentlessly."
"Rebuilding trust with the community will be challenging after the release of the incriminating body camera footage."
"The story will continue to gain widespread coverage, especially as more people view the footage."

### AI summary (High error rate! Edit errors on video page)

The Associated Press uncovered a possible cover-up within law enforcement in Louisiana related to the death of Ronald Green in May 2019.
Initially, Green's family was told he died in a car crash, but the footage revealed a brutal incident.
Federal charges against the officers involved were considered but not pursued due to concerns about proving willful misconduct in court.
The state has decided to pursue charges, resulting in the indictment of five officers for offenses ranging from negligent homicide to malfeasance in office.
The footage was so disturbing that even the state police's use of force instructor had nothing positive to say and viewed it as warranting charges.
The journalists at the Associated Press played a significant role in bringing about justice in this case by pursuing the story relentlessly.
Despite initial allegations of a cover-up, progress in the case has led to cracks in the "thin blue line" in Louisiana.
Rebuilding trust with the community will be challenging after the release of the incriminating body camera footage and the disturbing actions it captured.
The footage, while recently made public in May 2021, has been around for some time and showcases actions that no one could deem as normal in law enforcement.
Law enforcement officials in the state, who typically support officers, are now openly condemning their actions due to the severity of the footage.
The story will continue to gain widespread coverage, especially as more people view the footage, which is among the most disturbing seen by Beau.

Actions:

for louisiana residents, journalists, activists.,
Support investigative journalism by following and promoting the work of organizations like the Associated Press (exemplified).
Advocate for transparency and accountability within law enforcement by engaging with local officials and demanding justice for victims (exemplified).
Join community efforts to rebuild trust and foster positive relationships between law enforcement and residents (exemplified).
</details>
<details>
<summary>
2022-12-15: Let's talk about congressional comments on the 6th.... (<a href="https://youtube.com/watch?v=LHDpmYPWnx0">watch</a> || <a href="/videos/2022/12/15/Lets_talk_about_congressional_comments_on_the_6th">transcript &amp; editable summary</a>)

Beau questions the seriousness of a Congress member's comments and examines the implications of armed resistance fantasies on January 6th, prompting reflection on the true intentions behind such statements.

</summary>

"That is not how you win."
"Who is we?"
"They're telling you who they are."
"Still suggesting that we would have won indicates that she still wished they had."
"Whoever we is would have done that."

### AI summary (High error rate! Edit errors on video page)

Addresses comments made by a sitting member of Congress, referring to them as the "space laser lady" and discussing her statement about organizing to win with Steve Bannon on January 6th.
Questions the appropriateness of a Congress member joking about armed resistance to the US government.
Criticizes the notion of winning through armed entry and capturing symbolic buildings like Walmart, pointing out the impracticality of such actions.
Raises the question of who "we" refers to in the context of the Capitol storming, suggesting it includes the Congress member herself.
Examines the persistence of false claims and desires for victory despite debunked narratives and court losses, indicating a disregard for democracy and the Constitution.

Actions:

for congressional accountability advocates,
Contact elected representatives to express concerns about inappropriate statements made by officials (implied)
Stand against narratives that undermine democracy and the Constitution (implied)
</details>
<details>
<summary>
2022-12-15: Let's talk about Trump's major announcement.... (<a href="https://youtube.com/watch?v=t6ysC86SvE8">watch</a> || <a href="/videos/2022/12/15/Lets_talk_about_Trump_s_major_announcement">transcript &amp; editable summary</a>)

Trump's announcement serves as a distraction from his plummeting polling numbers, prompting a desperate attempt to re-energize his image and create buzz around himself as a product in the face of unfavorable ratings and primary losses.

</summary>

"His announcement is the event, not whatever he's going to announce."
"Among registered voters, he has a 31% favorability rating. 59% view him unfavorably."
"He cannot win with those numbers and he knows it."
"His only option to change the odds is to just out-Trump himself and go all in."
"I'm fairly certain that's what he believes."

### AI summary (High error rate! Edit errors on video page)

Trump's major announcement is a distraction from his plummeting polling numbers.
Speculation about the announcement ranges from returning to Twitter to running for Speaker of the House.
Trump's favorability ratings have hit an all-time low among registered voters and independents.
Only 20% of Republicans view Trump favorably, a significant drop from his prior popularity in the party.
Trump's numbers are even worse in primary polling, where he loses to DeSantis by a large margin.
Biden's favorability ratings are rising, adding to Trump's discomfort.
Trump's announcement aims to re-energize his image and create buzz around him as a product.
Trump may be realizing the legal jeopardy he faces and the unlikelihood of returning to the White House.
To change his odds, Trump plans to double down on being erratic and a maverick, believing it resonates with people.
Beau doubts the effectiveness of Trump's strategy but acknowledges it may guide his behavior.

Actions:

for political observers,
Monitor political developments closely (implied)
</details>
<details>
<summary>
2022-12-15: Let's talk about McCarthy vs McConnell.... (<a href="https://youtube.com/watch?v=iFSq77qnLO0">watch</a> || <a href="/videos/2022/12/15/Lets_talk_about_McCarthy_vs_McConnell">transcript &amp; editable summary</a>)

Beau breaks down the power struggle between McCarthy and McConnell within the Republican Party, cautioning against underestimating McConnell's political prowess.

</summary>

"It's like Freddie versus Jason. I mean, who do you really root for there?"
"McConnell will come out and say something very personable. He will come out and say, hey, you know, I've always liked young McCarthy. He's a good man. And then quietly work to destroy him."

### AI summary (High error rate! Edit errors on video page)

McCarthy, in pursuit of becoming Speaker of the House, has sacrificed his power by pandering to different factions within the Republican Party.
This has made McCarthy more of an errand boy for Republicans with strong social media followings, rather than a leader.
McCarthy has opposed reaching a budget agreement with the Democratic Party and has clashed with McConnell, the Republican Senate leader.
McConnell is known for his political acumen and understands the importance of US assistance to Ukraine in countering Russia.
McConnell is likely to push for more aid to Ukraine in any spending package, a stance that may create tension between him and McCarthy.
The conflict between McCarthy and McConnell resembles a showdown between Freddie versus Jason – a difficult choice.
McCarthy's allegiance to hardline Republicans, especially those with a strong social media presence, influences his positions.
In contrast, McConnell prioritizes power and is not swayed by social media dynamics.
Beau offers political advice to McCarthy, warning against picking a fight with McConnell if he wants to maintain his position.
He predicts that McConnell may publicly praise McCarthy while working behind the scenes to undermine him.

Actions:

for political observers,
Support political candidates who prioritize policies over social media presence (implied)
</details>
<details>
<summary>
2022-12-14: Let's talk about the end of the Special Master.... (<a href="https://youtube.com/watch?v=VuHleUQ4Bs0">watch</a> || <a href="/videos/2022/12/14/Lets_talk_about_the_end_of_the_Special_Master">transcript &amp; editable summary</a>)

The special master position is gone, leaving Trump exposed to direct access for building a case with potential erratic behavior ahead as he grapples with legal jeopardy.

</summary>

"Delay, delay, delay, delay, delay."
"Fox News talking points are not going to work in a federal courtroom."
"He might be at the point where he's beginning to understand the legal jeopardy that he's in."

### AI summary (High error rate! Edit errors on video page)

The special master position, tasked with going through Trump's documents, no longer exists after an appeal in the 11th circuit.
Department of Justice prosecutors now have direct access to tens of thousands of documents to build their case.
Trump's legal strategy of delay tactics is evident in requesting a special master, now proven to lack a legal basis.
Trump's team chose not to take the case to the Supreme Court, indicating a potential understanding of the gravity of the situation.
There are uncertainties regarding Trump's potential defense strategy and his realization that Fox News talking points won't suffice in court.
The documents case presents objective charges, leaving Trump in a precarious legal position.
Trump may be coming to terms with the legal jeopardy he faces, leading to potential erratic behavior as consequences sink in.

Actions:

for legal observers, political analysts.,
Stay informed on the legal developments surrounding the case (exemplified).
Monitor Trump's behavior and responses for potential insights into his understanding of the situation (exemplified).
</details>
<details>
<summary>
2022-12-14: Let's talk about the US, Ukraine, and Patriots.... (<a href="https://youtube.com/watch?v=5iIJ-69yefg">watch</a> || <a href="/videos/2022/12/14/Lets_talk_about_the_US_Ukraine_and_Patriots">transcript &amp; editable summary</a>)

Beau reports on the Department of Defense delivering the Patriot missile system to Ukraine, potentially signaling a shift towards more advanced equipment, in response to recent events near the Russian border.

</summary>

"Breaking news about the Department of Defense delivering the Patriot missile system to Ukraine."
"Putin's actions have led to updated air defense being positioned against their border."
"This may signal a willingness of the United States to put more technologically advanced stuff into play in Ukraine."

### AI summary (High error rate! Edit errors on video page)

Breaking news about the Department of Defense delivering the Patriot missile system to Ukraine to alleviate issues.
The system is an air defense system with a decent range to disrupt incoming threats in Ukraine.
The delivery of the system still needs to be signed off by the Secretary of Defense and Biden.
Recent events in Poland, where something fell, are being used as a pretext to get air defense closer to the Russian border.
Putin's actions have led to updated air defense being positioned against their border as a response to his invasion of Ukraine.
This move signals a shift towards potentially putting more technologically advanced equipment into play in Ukraine.
Beau speculates on the possibility of the United States introducing Reaper drones into the region as well.

Actions:

for global citizens,
Contact organizations supporting peace efforts in Ukraine (implied)
Stay informed about international relations and conflicts (implied)
</details>
<details>
<summary>
2022-12-14: Let's talk about legislation banning TikTok.... (<a href="https://youtube.com/watch?v=MBbPw-fTQDk">watch</a> || <a href="/videos/2022/12/14/Lets_talk_about_legislation_banning_TikTok">transcript &amp; editable summary</a>)

Legislation targeting TikTok and similar platforms is more about influence operations than espionage, focusing on protecting power and control at the top rather than individuals.

</summary>

"It's really about the ability of the Chinese government to potentially influence large segments of the US population by controlling what's in their feed."
"It's not really about protecting you. It's about protecting power and control up at the top."
"Make no mistake about it, this isn't really about protecting you. It's about protecting power and control up at the top."

### AI summary (High error rate! Edit errors on video page)

Legislation introduced aims to ban TikTok in the United States and any network substantially controlled by an opposition nation like China, North Korea, Russia, Iran.
Concern is that an opposition nation could use platforms like TikTok for coordinated influence operations targeting the US population.
The focus is on the Chinese government potentially controlling what appears in people's feeds to influence large segments of the US population.
Espionage concerns are secondary; TikTok is already banned in defense agencies for such reasons.
Likelihood of legislation passing in the current Congress is low due to limited time, but it may pass when the new Congress convenes.
Republicans are likely to support the legislation, viewing it as a way to combat the influence of China, especially on younger generations.
If the legislation passes the House and Senate, it's expected that President Biden will sign it.
If TikTok is impacted, it may be spun off as a public company based in the US or be replaced by a similar network.
Ultimately, the focus is more on maintaining power and control rather than protecting individuals.

Actions:

for legislative observers,
Monitor the progress of the legislation and advocate for transparency and accountability (suggested).
Stay informed about the potential impacts on social media platforms and influence operations (suggested).
</details>
<details>
<summary>
2022-12-13: Let's talk about the text messages of Mark Meadows.... (<a href="https://youtube.com/watch?v=GRJ1zUecFuo">watch</a> || <a href="/videos/2022/12/13/Lets_talk_about_the_text_messages_of_Mark_Meadows">transcript &amp; editable summary</a>)

Mark Meadows and members of Congress discussed extreme measures to keep Trump in office, including advocating for martial law, despite court rulings against their claims.

</summary>

"We are at a point of no return in saving our Republic. Our last hope is invoking Marshall, misspelled, law."
"Creating a dictatorship declaring martial law does not save a republic, it subverts it."
"Even after everything that happened on the 6th, there were still at least a few who were entertaining the idea of using the United States military to suppress the people."

### AI summary (High error rate! Edit errors on video page)

Mark Meadows' text messages have been made public, revealing his contact with over 30 members of Congress before the events of January 6th.
Meadows and others were discussing ways to keep Trump in office, disregarding the voice of the people and subverting democracy.
Representative Ralph Norman's message urging the invocation of martial law to save the Republic has gained attention, despite a misspelling.
The message was sent on January 17th, three days before Biden's inauguration, indicating ongoing attempts to overturn the election results.
Advocating for martial law to save the Republic goes against democratic principles and actually subverts them.
Meadows was being urged by a member of Congress to convince Trump to declare martial law after the events of January 6th.
Despite court rulings and the truth coming out, some individuals were still willing to use military force to suppress the will of the people.
The actions and intentions of those pushing for extreme measures after the election raise serious questions about their fitness for office.

Actions:

for concerned citizens, democracy advocates.,
Contact your representatives to ensure they uphold democratic principles (suggested).
Stay informed about political developments and hold elected officials accountable (implied).
</details>
<details>
<summary>
2022-12-13: Let's talk about my son's questions about Christmas cards.... (<a href="https://youtube.com/watch?v=007h2thmT5U">watch</a> || <a href="/videos/2022/12/13/Lets_talk_about_my_son_s_questions_about_Christmas_cards">transcript &amp; editable summary</a>)

Beau's son challenges the tradition of Christmas cards, pointing out their environmental impact, leading to a prediction of its eventual fade and potential backlash.

</summary>

"Tradition is just peer pressure from dead people."
"Just because we always did something one way isn't a reason to continue doing it that way if a better option presents itself."

### AI summary (High error rate! Edit errors on video page)

His son questioned the tradition of sending Christmas cards, opting for texts instead.
The son challenged the purpose of Christmas cards beyond tradition.
Beau realized his mistake in the convo but continued.
His son called tradition "peer pressure from dead people."
The environmental impact of Christmas cards was brought up by the son.
About 3,000 Christmas cards come from one tree, leading to half a million trees used per year in the U.S.
Beau predicts that the younger generation will move away from the tradition of sending Christmas cards.
He anticipates backlash, particularly from the Republican Party, labeling it as a "war on Christmas."

Actions:

for parents, environmentalists,
Opt for environmentally friendly alternatives to traditional practices (implied)
</details>
<details>
<summary>
2022-12-13: Let's talk about making a pirate's wish come true.... (<a href="https://youtube.com/watch?v=KVeNgGwaafE">watch</a> || <a href="/videos/2022/12/13/Lets_talk_about_making_a_pirate_s_wish_come_true">transcript &amp; editable summary</a>)

Beau talks pirates, dreams, and how viewers can help a young man's wish come true by subscribing to his YouTube channel.

</summary>

"What's a pirate's favorite letter? And you'd be forgiven for thinking that it's R. But you'd be wrong. It's really the C."
"Statistically speaking, if everybody who watches one of these videos in the first twenty-four hours, if y'all all subscribe to this young man's channel, well, his dream is accomplished, and you have helped make a wish come true."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introducing a different topic today about pirates and dreams.
Revealing a popular question about a pirate's favorite letter being "R" or "C."
Describing a brave young man with a YouTube channel who dreams of silver.
Mentioning how Captain Jack Sparrow is helping the young man halfway.
Urging viewers to subscribe to the young man's channel to help make his wish come true.
Comparing his own support with a jar of dirt and the viewers.
Encouraging everyone to subscribe within the first twenty-four hours to achieve the dream.
Assuring that achieving the remaining half of the dream won't be difficult.
Sharing the link to the young man's channel for those interested.
Stating that subscribing will make a significant difference.

Actions:

for youtube viewers,
Subscribe to the young man's YouTube channel (suggested)
</details>
<details>
<summary>
2022-12-13: Let's talk about a possible change in the air over Ukraine.... (<a href="https://youtube.com/watch?v=Ngn7rMk0z5Y">watch</a> || <a href="/videos/2022/12/13/Lets_talk_about_a_possible_change_in_the_air_over_Ukraine">transcript &amp; editable summary</a>)

The United States Air Force and the Pentagon are at odds over providing older drone technology to Ukraine, potentially altering the conflict dynamics significantly.

</summary>

"If it transfers, if this kind of transfer goes through, expect the entire face of that war to change very, very quickly."
"All of those static positions that the Russian military has been relying on, those are no longer obstacles for the Ukrainian military to overcome."
"Using them as close air support, using them in the role that Ukraine is probably going to use them in, there are probably some in the Pentagon who are willing to send the drones over just to see how it plays out."

### AI summary (High error rate! Edit errors on video page)

The United States Air Force and the Pentagon are in disagreement over providing older versions of Reapers to the Ukrainian military, which could have a significant impact on the conditions in Ukraine.
Concerns from the Pentagon include the potential for these drones to be shot down, leading to Russia gaining access to the technology within them and potentially sharing it with other countries like Iran and North Korea.
The Air Force is willing to provide these drones because they are not top-tier technology anymore but could still be a game-changer for Ukraine if deployed effectively.
Ukraine sees these drones as a way to shift the balance of power in the conflict and has been pushing to acquire them.
There are concerns about the technology falling into the wrong hands, but Ukraine has offered assurances that they won't target Russia with these drones and even provide targeting packages.
The Pentagon may be hesitant about having final approval on targeting packages in Ukraine and getting more involved in the conflict.
The use of drones in this manner, as close air support, is not how they were originally intended but could provide valuable insights for future drone development.
The outcome of this debate between the Air Force and the Pentagon could have a significant impact on the conflict in Ukraine and potentially change the dynamics of the war.

Actions:

for policy makers, military analysts,
Monitor developments in the debate between the United States Air Force and the Pentagon over providing drone technology to Ukraine (implied).
Stay informed about potential shifts in the conflict dynamics in Ukraine due to the outcome of this technology transfer (implied).
</details>
<details>
<summary>
2022-12-12: Let's talk about Trump, Whelan, and Bout.... (<a href="https://youtube.com/watch?v=MvAQQ9kYnsY">watch</a> || <a href="/videos/2022/12/12/Lets_talk_about_Trump_Whelan_and_Bout">transcript &amp; editable summary</a>)

Analyzing former President Trump's questionable statements, lack of moral framework, and attempts to use political agendas to paint himself as better at foreign policy, Beau criticizes Trump's foreign policy disaster.

</summary>

"Being more concerned about punishment rather than justice, that's not really a sign of good leadership right there."
"Trump was a foreign policy disaster. There is no other way to say it."
"His statements, they're not worth much."
"It's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analyzing former President Trump's statements on various issues.
Questioning Trump's attitude towards justice and leadership.
Examining Trump's claim of turning down a deal with Russia.
Criticizing Trump's prioritization of punishment over justice.
Pointing out the lack of moral framework in Trump's statements.
Refuting Trump's justification for not making a deal involving Paul Whelan.
Providing insights from Fiona Hill and Whelan's family on Trump's lack of interest in the case.
Mentioning Trump's past dealings with Saudi Arabia in comparison to his stance on Paul Whelan.
Dismissing Trump's statement as mere ranting without a moral philosophy.
Exploring the implications of Trump's claims and how the situation has evolved since 2018.
Accusing Trump of using Whelan for his own political agenda.
Condemning Trump as a foreign policy disaster even after leaving office.

Actions:

for critically-minded individuals,
Contact organizations working on foreign policy issues (implied)
Stay informed about political agendas and statements (implied)
</details>
<details>
<summary>
2022-12-12: Let's talk about Trump starting his own party.... (<a href="https://youtube.com/watch?v=4EOlhYJIQuY">watch</a> || <a href="/videos/2022/12/12/Lets_talk_about_Trump_starting_his_own_party">transcript &amp; editable summary</a>)

Beau predicts Trump's potential third party may not win the White House but could outlast him, driven by financial motivations and loyalty of his base.

</summary>

"It's not about winning. If he loses the Republican nomination, he knows he's going to lose the race."
"He's not going to win the White House running as a third party, but he might establish a third party that actually is around for longer than he is."
"It's all about the money with him."

### AI summary (High error rate! Edit errors on video page)

Predicts Trump will likely lose the Republican nomination based on current polling and trends.
Assumes Trump won't get indicted between now and then.
Suggests that Trump may start a third party if he loses the nomination to make money off his base.
Notes that Trump's base is loyal and may continue to support him financially.
Speculates that while Trump's third party won't win the White House, it could be more successful than other third parties in the US.
Proposes that a Trump party could attract current political officeholders to switch parties and join him.
Suggests that a Trump party might help the Republican party return to a more moderate conservative stance by drawing away far-right members.
Envisions the possibility of a Trump party winning House seats in deeply conservative areas.
Emphasizes that Trump's primary goal will be financial gain rather than political success.
Warns that even if Trump leaves the Republican Party, the ideology of Trumpism will likely persist with another similar candidate.
Concludes that Trump may not care about the long-term success of his potential third party as much as extracting money from his supporters.

Actions:

for political analysts, voters,
Monitor political developments and trends for potential impacts on the upcoming election (implied).
</details>
<details>
<summary>
2022-12-12: Let's talk about Libya, the US, and Pan Am.... (<a href="https://youtube.com/watch?v=5NRMQWX4oh4">watch</a> || <a href="/videos/2022/12/12/Lets_talk_about_Libya_the_US_and_Pan_Am">transcript &amp; editable summary</a>)

Massoud from Pan Am flight 103 case now in US custody, sparking speculation on Libyan involvement and potential motives amid economic struggles.

</summary>

"For a lot of younger people, the only comparison is what happened in September."
"Is there a reason for the government there to suddenly want to help the US. Yeah, there actually is."
"Everything's going to be brought back up again."

### AI summary (High error rate! Edit errors on video page)

Pan Am flight 103 incident, an airliner exploded over Lockerbie, Scotland in 1988, killing 259 in the air and 11 on the ground.
One of the key figures sought after in the Pan Am flight 103 case, Massoud, is now in US custody.
Speculation surrounds how Massoud ended up in US custody, with rumors suggesting Libyan involvement.
Libya, with competing administrations in Tobruk and Tripoli, may have assisted in extracting Massoud due to international pressure and financial struggles.
The National Oil Corporation in Libya recently sought foreign investors, hinting at economic desperation.
The motive behind Libya's possible involvement in handing over Massoud to the US could be to attract European investment.
Despite rumors implicating the Tripoli administration, the exact details of how Massoud was transferred to the US remain undisclosed.
The US government remains vague about the circumstances of Massoud's custody, fueling speculation.
Massoud's trial in the United States is anticipated to reignite interest in the Pan Am flight 103 incident.
The case is expected to resurface in headlines, bringing awareness to those unfamiliar with the event.

Actions:

for international observers,
Monitor updates on the Pan Am flight 103 case to stay informed and aware of developments (implied).
</details>
<details>
<summary>
2022-12-11: Let's talk about rules in Minnesota for cops.... (<a href="https://youtube.com/watch?v=5p2xujqRVEE">watch</a> || <a href="/videos/2022/12/11/Lets_talk_about_rules_in_Minnesota_for_cops">transcript &amp; editable summary</a>)

The post board in Minnesota proposes changes to deny or revoke cop certifications for hate-related activities, facing opposition from law enforcement associations and raising questions on public trust.

</summary>

"You shouldn't want bigots on your departments, plain and simple. They're a liability to the department, to other officers, and to the public."
"Having law enforcement associations oppose regulations that prohibit bigots from gaining life and death power over people decreases public trust."
"Make the calls. Y'all have enough legislators in your pocket. Back the blue, right?"

### AI summary (High error rate! Edit errors on video page)

The post board in Minnesota has proposed two changes to regulations that could deny or revoke a cop's certification if found spreading hate material online or participating in hate group forums.
These proposed changes also include requiring more information during background checks, such as asking if false information had been provided in court.
Law enforcement associations in Minnesota have opposed these changes, delaying their implementation since the first proposal in 2020.
The president of the Minneapolis Police Federation believes these changes won't increase public trust and may hinder recruiting efforts, despite aiming to keep bigots out of law enforcement.
Beau questions why law enforcement associations oppose regulations that aim to prevent bigots from having power over people, stating it decreases public trust.
The pushback from law enforcement is to argue that the legislature, not the post board, should make these decisions.
Beau challenges law enforcement associations to use their lobby power to push for these rule changes through the statehouse, expressing skepticism that they will do so.

Actions:

for law enforcement reform advocates,
Contact your legislators to support regulations denying certification for hate-related activities (implied)
</details>
<details>
<summary>
2022-12-11: Let's talk about Trump not being in contempt.... (<a href="https://youtube.com/watch?v=14NsKm90NR4">watch</a> || <a href="/videos/2022/12/11/Lets_talk_about_Trump_not_being_in_contempt">transcript &amp; editable summary</a>)

Department of Justice seeks to hold Trump accountable for potentially withheld documents, cautioning against viewing the lack of contempt as a victory.

</summary>

"Be careful what he wishes for, because you can't always get what you want."
"Trump's legal strategy of delay, delay, delay doesn't really work in a case where any leniency that he might receive kind of hinges on him not delaying."
"This is more of DOJ putting a shot across the bow and saying, you need to give us the stuff back."

### AI summary (High error rate! Edit errors on video page)

Department of Justice sought to hold Trump and legal team in contempt for not being forthcoming about potentially more documents.
DOJ's main goal is to remove any classified documents from circulation and retrieve them.
The judge did not hold Trump in contempt, prompting Trump world to claim victory.
DOJ may have cause to believe Trump still possesses documents, potentially leading to further legal action.
Trump's legal strategy of delaying may not work in his favor this time.
DOJ's actions are a warning shot to Trump, urging him to return any documents.
Viewing this as a win may lead to more actions from the Department of Justice.

Actions:

for legal analysts, concerned citizens,
Stay informed on the developments in the Trump document case and its implications (implied)
</details>
<details>
<summary>
2022-12-11: Let's talk about Hannity almost getting it.... (<a href="https://youtube.com/watch?v=llisL4_57Yg">watch</a> || <a href="/videos/2022/12/11/Lets_talk_about_Hannity_almost_getting_it">transcript &amp; editable summary</a>)

Beau outlines the Republican Party's need to evolve, embrace progressive ideas, and move away from fear-based strategies to avoid continued losses.

</summary>

"Stop looking for somebody to hate and try to make the country better."
"The Republican Party has to become more progressive. Or it will fade away."
"It's all fear-based."
"Their voters tend to show up."
"The Republican Party, they're going to continue to lose until they become a little bit more woke."

### AI summary (High error rate! Edit errors on video page)

Hannity outlines everything wrong with the Republican Party but fails to bring all the pieces together.
Democrats focus on getting enough ballots to win, not old-school campaign tactics like rallies and kissing babies.
Hannity acknowledges that Republicans need to embrace mail-in voting, despite years of opposition within the party.
Mail-in voting makes it easier to vote but doesn't create new voters for the Republican Party.
Republican campaign strategies like rallies and in-person events are outdated and unappealing to younger voters.
The Republican Party's establishment figures are seeking new marketing strategies but are missing the mark.
Online presence is weak for Republicans due to fact-checking exposing bad policies.
Policy issues, not marketing strategies, are at the core of the Republican Party's problems.
Acknowledgment within the Republican Party of the need for change, but failure to connect it to outdated policies.
Republican Party must evolve and embrace progressive ideas to avoid continued losses.

Actions:

for political activists and party strategists,
Embrace early voting and mail-in voting (implied)
</details>
<details>
<summary>
2022-12-10: Let's talk about Sinema leaving the Democratic party and the future.... (<a href="https://youtube.com/watch?v=9WNpAfTK1wc">watch</a> || <a href="/videos/2022/12/10/Lets_talk_about_Sinema_leaving_the_Democratic_party_and_the_future">transcript &amp; editable summary</a>)

Sinema's move to become an independent in Arizona raises questions about her true motives and potential impact on Democratic dynamics, prompting considerations on the future political landscape.

</summary>

"Most politicians, to some degree, are self-serving."
"There may be a chance that the Democratic Party does not run somebody against her."
"A grassroots effort to elevate other candidates starting now probably be pretty successful."
"Most of it depends on what the average Democrat in Arizona decides to do."
"Her votes for anything that is a major Democratic agenda piece, they're going to come at a cost."

### AI summary (High error rate! Edit errors on video page)

Sinema is leaving the Democratic Party in Arizona to become an independent, not planning to caucus with Republicans.
She hopes to maintain her committee positions and caucus with Democrats even after leaving the party.
Sinema's goal appears to be trying to regain the power she had before Warnock's win shifted the Senate majority to the Democrats.
Beau criticizes Sinema for not truly representing Democratic values and being self-serving in her actions.
He believes that Sinema's move may not be successful in the long run, especially in polarized election cycles.
Beau predicts that the Democratic Party may either placate Sinema or actively work against her to secure their majority.
There is a possibility of a progressive Democrat challenging Sinema in Arizona, depending on grassroots efforts.
Sinema's votes on major Democratic agenda items may come at a cost, with more focus on serving her contributors.
Beau suggests that the future outcomes depend on how the average Democrat in Arizona responds and organizes for a replacement candidate pursuing a progressive agenda.

Actions:

for arizona democrats,
Organize grassroots efforts to elevate progressive candidates in Arizona (implied)
Respond actively and organize for a replacement candidate pursuing a progressive agenda (implied)
</details>
<details>
<summary>
2022-12-10: Let's talk about Republicans at a crossroads with Trump.... (<a href="https://youtube.com/watch?v=UZFt_Bp0iNY">watch</a> || <a href="/videos/2022/12/10/Lets_talk_about_Republicans_at_a_crossroads_with_Trump">transcript &amp; editable summary</a>)

The Republican Party faces a critical decision: destroy Trump and his influence or risk losing political ground in 2024.

</summary>

"They either attempt to destroy him or they just accept that he runs the Republican Party."
"If they go that route, they will lose the House and the Senate."
"The Republican Party is kind of facing an existential threat."
"He's going to end up destroying you."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is at a crossroads, having run out of time to deal with Trump's influence.
They must choose between destroying Trump and his followers or falling in line obediently.
Trump's base remains energized and will impact not just the presidential primary but all other elections.
Despite Trump's toxicity among rank-and-file voters, his candidates may still win due to lack of visibility.
Republicans need to decide whether to fully embrace Trump or actively work to politically destroy him.
Failing to take a clear stance may result in losing the House and the Senate in 2024.
Delaying the decision will only empower MAGA candidates, leading to potential losses in the general election.
Lindsey Graham's warning about Trump's impact on the party may prove prophetic.

Actions:

for political strategists, republican party members,
Take a decisive stance on Trump's influence within the party (implied)
Actively work towards returning the Republican Party to its conservative roots (implied)
</details>
<details>
<summary>
2022-12-10: Let's talk about Republicans assessing Biden.... (<a href="https://youtube.com/watch?v=y14G0QArX24">watch</a> || <a href="/videos/2022/12/10/Lets_talk_about_Republicans_assessing_Biden">transcript &amp; editable summary</a>)

Republicans underestimate Biden; Newt Gingrich calls for rethinking defeating socialism; Leftists urged to push Biden for more socialism.

</summary>

"Opposition evaluation is always going to be more accurate than a self-evaluation."
"The Republican Party struggles to understand that their policies, not just their framing, contribute to their losses."
"If you're on the left, now might be the time to really start educating people on what that word means..."

### AI summary (High error rate! Edit errors on video page)

Republicans are underestimating President Joe Biden by focusing on his flaws and not recognizing his successes.
Biden has effectively handled the situation in Ukraine without deploying American troops.
Newt Gingrich suggests that Republicans need to rethink their approach to defeating socialism.
Opposition evaluation is more accurate than self-evaluation, according to Beau.
The Republican Party struggles to understand that their policies, not just their framing, contribute to their losses.
Leftists should push Biden to be more socialist, contrary to the Republican narrative.
Educating people on the true meaning of socialism can be beneficial for the left.

Actions:

for politically engaged individuals,
Educate people on the true meaning of socialism (implied)
Push Biden to adopt more socialist policies (implied)
</details>
<details>
<summary>
2022-12-09: Let's talk about an update on Moore County.... (<a href="https://youtube.com/watch?v=ytw-q142Wn4">watch</a> || <a href="/videos/2022/12/09/Lets_talk_about_an_update_on_Moore_County">transcript &amp; editable summary</a>)

Beau provides updates on Moore County, including casings found by law enforcement and upcoming search warrants, hinting at potential significant developments soon.

</summary>

"Things are reportedly starting to come back on and everything's starting to move along nicely."
"I have a feeling that we may see one of those, you know, America's most incompetent criminal things happening here."
"If they show any relationship whatsoever to that being a result in any way to this, this goes to a whole new level."

### AI summary (High error rate! Edit errors on video page)

Providing an update on Moore County and its progress after an incident.
Law enforcement found casings from the rounds, potentially linking the weapon to the incident.
The significance of the casings lies in leading to the location the shots were fired from.
Law enforcement is offering a reward for information leading to arrest and conviction.
They have applied for search warrants without disclosing the details to the media.
Beau predicts a potential twist in the search warrant scenario.
The investigation is ongoing, and there may be quick progress due to the search warrants.
The blackout in Moore County resulted in casualties.
If any casualties are linked to the incident, it escalates the situation significantly.
Expect more information soon on what happened and who's responsible.

Actions:

for law enforcement officials,
Provide any information related to the incident to law enforcement (suggested)
Stay informed and aware of updates regarding the investigation (implied)
</details>
<details>
<summary>
2022-12-09: Let's talk about Griner, Whelan, and Bout.... (<a href="https://youtube.com/watch?v=GMldJ2oXZqw">watch</a> || <a href="/videos/2022/12/09/Lets_talk_about_Griner_Whelan_and_Bout">transcript &amp; editable summary</a>)

Beau breaks down a recent trade deal involving Bout, Greiner, and Whalen, criticizing Trump's role and warning against telegraphing intentions in negotiations.

</summary>

"If anybody could have worked out that deal, Bout for Whelan, it would have been Trump."
"You can't telegraph your wishes to the opposition. It doesn't work."
"Stop being a national security risk, stop tying up the FBI's counterintelligence teams."
"This is a good trade. The people who don't were stirred up by somebody saying, 'oh, this is what I would have done,' when he literally didn't do it."
"In the process of turning Paul Whelan into a political pawn to make Biden look bad, you kept him away from his family longer."

### AI summary (High error rate! Edit errors on video page)

Analyzing a recent trade deal involving Bout, Greiner, and Whalen.
The Biden administration's deal with Putin to exchange Bout for Greiner.
Some argue that Whalen should have been part of the deal.
Victor Bout's value questioned due to outdated skill set and high visibility.
Doubts about Bout's future usefulness to Russia.
Addressing Trump's claims and actions regarding the trade deal.
Criticizing Trump's approach to handling international prisoner exchanges.
Warning against telegraphing intentions in negotiations.
Suggestions for how Trump could actually help in bringing Whalen home.
Critiquing the politicization of the trade deal and its impact on Whalen.

Actions:

for international relations watchers,
Contact your representatives to advocate for responsible and strategic approaches to international negotiations (implied).
Stay informed about international relations to better understand complex trade deals and diplomatic actions (implied).
</details>
<details>
<summary>
2022-12-08: Let's talk about the big shift in Ukraine and Russia.... (<a href="https://youtube.com/watch?v=Mhi1XwoTjKU">watch</a> || <a href="/videos/2022/12/08/Lets_talk_about_the_big_shift_in_Ukraine_and_Russia">transcript &amp; editable summary</a>)

Ukraine's strategic attacks inside Russia raise questions of effectiveness and potential consequences, amid conflicting opinions on the approach.

</summary>

"Accidents happen."
"This puts that in jeopardy."
"They're winning when it comes to the information space."
"It's a risk."
"They may be more anxious to put Russia on its back hill."

### AI summary (High error rate! Edit errors on video page)

Ukraine has engaged in multiple hits within Russia, targeting airfields and fuel storage facilities, raising questions about the equipment and technology being used.
Ukraine is suspected to be using repurposed Soviet-era drones or locally produced drones to conduct the attacks.
The accuracy of the attacks is attributed to the use of GPS satellites or laser targeting by teams on the ground.
Russia's air defense systems have shown weaknesses, with multiple successful hits by Ukraine over several days.
The attacks are causing Russia to divert resources and attention, potentially weakening their position in Ukraine.
There are conflicting opinions on whether these attacks by Ukraine are a good idea or not.
Concerns are raised about potential accidents or hitting illegitimate targets that could change the dynamics of the conflict.
Ukraine's success in the information war and propaganda is noted, but there are worries about potential escalation due to these attacks.
Beau expresses his opinion that while easy for some to criticize, those in Ukraine facing the conflict have the right to make decisions on their actions.
The situation in Ukraine and Russia is dynamic, with uncertainties about how these attacks will impact the ongoing conflict.

Actions:

for activists, policymakers, analysts,
Support organizations aiding civilians affected by the conflict (suggested)
Stay informed about the situation in Ukraine and Russia through reliable sources (implied)
</details>
<details>
<summary>
2022-12-08: Let's talk about good news in San Francisco.... (<a href="https://youtube.com/watch?v=7pWG40rHYLU">watch</a> || <a href="/videos/2022/12/08/Lets_talk_about_good_news_in_San_Francisco">transcript &amp; editable summary</a>)

San Francisco rejected a broad policy on armed robots due to public outcry and the need for stricter, more justifiable regulations.

</summary>

"We need to be looking at a way to stop that rather than new tools to do it."
"The overwhelming outcry came from the fact that the policy was just ridiculously broad."
"The key thing to remember when you're talking about anything like this is to actually read the policy, not the sound bites."
"The department will undoubtedly try to sneak this through again."
"It shouldn't be, well, maybe it would be justified. No, it has to already be deemed justified to take that thing out of the van."

### AI summary (High error rate! Edit errors on video page)

San Francisco Police Department proposed a policy allowing armed robots, which was initially approved but later voted down by the Board of Supervisors due to public outcry.
The chief of police defended the use of robots as a last resort option in deadly force situations to potentially prevent mass violence.
The policy faced criticism for being overly broad, allowing for the apprehension of criminals, which could lead to misuse and unjustified lethal force.
The public's demand for more restrictive policies stems from witnessing officers harming innocents and escaping accountability by citing training and policy.
Beau warns that the department may attempt to reintroduce the policy once public attention wanes and urges people to scrutinize future policies for their scope and potential consequences.
Deploying armed robots should be strictly limited to mass incidents, with clear justifications for lethal force already in place.
There is a concern that broad policies on armed robots could result in mission creep and increased loss of innocent lives due to hasty deployment of lethal force.

Actions:

for policy advocates,
Scrutinize future policies on armed robots for their scope and potential consequences (suggested)
Advocate for strict regulations on the deployment of armed robots only in mass incidents with clear justifications for lethal force (suggested)
</details>
<details>
<summary>
2022-12-08: Let's talk about dancing, history, and changing society.... (<a href="https://youtube.com/watch?v=PDbp1Rt1NPs">watch</a> || <a href="/videos/2022/12/08/Lets_talk_about_dancing_history_and_changing_society">transcript &amp; editable summary</a>)

Beau explains how drag shows by USGIs during World War II were part of a larger effort to change societal norms, particularly in accepting women as equals in the military.

</summary>

"They didn't fight, they didn't complain, they just did their jobs."
"It's hard for a lot of people to believe, but during World War II, the U.S. military, they were woke AF in a whole lot of ways."
"What's so wrong with just accepting?"

### AI summary (High error rate! Edit errors on video page)

Addresses a message critiquing his analogy comparing dancing Ukrainian soldiers to drag shows by American soldiers during World War II.
Explains the historical context of drag shows by USGIs during World War II and their connection to changing societal norms.
Mentions the establishment and success of the Women's Army Corps (WACC) in 1942.
Notes the significant contributions of women from WACC in advising men on makeup for performances.
Emphasizes the success of WACC and how it exceeded recruitment goals, leading to its expansion.
Describes the societal views on women during World War II as weaker, lesser, and unequal.
Explains why women were prohibited from performing on stage during the USGIs' drag shows.
Clarifies the different reasons behind the prohibition of women from participating in the shows by WACC and the War Department.
Points out that the shows aimed to push for societal acceptance of women as equals within the military.
Concludes by reflecting on the progressive changes within the U.S. military during World War II and their impact on society.

Actions:

for history enthusiasts, advocates for societal change,
Research and share more about the historical contributions of women in the military during World War II (suggested).
Support initiatives that aim to challenge societal norms and push for equality (implied).
</details>
<details>
<summary>
2022-12-08: Let's talk about McCarthy's first agenda item.... (<a href="https://youtube.com/watch?v=invG4JDJzsI">watch</a> || <a href="/videos/2022/12/08/Lets_talk_about_McCarthy_s_first_agenda_item">transcript &amp; editable summary</a>)

Kevin McCarthy plans to block defense spending in the House of Representatives until the vaccine mandate is removed, risking national security and weakening military readiness for future emergencies, despite high compliance rates among active duty members.

</summary>

"The Republican agenda is to weaken the U.S."
"McCarthy's actions are setting a trend for a readiness issue next time."
"The real issue lies with the National Guard and Reserve members who may not be vaccinated."
"The Republican Party's move to block the NDAA sets a dangerous trend."
"Given the Venn diagram of those refusing the vaccine and those with extremist beliefs, I'd want them gone anyway."

### AI summary (High error rate! Edit errors on video page)

Kevin McCarthy plans to use a roadblock to defense spending in the House of Representatives by withholding the NDAA until the vaccine mandate is removed.
Active duty military shows high compliance with the vaccine mandate, with over 97% reported compliance.
The real issue lies with the National Guard and Reserve members who may not be vaccinated, affecting readiness during emergencies.
Refusing the vaccine doesn't necessarily mean active duty members are against it; some may just be out of date.
The Republican Party's move to block the NDAA sets a dangerous trend for readiness in future emergencies.
McCarthy's actions are seen as weakening the U.S. military's readiness and serving the Republican Party's agenda.
There's concern that those refusing vaccines might have extremist beliefs, leading to further complications within the military.
Despite disagreements within the military, top brass remains firm on the importance of the vaccine mandate.
The Republican Party's strategy to obstruct the NDAA marks their initial step towards achieving their goals.
This move by the Republican Party is viewed as prioritizing their agenda over national security.

Actions:

for military personnel, policymakers,
Contact policymakers to express support for maintaining the vaccine mandate to ensure military readiness (suggested)
Join or support organizations advocating for the importance of vaccination within the military (implied)
</details>
<details>
<summary>
2022-12-07: Let's talk about what Warnock's win means.... (<a href="https://youtube.com/watch?v=sxaDU9fIyRU">watch</a> || <a href="/videos/2022/12/07/Lets_talk_about_what_Warnock_s_win_means">transcript &amp; editable summary</a>)

Warnock's win in Georgia shows the Democratic Party's need to mobilize and retain unlikely voters, while the Republican Party faces repercussions for not relying on data.

</summary>

"The enthusiasm that was harnessed during the midterms is not a guarantee for the Democratic Party."
"They have to get them active and keep them active."
"The last time there was a major race where that was significantly at play, it was people saying there's no way Trump's going to win."
"There will undoubtedly be rumors and claims, because that's just how it goes now."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Warnock won the Georgia race by 2.6 points, exactly as the polls predicted.
The Democratic Party shouldn't celebrate too much because the unlikely voters from the midterms didn't show up as expected.
The enthusiasm from the midterms is not a guaranteed win for the Democratic Party in 2024.
To mobilize the base, the Democrats should focus on delivering for the people who put them in office.
The Democratic Party needs to work on keeping the unlikely voters active and engaged.
There's a danger in assuming victory and not showing up to vote, similar to what happened in the last major race.
The Republicans are facing repercussions for their pundits' predictions that didn't come true due to their audience not looking at data.
The aftermath of the Republican loss may include rumors and claims, especially from the MAGA faction and the Sedition Caucus.
The Republican Party might not push back against these claims as a whole.

Actions:

for political analysts, activists,
Mobilize unlikely voters to ensure they stay active (implied)
Push back against rumors and claims to maintain truth (implied)
</details>
<details>
<summary>
2022-12-07: Let's talk about the Committee's referrals.... (<a href="https://youtube.com/watch?v=Y7Od12rzEEc">watch</a> || <a href="/videos/2022/12/07/Lets_talk_about_the_Committee_s_referrals">transcript &amp; editable summary</a>)

Beau questions the significance of committee news and speculates on its implications for Trump, hinting at potential future indictments and rough times ahead.

</summary>

"The referrals are kind of just extra, with the exception of the subpoena thing."
"I don't think that they [the committee] issue these referrals unless they were very certain that the Department of Justice was going to move forward."
"Trump's week has been pretty bad. I have a feeling his better days may be behind him."

### AI summary (High error rate! Edit errors on video page)

Beau questions the significance of news from the committee and whether it truly matters, considering the current circumstances.
The committee plans to make criminal referrals to the Department of Justice related to January 6, but Beau doubts if it will lead to any substantial action.
Despite Trump's lack of cooperation with subpoenas, Beau believes these referrals won't spark new actions from the Department of Justice.
Beau speculates that the committee might be issuing these referrals to take credit for actions already in progress.
He suggests that the committee's move could be a sign that the Department of Justice will proceed further with the investigation.
Beau predicts a rough road ahead for Trump, indicating that his tough week might be a sign of things to come.
The committee seems confident that the Department of Justice will act on the referrals, possibly leading to indictments in the future.
Beau contemplates whether the referrals are necessary or just a formality as the proceedings wind down.
He hints at a potential turning point for Trump as the investigation progresses.
Beau ends with a casual "y'all have a good day" sign-off.

Actions:

for political observers, trump supporters,
Follow developments in the Department of Justice's actions (implied)
Stay informed about the ongoing investigations (implied)
</details>
<details>
<summary>
2022-12-07: Let's talk about Trump companies being found guilty.... (<a href="https://youtube.com/watch?v=Z4Ys2dugL3U">watch</a> || <a href="/videos/2022/12/07/Lets_talk_about_Trump_companies_being_found_guilty">transcript &amp; editable summary</a>)

Trump companies found guilty of tax fraud in New York, paving the way for potential future legal troubles for the Trump family, with fines likely and civil cases looming.

</summary>

"Trump companies found guilty on all counts in New York for tax fraud."
"Former President Trump and his family not at risk of jail time, but a building block for targeting them."
"Outcome may impact civil cases, including a quarter-billion-dollar one in New York."
"Legal exposure greater in criminal case, but Mar-a-Lago documents case is more concerning."
"Plastic, plastic, a whole lot easier."

### AI summary (High error rate! Edit errors on video page)

Trump companies found guilty on all counts in New York for tax fraud involving the Trump Corporation and Trump Payroll Corporation.
Former President Trump and his family are not at risk of going to jail in this case, but legal analysts see this as a building block to target them as individuals in the future.
The case will likely result in fines for the companies rather than imprisonment.
Legal analysts suggest that this case could make it easier to pursue civil cases against the Trump Organization, including a significant quarter-billion-dollar case in New York.
The criminal tax case's outcome may impact the civil case involving asset valuation discrepancies and make it easier to proceed.
Some analysts believe that Trump's legal exposure is greater in the criminal case than in other ongoing cases, but Beau is skeptical and sees the Mar-a-Lago documents case as more concerning.
Beau advises Mar-a-Lago employees to stock up on plastic, hinting at a potential reaction from Trump to the news.

Actions:

for legal analysts,
Stock up on plastic (implied)
</details>
<details>
<summary>
2022-12-06: Let's talk about what Georgia can tell us about unlikely voters.... (<a href="https://youtube.com/watch?v=eyZNvSravnM">watch</a> || <a href="/videos/2022/12/06/Lets_talk_about_what_Georgia_can_tell_us_about_unlikely_voters">transcript &amp; editable summary</a>)

Georgia's Senate runoff holds significant implications for various political parties and factions, with potential insights into the impact of unlikely voters.

</summary>

"If Walker loses by more than three points, we can assume that those unlikely voters are now politically active."
"It's good for the Democratic Party and actual conservatives within the Republican Party, bad for the Trump faction."
"This may be a race that takes a little bit of time to count."

### AI summary (High error rate! Edit errors on video page)

Georgia's Senate runoff is a key focus due to its importance for various political parties and factions.
The Democratic Party wants the seat for a clear majority, while the Republican Party desires a power-sharing agreement.
Trump is desperate for Walker to win, while the Republican Party, lacking courage to oppose Trump, needs Walker to lose as well.
Walker's expected loss in the election may indicate the trend of unlikely voters being politically active.
If Walker loses by more than three points, it suggests that unlikely voters are now engaged politically.
Previous elections showed the impact of unlikely voters, potentially affecting polling accuracy.
The outcome will reveal the dynamics within political parties and the influence of different factions.
Walker's expected loss has varying implications for different groups within the political spectrum.
The election results may take some time to be finalized, potentially due to a close race.
The significance of this runoff election extends beyond political parties and influences the future political landscape.

Actions:

for political observers,
Monitor the results and election updates closely (implied).
Stay informed about the impact of the Georgia Senate runoff on future political trends (implied).
</details>
<details>
<summary>
2022-12-06: Let's talk about the most important climate issue.... (<a href="https://youtube.com/watch?v=UBYugVY4F3w">watch</a> || <a href="/videos/2022/12/06/Lets_talk_about_the_most_important_climate_issue">transcript &amp; editable summary</a>)

Beau addresses the urgency of climate change, stressing the need for widespread systemic changes to ensure a livable planet for future generations.

</summary>

"It's not that one thing has to change. It's that everything has to change."
"Either we come together and treat this as the global emergency that it is, or we don't."
"Humanity has to deal with climate change. There's not a choice here."
"If we don't act on climate change, they won't be there."

### AI summary (High error rate! Edit errors on video page)

Addresses the urgency of climate change and the need for immediate action.
Explains that there is no one magic solution but rather a need for widespread systemic changes.
Emphasizes that everything, from energy sources to agriculture practices, must change to combat climate change.
Talks about the importance of celebrating small wins in transitioning to sustainable practices.
Acknowledges that time is running out to mitigate the worst effects of climate change.
Asserts that systems will change one way or another, voluntarily now or abruptly later due to crisis.
Describes the inevitable transition away from fossil fuels, whether voluntarily or due to supply chain disruptions.
Stresses the necessity for humanity to address climate change now to ensure a livable planet for future generations.

Actions:

for climate activists and concerned citizens.,
Start celebrating small wins in transitioning to sustainable practices (exemplified).
Advocate for systemic changes in energy, waste management, and agriculture practices (suggested).
Join or support organizations working towards climate action (implied).
</details>
<details>
<summary>
2022-12-06: Let's talk about dancing, Ukraine, and history.... (<a href="https://youtube.com/watch?v=f-jDiyrT-RA">watch</a> || <a href="/videos/2022/12/06/Lets_talk_about_dancing_Ukraine_and_history">transcript &amp; editable summary</a>)

Beau explains the history of military dances and stress relief, debunking hyper-masculine myths by sharing the surprising tradition of drag shows among US troops in World War II.

</summary>

"I personally prefer the Pikachu dance."
"It's normal. It's good. It is a good thing to relieve stress in that way."
"The hyper-masculine soldier from the 1980s action flicks, that's a myth. It's not real."
"It's a myth."

### AI summary (High error rate! Edit errors on video page)

The Republican Party is upset about a Ukrainian soldier dancing, comparing them to the village elders from Footloose.
Some criticize the soldier's dance as unprofessional and question her role because she didn't have ammo pouches.
Beau defends the soldier's dance, pointing out that military traditions of chanting and dancing date back centuries.
Stress relief through dancing is common in high-stress jobs like the military or nursing.
Beau mentions how dancing, singing, and laughter have been activities before combat since ancient times.
He debunks the hyper-masculine myth by sharing a surprising history of drag shows among US troops in World War II.
Drag shows were so common that manuals with dress patterns were distributed for soldiers to perform in them.
Beau explains that stress relief activities like dancing are vital for maintaining morale and coping with difficult situations.
He contrasts the videos of Ukrainian soldiers dancing with those of another military displaying toughness through shirtless backflips and axe-throwing.
Beau challenges the myth of the hyper-masculine soldier from 1980s action films, stating it's a fabricated concept.

Actions:

for military personnel, history enthusiasts, social media users,
Support stress-relief activities in high-stress jobs (suggested)
Challenge hyper-masculine myths in your community (implied)
</details>
<details>
<summary>
2022-12-05: Let's talk about the Republican response to Trump's new push.... (<a href="https://youtube.com/watch?v=8YXDQ7mtL0Q">watch</a> || <a href="/videos/2022/12/05/Lets_talk_about_the_Republican_response_to_Trump_s_new_push">transcript &amp; editable summary</a>)

Republican Party's silence on Trump's desire to terminate the Constitution may turn them into the Anti-Constitution Party if they fail to stand up to him now.

</summary>

"The silence shows how performative their love of country is and the love of Constitution."
"If they don't stand up to him now, he very well might turn the Republican Party into the Anti-Constitution Party."
"Trump had dinner with somebody who is known for openly calling for dictatorship, and not long after, Trump is saying to terminate the U.S. Constitution."

### AI summary (High error rate! Edit errors on video page)

Republican Party's lack of response to Trump's statements about the US Constitution.
Most of the Republican Party has remained silent or adopted a strategy of ignoring Trump's remarks.
Some exceptions like Mike Turner and Liz Cheney have spoken out against Trump's ideas.
Representative Joyce exemplifies the strategy of ignoring Trump's statements and hoping he goes away.
Despite silence, Republicans like McCarthy claim to love the Constitution but fail to address Trump's remarks.
Trump's desire to terminate the Constitution is a serious concern that Republicans must confront.
Standing up to Trump is necessary to prevent him from dominating the Republican primary.
Failure to oppose Trump could result in turning the Republican Party into the Anti-Constitution Party.
Trump's statements are not just hyperbole; he doubled down on his desire to terminate the Constitution.
Republicans need to push back strongly against Trump to prevent him from pursuing his extreme ideas.

Actions:

for republicans, political activists,
Call out and challenge political leaders who fail to oppose dangerous ideologies like terminating the Constitution (implied).
Support Republican figures like Mike Turner and Liz Cheney who speak out against extreme ideas (implied).
</details>
<details>
<summary>
2022-12-05: Let's talk about news sources and trade magazines.... (<a href="https://youtube.com/watch?v=UD5-nSwi0C0">watch</a> || <a href="/videos/2022/12/05/Lets_talk_about_news_sources_and_trade_magazines">transcript &amp; editable summary</a>)

Beau explains his diverse information sources, including trade journals, to gain a well-rounded understanding and predict industry influences on politics.

</summary>

"Every outlet has a bias."
"There's a lot that can be gleaned out of these sources."
"You can't really tell the future, but you have a much, much more informed guess."

### AI summary (High error rate! Edit errors on video page)

Starts by discussing his sources for staying informed, prompted by a question on Twitter.
Begins his daily information consumption with the Associated Press (AP) due to its factual reporting with minimal analysis.
Moves on to sources with different biases, including major news outlets and social media for public reactions.
Advocacy organizations and trade journals are also part of his information sources for diverse perspectives.
Emphasizes the importance of reading polls, surveys, and studies to inform opinions effectively.
Explains the concept of trade journals using examples like The Hollywood Reporter and Billboard for specific industries.
Notes that trade journals are biased towards the industries they represent but contain valuable information.
Suggests that insights from trade journals, especially when linked with sources like AP, can enhance understanding, particularly in areas like US politics.
Points out that industry suggestions in these publications can predict lobbying actions and political stances.
Encourages utilizing such sources for a broader and more informed perspective on various topics, including politics.

Actions:

for information seekers,
Follow diverse sources of information to gain a comprehensive understanding (exemplified)
Read trade journals related to specific industries to uncover valuable insights (exemplified)
Analyze industry suggestions in trade magazines to predict lobbying actions (implied)
</details>
<details>
<summary>
2022-12-05: Let's talk about Moore County, North Carolina.... (<a href="https://youtube.com/watch?v=Opp09pm_r68">watch</a> || <a href="/videos/2022/12/05/Lets_talk_about_Moore_County_North_Carolina">transcript &amp; editable summary</a>)

Electrical substations offline in Moore County, potential domestic terrorism, FBI joins investigation, and lives at risk due to power outage in freezing temperatures.

</summary>

"It may not matter. It may not matter."
"If any deaths occur due to the outage, individuals involved may quickly cooperate with law enforcement."
"There are a lot of developments here. There are a lot of things that can change all of the math in how this plays out."

### AI summary (High error rate! Edit errors on video page)

Electrical substations in Moore County, North Carolina were taken offline abruptly, leaving tens of thousands without electricity.
The sheriff's department doesn't have suspects yet, but the FBI has joined the investigation due to resource limitations.
Speculation on social media about the incident is compelling but not evidence; investigators are needed to verify.
The incident may be considered domestic terrorism depending on the intent of those responsible.
Moore County has a significant population over 65, and the power outage occurred in freezing temperatures.
North Carolina has felony murder charges, and the outage's impact on lives could be viewed as causal.
Those responsible likely didn't anticipate the potentially fatal consequences of cutting power to older residents.
If any deaths occur due to the outage, individuals involved may quickly cooperate with law enforcement.
Official information and details on the investigation are limited at this point.
The FBI's involvement suggests the coordinated incident is more complex than simple vandalism.

Actions:

for community members, investigators.,
Reach out to local authorities with any information that might assist the investigation (implied).
Ensure the well-being of vulnerable community members during power outages (implied).
</details>
<details>
<summary>
2022-12-04: Let's talk about the secret in Trump's statement.... (<a href="https://youtube.com/watch?v=YgkYUNJIDWI">watch</a> || <a href="/videos/2022/12/04/Lets_talk_about_the_secret_in_Trump_s_statement">transcript &amp; editable summary</a>)

Beau reveals Trump's authoritarian desires and calls for accountability from his supporters and endorsed candidates, signaling a critical wake-up call for the Republican Party.

</summary>

"Terminate articles to include the Constitution."
"You cannot support MAGA and support the Constitution because MAGA is telling you they're gonna terminate the Constitution."
"This needs to be the biggest wake-up call for the Republican Party."
"Every person who got an endorsement from him needs to answer for this."
"They all need to answer."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's statement and its significance.
Trump's statement reveals his desire for autocratic control and subversion of the Constitution.
Trump's call to terminate articles, including those in the Constitution, based on false claims of fraud.
The statement exposes Trump's true intentions and authoritarian tendencies.
Individuals who supported Trump and his endorsed candidates share his beliefs.
Urges people to wake up to the reality of Trump's intentions and the danger they pose.
Calls for a wake-up call within the Republican Party regarding Trump's statement.
Advocates for holding political candidates accountable for their stance on terminating the Constitution.

Actions:

for politically engaged citizens,
Question political candidates on their stance regarding Trump's statement about terminating the Constitution (suggested).
Demand direct answers from candidates on whether they agree or disagree with Trump's statement (suggested).
Initiate focused discourse on Trump's statement and its implications within political circles and communities (implied).
</details>
<details>
<summary>
2022-12-04: Let's talk about the X-37 and space.... (<a href="https://youtube.com/watch?v=rskxiGffQxY">watch</a> || <a href="/videos/2022/12/04/Lets_talk_about_the_X-37_and_space">transcript &amp; editable summary</a>)

Beau delves into the secretive world of the X-37 space plane, detailing its lengthy time in orbit, experimental purposes, and the mystery shrouding its undisclosed projects.

</summary>

"908 days in orbit. It's a pretty big achievement."
"There are two, we think, of these aircraft."
"It's tiny."

### AI summary (High error rate! Edit errors on video page)

Talks about the X-37, a secretive space plane that recently returned to Earth after 908 days in orbit.
Describes the X-37 as a small, baby space shuttle around 30 feet long.
Mentions that there are two X-37 aircraft owned by the US government, with a combined time in space of over 10 years.
Explains that the X-37 conducts experiments in space, including testing the effects of space on seeds and experimenting with electromagnetic propulsion.
Notes the secrecy surrounding the X-37, with public-facing experiments and undisclosed projects.
Traces the history of the X-37 from a joint NASA DARPA project to a Department of Defense project.
Compares the US's X-37 project to a similar Chinese craft, indicating the US's decade-long lead in this technology.

Actions:

for space enthusiasts, researchers, military analysts.,
Research more about the X-37 and its experiments (suggested).
Stay informed about advancements in space technology (implied).
</details>
<details>
<summary>
2022-12-04: Let's talk about running the country like a business.... (<a href="https://youtube.com/watch?v=p5opg_D1YjY">watch</a> || <a href="/videos/2022/12/04/Lets_talk_about_running_the_country_like_a_business">transcript &amp; editable summary</a>)

Beau challenges the notion of running the country like a business and debates its implications, revealing its flaws and negative impact on the working class.

</summary>

"Most of you traded your country for a red hat."
"Why do you like him? Because he ran the country like a business."
"Running the country like a business is bad for the working class."

### AI summary (High error rate! Edit errors on video page)

Enters a gas station late and encounters a man wearing a red hat, triggering a political debate.
Engages in a 10-15 minute debate about Trump running the country like a business.
Questions the notion of running the country like a business, challenges the popular slogan.
Points out the flaws in the concept of treating the government like a business based on profit.
Suggests that running the country like a business may not be beneficial for the working class.
Encourages questioning the idea whenever it is brought up to reveal its flaws.

Actions:

for american citizens,
Question the concept of running the country like a business when discussing politics (exemplified).
Encourage critical thinking and questioning of political slogans in everyday interactions (exemplified).
</details>
<details>
<summary>
2022-12-03: Let's talk about what the Senate can learn from Roman roads.... (<a href="https://youtube.com/watch?v=yyC2DJO7vQ4">watch</a> || <a href="/videos/2022/12/03/Lets_talk_about_what_the_Senate_can_learn_from_Roman_roads">transcript &amp; editable summary</a>)

Roman roads from ancient times are linked to modern economic prosperity, stressing the critical role of infrastructure in driving economic growth and the urgent need for sustainable investments in the US.

</summary>

"Infrastructure causes economic activity. It causes economic growth."
"Infrastructure means money and it doesn't matter what kind you're talking about."
"We're at a crossroads and we need to make the turn to start building infrastructure that is not only economically viable, but also sustainable."

### AI summary (High error rate! Edit errors on video page)

Roman roads built thousands of years ago are linked to economic prosperity today based on a study using satellite images taken at night.
The study found that living near Roman roads meant living in a more economically prosperous area.
There's a unique link between Roman roads from ancient times and economic activity today, although a causal relationship hasn't been established yet.
The causal relationship is unclear - it's unknown whether Roman roads caused prosperity or just connected areas already prospering.
Infrastructure is a key driver of economic activity and growth, as shown by the impact of Roman roads on economic prosperity.
The survey found that areas in the US with less infrastructure tend to have lower economic prosperity.
Infrastructure, whether it's roads, railroads, or broadband, plays a vital role in creating prosperity.
Maintaining infrastructure is not just about the ability to move; it significantly impacts economic activity.
The US needs to invest in sustainable infrastructure to avoid falling behind and causing damage to the country and the world.
Making the decision to invest in viable and sustainable infrastructure is critical for long-term impact.

Actions:

for policy makers, community planners,
Invest in local infrastructure projects (implied)
Advocate for sustainable infrastructure development (implied)
Support policies promoting economic growth through infrastructure investments (implied)
</details>
<details>
<summary>
2022-12-03: Let's talk about newer members of NATO.... (<a href="https://youtube.com/watch?v=uJiCUqetS0A">watch</a> || <a href="/videos/2022/12/03/Lets_talk_about_newer_members_of_NATO">transcript &amp; editable summary</a>)

Beau explains the shift in NATO's defense stance due to Russia's actions in Ukraine, strengthening NATO against Russia and involving newer members in a more active role.

</summary>

"With hundreds of thousands of troops moving to actual Eastern border inside former Eastern Bloc NATO members, now I feel like NATO is considering them equal members."
"Almost every single one of the new members of NATO was like, yeah, let's do that. They didn't hesitate."
"For the older members of NATO, we got to see newer members act like NATO."
"That definitely strengthens NATO's hand to the detriment of Russia."
"It is one more card that Russia got dealt that doesn't go with anything else in its hand."

### AI summary (High error rate! Edit errors on video page)

Explains the impact of Russia's actions in Ukraine on NATO and Eastern European countries.
Describes how newer former Eastern Bloc NATO members were considered secondhand members, serving as a buffer during the Cold War.
Notes the shift in NATO's stance towards defending territorial integrity following Russia's troop movements to the Eastern border inside former Eastern Bloc NATO members.
Acknowledges that NATO's behavior traditionally involves absorbing the first hit in defensive alliances.
Points out that newer NATO members were seen as speed bumps or tripwires, absorbing initial hits to buy time for NATO to respond.
Mentions the change in troop positions due to Russia's invasion of Ukraine, leading to more assets moving further east.
Emphasizes the willingness of newer NATO members to assist Ukraine geopolitically without hesitation.
Suggests that the behavior of newer NATO members has strengthened NATO's position against Russia.
Raises awareness of the geopolitical implications and NATO's resolve in response to Russia's actions.
Concludes by discussing the impact of recent events on international dynamics, referencing a poker game analogy.

Actions:

for international policymakers,
Support Eastern European countries in ensuring their territorial integrity (exemplified)
Advocate for continued solidarity and support within NATO (implied)
</details>
<details>
<summary>
2022-12-03: Let's talk about Sweden, Finland, and cold weather gear.... (<a href="https://youtube.com/watch?v=hVGngD8fSNg">watch</a> || <a href="/videos/2022/12/03/Lets_talk_about_Sweden_Finland_and_cold_weather_gear">transcript &amp; editable summary</a>)

Ukraine and Russia face gear shortages, but Finland and Sweden's aid could shift the conflict dynamics over the winter.

</summary>

"If your troops are fighting to stay warm, they have absolutely no interest in fighting the opposition."
"While Russian troops are huddled together trying to stay warm, Ukrainian troops can actually be out there maneuvering."
"In the absence of this equipment showing up, we're probably not going to see a whole lot of movement until spring."

### AI summary (High error rate! Edit errors on video page)

Ukraine and Russia struggling to obtain cold weather gear for their troops.
Cold weather gear like jackets, gloves, sleeping bags, tents, and stoves are vital for troops.
Troops fighting to stay warm won't be focused on fighting the opposition.
Lack of cold weather gear could lead to static lines and favor Russia.
Finland and Sweden are providing new packages of cold weather gear to Ukraine.
Finland's package is worth around $55-$60 million, and Sweden's is around $275-$285 million.
Finland and Sweden have good cold weather gear and can spare some due to their military structure.
With adequate cold weather gear, Ukrainian forces can have an edge over Russian troops.
NATO likely prioritizing getting Ukrainian forces the needed clothing and equipment for winter.
Without proper gear, movement in the conflict may be minimal until spring.

Actions:

for military support organizations,
Provide cold weather gear to Ukrainian forces (implied).
Support military organizations sending aid (implied).
</details>
<details>
<summary>
2022-12-02: Let's talk about the 11th circuit, Trump, and the separation of powers.... (<a href="https://youtube.com/watch?v=JV5xXatomjg">watch</a> || <a href="/videos/2022/12/02/Lets_talk_about_the_11th_circuit_Trump_and_the_separation_of_powers">transcript &amp; editable summary</a>)

Beau talks about the 11th Circuit's ruling on Judge Cannon's order, deeming the special master unnecessary and a violation of separation of powers, with implications for Trump's legal challenges. Despite setbacks, the DOJ is expected to accelerate post-ruling, leading to further developments.

</summary>

"It violates the separation of powers as outlined in the Constitution."
"It's just delay, delay, delay, delay, delay."
"There are a limited number of ways in which this can end up turning out favorably."
"They're just starting to kind of feeling like they're moving in the right direction."
"This is definitely not the last we're going to hear about this."

### AI summary (High error rate! Edit errors on video page)

Talking about the 11th Circuit, a panel composed entirely of GOP appointees, and their decision on Judge Cannon's order regarding the special master.
The 11th Circuit panel determined that the special master is unnecessary and violates the separation of powers outlined in the Constitution.
They criticized the idea of allowing any subject of a search warrant, or only former presidents, to block government investigations post-warrant execution.
Emphasizing that the ongoing investigation is under the executive branch, and judicial interference at this stage breaches the separation of powers.
Judge Cannon's order granting the special master was vacated, considered a delay tactic from Trump in legal matters.
Despite the delay tactic working for a while, it's no longer holding up, allowing the FBI and Department of Justice to proceed without the imposed stipulations.
This ruling signifies a significant setback for Trump in the documents case, with limited favorable outcomes for him diminishing rapidly.
Anticipating an acceleration in the case post-midterms as the DOJ gains momentum, irrespective of the recent ruling's effects.
The DOJ is expected to proceed more swiftly as they perceive things moving positively now that obstacles are being cleared.
Implying that the developments post-ruling will continue to unfold, indicating ongoing updates on the matter.

Actions:

for legal analysts, political enthusiasts,
Monitor updates on legal proceedings (implied)
Stay informed about the implications of separation of powers in legal cases (implied)
</details>
<details>
<summary>
2022-12-02: Let's talk about San Francisco and robots.... (<a href="https://youtube.com/watch?v=BMlpjsEAx40">watch</a> || <a href="/videos/2022/12/02/Lets_talk_about_San_Francisco_and_robots">transcript &amp; editable summary</a>)

San Francisco PD's broad policy allowing robots for lethal force is a horrible idea that will inevitably lead to misuse, urging residents to speak out against it.

</summary>

"This is a horrible, horrible idea. This will go bad."
"Civilian police departments should not need to be able to mete out lethal force via a robot."
"It will become too tempting to use it in situations in which they do not have the information necessary."

### AI summary (High error rate! Edit errors on video page)

San Francisco PD is considering using robots to deploy lethal force.
The policy allowing the use of robots for criminal apprehensions is incredibly broad.
There are very few situations where using robots for lethal force is remotely acceptable.
The policy lacks limitations and allows too much room for misuse.
Only the highest trained individuals with the best information should handle such technology.
San Francisco residents are urged to make their voices heard on this issue.
Civilian police departments should not have the ability to use robots for lethal force.
The misuse of robots in this manner is inevitable and will result in lost innocence.
The lack of necessary information and training makes this policy a dangerous idea.
San Francisco's Board of Supervisors needs to reconsider this decision.

Actions:

for san francisco residents,
Make your voice heard on the issue (suggested)
Advocate against the use of robots for lethal force (implied)
</details>
<details>
<summary>
2022-12-02: Let's talk about Biden wanting to change the primaries.... (<a href="https://youtube.com/watch?v=CwVTvKVPRQ0">watch</a> || <a href="/videos/2022/12/02/Lets_talk_about_Biden_wanting_to_change_the_primaries">transcript &amp; editable summary</a>)

The Democratic Party considers changing the primary order to better represent its diverse base and capitalize on internal party dynamics.

</summary>

"Moving South Carolina up and having the lead off a state that is diverse, that will give a much more accurate read on how the rest of the country thinks."
"Setting the tone and getting candidates that energize all of the base rather than just the lily white areas is probably a good idea."

### AI summary (High error rate! Edit errors on video page)

The Democratic Party is discussing changing the order in which states vote during the primaries, specifically moving up South Carolina as the first state.
The early primaries play a critical role in setting the tone for a candidate's campaign and can essentially make or break it.
Currently, states like New Hampshire and Iowa, which are predominantly white, kick off the primary season, leading to a lack of diversity in representing the Democratic Party's base.
South Carolina, with a more diverse population, could provide a more accurate reflection of the party's broader demographics and ideologies.
Moving South Carolina up could indicate a shift in the Democratic Party establishment's recognition of the importance of Black Americans in the party's success.
The goal behind changing the primary order is to make the process more representative and responsive to what the party's base actually wants.
Opponents of this change, such as New Hampshire and Iowa, may resist due to losing their early primary status and the influence it carries.
New Hampshire, for instance, has laws to ensure its primary remains first or among the top three due to its perceived importance in setting the campaign tone.
Despite potential opposition, Beau believes that this change is necessary for the Democratic Party to capitalize on opportunities presented by the Republican Party's internal struggles.
Shifting the primary order could help in selecting candidates who energize the entire base, including those from more diverse areas within the party.

Actions:

for political activists, democratic party supporters,
Contact local Democratic Party officials to express support for changing the primary order to better represent diversity (suggested)
Attend or organize community meetings to advocate for a more inclusive primary process (implied)
</details>
<details>
<summary>
2022-12-01: Let's talk about why it's important to understand US capabilities.... (<a href="https://youtube.com/watch?v=E1sXe9zNau0">watch</a> || <a href="/videos/2022/12/01/Lets_talk_about_why_it_s_important_to_understand_US_capabilities">transcript &amp; editable summary</a>)

Beau explains the objective reality of U.S. military superiority and advocates for a shift towards a humanitarian approach in global conflicts, aiming to save lives and empower affected nations.

</summary>

"Facts are not inherently good or bad. It's how they're deployed, just like any other technology."
"The technology has advanced to the point that humanity's ability to conduct violence has advanced to the point where non-state actors are capable of engaging in a low-intensity conflict that undermines even the highest levels of technology."
"It's a statement that can be used to inoculate against it."
"If that style was used, being the world's EMT instead of being the world's policeman, if that's what was done, we probably wouldn't need the war."
"The United States has an unparalleled ability to wage war."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of understanding the U.S.'s military advancement.
Counters the notion that discussing U.S. military superiority is imperialist propaganda.
Points out that discussing military technology facts is not propaganda but an objective reality.
States that the U.S. is decades ahead in military technologies compared to other nations.
Suggests that this awareness could actually reduce defense expenditures.
Raises questions about why conflicts haven't ended favorably despite U.S. military superiority.
Proposes a shift towards a more humanitarian approach post-conflict.
Advocates for a role as the world's "EMT" rather than a global police force.
Emphasizes that deploying facts appropriately can lead to positive outcomes.
Encourages reflection on more peaceful approaches to global conflicts.

Actions:

for policy advocates,
Advocate for a shift towards a humanitarian approach in global conflicts (implied)
Support policies that prioritize post-conflict rebuilding efforts over military occupation (implied)
</details>
<details>
<summary>
2022-12-01: Let's talk about the railroads.... (<a href="https://youtube.com/watch?v=SktGz0IL0nw">watch</a> || <a href="/videos/2022/12/01/Lets_talk_about_the_railroads">transcript &amp; editable summary</a>)

Railway workers face negotiations over scheduling, with potential economic repercussions, as government intervention favors industry over workers' quality of life.

</summary>

"If your job is so vital that if you don't show up, the whole country stops, you probably deserve a decent quality of life."
"The administration and Congress had the ability to stand with the workers, but they did the absolute bare minimum."
"It truly damaged his standing there and the Democratic Party standing as a whole with organized labor."
"Nothing. Nothing. It doesn't cause billions of dollars per day in economic damage to the United States. That happens if the workers don't show up."
"Biden has a pretty strong labor record. And in this situation, he's kind of sided with the bosses."

### AI summary (High error rate! Edit errors on video page)

Railway workers and bosses are negotiating a new contract, primarily focusing on scheduling issues rather than money.
Workers are treated poorly, expected to work long hours without adequate time off.
There was a tentative agreement that the unions voted down, leading to the possibility of a strike on December 9th.
A strike could cause billions of dollars in economic damage to the US, prompting government intervention.
The House passed a bill imposing the rejected labor deal and providing additional sick days, now awaiting Senate approval.
Bernie Sanders will play a key role in pushing the legislation through the Senate.
Beau criticizes the Republican Party for prioritizing political power over the well-being of the economy and people.
The Democratic Party's trust in Republicans to act sensibly is questioned by Beau.
Beau expresses disappointment in Biden siding with railway companies over workers, damaging his standing with organized labor.
The moral aspect is emphasized, where workers' absence causes economic harm while CEOs' absence does not.

Actions:

for congress members,
Contact Congress members to advocate for fair treatment of railway workers (suggested)
Stay informed about the ongoing negotiations and potential strike actions (suggested)
</details>
<details>
<summary>
2022-12-01: Let's talk about Trump's new campaign policy.... (<a href="https://youtube.com/watch?v=ZaBmktLkXc0">watch</a> || <a href="/videos/2022/12/01/Lets_talk_about_Trump_s_new_campaign_policy">transcript &amp; editable summary</a>)

Policy changes at Trump's campaign reveal a decline in his independence and decision-making abilities, potentially disappointing his supporters.

</summary>

"Trump's draw for a lot of people was that he was seen as a maverick."
"That excitement that occurred during that first campaign and the shielding that was put up around him when he was in the White House, it's all kind of tumbling down around him."
"This news is just a bad sign for his campaign."
"The campaign is running him."
"There's going to be another little letdown as they realize the person they framed as almost a deity is, according to his campaign staff, somebody who can't even decide who to have a meal with properly."

### AI summary (High error rate! Edit errors on video page)

Policy changes at the Trump campaign are happening in response to a dinner that sparked controversy.
Trump's campaign is implementing new protocols to ensure he only meets approved and fully vetted individuals.
A senior campaign official will now accompany Trump at all times, similar to his White House setup.
The need for constant supervision suggests Trump lacks the judgment to decide whom he can talk to.
All individuals meeting with Trump will now undergo full vetting before the meeting.
This shift in handling Trump is seen as a negative sign for his campaign.
Trump's appeal as a maverick who does what he wants is diminishing with these new restrictions.
The public realization that Trump is being managed in this manner may lead to a decline in his support.
The facade around Trump is crumbling as more people begin to see through it.
Trump's campaign is now perceived as running him, rather than the other way around.
The revelation that Trump's interactions are controlled by others may disappoint his base.
Supporters who viewed Trump almost as a deity may be disillusioned by these new developments.

Actions:

for campaign watchers,
Share information about these policy changes with others to keep them informed (suggested).
Stay updated on developments within political campaigns and share relevant news with your community (suggested).
</details>
