---
title: Let's talk about a video game habit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=krlfp5BtZjA) |
| Published | 2022/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A single mom is torn about getting Google Play cards for her daughter, who plays a game where she builds a city and runs a business.
- The mom worries that the game gives her daughter an unrealistic idea of running a business and may be taking her away from real life.
- Beau suggests getting the cards and also opening accounts on various platforms together to challenge her daughter.
- He mentions underestimating kids' abilities and the importance of providing challenges for them to shine.
- Beau encourages the mom to support her daughter's interest in running a business, as it could be a valuable learning experience.
- He points out that kids today have a good understanding of marketing and brand management due to social media influences.
- Beau recommends embracing the daughter's interest in business and letting her experience the learning curve early on.

### Quotes

- "Our kids are doing something and we just don't get it."
- "They just need the challenge. They need that passion for something."
- "You can be the facilitator of her dream rather than the mom who wants you to put down the phone."

### Oneliner

A single mom seeks advice on her daughter's gaming obsession with running a business, prompting Beau to suggest embracing the interest and providing a real-world challenge.

### Audience

Single parents

### On-the-ground actions from transcript

- Open accounts on platforms like Teespring, Redbubble, Spreadshirt, Fine Art America together to challenge and support the daughter's interest in running a business (implied).

### Whats missing in summary

Beau's compassionate and practical approach to supporting a child's interest in business and providing real-world challenges.

### Tags

#Parenting #Support #Challenge #Gaming #Business


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a question from a parent,
and video games,
and
challenges.
This doesn't seem like it would apply to everybody,
but I think it might.
With everything you've been covering lately this seems trivial,
but I don't really know who to ask. I'm a single mom, my daughter is sixteen, and she's
always playing this game on her phone where she's building a city and running a
business in the game.
She's asked for Google Play cards for Christmas,
and I'm torn about getting them.
She's a great girl, and I know this sounds stupid because she gets good
grades and doesn't get into trouble,
and I'm sitting here complaining about her playing a game.
She talks about what her factory's made
like it's real life,
and I'm worried that she's getting an unrealistic idea of what running a
business is like.
She says she wants to set up
a business online
and sell her artwork on clothes and other stuff one day.
I just worry the
game makes her think it'll be too easy.
I also feel like we're growing apart,
and the game is taking her out of her real life.
Any advice?
I mean, yeah.
You know, it's something that we as
parents deal with often.
Our kids are doing something and we just don't get it.
We just don't get it.
Or,
we're expecting one outcome from it,
and it's really another.
Something entirely different occurs.
If this was me,
I would get the cards.
I would.
But,
I would also
open up an account
on Teespring,
Redbubble, Spreadshirt, Fine Art America. There's a whole bunch of
places that don't require any investment
except for time, which I'm sure as a single mom you just have
oodles of spare time laying around.
But, it would give you something to do together,
and it would give her that challenge upfront.
A lot of times,
I've found that
we underestimate
our kids.
And this goes to, well,
underestimating people in general.
A lot of times for somebody to really shine,
they just need the challenge. They need
that
opportunity, that thing that
they have a passion for,
and they need to be able to do it.
If it was me,
that early on,
this is my interest, this is what I want to do,
and
you're talking about
an activity, you're going to have to run it,
so you're talking about an activity that
will
at least put you together for that.
And see where it leads.
She might surprise you.
Running a business is
difficult. However,
our kids today,
they understand so much more
about marketing and brand management
and
everything
than we did.
They have to, because of social media presences that they have,
they are
constantly
witnessing influencers
and people who
have those brands
and have that merchandise that she's trying. We have it on this channel.
And I will tell you this,
the people who make that and the people who do illustrations for that type of
stuff,
they're not badly paid.
I would encourage
not just the recreational aspect of the game,
but what she's really showing an interest in
is
running the business.
Most people's business, their first one fails.
So it would probably be better for that to happen
and her to get that learning curve
underway
earlier.
And you get to be a part of it.
You can be
the facilitator of her dream
rather than
the mom who wants you to put down the phone.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}