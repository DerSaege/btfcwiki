---
title: Let's talk about the what-ifs of the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yYVD3w0XfoA) |
| Published | 2022/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why a self-coup in the United States through the seizure of Congress wouldn't work.
- Describes the key factors needed for a self-coup to occur successfully.
- Mentions the structural resilience of the U.S. government against such attempts.
- Lists the chain of command required for a self-coup to be successful, starting from the president down to Homeland Security.
- Emphasizes the importance of all individuals in the chain being complicit for the coup to succeed.
- Points out that even if the military were used to impose rule, the numbers required for occupation don't exist.
- States that a self-coup initiated in a public and forceful manner in the U.S. wouldn't be successful due to the outrage and potential conflicts it could spark.

### Quotes

- "You can't initiate a self-coup in the United States by the seizure of Congress. It will not work."
- "The outrage of trying to initiate something like this through force that is very public, it won't work."
- "Even if they had taken the building, they wouldn't have won."
- "The numbers just don't exist."
- "The U.S. government is structured to survive a nuclear attack."

### Oneliner

Beau explains why a self-coup through seizing Congress wouldn't work, detailing the necessary factors and structural resilience of the U.S. government.

### Audience

Citizens, Political Observers

### On-the-ground actions from transcript

- Understand the structural and procedural safeguards in place in the U.S. government to prevent a self-coup (implied).

### Whats missing in summary

In-depth analysis and further exploration of the implications of attempting a self-coup in the United States.

### Tags

#USGovernment #Coup #Resilience #StructuralSafeguards #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we're going to talk about what ifs.
We're going to talk about what ifs when it comes to the sixth.
Because in a recent video, I said something to the effect
of, great, you captured the building.
Now what?
It's about as important as a Walmart.
And there were a number of people who questioned that.
And the one part of it that really got my attention
was somebody who said, are you saying
that it's just absolutely impossible for a coup
to occur in the United States?
Definitely not.
That's not what I'm saying.
What I'm saying is that you can't initiate a self-coup
in the United States by the seizure of Congress.
It will not work.
It won't work.
So a self-coup is when a person who is in office
attempts to stay through extra-legal means and just take over.
In the US, this is difficult to begin with.
For this to work, for a self-coup to work, it has to happen quickly and it has to happen
without conflict.
If conflict starts, it spreads like wildfire and there's no way to know that the person
who initiated the self-coup would remain in power.
So at that point, it's just up in the air.
When people, when wannabe dictators try to engage in a self-coup, the key part is that
it has to happen quickly and quietly.
It can't start with something that tips everybody off to knowing that something is wrong.
If that occurs, the whole process breaks down.
The reason a self-coup initiating in this fashion in the U.S. just would not work, part
of it has to do with civics, it has to do with how the U.S. government is structured
just naturally with the different levels of government.
And then it also has a whole lot to do with the fact that since 1945, the U.S. government
has been structured, the federal government has been structured in such a way that it
would continue to function if DC disappeared in a flash of light.
There's a lot of continuity of government plans.
So starting off with something that clearly and openly says, this is not part of the US
Constitution.
This is something that is going to provoke outrage among some people.
won't work because there's a giant list of people that have to be in on it for
it to function. We're going to start with that list. Most people know that if the
president goes, the vice president is now in charge. A lot of people know that
next is the speaker of the house. Fewer know that the next is somebody in the
Senate. Even fewer know that after that it becomes Secretary of State, Treasury,
Defense, the Attorney General, Secretary of the Interior, Secretary of Agriculture,
Commerce, Labor, Health and Human Services, Housing and Urban Development, Treasury,
Energy, Veterans Affairs, and finally Homeland Security. All of those people
have to be in on it. If there is one break in this group, that person becomes the
new de facto leader of the federal government.
They become a rallying point and then it's not quickly so it fails.
It doesn't work or it turns into conflict.
After this list, they need at least a majority on the Supreme Court to be in their pocket.
Then they have to have the Joint Chiefs, all of them, and after that they have to have
commander of every single command because if one of them doesn't go along with it, it initiates
that conflict. Then you need the governors and then if you have all the governors you also have
to have all of the National Guard commanders for the various states. There's a lot to it to try
to do it the way it may have been tried, it wouldn't have worked.
I'm not saying it wouldn't have gone bad, but he wouldn't have succeeded.
The United States is very resilient in this regard because the federal government was
structured to survive a nuclear attack and because the US Constitution does have a lot
of separation of powers.
And because we have a federal system with various levels of government, understand even
in a state where the governor decides to go along with it, those at the county level may
not.
Initiating something like this through force that is very public, it won't work.
And then I know the next question from somebody would be something along the lines of, well,
what if they just don't care and they use the military to impose their rule?
I mean, sure, but let's assume that the entire military, everybody goes along with it.
Every single person.
They don't have the troops.
They don't have the numbers.
We have talked about the number of people that it would take to occupy the United States
on this channel before.
The numbers just don't exist.
If you were to call up everybody, everybody, they just don't exist.
It's not plausible to do it the way some people might have attempted.
It wouldn't work.
But that doesn't mean that the United States is coup-proof.
Just a self-coup initiated through this means.
It wouldn't happen.
The outrage would be too much, and it would initiate a conflict.
So those who are in the Sedition Caucus, the MAGA crowd, who were talking about how they
could have won, even if they had taken the building they wouldn't have won.
So there is one benefit to the just giant maze that exists that is the U.S. government
as a whole.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}