---
title: Let's talk about Trump calling out the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VRt88sFIx2o) |
| Published | 2022/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques Trump's response to the committee calling him out, particularly focusing on Trump labeling the committee members as Marxists.
- Beau breaks down the definition of Marxism as a belief in a stateless, classless, moneyless society where workers control the means of production.
- Questions whether members of Congress, who are often wealthy and receive campaign contributions from big businesses, truly advocate for the working class as Marxists do.
- Suggests that Trump and others who use the term "Marxist" as a scare tactic are fully aware of its meaning but exploit the ignorance of their base for manipulation.
- Points out how certain media outlets profit from spreading misinformation and manipulating their audience's lack of knowledge.
- Emphasizes the manipulation and trickery employed by certain political figures and media to control their supporters and advance their agendas.
- Calls out the Republican Party for deceiving its supporters about various issues, including the election, and warns about ongoing manipulation tactics.
- Urges people to recognize when they have been deceived and to be critical of the information they receive from political figures and media.

### Quotes

- "They stand there at those rallies. They give those speeches. They throw out that word. They get everybody angry. Then they walk behind that curtain and they laugh because they know what it means."
- "They know that the American education system isn't great. They know their base doesn't know what Marxism is. Because quote, I love the uneducated because they're easy to manipulate."
- "They played you about the election. They lied to you about that. And now they've moved on. A new term. They're lying to you again."
- "One of the hardest things in the world is to admit that you have been tricked."
- "They're tricking you."

### Oneliner

Beau breaks down the manipulation behind calling committee members Marxists, exposing the exploitation of ignorance for political gain and profit.

### Audience

Voters, Media Consumers

### On-the-ground actions from transcript

- Educate yourself on political terminologies and ideologies to be less susceptible to manipulation (implied).
- Stay informed from diverse and credible sources to combat misinformation (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how political figures use scare tactics and misinformation to manipulate their supporters, urging individuals to be vigilant and informed in the face of such tactics.

### Tags

#Manipulation #PoliticalDeception #Misinformation #Education #Awareness


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Trump,
former President Trump, responding to the committee,
calling him out, because he put out his televised response.
And it's a normal Trump speech.
But one part of it, really, it's stuck in my ear,
and it just rattled around.
And it's not the first time I'd heard it.
It was just the first time I heard it in that context
and in that way.
He said that the people on the committee were Marxists.
You look that word up, and it says
somebody who believes in the writings,
advocates for the writings of Marx.
And you look that up.
Look up Marxism.
What you find out is that it's a belief in a stateless,
classless, moneyless society where the workers have seized
the means of production, and they control everything.
You really think people in Congress
are advocating for the working class?
I mean, is that the image you get
when you think of people in Congress?
Right now, close your eyes and imagine Congressperson.
What gets conjured up when you think of that?
Somebody in the upper class, a multimillionaire
who's part of the US government, the antithesis
of stateless, classless, and moneyless, the exact opposite.
And then you think about who gives them campaign contributions.
All those big businesses, they all own Congresspeople, right?
Do you really think they'd be giving millions of dollars
to people who are advocating that their businesses be
seized by the workers?
Doesn't make sense.
It doesn't track at all, like in any way.
Right?
But here's the other part to this.
When you really think about it, is it a mistake?
I am fairly certain that when Trump was getting his economics
degree at the Wharton School, that he probably
encountered the economic theory of Marxism.
I would imagine that he ran into that.
And then you think about the other people
that throw this term out.
Where'd they go to school?
Harvard, Yale, Brown.
You think they don't know what it means?
They totally do.
They just know that you don't.
So they can throw out that word and use it to rile people up,
to scare people, to manipulate people, to trick people,
to get them to do what they want.
They stand there at those rallies.
They give those speeches.
They throw out that word.
They get everybody angry.
Then they walk behind that curtain
and they laugh because they know what it means.
And they know there aren't any Marxists in Congress.
That's not a thing.
They're tricking you.
And if you don't believe it, look at the reporting
about those depositions in that Fox News case.
I did not believe it for one second.
Quote.
But that didn't stop them from having wall-to-wall coverage
of all of those claims about the election.
They put it out there because it made them money, right?
Certainly how it seems.
And I would imagine behind the scenes, offset,
they were laughing about it.
They know that the American education system isn't great.
They know their base doesn't know what Marxism is.
Because quote, I love the uneducated
because they're easy to manipulate.
They have tricked y'all.
They have tricked you.
One of the hardest things in the world
is to admit that you have been tricked.
And there's a whole lot of people in the Republican Party
who are going to have to admit that they've
got to do the hardest thing in the world.
They played you.
And they're still trying to do it.
They played you about the election.
They lied to you about that.
And now they've moved on.
A new term.
It's not a new term.
It's an old term.
But they're going to bring it back out because they know
you don't know what it means.
And they know that, well, for a lot of people,
since they don't know what it means,
there's no way that they'll ever get
that it's the exact opposite of what they're saying.
They're lying to you again.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}