---
title: Let's talk about the X-37 and space....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rskxiGffQxY) |
| Published | 2022/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the X-37, a secretive space plane that recently returned to Earth after 908 days in orbit.
- Describes the X-37 as a small, baby space shuttle around 30 feet long.
- Mentions that there are two X-37 aircraft owned by the US government, with a combined time in space of over 10 years.
- Explains that the X-37 conducts experiments in space, including testing the effects of space on seeds and experimenting with electromagnetic propulsion.
- Notes the secrecy surrounding the X-37, with public-facing experiments and undisclosed projects.
- Traces the history of the X-37 from a joint NASA DARPA project to a Department of Defense project.
- Compares the US's X-37 project to a similar Chinese craft, indicating the US's decade-long lead in this technology.

### Quotes

- "908 days in orbit. It's a pretty big achievement."
- "There are two, we think, of these aircraft."
- "It's tiny."

### Oneliner

Beau delves into the secretive world of the X-37 space plane, detailing its lengthy time in orbit, experimental purposes, and the mystery shrouding its undisclosed projects.

### Audience

Space enthusiasts, researchers, military analysts.

### On-the-ground actions from transcript

- Research more about the X-37 and its experiments (suggested).
- Stay informed about advancements in space technology (implied).

### Whats missing in summary

The full transcript provides additional context on the history and significance of the X-37 project, offering a deeper understanding of its secretive nature and potential implications for space research and exploration.

### Tags

#Space #X-37 #SpaceTechnology #Secrecy #Research #Experiments


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about secrets in space,
in space planes, in something called the X-37, which
returned to Earth earlier this month.
OK, so the X-37 with flight OTV-6,
OTV standing for Orbital Test Vehicle-6,
meaning the sixth flight.
This is a space plane type thing.
It looks like a little baby space shuttle.
It's pretty short.
I want to say it's like 30 feet long or something like that.
If you see a photo of it without something for reference
as to size, you would be forgiven
for thinking it is roughly the same size as a space shuttle.
It's not.
It's tiny.
On OTV-6, it was up there for 908 days.
908 days, this thing spent in space.
There are two, we think, of these aircraft.
The US government has two of these aircraft.
And together, they have spent around a little more
than 10 years in space.
Why haven't you heard about it?
Because it's kind of secretive.
The existence isn't a secret.
But there's a lot of secrecy surrounding this thing.
So brief history.
Started as a joint NASA DARPA project, I think, back in 1999.
It became a DOD project, Department of Defense, in 2004.
Now, what does it do while it's up there?
That's the big question.
So it conducts experiments.
It experiments.
It's a scientific thing.
You can find information about it testing
the effects of space on seeds.
So we can figure out how to grow food on other planets and stuff
like that, or experiments on electromagnetic propulsion,
or turning solar into microwaves and stuff like that.
However, given the amount of secrecy
that surrounds this thing, this is probably a two-tier project,
meaning there is an aspect of it that is public-facing,
that they can talk about.
Hey, we're doing these cool scientific things.
And then there are some other experiments going on.
There's something else that's occurring.
What that is, I don't have a clue.
I couldn't even guess.
I would imagine if it was written down
on a document somewhere, it would
be stored in a pretty secure military installation,
or perhaps a broom closet at Mar-a-Lago.
But we don't know a lot about it.
However, 908 days in orbit.
It's a pretty big achievement.
Now, it is worth noting that the Chinese have a similar craft.
And I want to point out that it took off for the first time,
I believe, in 2020.
So this is yet another project where the United States
has a decade-long lead on our nearest peer.
So that could theoretically be used
to illustrate that maybe we're overshooting our research
goals just a wee bit.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}