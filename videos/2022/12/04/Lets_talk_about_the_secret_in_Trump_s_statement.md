---
title: Let's talk about the secret in Trump's statement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YgkYUNJIDWI) |
| Published | 2022/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Trump's statement and its significance.
- Trump's statement reveals his desire for autocratic control and subversion of the Constitution.
- Trump's call to terminate articles, including those in the Constitution, based on false claims of fraud.
- The statement exposes Trump's true intentions and authoritarian tendencies.
- Individuals who supported Trump and his endorsed candidates share his beliefs.
- Urges people to wake up to the reality of Trump's intentions and the danger they pose.
- Calls for a wake-up call within the Republican Party regarding Trump's statement.
- Advocates for holding political candidates accountable for their stance on terminating the Constitution.

### Quotes

- "Terminate articles to include the Constitution."
- "You cannot support MAGA and support the Constitution because MAGA is telling you they're gonna terminate the Constitution."
- "This needs to be the biggest wake-up call for the Republican Party."
- "Every person who got an endorsement from him needs to answer for this."
- "They all need to answer."

### Oneliner

Beau reveals Trump's authoritarian desires and calls for accountability from his supporters and endorsed candidates, signaling a critical wake-up call for the Republican Party.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Question political candidates on their stance regarding Trump's statement about terminating the Constitution (suggested).
- Demand direct answers from candidates on whether they agree or disagree with Trump's statement (suggested).
- Initiate focused discourse on Trump's statement and its implications within political circles and communities (implied).

### Whats missing in summary

The full transcript provides in-depth analysis and implications of Trump's statement, urging individuals to critically scrutinize political candidates and their alignment with authoritarian tendencies.

### Tags

#Trump #Authoritarianism #Accountability #RepublicanParty #PoliticalCandidates


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump's statement and what it means and
the secret that is contained in this statement, it's probably the most
important statement he's ever made.
The most revealing statement he has ever made.
I'm going to read the whole thing.
I'm gonna read all of it. I will tell you there are some things in here that aren't true. There
are some things in here that are misframed. All of the normal stuff. But this is the most important
thing he has ever said. So with the revelation of massive and widespread fraud and deception
in working closely with big tech companies, the DNC, and the Democrat Party. Do you throw
the presidential election results of 2020 out and declare the rightful winner, or do you have a new
election? A massive fraud of this type and magnitude allows for the termination of all rules,
regulations, and articles, even those found in the Constitution, his words,
our great founders did not want and would not condone false and fraudulent elections.
There were no false and fraudulent elections.
That's a lie.
He's just a liar.
But let's focus on the important part here.
of all rules, regulations, and articles, even those found in the Constitution.
What does this mean?
It means that everybody who said he was going to try to cling to power, and then he did,
means they were right.
Those who said he was trying to subvert the Constitution were right.
They were saying it for six years now and they were right.
He is flat out saying to terminate articles of the Constitution based on what?
His hurt feelings or based on what everybody knew was coming?
His desire for autocratic control.
desire to be a dictator like those he exchanges love letters with.
He issues this statement on the heels of a bunch of people going to prison for him.
They're going to go to prison and they're members of a group and the very name of the
group says that they won't break an oath to the very document he is saying to terminate.
He tricked them and he tricked a lot of other people.
That should be plainly obvious by now.
Everything that everybody said about him was true.
Now let's go to the important part, the part that you can actually do something about.
statement, it's all of MAGA. All of it. Everyone he endorsed, everyone who
placated him, everyone who followed him around and bent the knee and kissed his
ring, they feel the same way. They're the exact same type of person. They share the
same beliefs. You cannot support MAGA and support the Constitution because MAGA
is telling you they're gonna terminate the Constitution. There is no other way
to read this. Those who are in Georgia, you might want to pay attention to this
because if you think Walker is gonna be anything other than a puppet for a man
who has openly stated he would like to terminate the Constitution. I don't know
what to tell you. You've lost the plot. This statement tells the entire world
who he is. There were people who were taking his statements in the past and
saying this is what he's saying. He's dog-whistling. This is what he means and
people didn't want to believe it. Now it's him saying it. Terminate articles to
include the Constitution. It's time for a whole lot of people to wake up to what
he is and he's telling you now. It's not his political opposition, it's not
political commentators, it's not experts on fascism, it's him, he's saying it.
Those same commentators, those same experts will tell you that what you are now witnessing
with him is true for all of his supporters, for all of those political candidates who
enabled him.
It's all the same.
This needs to be the biggest wake-up call for the Republican Party.
Every single person who runs needs to be asked specifically, what do you think about Donald
Trump's statement about terminating the Constitution?
They need to be asked directly and their only answers can be, I agree or I disagree.
That's it.
no hawing, no trying to placate the guy so they don't get a mean tweet because
that's how he kept them in line. But at their core they were willing to go along
with it because they're the same. This needs to be the discussion right now.
This needs to be what people are talking about. Nothing else. Every person who got
an endorsement from him needs to answer for this. They need to explain why
somebody who wants to subvert the Constitution of the United States
believed they were a good choice. Those who mimic his rhetoric, his authoritarian
style. They all need to answer. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}