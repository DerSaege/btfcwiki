---
title: Let's talk about running the country like a business....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p5opg_D1YjY) |
| Published | 2022/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Enters a gas station late and encounters a man wearing a red hat, triggering a political debate.
- Engages in a 10-15 minute debate about Trump running the country like a business.
- Questions the notion of running the country like a business, challenges the popular slogan.
- Points out the flaws in the concept of treating the government like a business based on profit.
- Suggests that running the country like a business may not be beneficial for the working class.
- Encourages questioning the idea whenever it is brought up to reveal its flaws.

### Quotes

- "Most of you traded your country for a red hat."
- "Why do you like him? Because he ran the country like a business."
- "Running the country like a business is bad for the working class."

### Oneliner

Beau challenges the notion of running the country like a business and debates its implications, revealing its flaws and negative impact on the working class.

### Audience

American citizens

### On-the-ground actions from transcript

- Question the concept of running the country like a business when discussing politics (exemplified).
- Encourage critical thinking and questioning of political slogans in everyday interactions (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of the flawed concept of running a country like a business and its negative consequences for society.

### Tags

#Politics #Trump #Government #Business #WorkingClass


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a very popular slogan in American politics
and a conversation I had at a gas station.
So it's late and I walk into the gas station.
I'm going to get some coffee. I get my coffee.
I walk up to pay for it.
There's a guy who is finishing up paying
and when he turns around, he reads my shirt.
And it's the one that says,
most of you traded your country for a red hat.
And he is wearing a red hat.
And he does not look happy with my wardrobe.
So as I buy my coffee, I'm definitely angled so I can still see him.
And he is standing there waiting for me to finish.
So I'm like, great. This is just what I need tonight.
I pay and I walk over to him and he's like, you don't like Trump?
I'm like, not particularly. He's like, why not?
We don't have that kind of time, dude.
And he kind of laughed. I'm like, all right,
so maybe this isn't going to go the way I thought it was.
It's like, why do you like him?
And he said, because he ran the country like a business.
Now, there's a whole bunch of commentary there that can be made,
especially the idea of Trump running the country like one of his businesses.
But my initial reaction was, who told you that was a good thing?
And the look on his face let me know that nobody had ever questioned that position before.
He just kind of paused and cocked his head.
We wound up talking about this for like 10 or 15 minutes outside the gas station.
I asked him if he liked his boss.
He's like, no. And by this point, I've already found out he hangs drywall.
And he's like, no, not really.
I'm like, what's your boss's job?
And he had a very colorful way to say, to ride me constantly and get a lot of productivity out of me.
And from there, so his job is to get as much out of you as possible while giving you as little in return, right?
Well, yeah. See a pretty successful business?
Well, yeah. You really think we should introduce that nationwide?
Man, I wish I had to talk to you.
This concept, this idea of running the country like a business where everything should be turning a profit,
where everything should be theoretically based on some capitalist view of the world,
doesn't make any sense when you really think about it.
When you look at what the government is supposed to do, how it is supposed to function,
the idea of running it as a business means that the government fails in all of the promises
that are laid out in the founding documents.
This is a horrible slogan that we probably don't interrogate enough.
We don't ask people why they think this is a good idea,
because it doesn't take long to get them to realize this is a horrible idea.
If you have a country where the government's entire purpose is to get as much out of you as possible
while giving you as little in return, I don't feel that most people would want that.
The idea of running the country like a business is bad for the working class.
It is bad for those people who seem most enamored with it.
It might be worth questioning anytime you hear this,
because it didn't take long for this guy to realize that that's not such a great qualification after all.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}