---
title: Let's talk about rules in Minnesota for cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5p2xujqRVEE) |
| Published | 2022/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The post board in Minnesota has proposed two changes to regulations that could deny or revoke a cop's certification if found spreading hate material online or participating in hate group forums.
- These proposed changes also include requiring more information during background checks, such as asking if false information had been provided in court.
- Law enforcement associations in Minnesota have opposed these changes, delaying their implementation since the first proposal in 2020.
- The president of the Minneapolis Police Federation believes these changes won't increase public trust and may hinder recruiting efforts, despite aiming to keep bigots out of law enforcement.
- Beau questions why law enforcement associations oppose regulations that aim to prevent bigots from having power over people, stating it decreases public trust.
- The pushback from law enforcement is to argue that the legislature, not the post board, should make these decisions.
- Beau challenges law enforcement associations to use their lobby power to push for these rule changes through the statehouse, expressing skepticism that they will do so.

### Quotes

- "You shouldn't want bigots on your departments, plain and simple. They're a liability to the department, to other officers, and to the public."
- "Having law enforcement associations oppose regulations that prohibit bigots from gaining life and death power over people decreases public trust."
- "Make the calls. Y'all have enough legislators in your pocket. Back the blue, right?"

### Oneliner

The post board in Minnesota proposes changes to deny or revoke cop certifications for hate-related activities, facing opposition from law enforcement associations and raising questions on public trust.

### Audience

Law enforcement reform advocates

### On-the-ground actions from transcript

- Contact your legislators to support regulations denying certification for hate-related activities (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of proposed changes in law enforcement regulations in Minnesota and the opposition faced from law enforcement associations.

### Tags

#Minnesota #LawEnforcement #Regulations #PublicTrust #Opposition


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about some news out
of Minnesota, a couple of proposals,
and then we have some responses from people
within that field, a couple of quotes,
and we're going to go over those,
and just kind of run through everything.
Now, the post board in Minnesota,
this is the police officer, or I'm sorry,
peace officer standard and training board.
They kind of set regulations.
They have proposed two changes.
One of them I know has already been sent to an administrative
judge to kind of be gone over.
These could be implemented as soon as the spring,
and these changes would deny or revoke
the post certification, the licensing for a cop up there.
One of the changes would make it to where
you couldn't hold one of their licenses
if you were found to be spreading hate material online
or participating in hate group forums,
because apparently that's not a standard already.
The other requires more information
during the background check phase, during the application,
and one of the questions that would be asked
is, have you ever provided false information
that a prosecutor had to disclose in court?
Which, I mean, to me, that also seems
like something that should take place during the interview
phase.
So these two proposals are being put forward.
The one about online activity was first proposed in 2020
and still hasn't been implemented,
partially because law enforcement associations
in Minnesota are opposed to it.
Now, the general counsel for the Minnesota Sheriffs Association
said, well, I suspect we all agree we do not
want bigots in law enforcement.
We do not always agree on what a bigot is.
I mean, if you're spreading hate material online,
that would be one of the things that would be
a bigot, that would be what most people would
consider an indication.
And then we have this gem of a quote
from the president of the Minneapolis Police Federation.
Not only do we believe that these changes won't achieve
the desired outcomes of increased public trust,
but we believe it will negatively impact recruiting
and retaining efforts.
What do you call recruiting?
A sheet store?
How is this going to complicate things here?
These are people you shouldn't want on your departments,
plain and simple.
They're a liability to the department.
They're a liability to the other officers.
They're a liability to the public.
This shouldn't be a debate.
And then as far as increasing the public trust,
let me tell you what certainly doesn't
increase the public trust.
Having law enforcement associations
oppose regulations that would prohibit bigots
from gaining life and death power over people.
That decreases the public trust.
You don't have to be a sociologist to understand that.
So law enforcement associations are apparently upset,
and they are in opposition to these rule changes.
They want to be able to recruit bigots and people
who would provide false information to prosecutors.
I mean, that doesn't inspire trust in me.
Just saying.
I don't think, wow, I really trust cops up there now.
The push here from the law enforcement side
is to try to say that, well, the legislature
should be the ones to make this decision, not the post board.
OK, put your money where your mouth is.
Y'all got a really powerful lobby.
Have the legislature make these changes then.
If it's really just a procedural thing
that you're concerned about, make the calls.
Y'all have enough legislators in your pocket.
They'll back you.
Back the blue, right?
Make the call.
Use the power of your lobby to get these regulations
through the statehouse.
I'm willing to bet they're not going to.
This started being proposed in 2020.
I would imagine that there's a reason for that
up there in Minnesota.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}