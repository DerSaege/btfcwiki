---
title: Let's talk about Hannity almost getting it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=llisL4_57Yg) |
| Published | 2022/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hannity outlines everything wrong with the Republican Party but fails to bring all the pieces together.
- Democrats focus on getting enough ballots to win, not old-school campaign tactics like rallies and kissing babies.
- Hannity acknowledges that Republicans need to embrace mail-in voting, despite years of opposition within the party.
- Mail-in voting makes it easier to vote but doesn't create new voters for the Republican Party.
- Republican campaign strategies like rallies and in-person events are outdated and unappealing to younger voters.
- The Republican Party's establishment figures are seeking new marketing strategies but are missing the mark.
- Online presence is weak for Republicans due to fact-checking exposing bad policies.
- Policy issues, not marketing strategies, are at the core of the Republican Party's problems.
- Acknowledgment within the Republican Party of the need for change, but failure to connect it to outdated policies.
- Republican Party must evolve and embrace progressive ideas to avoid continued losses.

### Quotes

- "Stop looking for somebody to hate and try to make the country better."
- "The Republican Party has to become more progressive. Or it will fade away."
- "It's all fear-based."
- "Their voters tend to show up."
- "The Republican Party, they're going to continue to lose until they become a little bit more woke."

### Oneliner

Beau outlines the Republican Party's need to evolve, embrace progressive ideas, and move away from fear-based strategies to avoid continued losses.

### Audience

Political activists and party strategists

### On-the-ground actions from transcript

- Embrace early voting and mail-in voting (implied)

### Whats missing in summary

Full understanding of the Republican Party's need to evolve and embrace progressive policies to avoid further decline.

### Tags

#RepublicanParty #ProgressiveIdeas #PoliticalStrategy #PolicyChange #VoterEngagement


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about something Hannity said.
If you watch enough conservative TV,
you will eventually see an episode
where a commentator lists all of the problems.
And if they put any thought into what they had said,
they would actually see the solution.
And they would understand the real issue
with the Republican Party and why the Republican Party is
on the losing streak that it's on.
Normally, it's a new commentator.
It's normally not Sean Hannity.
He's been around a long time.
And he's typically a little bit more astute.
This little rant that he went on, it outlined everything
wrong with the Republican Party.
But it didn't bring all the pieces together.
So we're going to do that now.
So he starts off by saying that Democrats are not
interested in the quality of a candidate.
He said this after Georgia, you know,
werewolf Walker versus Warnock.
I'm fairly certain that this part was just
him playing to the base.
Because later on, he actually talked about a bad candidate
that the Republican Party ran.
But he goes on, he says, you know,
they're not interested in the quality of a candidate
or holding rallies or exciting voters or kissing babies
or shaking hands or going to fairs.
Remember this part.
Rallies, kissing babies, shaking hands, going to fairs.
You know, stuff that would win an election in the 1950s.
He goes on to say, Democrats are only
interested in getting their hands on enough ballots
that they believe will put them over the top
to win an election.
Well, I mean, yeah, that's how you win an election.
Elections aren't determined by the number of babies kissed.
From here, he goes on to talk about a candidate,
I want to say the one in Pennsylvania,
and he talked about how their stance on family planning
just destroyed them.
There was no way that they could win.
It was a bad candidate.
And he made the connection to bad policy as well.
And then he went on to say, well, the Republican Party
needs to embrace mail-in voting, which
is going to be really hard because you've had six years
of major figures in the Republican Party saying
that it doesn't work.
Now, Hannity is kind of right in almost everything
that he's saying.
But he's saying the solution is to embrace mail-in voting.
The thing is, mail-in voting doesn't actually
create new voters.
It just makes it easier for people to vote.
The Republican Party generally doesn't have bad turnout
because their voting base can typically get the day off.
They may be retired.
They may be in a position that they can take the day off.
So I don't know that that's going to help them a whole lot.
But at least he's kind of thinking
about changing something and progressing and updating
to something new.
Now let's go back to what he was complaining
about the Democratic Party not being interested in.
Not interested in holding rallies, kissing babies,
shaking hands, going to family parties,
shaking hands, going to fairs.
This type of campaigning, it's integral to the Republican
Party for two reasons.
One, this is what older people like to do.
The problem is because of, let's just say,
the hastened departure of a lot of their base
due to a whole lot of commentators
leaning into the idea that their base didn't
need to protect themselves in the middle
of a global pandemic, there's not as many of them.
They're being outnumbered now by younger voters
who see kissing babies, shaking hands, and fairs,
and rallies as cliche.
It's something that scheming politicians do.
It's something from the 1940s.
It's old.
It's outdated.
And this is what he's complaining about.
And he makes the connection later
to a bad policy and a bad candidate.
Follow it through.
It's just like the idea of what should be a campaign is
outdated.
The policies are outdated.
Nobody wants them.
Nobody wants to vote for people who
are promising to take them back to the good old days,
a myth circulated about the 1950s based on TV shows.
Nobody actually wants that.
Not enough of the people you need to win an election anyway
to get those ballots.
That's how you win an election, not kissing babies.
It's the policies.
You have a lot of establishment figures
within the Republican Party right now looking
for new marketing strategies.
That's not it.
The Republican Party, as far as all of this stuff that's
listed, kissing babies, holding rallies, all of this stuff,
it's all in person.
The Republican Party of today needs it to be in person.
They're not doing well online.
They're not doing well at converting younger people
to their position online.
Why?
Because it's online and they can be fact checked.
Their policies are bad.
So people don't vote for them.
And this is the piece that just would tie all of this
together.
He was so close to getting it, but never brought it
all together, never brought all the points together.
Understanding that the Republican Party is not
performing well.
And that in this case here, it had to do with bad policy.
But really trying to tie it to just marketing.
That's not it.
It's not going to help.
Early voting, yeah, the Republican Party
should totally embrace early voting and mail-in voting.
Everybody should vote.
But that's not going to help them win.
It won't.
Their voters tend to show up.
So what you have right now is a lot
of people in the Republican Party,
especially those that are thought leaders,
acknowledging that there's a major issue.
But they're not putting the pieces together
to realize that it's their policies.
It's their ideas.
It's wanting to take away people's rights.
It's villainizing others.
It's having rallies that look like they
belong in the 1930s or 40s.
Those ideas, they're stale.
They're outdated, just like the campaigning ideas.
The Republican Party, they're going to continue to lose
until they become a little bit more woke.
The Republican Party has to progress.
And if they don't, they'll continue to lose.
And he even pointed out the internal moves
that people are making.
In this same segment, he talked about how people
are moving to Florida.
But Florida's already a red state.
So it's not going to help them.
And the people that are leaving California,
they're moving to red states.
So it's going to make those harder to win.
And he understands all of this.
He's pointing all of this out.
But he's not tying the fact that the people moving to Florida
are older.
They have outdated ideas.
They didn't evolve with the older people
who are more accepting of more liberal ideas,
more progressive ideas.
They stayed the same.
That's who they're catering to.
It's a demographic that is shrinking.
The Republican Party has to become more progressive.
Or it will fade away.
Mel-in voting isn't going to save them.
Policy changes will.
Stop looking for somebody to hate
and try to make the country better.
Stop trying to scare everybody into voting for you.
Because that's the Republican way of doing everything.
It's all fear-based.
The younger crop that the Republican Party
likes to mock so much, they grew up in schools
where they had things to be afraid of.
They're not afraid of your imaginary monsters.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}