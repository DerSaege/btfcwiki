---
title: Let's talk about Trump not being in contempt....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=14NsKm90NR4) |
| Published | 2022/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice sought to hold Trump and legal team in contempt for not being forthcoming about potentially more documents.
- DOJ's main goal is to remove any classified documents from circulation and retrieve them.
- The judge did not hold Trump in contempt, prompting Trump world to claim victory.
- DOJ may have cause to believe Trump still possesses documents, potentially leading to further legal action.
- Trump's legal strategy of delaying may not work in his favor this time.
- DOJ's actions are a warning shot to Trump, urging him to return any documents.
- Viewing this as a win may lead to more actions from the Department of Justice.

### Quotes

- "Be careful what he wishes for, because you can't always get what you want."
- "Trump's legal strategy of delay, delay, delay doesn't really work in a case where any leniency that he might receive kind of hinges on him not delaying."
- "This is more of DOJ putting a shot across the bow and saying, you need to give us the stuff back."

### Oneliner

Department of Justice seeks to hold Trump accountable for potentially withheld documents, cautioning against viewing the lack of contempt as a victory.

### Audience

Legal analysts, concerned citizens

### On-the-ground actions from transcript

- Stay informed on the developments in the Trump document case and its implications (implied)

### Whats missing in summary

Insights into potential future legal proceedings and consequences beyond the current situation.

### Tags

#Trump #DepartmentOfJustice #LegalSystem #Contempt #Documents


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the latest developments
in the Trump document case
and what went on there over the last few days
and why even though Trump world is kind of claiming victory,
they might not should be doing that.
So if you missed what happened,
the Department of Justice is reported
to have gone before a judge
and attempted to get the judge to hold Trump
and the legal team in contempt.
The basis of this is it appears that the Department of Justice
doesn't believe Trump and team are really forthcoming
when it comes to the possible existence
of even more documents.
So they went to the judge and tried to get the judge
to hold the team in contempt
and kind of force them to turn over anything that they have.
This indicates that the Department of Justice
believes he has more documents.
It's worth noting that right now,
one of the most important things for DOJ
is to get any of the classified stuff out of circulation
and get it back.
That's a really important thing
for them to be doing right now.
The judge sided with Trump and said,
basically we're not gonna hold him in contempt right now.
Y'all need to work this out.
Trump world is claiming victory.
They're like, this is our moment to shine.
Being not held in contempt is a win for them at this point.
The thing about it is,
is that somebody might want to remind Trump
to be careful what he wishes for,
because you can't always get what you want.
The Department of Justice attempted to use the courts
to get Trump and team to return documents
they believe he still might have.
DOJ is not without power of its own.
If DOJ believes that he still has these documents,
they probably have cause.
I wonder if that cause is probable.
This might lead to the execution of another search warrant.
This might lead to them searching more of his properties.
If they believe that would not be successful,
well, then they just move to the charging an indictment phase.
This really, I don't think this is the win that they think it is.
Trump's legal strategy of delay, delay, delay
doesn't really work in a case where
any leniency that he might receive
kind of hinges on him not delaying
and getting stuff that should have never been in circulation
out of circulation.
I know the read on it from the Trump side of things
is that somehow this shows that it's political
and blah, blah, blah, blah.
No, if they were out to embarrass him
and they had reason to believe that he still has documents,
they could have executed a search warrant.
They could have moved to the indictment phase already.
I don't see this as the Department of Justice losing.
This is more of DOJ putting a shot across the bow
and saying, you need to give us the stuff back.
Taking this as a win is probably very ill-advised
for the Trump team
because it's likely to just spur other action
from the Department of Justice, three different channels.
So that's what's happened.
There are undoubtedly going to be tons of legal experts
weighing in on this.
One thing I do want to point out
is that everything we know about what happened
is kind of secondhand.
This was a closed hearing, so take that into consideration.
People are reading it in their own ways,
and this includes me.
I would temper any belief that you might have
or anything that comes out of this,
just understand that there's a lot of it
that is as reported by, you know,
and it's based on sourcing that anybody
who has firsthand information has a vested interest in it,
so they'll be presenting their side.
But this, to me, indicates
that we might see things speed up again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}