---
title: Let's talk about dancing, history, and changing society....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PDbp1Rt1NPs) |
| Published | 2022/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a message critiquing his analogy comparing dancing Ukrainian soldiers to drag shows by American soldiers during World War II.
- Explains the historical context of drag shows by USGIs during World War II and their connection to changing societal norms.
- Mentions the establishment and success of the Women's Army Corps (WACC) in 1942.
- Notes the significant contributions of women from WACC in advising men on makeup for performances.
- Emphasizes the success of WACC and how it exceeded recruitment goals, leading to its expansion.
- Describes the societal views on women during World War II as weaker, lesser, and unequal.
- Explains why women were prohibited from performing on stage during the USGIs' drag shows.
- Clarifies the different reasons behind the prohibition of women from participating in the shows by WACC and the War Department.
- Points out that the shows aimed to push for societal acceptance of women as equals within the military.
- Concludes by reflecting on the progressive changes within the U.S. military during World War II and their impact on society.

### Quotes

- "They didn't fight, they didn't complain, they just did their jobs."
- "It's hard for a lot of people to believe, but during World War II, the U.S. military, they were woke AF in a whole lot of ways."
- "What's so wrong with just accepting?"

### Oneliner

Beau explains how drag shows by USGIs during World War II were part of a larger effort to change societal norms, particularly in accepting women as equals in the military.

### Audience

History enthusiasts, advocates for societal change

### On-the-ground actions from transcript

- Research and share more about the historical contributions of women in the military during World War II (suggested).
- Support initiatives that aim to challenge societal norms and push for equality (implied).

### Whats missing in summary

The full transcript delves deeper into the historical context and societal attitudes towards gender roles during World War II.

### Tags

#History #SocietalChange #GenderEquality #USMilitary #WorldWarII


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about history again a little bit
and provide some information about changing the order of society.
This stems from that video about the dancing Ukrainian soldier.
And a message came in that had problems with the analogy and the comparison I used
and talking about the shows that American soldiers put on.
So here's the message.
I'm not against the kid dancing in that video,
but I do have something that bothered me.
Your drag show comparison doesn't really hold up to scrutiny.
Soldiers in drag during World War II was just guys being guys and blowing off steam,
like you said, but it wasn't part of some orchestrated campaign
to change the order of society and force acceptance.
They just didn't have women to fill those roles.
And to be clear, I'm not one of those nuts who thinks a drag show is the downfall of society.
I feel like it's getting shoved in my face a little bit,
but I've had worse things shoved in my face.
Phrasing. I normally love your historical analogies,
and this one doesn't seem to hold up.
If I'm wrong, tell me. You're wrong.
Not horribly wrong. You're making a very common mistake.
You are looking at the societal order today and comparing the order of today to the order at the time.
So you don't see that those drag shows by USGIs during World War II were a direct result of the War Department
attempting to drastically change the order of society.
Okay, so you're right about them just not having women to fill those roles in the beginning,
in the very beginning, early, early beginning.
In May of 1942, WACC was established.
Now, first it was W A A C, and then it just became W A C, Women's Army Corps.
1942 in May, very early on.
Some of the women from WACC, they actually advised the men on how to do their makeup before they went on stage.
They were definitely present.
The thing is, WACC was wildly, wildly successful.
It started with them wanting and hoping to get 25,000 women.
They met that recruitment goal and exceeded it in its first year.
It was so successful that they expanded it by 400%.
MacArthur said that the women of WACC were his best soldiers.
They didn't fight, they didn't complain, they just did their jobs.
The brass was so impressed with the concept that they wanted to draft women during World War II,
but they realized there would be pushback.
Congress was just like, no, we're not doing that.
And the pushback was to be expected because women were weaker, lesser, other, property.
They were viewed as unequal.
So there would be pushback from society at large, and maybe the military structure just wasn't ready for it yet.
So why do you think women weren't allowed on stage?
Because they weren't.
They were there.
They helped the men do their makeup, helped with the dresses at times,
but they weren't allowed to perform.
Now, the reasoning within WACC, the brass just at WACC, really had to do with the image.
They were concerned about the image of some of the more forward routines.
They were worried that they might be shoving something in somebody's face,
and they didn't want to be associated with that.
But the War Department as a whole, see, they had this other thing.
They had an entirely different line of reasoning, and it led to women being prohibited from participating at all.
They couldn't even just get up there and sing.
They weren't allowed on stage.
Because the War Department was trying to orchestrate a fundamental change of society,
they wanted women viewed as equal within the military.
That is why those shows existed.
The women were there.
In a lot of the situations, the women were there.
Not always, not in some places overseas, but they were there often enough that they could have been on stage at times.
But they were prohibited from doing so because they wanted to maintain the image of a woman soldier,
rather than a performer, rather than eye candy.
They wanted to, well, force acceptance.
So yes, it absolutely was about changing societal order, just a different way.
And the fact that you don't see it, and you don't recognize that that's why it was occurring,
or even kind of contemplate it, even if you didn't know the history, means it worked.
That's why you don't recognize it.
It's hard for a lot of people to believe, but during World War II, the U.S. military,
they were woke AF in a whole lot of ways.
And a lot of the progress that was made within the military later transferred outside of it.
Whack, as an example, the specialty schools that they would go to to learn how to do their jobs,
they were integrated in the early 40s.
A little bit of a shake-up of the societal order.
Now, there's your history.
That is definitely, the shows are definitely a byproduct of that desire to shake things up and change things.
As far as the rest of it, the one question I would have, why does the acceptance have to be forced?
What's so wrong with just accepting?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}