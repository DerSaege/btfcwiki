---
title: Let's talk about McCarthy's first agenda item....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=invG4JDJzsI) |
| Published | 2022/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Kevin McCarthy plans to use a roadblock to defense spending in the House of Representatives by withholding the NDAA until the vaccine mandate is removed.
- Active duty military shows high compliance with the vaccine mandate, with over 97% reported compliance.
- The real issue lies with the National Guard and Reserve members who may not be vaccinated, affecting readiness during emergencies.
- Refusing the vaccine doesn't necessarily mean active duty members are against it; some may just be out of date.
- The Republican Party's move to block the NDAA sets a dangerous trend for readiness in future emergencies.
- McCarthy's actions are seen as weakening the U.S. military's readiness and serving the Republican Party's agenda.
- There's concern that those refusing vaccines might have extremist beliefs, leading to further complications within the military.
- Despite disagreements within the military, top brass remains firm on the importance of the vaccine mandate.
- The Republican Party's strategy to obstruct the NDAA marks their initial step towards achieving their goals.
- This move by the Republican Party is viewed as prioritizing their agenda over national security.

### Quotes

- "The Republican agenda is to weaken the U.S."
- "McCarthy's actions are setting a trend for a readiness issue next time."
- "The real issue lies with the National Guard and Reserve members who may not be vaccinated."
- "The Republican Party's move to block the NDAA sets a dangerous trend."
- "Given the Venn diagram of those refusing the vaccine and those with extremist beliefs, I'd want them gone anyway."

### Oneliner

Kevin McCarthy plans to block defense spending in the House of Representatives until the vaccine mandate is removed, risking national security and weakening military readiness for future emergencies, despite high compliance rates among active duty members.

### Audience

Military personnel, policymakers

### On-the-ground actions from transcript

- Contact policymakers to express support for maintaining the vaccine mandate to ensure military readiness (suggested)
- Join or support organizations advocating for the importance of vaccination within the military (implied)

### Whats missing in summary

Detailed insights on the potential consequences of compromising military readiness and national security for political agendas.

### Tags

#KevinMcCarthy #NDAA #VaccineMandate #MilitaryReadiness #RepublicanParty


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about what
Kevin McCarthy plans to do in the House of Representatives. He has announced that what he
sees as the first Republican win will be him throwing up a roadblock to defense spending,
not for any like noble reason or anything like that. His plan is to hold the NDAA
until they basically get rid of the vaccine mandate. And this is going to be the first
Republican win. Okay, so we'll kind of go through it piece by piece. The first part that you really
need to know is that widespread refusal, that's not a thing. That's not a thing. When it comes to
the active duty, last numbers I saw were 97 point something percent compliance. Now please understand
being non-compliant doesn't actually mean that they're refusing to get the vaccine. It just may
mean that they're not up to date. So that other two and a half percent, some of them are just
E4s being E4s. So as far as active duty, it's not really an issue. The numbers really start to show
up when you start looking at National Guard and Reserve. And I know what the active duty guys are
thinking right now. You're like, and so what? Yeah, here's the thing. If there is another
widespread pandemic, who shows up? It's not active duty. It's the National Guard that gets called up.
They need to be vaccinated. That's where the issue is occurring. And those are the people
that need it most. And them showing up to help while being unvaccinated is a readiness issue.
At the same time, right now, due to the trend when it comes to the strength of the current
public health issue, the increased immunity, the better treatments, so on and so forth,
it's not a huge readiness issue at the moment. However, McCarthy doing this, the Republican Party
doing this, is setting a trend for a readiness issue next time.
Now, as far as I'm concerned, given the Venn diagram that would show up if you
pulled those who are refusing the vaccine and those who have extremist beliefs, I mean, if I
was Secretary of Defense, I would want them gone anyway. But given the issues they're having with
recruitment, they're probably slow walking getting people out. That being said, the brass has been
very adamant that they want the mandate as recently as this month, well, last month. So,
this is the Republican Party's starting point for the Venn diagram.
This is the Republican Party's starting point for their agenda. They are setting out
to hold up the NDAA, the budget for the entire military, to make certain that
there's a readiness issue. The Republican agenda is to weaken the U.S.
That is the first win of the Republican Party. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}