---
title: Let's talk about good news in San Francisco....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7pWG40rHYLU) |
| Published | 2022/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- San Francisco Police Department proposed a policy allowing armed robots, which was initially approved but later voted down by the Board of Supervisors due to public outcry.
- The chief of police defended the use of robots as a last resort option in deadly force situations to potentially prevent mass violence.
- The policy faced criticism for being overly broad, allowing for the apprehension of criminals, which could lead to misuse and unjustified lethal force.
- The public's demand for more restrictive policies stems from witnessing officers harming innocents and escaping accountability by citing training and policy.
- Beau warns that the department may attempt to reintroduce the policy once public attention wanes and urges people to scrutinize future policies for their scope and potential consequences.
- Deploying armed robots should be strictly limited to mass incidents, with clear justifications for lethal force already in place.
- There is a concern that broad policies on armed robots could result in mission creep and increased loss of innocent lives due to hasty deployment of lethal force.

### Quotes

- "We need to be looking at a way to stop that rather than new tools to do it."
- "The overwhelming outcry came from the fact that the policy was just ridiculously broad."
- "The key thing to remember when you're talking about anything like this is to actually read the policy, not the sound bites."
- "The department will undoubtedly try to sneak this through again."
- "It shouldn't be, well, maybe it would be justified. No, it has to already be deemed justified to take that thing out of the van."

### Oneliner

San Francisco rejected a broad policy on armed robots due to public outcry and the need for stricter, more justifiable regulations.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Scrutinize future policies on armed robots for their scope and potential consequences (suggested)
- Advocate for strict regulations on the deployment of armed robots only in mass incidents with clear justifications for lethal force (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the concerns surrounding the use of armed robots by law enforcement and the importance of ensuring strict, justifiable policies to prevent misuse and protect innocent lives.

### Tags

#SanFrancisco #PolicePolicy #ArmedRobots #PublicSafety #Accountability #CommunityPolicing


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about San Francisco
and robot arms and when,
at least temporarily, for the moment.
So we're going to talk about what happened.
We're going to talk about what is likely to occur
in the future, and we're gonna talk about why it happened
as we go through a statement from the chief out there.
Now, if you don't know what I'm talking about,
you missed the earlier coverage.
The San Francisco Police Department put forth a policy
for approval that allowed them to use armed robots.
So initially that policy was approved.
There was an outcry.
The Board of Supervisors, I think that's what it's called
out there, reversed course, and they have voted it down.
It will not become policy at the moment.
Somebody on the board put out a statement
that basically said, hey, we live in a time
where police are taking out more and more each year.
We need to be looking at a way to stop that
rather than new tools to do it.
It's a good statement.
Let's hope they stick to it.
So this is what the chief of police had to say.
The use of robots in potentially deadly force situations
is a last resort option.
We live in a time when unthinkable mass violence
is becoming more commonplace.
We need the option to be able to save lives
in the event we have that type of tragedy in our city.
That's good.
I mean, I can see that.
I could see something like that getting through.
The problem is that isn't at all what the policy said.
If this is actually the concern and you want to use them
in the event of a mass incident,
then write a policy that reflects that.
The overwhelming outcry came from the fact
that the policy was just ridiculously broad.
Apprehension of criminals was one of them.
That can mean anything.
In fact, when I did the first video,
the only thing I could think of that didn't fall under
what would be covered by that policy
was a normal traffic stop.
But if somebody in the car had a warrant,
well, then they could use it.
It was ridiculously broad.
What has happened is that over the years,
the public have witnessed officers taking out innocents
and then walking free because in court, they said,
well, that's what I was trained to do.
That's what the policy says.
You can't hold it against me.
So now the public wants more restrictive policies,
cause and effect.
The department will undoubtedly try to sneak this through again.
They will try to wait until the outcry has faded
and try to push it through.
The key thing to remember when you're talking
about anything like this is to actually read the policy,
not the sound bites.
This sound bite, that sounds great.
And if that's what the policy said,
if the policy was just like, hey,
we want to be able to use armed robots
in the event of a mass incident,
there probably wouldn't have been an outcry.
But that's not what it said.
And that's not how it would be used.
There would be mission creep, so to speak.
And eventually, you would see it used a lot.
And there's a guarantee that it will be misused
under the policy that was just rejected.
So while people in San Francisco made their voices heard,
they made the right call, and the board rejected this policy,
this isn't over.
The department will continue to try to push this.
As they do, just read the policy and think
about the situations that would fall under any new policy
they submit and how broad it is.
When you're talking about a narrow scope, that's one thing.
But when you're talking about something that is just
ridiculously broad, that will lead to more people being lost,
more innocents being lost.
Because the thing that even statesmen, policy makers
of all kinds, face is that it's just so easy
to deploy lethal force via a robot.
So they do it at times when they don't have all the information,
and it leads to really bad outcomes.
If San Francisco goes forward with this in any way,
it needs to be an incredibly restrictive policy,
something for mass incidents only.
And I mean, even in this soundbite, even in this little quote,
there's still an issue, because this is potential lethal force.
No, if you're deploying it, lethal force
should already be justified.
It shouldn't be, well, maybe it would be justified.
No, it has to already be deemed justified
to take that thing out of the van.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}