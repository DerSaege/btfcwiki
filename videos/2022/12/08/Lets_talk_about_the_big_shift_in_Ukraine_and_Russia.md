---
title: Let's talk about the big shift in Ukraine and Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Mhi1XwoTjKU) |
| Published | 2022/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ukraine has engaged in multiple hits within Russia, targeting airfields and fuel storage facilities, raising questions about the equipment and technology being used.
- Ukraine is suspected to be using repurposed Soviet-era drones or locally produced drones to conduct the attacks.
- The accuracy of the attacks is attributed to the use of GPS satellites or laser targeting by teams on the ground.
- Russia's air defense systems have shown weaknesses, with multiple successful hits by Ukraine over several days.
- The attacks are causing Russia to divert resources and attention, potentially weakening their position in Ukraine.
- There are conflicting opinions on whether these attacks by Ukraine are a good idea or not.
- Concerns are raised about potential accidents or hitting illegitimate targets that could change the dynamics of the conflict.
- Ukraine's success in the information war and propaganda is noted, but there are worries about potential escalation due to these attacks.
- Beau expresses his opinion that while easy for some to criticize, those in Ukraine facing the conflict have the right to make decisions on their actions.
- The situation in Ukraine and Russia is dynamic, with uncertainties about how these attacks will impact the ongoing conflict.

### Quotes

- "Accidents happen."
- "This puts that in jeopardy."
- "They're winning when it comes to the information space."
- "It's a risk."
- "They may be more anxious to put Russia on its back hill."

### Oneliner

Ukraine's strategic attacks inside Russia raise questions of effectiveness and potential consequences, amid conflicting opinions on the approach.

### Audience

Activists, policymakers, analysts

### On-the-ground actions from transcript

- Support organizations aiding civilians affected by the conflict (suggested)
- Stay informed about the situation in Ukraine and Russia through reliable sources (implied)

### Whats missing in summary

Insights on the potential long-term implications and geopolitical repercussions of Ukraine's attacks within Russia.

### Tags

#Ukraine #Russia #Conflict #Geopolitics #MilitaryStrategy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the big news coming out
of Russia and Ukraine.
And we're going to go over two theories, two possibilities,
and two schools of thought.
We're going to kind of go over what's happening,
and then we're going to talk about whether or not it's
a good idea.
And there are competing concepts throughout all of this.
OK, so what is the big news, in case you've missed it?
Ukraine, over multiple days, has engaged in multiple hits
within Russia.
And within Russia, I don't mean just over the border.
We're talking 400 miles or more.
They've hit a couple of airfields,
and I want to say it's a fuel storage place.
All legitimate targets.
This is why NATO did not provide the longer range stuff.
Recently, in a video, we talked about knowing
is half the battle.
The other half is violence conducted over hundreds
of miles and accurate to within meters.
Yeah, it seems like they got that.
But this isn't going to be NATO gear.
And that brings up the question, what are they doing it with?
And there are two competing theories, really three,
but two for the most part.
One is that they're using repurposed Swifts.
Swifts were Soviet era photography drones,
for lack of a better term.
And the theory is that they kind of took components out
that were unnecessary and replaced them
with stuff that goes boom.
And that's what they're using.
Another theory is that it's something
that they came up with on their own,
that it is locally produced.
That possibility should really concern Moscow.
The third theory, which is kind of a sub theory,
is that basically they're just kind of using
the shell from the Swift.
And everything else is made in Ukraine.
OK, so that's how they're doing it, theoretically.
How are they targeting it, because they've been accurate?
There are two competing theories there.
One is the use of GPS satellites, tech
that Americans are familiar with.
The other is that they have people,
whether they be sympathetic Russians or a very well-trained
special operations team or teams inside of Russia,
lazing the targets.
For those not familiar with this,
gross oversimplification, a laser pointer
that talks to the drone as it flies in.
And wherever that thing is pointed,
well, that's where it's going to land.
And that would also explain the accuracy.
If I was Ukraine at this point, I
would be doing everything I possibly
could to make Russia believe that it was teams
lazing the targets, whether or not it is.
Like at this point, I would be at the broadcasting,
fake radio messages to teams that don't exist,
and doing everything within my power
to make them believe that's how it's being done.
OK, so from there, we go to the Russian side.
What's going on there?
First, they have a problem with their air defense,
which is odd because most estimates suggested
that it was up to par.
It was one of the few things that even skeptics believed
was still functional.
It has huge issues.
The first hit, well, that's understandable.
They didn't know it was coming.
Multiple hits over multiple days,
and they're getting through.
They're getting through Russian air defense.
That's embarrassing.
That is embarrassing.
And it is most likely a training issue,
which is really the worst case scenario for Russia.
If it was an equipment failure, they
could probably move equipment from areas
that are less likely to have something go over them
and replace the equipment.
If it's a training issue, that takes longer to fix.
The other thing that should concern Russia
is that the odds are Ukraine isn't
going to go full range with these things the first few times
they use them.
In all likelihood, Moscow is in range.
That is probably a large concern now.
Now, we get to the big question.
Is this a good idea?
There are two competing schools of thought,
and I will go through both of them
and then I'll tell you which one I'm in.
The first school of thought says this
is a good idea because applying pressure inside Russia
through this means it requires Russia
to focus attention on protecting their own country
rather than the invasion.
It requires them to divert resources and people
to counter this.
Obviously, they have to do something
about their air defense, but then they
need people looking for the teams that may or may not
be lazing these targets.
There's a lot of resources that would get consumed and kind
of keep Russia on its back hill, especially given the fact
that Russia is having a lot of issues with forced generation,
with getting troops as it is.
Diverting them out weakens Russia's position
inside of Ukraine.
All of this is true.
All of this is true.
And thus far, the way Ukraine has gone about it,
I should point out that Ukraine still hasn't actually
said it was them.
But I mean, I feel safe in suggesting that maybe they
had something to do with it.
Ukraine's been doing well with it.
So the competing school of thought
here is that it's a bad idea.
And I'm actually in that camp.
The reason is simple.
Accidents happen.
Accidents happen.
Right now, they're doing great.
Those things, they are obviously hitting
right where they want them to.
And if there was a guarantee that that would continue,
it might change the math.
But as it stands, Russia's resolve is failing.
In the beginning, all of Russia was behind it.
All wars were popular for the first 30 days.
Once conscription started, resolve
started to kind of break down.
And then when the Ukrainian counteroffensives
were so successful, you started seeing actual dissent,
and even dissent on the airwaves.
That is a good position for Ukraine to be in.
They are winning when it comes to the information space.
They are dominating the information space,
the propaganda war.
If one of these things goes down in the wrong spot
and hits something that isn't a legitimate target,
all of that might change.
And it could change very quickly,
depending on what's hit.
It could be something that would reinvigorate
Russian resolve, which isn't something Ukraine wants.
Right now, they're in a good position.
This puts that in jeopardy.
Now, the question that I think most Americans want answered
is, is this going to cause an escalation?
It might.
It might.
That's a risk.
It's a risk.
But it is a decision that the people in Ukraine get to make.
And that is also something that everybody
needs to keep in mind when they're
talking about the two schools of thought
when it comes to whether or not this is a good idea.
I'm in the camp that says this isn't a good idea.
But it's also super easy for me to say that in a place that's
warm, that has running water, that has electricity,
and that doesn't have Russian rockets falling on it.
They're the ones that are making these calls.
And in the situation they're in, they
have the right to make them.
Understand, this is completely within bounds
as far as the laws of combat.
There's nothing wrong with what they're doing.
And it's easy for us in locations that are safe to say,
be cautious, be cautious.
They may be more anxious to put Russia on its back hill
and hopefully win the war faster.
And that is probably feeding into their decision.
And I can't say that they're wrong for wanting that.
When we talk about strategies and we
talk about what countries should do or shouldn't
do in a given situation, we have to remember
that we're doing it from the comfort of a place that
is relatively safe.
So that's what's going on.
Moscow's response to the first couple of hits
was just more of the same.
The reality is eventually they're going to run out.
And given the various scenarios as far
as where Ukraine is getting its stuff to do this,
Russia might run out before Ukraine.
There's a dynamic shift that has occurred.
And we're going to have to wait and see
how much it's going to shift things
on the ground and politically.
If I was Putin, I would be very concerned about the reactions
to this news in the country.
Because while Ukraine has to worry about something
landing in the wrong spot, Putin has
to worry about something landing in the right spot
and in the wrong spot.
Because there's also the possibility
that the local populace in Russia
blames him if something goes wrong
and an illegitimate target is hit.
So there's probably going to be a lot of news coming out
of Ukraine over the next month that
will kind of shape what happens in the future based
on this being deployed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}