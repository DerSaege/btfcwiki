---
title: Let's talk about the railroads....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SktGz0IL0nw) |
| Published | 2022/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Railway workers and bosses are negotiating a new contract, primarily focusing on scheduling issues rather than money.
- Workers are treated poorly, expected to work long hours without adequate time off.
- There was a tentative agreement that the unions voted down, leading to the possibility of a strike on December 9th.
- A strike could cause billions of dollars in economic damage to the US, prompting government intervention.
- The House passed a bill imposing the rejected labor deal and providing additional sick days, now awaiting Senate approval.
- Bernie Sanders will play a key role in pushing the legislation through the Senate.
- Beau criticizes the Republican Party for prioritizing political power over the well-being of the economy and people.
- The Democratic Party's trust in Republicans to act sensibly is questioned by Beau.
- Beau expresses disappointment in Biden siding with railway companies over workers, damaging his standing with organized labor.
- The moral aspect is emphasized, where workers' absence causes economic harm while CEOs' absence does not.

### Quotes

- "If your job is so vital that if you don't show up, the whole country stops, you probably deserve a decent quality of life."
- "The administration and Congress had the ability to stand with the workers, but they did the absolute bare minimum."
- "It truly damaged his standing there and the Democratic Party standing as a whole with organized labor."
- "Nothing. Nothing. It doesn't cause billions of dollars per day in economic damage to the United States. That happens if the workers don't show up."
- "Biden has a pretty strong labor record. And in this situation, he's kind of sided with the bosses."

### Oneliner

Railway workers face negotiations over scheduling, with potential economic repercussions, as government intervention favors industry over workers' quality of life.

### Audience

Congress Members

### On-the-ground actions from transcript

- Contact Congress members to advocate for fair treatment of railway workers (suggested)
- Stay informed about the ongoing negotiations and potential strike actions (suggested)

### What's missing in summary

The full transcript provides a detailed analysis of the railway workers' situation, the political dynamics involved, and the moral considerations that should guide decision-making.

### Tags

#RailwayWorkers #Negotiations #GovernmentIntervention #EconomicImpact #WorkerRights


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the railways.
We're going to talk about what's going on there,
what Congress and the administration have done
and are doing and are expected to do.
That's going to be one part of this video.
And then we're going to talk about the giant unforced error
that has just occurred.
OK, so if you have no idea what's going on,
the railway workers, we're going to simplify this
because there's a bunch of different unions involved
and a bunch of different companies.
We're going to keep it simple.
The workers and the bosses, they're
negotiating a new contract.
And negotiations have been running, let's just say,
it's been tense.
The bosses are not wanting to basically give anything up.
In this case, it's not really about money.
It's about scheduling.
And the way rail companies have been running,
in the less than decent way, their employees
end up being treated.
What they're expected to do, how long
they're expected to be on call, how they really
can't take time off.
And that's what this is really hinging on.
There was a tentative agreement that went to the unions
a while back and they voted it down.
So December 9th is the date where
the unions might go on strike.
So from the administration's point of view
and the government's point of view in general,
if the workers go on strike, it causes roughly a couple
billion dollars per day in economic damage
to the United States.
The US economy can't really take many more stressors.
This would be a huge one, especially
right before the holidays.
So the government has decided to intervene.
Biden kind of told Congress, hey, do something about this.
The House passed a bill basically imposing the labor
deal that was voted down.
So that's being forced upon the industry.
There is an additional piece of legislation
that gives them seven more sick days a year.
OK, so they didn't do the absolute bare minimum.
They did a little bit more.
Now from this point, it goes to the Senate,
where basically it's up to Bernie.
Bernie has to find a way to get this through.
The Republican Party, we don't know what they're going to do.
If it doesn't go through, the economy suffers
and they get to blame Biden.
The Republican Party has not demonstrated
that it will put the country first.
It cares more about political power.
And if people have to suffer for that,
well, that's just the way it is.
Keep in mind, this is the party that
lost a whole bunch of money.
That lost a whole bunch of its own base
because they didn't want to take basic safety procedures.
They didn't want to take basic precautions.
They didn't advocate for it.
Now, the good news is that the reason the Republican Party
was willing to lose a large section of its own base
was to keep the economy going.
So there is a chance that they go along with this.
But I would point out that the Democratic Party has put itself
in a situation where they are trusting Republicans
to do the sensible thing at the moment.
I don't know that that's a winning strategy.
I don't know that a lot of people have won making that bet.
So best case scenario, if you're concerned, is the economy,
is that Bernie can pull it off and get this through
with the additional sick days.
That's the best case scenario for the sake of the economy.
Now let's talk about the unforced error
that has happened.
Biden has a pretty strong labor record.
And in this situation, he's kind of sided with the bosses,
with the railway companies, not the workers.
Progressives in the House had to fight
to get those seven jobs.
Progressives in the House had to fight to get those seven days.
And I understand that's not enough.
It truly damaged his standing there
and the Democratic Party standing as a whole
with organized labor at a time when support for organized labor
is surging.
That was a bad move politically.
Now let's talk about the moral aspect of this.
What happens if all the CEOs of these companies
decide to, I don't know, go play golf together
for a couple of weeks?
Nothing.
Nothing.
It doesn't cause billions of dollars per day
in economic damage to the United States.
That happens if the workers don't show up.
That alone should have been the deciding factor
in where the weight was thrown,
on which side they chose to be on.
And it wasn't.
The administration and Congress had the ability
to stand with the workers.
They had a whole bunch of options on the table
and they did the absolute bare minimum for the workers.
I think this is going to come back to bite them.
I understand the government's point of view
in wanting to avoid that economic devastation.
And yeah, they're right.
The US really can't take that right now.
However, they didn't have to just abandon the unions
during this process.
They could have gone much further than they did
and they could have stood with the workers
to get them a decent standard of living,
which is what they're after.
This isn't about money here.
This is about being able to have a quality of life.
And I would suggest if your job is so important,
if your industry is so important,
that if you don't show up to work,
the entire country stops, you probably
deserve a decent quality of life.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}