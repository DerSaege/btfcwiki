---
title: Let's talk about why it's important to understand US capabilities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E1sXe9zNau0) |
| Published | 2022/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of understanding the U.S.'s military advancement.
- Counters the notion that discussing U.S. military superiority is imperialist propaganda.
- Points out that discussing military technology facts is not propaganda but an objective reality.
- States that the U.S. is decades ahead in military technologies compared to other nations.
- Suggests that this awareness could actually reduce defense expenditures.
- Raises questions about why conflicts haven't ended favorably despite U.S. military superiority.
- Proposes a shift towards a more humanitarian approach post-conflict.
- Advocates for a role as the world's "EMT" rather than a global police force.
- Emphasizes that deploying facts appropriately can lead to positive outcomes.
- Encourages reflection on more peaceful approaches to global conflicts.

### Quotes

- "Facts are not inherently good or bad. It's how they're deployed, just like any other technology."
- "The technology has advanced to the point that humanity's ability to conduct violence has advanced to the point where non-state actors are capable of engaging in a low-intensity conflict that undermines even the highest levels of technology."
- "It's a statement that can be used to inoculate against it."
- "If that style was used, being the world's EMT instead of being the world's policeman, if that's what was done, we probably wouldn't need the war."
- "The United States has an unparalleled ability to wage war."

### Oneliner

Beau explains the objective reality of U.S. military superiority and advocates for a shift towards a humanitarian approach in global conflicts, aiming to save lives and empower affected nations.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Advocate for a shift towards a humanitarian approach in global conflicts (implied)
- Support policies that prioritize post-conflict rebuilding efforts over military occupation (implied)

### Whats missing in summary

In-depth analysis on historical conflicts and the implications of U.S. military doctrine.

### Tags

#MilitarySuperiority #GlobalConflicts #HumanitarianApproach #DefenseSpending #USMilitary


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a message I received,
and we are going to talk about why it's important
for everybody to understand how far ahead the U.S. is
when it comes to military applications and stuff like that.
Because I got a message and there's a read on why I talk about this,
and it's... I feel like maybe I haven't been clear.
Okay, so when you talk about how far ahead the U.S. is militarily,
it reeks of imperialist propaganda and justifies America's massive defense budget.
The entire video responding to the Russian troll was just an ad for Raytheon.
Give me any other way to take that.
The Highmars is made by Lockheed Martin, but I mean, that doesn't...
that's not critical to your point.
The idea here is that talking about how far the U.S. is ahead
when it comes to military applications and the R&D that goes along with it
and all of that stuff, that it is imperialist propaganda
and is somehow supportive of massive defense budgets.
Okay, imperialist propaganda.
It's not. It's objective statement of fact.
The United States, when it comes to most military technologies,
is decades ahead of its nearest competitor.
The equipment that is being used in Ukraine right now,
most of it has its base model. Yes, they were upgraded over time,
but most of it has the base model being developed in the 1990s.
Some of the systems are from before then.
Almost all of them are from the 1900s.
It's objective statement of fact.
The United States has an unparalleled ability to wage war.
There is no other nation state on the planet that can go toe-to-toe with the U.S.
Does not exist. Objective statement of fact.
It's not propaganda. It's just an accurate assessment of the situation
and justifies America's massive defense budget.
Does it though? Does it?
Or would it maybe inoculate people against fear-mongering
if they understood how far the U.S. was ahead?
The next time Lockheed Martin or Raytheon pushes a bunch of Congress people and says,
hey, we need to spend $700 billion on R&D or we'll fall behind.
People who understand how far ahead the U.S. is when it comes to R&D of this sort,
they're sitting there saying, no, we could do no R&D for five years
and still be a decade ahead.
It doesn't justify the massive defense expenditures.
If anything, it's a fact.
It's a statement that can be used to inoculate against it.
My guess, and it is a guess, is the reason you feel this way is because you have bought in
to the struggles of our betters, flags and maps and interests of nations and all of that.
And you have found yourself vested on one side or the other.
So when you hear information that is counter to what you want to hear,
well, it's propaganda and you can only see it through the lens of nationalism.
It can only be used to justify massive defense expenditures.
If you weren't concerned about which nation was winning, that's not how you'd see it.
You would see it as a tool to suggest the U.S. is spending too much on defense research.
It's actually, I don't want to say too far ahead, but its doctrine is being overshot.
And it could be used to reduce defense expenditures.
Aside from that, if you understand that the U.S. has an unparalleled ability to wage war,
more so than any country in the history of humanity, it prompts another question.
That's the case. It's a statement of fact.
So what has happened the last few wars?
Why has it gone down the way it did?
If nobody can go toe to toe with the U.S., why have these conflicts ended the way they did?
And the answer to that is simple.
The technology has advanced to the point that humanity's ability to conduct violence
has advanced to the point where non-state actors are capable of engaging in a low-intensity conflict
that undermines even the highest levels of technology.
Which means the U.S. doctrine is wrong.
It means that it's not effective because at the end it doesn't win.
That's why there's always that qualifier of going toe to toe.
There's not a country that can do that.
However, once the United States arrives and tries to put boots on the ground
and stay there and occupy it, everything changes.
And if you can grasp that and understand that, you can move to the point where,
hey, so what are we doing wrong?
And that's when you run into the idea of the Department of Something Else, as Barnett calls it,
or being the world's EMT, that secondary force that's primarily there
for after the toe-to-toe fighting is over.
They're there to rebuild the country.
And if that was implemented, what you would soon find out, that if that style was used,
being the world's EMT instead of being the world's policeman,
if that's what was done, we probably wouldn't need the war.
You could just go in and use that method from the start.
And it would save a whole lot of lives.
And incidentally, it would put the countries under the control of the people in those countries.
It would be kind of the opposite of imperialism.
Facts are not inherently good or bad.
It's how they're deployed, just like any other technology.
Anyway, it's just a thought.
And I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}