---
title: Let's talk about Trump companies being found guilty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Z4Ys2dugL3U) |
| Published | 2022/12/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump companies found guilty on all counts in New York for tax fraud involving the Trump Corporation and Trump Payroll Corporation.
- Former President Trump and his family are not at risk of going to jail in this case, but legal analysts see this as a building block to target them as individuals in the future.
- The case will likely result in fines for the companies rather than imprisonment.
- Legal analysts suggest that this case could make it easier to pursue civil cases against the Trump Organization, including a significant quarter-billion-dollar case in New York.
- The criminal tax case's outcome may impact the civil case involving asset valuation discrepancies and make it easier to proceed.
- Some analysts believe that Trump's legal exposure is greater in the criminal case than in other ongoing cases, but Beau is skeptical and sees the Mar-a-Lago documents case as more concerning.
- Beau advises Mar-a-Lago employees to stock up on plastic, hinting at a potential reaction from Trump to the news.

### Quotes

- "Trump companies found guilty on all counts in New York for tax fraud."
- "Former President Trump and his family not at risk of jail time, but a building block for targeting them."
- "Outcome may impact civil cases, including a quarter-billion-dollar one in New York."
- "Legal exposure greater in criminal case, but Mar-a-Lago documents case is more concerning."
- "Plastic, plastic, a whole lot easier."

### Oneliner

Trump companies found guilty of tax fraud in New York, paving the way for potential future legal troubles for the Trump family, with fines likely and civil cases looming.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Stock up on plastic (implied)

### Whats missing in summary

Insight into the potential consequences for the Trump Organization and the broader implications of the trial.

### Tags

#Trump #TaxFraud #LegalAnalysis #CivilCase #CriminalCase


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the news
out of New York.
If you have missed it,
the Trump companies were found guilty on all counts.
This has prompted a whole bunch of questions
and we're just gonna kind of run through them
and talk about the downstream effects of this trial
and what's happened.
First is this is broadly being said
as the Trump Organization.
It's really two companies,
the Trump Corporation
and I think the other one is Trump Payroll Corporation.
Those were the ones that were found guilty.
And it's basically, to oversimplify it, it's tax fraud.
That's really what this is about.
Now, people are asking,
does this put former President Trump
or his family, are they at risk?
Are they gonna go to jail over this?
Nah, no, not this case.
However, there are a bunch of legal analysts
who are saying that this is a building block
to going after the Trump family as individuals.
Right now, they're talking about the companies
because in the United States, companies are people.
They don't lock up companies.
What they end up doing,
it's gonna be a fine.
That's what's gonna happen here.
The analysis saying that this is gonna be a building block
to going after them as individuals,
that's outside my scope.
The people that are saying that this is leading to,
they're mostly ex-prosecutors.
So maybe, but I don't know.
The other question,
is this the case that is just going to stand a really good chance
of just absolutely destroying Trump Organization?
No, oddly enough.
I know it's hard to keep track
because there are so many cases involving Trump.
This is a criminal tax case up there.
The one that is looking at like a quarter billion dollars,
that is a civil case in New York.
It's actually different cases.
Whether or not that actually leads
to Trump Organization just going away,
we'll have to see what happens during that case.
I will say there aren't a lot of companies
that can take a quarter billion dollar hit.
Now, legal analysts are saying that today's events
involving the tax case will make it much easier
for the civil case involving the discrepancies,
let's say, as far as the asset valuation
and all that stuff, the civil case.
So this will make it easier for the civil case.
That makes sense to me.
Some of the analysts are saying that
because of the events in the trial
for the criminal case up there,
the one where the companies were found guilty,
that this should actually be Trump's focus
because he is at greater legal exposure there
than any of the other cases.
I don't know about that.
I think that might be more wishful thinking on their part.
To me, looking at everything,
the documents case down there in Mar-a-Lago,
that is the one he should be worried about
because that's just wild.
And while we're talking about Mar-a-Lago,
I would just like to provide some friendly advice
to the employees down there.
Plastic, plastic, a whole lot easier.
Because I got a feeling Trump might have a reaction
to the news today.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}