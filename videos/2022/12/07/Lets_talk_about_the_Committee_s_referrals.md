---
title: Let's talk about the Committee's referrals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y7Od12rzEEc) |
| Published | 2022/12/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau questions the significance of news from the committee and whether it truly matters, considering the current circumstances.
- The committee plans to make criminal referrals to the Department of Justice related to January 6, but Beau doubts if it will lead to any substantial action.
- Despite Trump's lack of cooperation with subpoenas, Beau believes these referrals won't spark new actions from the Department of Justice.
- Beau speculates that the committee might be issuing these referrals to take credit for actions already in progress.
- He suggests that the committee's move could be a sign that the Department of Justice will proceed further with the investigation.
- Beau predicts a rough road ahead for Trump, indicating that his tough week might be a sign of things to come.
- The committee seems confident that the Department of Justice will act on the referrals, possibly leading to indictments in the future.
- Beau contemplates whether the referrals are necessary or just a formality as the proceedings wind down.
- He hints at a potential turning point for Trump as the investigation progresses.
- Beau ends with a casual "y'all have a good day" sign-off.

### Quotes

- "The referrals are kind of just extra, with the exception of the subpoena thing."
- "I don't think that they [the committee] issue these referrals unless they were very certain that the Department of Justice was going to move forward."
- "Trump's week has been pretty bad. I have a feeling his better days may be behind him."

### Oneliner

Beau questions the significance of committee news and speculates on its implications for Trump, hinting at potential future indictments and rough times ahead.

### Audience

Political observers, Trump supporters

### On-the-ground actions from transcript

- Follow developments in the Department of Justice's actions (implied)
- Stay informed about the ongoing investigations (implied)

### Whats missing in summary

Insights into the potential political ramifications and legal consequences of the committee's actions.

### Tags

#CommitteeNews #DepartmentOfJustice #January6 #Trump #Indictments


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the news
from the committee and whether or not
that news really matters.
The committees kind of indicated that they
are going to proceed with making referrals.
And when I first heard it, it didn't really
make any sense at this point.
It would have made sense earlier.
But right now, I mean, what is it really going to do?
Is it going to initiate any action?
Not really.
The committee has indicated that it's
going to issue criminal referrals
to the Department of Justice for things related to January 6.
Now, with the notable exception of Trump
not cooperating with a subpoena, not cooperating
with the committee, none of it actually matters.
It doesn't initiate any action.
The Department of Justice has a grand jury going on.
And we just found out subpoenas have been flying out
all over the country since last month.
The Department of Justice isn't going to start anything new
because of these referrals.
So why are they doing them?
One thing that we know is that the members of the committee,
they're not bad at what they do.
They're pretty politically astute people.
So if the referrals aren't going to initiate any action
because that action is already being taken,
what would be another reason to issue the referrals?
To take credit.
If I'm Trump or in Trump's circle,
this is really, really a bad sign.
The committee absolutely believes
that the Department of Justice is going to proceed.
I can't think of any other way to read this.
They wouldn't issue the referrals
if they didn't think that DOJ was going to move forward
with it beyond the stage that it's already in.
The stage it is already in is beyond the stage that
would be triggered by the referrals.
So the referrals are kind of just extra,
with the exception of the subpoena thing.
So they have nothing to gain by issuing the referrals
unless they know something we don't.
And they're positioning themselves to say,
we had the committee, we issued the referrals,
Department of Justice then moved on it.
I don't think that they would issue these referrals
unless they were very certain that the Department of Justice
was going to move forward from where they're at now.
And where they're at now is a grand jury.
The next stage here would kind of be indictments.
It's a unique development, or I guess the alternative
would be they're just trying to close it out,
because it's winding down at the end of this month.
But they could close it out with a report.
I don't know that the referrals are necessary
to close down the hearings.
It's not something that anybody was really
expecting at this point, because the process has already started.
This may be just cosigning what they have heard
is going to occur when it comes to the Department of Justice.
Trump's week has been pretty bad.
I have a feeling his better days may be behind him.
It may get real bumpy for him from here on out.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}