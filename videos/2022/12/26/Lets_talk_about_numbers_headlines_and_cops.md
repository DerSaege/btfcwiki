---
title: Let's talk about numbers, headlines, and cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=h8hTI83susc) |
| Published | 2022/12/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Accepting a challenge to analyze numbers and headlines, particularly regarding police-related information.
- Responding to a message about 650 cops being gunned down this year, Beau clarifies that 655 cops died in the line of duty in 2021.
- Points out that COVID was the leading cause of death for cops, not gunfire, with 472 deaths attributed to COVID in 2020 and 214 deaths from gunfire in 2022.
- Emphasizes the importance of fact-checking and getting information from reliable sources like the Officer Down Memorial page.
- Criticizes the media for framing information in a way that generates emotional reactions rather than providing accurate data.
- Concludes by stating that COVID, not criminal activity, is responsible for the majority of cop deaths, urging people to critically analyze the information they receive.

### Quotes

- "You were provided information that, while accurate, led you to the wrong conclusion."
- "The person responsible for it, who's responsible for it, COVID."

### Oneliner

Beau challenges misconceptions about police deaths, revealing COVID as the primary cause and urging critical analysis of headlines and information.

### Audience

Critical thinkers, fact-checkers.

### On-the-ground actions from transcript

- Fact-check statistics from reliable sources like the Officer Down Memorial page (suggested).
- Encourage others to verify information before accepting it as truth (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of police deaths, revealing the true causes and advocating for critical analysis of information. 

### Tags

#Police #Misinformation #FactChecking #COVID19 #MediaBias


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about numbers
and how to process headlines and information.
And I'm also accepting a challenge that
was posed to me via a message.
So we are going to go over some numbers.
We're going to track down what is contained in the message
and kind of parse through the information
and see if maybe headlines are written
to generate an emotional outcome rather than provide you
accurate information.
It is worth noting, I got this message shortly
after tweeting out news that five cops in Louisiana
have been charged.
Beau, you've been putting out a lot of videos this week
that seem to be against police and are always
for what you've called social or racial justice.
You like numbers.
So here's one I got from Fox, but then checked
from a source you would believe.
That was a good move.
650 cops were gunned down this year.
Maybe you can find the numbers on who is responsible,
because you know the media won't tell us that.
They've got a tough and dangerous job.
Sometimes it's racial profiling, and sometimes it's just, darn,
fine detective work.
OK.
Challenge accepted.
First, I would like to say that I've seen better detective
work on Reno 911.
OK, 650 this year.
No, that's not true.
That's inaccurate.
That is not accurate information.
655 in the line of duty deaths was the last year, 2021.
We will get to this year's numbers as well.
Don't worry.
655.
You want to know who's responsible.
There is a reason you're not getting that from the media
source you apparently prefer, because it
doesn't fit their narrative.
655 in the line of duty deaths, not gunned down.
The number one assailant, the demographic
that took out the most cops, was COVID.
472 of those were COVID.
Add in 23 heart attacks, 14 related to 911 related illnesses.
If you don't know what that's about, that is a real thing,
just so you know.
It's cops breathing stuff in that day,
having really long-term effects.
But it's line of duty, not gunned down.
So that year, there were 64 that were actually by gunfire.
Two of those were inadvertent, is how they classify it.
So an accident or friendly fire, something like that.
So less than 10% of the number you were actually given.
If you want to check these numbers,
you can get them from the Officer Down Memorial page.
It breaks it down by year, category, everything.
Provides all of the information you need.
If you don't believe the media, the media
didn't tell you it was COVID, because I mean, let's be real.
Fox isn't going to tell you that.
OK, what about this year, 2022?
The year is not over yet, but we're in December.
We've got a pretty good set of numbers to work with.
Leading cause of death, guess, COVID, once again.
Once again, it is COVID.
This year, there was a total of 61 from gunfire.
Four of those were inadvertent.
Total of 214 this year, 214.
Lower number of deaths from COVID, 472.
That number is now dropped, along with everything else.
214 doesn't sound nearly as high as 650, right?
So here's the reality of it.
You were provided information that, while accurate,
led you to the wrong conclusion, probably
because it was framed that way.
If you got it off of Fox, my guess
is when they said 650, and they probably
said in the line of duty.
But behind them, they had a picture
of somebody that didn't look like you, and it was scary.
And they used that to manipulate you.
That's how it works.
The person responsible for it, who's responsible for it,
COVID.
That's who's responsible for it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}