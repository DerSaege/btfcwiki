---
title: Let's talk about accidentally left Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=78v_RetwCdE) |
| Published | 2022/12/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Videos circulating show hard-right Trump supporters expressing leftist economic ideas like getting rid of banks and questioning the need for money.
- Rural Americans live by leftist economic principles in daily life through co-ops, unions, farm aid, and resource redistribution.
- Despite appearing leftist economically, the MAGA crowd is socially conservative, regressive, and sometimes racist.
- Their desire to eliminate banks and landlords stems from seeking a reset of the current system to benefit themselves.
- Their economic beliefs are often rooted in conspiracy theories and blaming specific groups.
- They are not seeking equality but rather hoping for a shift in power dynamics.
- Winning cultural and moral battles is key to reaching these individuals, as appealing to economic theories may reinforce negative stereotypes.
- The goal is to lead them to seek real freedom for everyone and reject the concept of oppressive power structures.
- Until they become socially progressive, they are not allies in the fight against inequality and racism.
- To create a society based on economic equality, it's vital to address cultural and moral aspects rather than solely focusing on economic theories.

### Quotes

- "Rural people live our lives by leftist economic principles every day."
- "They're not looking for equality."
- "We have to win the cultural battles."
- "They're not allies."
- "Playing into the economic stuff isn't going to help."

### Oneliner

Hard-right Trump supporters express leftist economic ideas but lack social progressiveness, not allies in the fight against inequality.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Challenge conspiracy theories and negative stereotypes through community education and engagement (implied).
- Engage in cultural and moral dialogues to foster social progressiveness among those with differing economic beliefs (implied).

### Whats missing in summary

The nuances of navigating ideological differences for social progressiveness.

### Tags

#PoliticalBeliefs #EconomicIdeas #SocialProgressiveness #Inequality #ConspiracyTheories


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about some videos that have been circulating,
some philosophical and economic theory,
and we're going to talk about an idea that is starting to surface
about the right in the United States
and whether or not it actually holds up to scrutiny.
If you've missed it, lately there have been a lot of videos
of hard, hard, hard right Trump supporting people.
And they're being interviewed and they're being asked about what they think needs to be done.
And they're saying things like, get rid of the banks.
You have one that has gone viral is a person actually saying, I don't know why we need money.
They are talking about what would appear to be leftist economic principles.
And by leftist, I don't mean liberal.
I mean, crossing over that line, being not capitalist.
And that's what it sounds like in all of these videos.
And yeah, I mean, it's something that you've heard me say on this channel over and over again.
Rural people live our lives by leftist economic principles every day.
From the co-op to the union to farm aid to the redistribution of resources via the church.
It's all there.
It's just a rural American version of it.
But they're not leftists.
Don't make that mistake.
That's the point of a lot of these videos is that the MAGA crowd is just confused.
They're really leftists, but they just don't know because they were tricked.
I mean, there's an element of truth to that, but not in the way a lot of people think.
In these videos where I have talked about this in the past, one thing I always point out is that while they believe
in leftist economic principles, they are socially conservative to an extreme.
Sometimes they're socially conservative.
Sometimes they're socially regressive.
Sometimes they are just straight racists.
And those two things don't go together.
They are not looking for equality.
So it's not really leftist economic principles because that's the point of it, right?
They're not leftists.
When they talk about getting rid of the banks and getting rid of the landlords and all of this stuff,
they're not doing it coming from a place of left-leaning theory.
They're hoping for a reset of the current system.
They know they have a boot on their neck, and they're hoping that if a reset button gets pushed,
that when things shake out, they'll be the boot owner this time.
That's what they're looking for.
They very easily can be reached, but it's not by appealing to the economic theory.
Most of their beliefs and why they choose the industries that they want to get rid of,
those are rooted in conspiracy theories.
And if you follow it back far enough, they're always blaming the same group of people.
They're not looking for equality.
So feeding into that side of it, the economic theory side, doesn't do a lot of good,
because what you end up doing is reinforcing some very negative stereotypes.
You end up reinforcing those conspiracy theories.
We have to win the moral battles, the social battles.
We have to get through the cultural aspects of this.
So they want equality.
They want real freedom for everybody.
We have to get them to where they don't want a boot to exist.
Not they just want to get rid of the current boot owner, so maybe one day their foot will be in it.
It's a difference of intent, and it's major.
Until they become more socially progressive, they're not your allies.
And don't think that they are, because they have a very different goal.
If we want to move to the world of Star Trek rather than Mad Max, we have to win the cultural battles.
We have to defeat racism, not just at the institutional level, but in people's minds.
We have to actually embody all of those ideas and get it out there,
and get those people who are aligned economically in theory.
You have to give them the moral framework for that economic theory to work.
It doesn't matter if they want a stateless, classless, moneyless society,
if that society only encompasses people who look like me.
They're not allies.
They can be reached, but we have to win the cultural and the moral fights.
That's where you have to talk to them.
Playing into the economic stuff isn't going to help.
In fact, it may make things worse.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}