---
title: Let's talk about a question on heroes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E3d2OtAGpJs) |
| Published | 2022/12/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People still seek heroes to look up to, desiring to measure themselves against someone who has lived the life they aspire to.
- Musk suspended the Jet Tracker account despite declaring he was leaving it up on his new Twitter account, raising questions about heroism in today's world.
- History's great personalities, like Gandhi and Mandela, are not without flaws, illustrating the impossibility of finding a perfect hero.
- Beau admires Teddy Roosevelt but acknowledges his flaws, underscoring that everyone, including heroes, is flawed.
- Heroic people do not exist, only heroic actions, as heroism can often mask underlying frustrations.
- It's possible to admire individuals for specific characteristics, but expecting perfection leads to disappointment.
- The pursuit of an idolized, perfect figure to always look up to is futile; the only person to measure yourself against is your past self.
- Continuous self-improvement should be the focus, rather than searching for a flawless hero to guide your life.

### Quotes

- "Nobody is perfect like that."
- "Everybody is flawed."
- "The only person that you should measure yourself against is you."
- "Your childhood hero should kind of be irrelevant at this point."
- "If you have lived five, ten, 15 years and you haven't changed your beliefs your opinions you've wasted your life."

### Oneliner

In today's flawed world, seeking heroes is natural, but the true measure of growth lies in self-improvement, not idolizing perfection.

### Audience

Aspiring individuals seeking guidance.

### On-the-ground actions from transcript

- Measure your growth by comparing yourself to your past self (implied).

### Whats missing in summary

The full transcript delves into the inherent flaws of historical figures and the futility of searching for perfection in heroes, guiding individuals towards focusing on personal growth and self-improvement.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about a question of heroism.
Heroes and people to look up to and all of that stuff.
Because today, people still look for that.
It's probably something natural, innate.
People desire somebody to have lived that life that they want to measure themselves
against.
Here's the question.
So a person's conviction or values is tested by actions when they themselves are involved.
Musk failed and suspended the Jet Tracker account after specifically identifying that
account as one he was leaving up in this new Twitter. In this day and age is it
possible to look up to anyone? Are any of history's great personalities without
flaws? And then there's little historical tidbits about Gandhi and
Mandela and a whole bunch of people just kind of illustrating that he couldn't
find any. There's two questions. In this day and age is it possible to look up to
anyone? Are any of history's great personalities without flaws? Let's start
with that one. No. No. They were people. Of course they're flawed. Of course
they're flawed. They were people, spoiler, been working on a book about this exact
topic. I think most people who have watched this channel for any length of
time know that I really look up to Teddy Roosevelt. There's a whole chapter about
about all of the messed up stuff that he did. All of his flaws. And that leads you
to the next question. In this day and age, is it possible to look up to anyone?
Yeah. Just not in totality because everybody's flawed. Everybody. There are
no exceptions to this. I was told that heroic people didn't exist by a hero,
certified, had the certificate and everything, recognized as a hero, and one
day when he was feeling especially introspective, he told me that heroic
people didn't exist, only heroic actions, and most times the heroism was just a
mask for frustration. It took me like another eight months to actually really
understand what he was saying. And he's right, nobody is perfect like that. You
You can look up to individual characteristics within a person, but nobody is going to meet
all of the expectations that we put on the idea of a hero or somebody who is admirable
to that degree.
Everybody is flawed.
Those who purity test online, trust me, they have problematic takes in their past.
is flawed. There are no exceptions to that. So you can look up to people for
for their actions, for characteristics, but don't expect them to be perfect.
They'll always let you down because nobody's perfect. Everybody. Everybody is
flawed. The reason I think a whole lot of people want there to be an idol, want
there to be somebody who is perfect, who is somebody that they can always
look up to in any situation is because they want somebody to measure
themselves against. The reality is the only person that you should measure
yourself against is you. Are you better than you were yesterday, five years ago,
ten years ago? If yes, then you're on the right track. If not, you have work to do.
Look for people that have characteristics that you admire and do
it that way. Don't try to find one person to guide your life. They will always let
you down. It's your life, it's your morals, your beliefs. They should change. Your
childhood hero should kind of be irrelevant at this point other than
through a nostalgic idea of right and wrong because if you have lived five, ten,
15 years and you haven't changed your beliefs your opinions you've wasted your
life the only person you need to measure yourself against is you anyway it's just
Just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}