---
title: Let's talk about George Santos and blame....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QU_vg5Uwx4k) |
| Published | 2022/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the accountability for George Santos' election win in New York, where he provided false information about his background.
- Allegations range from padding his resume to misrepresenting his educational background, work history, and heritage.
- Debate centers on blaming either the Democratic Party or the media for not uncovering Santos' fabrications during the election.
- Beau argues that both the Democratic Party and the media are responsible for failing to thoroughly vet Santos.
- The Republican Party running a deceitful candidate like Santos should be the focus of blame, but the blame game shifts between Democrats and the media.
- Independents and Democrats question who to blame, with the Republican Party escaping scrutiny despite running the deceptive candidate.
- Beau warns that the Republican Party's lack of accountability for running dishonest candidates could alienate independent and first-time voters.
- The Republican Party's pattern of deception harms its credibility and trust with voters, especially independents.
- The ongoing damage caused by the Republican Party's deceit may have long-lasting consequences for their reputation and voter base.
- Beau underscores the importance of accountability and transparency in political parties to uphold democracy.

### Quotes

- "Both entities dropped the ball on this one."
- "The Republican Party has lost the trust of the American voter to such an extreme."
- "Trust me, there are a whole bunch of independent first-time voters who will never vote for the Republican Party again after this."
- "The damage that the Republican Party is doing to itself right now will last decades."
- "The Republican Party has become synonymous with trying to undermine democracy in any way."

### Oneliner

Exploring accountability for a deceitful candidate in New York reveals a concerning lack of trust in the Republican Party, as Democrats and the media face blame while the real issue lies within the GOP.

### Audience

Voters, Independents

### On-the-ground actions from transcript

- Hold political parties accountable for the candidates they endorse (implied)
- Encourage thorough vetting processes for all candidates, regardless of party affiliation (implied)
- Support transparency and honesty in political campaigns (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the erosion of trust in the Republican Party due to their candidates' deceitful practices, urging a reexamination of accountability and transparency in political processes.

### Tags

#Accountability #PoliticalDeceit #TrustInPolitics #ElectionIntegrity #RepublicanParty


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about George Santos
and who's at fault,
because that's a discussion that is taking place
in a lot of corners in the United States right now.
Who is really the entity that should shoulder
the responsibility for what happened?
Now, if you don't know what occurred,
a candidate who won their election in New York
has since been found to have provided
a little bit of fiction with their background.
They creatively interpreted some things.
Like pretty much everything.
There are actually so many allegations now,
I can't even keep track of them all.
Everything from educational background
to where they worked, to heritage, I mean, everything.
Some of it is normal padding of a resume.
Some of it isn't.
So this person won their election.
The election's over.
They won.
And the discussion that's taking place is,
because it's the United States, you know, who to blame?
Who to blame?
The two candidates being offered up
to accept the responsibility
are the Democratic Party or the media.
Okay, Santos is a Republican,
but the Democratic Party, by this argument,
should have engaged in basic opposition research
and they would have uncovered this.
Yes and no.
They would have uncovered a lot of it.
Some of this is outside the scope
of normal basic opposition research.
But some of it certainly would have been uncovered
if they had done that.
The other candidate is the media.
The local media should have investigated somebody
who is running for public office in a position of trust,
running to represent the area that they cover.
And they should have investigated them a little bit
and found more out than basic opposition research would have.
I don't know that this is an either or moment.
That's the debate, is which one is to blame.
And you see people shuffling the blame back and forth.
It's not either or.
Both entities dropped the ball on this one.
But that's not who's to blame.
That's not who's to blame.
Only in today's age would the Republican Party
running a candidate like this somehow be the fault
of the Democratic Party or the media.
Think about what's being said there.
The reality is the fact that the Republican Party
who ran the candidate is not even in the discussion
about who to blame, that should terrify the Republican Party.
Because this conversation,
it's not taking place among Republicans.
It's taking place among independents and Democrats.
That's who's trying to figure out who to blame.
And they don't even consider blaming the Republican Party
because they, including the independents,
just accept the fact that the Republican Party
is going to lie and be deceitful.
The Republican Party has lost the trust
of the American voter to such an extreme
that when a candidate runs
and a situation like this is uncovered,
they don't even view them as being at fault
because that's just how horrible they are.
That's how much they lie.
That's how much they manipulate their own voters
and play them.
That should really worry the Republican Party.
Their base, some of them, it doesn't matter.
They're going to vote for the R no matter what.
But those independents, something like this,
they won't forget it.
They absolutely will not forget it.
And the fact that the Republican Party
really isn't talking about this, the leadership,
that says a lot too.
Those candidates,
they say a lot to the voter.
That's who the voter is putting their faith in.
And if that faith is misplaced
and the Republican Party does nothing about it,
it's going to matter.
Trust me, there are a whole bunch
of independent first-time voters
who will never vote for the Republican Party
again after this.
The Republican Party ran a candidate
with a less than accurate bio.
And the conversation in the United States
is whether or not it's appropriate
to blame the Democratic Party or the media
because it's just expected for the Republican Party
to lie to the American people.
And it's the Democratic Party
that is expected to expose those lies,
that is expected to uphold democracy
because the Republican Party has become synonymous
with trying to undermine it in any way, shape, or form.
The damage that the Republican Party
is doing to itself right now will last decades.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}