---
title: Let's talk about Hutchinson, theories, and fireplaces....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1nwPapcUcH4) |
| Published | 2022/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hutchinson revealed Meadows was burning documents in the White House fireplace a couple of times a week.
- The White House has burn bags for incinerating sensitive documents, but Meadows chose to burn them himself.
- Trump administration staff used to piece together torn documents and burn the rest to comply with federal law.
- Meadows likely burned documents that couldn't be seen by White House staffers going through classified information.
- The documents burned were probably so sensitive that even attempting to piece them together for retention was risky.
- Meadows burning documents multiple times a week indicates extreme secrecy and importance attached to them.
- Conspiracy theories were brought into the White House by external sources like the "space laser lady" and Peter Navarro.
- The fact that higher-ups needed to be briefed on conspiracy theories debunks the credibility of those theories.
- Trump White House possibly entertained conspiracy theories to energize their base, as suggested by the need for briefings.
- Briefings on conspiracy theories could serve as evidence to help individuals break free from harmful beliefs.

### Quotes

- "Those briefings might be the best evidence to help break people free from it and make them really start to question what they have come to believe."
- "Trump White House possibly entertained conspiracy theories to energize their base, as suggested by the need for briefings."

### Oneliner

Hutchinson revealed Meadows burning sensitive documents, while conspiracy theories were briefed in the White House, debunking their credibility and hinting at a manipulative agenda.

### Audience

Media consumers

### On-the-ground actions from transcript

- Contact media outlets to ensure coverage of the revelations from Hutchinson's testimony (suggested)
- Engage in critical discourse to debunk conspiracy theories and encourage questioning of beliefs (implied)

### Whats missing in summary

Insight into the potential impact of media coverage of Hutchinson's revelations, leading to increased public awareness and scrutiny.

### Tags

#Government #ConspiracyTheories #TrumpAdministration #Secrecy #MediaCoverage


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about the latest
information to come from Hutchinson.
There's a lot of it that's coming out in the transcripts
from the committee.
But there are two pieces of information
that I find incredibly interesting.
And I just kind of want to draw attention
two little segments of this. The first is that according to Hutchinson, Meadows
was burning documents in the fireplace at the White House. That in and of itself
is interesting and according to her it was a couple of times a week in December
and January. That's interesting. It becomes more interesting if you know
that the White House has access to burn bags. Burn bags are exactly what they
sound like. They are paper bags. They have patterns on the outside of them, but the
stuff inside gets burned. It gets incinerated. They get transferred to a
different location and they're burned. Choosing to burn these documents
himself. You have to ask why, when that's available. I think I know the answer.
Throughout the Trump administration, Trump would tear up documents and put
them in burn bags. Staffers, in an attempt to stay in compliance with
federal law when it comes to certain documents having to be retained, they
would dump them out and like try to put them together like a jigsaw puzzle and
tape the ones together that had to be retained and then burn the rest. So if
you're Meadows and you have documents that you want to keep close hold, nobody
should see these. You can't give them to the staffers. You can't put it in a burn
bag and have the staffers do that. This indicates to me that whatever he was
burning was something that people inside the White House could not see. People who
would go through classified documents from the President, they couldn't see it.
see it. That's what it says to me. There aren't really any legitimate documents
like that. Whatever they detailed was likely something so out of the norm that
if a staffer saw it trying to play Trump jigsaw, they would report it. That's
a really interesting piece of information because there's no reason
to burn something in the fireplace there. Not a couple of times a week. I can see
maybe once being mad and just throwing something into a fireplace. I can see
that. But a couple times a week, nah, those are documents you don't want seen by people
not in the loop. That's interesting. And I have a feeling we will see that material again.
Not what was burned, but the fact that this occurred. The other thing is that according
to the testimony, certain conspiracy theories were pushed into the White
House through the space laser lady and I think Peter Navarro. They would bring in
the conspiracy theories and kind of brief the higher-ups on them on what
was being said. That's also really interesting because we're talking about
conspiracy theories that were very harmful. We're talking about theories that to this day
there are support groups for family members who had a loved one fall down into these theories.
The reason I find it interesting that people from outside of the White House were bringing
this information in and explaining these theories to the higher-ups is the theory
themselves. Understand that fact debunks the theories. Why would the higher-ups
need to know if they were in on the plan.
It certainly appears, based on this, that the Trump White House played into this stuff,
gave the appearance and a nod to it for the sake of keeping a base energized, some might
say radicalized. But the fact that they had to be briefed on the theories kind
of shows that they're not real, right? If Trump was really in contact with L or M
or whoever, he would know what the plan is. He wouldn't need the drops explained
to him because he would know what they are. That little bit of information
should probably be all over the media because that theory is still out there
and there's still adherence to it. Those briefings might be the best evidence to
help break people free from it and make them really start to question what they
have come to believe. There's a lot of information that is coming out from the
committee and there's going to be more undoubtedly. Most of it is stuff that at
at first glance doesn't seem that important, but it'll come up again.
Believe me, there are going to be a lot of people who want to know what was being burned
and who gave him the documents that were burned and who he met with and talked to about those
documents.
We'll probably see that in future proceedings.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}