---
title: Let's talk about the missing Republican committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ft8kBLYcN9E) |
| Published | 2022/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party is set to take over the House of Representatives soon and has promised numerous committees, but there is one missing committee.
- The missing committee is the one that was supposed to look into the allegations from the 2020 election.
- Instead of focusing on investigating the election allegations, they plan to look at Fauci, a laptop, and "investigate the investigators."
- Despite claiming the election was bad, there seems to be no interest from the Republican Party in getting to the bottom of the allegations.
- Beau questions whether the lack of interest in investigating the election claims implies that the party knows they were baseless all along.
- He suggests that if the party truly believed in the claims they made, they should be eager to bring in those involved and have them provide evidence under oath.
- Beau believes that the American people are more interested in seeing those who made the allegations being held accountable rather than investigating other issues.
- The lack of interest in pursuing this investigation suggests that the Republican Party is aware that the allegations were false and baseless.
- Bringing people in to talk about the election claims could reveal their complicity in spreading misinformation and undermining democratic institutions.
- Beau criticizes the Republican Party for their propaganda and lack of interest in addressing the baseless election allegations.

### Quotes

- "Their propaganda is bad and they should feel bad."
- "The missing committee is the one that was supposed to look into the allegations from the 2020 election."
- "They know it's a lie. They know the allegations were baseless. They know they were made up."
- "The American people are more interested in seeing those who made the allegations being held accountable."
- "If the party truly believed in the claims they made, they should be eager to bring in those involved and have them provide evidence under oath."

### Oneliner

The missing committee on 2020 election allegations reveals the Republican Party's baseless claims and lack of accountability, exposing their propaganda.

### Audience

Voters, Activists, Concerned Citizens

### On-the-ground actions from transcript

- Hold elected officials accountable for spreading misinformation (implied)
- Support fact-checking initiatives to combat propaganda (implied)

### Whats missing in summary

Analysis of the potential consequences of the Republican Party's refusal to address the baseless election allegations.

### Tags

#RepublicanParty #ElectionAllegations #Propaganda #Accountability #Misinformation


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about a missing committee.
The Republican Party is set to take over the House soon. They're going to have control of
the House of Representatives and they have promised committee after committee.
But there's one that's missing and it tells us a whole lot. I want to know where the committee is.
Where's all of the hype about the committee that's going to look into all of the allegations from 2020.
They're not like trumpeting that. It's strange. They're talking about how they're going to
investigate the investigators. They're going to look at Fauci. They're going to look at a laptop.
They told the American people that the election was bad.
Doesn't it seem weird that they're not going to try to get to the bottom of that?
I mean, to me, I mean, if I believed what they said they believed,
I would want to bring that up. I'd want to look into that.
Unless, of course, I didn't believe that and I was just telling that to the American people.
I knew it was a lie when I said it.
I mean, it seems like it would be really important to them to bring all of these people in
and put them under oath and ask them for their evidence.
These people who played a pretty big part in what certainly appears to be a coup attempt.
Where's that investigation? I think the American people would be far more interested in that
than anything else. I think they would definitely be more interested in seeing all of these people
who made these allegations to the media so they could be delivered to the American people
to stir people up, to undermine faith in democratic institutions.
I think the American people would love to see them under oath repeating those same claims
and providing the evidence that they said they had.
It seems like that would be, you know, prime time.
But it doesn't seem like the Republican Party has any interest in doing that.
And the reason is simple. They know it's a lie.
They know the allegations were baseless. They know they were made up.
And they know that if they bring people in to talk about it, that that's what will be discovered.
And it will show their own complicity in everything that happened.
Their propaganda is bad and they should feel bad.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}