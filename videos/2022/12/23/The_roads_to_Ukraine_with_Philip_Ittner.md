---
title: The roads to Ukraine with Philip Ittner....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3ZNIRzPQLCk) |
| Published | 2022/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction of guest, Phil Itner, a journalist with decades of experience working for various broadcasters.
- Phil's background as a war correspondent embedded with military divisions during significant events.
- Phil's love for Ukraine, its people, and his hope for lasting peace after this war.
- Phil's decision to provide context and depth in his reporting from Ukraine, focusing on understanding and living with the Ukrainian people.
- Challenges faced in Ukraine, including lack of heating and spotty water supply.
- Ukrainians' resolve to endure hardships rather than return to being under Moscow's control.
- Anecdote of a woman's breakdown in a grocery store due to power outage, showcasing the collective resilience and support among Ukrainians.
- Ukrainians' resourcefulness in combating the cold, including using space heaters, candles, stocking up on essentials, and utilizing underground structures for warmth.
- Phil's struggle to maintain objectivity while emotionally invested in the war and the Ukrainian cause.
- Phil's coping mechanism of humor and resolve in the face of war-related trauma.
- Morale boost from President Zelensky's visit to Bakhmut and his role as a rallying point for the Ukrainian people.
- Ukrainians' creativity and resilience in maintaining joy and normalcy during the conflict, such as painting tank traps for the holidays and embracing music and art.
- Ukrainian people's unity and determination to fight back against Russian aggression, exemplified by their diverse contributions to the war effort.
- Call to support Ukraine's just cause and stand against autocracy.

### Quotes

- "There is still joy. There is still, there's almost, there's a determined, it's almost a way of fighting back, is to keep joy and happiness and laughter in your life."
- "You can't crush the human spirit. And Ukrainians are showing that."
- "But the Ukrainian cause is just. And we should support them."

### Oneliner

Beau and Phil dive deep into the resilience and unity of the Ukrainian people amidst war, showcasing their resourcefulness, resolve, and unwavering spirit against Russian aggression.

### Audience

Supporters of Ukraine

### On-the-ground actions from transcript

- Support Ukrainian craftsmen by purchasing Ukrainian-made products to aid the local economy (suggested).
- Share stories of Ukrainian resilience and creativity in the face of war to raise awareness and support for Ukraine (exemplified).

### Whats missing in summary

A detailed exploration of the challenges faced by Ukrainians during the ongoing war, their coping mechanisms, and the importance of global support for Ukraine.

### Tags

#Ukraine #War #Resilience #Unity #Support


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we have a guest with us.
We're gonna be talking about the roads to Ukraine.
Do you wanna introduce yourself?
Yeah, Bo, thanks for giving the opportunity
to talk to you and to your audience.
My name is Phil Itner.
I'm a journalist, a broadcast journalist
for about a quarter of a century,
going on three decades, revealing my age.
I worked for a number of broadcasters,
whether it was NBC, ABC, CNN.
I was a freelance gun for hire, such as it was.
Predominantly my time, however, was spent with CBS News.
I was a contracted journalist for CBS News for many years.
I was embedded with 3rd Infantry Division
during the Iraq invasion.
I was in Afghanistan about a week after 9-11.
So for better or worse, people have categorized me
as a war correspondent,
and I had hoped to put those days behind me.
I truly had.
But sadly, war has come to Ukraine,
and it's a country that I love dearly
and have been coming to for 21 years
since the visit of Pope John Paul II
and the 20th anniversary of the Chernobyl accident,
and have come here repeatedly
because it's a lovely place filled with lovely people
and people who really don't deserve
what's happening to them.
So I had hoped to put my war years behind me,
and I sincerely hope that this is the last one I'll ever see
because war is an awful thing,
and I've had a belly full of it.
But this is my last war,
and I'm glad that it is in many ways
because if there is such a thing as a good war,
this might be it.
The contradiction between right and wrong
and good and evil are pretty clearly outlined here.
And also, I'm humbled to be in a place
watching the birth of a nation
and how people strive for independence
from what really is a despotic and repressive power
that has subjugated Ukraine for centuries.
And finally, now the Ukrainian people
are standing up and drawing a line in the sand
and saying, no, we're going to forge our own future.
And it's an amazing thing to have the privilege
to be here and bear witness to.
And where are you at right now?
What city?
I am in the lovely city of Kiev.
People have called it Kiev because the fanatics are,
as is so much in Ukraine,
everything is dripping with contextual meaning.
So Western English speakers have shortened it
to a one-syllable word of Kiev.
Many Ukrainians also refer to it that way.
I have always referred to it as Kiev
as opposed to the Russified version, which is Kiev.
If you can hear that distinction,
basically it's where do you put the law?
The end of the first syllable
or the beginning of the second syllable.
For an English speaker, that's kind of inscrutable.
But I am in the lovely capital and it is beautiful.
Despite the difficulties that we endure,
I don't have heating in my building currently.
The water is spotty at best.
I am greatly lucky that the power is reliable
in my building.
I just lucked out that way.
So everything else can kind of go by the wayside.
As long as I have the internet and power,
I can get space heaters.
The water can be tricky.
That's unfortunate.
It's a good thing that you can't smell through the camera.
But yeah, I've been coming here for 21 years.
I spent a lot of time at the beginning of the war in Lviv
until kind of we figured out what was going on.
And then I moved here.
I've been down to Odessa.
I was out in Kharkiv.
I have intentions to go to a number of different places.
But my intention was not to do what I knew
that the networks were gonna do.
Because I've been in news for so long,
I didn't wanna come here and do what the guys
that are in the business now were gonna do.
And that is the kind of race, the horse race.
Who's winning, who's losing, what battle for what city.
Go to the refugees, go to the hospitals,
get the stuff that's gonna get the views and the clicks.
Because of my history and my understanding of the country
and my relationship with the country,
I wanted to provide something that you can't really do
on network television or 24-hour cable news.
And that is to get context and depth
and a lot more understanding.
And also to live with the Ukrainians,
not up in the intercom hotel,
where the power goes out and the genie kicks in immediately.
And that's not to disparage my fellow journalists
who are on the front lines doing that.
Somebody has to do that, absolutely.
I'm not a big fan of corporate management
or the leadership of the networks
or the ownership of the networks.
But the guys who are on the ground here, by and large,
although there are certainly,
there are people who tow the corporate line,
but by and large, the guys who are on the front lines
are transmitted by what the news media is nowadays.
And so I had the opportunity to do this now
in current technology being what it is
and our capabilities that I wanted to come here
and provide an alternative that gives a little bit more
understanding of how it is we got to where we are,
why this fight is so important
and who the Ukrainian people are
and why it is they're fighting this.
So yeah, I'm gonna be here.
I keep saying I'm here until I'm drinking champagne
on Trishachik in Kiev on victory day.
Trishachik is basically the main street.
That's my...
So you mentioned something that I'm sure everybody
that's watching this is kind of curious about.
How are people getting by there?
Like, I mean, we get the reports,
but there's an element of,
let's just say either cheerleading or whatever.
What's it really like?
It sucks.
It sucks.
It sucks.
It's not fun.
Nobody's enjoying this.
Yeah.
There is a resolve though.
Every Ukrainian I know,
every Ukrainian I come into contact with,
taxi drivers, guys behind the bar at my local little cafe,
or people that I just meet on the street
or shopping or whatever, just out and about.
The general attitude is, this is awful.
This is terrible.
We hate this.
But what are we gonna do?
Our options are either we suffer through this awful winter
and the deprivations and the horror
and coming under fire and living without power
and living without heat
and all the things that we have to suffer through.
We either suffer through that
or we go back to being subjects of Moscow.
And we know what that's like.
For centuries, the Russians have abused this country,
literally committed massacres and genocidal acts.
And the Ukrainians have had enough.
And so their resolve is, it's breathtaking though.
It is humbling.
It's deeply humbling to be here and watch
because they are, yeah, you can see it.
There's a thin veneer.
You just know that if the right buttons were pushed,
the tears would come, the frustration, the anger.
It's bubbling right beneath the surface,
but there's also resolve.
I'll tell you an anecdotal story.
Maybe that'll give you some insight.
So about a month ago, I went out shopping
and this was the initial stages of Russia's attack
on the electrical grid and the infrastructure here.
And so among other things,
but one of the things that Ukrainians
and residents of Kiev have had to learn
is that when there's power at the local supermarket, you go.
If you see that they have power,
you go right then and there
because who knows what's gonna happen in an hour.
So the lines are enormous.
The grocery stores get flooded.
Everybody's standing in line in my story.
And we're all, we've got our goods.
We're ready to buy and get back home.
And the power goes out.
Just boom.
And we're in darkness in a grocery store.
And one woman loses it.
She just, she's had enough.
She's frustrated.
You can clearly see it.
She starts yelling at the local staff.
Why don't you have a generator?
All the other grocery stores have generators.
Many, many grocery stores have generators.
They kick in immediately.
Why don't you have one?
And she's just losing it.
And the rest of the line lets her vent for a little bit
and then goes, you know,
we're all going through this together.
Look, maybe the generator was in another branch.
Maybe they're out of gas for it.
You know, we're all going through this.
And she kind of grumbled and she shuffled her feet
and she went, yeah, okay.
And that was it.
That was it.
It's, nobody likes this.
Let's not make any illusions that this is easy
or something that people are proudly,
you know, the blitz spirit, you know,
it's not all, you know, resolve and proud,
you know, defiance to Moscow.
You have bad days.
You have dark, dark days.
You have days when you're looking
and you're looking at the refugees.
You're looking at the kids.
It's always the kids for me.
With their thousand yard stairs,
seeing things that they never should have seen,
spending a Christmas separated from their loved ones,
you know, living in refugee centers,
living in strangers' homes.
It is very hard.
It is extremely difficult.
But having said that, the blitz spirit is here.
And the anger is directed,
sometimes it bubbles over,
but the anger is directed at Russia.
It's doing the absolute opposite
of what I think Putin intended to do,
to break the spirit here.
It's stealing their resolve.
But, you know, you ask what it's like to live here.
And it's difficult.
It's really hard.
And I comparatively have a little bit of survival's gift
because I have electricity by and large.
Normally it goes out occasionally,
but compared to some of my friends,
and certainly compared to people
who are in smaller towns and villages
or closer to the front lines,
you know, this is, you know, Eden.
This is the garden of Eden
compared to how those people are living.
But what are we supposed to do?
So how are they handling-
It's what I hear all the time.
What are we supposed to do?
Right.
Well, you know, how are they handling the heat?
Like that's one of the big questions.
Yeah, the heating issue, because it is getting cold.
I mean, colder, as a matter of fact,
overnight it really plummeted here.
That is one of the biggest issues.
People combat it with a number of different things.
If they're able to have power or a generator,
they get space heaters.
In many, certainly urban centers, heating is centralized.
So it goes to, the entire building has heat or doesn't.
And that's the case in my building, for example.
I do not control the heat in my apartment.
It's controlled throughout the building.
And it's like that, it's a Soviet planning thing.
So heating is a major issue.
I know this is gonna sound crazy, but even a candle,
a couple of candles, if your power is out,
people have been using candles as heat sources.
Light and heat come from candles.
You'd be amazed if you can put yourself in a small room,
how much a candle or two or three can change it.
I know that sounds ridiculous, but it's true.
And Ukrainians are also very resourceful people.
They know what's coming.
They have stocked up on water.
They've stocked up on food that does not spoil.
So your dry foods and things like that, pastas.
They have cooking, little propane stoves
that they'll have indoors.
They know what's coming,
so they have prepared for it as best they can.
But yeah, you're right.
Heat is the real big issue.
Now, another thing that I'll make note of
is the fact that winter is harsh in this part of the world
and always has been.
So the cities themselves are built
with a second subterranean city beneath them
because you have to go beneath the roadways to crawl
because they make these multi-lane roadways
in many of the cities.
And so you have to go underneath the ground
to traverse the streets.
Also, in addition to that, they build it
because you can maintain heat and you can maintain light.
During the Soviet era, they were concerned
about a Cold War turning hot.
So they built these kinds of things
as bomb shelters originally,
not realizing that at some point
it would actually be Russia and Ukraine
combating one another.
So there's a whole subterranean world.
The metro is built very deep.
There's shopping centers
that are almost entirely underground.
People rely on those,
especially when the air raid sirens sound.
They're getting through.
They're making the best of a bad situation.
But again, make no doubt about it.
This is incredibly hard.
The resilience of the Ukrainian people
is nothing short of breathtaking.
So let me ask you this
because it's something that I've...
You've been around.
You've been around.
You've seen a little bit.
Well, hello, Stanley.
Yeah, right.
Not what I meant,
but I think I've heard stories about that too.
What I mean is you're there.
You have seen similar things before.
How hard is it to...
How hard is it to maintain the humor
and the, I guess, the distance
that is often required
when you're actually trying to be objective
and gather context?
Yeah, it's very difficult.
And it's difficult for me in this war
because I feel such an affinity for these people.
But I do try and be as objective as possible
with the understanding that this is the first war
that I've actually taken a side on.
I am on the Ukrainian side,
make no doubt about it.
That does not blind me to the faults in Ukraine
or the faults in their actions
or the faults in how they conduct themselves
or the makeup of certain sectors of Ukrainian society.
I mean, there are Nazis in Ukraine, that's true.
But there are also Nazis in Russia
and there are Nazis in Britain.
And if you hadn't heard,
sadly, there are Nazis in America.
That does not grant Canada
the right to bomb Pasadena
because of Charlottesville, if you catch my drift.
So how do I keep my humor?
How do I keep my spirits up?
You have to.
And this is something,
this is a defense mechanism that I learned
in all the wars that I've covered.
And this is about my 10th or 12th war.
It's a defense mechanism.
The human spirit has to do it.
It's gallows humor.
It's resolving not to let the horrors that surround you
permeate into who you are.
That is a losing battle, I'm sad to say.
I will bow, not to be maudlin
or to bring things down here too much,
but I will be dealing with what I have seen
in the various war zones that I have lived in
and covered over the last quarter of a century,
most likely for the rest of my life.
Post-traumatic stress is a very serious thing
and it needs to be taken seriously.
And I address it as best I can.
I try to be as healthy mentally as I possibly can
because it will drag you down.
It will absolutely drag you down.
And I see that here amongst the Ukrainian people.
Lots of Ukrainians have never gone through war.
This is their first experience.
I've been coming to war for 25 years.
And I've seen, as I say, it's the kids that normally
get me the most.
But I've seen, I won't get explicit,
but there are images that will stay with me
for the rest of my life.
But I'll live with them.
I try to deal with it in a healthy manner.
But war is an awful thing.
And when this is done, I swear, when this is done,
I'm going to go do documentaries about the migratory patterns
of the monarch butterfly or the beauty of the Pacific Highway.
War is awful.
But being angry at war is like being angry at a tornado
or a hurricane.
It's just this force majeure thing.
Change of tone.
And speaking of image, I was going through your social media
in the run up.
Oh, dear.
No, no.
You've got some interesting holiday decorations around you.
I saw.
I do.
Yeah.
So tell us about that.
What have I got here?
I've got my, well, I have my Christmas tree near me.
I'll pull off one of the ornaments here.
You can see some of the things we've got here.
This is a, I don't know if you can see that there or not.
There we go.
There's the famous Russian, the stamp, the Ukrainian stamp,
kind of telling Russian worship where to go and what to do.
I've got patron, the bomb sniffing dog
as an ornament here.
Again, don't know if you can see that.
I've got my traditional Ukrainian mace, the bolivar.
You know, I buy this stuff partly for something
to kind of decorate my flat with or to kind of have
when I do podcasts or blogs or blogs,
but also partly to support Ukrainian craftsmen.
I have in the other room, I've got a little wooden toy
haimars that somebody, a carpenter is taking
and making little toy haimars.
Behind me, I have Olga of, Olga Kiev,
who is shown with the Turkish drone saying,
unleash the Bayraktars, the Turkish drone.
There's an image of a, it looked like a tank trap
that was decorated with holiday.
Yeah.
Yeah.
I mean, boy, the Ukrainian people, I tell you what.
They've painted tank traps for the season.
You know, they've got the tree.
This Christmas is very muted, understandably so.
But they're still going to be Christmas.
You know, I was, they're still going to be Christmas.
I was watching Zelensky's speech to Congress.
And he, you know, he said, look, we'll get through,
Ukrainians will get through a difficult holiday season,
but make no mistake about it,
there will still be Christmas.
There is still joy.
There is still, there's almost, there's a determined,
it's almost a way of fighting back,
is to keep joy and happiness and laughter in your life.
I don't know if you saw at the very beginning of this war,
Bo, there was a video and it brought tears to my eyes.
And it helped me make the decision to come here.
And the decision to be here was the right one.
There was an orchestra in Odessa,
and Odessa was being pummeled at the time,
and preparing for a physical invasion.
And there was a military brass band
that played in the central square in Odessa,
the Bobby McFerrin song, Don't Worry, Be Happy.
Just refusing to let the Russians steal from them,
the one thing that they might have control of,
when the air raid sirens go,
when you know that there are tanks coming your way,
when you know that they are destroying
your electrical infrastructure,
there's very little you can do about that.
You can build up your water,
you can go to the air raid shelters, all of that.
By and large, things are out of your control.
One thing you have control over is your personal disposition
and how you respond to those external things.
And they have responded with humor,
they have responded with art,
they have responded with music.
I mean, they will not be broken.
It's just such a massive miscalculation on Russia's part
to think that they can crush the Ukrainian spirit.
There is music everywhere.
I mean, we're looking at the centenary
of the debut at Carnegie Hall,
the carol of the bells, that...
Da-da-da-da-da-da-da-da-da-da-da-di-di-di-di-di-di.
That's a Ukrainian composer,
who, by the way, was murdered by the Russians
because that song is from a Ukrainian folk song.
And recognition that there is such a thing
as Ukrainian culture independent of Russian
is very forbidden.
He was murdered by the Russians.
But 100 years later, we can still enjoy that carol.
It's a beautiful song, it's haunting.
And I hear it all over the place.
I was in the rail station yesterday.
There were Christmas carolers.
There was a Santa Claus.
Kids coming and going from Kiev,
maybe going to villages where their grandfathers
and grandparents live,
and so they want to be together during the holidays.
Or maybe, as is so often the case,
people who have been away from their families
for months during this war cannot fathom the idea
or cannot bear the idea of being separated during the holiday season.
So you see many of them coming back.
I saw people embracing at the railway station,
people who probably haven't seen their loved ones in months.
And little kids, you know, as traumatized as they are,
as dirty as they are, as just, you know, disheveled as they are,
they see Santa and they're kids again.
It's so heartening to see.
And the big smile comes out and they sit on Santa's lap.
Some of them are, of course, terrified,
because, you know, I don't know if you ever saw
the Christmas story movie, but they do that scene
where Ralphie's going to Santa.
He's absolutely terrified of this big guy.
There's some of that, but they're kids again for just a moment.
And you can't crush the human spirit.
And Ukrainians are showing that.
And anyhow, you know, that's how we survive mentally, emotionally.
I hate war.
I never want to see another one.
But yeah, you find a way to get through it
without letting it destroy you.
At least that's the hope.
So what's the reaction there to Zelensky's trip to the front?
Like, I'm sure the trip here has created headlines,
but I'm more concerned or more curious
about how the Ukrainian people responded to the trip.
To Bakhmut?
Yeah.
The trip to Bakhmut was really a huge morale boost.
I know I have contacts in the Ukrainian intelligence service.
A lot of people did not want him to do that.
Imagine.
Oh, I mean, yeah.
I mean, you saw some of the footage, I'm sure.
I mean, you can hear incoming.
You can clearly hear incoming.
He is somewhere in that factory where he's giving the medals
and he received the flag, which he then subsequently
presented to Congress.
You can hear Shelly.
That was no joke.
That wasn't a PR stunt.
That wasn't orchestrated.
He really was in harm's way.
And that's something Zelensky has been very good about,
as a leader of morale and as a representative
to the global, on the global stage.
But the visit to Bakhmut was really important for a variety
of reasons, not least of which because you saw him go and stand
on front lines with the guys who are fighting and dying.
And that is a huge thing.
But also because the narrative about Bakhmut by people who want
to see Ukraine lose is that, oh, Bakhmut is imminent.
The collapse of Bakhmut is imminent.
And when Bakhmut falls, the whole House of Cards
and the Ukrainian defense is going to fall with it.
Well, one, that's not true.
There are alternatives to Bakhmut.
Bakhmut is important because the Russians seem
intent on taking it.
So they keep crashing on the Ukrainian lines at Bakhmut.
And the Ukrainians are like, if you want to come and die,
if this is the hill you want to die on,
we're happy to facilitate.
But Bakhmut is not essential to Ukraine's defense.
But the guys who want to see Ukraine defeated in this war
have been saying, oh, Bakhmut, it's imminent.
It's imminent.
And then, you know, and they've been saying that for weeks.
For weeks.
It's going to happen in the next 12 hours, 24 hours,
Bakhmut will fall.
And then to have the president of the country
not only go to Bakhmut, but go to Bakhmut
while fighting is going on.
And, you know, and stand there on that territory,
you know, it was a huge thing.
Lots of people really appreciated
his visit to Bakhmut.
So that was a heck of a thing.
The guy, I have, I know lots of Ukrainians
who have their criticism of Zelensky.
I have my criticism of Zelensky in the run-up to the war
and the period prior to this full-scale war.
And we'll see what happens when the war is over.
There's no guarantee.
I mean, Churchill lost his leadership
of the Conservative Party after the Second World War.
You would have thought that he would have
been able to stay in power for as long as he wanted.
But he was, you know, ushered out in the post-war period.
I have spoken to many Ukrainians who said,
I disagree with Zelensky politically,
but we're at a time of war.
And now is not the time for internal griping.
And I will stand behind my president at this time of war.
But after it's done, we go back to political infighting.
We have parties here that will try to, you know,
get their guy hit.
You know, there's been a lot of talk about the lack
of democracy here and him getting rid of political parties
or silencing journalists.
A lot of those guys were paid by the Russians.
They were Russian agents posing as political parties
or as journalists.
Not everybody.
When it's all said and done, we'll have a review
and we'll see, you know, Lincoln, you know,
suspend the habeas corpus.
You know, I'm sure that there are going to be critics
of Zelensky who will look back and say he did this
that was very undemocratic or he did that
that was against the rule of law.
But right now, this is a knife fight.
And only one person will walk out of this.
And, you know, one side is going to walk out of this.
And so lots of Ukrainians are like,
we'll take up our issues with Zelensky after the war.
But right now, you know, to see him go to Bakhmut
and to see him so well-received and being such a very good
representative of Ukraine on the global stage,
lots of people are begrudgingly saying, you know,
OK, well, he was the right guy for the job.
Yeah, when all of this started, I was like,
this is actually, this is the guy they need
for the diplomacy in the run-up to the war.
But once the shooting starts, he probably needs
to step out of the way.
And I have happily eaten my words on that.
He's done pretty well as far as, I mean,
his mission at this point in time when you're fighting
an existential conflict is to be the rallying point,
to be that symbol.
And you have to maintain that image at all times.
And he's done wonderfully.
I mean, so.
Yeah, I think you're right.
I mean, to see him in the very early stages of the war
coming out from the bunker and like going and filming himself
in very, very vulnerable positions in Kiev,
I mean, at the very beginning of the war,
to see him out in the streets of Kiev
was an enormous morale booster.
And then you're absolutely right about the other thing
that I find fascinating.
And if you go back to his speeches,
and if you, from this point forward, moving forward,
if you pay attention to this, he is an excellent orator.
And he is excellent at tailoring his speeches to the audience
that he's speaking to.
When he spoke to Congress yesterday, my time now,
he references the Ardennes, the Battle of the Bulge.
He referenced Saratoga, the Battle of Saratoga.
He quoted FDR.
The guy knew who he was talking to,
and he tailored his message to that.
When he spoke to the British Parliament,
he talked about fighting them on the landing grounds,
and we'll fight them on the beaches.
He paraphrased that in Ukrainian terms.
When he spoke to the French, he talked
about egalite fraternite liberte,
these Western ideals.
He is absolutely, he's, OK, so he's an actor and a comedian.
So maybe that is his forte.
That is his area of expertise, and he's
done it extremely well.
But he has also been very fortunate in that he
has a lot of people on his team, whether he picked them
or whether they were there by providence.
Zalouche, the head of the army here, head of the military,
is an incredibly capable commander.
He has been incredible in his tactical maneuvering.
And when we talk about the counteroffensive that
was launched about two or three months ago, where everybody
thought that they were going to surge on Kherson
in the southern part of the country,
and what they did was make the Russians believe that indeed
was what their objective was, with all the while,
what they really wanted to do was
secure Kharkiv in the north.
And they totally fooled the Russians.
Zelensky is great in front of the camera.
He's great for boosting morale.
He's great for gathering worldwide support.
But he's also really blessed in that there
are a lot of smart, capable Ukrainians
who are running finances, who are running
the electrical grid.
And then also just everybody's in this battle.
Ukrainian people have rallied.
They're making heroes out of civil engineers.
I mean, there are posters, Bo.
If you go around this city right now,
there are posters of guys fixing plumbing,
or posters of guys sweeping up the debris
from a bombed out building.
And they're made heroes.
They're like, yay, our civil engineers.
OK.
Name me, what was the last time you
saw in your local city or state or whatever,
where they were like, hurray for the plumbers.
But it's this sense of we're all in this together.
Everybody does everything that they possibly can.
So Zelensky may have set the tone and the attitude
and the mood of the Ukrainian people.
But it's the Ukrainian people who have adopted that
and took it on themselves and said, right,
what do I need to do?
How can I help?
What things can I do?
A woman out in Lviv who was a tour guide,
she is now running camps for traumatized children, PTSD.
I know a guy here who ran a language school.
And he's now converted the language school
to a media center so that people can continue
to get out their blogs and their message to the wider world.
Everybody is looking for something to do.
And that, in many ways, is down to Zelensky's attitude of,
now is the time for all of us to band together.
And that is happening.
All right, well, we have to wind it up here.
We're already a little over.
But is there any last-
Well, if you ever want to talk again,
and this war, sadly, I think it's going to go pretty deep
into 2023.
But if you ever want to talk again, I'm at your disposal.
Last words, I mean, this is an awful war.
It's so hard to see the suffering
of the Ukrainian people up close.
It is frustrating to hear people who are apologists for Russia
or who are so concerned about American military adventurism
that they are willing to let the Ukrainian people suffer
because, God forbid, America should support Ukraine
with weapons or the tools that they need to fight so that they
can secure their own independence
and self-determination.
But it's humbling to be here.
And I'm proud to be here.
It's a privilege to be here.
I wouldn't think of being any other place.
But hopefully, this is my last war.
But it's not hyperbole.
This is an historic fight.
I lived in Russia.
I know the character of the Russian people.
I know what it is that they think they're fighting for.
There's a lot of dysfunction in Russia.
This issue was kicked down the road.
At the fall of the Soviet Union, the chaos of the decade
after the fall of the Soviet Union
brought into power Vladimir Putin.
I was in Moscow.
I saw that happen.
I saw him bombing his own apartment
buildings to justify a war in Chechnya.
We should have seen this coming.
But like Zelensky said in his speech to Congress last night,
this is not a war that can be postponed or delayed.
I don't want this war to happen.
I wish it wasn't happening.
But this war was instigated by Russia.
It was brought upon the world by Russia.
There was no significant provocation
that justifies anything that's being done here.
And I don't like the fact that this war has landed on our shores
at this time, but it happened.
You've got to pick a side.
Either you are standing with an autocracy
that brutalizes its own people, or you
stand with a messy, difficult system like liberal democracy.
Those are the choices.
And I know which side I'm on.
So I'm going to stick it out.
And if you want to talk again, I'm here any time.
But certainly, I suggest that everybody
who is listening or watching this
recognize that it's not a perfect country.
There is no perfect country.
But the Ukrainian cause is just.
And we should support them.
All right.
Well, we'll end it on that.
Thank you, Michael.
All right, everybody.
It's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}