---
title: Let's talk about how Biden's new policy is working....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PyalYfO_-jI) |
| Published | 2022/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration formalized policy changes on the use of special operations or drone forces, making decisions at the White House to limit civilian loss.
- Beau expresses skepticism about the effectiveness of the policy due to the temptation drones pose and past unsuccessful policies.
- A recent raid in Syria, carried out through a special operations helicopter-borne raid, demonstrated a cautious approach by the White House to limit civilian loss.
- Despite the ideal use of a drone in the raid, no civilian casualties were reported, indicating the administration's seriousness in adhering to the policy.
- The Biden administration aims for almost zero civilian loss in such operations.
- The raid provided an option to surrender, but the individuals chose a different route, resulting in a successful mission with no civilian casualties.
- The US military may be shifting focus towards capturing individuals in operations and rebuilding intelligence-gathering capabilities.
- This early implementation of the policy is a hopeful sign in US drone use, reflecting a more sensible approach after 20 years.
- The Biden administration's policy appears to be achieving its goals, though it requires political will and commitment to maintain safety and effectiveness.
- The success of the policy depends on the administration's dedication to keeping special operations prepared and in the necessary locations.

### Quotes

- "The United States has finally developed a drone policy that makes sense."
- "It is more difficult to do it this way. It is safer."
- "The Biden administration's policy appears to be holding up and accomplishing its goals."

### Oneliner

The Biden administration's cautious approach to special operations shows promise in limiting civilian loss and developing a sensible drone policy.

### Audience

Policy Analysts, Activists

### On-the-ground actions from transcript

- Support organizations advocating for transparency and accountability in special operations and drone policies (suggested).
- Stay informed about updates and developments in US military strategies and policies (implied).
  
### Whats missing in summary

The full transcript provides a detailed analysis of the recent US policy changes regarding special operations and drone forces, offering insights into the administration's approach to limiting civilian casualties and enhancing operational efficiency.

### Tags

#USPolicy #DroneForces #CivilianLoss #MilitaryStrategy #BidenAdministration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about some news
that occurred earlier in the month
and see what we can infer from it
in relation to a Biden administration policy
and kind of go through it and see where it leaves us.
Because about two months ago,
the Biden administration announced the formalization
of a series of policy changes that it had made,
basically its first year in office.
And they made it formal.
And the general tone of it is that,
well, the White House is making this decision now.
And it related to the use of US special operations
or drone forces.
And the end result here was that those decisions
now get made at the White House.
And the goal was to limit civilian loss.
If you go back and watch that video,
I am openly skeptical of whether or not
this is going to work, mainly because drones
are very tempting.
We have had a lot of policies over the years.
None of them have been good.
The policy that the Biden administration laid out
looks good on paper, but it has to be adhered to to work.
So what happened?
In Syria, there was a raid.
Special operations helicopter-borne raid.
And there have been other raids since this policy
went into effect.
However, this is the first one that,
based on all the information I have,
really, this is an ideal use of a drone,
that this would be the moment to use one.
So what we can infer from that is that the White House
is actually taking this seriously
and is perhaps even being more cautious
than they really have to be to limit civilian loss.
Their goal was almost zero,
is actually what they said their goal was.
And it looks like they're trying to stick to that.
So we can infer, based on the operations that have occurred
since this policy went into effect,
that at least for the time being,
the Biden administration is actually serious about this.
This raid, from my understanding,
they went in, provided an opportunity to surrender.
Those inside chose a different route,
and that went about as well as you could expect.
But there were no civilian loss,
and everything went according to plan.
We can also kind of infer from this
that the US military is possibly starting to move back
to a higher focus on K or C stuff,
operations where the ideal would be to capture the person,
but there are other options on the table,
and focus on rebuilding
intelligence-gathering capabilities there.
That's the other thing that we can infer from this.
So it is still very early in this policy,
but it's a hopeful sign,
and we don't get hopeful signs
when it comes to the US use of drones.
That doesn't happen very often.
It looks, again, early on,
it looks, though, that the United States
has finally developed a drone policy that makes sense.
It took 20 years, but we're here.
So the Biden administration's policy
does appear to be holding up and accomplishing its goals.
It is more difficult to do it this way.
It is safer.
It all depends on the political will
and the political will of the Biden administration
and how well they can hold up when it becomes,
when expediency becomes necessary,
if they can really focus on keeping
the special operations community up to speed
and in the locations they need to be in to do it in person.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}