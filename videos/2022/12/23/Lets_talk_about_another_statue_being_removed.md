---
title: Let's talk about another statue being removed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GtvuMLwzInc) |
| Published | 2022/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction about discussing the removal of a statue.
- The statue in question is of Roger Brooke Taney, overshadowed by the infamous Dred Scott decision he wrote.
- The Dred Scott decision held that black Americans couldn't be citizens and that black people had no rights white people were bound to respect.
- Taney viewed the founding documents as an agreement to keep a permanent underclass, not as a promise of equality.
- Despite objections to Taney's views then and now, his statue is being removed by vote.
- The statue will be replaced by Thurgood Marshall, the first black associate justice of the Supreme Court.
- Beau sees this as a symbolic act, acknowledging that the promises of the founding documents are still unfulfilled.
- Symbolic acts like statue removals move towards real and tangible actions to address past wrongs.
- Beau expresses the importance of not just removing statues for embarrassment's sake but taking concrete steps to right historical injustices.
- Beau concludes with a call to aim for real action beyond symbolic gestures.

### Quotes

- "Removing a statue because it is embarrassing, yeah, I get it. Righting the wrongs, that's a whole lot better."
- "The promises laid out in those founding documents are still not being lived up to."
- "But each symbolic act carries us closer to real action, tangible action, to right the many things that this country has done."

### Oneliner

Beau talks about the removal of a statue of Roger Brooke Taney, known for the infamous Dred Scott decision, and the importance of moving from symbolic gestures towards concrete actions to address historical injustices.

### Audience

History buffs, activists, voters

### On-the-ground actions from transcript

- Advocate for the removal of statues glorifying figures linked to systemic racism (exemplified)
- Support initiatives that address historical injustices and work towards real action (exemplified)

### Whats missing in summary

Beau's emotional tone and full depth of analysis on the symbolism of statue removals and the need for tangible actions beyond symbolic gestures. 

### Tags

#StatueRemoval #HistoricalInjustice #SymbolicActions #RealChange #SystemicRacism


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about statues and discuss yet another statue being removed.
One that is leaving the Capitol.
It's a statue of Roger Brooke Taney.
Now if you're not familiar with that name, that's okay.
His name is overshadowed by the name of a decision he wrote.
The Dred Scott decision.
If you're not familiar with this, it was a Supreme Court decision that basically held
that black Americans couldn't be citizens.
Was really what it was focused on.
But the language in that decision will live on forever.
It held that black people had no rights that white people were bound to respect.
Taney looked at the founding documents of this country as an agreement to keep a permanent
underclass.
He looked at phrases, philosophical phrases, that lovely flourishing language, all men
created equal, and didn't see it as a promise of a better day.
He saw it as an agreement to constantly and forever keep people down.
His statue is being removed.
His statue is being removed.
There will undoubtedly be people crying about moral relativism and historical relativism
and so on and so forth.
You don't have to do that with Taney.
Taney, his views are objectionable today.
They were also objectionable then.
This wasn't a period in time when there weren't a whole bunch of voices talking about how
far this country had strayed, how it isn't living up to those promises.
So it was removed by vote.
It's going to Biden.
I'm certain he'll sign it.
In a nice little bit of poetic justice for the justice, his statue will be replaced by
Thurgood Marshall, who if you don't know, was the first black associate justice of the
Supreme Court.
That's nice.
I like that.
It's a symbolic act.
It is a symbolic act.
The promises laid out in those founding documents are still not being lived up to.
There are a whole lot of people who do not have the benefits that were enshrined in those
promises.
But each symbolic act carries us closer to real action, tangible action, to write the
many things that this country has done.
Removing a statue because it is embarrassing, yeah, I get it.
Righting the wrongs, that's a whole lot better.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}