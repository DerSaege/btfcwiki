---
title: Let's talk about a McCarthy quote you should remember....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ydwyQTJR1pE) |
| Published | 2022/12/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The final committee hearing was rescheduled from Wednesday to Monday.
- McCarthy called the committee the least legitimate in American history.
- The committee provided vital information to the American people about attempts to overturn an election.
- The American people gained insight into the plot to prevent a lawful transfer of power.
- Despite McCarthy's statement, the committee may be viewed as one of the most significant in the last half century.
- McCarthy's statement should be remembered during future investigations by the Republican Party.
- McCarthy's immediate dismissal of the committee's legitimacy raises questions about his involvement or knowledge of the events.
- The statement about the committee's legitimacy should be a prominent consideration during all future hearings and investigations.
- McCarthy's preemptive judgment on the committee's legitimacy may suggest a desire to deflect from the real issues.
- The American people should not overlook McCarthy's statement and should keep it in mind moving forward.

### Quotes

- "The reason it's important to remember this as you are watching that final closing."
- "That statement, about it being the least legitimate in American history, needs to be at the front of your mind."
- "That statement, the least legitimate in American history, that needs to be remembered by the American people."

### Oneliner

Beau stresses the importance of remembering McCarthy's claim that the committee is the least legitimate in American history during future investigations and hearings.

### Audience

American citizens

### On-the-ground actions from transcript

- Keep McCarthy's statement in mind during future hearings and investigations by the Republican Party (implied).

### Whats missing in summary

Full context and analysis of McCarthy's statement and its implications.

### Tags

#Committee #McCarthy #AmericanHistory #Investigations #RepublicanParty


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to
talk about
how you might should view the committee.
Now, just so you know,
initially it appeared that the final committee hearing
was going to be on Wednesday. It is now going to be on Monday.
And there are some things that you should keep in mind as you watch it.
The first is
there wasn't a lot of cross.
You know, a lot of this evidence hasn't been tested
in court.
And
that leads us to a statement
from McCarthy,
who is the presumptive
incoming Speaker of the House.
Something he said
back on June 9th.
He said that the committee
was the least legitimate committee in American history.
That's a statement
that I feel will haunt him.
I feel like that statement
will probably find its way into history books.
I want you to think about everything
that we learned through that committee.
Everything that
was presented to the American people,
stuff the American people would not know yet
if it weren't for that committee.
Because DOJ wouldn't be releasing a lot of this.
The American people would not have a clear understanding
of how
involved
the
plot was.
And he referred to it as the least legitimate.
That statement,
that's going to be around for a while.
And when you really look at this committee,
and when you really
look at
what it provided
the American people as far as context,
as far as understanding
the various prongs and elements
that went into an attempt
to overturn
an election,
to prevent
the lawful transfer of power,
everything
that the former members of Congress
outlined in that open letter.
When you think about how the committee
provided that information to the American people,
I have a feeling that it's not going to be viewed as the least legitimate in
American history.
I feel like it might be viewed as one of the most important
in the last half century.
The reason it's important to remember this
as you are watching
that final closing
with what I'm sure is going to be a great multimedia presentation and all of
the buzz surrounding it.
The reason you need to remember this statement
is because it certainly appears that the Republican Party's plan
is to
launch a series of investigations
to
basically undermine all of this,
to play politics,
rather than focusing
on correcting the problem
and cleaning up their own house, pardon the pun,
they're going to try to deflect.
They've made that abundantly clear.
That statement,
about it being the least legitimate in American history, needs to be at the
front of your mind
during every one of those hearings,
every one of those investigations that they launch.
Because when McCarthy saw this being formed, he made that statement
when the committee was being put together.
Pelosi was like, no, I'm certainly not putting these people
on the committee.
That's when he made this statement.
So
assuming
that he didn't know
anything that was going on,
let's assume for a second
he was not part
of this plot in any way, shape, or form,
totally innocent.
When he saw
an attempt
to understand
what happened that day,
his first instinct
was that, well, it's not legitimate,
it's just politics.
It's just them trying to
drum something up,
because that's exactly what he'd do.
That's why he viewed it that way.
And then, of course, the alternative is that he totally knew what happened and
knew what they were going to uncover,
which is even worse.
That statement,
the least legitimate in American history,
that needs to be
remembered
by the American people.
And it needs to be,
honestly, that should be the forward
to the report.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}