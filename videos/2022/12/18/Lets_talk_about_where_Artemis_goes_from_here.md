---
title: Let's talk about where Artemis goes from here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jqUjgSHIW0M) |
| Published | 2022/12/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Artemis 2 will fly astronauts around the moon in 2024, with Artemis 3 following to put people back on the moon.
- NASA's Artemis program aims to establish a research base near the southern pole of the moon and a lunar space station for refueling missions.
- The delays in the Artemis missions are primarily due to budget constraints rather than technical upgrades.
- The successful completion of Artemis 1's mission involved deep space maneuvers and system tests on the Orion spacecraft.
- The spacecraft completed a 240,000-mile journey and was retrieved by the Navy on December 11.
- Following splashdown, the craft is being transported back to NASA for thorough examination.
- The current timeline for the Artemis missions includes establishing a base on the moon within the next few years.
- Financial constraints have caused significant delays in the Artemis program, impacting the timing of future missions.
- Despite budget challenges, NASA is continuing its efforts to advance towards establishing a base on the moon.
- The success of Artemis 1 marks a significant step towards the United States' goal of lunar exploration.

### Quotes

- "Artemis 2 will fly astronauts around the moon sometime in 2024."
- "It had to do with trying to fill a budget hole a few years ago. This is literally all about money."
- "The delays in the Artemis missions are primarily due to budget constraints rather than technical upgrades."
- "The successful completion of Artemis 1's mission involved deep space maneuvers and system tests on the Orion spacecraft."
- "Despite budget challenges, NASA is continuing its efforts to advance towards establishing a base on the moon."

### Oneliner

Artemis program faces budget delays in lunar missions, aiming to establish a base on the moon within the next few years.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Support funding for NASA's Artemis program to ensure timely progress (implied).

### Whats missing in summary

Details on specific deep space maneuvers and system tests conducted during Artemis 1 mission.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Artemis.
It's back.
And we're going to talk about how everything is progressing
and what the next phases are and go from there.
So Artemis 1 completed its 25-day or so mission in space.
That mission has closed.
The Orion spacecraft made it back to Earth.
Splashdown was perfect.
It was scooped up by the Navy on December 11.
It has now made it to port.
And this is all after completing its 240,000-mile journey,
where it was tested.
There was a checklist of deep space maneuvers and things
they wanted to make sure worked.
This was unmanned.
And it did well.
It did well.
Now the craft is being transported back to NASA,
where NASA is going to go over it with a fine-tooth comb,
looking for anything that might have gone wrong,
any system that failed, any physical damage,
stuff like that.
Now, so what's next in the race to the moon here?
Artemis 2 will fly astronauts around the moon sometime
in 2024.
At least that's the schedule.
And that is a ways away.
After that, maybe a year or two, Artemis 3
will put people back on the moon,
put people back on the moon.
And all of this is in pursuit of putting
a base near the southern pole of the moon, a research base.
And there will be a lunar space station
that will orbit the moon that will be used as kind of a truck
stop on the way there.
This lengthy delay that is occurring,
when you're talking about the length of time
between Artemis 1, Artemis 2, and Artemis 3,
and you're wondering why it's taking so long.
Is there something that they're going to be doing,
upgrading, anything like that?
Not really.
That's not really why it's going to have such a giant rest.
It's budget.
It's entirely about money.
It had to do with trying to fill a budget hole a few years ago.
And that has put a delay on everything else.
This is literally all about money.
One of those situations where people will undoubtedly say,
throwing money at things doesn't fix things.
Well, if the problem is a lack of funding, it does.
And that's what NASA is dealing with here.
The person I talked to actually said
that these gaps could be much smaller if they had more money.
But given the current political climate
and the way the economy is, I doubt
that money's coming anytime soon.
But the interesting part, the big news,
everything went as it was supposed to.
And now they'll be doing their physical examinations
and checking everything out.
But at this point, the United States
is still on course to put a base on the moon
within the next few years.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}