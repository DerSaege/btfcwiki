---
title: Let's talk about Republicans addressing the elephant in the room....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2NvQvjZS30o) |
| Published | 2022/12/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans addressed the January 6th insurrection with an open letter from former House members.
- The campaign to overturn the election involved alarming actions by lawmakers, including advocating for martial law and interference with the electoral vote counting.
- Lawmakers sought presidential pardons and shared the goal of preventing the lawful transfer of power.
- The letter stresses the need for legal accountability for lawmakers' actions outside of their legislative duties.
- The Office of Congressional Ethics is urged to thoroughly investigate members involved in the events leading up to January 6th.
- Accountability is seen as fundamental to the integrity of the legislative branch and democracy.
- The letter calls for lessons to be learned from accepting defeat at the ballot box.
- The failure to hold those involved accountable risks history repeating itself.
- A bipartisan effort is appreciated since the majority party in the House seems reluctant to address the issue.
- The resilience of the U.S. system is emphasized as being dependent on the faith and belief of its citizens in the democratic process.

### Quotes

- "The Constitution becomes just another antiquated cutesy document."
- "Supporting a coup attempt will not be tolerated in Congress."
- "Accountability is fundamental to the integrity of the legislative branch and democracy."

### Oneliner

Former House members demand accountability for lawmakers' roles in the January 6th insurrection, stressing the importance of upholding democracy through legal investigations and disciplinary actions.

### Audience

Former lawmakers, concerned citizens

### On-the-ground actions from transcript

- Demand thorough investigation by the Office of Congressional Ethics into members involved in the events leading up to January 6th (suggested)
- Support disciplinary actions by the House if appropriate (suggested)
- Hold accountable those who participated in the insurrection to uphold democracy (implied)

### Whats missing in summary

Importance of bipartisan efforts and the need for public pressure to ensure accountability and protect democracy.

### Tags

#Accountability #Democracy #January6th #Congress #Bipartisan #LegalInvestigation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Republicans addressing the elephant in the room.
An open letter went out from former members of the House of Representatives.
There are a whole lot of Republican signatures on this.
And I don't think it's getting the press that it should.
I'm not going to read the whole thing, but I'm going to read a lot of it.
I think that it's something that people should probably hear.
As is now clear, January 6th was only one event among many that together constituted
an extraordinary campaign to overturn an election.
The scale and audacity of the campaign is profoundly troubling.
Among the most alarming findings is that various members of Congress participated in it.
We now know, for example, that sitting lawmakers corresponded and met with White House officials
and allies to plot various prongs of the campaign, including to advocate that the president declare
martial law, that states submit false certificates of electoral votes to Congress, that the vice
president, in contravention of his constitutional duties, interfere with the counting of electoral
votes, and that federal law enforcement authorities be enlisted to interfere with the election.
Among other startling facts, we now also know that various sitting lawmakers sought presidential
pardons.
These lawmakers stopped short of storming the Capitol themselves, but they shared a
common goal with those who did, to prevent the lawful transfer of power for the first
time in the Republic's history.
And as with those who stormed the Capitol, they must be held accountable.
They go on to talk about how, as lawmakers, as people who were in Congress, they understand
disagreeing with your political opposition.
There are Democratic party members who signed this as well.
And they talk about how that disagreement is good for a healthy democracy, but what
happened isn't.
Our Congress enjoys unique protections in our constitutional order.
Generally, the Constitution insulates lawmakers from liability arising out of the performance
of their legislative duties in order to permit open deliberation by our representatives.
But these protections are not absolute.
The Constitution permits exceptions, including for actions that constitute treason, felony,
and breach of the peace.
Members whose misconduct falls outside of their constitutionally protected legislative
duties are no more immune to legal accountability than their fellow citizens.
We'll see about that, I guess.
They go on.
Based on the facts and findings to date, we urge you to demand that the Office of Congressional
Ethics thoroughly investigate those members who played a role in the events leading up
to and on January 6th.
And if appropriate, that the House exercise its disciplinary functions.
At stake is not only the institutional integrity of the legislative branch to draw and enforce
bright lines of ethical conduct, but the principle of accountability upon which our democracy
rests.
I mean, yeah.
They go on.
All of us have won elections, and many of us have lost them too.
The world's greatest and oldest democratic experiment persists because we, like most
of you, have accepted defeat at the ballot box, and then worked even harder to earn our
constituent support the next time around.
If those who have eschewed that basic Republican tenet evade accountability, we fear lessons
will go unlearned and that history will repeat itself.
I mean, yeah.
No notes.
The right.
It is nice to see a bipartisan effort from former members of Congress addressing this
because one political party, the party that is now the majority in the House of Representatives,
doesn't want to address it.
They want to look anywhere else.
They want to talk about anything other than the fact that by everything that we have seen,
there are certain members who are currently still in Congress, sitting members of Congress,
who definitely appear to have participated in a failed coup attempt.
It's probably something that Congress itself should look into.
The system that exists in the United States, in its functions, it is incredibly resilient.
The way it is structured, it is incredibly resilient.
However, it is all based on belief.
It's all based on the faith of the average citizen.
Without that faith, without the belief that those in Congress are supposed to be there
and are the sort of people who can accept democracy, the whole system falls apart.
The Constitution becomes just another antiquated cutesy document.
That is what the America First crowd has accomplished.
And that's what's at stake.
That's what's at risk here.
The House of Representatives, the Congress as a whole, they have an obligation to look
into this and to demonstrate to the American people that supporting a coup attempt, an
attempt to overturn an election, to dismantle the American system that they all pretend
to love, that that kind of behavior will not be tolerated in Congress.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}