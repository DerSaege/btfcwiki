---
title: Let's talk about Trump's pardon possibilities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KKXa7FrQO_8) |
| Published | 2022/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on the possibility of former President Trump receiving a pardon in the future, potentially from an ally.
- Suggests that delaying tactics might be linked to securing a pardon even if Trump doesn't return to the White House.
- Believes that there are individuals within the Republican Party who might pardon Trump if he faces convictions or preemptively to avoid investigations.
- Urges for candidates in the Republican Party, including moderators, to be questioned directly about their stance on pardoning Trump to distinguish between different factions within the party.
- Indicates that those willing to pardon Trump may hinder the progression of the Republican Party and push it further into irrelevance.
- Stresses the importance of the Republican Party moving towards a more progressive stance to remain relevant and attract voters, particularly on social issues.
- Warns that failure to adapt and become more progressive may lead to irrelevance and reputational damage, especially from the "Sedition Caucus."
- Points out that many within the Republican Party are motivated by maintaining power rather than ideology, making self-defense a key factor in their decisions.
- Emphasizes the need to repeatedly question candidates about their willingness to pardon Trump as a critical issue within the party.

### Quotes

- "They have to become more progressive so they can at least stay within the Overton window of the United States."
- "At this point, the normal conservatives within the Republican Party, they have to be acting on self-defense for their own positions of power."
- "That question about whether or not a particular candidate would pardon Trump needs to be asked over and over and over again."

### Oneliner

Beau stresses the importance of questioning Republican candidates on pardoning Trump to distinguish between factions and urges the party to embrace progressiveness to avoid irrelevance.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Question Republican candidates on their stance regarding pardoning Trump repeatedly (suggested)
- Advocate for progressive policies within the Republican Party (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential impact of pardoning former President Trump on the future of the Republican Party. Viewing the complete video can offer additional insights into Beau's perspective on this issue.

### Tags

#RepublicanParty #Pardon #Progressive #Trump #Factions


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the possibility
of former president Donald J. Trump
receiving a pardon at some point in the future.
In a recent video, I mentioned this as a possibility.
In fact, I said that it might be one of the reasons
he was trying to delay, delay, delay, delay,
because even if he doesn't get back into the White House,
maybe one of his allies does,
somebody willing to pardon him.
And I've had a lot of people send messages and ask,
do I really think that that's a possibility?
Do I think that Trump would be pardoned?
I definitely think it's a possibility.
I definitely think there are people within the Republican Party
who would absolutely pardon him
if he was convicted of any of the multiple things
he's been accused of or pardon him preemptively
to shut down any possible investigation.
I definitely think that's a possibility.
I think it is probably incredibly important for,
well, basically everybody,
the Democratic Party, journalists, moderators,
everybody, to include those moderating
during the Republican primaries,
to ask candidates directly whether or not
they would pardon former President Trump
if he was eventually convicted
or if they would issue a preemptive pardon.
That answer, if it's anything other than no,
they probably shouldn't be considered.
That's a question that will probably end up
being one of the dividing lines
between the normal conservatives,
the Republican Party as it was,
and the MAGA faction, the Sedition Caucus,
that crew that is actually likely to abandon
the Republican Party and go third party at some point
or attempt to continue to exert more and more control
over the Republican Party
and eventually destroy any hope
of having a real conservative party in the United States.
Those who would say,
yeah, I would think about pardoning him or something,
anything other than a flat no,
those people are the people who want to drag
the Republican Party even further into the past.
If the Republican Party is to remain relevant,
they have to move forward.
As hard as this is for many people
in the Republican Party to believe,
the only future for a Republican Party
is to become more progressive.
The dividing lines between what is acceptable levels
of being a conservative and being totally outdated,
the Republican Party is moving further into totally outdated,
and the rhetoric that's going along with that
is pushing more and more voters away.
The future for the Republican Party
is a more progressive party.
I'm not saying that they're going to become,
you know, raging liberals or anything,
but they have to become more progressive
so they can at least stay within the Overton window
of the United States, particularly on social issues.
If they don't do that, they will fade into irrelevance,
but not before the Sedition Caucus winds up
just absolutely destroying all of their reputations.
At this point, the normal conservatives
within the Republican Party,
they have to be acting on...
They have to be acting out of self-defense
for their own positions of power,
because, I mean, let's be honest,
most of these people are not ideologically motivated.
They're about maintaining their power in the status quo.
That's going to be a very strong motivator for them.
That question about whether or not a particular candidate
would pardon Trump needs to be asked
over and over and over again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}