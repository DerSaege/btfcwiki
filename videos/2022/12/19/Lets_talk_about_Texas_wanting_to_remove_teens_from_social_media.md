---
title: Let's talk about Texas wanting to remove teens from social media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ThhJu4t1_U8) |
| Published | 2022/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas proposed a law to ban under-18s from social media and require photo ID for account opening.
- The rationale behind the law is framed as protecting children, but Beau doubts Texas's interest in child protection.
- The true goal behind the law appears to be controlling the flow of information.
- The Republican Party equates social media with their decline in power, leading to this proposed law.
- Beau suggests the simple solution for the Republican Party is to stop lying.
- The proposed law aims to keep younger people ignorant, limit free speech, and control information access.
- Banning under-18s from social media could cut off educational opportunities, especially on platforms like YouTube.
- The law could unintentionally create a surveillance apparatus by linking photo IDs to accounts.
- Many do not realize YouTube is considered social media due to its user-generated content.
- Beau criticizes the law for potentially making Texans less informed and limiting their decision-making abilities.

### Quotes

- "The goal is to control the flow of information."
- "They want to keep younger people ignorant. They want to remove their ability to really engage in any kind of free speech."
- "All it will do is make those people in Texas less informed, less able to make decisions on their own."

### Oneliner

Texas proposed a law to ban under-18s from social media under the guise of child protection, aiming to control information flow and limit young people's access to education and free speech.

### Audience

Texans, Activists, Parents

### On-the-ground actions from transcript

- Rally against the proposed law in Texas (suggested)
- Contact state legislators to voice opposition to the law (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the detrimental effects of the proposed Texas law beyond just social media restrictions. 

### Tags

#Texas #SocialMedia #YouthRights #InformationAccess #Education


## Transcript
Well, howdy there internet people, it's Bo again. So today we are going to talk about a
proposed law in Texas and we're going to talk about how it's being framed, we're going to talk
about what it would actually do, and we're going to talk about why the Republican party
wants this kind of law in the books in the first place. If you missed it, a state legislator out
there in Texas has proposed a law that would prohibit those under the age of
18 from being on social media. It would also require social media companies to
obtain photo ID of anybody who opens up an account. This is being framed as, oh
we're gonna protect the children. Yeah, given events in Texas, I don't believe
that the state legislature there has any interest in protecting children. I don't
believe it at all. All of their actions seem very contrary to that. The goal is
to control the flow of information. As we talked about recently with the move
against TikTok, the Republican Party is equating social media with their decline
in power. And it's kind of true. They are right in that aspect because the ability
to fact-check what Republican leaders say is definitely undermining young
people's confidence in that party. I mean the simple solution here is stop lying
but whatever. That doesn't seem like something they'll be willing to
entertain. So they want to keep younger people ignorant. They want to remove
their ability to really engage in any kind of free speech. They want to remove
their ability to have access to information. They want them easy to
manipulate, easy to control, and easy to indoctrinate. That's what they want. A
A byproduct of this cuts off educational opportunities for people under 18 all over the state.
I want you to think about how often YouTube gets used as an educational tool for kids
doing research for homework.
Parents using it to understand their kids' homework, and most of it is brought up by
the kids who wouldn't have accounts anymore. This would be devastating to
what passes for education in Texas. The byproduct of this is that it creates a
surveillance apparatus. Your photo ID would be on file and linked to your
account. That's what happens. One of the funniest things that I see from people
in the comments is, you know, I don't use social media. You do. You do. Most people
don't think of YouTube as social media, but it absolutely is. I don't think it
would be possible to come up with a legal definition for social media that
wouldn't include YouTube because of all of the user-generated content. This is
obviously just a horrible idea. All it will do is make those people in
Texas less informed, less able to make decisions on their own. It takes the
the children of Texas and turns them into good little worker bees and makes
certain that they have no opportunity to actually advance. They only get to know
what the state says is okay. Meanwhile, the state gets a surveillance apparatus
geared to make sure that, well, you're not engaged in wrong think. Anyway, it's
It's just a thought.
I'll have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}