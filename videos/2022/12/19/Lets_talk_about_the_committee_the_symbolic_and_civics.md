---
title: Let's talk about the committee, the symbolic, and civics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pBDODYrsxvM) |
| Published | 2022/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of symbolism in the context of criminal referrals against the former president.
- Emphasizes that symbolism is significant because it conveys messages beyond the normal activist community.
- Describes the process of different branches of government signing off on determinations related to the case.
- Illustrates how the system functions with the legislative, judicial, and executive branches making determinations.
- Acknowledges that while symbolic, the referrals are not irrelevant and play a vital role in showcasing a unified decision by all branches of government.
- Advocates for understanding the symbolic importance and not letting commentators downplay its significance.
- Stresses that symbolism carries messages to the public and is a key aspect of activism and government operations.

### Quotes

- "Symbolism matters."
- "It's showing that the system as a whole, the government as a whole, representatives of all three branches have made the same determination."
- "The symbolic matters a lot."

### Oneliner

Beau explains the importance of symbolism in government actions and how it carries messages to the public.

### Audience

Government observers

### On-the-ground actions from transcript

<!-- Skip this section if there aren't physical actions described or suggested. -->

### Whats missing in summary

The full transcript provides additional details on the significance of symbolic acts in government and activism.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the committee.
We're going to talk about the symbolic.
And we're going to talk about civics, some basic civics,
because some commentators are missing something.
They continue to talk about the referrals being symbolic.
And in case you missed it, the committee met,
and they did decide to issue referrals, criminal referrals,
against the former president.
You have a lot of commentators saying that they're symbolic.
And they are.
Don't get me wrong.
They are symbolic to the Department of Justice's
charging decision.
Somewhere along the line, symbolic in the commentator's
view became irrelevant.
That's the tone.
Symbolic really doesn't matter.
No.
Symbolism is super important, actually.
Don't believe me?
If this video had been filmed and the patch on my hat
had been upside down, how would you take it?
Symbolism matters.
We have talked about it on the channel
before through the framing of the performative.
When people are talking about activism of various kinds,
and people, oh, that's performative,
and they blow it off.
I love the performative, because it gets the message out
into the public discourse beyond the normal activist
community.
It carries that message forward.
It's them cosigning it.
And with a lot of activism, a big part of it
is getting it out into the public discourse.
So it's important.
The people who get the goods may roll their eyes at it,
but it matters.
The same is true here.
When the committee started, within the first few minutes,
they brought up how a federal judge had
looked at just a sliver of their evidence
through the window of the crime fraud exemption.
And that federal judge determined that a crime
had likely taken place.
Now you have Congress saying a crime has likely taken place.
And then you'll have the Department of Justice
making the final determination on it, right?
What are the three co-equal branches of government?
Legislative, judicial, and executive.
The federal judge, representing the judicial branch,
signs off on it.
The committee, representing Congress,
the legislative branch, signs off on it.
When the Department of Justice makes its determination,
that's the executive branch.
All three branches of government signing off on it.
Then, once an indictment, if it occurs,
then it would go back to the judicial.
And the judicial would be looking at this case
and determining whether or not laws
created by the legislative and the executive were violated.
This is how the system is actually supposed to work,
three co-equal branches of government.
So, yes, it is symbolic when it comes
to DOJ's charging decision.
But it is not irrelevant.
It's not that it doesn't matter.
It's showing that the system as a whole,
the government as a whole,
representatives of all three branches
have made the same determination.
And it helps carry that message to the public.
So, yes, symbolic.
But don't let the commentators make that word mean irrelevant.
The symbolic matters a lot.
So, that's just a key piece of information
I wanted to get out now before that narrative
just really gets underway.
There will be a whole bunch more commentary
coming on the committee as a whole
and what's going on from here coming over the next few days.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}