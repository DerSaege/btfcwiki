---
title: Let's talk about dropping people in DC and a song from the 1990s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gBliegqIoiA) |
| Published | 2022/12/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about a song from the 90s that spent 22 weeks on the country music charts, not really a country song, but a Christian song.
- Mentions people being dropped off in DC in freezing temperatures on Christmas by a governor for a political point.
- Acknowledges the justified anger towards the governor's actions.
- Praises the Migrant Solidarity Mutual Aid Network for providing shelter, food, clothes, and toys to those dropped off, showcasing the power of community networks.
- Points out the irony that leftists teamed up with religious institutions to help those in need, contrasting with the governor's actions.
- Criticizes the governor for using people as props and not following the values of his supposed Christian beliefs.
- Expresses concerns about the conservative Christian movement being consumed by hate and politicization.
- Raises the question of what if Jesus comes back as one of the city folk mentioned in the Christian song.
- Concludes by urging those who support the governor's actions to "need Jesus" and states there is no defense for the governor's actions.

### Quotes

- "Y'all need Jesus."
- "There is absolutely no way to defend this."

### Oneliner

Beau talks about a 90s Christian song, contrasts it with a governor's inhumane actions, praises community aid efforts, and criticizes politicization of conservative Christianity.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Support and volunteer with organizations like the Migrant Solidarity Mutual Aid Network (exemplified)

### Whats missing in summary

The emotional impact of the contrast between community aid efforts and the governor's actions, as well as the call for reflection on religious and moral values.

### Tags

#CommunityAid #ConservativeChristianity #PoliticalInjustice #MigrantSolidarity #CommunityNetworks


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about a song from the 90s.
And those people getting dropped off in DC the way they did.
And what that song can kind of teach us.
So this song, it was on the country music charts
in 95, 96, in that range, for 22 weeks or so.
At two different times, it was kind of strange.
And there's more that's unique about it.
But I had just finished showing this song to somebody
to make a very similar point to the one I'm making right now.
And when I was done, I happened to glance at the news.
And that's when I found out that presumably some governor,
nobody has taken responsibility for this yet,
but some governor had a whole bunch of people
dumped off a bus in DC with their kids
in freezing temperatures on Christmas
to make some political point.
And there's a whole lot of anger about that.
And do not get me wrong, that anger is totally justified.
Y'all can have that.
But I want to talk about something else.
A group called the Migrant Solidarity Mutual Aid Network
jumped into action, teamed up with churches, synagogues,
religious institutions.
And they got those people to a place
where they had shelter, food, clothes, even toys for the kids.
That is the power of an established community network.
You can have a governor doing everything within their power
to make things worse.
And a network like that can mitigate the harm.
And then you got to think about that name.
Migrant Solidarity Mutual Aid Network.
Now I didn't look it up.
I don't need to.
Those are leftists.
Those terms, those are leftists.
And they're teamed up with religious institutions
to take care of the stranger in their land,
feed, shelter, and clothe people.
Because some governor from the Bible Belt
was elected by a whole bunch of people who obviously have not
read their Bible, or at least didn't understand it.
And he's using people as props, dropping them off
in freezing temperatures on Christmas.
And it was leftists teamed up with religious institutions
that fixed it.
That's wild.
That song, the reason it's interesting
that it spent 22 weeks on the hot country singles chart,
because it's not a country song.
I mean, not really.
Doesn't say anything about mama or prison or trains.
It actually does say something about a train.
That's hilarious.
But it's not a country song.
It's a Christian song.
And not in like the cutesy way the country music stars get
together and record a new version of Amazing Grace
or something like that.
It's legitimately a Christian song.
And it stayed on the charts that long.
Because in the mid-90s, the population liked it.
And that shows how far the conservative Christian movement
in this country has fallen, how much it has been consumed
by hate and been politicized.
Because if you go back and listen to that song,
they're the city folk that Colin Ray is
talking about in the song.
What if Jesus comes back like that?
In fact, being the city folk, it's
literally a talking point now.
You got to clear those people out.
I know it's a cliche to say this in the South,
but there's a whole lot of Christians down here.
Y'all need Jesus.
There's no way to defend this.
There is absolutely no way to defend this.
All of it, it's wrong.
Yet, the governor's responsible.
They can't re-elect.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}