---
title: Let's talk about understanding social media influence operations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cQcxHvAxrzQ) |
| Published | 2022/12/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains influence operations on social media as marketing, advertising, and not just spy stuff.
- Describes how amplifying existing movements on social media can disrupt the status quo in a target country.
- Points out that stability is the ultimate goal in foreign policy.
- Mentions the role of amplifying opposition to create disunity and weaken a country's ability to respond internationally.
- Uses the example of the Black Lives Matter movement to illustrate how influence operations can work.
- Talks about how the US conducts influence operations in other countries and how the Kurdish people's decentralized movement hinders US intervention.
- Suggests that addressing inequality is the key to preventing exploitation of marginalized groups.
- Emphasizes that racial justice in the US is a matter of national security.
- Raises concerns about platforms like TikTok being used for influence operations due to their ability to control algorithms and spread messages.
- Advocates for addressing underlying issues rather than just trying to stop the exploitation.

### Quotes

- "It's marketing, it's advertising, nothing more."
- "Yes, racial justice in the United States is a matter of national security."
- "The easy fix here is to fix the inequality."
- "To me, the solution is fix the problem, fix the thing that can be exploited."
- "When people talk about influence operations, think of it in terms of Coca-Cola and Pepsi, not in terms of cameras and trench coats."

### Oneliner

Beau explains influence operations as marketing on social media, showing how amplifying existing movements can disrupt stability in a target country, and advocates for addressing inequality as a solution to prevent exploitation.

### Audience

Internet users

### On-the-ground actions from transcript

- Address inequality to prevent exploitation of marginalized groups (suggested)
- Advocate for racial justice as a matter of national security (suggested)

### Whats missing in summary

The full transcript provides a detailed explanation of influence operations on social media, using real-world examples to illustrate the impact of amplifying movements and advocating for addressing inequality as a preventive measure against exploitation.

### Tags

#InfluenceOperations #SocialMedia #ForeignPolicy #RacialJustice #Inequality


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about influence
on the internet and how it relates to foreign policy
and how that relates to recent news
concerning social media platforms
and the US stance towards them.
Recently, we talked about how there is a large push
There is a large push to basically get rid of TikTok in the US.
And I used the term influence operation.
And a whole bunch of messages came in asking what one was, in essence.
And here's the thing.
When you are talking about influence operations on social media, you're way over-complicating
it.
Okay?
Don't think of it as spy stuff.
It is, but don't think of it that way.
It's marketing, it's advertising, nothing more.
That's all it is.
And it's incredibly effective.
Let me give you an example of how it can work.
Let's say your target country has a permanent underclass,
a group of people who are not treated fairly.
They are treated unfairly economically.
They are treated unfairly by the criminal justice system,
by state security.
They are an underclass.
That demographic, that underclass
of people and their allies, they will always
have an organic movement striving for change,
striving for justice, striving for things to be fair,
because people like things to be fair, most people anyway.
So that organic movement exists in your target country.
An influence operation, a successful one,
would be to just amplify them.
On social media, you amplify their message,
so they get more people.
As that movement grows, it's capable of disrupting
the status quo in that country.
When you're talking about foreign policy,
Remember, status quo is stability.
Stability is good.
Because you're not actually, as the person running an influence
operation, you don't care about morality or ideology
or any of that.
It's about power because it's about foreign policy.
So you amplify that message.
You help that movement grow.
You get that movement out there and into the streets.
Now, since, again, you don't care
morality or ideology. You don't care about social change. What would be really
smart to do at the same time? Amplify their opposition. You amplify their
opposition so you create a very polarized topic. You create disunity in
your target country. You create us-versus-them mentality and it sows
discord, right? If that happens, that country is not as capable of responding
to things on the international scene. So if you were hypothetically preparing to
manufacture a pretext to invade a neighboring country, you might do that. If
somebody did this, if a nation did this to the United States, what would it look
like the summer of Black Lives Matter because it happened, it happened.
Now what does this mean?
Does this mean that the movement of Black Lives Matter, that it was a Russian propaganda
effort?
No, it was organic and that movement while there were formalized structures that were
called BLM, the movement as a whole was decentralized despite some headlines from the time.
You can't co-opt a decentralized movement like that.
They amplified both sides, created disunity, and basically weakened the US's ability to
respond on the international scene.
That's how an influence operation works.
Now, just to be clear, the United States does this all the time.
The US does it to other countries all the time.
And I am of the firm belief that the only reason the United States always stops one
step short of helping the Kurdish people gain independence is because the Kurdish people
are spread out across a number of hotspot countries, which means they always have that
organic movement that they can tap into.
So that's how they run.
It's advertising.
more. Can TikTok do this? Yeah, it's kind of the perfect platform to do it really.
Yes, it can be used to do that because all they have to do is amplify whatever
message they want. Every third or fourth video you see is something amplifying
that message, getting it into the political discussion and creating that
division or perhaps the alternative to this is to feed into a narrative that
undermines US national security from a different perspective. Say feeding into
conspiracy theories telling people to not take preventative measures during
the middle of a pandemic or feeding into conspiracy theories that undermine faith
in the election system. This is how they would be used and maybe have been. One of
the important things to note from this is rather than looking at it and saying
oh well this demographic they can be exploited. Why can they be exploited?
Because they're not treated fairly. It's organic. They have a legitimate concern
So they can be exploited by a foreign intelligence service.
The easy fix here is not to start locking down that demographic and putting them under tighter security
or getting rid of social media platforms that could be used to do that or anything like that.
The easy fix here is to fix the inequality.
If you treat them fairly they're no longer an easily exploitable force. Yes,
racial justice in the United States is a matter of national security. That
should be the takeaway. So given that TikTok is the perfect platform to do
this and it's not really that hard because all you're doing is controlling
algorithm to create what appears to be a viral messaging of a topic, US
counterintelligence does have a legitimate concern about this. They
really do. I personally think they're going about it the wrong way. To me the
solution is fix the problem, fix the thing that can be exploited, not
constantly try to play goalie and stop people from reaching them. So when people
talk about influence operations, think of it in terms of Coca-Cola and Pepsi, not
in terms of cameras and trench coats. Anyway, it's just a thought. Y'all have a
a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}