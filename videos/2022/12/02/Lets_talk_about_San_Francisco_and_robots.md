---
title: Let's talk about San Francisco and robots....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BMlpjsEAx40) |
| Published | 2022/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- San Francisco PD is considering using robots to deploy lethal force.
- The policy allowing the use of robots for criminal apprehensions is incredibly broad.
- There are very few situations where using robots for lethal force is remotely acceptable.
- The policy lacks limitations and allows too much room for misuse.
- Only the highest trained individuals with the best information should handle such technology.
- San Francisco residents are urged to make their voices heard on this issue.
- Civilian police departments should not have the ability to use robots for lethal force.
- The misuse of robots in this manner is inevitable and will result in lost innocence.
- The lack of necessary information and training makes this policy a dangerous idea.
- San Francisco's Board of Supervisors needs to reconsider this decision.

### Quotes

- "This is a horrible, horrible idea. This will go bad."
- "Civilian police departments should not need to be able to mete out lethal force via a robot."
- "It will become too tempting to use it in situations in which they do not have the information necessary."

### Oneliner

San Francisco PD's broad policy allowing robots for lethal force is a horrible idea that will inevitably lead to misuse, urging residents to speak out against it.

### Audience

San Francisco residents

### On-the-ground actions from transcript

- Make your voice heard on the issue (suggested)
- Advocate against the use of robots for lethal force (implied)

### Whats missing in summary

The full transcript provides additional insights on the potential dangers and ethical concerns surrounding the use of robots for deploying lethal force by civilian police departments.

### Tags

#SanFrancisco #Robots #LethalForce #PolicePolicy #CommunitySafety


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about San Francisco
and robo-cops I guess.
Because the PD out there, the police department out there,
would like to use robots in a few situations
that we will go over.
We have talked about this on this channel repeatedly.
The reason on the foreign policy scene
drones are used so much is because they
lack the risk associated with having
American boots on the ground.
They don't risk American lives.
So the use of a drone becomes incredibly tempting,
even when the information isn't there to back it up,
which leads to a lot of problems.
This habit, this is something that policymakers
on the international scene fall prey to.
They begin to accept the idea that it's better
to use the drone just because it's easier,
because it doesn't risk an American life.
Even though we all know about the mistakes that occur.
San Francisco PD would like to use robots to deploy lethal force.
This is a horrible, horrible idea.
This will go bad.
Not might.
This will go bad.
This will be misused.
Innocence will be lost.
The policy is incredibly broad.
The robots listed in this section
shall not be utilized outside training and simulations,
criminal apprehensions, critical incidents,
exigent circumstances, executing a warrant,
or during suspicious device assessments.
Criminal apprehensions.
I mean, that's pretty much catch-all for everything.
This isn't a limiting policy in any way.
This is incredibly broad.
It allows way too much room for misuse.
And understand, there are very, very few situations
in which something like this would even
be remotely acceptable.
And even then, only by the highest trained people
with the best information, which to be honest,
based off of what I've seen on this policy,
isn't going to exist in San Francisco PD.
This is a horrible idea.
Now, I guess they're called Board of Supervisors out there.
They have done one vote, but they have to do another.
If you live in San Francisco, I would strongly
suggest you make your voice heard.
I'll have a link below to kind of help with the process.
This is not something that needs to happen.
A civilian police department does not
need to be able to mete out lethal force via a robot.
It will become too tempting to use it
in situations in which they do not
have the information necessary.
It will get out of hand.
In a sense, it will be lost.
There's not a maybe here.
This isn't thought out.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}