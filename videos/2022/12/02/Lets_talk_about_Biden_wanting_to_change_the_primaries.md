---
title: Let's talk about Biden wanting to change the primaries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CwVTvKVPRQ0) |
| Published | 2022/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Democratic Party is discussing changing the order in which states vote during the primaries, specifically moving up South Carolina as the first state.
- The early primaries play a critical role in setting the tone for a candidate's campaign and can essentially make or break it.
- Currently, states like New Hampshire and Iowa, which are predominantly white, kick off the primary season, leading to a lack of diversity in representing the Democratic Party's base.
- South Carolina, with a more diverse population, could provide a more accurate reflection of the party's broader demographics and ideologies.
- Moving South Carolina up could indicate a shift in the Democratic Party establishment's recognition of the importance of Black Americans in the party's success.
- The goal behind changing the primary order is to make the process more representative and responsive to what the party's base actually wants.
- Opponents of this change, such as New Hampshire and Iowa, may resist due to losing their early primary status and the influence it carries.
- New Hampshire, for instance, has laws to ensure its primary remains first or among the top three due to its perceived importance in setting the campaign tone.
- Despite potential opposition, Beau believes that this change is necessary for the Democratic Party to capitalize on opportunities presented by the Republican Party's internal struggles.
- Shifting the primary order could help in selecting candidates who energize the entire base, including those from more diverse areas within the party.

### Quotes

- "Moving South Carolina up and having the lead off a state that is diverse, that will give a much more accurate read on how the rest of the country thinks."
- "Setting the tone and getting candidates that energize all of the base rather than just the lily white areas is probably a good idea."

### Oneliner

The Democratic Party considers changing the primary order to better represent its diverse base and capitalize on internal party dynamics.

### Audience

Political activists, Democratic Party supporters

### On-the-ground actions from transcript

- Contact local Democratic Party officials to express support for changing the primary order to better represent diversity (suggested)
- Attend or organize community meetings to advocate for a more inclusive primary process (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of why changing the order of primaries is significant for the Democratic Party's representation and success.

### Tags

#DemocraticParty #PrimaryOrder #Diversity #BlackAmericans #PoliticalStrategy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
the Democratic Party's discussions over changing the order
in which states vote during the primaries
and why it matters.
The media is covering this.
This is actually getting some coverage,
but I don't think they're doing a good job
of putting it into context and judging by my inbox.
Y'all don't think so either.
So we're gonna kind of go over
what they're talking about doing,
why they're talking about doing it,
and why there might be some resistance to it,
and more importantly, what it means,
because it actually kind of means something cool
as we go through all of this.
So what's happening starting there?
Biden has made it pretty clear.
He thinks that the order in which states conduct
their primaries should change.
While it's not in the memo or letter that was sent,
it has been heavily hinted that he thinks South Carolina
should be moved up, maybe even the first one.
And the question from there kind of becomes,
why does it even matter what order they go in?
When somebody is running to get the nomination,
there's a field of candidates,
and those first few primaries really set the tone.
In fact, if somebody doesn't do well in the first couple,
well, their campaign's pretty much over.
So think about the base of the Democratic Party.
It doesn't all look the same, right?
The people that make up that base,
it's a pretty diverse base.
We've talked about it on the channel.
It's a coalition party.
It's not even, I mean,
set aside the normal demographic information.
Even when it comes to ideology, it's a coalition.
And then you look at the demographics.
It is incredibly diverse.
The two leading states right now,
the two states that kind of kick everything off,
New Hampshire and Iowa.
When you think New Hampshire and Iowa,
do you think a bastion of diversity?
No, you probably don't, because they're not.
New Hampshire is 92.8% white.
It is only 1.9% black.
Iowa is 89.09% white.
It is only 3.72% black.
Do those percentages even remotely represent
the votes that the Democratic Party captures?
No, not at all.
What about South Carolina?
South Carolina is 68.6% white and 26.7% black.
That's why.
That's why this is coming up.
This is, the reason it's kind of cool
is because this is the first sign I've seen
that the Democratic Party establishment
is really beginning to understand
how crucial black Americans are to the success of the party
and how if they don't get candidates
that black Americans want,
well, they're not energized, so they don't show up.
Moving South Carolina up and having the lead off
a state that is diverse,
that will give a much more accurate read
on how the rest of the country thinks,
and more importantly, how their base thinks,
is something that should have happened a long, long time ago.
So that's the motive behind it.
The idea is to make the primaries,
those first primaries, more representative
and get a better feel for what their base actually wants.
I know that's weird to say
because this is not something that,
this is not something that the Democratic Party
establishment's really known for,
but that's the goal.
It's a good move.
Will there be opposition?
Of course New Hampshire and Iowa are going to be furious.
They'll try to stop it.
I don't know if they'll be able to.
I think New Hampshire actually has like a state law
that says that their primary has to be first
or maybe top three.
I don't really remember,
but I think they have their own law
because they know how important it is.
They understand that those first primaries set the tone.
So they want somebody who their state approves of
to get that momentum early.
So there will be opposition to it,
but it's more,
I think the opposition will be more about maintaining
those states' power than about denying the base the say,
which is probably how it's going to come off,
even though that's not,
that's probably not why they're going to fight it.
That's definitely how it's going to be perceived.
In the end, this needs to happen.
Whether it happens now or later, I don't know.
But if the Democratic Party wants to capitalize on the edge
it has been given by the Republican Party
just tearing itself apart,
this would be a good way to do it.
Setting the tone and getting candidates
that energize all of the base
rather than just the lily white areas
is probably a good idea.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}