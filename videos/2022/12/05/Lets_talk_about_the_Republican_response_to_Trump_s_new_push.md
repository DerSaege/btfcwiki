---
title: Let's talk about the Republican response to Trump's new push....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8YXDQ7mtL0Q) |
| Published | 2022/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Party's lack of response to Trump's statements about the US Constitution.
- Most of the Republican Party has remained silent or adopted a strategy of ignoring Trump's remarks.
- Some exceptions like Mike Turner and Liz Cheney have spoken out against Trump's ideas.
- Representative Joyce exemplifies the strategy of ignoring Trump's statements and hoping he goes away.
- Despite silence, Republicans like McCarthy claim to love the Constitution but fail to address Trump's remarks.
- Trump's desire to terminate the Constitution is a serious concern that Republicans must confront.
- Standing up to Trump is necessary to prevent him from dominating the Republican primary.
- Failure to oppose Trump could result in turning the Republican Party into the Anti-Constitution Party.
- Trump's statements are not just hyperbole; he doubled down on his desire to terminate the Constitution.
- Republicans need to push back strongly against Trump to prevent him from pursuing his extreme ideas.

### Quotes

- "The silence shows how performative their love of country is and the love of Constitution."
- "If they don't stand up to him now, he very well might turn the Republican Party into the Anti-Constitution Party."
- "Trump had dinner with somebody who is known for openly calling for dictatorship, and not long after, Trump is saying to terminate the U.S. Constitution."

### Oneliner

Republican Party's silence on Trump's desire to terminate the Constitution may turn them into the Anti-Constitution Party if they fail to stand up to him now.

### Audience

Republicans, Political Activists

### On-the-ground actions from transcript

- Call out and challenge political leaders who fail to oppose dangerous ideologies like terminating the Constitution (implied).
- Support Republican figures like Mike Turner and Liz Cheney who speak out against extreme ideas (implied).

### Whats missing in summary

The full transcript provides detailed insights into the Republican Party's response to Trump's alarming statements about the US Constitution. Viewing the entire video can offer a more comprehensive understanding of Beau's analysis and arguments.

### Tags

#RepublicanParty #Trump #USConstitution #PoliticalStrategy #Silence


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the response,
the reaction, or lack thereof, from the Republican
Party, when it comes to former President Trump's recent
statements about the US Constitution.
For the most part, there hasn't been one.
been crickets. For the most part, the Republican Party seems to have adopted a strategy, and
we're going to talk about that strategy. But first, I do want to point out that we are
talking about most of the Republican Party. There are some exceptions to what I'm saying.
Mike Turner is an example of one, very clearly, vehemently disagreed with the idea of terminating
the U.S. Constitution. Liz Cheney came out and called him an enemy of the
Constitution. There are a few Republicans speaking out, but there aren't many, and
they are exactly who you would expect them to be. For the most part, it
appears that the Republican Party would just like to ignore him and hope he goes
away best exemplified by Representative Joyce and the comments that were made
in response to these questions because he was asked you know how he felt about
it and he was basically like you know what I don't even need to respond to
that it doesn't matter he's not gonna win the primary anyway so we don't have
to worry about that thing that he said about shredding the U.S.
Constitution, we just don't need to talk about it, general tone, but in the same
interview, he was later asked if he would support Trump if he became the nominee.
And he said he would support whoever the Republican nominee is.
So, he would support a Republican nominee who wanted to terminate the Constitution.
That's only part of the reason this strategy won't work.
Here's the thing.
Winning the libs, that's not a winning electoral strategy for a general election.
Trashing rhinos, oh that totally wins a Republican primary, and that's exactly what Trump's
going to do.
If they don't stand up to him, he very well might win the nomination.
They have to stand up to him now, because if they don't, he will run over them during
that primary, and if he does that, once he has the nomination, well, they'll support
whoever wins the nomination.
If they don't stand up to him now, he very well might turn the Republican Party into
the Anti-Constitution Party because they won't stand up to him.
The silence shows how performative their love of country is and the love of Constitution.
You got McCarthy there saying that he wants to read the Constitution on the House floor
and that's what he would do if he was speaker.
But has said nothing about the de facto leader of the Republican party saying he wants to
terminate the Constitution.
That seems odd.
It's almost like reading the Constitution on the House floor is just a show for those
people who still believe in those silly notions.
That's what it seems like.
And for those who would say that Trump was just, you know, being Trump and being full
of hyperbole, that's not really what he meant, it's worth noting he doubled down on it.
There are those who tried to reframe what he said and twist it.
He doubled down on it.
He said that the situation called for an unprecedented cure, quote.
That cure, of course, being, you know, destroying the US Constitution.
This isn't going to go away.
The Republican Party can't ignore this because they have to look at the chain of events and
understand what happened.
Trump had dinner with somebody who is known for openly calling for dictatorship, and not
long after, Trump is saying to terminate the U.S. Constitution.
This isn't going to go away.
This is going to be his new build the wall.
The mild pushback, the Republican Party seems to think it's going to be enough.
Nope, it won't.
If they don't push back and push back hard, not just will he stand a much better chance
of winning the primary, if he wins, he's going to go through with it.
going to try to because well they'll support him if he's the nominee. Anyway
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}