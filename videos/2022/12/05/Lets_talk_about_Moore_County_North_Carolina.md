---
title: Let's talk about Moore County, North Carolina....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Opp09pm_r68) |
| Published | 2022/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Electrical substations in Moore County, North Carolina were taken offline abruptly, leaving tens of thousands without electricity.
- The sheriff's department doesn't have suspects yet, but the FBI has joined the investigation due to resource limitations.
- Speculation on social media about the incident is compelling but not evidence; investigators are needed to verify.
- The incident may be considered domestic terrorism depending on the intent of those responsible.
- Moore County has a significant population over 65, and the power outage occurred in freezing temperatures.
- North Carolina has felony murder charges, and the outage's impact on lives could be viewed as causal.
- Those responsible likely didn't anticipate the potentially fatal consequences of cutting power to older residents.
- If any deaths occur due to the outage, individuals involved may quickly cooperate with law enforcement.
- Official information and details on the investigation are limited at this point.
- The FBI's involvement suggests the coordinated incident is more complex than simple vandalism.

### Quotes

- "It may not matter. It may not matter."
- "If any deaths occur due to the outage, individuals involved may quickly cooperate with law enforcement."
- "There are a lot of developments here. There are a lot of things that can change all of the math in how this plays out."

### Oneliner

Electrical substations offline in Moore County, potential domestic terrorism, FBI joins investigation, and lives at risk due to power outage in freezing temperatures.

### Audience

Community members, investigators.

### On-the-ground actions from transcript

- Reach out to local authorities with any information that might assist the investigation (implied).
- Ensure the well-being of vulnerable community members during power outages (implied).

### Whats missing in summary

Details on specific actions individuals can take to support older residents in Moore County during power outages.

### Tags

#MooreCounty #NorthCarolina #PowerOutage #FBI #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about what's
going on in Moore County, North Carolina,
try to answer some of the questions about what
happened up there, and just kind of go over everything.
Now, if you don't know what happened at all,
let's just say some electrical substations were taken offline
abruptly in what appears to be a pretty coordinated manner,
plunging tens of thousands into darkness with no electricity.
Now, at time of filming, the sheriff's department
there has said that they don't really have any suspects.
I have seen some of the information
that is floating around on social media about this.
It is compelling, but it's not evidence.
It's compelling.
It would need investigators to run that down.
A lot of sheriff's departments in North Carolina,
well, they don't really have the resources to do that.
Interesting side note, the FBI has joined the investigation.
They do have those resources.
So those little packages that are out there on social media
talking about who may have been behind this,
I would imagine that they will be looked at.
Now, one of the big questions that has come in to me
is, is this the T word?
Well, maybe.
Maybe.
It all depends on the intent of the people who did it.
Everything else is there, but it depends
on the intent of the people who did it
whether or not that label can accurately be applied.
But here's the thing.
It may not matter.
It may not matter.
And I know that sounds weird, because I'm
certain that people would want that charge.
But there are three interesting facts about Moore County
that you might need to know.
The first is that, if I'm not mistaken,
the largest age group in Moore County is 65 plus.
The next interesting fact is that at time of filming,
according to the weather I got on the internet,
it's 31 degrees there with no power.
The third interesting fact is that Moore County
sits in North Carolina, a state that
has felony murder charges.
I'm not a lawyer.
I do not know.
But given North Carolina's reputation
when it comes to this sort of thing,
they very well may view this as causal.
And anybody who is lost due to the temperature
because the power is out, well, that may be part of,
they may view that as directly related to the substations
being knocked out.
I am fairly certain that the people who did this,
they didn't see that coming.
They didn't think that through.
And it is pretty likely that if that was to occur,
and generally speaking, when you plunge
tens of thousands of people, with a whole lot of them
being older, into freezing temperatures,
you often lose somebody.
If that was to occur, I would imagine
those on the periphery of this, they
will probably be talking to law enforcement very, very quickly.
At this point in time, there is no official word.
Yeah, we know what it looks like.
We do.
There are some compelling little bits out there.
They haven't been run down yet.
Or if they have, that hasn't been disclosed.
So we're going to have to wait on this.
But there are a lot of developments here.
There are a lot of things that can change all of the math
in how this plays out.
But the key parts here, the FBI has joined the investigation.
And at time of filming, not a lot of information
has been released.
And yeah, it does look coordinated.
It doesn't.
I would be surprised if this was just a simple case of vandals.
It's not out of the realm of possibility.
But it would be surprising.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}