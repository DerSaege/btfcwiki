---
title: Let's talk about news sources and trade magazines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UD5-nSwi0C0) |
| Published | 2022/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Starts by discussing his sources for staying informed, prompted by a question on Twitter.
- Begins his daily information consumption with the Associated Press (AP) due to its factual reporting with minimal analysis.
- Moves on to sources with different biases, including major news outlets and social media for public reactions.
- Advocacy organizations and trade journals are also part of his information sources for diverse perspectives.
- Emphasizes the importance of reading polls, surveys, and studies to inform opinions effectively.
- Explains the concept of trade journals using examples like The Hollywood Reporter and Billboard for specific industries.
- Notes that trade journals are biased towards the industries they represent but contain valuable information.
- Suggests that insights from trade journals, especially when linked with sources like AP, can enhance understanding, particularly in areas like US politics.
- Points out that industry suggestions in these publications can predict lobbying actions and political stances.
- Encourages utilizing such sources for a broader and more informed perspective on various topics, including politics.

### Quotes

- "Every outlet has a bias."
- "There's a lot that can be gleaned out of these sources."
- "You can't really tell the future, but you have a much, much more informed guess."

### Oneliner

Beau explains his diverse information sources, including trade journals, to gain a well-rounded understanding and predict industry influences on politics.

### Audience

Information seekers

### On-the-ground actions from transcript

- Follow diverse sources of information to gain a comprehensive understanding (exemplified)
- Read trade journals related to specific industries to uncover valuable insights (exemplified)
- Analyze industry suggestions in trade magazines to predict lobbying actions (implied)

### What's missing in summary

Beau's engaging delivery and additional examples could provide further depth to the importance of varied information sources.

### Tags

#InformationConsumption #DiverseSources #TradeJournals #Bias #USPolitics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about information consumption
and sources that I use and kind of how I stay informed.
And we're really going to do this because somebody asked
this question over Twitter.
And I went through it.
And one of the items that I use prompted
a whole lot of questions.
And I realized a whole lot of people are either unaware
or definitely underutilizing this particular type of source.
OK, so generally, every day, I start with AP,
the Associated Press.
Pretty much read everything that they publish in English.
From there, I move to sites that have a more open bias.
AP is pretty bare-bones reporting.
It's very factual.
There's not a lot of analysis.
Therefore, there's not a lot of room
for inserting their opinion.
And then you can move to the outlets that are generally
seen as the big news outlets.
They have a bias, all of them, whatever your favorite one is.
They have a bias, too.
Every outlet has a bias.
Then I tend to scroll social media
and look at people's reactions to various news events
to get kind of a feel on where people are at.
Then I will look to both left and right advocacy
organizations.
Every advocacy group doesn't matter what the cause is,
whether it's the environment or civil rights.
They have what amounts to an online magazine,
even if it's just something that appears kind of low-tech
like a blog.
They have something.
They tend to have information that
doesn't really get reported.
And then, as you all know, I read a lot of polls, surveys,
and studies, because that can give you
the data to better inform whatever opinion that you're
shaping.
Then I mentioned trade journals.
And it just prompted a flood of questions.
A whole lot of people really didn't even
seem to know what they were.
So I'm going to give you two examples that you're probably
familiar with, and then just kind of explain
that these exist for everything.
The Hollywood Reporter, it's a magazine.
It covers film.
People don't see it as a trade magazine,
because it's a lot of celebrity stuff's in it.
But that's really what it is.
It's just for that particular industry.
Another one is Billboard, the music industry.
Every industry has magazines or outlets like this.
Everything from shipping to defense to, name it,
retail, welding, papermaking, it doesn't matter.
Everything has their own outlet.
Now, these outlets are incredibly biased.
They are there to represent the interest of the group
that they are kind of filling a void for.
Whatever the industry is, their industry
is obviously going to be portrayed favorably.
However, the reason I started talking about supply chain
issues long before everybody else
is because I read shipping journals, trade magazines
for international shipping.
There's a lot of information in there
that is incredibly useful, especially
if you can tie it to a source like AP.
And you can draw those lines and those connections.
The people who write for those types of magazines,
for trade magazines, trade journals,
whatever you want to call them, they
tend to be very well informed on their particular industry.
You want to know about mining and where
they're going to start mining?
Look in a mining trade magazine.
They'll tell you.
Because there will be basically help
wanted for managers at a new mine.
If you want to know about defense,
Janes, Defense One, there's a bunch of them.
All of this information is out there.
But this is typically information
that would only be consumed by a small group of people.
If you're wanting to become better informed,
there's a lot that can be gleaned out of these sources,
especially when you're talking about anything dealing
with US politics.
Because large industries tend to have large lobbies.
So when you see a suggestion in one of these magazines,
it says this is what we need a law,
or this is why this law is bad for us.
And then you see that industry start
to donate a lot of money to a particular candidate's
re-election campaign, well, you can pretty much
guess how they're going to vote.
It's part of that thing that helps create a crystal ball.
You can't really tell the future,
but you have a much, much more informed guess.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}