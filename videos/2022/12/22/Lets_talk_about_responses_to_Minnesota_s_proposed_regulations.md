---
title: Let's talk about responses to Minnesota's proposed regulations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GIajeWHZTow) |
| Published | 2022/12/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Addresses proposed regulations in Minnesota regarding law enforcement officers participating in online hate speech.
- Explains the purpose of the First Amendment and distinguishes it from the concept of free speech.
- Argues that law enforcement officers, as agents of the government, should be held to a higher standard.
- Suggests that being a bigot contradicts the job qualifications for law enforcement officers.
- Draws parallels with other professions like nursing and fast food service to illustrate the impact of biases on job performance.
- Counters the argument that regulating hate speech makes it more powerful, especially in the context of law enforcement.
- Points out the danger of bigoted officers within police departments and their potential impact on public trust and safety.

### Quotes

- "Not being a bigot is kind of a bona fide job qualification for law enforcement."
- "There is no reason for an officer to oppose getting rid of bigots on police forces, except for one."
- "Bigots on police departments. They are a danger to the department."
- "I personally cannot think of anything that makes that kind of speech more powerful than giving it a badge and a gun."
- "Making it more powerful."

### Oneliner

Beau explains why law enforcement officers participating in hate speech should face consequences, linking it to public trust and safety, and challenges the argument against regulating such speech.

### Audience

Law enforcement reform advocates

### On-the-ground actions from transcript
- Advocate for regulations that hold law enforcement officers accountable for hate speech (suggested)
- Support initiatives to remove bigoted officers from police forces (suggested)

### Whats missing in summary

The full transcript provides a detailed argument against allowing law enforcement officers to participate in hate speech and stresses the importance of maintaining public trust and safety within police departments.

### Tags

#LawEnforcement #HateSpeech #Regulations #PublicTrust #Accountability


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk a little bit more
about some proposed regulations in Minnesota.
We talked about it recently and provided an overview
and detailed the opposition to the new regulations.
And since that video went out, I have
received a number of messages from people offering a rebuttal.
Most of them, in some way, shape, or form,
it was clear from their profile, they were in some way
associated with law enforcement.
If you missed that video, there is a standards board
in Minnesota.
And they want to make it to where
if a law enforcement officer is participating
in online hate speech, if they're participating
in a forum that is about hate, that their license
can be revoked and they can't be a cop anymore.
I think that's a good thing.
I mean, I just must be naive.
The three arguments that were made,
one is a First Amendment one, one is a free speech one,
which those are different.
The First Amendment is kind of a legal structure.
Free speech is a philosophical one.
And then one about power that's kind of interesting.
OK, so the First Amendment, they have a First Amendment right
to say these things.
What is the purpose of the First Amendment?
Not what does it protect, but what's its actual purpose?
The purpose is to protect the citizens, the public,
from Congress specifically, but the government as a whole.
That's the purpose of the First Amendment.
Which is a cop?
Are they the public?
Or are they the enforcement arm of government?
And as the enforcement arm, should
be held to a little bit of a higher standard.
When you are acting as an agent of government,
you don't really have a First Amendment right,
because you're on the other side of that.
You can say that they're doing it in their off time,
or whatever.
And that would then move more to the free speech argument.
Leave the legal structure and go to the philosophical one.
Free speech is not what a lot of people think it is.
Nobody's saying that these individuals can't be bigots.
They're saying they can't be a bigot and be a cop.
That's what they're saying.
Not being a bigot is kind of a bona fide job qualification
for law enforcement, because you have to be
able to treat people fairly.
And if you're bigoted, you can't.
Take this logic to any other public service.
Let's just say nursing.
If there was a nurse who was a part of an online group that
believed anybody over the age of 60 was just a useless person
and didn't deserve to really be here anymore,
because they're too old, should that nurse
be taking care of patients of that age?
Should they be a nurse at all?
Would you want them taking care of your family member?
Do you think they'd get the same quality of care?
Or do you think that that bias might show in their work?
Think about all of the different online groups
that you wouldn't want your kid's teacher to be a part of.
It's not that the individual can't be a part of this group.
They just can't be a part of it and be in public service,
because it undermines the public trust.
But let's take it even further.
Let's say, let's take a fast food restaurant.
Somebody working at a fast food restaurant
is discovered by their boss to be a part of an online forum
that encourages adding extra ingredients to food.
Should that person keep their job?
Probably not, right?
The arguments are bad.
Now let's get to the one about power,
because interestingly enough, this
was brought up by three people that I saw.
Like I saw the messages from three separate people.
The general idea is that when it comes
to speech of this sort, hateful, bigoted speech,
that you shouldn't regulate it.
You shouldn't enact laws against it.
You shouldn't try to prohibit it in any way,
because then it makes that speech more powerful,
becomes a forbidden fruit, because there's
a whole lot of people who became bigots because somebody told
them not to be a bigot.
I don't buy that at all.
But let's just take it for what it's worth.
The idea is that if you regulate that speech,
even in just a limited fashion for law enforcement,
if you regulate that speech, you're making it more powerful,
and therefore it's more of a draw.
Making it more powerful.
I personally cannot think of anything
that makes that kind of speech more powerful than giving it
a badge and a gun.
It doesn't hold up to scrutiny.
Here's the thing, since most of the people who
sent stuff like this had a thin blue line or whatever imagery,
I'm going to say this.
Bigots on police departments.
They are a danger to the department,
because their bias that would make them more likely to engage
in false arrest, excessive force, so on and so forth,
that then undermines public trust
and makes other officers more at risk.
I would say that it also obviously
endangers the public, because those biases could
lead to their false arrest, or the force being used on them
wrongly, or maybe even to an extreme, them being lost.
There is no reason for an officer
to oppose getting rid of bigots on police forces,
except for one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}