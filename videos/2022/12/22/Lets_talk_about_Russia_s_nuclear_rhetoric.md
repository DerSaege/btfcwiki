---
title: Let's talk about Russia's nuclear rhetoric....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wpXN2a1393g) |
| Published | 2022/12/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the Russian government engaging in unsettling rhetoric, making people nervous, especially the younger generation unfamiliar with Cold War tensions.
- Mentions Putin discussing the possibility of removing their policy against a first strike and commanders expressing a desire to use nuclear weapons.
- Explains the concept of Mutually Assured Destruction (MAD) in the context of nuclear weapons.
- Emphasizes that the key element for a successful first strike with nuclear weapons is surprise, which is why they wouldn't openly telegraph their intentions.
- States that the saber rattling and posturing from Russia are intended to unsettle the West and question their resolve.
- Notes that for Putin, resorting to nuclear saber rattling may inadvertently embolden the West by implying weakness in Russia's conventional forces.
- Predicts a similar pattern of rhetoric when tensions escalate with China in the future.
- Assures viewers that if there were genuine concerns about strategic arms, he'd make a video expressing those concerns.

### Quotes

- "It is saber rattling. It is rhetoric."
- "If you are going to attempt a first strike, do you know the number one thing you need for it to be a quote success? Understand there is no success when you're talking about nuclear weapons."
- "I wouldn't worry about that too much."

### Oneliner

Beau addresses Russian nuclear rhetoric, stressing it as saber rattling to unsettle the West and downplaying genuine concerns about strategic arms.

### Audience

Global citizens

### On-the-ground actions from transcript

- Stay informed on international relations and nuclear policies (implied)

### Whats missing in summary

Beau's engaging delivery and nuanced analysis are best experienced in the full video.

### Tags

#Russia #NuclearWeapons #InternationalRelations #SaberRattling #Geopolitics


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
rhetoric. We're going to talk about some things that are making people nervous and
they really shouldn't be. The Russian government has started engaging in a lot of rhetoric
where they appear to be thinking about the unthinkable. This has put some people on edge.
Again, this is mostly people who are younger because they weren't around during the Cold
War. This is normal. It's normal. The two pieces that people have asked about the most
is Putin talking about getting rid of their policy against a first strike and then some
public statements from commanders about the desire to use them. There is, when it comes
to nuclear weapons, there's that overwhelming concept of MAD, Mutually Assured Destruction.
If they launch, we launch, everybody launches, everybody has a bad day. If you are going
to attempt a first strike, do you know the number one thing you need for it to be a quote
success? Understand there is no success when you're talking about nuclear weapons. But
the number one thing you need if you want to attempt it is surprise. They won't telegraph
it. They're not going to come out and say, oh yeah, we're going to remove our prohibition
against first strikes. Why would they do that? It's saber rattling. It is meant to do exactly
what it's doing, which is put people on edge and make the West question their resolve.
Is it really worth helping Ukraine if it could lead to Putin being a madman and all of that?
When it's working for the most part, it's unnerving. People who are older, who lived
through the Cold War, they're like, yeah, okay, you know, wake me up if something launches,
or better yet, don't. So this isn't something I would worry about. It is saber rattling.
It is rhetoric. And it's posturing that is supposed to put the West on edge. The problem
for Putin is that he's still getting bad information. Having to constantly resort to nuclear saber
rattling, it doesn't put the West on edge. In many ways, it emboldens the West. Because
what it ends up doing is sending a very clear message of our conventional forces aren't
up to the task. We may have to go to the strategic level, which is the opposite message of what
he wants to send. He wants to send a position of strength. He wants to convey that strength.
The saber rattling doesn't do it. It doesn't work. I will go ahead and say in two years
or so, when tensions really start to heat up with China, expect the exact same thing.
It will happen. It's rhetoric. It's posturing. It's not something I would worry about. And
believe me, if anything was to occur on the strategic arms front that I was even remotely
concerned about, you'd get a video. That's one thing you don't have to send a message
about. Because if there is ever even the slightest bit of doubt or concern, I will express it.
I wouldn't worry about that too much. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}