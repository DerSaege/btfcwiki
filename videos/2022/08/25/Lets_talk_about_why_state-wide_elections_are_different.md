---
title: Let's talk about why state-wide elections are different....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JTLmeJ4UtWs) |
| Published | 2022/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responds to a viewer's inquiry about Senator McConnell's statement regarding candidate quality in Senate vs. House races.
- Views McConnell's statement as an unintentional admission of the importance of gerrymandering in House races.
- Explains the difference in candidate quality requirement between Senate and House due to gerrymandering.
- Points out how districts drawn during redistricting benefit the party in power, diluting opposition votes.
- Notes that district maps are often nonsensical, not based on population or geography, but manipulated for political advantage.
- Raises concern over how redistricting manipulations ensure party power rather than fair representation.
- Observes that gerrymandering is a longstanding issue in the U.S., with increasing impact on state political leanings.
- Mentions both Republican and Democratic parties' involvement in gerrymandering but suggests Democrats might be slightly less blatant.
- Implies that McConnell's slip of information about gerrymandering may hint at the Republican Party's heavy reliance on it.
- Concludes with a reflection, urging viewers to analyze the situation critically.

### Quotes

- "There's no districts. So that candidate really has to be of a higher quality."
- "They don't make any sense. They're not based on population. They're not based on geography. They are just random lines."
- "It's becoming more pronounced because for the Republican Party there are a lot of states where if there wasn't a very concerted effort to draw up favorable districts, well they wouldn't be purple states, they'd be blue states."
- "Understand the Democratic Party does also engage in this. They just seem to be a wee bit more fair while they're, you know, definitely still trying to give themselves an edge."
- "It's just a thought. Y'all have a good day."

### Oneliner

Beau dissects McConnell's unintentional admission on gerrymandering's impact on candidate quality and political manipulation in Senate and House races.

### Audience

Voters, political activists.

### On-the-ground actions from transcript

- Examine your state district maps to understand gerrymandering's impact (implied).

### Whats missing in summary

Deeper insights into the implications of gerrymandering and the need for fair redistricting processes.

### Tags

#Gerrymandering #PoliticalManipulation #FairRepresentation #Senate #House


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk a little bit more
about something that McConnell said, Senator McConnell.
After that last video, I got this message.
After your McConnell video,
I went and found the clip of him talking.
What did he mean when he said it was different
in a statewide race?
Why do the candidates need to be of a higher quality
for the Senate than in the House?
Just curious.
What did he mean?
I can't really speak to what Senator McConnell meant.
I can say how I took it.
It seemed to me to be an unintentional admission of how important gerrymandering is when it
comes to the House of Representatives.
See, in the Senate, it's the state.
There's no districts.
So that candidate really has to be of a higher quality.
I mean, to use his term, they really
do, because they have to be able to cross
over different demographic lines.
In the House of Representatives, there's districts.
And those districts are drawn up by whatever party is in power
at the state level when redistricting occurs.
That party tends to create districts
that benefit their party.
And what they end up doing is they dilute the voting blocks.
And so, let's say you have an area that would make sense to be a district all on its own,
like a suburb of a major city.
Well, if that suburb is likely to vote for the opposing party, well, they may just decide
to break that suburb up into two districts that would create a situation where those
people are outnumbered by other people in the district who would vote for the party
in power.
That's how I took it.
He might have meant something else, but I mean, that is the main difference.
When you're talking about national politics, the districts are important.
Pull up your state district maps and look at them.
They don't make any sense.
They're not based on population.
They're not based on geography.
They are just random lines, and it's because over the years, during the redistricting process,
the party in power will take a little bit of votes from over here and throw them into
this district, or maybe put some of the votes from that district into another to balance
things out, and the whole goal of this is to make sure their party maintains power.
becoming a real problem in the U.S. It's been a problem for a long time but it's becoming more
pronounced because for the Republican Party there are a lot of states where if there wasn't a very
concerted effort to draw up favorable districts, well they wouldn't be purple states, they'd be
blue states. It does happen. Understand the Democratic Party does also engage in
this. They just seem to be a wee bit more more fair while they're, you know,
definitely still trying to give themselves an edge. It's just not as
blatant. How about that? And the Democratic Party doesn't rely on it as
as heavily as the Republican Party seems to. So that's probably why that little bit of information
slipped out when McConnell was talking about that.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}