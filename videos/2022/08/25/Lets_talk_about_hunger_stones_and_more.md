---
title: Let's talk about hunger stones and more....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=adUmAUFMzOY) |
| Published | 2022/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of hunger stones as markers near rivers in Europe that indicate famine when visible due to low water levels.
- Mentions the current 500-year drought in Europe leading to various historical structures and artifacts becoming visible due to low water levels.
- Points out similar situations in other parts of the world like China, where ancient Buddhist statues have become visible due to low water levels in the Yangtze.
- Raises concerns about the increasing global population's impact on water resources and the potential effects of climate change.
- Emphasizes the importance of taking real action beyond campaign promises to address climate change effectively.
- Stresses the need for mobilization and concrete policies to combat climate change on a global scale.
- Urges acknowledgment of the reality of climate change and the necessity of immediate action to prevent further deterioration.
- Warns about the worsening effects of climate change if high emissions levels persist.
- Calls for awareness and proactive measures to address the environmental challenges faced globally.

### Quotes

- "We can't act like this isn't happening."
- "It's not just Europe. It's not just the US. It is a real issue."
- "We can't allow ourselves to look away on this one."
- "We need that mobilization."
- "As long as our emissions continue, it's going to get worse."

### Oneliner

Beau explains hunger stones, global drought effects, and the urgency of concrete climate action beyond promises.

### Audience

Climate activists, policymakers, communities

### On-the-ground actions from transcript

- Advocate for concrete climate policies (implied)
- Support initiatives for renewable energy transition (implied)
- Raise awareness about climate change effects (implied)

### Whats missing in summary

The detailed examples of historical structures and artifacts becoming visible due to low water levels and the potential consequences of ignoring the effects of climate change.

### Tags

#ClimateChange #GlobalDrought #HungerStones #RenewableEnergy #EnvironmentalAction


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about hunger stones
and things that aren't hunger stones,
but kind of act like hunger stones.
We're gonna do this because I got a question.
If you don't know what a hunger stone is,
they are markers and they're in Europe.
They're placed near rivers
and they were placed when the river was incredibly low.
Then when the river rises, they're submerged.
It was kind of a way to tell future generations,
hey, if the water level drops and you can see this,
well, you might have famine on the way.
A lot of the stones have less than encouraging messages.
Stuff like, if you see me, cry.
The question was basically, is this really that important?
Because water levels have obviously been this low before,
so why are people making a big deal about it?
Europe right now is, I want to say,
the most recent study I saw said
it's in the middle of a 500-year drought.
And it's not just the stones that are visible.
There's a bridge near Rome that was built around 50 AD, I think.
And you could always see a little bit,
but now it's pretty much all there.
The place known as Spanish Stonehenge is now visible.
There are barges and warships that had sank that are now visible.
It's not just the hunger stones.
Aside from that, in China, the Yangtze,
there were some Buddhist statues that they think are around 600 years old
that are now visible.
Water levels there are so low, they're
having issues with hydroelectric power now.
I think on the channel, we have talked about the Klamath, the Colorado,
the Rio Grande, and the Euphrates.
It's not just Europe.
It's not just the US.
It is a real issue.
Now, as far as it happened before, so it's not that bad,
keep in mind that these hunger stones are old.
We have a lot more people now.
A lot more people means we need more water.
So my concern is that this is kind of a way
of looking at the beginning effects of climate change
and saying, oh, this is just the new normal.
It won't be that bad.
No, it could get really bad.
We're making progress.
The Inflation Reduction Act has a huge, huge amount of money
being devoted to the climate issues.
And there are plans to switch over our energy.
The thing is, we have to make sure that that happens.
They can't be campaign promises.
They have to be administration policies.
They have to be things that actually occur.
What's happened so far, it's good, but it's not enough.
We need that mobilization.
And it's not just the US.
We can't act like this isn't happening.
Climate change is real, and we're starting to see it.
We can't allow ourselves to look away on this one.
We can't act like this isn't important
or that it'll fix itself.
As long as our emissions are high,
as long as our emissions continue,
it's going to get worse.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}