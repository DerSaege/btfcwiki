---
title: Let's talk about some weird legislation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tj7IcScwQvs) |
| Published | 2022/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analysis of strange legislation changing definitions regarding UFOs.
- Definitions include transmedium objects and unidentified aerospace undersea phenomena.
- Legislation requests reports on unidentified aerospace undersea phenomena activities.
- Involvement of various government agencies in the investigation.
- Two possible interpretations: searching for advanced technologies or believing in aliens.
- Speculation that the program may be a cover for investigating near-peer technologies.
- Emphasis on the idea that advanced technology can be mistaken for magic or aliens.

### Quotes

- "Anyway, it's just a thought."
- "This is about aliens, just so you know."
- "Any sufficiently advanced technology is indistinguishable from magic or aliens."

### Oneliner

Beau dissects legislation on UFOs, questioning if it's a cover for advanced tech surveillance or genuine alien belief.

### Audience

Curious individuals, UFO enthusiasts, skeptics

### On-the-ground actions from transcript

- Stay informed about government legislation and activities related to UFOs (implied)
- Engage in community dialogues about advanced technology and potential extraterrestrial phenomena (implied)

### Whats missing in summary

The full transcript provides a deeper dive into the possible motivations behind legislation on UFOs and offers insights into the intersection of advanced technology and alien phenomena.

### Tags

#UFOs #GovernmentLegislation #AdvancedTechnology #Aliens #Speculation


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about something weird.
One of y'all sent me something weird,
which that in and of itself isn't unusual.
Y'all send me weird things all the time,
but this is strange.
It's some legislation,
came out of the Senate Select Committee on Intelligence,
and it changes some definitions.
The message was, hey,
is there some context to this that I'm missing?
So what we're going to do is we're going to go over the definition changes,
and then we're going to see if we can find the context.
At the end, you will be left with two options.
This is about aliens, just so you know.
Okay, so the definition changes.
The one that really matters,
I will go ahead and tell you that transmedium objects,
because I think this term is going to come up.
That is an object that can transition between space and our atmosphere,
or our atmosphere, and going underwater.
The term unidentified aerospace undersea phenomena.
This is the new term for UFO.
Okay, the term means airborne objects that are not immediately identifiable,
transmedium objects or devices,
and submerge objects or devices that are not immediately identifiable,
and that display behavior or performance characteristics,
suggesting that the objects or devices may be related to the objects or devices
described in subparagraph A or B.
And does not include temporary non-attributed objects
or those that are positively identified as man-made.
Aliens.
Okay, yeah, I mean that's weird, right?
I mean that's strange and yeah, okay.
So the question is, is there any context to this?
Is there any other explanation than the Senate Intelligence Committee
drafting these definitions in legislation that's basically asking for reports
from a task force designed to look at UFOs?
Maybe.
In the reports, you know, in this legislation,
it lays out what is supposed to be in the reports.
And item 8, this is on page 99 if you're actually going to read this,
an assessment of any activity regarding unidentified aerospace undersea phenomena
that can be attributed to one or more adversarial foreign governments.
Item 9, identification of any incidents or patterns regarding unidentified
aerospace undersea phenomena that indicate a potential adversarial foreign
government may have achieved a breakthrough aerospace capability.
And it goes on.
Now, who's involved in this?
The core group, as outlined in the legislation,
is the Central Intelligence Agency, the National Security Agency,
the Department of Energy, the National Reconnaissance Office,
the Air Force, the Space Force, the Defense Intelligence Agency,
and the National Geospatial Intelligence Agency.
And a whole bunch of people just learned about spy agency they'd never heard of.
Okay, so at the end of this, you kind of have two options.
Either this is a white mask for a program that is really geared towards looking for
near-peer technologies and it's being kind of cloaked in the idea that they're looking for UFOs.
Or there are people, I believe this was unanimous,
people on the Senate Select Committee on Intelligence who think there are aliens.
You can go however you want with it.
My gut reaction tells me that they have it framed this way because then they're not
really conducting defense surveillance.
This is a scientific endeavor and this provides them a thin veneer to investigate this kind of stuff.
But they're really looking for foreign powers that may have developed an advanced technology.
So they will investigate things that people see that aren't readily identifiable and go from there.
It's important to note that any sufficiently advanced technology is indistinguishable from magic or aliens.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}