---
title: Let's talk about WHO changing names and changing as a person....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GPGj9ktjS-w) |
| Published | 2022/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The World Health Organization is updating its guidance on naming things, sparking a question on name changes.
- Beau shares his personal growth journey, transitioning from a racist and angry person to someone striving to be better.
- Mentioning a YouTube streamer, Streamer X, who helped him move away from the far right ideology.
- Beau acknowledges his anger towards liberal changes and credits Streamer X for introducing him to new perspectives.
- Addressing a viewer's question on the World Health Organization changing the name of Monkeypox.
- Beau commends the viewer for their positive change in attitude following a wake-up moment post-Capitol events.
- Explaining the WHO's rationale behind avoiding names with cultural or ethnic references to minimize negative impacts.
- Providing examples of naming changes to reduce stigma and harm, such as renaming variants from geographical to clade designations.
- Emphasizing that the naming adjustments aim to prevent harm and not cater to people's feelings.
- Beau clarifies that the renaming efforts are not about "woke garbage" but about reducing stigma and harm.

### Quotes

- "What possible reason is there for who, the World Health Organization, to change the name of Monkeypox?"
- "It's not woke garbage. There's a method to it."
- "They're trying to remove the stigma, so that doesn't happen anymore."

### Oneliner

Beau explains the WHO's naming changes to reduce stigma and harm, not catering to feelings, but promoting a methodical approach.

### Audience

Online viewers

### On-the-ground actions from transcript

- Understand the reasoning behind naming changes by the World Health Organization (implied).
- Support efforts to reduce stigma and harm through updated naming conventions (implied).

### Whats missing in summary

Beau's personal anecdotes and journey towards positive change.

### Tags

#NameChanges #WorldHealthOrganization #StigmaReduction #Understanding #PersonalGrowth


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about what's in a name
and why names change sometimes.
If you missed the news, the World Health Organization
is kind of putting out new guidance on how to name things.
And it prompted a question.
This is a long lead-in to the question,
but it kind of leads to something that's...
I think it's interesting.
So I'm going to read the whole thing.
I used to be a really horrible person, really racist,
really angry, and just really bad.
I'm trying to do better, and my life has gotten better.
I'm happier, but I still have strong reaction
to liberal changes for no reasons.
I guess I'm just an angry person.
It was actually...
there's a name of somebody who's on YouTube here.
I would consider this a compliment,
but I don't know that this person would.
So we're just going to say Streamer X.
It was actually Streamer X who got me out of the far right.
But I was still angry as I'll get out all the time,
because he's still just arguing and yelling all the time.
But his show introduced me to you and others.
You do a good job of explaining woke garbage in real terms.
Even if I still think it's dumb, I understand the point.
What possible reason is there for who, the World Health
Organization, to change the name of Monkeypox?
OK, so first, anytime somebody sends me
a message that says I used to be a horrible person,
I tend to assume that they are concern trolling.
And I look at their social media.
And normally I found out, no, you're still a horrible person.
Not the case this time.
You scroll through the social media,
and you find that there's a specific moment around the time
something happened at the Capitol
that there's a change in tone.
Almost like that was a wake up moment for them.
So good for you.
Now, to answer the question, what possible reason
does the World Health Organization
have for changing the way they name things?
You.
Well, the person you used to be.
Because if you go back before the 6th, what did you call it?
What did you call coronavirus?
China, right?
The Wuhan.
During that same period, and you can look it up,
there's a bunch of reporting from the BBC from everywhere
that talks about how there was an uptick in violence
against Asian people.
The new guidance that the World Health Organization
is putting out.
The idea is to avoid naming anything
with a reference to a cultural, social, national, regional,
professional, or ethnic group, and to minimize
the negative impact on trade, travel, tourism,
or animal welfare.
So variants, like with monkeypox,
I guess they were Congo basin and West African,
they are now clade one and two.
And eventually, they're going to come up
with a whole new name for it.
It's not just this last one.
It's not just what happened with COVID.
There are tons of examples.
When swine flu, it hit that industry really hard.
So the idea, if I'm not mistaken,
and I understand the guidance, which is iffy to be honest,
it seems like they want to incorporate
the symptoms and the pathogen in the name.
If I understood it correctly, that's
how they want things named.
They're trying to get rid of the stigma that accompanies
a bad naming convention.
So that's the reason.
It's not, it doesn't have anything
to do with people's feelings.
It has to do with the fact that people got hurt,
and they're trying to remove the stigma,
so that doesn't happen anymore.
I mean, I get it.
Those who are reluctant to challenge the stigma
get it, those who are reluctant to change,
those who have those reactions.
And especially if you're kind of conditioned
to have an anger reaction to changes.
I can see how it might be bothering,
but it's not woke garbage.
There's a method to it.
And the idea is to limit harm based on your social media
from the 6th on.
Seems like it might be something that you might not even
think is dumb.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}