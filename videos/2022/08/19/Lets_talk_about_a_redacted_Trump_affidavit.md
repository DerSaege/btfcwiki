---
title: Let's talk about a redacted Trump affidavit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d7kTklihAi4) |
| Published | 2022/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the process of a document presented by the Department of Justice to a judge to obtain a warrant.
- Mentions the media's interest in unsealing the document related to the investigation.
- Indicates that the investigation is in its early stages, with potentially wider implications than initially thought.
- Notes the presence of substantial grand jury information in the affidavit.
- Points out the desire to protect witnesses involved in the case.
- Describes the redaction process undertaken by the Department of Justice to protect sensitive information.
- Speculates on the limited new information that may be revealed upon the document's release.
- Emphasizes that the document presents only the Department of Justice's perspective.
- Raises the possibility of the judge releasing the entire document despite national security concerns.
- Expresses the lack of positive outcomes for Team Trump based on the disclosed information.

### Quotes

- "There are a lot of people asking, how can it be in the early stages? They have the documents, and they have all of this information, right? Yeah, that's the bad news for Team Trump."
- "The length of it alone though, because it has been described as being a long document, that's kind of telling in and of itself."
- "There's no counter to it. You are getting one perspective on it and it is a document designed to get a warrant."
- "So at the end of this, there's not a lot of good news for Team Trump."
- "But there's also not a lot for the public, at least not yet."

### Oneliner

Beau explains the ongoing process involving a sealed DOJ document related to the investigation, hinting at potential wider implications and limited positive outcomes for Team Trump.

### Audience

Observers, DOJ followers

### On-the-ground actions from transcript

- Contact media outlets to advocate for transparency in the release of relevant documents (suggested).
- Stay informed about developments in the investigation and subsequent document release (exemplified).

### Whats missing in summary

Insights into the potential consequences of the investigation and document release.

### Tags

#DOJ #Investigation #Transparency #Trump #Affidavit


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Trump, DOJ, that affidavit, redaction,
everything that's going on, and kind of look at the process,
what we're likely to see at the end of it,
and anything we picked up during the attempt to get it unsealed.
If you don't know what I'm talking about, there's an affidavit,
and this document is the document that the Department of Justice presents to a judge,
and it lays out their case, kind of.
It explains why they think they need a warrant,
and the judge makes the determination based on that.
In this case, the judge felt that a warrant was appropriate.
Now, the warrant has been unsealed.
This document hasn't been.
The media wants to see it.
They filed suit to get it unsealed.
Trump is saying that he wants it unsealed,
but I would point out that his lawyers, they didn't join the suit.
So take from that what you will.
In the process of the media outlets trying to gain access,
there was some information that was picked up.
First is that DOJ is saying that this investigation is in the early stages.
There are a lot of people asking, how can it be in the early stages?
They have the documents, and they have all of this information, right?
Yeah, that's the bad news for Team Trump.
If this is the early stages of the investigation, that's not good news,
because they may be looking at something wider in scope
than what's detailed in the warrant.
Now, we also found out that the affidavit contains substantial grand jury information.
For the document to contain substantial grand jury information,
that means the grand jury has a substantial amount of information.
Not good news for Team Trump.
They also said that they want to protect several witnesses.
Not ideal, okay?
So as far as the proceedings, there wasn't a lot of silver lining for Trump world.
Now, the flip side of this is that the judge seems inclined to, well,
to release at least parts of this document.
The Department of Justice doesn't want it released.
The judge has given DOJ like a week to go through and make redactions.
When you see those documents online and it's just, you know, it's all blacked out,
those are redactions.
So what's going to happen here is the Department of Justice is going to go through,
and they're going to block out names of witnesses,
things that witnesses said that might be used to identify them.
Any electronic or video surveillance that they might have,
the information from the grand jury,
any information obtained via a subpoena that Trump doesn't know about concerning records,
stuff like that.
What they're going to do is they're going to go through and try to make sure that their
sources, their means and methods are safe and don't end up, you know, in a club in Florida.
If somebody had done that to begin with,
I'm willing to bet that a lot of this wouldn't be happening right now.
So I personally find that aspect of it very poetic.
Now when the document comes out, are we going to learn anything new?
If they do it right, if they redact the document properly, not much.
There will be stuff that can be inferred, but more than likely,
there's not going to be a lot of groundbreaking stuff in it.
The length of it alone though, because it has been described as being a long document,
that's kind of telling in and of itself.
They have a lot of stuff that they presented as part of their case to establish cause to
get the warrant.
So they have done a lot of groundwork and now they actually have the documents,
but also they're saying that's the beginning stages.
You can take from that what you will.
One thing that I do want to point out about these documents is that they present the Department
of Justice's point of view.
They present DOJ's point of view and that is it.
There's no counter to it.
You are getting one perspective on it and it is a document designed to get a warrant.
So this is one of those things that what's left, take it with a grain of salt.
The important stuff, they're not going to want released.
So it will be redacted if the judge goes with what the Department of Justice wants.
Now the judge does have the option of just releasing the whole thing.
That seems really unlikely given the national security overtones to it though.
So that's what happened.
That's what you can expect when and if this document is released later.
But the key elements, substantial grand jury information, several witnesses, beginning
stages, none of that's good.
Now at the same time, the judge, given the situation, the judge may see a substantial
public interest in the public knowing as much as possible without derailing the investigation.
So DOJ could go through and redact like a wish list and then the judge ask them to explain
portions of it.
And if the judge doesn't like DOJ's explanation, well that part may not be blacked out.
So at the end of this, there's not a lot of good news for Team Trump.
The information that we kind of got to see or got to hear about.
But there's also not a lot for the public, at least not yet.
And we don't know how much is going to be released in the end.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}