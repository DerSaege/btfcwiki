---
title: "Let's talk about an update on the Vanessa Guill\xE9n case...."
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=darQhzIQMNE) |
| Published | 2022/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on the Vanessa Guillen case, mentioning the involvement of viewers in pressuring for results.
- Vanessa Guillen, a 20-year-old, was killed at Fort Hood in 2020, leading to public pressure, legislation, and reforms.
- Vanessa Guillen's family is pursuing a $35 million lawsuit for wrongful death and alleging military negligence.
- Department of Defense might argue against damages citing a law, but exceptions have been made.
- The case holds significance due to ongoing harassment and violence issues within the Department of Defense.
- Despite some progress, the problem is far from resolved, warranting continued attention.
- Public pressure has been instrumental in bringing about changes and maintaining focus on the issue.
- $35 million is not justice but a step in the right direction, potentially prompting further reforms.
- Financial penalties like this lawsuit can drive change and encourage more progress.
- Beau stresses the importance of following the case to understand consequences when progress stalls.

### Quotes

- "Public pressure really did create change."
- "Justice is her still being here."
- "Financial penalties tend to promote change."

### Oneliner

Beau provides an update on the Vanessa Guillen case, stressing the importance of ongoing attention to address harassment and violence issues within the Department of Defense, showcasing how public pressure can drive change.

### Audience

Advocates, activists, viewers

### On-the-ground actions from transcript

- Contact oversight, Senate Armed Services Committee to build pressure (implied)
- Stay informed and follow cases of injustice within institutions (suggested)

### Whats missing in summary

The emotional impact on Vanessa Guillen's family and the broader implications of seeking justice through legal means. 

### Tags

#Justice #VanessaGuillen #PublicPressure #DepartmentOfDefense #Reform


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to provide an update on a case
that we have been following on this channel
for quite some time.
Continuing to monitor it because I know when it happened,
a whole lot of y'all got personally involved,
contacting oversight,
contacting the Senate Armed Services Committee,
and really helped build a lot of pressure
that has put together,
it's achieved results.
So today we're gonna provide a little update
on what's going on in the case of Vanessa Guillen.
And for those that don't know,
back in 2020, she was killed at Fort Hood.
She was 20 years old.
Since then, because of public pressure,
there has been legislation,
there has been reform,
there has been a lot of movement in this regard.
Because of a ruling,
and I wanna say it was the Ninth Circuit,
her family is open to pursuing a lawsuit.
And they have filed or will file a $35 million lawsuit
basically for wrongful death,
along with a lot of other things,
alleging that the military had a duty
to stop this from happening,
which seems apparent to me.
How this suit is going to play out,
because undoubtedly the Department of Defense
is going to try to say
that her family's barred from seeking damages
because she was at work at the time,
and anyway, there's a law.
However, there have been exceptions made in this,
and they're going to try to fight this.
The case itself seems pretty apparent,
but there's the legal aspects of it
that have to play out first.
Now, the reason I like following this case
is because one, it's important.
This is an ongoing issue.
This type of harassment that leads to violence
is an ongoing issue within the Department of Defense.
Major strides, you know, sure,
but it's nowhere near fixed,
and I don't think it's a good idea to act like it is.
So I think it's important to follow the case
for that reason and to remind people
of what the consequences are
when the strides aren't maintained,
when there isn't forward progress being made.
Aside from that, this is also one of those situations
where public pressure really did create change.
Thought changed, thought shifted.
This needs to be fixed,
and there was an outcry loud enough
to get the attention of those in power,
and it's bringing about change.
Now, $35 million, it's not justice.
Justice is her still being here,
but it's a step in the right direction,
and when you are talking about financial penalties,
that tends to promote change,
and it may encourage yet another round of reforms
and more forward progress.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}