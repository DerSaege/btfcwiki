---
title: Let's talk about Dems, Trump, and the boy who cried wolf....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fAjzm13tyqk) |
| Published | 2022/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the credibility of the Democratic Party and the "boy who cried wolf" analogy.
- Analyzing the traditional story of the boy who cried wolf.
- Suggests treating every threat as real to prevent negative consequences.
- Expresses skepticism towards the traditional moral of the story.
- Questions the perspective from which the story is told and criticizes the villagers.
- Proposes telling the story from the perspective of the wolf, questioning the villagers' actions.
- Argues that investigating real threats is more critical than the traditional moral of not lying.
- Concludes that the current situation involves the pursuit of the wolf, unlike the traditional story.
- Encourages a different interpretation of the tale, focusing on pursuing and investigating threats.
- Emphasizes the importance of taking action when faced with real dangers.

### Quotes

- "Treat every threat as if it's real. Otherwise, the little boy is going to get eaten."
- "Maybe he can get that little human, but the little human starts screaming again."
- "Make sure that if there is a wolf looking to attack your capital or the flock, you actually investigate."
- "Even in your version of that story, there is a wolf at the end."
- "It's just a thought."

### Oneliner

Beau questions the credibility of the Democratic Party using the "boy who cried wolf" analogy, encouraging treating every threat seriously to prevent negative outcomes.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Investigate potential threats seriously (implied)
- Take action when faced with real dangers (implied)

### Whats missing in summary

The full transcript provides a critical analysis of the traditional "boy who cried wolf" story, offering a different perspective and urging individuals to take real threats seriously.

### Tags

#DemocraticParty #Credibility #BoyWhoCriedWolf #Threats #Investigate


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk a little bit
about the credibility of the Democratic Party for a moment.
We're gonna do this because I got a message
and it references something that I've always
like had an issue with,
like as far as like how the story plays out.
So we're kind of gonna go through that.
And we're gonna talk about whether or not
the Democratic Party is the boy who cried wolf.
Because here's the message.
Beau, let's talk about your childlike naivete.
I wish there was a way to only subscribe
to your foreign policy stuff
because you're so much of a Democrat
that you don't even realize the Dems
with this classified stuff
have become the boy who cried wolf.
Scandal after scandal, and there's no wolf.
This will be the same.
Okay, all right.
Well, let's talk about the boy who cried wolf.
Real quick, we'll run through a traditional telling of it.
And then let's actually look at the story, okay?
So the story is this village sends this boy up
to guard the flock, right?
And he's up there and he cries out wolf.
And the village comes running up.
And in some versions, they pat him on the back
because they think that he scared the wolf away.
Okay, and then they go back down the hill.
Little boy's up there still, he cries out wolf.
They come running up.
There's no wolf.
They look around, they don't see a wolf.
They go back down the hill.
Third time, he cries wolf.
They run up, there's no wolf.
They go back down the hill.
Fourth time, he cries wolf.
And they don't believe him.
They don't show up.
And a wolf eats him.
Okay, so the traditional moral that goes along
with this story is don't lie.
Don't be the boy who cried wolf.
I would suggest a complementary moral is treat every threat
as if it's real.
Otherwise, the little boy is going to get eaten.
And I mean, that seems like a bad thing to me, you know.
But see, admittedly, I'm a professional paranoid.
Humpty Dumpty didn't fall, he was pushed.
So I have another issue with this story.
Who's telling it?
The village, right?
It's not the little boy because he's not here anymore.
This story comes from the people
who admittedly let a little boy get eaten.
I mean, set aside the fact that in the traditional telling,
there is a wolf at the end of it,
which seems to be a part of the story everybody seems to miss.
But when that story is told, you're taking the word
of the people who let the little boy get eaten.
Let's look at this a different way.
You get a call and you're asked to come out
and try to figure out what happened.
And you show up, you're up on top of that hill, the open area,
and there's what used to be a little boy.
And there's the field and the flock and the tree line,
the villages down the hill.
And the villagers tell you, well, he kept crying wolf,
but there was no wolf.
I mean, the little boy would demonstrate otherwise.
I don't know why you would buy their version of events.
Doesn't add up.
Rather than telling that story from the perspective
of the people who let a little boy get eaten,
which I think today we would call like woeful negligence
or something, let's tell the story
from the perspective of a wolf.
So the wolf, he sees that flock and he wants that flock.
So he leaves the tree line and he goes out into the field.
And then this human starts screaming, probably scared him.
So he ran back to the tree line.
And then all these bigger humans show up
and they stand around for a little bit.
And then they go back down the hill.
So he's like, okay.
He goes back out.
Maybe he can get that little human,
but the little human starts screaming again.
So he runs off scared to the tree line,
watches the people come up.
Those people, they're not really looking for him, right?
They're not actually out there investigating to see
if he's in the tree line.
They're just kind of looking around.
They're not actually pursuing him.
I mean, maybe he gets a little bit more bold.
He hasn't suffered any consequences for his actions.
So the wolf progresses to go a little bit further
the next time and then takes off
when he sees the people coming.
And then on that last time,
well, he's right up on that little boy.
Little boy's making all that noise.
But just like all the times before,
nobody's stopping him.
So he goes ahead and eats the little boy.
That seems a whole lot more likely, right?
I mean, if you're not taking the word of
the people who let the little boy get eaten.
I mean, it seems like the real moral of the story
is to make sure that if there is a wolf
that's looking to attack your capital or the flock, sorry,
that you actually investigate.
I mean, that's the way I would look at it.
But I mean, I don't know.
It's just a children's story.
I don't think that this is a boy who cried wolf scenario
other than the fact that this time
the wolf's being pursued.
Even in your version of that story,
there is a wolf at the end.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}