---
title: Let's talk about North Dakota and a tale of two candidates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ImPiNq-PY7s) |
| Published | 2022/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of a U.S. House of Representatives race in North Dakota with two candidates.
- Republican incumbent vs. Democratic challenger dynamics until independents started showing up.
- Mention of Karl Mundt, a former Miss America, as an independent candidate with celebrity status.
- Details on Mundt's background: Brown and Harvard Law graduate, focused on racial justice, equality, volunteer work.
- Media coverage focusing on Mundt's Miss America status rather than her platform.
- Mundt's platform still being developed but leaning centrist and independent.
- Potential impact of Mundt's name recognition and celebrity status on the race dynamics.
- Speculation on Republican women in red states possibly voting for an independent like Mundt over a Democrat due to party loyalty.
- Prediction that Mundt's candidacy could alter the dynamics of the race and potentially influence future political strategy.
- Beau's closing thoughts on the significance of watching this race unfold.

### Quotes

- "Republican women who do not want to deny their daughters options, that's a pretty strong motivator."
- "This is probably going to be somebody who's going to kind of level the race."
- "Regardless of outcome, this is probably something we need to watch."

### Oneliner

Beau examines a U.S. House race in North Dakota with Karl Mundt, a former Miss America, potentially altering dynamics and political strategies.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Monitor the U.S. House race in North Dakota for potential shifts in dynamics and political strategies (implied).

### Whats missing in summary

Further insights on the potential implications of independent candidates in reshaping political landscapes. 

### Tags

#USPolitics #HouseRace #NorthDakota #IndependentCandidate #PoliticalStrategy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a U.S. House of Representatives race
and a tale of two candidates.
So the race in question is one that I know everybody was following very, very closely.
The U.S. House of Representatives race in North Dakota.
Okay, maybe most people weren't actually paying attention to this one.
Because up until very recently, the dynamics were exactly what you would expect.
Republican incumbent, red state, Democratic challenger.
But see, over the last few days, something else has happened.
Seeing those static dynamics, independents are starting to show up.
Now, you might have heard about one of them because they're in the media,
because they already have celebrity status.
Karl Mundt, former Miss America.
And sure, that's interesting. Definitely going to change the dynamics.
But let's talk about a different candidate.
One who graduated from Brown and Harvard Law,
got an award for client-centered representation with a focus on racial justice and equality,
put in like a thousand hours of pro bono work,
volunteered with Children's Miracle Network Hospitals, Make-A-Wish, the USO,
just tons of volunteer activity,
and said that one of the catalysts for deciding to run was the overturning of Roe,
and that they don't believe that government should be in, you know, your bedroom.
Here's the thing. It's the same candidate.
That's Karl Mundt.
But what do you think the media is covering?
Generally speaking, they're talking about the fact, former Miss America.
That's the headline because it's American politics.
Now, I do want to say I haven't really examined her platform
because she doesn't look like she really has one fleshed out yet.
Still in the process of gathering signatures to get on the ballot and all of that stuff.
There's general information, kind of looks centrist.
Gone out of her way to say she's not a Republican or a Democrat.
She's a person, not a party, you know, normal independent stuff.
But here's the reason I think this is going to be interesting.
Because she does have name recognition.
She does have that celebrity status.
And then when people look into her, they would find that background.
Right? Then I want you to think about when we were talking about women in red states.
And what they might be willing to do because of Roe.
For a whole lot of them, I bet that'd be a whole lot easier
if it was an I rather than a D.
Those Republican women who do not want to deny their daughters options,
that's a pretty strong motivator.
We saw that in Kansas.
They very well might walk into that booth and vote for an independent before they would vote for a Democrat,
simply because of party loyalty in that ingrained red team, blue team fight.
I think this entry, assuming she gathers her signatures, gets on the ballot,
which I don't think that would be hard.
I want to say in North Dakota, you need like a thousand signatures or something.
I have a feeling that this is probably going to alter the dynamics.
And it won't be a normal spoiler third candidate.
This is probably going to be somebody who's going to kind of level the race.
At the end, I think they'd end up, she'll end up taking more votes away from the Republican Party
because of the Republican women who may have decided to vote another way.
But regardless of outcome, this is probably something we need to watch,
because I have a feeling it might shape future political strategy.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}