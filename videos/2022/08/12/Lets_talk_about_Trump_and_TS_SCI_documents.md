---
title: Let's talk about Trump and TS/SCI documents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L9adxOwl5QA) |
| Published | 2022/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Justice recently searched Donald J. Trump's home to recover documents, including those labeled TSSCI, suggesting a violation of the Espionage Act.
- TSSCI stands for Top Secret Sensitive Compartmented Information, representing highly sensitive material that requires special protection.
- Former military and intelligence personnel on social media are making statements about the seriousness of mishandling classified documents, contrasting them with the reality of consequences.
- Trump claims the recovered documents were declassified before he left office, but the process of declassification is not solely at the president's discretion.
- Beau criticizes Trump's actions for potentially compromising national security and undermining the entire classification system of the US.
- Storing classified documents in an unsecure location by the pool raises concerns about the disregard for proper handling and protection of sensitive information.
- Beau warns Trump's allies to reconsider their support, as standing by him after this incident could jeopardize national security and military strength.
- Beau urges for a stop to the inflammatory rhetoric and baseless claims that could incite harmful actions from individuals influenced by political narratives.

### Quotes

- "These are big secrets."
- "Trump's actions undermine the entire classification system that the US has."
- "That's wild."
- "Y'all have a good day."

### Oneliner

The Department of Justice's search for classified documents at Trump's residence raises concerns about national security and the mishandling of sensitive information, urging a reevaluation of political allegiances. 

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Reassess political allegiances for national security (implied)
- Stop spreading baseless claims and inflammatory rhetoric (implied)

### Whats missing in summary

The detailed breakdown and analysis of the consequences of mishandling classified information and the potential impacts on national security. 

### Tags

#Trump #NationalSecurity #ClassificationSystem #EspionageAct #PoliticalAllegiances


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about the ongoing saga
related to the former president of the United States,
Donald J. Trump, and his adventures and misadventures
with the Department of Justice.
Recently, the Department of Justice searched his home.
The speculation was that it was about an attempt
to find some documents.
His allies said that this was baseless.
OK?
Reporting is now suggesting that among the documents
recovered at his home during the search
include documents that had the label TSSCI.
We will get to what that means in a minute.
Now, that is in addition to other classified documents.
But we're just going to focus on that one
when we talk about it.
It is also being reported that the warrant indicates
that one of the violations that the Department of Justice
may be looking into is a violation of the Espionage Act.
So, TSSCI.
The media is going to be talking about classifications all day,
I'm sure.
This will be the one time the media understates something.
They'll go through the various types.
What you need to know is that TS, top secret.
OK?
This is incredibly sensitive material to begin with.
SCI is kind of a modifier.
Sensitive, compartmented information.
This is a batch of secrets that is so top secret
they can only be viewed in a room specifically designed
to protect secrets.
There are a lot of formers on social media.
Formers.
Former military, former intelligence.
And they're making these statements about,
you know, what would happen if I had done this.
First, let's be real.
You would never be able to do this.
There are too many safeguards in place to stop you.
This wouldn't happen.
And if it did, it would be something
that was very intentional and required a lot of work.
But the statements to me, they're cute.
They're funny.
Because it's stuff like, you know, if I did this,
they would have already arrested me.
No, if you did this, you'd be in the basement somewhere
being interrogated until you talked.
And they would not hold anything back.
I'd be looking at 10 to 20.
No, if you took SCI documents and were given the opportunity
to return them, elected not to, and, you know,
went about your day possibly meeting with foreign nationals,
no, you wouldn't be looking at 10 to 20.
10 to 20 would be what the groundskeeper tells your family
when they go to visit you.
Row 10, plot 20.
There's no way that the media is going to frame this properly.
These are big secrets.
Now, the president, I guess, the former president, I guess,
is now saying that these documents were declassified.
He declassified them before he left office.
Okay, maybe, sure, that's possible.
It's worth noting that that process doesn't just happen
because the president thinks it does.
It's not just something that he can think,
oh, well, these are no longer classified.
It doesn't work like that.
His allies who called this baseless,
who called the search baseless and said it was a political job
and all of this stuff and riled up people,
they need to take a look at themselves right now,
and you need to figure out where you want to be
because this is serious.
This isn't a joke.
These are documents related to national security.
This is stuff spy movies are based on.
If you choose to stand beside this man politically after this,
you never get to talk about supporting the troops again
because you're putting them at risk.
You never get to talk about wanting a strong country again
because you're weakening it.
You never get to talk about how diversity training
is weakening the military
because nothing weakens it more
than not being able to protect its secrets.
Trump's actions, regardless of how this shakes out
as far as criminal liability for him,
his actions undermine the entire classification system
that the US has
and is going to require a massive reworking
of how classified material is handled
simply because he apparently wanted to store some of them
in a storage room next to the pool.
Next to the pool.
You know, chlorine, tablets,
TSSCI documents right next to it.
That's wild.
That's wild.
Some of the documents that were recovered from his residence
were in a storage room by the pool.
Call me crazy, but I don't think that's really a secure location.
Even if he, you know, blinked and nodded
or whatever he thinks needs to be done to declassify something,
even if he did that beforehand,
that's probably not an appropriate place
to store that stuff.
This is going to be ongoing.
This is really bad news for the former president.
It is even worse news for his allies
because you're going to have to make a choice.
I would stop with the rhetoric.
It's already out of hand.
There are already people doing things
that they probably wouldn't be doing
if people weren't riling them up with baseless claims.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}