---
title: Let's talk about Pennsylvania, Perry, and subpoenas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2pguh5L8ESM) |
| Published | 2022/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Several Pennsylvania state House and Senate Republicans were visited by the Federal Bureau of Investigation or given subpoenas.
- Some members were explicitly told they were not targets of the investigation.
- Scott Perry stated that his lawyers were informed he's not a target and he's cooperating.
- The Department of Justice, Office of Inspector General (OIG) is running the investigation.
- Different labels like witness, person of interest, subject, target have fluid meanings in investigations.
- The status of individuals in investigations can change as new information emerges.
- The investigation is likely related to the fake electors scheme or the pressure campaign against Pence.
- While current coverage focuses on classified material investigation, there are other ongoing investigations.
- People may be involved in multiple aspects of the investigation with overlapping connections.
- The Department of Justice is continuing its investigations despite political claims.

### Quotes

- "Different terms like witness, person of interest, subject, target have fluid meanings in investigations."
- "People may believe they aren't targets but could end up as targets as investigations progress."

### Oneliner

Several Pennsylvania Republicans visited by FBI, some not targets; DOJ investigation continues with fluid statuses, potential ties to fake electors scheme or Pence pressure campaign.

### Audience

Legal Observers

### On-the-ground actions from transcript

- Stay updated on the developments in ongoing investigations (implied).
- Stay informed about the different terms used in investigations and their fluid meanings (implied).
- Cooperate fully with legal processes if involved in any investigations (implied).

### Whats missing in summary

Full context and detailed explanations are missing from the summary.

### Tags

#Pennsylvania #FBI #DepartmentOfJustice #Investigations #LegalProcess


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
Pennsylvania and what's going on up there as the mystery deepens and the federal government's
operations continue to roll on. So reporting is saying that several members of the state
House and Senate, all Republicans, were visited by the Federal Bureau of Investigation or they
were given subpoenas. Now it's interesting to note that some of them were apparently
explicitly told that they were not targets of the investigation. In a related development,
Scott Perry has said in a statement that his lawyers were told that he's not a target of the
investigation and that he's directed his lawyers to cooperate, make sure the feds can get whatever
they need as long as it's not protected by speech and debate clause, you know, stuff like that.
All of this tracks, remember when we talked about it, I think yesterday, I said it was weird
because it appears that this investigation is being run by the Department of Justice, OIG,
the Office of Inspector General. Perry never worked for DOJ, so they would be looking for
somebody else. So it's unsurprising that he's not a target. This is a good point to bring this up,
though, because while in this case these labels won't necessarily change, there are different
terms, witness, person of interest, subject, target, they all mean different things and they're
fluid terms. The way the Department of Justice views somebody and what category they're in
changes as new information presents itself. In this case, because it is an investigation run by
the OIG, they're going to have a narrow scope of what they're looking for, but in other cases,
just because you hear somebody initially say, oh no, I was told I was a witness, I was a subject,
that might change at a later date. So it's important to keep that in mind as these
movements continue to happen, because there are certainly people who believe
that they aren't targets who may end up being targets as the investigations progress.
This is another sign that DOJ is just rolling along regardless of
political claims that are being made. They're just doing their job. Now this investigation,
best guess, again we don't know this, but best guess is this has to do with the fake electors
scheme or maybe the pressure campaign against Pence. Right now all the coverage is focused on
the classified material investigation. It's important to remember that there's a bunch of
other investigations going on, and there are a lot of people who might be involved in
multiple prongs of that investigation, and there might be a lot of overlap.
So just bear that in mind when information comes out.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}