---
title: Let's talk about Biden's student debt relief policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eNhGoVucluE) |
| Published | 2022/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Biden administration's policy on student debt and universal access to higher education.
- Debunks the myth that the working class will be paying off the student loan debt of doctors and lawyers.
- Points out the hypocrisy of right-wing rhetoric regarding taxes and job creators.
- Addresses the argument that removing student debt forgiveness is a tool for military recruitment.
- Challenges those who say "but I paid mine" by suggesting it may be a result of voting against universal education.
- Notes that universal access to higher education benefits society as a whole.
- Advocates for universal higher education, even though the current policy doesn't go far enough.
- Emphasizes that the right wing opposes education to maintain control over the populace.
- States that those who oppose universal education are voting against their children's interests.
- Argues that billionaires, as job creators, should bear the cost of higher education.

### Quotes

- "You paid yours off? Well, that's probably because of the way you voted."
- "They're doing everything they can to get people to vote against their own interest."
- "This helps those on the bottom."
- "If you're opposing the idea of universal education, you're voting against your kids' interests."
- "They want you easy to control."

### Oneliner

Beau breaks down myths and challenges the opposition, advocating for universal higher education as a benefit to society.

### Audience

Voters, Students, Advocates

### On-the-ground actions from transcript
- Advocate for universal higher education (implied)
- Educate others on the benefits of universal access to education (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the misconceptions surrounding student debt and higher education, advocating for universal access while challenging opposing viewpoints.

### Tags

#StudentDebt #HigherEducation #UniversalAccess #Voting #CommunityBenefit


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about student debt
and universal access to higher education.
Because the Biden administration, they enacted this policy.
I'm sure you haven't heard of it,
but they enacted this policy
and man, it got a lot of people in the feelings.
So a whole bunch of talking points have surfaced
on social media and in media in general.
We're gonna go through them
and see if they hold up to any scrutiny whatsoever.
My personal favorite is that the working class is now going to be paying the
student loan debt of doctors and lawyers. No, no, that's just something they made up.
That doesn't even make sense. You can check me on this, but I'm pretty sure that
like a primary care physician, I'm pretty sure that they make like a
quarter mill a year on average in the US. That's above the $125,000 cutoff, isn't it?
So this doesn't apply to them, so that's just something they made up to get people angry to vote against their own
interests. That's what it seems like to me.
Now, there could be exceptions to that, you know, where people in fields that could make a ton of money are choosing to
work in an area that is economically disadvantaged, therefore they make less money.  But I've got to be honest, and this
is just me talking, but if that's the case,  case they're probably choosing to serve that area for a good reason and I'm
glad
you caught a break. The other part to this that bothers me is that all of a
sudden the working class is paying taxes which is weird because the right-wing
rhetoric for like 40 years ever since Reagan has been that the working class
doesn't pay taxes, that it's the job creators, they pay all the taxes.
That's why they get those, you know, tax cuts worth trillions of dollars.
But all of a sudden, when the policy is something that is going to benefit those on the bottom,
now they're trying to switch rhetoric?
No, no, that's not how this works.
You have held this position for 40 years, that's your position, right?
It's not like you've been lying to people this whole time, trying to get them to vote
against their own interest, to support the billionaires over their neighbor.
I mean, certainly that's not the case.
Another one that has shown up is that this is removing a tool for the military, for recruitment.
And what that means is that the military can't dangle a scholarship in front of economically
disadvantaged people to get them to sign up for something that they wouldn't normally
do.
Good.
And then the final one is, but I paid mine.
Okay, yeah.
Alright, but the odds are that's your fault.
If you're saying that now and saying, you know, I paid mine, I struggled and had to
pay mine and now somebody else is getting a break, the odds are you're against the idea
access to education, right? See, you voted that way. There are countries in Europe
that have had universal access to higher education for years. We could have it
here, but you vote against it because they tricked you into voting against
your own interests. You paid yours off? Well, that's probably because of the way
you voted. And if you're one of those people who paid theirs off and, you know,
support the idea of universal education or student debt relief, odds are you're not making
this argument right now.
The people making this argument right now are the people who have stopped having access
to universal higher education in the US.
They voted against it.
Elections have consequences.
This was your choice.
That's the government you wanted, right?
At least the one you deserve.
OK, now, I made some of these points on Twitter and, of course, got responses.
And one of the big ones has been, well, what are you getting out of it?
How much student debt did you get canceled?
That's why you're in favor of it.
None, not a penny.
Not a penny.
Nobody in my family.
Even if you go to my extended family, the last person that had student debt paid theirs
off a couple years ago.
benefit me monetarily in any way. However, it does benefit me in other ways. There's
a person who works on this channel, and we haven't done the math yet completely to figure
out exactly how it's going to break down, but either it totally wiped out their student
debt or they have like a grand or two left. That benefits me because I like that person.
It does hurt me in a way, though, because a lot of people who have watched this channel
for a while, if I say, you know, I was at the gas station, you know you're about to
hear a story about a woman who was a CNA going to nursing school, working at the gas station
because working at a gas station pays more than working as a CNA, which is a problem
in and of itself.
But there's been a whole lot of videos that were prompted because of her.
She's not going to be there anymore because she can turn in her two-week notice because
the reason she was working there was to pay stuff down.
This is life-changing for some people.
And then there's also the fact that we live in a society and I like people having access
to education.
I like educated people.
You don't have to go to college to be educated, but that's one way to do it and I think people
should have those opportunities, that benefits me too.
Those are the benefits I got out of it.
The reality here is that they're doing everything they can to get people to vote against their
own interest.
This helps those on the bottom.
This only helps people making less than 125K.
This helps people who are in a position where there are tens of thousands of dollars in
student debt.
They're down.
They need help, and this helps them.
I know there's a lot of people who say this doesn't go far enough, and I agree.
I agree.
I am somebody who is in favor of literally universal higher education.
If you want to go, you get to go, right?
Because it benefits society as a whole.
I'm in favor of that.
So I do agree, it doesn't go far enough, but at the same time, don't frame this as nothing.
This is life changing for some people.
And even if you're one of those who lives in an area that because the cost of living
is higher, the wages are higher, and you may have $40,000 and this is only taking out $10,000,
again don't frame it as nothing.
Because it's still, somebody walked up and either handed you $10,000 or $20,000, that's
That's not nothing.
And when you frame it that way, believe me, there are people who make $26,000 or $32,000
a year looking at you like you are out of your mind.
This is a big deal for a lot of people.
It helped.
The reason the right wing is pushing back on it so hard is because they don't want
an educated populace.
They don't want people who can look at their talking points and say, no, that's not true.
They want you easy to control and when they convince you to vote against your own interest,
which is what you're doing if you're opposing this, they've got you and they've got your
kids too.
If you're opposing the idea of universal education, you're voting against your kids' interests,
not just yours, your kids as well.
And when it comes to those billionaires who, you know, the job creators, they're the ones
that are going to end up footing the bill.
Who gets the most economic advantage out of these degrees?
The people getting the paycheck or the people signing the checks?
It's the people signing the checks.
They get the most money out of it.
They're paying for it, good.
That's how it's actually supposed to work.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}