---
title: Let's talk about Biden's CHIPs executive order....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2S6zKgNQckY) |
| Published | 2022/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden signed an executive order to create a 16-person team to implement the CHIPS Act swiftly, which involves a $280 billion investment in US semiconductor manufacturing capabilities.
- The domestic reason for this initiative is to secure the supply chain in case of global disruptions by ensuring US manufacturers have access to locally made components.
- Another reason is to reduce China's leverage in the production of semiconductors and increase the amount of US chips available.
- While boosting US production capability is beneficial, the goal is not to localize all production, as seen with the negative impact of localized production like the baby formula incident.
- The initiative aims to strike a balance between enhancing US production capabilities, maintaining international trade relationships to prevent tensions, and avoiding an economic arms race.
- Decentralization in production helps prevent disruptions and fosters peace by keeping economies interconnected.
- The Biden administration has taken necessary steps to expedite the initiative, but it received limited attention due to the overshadowing news of student loan relief.

### Quotes

- "Decentralization is harder to disrupt, and it helps promote peace by keeping the economies talking to each other."
- "Maintaining a balance with other countries so it doesn't start an economic arms race on the international scene."
- "While that sounds good, it's actually not. Look at what happened with the baby formula."
- "The interests of money tend to win out over a lot of other interests at times that might start a war."
- "It's worth noting what the Biden administration needed to do to get it moving quickly is done."

### Oneliner

Biden's executive order on semiconductor manufacturing aims to enhance US production while maintaining global trade balance and preventing economic tensions.

### Audience

Policy makers, tech industry leaders.

### On-the-ground actions from transcript

- Support policies that balance domestic production capabilities with international trade relationships (implied).
- Advocate for initiatives that enhance national production without risking economic tensions (implied).

### Whats missing in summary

The full transcript provides additional context on the significance of maintaining a balance between boosting domestic production and international trade relationships for economic stability and peace.


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about the
other thing that Biden did yesterday. It didn't get a lot of coverage because it was definitely
overshadowed by the whole student debt relief thing that impacted many more people than this
does. But this is definitely worth covering still, especially since we haven't really
covered this topic in depth on the channel. He signed an executive order that is designed to
create, I want to say it's 16 people, a 16 person team to make the CHIPS Act happen quickly.
Accomplish the goals of the CHIPS Act and get it out there and make it start working now.
Since we haven't really talked about that, what is it? It's a $280 billion, roughly, I think,
investment in US manufacturing capabilities dealing with semiconductors. So why? Why are we doing
this, right? The open reason, the domestic reason, is this helps safeguard the supply chain
in the event of another global disruption. US manufacturers will still be able to get the
components they need because some of them are made here in the United States. Right? Easy.
And this team is being put together to make that happen quickly. Now, there's the other reason.
You know, we have that international poker game where everybody's cheating.
The production of semiconductors within the United States, it reduces China's leverage.
Right? And at the table, it increases the amount of US chips, pun totally intended.
Now, there's good and bad with that. The good is that this does increase US production capability.
So in a supply chain scenario situation, sure, it helps offset things, helps keep things rolling.
At the same time, it's not an attempt to bring everything home, which we've talked about it on
the channel before. While that sounds good, it's actually not. Look at what happened with the baby
formula. When all of your production is localized, if something happens, well, that's it because
there's no established foreign trade. So it's big enough to be scaled up if the relationship between
the United States and China does go south. If it truly deteriorates, well, there you go. You can
scale up the production capabilities that are being built under this. However, they're not
getting them to that point immediately, which is good because trade between countries tends to
prevent tensions, or at least helps cool them. When you're talking about the game at that level,
money is a big deal and trade is money. So this seems to strike a happy medium between
getting the US up to speed and getting it to where it should be, and maintaining a balance
with other countries so it doesn't start an economic arms race on the international scene
where people start trying to bring all of their production home. Because again, while that's a
great nationalist talking point, in real life it pretty much always fails. You want those
sprawling webs. Decentralization is harder to disrupt, and it helps promote peace by keeping
the economies talking to each other. And the interests of money tend to win out over a lot
of other interests at times that might start a war. So that's where it's at. It's worth noting
what the Biden administration needed to do to get it moving quickly is done, but it barely made the
news because of the student loan stuff. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}