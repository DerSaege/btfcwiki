---
title: Let's talk about Trump's unsealed affidavit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QCm9-DilILA) |
| Published | 2022/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of unsealed documents leading to a search warrant at Mar-a-Largo.
- Lack of new information due to the DOJ doing their job properly.
- Media likely to focus on new stamps on classified documents, but definitions are available in the affidavit.
- Discrepancy between the Trump team's and DOJ's version of events regarding why the closet was locked.
- DOJ included a letter from Team Trump in the warrant application, stating authority to declassify documents.
- Judge still approved the warrant despite the letter, indicating it doesn't affect the case.
- Technicality about the president not being a specific role under the laws cited.
- The judge considered all arguments before issuing the warrant, making certain points moot.
- Key point: Judge authorizing search of a former president's home after reviewing arguments.
- Nothing in the documents significantly alters previous commentary on the channel.
- Speculation on DOJ's next steps and potential charges.
- Uncertainty on whether charges will be pursued or if DOJ will continue investigating for wider charges.
- Waiting to see DOJ's next move.

### Quotes

- "The judge saw that and provided the warrant anyway."
- "That is, that's a dead talking point now."
- "But it depends on whether or not they want to."

### Oneliner

Beau details unsealed documents leading to Mar-a-Largo search warrant, noting lack of new info, discrepancies, and judge's approval despite Trump team's arguments.

### Audience

Legal analysts and concerned citizens.

### On-the-ground actions from transcript

- Stay informed on legal proceedings and developments (implied).
- Support transparency and accountability in government actions (implied).

### Whats missing in summary

Insights into the potential implications of the search warrant and the broader legal implications.

### Tags

#DOJ #SearchWarrant #TrumpTeam #LegalProceedings #GovernmentAccountability


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the unsealed documents
that led to the search warrant down there at Mar-a-Largo.
OK.
So what's in it?
Not a whole lot new.
When we talked about it before, I
said if the DOJ did their jobs, there
wouldn't be a lot of new information.
They did their jobs right.
There's not a lot of new information.
There are a couple of things that the media is just
going to probably repeat over and over again now.
And then there's one thing that I think is really important.
The media is probably going to talk about all
of the new different stamps that can be found on classified
documents and provide definitions, so on and so forth.
Those definitions are actually available in the affidavit
if you wanted to go through them.
None of it significantly changes anything
that we've talked about so far.
There is a, let's just call it a discrepancy,
between the Trump team's version of events
when it comes to why the closet was locked,
and DOJ's version of events.
I don't know that it's going to matter long term,
But there is a discrepancy there.
The thing that I find interesting
is that DOJ included a letter from Team Trump that said,
he has the authority to declassify all these documents.
They actually included that in their application
for the warrant.
The judge saw that and provided the warrant anyway.
So for those who are still hanging on to that loophole
that your uncle on Facebook found,
a judge has already looked at it.
It doesn't apply.
It has nothing to do with what's going on,
because none of the laws cited require
the documents to be classified.
It requires defense information, and that's there.
There's also a technicality as far as, well,
the president isn't technically an officer, employee, consultant,
or contractor.
Again, that's one of those things.
OK, but that's worse.
And I don't think it mattered because I actually
don't think that that's the statutes that they
cited to get the warrant.
But that was also presented.
There was a letter from Trump's counsel included by the Department of Justice and given to
the judge.
So the judge had access to those arguments prior to issuing the warrant.
That is, that's a dead talking point now.
It has already kind of been looked at and a judge fully aware that, you know, they were
authorizing a search of the home of a former president looked at the
information provided and was like yes this doesn't apply and signed off that
to me is probably the the most important piece in it the other stuff I mean it's
open for commentary but there's not much there there isn't anything that I see
that sticks out that alters any of the commentary that we've had on this
channel before.
Everything's moving pretty much as expected.
So kind of a week-long wait for not a whole lot,
other than to be able to just truly throw away the,
but he declassified it with a blink and a nod argument.
So now, we have to wait and see where DOJ goes from here.
And that's anybody's guess.
That is anybody's guess.
To me, it seems apparent that they actually
have enough evidence.
If they wanted to, I believe they could seek charges.
But it depends on whether or not they want to, whether or not
they feel that is in the interest of justice,
or whether or not they want to continue digging
to see if there are other things that might lead the charges
to be wider in scope.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}