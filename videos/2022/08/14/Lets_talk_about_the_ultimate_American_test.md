---
title: Let's talk about the ultimate American test....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=04vox0BttPM) |
| Published | 2022/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a different format for his video, inviting viewers to participate in a quiz.
- He presents 10 questions related to American history and government, encouraging viewers to write down their answers.
- Questions range from the number of amendments in the U.S. Constitution to identifying writers of the Federalist Papers.
- Beau provides answers to the questions, revealing that they are from the American citizenship test.
- He mentions that most people can answer only five questions correctly, which is insufficient to pass the citizenship test.
- Beau references a recent incident where someone suggested immigrants should pass a test before entering the country.
- The person who made the suggestion agreed to take the test themselves but could only answer three questions.
- Beau concludes by leaving viewers with some food for thought about citizenship tests and knowledge of American history.

### Quotes

- "This is the ultimate test of America."
- "Most people can answer five of them. Incidentally, that means you don't pass."
- "America first, right?"
- "Y'all have a good day."

### Oneliner

Beau invites viewers to test their knowledge with 10 questions from the American citizenship test, shedding light on the importance of civic education and challenging assumptions about immigrants' knowledge.

### Audience

Viewers, citizens

### On-the-ground actions from transcript

- Take an American citizenship test (implied)
- Engage in civic education to understand American history and government (implied)

### Whats missing in summary

The full video experience and Beau's engaging presentation style.

### Tags

#AmericanHistory #CitizenshipTest #CivicEducation #Knowledge #Immigration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today is going to be very different than a normal video.
Had an opportunity to use something
that I have used many times in the past,
and I thought it might be fun to share this with y'all.
So get a piece of paper and a pencil or a crayon or a pen
or lipstick, something to write with,
or get some of these phones.
You can type it into that, because what's about to happen
is I'm going to go through 10 questions.
I'll read the question, give you a pause,
read the next question.
At the end, I will go back through and give you
all of the answers.
And this is the ultimate test of America.
All right.
So let's start off with, how many amendments does
the U.S. Constitution have? What are two rights in the Declaration of
Independence? Who makes federal law? The House of Representatives has how many
voting members? What are two ways that you can, as an American, participate in
democracy? When was the Constitution written? Probably should have given a
longer pause for that last one. The Federalist Papers supported the passage
of the U.S. Constitution, name the writers.
What territory did the United States buy in 1803?
What did Susan B. Anthony do?
Who was president during the Great Depression and World War II?
Okay, so the answers.
Number one, there are 27 amendments to the United States Constitution.
Number two, life, liberty, and the pursuit of happiness.
Number three, Congress, oddly enough, nobody's ever actually answered that question right
when I've asked.
Number four, 435.
Number five, there's a list here that I have written down.
So options are to participate in American democracy, vote, join a party, help a campaign,
join a civic group or a community group, correspond with an elected official and provide them
your opinion, run for office yourself, write to a newspaper, publicly support or oppose
an issue.
Number six, when was the Constitution written?
September 17th, 1787.
Number seven, who wrote the Federalist Papers?
Madison, Hamilton, and Jay.
Number eight, it's the Louisiana Purchase.
Number nine, women's rights.
Civic rights is also an acceptable answer on the test.
And number 10, that was Roosevelt.
Okay, so these questions all come from the American citizenship test.
That's where they come from.
In my experience, most people can answer five of them.
incidentally would mean you don't pass. This came up recently because I heard
somebody talking about some immigrants and basically saying that this family
should have had to have to pass a test before we let those sorts in. Yeah, there
There is a test.
And fun fact, the person who said that
did agree to take the test.
And they answered three.
America first, right?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}