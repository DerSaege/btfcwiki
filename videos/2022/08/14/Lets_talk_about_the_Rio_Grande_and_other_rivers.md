---
title: Let's talk about the Rio Grande and other rivers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8vSvi8GH5bY) |
| Published | 2022/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the impact of water scarcity on rivers across the globe, not just the Colorado or southwestern United States.
- Noting the drying up of the Rio Grande in Albuquerque for the first time in 40 years.
- Mentioning significant changes in the Thames River in London, with its head reportedly moving two to five miles due to drying up.
- Bringing attention to water issues with the Euphrates in Iraq, necessitating a deal with Turkey to prevent becoming a country with no rivers.
- Stressing the need for real action, better water management, planning, and research at a federal level to address the escalating global water crisis.
- Urging for environmental issues to become campaign topics and appear on ballots for immediate action.
- Emphasizing that this issue won't disappear overnight and requires urgent attention and significant changes to mitigate its impact.
- Calling on individuals, especially those in water-stressed regions, to start discussing and taking action on these critical water issues now.

### Quotes

- "We need big change, lots of change, better water management, better planning, better research."
- "This isn't something that stops on a dime."
- "Environmental issues need to get on the ballot and fast."

### Oneliner

Beau stresses the urgent need for real action and better water management at a federal level to address the escalating global water crisis.

### Audience

Global citizens

### On-the-ground actions from transcript

- Advocate for better water management practices in your community (implied).
- Raise awareness about water scarcity and its impacts on rivers (implied).
- Support and push for environmental issues to be included in political campaigns and ballots (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of water scarcity issues affecting rivers globally, urging immediate action and significant changes to tackle the crisis effectively.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about rivers.
That's a subject that has come up on the channel a lot,
but we've primarily been focused on the Colorado,
even though recently we did bring up Rhine.
We're going to talk about some other rivers,
because that same issue of less water
is occurring in a lot of places.
And I think it might be a good idea to show the scope of it.
Here in the US, on top of the Colorado, the Rio Grande,
that section, Albuquerque, the little northern part there,
that went dry, first time in 40 years.
It went dry.
Now, my understanding is that some rain
brought a little of it back, but it is not in a good way.
Now, on the other side of the Atlantic, you have the Thames.
You know, the one that London, you know,
really important one.
Its head has reportedly moved two to five miles.
The source of it has moved two to five miles
because it dried up.
That seems noteworthy.
They're saying that it's the first time that's happened.
The Euphrates in Iraq, they're having issues with that.
They're going to have to work out a deal with Turkey,
or they will be a country with no rivers.
This isn't localized to the Colorado.
It's not localized to the southwestern United States.
This is a big deal.
We need to start paying attention to this.
This needs to become a campaign issue.
This needs to be something that is on the ballot
because we have to get real action.
This needs to be a kitchen table issue,
as the political strategists like to call it.
For those in the southwestern United States,
it should be already.
You're just starting to get measures.
I don't want to call them solutions.
You're starting to get measures, like in Vegas with the pools.
You're starting to see stuff like that.
They're going to become more and more pronounced.
This should be something you're talking about
if you live there, but they're not solutions.
We need big change, lots of change,
better water management, better planning, better research.
That's going to have to come from the federal level.
This issue isn't going to go away.
It's something that will continue to return,
and every time it comes back, it's
going to get worse and worse.
This needs attention, and we need to start now
because this isn't something that stops on a dime.
This needs pretty immediate action.
Environmental issues need to get on the ballot and fast.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}