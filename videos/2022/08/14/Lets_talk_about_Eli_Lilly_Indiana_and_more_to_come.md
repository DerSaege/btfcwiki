---
title: Let's talk about Eli Lilly, Indiana, and more to come....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WGozkamvVC0) |
| Published | 2022/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Announcement out of Indianapolis, Indiana, regarding a global company planning for more employment growth outside of Indiana due to a new law.
- Eli Lilly, a company headquartered in Indianapolis for over 145 years, is concerned about recruiting and attracting talent to a state that restricts rights.
- The new law is a result of a Supreme Court reversal that has led to concerns about recruitment and job retention in Indiana.
- This move by Eli Lilly may lead to job losses in Indiana and a shift of jobs elsewhere.
- The impact of this decision goes beyond one company as other companies may silently be making similar decisions due to laws curtailing rights.
- Companies with consumer-facing operations may choose to move jobs elsewhere to avoid upsetting a portion of the population supporting such laws.
- Republican leadership's victory in enacting these laws may result in economic downturns in states implementing them.
- Anticipated consequences include job losses, higher taxes, worsening education, and increased need for assistance as family planning rights are restricted.
- States with such legislation may face serious economic challenges in the coming years, with impacts not fully felt until later.
- The choices made by leaders to restrict rights based on outdated polling data may lead to long-term negative economic repercussions.

### Quotes

- "It's not one company. There's a bunch. They just can't all announce it."
- "This is your state on Republican leadership."
- "Less jobs. That's the Republican Party victory."
- "The economies of these states will suffer dramatically."
- "They chose to embark on a multi-decade campaign to strip half the nation of their rights."

### Oneliner

An Indianapolis company's decision to move jobs due to restrictive laws signals broader economic repercussions and challenges Republican leadership's choices.

### Audience

Indiana residents, activists, policymakers

### On-the-ground actions from transcript

- Contact local legislators to express concerns about the economic impacts of restrictive laws (suggested).
- Support organizations advocating for rights and fair employment practices in Indiana (exemplified).
- Engage in community dialogues about the implications of legislation on job retention and recruitment (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the economic consequences of laws curtailing rights and the long-term effects on job markets and education systems.

### Tags

#EliLilly #Indiana #RepublicanLeadership #EconomicImpact #JobLosses


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about an
announcement out of Indiana, out of Indianapolis, Indiana, and what it means.
What it means for the people there and what it means for people in a lot of places who
may not have understood what was going to happen. A whole lot of people did
understand what was going to happen because a whole lot of people talked
about it before it ever did happen. In fact, when this news broke, my phone just
blew up because of everybody tagging me. So this is a part of a statement from
Eli Lilly. As a global company headquartered in Indianapolis for more
than 145 years, we work hard to retain and attract thousands of people who are
important drivers of our state's economy. Given this new law, we will be forced to
plan for more employment growth outside of our home state. This new law,
unsurprisingly, is one of those laws that's being pushed since that Supreme
Court reversal. You know, the one taking away half the country's rights. Now,
Lilly is afraid that they're going to have trouble recruiting, getting people to move
to a state that is curtailing the rights of half the population. They're worried
about that because they should be worried about it. They're right. They're
not going to be able to recruit to get people to move there. So they're moving
the jobs outside of Indiana. Thank your state legislature for that. Now, this is
something that a whole lot of people knew was coming and has been talked
about for a while, but you still have some people living in denial saying, oh
well, it's just one company. Okay, let's pretend that's true for a second. Sure,
but it's a company that's been there for 145 years. That company has called that
state home for longer than a whole lot of states have even existed. If I'm not
mistaken, it's worth a little bit more than a quarter of a trillion dollars. And
I want to say it's one of the largest employers up there. But the new jobs?
Well, they'll be elsewhere. That seems like a pretty big loss. That seems like a
pretty big loss. But the reality is, it isn't just one company. Lilly's in a
unique position. If you need their products, you're gonna buy them. You're
not gonna say, oh no, they went woke. They have a political decision I don't
agree with. I'm not gonna buy my meds. That's not gonna occur. So they can make
this announcement. You need to be worried about all of the companies that can't
publicly say it, but are doing the exact same thing. Companies that have
consumer-facing operations, they don't want to upset the small portion
of the population that is in support of these laws. So they're just gonna move
their jobs elsewhere and not say anything, because they're still gonna
have the trouble with recruiting. This isn't a surprise to anybody except the
Republican Party. They wanted to claim credit for Kansas. Oh look, we're
defenders of democracy. Kansas chosen. They decided they want choice.
Yay, that's us. We did that. No, no, that's not what happened. This, this is your
victory. Less jobs. That's, that's the Republican Party victory. But that's just
step one of their victory. Step two is more kids. Cause and effect. You take away
family planning, you're gonna have more kids. A lot of them are gonna be unplanned.
They're gonna need assistance. Then in five or six years, they're gonna end up
going to school, but your jobs are leaving. The good jobs, they're gonna be
gone. So your jobs are getting worse and your taxes are gonna get higher, or
education is gonna get worse. One of the two. This is your state on Republican
leadership. States that enacted this kind of legislation will have a serious
economic downturn. It'll start now, but it's not gonna get bad for five or six
years. That's when it's really gonna get bad. And by then, don't worry, they'll find
something else to blame it on and pretend like it wasn't entirely their
fault because they chose to embark on a multi-decade campaign to strip half the
nation of their rights based on polling from the 70s that they never updated.
When they started this, yeah, it was a winning political position. Something to
energize the base with. That's not the case anymore. It's wildly unpopular. This
is just the first part. It's not one company. There's a bunch. They just can't
all announce it. It's gonna get worse from here. The economies of these states
will suffer dramatically. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}