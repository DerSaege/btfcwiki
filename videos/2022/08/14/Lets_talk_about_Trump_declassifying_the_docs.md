---
title: Let's talk about Trump declassifying the docs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=k2CEEgIRGd0) |
| Published | 2022/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of understanding the secrets contained in declassified documents
- Points out the risk to national security posed by improperly stored documents
- Emphasizes that declassification of documents does not eliminate the threats posed by their contents
- Challenges the notion that declassifying documents is a legal loophole
- Suggests that retaining declassified documents could be considered a crime
- Urges people to familiarize themselves with relevant laws before attempting to exploit loopholes
- Stresses that declassified documents remain U.S. government property
- Addresses the breach of national security and its significant implications
- Asserts that political spin cannot change the reality of the compromised secrets in the documents
- Encourages allowing due process to address the issue, without seeking loopholes

### Quotes

"Those secrets were still there. Those people who generated intelligence, they were still at risk."
"There are no talking points that are going to change the fact that TSSEI documents and the secrets contained on those papers, even if he took a Sharpie and marked out TSSEI, those secrets were still there."
"Don't look for loopholes. There aren't any on this one, it's a big deal."

### Oneliner

Beau stresses the continued threat to national security posed by declassified documents and warns against seeking legal loopholes.

### Audience

Citizens, concerned individuals

### On-the-ground actions from transcript

- Stay informed about the implications of improperly handled classified information (implied)
- Advocate for accountability and adherence to laws regarding classified information (implied)

### Whats missing in summary

In-depth analysis of legal implications and potential consequences of mishandling classified information.

### Tags

#NationalSecurity #Declassification #LegalImplications #Secrets #Accountability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about a world of nonsense.
A world where nothing will be what it is because everything
will be what it isn't.
And loopholes, technicalities, declassification,
and objective realities.
We're going to do this because my inbox is
full of a bunch of people saying,
But he declassified them.
He declassified these documents.
Did it.
It's his power, absolute authority to do so.
He declassified them.
And then I had a person who asked,
why did you spend so much time talking about the fact
that they were TSSCI if it really doesn't matter?
Because I've mentioned how it doesn't matter on Twitter.
So the idea here, the reason that I spent so much time
talking about the fact that they were TSSCI,
was to impress upon people the importance of the secrets
contained in those documents.
I apparently wrongly believed that if people understood
the significance of the types of secrets that
contained on these documents, that their loyalty to their country would
win out over their loyalty to dear leader. I was wrong. Ever been wrong?
Happened to me. Instead, people are saying that, well, he declassified all of them.
And I know there's gonna be a whole bunch of people that want to argue that
in the comments. You can if you want to, but for the purposes of this video, let's
just saying that it's true, okay? He blinked, he nodded, and poof, they're
declassified. Cool! That's just a label though, right? The secrets that were on
those documents, they're still there, and they were still, according to reporting,
improperly stored. They were still a a ready threat to national security. That
doesn't change just because the classification level apparently
disappeared, right? Those secrets were still there. Those people who generated
intelligence, they were still at risk. The means and methods contained in those
documents, they're still compromised. That doesn't change anything. Most
importantly, it's not quite the legal loophole you might believe. Understand,
none of the statutes referenced, none of the laws that they're talking about
about, actually require the documents to be classified.
Even in a situation where the president did blink and nod, that's still defense information.
It's not quite the legal loophole you think it is.
Aside from that, the concept being presented by this, oh, well, I declassified them nonsense,
Because I took these documents, I declassified them, and then I kept them because they're
no longer classified.
Yeah, I'm not a lawyer, but I'm pretty sure that's actually an admission of a crime.
Just so you know.
Before you try to find a loophole, you really might want to take a look at the laws pertaining
to this.
It's unlikely that Uncle Bob on social media is going to think of a loophole that the people
in charge of the National Security Division of the FBI overlooked.
The idea is, well, he declassified them, therefore it's okay.
Yeah, those documents, they don't belong to Trump.
They're still US government property, even in a declassified state.
His statement, apparently, the messaging that has gone out is that he declassified them
and then stole them.
Not quite the win that people might be thinking it is.
This breach of national security is big.
This isn't a normal political thing that's just going to go away.
There are no talking points that are going to change the fact that TSSEI documents and
the secrets contained on those papers, even if he took a Sharpie and marked out TSSEI,
those secrets were still there.
They were, according to all reporting, improperly stored.
Nothing is going to change that.
No amount of political spin will alter that fact.
This is a big deal.
It's not going to go away.
I would suggest that everybody allow the process to work here.
Don't look for loopholes.
There aren't any on this one, it's a big deal, and there's nothing that Trump could do under
his authority that would change what has happened since he left office, which is the important
part.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}