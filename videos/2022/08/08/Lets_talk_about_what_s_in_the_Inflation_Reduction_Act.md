---
title: Let's talk about what's in the Inflation Reduction Act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DqjezsChg8o) |
| Published | 2022/08/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Inflation Reduction Act and its impending passage through the House, awaiting Biden's signature.
- Notes the lack of focus on the actual content of the Act in public discourse, with more emphasis on political implications.
- Acknowledges the significance of the Act for the Democratic Party, falling between transformative and insignificant.
- Details the financial aspects, such as a 15% minimum tax on billion-dollar companies and a 1% excise tax on stock buybacks.
- Mentions that the Act does not raise taxes for those earning less than $400,000 annually.
- Outlines healthcare provisions including subsidies, a $35 cap on insulin, vaccination funds, and Medicare prescription cost negotiation.
- Breaks down the $375 billion climate investment, the largest in U.S. history, covering clean energy, electric car rebates, carbon capture, methane emission fees, and drought alleviation funds.
- Points out the Band-Aid nature of some climate solutions, like money for the Southwest drought and Colorado River conservation.
- Addresses the criticism of increased federal land access for oil and gas drilling within the Act.
- Concludes that while the Act is a positive step, it falls short of transformative change, especially in climate action.

### Quotes

- "This isn't bad, but they should have gone further."
- "The big win here is the climate."
- "We have a long way to go."

### Oneliner

Beau outlines the Inflation Reduction Act, focusing on its financial, healthcare, and climate components, noting both positive steps and areas for improvement.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Contact your representatives to push for further climate change legislation (implied).
- Get involved in local conservation efforts to address immediate environmental concerns (exemplified).

### Whats missing in summary

Details on how the Act impacts different socio-economic groups and industries.

### Tags

#InflationReductionAct #ClimateChange #Policy #Healthcare #Taxation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about what's in it.
You know, the Inflation Reduction Act.
That is, at this point, it's as good as passed.
It has to make it through the House, which it will,
and then Biden has to sign it.
There's a whole bunch of people that I have seen
talking about this on social media,
and they're going back and forth talking about
whether it's good, it's bad, it's amazing, it's horrible.
But all of the framing is about the political effects
of it getting passed,
not a whole lot about what's actually in it.
So a quick note on the political side,
yeah, this is a big deal for the Democratic Party.
Getting this through is a big deal.
This is not the FDR-style transformative legislation
that the Biden administration said it wanted initially,
but this also isn't small potatoes.
So there's a lot of room between it doesn't matter
and what was initially proposed.
This is, parts of it are, yeah, whatever,
parts of it are a big deal.
So we're going to go through it.
So on the finance side,
there's a 15% minimum tax on companies
that make more than a billion dollars a year.
Okay, I mean, that seems like it should have been there
a long time.
There's a 1% excise tax on stock buybacks.
Sure, I get it to set the tone,
but at the end of the day, it's 1%.
That's not going to be huge.
Now, because this is something that everybody's fighting over,
I went to the AP, Associated Press.
According to the Associated Press,
this does not, does not raise taxes
on people who make less than $400,000 a year.
Okay, when it comes to the health care side of things,
there are subsidies for people
who are paying their own insurance.
There's a $35 cap on insulin,
but it doesn't apply to private insurers.
There's money for vaccination.
There is, let's see, oh, the big thing
for the Democratic Party,
the ability to negotiate prescription costs for Medicare.
There's also something in there about a $2,000 cap
on out-of-pocket expenses for meds,
and I'm going to be honest, I do not understand how that works.
Now let's get to the climate.
Big, big, $375 billion.
If I am not mistaken, this is the biggest,
this is the biggest climate change investment
the country's ever made.
Now, it is broken down into a bunch of different types of stuff.
So rather than getting into every single detail,
we're going to break it down into types.
Tons of investment in clean energy, okay, lots.
There are tax rebates for you to buy an electric car.
And the cool thing is this applies to new and used cars, I think.
There's money for carbon capture technology,
which if I've got that right, that's just money for Exxon.
There are fees for excess methane emissions
from oil and gas drilling.
There's $4 billion to deal with the drought in the Southwest.
Some of that money is actually conservation money
for the Colorado River.
Before anybody gets too excited about that,
this is cool, but this is buying a Band-Aid.
It's not fixing it.
It's good that it's there for the immediate,
but this is not solving, this isn't solving the problem money.
This is just mitigating in the very, very short term.
Okay, so that's what's in it.
Is there anything bad?
There's, it seems that oil and gas companies
are going to have more access to federal land for leases to drill.
That doesn't really square with the rest of the package.
That may have just been something that they had to throw in
to get certain senators on board.
There's a lot of stuff to counter that,
and it doesn't seem too expansive.
The general take from people who have looked at this
is that it's going to have a massive impact on emissions
and getting the United States to the goals that have been set.
It isn't the transformative mobilization
as far as the climate that we actually need,
but it's a good start.
I don't think you're going to see a lot of people
complaining about this except for the absolute doomers.
I think most people are going to be pretty happy with that.
The other stuff, it could be better,
but from the Democratic side of the aisle,
you're not going to see a lot of complaints.
It's, I think for most people,
it's going to be more scale than scope.
This isn't bad, but they should have gone further,
you know, type of thing.
Is this going to be enough to really hammer
when it comes to the midterms?
The climate stuff might be.
The climate stuff might be,
and there are some people when it comes to the health care stuff
that it's really going to matter and it might swing their vote.
As far as the tax stuff, I don't think that's enough
to truly upset the wealthiest,
and I don't think anybody else is really going to care
because it's not enough to really pull off
one of those transformative programs.
So the big win here is the climate.
That's a sentence you don't get to say very often.
Could the climate portion have been better?
Of course. We have a long way to go.
This isn't a win.
This doesn't put us to the point
where we're actually hitting the goals,
but it gets us a whole lot closer,
and there will be more legislation to follow, I'm sure.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}