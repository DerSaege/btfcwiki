---
title: Let's talk about Finland, Sweden, NATO, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aijHpxWINSw) |
| Published | 2022/08/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the addition of Finland and Sweden to NATO and addresses questions and concerns surrounding it.
- Emphasizes that adding these countries to NATO does not increase U.S. security commitments, nor does it increase the likelihood of war; in fact, it decreases it.
- Points out that alliances, especially with the addition of nuclear weapons, act as stabilizing forces rather than instigators of war.
- Argues that Russia lost the war in Ukraine regardless of the fighting on the ground, citing the goals of the war and Russia's loss.
- Asserts that politicians opposed to Sweden and Finland joining NATO are on the wrong side of the decision, supporting Russia's loss and increasing security for both NATO and Europe.

### Quotes

- "Wars are not about the fighting. Wars have other goals. Russia lost."
- "Unless they're rooting for Putin, they're on the wrong side of that decision."
- "It increases NATO's influence. It increases NATO's security. It increases European security."

### Oneliner

Beau explains the addition of Finland and Sweden to NATO, clarifying misconceptions and asserting Russia's loss in the war.

### Audience

Policy analysts, NATO members.

### On-the-ground actions from transcript

- Advocate for informed foreign policy decisions (implied).
- Support alliances for stability and security (implied).

### Whats missing in summary

The detailed nuances and explanations provided by Beau can be fully understood by watching the full transcript.

### Tags

#NATO #Russia #ForeignPolicy #Alliances #Security


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about the addition of Finland
and Sweden to NATO.
There have been a lot of questions
that have arisen over the last week,
and we're just going to kind of run through it.
Because this is the culmination of a lot of efforts
that started when Russia first invaded Ukraine.
Okay, so the first real question,
and it's really the only question
that kind of leads into everything else,
does this increase U.S. security commitments?
Is it bad for the U.S. to have these countries in NATO?
Does it promote the likelihood of war?
The answer to that is no.
No, it does not.
It does the exact opposite.
Now, when we're talking about this,
we're talking about foreign policy.
One of the reasons that people have a hard time
understanding foreign policy is because of those
who are generally interested in it.
They're interested in politics.
They have their own political beliefs.
They have their own ideology,
and they like to view the world
through the lens of their ideology.
Can't do that.
You have to view the world as it is to actually understand it
and then hopefully be able to influence it.
Think back to the beginning of the Russian invasion of Ukraine
when NATO was like,
you know what, we'll give Ukraine weapons.
There were a whole bunch of people saying,
no, don't do that because if you do,
Russia will attack NATO, and there were a whole bunch of people
that were like, no, they won't.
That's not going to happen because Russia doesn't stand a chance
against NATO in a toe-to-toe fight.
They absolutely do not.
Russia's having a hard time after all of this time
dealing with Ukraine.
It is impossible for them to go toe-to-toe with NATO.
They don't want to do it.
And now if you knew all of this,
understand that the Russian government,
contrary to a lot of coverage,
they're not crazy people.
They know that they can't go toe-to-toe with NATO.
So what does that mean if Finland and Sweden join NATO?
And the Russian government knows that by attacking them,
that they'll have to fight all of NATO.
It means they won't attack them.
It's that simple.
They're not, they aren't cartoon villains.
If they know they can't win, they won't attack.
Large alliances since the addition of nuclear weapons
have become, they're stabilizing forces.
And I know that's contrary to what people are typically
taught in high school and middle school
when you're talking about the onset of World War I.
Dynamics changed.
Think about the Cold War.
Alliances, border to border, right?
Competing alliances, opposing alliances, and no war.
The alliances are stabilizing.
Okay, so from that point,
does it increase the likelihood of war?
No. In some ways, it decreases it.
Now, I don't know that Russia had any designs
on invading Finland or Sweden.
I don't actually think that was in their playbook anytime soon.
However, now we know it's not.
Aside from that, what are the other benefits?
This cements Russia's loss for the war in Ukraine.
Doesn't matter.
We talked about it months ago.
There came a certain point because of the political developments
that it doesn't matter what happens on the ground.
Russia lost the war.
Even if they took all of Ukraine and held it,
they lost the war because wars aren't about the fighting.
Wars are a continuation of politics by other means, right?
What were the goals of the war in Ukraine for Russia?
They were pretty simple.
Increase Russian influence in Europe.
Convince other countries that they would be better off
in their sphere of influence rather than the NATO sphere of influence.
Convince countries to stay off their border
if they are going to be friendly buddy-buddy with NATO.
And good old-fashioned imperialism.
Take some dirt, plant a flag.
Those were the goals.
With Sweden and Finland joining NATO, Russia lost.
The only option they have left,
the only thing that they could possibly succeed at at this point
is good old-fashioned imperialism.
And with everything they've lost, the troops,
the standing in the world, the economic hits,
the degradation to their military capabilities,
that is a super expensive real estate transaction.
That's a lot to lose for just dirt.
Wars are not about the fighting.
Wars have other goals.
Russia lost.
They lost months ago.
This is just cementing the loss and making it official.
They are still fighting in Ukraine.
And they can try mission creep style
to achieve some wider goal than what they really wanted, all of this.
But it doesn't matter what they do now.
They lost the war.
Grand scheme, they lost the war, even if they win the fighting.
So I understand that there were some politicians who were opposed
to this in the United States.
They should never be anywhere near a foreign policy decision again.
Unless they're rooting for Putin,
they're on the wrong side of that decision.
Confirming Sweden and Finland into NATO cements Russia's loss.
It increases NATO's influence.
It increases NATO's security.
It increases European security.
It in some ways increases Russian security.
Because with Finland in NATO, there is zero chance
that Finland is going to launch an unprovoked attack on Russia.
The lines, you know, Russia has been very, very adamant
that they don't want NATO countries on their border.
It's because they're thinking in military terms from 50 years ago.
It doesn't matter anymore.
It doesn't add increased security to have a buffer
because NATO can fly over the buffer.
It's just not, it's an ancient strategy now.
It's obsolete.
It doesn't work.
But they're hardwired into it because people like to do things
the way they've always been done.
Now I understand that people are going to say Russia had these other objectives in Ukraine.
No, they didn't.
No, they didn't.
That's just how they sold the war, how they justified it.
Those weren't the goals.
Don't fall for that.
Every country in the world, every country, no exceptions,
when they go to war that is an elective war,
they're choosing to do it, not one out of defense,
but when they are choosing to do,
one, they try to frame it as defensive.
Two, they come up with a package to sell the people.
But it's always about the same thing at its core, money and power.
Don't fall for the packaging.
There's not exceptions to that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}