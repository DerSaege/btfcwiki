---
title: Let's talk about how Trump can't catch a break....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FrgtM4nFhzM) |
| Published | 2022/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is facing multiple legal challenges across different jurisdictions, including Department of Justice investigations and cases in New York and Georgia.
- Eric Hirschman, Trump's former lawyer and senior White House advisor, has been subpoenaed by a grand jury investigating a sustained campaign to hold on to power.
- Hirschman's testimony before the committee was frank, indicating potential trouble for Trump.
- The CFO of the Trump Organization in New York is reportedly considering entering into a plea agreement, which could spell bad news for Trump.
- Rudy Giuliani has been informed that he is a target of a grand jury in Georgia.
- The Department of Justice opposed unsealing an affidavit related to a search warrant at Mar-a-Lago, citing irreparable damage to an ongoing criminal investigation.
- The filing by the Department of Justice suggests they are pursuing a criminal case beyond just retrieving documents.
- The unsealed affidavit might disclose highly sensitive information about witnesses and could impact other high-profile cases.
- There are speculations about witnesses cooperating with the DOJ against Trump.
- Overall, the developments indicate a series of bad news for Trump with quickening pace of investigations and probes.

### Quotes

- "It's all bad."
- "I got a feeling it's going to be a long one."

### Oneliner

Trump faces multiple legal challenges and investigations across different jurisdictions, with developments indicating a series of bad news ahead.

### Audience

Legal observers, political analysts.

### On-the-ground actions from transcript

- Stay informed about the ongoing legal developments related to Trump's cases (suggested).
- Follow reputable news sources for updates on the investigations and probes (suggested).

### Whats missing in summary

Insight into the potential implications of these legal challenges on Trump's future and political standing.

### Tags

#Trump #LegalChallenges #DepartmentOfJustice #GrandJury #Investigations #PleaAgreement


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're gonna talk about how Trump
can't catch a break.
A lot of developments last night, yesterday.
We're gonna kind of recap some of the stuff that has gone on.
Again, this is multiple cases, multiple jurisdictions.
So just kind of roll with me on this.
Let's start with the grand jury,
the Department of Justice grand jury
that is looking into what we think is
the sustained campaign to hold on to power.
They have issued a subpoena for Eric Hirschman.
Eric Hirschman was Trump's lawyer
during the first impeachment,
then became a senior White House advisor.
Hirschman, after the election, was one of the few,
according to reporting, was one of the few
that was just pushing Trump to concede,
you know, early transition and all of that.
There were other people in the White House
who saw things differently.
Their advice, some might say, led to the events of the 6th.
Hirschman seems to have taken that personally.
Testimony before the committee was very frank.
You might remember him.
If you don't remember the name,
you might remember the clip of somebody saying,
and then I said,
I'm gonna give him the best free legal advice
he's ever gonna get.
He needs to get a great criminal attorney
because he's gonna need it.
Didn't seem to hold a lot back when talking to the committee.
Probably isn't gonna hold much back
when talking to the grand jury.
That's bad news for Trump.
Now, in New York, in the New York case
against Trump Organization,
it appears as though the CFO there is looking into,
entering into a plea agreement.
Now, rumor mill says it is just a plea agreement,
not a plea and cooperation agreement,
which would be really bad news for Trump.
But when the good news is that the CFO is taking a plea,
that's not really good news.
Now, back to Georgia,
where we talked about yesterday Lindsey Graham.
Rudy Giuliani was informed that he is a target
of that grand jury,
and there will be more on that later.
And then the really big news and bad news for Trump
is the Department of Justice
opposed unsealing the affidavit
that led to the search warrant down there at Mar-a-Lago.
It's not so much that they opposed unsealing it.
It's the reasons they gave.
They said it would cause irreparable damage
to this ongoing criminal investigation.
There are a lot of people who believed,
some probably still do,
that the Department of Justice
was really conducting a retrieval operation.
They were just getting those documents back,
getting them out of circulation,
and that was going to be the end of it.
The filing suggests otherwise.
The filing suggests they really are pursuing a criminal case
beyond just getting the documents back.
They're actually looking into a crime.
It also said that unsealing it
would disclose highly sensitive information
about witnesses, plural.
People in Trump's immediate circle
have gone to the other side.
That's pretty safe to say at this point.
Who? Nobody has a clue.
And there are a lot of people guessing,
given the current climate,
I don't know that it's a great idea to guess about that
because there are people
who are very upset and emotional about this,
and there's no reason to draw a bull's eye on somebody.
But the filing does indicate that there have been witnesses
who have spoken to DOJ that Trump may not know about.
And then it also says that if this was disclosed,
it might have a chilling effect on other high-profile cases.
Nothing in this filing is good news for Trump.
The only part of it that he might be able to latch onto
is some conspiratorial take
about why they don't want to open up the affidavit.
I don't know that he'll draw attention to it.
It seems like that would be a bad idea
because then people might read their reasoning
behind opposing unsealing it.
And there is nothing good in there for Trump.
It's all bad news, just varying levels of bad.
They do talk about the extremely sensitive nature
of the TSSCI documents.
It's just all bad.
I have a feeling that we're probably going to see
more days where all of the news that's coming out
is just going to be really bad news for Trump.
The pace of these investigations,
these probes, these grand juries seems to be quickening.
So just hang in for the ride
because I got a feeling it's going to be a long one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}