---
title: Let's talk about Graham and the Trump case in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sAw-RFy2S9M) |
| Published | 2022/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Lindsey Graham received a subpoena to talk to a special grand jury in Georgia.
- Graham's lawyers argued for immunity under the speech and debate clause and sovereign immunity, but the arguments were rejected.
- Graham is implicated in implying that officials should manipulate things for the former president.
- If Graham's statements don't hold up under scrutiny, he could face serious consequences, especially with others potentially cooperating with authorities.
- The investigation stems from the controversial phone call to find votes in Georgia.
- Raffensperger suggests Graham may have been involved in pressuring state officials.
- The grand jury is looking into various election-related offenses, including solicitation of election fraud and conspiracy.
- Graham's involvement in the grand jury investigation could lead to serious issues for him.
- The case involving Graham and the Trump circle is gaining momentum and could have significant consequences.
- This case is another one to keep an eye on in the broader context of legal actions involving Trump and his associates.

### Quotes

- "The worry is that if those statements aren't true and at some point somebody who is aware of the fact that they may not be true, perhaps somebody was in a room when there was a discussion that takes place and that person flips, then Graham gets wrapped up in a bunch of stuff."
- "Generally speaking, when you're talking to election officials and you're pressuring them to find votes, that is something that law enforcement refers to in technical terms as totally uncool and might lead to severe issues."

### Oneliner

Lindsey Graham faces implications and potential consequences in the Georgia grand jury investigation, adding to the legal troubles surrounding Trump and his associates.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Stay informed on the developments of the legal cases involving Trump and his associates (suggested)
- Monitor the outcomes of Graham's involvement in the grand jury investigation (suggested)

### Whats missing in summary

Detailed analysis and context on the broader legal implications of cases involving Trump and his circle.

### Tags

#LindseyGraham #GeorgiaGrandJury #TrumpCircle #LegalIssues #ElectionFraud


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
Lindsey Graham and the Trump case. No, not that Trump case. The other one. Probably not
that one either. Okay, so we're not talking about the Trump case dealing with the documents.
We're not talking about the ones dealing with the electors. We're not talking about the
one in New York. We're talking about the one in Georgia for those trying to keep track.
So Senator Graham was given a subpoena. He said, hey, you got to go talk to the special
grand jury. Graham was like, I don't want to. Well, I don't know if that's what Graham was
like, but his lawyers argued that he shouldn't have to. They used the argument of the speech
and debate clause, which does give legislators a lot of immunity for things that they say.
However, it's also, I think it's more narrow in scope than a lot of legislators may seem
to believe. They also argued that he had sovereign immunity. The federal judge was like, no,
rejected these arguments. You have to go talk to the special grand jury. Now, Raffensperger
has said that when he was talking to Graham, that he felt Graham was implying that Raffensperger
should find a way to make things more favorable to the former president. So Graham's going
to have to talk about that. Now, the real worry for Graham is if he goes in there and
he says, you know, I was just doing my job. I was trying to make sure there was integrity
in a federal election, or I was just trying to understand the process better so I could
better inform my constituents or future legislation. There are arguments that he could make. The
worry is that if those statements aren't true and at some point somebody who is aware
of the fact that they may not be true, perhaps somebody was in a room when there was a discussion
that takes place and that person flips, then Graham gets wrapped up in a bunch of stuff.
And there are a lot of people in Trump's circle who might be considering cooperating with
various authorities. One of the real issues for Trump world with all of these different
cases going on is that there's a lot of people speaking under oath. That testimony has to
match. With this many people talking, this many people trying to present their side of
things, there's almost an inevitability that somebody is going to walk into a perjury charge,
even if they don't mean to. Now, of course, this whole investigation stems from that perfect
phone call to find 11,780 votes or 11,870 votes, however many it was. Raffensperger,
in public statements, has seemed to indicate that he feels Graham might have been involved
with a campaign to pressure state officials. Generally speaking, when you're talking to
election officials and you're pressuring them to find votes, that is something that law
enforcement refers to in technical terms as totally uncool and might lead to severe issues.
The scope of what the grand jury is looking into is huge. They are looking into solicitation
of election fraud, the making of false statements to state and local governmental bodies, conspiracy,
profiteering, violation of oath of office, and any involvement in violence or threats
related to the elections administration. Now, that's the grand jury as a whole, not what
they're looking into Graham for specifically. But this is the firestorm that Graham is going
to be walking into if he does actually have to testify here.
So this is another case that we have to watch.
This is another case involving the Trump circle that is picking up steam and starting to move a little bit more quickly
and really has a lot of potential to create issues for a lot of those in Trump's orbit.
So anyway, it's just a thought.
I hope you have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}