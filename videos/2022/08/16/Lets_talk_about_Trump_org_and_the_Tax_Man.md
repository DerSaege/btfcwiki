---
title: Let's talk about Trump org and the Tax Man....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hJgB-imdqDY) |
| Published | 2022/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a tax case against the Trump Organization and CFO Allen Weisselberg in New York.
- Mentions the attempt by the Trump team to dismiss the case, which was not successful.
- Notes that the judge allowed the case to go to a grand jury, with jury selection starting on October 24th.
- Points out that some charges in the case carry significant potential sentences, with one ranging from five to 15 years.
- Raises the fact that tax cases like this can be lengthy, involving educating the jury and potentially taking months for a trial.
- Suggests that the trial could coincide with the midterms if the schedule holds, although Trump himself is not a defendant in this case.
- Indicates that this legal issue adds to the numerous other cases and investigations surrounding Trump and his associates.
- Emphasizes that even though this case may seem less significant to the American public, its outcomes can be unpredictable.
- Speculates that accountability for Trump may come from unexpected angles like taxes and documents rather than the expected sixth-related matters.
- Considers the possibility that cases like these could erode Trump's perceived invulnerability.

### Quotes

- "It is worth noting from a political standpoint that if jury selection begins October 24th, it's a safe bet that the trial will be going on during the midterms."
- "When cases go forward, they're not always what you think they'd be for."
- "There are cases springing up, there are investigations springing up, there are searches springing up, people inside his circle are just constantly being hit now."
- "There is this case and there's the document case, which is, that appears to be a very strong case."
- "It's possible that it ends up being taxes and documents that actually really starts to chip away at Trump's Teflon coating."

### Oneliner

A tax case against the Trump Organization and Allen Weisselberg could impact midterms, adding to legal worries for Trump while challenging public perceptions of accountability.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Follow updates on the case and its implications (suggested)
- Stay informed about legal developments related to accountability in political circles (suggested)

### Whats missing in summary

Insight into the potential broader implications of legal challenges on Trump's reputation and political influence. 

### Tags

#Trump #TaxCase #LegalIssues #Accountability #PoliticalImpact


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about Trump and the tax man.
We're gonna talk about the other Trump case.
No, not the other one in Georgia
and not the other one in DC, the other one in New York.
This case is against Trump Organization
and the CFO, Allen Weisselberg.
Allen Weisselberg.
It's a tax case and I wanna say it's over about 1.7 mil,
something like that.
The Trump team tried to get this all dismissed.
The judge heard the arguments
about it being politically charged,
you know, the DA's a Democrat, all of this stuff,
and was basically like, yeah, it went to the grand jury.
So jury selection begins October 24th and scheduled it.
These charges, some of them are pretty significant.
There's one where the sentence range
appears to be five to 15 years.
Keep in mind, Weisselberg's like 75 years old.
Now, cases like this, tax cases,
they tend to drag on and on.
A trial can take months
because there's a lot of educating the jury
as the case proceeds.
They can take a really long time.
It is worth noting from a political standpoint
that if jury selection begins October 24th,
it's a safe bet that the trial will be going on
during the midterms,
assuming that this schedule is adhered to.
I don't know how much significance it's gonna have,
if any, because Trump himself is not a defendant in this.
But this is yet another case.
It's another legal worry for Trump world.
There are cases springing up,
there are investigations springing up,
there are searches springing up,
people inside his circle
are just constantly being hit now.
This may seem to be one of the least significant
to the American people.
However, just remember, when cases go forward,
they're not always what you think they would be for.
I think most Americans,
when they think of accountability and Trump,
they're looking for something related to the sixth.
That may not be the way it shakes out.
There is this case and there's the document case,
which is, that appears to be a very strong case.
So it's possible that it ends up being taxes and documents
that actually really starts to chip away
at Trump's Teflon coating.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}