---
title: Let's talk about polling, planning, and a Republican question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0uNYKHHJ-d8) |
| Published | 2022/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into recent polling data revealing that seven out of 10 individuals want a ballot initiative to vote on family planning rights in their state.
- The polling data indicates that if given the chance, two to one individuals favor having family planning rights.
- Demographic breakdown shows that 60% of women and 47% of men support having these rights.
- Beau points out the significant discrepancy in support between men and women, reinforcing the narrative of men trying to control women's bodies.
- Surprisingly, 34% of Republicans are in favor of having these rights that were taken away by the Republican Party.
- Beau stresses the importance of these numbers in a government by the people, suggesting they should influence policy decisions.
- He notes the absence of a ballot initiative on this issue in most states, prompting a focus on voting for representatives instead.
- Beau predicts that voters may hold Republican representatives accountable for the loss of these rights.
- He questions whether Americans will prioritize other issues over the rights revoked by the Republican Party.
- Beau concludes by leaving his audience to ponder these political dynamics and their implications for the Republican Party.

### Quotes

- "These numbers should matter."
- "If we are a government by the people, these numbers should matter."
- "That only exists in a few states."
- "The hope for the Republican Party at this point is..."
- "Y'all have a good day."

### Oneliner

Seven out of 10 individuals want a ballot initiative to vote on family planning rights, posing a challenge for the Republican Party to face the implications of these numbers on upcoming elections.

### Audience

Voters, Political Activists

### On-the-ground actions from transcript

- Mobilize voters to prioritize reproductive rights in elections (implied)
- Support candidates who advocate for family planning rights (implied)
- Hold representatives accountable for their stance on reproductive rights (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of recent polling data on family planning rights and its potential impact on the Republican Party's electoral prospects. Viewing the full transcript can provide a deeper understanding of voter sentiments and political dynamics surrounding this issue.

### Tags

#Polling #FamilyPlanningRights #RepublicanParty #VotingRights #Representation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about some polling
and what that polling means for the Republican Party.
And a question it should be prompting them to ask.
And the fact that they're going to have to face this polling
and face it soon.
It's being discussed on social media.
However, there's an element that's being left out of it
because the polling is about a hypothetical.
And it's not the reality.
OK, so what's the polling?
Seven out of 10 want a ballot initiative.
They want to be able to personally vote
on whether or not their state has family planning rights.
They want to personally cast a vote in kind of a referendum
since the Republicans overturned Roe.
Seven out of 10 want that opportunity.
And if that opportunity was granted,
two to one would vote in favor of having family planning
rights in their state.
Those are big numbers.
It's also important to note some demographics in that.
When it comes to women, 60% to 25%
are in favor of having those rights.
When it comes to men, 47% to 32% are
in favor of having those rights.
By the way, guys, this is why when
women talk about it being men trying to control their bodies,
those numbers kind of showcase it.
Another particular number of concern for the Republican
Party is that 34% of Republicans are
in favor of having those rights, the rights
the Republican Party took away.
If we are a government by the people,
these numbers should matter.
And when you're looking at them, it's
clear that the support is there across the board.
It's an overwhelming majority, right?
But the problem for the Republicans
is that in most states, this ballot initiative,
it's not going to exist.
People aren't going to get to personally vote on this issue.
So what are they going to do?
What is on the ballot?
What will be?
Representatives.
If people can't vote to retain their rights
and us truly be a government of the people,
well, they have to vote on the representatives.
And the one fact that everybody knows
is that it's the Republican Party that
took away these rights that Americans say they have,
that Americans support by an overwhelming majority.
This ballot initiative that is in the poll,
that only exists in a few states.
That's not going to be on the ballot everywhere.
But those representatives, those senators, well, they will be.
And the Republican Party has to wonder
whether or not the voters are going to take it out on them.
I think a lot of them will.
The hope for the Republican Party at this point
is that the American people care more about some rich guy
getting his house searched than they
do about the rights that the Republican Party took from them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}