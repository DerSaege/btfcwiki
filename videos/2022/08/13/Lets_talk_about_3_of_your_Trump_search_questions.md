---
title: Let's talk about 3 of your Trump search questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Bqw8G4FyRaA) |
| Published | 2022/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the situation involving Trump and the ongoing search.
- Addressing three common questions: Why isn't he in custody yet? What should happen? What if it was planted?
- Acknowledging the slow pace of the investigation and Garland's approach.
- Emphasizing the presumption of innocence for the former president.
- Sharing thoughts on potential arrest and suggesting home confinement.
- Responding to the idea of evidence being planted and challenging viewers to provide footage supporting that claim.
- Urging patience and allowing the justice system to work methodically.
- Cautioning against becoming emotional or demanding premature action.

### Quotes

- "What we really can't stand right now as a country is to have one political party going down the road of conspiracy theory and just wild claims."
- "If you believe Trump committed a crime, you want that case to be airtight."
- "You want that case to be perfect before it goes before a grand jury."

### Oneliner

Beau delves into Trump's situation, addresses common questions, and urges patience and faith in the justice system.

### Audience

Concerned Citizens

### On-the-ground actions from transcript

- Allow the justice system to work methodically (implied)
- Avoid premature demands for action (implied)

### Whats missing in summary

Insights on the potential implications of rushing legal processes and the importance of evidence-based decision-making.

### Tags

#Trump #JusticeSystem #PresumptionOfInnocence #ConspiracyTheories #Patience


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk a little bit more
about the situation involving Trump, the search,
what we've learned thus far, and your questions.
There are three that keep coming in.
The first two are pretty simple.
The third one is also simple, but will be entertaining.
So the first question is, why isn't he in custody yet?
The second is, what do I think should happen?
And the third is, but what if it was planted?
And we will answer all of them.
Okay, why isn't he in custody yet?
I don't know.
At the same time, I don't think that I know how
to do Garland's job better than Garland.
I know that there are a lot of people
who are less than enthusiastic
about the way he has done things.
He's moving very slowly and it bothers me as well.
However, I mean, his track record's not bad.
I would just allow him to do his job.
There may not, as of yet, be enough to warrant an arrest,
to put him in custody.
They may be working a wider case.
There may be other explanations for these documents.
I don't know what those would be, but maybe they exist.
At this point, the former president does have
the presumption of innocence.
It is how our system works.
What do I think should happen?
When Garland, when the evidence reaches the point,
if the evidence reaches the point
that it does warrant an arrest, the arrest should be made.
The only thing that I would suggest
is that he be put on home confinement
rather than detained in a jail.
And that is, I'm not even gonna lie,
that's for purely political reasons.
I think it would be smarter to put him on home confinement.
And I know there's also the possibility
of just letting him go, you know, ROR.
I don't really know that that's a good idea either.
If it was me, I would put him on home confinement
awaiting trial.
But again, the assumption now has moved
from nothing's going to happen
to why isn't he already in custody?
Just the wheels of justice turn slowly at DOJ.
Okay, so ride it out.
What if the FBI planted it?
Okay, I'll play your little silly game.
Sure, what if they planted it?
Well, if you're gonna make that allegation,
my suggestion would be you use the footage
from that clubhouse and show it.
See, here's this weird thing.
I'm gonna read a little bit of the property receipt,
and maybe you'll figure out why I think
there would be footage showing them doing it
if they actually did that.
They didn't.
Okay, one, executive grant of clemency
for Roger Jason Stone Jr.
That could be small.
That could be planted, all right?
Info, R.E. the President of France.
Leather-bound box of documents.
Various classified TSSCI documents.
Potential presidential record.
Binder of photos.
Binder of photos.
Handwritten note.
Box labeled A1.
Box labeled A12.
Box labeled A15.
Miscellaneous secret documents.
Box labeled A16.
Miscellaneous top secret documents.
Box labeled A17.
I wanna see the footage that Trump would have
from the club of the FBI agents carrying the boxes in, right?
I mean, that would make sense since they're planting it,
that they would need to carry it in.
So, I mean, I'm assuming he has that.
He could release it if he so chose,
and that would show that it was planted.
But I have a feeling that's not gonna happen.
We have to wait and see what plays out.
That's where we're at.
What we don't need is for people to start to get antsy
and start to get emotional about it
and start to demand action out of its proper time.
What we really can't stand right now as a country
is to have one political party
going down the road of conspiracy theory
and just wild claims,
and then the other become incredibly emotional
and start demanding the government act before it's ready.
If you believe Trump committed a crime,
you want that case to be airtight.
You want that case to be perfect
before it goes before a grand jury.
Let them work.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}