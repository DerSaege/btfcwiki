---
title: Let's talk about 87,000 new IRS agents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=l67iRb0Dpe0) |
| Published | 2022/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are spreading misinformation about 87,000 new IRS agents coming to audit people as a talking point against the Inflation Reduction Act.
- The $78 billion allocated in the Act for the IRS is spread out over 10 years to expand and hire new employees.
- Current IRS staffing is around 78,000, with half expected to retire in the next five years.
- The new employees won't all be auditors; they will also include customer service and IT staff.
- The fear-mongering claim that the new employees will focus on auditing the middle class or small businesses is false.
- The report suggests that by 2031, the IRS could hire 87,000 new employees due to the investment.
- The reality is that the increase in employees will be around 20,000 to 30,000, bringing the IRS back to previous staffing levels.
- There is no grand conspiracy to target the working class; it's merely fear-mongering and misinformation.
- Those worried about this misinformation are mostly W-2 employees who receive tax refunds, making them unlikely targets for collection.
- This misinformation is just another example of prominent Republicans creating scare tactics for their base.

### Quotes

- "It's fear-mongering. It's made up. It's just not true."
- "There is no big conspiracy here. No giant plot to go after the working class."
- "You can just add this to the giant list of things that prominent Republicans made up to scare their base."

### Oneliner

Republicans spread false fear-mongering about 87,000 new IRS agents coming to audit people, part of a pattern of misinformation to scare their base.

### Audience

Taxpayers

### On-the-ground actions from transcript

- Fact-check misinformation spread by politicians (implied)

### Whats missing in summary

Full context and detailed explanations can be better understood by watching the original video.

### Tags

#IRS #Republican #Misinformation #FearMongering #Taxpayers


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about 87,000 new IRS agents
coming to audit you, right?
You've probably heard that or seen that.
It is a new Republican talking point.
It's something they're pushing out there
to suggest that the Inflation Reduction Act
is actually bad for you, okay.
So, here's the problem with it.
It's not true.
It's made up.
It's not true.
But everything has a root, right?
So let's talk about the root, where this came from.
All right.
In the Inflation Reduction Act, there is $78 billion for the IRS to expand, to hire new
people, stuff like that.
That $78 billion is spread out over 10 years.
Current staffing at the IRS is about $78,000, however, 50% of them are expected to retire
within the next five years.
The stats say that they're going to net around $20,000 to $30,000 more than current level
78,000, which would bring them back up to the roughly 100,000 employees they had previously.
It's worth noting that the idea that the new employees, those that they met, that those
are going to be auditors, is also just not true.
Sure, some will, no doubt.
But they will also be customer service people on the phone, IT people, everything that it
takes to run a massive agency.
It's fear-mongering.
It's made up.
It's just not true.
The idea is false.
It is wholly inaccurate to describe any of these resources as being about increasing
audit scrutiny of the middle class or small businesses. That's a counselor for tax policy
and implementation at the Treasury Department. So the real root of this is a May 2021 report
that says if there was this massive investment made that the IRS would be
able to hire 87,000 new people by 2031. So there is some grounding in reality to
this story, but the idea that the IRS is bringing on 87,000 auditors to come
after you is silly. It's not true. It's fear-mongering. They lied again. There
will be a net increase in employees, but it's going to be 20 to 30,000 which will
bring them back up to previous staffing levels. No big conspiracy here. No, you
giant plot to go after the working class. The ultimate irony in this is that the
people that I have talked to that have complained about this are all W-2
employees who get a tax rebate at the end of the year. Tax refund. There's
nothing, there's no collection possibility from you. If they're already
giving you money back, and you're a W-2 employee, you're a normal working person,
this isn't something that even needs to really be on your radar, much less
something you need to worry about. So you can just add this to the giant list of
things that prominent Republicans made up to scare their base. Anyway, it's just a
thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}