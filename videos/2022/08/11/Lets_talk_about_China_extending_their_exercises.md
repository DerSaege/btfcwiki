---
title: Let's talk about China extending their exercises....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0nW-5yacEzc) |
| Published | 2022/08/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- China has extended exercises around Taiwan, causing news due to Taiwan official's invasion concerns.
- Countries like China conduct exercises for posturing, intelligence gathering, and training.
- Posturing is like being a tough guy in a bar, showing strength without wanting to fight.
- Intelligence gathering involves observing reactions to the exercises for battle planning.
- Training ensures all military pieces function together effectively.
- Repeated exercises can create the "boy who cried wolf" syndrome, where exercises are seen as routine.
- China's current posturing is influenced by the distracted US and internal political dynamics.
- China may be extending exercises due to Taiwan not engaging in counter exercises.
- Taiwan's response to the exercises was limited to artillery drills.
- China may extend exercises to gather more explicit intelligence or because pieces didn't work well in training.
- China, despite appearing confident, probably doesn't want to invade Taiwan but prefers peaceful reunification.
- Invasion carries risks like damaging infrastructure and financial costs, which China wants to avoid.
- The reasons for extending exercises could be posturing, intelligence gathering, training, or a potential live operation.
- China likely wants to bring Taiwan in peacefully, considering the risks and costs of invasion.

### Quotes

- "Posturing, intelligence gathering, training."
- "War is almost never a good decision, and countries do it all the time."
- "China probably also doesn't want to invade."

### Oneliner

Beau explains why China conducts exercises around Taiwan: posturing, intelligence gathering, and training, hinting at potential peaceful reunification despite invasion concerns.

### Audience

International observers

### On-the-ground actions from transcript

- Keep informed about international relations and conflicts (implied)
- Advocate for peaceful resolutions to conflicts (implied)

### Whats missing in summary

Analysis of potential diplomatic solutions

### Tags

#China #Taiwan #MilitaryExercises #InternationalRelations #PeacefulResolution


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about China and Taiwan.
China has extended the exercises that they
have going on around Taiwan.
This, of course, is causing news.
And what we're going to do today is talk again
about why countries engage in these kind of exercises.
The reason it's coming back up in the news
is because an official from Taiwan
was like, they're preparing to invade.
I mean, yeah, they are.
They said that, actually.
The Chinese government put out a statement that basically said,
you know, we're committed to peaceful reunification,
but we reserve the right.
You know, so why do countries engage
in these types of exercises?
We've talked about it on the channel
before in a video called, Let's Talk About What Happens Now
in Ukraine from April 23, 2021.
This video was not filmed during the buildup
immediately preceding the war.
It was filmed during the exercise months before.
OK?
In that, I outlined three main reasons that countries do this.
The first is posturing.
China is one of the big guys, you know,
one of the tough guys in the bar.
And they're looking at the other tough guys in the bar,
giving them dirty looks.
They don't actually want to fight, but posture is important.
That's part of this.
The second is intelligence gathering.
If there's a whole bunch of ships off your coast,
what do you do?
You start getting ready for an invasion, right?
You start making moves, preparing.
When you do that, the opposition nation
is able to gather intelligence on what you're doing.
That's how battle plans get drawn up.
The third is training, like legitimately training.
You know, it's a good idea to put all the pieces together
every once in a while and make sure
that they function together.
Okay, we'll go back through those here in a second,
but then there's a fourth benefit that is,
it's not really part of the plan
because everybody knows about it.
However, you'll see something funny.
If a country does these exercises repeatedly
or extends them or anything like this,
it creates the boy who cried wolf syndrome
and people start to basically assume
that the exercise is just posturing,
intelligence gathering, and training.
I say this in the video from April 23rd, 2021.
However, even though I know that that's a risk,
when the buildup occurred on the Ukrainian border
the second time, what did I say?
That's probably just an exercise.
Even if you know that that's a risk,
you can still succumb to it.
So in this specific case for China,
the posturing is good for them right now.
The United States is distracted,
partly because of what's going on in Europe
and partly because the United States
has a major political party inside the country
constantly screaming that it's weak.
Power is perception in a lot of ways,
and when you have a major political party,
I'm looking at you Republicans, inside the country
screaming the United States is weak,
that the military can't fight,
it weakens the country.
Good job, patriots.
So China feels probably more confident
than they should be about their position.
So they're just kind of flexing a little bit at the bar.
The second one was intelligence gathering.
Specific to this situation,
and I think this may have a lot to do
with why they're extending the exercise,
is my understanding is that Taiwan didn't take the bait.
They put their troops on alert,
but they didn't launch a counter exercise.
My understanding is that all they did was some drills
dealing with artillery.
Every country in the world knows
how the other side's artillery is gonna work.
It all pretty much works the same way.
So the Chinese didn't get the intelligence bonanza
that they should have.
Certainly, they were able to pick up a lot
from whether or not Taiwan's radar
was able to pick them up and stuff like that.
But it wasn't as explicit as it could have been.
It didn't show moves on the board being made.
So maybe they're hoping that if they extend it,
Taiwan will give them a little bit more information.
And then another option is that it didn't go well.
So training, they're extending it
because the pieces didn't work together very well.
So they're gonna try it again.
The United States runs exercises all the time.
And it's funny because we probably don't need to
as much as other countries
because we actually go to war a lot.
China hasn't run a lot of large military campaigns.
So in the event that they invade Taiwan,
that's gonna be one of their biggest operations
in a really long time.
So they need to train.
And then keeping that fourth option in mind,
yeah, maybe they are just deciding,
hey, well, let's just make it go live.
That seems unlikely
because China has to kind of look at what happened to Russia
and wonder how much clout it has,
how well it can weather an international storm.
Now, objectively, the answer to that is much better than Russia.
But how much better is the question?
China probably also doesn't want to invade.
My guess is that they want to bring Taiwan in peacefully.
It's better for them if it goes that way,
even if they have to use a lot of coercion.
However, if they use an invasion force, there's resistance.
This damages the infrastructure.
That's a lot of money.
And China has become very aware of the markets
and how much money influences how much power they have.
So I don't think they want to destroy the island.
It's not a good decision for them.
That being said, war is almost never a good decision,
and countries do it all the time.
So the reasons this could be extended, posturing, intelligence, training.
These are why countries engage in these exercises to begin with.
We know that China didn't get all of the intelligence that it could have.
So maybe they're hoping for that.
The other two, it's a guess.
And yeah, there is the possibility that China is considering just doing it now.
Seems unlikely, but I mean, we can't discount it.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}