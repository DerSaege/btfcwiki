---
title: Let's talk about Garland's statement on the Trump search....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IRyHUdBwbUI) |
| Published | 2022/08/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into Merrick Garland's recent statement, sparking curiosity and questions among viewers.
- Garland emphasized equal application of the law, presumption of innocence, and a plea to stop criticizing FBI agents.
- The Department of Justice filed to unseal a warrant and a property receipt, hinting at Trump's prior public comments.
- Garland seemed to suggest that Trump should have remained silent, potentially avoiding the current situation.
- He mentioned his approval of the search, leaving two possible interpretations: taking accountability or setting up a successful outcome narrative.
- Garland's main concern appears to be countering misinformation that could incite harmful actions, referencing past events where false narratives led to arrests and harm.
- By releasing accurate information, Garland aims to prevent the framing of the situation as a political event by certain individuals.
- The unveiling of the warrant and receipt holds the key to clarifying the situation, urging patience until then.
- Speculation surrounds the motives behind Garland's actions, with a reminder that Trump had the power to release the warrant and receipt earlier.
- Beau concludes with a reflective statement, hinting at the ongoing intrigue and the importance of accurate information dissemination.

### Quotes

- "Stop talking bad about him."
- "It really seems like he didn't want to go this route."
- "Almost 1,000 people got arrested."
- "It's going to take a lot of wind out of the sails of those who are trying to frame this as a political event."
- "Y'all have a good day."

### Oneliner

Beau delves into Merrick Garland's statement, addressing accountability, misinformation, and potential outcomes while awaiting the unsealing of a warrant and receipt to clarify the situation.

### Audience

Viewers, political observers

### On-the-ground actions from transcript

- Await the unsealing of the warrant and property receipt to gain clarity on the situation (implied).
- Counter misinformation by sharing accurate information with others (implied).

### Whats missing in summary

Insights into the possible implications of the unsealed warrant and receipt, and the broader impact of accurate information dissemination.

### Tags

#MerrickGarland #Justice #Accountability #Misinformation #Speculation #Unsealing


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Merrick Garland's
statement.
It just happened a little bit ago.
But in the time that has lapsed since then,
my inbox is nonstop filling up with questions about it,
what it meant.
It was a really short statement.
We're going to go through it.
We're going to talk about what was said, the two options that
are left as far as what it means.
One thing I do want to say is that I
don't like doing videos that are this speculative.
We don't have a lot of information.
This just happened.
This video is speculation based on a five-minute statement.
So take that into consideration when you're viewing it.
OK, so what did he say in his statement?
First is the normal stuff you get
from Garland, equal application of the law,
presumption of innocence, so on and so forth.
Also, don't mess with my FBI agents,
kind of what he's saying.
Stop talking bad about him.
He said that the Department of Justice
has filed to unseal the warrant and the property receipt.
Important part there.
He indicated that this wouldn't be happening
if Trump had not confirmed that it occurred and not
talked about it publicly.
Kind of seemed like he was saying, hey, you know,
you're pretty good at playing the fifth.
You should have done that.
You have the right to remain silent.
Maybe you should exercise it.
It really seems like he didn't want to go this route.
But here we are.
He also said he approved the search.
That's another important piece when
you're talking about the options and what this means.
So what does it mean?
It means either Garland just walked out and said,
I approve this search.
We're going to release the information.
It was a mistake, and it's my fault.
And he's taking accountability for his own actions.
Or he's setting it up to walk out and say,
hey, this is what we said we were going
to find in the search warrant, and this is what's
listed on the property receipt.
Golly gee whiz, it almost seems like we know what we're doing.
It's one of those.
Those are the most likely options
as far as this little five-minute statement
and what it means.
I think a more important motivating factor for Garland
is the rhetoric coming from certain people that
might lead others who maybe are less informed down a bad road.
There are people who are drastically
misrepresenting the situation.
And those misrepresentations could
cause people to take actions.
These people have done this before.
The last time they did it, it caused a bunch of harm.
It destroyed a bunch of lives.
And almost 1,000 people got arrested.
I think he might be trying to kind of head that off
by putting some accurate information out there
and making it harder to frame a false narrative.
Because if that warrant and that property receipt,
if those are close, it's going to take a lot of wind out
of the cells of those who are trying to frame
this as a political event.
So we're going to have to wait and see
what happens when the warrant and the receipt are unsealed.
Until then, it's speculation.
Anything anybody says is speculation until that point.
Keep in mind, Trump could have released the warrant
and the receipt any time he wanted to.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}