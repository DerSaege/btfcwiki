---
title: Let's talk about Trump's latest moves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QwM0CjNFmAQ) |
| Published | 2022/08/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on the developments surrounding the former president, focusing on his recent legal situation.
- Mentions that the former president invoked the fifth amendment over 400 times during a meeting in New York.
- Talks about the potential implications of invoking the fifth amendment in a civil case and how it may not be the wisest legal strategy.
- Reports on the existence of a confidential source at Mar-a-Lago causing turmoil within Trump's circle, leading to paranoia and finger-pointing.
- Suggests that those feeling vulnerable within Trump's circle may be incentivized to cooperate with federal authorities.
- Speculates that Trump may not be inclined to keep individuals around who he believes are cooperating, potentially leading to more people cooperating.
- Mentions the floating of the idea that the FBI planted evidence and sarcastically suggests that the Trump team should run with that defense strategy.
- Addresses questions regarding potential charges against Trump and dismisses exaggerated claims, stating that he has not been charged with anything yet.
- Downplays the likelihood of the former president facing prison time and suggests the possibility of home confinement instead.
- Comments on Team Trump fundraising off the situation and reports on considerations of Trump running in 2024 for immunity as president.
- Points out that amidst ongoing investigations, the current situation is relatively minor and could potentially be a tactic to induce paranoia and mistakes.
- Concludes by hinting at the possibility of induced errors due to paranoia within Trump's team and encourages viewers to have a good day.

### Quotes

- "It's not the crime, it's the cover-up."
- "As is often the case, you know, it's not the crime, it's the cover-up."
- "As president, he'll have immunity."
- "That's going to go over great."
- "Trump 2024 or busted."

### Oneliner

Beau provides insights into the legal developments surrounding the former president, discussing implications of invoking the fifth amendment, internal turmoil within Trump's circle, and the potential for induced errors due to paranoia.

### Audience

Political enthusiasts, legal observers.

### On-the-ground actions from transcript

- Stay informed on legal developments and their implications within political circles (exemplified).
- Monitor how paranoia and internal conflicts can impact decision-making within powerful groups (exemplified).
- Be aware of potential tactics used in legal cases to induce mistakes (exemplified).
  
### Whats missing in summary

Analysis of the potential political ramifications of ongoing investigations and legal proceedings.

### Tags

#LegalDevelopments #FormerPresident #TrumpAdministration #PoliticalAnalysis #InducedErrors


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, this morning, we're going to have another update on the former
president's developments.
So yesterday, the Trump team did in fact make their meeting in New York.
It is reported that the former president pled the fifth somewhere north of, you
know, 400 times. Now I personally do not hold it against somebody when they
exercise their constitutionally protected rights. That being said, in New
York my understanding is that in a civil case they can infer from people who
play the fifth. That may not have actually been the smartest legal strategy for him,
but we'll have to wait and see how that plays out. Reporting is now saying yes,
there absolutely is a confidential source, or at least was a confidential source at Mar Largo.
So, that information has undoubtedly turned Trump world against each other.
Most of them are very paranoid people, prone to believing in conspiracies and the like,
reporting the saying that they are throwing each other under the bus and everybody is
pointing fingers as to who the the flipper is. That is only going to feed
the paranoia and at the same time it puts the former president in a pretty
unenviable position because anybody in the circle who believes they're about to
be pushed outside of the circle because of an accusation, true or not, if they
think they're going to be disavowed and pushed outside of the realm of protection
that MagoWorld offers, it gives them incentive to cooperate with the feds.
That being said, Trump is probably not in the mood to keep people around that he
thinks are cooperating. This will probably be one of the developments
that leads to a lot more people starting to cooperate. Trump is not known for
being diplomatic. If he starts accusing random people within his circle of
cooperating, it might generate a lot of animosity and then a lot more people
will probably start cooperating.
They are floating the idea that the FBI planted evidence.
OK.
Yeah, I think Trump team should roll with that.
I think they should go with that.
Judges love that.
That's going to go over great.
And there is nothing better for defense
to have the narrative out there that the FBI planted stuff.
you did nothing wrong, and then evidence show up.
Talking about something that will undermine credibility
in the court of public opinion, and if that actually
makes it into a defense, that's not
going to go well, because the feds will walk in and explain
what their cause was to get the warrant.
And my guess is that's going to include specific testimony
from people who are going to say, we saw that stuff there before the feds ever showed up.
A lot of questions coming in, did Trump really increase the penalty on what he's going to
be charged in, is basically how these questions are framed.
First, there's a lot of investigations.
He hasn't been charged with anything yet.
It's a search warrant.
Trump world is blowing that way out of proportion. It's a search warrant. That's
what occurred. They executed a search warrant. Nobody's been charged. There are
a lot of investigations that could end with a lot of different statutes. Yes,
there is one statute that is relevant to the allegations concerning the
classified material that in 2018 Trump took from what I want to say was a
misdemeanor with a max penalty of a year and turned it into a felony with a max penalty of five years.
It's also worth noting, as people talk about accountability, it is really unlikely that
the former president, regardless of the situation, sees prison in the way I think some people are
envisioning. This would probably be a situation where a judge might order home
confinement or something like that. I find it very unlikely that the former
president is going to be, you know, chilling at Marion. It just doesn't seem
like something that's going to occur.
As is totally predictable, Team Trump is fundraising off of this for some reason.
Yes, the billionaire needs $45 from working class Republicans so he can fight the search
warrant that's already been executed or something.
I don't know.
It doesn't make any sense to me.
It's par for the course.
There has also been discussions reported
that suggest the former president is really
considering running in 2024, because as president,
he'll have immunity.
I mean, that's not what I would consider a good reason
to vote for somebody.
I don't know that I would want that information out there.
would make for some great campaign slogans, though, Trump 2024 or busted.
Now one of the things that I want to point out is that in the grand scheme of things
with the investigations that we know are going and those that are also likely, this is minor.
actually isn't a big thing in the grand scheme of things. I don't want to say
it's a smokescreen because it is a case and it's being laid out as a case. It
might be worth remembering though when you're talking about organized crime or
something like that, it's not out of the realm of possibility for law enforcement
to go shake things up a bit, go pick up some people for some chopped cars or minor stuff
to kind of induce more paranoia and then force mistakes.
As is often the case, you know, it's not the crime, it's the cover-up.
With the Trump team, it's unlikely that they have the discipline to stay battened down
for too long in a situation where paranoia has been induced and they are looking at charges,
the possibility of charges, even if they are relatively minor.
It might induce errors.
I don't want to say that that's a plan from DOJ, but it might be happening anyway.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}