---
title: Let's talk about Meadows, Trump, and the DOJ....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5PkvPcDSGuE) |
| Published | 2022/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's legal team is in direct talks with the Department of Justice, possibly to protect executive privilege.
- Speculation suggests Trump's legal team warned him against talking to Meadows, hinting at potential criminal liability.
- Trump's lawyers may be preventing him from talking to potential witnesses to avoid witness tampering charges.
- The Department of Justice's investigation appears to be accelerating, indicating a comprehensive probe into a large conspiracy case involving Trump and associates.
- Concerns arise that Trump's legal team may miss key aspects of the investigation, given its extensive scope.
- Uncertainty remains about the possibility of an indictment and its impact on the institution of the presidency.
- The investigation seems to be focusing on Trump as a central figure, potentially due to undisclosed evidence.
- The Garland Justice Department operates discreetly, leading to speculation about impending announcements regarding Trump.
- The lack of concrete information leads to various speculations, but the seriousness of the investigation is evident.
- Expectations suggest significant developments in the investigation within the next 45 days.

### Quotes

- "The Department of Justice's investigation is speeding up. It is moving more quickly and it is very, very comprehensive."
- "We know that it's a grand jury. We know that they're looking hard at Trump and associates."
- "My gut tells me that there's probably going to be some pretty big news in the next 45 days or so."

### Oneliner

Trump's legal team navigates potential criminal liability amidst accelerating DOJ investigation, with expectations for significant developments soon.

### Audience

Legal observers, political analysts

### On-the-ground actions from transcript

- Stay informed about updates in the investigation (implied)
- Monitor news outlets for any official announcements (implied)

### Whats missing in summary

Insights on the potential implications for the presidency and legal system.

### Tags

#DOJ #Trump #LegalTeam #Investigation #CriminalLiability


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to talk about
Trump, Meadows, and the Department of Justice
and the reporting that has come out involving those players.
The general tone of the reporting is that
Trump's legal team is in direct conversations
with the Department of Justice.
The speculation is that that has to do with him
wanting to protect executive privilege.
We've talked about it in other videos.
I don't expect that to go in Trump's favor.
The other piece of news is that apparently his legal team
has told him that he shouldn't really
be talking to Meadows anymore.
The read on that is that it's them
indicating their belief that Trump might be held criminally liable for some things.
And that Meadows may flip and cooperate with the Department of Justice to perhaps lessen
his own criminal liability.
This is the theory that is being floated all over the media.
And yeah, that's a read.
That's a read.
I can't say that that's wrong. I do want to suggest something else though. It may
also be that Trump's legal team doesn't want him talking to Meadows or anybody
who might be a witness because of the way Trump talks to people, because of his
habit of encouraging people to provide testimony in a certain way. They may be
be trying to safeguard their client against, well, I mean witness tampering
charges. That may also be a reason that that's occurring. Now, overall, does all
of this lead to the idea that Trump's legal team believes he's in criminal
jeopardy? Yeah, yeah, that definitely looks like what's happening and
and they're trying to prepare a defense.
However, as we talked about before,
at this point, if they weren't actively preparing
a defense ahead of time, they wouldn't be good lawyers.
They wouldn't be doing what they're supposed to do
to protect their client.
So I don't know how much you can read into that
as far as whether their expectations reflect
The real idea that Trump will be held criminally liable, they have to proceed
out of an abundance of caution. Well, they should if they want to safeguard their client.
We have seen some things in recent days with some unique behavior by different lawyers
associated with right-wing people. I wouldn't expect that in Trump's case. I wouldn't expect
lawyers to make mistakes like that. Another thing you can read into this is that the Department of
Justice's investigation is speeding up. It is moving more quickly and it is very, very comprehensive
when you look at all of the different pieces they're trying to pull together and all of
the different people they're talking to, this looks like a very large conspiracy case.
It looks like a case with a whole lot of little tentacles.
And the real worry for Trump and Trump associates is that they're going to miss one of those
tentacles.
they're not going to effectively cover their bases. At this point I'm still not
sure about an indictment because there is that concept of protecting the
institution of the presidency and whether or not I believe that an
indictment against somebody who has a lot of evidence suggesting they might
have engaged in criminal activity. Whether or not I believe that is
protecting the institution of the presidency doesn't matter. What matters
is whether or not the people making the decisions do, and we don't know that yet.
We don't know where they stand on that particular issue. What I do know is that
the people who are running the investigation, they definitely seem to be closing the circle
around Trump.
He seems to be the central figure.
Now they might be doing that just to eliminate options, or they might have a lot of evidence
that we don't know about, which is what I think is most likely, is that there's a lot
of information out there one way or the other that that the public isn't aware
of yet. The Garland Justice Department moves quietly and then boom out of
nowhere they announce what they're going to do. Does the Trump legal team talking
directly to the Department of Justice, does that mean that that announcement is
coming soon? We don't know. There's a lot of speculation coming from a lot of
different pundits when the real answer is, assuming that everybody is even
halfway doing their job, we don't have a clue. So without documents, without really
good sourcing who would be telling you something they shouldn't, it's all a
guess-and-gain. It is a guess-and-game at this point. But what we do know is that
it's serious. We know that it's a grand jury. We know that they're looking hard
at Trump and associates. So we're just gonna have to wait and see what DOJ
comes up with, if anything. My gut tells me that there's probably going to be some
pretty big news in the next 45 days or so. Anyway, it's just a thought. Y'all have
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}