---
title: Let's talk about an important message from New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y7QJqRsTzxA) |
| Published | 2022/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing an emerging public health issue related to polio in New York.
- New York state health officials are urging people to get vaccinated for polio due to concerns about community spread.
- Polio virus has been found in wastewater samples from Rockland and Orange counties with low vaccination rates.
- The confirmation of one paralyzed individual raises concerns about potential additional cases.
- State health officials advise ensuring vaccinations are up to date before sending kids to school.
- Emphasizing the importance of vaccination to prevent the spread of polio in the community.
- Polio was previously eradicated in the U.S. through vaccination efforts.
- Urging prompt vaccination to combat the re-emergence of polio.
- Stressing the need to prevent the situation from escalating.
- Encouraging individuals to consult with a doctor and get vaccinated as soon as possible.

### Quotes

- "New York state health officials are concerned about the possibility of community spread of polio."
- "This isn't something that we can let get out of hand."
- "This is a disease that was eradicated in the United States through vaccination."

### Oneliner

New York health officials raise alarm on community spread of polio, urging prompt vaccination to prevent escalation and protect public health.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Consult with a doctor to ensure vaccinations are up to date before sending kids to school (implied).
- Get vaccinated as soon as possible to prevent the spread of polio in the community (implied).

### Whats missing in summary

The importance of community-wide vaccination efforts to safeguard against the re-emergence of polio.

### Tags

#PublicHealth #Polio #Vaccination #CommunitySpread #NewYork


## Transcript
Well, howdy there, internet people. It's Beau again.
So today, we are going to talk a little bit more about an emerging public health issue.
There are a number going on right now. This is one that seems to be getting the least amount
of attention, and it, in my opinion, should probably be getting more. We've talked about
it a couple of times on the channel. There is new information.
Okay. New York state health officials have issued an enthusiastic call for people to get vaccinated
for polio because they are concerned about community spread of polio. I say again,
they are now concerned about community spread of polio. They have found the virus
in seven different wastewater samples from two different counties, Rockland and Orange.
Rockland and Orange counties. Unsurprisingly, these two counties are known for having a high
population of people who are opposed to vaccination. Statewide, the vaccination rate
is around 79-80%. In Rockland and Orange counties, it is around 59-60%.
Right now, they have the one confirmed, identified case, the person that we have talked about before,
the person who was paralyzed. Historically speaking, when you have a case where somebody
is paralyzed, you could have hundreds of other cases where people didn't exhibit the same symptoms
or people didn't exhibit those severe symptoms. Now that they have found the virus in multiple
wastewater samples from two different counties, they are concerned. They are concerned.
They are advising everybody to make sure that they're up to date before you send your kids
to school. Talk to a doctor. See what they say. Their advice is to get vaccinated as soon as
medically possible. Because I will say this one more time, New York state health officials
are concerned about the possibility of community spread of polio.
This isn't something that we can let get out of hand.
This is a disease that was eradicated in the United States through vaccination.
And now there is the concern about community spread.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}