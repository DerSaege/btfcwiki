---
title: Let's talk about the Jones verdict and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YCQoXbG2jB4) |
| Published | 2022/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Young says:

- Explains the substantial damages of around $49.3 million awarded in the Jones case.
- Mentions that there are more cases against Jones pending, indicating a potential increase in liability.
- Raises the possibility of the awarded amount being reduced due to a ratio for compensatory versus punitive damages in Texas civil law.
- Speculates on the impact of the verdict on curtailing disinformation spread by conspiracy theorists like Jones.
- Suggests that the outcome might not lead to widespread doubt among Jones' supporters but could prompt content creators like him to be more cautious with their claims.
- Believes that individuals spreading misinformation for profit may change their narratives to avoid legal repercussions, shifting focus to topics like aliens that can't sue.
- Anticipates a shift in the dynamics of such shows following the verdict, aiming to reduce real-world impacts rather than sparking a revelation among their audience.

### Quotes

- "This verdict and those that will probably follow are going to alter the dynamics of types of shows."
- "I'm hopeful that this is going to make things a little bit better."
- "There's going to alter their talking points on their shows to make them less open to legal action."
- "I think it's going to change things."
- "I don't think there's going to be a widespread epiphany from the base of these shows."

### Oneliner

Beau Young explains the impact of substantial damages awarded in the Jones case and how it might shape disinformation narratives online.

### Audience

Content Creators, Audiences

### On-the-ground actions from transcript

- Monitor and hold accountable content creators who spread misinformation for profit (implied).
- Advocate for stricter regulations on spreading false information online (implied).

### Whats missing in summary

Insights into the potential broader societal impact of increased accountability for spreading disinformation. 

### Tags

#Disinformation #ConspiracyTheories #LegalAccountability #ContentCreators #OnlineNarratives


## Transcript
Well, howdy there, internet people, it's Bo Young.
So today we are going to talk about the verdict
in the Jones case and the damages that were laid out by him
because they were substantial.
Looks like the total is around $49.3 million
that the jury has awarded to the plaintiffs in this case.
That is a substantial amount of money.
It definitely sets a tone, and it's also worth noting that there are more cases against Jones
to follow this.
For those who don't know who Jones is, you're lucky.
Aside from that, he is a conspiracy theorist, and he has made a lot of money over the years
peddling claims that are less than accurate.
In this particular case, it was pretty damaging to the people that he was making the claims
about.
So the questions here.
First thing I do want to point out, there are more cases to follow.
So the total number that he's going to end up being liable for is probably going to grow,
but also that it may shrink before that.
I've had a couple of people tell me that because of a weird quirk in the law there, this number
may be reduced because there's some ratio that may need to be applied when it comes
to compensatory versus punitive damages.
Now I know nothing about Texas civil law, so I have no clue if that's true, but I've
had a couple of people say it to the degree that it makes me wonder if that's something
that could happen.
Even if it does, there are more cases to follow.
Now, the other question that has been coming in about this is whether or not I think that
this is going to curb disinformation, wild claims on the internet.
Yes and no.
I think when most people are asking questions like this, what they're really asking is,
are the people he has tricked, the people who believe in him, are they suddenly going
to doubt their belief in him. I don't actually think so. I mean, it may happen with some
people but I don't think there's going to be a large downturn for Alex Jones because
of that. I don't think that's going to happen. What I do think will happen is that those
who run these sorts of shows are going to be much more careful about what they say and
And many of their claims may revert back to being about aliens and stuff along those lines.
Things, entities that can't sue them.
Because it's my opinion that most of these people, they don't actually believe what they're
saying.
They're doing it to generate revenue.
If they're going to lose that revenue, then they may alter their primary talking points.
If they're only talking about entities that don't exist, it is far less likely to have
the negative impacts that they have had in society.
So yeah, I think it's going to change things.
I think this verdict and those that will probably follow are going to alter the dynamics of
types of shows but I don't think it's because people are gonna look at this
and say oh he lied to me I think it's gonna be more along the lines of they're
going to alter their talking points on their shows to make them less open to
legal action which means they're going to have less real-world impact so I'm
I'm hopeful that this is going to make things a little bit better, but it's not in the way
that I think most people are asking that question.
I don't think there's going to be a widespread epiphany from the base of these shows.
Anyway, it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}