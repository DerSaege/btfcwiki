---
title: Let's talk about what happened in Kansas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8WAGPMtg6GU) |
| Published | 2022/08/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the events in Kansas surrounding voting behaviors and family planning rights.
- Mentions a video he previously made about growing up in rural Republican evangelical areas.
- Predicted a shift in voting behavior among women related to family planning rights.
- Details the voting patterns in Kansas counties in 2020.
- Notes the surprising drop in support for preserving family planning rights in some counties.
- Points out the high percentage of women in voter registrations since Roe was overturned.
- Talks about the unexpected reaction of political analysts to the voting trends.
- References the ranches and farms in Kansas to illustrate voting decisions.
- Mentions rural people's tendency to keep their political choices private.
- Concludes with a reflection on the voting behavior observed in Kansas.

### Quotes

- "This shouldn't have been a surprise."
- "If there is anything that rural people know how to do, it's shut up."

### Oneliner

Beau explains the voting trends in Kansas, predicting and analyzing the surprising shifts in rural areas regarding family planning rights, with insights into rural voter behavior dynamics.

### Audience

Voters, Political Analysts

### On-the-ground actions from transcript

- Preserve family planning options within communities (suggested)
- Encourage open dialogues on voting choices and political decisions (implied)

### Whats missing in summary

Insights on the broader implications of understanding rural voter behavior and its impact on political outcomes.

### Tags

#Kansas #VotingTrends #FamilyPlanningRights #RuralVoters #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about Kansas
and what happened in Kansas, whether or not
it should have been a surprise.
We're doing this because I had quite a few people ask why I
haven't put out a video about it,
to my way of thinking I did a couple months ago.
I put out a video talking about how
I grew up in rural Republican evangelical areas.
And that means I know a whole lot about two things, secrets
and hypocrisy.
And I said that I thought there were
going to be a whole lot of women who maybe they have something
they view as a dirty little secret in their past,
and they're going to have another one.
They're going to walk into that voting booth.
They're going to vote one way.
They're going to walk out and act
like that's not what they did.
That is what happened in Kansas.
So this is how it broke down.
In 2020, Biden won five counties in Kansas, five.
The vote to preserve the right to family planning,
19 counties voted a majority in favor of preserving that right.
It's a big shift.
Those were mostly urban and suburban areas.
There were some little rural patches in there,
but mostly rural and suburban.
But then you have some other things that are worth noticing.
In Hamilton County, as an example, in 2020,
Trump won with 81% of the vote.
Only 56% voted to get rid of that protection
so there could be a ban on family planning.
That's a drop of 25%.
25% people who are going to have dirty little secrets.
In Greeley County, Trump won that county with 85%.
60% voted in favor of removing those protections
so there could be a ban, a drop of 25%.
This shouldn't have been a surprise.
A lot of the political analysts are acting shocked by this.
Since Roe was overturned, since that decision was changed,
70% of voter registrations in Kansas have been women.
That should have been a sign.
This shouldn't have been shocking.
When you think about Kansas, you think about those ranches
and those farms.
You think about the ranch royalty out there.
You think the queen of that ranch
is really going to vote to remove the princess's options?
No.
They'll vote to preserve those options.
And then they'll walk out and never talk about it.
Because if there is anything that rural people know
how to do, it's shut up.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}