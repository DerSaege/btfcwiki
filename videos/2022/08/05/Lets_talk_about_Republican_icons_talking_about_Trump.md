---
title: Let's talk about Republican icons talking about Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g4RvcJoavKE) |
| Published | 2022/08/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commentary on the future of the Republican Party, focusing on figures like Karl Rove and Dick Cheney speaking out against Trump.
- Dick Cheney criticizes Trump, labeling him as a threat to the Republic and questioning his Republican identity.
- Cheney's statements suggest that Trump is an authoritarian using the Republican Party for his gain.
- Despite Cheney's own history, he recognizes Trump as a different, more extreme authoritarian figure.
- The Republican Party is urged to choose between being a legitimate political party or a cult of personality around Trump.
- Beau underscores the urgency for the Republican Party to make this decision promptly.

### Quotes

- "Dick Cheney has been a Republican since the 60s. He has photos of him chilling with Nixon. He is the Republican Party."
- "The Republican Party is facing a reckoning. They have to decide whether or not they want to be a legitimate political party, a conservative party."
- "They have to make it now. There's not a lot of time to think about this one. You had that time for six years and it was wasted."

### Oneliner

Beau delves into Dick Cheney's criticism of Trump, urging the Republican Party to choose between legitimacy and a cult of personality around Trump urgently.

### Audience

Republican voters

### On-the-ground actions from transcript

- Organize community dialogues to critically analyze the current state of the Republican Party and its future (implied).
- Reach out to fellow Republicans to have open and honest conversations about the direction of the party (implied).

### Whats missing in summary

Insight into Beau's perspective on how Republicans should navigate their party's future. 

### Tags

#RepublicanParty #Trump #DickCheney #Authoritarianism #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about the Republican Party and the future of the Republican Party.
And what's happening with icons from the Republican Party coming out of obscurity, coming out of retirement,
making statements about the current state of the Republican Party and what that really means.
You know, it all started, really, it really got rolling when Karl Rove came out and talked about Trump.
But here recently, Dick Birdshot Cheney came down from Mount Trickle Down,
where all of the other Republican demigods live, to talk about Trump.
And he said that he thought Trump was a coward, that he wasn't a real man, that he was rejected,
and most importantly, he said he was a threat to the Republic.
These icons of the Republican Party, they live by the 11th commandment,
as handed down by demigod Ronald Reagan.
And that commandment is, thou shalt not speak ill of thy fellow Republican.
But they're doing it now. Why?
Because Trump's not a Republican, and they know it.
They know it. Trump runs around, and undoubtedly, Trump and those like Trump
are going to call those people leveling these criticisms rhinos.
Dick Cheney, a rhino. Dick Cheney has been a Republican since the 60s.
He has photos of him chilling with Nixon.
He is the Republican Party.
And he says that Trump is a threat to the Republic because he recognizes that Trump isn't a Republican.
Trump's an authoritarian, trying to use the Republican Party to feather his own nest,
to gain power for himself and his kids.
That's what Dick Cheney is saying. He said he was trying to steal the election.
Dick Cheney is coming out and saying this because he knows Trump is an authoritarian.
And let's be super clear about this.
When I want to compare somebody to an authoritarian in the normal scope of American politics,
I compare them to Dick Cheney.
Trump is so much of an authoritarian that it even gave Dick Cheney pause.
Dick Cheney is not somebody who has ever really sided with small government,
who ever really sided with the idea of the working class.
Dick Cheney is an authoritarian himself.
But even he knows that Trump's a different kind.
And he's coming out and he's telling Republicans this.
Sure, it's in an ad for his daughter.
Yeah.
But that doesn't really change it, does it?
He's coming out and saying that Donald Trump and by extension those like him are a threat to the Republic.
It's something that Republicans should probably pay attention to.
And they need to think about how Trump rose to prominence.
It was through fear and hatred and by bashing the United States.
That's how he got where he is.
The Republican Party is facing a reckoning.
They have to decide whether or not they want to be a legitimate political party,
a conservative party, or they want to subjugate themselves in a cult of personality to one person
and those who can mimic him the best.
That's the decision that has to be made and they have to make it now.
There's not a lot of time to think about this one.
You had that time for six years and it was wasted.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}