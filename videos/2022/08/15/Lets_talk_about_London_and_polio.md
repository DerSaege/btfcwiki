---
title: Let's talk about London and polio....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=T0ai-jqiwQY) |
| Published | 2022/08/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Health officials in London are providing booster vaccines to children aged one through nine due to finding polio in the sewage of eight different boroughs.
- Despite not having many reported cases, the concern is that the virus is spreading, and severe cases may not have manifested yet.
- Most individuals infected with polio do not show symptoms, with only a small percentage suffering extreme symptoms.
- The genetically similar virus found in London has no clear connection to cases in New York.
- Health officials are urging everyone, especially children under five, to ensure their vaccinations are up to date.
- The presence of polio in London is mainly attributed to vaccine gaps and potential strains from countries still using the oral vaccine.
- It is emphasized that the vaccines used in the UK and the US do not present the same issues as the oral vaccine.
- The situation with polio is unexpected and should have been eradicated by now.
- Health officials are operating with caution due to the uncertainty surrounding the current spread of polio.
- Keeping vaccinations up to date is vital to prevent the spread of polio.

### Quotes

- "Make sure that the vaccines are up to date, because right now they don't know what's going on."
- "This is something that it should be gone. It should be gone."
- "The vaccines that are used in the United Kingdom and in the United States, they're not the oral vaccine anymore, and they don't have that same issue."

### Oneliner

Health officials in London are on high alert as they provide booster vaccines for children due to the unexpected resurgence of polio, stressing the importance of updated vaccinations to prevent further spread of the virus.

### Audience

Parents, caregivers, community members

### On-the-ground actions from transcript

- Ensure that your and your children's vaccinations are up to date (suggested)
- Stay informed about vaccine recommendations and updates (suggested)

### Whats missing in summary

Importance of community awareness and proactive measures in preventing the spread of polio. 

### Tags

#Polio #Vaccination #PublicHealth #London #Children #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about London and polio.
Health officials over there have made booster vaccines
available for children aged one through nine, I think.
It applies to roughly a million kids.
The reason they're doing this is they
found it in the sewage of eight different London boroughs.
That's not good news.
Now, one of the things that has come up in talking about this
is people saying, well, they're talking about it,
but they don't have cases, or they don't seem to.
Like in New York, as far as I know,
they're still just the one severe case.
But that's kind of how polio works.
I want to say it's only like one in 200
that become polio victims the way we think about them,
that suffer the extreme symptoms.
Most don't even know they had it.
They show no symptoms.
So I think the concern among health officials
is that right now it's spreading,
and the severe cases just haven't popped yet.
But they don't really know.
The virus that they found there is genetically
similar to the one they found here,
but they have no idea how it's connected.
They're saying because it was found in eight different
boroughs, it is relatively safe to assume
that this isn't an isolated thing with one close-knit group
because it's everywhere.
Well, not everywhere.
It's in eight boroughs.
So this tends to be worse in children under five,
and health officials there are strongly
suggesting that everybody get updated.
Make sure that the vaccines are up to date,
because right now they don't know what's going on.
They don't have a good handle on exactly what's happening,
and they're operating out of an abundance of caution
because it's polio.
This is something that it should be gone.
It should be gone.
This isn't something that should be showing up,
but here we are, mainly caused by vaccine gaps.
There is evidence that some of this, or at least the strain,
might have come from countries where they're still
using the oral vaccine, which can lead to shedding.
But the vaccines that are used in the United Kingdom
and in the United States, they're not the oral vaccine
anymore, and they don't have that same issue.
So just another reminder, make sure your vaccinations are
up to date, especially for your kids.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}