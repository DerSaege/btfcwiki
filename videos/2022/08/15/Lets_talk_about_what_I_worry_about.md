---
title: Let's talk about what I worry about....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YChQwoxJM0k) |
| Published | 2022/08/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message questioning his usual dismissal of concerns in the media.
- Identifies three major worries for people in the United States.
- First concern is climate change, urging action to cut emissions and mitigate effects.
- Points out cities already facing drastic policies due to climate change.
- Warns of the unstoppable consequences if action is delayed.
- Second worry is the rise of authoritarianism in the U.S.
- Notes a significant portion of the population desiring authoritarian leadership.
- Expresses concern about the difficulty of removing an authoritarian regime once established.
- Emphasizes the importance of pushing back against authoritarian tendencies.
- The third worry is the potential for catastrophic civil unrest in the U.S. due to accelerationists.
- Stresses that these concerns could escalate quickly and have long-lasting effects.
- Urges vigilance and action to prevent these scenarios from unfolding.

### Quotes

- "That's a worry. That's a real concern."
- "An authoritarian regime that really got seated in the United States would be very, very hard to dislodge."
- "Civil unrest in the United States would be catastrophic, and it would be very long-lasting."

### Oneliner

Beau warns about climate change, authoritarianism, and civil unrest in the U.S., urging vigilance and action to prevent catastrophic consequences.

### Audience

United States citizens

### On-the-ground actions from transcript

- Advocate for policies to cut emissions and mitigate the effects of climate change (implied)
- Push back against authoritarian tendencies through activism and awareness (implied)
- Work towards preventing civil unrest by promoting unity and understanding in communities (implied)

### Whats missing in summary

Beau's detailed explanations and examples of each concern can be better understood by watching the full video.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're gonna talk about what worries me.
I got a message that basically said,
hey, every time something comes up
and it pops in the media,
you kind of show up and say, don't worry about that.
That's not really gonna be a big deal.
And so far I've been right,
but what is it that really worries me?
Three things.
There are three things that I think people
in the United States really need to be concerned about.
The first is climate change.
It's real.
We're experiencing the effects.
The longer it takes to cut emissions
and start mitigating, the worse it's going to get.
We already have major cities instituting policies
designed to show how bad things are.
There are cities in the Southwest
that are making some pretty drastic overtures
when it comes to the supply of water.
That will get worse.
And if we wait too long, it'll be unstoppable.
It will be unstoppable.
That is a worry.
That's a real concern.
That's something that people really need
to get on the ballot.
That's something that people should pay attention to.
Another is the political situation in the United States,
with large portions of the population
craving authoritarian leadership
while thinking they're opposing tyranny.
They want that strong man leader
apparently completely unaware of the fact
that that's historically been the case.
The fact that that's historically
how authoritarian regimes get their foot in the door.
An authoritarian regime that really got seated
in the United States would be very, very hard to dislodge.
It would probably take other countries to do it,
and it would be really bad.
That's a concern.
The drive towards authoritarianism in the United States.
That's something that we should really watch.
Now, luckily, there is a lot of movement on this.
There are a lot of people who are pushing back.
We just need to make sure that that's the side that wins.
And then the third thing that worries me
is that accelerationists aren't careful what they wish for,
and they get it,
because civil unrest in the United States
would be catastrophic,
and it would be very long-lasting.
Those are the three things that I actually see as real concerns,
as things that if we're not careful and they start,
we may not be able to just pull back from easily.
Those are things that once they begin,
they have to run their course,
and there will be a lot of loss as that course is run.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}