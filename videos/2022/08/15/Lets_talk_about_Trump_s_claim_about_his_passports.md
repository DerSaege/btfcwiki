---
title: Let's talk about Trump's claim about his passports....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NOP531r6hNc) |
| Published | 2022/08/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses skepticism towards former President Donald Trump's claim about his passports, based on Trump's credibility.
- Trump alleged that his three passports, one expired, were stolen by the FBI during a raid at Mar-a-Lago.
- Beau clarifies the outdated meaning of "third world," correcting misconceptions about the term.
- If Trump's claim is true, Beau speculates that the investigation may be more advanced than public knowledge suggests.
- Beau outlines alternative explanations for the missing passports, including the possibility of diplomatic or technical reasons.
- The seizure of Trump's passports could signify serious legal trouble for him, restricting his ability to leave the country.
- Beau raises questions about the timing and implications of the alleged passport seizure.
- He suggests that such actions typically occur during a hearing after an individual has been taken into custody.
- Despite the seriousness of the situation, Beau encourages caution in drawing definitive conclusions without considering other possible explanations.
- If the FBI intentionally took Trump's passports without a technical reason, Beau indicates that it could spell significant trouble for Trump.

### Quotes

- "If the Feds actually took his passports and intended to, and it wasn't a technical thing dealing with the archives or something like that, he's in a lot of trouble."
- "I don't believe anything that he says based solely on the fact that he said it."
- "That's a literal statement of, hey, you don't get to leave the country."
- "It does look like for a while we will be running on that for a video schedule."
- "Y'all have a good day."

### Oneliner

Beau questions Trump's claim about stolen passports, hinting at potential legal trouble and advanced investigations while urging cautious interpretation of the situation.

### Audience

Political analysts, News enthusiasts

### On-the-ground actions from transcript

- Monitor developments in the investigation into Trump's passports (suggested)
- Stay informed about the evolving news related to this incident (suggested)

### Whats missing in summary

Insights on the broader implications of Trump's passport situation and the potential legal consequences for him.

### Tags

#DonaldTrump #Passports #FBI #Investigation #LegalTrouble


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about a statement,
a claim by former president Donald J. Trump
about his passports.
Before we get into this, I do want
to start by saying and remind everybody
that if Trump told me he was wearing a red tie
and I could visibly see that red tie,
I would still doubt that he was actually wearing a red tie.
I don't believe anything that he says based solely
on the fact that he said it.
That being said, this is what he said.
Wow.
In the raid by the FBI on Mar-a-Lago,
they stole my three passports, one expired,
along with everything else.
This is an assault on a political opponent
at a level never seen before in our country, third world.
First, just because it's just one of those things with me,
third world does not mean what people think it means.
First world, NATO and their allies,
US-aligned capitalist countries during the Cold War.
Second world, Soviet-aligned countries and their allies
to include China at the time.
Communist countries.
Third world, non-aligned, Ireland, Switzerland, Finland.
These were all third world countries.
It's an obsolete term that should probably
stop being used.
OK.
So let's just say that this is true for a moment.
The investigation is much further than we think.
If the Feds took his passports and did so intentionally,
this is...
the investigation is further along than we might believe.
Now, are there other explanations for this statement?
Sure.
One is he may have been a terrorist.
One is he made it up.
Another is that these passports were diplomatic
or some technical reason for them
to take them because they actually
would be belonging to the National Archives.
That's also a possibility.
But if this was an actual seizure of his passports,
that is really bad news for Trump.
That's a literal statement of, hey,
you don't get to leave the country.
That's a sign that something is pending.
Normally, as far as I know, this phase of things
where they actually do this, this
tends to take place at a hearing after they've already
been taken into custody.
So I have a lot of questions.
There are a lot of possible explanations,
assuming that what he is saying is true.
I wouldn't read too much into this,
other than there are other explanations.
But if the Feds actually took his passports
and intended to, and it wasn't a technical thing dealing
with the archives or something like that,
he's in a lot of trouble.
So that's the breaking development for today.
It does look like for a while we will
be running on that for a video schedule,
just to try to keep up with all of the news.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}