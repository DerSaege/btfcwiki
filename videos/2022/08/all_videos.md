# All videos from August, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-08-31: Let's talk about the explanation from Trump America deserves.... (<a href="https://youtube.com/watch?v=eDyqvJlOOYM">watch</a> || <a href="/videos/2022/08/31/Lets_talk_about_the_explanation_from_Trump_America_deserves">transcript &amp; editable summary</a>)

Trump's team offers excuses, not explanations, for retaining sensitive documents, leaving unanswered questions about national security and accountability.

</summary>

"The American people want to hear and deserve an explanation."
"Why couldn't he take the time to protect American lives? Was golf really that important?"
"We're not even talking about criminal liability at this point."

### AI summary (High error rate! Edit errors on video page)

Trump and his team have provided a litany of excuses but no explanations for the retention of sensitive documents.
Even if the documents were inadvertently transferred to Trump's club, there's been no effort to return them.
The lack of explanation raises questions about putting America first and protecting sensitive national security documents.
The focus is on why these documents were retained despite knowing they posed a risk to national security.
The absence of a clear reason for keeping the documents is what the American people deserve to hear.
The issue isn't just about criminal liability but about providing a straightforward answer to the public.
Supporters of Trump may also be curious about the purpose behind keeping these documents and prioritizing golf over national security.
The core question revolves around why Trump couldn't take the time to protect American lives by returning the sensitive documents.
There's a call for transparency and accountability in handling these confidential materials.
The lack of a genuine explanation remains a critical point that needs to be addressed.

Actions:

for american citizens,
Contact elected representatives to demand transparency on the handling of sensitive documents (suggested)
Join advocacy groups pushing for accountability and clarity in government actions (implied)
</details>
<details>
<summary>
2022-08-31: Let's talk about Trump, the DOJ response, and photos.... (<a href="https://youtube.com/watch?v=ODEsDBxgML0">watch</a> || <a href="/videos/2022/08/31/Lets_talk_about_Trump_the_DOJ_response_and_photos">transcript &amp; editable summary</a>)

Trump's request for document scrutiny met with DOJ response; documents found in office prompt suspicion, investigation ongoing.

</summary>

"Kick Rocks Kid."
"Given what we have now publicly seen, it seems as though they may have to investigate more."
"I understand those people who saw that photo, saw those documents all over the ground, and immediately wanted to see cuffs."
"If you can come up with a scenario in which those documents got there by accident in the shape that they were in, I would love to hear it."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump requested a special master to review documents, DOJ responded that Trump didn't have standing because the documents belong to the US government.
DOJ detailed attempts to quietly retrieve the documents, contrasting Trump team's claims of returning everything with the large number of documents found by feds.
The DOJ's response can be summarized as "Kick Rocks Kid," conveying a message to sit down and be quiet.
Some of the documents were found in Trump's office, leading Beau to believe their presence was intentional rather than accidental.
Beau expresses patience for further investigation despite calls for immediate action, as the situation may require more scrutiny.
Speculation arises about the potential espionage implications of the documents found.
Beau criticizes the Republican Party's response, focusing on framed Time magazines in the photo rather than the seriousness of the document situation.
Even if a special master is appointed, it only delays the case for Trump without offering significant help.
Intelligence community conducting a damage assessment indicates a prolonged process.
The documents found may involve human sources, suggesting a sensitive nature that warrants careful handling.

Actions:

for legal analysts,
Contact legal experts for insight on the implications of the document situation (suggested).
Stay informed on the developments and investigations regarding the documents (suggested).
Await further updates on the case before drawing conclusions (implied).
</details>
<details>
<summary>
2022-08-30: Let's talk about former Secret Service agent Tony Ornato.... (<a href="https://youtube.com/watch?v=O3cQuDb0MX4">watch</a> || <a href="/videos/2022/08/30/Lets_talk_about_former_Secret_Service_agent_Tony_Ornato">transcript &amp; editable summary</a>)

Former Secret Service agent Tony Ornata's retirement sparks speculation and questions about his past duties and future whereabouts, potentially impacting his return before the committee.

</summary>

"A lot is probably going to be made of his departure."
"You will see this material again in the future as things play out."
"This timeline, this information, just kind of file it away."

### AI summary (High error rate! Edit errors on video page)

Former Secret Service agent Tony Ornata's retirement has been announced.
Ornata served as Trump's deputy chief of staff for operations before returning to the Secret Service.
Ornata was at the center of a lot of testimony and questions after Hutchinson's testimony before the January 6th committee.
Speculation suggests Ornata may need to return to answer more questions.
James Murray, the Secret Service director, announced his retirement last month.
President Biden announced Kimberly Cheadle as Murray's replacement.
Ornata, having been with the Secret Service for 25 years, is leaving for the private sector.
Ornata's responsibilities included training new agents, raising concerns given the circumstances.
Speculation surrounds where Ornata will work next, with rumors suggesting not with Trump's companies.
There may be challenges in getting Ornata to return before the committee as a private citizen rather than a federal employee.

Actions:

for politically aware individuals,
Follow updates on Tony Ornata's future endeavors to stay informed (suggested).
Stay engaged with news regarding the Secret Service for further developments (suggested).
</details>
<details>
<summary>
2022-08-30: Let's talk about Trump's request for a Special Master.... (<a href="https://youtube.com/watch?v=CyZgwX9qtB4">watch</a> || <a href="/videos/2022/08/30/Lets_talk_about_Trump_s_request_for_a_Special_Master">transcript &amp; editable summary</a>)

Trump's request for a special master to handle documents may serve as a delay tactic, with questionable impact and transparency concerns, possibly affecting the investigation politically.

</summary>

"This isn't going to have a real impact on the case."
"It's a delay tactic."
"Might not have been a good move politically."
"We're about to find out how committed Trump is to these talking points about transparency."
"The odds of it delaying it beyond the midterms are pretty slim."

### AI summary (High error rate! Edit errors on video page)

Trump requested a special master outside of the Department of Justice to go through documents taken from his club before investigators access them.
DOJ filter teams already identified items they thought might be privileged, but the investigation doesn't rely on privileged documents.
Trump’s request may be a delay tactic or to benefit high-profile friends facing similar situations.
The judge wants a detailed inventory of items taken from Trump's residence, to be submitted under seal.
U.S. Intelligence and Counterintelligence are also reviewing the documents due to their classified nature.
The request for a special master might not significantly impact the investigation.
There's a hearing scheduled on Thursday regarding this matter.
Transparency may become an issue once the inventory is provided.
This move could potentially delay the investigation until right before the midterms, though delaying beyond that is unlikely.
Politically, this delay tactic might not be advantageous for Trump.

Actions:

for legal analysts, political commentators,
Attend the hearing scheduled on Thursday to stay informed (implied).
Monitor updates on the judge's request for a detailed inventory (implied).
Stay engaged with news on the investigation and potential delays (implied).
</details>
<details>
<summary>
2022-08-30: Let's talk about Trump's legal fees, Truth social, and dumpster fire.... (<a href="https://youtube.com/watch?v=Q62KWzaXS-g">watch</a> || <a href="/videos/2022/08/30/Lets_talk_about_Trump_s_legal_fees_Truth_social_and_dumpster_fire">transcript &amp; editable summary</a>)

Beau reveals how the lack of financial support from the Republican Party in Trump's legal case could lead to significant political shifts in the future.

</summary>

"The Republican Party is not covering legal fees for that little caper down there at his club."
"This money that Republican Party has been using to pay these legal bills. It's also money they've been using to keep a leash on Trump."
"He's kind of off the leash of the Republican Party."
"Trump might see that as a huge betrayal."
"Today's events, at least the events that became public today, they're probably going to shape things in the future."

### AI summary (High error rate! Edit errors on video page)

Trump's Truth Social app is not on Google Play Store due to lack of content moderation for extreme content.
The former president contributes to the negative impression by reposting content from Q people.
This news is detrimental for the already financially troubled app.
The Republican Party is not covering legal fees for an incident at Trump's club, unlike past cases.
Historically, the Republican Party has been paying legal fees for Trump, which has kept him in line.
Without funding for this particular case, Trump may see it as a betrayal and potentially break away from the Republican Party.
Trump could lead the MAGA faithful in forming a separate, more significant party if he decides to part ways with the Republicans.
Today's events and public reporting may shape future political dynamics.
Trump might interpret the lack of financial support in his current legal case as a significant betrayal.
This situation could lead Trump to act independently of the Republican Party.

Actions:

for political observers, republican party members,
Speculate on the potential consequences of Trump breaking away from the Republican Party (implied)
Stay informed about the evolving political landscape and potential party dynamics (implied)
Monitor Trump's actions and statements for signs of independent political moves (implied)
</details>
<details>
<summary>
2022-08-30: Let's talk about Georgia, Kemp, and a delay.... (<a href="https://youtube.com/watch?v=E418mc3TUsM">watch</a> || <a href="/videos/2022/08/30/Lets_talk_about_Georgia_Kemp_and_a_delay">transcript &amp; editable summary</a>)

The special grand jury in Georgia's investigation into election interference delays due to Governor Kemp's testimony, escalating political stakes to the national level.

</summary>

"The judge says that the governor has to talk to the special grand jury promptly after midterms."
"In the judge's attempt to reduce the political nature of the investigation within Georgia, kind of just got punted up to the national stage."
"So anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Special grand jury in Georgia investigating election interference wanted to hear from Governor Kemp.
Governor Kemp tried to avoid the subpoena, but the judge insisted he testify.
Judge concerned about political influence leading up to midterms if Kemp testified.
Ruling requires Governor Kemp to speak to the grand jury after midterms, delaying resolution.
Possibility of no resolution until after the new year if grand jury needs Kemp's testimony.
Delay could impact political decisions, especially Trump's announcement.
Judge's attempt to reduce political nature of investigation may have escalated it to national level.

Actions:

for georgia residents, political analysts,
Contact local representatives for updates on the grand jury investigation (implied)
Stay informed about the political developments in Georgia and beyond (implied)
</details>
<details>
<summary>
2022-08-29: Let's talk about the predictions about Trump's indictment.... (<a href="https://youtube.com/watch?v=YO7y6Z-_Bug">watch</a> || <a href="/videos/2022/08/29/Lets_talk_about_the_predictions_about_Trump_s_indictment">transcript &amp; editable summary</a>)

Beau questions the odd political implications and potential motivations behind a talking point predicting chaos if the former president were indicted.

</summary>

"Why? That's weird. That's a weird talking point."
"I don't understand the talking point."
"It's a bad talking point for a whole bunch of reasons."

### AI summary (High error rate! Edit errors on video page)

Questions the intent behind a new talking point coming from the right regarding potential consequences if the former president were to be indicted.
Points out that the talking point suggesting pandemonium in the streets doesn't make sense politically, given the former president's political acumen.
Contemplates what might happen if the former president is indicted and speculates on whether he'd take a plea deal or go to trial.
Considers the composition of a potential jury in a trial involving the former president, noting the presence of Trump loyalists.
Raises concerns about the talking point potentially inciting riots or violent responses.
Suggests that those pushing the talking point may believe in overwhelming evidence of guilt against the former president.
Questions the logic behind trying to stop the indictment if the evidence is weak, as a trial could re-energize the former president politically.
Concludes with uncertainty about the true motivations behind the talking point and its potential implications.

Actions:

for political analysts, activists,
Monitor and address potentially incendiary talking points in political discourse (suggested)
Advocate for fair and unbiased legal processes (implied)
</details>
<details>
<summary>
2022-08-29: Let's talk about a story out of Tennessee.... (<a href="https://youtube.com/watch?v=SgomADVb-9E">watch</a> || <a href="/videos/2022/08/29/Lets_talk_about_a_story_out_of_Tennessee">transcript &amp; editable summary</a>)

Former Tennessee house speaker and chief of staff face serious corruption charges involving a fabricated identity and monetary crimes, likely leading to national scrutiny and potential cooperation for broader revelations.

</summary>

"It involves a former house speaker and their former chief of staff."
"The truly wild part to me is that all of this, this giant story that is twisting and all of these allegations stem from what appears to be like 50 grand or so."
"If these were my clients, I would tell them to make preparations to go away."
"There may be motivation for them to cooperate if they do actually know something about more widespread corruption."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A former house speaker and chief of staff from Tennessee, Cassata and Cothra, have been charged with around 20 counts by the feds, starting from a simple white lie and escalating to serious crimes involving public corruption.
The allegations involve a company initially hired for political mailers during primaries, but then started doing business with the General Assembly, leading to accusations of enriching themselves improperly.
To carry out their scheme, Cassata and Cothra allegedly created a fictional person named Matthew Phoenix, with text messages discussing who should impersonate him on the phone, culminating in kickback schemes and money laundering charges.
Despite the relatively small amount of money involved (around 50 grand), the case has complex layers and serious implications, likely to attract national media attention if it proceeds to trial.
The evidence against the defendants seems strong, with a document-heavy case that could lead to severe penalties if proven guilty.
Speculation suggests that the federal government may be pressuring Cassata and Cothra to cooperate and reveal information about others involved in corruption, potentially indicated by the public nature of their arrests.
The unusual manner of arrest, without the usual privacy afforded to political figures, hints at hidden information in the government's case or a strategic move to compel cooperation from Cassata and Cothra.
The severity of the charges means there is little room for leniency, possibly motivating the accused to cooperate if they possess knowledge of broader corruption.
Despite the entertaining and bizarre aspects of the case, the charges are serious, indicating a potential for wider implications if cooperation leads to uncovering more corruption.
Beau predicts that this case will gain national attention as more details emerge, with increased media coverage expected.

Actions:

for legal observers,
Stay informed on the developments of this case and similar instances of alleged corruption (exemplified)
Advocate for transparency and accountability in political processes (implied)
</details>
<details>
<summary>
2022-08-29: Let's talk about Trump and witch hunts.... (<a href="https://youtube.com/watch?v=bGurLYv7-FY">watch</a> || <a href="/videos/2022/08/29/Lets_talk_about_Trump_and_witch_hunts">transcript &amp; editable summary</a>)

Beau debunks the notion of a "witch hunt" in Trump's FBI document search, citing thorough process and lack of publicity as evidence against it, reflecting on the embarrassment caused by mishandling sensitive documents.

</summary>

"That's not a witch hunt unless it's a witch hunt at the Sanderson sisters' house."
"That spell that forces people to attempt to protect the institution of the presidency, man, that spell is still active in D.C."
"It's not a witch hunt. Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of a "witch hunt" as looking for something that isn't really there or trying to embarrass someone by searching for something you know isn't there, typically targeting those with opposing beliefs.
Analyzes the situation with Trump and the FBI's search for documents, debunking the idea of it being a witch hunt.
Details the FBI's thorough process of obtaining a warrant and finding exactly what they were looking for, contrasting it with the typical notion of a witch hunt.
Suggests that if Trump had cooperated and not turned it into a political issue, the situation might have been resolved without public attention.
Points out that Trump's actions and desire for drama led to the embarrassing situation of sensitive documents being mishandled, reflecting poorly on the presidency and the country.

Actions:

for political observers, truth-seekers,
Stay informed and critically analyze political situations (implied)
Advocate for transparency and accountability in governmental actions (implied)
Support measures to safeguard sensitive information (implied)
</details>
<details>
<summary>
2022-08-28: Let's talk about the Florida governor's race.... (<a href="https://youtube.com/watch?v=hcNduOXZ_9g">watch</a> || <a href="/videos/2022/08/28/Lets_talk_about_the_Florida_governor_s_race">transcript &amp; editable summary</a>)

Beau breaks down the Florida governor's race as a mini 2020 presidential election, with DeSantis facing high stakes and the Democratic victory hinging on negative voter turnout against him.

</summary>

"This is a mini 2020 presidential election."
"My guess is that's not just empty words."
"The stakes are incredibly high for DeSantis."
"I can't make a prediction on this."
"The stakes are pretty high for DeSantis."

### AI summary (High error rate! Edit errors on video page)

Florida governor's race is being compared to a mini 2020 presidential election, with DeSantis as a mini Trump and Crist as a mini Biden.
Democratic party faces enthusiasm issues with Crist, seen as electable but lacking in voter drive.
Democratic victory strategy banks on negative voter turnout against DeSantis, similar to how people voted against Trump.
DeSantis faces the challenge of potential sabotage from the real Trump before the election.
Nikki Fried's main goal is to oust DeSantis, rallying behind Crist to achieve this.
DeSantis must win Florida convincingly to maintain his presidential aspirations.
Uncertainty surrounds the election outcome due to variables like negative voter turnout and real Trump's actions.

Actions:

for florida voters,
Rally behind Charlie Crist to defeat DeSantis (implied)
Stay informed about the candidates' positions and actions (suggested)
</details>
<details>
<summary>
2022-08-28: Let's talk about access to higher education.... (<a href="https://youtube.com/watch?v=D2NDt1yJzDk">watch</a> || <a href="/videos/2022/08/28/Lets_talk_about_access_to_higher_education">transcript &amp; editable summary</a>)

Beau explains opposition to free higher education as perpetuating class barriers, challenging the fear of competition among the privileged, and advocating for breaking down social divides.

</summary>

"It's credentialing. It's a club pass. It gets you into the club where the higher paying jobs are and that's it."
"Maybe somebody will get a little bit of social mobility out of it."
"Everybody's getting awfully scared of a little bit of competition all of a sudden."
"The system's broke in case you haven't figured it out."
"We pretend these class barriers don't exist."

### AI summary (High error rate! Edit errors on video page)

Explains the opposition to universal access to higher education and free college.
Describes how some view degrees as membership cards for higher paying job clubs.
Points out the class barrier maintained by keeping education expensive.
Talks about the fear of competition from bright, poor students by mediocre, upper-class kids.
Emphasizes the need to break down class barriers and level the playing field in education.
Challenges the notion of meritocracy and exposes the fear of competition among the privileged.
Addresses the existence of class barriers in the United States and the resistance to breaking them down.

Actions:

for education advocates, activists,
Advocate for policies that support universal access to higher education (suggested)
Support scholarships and financial aid programs for students from lower socioeconomic backgrounds (implied)
</details>
<details>
<summary>
2022-08-28: Let's talk about Trump's viability in 2024.... (<a href="https://youtube.com/watch?v=fmFUiw5mLvc">watch</a> || <a href="/videos/2022/08/28/Lets_talk_about_Trump_s_viability_in_2024">transcript &amp; editable summary</a>)

The future of the Republican party hinges on taking control from extremist elements loyal to Trump before midterms to prevent a damaging split.

</summary>

"It's not a question about Trump anymore. It's a question of whether the Republican party can show the courage to take control of its own party."
"If they do that, they will be able to sustain it for quite some time. They'll fall into an echo chamber."
"That crew of Republicans that wins their elections, that are really loyal to Trump rather than loyal to the Republican party or even the country, they will view it as their chance."
"The Republican leadership has to show some backbone. And if they don't, they're going to regret it."
"But if the Republican party doesn't take control before the midterms, they will carry a significant portion of the Republican base with them."

### AI summary (High error rate! Edit errors on video page)

Trump's political days are likely over in terms of getting into office, but he remains a powerful force as the face of Trumpism.
The real question is not if Trump will be viable in 2024, but if the Republican party will be able to survive and thrive.
The Republican party is running out of time and must address extremist elements within the party before the midterms.
If Trump-aligned candidates take power without Trump in office, there's a risk they may split off into a third party, leading to long-term sustainability.
This split could lead to an echo chamber effect, sustained by money and self-reinforcement, causing the Republican party to become a zombie.
It is imperative for the Republican party to show courage by taking control of its own party and adjusting certain talking points.
Some Republican candidates are already changing their views on certain issues, like women's health, indicating a potential shift in party ideology.
Established politicians within the Republican party need to step up and lead, especially in dealing with the influence of the MAGA movement.
The prospect of a third party splitting from the Republicans might give certain politicians temporary leadership roles, but long-term success is doubtful without Trump.
Failure by Republican leadership to take control before the midterms could result in a significant split within the party, with loyalists to Trump leaving with a portion of the base.

Actions:

for republican party members,
Take control of the Republican party and address extremist elements before the midterms (implied)
Show backbone and leadership within the party to prevent a damaging split (implied)
</details>
<details>
<summary>
2022-08-27: Let's talk about a term Biden used.... (<a href="https://youtube.com/watch?v=2mCee-jXmzY">watch</a> || <a href="/videos/2022/08/27/Lets_talk_about_a_term_Biden_used">transcript &amp; editable summary</a>)

Beau questions the appropriateness of Biden using the term "semi-fascism," urging people to understand the situation for what it is.

</summary>

"I don't know if it's a good idea politically, because it may be too much coming from Biden."
"When it comes to stuff like that, I think it's important for people to understand what they're dealing with."
"As far as the characteristics, the general philosophy, the undertones, the structure, goals. Yeah, I mean, it's an accurate term."
"So anyway, it's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau revisits Biden's recent speech and the term "semi-fascism" used.
A girlfriend introduced Beau's channel to her boyfriend during the Russia-Ukraine conflict.
The boyfriend was curious about Biden using the term "semi-fascism."
Beau explains that the term can be misunderstood due to people associating it with a specific subset from World War II Germany.
He mentions that not all fascists fit that specific image.
Beau references Lawrence Britt and Umberto Eco for discussing the characteristics of fascism.
He suggests watching his videos on Trump's time in office to understand the term better.
Beau believes that "semi-fascism" accurately describes Trumpism as an Americanized version of fascism.
He points out that Biden's use of the term might bother some people due to his usual mellow rhetoric.
Beau expresses the importance of calling things what they are to understand the situation better.

Actions:

for political analysts,
Watch videos discussing the characteristics of fascism for better understanding (suggested)
Engage in political discourse to understand terminology and positions better (implied)
</details>
<details>
<summary>
2022-08-27: Let's talk about Ukraine, Russia, and DOD naming it.... (<a href="https://youtube.com/watch?v=FywxSyMG294">watch</a> || <a href="/videos/2022/08/27/Lets_talk_about_Ukraine_Russia_and_DOD_naming_it">transcript &amp; editable summary</a>)

The US Department of Defense launches Operation Ukrainian Shield to assist Ukraine with a structured military approach, signaling commitment and pressuring Russia to decide on their actions.

</summary>

"Why don't ranchers name their livestock? It's harder to do something once something has a name, right?"
"The message is, you've got the watch. Ukraine has the time."

### AI summary (High error rate! Edit errors on video page)

The US Department of Defense announced a new operation to assist, train, and arm Ukrainian defenses, giving it a name, although the name hasn't been disclosed yet.
The operation is aimed at establishing a centralized structure with a one to three-star general in command to set up command and control and a logistical network.
This new structured military approach replaces the previous haphazard civilian efforts, making it easier to facilitate actions.
The named operation signifies a commitment to assisting Ukraine until the situation resolves itself.
By establishing a structured bureaucracy, equipment delays for Ukraine can be minimized, ensuring smoother flow of necessary supplies.
The message to Moscow is clear: Russia will have to fight for Ukraine if they want it, as the US and likely allied countries are committing to assisting Ukraine through this operation.
The operation's structure will provide clarity on the strategy and strength of the offensive, defensive, or resistance efforts that will be implemented.
Moscow's response seems to be a minor increase in military size, possibly indicating a strategy to buy time rather than significant action.
The United States is seemingly committed to aiding Ukraine for the foreseeable future, with a promise to revisit the topic once command and control structures are in place.

Actions:

for foreign policy analysts,
Support Ukrainian Aid Efforts (exemplified)
Stay Informed on Ukraine-Russia Relations (exemplified)
</details>
<details>
<summary>
2022-08-27: Let's talk about Trump demanding McConnell be replaced.... (<a href="https://youtube.com/watch?v=i2K5BdfakF4">watch</a> || <a href="/videos/2022/08/27/Lets_talk_about_Trump_demanding_McConnell_be_replaced">transcript &amp; editable summary</a>)

Trump's clash with McConnell could deepen the divide between Trumpism and Senate Republicans, potentially diminishing Trump's influence within the party.

</summary>

"Trump needed McConnell. McConnell didn't need him."
"One of the first rules of command is to never give an order that won't be followed."
"If the Republican Party doesn't do something about McConnell, they're basically saying that Trump doesn't matter anymore."
"The last thing he needed was to pick a fight with Republicans who are in office."
"Looks like it's already happened."

### AI summary (High error rate! Edit errors on video page)

Trump needed McConnell's help for his candidates, but McConnell didn't comply.
Trump accused McConnell of being a pawn for Democrats and called for a new Republican leader.
Overplaying his hand, Trump might have caused further division between him and Senate Republicans.
Ignoring Trump's orders could continue if it happens once, damaging Trump's influence.
Despite the situation, it's unlikely that McConnell will be immediately replaced by Senate Republicans.
Failure to act against McConnell could signal that Trump is losing relevance within the Republican Party.
Picking fights with Republicans in office could worsen Trump's political standing.
Trump's legal issues, social media network problems, and candidate failures are adding pressure.
Trump's inconsistency is notable, but overplaying his hand is a rare misstep.
Despite the situation, McConnell is likely to remain in his position for the foreseeable future.

Actions:

for political observers,
Monitor developments in the Trump-McConnell feud (implied).
Stay informed about the dynamics between Trump, McConnell, and Senate Republicans (implied).
</details>
<details>
<summary>
2022-08-26: Let's talk about a surprising internet development for Trump.... (<a href="https://youtube.com/watch?v=nmADwjSogjI">watch</a> || <a href="/videos/2022/08/26/Lets_talk_about_a_surprising_internet_development_for_Trump">transcript &amp; editable summary</a>)

News about Truth Social's economic difficulty, involving a $1.5 million debt, raises doubts about its future, leaving supporters of the former president searching for alternatives like Twitter.

</summary>

"News about Truth Social's economic difficulty is surprising most Americans."
"The lack of revenue or payment to vendors indicates a significant issue with the platform."
"The potential decline of Truth Social could open up space for other platforms like Twitter."
"Despite some anticipating Truth Social's struggles, many find it hard to believe in light of Trump's business success."
"The economic troubles facing Truth Social bring uncertainty about its future and viability."

### AI summary (High error rate! Edit errors on video page)

News about Truth Social's economic difficulty is surprising most Americans.
Truth Social, a social media giant founded by former President Donald J. Trump, reportedly owes their web host over $1.5 million.
The project's failure is unexpected considering the successful individuals involved.
The lack of revenue or payment to vendors indicates a significant issue with the platform.
The company's struggle to generate revenue may lead to its downfall.
Supporters of the former president may lack alternative platforms for similar information.
The potential decline of Truth Social could open up space for other platforms like Twitter.
Despite some anticipating Truth Social's struggles, many find it hard to believe in light of Trump's business success.
The economic troubles facing Truth Social bring uncertainty about its future and viability.
The situation raises questions about who will step in to fill the potential void left by Truth Social.

Actions:

for tech industry observers,
Monitor the developments in Truth Social's financial situation and its impact on the tech industry (implied).
</details>
<details>
<summary>
2022-08-26: Let's talk about Trump's unsealed affidavit.... (<a href="https://youtube.com/watch?v=QCm9-DilILA">watch</a> || <a href="/videos/2022/08/26/Lets_talk_about_Trump_s_unsealed_affidavit">transcript &amp; editable summary</a>)

Beau details unsealed documents leading to Mar-a-Largo search warrant, noting lack of new info, discrepancies, and judge's approval despite Trump team's arguments.

</summary>

"The judge saw that and provided the warrant anyway."
"That is, that's a dead talking point now."
"But it depends on whether or not they want to."

### AI summary (High error rate! Edit errors on video page)

Overview of unsealed documents leading to a search warrant at Mar-a-Largo.
Lack of new information due to the DOJ doing their job properly.
Media likely to focus on new stamps on classified documents, but definitions are available in the affidavit.
Discrepancy between the Trump team's and DOJ's version of events regarding why the closet was locked.
DOJ included a letter from Team Trump in the warrant application, stating authority to declassify documents.
Judge still approved the warrant despite the letter, indicating it doesn't affect the case.
Technicality about the president not being a specific role under the laws cited.
The judge considered all arguments before issuing the warrant, making certain points moot.
Key point: Judge authorizing search of a former president's home after reviewing arguments.
Nothing in the documents significantly alters previous commentary on the channel.
Speculation on DOJ's next steps and potential charges.
Uncertainty on whether charges will be pursued or if DOJ will continue investigating for wider charges.
Waiting to see DOJ's next move.

Actions:

for legal analysts and concerned citizens.,
Stay informed on legal proceedings and developments (implied).
Support transparency and accountability in government actions (implied).
</details>
<details>
<summary>
2022-08-26: Let's talk about Biden's student debt relief policy.... (<a href="https://youtube.com/watch?v=eNhGoVucluE">watch</a> || <a href="/videos/2022/08/26/Lets_talk_about_Biden_s_student_debt_relief_policy">transcript &amp; editable summary</a>)

Beau breaks down myths and challenges the opposition, advocating for universal higher education as a benefit to society.

</summary>

"You paid yours off? Well, that's probably because of the way you voted."
"They're doing everything they can to get people to vote against their own interest."
"This helps those on the bottom."
"If you're opposing the idea of universal education, you're voting against your kids' interests."
"They want you easy to control."

### AI summary (High error rate! Edit errors on video page)

Explains the Biden administration's policy on student debt and universal access to higher education.
Debunks the myth that the working class will be paying off the student loan debt of doctors and lawyers.
Points out the hypocrisy of right-wing rhetoric regarding taxes and job creators.
Addresses the argument that removing student debt forgiveness is a tool for military recruitment.
Challenges those who say "but I paid mine" by suggesting it may be a result of voting against universal education.
Notes that universal access to higher education benefits society as a whole.
Advocates for universal higher education, even though the current policy doesn't go far enough.
Emphasizes that the right wing opposes education to maintain control over the populace.
States that those who oppose universal education are voting against their children's interests.
Argues that billionaires, as job creators, should bear the cost of higher education.

Actions:

for voters, students, advocates,
Advocate for universal higher education (implied)
Educate others on the benefits of universal access to education (implied)
</details>
<details>
<summary>
2022-08-26: Let's talk about Biden's CHIPs executive order.... (<a href="https://youtube.com/watch?v=2S6zKgNQckY">watch</a> || <a href="/videos/2022/08/26/Lets_talk_about_Biden_s_CHIPs_executive_order">transcript &amp; editable summary</a>)

Biden's executive order on semiconductor manufacturing aims to enhance US production while maintaining global trade balance and preventing economic tensions.

</summary>

"Decentralization is harder to disrupt, and it helps promote peace by keeping the economies talking to each other."
"Maintaining a balance with other countries so it doesn't start an economic arms race on the international scene."
"While that sounds good, it's actually not. Look at what happened with the baby formula."
"The interests of money tend to win out over a lot of other interests at times that might start a war."
"It's worth noting what the Biden administration needed to do to get it moving quickly is done."

### AI summary (High error rate! Edit errors on video page)

Biden signed an executive order to create a 16-person team to implement the CHIPS Act swiftly, which involves a $280 billion investment in US semiconductor manufacturing capabilities.
The domestic reason for this initiative is to secure the supply chain in case of global disruptions by ensuring US manufacturers have access to locally made components.
Another reason is to reduce China's leverage in the production of semiconductors and increase the amount of US chips available.
While boosting US production capability is beneficial, the goal is not to localize all production, as seen with the negative impact of localized production like the baby formula incident.
The initiative aims to strike a balance between enhancing US production capabilities, maintaining international trade relationships to prevent tensions, and avoiding an economic arms race.
Decentralization in production helps prevent disruptions and fosters peace by keeping economies interconnected.
The Biden administration has taken necessary steps to expedite the initiative, but it received limited attention due to the overshadowing news of student loan relief.

Actions:

for policy makers, tech industry leaders.,
Support policies that balance domestic production capabilities with international trade relationships (implied).
Advocate for initiatives that enhance national production without risking economic tensions (implied).
</details>
<details>
<summary>
2022-08-25: Let's talk about why state-wide elections are different.... (<a href="https://youtube.com/watch?v=JTLmeJ4UtWs">watch</a> || <a href="/videos/2022/08/25/Lets_talk_about_why_state-wide_elections_are_different">transcript &amp; editable summary</a>)

Beau dissects McConnell's unintentional admission on gerrymandering's impact on candidate quality and political manipulation in Senate and House races.

</summary>

"There's no districts. So that candidate really has to be of a higher quality."
"They don't make any sense. They're not based on population. They're not based on geography. They are just random lines."
"It's becoming more pronounced because for the Republican Party there are a lot of states where if there wasn't a very concerted effort to draw up favorable districts, well they wouldn't be purple states, they'd be blue states."
"Understand the Democratic Party does also engage in this. They just seem to be a wee bit more fair while they're, you know, definitely still trying to give themselves an edge."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Responds to a viewer's inquiry about Senator McConnell's statement regarding candidate quality in Senate vs. House races.
Views McConnell's statement as an unintentional admission of the importance of gerrymandering in House races.
Explains the difference in candidate quality requirement between Senate and House due to gerrymandering.
Points out how districts drawn during redistricting benefit the party in power, diluting opposition votes.
Notes that district maps are often nonsensical, not based on population or geography, but manipulated for political advantage.
Raises concern over how redistricting manipulations ensure party power rather than fair representation.
Observes that gerrymandering is a longstanding issue in the U.S., with increasing impact on state political leanings.
Mentions both Republican and Democratic parties' involvement in gerrymandering but suggests Democrats might be slightly less blatant.
Implies that McConnell's slip of information about gerrymandering may hint at the Republican Party's heavy reliance on it.
Concludes with a reflection, urging viewers to analyze the situation critically.

Actions:

for voters, political activists.,
Examine your state district maps to understand gerrymandering's impact (implied).
</details>
<details>
<summary>
2022-08-25: Let's talk about the Klamath River and foreshadowing.... (<a href="https://youtube.com/watch?v=L1qo0pTbWpU">watch</a> || <a href="/videos/2022/08/25/Lets_talk_about_the_Klamath_River_and_foreshadowing">transcript &amp; editable summary</a>)

The Klamath River faces tension as local and federal entities clash over water usage, endangered species protection, and the dynamics of money.

</summary>

"Each situation like this is going to be different, but there's going to be a lot of the same dynamics at play, because it's not just water, it's money."
"This is something that could turn ugly."
"We just have to watch it and see."
"How the feds are going to react to situations like this, and how the locals are going to react."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

The Klamath River is in focus due to water usage issues caused by a drought this year.
The Bureau of Reclamation manages water usage at the federal level, while local entities like the Klamath Irrigation District are involved locally.
Tension arises as the local entity refuses to comply with a federal order to shut down water deliveries, citing a lack of legal justification.
The area has a history of being unfriendly to the federal government, adding to the tension.
Protecting endangered species in the river is a critical concern, especially for the native culture living along the river.
Different local districts are handling the situation diversely, with some seeking alternative water sources while others resist federal orders.
The conflict over water usage between federal and local entities may escalate and set a precedent for future disputes.
The involvement of money adds a complex layer to the dynamics of the situation.
The outcome of how the Bureau of Reclamation handles this conflict will have broader implications for similar scenarios in the future.
The situation has the potential to become contentious and attract national attention.

Actions:

for environmental activists, policymakers,
Monitor the situation closely and stay informed on how the conflict between federal and local entities unfolds (implied).
Support local efforts to find alternative water sources and comply with federal orders to avoid escalation (implied).
</details>
<details>
<summary>
2022-08-25: Let's talk about some weird legislation.... (<a href="https://youtube.com/watch?v=tj7IcScwQvs">watch</a> || <a href="/videos/2022/08/25/Lets_talk_about_some_weird_legislation">transcript &amp; editable summary</a>)

Beau dissects legislation on UFOs, questioning if it's a cover for advanced tech surveillance or genuine alien belief.

</summary>

"Anyway, it's just a thought."
"This is about aliens, just so you know."
"Any sufficiently advanced technology is indistinguishable from magic or aliens."

### AI summary (High error rate! Edit errors on video page)

Analysis of strange legislation changing definitions regarding UFOs.
Definitions include transmedium objects and unidentified aerospace undersea phenomena.
Legislation requests reports on unidentified aerospace undersea phenomena activities.
Involvement of various government agencies in the investigation.
Two possible interpretations: searching for advanced technologies or believing in aliens.
Speculation that the program may be a cover for investigating near-peer technologies.
Emphasis on the idea that advanced technology can be mistaken for magic or aliens.

Actions:

for curious individuals, ufo enthusiasts, skeptics,
Stay informed about government legislation and activities related to UFOs (implied)
Engage in community dialogues about advanced technology and potential extraterrestrial phenomena (implied)
</details>
<details>
<summary>
2022-08-25: Let's talk about hunger stones and more.... (<a href="https://youtube.com/watch?v=adUmAUFMzOY">watch</a> || <a href="/videos/2022/08/25/Lets_talk_about_hunger_stones_and_more">transcript &amp; editable summary</a>)

Beau explains hunger stones, global drought effects, and the urgency of concrete climate action beyond promises.

</summary>

"We can't act like this isn't happening."
"It's not just Europe. It's not just the US. It is a real issue."
"We can't allow ourselves to look away on this one."
"We need that mobilization."
"As long as our emissions continue, it's going to get worse."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of hunger stones as markers near rivers in Europe that indicate famine when visible due to low water levels.
Mentions the current 500-year drought in Europe leading to various historical structures and artifacts becoming visible due to low water levels.
Points out similar situations in other parts of the world like China, where ancient Buddhist statues have become visible due to low water levels in the Yangtze.
Raises concerns about the increasing global population's impact on water resources and the potential effects of climate change.
Emphasizes the importance of taking real action beyond campaign promises to address climate change effectively.
Stresses the need for mobilization and concrete policies to combat climate change on a global scale.
Urges acknowledgment of the reality of climate change and the necessity of immediate action to prevent further deterioration.
Warns about the worsening effects of climate change if high emissions levels persist.
Calls for awareness and proactive measures to address the environmental challenges faced globally.

Actions:

for climate activists, policymakers, communities,
Advocate for concrete climate policies (implied)
Support initiatives for renewable energy transition (implied)
Raise awareness about climate change effects (implied)
</details>
<details>
<summary>
2022-08-24: Let's talk about sewer surveillance.... (<a href="https://youtube.com/watch?v=rp4Mi8EDzRw">watch</a> || <a href="/videos/2022/08/24/Lets_talk_about_sewer_surveillance">transcript &amp; editable summary</a>)

Beau explains using wastewater as a public health tool to track diseases, like polio and monkeypox, through the Sewer Coronavirus Alert Network, while addressing concerns about potential misuse and promoting a positive outlook on technology.

</summary>

"You could develop a public health tool that could give advanced warning of diseases showing up."
"It's a really good concept."
"Technology is not inherently good or bad. It is what it is."
"Sure, you can picture a dystopian future, or you can work to build a better one."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of using wastewater as a public health tool to provide advanced warning of disease outbreaks.
Mentions the development of the Sewer Coronavirus Alert Network (SCAN) in the United States to track diseases like polio and monkeypox.
Points out that monitoring wastewater can help public health officials prepare and allocate resources before patients start showing up at hospitals.
Acknowledges the limitations of this method due to the absence of centralized sewer systems in some parts of the United States.
Addresses a question about the potential misuse of this technology for mass surveillance, stating that technology is neutral and its use depends on how it's implemented.
Encourages building a better future instead of dwelling on dystopian scenarios.

Actions:

for public health officials, researchers,
Monitor wastewater for disease detection (exemplified)
Support the development and implementation of advanced public health tools (exemplified)
Advocate for equitable access to public health resources (exemplified)
</details>
<details>
<summary>
2022-08-24: Let's talk about a cop, a guilty plea, and Breonna Taylor.... (<a href="https://youtube.com/watch?v=KTBOJ1Swoxs">watch</a> || <a href="/videos/2022/08/24/Lets_talk_about_a_cop_a_guilty_plea_and_Breonna_Taylor">transcript &amp; editable summary</a>)

Former Louisville cop pleads guilty in the Breonna Taylor case, potentially strengthening the federal case and indicating cooperation within their ranks.

</summary>

"It means life. You go into the cage, you come out when you go into the forever box."
"Goodlett's plea significantly strengthens the federal case, if that is, in fact, what's happening."
"This significantly strengthens the federal case, if that is, in fact, what's happening."

### AI summary (High error rate! Edit errors on video page)

Update on the Department of Justice news regarding the Breonna Taylor case.
Former Louisville cop, Kelly Goodlett, pleaded guilty for helping falsify information to obtain the warrant.
Goodlett wasn't indicted; she was charged via complaint and entered a guilty plea.
Goodlett's sentencing is scheduled for November 22nd, but there might be delays due to extenuating circumstances.
The judge's statement during the hearing indicates Goodlett's cooperation, which could influence her sentence.
This development is positive news for Breonna Taylor's family but bad news for other cops facing charges.
Three other cops are facing charges, with the maximum sentence being life imprisonment.
Beau clarifies that "life" in the federal system means exactly that - life without the possibility of parole.
Goodlett's potential sentence is uncertain, with the charge she pleaded to carrying a maximum of five years.
Beau had predicted the possibility of cops pleading guilty due to information suggesting cooperation within their ranks.

Actions:

for justice seekers,
Support organizations advocating for police accountability (implied)
Stay informed and engaged with updates on the case (implied)
</details>
<details>
<summary>
2022-08-24: Let's talk about Republicans becoming Democrats in Colorado.... (<a href="https://youtube.com/watch?v=ihnhq7oVER8">watch</a> || <a href="/videos/2022/08/24/Lets_talk_about_Republicans_becoming_Democrats_in_Colorado">transcript &amp; editable summary</a>)

Colorado state senator switches from Republican to Democrat citing election result denial and violent rhetoric impact, potentially signaling a shift in party dynamics without major policy changes.

</summary>

"Just because somebody changes parties doesn't mean they change positions."
"Changing parties doesn't mean they change positions, it's political affiliation, not policy decisions."
"One seat doesn't seem like a lot, but when there's already a gap, that one seat, that makes it even harder."
"Obviously, Republicans are super mad about this and throwing out some entertaining threatening recall and all of that stuff."
"It's politics, so you always have to look at the political considerations for that as well when somebody switches sides like this."

### AI summary (High error rate! Edit errors on video page)

Colorado state senator Kevin Priola recently switched from being a Republican to a Democrat, citing reasons like the denial of election results and violent rhetoric as the tipping point.
Priola may have realized that winning elections without Trump's support is challenging, leading him to believe that being a conservative Democrat in Colorado might offer better prospects.
Despite his switch, Priola remains pro-life and pro-Second Amendment, indicating that his policy positions may not change due to his new party affiliation.
His move impacts Colorado politics as it makes it harder for the Republican Party to gain control of the state Senate, even though it is just one seat.
While Priola's switch might lead to a few more candidates changing parties for electoral advantages, it does not signify a widespread trend of the Republican Party falling apart.
The possibility of Republicans leaving office and returning as members of another party is considered more likely than a mass exodus or switch in party affiliations.
Priola's move has angered Republicans, who are contemplating actions like threatening recalls, but the broader impact is yet to unfold.
Changing parties does not necessarily equate to changing policy positions; it often signifies a shift in political affiliation rather than core beliefs.
Priola's strategic shift may be aimed at appealing to independent voters in his district, where Republicans, Democrats, and unaffiliated voters are almost evenly split.
The decision to switch parties involves both personal considerations and political calculations, with an eye on maintaining independence and appealing to a diverse electorate.

Actions:

for politically engaged individuals,
Monitor local political developments for potential party switches (implied)
Stay informed about candidates' policy stances rather than focusing solely on party affiliations (implied)
</details>
<details>
<summary>
2022-08-24: Let's talk about 6 months in Ukraine.... (<a href="https://youtube.com/watch?v=T6nu-9WyVYU">watch</a> || <a href="/videos/2022/08/24/Lets_talk_about_6_months_in_Ukraine">transcript &amp; editable summary</a>)

Beau outlines the current state of the Ukraine-Russia conflict, predicting potential outcomes based on the dynamics between the two nations and external factors at play.

</summary>

"War isn't about the fighting on the ground. War is continuation of politics by other means."
"Everything going on now, it's waste. It's sunk cost fallacy."
"Nations don't have friends, they have interests."
"If it ends anytime soon, it'll be because Putin called it and accepted the situation Russia's been in for a while."
"They're fighting a war that is already lost and committing more and more to it."

### AI summary (High error rate! Edit errors on video page)

Ukraine and Russia situation likened to heavyweight boxers in a slugfest.
Initial estimates of the conflict's duration measured in days or weeks, now extended to months or even 36 months.
Russia has lost between 15,000 to 40,000 personnel, Ukraine between 9,000 to 12,000.
Breaking the static nature of the conflict for Russia requires a full mobilization, which Putin wants to avoid.
For Ukraine, Western commitment to win by providing full support is necessary.
War for Russia is lost; current actions are a waste and sunk cost fallacy.
Putin's objectives in Ukraine backfired, causing NATO expansion and degrading Russian capabilities.
Russia has become the junior partner in the Russia-Chinese friendship, with China using this position to gain leverage.
Russia's defense industry focus for the war impacts its exports and reliance on other suppliers.
Active resistance movement within Ukraine, committed to regaining all lost territories.

Actions:

for world leaders,
Support Ukraine with full backing and resources to break the static nature of the conflict (implied).
</details>
<details>
<summary>
2022-08-23: Let's talk about the newly-released NARA letter to Trump.... (<a href="https://youtube.com/watch?v=hc0b7OVeQD0">watch</a> || <a href="/videos/2022/08/23/Lets_talk_about_the_newly-released_NARA_letter_to_Trump">transcript &amp; editable summary</a>)

The National Archives letter to Team Trump reveals a lack of understanding of laws and an information silo mentality, not a government conspiracy against Trump.

</summary>

"For us commoners, this is the point where the FBI jumps out of a van, walks up and says, oh, okay, do me a favor, turn around, put your hands on the wall."
"It shows a government providing a former president who may not really understand things every possible opportunity to just give the stuff back."
"It doesn't demonstrate a concerted effort to get Trump."
"When you delay, you are willfully retaining those documents."
"I don't think that anybody who really sat down and read that document and looked at it could walk away thinking that it's good for Trump."

### AI summary (High error rate! Edit errors on video page)

National Archives letter dated May 10th sent to Team Trump sparks commentary and speculation.
Trump ally released the document believing it portrays the Biden administration as targeting Trump.
The information silo mentality influences how Trump supporters interpret the document.
Trump world may lack understanding of the laws governing the situation.
Holding onto classified documents without returning them creates legal issues.
The letter from the government aimed to get Trump to return the documents, not target him.
Returning the documents promptly could have prevented any legal repercussions.
Failure to return the documents led to the situation escalating.
Misunderstanding of the law or being stuck in an echo chamber are possible reasons for releasing the document.
The document's release may indicate a shift in allegiance away from Trump.
The released document may not be favorable to Trump when examined critically.

Actions:

for activists, researchers, voters,
Read the National Archives letter to understand the context (suggested)
Educate others on the laws governing classified documents (implied)
</details>
<details>
<summary>
2022-08-23: Let's talk about talking points that are destroying families.... (<a href="https://youtube.com/watch?v=hPFF-RPyM7Y">watch</a> || <a href="/videos/2022/08/23/Lets_talk_about_talking_points_that_are_destroying_families">transcript &amp; editable summary</a>)

Beau shares a powerful story of how divisive rhetoric can rip apart families, urging against prioritizing hate over loved ones.

</summary>

"It seems like it wouldn't be worth it to destroy your family over some slick graphics and bad talking points."
"Overheard a man going off about normal right-wing talking points."
"So when these policies, and these talking points, and this rhetoric gets pushed, it's impacting a child in every class."

### AI summary (High error rate! Edit errors on video page)

Overheard a man in a grocery store expressing anger over LGBTQ content in schools.
Shared statistics on LGBTQ identification percentages in the US.
Explained how policies and rhetoric impact LGBTQ children in schools.
Questioned the impact on parents if their child is LGBTQ.
Discussed the consequences of parents prioritizing rhetoric over family.
Revealed a personal anecdote about a man in the grocery store with an estranged LGBTQ son.
Urged against destroying families over hateful rhetoric.
Encouraged reflection on prioritizing family over divisive talking points.
Ended with a message of reflection and well-wishes for the day.

Actions:

for parents, families, advocates,
Prioritize open communication and understanding within families (implied).
Challenge harmful rhetoric and discriminatory policies that impact LGBTQ children (implied).
</details>
<details>
<summary>
2022-08-23: Let's talk about if Trump did declassify the documents.... (<a href="https://youtube.com/watch?v=OQrlJUHnzII">watch</a> || <a href="/videos/2022/08/23/Lets_talk_about_if_Trump_did_declassify_the_documents">transcript &amp; editable summary</a>)

Beau delves into the implications of Trump declassifying documents, stressing that knowing their content is key while noting that defense information, not classification, is legally significant.

</summary>

"In order for him to declassify them, that means he knows what the content is."
"It doesn't matter if he declassified them. It has no impact on the case whatsoever."
"The defense information, that's all that's required for the law."

### AI summary (High error rate! Edit errors on video page)

Addressing the declassification of documents by Trump and its significance.
Exploring the implications if Trump indeed declassified the documents.
Speculating on various scenarios where Trump's staff failed to follow protocols.
Pointing out that knowing the document content is critical if they were declassified.
Emphasizing the severity of the lapse in handling defense information.
Clarifying that the documents don't need to be classified, just contain defense information.
Stressing that the declassification doesn't impact the legal aspects of the case.
Mentioning the focus on defense information rather than classification in the law cited in the warrant.
Contrasting the concerns over defense information leakage with classification status.
Concluding that regardless of declassification, the severity of the lapse remains unchanged.

Actions:

for researchers, analysts, concerned citizens.,
Research the laws cited in the warrant to understand the legal implications (suggested).
Stay informed about defense information requirements in legal contexts (implied).
</details>
<details>
<summary>
2022-08-23: Let's talk about Trump, Special Masters, and a new subpoena.... (<a href="https://youtube.com/watch?v=uypNt8yhMhA">watch</a> || <a href="/videos/2022/08/23/Lets_talk_about_Trump_Special_Masters_and_a_new_subpoena">transcript &amp; editable summary</a>)

Beau explains Trump's disappointing major motion and a significant grand jury subpoena, indicating a troubling week for the former president.

</summary>

"The main part was asking for a special master."
"It separates information that's privileged from law enforcement."
"A new one that is looking for more information, more records, more information."
"Not a great start to the week for the former president."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains Trump's major motion which was a general disappointment and involved requesting a special master to safeguard privileged information from law enforcement.
Clarifies that the case will not likely hinge on the success of this motion.
Points out that the motion is related to a search at Mar-a-Lago on August 8th, just two weeks before the motion was filed.
Mentions a grand jury subpoena issued to the National Archives on August 17th, seeking more information about events before and after January 6th, which appears to be more critical than Trump's motion.
Notes that the Department of Justice seems to be accelerating the investigation and may soon request Trump's communications during that period.
Concludes by suggesting that it has not been a great start to the week for the former president.

Actions:

for legal analysts, political commentators.,
Analyze the legal implications and potential outcomes of the grand jury subpoena (implied).
Stay informed about the developments in the investigations related to Trump's communications during critical periods (implied).
</details>
<details>
<summary>
2022-08-22: Let's talk about Trump, executive privilege, and the search... (<a href="https://youtube.com/watch?v=ERs8Z023nQM">watch</a> || <a href="/videos/2022/08/22/Lets_talk_about_Trump_executive_privilege_and_the_search">transcript &amp; editable summary</a>)

Trump claims executive privilege over seized documents, but legal realities may not be in his favor.

</summary>

"He's not going to let reality get in the way of his talking points."
"Trump's not going to let reality get in the way of his talking points."
"This claim is going to resurface, as well as the attorney-client privilege thing."

### AI summary (High error rate! Edit errors on video page)

Trump claims executive privilege over documents seized by the FBI during the raid on Mar-a-Lago.
Trump wants documents, including privileged attorney-client material, returned to their location.
Executive privilege is a legal concept that grants privacy to the executive branch in decision-making.
The FBI raid was looking for government and presidential records.
Trump's claim that the seized documents are covered by executive privilege may resurface.
Even if the documents are covered by executive privilege, they should be at the National Archives, not with Trump.
Trump is likely to continue pushing his talking points despite legal realities.

Actions:

for legal analysts, political observers,
Stay informed on the legal developments surrounding the seized documents (implied)
Follow reputable sources for updates on Trump's claims and legal challenges (implied)
</details>
<details>
<summary>
2022-08-22: Let's talk about Trump's memoir defense.... (<a href="https://youtube.com/watch?v=AaRcIdS4eQ0">watch</a> || <a href="/videos/2022/08/22/Lets_talk_about_Trump_s_memoir_defense">transcript &amp; editable summary</a>)

Ohio Republican Mike Turner's defense of Trump using classified documents for a memoir unveils deeper security risks, revealing the Republican Party's incompetence in handling sensitive information.

</summary>

"Prosecutors love it when they have evidence of one crime and the defense itself makes it a larger crime."
"The reason the Republican Party is having such a hard time coming up with a defense is because there really isn't one."
"There are many members of the Republican Party that should not be anywhere near a classified document because they have no idea how to handle them."
"That's not a defense. That's what goes in a plea agreement."
"It's worse. This defense is worse than the evidence they already have."

### AI summary (High error rate! Edit errors on video page)

Ohio Republican Mike Turner proposed a defense for the former president's actions involving classified documents, suggesting that Trump took them to write a memoir due to memory challenges.
Critics mocked this defense, particularly targeting the former president's writing skills, but Beau sees a more concerning angle.
Beau points out that if removing and retaining the classified documents are crimes, planning to distribute or publish them could be an even larger offense.
Even if Trump intended to use a ghostwriter for his memoir, the access to the classified documents remains a significant security risk.
The defense of using the documents for a book project could be seen as an intent to distribute sensitive information, making the situation worse.
The Republican Party struggles to find a solid defense because their actions regarding classified information have revealed incompetence and lack of understanding of security protocols.
Beau questions the logic behind downplaying the seriousness of possessing stolen classified documents, stating that intending to distribute them is not a valid defense but rather something for a plea agreement.

Actions:

for political observers, concerned citizens,
Ensure proper handling of classified information (exemplified)
Advocate for stringent security protocols within political parties (exemplified)
</details>
<details>
<summary>
2022-08-22: Let's talk about Trump begging McConnell for help.... (<a href="https://youtube.com/watch?v=bU8djnLlx_M">watch</a> || <a href="/videos/2022/08/22/Lets_talk_about_Trump_begging_McConnell_for_help">transcript &amp; editable summary</a>)

Donald Trump seeks help from Mitch McConnell after McConnell's comments on candidate quality, showcasing the complex dynamics within the Republican Party.

</summary>

"He should spend more time and money helping them get elected."
"You can't win a primary without Trump, but you can't win a general with him."
"I don't see how it would benefit McConnell to help Trump."
"If Trump's candidates fall on their face hard enough during the midterms here, maybe there's a possibility of salvaging the Republican Party in time for 2024."
"I don't think that McConnell will be replying to this tweet."

### AI summary (High error rate! Edit errors on video page)

Senator Mitch McConnell's comments on the quality of candidates have led to Donald Trump seeking help from him.
McConnell's doubts about Republicans retaking the Senate and the impact of Trump-backed candidates.
Trump expressed his displeasure over McConnell's comments on Twitter, criticizing him for not supporting Republican candidates enough.
The message from Trump's tweet is clear: McConnell should spend more time and money helping candidates get elected.
Despite past conflicts, Trump now finds himself in need of McConnell's support for the upcoming elections.
McConnell's long history in politics and the Republican Party's reliance on Trump's base for primary wins.
Trump's candidates are struggling in the polls, prompting him to seek help from McConnell, who holds significant influence.
McConnell, a strategic and experienced politician, may not see the benefit in backing Trump's candidates.
McConnell's focus on salvaging the Republican Party post-midterms and preparing for 2024.
The dynamics between Trump and McConnell reveal tensions within the Republican Party and their differing priorities.

Actions:

for political observers, voters,
Support independent and moderate candidates in upcoming elections (implied)
Stay informed about political developments and party dynamics (implied)
</details>
<details>
<summary>
2022-08-22: Let's talk about Arkansas, Police, and a change in headlines.... (<a href="https://youtube.com/watch?v=YhT8hCrV7zY">watch</a> || <a href="/videos/2022/08/22/Lets_talk_about_Arkansas_Police_and_a_change_in_headlines">transcript &amp; editable summary</a>)

Beau addresses a recent incident in Arkansas involving officers allegedly beating someone, expressing doubt about justifying the extreme actions seen and calling for patience and further investigation to understand the truth and advance the discourse on use of force.

</summary>

"There's not enough for me to do that with."
"I think that might actually really help further the conversation about use of force in this country."

### AI summary (High error rate! Edit errors on video page)

Addressing a recent incident in Arkansas involving three officers, two deputies, and one city cop allegedly beating someone.
Mentioning the lack of lead-up in available footage to analyze the situation thoroughly.
Anticipating body camera dash cam footage to provide more context for a proper breakdown.
Expressing doubt about justifying the extreme actions seen in the available footage.
Noting that the Arkansas State Police have initiated an investigation, focusing on the use of force.
Expressing concern that narrowing the scope of the investigation may hinder reaching the truth.
Commenting on the headlines surrounding the incident, which are more descriptive and less sanitized than usual.
Appreciating that some outlets are reflecting the available footage rather than immediately giving law enforcement the benefit of the doubt.
Seeing potential for these descriptive headlines to advance the discourse on the use of force.
Concluding that there isn't enough information available to provide a detailed analysis and calling for patience while awaiting more details.

Actions:

for observers, activists, police reform advocates,
Contact Arkansas State Police for updates on the investigation (suggested)
Stay informed about developments in the case through reputable news sources (suggested)
</details>
<details>
<summary>
2022-08-21: Let's talk about coffee, Memphis, and the NLRB.... (<a href="https://youtube.com/watch?v=VTEKiExn6p0">watch</a> || <a href="/videos/2022/08/21/Lets_talk_about_coffee_Memphis_and_the_NLRB">transcript &amp; editable summary</a>)

National Labor Relations Board orders Starbucks to reinstate seven fired employees in Memphis after leading a unionization effort, underscoring the growing push for unions across industries amid economic challenges.

</summary>

"Unions are for everybody."
"The reality is a union is for any employee who isn't getting a fair shake or wants to collectively bargain."
"The push for unionization in the U.S. is growing across various industries."
"The increase in profits, stagnant wages, and rising prices across industries are fueling more union activity nationwide."
"Unions are, well, I mean, they're for everybody."

### AI summary (High error rate! Edit errors on video page)

National Labor Relations Board intervened on behalf of seven employees fired by Starbucks in Memphis.
The firings came after the employees led a unionization effort and allegedly allowed people in the store after hours.
The NLRB determined a discriminatory motive behind the terminations and ordered Starbucks to reinstate all seven employees.
Starbucks plans to appeal the judge's decision.
The lawyer for the NLRB emphasized the importance of allowing workers to freely exercise their right to join together and improve working conditions.
The push for unionization in the U.S. is growing across various industries.
Unions are not limited to specific types of jobs but are for any employee seeking fair treatment and better working conditions.
The ruling in favor of the fired Starbucks employees is significant for supporting the union movement.
The increase in profits, stagnant wages, and rising prices across industries are fueling more union activity nationwide.
Expectations are for a rise in union activity in various locations as these economic challenges persist.

Actions:

for employees, activists, labor advocates,
Support workers' rights to unionize (implied)
</details>
<details>
<summary>
2022-08-21: Let's talk about Trump, Biden, and popularity.... (<a href="https://youtube.com/watch?v=oav4cw3h5L0">watch</a> || <a href="/videos/2022/08/21/Lets_talk_about_Trump_Biden_and_popularity">transcript &amp; editable summary</a>)

Beau reveals a surprising perspective on Trump, Biden, and 2024 popularity, debunking myths and shedding light on voter dynamics.

</summary>

"Trump drove negative voter turnout."
"Trump is not a scary candidate."
"Being in an information silo can reinforce singular viewpoints and hinder critical thinking."

### AI summary (High error rate! Edit errors on video page)

Heard something mind-blowing about Trump, Biden, and popularity in 2024.
Encountered a talkative person at a gas station who shared a surprising story about an ex-Green Beret.
The ex-Green Beret keeps to himself not because of the town's false backstory but because he's hard left.
The man at the gas station claimed Trump is being investigated because they fear he will win in 2024.
Beau consumes right-wing media but was shocked by the belief that Trump is being investigated to stop him from running.
People believed Biden won in 2020 not because of his popularity, but to vote against Trump.
Trump is not a candidate who scares Democrats for 2024.
Polling shows that a significant percentage believe Trump should be charged, giving Democrats an advantage.
Being in an information silo can reinforce singular viewpoints and hinder critical thinking.
Despite assumptions, Trump does not have overwhelming support even in GOP primaries.
There is no conspiracy to stop Trump from running again; it benefits Democrats due to negative voter turnout.

Actions:

for political analysts, voters,
Challenge your own echo chamber beliefs (exemplified)
Engage in civil discourse with individuals of differing opinions (exemplified)
Encourage critical thinking and fact-checking within your community (exemplified)
</details>
<details>
<summary>
2022-08-21: Let's talk about Tasmanian Tigers and a comeback.... (<a href="https://youtube.com/watch?v=LNHmUyV74nQ">watch</a> || <a href="/videos/2022/08/21/Lets_talk_about_Tasmanian_Tigers_and_a_comeback">transcript &amp; editable summary</a>)

Beau introduces a project to resurrect the Tasmanian tiger, sparking controversy over de-extinction and raising questions about biodiversity preservation while seeing potential value in the endeavor.

</summary>

"Everything's impossible until it's not."
"Interest in the idea of extinct animals."
"It's out there. It's weird. It's good."
"I don't see the objection to trying."
"Maybe if we can get a handle on things at some point, we can help restore biodiversity through this method."

### AI summary (High error rate! Edit errors on video page)

Introducing Tasmanian Park, a scientific endeavor to bring back the extinct animal, the Tasmanian tiger, mylocene.
The project aims to extract DNA from skeletal remains, although they won't have the full genome.
Some compare this project to the controversial woolly mammoth resurrection project.
Critics argue that de-extinction is a scientific fairy tale and a waste of money.
Beau acknowledges the high costs of these projects and suggests the money could be better used to prevent current biodiversity loss.
Despite potential commercialization and ethical concerns, Beau sees value in sparking interest and curiosity in extinct animals and biodiversity.
He believes the project could lead to valuable research even if unsuccessful in bringing back the Tasmanian tiger.
Beau doesn't see significant harm in the project and believes it could potentially aid in biodiversity restoration.
He acknowledges the ethical question of the animal's health and uncertainties surrounding the project but ultimately sees no harm in attempting it.
Beau concludes by sharing his perspective that the project may have positive outcomes and encourages viewers to ponder the implications.

Actions:

for science enthusiasts, conservationists,
Support initiatives focused on biodiversity preservation and preventing animal extinction (implied)
Stay informed about scientific advancements in conservation efforts (implied)
</details>
<details>
<summary>
2022-08-20: Let's talk about water cuts to Arizona and Nevada.... (<a href="https://youtube.com/watch?v=ZhaelXGX-zA">watch</a> || <a href="/videos/2022/08/20/Lets_talk_about_water_cuts_to_Arizona_and_Nevada">transcript &amp; editable summary</a>)

Federal cuts in water allotments from the Colorado River signal the new normal of a prolonged drought, urging a shift in mindset towards addressing long-term climate challenges.

</summary>

"This is the new normal."
"We have to stop pretending."
"We can't continue to act like it's going to end tomorrow."

### AI summary (High error rate! Edit errors on video page)

Federal government announces cuts in water allotments from the Colorado River due to moving into a tier two shortage.
Arizona will lose 21% of its water allocation, equivalent to 592,000 acre feet.
Nevada will face an 8% cut, impacting an area with significant water needs.
Mexico will experience a 7% reduction in water flow.
Arizona's $15 billion agricultural industry heavily relies on these water flows.
The 22-year-long drought in the region is not just a temporary situation; it's the new normal.
Continuing to refer to the ongoing drought as temporary misleads people into expecting a quick resolution.
The federal government's actions aim to prevent a catastrophic loss in the Colorado River basin but may not be sufficient.
The increasing population and demands in the area, combined with limited water resources, pose a significant challenge.
It's time to acknowledge the reality of climate change and the prolonged impact of the drought on water availability.

Actions:

for southwestern residents,
Prepare for worsening water shortages by implementing water conservation measures (implied).
Support local initiatives promoting water recycling and conservation efforts (implied).
Stay informed and advocate for sustainable water management practices in the region (implied).
</details>
<details>
<summary>
2022-08-20: Let's talk about the next rocket to the moon.... (<a href="https://youtube.com/watch?v=VN-dKvEuBjw">watch</a> || <a href="/videos/2022/08/20/Lets_talk_about_the_next_rocket_to_the_moon">transcript &amp; editable summary</a>)

NASA plans to send a rocket to the moon in a month-long mission, marking a critical step towards extended human presence in space, amidst potential Earthly distractions.

</summary>

"This is one of the new steps towards getting us off this rock."
"It's a new phase."
"Getting off of this rock, that will be a crowning achievement."

### AI summary (High error rate! Edit errors on video page)

NASA is preparing to go back to the moon as part of the Artemis program.
A giant new rocket is set to launch on August 29th, with no crew onboard, just mannequins with sensors.
The capsule will orbit the moon for a couple of weeks before returning to Earth.
The whole mission is expected to take about a month and a half.
Previous dress rehearsals for the rocket haven't gone smoothly, but hopefully, this launch will be successful.
The ultimate goal is to have a crew on the moon by 2025 for extended periods.
Despite being a significant event, it might be overlooked due to distractions on Earth.
This mission marks a critical step towards humanity's goal of space exploration.
NASA aims to eventually have people on the moon for extended periods.
Getting humans off Earth is a monumental achievement, and this mission is a significant step in that direction.

Actions:

for space enthusiasts,
Stay updated on NASA's Artemis program progress (implied)
Support and advocate for space exploration initiatives (implied)
</details>
<details>
<summary>
2022-08-20: Let's talk about Kansas, recounts, perceptions, and realities.... (<a href="https://youtube.com/watch?v=IUA3sRYVdMM">watch</a> || <a href="/videos/2022/08/20/Lets_talk_about_Kansas_recounts_perceptions_and_realities">transcript &amp; editable summary</a>)

Recounts are about galvanizing bases, not changing outcomes; rare to alter elections, like in 2004's Washington case, with a swing of 129-133 votes.

</summary>

"Recounts are more about galvanizing a base and sowing doubt rather than actually changing outcomes."
"The outcome of an election being altered by a recount is incredibly rare."
"Politicians who push for recounts after losing by significant margins may not be fit for office."

### AI summary (High error rate! Edit errors on video page)

Recounts are more about galvanizing a base and sowing doubt rather than actually changing outcomes.
A recount altering an election outcome is incredibly rare.
Activists in Kansas are funding a recount of nine counties related to a ballot initiative on family planning, costing over a hundred grand.
In 2004 in Washington, the largest swing in a statewide election occurred, with Gregoire leading by 129 or 133 votes after a manual recount.
The outcome of an election being altered by a recount is highly unlikely, especially with significant margins like in Kansas, where 165,000 votes separate the candidates.
Politicians who push for recounts after losing by significant margins may not be fit for office.
Recounts are often used to bring in money, galvanize supporters, sow doubt, and keep people angry rather than genuinely change outcomes.

Actions:

for voters,
Support fair and transparent election processes (implied).
Stay informed about the realities of recounts and election outcomes (implied).
</details>
<details>
<summary>
2022-08-20: Let's talk about Biden, Trump, and the midterms being 3 months away.... (<a href="https://youtube.com/watch?v=f4dCH6meFB4">watch</a> || <a href="/videos/2022/08/20/Lets_talk_about_Biden_Trump_and_the_midterms_being_3_months_away">transcript &amp; editable summary</a>)

Beau points out Biden's achievements and contrasts them with Trump's troubles, setting the stage for Democrats in the upcoming midterms.

</summary>

"It gives Democrats something to run on."
"Biden has actually kind of done pretty well over the last couple of months."
"There are going to be some people, people who are more progressive, who are going to look at this and, as always, good start, not enough."
"That's a party that has real issues."
"But there's also still like three months left, so let's see what happens."

### AI summary (High error rate! Edit errors on video page)

Contrasts Biden's achievements with Trump's ongoing issues, providing Democratic candidates with positive talking points heading into the midterms.
Points out that Americans often credit the president with things beyond their control, bolstering Biden's image.
Lists legislative achievements like the CHIPS Act, PACT Act, Inflation Reduction Act, and bipartisan infrastructure law as wins for Democrats.
Mentions how even indirect effects like job creation and gas prices can be attributed to Biden in the public eye.
Notes that Biden's Senate experience may have influenced the successful passage of legislation, putting Republicans in a tough spot.
Suggests that Biden's handling of Trump's legal issues and staying off Twitter may appeal to voters tired of Trump's style of leadership.
Observes that the Republican Party is facing internal divisions, with some members looking to move on from Trump's influence.
Urges Democrats to focus on showcasing their accomplishments and energizing their base leading up to the midterms.
Acknowledges that while some may find Biden's progress insufficient, it still surpasses Trump's achievements in the eyes of many.
Encourages continued observation as the political landscape evolves in the months ahead.

Actions:

for voters, political observers,
Support Democratic candidates and help spread awareness of their accomplishments (exemplified)
Stay informed about political developments and encourage others to do the same (exemplified)
</details>
<details>
<summary>
2022-08-19: Let's talk about an update on the Vanessa Guillén case.... (<a href="https://youtube.com/watch?v=darQhzIQMNE">watch</a> || <a href="/videos/2022/08/19/Lets_talk_about_an_update_on_the_Vanessa_Guill_n_case">transcript &amp; editable summary</a>)

Beau provides an update on the Vanessa Guillen case, stressing the importance of ongoing attention to address harassment and violence issues within the Department of Defense, showcasing how public pressure can drive change.

</summary>

"Public pressure really did create change."
"Justice is her still being here."
"Financial penalties tend to promote change."

### AI summary (High error rate! Edit errors on video page)

Provides an update on the Vanessa Guillen case, mentioning the involvement of viewers in pressuring for results.
Vanessa Guillen, a 20-year-old, was killed at Fort Hood in 2020, leading to public pressure, legislation, and reforms.
Vanessa Guillen's family is pursuing a $35 million lawsuit for wrongful death and alleging military negligence.
Department of Defense might argue against damages citing a law, but exceptions have been made.
The case holds significance due to ongoing harassment and violence issues within the Department of Defense.
Despite some progress, the problem is far from resolved, warranting continued attention.
Public pressure has been instrumental in bringing about changes and maintaining focus on the issue.
$35 million is not justice but a step in the right direction, potentially prompting further reforms.
Financial penalties like this lawsuit can drive change and encourage more progress.
Beau stresses the importance of following the case to understand consequences when progress stalls.

Actions:

for advocates, activists, viewers,
Contact oversight, Senate Armed Services Committee to build pressure (implied)
Stay informed and follow cases of injustice within institutions (suggested)
</details>
<details>
<summary>
2022-08-19: Let's talk about a redacted Trump affidavit.... (<a href="https://youtube.com/watch?v=d7kTklihAi4">watch</a> || <a href="/videos/2022/08/19/Lets_talk_about_a_redacted_Trump_affidavit">transcript &amp; editable summary</a>)

Beau explains the ongoing process involving a sealed DOJ document related to the investigation, hinting at potential wider implications and limited positive outcomes for Team Trump.

</summary>

"There are a lot of people asking, how can it be in the early stages? They have the documents, and they have all of this information, right? Yeah, that's the bad news for Team Trump."
"The length of it alone though, because it has been described as being a long document, that's kind of telling in and of itself."
"There's no counter to it. You are getting one perspective on it and it is a document designed to get a warrant."
"So at the end of this, there's not a lot of good news for Team Trump."
"But there's also not a lot for the public, at least not yet."

### AI summary (High error rate! Edit errors on video page)

Explains the process of a document presented by the Department of Justice to a judge to obtain a warrant.
Mentions the media's interest in unsealing the document related to the investigation.
Indicates that the investigation is in its early stages, with potentially wider implications than initially thought.
Notes the presence of substantial grand jury information in the affidavit.
Points out the desire to protect witnesses involved in the case.
Describes the redaction process undertaken by the Department of Justice to protect sensitive information.
Speculates on the limited new information that may be revealed upon the document's release.
Emphasizes that the document presents only the Department of Justice's perspective.
Raises the possibility of the judge releasing the entire document despite national security concerns.
Expresses the lack of positive outcomes for Team Trump based on the disclosed information.

Actions:

for observers, doj followers,
Contact media outlets to advocate for transparency in the release of relevant documents (suggested).
Stay informed about developments in the investigation and subsequent document release (exemplified).
</details>
<details>
<summary>
2022-08-19: Let's talk about WHO changing names and changing as a person.... (<a href="https://youtube.com/watch?v=GPGj9ktjS-w">watch</a> || <a href="/videos/2022/08/19/Lets_talk_about_WHO_changing_names_and_changing_as_a_person">transcript &amp; editable summary</a>)

Beau explains the WHO's naming changes to reduce stigma and harm, not catering to feelings, but promoting a methodical approach.

</summary>

"What possible reason is there for who, the World Health Organization, to change the name of Monkeypox?"
"It's not woke garbage. There's a method to it."
"They're trying to remove the stigma, so that doesn't happen anymore."

### AI summary (High error rate! Edit errors on video page)

The World Health Organization is updating its guidance on naming things, sparking a question on name changes.
Beau shares his personal growth journey, transitioning from a racist and angry person to someone striving to be better.
Mentioning a YouTube streamer, Streamer X, who helped him move away from the far right ideology.
Beau acknowledges his anger towards liberal changes and credits Streamer X for introducing him to new perspectives.
Addressing a viewer's question on the World Health Organization changing the name of Monkeypox.
Beau commends the viewer for their positive change in attitude following a wake-up moment post-Capitol events.
Explaining the WHO's rationale behind avoiding names with cultural or ethnic references to minimize negative impacts.
Providing examples of naming changes to reduce stigma and harm, such as renaming variants from geographical to clade designations.
Emphasizing that the naming adjustments aim to prevent harm and not cater to people's feelings.
Beau clarifies that the renaming efforts are not about "woke garbage" but about reducing stigma and harm.

Actions:

for online viewers,
Understand the reasoning behind naming changes by the World Health Organization (implied).
Support efforts to reduce stigma and harm through updated naming conventions (implied).
</details>
<details>
<summary>
2022-08-19: Let's talk about Dems, Trump, and the boy who cried wolf.... (<a href="https://youtube.com/watch?v=fAjzm13tyqk">watch</a> || <a href="/videos/2022/08/19/Lets_talk_about_Dems_Trump_and_the_boy_who_cried_wolf">transcript &amp; editable summary</a>)

Beau questions the credibility of the Democratic Party using the "boy who cried wolf" analogy, encouraging treating every threat seriously to prevent negative outcomes.

</summary>

"Treat every threat as if it's real. Otherwise, the little boy is going to get eaten."
"Maybe he can get that little human, but the little human starts screaming again."
"Make sure that if there is a wolf looking to attack your capital or the flock, you actually investigate."
"Even in your version of that story, there is a wolf at the end."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Exploring the credibility of the Democratic Party and the "boy who cried wolf" analogy.
Analyzing the traditional story of the boy who cried wolf.
Suggests treating every threat as real to prevent negative consequences.
Expresses skepticism towards the traditional moral of the story.
Questions the perspective from which the story is told and criticizes the villagers.
Proposes telling the story from the perspective of the wolf, questioning the villagers' actions.
Argues that investigating real threats is more critical than the traditional moral of not lying.
Concludes that the current situation involves the pursuit of the wolf, unlike the traditional story.
Encourages a different interpretation of the tale, focusing on pursuing and investigating threats.
Emphasizes the importance of taking action when faced with real dangers.

Actions:

for politically engaged individuals,
Investigate potential threats seriously (implied)
Take action when faced with real dangers (implied)
</details>
<details>
<summary>
2022-08-18: Let's talk about secrets and plea deals.... (<a href="https://youtube.com/watch?v=Mrbt_h3ixfM">watch</a> || <a href="/videos/2022/08/18/Lets_talk_about_secrets_and_plea_deals">transcript &amp; editable summary</a>)

Jonathan and Diana's attempt to sell state secrets reveals gaps in sentencing for national security breaches.

</summary>

"No secrets were actually compromised."
"The judge rejected it, said that wasn't enough."
"12 and a half to 17 and a half years was not enough."
"Something that exposes national security is worthy of more than 12 and a half years."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Jonathan Tobey and his wife Diana are alleged to have attempted to sell U.S. state secrets to a foreign power.
Jonathan, a naval engineer, smuggled secrets out in various ways, including using peanut butter as a cover.
They passed the secrets to undercover FBI agents through dead drops, avoiding face-to-face meetings.
No secrets were actually compromised, but the couple attempted to enter a plea deal which was rejected by the judge.
The judge deemed the proposed 12.5 to 17.5 years sentence inadequate and demanded more time.
Diana, alleged to have acted as a lookout, was set to receive three years in the rejected plea deal.
The couple withdrew their guilty plea and are headed for trial next year unless a new deal is reached.
The classified information passed on was not top secret or compartmented, but still serious enough to warrant more than 12.5 to 17.5 years.
The judge views this crime, which exposes national security, as deserving of a punishment exceeding the initial plea deal.
It's uncertain if a new deal is being negotiated, but this case is likely to attract significant attention due to its national security implications.

Actions:

for legal observers,
Follow the developments of the trial and any potential new plea deals (implied)
Stay informed about cases involving national security breaches (implied)
</details>
<details>
<summary>
2022-08-18: Let's talk about Tucker, secrets, and methods.... (<a href="https://youtube.com/watch?v=B30XHQFx_00">watch</a> || <a href="/videos/2022/08/18/Lets_talk_about_Tucker_secrets_and_methods">transcript &amp; editable summary</a>)

Beau explains that classified information is more about protecting methods than the content itself, illustrating the importance of understanding how information is obtained.

</summary>

"Classified information is not about the content, but about how it was obtained."
"Declassification could expose methods that protect other secrets."
"Overclassification can flood potential targets for opposition intelligence."
"Understanding classified information goes beyond the content to how it was acquired."
"Protecting methods is key to safeguarding secrets."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson suggested that American people should know the contents of classified documents to determine if they are meaningful.
He pointed out the absurdity of classification by mentioning the declassification of a disappearing ink formula from World War I in 2011.
Beau explains that information can be classified not for its content, but for the method through which it was obtained.
The example of the disappearing ink formula was classified to protect the method, not because the formula itself was significant.
Classification is often about protecting methods that can lead to uncovering other secrets.
The CIA argued against releasing information in 1998 because they still used the method mentioned in the declassified documents.
Declassifying information can reveal methods that opposition intelligence can exploit, leading to the development of counterintelligence activities.
Beau illustrates the importance of protecting methods using an example of a ciphered message that Tucker Carlson wouldn't be able to decipher without knowing the method.
Overclassification in the U.S. is seen as a method to flood potential targets for opposition intelligence, not necessarily to trap individuals.
Understanding what is classified is not just about the information itself but also about how that information was obtained.

Actions:

for media consumers,
Analyze and question the intentions behind the classification of information (implied)
Advocate for transparency in government classifications to ensure accountability and prevent unnecessary secrecy (implied)
</details>
<details>
<summary>
2022-08-18: Let's talk about Republican money troubles.... (<a href="https://youtube.com/watch?v=EBdgmLajz70">watch</a> || <a href="/videos/2022/08/18/Lets_talk_about_Republican_money_troubles">transcript &amp; editable summary</a>)

The National Republican Senatorial Committee's cancellation of ad buys due to financial constraints signals trouble for struggling Republican candidates in key states.

</summary>

"They don't have the money because small donor donations have dried up."
"The problem is it's all going to Trump and he's not sharing."
"You know, there was a big hope in the Republican Party for a red wave, but at this point when you have candidates ten points down and the National Republican Senatorial Committee is just kind of like, yeah, whatever, we're not going to help you."

### AI summary (High error rate! Edit errors on video page)

The National Republican Senatorial Committee in Arizona, Pennsylvania, and Wisconsin canceled about ten million dollars worth of advertisement buys, impacting Republican candidates in those states like Dr. Oz, Ron Johnson, and Blake Masters.
Dr. Oz and Blake Masters are down in the polls by about ten points.
It appears that winning a Republican primary without Trump is difficult, but winning a general election with him is also challenging.
Ron Johnson seems to be doing okay in the polls currently.
The committee's decision to cancel ad buys is attributed to a lack of funds, as small donor donations have dried up within the Republican Party.
Most of the money raised on the right wing seems to be going to Trump rather than being shared with other candidates.
Without Trump contributing financially, these already struggling candidates are likely to fall further behind in the polls.
This financial setback is detrimental to the Republican Party's goal of retaking the Senate.
The lack of available funds will especially impact tight races where cash could make a significant difference.
People directing their donations towards Trump rather than the Republican Party is contributing to the financial strain on other candidates.
The situation suggests that similar scenarios may unfold in future races, with the party needing to prioritize candidates they believe can win and minimize losses.
The hope for a "red wave" in the Republican Party seems to be dwindling, especially when candidates are significantly trailing in the polls without support from the National Republican Senatorial Committee.
The Committee's lack of assistance to struggling candidates does not bode well for the anticipated "red wave."
The overall situation indicates a challenging road ahead for Republican candidates in various races.

Actions:

for political activists and donors.,
Support struggling Republican candidates financially (suggested).
Prioritize donations towards campaigns of candidates who need assistance (suggested).
</details>
<details>
<summary>
2022-08-18: Let's talk about NARA, Trump, and Obama.... (<a href="https://youtube.com/watch?v=p27qOHRjLDw">watch</a> || <a href="/videos/2022/08/18/Lets_talk_about_NARA_Trump_and_Obama">transcript &amp; editable summary</a>)

Beau clarifies the false claims about Obama's records, exposing them as deflection tactics, while underscoring the importance of not arguing with librarians regarding records.

</summary>

"No, he made it up. It's not true. It is inaccurate."
"If Trump had done the same thing, we wouldn't be having this conversation."
"Never argue with a librarian."
"That is such a short statement to be wrong 33 million times in."
"This misinformation about Obama's records is just one of many attempts to divert attention."

### AI summary (High error rate! Edit errors on video page)

Exploring the records and archives of former presidents Trump and Obama, focusing on the false claim about Obama keeping 30 million records.
Trump falsely claims that Obama took 30 million pages of records from his administration and didn't digitize them, which outraged historians.
The reality is that the National Archives and Records Administration (NARA) have exclusive custody of Obama's presidential records, both classified and unclassified.
NARA moved approximately 30 million pages of unclassified records to a facility in Chicago and maintains classified records in Washington DC.
Beau clarifies that Trump's statements about Obama's records are inaccurate and essentially made up.
Despite knowing where the records are kept, Trump continues to mislead by implying Obama took them with him.
The Presidential Records Act dictates that all presidential records belong to the US government and should be maintained by NARA.
Trump's false claims are viewed as a deflection tactic since he knows his base won't fact-check him and are unlikely to challenge his statements.
Beau stresses the importance of not arguing with librarians when it comes to records, as they know where everything is stored.
This misinformation about Obama's records is just one of many attempts to divert attention from the actions of the Trump administration upon leaving office.

Actions:

for history buffs,
Contact local libraries or archives to learn more about preserving and accessing historical records (suggested)
Support organizations that advocate for transparency and accountability in government record-keeping (implied)
</details>
<details>
<summary>
2022-08-17: Let's talk about whether Cheney's loss is Trump's win.... (<a href="https://youtube.com/watch?v=ua5vk5wyUMg">watch</a> || <a href="/videos/2022/08/17/Lets_talk_about_whether_Cheney_s_loss_is_Trump_s_win">transcript &amp; editable summary</a>)

Liz Cheney's loss in a very red area, where even one in four Republicans voted for her, signals potential trouble for Team Trump's popularity.

</summary>

"One in four Republicans voted for the person who is openly and actively trying to put Trump in jail."
"This is pretty bad news for the Trump team."
"Even in very red areas, there are a whole lot of people who side with the person trying to put Trump away than Trump."
"This was a push by the Trumpist factions of the Republican Party to get her out."
"Cheney's ability to capture even one out of four votes in that location is really bad news for Trump and company."

### AI summary (High error rate! Edit errors on video page)

Liz Cheney lost in what's being seen as a blowout for the Trump side.
Cheney is getting around 33% of the votes, with some Democrats crossing over to help.
Even if one in four Republicans voted for Cheney, who is trying to put Trump in jail, it may not be a win for Trump.
This loss in a very red area could indicate bad news for Team Trump in terms of popularity.
The primary win for Team Trump may not translate to general elections.
The push to remove Cheney was driven by Trumpist factions within the Republican Party.
Cheney's ability to capture even one out of four votes in that location is seen as unfavorable news for Trump.

Actions:

for political analysts,
Analyze voter trends in red areas (implied)
Stay informed about political shifts and factional dynamics within parties (implied)
</details>
<details>
<summary>
2022-08-17: Let's talk about where Liz Cheney goes from here.... (<a href="https://youtube.com/watch?v=dM4C4iqDlCw">watch</a> || <a href="/videos/2022/08/17/Lets_talk_about_where_Liz_Cheney_goes_from_here">transcript &amp; editable summary</a>)

Representative Liz Cheney's future in the Republican Party is secured, poised to lead if it breaks free from Trumpism or remembered as a missed chance for change.

</summary>

"Her legacy is secured. She will be in the history books and she will be remembered more favorably than Vice President Cheney."
"Cheney has positioned herself to be a leader in the Republican Party in the event that it wakes up."
"Win or lose, her position is secured."
"It's odd that a Cheney has positioned themselves to be recorded in history books as a real defender of the American experiment."
"She'll be the candidate that people look back on and say, well, this person could have led the Republican Party somewhere better."

### AI summary (High error rate! Edit errors on video page)

Representative Liz Cheney is now free from political concerns after being removed from her position, allowing her to pursue the truth without worries.
Whether Cheney wins or loses the ongoing voting, her future remains significant in the short and long term.
In the short term, if Cheney lost, she may be seen as a Bernie-type figure within the Republican Party.
If Cheney wins, she could become a powerful voice and a key player within the Republican Party.
Cheney's legacy is already secured, and she is likely to be remembered more favorably than Vice President Cheney.
Cheney may become a candidate for an executive branch position in the near future, depending on what happens to Trump and Trumpism.
Her future heavily relies on whether the Republican Party breaks free from Trumpism.
Cheney has positioned herself as a potential leader in the Republican Party if it undergoes a transformation.
The ongoing primary election will have a decisive impact on shaping history books and the future of the Republican Party.
Cheney's role will be pivotal in either leading the party in a new direction or being remembered as a missed chance for change.

Actions:

for political observers,
Support political candidates who prioritize truth and accountability within their party (suggested).
Stay informed about the ongoing developments in the Republican Party and how they may shape future leadership (exemplified).
</details>
<details>
<summary>
2022-08-17: Let's talk about the damage assessment about Trump's docs.... (<a href="https://youtube.com/watch?v=p5AGk2RM8pQ">watch</a> || <a href="/videos/2022/08/17/Lets_talk_about_the_damage_assessment_about_Trump_s_docs">transcript &amp; editable summary</a>)

House and Judiciary Committees seek assessment on Trump's clubhouse documents, with limited public insight expected; speculation surrounds prior knowledge and potential risks.

</summary>

"It's a world of secrets, so we're not going to find anything out for a while."
"If they become even more fired up, that's probably a pretty good sign that people were put at risk."
"Now, as far as the intelligence community, they would do this anyway, this kind of assessment."

### AI summary (High error rate! Edit errors on video page)

House Intelligence and Oversight Committees seek a damage assessment from the intelligence community regarding documents found at Trump's clubhouse.
Judiciary Committee doesn't seem concerned about the situation and considers it a normal search warrant.
The briefing on the matter will likely take place behind closed doors, limiting public access to information.
Classified documents involved are likely TSSCI, which may restrict what can be shared publicly.
Any insights from the briefing may have to be inferred from the reactions of those involved rather than direct statements.
It is speculated that the intelligence community might have already conducted a damage assessment before the search warrant.
The extent of Department of Justice's (DOJ) prior knowledge about the documents found remains unknown.
Counterintelligence personnel probably assessed the situation once they realized classified documents were involved.
Speculation surrounds whether DOJ was aware of the exact nature of the documents found or simply knew they were classified.
The public might have to rely on subtle cues from those involved to gauge the severity of the situation.

Actions:

for policy analysts,
Monitor public statements from involved officials for any subtle cues indicating severity (implied).
</details>
<details>
<summary>
2022-08-17: Let's talk about the 5 words that might sink Trump.... (<a href="https://youtube.com/watch?v=4oYBWgjFr7w">watch</a> || <a href="/videos/2022/08/17/Lets_talk_about_the_5_words_that_might_sink_Trump">transcript &amp; editable summary</a>)

Beau analyzes whether a statement from Trump's advisors about possession of government-owned boxes constitutes an admission, cautioning against premature conclusions in the serious investigation.

</summary>

"It's not theirs, it's mine."
"I mean those five words, that's really bad and it totally sounds like Trump."
"Five words that will probably haunt the former president for quite some time until this case is resolved one way or another."

### AI summary (High error rate! Edit errors on video page)

Analyzing whether a quote from Trump's advisors about possession of boxes is an admission.
Trump reportedly said, "It's not theirs, it's mine," concerning government-owned boxes.
Possession is a critical aspect in many laws, making the statement concerning.
The statement could be troubling for prosecutors and Trump's attorneys.
Some of Trump's advisors who heard the statement have talked to the FBI.
MSNBC framed it as an admission, but there's a significant difference between informal statements and testimonies under oath.
Speculation in such a serious investigation may not be wise.
If the statement reaches the FBI and is part of a sworn statement, it could be extremely damaging.
However, it's uncertain if the statement made it to the FBI.
Beau advises managing expectations and following the pace of the investigation based on confirmed information.
Trump's statement, if true, could have serious implications.
While Trump's circle may disclose damaging information to media, it doesn't necessarily prove possession for legal purposes.
These five words could haunt Trump until the case is resolved.
Beau suggests waiting for further developments rather than jumping to conclusions.

Actions:

for legal analysts,
Stay updated on confirmed information related to the investigation (suggested)
Avoid speculation and premature conclusions (suggested)
</details>
<details>
<summary>
2022-08-16: Let's talk about how Trump can't catch a break.... (<a href="https://youtube.com/watch?v=FrgtM4nFhzM">watch</a> || <a href="/videos/2022/08/16/Lets_talk_about_how_Trump_can_t_catch_a_break">transcript &amp; editable summary</a>)

Trump faces multiple legal challenges and investigations across different jurisdictions, with developments indicating a series of bad news ahead.

</summary>

"It's all bad."
"I got a feeling it's going to be a long one."

### AI summary (High error rate! Edit errors on video page)

Trump is facing multiple legal challenges across different jurisdictions, including Department of Justice investigations and cases in New York and Georgia.
Eric Hirschman, Trump's former lawyer and senior White House advisor, has been subpoenaed by a grand jury investigating a sustained campaign to hold on to power.
Hirschman's testimony before the committee was frank, indicating potential trouble for Trump.
The CFO of the Trump Organization in New York is reportedly considering entering into a plea agreement, which could spell bad news for Trump.
Rudy Giuliani has been informed that he is a target of a grand jury in Georgia.
The Department of Justice opposed unsealing an affidavit related to a search warrant at Mar-a-Lago, citing irreparable damage to an ongoing criminal investigation.
The filing by the Department of Justice suggests they are pursuing a criminal case beyond just retrieving documents.
The unsealed affidavit might disclose highly sensitive information about witnesses and could impact other high-profile cases.
There are speculations about witnesses cooperating with the DOJ against Trump.
Overall, the developments indicate a series of bad news for Trump with quickening pace of investigations and probes.

Actions:

for legal observers, political analysts.,
Stay informed about the ongoing legal developments related to Trump's cases (suggested).
Follow reputable news sources for updates on the investigations and probes (suggested).
</details>
<details>
<summary>
2022-08-16: Let's talk about Trump org and the Tax Man.... (<a href="https://youtube.com/watch?v=hJgB-imdqDY">watch</a> || <a href="/videos/2022/08/16/Lets_talk_about_Trump_org_and_the_Tax_Man">transcript &amp; editable summary</a>)

A tax case against the Trump Organization and Allen Weisselberg could impact midterms, adding to legal worries for Trump while challenging public perceptions of accountability.

</summary>

"It is worth noting from a political standpoint that if jury selection begins October 24th, it's a safe bet that the trial will be going on during the midterms."
"When cases go forward, they're not always what you think they'd be for."
"There are cases springing up, there are investigations springing up, there are searches springing up, people inside his circle are just constantly being hit now."
"There is this case and there's the document case, which is, that appears to be a very strong case."
"It's possible that it ends up being taxes and documents that actually really starts to chip away at Trump's Teflon coating."

### AI summary (High error rate! Edit errors on video page)

Explains a tax case against the Trump Organization and CFO Allen Weisselberg in New York.
Mentions the attempt by the Trump team to dismiss the case, which was not successful.
Notes that the judge allowed the case to go to a grand jury, with jury selection starting on October 24th.
Points out that some charges in the case carry significant potential sentences, with one ranging from five to 15 years.
Raises the fact that tax cases like this can be lengthy, involving educating the jury and potentially taking months for a trial.
Suggests that the trial could coincide with the midterms if the schedule holds, although Trump himself is not a defendant in this case.
Indicates that this legal issue adds to the numerous other cases and investigations surrounding Trump and his associates.
Emphasizes that even though this case may seem less significant to the American public, its outcomes can be unpredictable.
Speculates that accountability for Trump may come from unexpected angles like taxes and documents rather than the expected sixth-related matters.
Considers the possibility that cases like these could erode Trump's perceived invulnerability.

Actions:

for political analysts, concerned citizens,
Follow updates on the case and its implications (suggested)
Stay informed about legal developments related to accountability in political circles (suggested)
</details>
<details>
<summary>
2022-08-16: Let's talk about Rudy in Georgia.... (<a href="https://youtube.com/watch?v=tn5nTVu7cT0">watch</a> || <a href="/videos/2022/08/16/Lets_talk_about_Rudy_in_Georgia">transcript &amp; editable summary</a>)

Beau provides updates on Giuliani becoming a target in the Georgia case, signaling legal trouble and potential case acceleration amidst other unfolding events.

</summary>

"Target means they're trying to fit you for matching bracelets."
"The testimony that day will be brought to you by the number five."
"They are now bringing in people who they consider targets."

### AI summary (High error rate! Edit errors on video page)

Talking about the Georgia case involving Rudy Giuliani, following up on a previous video discussing Lindsey Graham.
Giuliani was expected to testify to the grand jury but has been informed that he is now a target of the probe.
Being labeled as a target means legal trouble is imminent, with matching bracelets likely to follow.
It's unlikely Giuliani will provide comic relief in his testimony; the fifth amendment might be his main response.
Bringing in targets for questioning signals a significant phase nearing conclusion in the legal process.
This development could expedite the case's progress, coinciding with other ongoing legal matters.
While comic relief may not be on the horizon, the legal process seems to be gaining momentum.
It's advised to keep an eye on Georgia amidst the other unfolding events.

Actions:

for legal observers,
Keep informed about legal proceedings in Georgia (implied)
</details>
<details>
<summary>
2022-08-16: Let's talk about Graham and the Trump case in Georgia.... (<a href="https://youtube.com/watch?v=sAw-RFy2S9M">watch</a> || <a href="/videos/2022/08/16/Lets_talk_about_Graham_and_the_Trump_case_in_Georgia">transcript &amp; editable summary</a>)

Lindsey Graham faces implications and potential consequences in the Georgia grand jury investigation, adding to the legal troubles surrounding Trump and his associates.

</summary>

"The worry is that if those statements aren't true and at some point somebody who is aware of the fact that they may not be true, perhaps somebody was in a room when there was a discussion that takes place and that person flips, then Graham gets wrapped up in a bunch of stuff."
"Generally speaking, when you're talking to election officials and you're pressuring them to find votes, that is something that law enforcement refers to in technical terms as totally uncool and might lead to severe issues."

### AI summary (High error rate! Edit errors on video page)

Lindsey Graham received a subpoena to talk to a special grand jury in Georgia.
Graham's lawyers argued for immunity under the speech and debate clause and sovereign immunity, but the arguments were rejected.
Graham is implicated in implying that officials should manipulate things for the former president.
If Graham's statements don't hold up under scrutiny, he could face serious consequences, especially with others potentially cooperating with authorities.
The investigation stems from the controversial phone call to find votes in Georgia.
Raffensperger suggests Graham may have been involved in pressuring state officials.
The grand jury is looking into various election-related offenses, including solicitation of election fraud and conspiracy.
Graham's involvement in the grand jury investigation could lead to serious issues for him.
The case involving Graham and the Trump circle is gaining momentum and could have significant consequences.
This case is another one to keep an eye on in the broader context of legal actions involving Trump and his associates.

Actions:

for legal observers, political analysts,
Stay informed on the developments of the legal cases involving Trump and his associates (suggested)
Monitor the outcomes of Graham's involvement in the grand jury investigation (suggested)
</details>
<details>
<summary>
2022-08-15: Let's talk about what I worry about.... (<a href="https://youtube.com/watch?v=YChQwoxJM0k">watch</a> || <a href="/videos/2022/08/15/Lets_talk_about_what_I_worry_about">transcript &amp; editable summary</a>)

Beau warns about climate change, authoritarianism, and civil unrest in the U.S., urging vigilance and action to prevent catastrophic consequences.

</summary>

"That's a worry. That's a real concern."
"An authoritarian regime that really got seated in the United States would be very, very hard to dislodge."
"Civil unrest in the United States would be catastrophic, and it would be very long-lasting."

### AI summary (High error rate! Edit errors on video page)

Received a message questioning his usual dismissal of concerns in the media.
Identifies three major worries for people in the United States.
First concern is climate change, urging action to cut emissions and mitigate effects.
Points out cities already facing drastic policies due to climate change.
Warns of the unstoppable consequences if action is delayed.
Second worry is the rise of authoritarianism in the U.S.
Notes a significant portion of the population desiring authoritarian leadership.
Expresses concern about the difficulty of removing an authoritarian regime once established.
Emphasizes the importance of pushing back against authoritarian tendencies.
The third worry is the potential for catastrophic civil unrest in the U.S. due to accelerationists.
Stresses that these concerns could escalate quickly and have long-lasting effects.
Urges vigilance and action to prevent these scenarios from unfolding.

Actions:

for united states citizens,
Advocate for policies to cut emissions and mitigate the effects of climate change (implied)
Push back against authoritarian tendencies through activism and awareness (implied)
Work towards preventing civil unrest by promoting unity and understanding in communities (implied)
</details>
<details>
<summary>
2022-08-15: Let's talk about Trump's claim about his passports.... (<a href="https://youtube.com/watch?v=NOP531r6hNc">watch</a> || <a href="/videos/2022/08/15/Lets_talk_about_Trump_s_claim_about_his_passports">transcript &amp; editable summary</a>)

Beau questions Trump's claim about stolen passports, hinting at potential legal trouble and advanced investigations while urging cautious interpretation of the situation.

</summary>

"If the Feds actually took his passports and intended to, and it wasn't a technical thing dealing with the archives or something like that, he's in a lot of trouble."
"I don't believe anything that he says based solely on the fact that he said it."
"That's a literal statement of, hey, you don't get to leave the country."
"It does look like for a while we will be running on that for a video schedule."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau expresses skepticism towards former President Donald Trump's claim about his passports, based on Trump's credibility.
Trump alleged that his three passports, one expired, were stolen by the FBI during a raid at Mar-a-Lago.
Beau clarifies the outdated meaning of "third world," correcting misconceptions about the term.
If Trump's claim is true, Beau speculates that the investigation may be more advanced than public knowledge suggests.
Beau outlines alternative explanations for the missing passports, including the possibility of diplomatic or technical reasons.
The seizure of Trump's passports could signify serious legal trouble for him, restricting his ability to leave the country.
Beau raises questions about the timing and implications of the alleged passport seizure.
He suggests that such actions typically occur during a hearing after an individual has been taken into custody.
Despite the seriousness of the situation, Beau encourages caution in drawing definitive conclusions without considering other possible explanations.
If the FBI intentionally took Trump's passports without a technical reason, Beau indicates that it could spell significant trouble for Trump.

Actions:

for political analysts, news enthusiasts,
Monitor developments in the investigation into Trump's passports (suggested)
Stay informed about the evolving news related to this incident (suggested)
</details>
<details>
<summary>
2022-08-15: Let's talk about Russia implying they have the Trump docs.... (<a href="https://youtube.com/watch?v=NlVhDJwY8uk">watch</a> || <a href="/videos/2022/08/15/Lets_talk_about_Russia_implying_they_have_the_Trump_docs">transcript &amp; editable summary</a>)

Russian media clips spreading unfounded claims about FBI search on former president's clubhouse, urging caution and sticking to reported facts to avoid misleading narratives and potential manipulation.

</summary>

"Stick to the facts as reported."
"Wait for the information to come out. Wait for the reporting. Wait for the documents."
"Don't let this bait encourage you to start speculating beyond the investigation."

### AI summary (High error rate! Edit errors on video page)

Russian media clips going viral in the U.S. claim FBI searched former U.S. president's clubhouse for secret info on a nuclear weapons program.
Caution against letting the narrative be shaped by unverified claims from Russian media.
Stick to the reported facts: FBI searched, found documents including TSSCI, defense information improperly stored by the former president.
Speculating beyond reported facts can lead to unmet expectations and diversion from the truth.
Warns of information operations aimed at manipulating public opinion and toying with U.S. intelligence.
Emphasizes staying in line with ongoing investigations and not jumping ahead to avoid misleading narratives.
Russia's goal is to sow division and destabilize the United States, making it vital not to let foreign narratives shape domestic discourse.
Suggests that Russia no longer views Trump favorably and may see him as a disposable asset.
Urges waiting for verified information, reporting, and documents before drawing conclusions.
Advocates for staying patient and not accepting unconfirmed information as facts to avoid falling into potential information operation traps.

Actions:

for news consumers,
Wait for verified information before forming opinions (suggested)
Avoid spreading unconfirmed claims (implied)
</details>
<details>
<summary>
2022-08-15: Let's talk about London and polio.... (<a href="https://youtube.com/watch?v=T0ai-jqiwQY">watch</a> || <a href="/videos/2022/08/15/Lets_talk_about_London_and_polio">transcript &amp; editable summary</a>)

Health officials in London are on high alert as they provide booster vaccines for children due to the unexpected resurgence of polio, stressing the importance of updated vaccinations to prevent further spread of the virus.

</summary>

"Make sure that the vaccines are up to date, because right now they don't know what's going on."
"This is something that it should be gone. It should be gone."
"The vaccines that are used in the United Kingdom and in the United States, they're not the oral vaccine anymore, and they don't have that same issue."

### AI summary (High error rate! Edit errors on video page)

Health officials in London are providing booster vaccines to children aged one through nine due to finding polio in the sewage of eight different boroughs.
Despite not having many reported cases, the concern is that the virus is spreading, and severe cases may not have manifested yet.
Most individuals infected with polio do not show symptoms, with only a small percentage suffering extreme symptoms.
The genetically similar virus found in London has no clear connection to cases in New York.
Health officials are urging everyone, especially children under five, to ensure their vaccinations are up to date.
The presence of polio in London is mainly attributed to vaccine gaps and potential strains from countries still using the oral vaccine.
It is emphasized that the vaccines used in the UK and the US do not present the same issues as the oral vaccine.
The situation with polio is unexpected and should have been eradicated by now.
Health officials are operating with caution due to the uncertainty surrounding the current spread of polio.
Keeping vaccinations up to date is vital to prevent the spread of polio.

Actions:

for parents, caregivers, community members,
Ensure that your and your children's vaccinations are up to date (suggested)
Stay informed about vaccine recommendations and updates (suggested)
</details>
<details>
<summary>
2022-08-14: Let's talk about the ultimate American test.... (<a href="https://youtube.com/watch?v=04vox0BttPM">watch</a> || <a href="/videos/2022/08/14/Lets_talk_about_the_ultimate_American_test">transcript &amp; editable summary</a>)

Beau invites viewers to test their knowledge with 10 questions from the American citizenship test, shedding light on the importance of civic education and challenging assumptions about immigrants' knowledge.

</summary>

"This is the ultimate test of America."
"Most people can answer five of them. Incidentally, that means you don't pass."
"America first, right?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a different format for his video, inviting viewers to participate in a quiz.
He presents 10 questions related to American history and government, encouraging viewers to write down their answers.
Questions range from the number of amendments in the U.S. Constitution to identifying writers of the Federalist Papers.
Beau provides answers to the questions, revealing that they are from the American citizenship test.
He mentions that most people can answer only five questions correctly, which is insufficient to pass the citizenship test.
Beau references a recent incident where someone suggested immigrants should pass a test before entering the country.
The person who made the suggestion agreed to take the test themselves but could only answer three questions.
Beau concludes by leaving viewers with some food for thought about citizenship tests and knowledge of American history.

Actions:

for viewers, citizens,
Take an American citizenship test (implied)
Engage in civic education to understand American history and government (implied)
</details>
<details>
<summary>
2022-08-14: Let's talk about the Rio Grande and other rivers.... (<a href="https://youtube.com/watch?v=8vSvi8GH5bY">watch</a> || <a href="/videos/2022/08/14/Lets_talk_about_the_Rio_Grande_and_other_rivers">transcript &amp; editable summary</a>)

Beau stresses the urgent need for real action and better water management at a federal level to address the escalating global water crisis.

</summary>

"We need big change, lots of change, better water management, better planning, better research."
"This isn't something that stops on a dime."
"Environmental issues need to get on the ballot and fast."

### AI summary (High error rate! Edit errors on video page)

Exploring the impact of water scarcity on rivers across the globe, not just the Colorado or southwestern United States.
Noting the drying up of the Rio Grande in Albuquerque for the first time in 40 years.
Mentioning significant changes in the Thames River in London, with its head reportedly moving two to five miles due to drying up.
Bringing attention to water issues with the Euphrates in Iraq, necessitating a deal with Turkey to prevent becoming a country with no rivers.
Stressing the need for real action, better water management, planning, and research at a federal level to address the escalating global water crisis.
Urging for environmental issues to become campaign topics and appear on ballots for immediate action.
Emphasizing that this issue won't disappear overnight and requires urgent attention and significant changes to mitigate its impact.
Calling on individuals, especially those in water-stressed regions, to start discussing and taking action on these critical water issues now.

Actions:

for global citizens,
Advocate for better water management practices in your community (implied).
Raise awareness about water scarcity and its impacts on rivers (implied).
Support and push for environmental issues to be included in political campaigns and ballots (implied).
</details>
<details>
<summary>
2022-08-14: Let's talk about Trump declassifying the docs.... (<a href="https://youtube.com/watch?v=k2CEEgIRGd0">watch</a> || <a href="/videos/2022/08/14/Lets_talk_about_Trump_declassifying_the_docs">transcript &amp; editable summary</a>)

Beau stresses the continued threat to national security posed by declassified documents and warns against seeking legal loopholes.

</summary>

"Those secrets were still there. Those people who generated intelligence, they were still at risk."
"There are no talking points that are going to change the fact that TSSEI documents and the secrets contained on those papers, even if he took a Sharpie and marked out TSSEI, those secrets were still there."
"Don't look for loopholes. There aren't any on this one, it's a big deal."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of understanding the secrets contained in declassified documents
Points out the risk to national security posed by improperly stored documents
Emphasizes that declassification of documents does not eliminate the threats posed by their contents
Challenges the notion that declassifying documents is a legal loophole
Suggests that retaining declassified documents could be considered a crime
Urges people to familiarize themselves with relevant laws before attempting to exploit loopholes
Stresses that declassified documents remain U.S. government property
Addresses the breach of national security and its significant implications
Asserts that political spin cannot change the reality of the compromised secrets in the documents
Encourages allowing due process to address the issue, without seeking loopholes

Actions:

for citizens, concerned individuals,
Stay informed about the implications of improperly handled classified information (implied)
Advocate for accountability and adherence to laws regarding classified information (implied)
</details>
<details>
<summary>
2022-08-14: Let's talk about Eli Lilly, Indiana, and more to come.... (<a href="https://youtube.com/watch?v=WGozkamvVC0">watch</a> || <a href="/videos/2022/08/14/Lets_talk_about_Eli_Lilly_Indiana_and_more_to_come">transcript &amp; editable summary</a>)

An Indianapolis company's decision to move jobs due to restrictive laws signals broader economic repercussions and challenges Republican leadership's choices.

</summary>

"It's not one company. There's a bunch. They just can't all announce it."
"This is your state on Republican leadership."
"Less jobs. That's the Republican Party victory."
"The economies of these states will suffer dramatically."
"They chose to embark on a multi-decade campaign to strip half the nation of their rights."

### AI summary (High error rate! Edit errors on video page)

Announcement out of Indianapolis, Indiana, regarding a global company planning for more employment growth outside of Indiana due to a new law.
Eli Lilly, a company headquartered in Indianapolis for over 145 years, is concerned about recruiting and attracting talent to a state that restricts rights.
The new law is a result of a Supreme Court reversal that has led to concerns about recruitment and job retention in Indiana.
This move by Eli Lilly may lead to job losses in Indiana and a shift of jobs elsewhere.
The impact of this decision goes beyond one company as other companies may silently be making similar decisions due to laws curtailing rights.
Companies with consumer-facing operations may choose to move jobs elsewhere to avoid upsetting a portion of the population supporting such laws.
Republican leadership's victory in enacting these laws may result in economic downturns in states implementing them.
Anticipated consequences include job losses, higher taxes, worsening education, and increased need for assistance as family planning rights are restricted.
States with such legislation may face serious economic challenges in the coming years, with impacts not fully felt until later.
The choices made by leaders to restrict rights based on outdated polling data may lead to long-term negative economic repercussions.

Actions:

for indiana residents, activists, policymakers,
Contact local legislators to express concerns about the economic impacts of restrictive laws (suggested).
Support organizations advocating for rights and fair employment practices in Indiana (exemplified).
Engage in community dialogues about the implications of legislation on job retention and recruitment (implied).
</details>
<details>
<summary>
2022-08-13: Let's talk about polling, planning, and a Republican question.... (<a href="https://youtube.com/watch?v=0uNYKHHJ-d8">watch</a> || <a href="/videos/2022/08/13/Lets_talk_about_polling_planning_and_a_Republican_question">transcript &amp; editable summary</a>)

Seven out of 10 individuals want a ballot initiative to vote on family planning rights, posing a challenge for the Republican Party to face the implications of these numbers on upcoming elections.

</summary>

"These numbers should matter."
"If we are a government by the people, these numbers should matter."
"That only exists in a few states."
"The hope for the Republican Party at this point is..."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau delves into recent polling data revealing that seven out of 10 individuals want a ballot initiative to vote on family planning rights in their state.
The polling data indicates that if given the chance, two to one individuals favor having family planning rights.
Demographic breakdown shows that 60% of women and 47% of men support having these rights.
Beau points out the significant discrepancy in support between men and women, reinforcing the narrative of men trying to control women's bodies.
Surprisingly, 34% of Republicans are in favor of having these rights that were taken away by the Republican Party.
Beau stresses the importance of these numbers in a government by the people, suggesting they should influence policy decisions.
He notes the absence of a ballot initiative on this issue in most states, prompting a focus on voting for representatives instead.
Beau predicts that voters may hold Republican representatives accountable for the loss of these rights.
He questions whether Americans will prioritize other issues over the rights revoked by the Republican Party.
Beau concludes by leaving his audience to ponder these political dynamics and their implications for the Republican Party.

Actions:

for voters, political activists,
Mobilize voters to prioritize reproductive rights in elections (implied)
Support candidates who advocate for family planning rights (implied)
Hold representatives accountable for their stance on reproductive rights (implied)
</details>
<details>
<summary>
2022-08-13: Let's talk about Cracker Barrel and sausage.... (<a href="https://youtube.com/watch?v=5vp5GiRnEpc">watch</a> || <a href="/videos/2022/08/13/Lets_talk_about_Cracker_Barrel_and_sausage">transcript &amp; editable summary</a>)

Cracker Barrel's plant-based sausage uproar reveals anti-woke culture as anti-freedom, seeking control over others' choices, reflecting the shift in the Republican Party towards dictating rather than representing.

</summary>

"They're not concerned about their own sausage. They're concerned about somebody else enjoying sausage."
"Anti-woke means anti-freedom. It means pro-control."
"They want to dictate from up on high what you can do, down to what kind of sausage you can enjoy."
"The Republican Party doesn't want to represent. They want to rule the people."
"Maybe it would be a whole lot better for everybody if people stopped trying to inspect each other's sausages."

### AI summary (High error rate! Edit errors on video page)

Cracker Barrel introduced a plant-based sausage as an additional option alongside their regular meat options.
Some segments of the public accused Cracker Barrel of going "woke" and threatened to stop eating there.
The outrage over a plant-based option at Cracker Barrel provides insight into the anti-woke culture.
Anti-woke culture is essentially anti-freedom, resisting new options and change out of fear and control.
Those resisting new options are more concerned with others enjoying something different than their own choices.
Anti-woke mentality ultimately seeks to control and dictate what others can do or enjoy.
The Republican Party is depicted as moving towards a stance of dictating rather than representing the people.
Beau suggests that people should stop being concerned about others' choices, like sausage preferences, and focus on their own lives.

Actions:

for general public, consumers,
Support businesses offering diverse menu options (implied)
Advocate for freedom of choice in products and services (implied)
</details>
<details>
<summary>
2022-08-13: Let's talk about 87,000 new IRS agents.... (<a href="https://youtube.com/watch?v=l67iRb0Dpe0">watch</a> || <a href="/videos/2022/08/13/Lets_talk_about_87_000_new_IRS_agents">transcript &amp; editable summary</a>)

Republicans spread false fear-mongering about 87,000 new IRS agents coming to audit people, part of a pattern of misinformation to scare their base.

</summary>

"It's fear-mongering. It's made up. It's just not true."
"There is no big conspiracy here. No giant plot to go after the working class."
"You can just add this to the giant list of things that prominent Republicans made up to scare their base."

### AI summary (High error rate! Edit errors on video page)

Republicans are spreading misinformation about 87,000 new IRS agents coming to audit people as a talking point against the Inflation Reduction Act.
The $78 billion allocated in the Act for the IRS is spread out over 10 years to expand and hire new employees.
Current IRS staffing is around 78,000, with half expected to retire in the next five years.
The new employees won't all be auditors; they will also include customer service and IT staff.
The fear-mongering claim that the new employees will focus on auditing the middle class or small businesses is false.
The report suggests that by 2031, the IRS could hire 87,000 new employees due to the investment.
The reality is that the increase in employees will be around 20,000 to 30,000, bringing the IRS back to previous staffing levels.
There is no grand conspiracy to target the working class; it's merely fear-mongering and misinformation.
Those worried about this misinformation are mostly W-2 employees who receive tax refunds, making them unlikely targets for collection.
This misinformation is just another example of prominent Republicans creating scare tactics for their base.

Actions:

for taxpayers,
Fact-check misinformation spread by politicians (implied)
</details>
<details>
<summary>
2022-08-13: Let's talk about 3 of your Trump search questions.... (<a href="https://youtube.com/watch?v=Bqw8G4FyRaA">watch</a> || <a href="/videos/2022/08/13/Lets_talk_about_3_of_your_Trump_search_questions">transcript &amp; editable summary</a>)

Beau delves into Trump's situation, addresses common questions, and urges patience and faith in the justice system.

</summary>

"What we really can't stand right now as a country is to have one political party going down the road of conspiracy theory and just wild claims."
"If you believe Trump committed a crime, you want that case to be airtight."
"You want that case to be perfect before it goes before a grand jury."

### AI summary (High error rate! Edit errors on video page)

Exploring the situation involving Trump and the ongoing search.
Addressing three common questions: Why isn't he in custody yet? What should happen? What if it was planted?
Acknowledging the slow pace of the investigation and Garland's approach.
Emphasizing the presumption of innocence for the former president.
Sharing thoughts on potential arrest and suggesting home confinement.
Responding to the idea of evidence being planted and challenging viewers to provide footage supporting that claim.
Urging patience and allowing the justice system to work methodically.
Cautioning against becoming emotional or demanding premature action.

Actions:

for concerned citizens,
Allow the justice system to work methodically (implied)
Avoid premature demands for action (implied)
</details>
<details>
<summary>
2022-08-12: Let's talk about Trump and TS/SCI documents.... (<a href="https://youtube.com/watch?v=L9adxOwl5QA">watch</a> || <a href="/videos/2022/08/12/Lets_talk_about_Trump_and_TS_SCI_documents">transcript &amp; editable summary</a>)

The Department of Justice's search for classified documents at Trump's residence raises concerns about national security and the mishandling of sensitive information, urging a reevaluation of political allegiances.

</summary>

"These are big secrets."
"Trump's actions undermine the entire classification system that the US has."
"That's wild."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The Department of Justice recently searched Donald J. Trump's home to recover documents, including those labeled TSSCI, suggesting a violation of the Espionage Act.
TSSCI stands for Top Secret Sensitive Compartmented Information, representing highly sensitive material that requires special protection.
Former military and intelligence personnel on social media are making statements about the seriousness of mishandling classified documents, contrasting them with the reality of consequences.
Trump claims the recovered documents were declassified before he left office, but the process of declassification is not solely at the president's discretion.
Beau criticizes Trump's actions for potentially compromising national security and undermining the entire classification system of the US.
Storing classified documents in an unsecure location by the pool raises concerns about the disregard for proper handling and protection of sensitive information.
Beau warns Trump's allies to reconsider their support, as standing by him after this incident could jeopardize national security and military strength.
Beau urges for a stop to the inflammatory rhetoric and baseless claims that could incite harmful actions from individuals influenced by political narratives.

Actions:

for politically engaged citizens,
Reassess political allegiances for national security (implied)
Stop spreading baseless claims and inflammatory rhetoric (implied)
</details>
<details>
<summary>
2022-08-12: Let's talk about Pennsylvania, Perry, and subpoenas.... (<a href="https://youtube.com/watch?v=2pguh5L8ESM">watch</a> || <a href="/videos/2022/08/12/Lets_talk_about_Pennsylvania_Perry_and_subpoenas">transcript &amp; editable summary</a>)

Several Pennsylvania Republicans visited by FBI, some not targets; DOJ investigation continues with fluid statuses, potential ties to fake electors scheme or Pence pressure campaign.

</summary>

"Different terms like witness, person of interest, subject, target have fluid meanings in investigations."
"People may believe they aren't targets but could end up as targets as investigations progress."

### AI summary (High error rate! Edit errors on video page)

Several Pennsylvania state House and Senate Republicans were visited by the Federal Bureau of Investigation or given subpoenas.
Some members were explicitly told they were not targets of the investigation.
Scott Perry stated that his lawyers were informed he's not a target and he's cooperating.
The Department of Justice, Office of Inspector General (OIG) is running the investigation.
Different labels like witness, person of interest, subject, target have fluid meanings in investigations.
The status of individuals in investigations can change as new information emerges.
The investigation is likely related to the fake electors scheme or the pressure campaign against Pence.
While current coverage focuses on classified material investigation, there are other ongoing investigations.
People may be involved in multiple aspects of the investigation with overlapping connections.
The Department of Justice is continuing its investigations despite political claims.

Actions:

for legal observers,
Stay updated on the developments in ongoing investigations (implied).
Stay informed about the different terms used in investigations and their fluid meanings (implied).
Cooperate fully with legal processes if involved in any investigations (implied).
</details>
<details>
<summary>
2022-08-12: Let's talk about North Dakota and a tale of two candidates.... (<a href="https://youtube.com/watch?v=ImPiNq-PY7s">watch</a> || <a href="/videos/2022/08/12/Lets_talk_about_North_Dakota_and_a_tale_of_two_candidates">transcript &amp; editable summary</a>)

Beau examines a U.S. House race in North Dakota with Karl Mundt, a former Miss America, potentially altering dynamics and political strategies.

</summary>

"Republican women who do not want to deny their daughters options, that's a pretty strong motivator."
"This is probably going to be somebody who's going to kind of level the race."
"Regardless of outcome, this is probably something we need to watch."

### AI summary (High error rate! Edit errors on video page)

Overview of a U.S. House of Representatives race in North Dakota with two candidates.
Republican incumbent vs. Democratic challenger dynamics until independents started showing up.
Mention of Karl Mundt, a former Miss America, as an independent candidate with celebrity status.
Details on Mundt's background: Brown and Harvard Law graduate, focused on racial justice, equality, volunteer work.
Media coverage focusing on Mundt's Miss America status rather than her platform.
Mundt's platform still being developed but leaning centrist and independent.
Potential impact of Mundt's name recognition and celebrity status on the race dynamics.
Speculation on Republican women in red states possibly voting for an independent like Mundt over a Democrat due to party loyalty.
Prediction that Mundt's candidacy could alter the dynamics of the race and potentially influence future political strategy.
Beau's closing thoughts on the significance of watching this race unfold.

Actions:

for voters, political analysts,
Monitor the U.S. House race in North Dakota for potential shifts in dynamics and political strategies (implied).
</details>
<details>
<summary>
2022-08-11: Let's talk about Trump's latest moves.... (<a href="https://youtube.com/watch?v=QwM0CjNFmAQ">watch</a> || <a href="/videos/2022/08/11/Lets_talk_about_Trump_s_latest_moves">transcript &amp; editable summary</a>)

Beau provides insights into the legal developments surrounding the former president, discussing implications of invoking the fifth amendment, internal turmoil within Trump's circle, and the potential for induced errors due to paranoia.

</summary>

"It's not the crime, it's the cover-up."
"As is often the case, you know, it's not the crime, it's the cover-up."
"As president, he'll have immunity."
"That's going to go over great."
"Trump 2024 or busted."

### AI summary (High error rate! Edit errors on video page)

Provides an update on the developments surrounding the former president, focusing on his recent legal situation.
Mentions that the former president invoked the fifth amendment over 400 times during a meeting in New York.
Talks about the potential implications of invoking the fifth amendment in a civil case and how it may not be the wisest legal strategy.
Reports on the existence of a confidential source at Mar-a-Lago causing turmoil within Trump's circle, leading to paranoia and finger-pointing.
Suggests that those feeling vulnerable within Trump's circle may be incentivized to cooperate with federal authorities.
Speculates that Trump may not be inclined to keep individuals around who he believes are cooperating, potentially leading to more people cooperating.
Mentions the floating of the idea that the FBI planted evidence and sarcastically suggests that the Trump team should run with that defense strategy.
Addresses questions regarding potential charges against Trump and dismisses exaggerated claims, stating that he has not been charged with anything yet.
Downplays the likelihood of the former president facing prison time and suggests the possibility of home confinement instead.
Comments on Team Trump fundraising off the situation and reports on considerations of Trump running in 2024 for immunity as president.
Points out that amidst ongoing investigations, the current situation is relatively minor and could potentially be a tactic to induce paranoia and mistakes.
Concludes by hinting at the possibility of induced errors due to paranoia within Trump's team and encourages viewers to have a good day.

Actions:

for political enthusiasts, legal observers.,
Stay informed on legal developments and their implications within political circles (exemplified).
Monitor how paranoia and internal conflicts can impact decision-making within powerful groups (exemplified).
Be aware of potential tactics used in legal cases to induce mistakes (exemplified).
</details>
<details>
<summary>
2022-08-11: Let's talk about Garland's statement on the Trump search.... (<a href="https://youtube.com/watch?v=IRyHUdBwbUI">watch</a> || <a href="/videos/2022/08/11/Lets_talk_about_Garland_s_statement_on_the_Trump_search">transcript &amp; editable summary</a>)

Beau delves into Merrick Garland's statement, addressing accountability, misinformation, and potential outcomes while awaiting the unsealing of a warrant and receipt to clarify the situation.

</summary>

"Stop talking bad about him."
"It really seems like he didn't want to go this route."
"Almost 1,000 people got arrested."
"It's going to take a lot of wind out of the sails of those who are trying to frame this as a political event."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau delves into Merrick Garland's recent statement, sparking curiosity and questions among viewers.
Garland emphasized equal application of the law, presumption of innocence, and a plea to stop criticizing FBI agents.
The Department of Justice filed to unseal a warrant and a property receipt, hinting at Trump's prior public comments.
Garland seemed to suggest that Trump should have remained silent, potentially avoiding the current situation.
He mentioned his approval of the search, leaving two possible interpretations: taking accountability or setting up a successful outcome narrative.
Garland's main concern appears to be countering misinformation that could incite harmful actions, referencing past events where false narratives led to arrests and harm.
By releasing accurate information, Garland aims to prevent the framing of the situation as a political event by certain individuals.
The unveiling of the warrant and receipt holds the key to clarifying the situation, urging patience until then.
Speculation surrounds the motives behind Garland's actions, with a reminder that Trump had the power to release the warrant and receipt earlier.
Beau concludes with a reflective statement, hinting at the ongoing intrigue and the importance of accurate information dissemination.

Actions:

for viewers, political observers,
Await the unsealing of the warrant and property receipt to gain clarity on the situation (implied).
Counter misinformation by sharing accurate information with others (implied).
</details>
<details>
<summary>
2022-08-11: Let's talk about China extending their exercises.... (<a href="https://youtube.com/watch?v=0nW-5yacEzc">watch</a> || <a href="/videos/2022/08/11/Lets_talk_about_China_extending_their_exercises">transcript &amp; editable summary</a>)

Beau explains why China conducts exercises around Taiwan: posturing, intelligence gathering, and training, hinting at potential peaceful reunification despite invasion concerns.

</summary>

"Posturing, intelligence gathering, training."
"War is almost never a good decision, and countries do it all the time."
"China probably also doesn't want to invade."

### AI summary (High error rate! Edit errors on video page)

China has extended exercises around Taiwan, causing news due to Taiwan official's invasion concerns.
Countries like China conduct exercises for posturing, intelligence gathering, and training.
Posturing is like being a tough guy in a bar, showing strength without wanting to fight.
Intelligence gathering involves observing reactions to the exercises for battle planning.
Training ensures all military pieces function together effectively.
Repeated exercises can create the "boy who cried wolf" syndrome, where exercises are seen as routine.
China's current posturing is influenced by the distracted US and internal political dynamics.
China may be extending exercises due to Taiwan not engaging in counter exercises.
Taiwan's response to the exercises was limited to artillery drills.
China may extend exercises to gather more explicit intelligence or because pieces didn't work well in training.
China, despite appearing confident, probably doesn't want to invade Taiwan but prefers peaceful reunification.
Invasion carries risks like damaging infrastructure and financial costs, which China wants to avoid.
The reasons for extending exercises could be posturing, intelligence gathering, training, or a potential live operation.
China likely wants to bring Taiwan in peacefully, considering the risks and costs of invasion.

Actions:

for international observers,
Keep informed about international relations and conflicts (implied)
Advocate for peaceful resolutions to conflicts (implied)
</details>
<details>
<summary>
2022-08-10: Let's talk about how Republicans can break from Trump.... (<a href="https://youtube.com/watch?v=Xfn1FduVhr4">watch</a> || <a href="/videos/2022/08/10/Lets_talk_about_how_Republicans_can_break_from_Trump">transcript &amp; editable summary</a>)

Beau questions the Republican Party's inability to break away from Trump, foreseeing future shifts post-midterms and warning against Trump-like behavior causing losses.

</summary>

"When did the Republican Party get so bad at politics?"
"He's bad for the Republican Party. He's damaging to it."
"All they have to do to just let Trump fade away is to just let him fade away."
"But apparently the Republican leadership is now so weak, they can't do that."
"Right now, it's all about keeping that base energized for the midterms."

### AI summary (High error rate! Edit errors on video page)

Questions the Republican Party's political maneuvering skills, separate from policy.
Criticizes the leadership for failing to shake off Trump despite the losses suffered.
Points out that Trump is damaging to the Republican Party and questions why the leadership cannot break away from him.
Suggests that letting Trump fade away does not require political maneuvering, just a lack of defense during scandals.
Foresees a push within the Republican Party to distance themselves from Trump after the midterms.
Expects a shift in Republican behavior post-midterms and notes dissatisfaction with Trump's actions, such as the NATO incident.
Anticipates that Republicans may not defend Trump as much in the future to keep the base energized for the midterms.
Warns that Trump-like elected officials could cause further losses for the Republican Party.
Concludes by predicting a future shift within the Republican Party due to tactical errors made.

Actions:

for political observers,
Stop rushing to Trump's defense during scandals (implied)
Encourage elected officials to break away from Trump (implied)
</details>
<details>
<summary>
2022-08-10: Let's talk about a weird story out of Michigan.... (<a href="https://youtube.com/watch?v=GpUfHewqNFo">watch</a> || <a href="/videos/2022/08/10/Lets_talk_about_a_weird_story_out_of_Michigan">transcript &amp; editable summary</a>)

Dana Nessel requests a special prosecutor to look into her political opponent, Matthew DiPerno, sparking national interest and wild accusations.

</summary>

"This is something that they'll be able to seize on, and you will see a lot of people make wild accusations."
"Given the tie to elections, the tie to Trump, and everything else that's going on, expect to see this story all over the news."

### AI summary (High error rate! Edit errors on video page)

Dana Nessel, the Democratic attorney general of Michigan, requested a special prosecutor to look into a Republican candidate, Matthew DiPerno, who is the presumptive nominee for attorney general.
The investigation by the Michigan State Police into breaching voting machines led to DiPerno, Nessel's political opponent.
The allegations against DiPerno are not proven yet but are outlined in the request for a special prosecutor to limit political appearance.
DiPerno, a Trump-backed candidate, denies the allegations as a political witch hunt, claiming they are untrue.
Despite accusations of political shenanigans, the timing seems off for this to be a strategic political move, as DiPerno hasn't formally secured the nomination.
The story is in its early stages and expected to become national news due to its ties to elections, Trump, and conspiracy theories.
Beau advises waiting for the appointment of a special prosecutor and their findings before drawing conclusions.
The saga is anticipated to attract attention from media and conspiracy theorists as it unfolds.
Beau suggests refraining from speculating until more information is available due to the potential sensationalism surrounding the case.
Given the high-profile nature of the story, Beau predicts widespread media coverage in the coming days.

Actions:

for michigan residents, political enthusiasts,
Stay informed about the developments in the investigation and the appointment of a special prosecutor (implied)
Avoid spreading or engaging with unverified information until official statements are released (implied)
</details>
<details>
<summary>
2022-08-10: Let's talk about 5 stories from Trump's very bad 48 hours.... (<a href="https://youtube.com/watch?v=KErWWzsGSCE">watch</a> || <a href="/videos/2022/08/10/Lets_talk_about_5_stories_from_Trump_s_very_bad_48_hours">transcript &amp; editable summary</a>)

Breaking down Trump's turbulent 48 hours: Giuliani faces a Georgia grand jury, a frantic search for top attorneys, testimonial consistency concerns, and legal circles tightening around Trump.

</summary>

"Trump might have issues finding attorneys that want to take on this task for a whole bunch of reasons."
"It's been described as Apprentice, get out of federal prison edition."
"Given some of their habits of being economical with the truth and kind of improvising a little bit when they're telling stories, that could be really bad for them."

### AI summary (High error rate! Edit errors on video page)

Breaking news prompted a departure from the normal video format to cover multiple topics related to Trump's troubled 48 hours.
Rudy Giuliani faced a Georgia grand jury after trying to avoid it, claiming he couldn't fly and being told to take a train or an Uber.
Trump is reportedly seeking a top criminal defense attorney as legal troubles intensify.
Republican congressperson Scott Perry had their phone taken by the feds, potentially linked to investigations involving Jeffrey Clark and John Eastman.
The House Ways and Means Committee can access Trump's tax returns after a 3-0 appellate court ruling in their favor.
Trump may testify in the New York civil case involving the Trump Organization, with implications for his legal team's strategies.
Testimonies from various locations must match, raising the stakes for those close to Trump who may have been inconsistent.
The converging legal challenges suggest a tightening circle around Trump and his associates, with ongoing developments expected.

Actions:

for political analysts, news followers,
Follow ongoing developments closely, especially related to legal proceedings and investigations (implied)
Stay informed about the evolving situation and its implications for the political landscape (implied)
</details>
<details>
<summary>
2022-08-09: Let's talk about the FBI searching Trump's home.... (<a href="https://youtube.com/watch?v=dSQLnykyySs">watch</a> || <a href="/videos/2022/08/09/Lets_talk_about_the_FBI_searching_Trump_s_home">transcript &amp; editable summary</a>)

The FBI's search warrant at Trump's home signals a significant development with high evidence suggesting wrongdoing, sparking speculation on the document's significance and potential legal implications.

</summary>

"This is really bad news."
"Does this mean that an indictment is imminent? No, it really doesn't."
"There might be overlap between them that we're not aware of."
"This is really bad for the former president."
"Those who may want to protect the institution of the presidency, my guess is that they have lost at least a major battle."

### AI summary (High error rate! Edit errors on video page)

The FBI executed a search warrant at former President Donald Trump's home, possibly related to the removal of classified documents from the White House.
The warrant indicates a high level of evidence suggesting a crime was committed and incriminating evidence may be in Trump's possession.
There are suggestions that some documents were returned and seized during the search, hinting at their importance.
Speculation revolves around whether the documents are classified or just should have been in the National Archives, with potential overlap between different cases.
FBI's actions indicate that the Justice Department may be overcoming hurdles to prosecute a former president, signaling a significant development.
While an imminent indictment is not guaranteed, the situation does not suggest that investigations into Trump will dissipate.

Actions:

for legal analysts, political commentators,
Stay informed on the developments and implications of the investigations (suggested)
Analyze the situation critically and contribute to informed discourse (implied)
</details>
<details>
<summary>
2022-08-09: Let's talk about Republicans calling for an FBI hearing.... (<a href="https://youtube.com/watch?v=uwAdGas0f90">watch</a> || <a href="/videos/2022/08/09/Lets_talk_about_Republicans_calling_for_an_FBI_hearing">transcript &amp; editable summary</a>)

Republicans' calls for investigations into the Department of Justice's handling at Mar-a-Lago are seen as attempts to frame Trump as a victim and energize their base, risking repeating harmful past actions.

</summary>

"This call, if anything, it shows the importance of DOJ continuing. Because they've learned nothing."
"I've seen this movie before. I know how it ends."
"Lie to the base, give them an enemy, rile them up, and therefore they'll coalesce together and support Trump."

### AI summary (High error rate! Edit errors on video page)

Republicans want to hold hearings to look into the Department of Justice's actions at Mar-a-Lago, with some calling to defund the FBI and labeling them as radically left.
Beau believes it's necessary to have hearings and investigations into how the Department of Justice handled the situation at Mar-a-Lago, despite not thinking they did anything wrong.
He speculates that a hearing might reveal that the Department of Justice moved slowly and was cautious, potentially influencing Trump positively.
Beau suggests that Republicans calling for investigations are not genuinely seeking corruption but trying to frame a narrative where Trump is the victim and to energize their base.
He points out the danger in portraying government institutions as enemies and warns against repeating past actions that led to harm.
Beau criticizes the tactic of lying to the base, creating enemies, and riling them up to support Trump, which he believes might backfire in the current political climate.

Actions:

for political observers, activists,
Monitor and advocate for transparent and unbiased investigations into government actions (implied)
Stay informed and push for accountability in political narratives (implied)
</details>
<details>
<summary>
2022-08-09: Let's talk about Dark Brandon.... (<a href="https://youtube.com/watch?v=OereBo7AQE8">watch</a> || <a href="/videos/2022/08/09/Lets_talk_about_Dark_Brandon">transcript &amp; editable summary</a>)

Beau explains the satirical meme "Dark Brandon," depicting President Biden as a radical progressive in control, shedding light on lesser-known achievements while clarifying Biden's actual policies.

</summary>

"Dark Brandon is a meme. It's a joke."
"It's ironic. It's a joke."
"It's worth looking at them. It's entertaining."
"Just understand that it's a joke."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the emergence of the internet meme "Dark Brandon," depicting President Biden as a radical progressive in control.
Describes the meme as a joke, originating from the idea of Biden being a centrist and some desiring a more progressive leader.
Notes the meme mocks Republican narratives attributing success to their chosen candidate while ignoring failures.
Observes how the meme has moved to the mainstream, creating confusion for those not familiar with online circles.
Suggests that the meme might actually benefit the Biden administration by shedding light on their lesser-known achievements.
Points out that Biden is not as progressive as some wish, being more center-right and a centrist.
Encourages understanding that "Dark Brandon" is meant to be entertaining and satirical, not a reflection of Biden's actual policies.

Actions:

for political enthusiasts,
Share informative content about Biden's actual policies and achievements (suggested)
Engage in political discourse to clarify misconceptions and share accurate information (implied)
</details>
<details>
<summary>
2022-08-08: Let's talk about what's in the Inflation Reduction Act.... (<a href="https://youtube.com/watch?v=DqjezsChg8o">watch</a> || <a href="/videos/2022/08/08/Lets_talk_about_what_s_in_the_Inflation_Reduction_Act">transcript &amp; editable summary</a>)

Beau outlines the Inflation Reduction Act, focusing on its financial, healthcare, and climate components, noting both positive steps and areas for improvement.

</summary>

"This isn't bad, but they should have gone further."
"The big win here is the climate."
"We have a long way to go."

### AI summary (High error rate! Edit errors on video page)

Explains the Inflation Reduction Act and its impending passage through the House, awaiting Biden's signature.
Notes the lack of focus on the actual content of the Act in public discourse, with more emphasis on political implications.
Acknowledges the significance of the Act for the Democratic Party, falling between transformative and insignificant.
Details the financial aspects, such as a 15% minimum tax on billion-dollar companies and a 1% excise tax on stock buybacks.
Mentions that the Act does not raise taxes for those earning less than $400,000 annually.
Outlines healthcare provisions including subsidies, a $35 cap on insulin, vaccination funds, and Medicare prescription cost negotiation.
Breaks down the $375 billion climate investment, the largest in U.S. history, covering clean energy, electric car rebates, carbon capture, methane emission fees, and drought alleviation funds.
Points out the Band-Aid nature of some climate solutions, like money for the Southwest drought and Colorado River conservation.
Addresses the criticism of increased federal land access for oil and gas drilling within the Act.
Concludes that while the Act is a positive step, it falls short of transformative change, especially in climate action.

Actions:

for policy advocates,
Contact your representatives to push for further climate change legislation (implied).
Get involved in local conservation efforts to address immediate environmental concerns (exemplified).
</details>
<details>
<summary>
2022-08-08: Let's talk about Finland, Sweden, NATO, and the future.... (<a href="https://youtube.com/watch?v=aijHpxWINSw">watch</a> || <a href="/videos/2022/08/08/Lets_talk_about_Finland_Sweden_NATO_and_the_future">transcript &amp; editable summary</a>)

Beau explains the addition of Finland and Sweden to NATO, clarifying misconceptions and asserting Russia's loss in the war.

</summary>

"Wars are not about the fighting. Wars have other goals. Russia lost."
"Unless they're rooting for Putin, they're on the wrong side of that decision."
"It increases NATO's influence. It increases NATO's security. It increases European security."

### AI summary (High error rate! Edit errors on video page)

Explains the addition of Finland and Sweden to NATO and addresses questions and concerns surrounding it.
Emphasizes that adding these countries to NATO does not increase U.S. security commitments, nor does it increase the likelihood of war; in fact, it decreases it.
Points out that alliances, especially with the addition of nuclear weapons, act as stabilizing forces rather than instigators of war.
Argues that Russia lost the war in Ukraine regardless of the fighting on the ground, citing the goals of the war and Russia's loss.
Asserts that politicians opposed to Sweden and Finland joining NATO are on the wrong side of the decision, supporting Russia's loss and increasing security for both NATO and Europe.

Actions:

for policy analysts, nato members.,
Advocate for informed foreign policy decisions (implied).
Support alliances for stability and security (implied).
</details>
<details>
<summary>
2022-08-08: Let's talk about Biden's public health declaration.... (<a href="https://youtube.com/watch?v=poSWNeiBmDA">watch</a> || <a href="/videos/2022/08/08/Lets_talk_about_Biden_s_public_health_declaration">transcript &amp; editable summary</a>)

The Biden administration declares a public health emergency in response to monkeypox, urging precautionary measures for all demographics.

</summary>

"Avoid kissing or more with people who have the rash."
"Be aware of this. And when we find out more about vaccine rollout and stuff like that, I'll let y'all know."
"I don't think it's a good idea to think, oh, this isn't my problem. I don't need to know anything about it if you're not in that demographic."

### AI summary (High error rate! Edit errors on video page)

The Biden administration declared a public health emergency, allowing access to emergency funding, accelerated vaccine production, and easier data collection.
The declaration was made in response to monkeypox, with around 6,600 confirmed cases in the U.S.
California, New York, and Illinois issued their own state-level declarations, and the World Health Organization called it a global public health emergency.
The messaging around monkeypox protection seems targeted to a specific demographic.
Guidelines to protect against monkeypox include avoiding skin-to-skin contact with rash-infected individuals, not sharing items that may come into contact with fluids, and frequent handwashing.
Monkeypox may spread through respiratory secretions; research is ongoing on transmission by asymptomatic individuals.
Beau stresses the importance of being informed and taking precautions, as diseases like monkeypox are not confined to specific demographics.

Actions:

for public,
Wash hands frequently with soap and water or sanitizer to prevent the spread of monkeypox (suggested).
Avoid skin-to-skin contact with individuals displaying monkeypox-like rashes to reduce transmission risks (suggested).
Stay informed about vaccine rollout updates and precautionary measures against monkeypox (implied).
</details>
<details>
<summary>
2022-08-07: Let's talk about Republicans claiming victory in Kansas.... (<a href="https://youtube.com/watch?v=QOCOdAiHj-A">watch</a> || <a href="/videos/2022/08/07/Lets_talk_about_Republicans_claiming_victory_in_Kansas">transcript &amp; editable summary</a>)

Republicans claiming to champion democracy after failed authoritarian moves, facing political consequences for trying to take away people's rights.

</summary>

"No, you don't get credit for being a champion of democracy because you're a failed authoritarian goon."
"You made it really clear what the goal was. This is your failure."
"You pushed them away. You tried to take away their rights. You villainized them."
"People had rights. You tried to take them away. Now you're suffering for it politically."
"Deal with it."

### AI summary (High error rate! Edit errors on video page)

Republicans claiming to be champions of democracy after a failed authoritarian move.
Kansas allowed family planning, so Republicans sent it back to states claiming victory.
Michigan fighting a law from 1931 with trigger laws in place, denying people a voice.
Republican Party wants to rule rather than represent the people.
Accusation of launching a multi-decade campaign to take away rights.
Consequences: economic and political costs for states following anti-rights decisions.
Accusation of painting women needing family planning services as evil.
Criticism of Republicans trying to reframe as champions of democracy after backlash.
People had rights, Republicans tried to take them away, now facing political consequences.
Republicans pushed people away by trying to take away their rights and villainizing them.

Actions:

for voters, activists, community members,
Organize community forums to raise awareness about reproductive rights (suggested)
Support organizations advocating for family planning services (implied)
Get involved in local politics to ensure representation over rule (implied)
</details>
<details>
<summary>
2022-08-07: Let's talk about Pelosi's Taiwan trip.... (<a href="https://youtube.com/watch?v=BLoEj8EGQg4">watch</a> || <a href="/videos/2022/08/07/Lets_talk_about_Pelosi_s_Taiwan_trip">transcript &amp; editable summary</a>)

Pelosi's trip to Taiwan wasn't as significant as portrayed, and media sensationalism often exaggerates international events in the context of a modern Cold War.

</summary>

"China's military invade somewhere because Nancy Pelosi went on a trip."
"Inflaming tensions right now, probably not a good move."
"It's another Cold War. There's going to be a lot of saber rattling, a lot of tensions."

### AI summary (High error rate! Edit errors on video page)

Pelosi's trip to Taiwan wasn't discussed earlier because it wasn't considered that significant.
The portrayal of Pelosi's trip as a trigger for military action between major powers is exaggerated and unrealistic.
China's potential invasion of Taiwan wouldn't be due to Pelosi's visit but instead part of pre-existing plans.
The US and China are engaged in a Cold War-like near-peer competition with little actual fighting.
Media profit motives often lead to sensationalized coverage of international events.
Inflaming tensions with China through Pelosi's visit may not have been a wise move, especially given the situation in Ukraine.
Russia is only a near peer to the US due to its nuclear capabilities, not overall strength.
Timing matters in international relations, and the timing of Pelosi's visit may not have been ideal.
The US aims to pivot towards the Pacific and establish a firm stance, which was part of the rationale behind Pelosi's trip.
International events are often sensationalized by the media, leading to exaggerated scenarios.

Actions:

for policy analysts, media consumers,
Monitor international events and question sensationalized coverage (implied)
Advocate for balanced and nuanced reporting on geopolitical developments (implied)
</details>
<details>
<summary>
2022-08-07: Let's talk about CMEs, Carrington, and the media.... (<a href="https://youtube.com/watch?v=FUo4REl7YUc">watch</a> || <a href="/videos/2022/08/07/Lets_talk_about_CMEs_Carrington_and_the_media">transcript &amp; editable summary</a>)

Beau explains solar storms, warns against media sensationalism, and encourages focusing on real global threats rather than hyped non-issues.

</summary>

"The reality is you have to watch for those types of articles, those articles that are making bold claims, predicting things that are scary."
"With everything that we have to actually worry about, we shouldn't spend time worrying about the things that aren't actually global threats."

### AI summary (High error rate! Edit errors on video page)

Explains solar storms, specifically CMEs and their potential impact on Earth.
Describes the scale of disruptions caused by CMEs, ranging from GPS issues to total electricity shutdown.
Mentions the current low-level CME headed towards Earth.
Emphasizes that despite media hype, this particular event shouldn't be a significant concern.
Notes that between now and 2025, such events may become more frequent, but mitigation plans are in place.
Criticizes sensationalist journalism that capitalizes on fear to generate clicks and revenue.
Stresses the effectiveness of early warning systems for solar storms.
Encourages awareness of legitimate threats and avoiding unnecessary worry about non-global threats.
Provides context on the Carrington event and suggests comparing it to the current situation for perspective.
Concludes by reassuring viewers that while disruptions may occur, they are unlikely to be catastrophic.

Actions:

for science enthusiasts,
Stay informed about solar storms and their potential impacts (implied)
Avoid sharing or engaging with sensationalist articles for clicks (implied)
</details>
<details>
<summary>
2022-08-06: Let's talk about the Jones verdict and what it means.... (<a href="https://youtube.com/watch?v=YCQoXbG2jB4">watch</a> || <a href="/videos/2022/08/06/Lets_talk_about_the_Jones_verdict_and_what_it_means">transcript &amp; editable summary</a>)

Beau Young explains the impact of substantial damages awarded in the Jones case and how it might shape disinformation narratives online.

</summary>

"This verdict and those that will probably follow are going to alter the dynamics of types of shows."
"I'm hopeful that this is going to make things a little bit better."
"There's going to alter their talking points on their shows to make them less open to legal action."
"I think it's going to change things."
"I don't think there's going to be a widespread epiphany from the base of these shows."

### AI summary (High error rate! Edit errors on video page)

Explains the substantial damages of around $49.3 million awarded in the Jones case.
Mentions that there are more cases against Jones pending, indicating a potential increase in liability.
Raises the possibility of the awarded amount being reduced due to a ratio for compensatory versus punitive damages in Texas civil law.
Speculates on the impact of the verdict on curtailing disinformation spread by conspiracy theorists like Jones.
Suggests that the outcome might not lead to widespread doubt among Jones' supporters but could prompt content creators like him to be more cautious with their claims.
Believes that individuals spreading misinformation for profit may change their narratives to avoid legal repercussions, shifting focus to topics like aliens that can't sue.
Anticipates a shift in the dynamics of such shows following the verdict, aiming to reduce real-world impacts rather than sparking a revelation among their audience.

Actions:

for content creators, audiences,
Monitor and hold accountable content creators who spread misinformation for profit (implied).
Advocate for stricter regulations on spreading false information online (implied).
</details>
<details>
<summary>
2022-08-06: Let's talk about an important message from New York.... (<a href="https://youtube.com/watch?v=Y7QJqRsTzxA">watch</a> || <a href="/videos/2022/08/06/Lets_talk_about_an_important_message_from_New_York">transcript &amp; editable summary</a>)

New York health officials raise alarm on community spread of polio, urging prompt vaccination to prevent escalation and protect public health.

</summary>

"New York state health officials are concerned about the possibility of community spread of polio."
"This isn't something that we can let get out of hand."
"This is a disease that was eradicated in the United States through vaccination."

### AI summary (High error rate! Edit errors on video page)

Addressing an emerging public health issue related to polio in New York.
New York state health officials are urging people to get vaccinated for polio due to concerns about community spread.
Polio virus has been found in wastewater samples from Rockland and Orange counties with low vaccination rates.
The confirmation of one paralyzed individual raises concerns about potential additional cases.
State health officials advise ensuring vaccinations are up to date before sending kids to school.
Emphasizing the importance of vaccination to prevent the spread of polio in the community.
Polio was previously eradicated in the U.S. through vaccination efforts.
Urging prompt vaccination to combat the re-emergence of polio.
Stressing the need to prevent the situation from escalating.
Encouraging individuals to consult with a doctor and get vaccinated as soon as possible.

Actions:

for health-conscious individuals,
Consult with a doctor to ensure vaccinations are up to date before sending kids to school (implied).
Get vaccinated as soon as possible to prevent the spread of polio in the community (implied).
</details>
<details>
<summary>
2022-08-06: Let's talk about Meadows, Trump, and the DOJ.... (<a href="https://youtube.com/watch?v=5PkvPcDSGuE">watch</a> || <a href="/videos/2022/08/06/Lets_talk_about_Meadows_Trump_and_the_DOJ">transcript &amp; editable summary</a>)

Trump's legal team navigates potential criminal liability amidst accelerating DOJ investigation, with expectations for significant developments soon.

</summary>

"The Department of Justice's investigation is speeding up. It is moving more quickly and it is very, very comprehensive."
"We know that it's a grand jury. We know that they're looking hard at Trump and associates."
"My gut tells me that there's probably going to be some pretty big news in the next 45 days or so."

### AI summary (High error rate! Edit errors on video page)

Trump's legal team is in direct talks with the Department of Justice, possibly to protect executive privilege.
Speculation suggests Trump's legal team warned him against talking to Meadows, hinting at potential criminal liability.
Trump's lawyers may be preventing him from talking to potential witnesses to avoid witness tampering charges.
The Department of Justice's investigation appears to be accelerating, indicating a comprehensive probe into a large conspiracy case involving Trump and associates.
Concerns arise that Trump's legal team may miss key aspects of the investigation, given its extensive scope.
Uncertainty remains about the possibility of an indictment and its impact on the institution of the presidency.
The investigation seems to be focusing on Trump as a central figure, potentially due to undisclosed evidence.
The Garland Justice Department operates discreetly, leading to speculation about impending announcements regarding Trump.
The lack of concrete information leads to various speculations, but the seriousness of the investigation is evident.
Expectations suggest significant developments in the investigation within the next 45 days.

Actions:

for legal observers, political analysts,
Stay informed about updates in the investigation (implied)
Monitor news outlets for any official announcements (implied)
</details>
<details>
<summary>
2022-08-05: Let's talk about what happened in Kansas.... (<a href="https://youtube.com/watch?v=8WAGPMtg6GU">watch</a> || <a href="/videos/2022/08/05/Lets_talk_about_what_happened_in_Kansas">transcript &amp; editable summary</a>)

Beau explains the voting trends in Kansas, predicting and analyzing the surprising shifts in rural areas regarding family planning rights, with insights into rural voter behavior dynamics.

</summary>

"This shouldn't have been a surprise."
"If there is anything that rural people know how to do, it's shut up."

### AI summary (High error rate! Edit errors on video page)

Explains the events in Kansas surrounding voting behaviors and family planning rights.
Mentions a video he previously made about growing up in rural Republican evangelical areas.
Predicted a shift in voting behavior among women related to family planning rights.
Details the voting patterns in Kansas counties in 2020.
Notes the surprising drop in support for preserving family planning rights in some counties.
Points out the high percentage of women in voter registrations since Roe was overturned.
Talks about the unexpected reaction of political analysts to the voting trends.
References the ranches and farms in Kansas to illustrate voting decisions.
Mentions rural people's tendency to keep their political choices private.
Concludes with a reflection on the voting behavior observed in Kansas.

Actions:

for voters, political analysts,
Preserve family planning options within communities (suggested)
Encourage open dialogues on voting choices and political decisions (implied)
</details>
<details>
<summary>
2022-08-05: Let's talk about the results of the Arizona primary.... (<a href="https://youtube.com/watch?v=Fmnmh9wLFLg">watch</a> || <a href="/videos/2022/08/05/Lets_talk_about_the_results_of_the_Arizona_primary">transcript &amp; editable summary</a>)

Arizona's Republican primary victory of "mini-Trumps" poses challenges as citizens rejected Trump, leading to potential voter turnout issues against Trumpism.

</summary>

"Putting up a bunch of Trumpy candidates makes sense."
"Arizona is Trump country when it comes to the Republican Party."
"The Republican Party has put itself in a position where you can't win a primary without Trump, but you can't win a general with him."

### AI summary (High error rate! Edit errors on video page)

Recap of the Arizona primary results where many "mini-Trumps" won.
Arizona being considered "Trump country" within the Republican Party due to his previous success in the state.
Trump's loss in Arizona during the 2020 election despite his earlier victory in 2016.
Republican Party now running candidates who are similar to Trump despite his loss in the state.
Citizens of Arizona rejected Trump before January 6th, before various events unfolded.
AG in Arizona debunked claims from an audit regarding deceased people supposedly voting.
Candidates who won the Republican primary focused on election security, now needing to address appearing deceived.
Arizona leaning towards a Democratic candidate for president for the first time since 1996.
Speculation that carbon copy Trump candidates may drive negative voter turnout as seen in previous elections.
The Republican Party faces a dilemma where they need Trump's support in primaries but struggle to win in general elections with him.

Actions:

for political analysts, voters,
Organize voter education drives on candidate platforms and election processes (suggested)
Mobilize voter registration efforts to increase civic engagement (suggested)
</details>
<details>
<summary>
2022-08-05: Let's talk about Republican icons talking about Trump.... (<a href="https://youtube.com/watch?v=g4RvcJoavKE">watch</a> || <a href="/videos/2022/08/05/Lets_talk_about_Republican_icons_talking_about_Trump">transcript &amp; editable summary</a>)

Beau delves into Dick Cheney's criticism of Trump, urging the Republican Party to choose between legitimacy and a cult of personality around Trump urgently.

</summary>

"Dick Cheney has been a Republican since the 60s. He has photos of him chilling with Nixon. He is the Republican Party."
"The Republican Party is facing a reckoning. They have to decide whether or not they want to be a legitimate political party, a conservative party."
"They have to make it now. There's not a lot of time to think about this one. You had that time for six years and it was wasted."

### AI summary (High error rate! Edit errors on video page)

Commentary on the future of the Republican Party, focusing on figures like Karl Rove and Dick Cheney speaking out against Trump.
Dick Cheney criticizes Trump, labeling him as a threat to the Republic and questioning his Republican identity.
Cheney's statements suggest that Trump is an authoritarian using the Republican Party for his gain.
Despite Cheney's own history, he recognizes Trump as a different, more extreme authoritarian figure.
The Republican Party is urged to choose between being a legitimate political party or a cult of personality around Trump.
Beau underscores the urgency for the Republican Party to make this decision promptly.

Actions:

for republican voters,
Organize community dialogues to critically analyze the current state of the Republican Party and its future (implied).
Reach out to fellow Republicans to have open and honest conversations about the direction of the party (implied).
</details>
<details>
<summary>
2022-08-04: Let's talk about Pat Cipollone's subpoena... (<a href="https://youtube.com/watch?v=3JkSjBLTUy0">watch</a> || <a href="/videos/2022/08/04/Lets_talk_about_Pat_Cipollone_s_subpoena">transcript &amp; editable summary</a>)

Pat Cipollone faces a DOJ subpoena, revealing deeper scrutiny into Trump with ethical disclosure negotiations ongoing.

</summary>

"DOJ is looking at Trump. There's no other reason to talk to Cipollone."
"This subpoena is very indicative of a Department of Justice that is looking into the Oval Office."
"If DOJ is smart, they'd recognize him as a witness that wants to provide information."
"This is going to be the real thing that kind of breaks through the wall to get inside that inner circle with all of Trump's people."

### AI summary (High error rate! Edit errors on video page)

Pat Cipollone received a subpoena from the federal grand jury, sparking a unique situation due to his prior testimony invoking executive privilege.
The Department of Justice (DOJ) issued the subpoena, not Congress, indicating a departure from their usual defense of privilege.
DOJ's involvement suggests a deeper look into Trump, as other information could be obtained without Cipollone.
There are ongoing negotiations to determine what Cipollone can disclose, with potential routes including discussing matters outside his duties or crimes where privilege doesn't apply.
Cipollone seemed ethically bound by executive privilege during his previous testimony, hinting that he might share more if those restrictions are lifted.
The length of the fight over privilege could be months, but Beau believes DOJ should facilitate Cipollone's ethical disclosures efficiently.

Actions:

for legal observers,
Support organizations advocating for transparency and accountability in legal proceedings (implied).
</details>
<details>
<summary>
2022-08-04: Let's talk about Jones, phones, and the committee.... (<a href="https://youtube.com/watch?v=p7LMUze6Dq0">watch</a> || <a href="/videos/2022/08/04/Lets_talk_about_Jones_phones_and_the_committee">transcript &amp; editable summary</a>)

The contents of a phone in a defamation suit involving Alex Jones create grounded speculation and anticipation for his prominent role in upcoming committee hearings.

</summary>

"There's a lot of grounded speculation going on."
"In season two of the committee hearings, Alex Jones is probably going to be a prominent figure."

### AI summary (High error rate! Edit errors on video page)

Talking about the contents of a phone in a defamation suit involving Alex Jones that drew a lot of interest.
Defense attorneys inadvertently turned over the entire contents of the phone to the plaintiffs.
Alex Jones, a conspiracy theorist with a show that has significant reach and influence.
Speculation about the contents of the phone and the likelihood of revealing incriminating information or establishing involvement.
Committee drawing up subpoenas to access the phone's contents within minutes of learning about it.
Grounded speculation about the potential revelations from the phone.
Alex Jones made unique claims about being asked to lead a march by the former president and being present.
The committee working to obtain the phone's contents to shed light on various activities.
Anticipation that Alex Jones will be a prominent figure in season two of committee hearings.
Uncertainty about the phone's contents but certainty about Alex Jones's future involvement.

Actions:

for legal analysts, conspiracy theory followers.,
Contact legal representatives to stay updated on the defamation suit proceedings (implied).
Follow credible news sources for updates on the situation (implied).
</details>
<details>
<summary>
2022-08-04: Let's talk about DOJ, Breonna Taylor, and Louisville.... (<a href="https://youtube.com/watch?v=N0MH-qtyjaA">watch</a> || <a href="/videos/2022/08/04/Lets_talk_about_DOJ_Breonna_Taylor_and_Louisville">transcript &amp; editable summary</a>)

Department of Justice charges cops in Louisville over Breonna Taylor's death, leading to further probes into the department.

</summary>

"Department of Justice charges four former or current cops in Louisville."
"Federal theory is that they falsified the initial search warrant."
"The Justice Department's process is unpredictable."

### AI summary (High error rate! Edit errors on video page)

Department of Justice charges four former or current cops in Louisville related to Breonna Taylor's death in March 2020.
Charges include civil rights offenses, obstruction, use of force, and unlawful conspiracy.
Allegation is that the cops falsified the initial search warrant leading to a comprehensive probe.
The investigation began around April 2021 and is now yielding results.
The Justice Department's process is unpredictable and catches people off guard.
Federal convictions tend to be higher than state convictions, possibly leading to plea deals.
Ongoing separate probe into the Louisville department for racial discrimination and excessive force.
Expect more developments from the ongoing department probe.
The charges may lead to convictions due to federal involvement.

Actions:

for justice seekers,
Contact local officials for updates on police reform probes (suggested).
Join or support organizations working on police accountability and racial justice (implied).
</details>
<details>
<summary>
2022-08-03: Let's talk about who will pay for climate research.... (<a href="https://youtube.com/watch?v=PBzPtjQNvuo">watch</a> || <a href="/videos/2022/08/03/Lets_talk_about_who_will_pay_for_climate_research">transcript &amp; editable summary</a>)

Addressing the flaws in for-profit research, Beau stresses the necessity of clean, limitless energy to combat climate change, urging for public or individual initiatives over corporate solutions.

</summary>

"There's no motivation for it to happen that way."
"It's one of the major flaws in the system that we currently have."
"If you are waiting for the corporations to save us, you're going to be waiting a long time."

### AI summary (High error rate! Edit errors on video page)

Addresses the topic of for-profit research in relation to mitigating climate change.
Contrasts NASA's research approach with SpaceX's for-profit endeavors.
Points out the lack of motivation for the market to solve issues like climate change.
Emphasizes the need for clean, limitless energy and water to address climate change effectively.
Questions the profitability of producing clean water versus bottling and selling it in limited supply.
Argues that for-profit companies lack the motivation to conduct research for solutions that may not be cost-effective.
Describes corporations as solely profit-driven entities that may impede research if it affects their bottom line.
Suggests that real solutions for climate change require either individual efforts or public funding.
Mentions the scarcity of research for diseases that affect a small population due to cost-effectiveness.
Advocates for research driven by knowledge-seeking goals rather than profit-driven motives.

Actions:

for climate activists, scientists,
Advocate for public funding for research into clean, limitless energy (implied).
Support individual initiatives focused on addressing climate change (implied).
</details>
<details>
<summary>
2022-08-03: Let's talk about the Senate bill to codify Roe.... (<a href="https://youtube.com/watch?v=WJ4yE8qPMd8">watch</a> || <a href="/videos/2022/08/03/Lets_talk_about_the_Senate_bill_to_codify_Roe">transcript &amp; editable summary</a>)

Beau analyzes a bill in the Senate to codify Roe, focusing on its symbolic nature and impact on voter turnout rather than its potential to pass.

</summary>

"It's not just putting people on record, because when the bill is destined to be defeated anyway, people can vote however they want."
"There is a wider purpose for this. It's not just putting people on record."
"This really seems to be more of a tool to drive voter turnout to get more seats for people who vote to codify it."

### AI summary (High error rate! Edit errors on video page)

Explains a semi-bipartisan bill in the Senate to codify Roe, with low chances of passing.
Suggests that the bill may be more about getting senators on record rather than passing.
Points out the potential purpose behind Democrats pushing this bill before midterms.
Mentions the bill's wider purpose of reminding constituents about the issue and driving voter turnout.
Notes that votes on this bill might serve as signals rather than impacting the outcome.
Acknowledges the slim chance of Republican senators supporting the bill due to generated heat.

Actions:

for voters, political observers,
Contact your senators to express support or opposition to the bill (implied).
</details>
<details>
<summary>
2022-08-03: Let's talk about Trump's lawyers preparing for charges.... (<a href="https://youtube.com/watch?v=A2tNDGxEKfo">watch</a> || <a href="/videos/2022/08/03/Lets_talk_about_Trump_s_lawyers_preparing_for_charges">transcript &amp; editable summary</a>)

Trump's legal team's preparation for possible criminal charges reveals a potential strategy of throwing others under the bus, which could heavily impact their cooperation decisions.

</summary>

"Trump's legal team should have been worried months ago."
"The part about them throwing people under the bus, that's the important part."
"There's no real new developments there."
"It might heavily influence their decision-making process."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Trump's legal team is reportedly preparing for the possibility of a criminal charge, which has caught people's attention.
The team should have been worried and preparing months ago, so them doing it now shows they are behind.
One potential strategy being considered is throwing people like Meadows or Eastman under the bus to protect Trump.
Broadcasting this strategy may not be wise as Meadows and Eastman could cooperate with prosecutors to protect themselves.
Hutchinson's testimony seems to have put Trump's legal team on edge, especially if corroborated by Meadows.
Overall, there are no significant new developments in the reporting, just showing that Trump's legal team may be running behind.
The focus should be on the possibility of throwing others under the bus, as it could heavily influence their cooperation decisions.

Actions:

for legal observers,
Stay informed on legal developments and strategies (implied)
Cooperate with legal authorities if approached for information (implied)
</details>
<details>
<summary>
2022-08-02: Let's talk about the wages of freedom.... (<a href="https://youtube.com/watch?v=RC-_fFq1mQA">watch</a> || <a href="/videos/2022/08/02/Lets_talk_about_the_wages_of_freedom">transcript &amp; editable summary</a>)

Freedom in the US is tied to money, with lack of funds leading to coercion and a life devoid of true freedom, perpetuated by politicians opposing fair wages.

</summary>

"Politicians who push back against the minimum wage are anti-freedom."
"In the US, freedom is equated with money."
"Those on the bottom are kept in line through coercion."
"Lack of money leads to a life of necessity and coercion, not freedom."
"Changing social classes in the US is rare and not easily achievable."

### AI summary (High error rate! Edit errors on video page)

Freedom is a term thrown around in the United States without a clear definition.
People often define freedom as the ability to do what you want.
In reality, freedom is about the absence of necessity, coercion, or constraint in choice or action.
In the US, freedom is equated with money due to the societal system.
Without money, individuals are coerced into working even when sick to avoid homelessness.
Politicians opposing minimum wage and living wage are, by definition, anti-freedom.
The coercion of the system keeps the lower levels of society in place.
Lack of money leads to a life of necessity and coercion, not freedom.
Changing one's social class in the US is rare and not easily achievable.
Politicians defending corporations over citizens perpetuate a system of coercion and lack of freedom.

Actions:

for advocates for fair wages,
Advocate for raising the minimum wage and a living wage for all (suggested)
Support policies that empower individuals financially (implied)
</details>
<details>
<summary>
2022-08-02: Let's talk about Trump and Arizona's test.... (<a href="https://youtube.com/watch?v=Y6ICuroIsdE">watch</a> || <a href="/videos/2022/08/02/Lets_talk_about_Trump_and_Arizona_s_test">transcript &amp; editable summary</a>)

Analyzing Arizona's Republican primary for clues on the GOP's stance on Trump might be misleading, as the state's unique support for him doesn't necessarily mirror the national sentiment.

</summary>

"Arizona is a place where Trump enjoys a lot of support."
"I don't think that's a good read at all."
"Trumpism is more popular than Trump at this point."
"I don't see Arizona as a good gauge for the rest of the country."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analyzing the results from Arizona's Republican primary, media tries to gauge if the party is breaking from Trump.
Arizona is a place where Trump has considerable support, where his grievances and theories resonate with the people.
Beau believes Arizona is not a good gauge to understand the national Republican Party's mindset.
The true indication will be if Trump's candidates are decisively defeated, signaling a move away from him.
Beau questions the media's strong connection between Trump and his endorsed candidates.
Trumpism appears more popular than Trump himself currently.
Conservative media turning on Trump and potential 2024 nominees gaining prominence are more significant factors.
Arizona is viewed as an outlier state due to its resonance with Trump's politics, making it unreliable for national predictions.
Beau stresses the importance of waiting to see impactful outcomes unless Trump's candidates face a significant loss.
Concluding with a call for patience and leaving it as food for thought for the audience.

Actions:

for political analysts,
Wait for decisive outcomes before drawing conclusions (implied)
</details>
<details>
<summary>
2022-08-02: Let's talk about Biden's move over there.... (<a href="https://youtube.com/watch?v=lu9liunbFjM">watch</a> || <a href="/videos/2022/08/02/Lets_talk_about_Biden_s_move_over_there">transcript &amp; editable summary</a>)

Beau explains the significance of Biden's strike, violation of agreements, and political pushback, anticipating no increase in US involvement in Afghanistan.

</summary>

"Yeah, it's really that big of a win for this campaign."
"The odds of them being as effective are slim to none."
"The only reason that this occurred is because they violated the agreement."
"It's not that they have some qualm with violating the sovereignty of another country."
"US involvement in Afghanistan is going to look exactly like it just did."

### AI summary (High error rate! Edit errors on video page)

Beau addresses three questions asked after Biden's announcement of a strike hitting Zawari.
He explains the importance of the strike, describing Zawari as a bad person with a history of hitting civilians directly.
Beau stresses that despite the strike, the group will likely continue with someone else stepping up, but it will be challenging to rally the same support.
The strike does violate the peace agreement with Afghanistan as the United States was not supposed to have assets in the country.
Beau points out that the right wing is pushing back because of past government decisions under Trump, not due to concerns about violating another country's sovereignty.
He believes the Biden administration may struggle to frame this as a significant win due to messaging issues.
Beau does not anticipate increased US involvement in Afghanistan, suggesting a continued standoff approach with selective responses.

Actions:

for policy analysts, political activists.,
Analyze and question government decisions (exemplified)
Advocate for transparent messaging from administrations (implied)
Stay informed and engaged with foreign policy developments (implied)
</details>
<details>
<summary>
2022-08-01: Let's talk about the Supreme Court altering football.... (<a href="https://youtube.com/watch?v=3ZtiYGQcbWI">watch</a> || <a href="/videos/2022/08/01/Lets_talk_about_the_Supreme_Court_altering_football">transcript &amp; editable summary</a>)

The Supreme Court ruling's impact on Southern football reveals deeper societal values and concerns, intertwining politics and sports loyalty.

</summary>

"Because for many of them, they are the sort that engages in bumper sticker politics. It's team mentality."
"In the South, football is life."
"This might be something to bring up."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court's ruling will alter Southern football due to overturned long-standing rulings, impacting recruitment and careers.
Concerns arise about players paying "gold diggers," dropping out to pay child support, and losing endorsement deals post the ruling.
Conservative individuals express worries that Roe being overturned may disrupt Southern football, affecting talent recruitment at universities.
Universities known for producing NFL players may struggle to attract talent, leading to a downturn in the quality of players they work with.
Despite not focusing on the right issues, the situation mirrors states enacting family planning restrictions leading to economic decline.
Many in the South equate football with life, showcasing the deep impact the ruling may have on communities.
People experiencing buyer's remorse over the decision due to its impact on their beloved sport and team loyalty.
The overlap between bumper sticker politics followers and sports enthusiasts is noted, hinting at a team mentality in approaching issues.
While not the ideal way to address the issue, discussing the ruling's impact on football might resonate with fence-sitters for whom football holds significant importance.

Actions:

for sports enthusiasts, political followers,
Initiate community dialogues about the intersection of sports, politics, and societal values (suggested).
Advocate for comprehensive sex education and reproductive rights to address broader issues impacting communities (implied).
</details>
<details>
<summary>
2022-08-01: Let's talk about NASA and climate change.... (<a href="https://youtube.com/watch?v=M7v5um8hQHk">watch</a> || <a href="/videos/2022/08/01/Lets_talk_about_NASA_and_climate_change">transcript &amp; editable summary</a>)

Beau explains the symbiotic relationship between NASA's innovations and climate change initiatives, advocating for support in both areas to address critical challenges effectively.

</summary>

"NASA's job is to basically make sure that people can survive in places where they shouldn't be able to."
"Most people, people care about the pebble in their shoe. And they're just starting to fill it."
"It's not either or. With the money that we spend on stuff that we really don't need to, I think that we can do both."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of NASA's projects in relation to climate change.
Acknowledges the need for urgent action to mitigate climate change impacts.
Lists various critical areas for immediate focus, such as water purification, pollution remediation, solar research, and food preservation.
Points out that NASA's technology has been instrumental in various everyday innovations like LASIK eye surgery, baby formula, and cordless vacuums.
Emphasizes NASA's role in developing technology to ensure human survival in challenging environments.
Advocates for continued support for NASA alongside dedicated climate change projects.
Suggests using NASA's track record of spinoff technologies as a tool to advocate for climate-focused projects.
Stresses the potential for significant benefits and spinoffs from well-funded climate change initiatives.
Encourages the idea of simultaneously supporting NASA and investing in climate change solutions.
Concludes with a call to action to recognize the dual benefits of supporting both NASA and climate change projects.

Actions:

for advocates and policymakers,
Support NASA initiatives and advocate for increased funding for climate change projects (implied).
Use NASA's technological advancements as examples to garner support for climate change initiatives (implied).
</details>
<details>
<summary>
2022-08-01: Let's talk about DARPA's Gambit.... (<a href="https://youtube.com/watch?v=cQwPJ3621lI">watch</a> || <a href="/videos/2022/08/01/Lets_talk_about_DARPA_s_Gambit">transcript &amp; editable summary</a>)

DARPA's Gambit project seeks to revolutionize military technology with an advanced air-to-ground missile using a rotating detonation engine, potentially impacting global power dynamics.

</summary>

"DARPA's Gambit project aims to develop an air-to-ground missile using a rotating detonation engine."
"This could be revolutionary when it comes to air-to-ground missiles and have broader applications in military technology."
"Russia and China will be watching this project very closely."

### AI summary (High error rate! Edit errors on video page)

DARPA's Gambit project aims to develop an air-to-ground missile using a rotating detonation engine (RDE), an advanced technology with possible military applications.
The RDE technology is an enhanced version of the pulse detonation engine (PDE) and could result in an incredibly fast, small, and fuel-efficient missile, potentially reaching speeds between Mach 4 and 6.
This 36-month project seeks to field a weapons prototype to counter near-peer adversaries, offering a significant advancement over hypersonic missiles.
The development of this system could have broader applications beyond missiles, potentially impacting aircraft technology in the future.
The project, named Gambit, will involve collaboration with defense industry partners to bring this concept to fruition.
Countries like Russia and China will closely monitor this project, as it could affect their strategies in deterring US forces.
If successful, this project could revolutionize air-to-ground missiles and have far-reaching implications for military technology.

Actions:

for military technology enthusiasts,
Stay informed about DARPA's Gambit project and its developments (implied)
Follow advancements in military technology and their potential implications (implied)
</details>
