---
title: Let's talk about Jones, phones, and the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p7LMUze6Dq0) |
| Published | 2022/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the contents of a phone in a defamation suit involving Alex Jones that drew a lot of interest.
- Defense attorneys inadvertently turned over the entire contents of the phone to the plaintiffs.
- Alex Jones, a conspiracy theorist with a show that has significant reach and influence.
- Speculation about the contents of the phone and the likelihood of revealing incriminating information or establishing involvement.
- Committee drawing up subpoenas to access the phone's contents within minutes of learning about it.
- Grounded speculation about the potential revelations from the phone.
- Alex Jones made unique claims about being asked to lead a march by the former president and being present.
- The committee working to obtain the phone's contents to shed light on various activities.
- Anticipation that Alex Jones will be a prominent figure in season two of committee hearings.
- Uncertainty about the phone's contents but certainty about Alex Jones's future involvement.

### Quotes

- "There's a lot of grounded speculation going on."
- "In season two of the committee hearings, Alex Jones is probably going to be a prominent figure."

### Oneliner

The contents of a phone in a defamation suit involving Alex Jones create grounded speculation and anticipation for his prominent role in upcoming committee hearings.

### Audience

Legal analysts, conspiracy theory followers.

### On-the-ground actions from transcript

- Contact legal representatives to stay updated on the defamation suit proceedings (implied).
- Follow credible news sources for updates on the situation (implied).

### Whats missing in summary

Details on the potential impact of the phone's contents on ongoing investigations. 

### Tags

#DefamationSuit #AlexJones #ConspiracyTheorist #CommitteeHearings #Speculation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about the contents of a phone
that came up today and immediately drew
a whole lot of interest.
We became aware of the contents of the phone
because in a suit, the defense attorneys inadvertently,
apparently, turned over the entire contents of the phone
to the plaintiffs.
The suit in question is a defamation suit
involving Alex Jones.
If you don't know who Alex Jones is,
he is a conspiracy theorist.
And he has a show that has a lot of reach
and a lot of influence.
Most people who are aware of Jones, most of them
view him as an internet meme, as a joke.
And while, yes, you can look at Jones that way,
it's important to note that he is really plugged in.
This is a person who knows a whole lot of people
in a whole lot of movements, movements
that are alleged to have been involved with some activities
on January 6th.
The contents of that phone, what it is, nobody knows yet.
It's speculation at this point.
But I think most people, myself included,
believe there is a really high likelihood
that there are things on that phone that, even if not
incriminating for various people,
they would certainly help establish who knew what, when,
and who was involved.
Because he really is plugged in.
He talks to a lot of people.
The committee agrees.
Reporting suggests that within minutes, minutes
of the revelation in the courtroom
that the plaintiff's attorneys have possession of this,
the committee was drawing up subpoenas
to get their hands on it.
Again, we don't know what's on it.
That's, anybody who is talking about it, it is speculation.
However, there's a lot of grounded speculation
that's going on.
Jones himself has made some very unique claims
about being asked to lead the march by the former president.
And was present.
There is probably messages on that phone
that are going to shed a lot of light on things.
And the committee is currently working
to get their hands on it.
While we don't know what the contents of that phone
will reveal, the one thing that is certain
is that in season two of the committee hearings,
Alex Jones is probably going to be a prominent figure.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}