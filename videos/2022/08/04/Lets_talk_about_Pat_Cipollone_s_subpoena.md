---
title: Let's talk about Pat Cipollone's subpoena...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3JkSjBLTUy0) |
| Published | 2022/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pat Cipollone received a subpoena from the federal grand jury, sparking a unique situation due to his prior testimony invoking executive privilege.
- The Department of Justice (DOJ) issued the subpoena, not Congress, indicating a departure from their usual defense of privilege.
- DOJ's involvement suggests a deeper look into Trump, as other information could be obtained without Cipollone.
- There are ongoing negotiations to determine what Cipollone can disclose, with potential routes including discussing matters outside his duties or crimes where privilege doesn't apply.
- Cipollone seemed ethically bound by executive privilege during his previous testimony, hinting that he might share more if those restrictions are lifted.
- The length of the fight over privilege could be months, but Beau believes DOJ should facilitate Cipollone's ethical disclosures efficiently.

### Quotes

- "DOJ is looking at Trump. There's no other reason to talk to Cipollone."
- "This subpoena is very indicative of a Department of Justice that is looking into the Oval Office."
- "If DOJ is smart, they'd recognize him as a witness that wants to provide information."
- "This is going to be the real thing that kind of breaks through the wall to get inside that inner circle with all of Trump's people."

### Oneliner

Pat Cipollone faces a DOJ subpoena, revealing deeper scrutiny into Trump with ethical disclosure negotiations ongoing.

### Audience

Legal Observers

### On-the-ground actions from transcript

- Support organizations advocating for transparency and accountability in legal proceedings (implied).

### Whats missing in summary

Insights on the potential impact of Cipollone's testimony on the investigation and the broader political sphere.

### Tags

#DOJ #Subpoena #ExecutivePrivilege #Trump #Investigation


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about Pat
Cipollone getting a subpoena from the federal grand jury and what that means and the unique
situation that it presents. So Cipollone's already talked to the hearing,
already provided testimony there, but there were times when he said,
privilege, you know, invoking executive privilege, limiting the scope of what he
feels he can talk about. And if you look back at the testimony,
almost all of it had to do with one thing and one thing in particular, Trump's exact words.
We don't talk about Trumpo. He gave everything else that he could. He didn't provide that
information. Now, DOJ is behind the subpoena. It's not Congress. One of the reasons executive
privilege exists is to help maintain the separation of powers. DOJ is part of the executive.
So there's something unique there in a normal timeline, in a timeline that doesn't include
the Department of Justice investigating a former president. It's the Department of Justice that
is making the argument that people like Cipollone don't have to testify. They're normally defending
privilege. This subpoena suggests that they're going to try to get through it. Now, given the
fact that it is the Department of Justice that is normally defending privilege, they know the
arguments. They created most of them. They know what Cipollone's attorneys are going to say.
They also know how to get through them. My understanding is that at this point,
there are conversations, discussions, negotiations going on about what Cipollone can say.
If you really kind of think about it, DOJ has a couple of routes they can go. One is ask him
about stuff that isn't within the scope of his duties. Therefore, executive privilege doesn't
apply. That's what you got at the hearing. The other is to say that if there's crime,
there's no privilege. Now, the thing with that is that they might be willing to allow
Cipollone to draw that line. When you look at his testimony from the hearing, this wasn't somebody
who was hiding behind the concept of executive privilege. This was somebody who felt restricted
by it, who wanted to provide more information but didn't because felt ethically bound by this
concept. If that concept is pierced, if it is removed, if the scope of what he is ethically
allowed to talk about is widened, I think that Cipollone would be willing to provide information
and draw the ethical line accurately. As far as this is something that might be related to a crime,
this isn't. But we'll have to see how that plays out. The other thing that this does is that it
shows without a doubt DOJ is looking at Trump. There's no other reason to talk to Cipollone.
None. Everything else could be gathered by other people. Fighting the privilege claim
is a fight that they would only need to undertake if they were trying to get to Trump.
Everybody else, everything else that's going on there, they don't need him. This subpoena
is very indicative of a Department of Justice that is looking into the Oval Office.
So there are reports suggesting that this fight over privilege could take a really,
really long time, months. I don't know that that's how this is going to play out.
It seems to me that if DOJ is smart, they would recognize him as a witness that wants to provide
information. And they just have to give him the mechanisms that allows him to do it in a way that
is ethical by his standards. I don't think that's going to be that hard.
Now, I could be wrong. Maybe Cipollone is hiding behind it. But that's not my read.
That's not what I saw in the testimony. So either way, this is something to continue to watch.
And this is going to be the real thing that kind of breaks through the wall to get inside that inner
circle with all of Trump's people. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}