---
title: Let's talk about DOJ, Breonna Taylor, and Louisville....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N0MH-qtyjaA) |
| Published | 2022/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice charges four former or current cops in Louisville related to Breonna Taylor's death in March 2020.
- Charges include civil rights offenses, obstruction, use of force, and unlawful conspiracy.
- Allegation is that the cops falsified the initial search warrant leading to a comprehensive probe.
- The investigation began around April 2021 and is now yielding results.
- The Justice Department's process is unpredictable and catches people off guard.
- Federal convictions tend to be higher than state convictions, possibly leading to plea deals.
- Ongoing separate probe into the Louisville department for racial discrimination and excessive force.
- Expect more developments from the ongoing department probe.
- The charges may lead to convictions due to federal involvement.

### Quotes

- "Department of Justice charges four former or current cops in Louisville."
- "Federal theory is that they falsified the initial search warrant."
- "The Justice Department's process is unpredictable."

### Oneliner

Department of Justice charges cops in Louisville over Breonna Taylor's death, leading to further probes into the department.

### Audience

Justice Seekers

### On-the-ground actions from transcript

- Contact local officials for updates on police reform probes (suggested).
- Join or support organizations working on police accountability and racial justice (implied).

### Whats missing in summary

Further details on the ongoing probe into patterns of racial discrimination and excessive force within the Louisville department.

### Tags

#Justice #PoliceReform #BreonnaTaylor #DepartmentOfJustice #Louisville #RacialJustice


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about some news out of Louisville
and out of the Department of Justice.
And it's news that I am sure a lot of people
have been waiting on.
A lot of people have probably given up hope
that they were ever going to hear this news.
But today, the Department of Justice
announced that four former or current cops there
in Louisville have been charged.
The charges stem from the death of Breonna Tyler
back in March of 2020, I think.
The charges are civil rights offenses, obstruction, use
of force and unlawful conspiracy.
Now, it's kind of hard to pin it down at the moment
because it's over a couple of indictments
and an additional information.
But it's pretty comprehensive.
The federal theory, the allegation,
is that they falsified, that some of them
falsified, the initial search warrant.
From that point forward, everything is bad.
This probe began in, I want to say, April of 2021.
We're just getting the results of it now.
This is how the Garland Justice Department works.
You don't know it's coming.
I'm willing to bet that there are people who watch this channel,
who watch channels that are very plugged in to what's happening in the Justice Department.
As far as I know, there weren't a lot of hints that this was coming.
Now anytime something like this comes up, the question is, okay, they were charged,
are they going to be convicted?
The feds are different than the state.
They have a much higher rate of gaining convictions.
I would also suggest that if the feds have half what they kind
of suggested they have, you might even see people
plea and not go to trial.
Generally speaking, when the feds are like, oh, yeah,
y'all had a conversation in a garage,
that's a really bad sign for the defense.
And that happened here.
Now, the other thing to note, and the other thing
that's important is that there is a separate ongoing probe
into the department as a whole, looking
into patterns of racial discrimination
and excessive force.
This probe has been going on a really, really long time.
Generally, if they don't find anything, the probe stops.
It, to my understanding, has not stopped.
So I would expect more.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}