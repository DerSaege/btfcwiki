---
title: Let's talk about if Trump did declassify the documents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OQrlJUHnzII) |
| Published | 2022/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the declassification of documents by Trump and its significance.
- Exploring the implications if Trump indeed declassified the documents.
- Speculating on various scenarios where Trump's staff failed to follow protocols.
- Pointing out that knowing the document content is critical if they were declassified.
- Emphasizing the severity of the lapse in handling defense information.
- Clarifying that the documents don't need to be classified, just contain defense information.
- Stressing that the declassification doesn't impact the legal aspects of the case.
- Mentioning the focus on defense information rather than classification in the law cited in the warrant.
- Contrasting the concerns over defense information leakage with classification status.
- Concluding that regardless of declassification, the severity of the lapse remains unchanged.

### Quotes

- "In order for him to declassify them, that means he knows what the content is."
- "It doesn't matter if he declassified them. It has no impact on the case whatsoever."
- "The defense information, that's all that's required for the law."

### Oneliner

Beau delves into the implications of Trump declassifying documents, stressing that knowing their content is key while noting that defense information, not classification, is legally significant.

### Audience

Researchers, analysts, concerned citizens.

### On-the-ground actions from transcript

- Research the laws cited in the warrant to understand the legal implications (suggested).
- Stay informed about defense information requirements in legal contexts (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of Trump's potential declassification and clarifies the legal significance of defense information over document classification.

### Tags

#Trump #Declassification #DefenseInformation #LegalImplications #SecurityBreach


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about
Trump declassifying the documents.
And we're gonna say that he did, okay?
The reason we're doing this is after the last video
where I was talking about the memoir defense,
I had a lot of people say,
hey, you're just ignoring the fact
that he declassified these documents.
Okay, that doesn't matter,
but basically a lot of the messages have the tone
that just because I say it doesn't matter
doesn't mean that it actually doesn't matter.
So what we're gonna do is we're going to go through
the idea, despite all likelihood,
that he actually declassified these documents.
I mean, we have to come up
with some explanations along the way,
but we're gonna do that.
So what we're gonna say is that while he was in office,
you know, he tapped him with his wand
and said, poof, they're declassified.
And he looked at a staff member and said,
hey, make sure you go through the protocols on this.
And that staff member just didn't do it.
Okay, therefore he doesn't know
that he's in possession of TSSCI documents, okay?
Because he thinks they've been declassified.
All right, I'll even go a step further.
Let's protect his staff member and say,
even the staff member did everything correctly
and just there's a bureaucratic mix up.
There's no record of this ever happening, all right?
Super unlikely, but let's say that it's true, okay?
So Trump declassified these documents
and then took them down there.
Or somebody else took them down there
and he just failed to return them when asked, all right?
Okay, that's worse.
You get how that's worse, right?
Do you? No?
In order for him to declassify them,
that means he knows what the content is.
He knows whether or not they contain defense information.
This is the point where the people who want to believe
that he declassified them and therefore everything's okay
need to actually go and read the laws cited in the warrant.
None of those laws require the documents to be classified.
They only require that there's defense information.
That's what it requires.
And if he declassified them, he knows what's in the document.
So he can't even pretend that he didn't know anymore.
It's not helpful.
It's actually not a defense.
So once again, the position being presented,
they're declassified.
It doesn't actually change anything related to the law.
The reason there are a whole bunch of people
who are screaming that he had TSSEI documents
is because that's really bad and not just Trump's behavior.
There is a whole counterintelligence operation
that is going to go on.
There's going to be an assessment.
There's going to be analysis.
There's going to be a...
They're going to hot wash this to death,
trying to figure out what went wrong
and how this even happened.
When people talk about the TSSEI documents,
it's not because that increases the penalty.
It's because it shows the severity of the lapse.
The defense information,
that's all that's required for the law.
As far as the commentary,
people are talking about the highest level
because that kind of defense information
never should have got out.
So it doesn't matter if he declassified them.
It has no impact on the case whatsoever
because the laws that were cited in that warrant
do not require the documents to be classified.
They just have to be defense information.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}