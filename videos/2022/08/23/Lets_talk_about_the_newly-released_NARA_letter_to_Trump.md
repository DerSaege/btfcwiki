---
title: Let's talk about the newly-released NARA letter to Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hc0b7OVeQD0) |
| Published | 2022/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- National Archives letter dated May 10th sent to Team Trump sparks commentary and speculation.
- Trump ally released the document believing it portrays the Biden administration as targeting Trump.
- The information silo mentality influences how Trump supporters interpret the document.
- Trump world may lack understanding of the laws governing the situation.
- Holding onto classified documents without returning them creates legal issues.
- The letter from the government aimed to get Trump to return the documents, not target him.
- Returning the documents promptly could have prevented any legal repercussions.
- Failure to return the documents led to the situation escalating.
- Misunderstanding of the law or being stuck in an echo chamber are possible reasons for releasing the document.
- The document's release may indicate a shift in allegiance away from Trump.
- The released document may not be favorable to Trump when examined critically.

### Quotes

- "For us commoners, this is the point where the FBI jumps out of a van, walks up and says, oh, okay, do me a favor, turn around, put your hands on the wall."
- "It shows a government providing a former president who may not really understand things every possible opportunity to just give the stuff back."
- "It doesn't demonstrate a concerted effort to get Trump."
- "When you delay, you are willfully retaining those documents."
- "I don't think that anybody who really sat down and read that document and looked at it could walk away thinking that it's good for Trump."

### Oneliner

The National Archives letter to Team Trump reveals a lack of understanding of laws and an information silo mentality, not a government conspiracy against Trump.

### Audience

Activists, Researchers, Voters

### On-the-ground actions from transcript

- Read the National Archives letter to understand the context (suggested)
- Educate others on the laws governing classified documents (implied)

### Whats missing in summary

Detailed analysis of the National Archives letter and its implications.

### Tags

#NationalArchives #TeamTrump #InformationSilo #UnderstandingLaws #LegalImplications #GovernmentLetter


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about
the National Archives letter dated May 10th
that was sent to Team Trump.
There's going to be a lot of commentary and speculation
about this document and why it was released
by a Trump ally.
Certainly some of the commentary
is going to include the phrase with friends like this,
who needs enemies and that's one of the big questions is why on earth would a
Trump ally release this? There's two answers. One is that when you're in an
information silo, when you're creating that information, there are times when
that's also what you start consuming. They believe that this document shows the
Biden administration out to get Trump, shows a government just intent on getting Trump.
That's what they think it shows.
I'm fairly certain that that's not how the rest of the United States, the average American,
is going to view this letter.
The information silo aspect is one part of it.
is I'm starting to think that Trump world literally does not understand the
laws governing this. I think they're viewing it more like trying to get
documents in a civil case. They don't know and since they don't know they'll
lose the battle, because knowing is half the battle, right?
Okay, so to put this in its simplest form, let's pretend I'm holding a document.
This document is full of national defense information, regardless of its classification.
It contains defense information, and it is property of the U.S. government.
I'm holding this document and a representative of the U.S. government walks up and says,
hey, we'd like that document back.
And I say, no, it's mine.
For us commoners, this is the point where the FBI jumps out of a van, walks up and says,
oh, okay, do me a favor, turn around, put your hands on the wall.
And that's it.
We get arrested at that point.
Rather than showing a government intent on getting Trump, it shows a government providing
a former president who may not really understand things every possible opportunity to just
give the stuff back.
That's what it actually shows.
It doesn't demonstrate a concerted effort to get Trump.
I am now of the belief that if Trump had returned these documents relatively quickly after they
were discovered missing, nothing would have happened.
Regardless of the fact that some of those documents were incredibly classified, nothing
would have happened.
It was the, well, it was the desire to keep control of them, some might say, willfully
retain the documents that brought us to this position.
That's how we got to where we're at.
That letter does not show a government attempt to get Trump on something.
It shows them giving every opportunity for him to just give this stuff back and this
go away.
Now, those are the only two reasons that I can come up with as to why this was released.
The person who released it has fallen into an echo chamber partially of their own creation
and actually believes that the majority of Americans have adopted that conspiratorial
mindset.
Or they literally do not understand how this law works.
The law is governing these documents.
They don't understand them.
You're viewing it like a civil case where you just delay, delay, delay, delay, delay.
Yeah.
When you delay, you are willfully retaining those documents.
That's not good under these laws.
It's one of the two, or maybe the guy is no longer an ally of Trump.
I don't know.
I mean, I don't think that anybody who really sat down
and read that document and looked at it
could walk away thinking that it's good for Trump
without one of those things at play.
I will try to find a link to the document,
have it down below so y'all can read it yourselves.
But yeah, it is definitely something else.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}