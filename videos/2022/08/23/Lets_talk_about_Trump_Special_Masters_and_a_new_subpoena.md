---
title: Let's talk about Trump, Special Masters, and a new subpoena....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uypNt8yhMhA) |
| Published | 2022/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Trump's major motion which was a general disappointment and involved requesting a special master to safeguard privileged information from law enforcement.
- Clarifies that the case will not likely hinge on the success of this motion.
- Points out that the motion is related to a search at Mar-a-Lago on August 8th, just two weeks before the motion was filed.
- Mentions a grand jury subpoena issued to the National Archives on August 17th, seeking more information about events before and after January 6th, which appears to be more critical than Trump's motion.
- Notes that the Department of Justice seems to be accelerating the investigation and may soon request Trump's communications during that period.
- Concludes by suggesting that it has not been a great start to the week for the former president.

### Quotes

- "The main part was asking for a special master."
- "It separates information that's privileged from law enforcement."
- "A new one that is looking for more information, more records, more information."
- "Not a great start to the week for the former president."
- "Y'all have a good day."

### Oneliner

Beau explains Trump's disappointing major motion and a significant grand jury subpoena, indicating a troubling week for the former president.

### Audience

Legal analysts, political commentators.

### On-the-ground actions from transcript

- Analyze the legal implications and potential outcomes of the grand jury subpoena (implied).
- Stay informed about the developments in the investigations related to Trump's communications during critical periods (implied).

### Whats missing in summary

Deeper insights into the potential legal consequences and political ramifications of the investigations discussed in the transcript.

### Tags

#Trump #LegalAnalysis #DepartmentOfJustice #Investigations #SpecialMaster


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Trump and his major motion and special masters and
a subpoena that I don't feel like got the coverage it should yesterday.
But before we get into that, because I do put Easter eggs on the shelves, I had people
ask why George was moved. There are some young people who watch the channel with their family
and they were very concerned that George was going to be smushed by the boxes.
So he has changed position. That's not some joke that nobody's getting.
Okay, so Trump's major motion turned out to be a general disappointment.
The main part was asking for a special master. A special master is a person who adds an additional
layer of safeguard on top of the filter team.
And what it does is it separates information that's privileged from law enforcement.
That's the purpose.
I don't think that the case is going to hinge on whether or not this motion is successful.
I'd like to remind everybody that this motion is in relation to the search at Mar-a-Lago
that took place on August 8th, two weeks before the motion was filed.
Two weeks in a national security investigation.
There's no way that on August 9th, a whole bunch of counterintelligence agents walked
into a room and the lead on the case was like, everybody grab a box.
So this is still a very relevant, important motion, I'm sure.
The thing I think that might have been overlooked is that in the other case, no not the one
in Georgia, no not the one in New York, the federal DOJ case looking into the events of
the 6th, that one, the one that is looking into the alleged fake electors attempt to
stay in power and disrupt the peaceful transition of power, the coup thing.
So there was a grand jury subpoena that was issued to the National Archives on August
17th, a new one, not the one that everybody already knew about.
This is a new one that is looking for more information, more records, more information
in a period both before and after January 6th.
That is probably more important now than the motion.
It appears as though the Department of Justice is also quickening the pace of that investigation
and it seems as though they might be moving to the point where they are going to ask for
Trump's communications during that period.
Not a great start to the week for the former president.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}