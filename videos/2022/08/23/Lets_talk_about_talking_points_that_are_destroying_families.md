---
title: Let's talk about talking points that are destroying families....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hPFF-RPyM7Y) |
| Published | 2022/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overheard a man in a grocery store expressing anger over LGBTQ content in schools.
- Shared statistics on LGBTQ identification percentages in the US.
- Explained how policies and rhetoric impact LGBTQ children in schools.
- Questioned the impact on parents if their child is LGBTQ.
- Discussed the consequences of parents prioritizing rhetoric over family.
- Revealed a personal anecdote about a man in the grocery store with an estranged LGBTQ son.
- Urged against destroying families over hateful rhetoric.
- Encouraged reflection on prioritizing family over divisive talking points.
- Ended with a message of reflection and well-wishes for the day.

### Quotes

- "It seems like it wouldn't be worth it to destroy your family over some slick graphics and bad talking points."
- "Overheard a man going off about normal right-wing talking points."
- "So when these policies, and these talking points, and this rhetoric gets pushed, it's impacting a child in every class."

### Oneliner

Beau shares a powerful story of how divisive rhetoric can rip apart families, urging against prioritizing hate over loved ones.

### Audience

Parents, Families, Advocates

### On-the-ground actions from transcript

- Prioritize open communication and understanding within families (implied).
- Challenge harmful rhetoric and discriminatory policies that impact LGBTQ children (implied).

### Whats missing in summary

The emotional impact and depth of the personal story shared by Beau.

### Tags

#LGBTQ #Family #Rhetoric #Community #Hate #Statistics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about a conversation
I overheard.
We're going to talk about some statistics
and we are going to talk about how some talking
points and rhetoric that is coming out
of the right-wing family values crowd
is ripping apart right-wing families.
This is one of those stories where the names and locations
have been changed to protect the innocent.
I'm in a, let's say it's a grocery store.
And I'm walking along, and I hear this guy.
And he is going off about normal right-wing talking points.
And he's super mad that there is so much gay stuff in schools.
He doesn't believe it should be there.
Now, he doesn't say gay stuff.
He uses some terminology that I would not use on this channel,
which is kind of why it caught my ear.
Because while small towns in the southern United States
may not be the most welcoming places on the planet
for this community, there are also
some things in small towns in the U.S., in the South, that you don't talk about in public,
loudly.
People kind of are polite, they're kind, they watch their language, stuff like that.
This wasn't happening.
So that's what caught my ear.
And I hear him, and I kind of look over at him, he doesn't look at me, but whatever,
and I go on about my business.
I take two, maybe three steps before something clicks.
So here's some statistics for you.
According to a recent Gallup poll,
7.1% of adults in the US identify as LGBTQ.
I have seen studies that say one in five Gen Z adults identify
that way.
And in 2017, the CDC did a study that said 8% of high school
teens identify that way.
So no matter what, no matter how you break it down,
in every class of 20, there's at least one.
There's at least one in every class of 20.
So when these policies, and these talking points,
and this rhetoric gets pushed, it's
impacting a child in every class, in every school where
this stuff gets implemented.
And the people who support it, they support it publicly.
They talk about it.
What if that one student in that class,
What if that's your kid's friend?
What if that's like their best friend?
What if they hang out all the time?
What do you think your kid is going to think of you
and your rhetoric?
Probably going to drive a little bit of a wedge there.
They're probably going to lose respect for you,
but there's another question.
What if that one kid in 20, one kid in each class,
What if it's your kid? Oh, you would know, right? Would you though? If you had that
kind of attitude, something that would be displayed publicly, do you really expect
your child to tell you? They probably won't. They wouldn't because they don't
want to deal with the fallout. Imagine missing out on major parts of your
child's life simply because you listened to some talking head who doesn't care
about you, has never met you, does not care about your kid, but you're willing
throw away your family for them because they told you to be mad. That's something
else and it's what happened to that guy in that grocery store because that's
what clicked. His son lives a couple counties away with a roommate. I know his
roommate. They're pretty close. I am certain that this guy has no clue. Some
person he has never met knows more about his kid than he does. He probably thinks
the reason the kid doesn't want to have anything to do with him is because of
his drinking or whatever. No, it's because he's parroting these bigoted talking points.
And when you say stuff like this, you could be talking about your kid's best friend
or your kid, and not even know it.
It seems like it wouldn't be worth it to destroy your family over some slick graphics
and bad talking points.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}