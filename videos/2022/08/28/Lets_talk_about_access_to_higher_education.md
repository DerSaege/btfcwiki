---
title: Let's talk about access to higher education....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=D2NDt1yJzDk) |
| Published | 2022/08/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the opposition to universal access to higher education and free college.
- Describes how some view degrees as membership cards for higher paying job clubs.
- Points out the class barrier maintained by keeping education expensive.
- Talks about the fear of competition from bright, poor students by mediocre, upper-class kids.
- Emphasizes the need to break down class barriers and level the playing field in education.
- Challenges the notion of meritocracy and exposes the fear of competition among the privileged.
- Addresses the existence of class barriers in the United States and the resistance to breaking them down.

### Quotes

- "It's credentialing. It's a club pass. It gets you into the club where the higher paying jobs are and that's it."
- "Maybe somebody will get a little bit of social mobility out of it."
- "Everybody's getting awfully scared of a little bit of competition all of a sudden."
- "The system's broke in case you haven't figured it out."
- "We pretend these class barriers don't exist."

### Oneliner

Beau explains opposition to free higher education as perpetuating class barriers, challenging the fear of competition among the privileged, and advocating for breaking down social divides.

### Audience

Education advocates, activists

### On-the-ground actions from transcript

- Advocate for policies that support universal access to higher education (suggested)
- Support scholarships and financial aid programs for students from lower socioeconomic backgrounds (implied)

### Whats missing in summary

The full transcript provides a deep dive into the societal implications of opposition to universal access to higher education and the perpetuation of class barriers through expensive education systems. Viewing the full transcript will provide a comprehensive understanding of these issues. 

### Tags

#HigherEducation #ClassBarriers #FreeCollege #Meritocracy #SocialJustice


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
universal access to higher education, free college, and barriers, and the
opposition to people being able to go to college if they choose, and where that
opposition sometimes comes from. We're going to do this because in a recent video I
talked about how I was very much in favor of the idea of universal higher
education. I think it's a good thing. It's a good thing for individuals. It's a good
thing for society as a whole. And there's that pushback. Generally the pushback was
centered around the idea that if degrees were free, well then they would be
worthless. I chose one of a couple messages that had this tone to read.
Universal free education only screws people who can afford one now. If
everybody can get a degree for free, they're worth less. That's the part you
don't understand. Learn to economics. Okay. I know that that attitude for a lot of
people watching this channel is going to be very strange. So let me break down
that position first so people understand where they're coming from with this. For
most people who watch this channel, you view a degree as a symbol of education,
right? This means that you got education in this topic. For people who have this
position, nah. It's a membership card. It has nothing to do with that. It's
credentialing. It's a club pass. It gets you into the club where the higher
paying jobs are and that's it. It has that value. And they're not entirely
wrong about that. That degree does open doors to higher paying jobs. Over the
course of your life, the median income for somebody who went to high school is
1.6 million. For somebody who went to college is 1.9. Bachelor's degree 2.8.
Master's 3.2. Doctoral 4. So the position that a degree opens doors and gets you
into higher paying jobs, that's generally true. Now understand this is a median.
There are exceptions to this. But overall it's true. So when you look at it like
that, what are these messages saying? Short and sweet. They want to keep that
class barrier up. See those people who can afford and buy this, I mean those
people who have parents who can afford to send them to college without
saddling them with a bunch of debt and stuff like that, they get to stay in that
upper-class club. They know they're going to get one of those jobs. They've got the
degree. They have the credentials and they're good to go. They have the
connections. What they don't want is to have to compete against people outside
of that class. And see when you're talking about those on the lower rungs
of the socioeconomic ladder, the way that they can get to university is through a
scholarship or through taking on a bunch of debt. If you take on a bunch of debt
though, even once you get into that upper-class club, well you're paying that
debt back. So you may not have the opportunity to make the moves necessary
to keep you in that upper-class club. Right? That's just a lottery for the
commoners. Maybe somebody will get a little bit of social mobility out of it.
The goal of this is to make sure that education is expensive so the commoners
stay where they're supposed to be down on the bottom. That's the goal. And if
education was free, if people could access higher education just based on
merit, right? Just on their ability to invest the time and get the grades, well
that means that the mediocre upper-class kid, he has to compete against the really
bright poor kid. And that's somebody he doesn't have to compete against right now.
And that's scary because it undermines that class system. That class divide
shrinks when that occurs.
Okay, so at this point, to the person who sent this message, I hope it's clear that
it's not that I don't understand that this is what's going to happen, it's that I'm
counting on it. I'm not a person who thinks that upholding unjust, unnecessary
hierarchies is a good thing. For those people who always like to talk about
merit and pretend to be self-made, everybody's getting awfully scared of a
little bit of competition all of a sudden. Don't worry, your parents'
connections through their friends and all of that stuff, and the fact that the
poor kid who makes it into your circle may not know all the proper etiquette
and those rules and know how to frame the thank-you notes and all of that,
don't worry, that'll still keep your position secure. I think this gets to a
crux of an issue that exists in the United States, where we pretend these
class barriers don't exist. We pretend it's all about merit. It's not, because
anytime somebody tries to remove one of these class barriers, people freak out.
And they talk about how it'll just be so upsetting to the system. Yeah, that's the
point. The system's broke in case you haven't figured it out. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}