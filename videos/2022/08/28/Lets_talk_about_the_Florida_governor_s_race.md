---
title: Let's talk about the Florida governor's race....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hcNduOXZ_9g) |
| Published | 2022/08/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Florida governor's race is being compared to a mini 2020 presidential election, with DeSantis as a mini Trump and Crist as a mini Biden.
- Democratic party faces enthusiasm issues with Crist, seen as electable but lacking in voter drive.
- Democratic victory strategy banks on negative voter turnout against DeSantis, similar to how people voted against Trump.
- DeSantis faces the challenge of potential sabotage from the real Trump before the election.
- Nikki Fried's main goal is to oust DeSantis, rallying behind Crist to achieve this.
- DeSantis must win Florida convincingly to maintain his presidential aspirations.
- Uncertainty surrounds the election outcome due to variables like negative voter turnout and real Trump's actions.

### Quotes

- "This is a mini 2020 presidential election."
- "My guess is that's not just empty words."
- "The stakes are incredibly high for DeSantis."
- "I can't make a prediction on this."
- "The stakes are pretty high for DeSantis."

### Oneliner

Beau breaks down the Florida governor's race as a mini 2020 presidential election, with DeSantis facing high stakes and the Democratic victory hinging on negative voter turnout against him.

### Audience

Florida Voters

### On-the-ground actions from transcript

- Rally behind Charlie Crist to defeat DeSantis (implied)
- Stay informed about the candidates' positions and actions (suggested)

### Whats missing in summary

Insight into the potential impact of the Florida governor's race on national politics.

### Tags

#Florida #GovernorRace #DeSantis #Crist #NikkiFried


## Transcript
Well, howdy there, internet people, It's Bo again.
So today we are going to talk about the Florida governor's
race.
A lot of questions have come in about it.
I think a lot of it's prompted by the fact
that we had Nikki, one of the candidates, Nikki Fried,
over on the other channel for an interview,
very well received by a lot of you.
She didn't win.
Charlie Crist won the nomination.
there's a lot of questions about how it's shaping up. And for those outside Florida,
the easiest way to frame it is that this is a mini 2020 presidential election. It's a replay.
You have mini Trump in DeSantis, far right authoritarian. You have mini Biden in Christ,
centrist, long legislative history. That's what you've got. For the Democratic party,
I think they're going to have a problem with enthusiasm.
Crist is somebody who people looked at as, well, he's electable, right?
But that doesn't always drive people to show up on election day.
They're going to have to hope that many Trump creates as much negative voter turnout, people
showing up to vote against DeSantis as real Trump did.
That's their most likely course to victory.
Now for DeSantis, his worry is something entirely different because he has to worry that real
Trump is going to do something between now and the election that could torpedo him.
And that's not an unreasonable fear.
The former president is, well, somewhat erratic these days, and it's not out of the realm
of possibility that some of his behavior kind of blows onto DeSantis.
It's also not impossible for the former president to lash out at DeSantis because he doesn't
feel like DeSantis is protecting him or whatever because Trump's in Florida as
well. Now Nikki, because people have asked where she's at on it, she has always
been very upfront about the fact her main purpose in running is to get
DeSantis out. She's already released a statement, we have to make Ron DeSantis
a one-term governor and now that means rallying behind Charlie Crist. My guess
is that's not just empty words. After most concessions, the candidate who lost says,
we have to rally behind our party and all of that, and then that's the end of it. That's
probably unlikely in this case. I have a feeling that she might get out there and actually
try to rally people behind Crist, just on the platform of, we're trying to get rid
of DeSantis.
Now the stakes are incredibly high for DeSantis, because if he doesn't win Florida, that's
it, his presidential aspirations are gone, because the Republican Party, they'll know
that if he can't carry Florida as a governor, he won't be able to carry it as a presidential
candidate.
So this is a very important election for him.
And not just does he have to win, he has to win convincingly.
As far as what's going to happen, no clue.
I can't make a prediction on this because a lot of it depends on variables far outside
the norm and not knowing how much negative voter turnout many Trump will
create and not knowing what real Trump is gonna do, without knowing that I
can't really make any kind of educated guess as to what's gonna happen. But the
stakes are pretty high for DeSantis so I would expect him to pull out all the
stops in trying to get reelected.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}