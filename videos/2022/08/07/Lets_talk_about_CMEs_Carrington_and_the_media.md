---
title: Let's talk about CMEs, Carrington, and the media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FUo4REl7YUc) |
| Published | 2022/08/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains solar storms, specifically CMEs and their potential impact on Earth.
- Describes the scale of disruptions caused by CMEs, ranging from GPS issues to total electricity shutdown.
- Mentions the current low-level CME headed towards Earth.
- Emphasizes that despite media hype, this particular event shouldn't be a significant concern.
- Notes that between now and 2025, such events may become more frequent, but mitigation plans are in place.
- Criticizes sensationalist journalism that capitalizes on fear to generate clicks and revenue.
- Stresses the effectiveness of early warning systems for solar storms.
- Encourages awareness of legitimate threats and avoiding unnecessary worry about non-global threats.
- Provides context on the Carrington event and suggests comparing it to the current situation for perspective.
- Concludes by reassuring viewers that while disruptions may occur, they are unlikely to be catastrophic.

### Quotes

- "The reality is you have to watch for those types of articles, those articles that are making bold claims, predicting things that are scary."
- "With everything that we have to actually worry about, we shouldn't spend time worrying about the things that aren't actually global threats."

### Oneliner

Beau explains solar storms, warns against media sensationalism, and encourages focusing on real global threats rather than hyped non-issues.

### Audience

Science enthusiasts

### On-the-ground actions from transcript

- Stay informed about solar storms and their potential impacts (implied)
- Avoid sharing or engaging with sensationalist articles for clicks (implied)

### Whats missing in summary

Comparison of current CME event to historical Carrington event for perspective.

### Tags

#SolarStorms #CMEs #MediaSensationalism #EarlyWarningSystems #GlobalThreats


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about solar storms, CMEs,
coronal mass ejections, and you, and the media.
So what is a CME?
It is a solar storm that leaves the sun.
Now, it can travel in any direction.
The ones that we care about are the ones
that are going to hit Earth.
If they hit, they cause disruptions,
depending on how strong they are.
It can be something as simple as the GPS on your phone
doesn't work so well, to barely even noticeable,
to pretty much shutting down all electricity.
Now, that is a five on the scale.
The one that is headed to us right now at time of filming
is a one, and as near as I can tell, it's a low one.
Odds are you didn't even notice this.
I'm recording this video on Monday,
but I plan to not release it until after it hit.
It might go out early if the articles and videos that
have been produced about this, talking
about the original Carrington event,
cause people a little too much concern.
This shouldn't be a big deal.
These events will become more frequent between now and 2025.
We're going to have more and more,
and some may actually cause some disruptions.
This one really shouldn't.
But it's worth being aware of them,
because large ones can cause problems.
One thing to note from this is that the early warning systems,
they work.
That's how we know it's coming.
And there are plans to mitigate these things.
Basically, we turn everything off.
There is an entire genre of journalism
that focuses on taking something that is scientific, that most
people don't have a lot of knowledge on,
because it's a rare event, and getting a whole lot of clicks
by scaring people with it, by painting that worst case
scenario.
Again, this is level one.
The people who wrote articles that
talked about all of the doom and gloom that contributed
to everybody doom scrolling, they knew that.
They knew it wasn't going to be a big deal.
And if I'm wrong, I mean, y'all are going
to be able to watch this anyway.
So I'm not going to be a bad guy.
So there is that.
The reality is you have to watch for those types of articles,
those articles that are making bold claims, predicting things
that are scary, because they know that they'll be shared.
They know that it will elicit a response.
It will drive traffic.
And therefore, it's going to generate ad revenue.
That's the goal.
With everything that we have to actually worry about,
we shouldn't spend time worrying about the things that
aren't actually global threats.
The early warning system works.
The plans to deal with this are pretty simple.
This isn't a high priority on my concern list,
even though I would imagine that sometime in the next three
years, there will be an event that actually causes
a decent disruption here on Earth.
But it's probably not going to be a doomsday scenario.
So if you want to know a little bit more about it,
I'll put an older video down below that
talks about the original Carrington event.
And you can compare what happened today,
assuming it went out when I wanted it to,
to what happened back then and see if the hype was worth it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}