---
title: Let's talk about Republicans claiming victory in Kansas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QOCOdAiHj-A) |
| Published | 2022/08/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans claiming to be champions of democracy after a failed authoritarian move.
- Kansas allowed family planning, so Republicans sent it back to states claiming victory.
- Michigan fighting a law from 1931 with trigger laws in place, denying people a voice.
- Republican Party wants to rule rather than represent the people.
- Accusation of launching a multi-decade campaign to take away rights.
- Consequences: economic and political costs for states following anti-rights decisions.
- Accusation of painting women needing family planning services as evil.
- Criticism of Republicans trying to reframe as champions of democracy after backlash.
- People had rights, Republicans tried to take them away, now facing political consequences.
- Republicans pushed people away by trying to take away their rights and villainizing them.

### Quotes

- "No, you don't get credit for being a champion of democracy because you're a failed authoritarian goon."
- "You made it really clear what the goal was. This is your failure."
- "You pushed them away. You tried to take away their rights. You villainized them."
- "People had rights. You tried to take them away. Now you're suffering for it politically."
- "Deal with it."

### Oneliner

Republicans claiming to champion democracy after failed authoritarian moves, facing political consequences for trying to take away people's rights.

### Audience

Voters, activists, community members

### On-the-ground actions from transcript

- Organize community forums to raise awareness about reproductive rights (suggested)
- Support organizations advocating for family planning services (implied)
- Get involved in local politics to ensure representation over rule (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's attempts to paint themselves as champions of democracy despite their actions against people's rights. Watching the full video gives a deeper insight into the consequences of authoritarian approaches to governance.

### Tags

#RepublicanParty #Authoritarianism #Rights #Democracy #PoliticalConsequences


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk a little bit about the new talking point from the Republican
Party.
Because see, look, we were right.
After our illustrious win up at the Supreme Court that we fought for decades for, see,
we're right.
The states are going to make their own decisions and it's all going to be wonderful.
No, no, no, that isn't going to fly.
That isn't going to fly.
The idea is that because Kansas pushed back, Kansas is still going to allow family planning,
that the Republicans were right to send it back to the states and that they're somehow
champions of democracy.
No, you don't get credit for being a champion of democracy because you're a failed authoritarian
goon.
That is absolutely not how that works.
Yeah, it worked in Kansas because they had a vote.
They had a referendum.
What about Michigan?
You know, where people are fighting against a law from 1931?
Peer pressure from dead people.
No, you took away people's rights.
What about all those trigger laws passed years ago on the books?
The people today, they don't get a voice in that.
The Republican Party wants to rule rather than represent.
And now they're claiming to be champions of democracy because they got defeated.
No that is not how that works.
This is your decision.
You're the dog who caught the car.
You own this.
Half the country knows that you went out of your way.
You launched a multi-decade campaign to take away their rights.
You don't get to pretend as though somehow now you weren't trying to do that.
You made it really clear what the goal was.
This is your failure.
This is what happens when authoritarians try to take rights from people.
Yeah, you've succeeded in a few states.
And for some time, they're going to be in a bad way.
But it's going to cost you economically in the state.
It's going to cost you politically with the votes.
You don't get to change your framing now.
All this time, you painted women who needed family planning, who needed these services
as evil.
And now you're surprised that they're turning against you.
They didn't turn against you.
You pushed them away.
You tried to take away their rights.
You villainized them.
You othered them.
Not the other way around.
You sold them out.
You don't get to claim now that you were doing it so you could be a champion of democracy.
This was already decided.
People had rights.
You tried to take them away.
Now you're suffering for it politically.
Deal with it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}