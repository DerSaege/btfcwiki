---
title: Let's talk about the Supreme Court altering football....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3ZtiYGQcbWI) |
| Published | 2022/08/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court's ruling will alter Southern football due to overturned long-standing rulings, impacting recruitment and careers.
- Concerns arise about players paying "gold diggers," dropping out to pay child support, and losing endorsement deals post the ruling.
- Conservative individuals express worries that Roe being overturned may disrupt Southern football, affecting talent recruitment at universities.
- Universities known for producing NFL players may struggle to attract talent, leading to a downturn in the quality of players they work with.
- Despite not focusing on the right issues, the situation mirrors states enacting family planning restrictions leading to economic decline.
- Many in the South equate football with life, showcasing the deep impact the ruling may have on communities.
- People experiencing buyer's remorse over the decision due to its impact on their beloved sport and team loyalty.
- The overlap between bumper sticker politics followers and sports enthusiasts is noted, hinting at a team mentality in approaching issues.
- While not the ideal way to address the issue, discussing the ruling's impact on football might resonate with fence-sitters for whom football holds significant importance.

### Quotes

- "Because for many of them, they are the sort that engages in bumper sticker politics. It's team mentality."
- "In the South, football is life."
- "This might be something to bring up."

### Oneliner

The Supreme Court ruling's impact on Southern football reveals deeper societal values and concerns, intertwining politics and sports loyalty.

### Audience

Sports enthusiasts, political followers

### On-the-ground actions from transcript

- Initiate community dialogues about the intersection of sports, politics, and societal values (suggested).
- Advocate for comprehensive sex education and reproductive rights to address broader issues impacting communities (implied).

### Whats missing in summary

Deeper insights into the societal implications of intertwining sports, politics, and personal values could be gained from watching the full transcript.

### Tags

#SupremeCourt #SouthernFootball #SocietalValues #Politics #CommunityPolicing


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about how the Supreme Court is altering Southern football.
Because of a long-standing ruling that has been overturned, things are going to change.
And I first heard this a few weeks ago, and it was brilliant.
When I heard it, I was like, yeah, that's true. That's probably going to happen.
I didn't make a video about it at the time because it's bad messaging, plain and simple.
It doesn't center the right things to me.
You know, it doesn't center women's rights. It doesn't center bodily autonomy.
It doesn't center family planning. It centers men.
But it's also one of those pebble in the shoe things.
I probably wouldn't have made a video about it ever had I not heard the argument in the wild.
I was sitting at the burger joint making my $8 installment on my heart attack,
and I was listening to the people next to me.
And they started talking about how Southern football is over.
How in just a few years it's going to be really hard to recruit.
Because of that ruling being overturned, well, players will have their careers derailed.
They will end up paying, quote, gold diggers.
Some will drop out and get jobs to pay child support.
They may lose endorsement deals because it's seen as a bad thing.
Now, I know the people who are talking about it, none of them are what I would call liberal in any way.
These are very conservative people.
Their concern was that Roe being overturned, well, it might mess up Southern football.
Because as it stands, there are a lot of programs at various universities that people,
if they want to get to the NFL, they try to go to these universities.
Because these coaches, the staff that's involved, they build players.
But if they can't attract the raw talent, because the talent can go elsewhere and not run the risk,
well, they won't have as much to work with.
This was the consensus of the hamburger joint sportscasters, I guess.
It's not wrong. I mean, it's a true statement.
This is probably going to happen.
There will be incidents that become cautionary tales and it will probably hinder recruitment.
It's not good messaging because it doesn't center the right things,
but it's not that far off from acknowledging that states who enact restrictions on family planning
will see an economic downturn.
It's the same principle.
It's people talking about what they care about, the pebble in their shoe, the thing that bothers them.
And there are a lot of people who might consider themselves pro-life,
but it's important to remember that in the South, football is life.
And there seemed to be some buyers regret over this decision.
Doesn't seem like something they really wanted once it started impacting the home team.
Because for many of them, they are the sort that engage in bumper sticker politics.
It's team mentality.
There's probably a huge overlap between people who view politics that way and people who follow sports very closely.
So again, not great messaging.
Not, to my way of thinking, the correct way to look at this issue.
But if you have somebody who's on the fence and football is really important to their life,
this might be something to bring up.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}