---
title: Let's talk about NASA and climate change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M7v5um8hQHk) |
| Published | 2022/08/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of NASA's projects in relation to climate change.
- Acknowledges the need for urgent action to mitigate climate change impacts.
- Lists various critical areas for immediate focus, such as water purification, pollution remediation, solar research, and food preservation.
- Points out that NASA's technology has been instrumental in various everyday innovations like LASIK eye surgery, baby formula, and cordless vacuums.
- Emphasizes NASA's role in developing technology to ensure human survival in challenging environments.
- Advocates for continued support for NASA alongside dedicated climate change projects.
- Suggests using NASA's track record of spinoff technologies as a tool to advocate for climate-focused projects.
- Stresses the potential for significant benefits and spinoffs from well-funded climate change initiatives.
- Encourages the idea of simultaneously supporting NASA and investing in climate change solutions.
- Concludes with a call to action to recognize the dual benefits of supporting both NASA and climate change projects.

### Quotes

- "NASA's job is to basically make sure that people can survive in places where they shouldn't be able to."
- "Most people, people care about the pebble in their shoe. And they're just starting to fill it."
- "It's not either or. With the money that we spend on stuff that we really don't need to, I think that we can do both."

### Oneliner

Beau explains the symbiotic relationship between NASA's innovations and climate change initiatives, advocating for support in both areas to address critical challenges effectively.

### Audience

Advocates and policymakers

### On-the-ground actions from transcript

- Support NASA initiatives and advocate for increased funding for climate change projects (implied).
- Use NASA's technological advancements as examples to garner support for climate change initiatives (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of how NASA's technological spinoffs can benefit climate change initiatives and why supporting both NASA and climate-focused projects is vital for addressing global challenges effectively.

### Tags

#NASA #ClimateChange #Innovation #Technology #Advocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk a little bit more
about NASA and the climate.
Because in a recent video, I talked about some
of NASA's upcoming projects.
And a whole lot of people were like,
why are we wasting money on this?
We should be focused on the climate.
And I get it.
Believe me.
I understand.
When you see the price tags on some of this stuff,
no doubt your first thought is probably
there might be some better ways to spend this money.
And I do not disagree with the idea
that we need a massive project immediately
to start working on mitigating the impacts of climate change.
And I mean, I could give you a list of stuff
that we need to start working on like now.
Just off the top of my head, imaging
to find water underground, water purification systems that
are really, really good, allowing
us to use incredibly dirty water because we're
going to need to.
We probably need to be dumping a bunch of money
into pollution remediation.
Would be good to put a lot of research into solar.
We probably also need to figure out
how we're going to implement air scrubbers.
Because as the temperature warms,
disease, the range of disease, will spread.
Probably need a bunch of money that
is geared towards figuring out how to keep food good
for longer periods, make sure there's
no bacteria and toxins in them, so on and so forth.
I agree.
Don't get me wrong, and that's just the tip of the iceberg.
But here's the reason I will never stop supporting NASA.
The best technology that we have in each one of those things
I named came from NASA.
NASA's job is to basically make sure
that people can survive in places
where they shouldn't be able to.
And that's a skill set we're going to need.
NASA has a lot of spinoffs, way more than you might think.
The ones I just named, that is all just stuff
that has to do with climate.
If you have ever had LASIK eye surgery, thank NASA.
You got a really good prosthetic, thank NASA.
Ever use one of those silver blankets?
Now you can probably guess why they call them space blankets.
The weird flare thing they use to get rid of landmines
came from NASA.
It's tons of stuff.
Baby formula.
I want to say 90% of baby formula in the US
uses a technique that kind of came from NASA.
And this is all really important stuff.
It's all spinoffs.
But it doesn't have to be anything that dramatic either.
Cordless vacuums.
If you are watching this on a mobile phone or a tablet,
odds are that the camera in it has a CMOS, C-M-O-S,
sensor in it.
NASA.
Now, does this mean that we don't
need a project specifically aimed at the climate that
is similar in scope, that is well-funded?
No.
This is just a talking point that you
can use to convince other people that we need it.
When you have a massive goal and you
have a bunch of challenges that you
know that you have to overcome, when
you put dedicated and informed people on task
and you give them the resources, they're
going to solve the problems.
And they're going to develop a whole lot of other stuff
along the way.
Most people, people care about the pebble in their shoe.
And they're just starting to fill it.
When you're talking about the price tax of some
of the projects that are needed to deal with climate,
it might be worth mentioning all of the benefits
that are byproducts.
They're spinoffs.
I'm sure somewhere there's a giant list of the stuff
that we use all the time that came from NASA.
There will be a similar list of stuff that made it
to the consumer world, made it to the medical world,
made it to the agricultural world from a project that
is designed to mitigate climate change.
It's a useful tool when you're trying to convince people.
But because of this, and it is a fact,
I'll never stop supporting NASA.
It's not either or.
With the money that we spend on stuff
that we really don't need to, I think that we can do both.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}