---
title: Let's talk about Trump's legal fees, Truth social, and dumpster fire....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q62KWzaXS-g) |
| Published | 2022/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's Truth Social app is not on Google Play Store due to lack of content moderation for extreme content.
- The former president contributes to the negative impression by reposting content from Q people.
- This news is detrimental for the already financially troubled app.
- The Republican Party is not covering legal fees for an incident at Trump's club, unlike past cases.
- Historically, the Republican Party has been paying legal fees for Trump, which has kept him in line.
- Without funding for this particular case, Trump may see it as a betrayal and potentially break away from the Republican Party.
- Trump could lead the MAGA faithful in forming a separate, more significant party if he decides to part ways with the Republicans.
- Today's events and public reporting may shape future political dynamics.
- Trump might interpret the lack of financial support in his current legal case as a significant betrayal.
- This situation could lead Trump to act independently of the Republican Party.

### Quotes

- "The Republican Party is not covering legal fees for that little caper down there at his club."
- "This money that Republican Party has been using to pay these legal bills. It's also money they've been using to keep a leash on Trump."
- "He's kind of off the leash of the Republican Party."
- "Trump might see that as a huge betrayal."
- "Today's events, at least the events that became public today, they're probably going to shape things in the future."

### Oneliner

Beau reveals how the lack of financial support from the Republican Party in Trump's legal case could lead to significant political shifts in the future.

### Audience

Political observers, Republican Party members

### On-the-ground actions from transcript

- Speculate on the potential consequences of Trump breaking away from the Republican Party (implied)
- Stay informed about the evolving political landscape and potential party dynamics (implied)
- Monitor Trump's actions and statements for signs of independent political moves (implied)

### Whats missing in summary

Insights into the potential ramifications of Trump distancing himself from the Republican Party, and the implications of this shift on future political scenarios.

### Tags

#Trump #RepublicanParty #TruthSocial #LegalFees #PoliticalShifts


## Transcript
Well, howdy there, and I know people, let's bow again.
So today, we are going to talk about Trump's
just dumpster fire of the day.
A lot of this is information that he already knew,
but it is now being covered.
It's out in the public sphere.
And when it comes to Trump,
that's generally what he cares about.
At least that's what we see the reactions to.
Where to start?
Let's start with Truth Social.
Truth Social is not on the Google Play Store
because Google says there is a lack of content moderation
when it comes to extreme content.
The former president does not do a lot
to curtail this impression when he wakes up and starts
reposting a bunch of Q people.
This eliminates half of the smartphone market
when it comes to the app that is already in financial trouble.
This news breaking is really bad for the company.
This may be the thing that really does send it just down.
Because it is unlikely that the company is going to
introduce content moderation policies that are going to
matter, that will meet Google's guidelines, because
they would also apply to Trump.
And Trump doesn't stay within him.
That's why he's not on other social media platforms.
I feel like there were people who said
that this would happen.
OK.
Now, the big news though, that's not even the big worry.
The big news is that the Republican Party,
they're not paying the legal fees
for that little caper down there at his club.
The Republican Party's been footing the bill
for pretty much all of Trump's legal issues.
They've been paying the lawyers for those people
who have been wondering why are these lawyers getting paid
historically there have been glitches in people who work for Trump getting paid.
It's because the Republican Party is paying for them. That's not the case with
the incident down there at his club. That is what is being reported. The
reporting suggests that the Republican Party has basically said you're on your
own when it comes to the investigation dealing with classified documents. Now
there's a lot that can be read into that and you're free to make whatever
speculation you would like. There is definitely some that I will make in
private. Aside from that, it's important to remember one thing. This money that
Republican Party has been using to pay these legal bills. It's also money they've been using
to keep a leash on Trump. It's reported that when he was leaving D.C., he was on the phone
with the party, and basically the promise of paying these legal bills, this is what
kept him in line. It's what stopped him from starting a third party. It's what is
currently stopping him from announcing before the midterms. There may be the
possibility that without this funding for this particular case that Trump sees
it as a betrayal and just goes all in. We've talked about the the possibility
of the MAGA faithful breaking away from the Republican Party. Trump himself
leading that charge, he could do it. He could do it and it would be far more
effective and a much larger larger party if he did it. Especially if he timed that
with an announcement as far as him running for president. So today's
events, at least the events that became public today, the reporting that became
public today, they're probably going to shape things in the future. And we need
to remember as we watch Trump's actions from this point that in some ways he's
He's kind of off the leash of the Republican Party.
Sure, they still are going to be paying the legal bills for all of the other cases.
But to Trump, I'm sure the one in which his home was searched is probably the most important.
And Trump might see that as a huge betrayal.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}