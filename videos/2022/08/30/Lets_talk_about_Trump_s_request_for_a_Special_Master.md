---
title: Let's talk about Trump's request for a Special Master....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CyZgwX9qtB4) |
| Published | 2022/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump requested a special master outside of the Department of Justice to go through documents taken from his club before investigators access them.
- DOJ filter teams already identified items they thought might be privileged, but the investigation doesn't rely on privileged documents.
- Trump’s request may be a delay tactic or to benefit high-profile friends facing similar situations.
- The judge wants a detailed inventory of items taken from Trump's residence, to be submitted under seal.
- U.S. Intelligence and Counterintelligence are also reviewing the documents due to their classified nature.
- The request for a special master might not significantly impact the investigation.
- There's a hearing scheduled on Thursday regarding this matter.
- Transparency may become an issue once the inventory is provided.
- This move could potentially delay the investigation until right before the midterms, though delaying beyond that is unlikely.
- Politically, this delay tactic might not be advantageous for Trump.

### Quotes

- "This isn't going to have a real impact on the case."
- "It's a delay tactic."
- "Might not have been a good move politically."
- "We're about to find out how committed Trump is to these talking points about transparency."
- "The odds of it delaying it beyond the midterms are pretty slim."

### Oneliner

Trump's request for a special master to handle documents may serve as a delay tactic, with questionable impact and transparency concerns, possibly affecting the investigation politically.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Attend the hearing scheduled on Thursday to stay informed (implied).
- Monitor updates on the judge's request for a detailed inventory (implied).
- Stay engaged with news on the investigation and potential delays (implied).

### What's missing in summary

Insights on the potential implications of Trump's request beyond its immediate procedural effects. 

### Tags

#Trump #DOJ #SpecialMaster #DelayTactic #Transparency


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Trump's request.
Trump's request to have somebody outside of DOJ go through the documents, the items that
were taken from his little club before the investigators get their hands on them.
The appointment of a special master.
Now DOJ uses filter teams, and the filter teams that are already in place did identify
some items that they thought might be privileged.
This isn't an incredibly unusual request when you're talking about high-profile people.
At the same time, the DOJ filter teams are already done.
The other interesting little tidbit about this is that it doesn't matter.
The investigation that is currently taking place, it doesn't hinge on any document that
might even remotely be privileged.
That's not something that's going to matter in the grand scheme of the investigation,
even if the special master goes in and removes anything that wasn't specifically laid out,
it doesn't alter anything.
This isn't going to have a real impact on the case.
So there are two reasons Trump could be looking to get this.
One, because he has high-profile friends who have them in their cases.
Or two, it's a delay tactic.
And that seems most likely.
But we don't know.
There will be a hearing about this, I want to say, on Thursday.
It's worth noting that in addition to the filter team already being done, that U.S.
Intelligence, Counterintelligence are also reviewing the documents because of the classified
nature.
Now, at the same time, the judge is looking to have the Department of Justice submit a
more detailed inventory of what was taken from Trump's residence.
And it will be delivered under seal.
We're about to find out how committed Trump is to these talking points about transparency.
I have a feeling that once that inventory is provided, that they may not be so interested
in being transparent about what was taken.
But we'll have to wait and see.
The other thing at play here is that this might be a moment for somebody close to Trump
to remind him to be careful what he wishes for.
It's a delay tactic.
The odds of it delaying it beyond the midterms are pretty slim.
The odds of it delaying it until right before, that could totally happen.
This might not have been a good move politically.
Legally, I don't really think it's going to matter at all.
Politically, I don't think it's going to give him the delay that he's looking for, which
is beyond the midterms.
Assuming that's what he's looking for.
It would make sense for him to try to delay these proceedings as much as humanly possible
to get past the midterms.
I don't think that it's going to go that long.
So we'll find out on Thursday what's going to happen.
My guess is a special master will be appointed.
But we're going to have to wait and see.
And I don't believe that Trump is going to get what he is looking for out of this motion.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}