---
title: Let's talk about former Secret Service agent Tony Ornato....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=O3cQuDb0MX4) |
| Published | 2022/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former Secret Service agent Tony Ornata's retirement has been announced.
- Ornata served as Trump's deputy chief of staff for operations before returning to the Secret Service.
- Ornata was at the center of a lot of testimony and questions after Hutchinson's testimony before the January 6th committee.
- Speculation suggests Ornata may need to return to answer more questions.
- James Murray, the Secret Service director, announced his retirement last month.
- President Biden announced Kimberly Cheadle as Murray's replacement.
- Ornata, having been with the Secret Service for 25 years, is leaving for the private sector.
- Ornata's responsibilities included training new agents, raising concerns given the circumstances.
- Speculation surrounds where Ornata will work next, with rumors suggesting not with Trump's companies.
- There may be challenges in getting Ornata to return before the committee as a private citizen rather than a federal employee.

### Quotes

- "A lot is probably going to be made of his departure."
- "You will see this material again in the future as things play out."
- "This timeline, this information, just kind of file it away."

### Oneliner

Former Secret Service agent Tony Ornata's retirement sparks speculation and questions about his past duties and future whereabouts, potentially impacting his return before the committee.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Follow updates on Tony Ornata's future endeavors to stay informed (suggested).
- Stay engaged with news regarding the Secret Service for further developments (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the personnel changes within the United States Secret Service, offering insights into the potential implications of Tony Ornata's retirement and future actions.


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about some personnel changes
at the United States Secret Service
and former Secret Service agent Tony Ornata.
If you missed the news, his retirement has been announced.
This is the Secret Service agent who
went on to serve as Trump's deputy chief of staff for operations and then wound up going
back to the Secret Service.
This is the person who found themselves at the center of a lot of testimony and questions
after Hutchinson's testimony before the January 6th committee.
This is the whole, you know, grab them by the neck thing, all of that.
It's worth noting Ornata has reportedly testified before the committee already.
However, there's a lot of speculation suggesting that they may want him to come back and answer
some other questions.
Now, a lot is probably going to be made of his departure, but it is worth remembering
that in addition to this particular person finding themselves at the center of a lot
of testimony, a lot of questions. There are other things going on at the Secret Service
right now. James Murray, the director, announced his retirement, I want to say last month.
Last week, President Biden announced that Kimberly Cheadle would be taking over, and
now you have or not are leaving. He's been there 25 years, so him leaving to go to the
private sector, in and of itself, that's not unheard of. It doesn't necessarily
mean anything. However, the speculation is definitely going to be there and
there are going to be a lot of people who kind of are going to breathe a sigh of relief
over this because part of the responsibilities that he had after he
He came back to the Secret Service after leaving Trump.
It dealt with training new agents, and that gave a lot of people pause, given the situation.
There's also going to be follow-up, I'm sure, about where he goes to work.
The rumor mill says that he will not be going to work for one of Trump's companies.
remains to be seen but that's what's being said. There's also going to be a
lot of talk about whether or not it's going to be harder to get him to come
back before the committee if they do in fact want him to come back because well
now he's a private citizen rather than a federal employee and that might change
things slightly. So this is something that you're gonna this is
something you're going to see again. You know, this is, you will see this material
again in the future as things play out when the committee returns, almost
certainly. So this timeline, this information, just kind of file it away.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}