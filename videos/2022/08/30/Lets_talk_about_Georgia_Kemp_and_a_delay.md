---
title: Let's talk about Georgia, Kemp, and a delay....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E418mc3TUsM) |
| Published | 2022/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special grand jury in Georgia investigating election interference wanted to hear from Governor Kemp.
- Governor Kemp tried to avoid the subpoena, but the judge insisted he testify.
- Judge concerned about political influence leading up to midterms if Kemp testified.
- Ruling requires Governor Kemp to speak to the grand jury after midterms, delaying resolution.
- Possibility of no resolution until after the new year if grand jury needs Kemp's testimony.
- Delay could impact political decisions, especially Trump's announcement.
- Judge's attempt to reduce political nature of investigation may have escalated it to national level.

### Quotes

- "The judge says that the governor has to talk to the special grand jury promptly after midterms."
- "In the judge's attempt to reduce the political nature of the investigation within Georgia, kind of just got punted up to the national stage."
- "So anyway, it's just a thought, y'all have a good day."

### Oneliner

The special grand jury in Georgia's investigation into election interference delays due to Governor Kemp's testimony, escalating political stakes to the national level.

### Audience

Georgia residents, political analysts

### On-the-ground actions from transcript

- Contact local representatives for updates on the grand jury investigation (implied)
- Stay informed about the political developments in Georgia and beyond (implied)

### Whats missing in summary

Insights into the potential implications of the grand jury investigation and the importance of staying informed on political matters.

### Tags

#Georgia #GovernorKemp #ElectionInterference #GrandJury #PoliticalStakes


## Transcript
Well, howdy there, internet people. It's Bo again. So today we are going to talk about Governor Kemp, the Governor of
Georgia. We're going to talk about a delay that is probably going to be pretty lengthy, and subpoenas, and the special
grand jury and everything that just happened.
So, if you don't know what was going on, the special grand jury in Georgia that was
looking into the allegations of election interference by Trumpworld was hoping to hear directly
from the governor.
The governor tried to get out of the subpoena.
The judge was like, no, you definitely need to go talk to him.
The judge was worried about the political influence that could happen if Kemp
testified now or anytime leading up to the midterms. And as much as I dislike
the the ruling, it makes sense because the reality is every candidate could
use this, his appearance politically in some way, either to cast themselves as somebody
who would never betray Trump or to show that, you know, Kemp was somehow subpoenaed and
therefore involved, even though that's not really what's going on.
You know, there's a lot to it.
Democratic candidates could use it to suggest, you know, corruption on the part of the Republican
party. There's a whole lot of different ways it could be spun. And yeah, that's true. So the judge
says that the governor has to talk to the special grand jury promptly after midterms. The problem
with this is that after the midterms comes the holidays. It seems unlikely that there will be
some formal resolution from the special grand jury in Georgia before the new year now because
of this.
Now that's assuming that the grand jury absolutely needs Kemp's testimony.
They may not, but if they were really hinging anything on that, you're probably not going
to get any kind of resolution whatsoever, any kind of decision, until after the new
year.
At which point, Trump will probably have made his decision about announcing, and that just
increases the political stakes of the entire investigation.
So in the judge's attempt to reduce the political nature of the investigation within Georgia,
kind of just got punted up to the national stage, is really what's going on.
So anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}