---
title: Let's talk about the Senate bill to codify Roe....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WJ4yE8qPMd8) |
| Published | 2022/08/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a semi-bipartisan bill in the Senate to codify Roe, with low chances of passing.
- Suggests that the bill may be more about getting senators on record rather than passing.
- Points out the potential purpose behind Democrats pushing this bill before midterms.
- Mentions the bill's wider purpose of reminding constituents about the issue and driving voter turnout.
- Notes that votes on this bill might serve as signals rather than impacting the outcome.
- Acknowledges the slim chance of Republican senators supporting the bill due to generated heat.

### Quotes

- "It's not just putting people on record, because when the bill is destined to be defeated anyway, people can vote however they want."
- "There is a wider purpose for this. It's not just putting people on record."
- "This really seems to be more of a tool to drive voter turnout to get more seats for people who vote to codify it."

### Oneliner

Beau analyzes a bill in the Senate to codify Roe, focusing on its symbolic nature and impact on voter turnout rather than its potential to pass.

### Audience

Voters, political observers

### On-the-ground actions from transcript

- Contact your senators to express support or opposition to the bill (implied).

### Whats missing in summary

Context on the potential implications of the bill's failure and how constituents can further advocate for reproductive rights.

### Tags

#Senate #Roe #Bill #VoterTurnout #Politics


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
politics, a bill headed into the Senate, performance art, getting out the vote,
and how sometimes a performative gesture actually serves a purpose, even though that purpose may not
always be readily apparent. If you don't know, there is a semi-bipartisan bill headed into the
Senate to codify Roe. It is incredibly unlikely that it pass. I don't want to say a zero percent
chance, but it's low. I don't believe this was actually intended to pass. I think the goal
behind this is to a. allow Republicans who are in support of codifying Roe to get on the record
saying that, and for Democrats to send a message to the states and everybody else that a majority
of senators are actually going to support it, even though it's probably not enough to get 60 votes.
Now, yeah, getting them on record, that's easy to say, especially when it's unlikely that the
bill is going to go anywhere. They could just put that on Twitter. There is another purpose
for Democrats. It reminds people that this is an issue. Right before the midterms,
there will be a vote that will remind everybody that this was taken away,
and it was taken away by Supreme Court justices that were confirmed by the Republican Party.
I imagine that a large part of this is actually an effort to remind people of this issue and to
drive voter turnout in hopes that they could get 60 votes later. I just think that there should be
a lot of managing of expectations with this because it's really unlikely that it makes it
through the Senate. But there is a wider purpose for this. It's not just putting people on record,
because when the bill is destined to be defeated anyway, people can vote however they want.
Once the outcome of the bill is determined, the votes don't matter anymore. It's more about
signaling to their constituents. Now, I mean, there is a slim possibility that enough heat
has been generated aimed at Republican senators that they might go for it, but I wouldn't count
on that. This really seems to be more of a tool to drive voter turnout to get more seats for people
who would vote to codify it. But we're gonna have to wait and see. Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}