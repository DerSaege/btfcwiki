---
title: Let's talk about Trump's lawyers preparing for charges....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A2tNDGxEKfo) |
| Published | 2022/08/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's legal team is reportedly preparing for the possibility of a criminal charge, which has caught people's attention.
- The team should have been worried and preparing months ago, so them doing it now shows they are behind.
- One potential strategy being considered is throwing people like Meadows or Eastman under the bus to protect Trump.
- Broadcasting this strategy may not be wise as Meadows and Eastman could cooperate with prosecutors to protect themselves.
- Hutchinson's testimony seems to have put Trump's legal team on edge, especially if corroborated by Meadows.
- Overall, there are no significant new developments in the reporting, just showing that Trump's legal team may be running behind.
- The focus should be on the possibility of throwing others under the bus, as it could heavily influence their cooperation decisions.

### Quotes

- "Trump's legal team should have been worried months ago."
- "The part about them throwing people under the bus, that's the important part."
- "There's no real new developments there."
- "It might heavily influence their decision-making process."
- "Y'all have a good day."

### Oneliner

Trump's legal team's preparation for possible criminal charges reveals a potential strategy of throwing others under the bus, which could heavily impact their cooperation decisions.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed on legal developments and strategies (implied)
- Cooperate with legal authorities if approached for information (implied)

### Whats missing in summary

The full transcript offers insights into the legal team's strategies and potential implications for cooperation among involved individuals.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about reporting that is coming out suggesting that Trump's
legal team is preparing for the possibility of a criminal charge.
That's how this news is being framed and people are focusing in on that aspect of it.
As soon as the story broke, that's what people are kind of zoning in on.
Yeah, I mean it's newsworthy that his legal team is preparing for this possibility.
However, that doesn't really mean anything.
The framing is that Trump's legal team is worried.
Trump's legal team should have been worried months ago and they should have been preparing
for that possibility way back then.
The fact that they're doing it now just means that they're running a little behind.
However, in that same reporting there is the suggestion that one of the possible strategies
that the legal team is contemplating or maybe contemplating is throwing people under the
bus, protecting Trump by kind of pinning everything on Meadows or Eastman.
I mean, yeah, that seems obvious.
The thing is, that's probably not something you want to broadcast.
As it stands, both Meadows and Eastman are in a position to influence the case the prosecution
puts forward.
If prosecutors were to go to Meadows or Eastman and say, hey, I mean you saw this reporting,
they're thinking about closing ranks and leaving you outside, do you have anything you would
like to tell us?
It might increase the likelihood that Eastman or Meadows cooperates.
The reporting is suggesting that it was Hutchinson's testimony that kind of really put Trump's
legal team on edge.
That would be even more the case.
It would be even more pronounced if that testimony was corroborated by Meadows, one of the people
that is said to be being considered for the esteemed position of Fall Guy.
Most of that reporting, not really significant.
There's no real new developments there.
There's nothing that we didn't know.
Or if anything, it shows that Trump's legal team is running a little behind.
Them preparing for that possibility doesn't necessarily mean they think it's inevitable
a good legal team would be prepared.
The part about them throwing people under the bus, that's the important part.
Because the people who might be standing a little close to the curve, they're going to
be paying attention to that.
And that might heavily influence their decision-making process when it comes to whether or not they're
going to cooperate.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}