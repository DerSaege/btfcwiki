---
title: Let's talk about who will pay for climate research....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PBzPtjQNvuo) |
| Published | 2022/08/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the topic of for-profit research in relation to mitigating climate change.
- Contrasts NASA's research approach with SpaceX's for-profit endeavors.
- Points out the lack of motivation for the market to solve issues like climate change.
- Emphasizes the need for clean, limitless energy and water to address climate change effectively.
- Questions the profitability of producing clean water versus bottling and selling it in limited supply.
- Argues that for-profit companies lack the motivation to conduct research for solutions that may not be cost-effective.
- Describes corporations as solely profit-driven entities that may impede research if it affects their bottom line.
- Suggests that real solutions for climate change require either individual efforts or public funding.
- Mentions the scarcity of research for diseases that affect a small population due to cost-effectiveness.
- Advocates for research driven by knowledge-seeking goals rather than profit-driven motives.

### Quotes

- "There's no motivation for it to happen that way."
- "It's one of the major flaws in the system that we currently have."
- "If you are waiting for the corporations to save us, you're going to be waiting a long time."

### Oneliner

Addressing the flaws in for-profit research, Beau stresses the necessity of clean, limitless energy to combat climate change, urging for public or individual initiatives over corporate solutions.

### Audience

Climate activists, Scientists

### On-the-ground actions from transcript

- Advocate for public funding for research into clean, limitless energy (implied).
- Support individual initiatives focused on addressing climate change (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the limitations of for-profit research in tackling climate change and underscores the importance of alternative funding sources for transformative solutions.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about for-profit research.
Because over the last few days, we've
talked a lot about NASA and how a lot of the technologies
that NASA has developed along the way
have applications when it comes to mitigating climate change.
A common response from those who think that spending money
on NASA is a waste or maybe not as cost-effective,
even if they don't think it's a waste,
is that SpaceX does what NASA does cheaper.
And if you're looking at one particular thing,
sure, that's true.
But that's not research.
That's a for-profit endeavor.
More importantly, when it comes specifically
to the concept of mitigating climate change,
the market isn't really going to solve the problem,
because there's no incentive to.
When you're actually talking about solving the problem
and solving it to a degree where we're actually getting ahead,
what do you really need?
Clean, limitless energy and clean, limitless water.
Name anything that the market produces that's limitless.
From a capitalist standpoint, what
is better to produce clean water, usable water, incredibly
cheaply, or to bottle it and sell it
in a limited supply, which is going to make more money?
What is better?
Sell it in a limited supply, which is going to make more money.
What is the market going to be driven to do?
When it comes to energy, what's the motivation there?
Do they really want it inexpensive and clean?
Does that help the company's bottom line?
Not really, right?
That type of research isn't going
to be conducted by for-profit companies.
Now, if that technology exists elsewhere,
yes, they will try to capitalize on the research
that other people did, but they're not
going to fund it themselves.
Corporations are cold, unfeeling, unthinking things.
They are driven to do one thing, make money.
They don't care about people.
They don't care about the environment.
If that research isn't cost effective,
they're not going to pay for it.
If that research is going to cut in to their profits,
they may even work to stop it.
The real solutions when it comes to the climate,
that's not going to be done through for-profit research.
There's no motivation for it to happen that way.
It's going to have to be done either by individuals who
are using their own resources to do it because they care,
or it's going to be publicly funded.
And you can look at pretty much anything and see this play out.
There are diseases that exist that there's not
a lot of research in figuring out how to cure or how to treat
because not enough people contract those diseases.
Not enough people have that illness.
Therefore, it's not cost effective
to put research into figuring out how to treat or cure it.
Energy companies are not going to pay to research
to put themselves out of business.
When you are talking about research and financing,
the best research always comes from research
that's open-ended, research that is looking
to learn for knowledge sake.
It should have a goal.
It should be driven towards a specific task.
The task of NASA isn't moving cargo.
That's why it's not exactly comparable to SpaceX.
The task of an agency that would be dedicated
to mitigating climate change, that
goal would be kind of like NASA, be very open-ended.
It would lead to research in a whole bunch of different areas.
It would lead to breakthroughs in technologies
in a whole bunch of different areas.
And eventually, it would get to the point
where that technology is being spun off the same way NASA does.
The main goals are to get people to understand
that clean, limitless energy, the big pipe dream,
the actual solution, if that occurred,
it would fundamentally alter our world, everything,
in the way that we do things.
But you can't expect a company to foot
the bill for the research that is going
to put them out of business.
It's just not how it works.
It's one of the major flaws in the system
that we currently have.
If you are waiting for the corporations to save us,
no matter how well-intended or how good the PR of that
corporation is, you're going to be waiting a long time.
It's not going to happen.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}