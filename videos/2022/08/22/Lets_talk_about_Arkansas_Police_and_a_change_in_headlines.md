---
title: Let's talk about Arkansas, Police, and a change in headlines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YhT8hCrV7zY) |
| Published | 2022/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a recent incident in Arkansas involving three officers, two deputies, and one city cop allegedly beating someone.
- Mentioning the lack of lead-up in available footage to analyze the situation thoroughly.
- Anticipating body camera dash cam footage to provide more context for a proper breakdown.
- Expressing doubt about justifying the extreme actions seen in the available footage.
- Noting that the Arkansas State Police have initiated an investigation, focusing on the use of force.
- Expressing concern that narrowing the scope of the investigation may hinder reaching the truth.
- Commenting on the headlines surrounding the incident, which are more descriptive and less sanitized than usual.
- Appreciating that some outlets are reflecting the available footage rather than immediately giving law enforcement the benefit of the doubt.
- Seeing potential for these descriptive headlines to advance the discourse on the use of force.
- Concluding that there isn't enough information available to provide a detailed analysis and calling for patience while awaiting more details.

### Quotes

- "There's not enough for me to do that with."
- "I think that might actually really help further the conversation about use of force in this country."

### Oneliner

Beau addresses a recent incident in Arkansas involving officers allegedly beating someone, expressing doubt about justifying the extreme actions seen and calling for patience and further investigation to understand the truth and advance the discourse on use of force.

### Audience

Observers, Activists, Police Reform Advocates

### On-the-ground actions from transcript

- Contact Arkansas State Police for updates on the investigation (suggested)
- Stay informed about developments in the case through reputable news sources (suggested)

### Whats missing in summary

Insights on the potential impact of public scrutiny on police behavior and accountability.

### Tags

#Arkansas #PoliceBrutality #MediaImpact #UseOfForce #CommunityPolicing


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about something
that occurred in Arkansas and how it impacts the media.
There was a situation in Arkansas,
and my inbox has just overfilled with people saying,
hey, can we get a breakdown, an analysis?
Tell us what went wrong.
The situation involved three officers,
looked like two deputies and one city cop,
and they were, for lack of a better term, beating someone.
The footage that's available, I can't do what I normally do.
There's no lead up.
There's not enough there for me to kind of break it down
and explain where it went wrong.
So I have to wait for the body camera dash cam footage.
That being said, I don't anticipate making a video
in the future in which I say that this was justified.
This is, it's pretty extreme.
There aren't a lot of situations in which what's visible
on the footage that's available is okay.
Now the Arkansas State Police has launched an investigation,
but there's already one little phrase that they threw out
that has kind of given me pause.
It says that they're only going to be looking
at the use of force, and that seems like it might be
something that narrows the scope of the investigation.
To a point where it may not actually reach the truth,
but we don't know.
We have to wait and see.
One thing that I did find incredibly interesting about this
is some of the headlines.
When something like this happens,
what do the headlines normally say?
Community outraged over violent arrest.
Officers suspended over use of force.
Something like that.
Very sanitized language.
Let me read you some headlines.
This is from KARK.
Three Arkansas officers suspended and under investigation
after video shows alleged beating.
Arkansas Times.
Crawford County officers caught on video pummeling shoeless man.
Deputies suspended.
Arkansas officers were suspended after video on social media
shows a police beating.
That's NPR.
It does appear that at least some outlets have adopted the stance of
it is what it appears.
Rather than immediately giving law enforcement the benefit of the doubt,
the headlines are starting to reflect the footage that is available.
I don't see how that can be a bad thing.
I think that might actually really help further the conversation about
use of force in this country.
But as far as this particular incident and me going through it and saying,
well, this happened, then this happened, then this happened.
There's not enough for me to do that with.
So we're just going to have to wait.
Anyway, it's just a thought. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}