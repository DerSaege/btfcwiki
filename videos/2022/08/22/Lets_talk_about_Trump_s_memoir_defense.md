---
title: Let's talk about Trump's memoir defense....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AaRcIdS4eQ0) |
| Published | 2022/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ohio Republican Mike Turner proposed a defense for the former president's actions involving classified documents, suggesting that Trump took them to write a memoir due to memory challenges.
- Critics mocked this defense, particularly targeting the former president's writing skills, but Beau sees a more concerning angle.
- Beau points out that if removing and retaining the classified documents are crimes, planning to distribute or publish them could be an even larger offense.
- Even if Trump intended to use a ghostwriter for his memoir, the access to the classified documents remains a significant security risk.
- The defense of using the documents for a book project could be seen as an intent to distribute sensitive information, making the situation worse.
- The Republican Party struggles to find a solid defense because their actions regarding classified information have revealed incompetence and lack of understanding of security protocols.
- Beau questions the logic behind downplaying the seriousness of possessing stolen classified documents, stating that intending to distribute them is not a valid defense but rather something for a plea agreement.

### Quotes

- "Prosecutors love it when they have evidence of one crime and the defense itself makes it a larger crime."
- "The reason the Republican Party is having such a hard time coming up with a defense is because there really isn't one."
- "There are many members of the Republican Party that should not be anywhere near a classified document because they have no idea how to handle them."
- "That's not a defense. That's what goes in a plea agreement."
- "It's worse. This defense is worse than the evidence they already have."

### Oneliner

Ohio Republican Mike Turner's defense of Trump using classified documents for a memoir unveils deeper security risks, revealing the Republican Party's incompetence in handling sensitive information.

### Audience

Political observers, concerned citizens

### On-the-ground actions from transcript

- Ensure proper handling of classified information (exemplified)
- Advocate for stringent security protocols within political parties (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into the potential legal implications and security concerns surrounding the mishandling of classified information by political figures.


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about the
Republican Party's ongoing quest, their search for a defense for the former president's actions
concerning those documents. Ohio Republican Mike Turner floated a new defense on Face the Nation,
and it's the idea that Trump took those documents, he had those documents, so he could write a
memoir because it's hard to remember everything that happened during your administration.
This idea was made fun of a lot in a lot of various ways, mostly insulting the former
president's writing ability. And while sure, you could look at it that way, I have another way
I would like to look at it. So the idea is that he took these documents, these documents, some of
which were labeled TSSCI, and he planned to use them to write a book. Okay, that's worse. I mean,
you see how that's worse, right? Let me tell you what prosecutors love. Prosecutors love it when
they have evidence of one crime and the defense itself makes it a larger crime. If removal of
these documents is a crime and willful retention of these documents is a crime, what do you think
distribution or publication is? And I know, the whole writing thing, that's fine. His writing
ability being questioned, that's fine. But he could use a ghostwriter. So the ghostwriter and
the editor has direct access to the documents. That's not good. That's worse. Even if he just
planned on writing it himself, that would also not be good. You can't get around the classification
by putting it on another page. This is, it's worse. This is worse. This defense is worse than
the evidence they already have. It's expressing intent to distribute the documents to a wider
range of people or distribute the information on them to a wider group of people. The reason the
Republican Party is having such a hard time coming up with this, coming up with a defense, is because
they're really kind of not one. If there is one thing we have learned as a nation from this
incident, it's that there are many members of the Republican Party that should not be anywhere near
a classified document because they have no idea how to handle them. They have no idea the security
precautions they should take. Think about the process there. The idea that, oh, don't worry
about those stolen documents, those documents he's not supposed to have. Don't worry about those. He
wasn't going to do anything nefarious with them. He was just going to distribute them. That's not
a defense. That's what goes in a plea agreement. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}