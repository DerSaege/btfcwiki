---
title: Let's talk about Trump, executive privilege, and the search...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ERs8Z023nQM) |
| Published | 2022/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump claims executive privilege over documents seized by the FBI during the raid on Mar-a-Lago.
- Trump wants documents, including privileged attorney-client material, returned to their location.
- Executive privilege is a legal concept that grants privacy to the executive branch in decision-making.
- The FBI raid was looking for government and presidential records.
- Trump's claim that the seized documents are covered by executive privilege may resurface.
- Even if the documents are covered by executive privilege, they should be at the National Archives, not with Trump.
- Trump is likely to continue pushing his talking points despite legal realities.

### Quotes

- "He's not going to let reality get in the way of his talking points."
- "Trump's not going to let reality get in the way of his talking points."
- "This claim is going to resurface, as well as the attorney-client privilege thing."

### Oneliner

Trump claims executive privilege over seized documents, but legal realities may not be in his favor.

### Audience

Legal analysts, political observers

### On-the-ground actions from transcript

- Stay informed on the legal developments surrounding the seized documents (implied)
- Follow reputable sources for updates on Trump's claims and legal challenges (implied)

### Whats missing in summary

The full transcript provides additional context and nuances to understand the ongoing situation with Trump's executive privilege claims and seized documents.

### Tags

#Trump #ExecutivePrivilege #LegalIssues #FBI #NationalArchives


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Trump,
executive privilege, and the search.
When all the news broke, I immediately focused on,
you know, the whole classified documents thing.
That seemed to be the most important item to me.
However, at the same time, I don't know, about a week ago,
the word out of Trump world, from Trump and his team,
was that some of the documents that were taken by the feds
were covered by executive privilege.
One example of his community saying this
comes from him himself, when he says,
oh great, it has just been learned that the FBI,
in its now famous raid on Mar-a-Lago,
took boxes of privileged attorney client material,
and also executive privileged material,
which they knowingly should not have taken.
By copy of this truth, I respectfully request
these documents be immediately returned to the location
from which they were taken.
Thank you.
So the first thing that we can gather from this
is that apparently the former president believes
that a wannabe tweet is an adequate substitute
for a filing with DOJ.
I'm not a lawyer, I don't even play one on TV,
but I'm pretty sure that's not the case.
The other thing that we can take away from this
is that he is saying that there is executive privileged
material in what they took, and he wants it back.
Now, executive privilege is a privilege granted
to the executive to allow them some privacy
when it comes to decision-making and stuff like that.
The idea that there are documents,
or perhaps records from his presidency,
that might in some way qualify
for some kind of privilege like this,
that were taken, that's not out of the realm of possibility
to be honest, it's really not.
I mean, these records, for lack of a better term,
we'll call them presidential records, okay?
They were likely to be there.
I mean, in fact, in the warrant, the FBI,
they said they were looking for any government
and or presidential record.
So, I mean, yeah, I'm pretty sure
they thought they got some.
I don't know that they didn't mean to take it,
as he seems to imply here.
I mean, it's literally listed on the warrant
as what they were looking for.
I cannot think of any situation
in which something would be covered by executive privilege
that is not also a presidential record.
I am not sure that it's a sound legal strategy
to walk out and scream at the top of your lungs,
hey, that thing in your warrant,
you found it and I'd like it back.
Again, not a lawyer, but just that doesn't seem
to fit very well with any kind of defense.
The reason I'm bringing this up after a week
is because I have a feeling this is about to resurface.
This claim is going to resurface,
as well as the attorney-client privilege thing.
That there may actually be some, I don't know.
I mean, that's not out of the realm of possibility,
but I would also point out that if it's something
like between Trump and the White House counsel,
I'm pretty sure that would also be a presidential record.
So even if these documents are covered
by executive privilege in some way,
or would have been in other circumstances,
he's not the one who's supposed to have them.
They're supposed to be sitting up at the National Archives.
So I have a feeling we're going to hear a little bit more
about this particular talking point over the next week,
but we'll have to wait and see what they argue.
So short takeaway is that this isn't going to go away.
Trump's not going to let reality get in the way
of his talking points.
So we're probably going to see a bunch of other claims
surface on social media that don't really make
a whole lot of sense.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}