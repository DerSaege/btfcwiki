---
title: Let's talk about Trump begging McConnell for help....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bU8djnLlx_M) |
| Published | 2022/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Mitch McConnell's comments on the quality of candidates have led to Donald Trump seeking help from him.
- McConnell's doubts about Republicans retaking the Senate and the impact of Trump-backed candidates.
- Trump expressed his displeasure over McConnell's comments on Twitter, criticizing him for not supporting Republican candidates enough.
- The message from Trump's tweet is clear: McConnell should spend more time and money helping candidates get elected.
- Despite past conflicts, Trump now finds himself in need of McConnell's support for the upcoming elections.
- McConnell's long history in politics and the Republican Party's reliance on Trump's base for primary wins.
- Trump's candidates are struggling in the polls, prompting him to seek help from McConnell, who holds significant influence.
- McConnell, a strategic and experienced politician, may not see the benefit in backing Trump's candidates.
- McConnell's focus on salvaging the Republican Party post-midterms and preparing for 2024.
- The dynamics between Trump and McConnell reveal tensions within the Republican Party and their differing priorities.

### Quotes

- "He should spend more time and money helping them get elected."
- "You can't win a primary without Trump, but you can't win a general with him."
- "I don't see how it would benefit McConnell to help Trump."
- "If Trump's candidates fall on their face hard enough during the midterms here, maybe there's a possibility of salvaging the Republican Party in time for 2024."
- "I don't think that McConnell will be replying to this tweet."

### Oneliner

Donald Trump seeks help from Mitch McConnell after McConnell's comments on candidate quality, showcasing the complex dynamics within the Republican Party.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Support independent and moderate candidates in upcoming elections (implied)
- Stay informed about political developments and party dynamics (implied)

### Whats missing in summary

Insights on the potential impact of McConnell's decisions on the Republican Party's future.

### Tags

#Politics #RepublicanParty #Elections #DonaldTrump #MitchMcConnell


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the quality of candidates
and the former president, Donald J. Trump,
begging Senator Mitch McConnell for help.
That's not the narrative right now,
but when I look at this, that's what's happening.
See, McConnell was asked about some of the candidates
and the likelihood of flipping the Senate,
the likelihood of Republicans retaking the Senate.
And he kind of indicated that he wasn't entirely sure
it was going to happen,
and he said that it has a lot to do
with the quality of the candidates,
meaning the Trump-backed candidates
that won their primary.
Now, this, of course, this hurt Trump's feelings.
It bothered him a little bit,
and he put out this tweet from Wish.
Why do Republican senators allow a broken-down hack politician,
Mitch McConnell, to openly disparage
hardworking Republican candidates
for the United States Senate?
This is such an affront to honor and to leadership.
He should spend more time and money
helping them get elected
and less time helping his crazy wife and family
get rich on China.
It's done in typical Trump fashion, right?
He can't just come out and say, I need help.
But the important part of this message
is he should spend more time and money
helping them get elected.
Suddenly, Trump needs McConnell.
And that's what this is.
Now, as far as the question in the beginning,
why do Republican senators allow blah, blah, blah to do this,
I mean, the answer to that's pretty simple.
He won his re-election campaign.
That's why.
But there's another reason.
But we need context for that reason.
We have to really understand some scope.
So we're going to go back in time.
I want you to imagine it is 1968.
It's 1968.
In Vietnam, the Tet Offensive is going on.
The Beatles are still recording.
I want to say they're working on the White Album.
Janis Joplin has just appeared on nationwide TV
for the first time.
We have lost Martin Luther King.
We are coming out of the Summer of Love.
And Woodstock hasn't happened yet.
Do you know where Mitch McConnell is?
Capitol Hill.
1968.
He was already on Capitol Hill.
He was a legislative assistant for Marlo Cook in the 60s.
Mitch McConnell has been Senator McConnell since 1985.
The Republican Party pushed aside the advice of somebody
who has won every re-election campaign since 1985
and who has been involved in politics since the 60s
to take the advice of Trump.
Now that the primaries are over and that wild rhetoric
that motivates that part of the Republican base is out there,
now they're trying to figure out how they're going to get
their normal base, independents, moderates,
and actually win the election.
We've talked about it for a while.
It seems that now even Trump is beginning to understand
you can't win a primary without Trump,
but you can't win a general with him.
A lot of Trump's candidates are double digits down in the polls,
and he needs help.
Otherwise, he is going to look ridiculous.
He needs Mitch McConnell.
And the thing is, McConnell doesn't need him.
The Republican Party has some funding issues,
in large part because Trump snapped up
all of those small amount donors.
He has them sending him $45
for whatever the cause of the week is.
That cuts in to the Republican fundraising infrastructure.
But he wants McConnell to come out
and spend some energy and money to back his candidates.
McConnell is a very, very shrewd person.
I don't like him.
I don't like his policies.
I don't like much about, well, really anything about him.
But I won't underestimate him.
He is a very shrewd man.
I don't see how it would benefit McConnell to help Trump.
I don't see how it would help him at all.
So I am doubtful that Trump's plea, his begging,
is actually going to cause McConnell to help,
because from McConnell's perspective,
if Trump's candidates fall on their face hard enough
during the midterms here,
maybe there's a possibility of salvaging
the Republican Party in time for 2024.
I would imagine that's how McConnell is going to look at it.
I don't think that McConnell will be replying to this tweet.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}