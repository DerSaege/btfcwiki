---
title: Let's talk about the FBI searching Trump's home....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dSQLnykyySs) |
| Published | 2022/08/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The FBI executed a search warrant at former President Donald Trump's home, possibly related to the removal of classified documents from the White House.
- The warrant indicates a high level of evidence suggesting a crime was committed and incriminating evidence may be in Trump's possession.
- There are suggestions that some documents were returned and seized during the search, hinting at their importance.
- Speculation revolves around whether the documents are classified or just should have been in the National Archives, with potential overlap between different cases.
- FBI's actions indicate that the Justice Department may be overcoming hurdles to prosecute a former president, signaling a significant development.
- While an imminent indictment is not guaranteed, the situation does not suggest that investigations into Trump will dissipate.

### Quotes

- "This is really bad news."
- "Does this mean that an indictment is imminent? No, it really doesn't."
- "There might be overlap between them that we're not aware of."
- "This is really bad for the former president."
- "Those who may want to protect the institution of the presidency, my guess is that they have lost at least a major battle."

### Oneliner

The FBI's search warrant at Trump's home signals a significant development with high evidence suggesting wrongdoing, sparking speculation on the document's significance and potential legal implications.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Stay informed on the developments and implications of the investigations (suggested)
- Analyze the situation critically and contribute to informed discourse (implied)

### Whats missing in summary

Insights on potential repercussions for Trump, implications for future investigations, and the broader political landscape.

### Tags

#DonaldTrump #FBI #Investigations #SearchWarrant #LegalImplications


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the new developments
regarding former President Donald J. Trump,
what those developments mean for him,
what they mean for any possible investigation,
and just kind of give an overview of the situation,
the conventional wisdom,
and some things that I think might be being left out.
There will be more about this later.
This is an overview this morning.
Okay, if you don't know what I'm talking about,
if you have missed the news,
the Federal Bureau of Investigation
executed a search warrant at the former president's home.
Now, conventional wisdom says they are looking for documents.
The general idea is that this is related to the allegation
that the former president or staff removed boxes of documents
from the White House that should not have been removed,
and some of those documents were classified secret.
Okay, now, as this story develops,
what we have heard is that the Trump team
has been talking to the Department of Justice about this.
There's been meetings and stuff like that, conversations.
There's also reporting suggesting that documents were returned,
but then there's the search,
and according to reporting,
the agents did leave with documents.
They seized papers.
Now, for the Department of Justice
to get a warrant like this to search a home
for a normal person, okay, we like to pretend
that there's not a multi-tiered justice system in this country.
There is.
For a normal person, the type of warrant they got,
a federal agent had to say,
this is the crime that we think was committed,
this is the evidence that we think we'll find there,
and this is the location,
and they have to get a judge to sign off on it.
Generally speaking, the standard of evidence
that is met in this phase of federal investigations,
this is pretty close to the standard of evidence
that in state investigations, people are getting arrested.
So that doesn't bode well for the former president.
Now, conventional wisdom says all of this
is about the classified documents,
and it very well may be.
However, there is the reporting
that says something was returned.
What wasn't would be a pretty big question here, right?
Because if documents were returned
and some stuff was withheld,
well, that kind of suggests that that stuff's super important.
There's no indication that those documents are classified.
Keep in mind, there were a bunch of documents that were taken.
Some were returned, according to reporting.
These documents may not have secrets on them.
They just may be otherwise damaging
to the former president.
Generally, people are looking at this as separate cases,
the January 6th case,
you know, the fake electors case, and the documents case.
There might be overlap between them
that we're not aware of.
So that's something just to kind of keep in mind.
Is this bad for the former president?
Yeah, this is really bad.
This is really bad for the former president.
This is really bad news.
At this level, they have a lot of evidence
to suggest that a crime has committed
and somehow the evidence to support it
is in Trump's possession.
That's generally not something
a criminal defense attorney wants to hear.
Now, when you think back to how specific
those warrants have to be
and what they're looking for
and the idea that that evidence is in this specific location,
you have to wonder where that information came from.
I haven't been shy about the fact that I think
that there might be people in Trump's circle
who are maybe very free
when talking to certain government agencies.
I'm even more convinced of that now.
I definitely think there are probably people in Trump's circle
who are playing for both teams.
The other thing to keep in mind is that all of this is occurring
while there is a grand jury convened in DC.
This is bad news for the former president.
Does this mean that an indictment is imminent?
No, it really doesn't.
But just the idea that FBI agents
showed up at the home of the former president
and executed a search warrant
kind of indicates that the Garland Justice Department
has resolved any theoretical legal issues
when it comes to prosecuting a former president.
It seems as though the objections are kind of gone.
Those who may want to protect the institution of the presidency,
my guess is that they have lost at least a major battle.
So that's what happened.
Now, there's going to be a lot of speculation about it.
Here's the thing, until Trump tells us exactly what was taken,
and believe me, somebody on his team is going to at some point,
there's going to be this speculation, but it's idle.
The only thing that it could really tell us
is if somebody was able to comb the records
and find out exactly what the federal agents
specified that they were looking for.
When we get that, yeah, we'll have a lot more information.
But right now, the general consensus
is that it's over these classified documents.
I'm not, I haven't seen anything to actually say that.
It's over documents, sure, but is it over classified documents
or is it over documents
that just should have been in the National Archives?
Maybe documents that other investigations were looking for.
We don't know any of that yet.
Again, there could be overlap between these cases.
So that's a general overview if you missed it.
This doesn't necessarily mean indictment is imminent,
but it's definitely not a sign
that these investigations are going to go away.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}