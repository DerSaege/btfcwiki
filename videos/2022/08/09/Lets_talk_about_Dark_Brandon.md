---
title: Let's talk about Dark Brandon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OereBo7AQE8) |
| Published | 2022/08/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the emergence of the internet meme "Dark Brandon," depicting President Biden as a radical progressive in control.
- Describes the meme as a joke, originating from the idea of Biden being a centrist and some desiring a more progressive leader.
- Notes the meme mocks Republican narratives attributing success to their chosen candidate while ignoring failures.
- Observes how the meme has moved to the mainstream, creating confusion for those not familiar with online circles.
- Suggests that the meme might actually benefit the Biden administration by shedding light on their lesser-known achievements.
- Points out that Biden is not as progressive as some wish, being more center-right and a centrist.
- Encourages understanding that "Dark Brandon" is meant to be entertaining and satirical, not a reflection of Biden's actual policies.

### Quotes

- "Dark Brandon is a meme. It's a joke."
- "It's ironic. It's a joke."
- "It's worth looking at them. It's entertaining."
- "Just understand that it's a joke."
- "Y'all have a good day."

### Oneliner

Beau explains the satirical meme "Dark Brandon," depicting President Biden as a radical progressive in control, shedding light on lesser-known achievements while clarifying Biden's actual policies.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Share informative content about Biden's actual policies and achievements (suggested)
- Engage in political discourse to clarify misconceptions and share accurate information (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the satirical meme "Dark Brandon," offering insights into its origins, purpose, and potential impact on public perception and Democratic messaging.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about something
that's happening on the Internet
that has prompted a whole lot of questions
from a lot of people.
The most interesting of which came from somebody
who I happen to know is actually in politics,
and the question was basically framed,
could you explain this as if you were talking to somebody
who was going to have to brief President Biden
on who Dark Brandon is?
Okay, so if you haven't experienced Dark Brandon yet,
rest assured you probably will.
It's a meme.
Dark Brandon is a meme.
It's a joke, and it depicts President Biden
as somebody who is totally in control
and ruthless in the pursuit of incredibly progressive ideas.
It's a joke.
It started, well, actually,
there's a long developmental history with this meme,
but to the point that matters,
it started with people who understood
that Biden is a centrist.
He's not incredibly progressive, but that's what they want.
Originally, it was for people kind of like me.
If you remember before the election,
Joe Biden's not a savior.
He's a centrist.
Joe Biden's not my man.
I want the Joe Biden from Republican memes.
That's Dark Brandon.
Dark Brandon is leaning into the Republican idea
that he is some super left-wing progressive
who's controlling everything behind the scenes
and pulling the strings and all of that
and taking it to an extreme.
Simultaneously, it's mocking the Republican tendency
under Trump and even now to attribute anything good
to their chosen candidate while saying, you know,
that they have to stick with the plan and all of that stuff
and have faith in the savior.
It's ironic.
It's a joke.
However, it has now crashed into the mainstream,
and it has left these circles of people who are very online.
Leaving some confusion for some people.
The general idea is that there's a whole lot of people
who want Dark Brandon,
who want a president who is just unstoppable
in pursuit of progressive ideas,
somebody who isn't afraid to look at a couple of senators
who may be stopping an important piece of legislation
involving the inflation and just be like,
you're going to pass this for the working class or else
while eating ice cream.
That's Dark Brandon.
It's entertaining as a joke.
It is important to remember that it is a joke.
Biden is not super progressive,
as much as people like me wish he was.
The interesting thing about it is that now
that it's hit the mainstream,
the smaller achievements that the Biden administration
is getting, I've talked about how the Democratic Party
is really bad at messaging,
and nobody knows about the smaller wins.
People are now finding out about them
because people are making memes with Dark Brandon
and crediting him for it.
Now, at the same time, this is a joke
about how the Republican Party will credit the president
with anything good and deny anything bad
or try to shape anything bad as part of some grand plan.
That's what Dark Brandon is.
That's what the memes are going to do.
It may, given the way US politics works,
it might actually be helpful for the Biden administration
because it does assist their messaging.
People will find out more about the lesser achievements
of the Biden administration,
the stuff that doesn't make headlines
because the media can't get ad revenue off of it.
And with the Biden administration,
he's actually had a lot of achievements,
but most aren't headline-grabbing.
They're not the sort that are going
to provoke a lot of discussion, but there's hundreds of them.
They're little baby things,
and eventually they'll all add up.
Dark Brandon taking credit for it
might actually really help the Democratic Party
get that messaging out
so people are aware of the smaller achievements
that the Biden administration has accomplished,
even though they may not be incredibly excited
about how Biden is sometimes portrayed.
It's worth looking at them.
It's entertaining.
Just understand that it's a joke,
half making fun of Biden while pushing progressive ideas
and half making fun of the Republican Party
in their belief that a president is a savior
who controls all.
But if I was a politician who had any influence
over Democratic messaging,
I'd probably lean into this a little bit
because for younger people,
this is going to be one of the coolest things
the Democratic Party's ever done.
So for everybody else, just remember it is a joke.
Biden isn't really a radical leftist.
He's center-right.
He's a centrist,
and his policies are somewhat socially progressive.
Dark Brandon doesn't actually change any of that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}