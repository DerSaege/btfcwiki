---
title: Let's talk about Republicans calling for an FBI hearing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uwAdGas0f90) |
| Published | 2022/08/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans want to hold hearings to look into the Department of Justice's actions at Mar-a-Lago, with some calling to defund the FBI and labeling them as radically left.
- Beau believes it's necessary to have hearings and investigations into how the Department of Justice handled the situation at Mar-a-Lago, despite not thinking they did anything wrong.
- He speculates that a hearing might reveal that the Department of Justice moved slowly and was cautious, potentially influencing Trump positively.
- Beau suggests that Republicans calling for investigations are not genuinely seeking corruption but trying to frame a narrative where Trump is the victim and to energize their base.
- He points out the danger in portraying government institutions as enemies and warns against repeating past actions that led to harm.
- Beau criticizes the tactic of lying to the base, creating enemies, and riling them up to support Trump, which he believes might backfire in the current political climate.

### Quotes

- "This call, if anything, it shows the importance of DOJ continuing. Because they've learned nothing."
- "I've seen this movie before. I know how it ends."
- "Lie to the base, give them an enemy, rile them up, and therefore they'll coalesce together and support Trump."

### Oneliner

Republicans' calls for investigations into the Department of Justice's handling at Mar-a-Lago are seen as attempts to frame Trump as a victim and energize their base, risking repeating harmful past actions.

### Audience

Political observers, Activists

### On-the-ground actions from transcript

- Monitor and advocate for transparent and unbiased investigations into government actions (implied)
- Stay informed and push for accountability in political narratives (implied)

### Whats missing in summary

Insights on the importance of holding government institutions accountable and the risks of manipulating political narratives.

### Tags

#Republicans #DepartmentOfJustice #Investigations #PoliticalNarratives #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about the Republicans
calling to hold hearings or otherwise investigate and look into the Department of Justice
over what happened down at Mar-a-Lago.
You know, there's been a lot of people in political office from the Republican side
saying that the answer to this is to go after or even defund the FBI.
They're calling them radically left.
Yes.
Because when I think radical left, I think FBI.
Anyway, a lot of questions have come in.
Is this intimidation?
Is it obstruction?
How do we stop it?
Don't.
Don't.
Don't stop it.
I, and I'm not joking when I say this, I actually think it's really important for there to be
hearings and an investigation into how the Department of Justice handled this.
This is the most expansive investigation they have ever had.
I don't actually think they did anything wrong, but it's important to hot wash it.
It's important to go through it afterward.
I would like to know how much, how much politics was given a consideration in it.
You know, the Republican Party, and we'll get to the real reason they're doing this
in a minute, you know, their idea is that they're going to find some kind of corruption.
I find that highly unlikely.
I think that if there are hearings into it, what we're going to find out is that the Department
of Justice moved slower and was influenced more to be cautious.
And if anything, it helped Trump.
That's what I think would be found in a hearing.
I don't oppose one, but I don't think the Republicans actually would pursue that.
Not really, because if they open that up, all of that information is going to come out.
There's going to be a whole lot of information that doesn't come out that would come out
in hearings.
And it's basically all going to be damaging to the Republican Party.
So I don't really think they're going to pursue that.
These calls, it's not about influencing the Department of Justice.
It's not that they really think that, you know, the guy Trump appointed is some radical
leftist.
It's about framing a narrative that Trump is the victim and that everybody else is to
blame and that there's some evil force out there to rile up their base, you know, to
get them motivated.
Apparently completely unaware of the fact that the last time they did this, a whole
bunch of people got hurt.
Almost a thousand people got arrested.
Lives were destroyed.
I guess they forgot about that.
This call, if anything, it shows the importance of DOJ continuing.
Because they've learned nothing.
They're doing it again.
They are doing it again.
They are setting their base up to be energized by a lie, by something that isn't true, that
paints government institutions as some mythical enemy out to get Trump.
I've seen this movie before.
I know how it ends.
And when it does, if it goes down that same road, make no mistake about it, it was the
people on TV last night framing that idea that are really behind it.
It's their fault.
They're putting that narrative out there.
Let's be honest.
This was a search warrant.
It was a search warrant over documents.
Conventional wisdom says it's over classified documents.
If there's a group of people who shouldn't be opposed to recovering classified documents,
it probably should be the people who are involved in making sure our counterintelligence activities
go according to plan.
And in some cases, those were the people casting doubt on it.
This is just like the other rhetoric you've gotten from the Republican Party for the last
year.
Lie to the base, give them an enemy, rile them up, and therefore they'll coalesce together
and support Trump.
That's what they're hoping to achieve by it.
And they very well might lose control of it again.
This isn't actually a great political tactic, especially given the current climate.
I don't oppose hearings or an investigation into how this went down.
I actually think it's really important that that occurs.
We need to know how the Justice Department came to the conclusions it came to so they
can be more effective at it.
I don't believe that there's going to be some massive corruption uncovered, but I think
it's really important for it to be hot-washed and examined, because given the current political
climate, they might have to conduct another similar investigation.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}