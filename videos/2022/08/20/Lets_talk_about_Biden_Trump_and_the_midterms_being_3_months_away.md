---
title: Let's talk about Biden, Trump, and the midterms being 3 months away....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f4dCH6meFB4) |
| Published | 2022/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contrasts Biden's achievements with Trump's ongoing issues, providing Democratic candidates with positive talking points heading into the midterms.
- Points out that Americans often credit the president with things beyond their control, bolstering Biden's image.
- Lists legislative achievements like the CHIPS Act, PACT Act, Inflation Reduction Act, and bipartisan infrastructure law as wins for Democrats.
- Mentions how even indirect effects like job creation and gas prices can be attributed to Biden in the public eye.
- Notes that Biden's Senate experience may have influenced the successful passage of legislation, putting Republicans in a tough spot.
- Suggests that Biden's handling of Trump's legal issues and staying off Twitter may appeal to voters tired of Trump's style of leadership.
- Observes that the Republican Party is facing internal divisions, with some members looking to move on from Trump's influence.
- Urges Democrats to focus on showcasing their accomplishments and energizing their base leading up to the midterms.
- Acknowledges that while some may find Biden's progress insufficient, it still surpasses Trump's achievements in the eyes of many.
- Encourages continued observation as the political landscape evolves in the months ahead.

### Quotes

- "It gives Democrats something to run on."
- "Biden has actually kind of done pretty well over the last couple of months."
- "There are going to be some people, people who are more progressive, who are going to look at this and, as always, good start, not enough."
- "That's a party that has real issues."
- "But there's also still like three months left, so let's see what happens."

### Oneliner

Beau points out Biden's achievements and contrasts them with Trump's troubles, setting the stage for Democrats in the upcoming midterms.

### Audience

Voters, Political Observers

### On-the-ground actions from transcript

- Support Democratic candidates and help spread awareness of their accomplishments (exemplified)
- Stay informed about political developments and encourage others to do the same (exemplified)

### Whats missing in summary

Detailed analysis and context on Biden's presidency and its impact on the upcoming midterms.

### Tags

#Biden #DemocraticParty #Midterms #Achievements #Trump #Legislation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Biden mostly
and how Biden is steering the Democratic Party
as we move into the midterms.
You know, Trump is in the middle of, like,
the worst couple weeks in modern American political history.
It's just one thing after another after another,
and it's all bad news.
By contrast, Biden is racking up achievements,
and those achievements are providing
Democratic candidates something to run on.
And a lot of them may not actually be things that he did.
They may not be things that he's responsible for.
But when the average American looks at the presidency,
they kind of credit them with a lot of things
that they don't actually have a lot of control over.
And it's all breaking his way.
At this moment, things are going his way.
You look at the legislative side of things.
You have the CHIPS Act.
You have the PACT Act.
You have the Inflation Reduction Act.
You have the bipartisan infrastructure law.
All of this stuff that he got through,
that it gives Democrats something to run on.
We did this.
We, you know, got this climate stuff through.
We got that tax on big business that never pays anything.
We got the cap on your insulin costs.
It gives them stuff to talk about heading
into the midterms, which are less than 90 days away.
For a certain group of Americans,
that strike in Afghanistan,
that's going to go over really well with them.
And then there's all the stuff that is not a direct effect
from Biden.
You know, the job creation, record low unemployment,
the expansion of NATO,
the gas prices going down.
Sure, he may have contributed to some of these things,
but generally speaking,
this isn't actually in the president's purview,
not really under their control.
However, that's not normally the way
the American people see it.
So he's going to get credit for this stuff,
even if it wasn't him,
or even if he didn't play a significant role in it.
And then on top of all of this,
the way the legislation was pushed through,
probably having something to do
with a lot of his time in the Senate,
it put the Republican Party in a position
that it voted against things its base wants.
I mean, the attack ads write themselves.
They voted against vets.
They voted against insulin.
Like, not just has he provided Democrats
who are running for re-election
or running for office something to say,
hey, look at what we did,
but also look at what they didn't do.
I mean, it's still 90 days away.
A lot can happen, you know, in that period.
But moving into the midterms at this point,
when you're looking at it,
Biden has actually kind of done pretty well
over the last couple of months.
Definitely a rocky start,
but as this stuff adds up,
it's going to energize a lot of the base.
And those independents who are going to be faced
with a party that is just neck deep in trouble
and a party that is at least moving in a direction,
more so than just standing still
trying to figure out what's going to happen
to their chosen mascot,
I think they'll probably side
with the party that's moving.
And then I think one of the smartest things
that the Biden administration has done
is they're not really weighing in on Trump's legal issues.
Can you imagine if Trump was in office right now
and it was his opposition that was in this situation,
was in the situation he's in?
It would be nonstop.
The Biden administration isn't doing it.
They're not on Twitter slamming Trump.
There's another accomplishment.
Biden managed to stay on Twitter.
I think that may actually carry a lot of weight
with the average voter.
There are even people on the Republican side of the aisle
who are now saying that the Republican base
is just exhausted with that style of leadership,
with what Trump offered.
They're ready to turn the page on it.
If there are elements within the Republican Party
that are feeling like that,
it's probably true of those independents,
those people who aren't strict adherents to any party.
And then when you look at the results
of the primary there with Cheney,
and you realize that even though she lost,
one out of four Republicans voted for the person
who is trying to take down the face of the Republican Party,
that's not a unified front.
That's a party that has real issues.
All the Democratic Party needs to do at this point
is continue the path they're on and actually get out there
and talk about what they accomplished.
You know, there are going to be some people,
people who are more progressive,
who are going to look at this and, as always, good start,
not enough.
But for most Americans, this is more than Trump got done
during his entire administration.
And because it happened so quickly,
it might actually overshadow the very bumpy start
that the Biden administration had.
But there's also still like three months left,
so let's see what happens.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}