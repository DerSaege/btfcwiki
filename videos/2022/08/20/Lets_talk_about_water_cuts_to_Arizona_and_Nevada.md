---
title: Let's talk about water cuts to Arizona and Nevada....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZhaelXGX-zA) |
| Published | 2022/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Federal government announces cuts in water allotments from the Colorado River due to moving into a tier two shortage.
- Arizona will lose 21% of its water allocation, equivalent to 592,000 acre feet.
- Nevada will face an 8% cut, impacting an area with significant water needs.
- Mexico will experience a 7% reduction in water flow.
- Arizona's $15 billion agricultural industry heavily relies on these water flows.
- The 22-year-long drought in the region is not just a temporary situation; it's the new normal.
- Continuing to refer to the ongoing drought as temporary misleads people into expecting a quick resolution.
- The federal government's actions aim to prevent a catastrophic loss in the Colorado River basin but may not be sufficient.
- The increasing population and demands in the area, combined with limited water resources, pose a significant challenge.
- It's time to acknowledge the reality of climate change and the prolonged impact of the drought on water availability.

### Quotes

- "This is the new normal."
- "We have to stop pretending."
- "We can't continue to act like it's going to end tomorrow."

### Oneliner

Federal cuts in water allotments from the Colorado River signal the new normal of a prolonged drought, urging a shift in mindset towards addressing long-term climate challenges.

### Audience

Southwestern Residents

### On-the-ground actions from transcript

- Prepare for worsening water shortages by implementing water conservation measures (implied).
- Support local initiatives promoting water recycling and conservation efforts (implied).
- Stay informed and advocate for sustainable water management practices in the region (implied).

### Whats missing in summary

The emotional and environmental toll of the prolonged drought on ecosystems, livelihoods, and communities.

### Tags

#WaterCuts #ClimateChange #ColoradoRiver #WaterConservation #Drought


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about water cuts in Colorado
and how it's going to impact Arizona and Nevada pretty soon.
The federal government has announced cuts
in the allotments of water that states
are going to be able to get from the Colorado River.
Now, this is because they are moving into a, quote,
tier two shortage.
So in January, the cuts will go into effect.
Arizona will lose 592,000 acre feet.
It's a term we've talked about on the channel before,
but to catch everybody up.
That is one acre of land, one foot deep in water.
That's hard to visualize, other than that's a lot of water,
right?
21%.
Arizona is getting a 21% cut in how much they can pull.
That's significant.
Nevada is getting an 8%.
And it's a smaller amount, but at the same time,
it's an area that needs it more.
But also at the same time, Nevada
has put a lot of infrastructure in to recycle water
and push water back out after it's been cleaned.
So they probably won't feel it, at least initially.
Arizona definitely will.
And then Mexico is getting a 7% cut in their flow.
This is going to heavily impact all of the normal things
you think about when you think about water.
You have a, I want to say it's like $15 billion
agricultural industry down there that relies pretty heavily
on these flows.
Now, one of the things that I think that we really
have to come to terms with is that this is the new normal.
When you look at media coverage of this, what you get is,
well, it's a drought.
Yeah, it's a drought.
How long has the drought lasted?
22 years.
That's not a drought.
That's the climate now.
That's what it is.
Sure, it might change in the future.
But by continuing to refer to it as a drought leading to this,
it leads people to the idea that, well, next spring,
it'll be back to normal.
No, this is 22 years of decline like this.
This drought has lasted 22 years.
And there's no signs of it letting up.
We have to stop framing this as something temporary.
We have to stop acting like in just a few short months,
it'll go away.
It won't.
This step by the federal government
is designed to stop a catastrophic loss
in the Colorado River basin.
And it's not.
Odds are this isn't going to be enough.
There's going to be more because there are still
more people moving there.
There are more demands on the water.
And there's a limited amount of water.
It's just math.
We have to stop pretending.
We have to face the fact that the climate is changing.
22 years of this drought.
We can't continue to act like it's going to end tomorrow.
If you're in these areas, you need
to realize that it's just getting worse.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}