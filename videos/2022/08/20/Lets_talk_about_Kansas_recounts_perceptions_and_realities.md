---
title: Let's talk about Kansas, recounts, perceptions, and realities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IUA3sRYVdMM) |
| Published | 2022/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts are more about galvanizing a base and sowing doubt rather than actually changing outcomes.
- A recount altering an election outcome is incredibly rare.
- Activists in Kansas are funding a recount of nine counties related to a ballot initiative on family planning, costing over a hundred grand.
- In 2004 in Washington, the largest swing in a statewide election occurred, with Gregoire leading by 129 or 133 votes after a manual recount.
- The outcome of an election being altered by a recount is highly unlikely, especially with significant margins like in Kansas, where 165,000 votes separate the candidates.
- Politicians who push for recounts after losing by significant margins may not be fit for office.
- Recounts are often used to bring in money, galvanize supporters, sow doubt, and keep people angry rather than genuinely change outcomes.

### Quotes

- "Recounts are more about galvanizing a base and sowing doubt rather than actually changing outcomes."
- "The outcome of an election being altered by a recount is incredibly rare."
- "Politicians who push for recounts after losing by significant margins may not be fit for office."

### Oneliner

Recounts are about galvanizing bases, not changing outcomes; rare to alter elections, like in 2004's Washington case, with a swing of 129-133 votes.

### Audience

Voters

### On-the-ground actions from transcript

- Support fair and transparent election processes (implied).
- Stay informed about the realities of recounts and election outcomes (implied).

### What's missing in summary

Importance of staying grounded in reality and not being swayed by baseless claims or unrealistic expectations when it comes to election recounts.

### Tags

#ElectionRecounts #Activism #Reality #Voting #PoliticalEngagement


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
perceptions and expectations and recounts and grounding ourselves in reality.
Recounts are becoming more and more common, not because the people involved think it's
actually going to change the outcome, at least those at the top, but because it's a good
way to galvanize a base and keep people engaged and sow doubt. Right? There are a lot of people
who have bought into baseless claims. Those people look at recounts as a way to alter
the outcome. Now, the reality is that an election outcome being altered by a recount is incredibly
rare. That's not something that happens very often. At time of filming, there's a recount
that's about to happen in Kansas. And some activists got together and they're paying
for a recount of nine counties, I think, in the ballot initiative dealing with family
planning. My understanding is that the people who are paying for this are putting it on
their credit cards. And we're talking about a lot of money, more than a hundred grand.
So I think, if I'm not mistaken, the largest swing, the largest lead that was deleted since
the year 2000 in a statewide election occurred in 2004 in Washington. It took seven months
to resolve the election totally. In the beginning, it looked like the candidate for governor,
Dino Rossi, was going to win. 261 votes ahead. In a statewide election, 261 votes? That's
not a lot. That is super close. So close, in fact, it triggered an automatic recount.
And when the automatic recount was over, that lead had shrank and he only was ahead by 42
votes. Then the manual recount started. And during the manual recount, because you actually
have people looking at things, they discovered some ballots that had been set aside, that
didn't get counted. Not rejected ballots, not anything like that, just ballots that
didn't get counted. When it was all said and done, Gregoire, the other candidate, was leading
by, depending on the source, 129 or 133 votes. That's the biggest swing. A grand total of
either 394 or 392 votes, depending on the source you use. Across an entire state, less
than 400 votes. And that was also almost 20 years ago. Technology's gotten a little bit
better. Things have gotten better. So when a candidate or a ballot initiative loses,
a recount probably isn't going to alter the outcome. I mean, this is one of the few times
the outcome was altered, and we're still only talking... we're talking about less than 400
votes here. Less than 400 votes. So what would it take to alter the outcome of the vote in
Kansas? 165. Thousand. There's no way. The outcome of that vote, that's not changing
because of a recount. It's, for many people, and I'm not saying this about the activists
behind this, I honestly think this is just straight disbelief, but I think for a lot
of people who push the recount narrative, it has nothing to do with actually thinking
it's going to change the outcome. It has to do with bringing in that money, galvanizing
that base, sowing that doubt, feeding that fear, keeping people angry. Again, I'm not
talking about the people in Kansas. The people behind that, as near as I can tell, I honestly
think that they think something is wrong because they just can't believe that that was the
outcome of the vote. I don't think that they're doing it for those reasons, but they're not
really politicians either. But given the amount of votes that would need to be changed, the
recount's pointless. It really is. When politicians do this and the numbers, when they lose an
election by 10,000 votes or something like that, and they start screaming about a recount,
refusing to concede and stuff like this, that should be a sign that this person actually
probably doesn't have a whole lot of business being in office. Because it is so rare that
an election is changed by a recount. And when they are changed, they're like this. 261 votes.
That's not a lot. If I'm not mistaken, there were two or three million votes counted in
that election. So just bear that in mind. Ground yourself in reality. When you are talking
about these recounts, when you hear these claims, especially if that politician needs
your money to help fund that recount, it's probably not going to give you the outcome
that you're looking for. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}