---
title: Let's talk about the next rocket to the moon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VN-dKvEuBjw) |
| Published | 2022/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA is preparing to go back to the moon as part of the Artemis program.
- A giant new rocket is set to launch on August 29th, with no crew onboard, just mannequins with sensors.
- The capsule will orbit the moon for a couple of weeks before returning to Earth.
- The whole mission is expected to take about a month and a half.
- Previous dress rehearsals for the rocket haven't gone smoothly, but hopefully, this launch will be successful.
- The ultimate goal is to have a crew on the moon by 2025 for extended periods.
- Despite being a significant event, it might be overlooked due to distractions on Earth.
- This mission marks a critical step towards humanity's goal of space exploration.
- NASA aims to eventually have people on the moon for extended periods.
- Getting humans off Earth is a monumental achievement, and this mission is a significant step in that direction.

### Quotes

- "This is one of the new steps towards getting us off this rock."
- "It's a new phase."
- "Getting off of this rock, that will be a crowning achievement."

### Oneliner

NASA plans to send a rocket to the moon in a month-long mission, marking a critical step towards extended human presence in space, amidst potential Earthly distractions.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Stay updated on NASA's Artemis program progress (implied)
- Support and advocate for space exploration initiatives (implied)

### Whats missing in summary

Details on the significance of prolonged human presence on the moon

### Tags

#NASA #SpaceExploration #Artemis #MoonMission #FutureInSpace


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about NASA
and what it's up to and what it plans to be up to
over the next couple of years.
And the first really big step that we're
taking to go back to the moon.
Right now, that giant new rocket is sitting on the launch pad.
NASA is aiming for a August 29th liftoff.
And this will be the first real part of Artemis.
There won't be any people in the capsule this time.
It'll just have a bunch of mannequins
that have sensors to feed information back.
But it's also not just a shoot the rocket up in the sky
and let it fall type of thing.
The capsule will go to the moon.
And it will orbit the moon at a distant orbit
for a couple of weeks before returning back to the Earth
and splashing down somewhere in the Pacific.
The whole flight should take about a month and a half.
This isn't the first time the rocket's
been on the launch pad.
There have been a couple of dress rehearsals
that haven't gone so well.
But hopefully, they have all the bugs worked out
and will actually get liftoff.
And we will get to see what the future of space exploration
looks like.
The current plan is to have a crew on the moon in 2025.
Not that far away.
This is one of those events that when it's happening,
odds are it's going to be overlooked.
This isn't going to get a lot of coverage.
People are just going to view it as a test
and probably won't care that much, especially
with everything going on here on Earth.
People will be distracted.
But this is one of those moments that it's
going to be in history books.
This is one of the new steps towards getting us off
this rock.
Yeah, it's happened before.
But this time, the goal seems to be a little bit more
of a longer endeavor.
Looks like they plan on eventually getting to a point
where people are there for extended periods.
It's a new phase.
And assuming humanity makes it, getting off of this rock,
that will be a crowning achievement.
And this is a first real big step towards doing that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}