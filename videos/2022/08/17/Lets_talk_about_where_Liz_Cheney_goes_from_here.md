---
title: Let's talk about where Liz Cheney goes from here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dM4C4iqDlCw) |
| Published | 2022/08/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Liz Cheney is now free from political concerns after being removed from her position, allowing her to pursue the truth without worries.
- Whether Cheney wins or loses the ongoing voting, her future remains significant in the short and long term.
- In the short term, if Cheney lost, she may be seen as a Bernie-type figure within the Republican Party.
- If Cheney wins, she could become a powerful voice and a key player within the Republican Party.
- Cheney's legacy is already secured, and she is likely to be remembered more favorably than Vice President Cheney.
- Cheney may become a candidate for an executive branch position in the near future, depending on what happens to Trump and Trumpism.
- Her future heavily relies on whether the Republican Party breaks free from Trumpism.
- Cheney has positioned herself as a potential leader in the Republican Party if it undergoes a transformation.
- The ongoing primary election will have a decisive impact on shaping history books and the future of the Republican Party.
- Cheney's role will be pivotal in either leading the party in a new direction or being remembered as a missed chance for change.

### Quotes

- "Her legacy is secured. She will be in the history books and she will be remembered more favorably than Vice President Cheney."
- "Cheney has positioned herself to be a leader in the Republican Party in the event that it wakes up."
- "Win or lose, her position is secured."
- "It's odd that a Cheney has positioned themselves to be recorded in history books as a real defender of the American experiment."
- "She'll be the candidate that people look back on and say, well, this person could have led the Republican Party somewhere better."

### Oneliner

Representative Liz Cheney's future in the Republican Party is secured, poised to lead if it breaks free from Trumpism or remembered as a missed chance for change.

### Audience

Political observers

### On-the-ground actions from transcript

- Support political candidates who prioritize truth and accountability within their party (suggested).
- Stay informed about the ongoing developments in the Republican Party and how they may shape future leadership (exemplified).

### What's missing in summary

The full transcript provides a detailed analysis of Liz Cheney's potential future within the Republican Party, including the impact of ongoing voting and the party's stance on Trumpism.


## Transcript
Well howdy there internet people, it's Bo again. So today we are going to talk about
Representative Liz Cheney. Where she goes from here, what happens next? And the
answer is pretty simple. Where she goes from here? Back to work. Back to that
committee. And she's totally free now. She doesn't have to worry about a primary, she
doesn't have to worry about political backlash. She can be as aggressive as she
wants to be in pursuing the truth and getting to the bottom of what happened.
No political concerns. Now to be honest, it doesn't appear that the political
concerns have weighed heavily on her thus far, but now there are none. There
are none. She is totally free in that regard. Now here's the fun part. Voting is
still going on. I have no idea whether or not she won or lost. At time of filming,
voting is happening. But it doesn't change anything. No matter if she wins or
if she loses, that's the reality for the short term. That's the reality. Now beyond
that, if she lost, okay she lost. What's her likely future? In the intermediate
term, she becomes a Bernie type character in the Republican Party. You will have
people who will say, you know, if we had gone with Cheney, we wouldn't have lost
this election. She will become an icon within the Republican Party as a symbol
of a different possible future. Now if she won, if she won, she will become one
of the most powerful voices in Congress. She will become a power broker within
the Republican Party. And where it goes in the long term all depends on what
happens to Trump. Her legacy is secured. Her legacy is secured. She will be in the
history books and she will be remembered more favorably than Vice President
Cheney. That part's done. But in the long term, it depends on what happens to Trump
and Trumpism. Either way, I would expect to see her as a candidate for some place
in the executive branch in the relatively near future, next 12 years,
probably sooner than that. She'll be a candidate. Will the Republican Party have
broken free from Trumpism by that point? We don't know. There's a lot up in the
air and the election that is occurring, that primary that's happening right now
as I'm filming this, it's going to shape the outcome of history books. It's going
to matter. In one way or another, it will be decisive. Cheney has positioned
herself to be a leader in the Republican Party in the event that it wakes up. If
the Republican Party realizes the errors of its ways, she will become a default
choice to lead it. If they don't, she will be the, she'll be the what-if story.
She'll be the candidate that people look back on and say, well, this person could
have led the Republican Party somewhere better, somewhere else, but we just didn't
see it at the time. It's odd that a Cheney has positioned themselves to be
recorded in history books as a real defender of the American experiment, but
that's what's happened. Win or lose, her position is secured. Just like many in
Trump's circle, their position is also already written. It's just not as
favorable. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}