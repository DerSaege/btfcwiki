---
title: Let's talk about whether Cheney's loss is Trump's win....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ua5vk5wyUMg) |
| Published | 2022/08/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Liz Cheney lost in what's being seen as a blowout for the Trump side.
- Cheney is getting around 33% of the votes, with some Democrats crossing over to help.
- Even if one in four Republicans voted for Cheney, who is trying to put Trump in jail, it may not be a win for Trump.
- This loss in a very red area could indicate bad news for Team Trump in terms of popularity.
- The primary win for Team Trump may not translate to general elections.
- The push to remove Cheney was driven by Trumpist factions within the Republican Party.
- Cheney's ability to capture even one out of four votes in that location is seen as unfavorable news for Trump.

### Quotes

- "One in four Republicans voted for the person who is openly and actively trying to put Trump in jail."
- "This is pretty bad news for the Trump team."
- "Even in very red areas, there are a whole lot of people who side with the person trying to put Trump away than Trump."
- "This was a push by the Trumpist factions of the Republican Party to get her out."
- "Cheney's ability to capture even one out of four votes in that location is really bad news for Trump and company."

### Oneliner

Liz Cheney's loss in a very red area, where even one in four Republicans voted for her, signals potential trouble for Team Trump's popularity.

### Audience

Political Analysts

### On-the-ground actions from transcript

- Analyze voter trends in red areas (implied)
- Stay informed about political shifts and factional dynamics within parties (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Liz Cheney's loss and its implications for Trump's popularity, offering insights into factional dynamics within the Republican Party.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Liz Cheney's loss,
but how it's also not Trump's win.
I'm recording this last night.
The votes, they're being tallied.
I'm watching the numbers come in.
She's lost.
And it's being kind of heralded as a blowout
for the Trump side.
And it would seem like it.
I mean, Cheney's getting around a third of the votes.
Now, keep in mind, it's not the final total, right?
But she's pulling in about 33%.
33%. And sure, there were Democrats who crossed over
to try to help.
Let's say that's 3%.
That's not really a win for the Trump side of things.
I mean, it's a win in the primary, sure.
You know what?
Let's just bump it up.
Let's just say it's only one in four Republicans.
Let's just say out of that 33% or so that she captured,
let's say 8% of them are Democrats
who crossed over to help.
It's not that high, but let's pretend that it is.
That means one in four Republicans voted for the person
who is openly and actively trying to put Trump in jail.
I don't know that that's a win.
What does that say about independents?
Where are they at?
I know that Trump world doesn't have much
to celebrate right now,
so they're going to latch onto this
and they're really gonna hammer it home.
But when you really think about what happens
outside of a primary, this isn't good news.
This is pretty bad news for the Trump team.
This is a very red place.
And among that very red population,
one out of four Republicans voted in favor of the person
who's trying to put Trump behind bars.
I don't know that I would take that as a win
but if that is the read in a primary,
the general feeling among the populace
is going to be very, very different.
It's a win for Team Trump in the sense of the primary.
But if you're looking at that as a gauge
of where Trump's popularity really sits, it's not good.
It's actually really bad news for them.
They're not gonna see it that way.
I mean, don't get me wrong.
They won't see it that way until the general.
But this certainly shows that even in very red areas,
there are a whole lot of people who would side
with the person trying to put Trump away than Trump
because that's what this was.
This was a push by the Trumpist factions
of the Republican Party to get her out.
The other candidate doesn't matter.
This is one of those moments where they tried
to motivate people via negative voter turnout,
people to vote against Cheney,
not necessarily for anybody else.
If in that location, Cheney got even one out of four,
which is less than the totals,
that's really bad news for Trump and company.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}