---
title: Let's talk about the 5 words that might sink Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4oYBWgjFr7w) |
| Published | 2022/08/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing whether a quote from Trump's advisors about possession of boxes is an admission.
- Trump reportedly said, "It's not theirs, it's mine," concerning government-owned boxes.
- Possession is a critical aspect in many laws, making the statement concerning.
- The statement could be troubling for prosecutors and Trump's attorneys.
- Some of Trump's advisors who heard the statement have talked to the FBI.
- MSNBC framed it as an admission, but there's a significant difference between informal statements and testimonies under oath.
- Speculation in such a serious investigation may not be wise.
- If the statement reaches the FBI and is part of a sworn statement, it could be extremely damaging.
- However, it's uncertain if the statement made it to the FBI.
- Beau advises managing expectations and following the pace of the investigation based on confirmed information.
- Trump's statement, if true, could have serious implications.
- While Trump's circle may disclose damaging information to media, it doesn't necessarily prove possession for legal purposes.
- These five words could haunt Trump until the case is resolved.
- Beau suggests waiting for further developments rather than jumping to conclusions.

### Quotes

- "It's not theirs, it's mine."
- "I mean those five words, that's really bad and it totally sounds like Trump."
- "Five words that will probably haunt the former president for quite some time until this case is resolved one way or another."

### Oneliner

Beau analyzes whether a statement from Trump's advisors about possession of government-owned boxes constitutes an admission, cautioning against premature conclusions in the serious investigation.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Stay updated on confirmed information related to the investigation (suggested)
- Avoid speculation and premature conclusions (suggested)

### Whats missing in summary

The full transcript provides detailed analysis and insights into the potential legal implications of a statement made by Trump's advisors regarding possession of government-owned boxes.

### Tags

#Trump #LegalImplications #Possession #Investigation #FBI


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about whether or not they got him, whether or
not that five word sentence is really an admission, which is how it's being framed.
I said, we will be talking about Trump, those boxes, who possessed them, and
the federal Bureau of Investigation, if you have no idea what I'm talking
about, some reporting came out in the New York Times, and there was a quote in it that
caught a whole lot of people's attention.
And the quote is from Trump's advisors, according to the reporting, and it says that this is
what Trump said to them about these boxes.
It's not theirs, it's mine.
Theirs being the government.
That is a really bad statement.
A lot of these laws, possession is a really defining thing in them.
Saying it's not theirs, the government, it's mine is a bad statement.
That's a statement that gave prosecutors goosebumps and his attorneys heartburn.
Now he is reported to have said this to his advisors and we know that some of his advisors
have talked to the FBI.
Cipollone, Philbin, if he said this to them, they probably relate it to the FBI.
And then we have the people who are talking to the FBI who we don't know.
don't know who they are. The thing is, I mean, yes, MSNBC in particular seized on
it and was like, that's an admission and that's an analysis of it. But, and it's a
big but, there's a huge difference between telling Maggie at the New York
times that this is what Trump said and saying it under oath.
I don't think that this is a good time to speculate a lot.
These are incredibly serious documents.
This is an incredibly serious investigation.
It's a big deal.
Speculation, probably not a good idea.
That being said, if that statement has made it to the FBI and somebody said that as part
of their sworn statement, yeah, I mean, that's keep you up at night bad.
That's a really, really, really bad statement, but we don't know that.
We know that somebody said that to the New York Times.
That's it.
So framing it as an admission, a confession, just pronouncing that they have him, I wouldn't
go that far.
I think right now is a good time to manage expectations, stay with the pace of the investigation
and what is publicly known and confirmed.
I mean those five words, that's really bad and it totally sounds like Trump.
I mean, it's not there.
It's just my, like, you can see him saying it,
but being able to see him say it and him actually saying it
and then it being testified to, that's a huge jump.
So while this is definitely one of those things that, if true,
Yeah, it's going to come up again at this point.
I would just wait.
As far as the possession aspect of it
and trying to see how the feds would frame it
as him having possession, I don't
think that they need statements like this to make that case.
I think they're well beyond that.
But if people in his circle are telling the New York Times this, just as part of an interview,
when the FBI starts interviewing them, they're definitely going to say things that are not
helpful to Trump in his circle. So five words that will probably haunt the
former president for quite some time until this case is resolved one way or
another. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}