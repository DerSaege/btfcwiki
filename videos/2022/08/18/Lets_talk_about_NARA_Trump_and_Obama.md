---
title: Let's talk about NARA, Trump, and Obama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p27qOHRjLDw) |
| Published | 2022/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the records and archives of former presidents Trump and Obama, focusing on the false claim about Obama keeping 30 million records.
- Trump falsely claims that Obama took 30 million pages of records from his administration and didn't digitize them, which outraged historians.
- The reality is that the National Archives and Records Administration (NARA) have exclusive custody of Obama's presidential records, both classified and unclassified.
- NARA moved approximately 30 million pages of unclassified records to a facility in Chicago and maintains classified records in Washington DC.
- Beau clarifies that Trump's statements about Obama's records are inaccurate and essentially made up.
- Despite knowing where the records are kept, Trump continues to mislead by implying Obama took them with him.
- The Presidential Records Act dictates that all presidential records belong to the US government and should be maintained by NARA.
- Trump's false claims are viewed as a deflection tactic since he knows his base won't fact-check him and are unlikely to challenge his statements.
- Beau stresses the importance of not arguing with librarians when it comes to records, as they know where everything is stored.
- This misinformation about Obama's records is just one of many attempts to divert attention from the actions of the Trump administration upon leaving office.

### Quotes

- "No, he made it up. It's not true. It is inaccurate."
- "If Trump had done the same thing, we wouldn't be having this conversation."
- "Never argue with a librarian."
- "That is such a short statement to be wrong 33 million times in."
- "This misinformation about Obama's records is just one of many attempts to divert attention."

### Oneliner

Beau clarifies the false claims about Obama's records, exposing them as deflection tactics, while underscoring the importance of not arguing with librarians regarding records.

### Audience

History Buffs

### On-the-ground actions from transcript

- Contact local libraries or archives to learn more about preserving and accessing historical records (suggested)
- Support organizations that advocate for transparency and accountability in government record-keeping (implied)

### Whats missing in summary

Deeper insights into the implications of spreading misinformation and the importance of holding leaders accountable for factual accuracy.

### Tags

#PresidentialRecords #Misinformation #NationalArchives #Accountability #Transparency


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about former presidents,
Trump and Obama and their records,
their archives and what has happened to one of them.
We're doing this because somebody sent me a message saying,
hey, can you put together a short video
talking about whether or not former president Obama
kept 30 million records from his administration.
For those who aren't following this particular thread
of Trump's many deflections,
let me read you a couple of statements
so you understand where it's coming from.
At the end of his presidency,
Barack Obama trucked 30 million pages
of his administration's records to Chicago,
promising to digitize them and eventually put them online,
a move that outraged historians.
More than five years after Obama's presidency ended,
the National Archives webpage reveals
that zero pages have been digitized and disclosed.
You know what's really weird
about this particular statement
is that he seems to know, Trump seems to know
that he should go to the National Archives webpage,
almost like that's who's supposed to have the records.
And since that's where he's checking,
it seems that he knows that's who does.
But he also seems to be leading people to believe
that Obama took his records with him when he left.
It's weird, it's conflicting in one statement.
Another one would be President Barack Hussein Obama,
you know, they're mad when they throw out that middle name,
kept 33 million pages of documents,
much of them classified.
How many of them pertain to nuclear?
Word is, lots.
Sure, okay, so what's supposed to happen
at the end of the presidency
is under the Presidential Records Act,
the National Archives is supposed
to basically take possession of everything.
That's how it's supposed to work
because those documents are still US government property.
If only the National Archives would put out a statement
explaining what happened.
Oh wait, they did.
The National Archives and Records Administration, NARA,
assumed exclusive legal and physical custody
of Obama presidential records
when President Barack Obama left office in 2017
in accordance with the Presidential Records Act.
NARA moved approximately 30 million pages
of unclassified records, unclassified records,
to a NARA facility in the Chicago area
where they are maintained exclusively by NARA.
Additionally, NARA maintains
the classified Obama presidential records
in a NARA facility in the Washington DC area.
As required by the PRI,
former President Obama has no control over where
and how NARA stores the presidential records
of his administration.
So short version, is anything that he said true?
Is anything Trump said true?
No, no, he made it up.
It's not true.
It is inaccurate.
Some might call it a lie,
especially given the fact that in one of the statements,
he makes it clear that it seems
that he understands where they're at.
So why'd he do it?
I mean, other than the obvious deflection,
because he knows that most of his base
isn't going to fact check him.
They are so far down into that information silo,
they are not going to try to refute dear leader.
They're gonna believe what they're told.
Just as a piece of advice for the future,
when it comes to records, never argue with a librarian.
Okay, they know where everything is.
This is just one of many stories
that have been thrown out in an attempt
to deflect from the Trump administration's actions
upon leaving office.
No, Obama didn't truck 30 million records or 33 million.
That is such a short statement to be wrong
33 million times in.
It didn't happen.
The National Archives maintains the records
as they're supposed to.
If Trump had done the same thing,
we wouldn't be having this conversation.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}