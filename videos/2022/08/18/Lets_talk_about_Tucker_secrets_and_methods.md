---
title: Let's talk about Tucker, secrets, and methods....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=B30XHQFx_00) |
| Published | 2022/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Tucker Carlson suggested that American people should know the contents of classified documents to determine if they are meaningful.
- He pointed out the absurdity of classification by mentioning the declassification of a disappearing ink formula from World War I in 2011.
- Beau explains that information can be classified not for its content, but for the method through which it was obtained.
- The example of the disappearing ink formula was classified to protect the method, not because the formula itself was significant.
- Classification is often about protecting methods that can lead to uncovering other secrets.
- The CIA argued against releasing information in 1998 because they still used the method mentioned in the declassified documents.
- Declassifying information can reveal methods that opposition intelligence can exploit, leading to the development of counterintelligence activities.
- Beau illustrates the importance of protecting methods using an example of a ciphered message that Tucker Carlson wouldn't be able to decipher without knowing the method.
- Overclassification in the U.S. is seen as a method to flood potential targets for opposition intelligence, not necessarily to trap individuals.
- Understanding what is classified is not just about the information itself but also about how that information was obtained.

### Quotes
- "Classified information is not about the content, but about how it was obtained."
- "Declassification could expose methods that protect other secrets."
- "Overclassification can flood potential targets for opposition intelligence."
- "Understanding classified information goes beyond the content to how it was acquired."
- "Protecting methods is key to safeguarding secrets."

### Oneliner
Beau explains that classified information is more about protecting methods than the content itself, illustrating the importance of understanding how information is obtained.

### Audience
Media consumers

### On-the-ground actions from transcript
- Analyze and question the intentions behind the classification of information (implied)
- Advocate for transparency in government classifications to ensure accountability and prevent unnecessary secrecy (implied)

### Whats missing in summary
The full transcript delves deeper into the nuances of classification and the importance of protecting methods in preserving secrets.

### Tags
#ClassifiedInformation #Transparency #GovernmentSecrecy #Methods #ProtectingSecrets


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Tucker Carlson
and secrets and methods.
Tucker Carlson did a bit on his show
in which he suggested that the American people
need to know exactly what was on the documents
so they could determine whether or not
what Trump had in his possession was really meaningful, whether or not it really should
be classified.
The problem with that is that most people don't know what needs to be classified.
And the proof of this is in Tucker's own bit.
As an example to show how ridiculous the classification system is, he talked about the fact that in
In 2011, the CIA declassified something that had been classified during World War I, and
it was the formula behind a disappearing ink.
What Tucker's trying to show here is that information can be classified for an extremely
long period of time, and there's no reason to have it classified.
That's the position he's trying to show.
understand for this to matter in Trump's case, he would have to have obsolete information.
But, more importantly, the overall theme there is that the disappearing ink,
it wasn't meaningful, so it should have been declassified long before then. Right?
He makes jokes about how it's available at a magic store. Yeah, but just fun fact, there's
actually a whole lot of stuff at magic stores that can be used in intelligence
work. But here's the most important part about whether or not Tucker would be
able to understand if something was meaningful. The example he chose to
illustrate how ridiculous our classification system is, is one that, well,
the reason it was classified was the method. We've talked about it on the
channel over and over again. Secrets aren't secret because of the
information. They're often secret because of how they were obtained, how the
information was obtained. They want to protect those methods. In this case, it's
it's a little bit different.
Sure, the actual formula for the disappearing ink,
that's not really that big of a deal,
but that's not why it was classified.
That's not why it remained secret for as long as it did.
It remained secret because it was still an approved method
of sending secret information.
So, in Tucker's example, he kind of demonstrated that even if he was personally handed the
documents to go through, he wouldn't know if they were meaningful.
It was secret because it showed a method.
that secret, protected the method, which protected other secrets.
And if there had been any research done by the writers of this little bit, they would
have known that in 1998, there was a Freedom of Information Act request about this information,
and the CIA argued against it.
And sometime between 1998 and probably 2004, they flat out said, the reason we can't release
this is because we still use it. By declassifying that information, it's an
admission that that method is still in play, which allows opposition
intelligence to develop counterintelligence activities and protocols
to disrupt it. That's why it was kept secret for so long. It was declassified
when a new technology came along to replace it. A good example of this and
how this works is over my shoulder. There's a jumble of letters, right? That's
actually a message. It's a message. It's a text that has been run through a
cipher. You can't read it. Tucker can't read this, right? Even if I was to say in
In order to understand that message, you need some Mark Twain books.
You still can't read it.
People who understand ciphers would know at that point, well, it's probably a book cipher.
But since they don't know the method, and they don't know whether or not a substitution
cipher was overlaid on top of it, it was, they won't be able to read it.
They won't be able to crack it.
Tucker can't read this.
So the method, the series of ciphers that was used to render that text, that method
has to say secret.
It has to stay secret, otherwise whatever that message is could be read.
The book cipher, incidentally, when you're talking about the age of tradecraft and whether
or not old stuff is any good, I want to say the book cipher was kind of first envisioned
about 200 years before the United States was founded, and it's still very viable as long
as you protect the method.
That's why things stay secret.
The only thing in Tucker's entire bit that was true is that the U.S. has a habit of overclassifying
stuff, of saying stuff is secret when it doesn't need to be.
But that's saying it's secret, not that it's compartmented.
When it comes to TSSEI documents, they're a big deal because they're protecting methods.
They're protecting how things are done, which protects all the other secrets.
Incidentally, the habit of overclassifying in the United States, some believe that that
in and of itself is a method.
If you classify a lot of stuff that doesn't necessarily need to be classified, it's not
to create a trap for a former president who's taking stuff he shouldn't be.
It's to present a field of targets that is very, very flooded.
So opposition intelligence might spend a whole lot of time trying to get access to something
that they could literally read on Wikipedia.
That's also a method of preserving secrets.
Some have suggested.
Tucker's bit was bad.
It showcased exactly why there is a misunderstanding when it comes to the public and what is considered
secret, top secret, or compartmented.
It's not necessarily about the information on the paper.
about how that was obtained. Think about it, one additional example. Let's say we
find out that Putin paid for his girlfriend to go to Milan, right? That's
not a secret. That doesn't even matter. That's not a big deal. But what if we
found that out from his secretary who handles all of his travel arrangements?
Oh, well then we have to make sure that that information doesn't get out.
We can't let anybody know that we know that, because it would compromise the method the
information came in, the way that information showed up.
That's the secret.
It says Tucker can't read this.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}