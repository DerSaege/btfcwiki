---
title: Let's talk about Republican money troubles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EBdgmLajz70) |
| Published | 2022/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The National Republican Senatorial Committee in Arizona, Pennsylvania, and Wisconsin canceled about ten million dollars worth of advertisement buys, impacting Republican candidates in those states like Dr. Oz, Ron Johnson, and Blake Masters.
- Dr. Oz and Blake Masters are down in the polls by about ten points.
- It appears that winning a Republican primary without Trump is difficult, but winning a general election with him is also challenging.
- Ron Johnson seems to be doing okay in the polls currently.
- The committee's decision to cancel ad buys is attributed to a lack of funds, as small donor donations have dried up within the Republican Party.
- Most of the money raised on the right wing seems to be going to Trump rather than being shared with other candidates.
- Without Trump contributing financially, these already struggling candidates are likely to fall further behind in the polls.
- This financial setback is detrimental to the Republican Party's goal of retaking the Senate.
- The lack of available funds will especially impact tight races where cash could make a significant difference.
- People directing their donations towards Trump rather than the Republican Party is contributing to the financial strain on other candidates.
- The situation suggests that similar scenarios may unfold in future races, with the party needing to prioritize candidates they believe can win and minimize losses.
- The hope for a "red wave" in the Republican Party seems to be dwindling, especially when candidates are significantly trailing in the polls without support from the National Republican Senatorial Committee.
- The Committee's lack of assistance to struggling candidates does not bode well for the anticipated "red wave."
- The overall situation indicates a challenging road ahead for Republican candidates in various races.

### Quotes

- "They don't have the money because small donor donations have dried up."
- "The problem is it's all going to Trump and he's not sharing."
- "You know, there was a big hope in the Republican Party for a red wave, but at this point when you have candidates ten points down and the National Republican Senatorial Committee is just kind of like, yeah, whatever, we're not going to help you."

### Oneliner

The National Republican Senatorial Committee's cancellation of ad buys due to financial constraints signals trouble for struggling Republican candidates in key states.

### Audience

Political activists and donors.

### On-the-ground actions from transcript

- Support struggling Republican candidates financially (suggested).
- Prioritize donations towards campaigns of candidates who need assistance (suggested).

### Whats missing in summary

Insight into the potential implications of this financial setback on the upcoming elections and the broader political landscape. 

### Tags

#RepublicanParty #CampaignFunding #ElectionStrategy #PoliticalDonations #GOP


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the National Republican Senatorial Committee
in Arizona, Pennsylvania, and Wisconsin
and a really bad sign
for the Republican Party,
particularly those running for Senate.
The committee decided to cancel
about ten million dollars worth of advertisement buys.
They're not going to make them.
Going to impact those three states, Arizona, Pennsylvania, and Wisconsin.
They won't be buying ads in support
of the Republican candidate
in that state.
Those candidates would be
Dr. Oz,
Ron Johnson, and Blake Masters.
Now Oz and Masters are both down in the polls like ten points.
It's almost like you can't win
a Republican primary
without Trump,
but you can't win a general with him.
Ron Johnson seems to be at the moment
doing okay in the polls, so that was a little surprising.
I mean ten million dollars, that's a lot to not spend,
so you have to ask why they're doing it.
Why have they decided to abandon
these
candidates?
And the simple answer is that they don't have the money.
That's conventional wisdom.
The Republican Party
doesn't have the money,
so they have to cancel
advertisement buys.
They don't have the money because small donor donations have dried up.
And that doesn't make any sense because we hear about all this money
that the right wing is taking in.
The problem is it's all going to Trump and he's not sharing.
So
unless Trump decides to
fork out ten mil
to help out,
these candidates,
they're already
behind in the polls for the most part
and they will probably slip further.
This is
not great for a Republican Party that was hoping to retake the Senate.
And in a lot of tight races,
that cash is going to pay off.
That cash that apparently
they don't have
because rather than supporting the Republican Party,
people sent their $45 to Trump to do
whatever with.
I'm going to guess
that we're going to see similar things play out in the future
in other races
where
the Republican Party is going to have to prioritize
who they think can win
and
try to limit
their losses.
You know, there was a big hope in the Republican Party for a red wave,
but at this point when you have candidates
ten points down
and the National Republican Senatorial Committee is just kind of like, yeah,
whatever, we're not going to help you.
That's
not a great sign
for
a red wave.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}