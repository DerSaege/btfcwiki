---
title: Let's talk about secrets and plea deals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Mrbt_h3ixfM) |
| Published | 2022/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Jonathan Tobey and his wife Diana are alleged to have attempted to sell U.S. state secrets to a foreign power.
- Jonathan, a naval engineer, smuggled secrets out in various ways, including using peanut butter as a cover.
- They passed the secrets to undercover FBI agents through dead drops, avoiding face-to-face meetings.
- No secrets were actually compromised, but the couple attempted to enter a plea deal which was rejected by the judge.
- The judge deemed the proposed 12.5 to 17.5 years sentence inadequate and demanded more time.
- Diana, alleged to have acted as a lookout, was set to receive three years in the rejected plea deal.
- The couple withdrew their guilty plea and are headed for trial next year unless a new deal is reached.
- The classified information passed on was not top secret or compartmented, but still serious enough to warrant more than 12.5 to 17.5 years.
- The judge views this crime, which exposes national security, as deserving of a punishment exceeding the initial plea deal.
- It's uncertain if a new deal is being negotiated, but this case is likely to attract significant attention due to its national security implications.

### Quotes

- "No secrets were actually compromised."
- "The judge rejected it, said that wasn't enough."
- "12 and a half to 17 and a half years was not enough."
- "Something that exposes national security is worthy of more than 12 and a half years."
- "It's just a thought, y'all have a good day."

### Oneliner

Jonathan and Diana's attempt to sell state secrets reveals gaps in sentencing for national security breaches.

### Audience

Legal observers

### On-the-ground actions from transcript

- Follow the developments of the trial and any potential new plea deals (implied)
- Stay informed about cases involving national security breaches (implied)

### Whats missing in summary

Insights on the potential implications of lenient sentencing for national security breaches.

### Tags

#NationalSecurity #SpyGames #PleaDeal #StateSecrets #LegalSystem


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about spy games,
and selling secrets, and peanut butter, and plea deals,
and things not going according to plan sometimes.
Jonathan Tobey and his wife Diana are alleged,
all of their activities in this video
alleged to have attempted to sell state secrets, U.S. secrets, to a foreign power.
Now, they were actually passing these secrets on to undercover FBI agents.
Jonathan was a naval engineer. He smuggled the secrets out in various ways. This is the
peanut butter sandwich person for those in the know. And then they would pass
them on to the people they believed were foreign power through dead drops. Dead
drops are it's a means of transferring information where you don't have to meet
face-to-face. It could be a stump in the woods, underside of a park bench, hollowed
out Coke can, a whole bunch of stuff like you would find in like a magic shop.
That'll be funny in a later video. But as far as we know, no secrets were actually
compromised. They attempted to enter a plea deal. He would receive somewhere
between 12 and a half years to 17 and a half years and she would get three, she
is alleged to have acted as his lookout. The judge rejected it, said that wasn't
enough. They needed more time. The couple has withdrawn their guilty plea and they
will be going to trial sometime next year unless some other deal is reached.
12 and a half to 17 and a half years was not enough. It is important to note that
he's accused of passing on classified and confidential information. Not secret,
not top secret, and certainly not compartmented information. 12 and a half
to 17 and a half years is not enough. Man, if you had anything to do with those
boxes, that's probably a really terrifying story. Now, there is the active
attempt to pass it on to a foreign power, but it is also much lower importance
when it comes to the information itself. I'm not sure whether or not a second deal
of some kind is in the works, but this is a story that will probably gain a lot of
attention for that fact there at the end, where it's very clear that at
At least this judge looks at this particular type of crime, something that exposes national
security, as something worthy of, well, at least we know more than 12 and a half years.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}