---
title: Let's talk about a weird story out of Michigan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GpUfHewqNFo) |
| Published | 2022/08/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dana Nessel, the Democratic attorney general of Michigan, requested a special prosecutor to look into a Republican candidate, Matthew DiPerno, who is the presumptive nominee for attorney general. 
- The investigation by the Michigan State Police into breaching voting machines led to DiPerno, Nessel's political opponent.
- The allegations against DiPerno are not proven yet but are outlined in the request for a special prosecutor to limit political appearance.
- DiPerno, a Trump-backed candidate, denies the allegations as a political witch hunt, claiming they are untrue.
- Despite accusations of political shenanigans, the timing seems off for this to be a strategic political move, as DiPerno hasn't formally secured the nomination.
- The story is in its early stages and expected to become national news due to its ties to elections, Trump, and conspiracy theories.
- Beau advises waiting for the appointment of a special prosecutor and their findings before drawing conclusions.
- The saga is anticipated to attract attention from media and conspiracy theorists as it unfolds.
- Beau suggests refraining from speculating until more information is available due to the potential sensationalism surrounding the case.
- Given the high-profile nature of the story, Beau predicts widespread media coverage in the coming days.

### Quotes

- "This is something that they'll be able to seize on, and you will see a lot of people make wild accusations."
- "Given the tie to elections, the tie to Trump, and everything else that's going on, expect to see this story all over the news."

### Oneliner

Dana Nessel requests a special prosecutor to look into her political opponent, Matthew DiPerno, sparking national interest and wild accusations.

### Audience

Michigan residents, political enthusiasts

### On-the-ground actions from transcript

- Stay informed about the developments in the investigation and the appointment of a special prosecutor (implied)
- Avoid spreading or engaging with unverified information until official statements are released (implied)

### Whats missing in summary

The full transcript provides detailed insights into the unfolding political controversy in Michigan and the potential implications of the investigation.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about some weird news
out of Michigan concerning the attorney general's race
up there.
OK, so to lay out the story, Dana Nessel
is the current attorney general.
She is a member of the Democratic Party.
The Michigan State Police was conducting an investigation
into basically an endeavor to breach voting machines.
According to the attorney general's request
for a special prosecutor, this investigation
led to Matthew DiPerno, a member of the Republican Party
and the presumptive nominee for attorney general.
So this investigation apparently led to her political opponent.
Now, I don't even know if you want
to call these allegations yet because they are coming out
in a weird way.
They're coming out in the form of a request
from the attorney general's office to this outside agency
within the state to appoint a special prosecutor
to look into all of this stuff.
The idea is to limit the appearance of it
being political.
And it says, when this investigation began,
there was not a conflict of interest.
However, during the course of the investigation,
facts were developed that DiPerno
was one of the prime instigators of the conspiracy.
That's in the request.
So that's not something that's actually been proven,
but this is what's motivating the request
for the special prosecutor.
If you look through it, it says that DiPerno
was at a hotel room where machines were like physically,
pieces of the machines, the tabulators,
were being breached.
Again, it's not something that's been presented in court yet,
but this filing is definitely of note.
DiPerno is a Trump-backed candidate,
given the history when it comes to election claims.
It's something that is going to be looked into.
Now, to be clear, DiPerno is like, yeah,
this is a political witch hunt.
And there are a lot of people who
are alleging that this is just political shenanigans
all the way around, that nothing contained in the request
is actually true.
I mean, it's politics, and so you can't really
just discount that and say, no, it's not that.
At the same time, if this was political shenanigans,
if this was a political play of some kind, it's a bad one.
It's not actually a good move, because DiPerno doesn't formally
have the nomination yet.
It would seem to me that if you were
going to use the power of the attorney general's office
to hit your opponent in this way,
you would do it after the nomination was final,
rather than providing almost a month
to make adjustments if the Republican Party so wanted.
So I don't know.
What I do know, this is really early in this saga.
This is going to be national news.
As this story develops, it will be a big thing,
not just with the media, but with those
who enjoy certain kinds of theories.
This is something that they'll be able to seize on,
and you will see a lot of people make wild accusations.
My suggestion, as the story plays out,
wait to find out whether or not a special prosecutor is
actually appointed and what that special prosecutor says.
Until then, I would just wait and see.
I wouldn't feed too much into this
until we actually have more to go on.
But given the tie to elections, the tie to Trump,
and everything else that's going on,
expect to see this story all over the news.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}