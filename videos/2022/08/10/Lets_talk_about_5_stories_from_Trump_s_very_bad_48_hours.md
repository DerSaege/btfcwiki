---
title: Let's talk about 5 stories from Trump's very bad 48 hours....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KErWWzsGSCE) |
| Published | 2022/08/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Breaking news prompted a departure from the normal video format to cover multiple topics related to Trump's troubled 48 hours.
- Rudy Giuliani faced a Georgia grand jury after trying to avoid it, claiming he couldn't fly and being told to take a train or an Uber.
- Trump is reportedly seeking a top criminal defense attorney as legal troubles intensify.
- Republican congressperson Scott Perry had their phone taken by the feds, potentially linked to investigations involving Jeffrey Clark and John Eastman.
- The House Ways and Means Committee can access Trump's tax returns after a 3-0 appellate court ruling in their favor.
- Trump may testify in the New York civil case involving the Trump Organization, with implications for his legal team's strategies.
- Testimonies from various locations must match, raising the stakes for those close to Trump who may have been inconsistent.
- The converging legal challenges suggest a tightening circle around Trump and his associates, with ongoing developments expected.

### Quotes

- "Trump might have issues finding attorneys that want to take on this task for a whole bunch of reasons."
- "It's been described as Apprentice, get out of federal prison edition."
- "Given some of their habits of being economical with the truth and kind of improvising a little bit when they're telling stories, that could be really bad for them."

### Oneliner

Breaking down Trump's turbulent 48 hours: Giuliani faces a Georgia grand jury, a frantic search for top attorneys, testimonial consistency concerns, and legal circles tightening around Trump.

### Audience

Political analysts, news followers

### On-the-ground actions from transcript

- Follow ongoing developments closely, especially related to legal proceedings and investigations (implied)
- Stay informed about the evolving situation and its implications for the political landscape (implied)

### Whats missing in summary

Insights into the potential long-term consequences of the legal challenges and testimonial inconsistencies for Trump and his inner circle.

### Tags

#Trump #LegalChallenges #Giuliani #PoliticalAnalysis #Testimony


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to do things a little bit
differently.
Normal format is one video, one topic.
News is breaking very, very quickly.
And I already have half a dozen or more videos in backlog
waiting to be published because they
keep getting bumped back.
So what we're going to do today is
hit a whole bunch of different topics
at once, all related to Trump's very, very bad 48 hours.
I mean, obviously, there's the whole search warrant thing,
which we've talked about, and it's being discussed everywhere.
On top of that, Rudy Giuliani was
attempting to get out of talking to the grand jury in Georgia
about possible election interference down there.
He kind of had a doctor's note saying that he couldn't fly.
The end result was him being told, well,
take a train or an Uber.
He was ordered to make the appointment.
There may be follow-up appeals on this,
but as it stands right now, Giuliani
is going to have to go to Georgia
and talk to the grand jury down there.
Rolling Stone, which has had a lot of good reporting lately,
is suggesting that Trump is in a frantic search
to find a really good criminal defense attorney, which
is probably a good idea.
The general tone is that Trump is
looking for just pit bull criminal defense attorneys
to try to get himself out of the circle that
seems to be closing around him.
It's been described as Apprentice,
get out of federal prison edition.
Trump might have issues finding attorneys
that want to take on this task for a whole bunch of reasons.
Scott Perry, a Republican congressperson,
had their phone taken by the feds.
Now, the interesting thing about this
is that apparently this was done at least in part
by the Justice Department Office of Inspector General.
That's weird, because the OIG investigates Justice Department
employees.
Perry never worked for the Justice Department,
which means it's relatively safe to assume
that this is somehow connected to Jeffrey
Clark, the investigation there, maybe even John Eastman,
because Clark also had their phone taken
and did work for the Justice Department.
That had to do with the idea, the way it's generally
being perceived, is that has to do with Trump trying
to encourage Clark to push the claims about the election
and maybe help interfere with the certification.
Now, no charges have been filed in this.
So everything is an allegation at this point.
And in some cases, there aren't even allegations.
But the phones are being taken.
And it's the OIG that's involved in it.
That leads me to believe that that's
going to be an ongoing story that probably
has a lot more to develop.
The House Ways and Means Committee
will be able to access Trump's tax returns.
Trump has tried to block this.
They went to the appellate court.
The appellate court ruled 3 to 0
that the Ways and Means Commission does actually
have a legitimate reason to access those records
and obtain them from the IRS and take a look at them.
My guess is that Trump is going to appeal this.
And he will try to fight it even further.
On top of all of that, it looks like Trump
might be testifying in the New York civil case,
the civil investigation into Trump organization.
We'll see how much his attorneys actually allow him to talk.
But it appears that he's willing to do that.
One of the things that's important to note
when it comes to a lot of these people giving testimony
in different locations about different things
is that that testimony has to match.
They can't go to Georgia and say one thing
and then say something else in D.C.
in front of a federal grand jury or something like that.
Given some of their habits of being economical
with the truth and kind of improvising a little bit
when they're telling stories, that
could be really bad for them.
It may end up leading to an investigation of its own
at some point.
All of this, yeah, it shows a tightening circle
around Trump and those who are very close to Trump.
Any of these stories is likely to develop and become
a little bit more important as time goes on.
Right now, we're just getting a bunch of information
coming from a whole bunch of different sources that's all
coming in.
And yes, each little bit is being
treated as a huge development.
Not all of them are.
But it's important to have this stuff on your radar
as you try to piece together what's happening.
Because as the Justice Department moves forward,
my guess is that the speed at which things develop
will only increase.
So if you don't have a good base to work from when you're
talking about all of the cast of characters involved in this,
it might get really confusing later on.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}