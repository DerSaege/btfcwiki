---
title: Let's talk about Trump, the DOJ response, and photos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ODEsDBxgML0) |
| Published | 2022/08/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump requested a special master to review documents, DOJ responded that Trump didn't have standing because the documents belong to the US government.
- DOJ detailed attempts to quietly retrieve the documents, contrasting Trump team's claims of returning everything with the large number of documents found by feds.
- The DOJ's response can be summarized as "Kick Rocks Kid," conveying a message to sit down and be quiet.
- Some of the documents were found in Trump's office, leading Beau to believe their presence was intentional rather than accidental.
- Beau expresses patience for further investigation despite calls for immediate action, as the situation may require more scrutiny.
- Speculation arises about the potential espionage implications of the documents found.
- Beau criticizes the Republican Party's response, focusing on framed Time magazines in the photo rather than the seriousness of the document situation.
- Even if a special master is appointed, it only delays the case for Trump without offering significant help.
- Intelligence community conducting a damage assessment indicates a prolonged process.
- The documents found may involve human sources, suggesting a sensitive nature that warrants careful handling.

### Quotes

- "Kick Rocks Kid."
- "Given what we have now publicly seen, it seems as though they may have to investigate more."
- "I understand those people who saw that photo, saw those documents all over the ground, and immediately wanted to see cuffs."
- "If you can come up with a scenario in which those documents got there by accident in the shape that they were in, I would love to hear it."
- "It's just a thought, y'all have a good day."

### Oneliner

Trump's request for document scrutiny met with DOJ response; documents found in office prompt suspicion, investigation ongoing.

### Audience

Legal analysts

### On-the-ground actions from transcript

- Contact legal experts for insight on the implications of the document situation (suggested).
- Stay informed on the developments and investigations regarding the documents (suggested).
- Await further updates on the case before drawing conclusions (implied).

### Whats missing in summary

Insights on potential legal repercussions and the importance of thorough investigation.

### Tags

#Trump #DOJ #investigation #documents #RepublicanParty


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump,
the Department of Justice, and their response
to Trump's request for a special master
to review the documents.
The judge said that the response had to be in by yesterday,
and it was dropped shortly before midnight.
So DOJ's response, it basically said that Trump didn't have standing because the documents
that he's talking about don't belong to him, they belong to the US government.
It detailed the attempts to get them back quietly and without searching the place.
It detailed the fact that it had been represented as though Trump team had engaged in a diligent
search and had returned everything.
And it contrasted that with the amount of documents that the feds found in just a couple
hours.
The short version in English of this document is Kick Rocks Kid.
That's really what it says.
It's basically, sit down, be quiet, and we're doing our thing.
One of the things that was released along with this was a photo.
In my mind, when I was picturing these documents, I just assumed that the outer cover was a
there anymore. And I wrongly assumed that. These documents were still very clearly marked, there
was no, there's no way to confuse what they are. If you've never seen one, it has like a bright border.
I mean there's no way to mistake this for anything else. According to the feds, some of the documents
were found in Trump's office.
With that in mind, I personally can no longer think of any scenario in which this wasn't
intentional.
I really can't.
Even a cursory skim through boxes, you'd see them.
I cannot envision a scenario now in which this was actually an accident or an oversight
or just mishandling of the information.
So I know that a lot of people had that reaction when they saw the photos.
Most of them are suggesting, you know, arrest him now.
I understand that reaction.
At the same time, I'm going to be more patient because of the photo.
Given what we have now publicly seen, it seems as though they may have to investigate more.
Because there isn't a ready explanation as to how this could have been an accident, they
They have to assume it was intentional, so they have to investigate and try to figure
out what was happening, and that's going to take time.
For all we know, we're going to discover that his little club down there was the largest
dead drop in espionage history.
The response from the Republican Party to this was surprising.
In the photo, where all of the documents are laid out all over the floor, like in a pentagram,
like they were going to summon the ghost of Bill Donovan himself, there's a box that
had some framed time magazines in the photo, and that is what they chose to
comment on. Either they don't understand the significance or they
are intentionally misleading people and acting as though something that is a
very big deal is not. I don't know how the judge is gonna rule on this. Again, I
would point out even if a special master was appointed, it doesn't actually help
Trump in regards to the overall case, it just delays it. But given what we've seen
and now the knowledge that the intelligence community is doing a
damage assessment, it's probably going to be a bit anyway. And the reporting that
suggested some of the documents had to do with human sources, some of the
covers made it very clear that yes, that's what it was.
So we're just going to have to wait now.
I understand those people who saw that photo, saw those
documents all over the ground, and immediately
wanted to see cuffs.
I understand it.
At the same time, if you can come up with a scenario in
which those documents got there by accident in the shape that they were in, I would love
to hear it.
I can't think of one, which means maybe they can't, and if they can't, they have to try
to discern what was happening.
And that may take a little bit of time.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}