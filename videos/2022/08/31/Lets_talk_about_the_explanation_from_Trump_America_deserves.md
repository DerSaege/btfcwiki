---
title: Let's talk about the explanation from Trump America deserves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eDyqvJlOOYM) |
| Published | 2022/08/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump and his team have provided a litany of excuses but no explanations for the retention of sensitive documents.
- Even if the documents were inadvertently transferred to Trump's club, there's been no effort to return them.
- The lack of explanation raises questions about putting America first and protecting sensitive national security documents.
- The focus is on why these documents were retained despite knowing they posed a risk to national security.
- The absence of a clear reason for keeping the documents is what the American people deserve to hear.
- The issue isn't just about criminal liability but about providing a straightforward answer to the public.
- Supporters of Trump may also be curious about the purpose behind keeping these documents and prioritizing golf over national security.
- The core question revolves around why Trump couldn't take the time to protect American lives by returning the sensitive documents.
- There's a call for transparency and accountability in handling these confidential materials.
- The lack of a genuine explanation remains a critical point that needs to be addressed.

### Quotes

- "The American people want to hear and deserve an explanation."
- "Why couldn't he take the time to protect American lives? Was golf really that important?"
- "We're not even talking about criminal liability at this point."

### Oneliner

Trump's team offers excuses, not explanations, for retaining sensitive documents, leaving unanswered questions about national security and accountability.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact elected representatives to demand transparency on the handling of sensitive documents (suggested)
- Join advocacy groups pushing for accountability and clarity in government actions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the lack of accountability in handling sensitive documents, urging for transparency and answers from those in power.

### Tags

#Trump #NationalSecurity #Accountability #Transparency #AmericanPeople


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about explanations, reasons,
excuses, documents, Trump, and getting to the point.
Since this whole thing started, the Trump world,
Trump, his team, his proxies, they
have offered the American people excuse after excuse after excuse. Well they were
declassified knowing that I mean the law says that isn't actually a defining
characteristic but they've provided just a litany of excuses. You know what I
haven't heard? An explanation. A single explanation for the apparent continued
retention of those documents. That's what we haven't heard. Let's say that these
documents were inadvertently transferred down to his little club. I know there's
a whole lot of people who don't believe that but let's just say it's true for a
second. The feds say, hey you have some documents that pose a grave risk to
national security and you are somebody who is an American citizen, somebody who
calls themselves a patriot, a nationalist even. Why are those documents still there?
We're talking about, what, 20 boxes?
How long does it take to go through that one person, a day, maybe two, when you're looking
for stuff that is clearly stamped?
I guess he couldn't get off the golf course for a day or two to help protect some of the
most sensitive documents the United States has.
There's been no explanation put forth for that.
And I think that's what the American people want to hear and deserve.
If you run around talking about putting America first no matter what, and you can't find
the time, you can't break free from the golf course for a couple of days to protect some
of this nation's most secretive documents to just flip through a few boxes and return
those so they don't jeopardize American means, methods, and assets lives?
I have questions.
At the end of this, that's what it's going to hinge on.
reasoning for the apparent continued retention of those documents all of that
time with the National Archives and the feds trying to get them back. When you
have somebody from the National Security Division show up and say hey we think
you have some stuff, a patriot, a nationalist, it seems like their first
response would be, well let's go find them right then and there. Take this
stuff, get it out of here, make sure it's secure. But apparently that's not what
happened. And the eternal question is why? That's the question that isn't going
to be answered by any excuse, by any search for a loophole or technicality.
and that's the crux of it. We're not even talking about criminal liability at
this point. We're talking about an answer that the American people deserve and
especially his followers, his supporters probably would like an explanation for
that. Those who haven't just signed away all of their free thinking, they probably
want to know why those documents were there. What purpose did he have for them?
Why he couldn't take the time to protect American lives? Was golf really that
important? That's the question. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}