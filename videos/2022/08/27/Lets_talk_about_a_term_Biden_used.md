---
title: Let's talk about a term Biden used....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2mCee-jXmzY) |
| Published | 2022/08/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau revisits Biden's recent speech and the term "semi-fascism" used.
- A girlfriend introduced Beau's channel to her boyfriend during the Russia-Ukraine conflict.
- The boyfriend was curious about Biden using the term "semi-fascism."
- Beau explains that the term can be misunderstood due to people associating it with a specific subset from World War II Germany.
- He mentions that not all fascists fit that specific image.
- Beau references Lawrence Britt and Umberto Eco for discussing the characteristics of fascism.
- He suggests watching his videos on Trump's time in office to understand the term better.
- Beau believes that "semi-fascism" accurately describes Trumpism as an Americanized version of fascism.
- He points out that Biden's use of the term might bother some people due to his usual mellow rhetoric.
- Beau expresses the importance of calling things what they are to understand the situation better.

### Quotes

- "I don't know if it's a good idea politically, because it may be too much coming from Biden."
- "When it comes to stuff like that, I think it's important for people to understand what they're dealing with."
- "As far as the characteristics, the general philosophy, the undertones, the structure, goals. Yeah, I mean, it's an accurate term."
- "So anyway, it's just a thought, y'all have a good day."

### Oneliner

Beau questions the appropriateness of Biden using the term "semi-fascism," urging people to understand the situation for what it is.

### Audience

Political analysts

### On-the-ground actions from transcript

- Watch videos discussing the characteristics of fascism for better understanding (suggested)
- Engage in political discourse to understand terminology and positions better (implied)

### Whats missing in summary

Deeper insights into the implications of political rhetoric and terminology can be gained by watching the full video.

### Tags

#Biden #SemiFascism #Trumpism #PoliticalRhetoric #Understanding


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Biden's recent speech
and a term he used and whether or not
that term was appropriate.
And we're also going to talk about a girlfriend
setting up her boyfriend.
Because I was sent this message and I find it very humorous,
just going to read the whole thing. I was introduced to your channel by my girlfriend
when Russia invaded Ukraine. When I asked her about this, she told me to send you a message
and ask you. I'm totally aware that I'm being set up right now, so please be gentle. I'm wondering
what you think of Biden using the term semi-fascism in his speech. Is that appropriate, or is it
political positioning. Okay, so one of the problems in the United States is that
when that term is used, people picture a very specific subset of them that
wear armbands, right? Germany, World War II, that crew. That's who they picture. Now
don't get me wrong those people yeah they're fascist but not all fascists are
them they're not interchangeable so that's one starting point and something
to kind of mull over now that ideology has characteristics and there are two
people that have talked about this that I think are really good one is Lawrence
Brit and the other is Umberto Eco. Now I have videos detailing using both lists.
The thing is you can find them searching my name in either the number 14 or the
word characteristics because those those things normally ended up in the title
And there's a bunch of them from Trump's time in office because, yeah, it's appropriate.
The only thing that isn't appropriate about the use of that term is the addition of semi.
That's what Trumpism is.
It's just an Americanized version of it.
So it's not inaccurate.
It is political positioning, but it's not inaccurate political positioning.
If you're really just kind of wanting to get a view of it, I would probably suggest you
start with a video titled, Let's Talk About Trump's Accomplishments.
And it goes through a list of six things that Trump supporters are going to be like, yes,
That's my guy."
And then one thing that they'll be like, yeah, that's not so great, but yeah, he definitely
does that.
Then six things that they're like, yes, that's my guy.
And then one more thing that they would begrudgingly admit, 14 items.
Those 14 items are the 14 characteristics of fascism.
I don't know if it's a good idea politically, because it may be too much coming from Biden.
Because Biden has been known to be very, very mellow in his rhetoric.
accurate or not, throwing out that term is going to bother people. I, myself, would
like to see things called what they are. When it comes to
stuff like that, I think it's important for people to understand what they're
dealing with. I don't know if that's true of the rest of the United States. So
So whether or not it's a good idea, that's up in the air.
Because when you're talking about how the average voter is going to respond to that
statement, I don't know.
I don't know.
But as far as the characteristics, the general philosophy, the undertones, the structure,
goals. Yeah, I mean, it's an accurate term, as disturbing as it might be. So anyway, it's
Just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}