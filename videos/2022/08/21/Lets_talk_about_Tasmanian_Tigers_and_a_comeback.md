---
title: Let's talk about Tasmanian Tigers and a comeback....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LNHmUyV74nQ) |
| Published | 2022/08/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing Tasmanian Park, a scientific endeavor to bring back the extinct animal, the Tasmanian tiger, mylocene.
- The project aims to extract DNA from skeletal remains, although they won't have the full genome.
- Some compare this project to the controversial woolly mammoth resurrection project.
- Critics argue that de-extinction is a scientific fairy tale and a waste of money.
- Beau acknowledges the high costs of these projects and suggests the money could be better used to prevent current biodiversity loss.
- Despite potential commercialization and ethical concerns, Beau sees value in sparking interest and curiosity in extinct animals and biodiversity.
- He believes the project could lead to valuable research even if unsuccessful in bringing back the Tasmanian tiger.
- Beau doesn't see significant harm in the project and believes it could potentially aid in biodiversity restoration.
- He acknowledges the ethical question of the animal's health and uncertainties surrounding the project but ultimately sees no harm in attempting it.
- Beau concludes by sharing his perspective that the project may have positive outcomes and encourages viewers to ponder the implications.

### Quotes

- "Everything's impossible until it's not."
- "Interest in the idea of extinct animals."
- "It's out there. It's weird. It's good."
- "I don't see the objection to trying."
- "Maybe if we can get a handle on things at some point, we can help restore biodiversity through this method."

### Oneliner

Beau introduces a project to resurrect the Tasmanian tiger, sparking controversy over de-extinction and raising questions about biodiversity preservation while seeing potential value in the endeavor.

### Audience

Science enthusiasts, conservationists

### On-the-ground actions from transcript

- Support initiatives focused on biodiversity preservation and preventing animal extinction (implied)
- Stay informed about scientific advancements in conservation efforts (implied)

### Whats missing in summary

The full transcript provides a nuanced exploration of the ethical and practical considerations surrounding de-extinction projects like resurrecting the Tasmanian tiger, encouraging viewers to think critically about the implications and potential benefits of such endeavors.

### Tags

#Deextinction #TasmanianTiger #Biodiversity #Ethics #Conservation


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to welcome you to Tasmanian Park.
I feel like I've seen this movie before.
We're going to talk about a scientific endeavor that aims to bring back an extinct animal,
the Tasmanian tiger, mylocene.
Now this animal went extinct in the mid 1930s, I think, and they want to bring it back.
It's generated a lot of controversy.
There's a similar project dealing with a woolly mammoth, but there are people asking a lot
of questions.
One thing that everybody wants known is that this won't exactly be a thylacine.
The idea is to extract the DNA from skeletal remains and they won't have the full genome.
So I really feel like there should be a little dancing DNA cartoon with a funny voice talking
over my shoulder.
Let's just hope they don't fill in any gaps in the sequence with frog DNA.
For a lot of people, a lot of people who are raising objections, they're saying that this
concept of de-extinction is a scientific fairy tale, science fiction.
It's not real.
I mean it is science fiction, but so were cell phones or going to the moon.
Everything's impossible until it's not.
The idea that it hasn't been done before isn't exactly a good argument against it in and
of itself.
Another is that it's going to take tens of millions of dollars to do this.
These projects are going to cost a lot of money and there are people who feel that money
could be better spent, I don't know, trying to stop biodiversity loss today.
And I get that.
That at least makes sense to me.
At the same time, I have to feel like a project like this could spark a lot of interest in
this topic.
Interest in the idea of extinct animals.
It may pique the curiosity of people who have no idea what biodiversity is, much less the
fact that we're losing it at an incredibly alarming rate.
I'm sure that this is going to become commercialized and it will be yet another scientific endeavor
gone awry at some point, taken over by the machinery of money.
But I can't bring myself to say that it would be all bad.
I mean, assuming that things don't go horribly wrong and the Tasmanian tiger take over the
entire planet.
When you're talking about this kind of science that is, for lack of a better word, kind of
on the fringe, if it works, it works.
If it doesn't, it will probably lead to research that could be very useful along the way.
When it comes to things like this, it's out there.
It's weird.
It's good.
I don't really see the harm in this, necessarily.
I don't see it in the way some people are picturing it, as though it absolutely will
pique curiosity and drive people to make sure that no more animals become extinct.
I don't see that either.
But I don't see how it could really hurt if they are able to pull it off.
I mean, aside from an ethical question of whether or not the animal is going to be healthy,
because they're kind of guessing here.
Aside from that, I don't see the objection to trying.
And it may also become useful later.
Maybe if we can get a handle on things at some point, we can help restore biodiversity
through this method.
It's not ideal, sure, but I also don't see it as something to fight against.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}