---
title: Let's talk about coffee, Memphis, and the NLRB....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VTEKiExn6p0) |
| Published | 2022/08/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- National Labor Relations Board intervened on behalf of seven employees fired by Starbucks in Memphis.
- The firings came after the employees led a unionization effort and allegedly allowed people in the store after hours.
- The NLRB determined a discriminatory motive behind the terminations and ordered Starbucks to reinstate all seven employees.
- Starbucks plans to appeal the judge's decision.
- The lawyer for the NLRB emphasized the importance of allowing workers to freely exercise their right to join together and improve working conditions.
- The push for unionization in the U.S. is growing across various industries.
- Unions are not limited to specific types of jobs but are for any employee seeking fair treatment and better working conditions.
- The ruling in favor of the fired Starbucks employees is significant for supporting the union movement.
- The increase in profits, stagnant wages, and rising prices across industries are fueling more union activity nationwide.
- Expectations are for a rise in union activity in various locations as these economic challenges persist.

### Quotes

- "Unions are for everybody."
- "The reality is a union is for any employee who isn't getting a fair shake or wants to collectively bargain."
- "The push for unionization in the U.S. is growing across various industries."
- "The increase in profits, stagnant wages, and rising prices across industries are fueling more union activity nationwide."
- "Unions are, well, I mean, they're for everybody."

### Oneliner

National Labor Relations Board orders Starbucks to reinstate seven fired employees in Memphis after leading a unionization effort, underscoring the growing push for unions across industries amid economic challenges.

### Audience

Employees, activists, labor advocates

### On-the-ground actions from transcript

- Support workers' rights to unionize (implied)

### Whats missing in summary

Full understanding of the impact of growing unionization efforts across various industries in the U.S.

### Tags

#Unionization #LaborRights #NLRB #Starbucks #EconomicJustice


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a little bit of labor news,
some union news out of Memphis.
The National Labor Relations Board
stepped in on behalf of some employees,
seven of them,
who had been fired by a particular coffee giant,
a coffee giant that has had,
I want to say a little bit more than 200
of its coffee shops vote to unionize recently.
If you don't know what I'm talking about, it's Starbucks.
So Starbucks
terminated employees and the reason that they said
they terminated these employees
was
they basically kind of
allowed people in the store after hours and stuff like that. They didn't maintain
a secure environment.
Now it just so happened that this occurred after
they started leading a unionization effort.
National Labor Relations Board got involved
and said, hey, this practice,
people being there after hours, this type of stuff,
this was something that occurred
before
the unionization started,
but it only became a problem afterward.
The judge agreed.
Such tolerance
before union activity, but terminations resulting thereafter,
supports an inference of discriminatory motive.
And the judge ordered Starbucks
to reinstate
all seven employees.
Now Starbucks is saying that they're going to appeal.
The lawyer
for
the National Labor Relations Board
said, today's federal court decision ordering Starbucks to reinstate the
seven unlawfully fired Starbucks workers
in Memphis is a crucial step
in ensuring that these workers and all Starbucks workers
can freely exercise their right
to join together to improve their working conditions and form a union.
The
drive
for unionization in the United States,
it's growing.
There are
a lot
of movements
in industries that you wouldn't typically think of
as union jobs.
The reality is a union is for any employee
who isn't getting a fair shake or wants to collectively bargain
to
get better working conditions, get better pay, get better benefits,
stuff like this.
That's who a union's for.
You don't have to be a welder.
You don't have to work a blue collar job.
Unions are,
well, I mean, they're for everybody.
This particular ruling
is
welcome
for those who are supportive
of the union movement
because there was another similar case recently
that didn't go well.
And there's also one pending
up in Buffalo
that we'll have to hear about sometime soon.
But
as
profits go up,
as wages stay the same,
as prices increase,
nationwide this is a thing impacting
pretty much every industry.
We can expect to see more union activity
in more locations.
You may not expect it.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}