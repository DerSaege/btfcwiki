---
title: Let's talk about Trump, Biden, and popularity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oav4cw3h5L0) |
| Published | 2022/08/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Heard something mind-blowing about Trump, Biden, and popularity in 2024.
- Encountered a talkative person at a gas station who shared a surprising story about an ex-Green Beret.
- The ex-Green Beret keeps to himself not because of the town's false backstory but because he's hard left.
- The man at the gas station claimed Trump is being investigated because they fear he will win in 2024.
- Beau consumes right-wing media but was shocked by the belief that Trump is being investigated to stop him from running.
- People believed Biden won in 2020 not because of his popularity, but to vote against Trump.
- Trump is not a candidate who scares Democrats for 2024.
- Polling shows that a significant percentage believe Trump should be charged, giving Democrats an advantage.
- Being in an information silo can reinforce singular viewpoints and hinder critical thinking.
- Despite assumptions, Trump does not have overwhelming support even in GOP primaries.
- There is no conspiracy to stop Trump from running again; it benefits Democrats due to negative voter turnout.

### Quotes

- "Trump drove negative voter turnout."
- "Trump is not a scary candidate."
- "Being in an information silo can reinforce singular viewpoints and hinder critical thinking."

### Oneliner

Beau reveals a surprising perspective on Trump, Biden, and 2024 popularity, debunking myths and shedding light on voter dynamics.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Challenge your own echo chamber beliefs (exemplified)
- Engage in civil discourse with individuals of differing opinions (exemplified)
- Encourage critical thinking and fact-checking within your community (exemplified)

### Whats missing in summary

The full transcript provides a critical analysis of voter behavior and challenges common misconceptions surrounding Trump's potential success in the 2024 election.

### Tags

#Trump #Biden #2024 #VoterDynamics #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Trump and Biden
and popularity in 2024 and having your mind blown.
Because I heard something
and I just ever had your mind blown,
happened to me, had my mind blown.
So we're going to talk about that.
I'm at the gas station and I get out
and I start to fill my tank.
Guy pulls up and I know who he is
and he's a very talkative person.
I'm like trying to avoid eye contact with him
because I don't want him to start talking to me.
But I fail and he does.
And he starts telling me the story about this guy I know.
Now this guy, what people in town know about him
is that he's an ex Green Beret who keeps to himself.
Because of that, they have created
like this whole backstory for the guy.
None of it is accurate.
The reason he keeps to himself
is because he doesn't like any of them.
He is, he's hard left.
Like under no pretext, workers of the world,
nothing to lose but your chains left.
He spent a lot of time with the Kurds
and there is something about being around a bunch of people
who have nothing and coming back to a whole bunch of people
who have everything and them,
anyway, it was transformative for him.
So he doesn't like talking to a lot of people.
This guy who is incredibly talkative
asked him what he thought about the documents.
And apparently he said he thought Trump should go to jail.
And I'm like, yeah, I bet he did.
You know, but this is the part that blows my mind.
The guy who's filling up his car, he's like,
hey, you know, the only reason
they're even investigating Trump
is because they're scared of him running in 2024.
What?
Why?
Because they know he'll win.
Okay.
And I didn't know what to say.
I consume a lot of right-wing media, okay?
And I do it because I think it's important
to understand where their people are coming from.
But I don't hear it the way they hear it.
And I certainly hadn't heard this.
I talked to some other people
who believe the exact same thing.
They believe that the FBI is investigating
to stop him from running in 2024.
Not because, you know, they thought he had classified
documents that turned out to be hanging out at his club.
So we're gonna kind of dive into this concept real quick.
If you are a Republican, ask yourself a question.
In 2020, did you think Biden was popular?
No, right?
No.
But he got more votes
than any other presidential candidate in history.
Why?
Why?
Why did Sleepy Joe get more votes than anybody else?
Because people showed up not to vote for Biden,
but to vote against Trump.
Trump drove negative voter turnout.
People showing up to vote against him.
That's what happened.
This occurred before the 6th.
This occurred before Trump got his hand caught
in the classified cookie jar.
Trump is not a candidate who scares Democrats in 2024.
If you look at the polling now, concerning Trump,
the question outside of Republican circles is,
do you think he should be charged?
And best case, the lowest percentage I was able to find
was 46%.
That means best case for Trump,
Democrats are starting off with a 46-point lead.
People who believe Trump should be charged
are not going to vote for him for president.
I would imagine that there are some Democratic strategists
who actually hope that whoever the candidate is in 2024
is running against Trump,
because it would be incredibly unlikely for Trump to win.
When you're in an information silo,
you're only getting that one viewpoint,
and it's self-reinforcing, and you get stuck.
And when it comes to the Trump faithful,
because of the behaviors associated with some people
involved in Trumpism,
not just have you filtered out dissenting voices,
but there are a lot of people who have opinions
other than pro-Trump who don't talk to you anymore.
Trump, he's not a scary candidate.
When you look at GOP primaries,
he doesn't even have, like, overwhelming support there.
When you look at Wyoming, where, you know,
everybody's talking about how pro-Trump everything is,
one in four Republicans voted for the woman
trying to put him in jail.
There's not a grand conspiracy
to stop Trump from running again.
That doesn't even make sense.
If you're a Democrat, you want him to run again
because he drives negative voter turnout.
More of your people will show up.
That means more of your down-ballot candidates
will win.
I was unaware that this was not just a belief,
but, like, a kind of common one.
And I don't even know what to say about it, to be honest.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}