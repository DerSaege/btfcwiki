---
title: Let's talk about the predictions about Trump's indictment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YO7y6Z-_Bug) |
| Published | 2022/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the intent behind a new talking point coming from the right regarding potential consequences if the former president were to be indicted.
- Points out that the talking point suggesting pandemonium in the streets doesn't make sense politically, given the former president's political acumen.
- Contemplates what might happen if the former president is indicted and speculates on whether he'd take a plea deal or go to trial.
- Considers the composition of a potential jury in a trial involving the former president, noting the presence of Trump loyalists.
- Raises concerns about the talking point potentially inciting riots or violent responses.
- Suggests that those pushing the talking point may believe in overwhelming evidence of guilt against the former president.
- Questions the logic behind trying to stop the indictment if the evidence is weak, as a trial could re-energize the former president politically.
- Concludes with uncertainty about the true motivations behind the talking point and its potential implications.

### Quotes

- "Why? That's weird. That's a weird talking point."
- "I don't understand the talking point."
- "It's a bad talking point for a whole bunch of reasons."

### Oneliner

Beau questions the odd political implications and potential motivations behind a talking point predicting chaos if the former president were indicted.

### Audience

Political analysts, activists

### On-the-ground actions from transcript

- Monitor and address potentially incendiary talking points in political discourse (suggested)
- Advocate for fair and unbiased legal processes (implied)

### Whats missing in summary

Insightful analysis on the potential political strategies at play during a hypothetical indictment scenario.

### Tags

#PoliticalAnalysis #FormerPresident #TalkingPoints #Indictment #LegalProcess


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to entertain one of my favorite questions. Why?
Anytime a new talking point comes out, that's really the question I try to ask.
Why? What's the intent? What are they trying to convey with this talking point?
And a new one has surfaced coming out of the right.
And it's coming from people who, some of which, are very close to the former president.
And it's making predictions about what would happen if he was indicted.
And the general idea is that there would just be pandemonium in the streets. It would be wild. Right?
Why? That's weird. That's a weird talking point. Because it doesn't make sense politically.
And whether you like Trump or not, he's not politically inept. Right? He's not.
Even if you don't like him, he isn't. There's a reason he has a hold over people the way he does.
So what happens if he goes and gets himself indicted here?
I mean, assuming the country doesn't, you know, erupt. What happens?
I don't think anybody has Trump taking a plea deal. Right?
If you like him, he's too determined and honorable for that.
If you don't like him, he's too much of an arrogant windbag. Right?
I don't think anybody has him taking a plea unless it's just overwhelming. Right?
This is a man who's going to take this to trial.
If he goes to trial, what happens? There's a jury box.
You figure what, 20% of the U.S. is like the MAGA faithful?
Statistically speaking, there's going to be at least one person on that jury
who has a red hat at home in their closet.
And yeah, it may only be one, but you only need one. Right?
And then he'd be acquitted. And if he's acquitted, that's vindication. Right?
That failing political career that he has, that political power that is just currently tanking,
it gets rebuilt.
So why this talking point about riots?
You know, there are a lot of people saying that those making the talking point
are attempting to subtly incite it, to put that idea out there, to plant that seed.
Maybe. I don't know that that's true, but it's a weird talking point.
It doesn't make sense politically, unless you're familiar with the evidence.
And you believe that whoever it is that's on that jury that owns that red hat
would be confronted with so much overwhelming evidence that they would have to convict.
Otherwise, this is a walk in the park.
Somebody on that jury is going to be a Trump loyalist.
So why try to stop the indictment in this way?
Why are you putting yourself in jeopardy of perhaps being responsible for really bad things,
morally or maybe even legally?
The only answer I can come up with is the people making this argument,
the people pushing this talking point, believe that not just is Trump guilty,
but that the evidence is just so overwhelming, even the most loyal followers would still convict.
I mean, maybe there's another reason. Maybe it is just simple prediction.
You know, maybe that's what it is. I don't know, but I don't understand the talking point.
It doesn't make sense because from a political standpoint,
if the evidence is as weak as they claim on the TV news, on Twitter, on the fake Twitter,
it would seem that they would kind of welcome the trial because that would totally re-energize Trump.
I don't know. But it's a bad talking point for a whole bunch of reasons.
Not only could it get somebody hurt, not only does it put people who are their most loyal followers at risk,
it doesn't even add up politically, unless they know he's guilty.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}