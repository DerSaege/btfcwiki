---
title: Let's talk about Trump and witch hunts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bGurLYv7-FY) |
| Published | 2022/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of a "witch hunt" as looking for something that isn't really there or trying to embarrass someone by searching for something you know isn't there, typically targeting those with opposing beliefs.
- Analyzes the situation with Trump and the FBI's search for documents, debunking the idea of it being a witch hunt.
- Details the FBI's thorough process of obtaining a warrant and finding exactly what they were looking for, contrasting it with the typical notion of a witch hunt.
- Suggests that if Trump had cooperated and not turned it into a political issue, the situation might have been resolved without public attention.
- Points out that Trump's actions and desire for drama led to the embarrassing situation of sensitive documents being mishandled, reflecting poorly on the presidency and the country.

### Quotes

- "That's not a witch hunt unless it's a witch hunt at the Sanderson sisters' house."
- "That spell that forces people to attempt to protect the institution of the presidency, man, that spell is still active in D.C."
- "It's not a witch hunt. Anyway, it's just a thought."

### Oneliner

Beau debunks the notion of a "witch hunt" in Trump's FBI document search, citing thorough process and lack of publicity as evidence against it, reflecting on the embarrassment caused by mishandling sensitive documents.

### Audience

Political observers, truth-seekers

### On-the-ground actions from transcript

- Stay informed and critically analyze political situations (implied)
- Advocate for transparency and accountability in governmental actions (implied)
- Support measures to safeguard sensitive information (implied)

### Whats missing in summary

The full transcript provides deeper insights into the handling of sensitive information in politics and the consequences of turning official matters into political drama. Viewing the full transcript gives a comprehensive understanding of the implications of mishandling confidential documents.

### Tags

#PoliticalAnalysis #Trump #FBI #WitchHunt #DocumentSearch


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about two words
that keep showing up in my inbox
over and over and over again.
Sometimes it's in a sentence
and sometimes it's just those two words.
If you're gonna send me something,
give me more than two words to work with.
And we're gonna go through that term
and see if it applies to the situation
with Trump and his little club.
Because there are a whole lot of people
repeating that idea that this is a witch hunt, right?
What is a witch hunt?
It's when you're looking for something
that isn't really there.
Or you're looking for something that you know isn't there,
but you're doing it to embarrass or damage
the person that it's being done to
or the group that it's being done to.
Typically a group or person that is of an opposition religion
or political belief or ideological belief.
That's what a witch hunt is.
Is that what happened here?
So the FBI, they have a long back and forth
with Trump team about obtaining these spell books, right?
And it's not going anywhere.
And they're told that everything was given back.
So they go to a wizard in long robes
and they say, hey, we want permission to go and look.
This is the reason we still think
these items are gonna be there.
These are the items we think are gonna be there.
And the wizard's like, okay, sure.
And signs off on that warrant.
And then they go and look
and they find exactly what they said they were gonna find.
That's not a witch hunt unless it's a witch hunt
at the Sanderson sisters' house.
They actually found what they said they were going to find.
That's kind of the opposite
of how witch hunts typically work.
The FBI is not out there running amok, amok, amok.
They did this by the numbers.
There is a spell that's at play here.
And it's one I've talked about on the channel
quite a few times.
I'm willing to bet that even if just six months ago
Trump team had given everything back,
that would have been the end of it.
That would have been the end of it.
Because that spell that forces people
to attempt to protect the institution of the presidency,
man, that spell is still active in D.C.
In fact, when it comes to the idea of damage
and embarrassment, right?
Also keep this in mind.
The FBI didn't issue a press release about this.
Trump started talking about it.
That's when the information came out.
It's really hard to embarrass somebody
if you keep it a secret.
And here's the thing that eventually,
once Trump and company realizes it,
it'll probably haunt them forever.
I'd give even odds on the idea that if he had said nothing,
there'd have been nothing.
That would have been the end of it.
If they actually retrieved everything
and he had not turned it into a political thing,
that would have been the end of it.
But now he has put the FBI in a position
where they kind of have to act.
He had SCI documents.
It's not a witch hunt.
At the end of this, they went through the steps.
They said what they were going to look for.
They gave the reason to go and look for it.
And lo and behold, it was there.
That's the exact opposite of a witch hunt.
And they didn't do it with a bunch of publicity.
Trump put the publicity on this.
And seriously, if he had just accepted
that they were doing a document retrieval,
getting that stuff back, if he had let it go,
they very well might have too.
Because that spell, that desire to protect
the institution of the presidency,
it is very, very strong.
And it is absolutely embarrassing
to the United States for the information to be out there.
And I'm not talking about what was in the documents.
I'm talking about the fact that the people of this country
elected somebody who would have some of this nation's most
sensitive secrets just hanging out at a club.
It's an embarrassment, not just to the country,
but to the institution of the presidency.
Trump's desire for constant drama,
his desire to feel like he's always being,
you know, targeted, kind of led to this.
Because there is a very strong possibility
that as long as they got everything back,
this would have gone away.
It's not a witch hunt.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}