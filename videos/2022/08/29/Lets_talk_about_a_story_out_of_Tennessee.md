---
title: Let's talk about a story out of Tennessee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SgomADVb-9E) |
| Published | 2022/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A former house speaker and chief of staff from Tennessee, Cassata and Cothra, have been charged with around 20 counts by the feds, starting from a simple white lie and escalating to serious crimes involving public corruption.
- The allegations involve a company initially hired for political mailers during primaries, but then started doing business with the General Assembly, leading to accusations of enriching themselves improperly.
- To carry out their scheme, Cassata and Cothra allegedly created a fictional person named Matthew Phoenix, with text messages discussing who should impersonate him on the phone, culminating in kickback schemes and money laundering charges.
- Despite the relatively small amount of money involved (around 50 grand), the case has complex layers and serious implications, likely to attract national media attention if it proceeds to trial.
- The evidence against the defendants seems strong, with a document-heavy case that could lead to severe penalties if proven guilty.
- Speculation suggests that the federal government may be pressuring Cassata and Cothra to cooperate and reveal information about others involved in corruption, potentially indicated by the public nature of their arrests.
- The unusual manner of arrest, without the usual privacy afforded to political figures, hints at hidden information in the government's case or a strategic move to compel cooperation from Cassata and Cothra.
- The severity of the charges means there is little room for leniency, possibly motivating the accused to cooperate if they possess knowledge of broader corruption.
- Despite the entertaining and bizarre aspects of the case, the charges are serious, indicating a potential for wider implications if cooperation leads to uncovering more corruption.
- Beau predicts that this case will gain national attention as more details emerge, with increased media coverage expected.

### Quotes

- "It involves a former house speaker and their former chief of staff."
- "The truly wild part to me is that all of this, this giant story that is twisting and all of these allegations stem from what appears to be like 50 grand or so."
- "If these were my clients, I would tell them to make preparations to go away."
- "There may be motivation for them to cooperate if they do actually know something about more widespread corruption."
- "It's just a thought, y'all have a good day."

### Oneliner

Former Tennessee house speaker and chief of staff face serious corruption charges involving a fabricated identity and monetary crimes, likely leading to national scrutiny and potential cooperation for broader revelations.

### Audience

Legal observers

### On-the-ground actions from transcript

- Stay informed on the developments of this case and similar instances of alleged corruption (exemplified)
- Advocate for transparency and accountability in political processes (implied)

### Whats missing in summary

The potential repercussions of the case on Tennessee's political landscape and the broader implications for public trust in government institutions.

### Tags

#Corruption #Tennessee #LegalSystem #NationalNews #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about a little caper
out of Tennessee.
And we're going to do this because a whole bunch of people
in Tennessee sent me links to it and said,
hey, could you look into this?
And it is wild.
It involves a former house speaker
and their former chief of staff.
The names are Cassata and Cothra.
The feds have picked them up and charged them with like 20 counts.
And I started looking through the publicly available stuff and it looks as though it
started as like a simple white lie and spiraled out of control.
And it went from something relatively simple to very serious crimes.
There are some serious allegations here.
It appears as though initially it dealt with a company that was going to send out political
mailers for candidates during the primaries.
And then it started doing business with the General Assembly.
Now as a lawmaker you can't enrich yourself that way.
And the allegations from there, and keep in mind all of this is alleged at this point,
The allegations from there suggest that these two just created a whole new person named
Matthew Phoenix, who owned the company, but this person didn't actually exist.
There are alleged text messages that have the two talking about who would impersonate
this person on the phone, and the charges stem from a kickback thing all the way to
money laundering.
And the truly wild part to me is that all of this, this giant story that is twisting
and all of these allegations stem from what appears to be like 50 grand or so, somewhere
around there, which in a public corruption case with this many moving parts, that's not
a lot of money.
So it's a wild case that I have a feeling is going to catch the attention of the national
media, especially if it goes to trial.
It is a document heavy case, which is really bad for the defendants because a lot of things
are written down.
A heavy hitting attorney out of Nashville, I want to say the name was Gary Blackburn,
said that if these were my clients, I would tell them to make preparations to go away.
The general consensus is that the federal government's case is incredibly strong.
And from what I saw, it did appear that way.
Now there's also a lot of speculation suggesting that the federal government may be attempting
to get them to cooperate and flip on other people who we don't know yet.
There are some things that make that plausible.
The actual arrests were very public.
When you're talking about political figures, generally speaking, they allow them a little
bit of privacy.
They call them up and say, hey, why don't you come turn yourself in?
And it's all done quietly and everything can be addressed later.
They actually went and cuffed these people, which is, that's out of the norm when you're
talking about political cases, that there may be information in the federal government's
case that isn't public yet that led them to do it in that manner.
Or it could be a show to demonstrate how serious the situation is in an attempt to get them
cooperate about something else. And again, as entertaining as this story is if you read through
it, and as bizarre as it is, these charges are real charges. This isn't something that is
generally given a slap on the wrist, so there may be motivation for them to cooperate if they do
actually know something about more widespread corruption. That's really all
I could dig up, but I do get the interest in it because it is kind of bizarre. But
rest assured I have a feeling that this will become national news. People will be
paying attention to this. So there will be more coverage. Anyway, it's just a
thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}