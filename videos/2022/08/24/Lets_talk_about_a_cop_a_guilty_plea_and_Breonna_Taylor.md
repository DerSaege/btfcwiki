---
title: Let's talk about a cop, a guilty plea, and Breonna Taylor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KTBOJ1Swoxs) |
| Published | 2022/08/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Update on the Department of Justice news regarding the Breonna Taylor case.
- Former Louisville cop, Kelly Goodlett, pleaded guilty for helping falsify information to obtain the warrant.
- Goodlett wasn't indicted; she was charged via complaint and entered a guilty plea.
- Goodlett's sentencing is scheduled for November 22nd, but there might be delays due to extenuating circumstances.
- The judge's statement during the hearing indicates Goodlett's cooperation, which could influence her sentence.
- This development is positive news for Breonna Taylor's family but bad news for other cops facing charges.
- Three other cops are facing charges, with the maximum sentence being life imprisonment.
- Beau clarifies that "life" in the federal system means exactly that - life without the possibility of parole.
- Goodlett's potential sentence is uncertain, with the charge she pleaded to carrying a maximum of five years.
- Beau had predicted the possibility of cops pleading guilty due to information suggesting cooperation within their ranks.

### Quotes

- "It means life. You go into the cage, you come out when you go into the forever box."
- "Goodlett's plea significantly strengthens the federal case, if that is, in fact, what's happening."
- "This significantly strengthens the federal case, if that is, in fact, what's happening."

### Oneliner

Former Louisville cop pleads guilty in the Breonna Taylor case, potentially strengthening the federal case and indicating cooperation within their ranks.

### Audience

Justice seekers

### On-the-ground actions from transcript

- Support organizations advocating for police accountability (implied)
- Stay informed and engaged with updates on the case (implied)

### Whats missing in summary

Details on the federal case's progress and potential implications.

### Tags

#BreonnaTaylor #DepartmentOfJustice #PoliceAccountability #Cooperation #FederalCase


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about some more news
out of the Department of Justice.
This time it's about the Breonna Taylor case.
A former Louisville cop, Kelly Goodlett,
has entered a plea of guilty.
She has admitted to helping falsify information
to obtain the warrant.
Now, what is interesting about this
is that Goodlett wasn't indicted.
She was charged via complaint and entered a guilty plea.
Some people already have their eyebrows raised.
Further, during the hearing, the judge
said that she's scheduled to be sentenced on November 22nd,
but there might be extenuating circumstances
that cause her sentencing to be delayed.
For those who don't know why I'm saying it like that,
that is a super strong signal that Goodlett is cooperating
and cooperating to the degree that it is expected
to influence her sentence.
So that is really, really good news for Breonna Taylor's family.
It is really, really bad news for the other cops.
There are three others that are facing charges.
I want to say the max sentence they're looking at is life.
And to go ahead and answer a question that always comes in,
what does life mean in the federal system?
It means life.
It doesn't mean 20 years.
It doesn't mean 30 years.
Doesn't mean parole after a set period of time.
It means life.
You go into the cage, you come out
when you go into the forever box.
Means life.
Now, as far as Goodlett, what is she looking at?
No clue.
No clue.
I want to say the charge she pled to has a max of five years,
but if she is cooperating, that sentence
will be reduced substantially.
So we don't know about that.
Now, to answer another question that has come in,
is how did I know somebody was going to plea two weeks ago?
Two weeks ago, I did an update on this
and said you might expect to see some cops plea.
I didn't know that Goodlett was going to plea.
What I knew was that the feds back then, two weeks ago,
had indicated that they knew the cops met in a garage
to get their story straight.
That's information you're kind of only going to get
if somebody is already talking.
So that's where we're at.
There's going to be more to come on this.
But Goodlett's plea, while it may not
seem like it because the sentence is going to be very
short, I would expect, it's probably the best news
that Taylor's family has gotten in quite some time
because this significantly strengthens the federal case,
if that is, in fact, what's happening.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}