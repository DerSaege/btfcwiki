---
title: Let's talk about sewer surveillance....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rp4Mi8EDzRw) |
| Published | 2022/08/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of using wastewater as a public health tool to provide advanced warning of disease outbreaks.
- Mentions the development of the Sewer Coronavirus Alert Network (SCAN) in the United States to track diseases like polio and monkeypox.
- Points out that monitoring wastewater can help public health officials prepare and allocate resources before patients start showing up at hospitals.
- Acknowledges the limitations of this method due to the absence of centralized sewer systems in some parts of the United States.
- Addresses a question about the potential misuse of this technology for mass surveillance, stating that technology is neutral and its use depends on how it's implemented.
- Encourages building a better future instead of dwelling on dystopian scenarios.

### Quotes

- "You could develop a public health tool that could give advanced warning of diseases showing up."
- "It's a really good concept."
- "Technology is not inherently good or bad. It is what it is."
- "Sure, you can picture a dystopian future, or you can work to build a better one."
- "Y'all have a good day."

### Oneliner

Beau explains using wastewater as a public health tool to track diseases, like polio and monkeypox, through the Sewer Coronavirus Alert Network, while addressing concerns about potential misuse and promoting a positive outlook on technology.

### Audience

Public health officials, researchers

### On-the-ground actions from transcript

- Monitor wastewater for disease detection (exemplified)
- Support the development and implementation of advanced public health tools (exemplified)
- Advocate for equitable access to public health resources (exemplified)

### Whats missing in summary

The full transcript provides additional context on the history of monitoring wastewater for disease outbreaks, the specific functions of the SCAN system, and the potential impact on healthcare logistics.

### Tags

#PublicHealth #WastewaterMonitoring #DiseaseOutbreaks #Technology #ResourceAllocation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the sewer.
We're going to talk about the sewer.
In a lot of the videos over the last couple of weeks
that have dealt with polio or whatever,
I've mentioned how researchers have found traces of something
in wastewater.
And it has prompted a lot of questions
about how this is happening, and why, and when did it start,
and all of that stuff.
So today, we are going to talk generally about wastewater
and the sewer system.
So the idea that you could develop a public health
tool that could give advanced warning of diseases showing up,
it's been around a while, that by monitoring wastewater,
you could pick up little bits and pieces
and know generally where a disease is present.
This concept has existed.
I want to say Israel's used it for a while
and had pretty good results with it.
In the United States, this was mainly done for research.
It wasn't something that was used as a public health tool,
because, well, there was no real money to use it in that way.
And it hadn't developed to that point.
However, COVID, it just provided a flood of resources.
So the United States now has something,
I want to say it's called SCAN, like Sewer Coronavirus Alert
Network, or something like that.
The name's obviously going to have to be changed,
because now it is tracking polio.
I think they're actually using it for monkeypox as well.
And it's just giving public health officials a heads up,
saying, hey, this specific thing is showing up in this area,
and it allows them to counter it.
And hopefully, as it progresses, it
might even help in logistics and making sure
that hospitals in certain areas have the resources they need
before they need them.
Because a lot of times, this stuff
will show up in the wastewater before the patients start
showing up at the hospital.
So having the resources to deal with it, I mean, that's amazing.
It's a really good idea.
It's a really good concept.
There are limitations to it, though.
In large parts of the United States,
there's no sewer system, no centralized one.
You know, where I live, it's septic systems.
So there's no way to monitor that.
So there are gaps in data.
But this will definitely help when
it comes to outbreaks, because those generally
happen in larger areas, places where there would
be a centralized system.
One of the questions seemed a little conspiratorial.
Could it be used?
Could this same technology be used for other stuff?
Technology is not inherently good or bad.
It is what it is.
It could be used for mass surveillance
of all kinds of things, yes.
Is it going to?
Probably not.
I mean, not anytime soon.
Sure, you can picture a dystopian future,
or you can work to build a better one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}