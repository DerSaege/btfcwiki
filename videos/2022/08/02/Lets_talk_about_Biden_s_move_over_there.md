---
title: Let's talk about Biden's move over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lu9liunbFjM) |
| Published | 2022/08/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses three questions asked after Biden's announcement of a strike hitting Zawari.
- He explains the importance of the strike, describing Zawari as a bad person with a history of hitting civilians directly.
- Beau stresses that despite the strike, the group will likely continue with someone else stepping up, but it will be challenging to rally the same support.
- The strike does violate the peace agreement with Afghanistan as the United States was not supposed to have assets in the country.
- Beau points out that the right wing is pushing back because of past government decisions under Trump, not due to concerns about violating another country's sovereignty.
- He believes the Biden administration may struggle to frame this as a significant win due to messaging issues.
- Beau does not anticipate increased US involvement in Afghanistan, suggesting a continued standoff approach with selective responses.

### Quotes

- "Yeah, it's really that big of a win for this campaign."
- "The odds of them being as effective are slim to none."
- "The only reason that this occurred is because they violated the agreement."
- "It's not that they have some qualm with violating the sovereignty of another country."
- "US involvement in Afghanistan is going to look exactly like it just did."

### Oneliner

Beau explains the significance of Biden's strike, violation of agreements, and political pushback, anticipating no increase in US involvement in Afghanistan.

### Audience

Policy analysts, political activists.

### On-the-ground actions from transcript

- Analyze and question government decisions (exemplified)
- Advocate for transparent messaging from administrations (implied)
- Stay informed and engaged with foreign policy developments (implied)

### Whats missing in summary

Full context and detailed analysis of each question and its implications. 

### Tags

#ForeignPolicy #BidenAdministration #Afghanistan #PeaceAgreement #GovernmentTrust


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about three questions
that were asked repeatedly after Biden's announcements
about what occurred over there.
Okay, so the three questions that came in
came in, or was it really that important?
The next is, does this violate the agreement, the whole peace thing?
And then the third is, why is the right wing pushing back and acting like this wasn't a
big deal or that it was bad?
So if you don't know what I'm talking about, the Biden administration launched a strike
hit Zawari. So let's start with, is it really that important? There aren't many
times when I'm talking about foreign policy, especially when I'm talking about
conflict, when I say somebody's a bad person. This is one of those times. Most
Most of the coverage has been focused on his involvement with One Day in September.
His history goes way beyond that.
This is a person who had no issue hitting civilians.
And I don't mean as collateral as, well, that's just how it happens.
With them as the target.
is a person who was on the DNR list for two decades. Yeah it's a it's really that
important it really is that big of a win for this campaign. He in many ways he was
the brains. He was the logistics, more so than names that people are more familiar
with. Now does this mean it's the end of that group? No, somebody else will step
up. They always do, but the odds of them being as effective are slim to none. The
odds of them being able to rally the same kind of support slim to none.
Yeah, it's really that important.
Does it violate the agreement, the peace agreement there with Afghanistan?
I mean, the United States was not supposed to have assets in country.
In fact, that's true, that is a violation of the agreement.
And I'm sure the government in Kabul is super mad about it, but they can get a cape, because
the only reason that this occurred is because they violated the agreement.
Their end of the agreement was that he wouldn't be there, so I don't think much is going
to come of that. Now why is the right wing pushing back? Because it was
probably a really bad idea to trust the current government there when the deal
was made and that's on Trump. The fact that this group was now operating there
yet again, that's on Trump. So what they're going to try to do is downplay it
and pretend like it wasn't a big deal, pretend like we shouldn't have done it,
whatever, because it shows that the government there, yeah they, maybe they
understand the art of the deal a little bit better than other people. So that's why they'll push back
against it. It's not that they have some qualm with violating the sovereignty of another country.
it's that it makes them look bad. So this is a major win for the Biden administration,
but I don't know that it will be cataloged as one because the administration has
a problem with messaging and I don't think that they're going to come out and
talk about the embassies and really hammer home how important this person
was. So this should be a major win for the Biden administration but it probably
won't be politically anyway.
So that's the general overview of it.
Another question that came in not quite as frequently
was, do I think that this is going
to lead to more involvement in Afghanistan?
No, no.
I think US involvement in Afghanistan
is going to look exactly like it just did,
standoff capability, and more of a selective approach
to dealing with any issues that might arise there.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}