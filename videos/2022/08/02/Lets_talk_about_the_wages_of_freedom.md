---
title: Let's talk about the wages of freedom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RC-_fFq1mQA) |
| Published | 2022/08/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Freedom is a term thrown around in the United States without a clear definition.
- People often define freedom as the ability to do what you want.
- In reality, freedom is about the absence of necessity, coercion, or constraint in choice or action.
- In the US, freedom is equated with money due to the societal system.
- Without money, individuals are coerced into working even when sick to avoid homelessness.
- Politicians opposing minimum wage and living wage are, by definition, anti-freedom.
- The coercion of the system keeps the lower levels of society in place.
- Lack of money leads to a life of necessity and coercion, not freedom.
- Changing one's social class in the US is rare and not easily achievable.
- Politicians defending corporations over citizens perpetuate a system of coercion and lack of freedom.

### Quotes

- "Politicians who push back against the minimum wage are anti-freedom."
- "In the US, freedom is equated with money."
- "Those on the bottom are kept in line through coercion."
- "Lack of money leads to a life of necessity and coercion, not freedom."
- "Changing social classes in the US is rare and not easily achievable."

### Oneliner

Freedom in the US is tied to money, with lack of funds leading to coercion and a life devoid of true freedom, perpetuated by politicians opposing fair wages.

### Audience

Advocates for fair wages

### On-the-ground actions from transcript

- Advocate for raising the minimum wage and a living wage for all (suggested)
- Support policies that empower individuals financially (implied)

### Whats missing in summary

The full transcript expands on the impact of financial coercion on freedom and the role of politicians in perpetuating this system.

### Tags

#Freedom #US #Money #Wages #Politicians #Coercion


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about the wages of freedom.
And what freedom really is.
Because that's a term that in the United States, that gets thrown around a lot.
But when you ask people, what does freedom mean to you?
Most times they really can't answer.
I mean, you get standard, canned, patriotic answers, mom and apple pie and all of that stuff.
So take a moment and try to come up with an encompassing definition for freedom.
Odds are you're probably going to come up with something along the lines of the ability to do what you want.
That's freedom, right?
And that may sound silly, but that's not actually too far from the standard definitions.
Like the quality or state of being free, such as the absence of necessity, coercion, or constraint in choice or action.
So if you need to do something, if you have to do it, if you're coerced into doing it,
if you don't have a lot of options, you're not free, right?
What does it take to be free in the land of the free, the home of the brave and all that stuff?
What does it actually take to be in a situation similar to that,
where you don't need to do whatever it is you're about to do?
You don't have outside forces pushing on your decision to an extreme level.
What's it take?
Money.
Money, power coupons, cash, currency.
In the United States, freedom is money.
Because that's the system that we have built.
That's the system that we have let get way out of hand.
If you don't have money, you're being coerced.
If you don't have cash, you don't have those power coupons,
you have to get up and go to work even if you're sick, because if you don't, you're going to be out in the street.
What does that say about politicians who push back against the minimum wage, against a living wage?
It says that by definition they are anti-freedom.
They don't want the American people to have freedom.
They don't want them to be free of coercion, because the coercion is what keeps the system running.
The coercion is what keeps those people on the bottom in their places.
And if they had freedom, if they had more power coupons, they would have more options.
And the more options they have, the harder they are to control.
The more of a voice they have.
When you look at the pay that the lower levels of our society get, it's not freedom.
It is not freedom.
They don't have a life free of necessity or coercion.
That is their life.
Their life is doing what they have to to survive.
And if you uphold that system, you are anti-freedom.
There's no other way to frame it.
The only attempt that people make in trying to justify this is,
well, you know, we live in a country that has the most opportunity in the world.
We live in the freest country in the world.
I don't know where they're getting these statistics.
And because we're in this freest country, these people on the bottom,
they have the ability to change their station.
So they are free.
Right. That totally makes sense.
So those people who currently live in objectively totalitarian countries,
well, they're really free deep down because they have the ability to sneak out, right?
They can change their location.
So really they're free, even if they are constantly acted upon by the state through coercive means.
You wouldn't buy that.
Those people in jail, they're really free because, you know, their station could change.
They could get an appeal or they could escape, right?
And then because that possibility exists, they're free.
The reality is changing your station in the United States,
moving between classes in the United States, there is a rarity.
That's not something that occurs very often.
Not enough to warrant the idea that those on the bottom have a way out.
Statistically, they don't.
They don't.
That's one of the biggest determining factors is where you started.
When you hear politicians push back against the citizens of this country
so they can defend the corporations of this country, understand they are anti-freedom.
They want that coercion in place.
They want those on the bottom kept in line through that coercion.
That's not freedom. It's the opposite of freedom.
They want them in a position that says,
hey, I know you're sick and all, but you need to get to work because rents due,
or get out on the street.
It's not exactly free.
And that situation exists for a whole lot of people
because the minimum wage in this country has been neglected.
Because prices have gone up, profits have gone up,
but those people on the bottom have been left behind
because that is where those politicians want them.
They want them in a position where they have no voice,
where they don't have power coupons, where they can't influence change
because those who are on the bottom of this system know the need for change the most.
If a politician is against raising the minimum wage,
if a politician is against a living wage for everybody,
regardless of that profession, they are anti-freedom.
They are pro-oppression.
They want that coercive system in place
because it safeguards the status quo
and therefore it safeguards them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}