---
title: Let's talk about a Thanksgiving Q&A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4srGMIICtN8) |
| Published | 2022/11/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau hosts a Thanksgiving special, answering questions from Twitter in a long format video.
- The questions cover a wide range of topics, from historical influences on Thanksgiving to personal anecdotes.
- Beau explains the humorous origins of items in his workshop and shares insights on past experiences and future plans.
- Beau touches on topics like playing tabletop role-playing games, relationships, favorite food recipes, and more.
- He candidly shares personal stories about animals, family dynamics, and even his own experiences with troublemaking in school.
- Beau provides a glimpse into his life, including interactions with his pets and reflections on past decisions.
- The transcript captures Beau's casual, off-the-cuff style and his willingness to share personal stories with his audience.
- Beau hints at upcoming changes in his content workflow, promising more behind-the-scenes footage and outtakes.
- The audience gets to see a mix of humor, personal reflections, and Beau's genuine engagement with viewer questions.
- The transcript showcases Beau's down-to-earth personality and his dedication to creating engaging content for his audience.

### Quotes

- "It's like one of those based on a true story things, but in the movie that's based on a true story, there's a ghost and a demon and a god and a ghost."
- "It's made up. It's a myth. The reality of what occurred is so far removed from the image that we have."
- "Yeah, if you look at that list of the stuff we're not supposed to discuss, that's what's getting people killed."
- "The school that I went to, heavily weighted tests. So yeah, I was very much the troublemaker."
- "So I hope y'all enjoyed this and I hope y'all have a good couple of days. Anyway, it's just a thought."

### Oneliner

Beau shares personal anecdotes, answers viewer questions, and hints at upcoming content changes in a Thanksgiving special Q&A session.

### Audience

Content creators and casual viewers

### On-the-ground actions from transcript

- Watch Beau's videos for insights into community gardening and other DIY projects (suggested)
- Support creators like Beau who share personal stories and engaging content (exemplified)

### Whats missing in summary

The full transcript captures Beau's engaging storytelling style and genuine interactions with his audience. For a more in-depth look at Beau's life and perspectives, watching the entire video is recommended.

### Tags

#Thanksgiving #Q&A #CommunityGardening #DIY #ContentCreation


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to do our Thanksgiving special.
Around the holidays I like to do a long format video that gives people something to listen to
that's longer than you know three to eight minutes in case they have to work or are traveling to get
to where they want to be or can't be where they want to be or don't have someplace they want to be.
I try to give people something that lasts a little bit longer than three to eight minutes. By the way
this is Maryland, we are in the barn tonight. So okay so these questions come from Twitter.
We're doing a little bit of a Q&A. Normally I haven't seen any of these before and you're
getting a very off-the-cuff answer. Some of these I have seen from notifications and
I would also point out that this time there's like 300 of them so it is unlikely
we're going to get through all of them. You're going to hang out here the whole time aren't you?
Yeah okay. Okay so let's just kind of dive right in here. Which U.S. president had more of an
influence on the Thanksgiving holiday? George Washington, Abraham Lincoln, or FDR? Most people
would say FDR because he created the modern Thanksgiving. I would say Abraham Lincoln
because of the way he tied it to the Civil War made it something that was widely observed and
kind of put it on the path to becoming a part of American mythology the way it has. Please tell us
about a couple of the items behind you that have a humorous origin. I'm assuming you're talking
about in the normal shop not right now. To me all of that's funny because if you don't know
when the channel started that was filmed in my actual workshop. As time went on and the channel
became more of a thing we rebuilt the shop because the workshop well A. you know work actually got
done in there. B. it wasn't soundproof it had a tin roof so if it was raining or somebody was
running a tractor or anything like that I couldn't record. So we rebuilt it. So to me all of that
stuff is funny because it's not actually the shop. Let's see have you ever played would you ever play
a tabletop role-playing game such as Dungeons and Dragons? Not something I've done. I did when I was
younger I played Twilight 2000 but I mean I'll try just about anything once.
So yeah I would consider playing it. You have to invite two people from politics past or present
to Thanksgiving dinner. One you like and one you hate. Who are they? Let's see right now Zelensky
and Putin. That that seems like a conversation that needs to take place. How can I get my beard
as nice as yours? I'm a trans man I've been trying for years. It's genetics. There's there I don't
think there's anything you can actually do to I don't think there's anything you can actually do
to make it grow fuller or anything like that. I've been able to grow a beard like this since I was
like 17. So I'm not sure there's anything you can do. Hi. What do you want to see Katie Porter
diagram on her whiteboard? I'd like to see her look at politicians votes and kind of do a little
flow chart from the various donations through the campaign their policy policies shifting and then
their their eventual votes. I think that would be a nice demonstration for the American people.
Let's see. I have some food questions. When I lived in the south everybody I talked to had
their own best recipe for cornbread. What's yours? Also, do you enjoy grit straight or do you add
stuff like us Yankees do? The cornbread thing that's there's only like five recipes. Everybody
pretends like it's some great secret. There's really only like five recipes. They're just
adding a bit of mystique. As far as grits, I will eat grits in just about any way shape or form.
There's a salt lick in front of me that definitely seems to have her attention right now. Am I in
your way? Let's see. Because of my recent... Really? Okay. Hey, hey, hey. No, no, no. That's
enough. All right. Back, back, back, back, back, back. Get out of camera. You ready for your close
up? Okay. Go play. Go do something. This is a joke, right? Okay. How about a reel of outtakes
and bloopers? Also, yeah, this could definitely be one, right? Okay. So, there's going to be a
change in our workflow soon, which will actually create a situation where outtakes, bloopers,
behind the scenes stuff gets recorded and distributed more often. We'll also be able to
put out more bow on the road type episodes. Let's see. We all know that our experiences in our past
shape who we are in the present. That said, if you were given the chance, would you change anything
about the past, good or bad, or do anything differently, knowing it would change the person
you currently are because of the experiences? So, the things that I would change, the things in my
life that I look back on and I'm like, man, I shouldn't have done that, or I wish I had done
that differently or anything along those lines. They're so far in the past that any change,
the butterfly effect, the ripple effect, would just be too much to bear or even really imagine.
So, I guess not as much as there are a lot of things in my past that I would like to change.
Let's see. I, well, going back just a few seconds, for those who are listening to this on a podcast
episode, all of that commotion, that's the horse. I said we were in the barn. One of the horses
decided to come up and say hi. Okay. Have you ever tried VR and or would you do something with it
for a video or fundraiser? Yeah, you need to look into some of the weird things I've done
for fundraisers. Yeah, I would definitely do something with VR for a fundraiser. I haven't
really done much with it. I've played with it once or twice, but that's it.
Which animated sitcom has the best Thanksgiving episode? I vote King of the Hill followed closely
by Bob's Burgers. Rick and Morty. Rick and Morty's Thanksgiving episode is definitely unique. It
has a nice tight moral at the end of it. The kid in the show has an assumption about why the United
States kind of leads the world, and the assumption is wrong. And even though the correction is framed
in a very sci-fi way, it is very applicable to American history and what actually happened. So
I would go there. Let's see. I've had livestock and such animals around my whole life.
I know how weird they can be. What's your funniest or strangest animal anecdote? I mean,
other than the one that just happened? Marilyn, that horse, gets jealous. And I don't mean that
in the way that all animals have a favorite person that they own. She gets legitimately jealous. If I
was to bring one of the other horses over here and start petting them, she would run over and
chase them off. She side-eyes my wife. She is legitimately jealous of me interacting with any
other living creature. What's your favorite color? I am colorblind. What's the worst trouble you got
in elementary school? I said this was going to be long format content. Not that long. I got in a
lot of trouble as a kid. So yeah, there's a whole lot of stories there. I was very well known for
taking unexcused absences, like from the third grade on. But I like to read, so I never had a
problem with my grades. So basically, the school kind of overlooked it for the most part.
So, please explain sweet potato and marshmallows for people who aren't American and shrink and
whore at the concept. There's no way to explain that. You just have to taste it. It looks horrible,
but it is good. Just try it if you're ever offered.
Did you propose to your girlfriend now?
Did she propose to you or a more mutual, sure, let's get hitched situation? I proposed to her
twice. The first time was at a Steve Earle concert, and I was very much like, hey, I'm going to marry
you one day. But let's just say I had been celebrating a lot at that concert, and that's
pretty much the last thing I have a clear memory of. So she said that didn't count, and I needed
to do it again, which I did later in a kind of more traditional way.
How many times a week does Miss Bow give you a well-deserved whooping in response to your
particular brand of humor? And there is a little note here saying,
talking about it being a verbal whooping and clarifying that. Probably, I don't know,
once every three weeks, but it's not really my humor that gets me into trouble.
I would say that I'm a very focused person. Other people might say that I have obsessive
tendencies and tend to forget everything else that's going on around me.
Everyone has that one story about that one time the dog or cat or horse or whatever did that thing,
the thing that time. I want to hear your story about the thing that time.
Let's see. In
Destro, one of the dogs, one year, my mother-in-law was cooking something right after
Thanksgiving, and it was something that had like a heavy tomato sauce. And while it was cooking,
she was eating a little bit of a tomato sauce, and I remember thinking, oh, I'm going to have
to eat that. And she was eating a little bit of a tomato sauce, and I remember thinking,
oh, I'm going to have to eat that. And she was eating a little bit of a tomato sauce,
and while it was cooking, he like stood and got in a position to allow him to eat out of
the cooking pot without burning himself. However, he ate so much, he got himself sick.
Let's see. What's the weirdest food you've ever eaten? Yeah, that's a long list.
I don't know. I'm going to say something.
I don't even know if that's the weirdest. I've eaten a lot of weird food in my life
and a lot of weird places. So let's just say goat. At the beginning or end of each video,
please step back so we can see your full shirt. Yeah, that looks weird in the framing,
but there will be something coming soon that will allow you to see the shirt.
So finally, I think I have figured that out. Do you think there's a chocolate syrup you could
apply to vanilla ice cream that would then qualify it as a chocolate ice cream? Instead,
do you think there's a volume of chocolate syrup? If so, what is that volume? Yes, that volume
exists. I don't know what it is. One to one by mass, I'm guessing. I have no clue. I have a
medical procedure Wednesday afternoon and another one early Friday. They're spinal, so no driving.
I'll be in a hotel by the hospital killing time. No Thanksgiving this year except gratitude for
getting this done. Yeah, and that's why I do these. Can you play any instruments? And if yes,
why am I 87% certain it's the bass guitar? Define play. I can play around with a couple of
instruments and yes, one of them is the bass guitar. I can kind of play the harmonica more than
anything else, but I still wouldn't want to get on stage doing it.
If you could invent something to help others, what would it be?
I don't know. Wow. Probably some mechanism for people who have a plan or a dream that would
help them become more self-sufficient or more financially stable that basically they could put
it out there and people could back it, kind of like Kickstarter or GoFundMe, but a little bit
more towards the idea of creating a long-term solution for that person rather than a one-off.
I received an invitation for Thanksgiving, but the salutation read turkey and nothing else.
Now maybe that's supposed to be humorous or maybe it's a test or maybe it's a revenge
Thanksgiving invitation. I don't know. How should I RSVP without offending the turkey?
RSVP with whatever dish you're going to bring.
My most favorite handmade or creative gifts you've received
and the least favorite of some that you've heard about.
Okay. So I've gotten a lot of stuff from people. I've gotten a lot of gifts from people.
I've gotten a lot of stuff from y'all that is handmade, such as the painting that's normally
behind me. I enjoy all of those. As far as things you didn't really like, I am very much a
it's the thought that counts kind of person. So I don't have an answer to that.
What's your favorite this shouldn't work, but it's delicious recipe that you feel like everyone
should know about? Cheesy rice, summer sausage, and ramen noodle wrapped up in a tortilla,
all smooshed up together. It's delicious. There's a whole bunch of people who are,
know where that recipe comes from. Let's see. Sing us a song, Mr. Piano Man. Yeah, no,
I can't sing anyway. And my throat is very sore right now. So if you hear it, that's a
lozenge rolling around in my mouth. But if you want a song, there's a Billy Joel song on the
main channel. If you go to the trailer, you'll get one there. We didn't start the fire.
Who's your favorite character on Inside Joth? And have you seen season two?
I have. I relate to Reagan on an abnormal level. And yeah, I watched most of season
two the first day came out. I sat down and watched it. Let's see. Do you feed
trimmings to Marilyn? No, but as you can see, she does come up and kind of mouth my beard
occasionally. What's the most precise you've gone when you do a, I want to say, lines?
I can't be the only one who's noticed this. It's not quite so this happened, I want to say, six
years, three months and five days ago, but sometimes it's close. I do know what you're
talking about. So when I say that, and I'm talking about dates or figures, I am fairly certain that
I'm correct. But I also did in fact check it before the video was recorded. So it's not like
when I say and it happened in, I want to say 1957, that I'm just taking a shot in the dark,
and I happen to be right. It's that I'm fairly certain that's the date. And if not, it's close
to that, but I didn't fact check it. So I want to use the proper provide some kind of disclaimer
that it's not fact checked. How do I get my mother to finally accept that Jane Fonda wasn't
a traitor to America during the Vietnam War? Good luck. Good luck. How old would you look if you
shaved? I don't know, maybe, probably 35, maybe. I don't know. I haven't seen my face in a really
long time. Do you stay in touch with the bows of the first through fourth columns? No, we're
compartmentalized. We're not allowed to talk to each other. What children's intellectual property,
book, movie, song, etc. Do you most often find yourself citing for some intellectual point?
Alice in Wonderland, probably. There's a lot of lessons to be learned there.
Though it seems like since the entire family is usually gathered together at Thanksgiving,
it's a good time to discuss deeply held personal beliefs like orientation, religion, politics,
and the current geopolitical landscape. What do you think? Yes. Yes. We have been told a long time
that we're not supposed to discuss politics or religion and stuff like this. Yeah, if you look
at that list of the stuff we're not supposed to discuss, that's what's getting people killed.
That's why people are dying. Yeah, I definitely think that it's appropriate to
discuss that stuff at any point in time. In fact, normally, and I'm still hoping to get it done,
even though we're running late here, I normally have a series that comes out,
How to Ruin Thanksgiving Dinner, talking about a specific topic to use with your
Fox News watching relatives.
When the pilgrims arrived, they were out of sugar, so they wouldn't have had cranberry sauce,
especially as they'd never seen the berries before, and the native populations didn't eat
them but use them for dyes. What other false authentic things are involved in the holiday?
Everything. It's made up. It's a myth.
The reality of what occurred is so far removed from the image that we have. It's not the same.
It's like one of those based on a true story things, but in the movie that's based on a true
story, there's a ghost and a demon and a god and a ghost and a demon and a god and a ghost.
It's that far removed. It's not real. Putting aside your love for wolves of all shapes and
sizes, do you agree with Hershel Walker's new stance on the ongoing conflict between
vampires and werewolves? I am so glad I have no idea what that means.
I'm not sure what that means. I'm not sure what that means.
I'm not sure what that means. I'm not sure what that means.
I'm not sure what that means. I'm so glad I have no idea what that means.
But now I feel obligated to go and look it up because it sounds like you said something wild.
What's your most exciting and or hilarious experience involving Thanksgiving cooking?
Uh, Baroness, my German shepherd, she got a small ham one year that was for her.
And the next year when I went to make a ham, she thought it was for her.
We have a lot of dogs taking food in my house. Not so much anymore, but when they were younger,
that was a very, very common theme.
Um, yeah, there will be, again, there are going to be some workflow changes.
You will see things from different angles. If you're in a hurry
to see the original shop from different angles, if you go to the Bow on the Roads channel,
you will find an episode about fundraising.
And in that, you actually get to see the shop from a whole bunch of different vantage points.
What's the contingency plan for a loss of the curious George ball cap or emblem?
Okay, so that sounds silly to worry about that. The reality is about two years ago,
I realized that that was something that could happen.
So I tracked down the company that I originally got the first one from,
and they didn't make them anymore. So I went to the company and I asked them,
and they didn't make them anymore. And I basically had to be like, okay,
how many of them do I have to order for you to make a run of them?
So I have 20 of these, those patches like stashed in different spots.
So that should never run. That should never become an issue.
Let's see.
Cranberry. I'm scrolling through some. Most of these are repeat questions.
To stick with the season, what do you cook for Thanksgiving and what method?
Oven smoking, deep fry. Normally ham and turkey and normally oven.
It's important to remember, I have a big, big, big oven.
It's important to remember I have a big family.
Let's see. What are your thoughts on ACDC, Like It, and DeLoreans?
Okay. So I like ACDC. As far as DeLoreans go, we actually saw one driving down the road the other
day and I was sitting there thinking like, man, I want one of those. And then I realized
I'm old and there's no way I'm getting that low to the ground and something anymore.
But they're cool. They're cool looking.
Tongue twister. How many times in a row can you say toy boat? Toy boat, toy boat, toy boat,
toy boat, toy boat, toy boat, toy boat. Apparently a lot. I don't think that one's going to get me.
What were you like as a little kid, specifically in school? I was the classic underachiever
as far as grades, attendance, everything like that. But the school that I went to,
heavily weighted tests. So yeah, I was very much the,
I was a troublemaker. I was a troublemaker.
When are we going to see the gardening video? Before spring, it will be out before spring,
which is when people are going to need it. It's in editing now. So everything's filmed.
It's now just being put together. And for those that don't know what that is,
we have been working on a video for months that lays out how to set up a community garden
in an inexpensive way. In fact, if you've been following on Twitter, the potatoes and, you know,
the cantaloupe and all of that stuff, the pictures that I've been posting, they're from that project.
Show us every single cat you have one by one on screen and tell us their names
so we can adore them from a distance. That is not possible right now.
They're actually not far from here, but they're not going to come over here and play with the
horses out running around and me doing something that they haven't seen before. And it would just
be weird. Would you rather live on a space station, a spaceship, another planet, or a house
or a house that floats around in the atmosphere? Yeah, that sounds horrifying. Another planet.
Funniest alien first contact scenario you can think of. Them showing up and
we're the descendants of a crashed
previous expedition or something like that. And of course, us being us, we attack them for it.
Let's see.
Why is everything actually Barack Obama's fault? You know why. You know why they frame it that way.
They can play on people's biases, either via skin tone or throwing out that middle name
and get people angry and emotional and then they lose all logical thought process.
Your favorite punk song. I can't answer that actually. There's a long running gag on the
channel. It's by David Rovix and it's called I'm a Better Blank Than You.
Okay, so I guess we're going to wrap it up here. There's still a whole bunch more,
but honestly my throat is killing me and that seems like a good place to stop. So I hope
y'all enjoyed this and I hope y'all have a good couple of days. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}