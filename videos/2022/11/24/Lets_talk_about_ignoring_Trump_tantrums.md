---
title: Let's talk about ignoring Trump tantrums....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XyqJjjcTY58) |
| Published | 2022/11/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Trump's behavior as temper tantrums is common, with the suggestion to ignore it like a child throwing a fit at home.
- Trump's rhetoric and tantrums impact others and cannot be disregarded like a child having a tantrum in isolation.
- A more fitting analogy is Trump as a disruptive student in a classroom, where his behavior influences others.
- Ignoring Trump's behavior leads to his followers chanting along with him, escalating to drastic actions like storming the principal's office.
- Trump's rhetoric must be countered using emotional appeals or factual arguments because it affects a wider audience and cannot be left unaddressed.
- The decision to stop talking about Trump lies with the Republican Party and his followers, not with individuals tired of discussing him.
- Trump's influence diminishes when his supporters recognize the harm in following him, leading to his behavior being inconsequential and easier to ignore.
- Despite Biden's victory, significant portions of the Republican Party still support Trump, necessitating continued engagement to counter his rhetoric.
- The analogy underscores the influence of all voters, including easily influenced individuals, in the political landscape.
- Trump's demands extend beyond mere tantrums, posing a threat to the rights of millions, making it imperative to address and counter his actions.

### Quotes

- "Trump's rhetoric must be countered using emotional appeals or factual arguments because it affects a wider audience and cannot be left unaddressed."
- "The decision to stop talking about Trump lies with the Republican Party and his followers, not with individuals tired of discussing him."
- "Trump's demands extend beyond mere tantrums, posing a threat to the rights of millions, making it imperative to address and counter his actions."

### Oneliner

Trump's behavior cannot be ignored like a child's tantrum; it must be countered due to its wider impact on others and political ramifications.

### Audience

Political activists and engaged citizens.

### On-the-ground actions from transcript

- Convince Republican Party members to re-evaluate their support for Trump (implied).

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's analogy of Trump's behavior in the political landscape.

### Tags

#Trump #PoliticalAnalysis #RepublicanParty #CounterRhetoric #VoterInfluence


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Trump and temper tantrums
and whether it's best to just ignore them or not.
Because I got a message, and I have seen this sentiment
expressed all over the place.
And the general idea is that Trump is a child,
and young Donnie is throwing a temper tantrum.
And that's what his rhetoric is.
And the best move is to just ignore it.
That analogy assumes that young Donnie is your child,
and you're at home.
And that's not the situation.
Young Donnie's rhetoric, his tantrums,
they don't exist in the vacuum of his room.
If you have a child at home who's
throwing a temper tantrum, sure, letting
them cry it out or whatever, that's a method.
I don't know that it's the best method, but it is a method.
But it doesn't fit with the situation
we're in nationwide, because young Donnie is not
in a vacuum.
His tantrum doesn't exist in a vacuum.
I think a better analogy would be
that you're a substitute teacher in a kindergarten class,
and young Donnie is one of your students.
And he throws his tantrum there.
You can't ignore it, because it's a disturbance
to the rest of the class, right?
And if young Donnie is over there banging on his desk,
screaming, we want candy, we want candy,
what happens if you ignore it?
If you ignore it, other easily impressionable children,
they start banging on their desk too,
chanting right along with him, build the wall.
Sorry, we want candy, we want candy.
You ignore it longer, eventually he
riles them up to the point where they
storm the principal's office.
Doesn't work.
It does not work, because his tantrums, his rhetoric,
it's not in a vacuum.
It has to be countered.
You can try using emotional rhetoric,
sending it the other direction.
You can try destroying it with facts and logic.
There are a bunch of different ways that it can be countered,
but it has to be countered, because it
doesn't exist in a vacuum.
Believe me, I understand the sentiment.
I cannot wait for the point in time
where I can go an entire week without typing the phrase,
let's talk about Trump and.
But we're not there yet.
And we don't get to make that decision either.
It's not up to us.
It's up to the rest of the class.
It's up to the Republican Party.
When the rest of the class, when those other easily
impressionable children, when they decide that they're not
going to follow him anymore, when they decide that he's
a detriment to them, when they decide that following him
leads to a loss of recess, that's
when his tantrums start to exist in a vacuum, when they don't
really matter, when we can ignore them.
It's up to them.
You would think that turning President Biden
into a historically winning president
would have been a sign, but it doesn't appear so.
There are still large segments of the Republican Party
that are still banging on their desks with him.
So we end up still talking about him,
because we have to counter it.
And the other part of this analogy
that is incredibly important to understand
is that in this world, all of those children,
they get to vote.
Their vote counts just as much as yours,
including the easily impressionable ones.
And young Donnie isn't banging we want candy.
He's banging on his desk, screaming and demanding
to take the rights away from millions.
Can't ignore it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}