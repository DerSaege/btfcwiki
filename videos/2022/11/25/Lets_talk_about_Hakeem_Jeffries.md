---
title: Let's talk about Hakeem Jeffries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A6kHh2eRx_4) |
| Published | 2022/11/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on Representative Hakeem Jeffries as Pelosi's potential replacement, likely the favorite for the position.
- Jeffries is poised to lead the Democratic Party in the House and could become the first black Speaker of the House by 2025.
- Outlines Jeffries' qualifications, including his educational background and political experience in Congress.
- Views Jeffries' potential leadership during a Republican-dominated House positively, noting Pelosi's presence as a resource until 2025.
- Expects Jeffries to redefine the term "corporate dim" by being pragmatic and open to incremental change while still leaning towards progressive beliefs.
- Suggests Jeffries could be even more powerful than expected due to the fractured nature of the Republican Party and potential infighting.
- Notes Jeffries' strong support in his blue district, enhancing his longevity in a leadership role.

### Quotes

- "He'll be more comfortable cutting deals."
- "In this position, he's going to be a corporate dim."
- "It looks really bad if the leader of your party in the House loses their election."

### Oneliner

Representative Hakeem Jeffries, a potential future Speaker of the House, brings a pragmatic approach that may shift the Democratic Party slightly leftward.

### Audience

Politically engaged viewers

### On-the-ground actions from transcript

- Support progressive candidates in your local elections (implied)
- Stay informed about political shifts and party leadership changes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Representative Hakeem Jeffries' potential as Pelosi's replacement and his likely impact on the Democratic Party.

### Tags

#HouseSpeaker #DemocraticParty #ProgressiveShift #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
Let's go again.
So today we are going to talk about Pelosi's replacement,
presumptive replacement, Representative Hakeem Jeffries.
Seems to be the favorite to get the slot.
It's not confirmed yet.
It's not set in stone,
but it seems like at this point,
it's going to be his position.
And everything between now and then is just a formality.
Now, this will place him in a position
where he is leading the Democratic Party in the House
and he is on track to become
the first black Speaker of the House as early as 2025.
So, obvious question.
Does he have the qualifications?
Yeah.
I don't remember where he did his undergrad.
His master's is from Georgetown.
Then I think he went to NYU Law,
clerked for a little bit,
was in the State Assembly in New York,
has been in Congress about 10 years.
So long enough to know how things work,
not as long as Pelosi, but that's a good thing.
This is a generational shift.
So he's not expected to have 20 years in Congress.
This is a move to shift the thought of the Democratic Party.
There are a lot of people who are saying
that he's going to have a hard time
because he's coming into this position,
he'll be going into this position with a Republican-dominated House
that is, let's just say, combative.
I disagree.
I disagree with that line of thinking.
Pelosi is stepping down from the leadership position.
She has her seat until 2025.
So he's not just being thrown out there.
He has somebody that he can turn to and say,
hey, what do I need to do here if he needs it?
And I would imagine that has a lot to do
with why she's stepping down
while she still has time in her seat.
So I don't think that he is going to end up over his head.
He'll have resources.
Okay, now the important question
to most people watching this channel.
Is he going to be another corporate dim quote
from like 15 messages?
And the answer to that is, of course, yes.
But he will redefine what corporate dim means.
Theoretically, Jeffrey's caucuses
with the progressive caucus, with AOC and that crew,
he is far more pragmatic and more open to incremental change,
which is probably what led him to be in the position
that he's headed into.
He will be more comfortable cutting deals.
But it appears as though his base beliefs
are more to the progressive side.
Him in this position moves the Democratic Party
a little bit to the left.
Not as much as most people watching this channel
would want them to move,
but certainly more to the left than Pelosi.
It's a slight shift, not a huge one.
Even if his base beliefs and what he believes
are much further to the left.
In this position, he's going to be a corporate dim.
So those beliefs will be tempered
in the name of being pragmatic
and making sure that the perfect
doesn't become the enemy of the good and all of that stuff.
Okay, so what else?
There is a slim chance, due to things that could occur
that are plausible, I don't know if they're probable,
but he actually could become Speaker before then.
There are a couple of scenarios where that could occur.
Even without that, I'm going to say that he is probably
going to be the most powerful person
in the House of Representatives walking in.
And I know that's counterintuitive
because the Democratic Party
doesn't have control of the House.
And that's true, but the Speaker of the House,
whoever it ends up being when it's all said and done,
they don't have control of the Republican Party.
The Republican Party is very fractured.
So I would suggest that Jeffries
is going to actually control more votes
than his Republican counterpart
because of the Sedition Caucus and all of those people.
There's going to be a lot of infighting.
So given the fact that he's going to control more votes
in a House that is a just razor-thin majority,
he's going to have a lot more power
than I think people are expecting him to,
maybe even more than he thinks he's going to have.
So that's a brief synopsis of him,
where he's at, what he's likely to be like.
Another good thing about him
as somebody in a leadership position
is that he's in a very blue area.
I want to say he got more than 70% of the vote last time
or something like that,
which is good when you're talking about somebody
who's going to be in a leadership role
because you want them there a long time.
It looks really bad if the leader of your party
in the House loses their election.
So that also helps his standing
because it's unlikely that he'll lose his seat.
So brief overview, yeah, not a bad choice.
For most people watching this channel,
not going to be as progressive as you want him to be,
but probably going to be a bit more progressive than Pelosi.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}