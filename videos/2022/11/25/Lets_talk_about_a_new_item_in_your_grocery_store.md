---
title: Let's talk about a new item in your grocery store....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FMuq9MaX64M) |
| Published | 2022/11/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Historic first: FDA approves lab-grown chicken for sale, soon in grocery stores.
- Upside's "cultured chicken" gets the green light, awaiting USDA inspection.
- Carbon footprint of lab-grown meat may be high due to current energy infrastructure.
- Transition to lab-grown meat could significantly reduce climate change impact in the long term.
- Public response to lab-grown meat availability remains to be seen.
- Beau supports lab-grown meat due to cleaner production environment and ethical considerations.
- Predicts public anxiety and debate over lab-grown meat in the future.
- Views lab-grown meat as a positive long-term benefit for everyone.
- Beau expresses willingness to consume lab-grown meat based on his experiences on ranches and slaughterhouses.
- FDA open to discussing similar approvals with other producers.

### Quotes

- "This will probably fuel a lot of anxiety in more anti-science circles."
- "I think that this is probably going to be a huge benefit to everybody over the long haul."
- "I'm pretty cool with something that came out of a much cleaner environment."

### Oneliner

FDA approves lab-grown chicken, sparking debates on climate impact and ethical consumption, with potential long-term benefits.

### Audience

Climate-conscious consumers

### On-the-ground actions from transcript

- Support and advocate for sustainable food production practices (implied)
- Stay informed about advancements in food technology and their environmental impact (implied)

### Whats missing in summary

The full transcript provides deeper insights into the potential implications of lab-grown meat on climate change and ethical considerations, encouraging a shift towards sustainable food production practices.


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about a historic first.
Something like this has never happened before and a couple of questions that it prompted
and what is in theory going to be in your grocery store soon. The FDA has given its nod
to a company called Upside to start selling a new product. That product is chicken,
but it's made without the chicken. I believe they're calling it cultured chicken or something
like that. The rest of us are probably going to call it lab-grown meat. The FDA has said
that they don't have any more questions when it comes to whether or not it is safe for human
consumption. Now the company still has to go through a USDA inspection. The thing is, I've
been on ranches. I'm fairly certain that a lab is going to pass. So I don't see that as being a major
holdup. That is probably going to occur. This is the first and only company that has gotten this
designation so far, but the FDA has made it very clear that they are willing to talk to other
producers when it comes to this. So we will soon have lab-grown meat available in the stores.
We will have to see how the public responds to it. Now, two questions. Is this going to be a
whole lot better for climate change? Maybe. We don't know yet. So food production is like one
of the big drivers of climate change. The thing is, with it being done in a lab the way it is,
the carbon footprint may still be pretty high, but that's because our energy infrastructure hasn't
switched over yet. So I would suggest that right now it's going to make a difference,
but it won't be huge. But as we transition, this will be huge, assuming that the public
is cool with this, is cool with this product. I think that over the long term, it will be
a significant reduction in the forces that drive climate change. It's not going to be an immediate
thing. And then the next question is, would you eat lab meat? Yeah, absolutely. Again,
I've been on ranches and in slaughterhouses. I'm pretty cool with something that came out of a
much cleaner environment. And then you have the ethical considerations as well. This is,
I guess, harm free would be a way to look at it. So you have that as well. I would imagine
that as soon as other news dies down, this is going to become a topic of discussion.
This will probably fuel a lot of anxiety in more anti-science circles. But this is not something
that I think is a bad thing. I think that this is probably going to be a huge benefit to everybody
over the long haul. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}