---
title: Let's talk about paywalls and democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Aau7ofIdR28) |
| Published | 2022/11/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses his stance against putting content behind paywalls due to his unique goals and approach.
- Acknowledges that other creators have their reasons for structuring their content with paywalls.
- Mentions the confusion surrounding his video titles, which have become a meme.
- Points out how putting content behind paywalls can prevent people from accessing information they might need.
- Talks about a thread tagging him where he couldn't access articles without paying, contrasting it with free access to questionable content on other platforms.
- Questions the impact of paywalls on democracy and informed citizenship.
- Suggests that providing better insight and facts at a cost leads to a more emotionally charged audience.
- Recognizes that some creators rely on paywalls for their business models while expressing gratitude for his supportive audience.
- Believes that limited options may lead people to choose bad ones due to information scarcity.
- Shares a metaphorical statement about drinking sand in an information desert due to lack of funds.

### Quotes

- "If you're in an information desert because you lack funds, eventually you'll drink the sand."
- "I don't think this person's wrong."
- "Democracy, the American experiment. It's advanced citizenship."
- "My titles at this point have become a meme about how confusing they are."
- "I know that just the title can stop somebody from listening and getting information that they might really need."

### Oneliner

Beau shares insights on paywalls, democracy, and informed citizenship, questioning the impact of limiting access to information on societal discourse and decision-making.

### Audience

Content creators, viewers

### On-the-ground actions from transcript

- Support content creators who provide valuable insights without paywalls (exemplified).
- Advocate for accessible information and fight against information scarcity (exemplified).

### Whats missing in summary

The full transcript provides a detailed exploration of the impact of paywalls on information accessibility, democracy, and informed citizenship, urging reflection on the consequences of limiting access to vital information.

### Tags

#Paywalls #Democracy #InformationAccessibility #ContentCreators #InformedCitizenship


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about paywalls
and American democracy and how the two relate
because of a unique chain of events.
In a recent video, I mentioned that I am very much
against putting my content behind paywalls.
It's not something I want to do.
Some people sent messages asking if, you know,
that was a shot at other creators
or something like that.
No, everybody has their own reasons
for structuring their content the way they do.
It's not necessarily a bad thing for my goals.
And this channel, it doesn't work.
You know, my titles at this point have become a meme
about how confusing they are.
They hit certain topics.
people have a general idea of what they're going to be about, but they don't
necessarily know what they're going to hear, which means I can reach across. I
can get people to listen to these videos that would not listen to it if they knew
they were about to hear something they were going to disagree with. So that, I
mean, I know that just the title can stop somebody from listening and getting
information that they they might really need, putting it behind a paywall, there
is no way anybody on the right is going to sign up for a subscription for for
me. I don't want to say anybody, very few, very few. So I had a bunch of questions
come in about this and shortly before then I got confirmation of something
that I have believed, but I never really
had any even anecdotal evidence of it.
But then I got tagged in this thread.
I can't figure out what the heck is going on with Nancy Pelosi
and her husband, because none of the gosh darned sites reporting
on it will let me read the articles
without asking me for money.
Meanwhile, I can read some absolute bull on Fox News or InfoWars for free.
Gee, I sure wonder why American democracy is dying.
I'm sure it has nothing to do with any of this.
Democracy, the American experiment.
It's advanced citizenship.
You have to be informed.
You have to work for it.
so on and so forth. If the outlets that are peddling emotional reactionary type
stuff, if they are giving away their content for free to the user and those
that are providing better insight and more facts are charging, you're going to
have a more emotionally charged group of people.
So it isn't a shot at other creators when I say that.
I know there are a lot of people who that is their whole
business model.
The only reason they can make content is
because of the paywalls.
I'm lucky.
I have y'all, and y'all are incredibly
supportive of this channel.
But for me and for my goals, I can't do that.
I can't put it behind a paywall.
And I don't think this person's wrong.
I think that if the only options that are readily available
are bad ones
those are going to be the ones that
that people choose
uh...
if you're in
an information desert
because you lack funds
uh...
eventually you'll drink the sand
it's just a thought
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}