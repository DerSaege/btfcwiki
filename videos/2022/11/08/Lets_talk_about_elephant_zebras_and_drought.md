---
title: Let's talk about elephant, zebras, and drought....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Uw9uwNSQXO4) |
| Published | 2022/11/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the impacts of climate change on animals in Kenya, specifically elephants, giraffes, and zebras.
- Noting how animals can serve as indicators of shifting climates before people are severely impacted.
- Sharing a report from the Wildlife Service in Kenya that details the loss of various animal species due to drought.
- Mentioning the staggering numbers of animals lost, including endangered species like grevy's zebras.
- Expressing concern over the lack of a plan to address the situation and the challenges of providing water and salt licks to the remaining animals.
- Emphasizing the importance of not overlooking the signs of environmental distress and the need to take action.
- Describing how the loss of these animals will lead to ongoing problems for both wildlife and local communities in accessing food and water.
- Implying that ignoring these warning signs could have severe consequences for the region's ecosystem and inhabitants.
- Noting that the current situation is a clear indication of the impact of climate change on the area.
- Drawing attention to the broader implications of the drought, affecting both animals and humans in the region.

### Quotes

- "This is one of those things that we really shouldn't ignore, but we probably will."
- "It's clear from this that there will be ongoing problems, not just with animals, but with people getting food, water in this region."

### Oneliner

Addressing the impacts of climate change on animals in Kenya reveals the urgent need to take action to prevent ongoing problems for both wildlife and local communities.

### Audience

Conservationists, Environmentalists, Wildlife Advocates

### On-the-ground actions from transcript

- Support local wildlife conservation efforts to protect endangered species like grevy's zebras (suggested).
- Join initiatives working to address climate change impacts on wildlife and habitats (implied).

### Whats missing in summary

The emotional plea and urgency conveyed by Beau in addressing the immediate need for action to prevent further loss of wildlife and potential food and water shortages in the region.

### Tags

#ClimateChange #WildlifeConservation #Kenya #Drought #EnvironmentalImpact


## Transcript
Well, howdy there, internet people. It's Billy Young. So today we are going to talk about
elephants and giraffes and zebras, oh my, and drought in Kenya. Normally, when we talk
about the impacts of climate change, we talk about how it hits people and how people are
affected. But there are obviously a whole bunch of other concerns, and there are things
that we will notice before the people start being impacted as severely as they eventually
will be. And one of the good canaries in the coal mine is animals and how they are responding
to shifting climates, particularly drought. We have a report from the Wildlife Service
there in Kenya. 205 elephants, 512 wildebeest, 381 zebras, that's the common type of zebra,
51 buffalo, 12 giraffe, and 49 grevy's, zebras. Those are endangered. Gone. Gone. Currently,
there isn't a plan to deal with this. You have experts talking about what to do. I mean,
a lot of them are saying that the solution is to bring in water. That's a lot of water
and salt licks. You're talking about thousands upon thousands of gallons of water a day.
I honestly don't know where they're going to get it, how they're going to manage getting
it to where it needs to be. This is a sign. I'm sure it will probably be overlooked like
a lot of others. This is one of those things that we really shouldn't ignore, but we probably
will. These animals, they're adapted to this area. And it's a harsh area to begin with.
When you start experiencing this kind of loss due to the climate, it's something to pay
attention to. It's clear from this that there will be ongoing problems, not just with animals,
but with people getting food, water in this region. It certainly appears that the devil
has come to Savo, but the devil's drought. Anyway, it's just a thought. Y'all have a
good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}