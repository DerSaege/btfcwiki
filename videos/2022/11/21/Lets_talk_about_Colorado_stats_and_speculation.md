---
title: Let's talk about Colorado, stats, and speculation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SdSmVsAeTvg) |
| Published | 2022/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculated on a potential domestic violence connection in recent Colorado incident causing pushback.
- Many mass incidents have a domestic violence connection, with statistics showing as high as 68.2%.
- Noted that domestic violence is underreported and a significant factor in mass incidents.
- Advocated for closing all loopholes related to domestic violence in gun control legislation.
- Encouraged individuals with past domestic violence incidents to support exploring the link between domestic violence and mass incidents.
- Believed that restorative justice is possible but emphasized the need for safeguards.
- Stressed the importance of studying the domestic violence connection to find effective solutions.
- Asserted that addressing domestic violence links may be more impactful than focusing on specific weapon types.
- Mentioned a fundraiser for domestic violence shelters coming up soon.

### Quotes

- "When it comes to mass incidents of this sort, these statistics are wild."
- "This is where you need to look."
- "There's a link. It needs to be explored."
- "The solution is there."
- "It's just a thought."

### Oneliner

Beau speculates on the domestic violence connection in the recent Colorado incident, urging exploration of the link to find effective solutions in addressing mass incidents.

### Audience

Advocates, activists, community members.

### On-the-ground actions from transcript

- Support exploring the link between domestic violence and mass incidents (suggested).
- Advocate for closing all loopholes related to domestic violence in gun control legislation (implied).
- Get ready to participate in the upcoming fundraiser for domestic violence shelters (implied).

### Whats missing in summary

Beau's personal anecdotes and experiences related to the topic.

### Tags

#Colorado #DomesticViolence #GunControl #Statistics #Advocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Colorado,
and statistics, and speculation, and pushback.
In the recent video about what happened in Colorado,
I speculated.
And that caused pushback that I receive
any time I bring this topic up.
It caused it to be more specific.
It is normally incredibly vague, so vague that until now,
I had no idea what the pushback actually was.
So we're going to go over this and kind of talk about it.
And there were also a lot of people
who did not know what I meant, because I haven't
talked about it in a while.
In that video, I said something to the effect of,
we're probably going to find out there's a DV connection.
And a lot of people asked what that meant.
DV, domestic violence.
When it comes to mass incidents of this sort,
these statistics are wild.
If you are looking for a marker, some kind of indicator,
something to be used to kind of guide things,
that's where you need to look.
Depending on the study, most times on this channel,
I use the one that is 60%.
60% of all of these mass incidents have a DV connection.
Whether it be the suspect has a documented history,
or the incident itself started that way.
60%.
Just so everybody knows, that's one of the lower numbers.
One of the more recent studies I've seen has it at 68.2%
for documented connections.
And please understand, this isn't something
that's well-documented.
This is one of the most under-reported things
in the US.
68.2%.
So I speculated and said, hey, we're
probably going to find out that happened.
And some people had issue with me speculating.
I would point out that, yes, June of 2021,
it does appear that the suspect in the Colorado incident
had another altercation, this one involving his mother,
that there was some kind of very weird resolution to.
But it falls into that category.
It's that speculation is based on those statistics.
So if you are somebody who is looking for gun control,
is looking for a solution to this, look there.
You're going to find your answer there.
There is a link.
OK.
So anytime I talk about that, I'm
going to get pushed back from people.
And it's normally very vague.
I have always chalked it up to the fact
that I have very strong opinions about this topic,
and I am very open with them.
I have a personal motivation.
There is a reason this channel does a fundraiser for DV
shelters every year.
Incidentally, that's coming up in like a week.
So get ready for that.
But this time, it was a little bit more specific.
And I always, I did, I always thought
it was because people didn't want
to push because of my background,
my personal motivations involving this.
Or it was a situation where they didn't
want to admit that they had a DV history.
So lots of pushback on this one.
And one person finally kind of put all of it out there.
And he was like, look, when I was, I think he said,
20-something, he got into a fight with his brother.
They were living together at the time.
That counts as domestic violence.
It's been 10 years, never been in trouble since.
I don't think I should be penalized for this.
And if they do legislation, it would include all of us.
And I started to think about the fact
this may have something to do with the pushback.
Because my number one, if there was
one piece of gun control legislation
that I definitely think needs to happen,
it's closing all loopholes when it comes to domestic violence.
All of them.
So with this in mind, here's the thing.
If you are in that category, if 10 years ago you
had an incident like this, you need
to support looking into this link more than anybody.
Because anecdotally, I don't have a study on this.
But from my personal observations of it,
I can tell you that it looks like the incident occurred
in 2021 and the mass incident occurred within three years.
I am a supporter of the idea of restorative justice.
I definitely believe people can change in all of that.
At the same time, there has to be a safeguard.
Support looking into this link.
And odds are the data will clear you.
But it's something that is so pronounced,
it cannot be ignored.
It's such a constant factor in these events,
particularly those that are in public.
It is such a common factor that it's something
I feel comfortable speculating about.
It's almost always there.
There's a link.
It needs to be explored.
It needs to be studied.
Because the solution is there.
That's where you're going to find it.
It's going to be far more effective than anything
having to do with any certain type of model of weapon
or anything like that.
This is where you need to look.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}