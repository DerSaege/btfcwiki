---
title: Let's talk about explaining Twitter to the working class....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BjI7tQ5Wm9o) |
| Published | 2022/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the notion that it's impossible to explain Twitter's situation to the working class.
- Mentions reading think pieces claiming the working class can't comprehend certain concepts.
- Questions the idea of dividing the working class into "laptop class" and working class.
- Illustrates a scenario involving a construction worker to explain complex changes at work.
- Expresses skepticism towards those who believe the working class cannot grasp certain economic concepts.
- Emphasizes the importance of directly engaging with the working class rather than making assumptions.

### Quotes

- "Just because we talk slow doesn't mean we think slow."
- "Your boss is asking you to show up for work and you say no? And you're already making 125 grand a year."
- "If you are part of the think peace crowd in these magazines, maybe talk to the working class."

### Oneliner

Beau explains Twitter's situation using a construction scenario, challenging assumptions about the working class's understanding.

### Audience

Working-class individuals

### On-the-ground actions from transcript

- Talk directly to the working class to understand their perspectives (suggested)
- Bridge the gap between different socioeconomic groups through open communication (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on the working class's ability to comprehend complex issues when explained effectively.


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today we're gonna talk about what's going on at Twitter
and try to explain it to the working class.
I have read a number of think pieces over the last few days
that have gone to great lengths to explain
that it is bizarre or impossible
to explain what's happening at Twitter to the working class
because they just don't understand
and they think it's so strange.
Your boss is asking you to show up for work and you say no?
And you're already making 125 grand a year.
And when you say that to the working class,
well, they just don't understand.
I also saw the phrase laptop class for the first time.
See, I don't know what that is.
I'm pretty sure that's just the working class.
I would point out that a guy that came out to pour concrete
for us was using a laptop.
But that idea of the laptop class versus the working class,
that seems to me to be a little bit more
than just a working class.
That seems like an attempt to divide the working class.
And if you're doing that, I've got to ask why.
I have questions.
Because it seems like your actual goal
may not be to better the conditions of the working class,
but in fact, divide them so they can't get better conditions.
So since it is bizarre and impossible
to explain what's happening at Twitter to the working class,
give it a shot.
You're a framer.
You frame houses.
You or your boss bid a job.
You're going to build ranch style houses.
They're 2,000 square foot each in a subdivision
just outside of Atlanta.
You build two of them.
And then the development company gets bought out
by somebody else.
And you're standing around on Monday looking around,
wondering what's going on.
Because there's no foundation for the next house.
And that's when the developer shows up.
Says, oh, oh, see, we paid $44 billion.
So we've got some changes.
And you're just going to have to be super hardcore about this.
The new houses, they're going to be 6,000 square feet.
They're going to be three stories.
And they're going to be in Albany.
And you're going to get paid the same amount.
By the way, we fired like a third of the framers.
You're going to do their job too and not
get paid any more for it.
We've got to make that money back somehow.
Use our human capital, I guess.
I'm fairly certain that the working class
can understand that.
And as far as, well, they already make $125,000 a year,
I know.
Because us folk that sound like this,
yeah, we don't know anything about cost of living
differences.
We don't have access to the internet.
We don't know that gas always costs more in California.
We might even have friends who live in California
who show up at our houses and be like,
what's the mortgage on this?
Less than your apartment.
These aren't hard concepts.
If you are part of the think peace crowd in these magazines,
before you talk about what the working class can't understand,
maybe talk to the working class.
Just because we talk slow doesn't mean we think slow.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}