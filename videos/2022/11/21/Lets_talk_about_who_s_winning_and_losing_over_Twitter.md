---
title: Let's talk about who's winning and losing over Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WZU1ZOr298Q) |
| Published | 2022/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines the impact of upheaval at Twitter on various social media platforms.
- Elon Musk is perceived to be losing control amidst the Twitter chaos.
- Other social networks like Co-host, tribal, counter-social, and mastodon are gaining new subscribers.
- People are not gravitating towards Facebook's metaverse project as their new social media home.
- Many believe in the concept of a metaverse in the future, but current trends show otherwise.
- The alternative networks being discussed share similarities but offer different blends of features.
- People seem hesitant about embracing the idea of a metaverse due to issues in its development process.
- Facebook's new project is not receiving as much attention as other social media platforms.
- The future of social media might see significant changes due to Musk's influence on Twitter.
- Facebook may be ahead of its time with its new project, potentially facing challenges in gaining popularity.

### Quotes

- "One, tried to reinvent the creator economy."
- "It doesn't appear that people are ready for an idea like metaverse."
- "Facebook is just a little bit ahead of its time."

### Oneliner

Elon Musk loses control as alternative social networks gain traction, leaving Facebook's metaverse project behind.

### Audience

Social media enthusiasts

### On-the-ground actions from transcript

- Join alternative social networks like Co-host, tribal, counter-social, or mastodon (implied)
- Stay updated on the developments in social media platforms (implied)

### Whats missing in summary

Insights on the potential implications of the shifting social media landscape and the need for adaptability.

### Tags

#SocialMedia #Twitter #ElonMusk #Facebook #Metaverse


## Transcript
Well, howdy there, internet people. It's Beau again.
So today, we are going to talk about who is losing the most.
Who's coming out on bottom with all of the upheaval at Twitter.
The conventional wisdom, the answer that is right there staring you in the face,
may not actually be the outfit in the worst trouble because of what's going on there.
Like the clear answer is Elon Musk is losing this.
It's spiraling out of control.
Maybe he can regain control of it later, but at this moment, it is not looking good.
So that would be the first option you would go to.
If you scroll Twitter, you will find people talking about where they're going.
Co-host, tribal, counter-social, mastodon, all of these other social networks.
And they could be perceived as being winners in this because they're picking up a lot of
new subscribers.
And this is true.
But that doesn't mean that Twitter is necessarily the biggest loser here.
Co-host, tribal, counter-social, and mastodon.
That's what's being listed.
That's where people are going.
Do you know where people aren't going?
Where people aren't saying that they're going to head to as their new social media home?
The microverse or metaverse, whatever it is.
Facebook's project?
If you look, it's barely mentioned.
Social media networks that are brand new, social media networks that are perhaps a little
unwieldy at times, are getting greater traction than the flagship from Facebook.
People are talking about these other networks, not the metaverse.
You know, a lot of people, when you talk about futurism, when you talk about the way things
will inevitably go, most people believe that there will be something like the metaverse.
But it doesn't appear at this moment that that's where people want to head.
The various networks that are being talked about, none of them are the same.
I mean, they all share similar features.
It's a social media network.
Mastodon is very, very similar to Twitter.
But they are different.
Many of them are blends of Twitter and Facebook.
People seem comfortable with that.
It doesn't appear that people are ready for an idea like metaverse.
And that may just be because of the issues that have come along with the development
process.
But I would take it as a not so great sign if I was hoping that Facebook's new project
was going to come out on top.
Because when you look, when you search for the different social networks on Twitter and
find out where people are going, most times it's not even in the list.
It's kind of an afterthought, if anything.
So while we can definitely expect there to be a lot of changes with social media because
of Musk taking over Twitter, it might be that the giants are running out of time.
Because one, tried to reinvent the creator economy.
And it may be that Facebook is just a little bit ahead of its time.
Anyway, it's just a thought.
And I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}