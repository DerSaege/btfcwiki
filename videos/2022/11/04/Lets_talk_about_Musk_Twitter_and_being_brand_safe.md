---
title: Let's talk about Musk, Twitter, and being brand safe....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=byazeV3gfis) |
| Published | 2022/11/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk's recent purchase of Twitter led to various problems, including advertisers pausing advertisements.
- Musk claimed activists were pressuring advertisers, causing a drop in revenue for Twitter, even though content moderation remained the same.
- Advertisers are concerned about the context in which their ads appear online, as it can impact their brand image.
- Advertisers avoid advertising on platforms with hateful or controversial content to prevent being associated with it inadvertently.
- Musk's marketing approach should prioritize brand perception, as any negative associations with Twitter could impact advertisers' decisions.
- Advertisers follow trends and demographics, shifting towards more diverse and tolerant audiences to grow their brand successfully.
- Large companies tailor their advertising messages to different demographics, including those related to orientation, identity, and race.
- Musk's subscription-based Twitter model relies on maintaining a brand-safe environment to attract large advertisers.
- Right-wing social media networks struggle to attract advertisers due to bigoted content, limiting their growth and improvements.
- Musk's understanding of branding and the importance of brand safety for advertisers is critical for the success of platforms like Twitter.

### Quotes

- "When you are on social media, you are the product."
- "If Musk wants advertisers to stay on Twitter, Twitter has to be brand safe."
- "The reason that all of the right-wing social media networks that start don't really go anywhere is because nobody wants to advertise on them."
- "Understand when you are on social media, you're the product."
- "Advertisers are trying to reach you."

### Oneliner

Elon Musk's actions with Twitter reveal the critical importance of brand safety and advertiser perception in the online space, impacting platforms' success and growth.

### Audience

Social media users

### On-the-ground actions from transcript

- Support diverse and tolerant content on social media platforms by engaging with and sharing positive messages (implied).
- Encourage brands to prioritize brand safety and inclusivity in their advertising strategies (implied).

### Whats missing in summary

The full transcript provides a comprehensive analysis of the impact of brand safety and advertiser perception on online platforms, urging individuals to understand their role as consumers and the power of diverse and tolerant content in shaping advertising trends.

### Tags

#ElonMusk #BrandSafety #Advertising #SocialMedia #Diversity


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about something
that happened with Elon Musk, that is pretty humorous,
and being brand safe, and what that concept means,
because we had questions about it.
And I was going to do a video on it,
and then something happened with Musk
that makes this a very teachable moment.
So before we get into this, I want
to say that this video is free for everybody.
Feel free to enjoy it, except for Elon Musk.
If you're watching this on your honor
as a billionaire of humanity or whatever,
I would like you to send me $8, please.
OK, so if you missed it, Musk bought Twitter,
and there have been a number of problems since then.
Advertisers are now pausing advertisements.
Musk tweeted this out.
Twitter has had a massive drop in revenue
due to activist groups pressuring advertisers,
even though nothing has changed with content moderation,
and we did everything we could to appease the activists.
Extremely messed up.
They're trying to destroy free speech in America.
First, I want to point out that the Twitter fact check feature
hit this tweet and provided additional context,
because as Musk might say, there might be more to this story.
Now, I also want to point out, before we
get into brand safe content and all of that,
that there are a lot of people who
are saying that because of content and all of that,
that activists and companies not advertising
because of content, that is free speech.
It's not destroying free speech.
That is literally free speech.
That is a type of speech.
OK, so why do advertisers even care?
Why do advertisers care about what kind of content
they're advertising?
Now, the simple answer is, well, they
might get boycotted for supporting
something that was hateful.
Yeah, but that's actually not really the big part of it.
It's larger than that.
It's not just a general concept.
On the internet, advertising is contextual.
It's often linked to the content around it.
Disaster relief, you are much more
likely to get an ad about first aid
kits or emergency supplies because of that content.
The ads often link up to the type of content being displayed.
Sometimes it's through keywords.
There's a whole bunch of different ways that it happens.
Sometimes it's the pattern of the viewer.
Make no mistake, when you are on social media,
you are the product.
The social media company is selling your eyes
to advertisers.
That's how they make money.
So when it comes to a site like Twitter,
why do advertisers care if they have hateful content?
Because they might end up linked to it.
They might end up advertising on it unintentionally.
Now, Musk's claim here is that they haven't done anything.
Musk, above all people, should know
that when it comes to marketing, perception is reality.
He's built his career on this fact.
So if he takes over and the use of a certain racial slur
shoots through the roof, the perception
is it has something to do with him.
And I would suggest that the reality is
those people that would use it believe
he would let them use it.
And that's why it's happening.
There's also the problem with Musk himself tweeting out
a less than accurate theory about the Pelosi's.
That was probably a huge red flag for advertisers.
So how does the contextual aspect of it
play into the advertising?
And why is there this huge concern?
I want you to think about all of the stereotypes associated
with food when it comes to ethnic groups
and racial groups, the stereotypes that
deal with food.
We're going to keep this real simple.
Those stereotypes, most of them are not good.
They're hateful.
They're mean.
If that content is allowed to be put out there,
and we'll use Irish people.
Let's say it has to do with drinking or potatoes.
And somebody is making a tweet that is a racial stereotype.
And then an alcohol company or a mashed potato company
advertises below it because of the context.
It looks like the advertiser is cosigning that message.
That is really bad for brands.
They don't want to be in that situation.
So they don't advertise on places
that allow that type of content.
The other part of this that everybody
needs to pay attention to is that the demographics
of the United States are changing.
The US is becoming more diverse, more tolerant, more liberal,
more accepting.
That's the general trend.
I understand that there are outliers.
But even those outliers would say the country
is becoming more diverse.
I mean, they would complain about it
and act like it's the end of the world.
But they would admit that.
So what we're dealing with here is a choice
that advertisers have.
Like an advertising company, it wants to grow a brand.
It wants to increase that brand's market share.
If you have one demographic saying go woke, go broke,
and it's shrinking, and another demographic saying
go fash, no cash, and that one's growing,
which one are they going to cater to?
The one that's growing.
It's that simple.
This is pure capitalism.
The US, because it is becoming more diverse,
companies will start to tailor their advertising to that.
Understand a lot of the positive messages
that come out from large companies dealing
with orientation, identity, race, all of this, a lot of it
has absolutely nothing to do with them
caring about any of these groups as people.
They're markets.
They're demographics to target with advertising.
And that's it.
Sure, it helps.
Don't get me wrong.
Rainbow colored candy is actually beneficial
because it helps get that message out there.
It helps bring it into the mainstream.
And that's good.
But these companies, they're not doing it for that reason.
They're doing it for advertising purposes.
They are not going to want to have their content, say,
advertising a rainbow candy next to a tweet making
fun of that community.
If Musk wants advertisers to stay on Twitter,
Twitter has to be brand safe.
And if he's going to allow racial slurs, conspiracy
theories, stuff like this to flourish on Twitter,
it won't be brand safe.
What's more is his subscription model
is going to rely heavily on large brands
being willing to pay that.
They won't, not if it's not brand safe.
And that's the situation he's dealing with.
And that's the problem that he is going to face.
The reason that all of the right wing social media networks
that start don't really go anywhere
is because nobody wants to advertise on them.
So they can't make more improvements
because the content is bigoted.
I'm not saying that all conservatives are racist.
But I'm saying that conservatives
aren't going to be mad generally if they see a racist comment
on social media.
Brands will.
They don't want to be associated with it
because it cuts them off from other demographics,
from other markets, from other ways for them to make money.
Again, this is pure capitalism, nothing else.
The fact that Elon Musk is apparently just missing this
blows my mind.
His entire fortune is based on him
being really good at branding.
I don't know how he's missing this.
His money is really, it is, it's all about branding.
It's all about the perception of the company
more than the company itself.
How he's missing this as a critical factor
when it comes to these companies making the decision
to yank their advertising and those advertising dollars
is beyond me.
I do not understand how he doesn't get it.
And attributing it to activists and saying
that's the end of free speech just doesn't make any sense.
Because if it was activists, it's not,
but let's pretend that it is.
That is free speech.
But given that it's not, and it's just
a function of the market, it's just a function of capitalism,
I don't know what his comments are really intended to do
other than perhaps he believes that the conservative base is
growing.
And he just has bad information there.
The trends are super clear on this.
So maybe he believes that there is a market for a conservative
only social media network.
If there was a market for that, it
would have been Truth Social.
And Truth Social's numbers are not good.
So understand when you are on social media,
you're the product.
Advertisers are trying to reach you.
And with current trends, current demographics,
current thought process, they are always
going to side right now with the side that
wants more humanity, more tolerance,
because that's the growing market.
That's where they can grow their brand.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}