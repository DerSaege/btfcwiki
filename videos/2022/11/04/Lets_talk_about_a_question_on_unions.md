---
title: Let's talk about a question on unions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GYZuf25fprw) |
| Published | 2022/11/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the question of whether unions are for everybody and why one should support or join them.
- Responding to a viewer questioning the necessity of unions when well-paid workers go on strike for more.
- Explaining the perception of unions always wanting more and workers being dissatisfied despite being well-paid.
- Pointing out that companies always strive to make more profit each year, so why shouldn't workers want a share of that success?
- Clarifying that unions help workers negotiate for a fair share of the company's increased profits.
- Emphasizing that unions are formed not when conditions are great, but when they need improvement.
- Asserting that union workers deserve more because they are the ones generating the wealth for the company.
- Advocating for supporting and possibly joining unions to ensure workers receive their fair share of the value they produce.
- Mentioning that even the Starbucks union, though new, may eventually follow suit in seeking fair compensation.
- Encouraging viewers to understand and back the collective bargaining power of unions to improve workers' lives.

### Quotes

- "Union workers deserve more because they are the ones making the money."
- "Divided you beg, united you bargain."
- "Support and join unions to get your fair share of the value you produce."

### Oneliner

Beau explains why unions are necessary, even for well-paid workers, by shedding light on the power of collective bargaining to secure fair compensation.

### Audience

Workers, supporters

### On-the-ground actions from transcript

- Support a union (suggested)
- Join a union (suggested)

### Whats missing in summary

The full transcript provides a detailed explanation of why unions are vital for all workers, regardless of their current pay, to secure fair compensation and improve working conditions.

### Tags

#Unions #CollectiveBargaining #FairCompensation #SupportWorkers #JoinUnions


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about unions and whether they're for everybody
and why you should support a union, maybe even want to join one,
even if you have a less than ideal perception of them,
even if you think they just always want more.
And we're going to go through a message that I was sent.
It's kind of long, but I'm going to read the whole thing
because I want to lay out the whole point that the person is making
and then we'll answer the question in it.
Beau, I love your videos, but I have one thing I just can't follow you on.
Unions. Delta pilots are voting for a strike.
Do you have any idea how much they make?
They're already very well paid. They just seem greedy.
The railroad strike you covered, they were already well paid, but they want more.
The miners you did the livestream for were already well paid.
At least they were up front enough to say it was about scheduling and other conditions issues.
Every single union I know of that goes on strike is already making more than me,
and I'm not paid badly. It makes it hard for me to support unions.
I guess the Starbucks people don't make more than me, but all the others do.
You're a really smart guy. You say unions are for everybody.
Explain to me why I should want to join a union or support strikes.
The idea here is that while unions are greedy, they always want more.
And the worker should be satisfied because they're already paid more
than the person who sent the message, who isn't paid badly.
That's the general idea.
Why does the boss always want more?
No matter what industry you work in, odds are the boss is on you or on your manager to beat LY.
To beat last year's numbers, right?
You got to make more than you did at this time last year,
because that's how they gauge things.
They always want to be making more, more, more.
Why is that okay for the boss but not okay for the worker?
That would be my question.
And if the company is making more every single year, if they're beating LY,
why shouldn't the worker, the person who makes the company make more,
why shouldn't they get a cut?
The way this is written, yeah, this is one set of words to describe this.
Every union you're aware of that goes on strike,
they already make more than you except for the Starbucks union.
Odds are you don't know what various unions make unless they go on strike,
because then the company always puts that information out there to create this exact sentiment.
Since you only know the wages of unions that go on strike,
another set of words to describe this, to say the same thing,
would be every union that you're aware of, the employees, the workers,
they make more than you with the exception of the Starbucks people,
the union that just started.
Give them a couple years.
That's why. It's one of those things that always comes up
because companies put a lot of effort into making this come up.
Union workers are already paid well, therefore they shouldn't want more.
How do you think they got to be paid well?
Through the union, because divided you beg, united you bargain.
You can strike a deal with the boss to get a little bit of that money that came in over L.Y.
That's why. People don't just show up at a job and unionize
when all of the conditions are great and they're making a bunch of money.
They unionize when the conditions aren't great.
They're not making a bunch of money, but that union gives them the power to make more.
The real takeaway here is that every union worker you know,
with the exception of the Starbucks one, the one that just started,
they all make more than you.
And they're not conditioned to be satisfied with what they already have.
They're using that power, that association to better their lives,
to get their cut of the value of their labor that they produce for the bosses.
They want their percentage of it.
So yeah, as long as the company's always making more, the union workers,
the people who are making that money for the bosses,
they're always going to want more because they're entitled to it.
They're the ones making the money.
The people in the suits, they always say, well, this is what my region did or whatever,
but it's the worker that creates that value that they capitalize on.
They make more because they have a union,
because they have that collective bargaining power.
That's why you should support them.
That's why you should probably want to join one.
And that's why over the next couple of years, Starbucks, they're going to need your help.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}