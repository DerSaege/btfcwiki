---
title: Let's talk about Trump, special counsels, advice, and delays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XIERuksebe0) |
| Published | 2022/11/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice considering special counsel for Trump cases and bringing in high-end advisors.
- Special counsel may be used to negate political appearances if Trump announces candidacy.
- Beau believes using a special counsel could be viewed as political, deviating from the norm.
- Bringing in high-end advisors suggests upcoming activity shortly after the election.
- Expect a flood of activity, more searches, and potentially indictments of high-profile individuals.
- The Department of Justice is closing the circle around Trump, preparing for action after the election.
- Unofficial rule of not taking action right before an election to avoid impropriety.
- Advisers are there for the possibility of going after Trump through normal processes.
- Special counsel is being considered in case Trump declares his 2024 candidacy.
- Beau expresses concerns about potential delays and the country's ability to afford them.
- Waiting for the verdict on the current sedition trial before proceeding might be wise.
- Beau advocates for sticking to the normal process and avoiding unnecessary delays.
- The documents case is not seen as complicated by Beau.
- Beau suggests that the Department of Justice needs to focus on the normal process with the advisers they have.

### Quotes

- "Two things: One is they're definitely going after Trump."
- "I personally think that's a bad idea."
- "The only delay at this point that makes sense to me is waiting for the verdict on the current sedition trial."
- "But a delay in putting together a special counsel's office and all of that stuff, it's just going to make the process run longer."
- "The normal process is probably what they need to stick to."

### Oneliner

Department of Justice considers special counsel and advisors for Trump cases, preparing for post-election activity, despite concerns about political appearances and unnecessary delays.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Wait for the verdict on the current sedition trial before proceeding (suggested)
- Stick to the normal process with the advisers in place (implied)

### Whats missing in summary

Further insights on the potential implications of using a special counsel and high-end advisors in Trump cases.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about special counsels
and advisors and delays
and what we can infer from all of the news
that has broken over the last, I don't know,
18 hours or so.
Okay, so if you missed the news,
there is reporting that is suggesting
the Department of Justice is entertaining the idea
of using a special counsel
when it comes to the Trump cases.
There is also reporting that suggests
they are bringing in high-end advisors.
That is pretty much confirmed.
So you have two pieces of news that are separate,
even though they're kind of being reported together.
The advisors, these are high-end prosecutors.
These are people that really understand national security,
and they are kind of being put on call
to deal with the two investigations,
the documents case and the January 6th case,
to just kind of show up as a ringer if need be.
There is also reporting that is separate from this
about a special counsel.
The Department of Justice believes
that if they use a special counsel,
they can negate it looking political.
So the idea would be to use a special counsel
if Trump decides to announce his candidacy.
I think they're wrong.
Given the recent counsels,
given, you know, Durham and all of this,
I think people would view it as political if they used one,
rather than just using the normal process that exists.
Anything that deviates from the norm,
they will spin and try to turn that
into a political witch hunt type of thing.
Now, what can we infer from all of this?
Two things.
One is they're definitely going after Trump.
The idea of bringing in a special counsel
clearly indicates that at least those
who are talking about this aspect of it,
they want to indict him.
The other part, bringing on all of the advisers,
that tells me that shortly after the election,
we're going to see a flood of activity.
We will see probably more searches,
maybe even indictments of high profile people.
This is the point where they really start
closing the circle around him.
And they stuck to their little unofficial rule
of not doing things right before an election
to avoid any appearance of impropriety.
But the whole time, they're bringing on people
and putting together a team so they can act right afterward.
So that's where we're at.
These two pieces of information,
they're being reported together everywhere.
You know, special counsel and advisers.
It's two different things.
The advisers are there for the possibility
of them going after Trump world using the normal process.
The special counsel is something that they are entertaining
in the event that Trump declares his candidacy for 2024
and they might want to try to insulate themselves.
I personally think that's a bad idea.
It would also cause yet another delay,
one that realistically,
I don't know that the country can afford.
The only delay at this point that would make sense to me
is waiting for the verdict on the current sedition trial,
the one that's already going on.
I think it might be wise to wait
until that verdict is returned to proceed.
But a delay in putting together a special counsel's office
and all of that stuff,
it's just going to make the process run longer
and run it into the next election,
which is not something this country needs.
The normal process is probably what they need to stick to.
That's why they have the advisers.
That's why they have the people they do.
And with the documents case, I mean, this isn't a...
It's not a complicated case.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}