---
title: The roads to North Dakota with Cara Mund....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=spT1ZpNOZoo) |
| Published | 2022/11/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introducing a candidate from North Dakota, Cara Mund, an independent running for the U.S. House against the Republican incumbent after the Democratic candidate suspended their campaign.
- Cara Mund shares her journey from starting a charity fashion show at 14 to becoming Miss America, attending Brown University and Harvard Law School, and working for a Republican senator in Washington, D.C.
- Mund explains her decision to split from the Republican Party due to changes in partisanship, restrictive party rules, and differences in policy, especially on women's reproductive rights.
- The biggest policy difference between Mund and the Republican incumbent is her pro-choice stance, influenced by concerns about women's healthcare and the recent Dobbs decision.
- Mund criticizes the incumbent's voting record, particularly on infrastructure, January 6th committee participation, and women's reproductive health, questioning his allegiance to the party over the people.
- Despite being an underdog in a red state, Mund is motivated by the opposition's increased spending, her strategic campaign approach, and the support she receives from individual donors over PAC money.
- Encouraging North Dakotans to vote for candidates who genuinely represent them and prioritize making an impact over seeking power or pleasing voters with superficial gestures.
- Acknowledging the challenges of running as an independent but staying hopeful and determined to bring change to North Dakota by challenging the status quo.
- Planning to focus on legislation related to the Farm Bill, codifying Roe v. Wade, affordable prescription drugs, and capping insulin prices if elected to Congress.
- Emphasizing her unique campaign approach, dedication to representing North Dakotans, and determination to prove skeptics wrong by working hard and staying true to her values.

### Quotes
- "People vote, PACs don't vote."
- "The easy road is not always the honorable road."
- "Pick a candidate that is going to align with your views because we don't know what's on the horizon."
- "For the people that have discouraged me or said there was no way I could possibly do it, they just make me work harder to prove them wrong."

### Oneliner
Beau introduces Cara Mund, an independent candidate from North Dakota challenging the Republican incumbent, focusing on her journey, policy differences, campaign challenges, and commitment to representing North Dakotans authentically.

### Audience
Voters in North Dakota

### On-the-ground actions from transcript
- Support Cara Mund's campaign by donating directly to her campaign fund (suggested)
- Encourage friends and family in North Dakota to research Cara Mund's platform and voting record before the election (implied)
- Ensure North Dakotans are aware of the importance of voting and encourage them to participate in the election (implied)

### Whats missing in summary
The full transcript provides a detailed insight into Cara Mund's background, motivations, policy positions, and campaign strategy, which can be best understood by watching the entire interaction.

### Tags
#NorthDakota #Election #IndependentCandidate #PolicyDifferences #CampaignStrategy


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we have a treat. We have a
conversation that we're going to have with a candidate from North Dakota. Would you like to
introduce yourself? Yes, thanks for having me. I'm Cara Mund. I'm the independent candidate for the
U.S. House. Uniquely enough, the Democratic candidate has suspended his campaign,
so the race is really down to me as the independent and the Republican incumbent.
All right. So everybody knows you as one thing, former Miss America, right? Let's talk about some
of your other qualifications. I heard that at some point you went to college.
I did. I did. Yes. So I was born and raised in Bismarck, North Dakota.
I was very active in my community growing up. At 14 years old, I started a charity fashion
show here in the state that went on for 10 years, raised over $100,000. So that was really my first
like figuring out how to kind of run a business on my own. I then went on to Brown University
in Providence, Rhode Island. And over spring break, I would come back and do the show.
I graduated with honors with a degree in business entrepreneurship organizations.
I then came back to the state and I wanted to be Miss North Dakota. I had tried twice before that.
It was my third time. I thought for sure this has got to be it. I didn't win. So I had to figure out
a backup plan or something else to do in the meantime. So I thought, you know, I've always been
really interested in politics and decided to go work for our Republican senator in Washington, D.C.,
someone that I've really admired. So I had that opportunity to be in D.C. and that was back in
2016. So I was there for the 2016 election. But then after that, I came back to the state and I
knew I always wanted to be Miss North Dakota, went on to become Miss North Dakota, became North
Dakota's first Miss America, where I had the opportunity to serve as the national goodwill
ambassador for the Children's Miracle Network hospitals. So that's kind of where I developed my
love for children, for making sure that affordable prescription drugs are available,
that people don't have to choose between putting food on the table and prescription drugs to
survive. So that's where I really developed that passion. I also was ambassador for the USO. So I
got opportunities I would have never had. And then from there, I went on to Harvard Law School,
where I just graduated with honors. I was preparing for the bar while also secretly planning a
campaign. Fortunate enough to have passed the bar, because you know if I wouldn't have passed,
they would have used it against me. So I did all of that, and now I'm fully running a campaign
all by myself. Okay, so that brings up an interesting thing.
You worked for a Republican senator, but you're running as an independent, and there are
definitely some noticeable policy differences. So what caused you to split from the Republican
Party, so to speak? And yeah, let's just start with that. Yeah, yeah. So when I was there in
2016, the party seemed very different than it is now. It seemed okay. If you, you know, crossed
the aisle, it wasn't betrayal to the party. I'm not seeing that now. And specifically in North
Dakota, the Republican Party has enacted two new rules. The first is that if you try to seek the
nod from any other party, including being an independent, you can't join them for six years
or get their endorsement for six years. The chairman of the North Dakota Republican Party
has stated that if I win, I won't even be allowed to walk on stage with the party, even if I'm
caucusing with them. So it's something that I don't agree with that just keeps people out.
Additionally, the party has also enacted a rule where you have to pay to get their endorsement,
and that payment can go all the way up to $5,000. So not only am I seeing the partisanship in DC,
but I'm seeing right here within our state, these mechanisms that are there, you know,
if I thought the same way I thought six years ago, I wouldn't be growing, I wouldn't be developing,
I wouldn't be innovating. But it's their way or no way. And I think it's been really telling
because I stated pretty early on that I thought I would probably caucus with the Republicans.
So when the Democratic candidate dropped out, I mean, it was a guaranteed seat for them.
Yet they continue to push that I am this liberal, that's one of the first things that they stated,
MUN's a liberal, MUN's liberal, and they've known me through the Republican Party. I was a State of
the Union guest. I had dinner with Mitch McConnell. Like, they know me. And it's not that they want a
Republican seat. It's that they want a Republican that they can control that will vote whatever
they say. And that's not a party that I want to be a part of. Makes sense. So what,
obviously, there are differences between you and the Republican candidate.
What's the biggest? Yeah. You know, my final straw to jump into the race while I was planning a
campaign was the Dobbs decision. And specifically now as a lawyer recognizing, you know, the Roe
decision relied so heavily on your right to privacy. And, you know, that right to privacy
opens up to same sex marriage, interracial marriage, right to contraceptives. We knew it
was a vulnerable decision. And that's why we asked those nominees under oath, you know,
are you going to uphold it as the law of the land? And, you know, we knew it was vulnerable,
but I don't think anyone ever expected it to truly get overturned. And especially in North Dakota,
we don't have a clinic that does elective abortions. So it really is women's health care.
We have one medical school in the state. I've talked to the students there. We're educating
these students. We're giving them a great education. And they don't want to stay in
North Dakota because they are fearful of being criminalized for just doing their job.
And so I am the pro-choice candidate. And, you know, prior to the Democrat dropping out,
he was also an anti-choice candidate. So until I had gotten in, we had two men that were anti-choice
and I didn't feel like I was being represented. We've also never had a female serve in the U.S.
House in North Dakota. We're one of three states. And, you know, now more than ever,
following that Dobbs decision, we need more women at the table and we needed that viewpoint
on the ballot. All right. So what else? I mean, that's a huge one. That's one that I definitely
think that's going to motivate a lot of voters. But what are some other differences that you might
have? Yeah. You know, starting back to so my last year of law school is really when I started
thinking about this and it was in November when the incumbent voted against the infrastructure
bill. And in a state like North Dakota, where we have miles of roadway, you know, where there's
not towns next door to each other, you know, our bridges, our waterways, we needed that funding.
And, you know, even the Internet connectivity that it brings to the state, you know, it brings
two billion dollars to North Dakota and he voted against it. And our two Republican senators voted
for it. And it just felt like, again, it's continuously the party over the people. It is
whatever Kevin McCarthy states is how we're going to vote. And going into the start of the year,
the incumbent was also put on the January 6th committee and he was approved to be on it. And he
had, you know, then at that point took himself off and said he wasn't going to participate.
Now he's been calling it this choreographed hearing and all this. And it's like you could
have been there. You could have been the voice. And North Dakota has one person in our whole state.
You are the lone voice and you didn't do it. So upholding democracy, we definitely are different
on. He stated in our third debate that, you know, no matter who the Democratic candidate is,
if the Republican candidate is President Trump, he will be voting for President Trump.
And that concerns me when, you know, we still have two years. We don't even know if he's going
to be an eligible candidate. And again, it's whatever the party states. And when he got
pressed, when I pressed him on that January 6th issue, he stated, well, that was a party decision
for me not to do it. And it's like you got to make your own decisions. You can't just do whatever
the party tells you to do. Then we go to the infant formula appropriations. He calls it a slush fund.
Our mothers, our fathers, they're still struggling to find formula. So voting against that, voting
against the cap on insulin, voting against contraceptives, voting against women's
reproductive health. He stated that he really does think that the Dobbs decision should go back to
the states, which, OK, fine, but you had the right then to codify it. And he thinks it should be a
states right. Well, when Lindsey Graham then comes up and brings a ban, you know, that's not the case.
You know, the party is completely contradicting themselves. And at the same point, you know,
he voted, voted and I agree with this to codify same sex marriage. So then why aren't you codifying
women's reproductive health? And so there's a lot of things we differ on there. And, you know,
there's things that we're similar on, I would say, like egg energy issues. You know, we're pretty
aligned there. But, you know, basic human rights, that that's where we differ. And democracy.
Small, small differences there. Right. Democracy.
If that's our threshold, like we got to do better. Right. OK, so you are the independent.
So you're you're definitely fighting an uphill battle in the state. But
it seems as though your opposition has been spending a whole lot of money to win a very,
very red state. What what do you is there anything you can take away from that?
Yeah. You know, I I think they know that this is a challenge and I'm glad I'm glad they should be
spending that money. Up until I announced I had seen no campaign ads. He actually didn't have his
website updated since his last race until the end of October when I called it out in the third
debate. And I told everyone in the debate, go to his website. He still hasn't even updated it.
You know, they just assumed that this was a slam dunk. And so it's interesting to me,
as so many people say, I got into the race late and I wasn't late. I was very strategic in the
sense that I knew if I announced earlier, I would always be facing this uphill battle of not being
able to outspend him. Like I just genuinely I can't do it. He's he's a very, very strong
candidate. I just genuinely I can't do it. He's a multimillionaire himself. He can fund his
campaign. He's given his campaign a two hundred thousand dollar loan as someone who just came out
of law school who has student debt. I can't do that. But I can be strategic and bring really
unique, innovative ideas to the campaign. So, you know, when we filed our FEC filings, it was talks
across the state. Right. Like he's he's raised two million dollars. My first filing, I think I had
seventy seven thousand. And then in our report leading up to the election, I had an additional
seventy five thousand. But what people aren't realizing is I'm not taking PAC money. And so
that's a big difference between us is, you know, he's he loves the PAC money. I am not taking the
PAC money. But when you look at individual contributions in two months, I outraised his
months or twenty three months of individual contributions under two hundred dollars.
And it's by quite a bit. I think it's like forty five to forty five thousand to nineteen thousand.
So I just keep reminding myself, people vote, PACs don't vote. And, you know, that individual
who maxed out their contribution to him, their vote holds the same as the person who gave me
a two dollar donation. Right. Right. And you you are. You're at a point in time when people in
rural areas and I'm sure this is true in North Dakota. Wow. Sure. There's the tradition of
voting Republican. There's also a lot of frustration because it's it has become everything
that Republicans, I mean, said the Democratic Party was at one time, you know, beholden to
billionaires and doing whatever they're told. And they're the Republican Party has actually
started saying, yes, we're going to do whatever they tell us, which is I mean, that's a bold move.
So if you had a message that you wanted to get out specifically to voters in North Dakota,
what would it be? Yeah, you'll pick the person who you think is genuinely going to represent
you. Look at our campaign ads. You know, who's the person that knows what it's like to be a
North Dakotan? Who knows, you know, the struggles, student loan debt? I've been there. I've got
farmers in my family. I think it's interesting how campaign ads now is specifically my opponent.
He's fixing a tractor. I don't know if he's ever fixed a tractor in his life. And a lot of people
talk about that, you know, and and it's like, who is the person who's trying to just please you to
your vote? And then also, what is their motivation to be in office? Is it for the power or is it
truly to make an impact? And, you know, it's interesting because I've been told by a lot of
Republicans who are unhappy with me running that I am running supposedly a dirty campaign. And I
was told this within the first 48 hours of running because I brought up his voting record. And it's
like, I don't know how that's a dirty campaign. That's the facts. You know, like, it's not my
fault. He didn't change his website. It's not my fault. He votes against women and infants and,
you know, infrastructure. It's it's you know, these are the facts. And I think part of it is they
don't want that to come to light. And so, you know, North Dakotans, I know we are smart individuals.
We're hardworking individuals. Our state was identified as the hardest working state just a
you truly think is going to represent you and is there for the right reason.
Right. And I mean, that's you, you have the voting record issue. I haven't I haven't personally gone
through his. But I'm certain that if you hopped on OpenSecrets.org and saw where most of his
money came from, I'd be willing to bet that most North Dakotans wouldn't like that. So
So you have some stuff going for you.
How are you trying to get the message out in this last week?
I mean, I would imagine you're running yourself ragged.
Yeah, it is a sprint to the end.
As much access I can get to people, the most interviews
that I can possibly do.
I am confident, though, on Tuesday,
I will have no regrets.
I'm proud that I didn't take PAC money.
It makes it a lot harder, but I continuously
state that, like, the easy road is not always the honorable
road, and that is the same with going a party.
It's the same with being beholden to special interests.
And one of my campaign ads, I talk about,
if his voting record aligns with what you want,
then vote for him, because that's what you're going to get.
And I confidently believe that there
are North Dakotans who don't align with that,
and who don't believe in that, and who also
want their right to privacy.
And so it also just, they got to get out to the polls.
And so really pushing that is, it's great if you support me.
It's great if you're commenting on social media and sharing.
But at the end of the day, you have to be there to vote,
because that's what really counts.
Yeah.
Yeah.
And that's, when you're looking at all the polls this year,
it is, it's hyper-partisan, and it is very much
geared towards looking at likely voters versus those
who may show up because of the Dobbs decision,
or just out of pure frustration and anger,
which is quite a few.
And when we look at Georgia's early voting,
and the numbers that are coming in,
I know that you're an outside shot.
I know you, I mean, and I know you know that.
But you have to feel like there's still a shot, right?
I do.
And we saw it in Kansas.
We saw it in Alaska.
North Dakota, we also have term limits on our ballot,
and we also have marijuana on the ballot, which
I think is going to bring out unique voters as well that
probably wouldn't have voted otherwise in a midterm.
So hopefully that some of those voters
align with what I'm saying, and are also
tired of the same old, same old, and we
can change that status quo.
But I don't think it's impossible.
I recognize that it is an uphill battle,
but just how you stated, I don't think
they would be spending as much if they
didn't think I had a shot.
Right.
So let's assume you win for a second.
You're going to caucus with the Republicans,
or you don't know yet?
I don't know yet.
I originally assumed Republicans, and I stated that.
And then I talked about these rules,
and then I stated in one of my very first interviews,
but I want to be welcome to the party,
and I don't want to be welcomed after I've won.
I want to see what you're doing now.
And they continue to label me as this liberal.
The incumbent had just did an interview earlier this week,
and they said, what's the biggest difference between you
two?
And he said, well, I'm a conservative,
and she's a liberal.
And it's the easiest thing they can
do is those labels that are attached,
and what that label supposedly means.
And so there's a lot of things that we can do.
We can keep pushing.
We can do whatever we can to possibly get there.
But I don't think it's impossible.
So do you have any legislation?
Obviously, women's reproductive rights, stuff like that.
Is there any legislation you would want to put forth
if you got to DC?
So the Farm Bill is going to be something that really matters
to North Dakotans specifically.
But I do want to hopefully codify Roe,
and I've been very, very clear about that.
And so codifying Roe, upholding democracy,
where this January 6th committee is going to continue to go,
there's a lot of things that I think
it'll just depend on which party is the majority, where that's
going to be.
And I guess that goes back to your caucusing question
that I also have is I'll have a lot more power
as an independent if I go with the majority party.
But I also think that those negotiations, when I'm there,
when I go for orientation right after winning, if I do win,
it'll be which party is going to be
willing to put me on committees, who's
going to give me leadership roles.
So that'll play a factor as well.
But women's reproductive health, potentially
capping insulin prices for all, and just
affordable prescription drugs across the board.
But I think what's so interesting
is I'm not afraid to say I'm not an expert in egg.
I'm not an expert in energy.
But I will have the team, I'll have the support,
and I know what North Dakotans need and what they want.
And so depending on who that party is,
what the circumstances are like, every month it keeps changing.
Who knows what January is going to be like?
But again, I think that's why it's so crucial
you pick a candidate that is going
to align with your views because we don't
know what's on the horizon.
Sounds great.
OK, final shot.
What do you want to say?
Oh my goodness.
So I think what makes my race so unique
is that I worked my way onto the ballot.
I took a petition around.
I got over double the amount of signatures
that I needed in the state.
I didn't go with a party, which was much harder.
I'm not taking PAC money.
And I'm also holding like 11 roles right now.
I'm head of social media.
I'm scheduler.
I'm candidate.
I'm speechwriter.
I'm researcher.
I mean, I am doing it all.
And that's exactly what I would do in Washington as well.
I mean, I'll have a whole team with me.
But knowing what this role means,
knowing the responsibility, and also knowing
the opportunities when I was Miss North Dakota,
when I was Miss America, when I was at Brown,
when I was at Harvard, people continuously
underestimated North Dakota.
And even in this race, there's very little coverage.
And it is possible that a Republican is not going to win.
And that's the same.
It's the same in DC.
And so throughout my whole life, I have been underestimated.
And I think part of it's being a woman.
Part of it's being from North Dakota.
And I've always proved them wrong.
And so I say for the people that have supported me,
I thank them.
But for the people, too, that have discouraged me or said
there was no way I could possibly do it,
they just make me work harder to prove them wrong.
So for North Dakotans that are out there,
I am here to represent you.
And I gave up a lot to do this.
I had a job with a big law firm that
was kind of the path of what you do after Harvard Law School.
But it was representing our state,
representing the women, equal rights, upholding democracy,
that were so important to me that I knew this
is what I need to do now.
Because if I didn't run, I would always wonder what if.
And I don't want that.
Sounds great.
OK, well, that's it for today.
I hope you all enjoyed it.
I know that there's a whole lot of people in North Dakota
that are going to be watching this.
And I'd have to agree.
Take a look at the voting records.
Take a look at where the money comes from.
And see who best represents you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}