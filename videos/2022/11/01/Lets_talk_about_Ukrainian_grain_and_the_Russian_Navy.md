---
title: Let's talk about Ukrainian grain and the Russian Navy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_epuZEQx-Hg) |
| Published | 2022/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia is cutting off Ukrainian grain shipments to Africa as a consequence of Ukraine's successful use of unmanned naval drones against the Russian Navy.
- The Russian government decided to withdraw from a deal allowing Ukrainian grain to reach countries in Africa, which are in desperate need of food.
- Ukraine's military operation against the Russian Navy, using naval drones, was described as a humiliating failure for Russia.
- Although the military effectiveness is debatable, the attack showcased a proof of concept, leading to a rewriting of naval textbooks.
- Russia claims to have repelled the attack, but Beau expresses skepticism regarding their statements.
- Russia's decision to block grain shipments to Africa can be seen as an act of imperialism to punish Ukraine for their military operation.
- Russia justifies its actions by claiming to protect the Ukrainian grain shipments, although it seems like they are actually protecting them from themselves.
- This move by Russia is aimed at gaining leverage on the international stage and punishing Ukraine by causing food shortages in Africa.
- The conflict between Russia and Ukraine is still in the invasion phase, with the occupation phase yet to come.
- The technology used by Ukraine against the Russian fleet could potentially be accessed by non-state actors like a Ukrainian resistance movement.

### Quotes

- "This war is lost. Russia lost."
- "They haven't really got to the occupation part of it yet."
- "Russia is protecting the grain shipments from themselves."
- "A lot of them were in countries that bordered areas that desperately need this food."
- "Russia is now going to allow people in the global south to starve so they can annex territory."

### Oneliner

Russia cuts off Ukrainian grain to Africa in response to successful Ukrainian naval drone attack, illustrating imperialistic tendencies and risking international backlash.

### Audience

Global citizens

### On-the-ground actions from transcript

- Contact organizations providing aid in Africa to support areas affected by the cut-off grain shipments (exemplified)
- Join or support Ukrainian humanitarian efforts to mitigate the impact of Russia's actions (exemplified)

### Whats missing in summary

Insights on the potential future escalation of the conflict between Russia and Ukraine.

### Tags

#Russia #Ukraine #Imperialism #MilitaryConflict #FoodSecurity


## Transcript
Well, howdy there, Internet people.
Let's go again.
So today we are going to talk about Ukrainian grain
and the Russian Navy and Africa
and what all of this has to do with each other.
The Russian government has made the decision
to withdraw from a deal that allowed Ukrainian grain
to be transported to countries in Africa
that are in desperate need of it.
They've pulled out of that deal.
They will now be blocking those grain shipments.
The Russian government decided to do this
because Ukraine had the just audacity to hit back
in what can only be described as a humiliating failure
on the part of the Russian Navy.
Ukraine used unmanned naval drones
to strike at their fleet.
Now, the military effectiveness of the attack,
it can be debated, but proof of concept is there,
and I assure you that naval textbooks are being rewritten
because of this.
The Russian government is saying they repelled the attack.
Footage exists, and let's just say
I am skeptical of their claims.
So because of this, Russia has said
that it's pulling out of this deal.
Why punish people in Africa?
Because Ukraine conducted a military operation
during a war?
Because it's imperialism.
To all of those people for who the last 250
days of this three-day war have been telling me
that Russia is a friend of the global south
and would never engage in imperialism,
Russia is now going to allow people in the global south
to starve so they can annex territory.
Russia's official stated reasoning
is that somehow these ships were protecting the Ukrainian grain
shipments from who exactly.
This is akin to somebody standing in a shop
talking to the shopkeeper while holding a baseball bat saying,
oh, nice shot.
Be ashamed if something happened to it.
Russia is protecting the grain shipments from themselves.
So this move is designed to gain leverage
on the international stage and hopefully curtail or punish
Ukraine by punishing people in Africa.
It's worth reminding everybody that Russia
hasn't got to the hard part yet.
This is still the invasion phase of this little adventure.
They haven't really got to the occupation part of it yet.
I understand that there are segments of land
that are currently being occupied,
but they haven't got to the occupation phase in earnest
yet.
It's also worth noting the technology
used to demonstrate that the Russian fleet is unsafe,
well, everywhere, is technology that
would be available to a non-state actor,
such as a Ukrainian resistance movement.
This war is lost.
This war is lost.
Russia lost.
All that is being determined now is how much of Russia
gets lost with it, how much waste occurs.
This move by Russia, cutting off these grain shipments,
it's not going to have the impact
on the international scene that they believe it will.
It will continue to still resolve those countries who
were not participating in sanctions
because they weren't involved.
Well, now they very well might.
A lot of them were in countries that bordered areas that
desperately need this food.
This is yet another bad decision in a long string
of bad decisions from Russian command.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}