---
title: Let's talk about an update on the Pelosi situation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e7OLFRhH318) |
| Published | 2022/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on the incident involving Pelosi in California, shedding light on the suspect's motivations.
- The suspect expressed a desire to break Pelosi's knees to show consequences for her actions and viewed her as a leader of Democratic lies.
- DOJ released information indicating the suspect's political motivations and the potential for federal charges due to the severity of the statements.
- Beau references the academic and legal definitions of domestic terrorism in relation to the suspect's actions.
- Suggests that the suspect talking could lead to further revelations about who influenced his beliefs regarding Pelosi.
- Calls out individuals spreading false narratives and posing as merely asking questions while making assertions.

### Quotes

- "I'd like to point everybody back to that academic definition."
- "This type of activity, it tends to escalate over time."
- "No, a lot of them were making assertions, that's not asking questions."

### Oneliner

Beau sheds light on the suspect's political motivations behind targeting Pelosi and the potential for federal charges, urging a deeper understanding of the situation beyond false narratives and posing questions.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Contact DOJ for updates on the case (suggested)
- Stay informed about the developments in the investigation (implied)

### Whats missing in summary

Insights on how false narratives and political motivations influence dangerous actions.

### Tags

#California #Pelosi #DOJ #PoliticalMotivations #FalseNarratives


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are going to provide a little bit of an
update on what happened out in California with the Pelosi's
and the questions that have been asked and how they
certainly appear to have been answered now.
there was a release put out by DOJ and it should clear some things up for
people who are believing less than accurate narratives. According to the
release in a recorded and Mirandized interview, the suspect said that he wanted
to break Pelosi's knees.
So she would have to be willed in to Congress
so people would understand there were consequences
to their actions.
And he did this because he viewed her
as one of the leaders of the pack
when it came to all the democratic lies.
As reporting suggested, the real reporting,
the accurate reporting, it appears his motivations
were political.
It is also worth noting that given the Department of Justice
level of interest in this, that there might be federal charges
stemming from this.
Based on the statements, they might be pretty severe.
I'd like to point everybody back to that academic definition
that I talk about a lot.
The unlawful use of violence or the threat of violence
to influence those beyond the immediate area of attack
to achieve a political, religious, ideological,
or monetary goal.
Based on the statement that is being reported,
that certainly sounds like what was going on.
Now, that's the academic definition.
The legal one says that domestic terrorism means activities
that involve acts dangerous to human life that
are a violation of the criminal laws of the United States
or of any state, and b, appear to be intended to intimidate
or coerce a civilian population to influence
the policy of a government by intimidation or coercion, to affect the conduct of a government
by mass destruction, assassination, or kidnapping, and occur primarily within the territorial
jurisdiction of the United States."
It certainly appears that if the feds wanted, they could definitely make a terrorism case
here.
on the statements and the overt actions, that certainly does appear to be what they might
be angling for.
So you can add that into the list of descriptions that some people might be defending along
with the horror movie villain thing.
This type of activity, it tends to escalate over time.
I would hope that people start to see what's going on.
Those who have fallen into these echo chambers, who were told all of these false narratives
about what was going on, and saying, oh, well, they're just asking questions.
No, a lot of them were making assertions, that's not asking questions.
a lot of times they were asking questions they knew the answer to.
By the way, something that I saw repeatedly posited from people who claimed to have a
background in security is why didn't they just release the surveillance tapes from inside
the house?
Because generally speaking, providing the layout of the home and the location of the
cameras is a bad idea, especially in the aftermath of an attempt. Most of the
people asking that question, just asking it, you know, trying to get that out
there, they know that. That's pretty basic information. So we can expect this
to go even further given the fact that he does appear to be talking, the
suspect appears to be talking, he very well might provide information about who
led him to believe that she was the leader of the pack of the Democratic
lies. That should be interesting. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}