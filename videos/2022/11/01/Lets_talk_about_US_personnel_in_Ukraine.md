---
title: Let's talk about US personnel in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dJ3ZUjw0wjw) |
| Published | 2022/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is working on improving the aid provided to Ukraine amidst concerns from the Russian government about weapons ending up in illicit markets.
- Russia expressed genuine concerns about weapons provided by the US potentially making their way off the battlefield and into underground markets.
- In response, the US plans to have weapons inspectors in Ukraine to keep records and ensure proper use of military aid.
- US personnel will oversee Ukrainian training to prevent misuse of the weapons.
- The plan aims to address Russian concerns and prevent aid from ending up on the black market.
- Russia actually requested this oversight to ensure better record-keeping and prevent weapons from being misused.
- Proper use of the aid is seen as key to preventing it from reaching illicit markets.
- The Russian government's outrage over this oversight may not have been the wisest move.
- The US is taking steps to be a better steward of military aid to Ukraine and ensure it is used appropriately.
- The goal is to avoid weapons provided for Ukraine ending up in unauthorized hands.

### Quotes

- "Russia literally asked for this. They wanted this oversight. They wanted Americans who would be willing to travel to Ukraine to keep track of them."
- "The best way to make sure it doesn't end up on the black market is to make sure that it's used properly."
- "This probably wasn't the smartest outrage to manufacture by the Russian government."

### Oneliner

The US is enhancing aid to Ukraine to address Russian concerns about weapons ending up in illicit markets, ensuring oversight and proper use.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Ensure proper oversight and record-keeping of aid (exemplified)
- Monitor training and usage of military aid to prevent misuse (exemplified)

### Whats missing in summary

The full transcript provides additional context on the importance of proper aid stewardship and addressing Russian concerns effectively.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about some developments
in the US assistance to Ukraine
and how the United States is going to attempt
to be a better steward of the aid that is being provided,
and how the US will attempt to alleviate and address
some of the russian government's very very real concerns
about that aid
the united states has been providing military aid
to ukraine
uh... in the form of military hardware
the russian government and some of their allies in the u.s congress
have expressed the very real concern
that some of those weapons might end up
in some underground market.
Russia is obviously truly concerned about the fact
that some of these weapons that they are currently
facing on the battlefield might make their way off
the battlefield and end up in some illicit market.
They're truly concerned about this.
They're very, very worried.
It's not a manufactured outrage at all.
It's very real.
In response, the United States has decided
to be a better steward of that military aid.
So there will be weapons inspectors there to keep records.
US personnel there in Ukraine, not near the fighting,
and they will be there to make sure that Ukrainian forces
are using those weapons properly.
Since they're going to be counting and keeping records,
they should probably also know how to use them.
So they could observe Ukrainian training.
So the Ukrainians can't say that they trained with them
and then just put them on a truck or something.
They should probably know how to use it
and how training is conducted.
They should know that too.
But they won't be doing training.
They'll just be there, you know, keeping track of things, working out of the defense attache's
office at the embassy.
So that is what the plan is to eliminate or at least help alleviate this very real Russian
concern.
A few years from now, when the inevitable story comes out about what's going to occur
with this, I just want it remembered that Russia literally asked for this.
They wanted this.
They wanted this oversight.
They wanted that in place.
They wanted Americans who would be the sort who would be willing to travel to Ukraine
to keep track of them and provide better record keeping.
So this stuff didn't end up on the black market.
I think the best way to make sure it doesn't end up
on the black market is to make sure that it's used properly.
This was probably not the smartest outrage to manufacture by the Russian
government. This probably wasn't what they... I imagine they saw this going
differently. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}