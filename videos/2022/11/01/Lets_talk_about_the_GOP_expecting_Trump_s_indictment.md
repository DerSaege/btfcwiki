---
title: Let's talk about the GOP expecting Trump's indictment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SMbBvOIs8dg) |
| Published | 2022/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican insiders and strategists expect Donald Trump to be indicted shortly after the election, within a timeframe of a couple of weeks to 90 days.
- They believe the documents case against Trump is strong, and Attorney General Garland will need to act swiftly post-election to avoid criticism of influencing a potential presidential run.
- Some anticipate that Trump's indictment could galvanize support behind him, but Beau questions this, suggesting that support may dwindle as evidence emerges.
- Beau predicts that Republicans who have been pushed around by Trump may use his indictment as an opportunity to settle scores.
- He doubts that Trump's allies within the Republican Party will stick by him, speculating that they might leverage his indictment for their own political advantage.
- Beau suggests that Trump's indictment could benefit some Republicans eyeing the 2024 nomination, potentially leading to limited support for Trump within the Republican Establishment.
- There is speculation within the Justice Department about handling the documents case and January 6th related issues together to avoid multiple federal indictments against the former president.
- Beau notes that Attorney General Garland is unpredictable in his moves, hinting that there may not be a definitive timeline for Trump's potential indictment.
- The anticipation of Trump's indictment within the Republican Party is seen as significant, prompting the need for the United States to prepare for potential repercussions and divisions among citizens.
- Beau points out that while an indictment may prompt some to re-evaluate their support for Trump, it could also reinforce the belief among others that there is a conspiracy against him.

### Quotes

- "Republican insiders expect Donald Trump to be indicted shortly after the election."
- "Support may dwindle as evidence emerges."
- "Anticipation of Trump's indictment is significant."

### Oneliner

Republican insiders anticipate Donald Trump's post-election indictment, raising questions about support and potential political ramifications, urging preparation for possible societal divides.

### Audience

Political analysts

### On-the-ground actions from transcript

- Prepare for potential societal divides and heightened tensions post-election (implied).
- Stay informed about developments related to potential indictments and political implications (implied).

### Whats missing in summary

Analysis of potential legal and political ramifications beyond immediate consequences.

### Tags

#DonaldTrump #GOP #Indictment #RepublicanParty #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Trump and GOP strategists and what they believe
is coming shortly after the election.
Reporting now suggests that Republican party insiders and strategists believe that the
former president of the United States, Donald J.
Trump, will be indicted shortly after the election.
The time period that they're giving ranges from a couple of weeks to up to 90 days.
Their belief is that the documents case is strong and that Garland will need to
act relatively quickly after the election to avoid any criticism that Trump's indictment
has to do with a potential presidential run.
The reporting also makes it seem as though they're kind of messaging saying, oh, it'll
be bad, it'll galvanize support behind him if he's indicted. I don't know that
that's true. The documents case is pretty strong from everything that we've seen
and I think for a lot of people the reality is they have listened to the
the punts, the talking heads, who will defend Trump no matter what he does.
As the evidence comes out after the indictments, I don't know that that support will remain.
And I think at that point, a lot of the Republicans who he has bullied and pushed around will
take the opportunity to settle scores.
I don't foresee a lot of his allies within the Republican Party sticking by him.
I think they might kind of just say, oh, well, this has happened and use it to their own advantage
because if there's anything that they learned from Trump, it's, you know, well, throw them
under the bus if it's good for your political career. Some of his strongest allies want to
be the nominee in 2024 as well. Him getting indicted, well, that only helps them. I don't know
that he's going to get a whole lot of tangible support from the Republican Establishment
if he is indicted. I also don't know that Garland is really running on a clock the way the Republican
strategist, apparently, believe.
Yeah, the documents case, it does seem pretty straightforward.
At the same time, there might be a desire
within the Justice Department to handle the documents
case and anything related to January 6th at the same time.
So a former president isn't hit with multiple separate federal indictments.
But I don't know, I mean the one thing that we do know about Garland is that he doesn't
really telegraph his moves, he just doesn't.
So we're going to have to wait and see, but I do find it interesting that talk within
the Republican Party at this point is basically planning for Trump to be indicted.
That's the reporting, and it seems noteworthy.
It seems like something that the United States should start to prepare for in its own way,
because undoubtedly it will stoke already present divides among citizens, especially
those people who have truly bought into Trumpism.
Some an indictment might help them see the light, but for others it may just be something
that they use to reinforce their belief that, well, everybody's out to get it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}