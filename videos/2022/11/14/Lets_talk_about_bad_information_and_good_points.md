---
title: Let's talk about bad information and good points....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7i-QPb-JsUk) |
| Published | 2022/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the need to debunk misinformation on a specific topic concerning civilian casualties.
- Acknowledging the importance of presenting arguments accurately to support valid points.
- Explaining the misconception that the US is worsening in protecting civilians during wartime, when in reality, it is improving.
- Clarifying the misconception that drones cause 90% civilian casualties and providing scenarios to demonstrate the fallacy.
- Emphasizing the challenge of determining accurate numbers regarding drones and civilian casualties due to the complex nature of targeting.
- Pointing out that the issue is not the precision of the weapons but rather the intelligence failures leading to civilian casualties.
- Advocating for focusing on anecdotes and intelligence failures rather than unreliable statistics to address the problem effectively.
- Mentioning the preference for politicians to use drones due to avoiding risks associated with boots on the ground, leading to compromised intelligence.
- Describing the historical changes in rules of engagement under different administrations, noting Trump's loosening of regulations.
- Expressing cautious optimism about Biden's new guidance on drone strikes but underscoring the uncertainty of its effectiveness without adherence over time.

### Quotes

- "The precision of the weapon was fine. The precision of the intelligence, the information that led them to say, yes, hit it, that's the problem."
- "Alter your argument because the point you're trying to make is 100% valid."
- "Don't get caught up in bad information when you're trying to make a good point."

### Oneliner

Addressing misconceptions on civilian casualties, Beau navigates through flawed data to underscore the significance of accurate arguments and intelligence in addressing the issue effectively.

### Audience

Advocates, Activists, Educators

### On-the-ground actions from transcript

- Advocate for better intelligence and approval processes in military operations (implied).

### What's missing in summary

The full transcript provides detailed insights on the challenges of addressing civilian casualties in warfare, stressing the importance of accurate information and intelligence in making valid arguments.

### Tags

#CivilianCasualties #Misinformation #Drones #MilitaryIntelligence #Advocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today is going to be a little bit different.
Got a bunch of messages about one topic in particular.
And the people are making a good point,
but they're using bad information to get there.
And because of that, they're drawing the wrong conclusion.
So what we're going to do is we're going to go through
and we're going to debunk the bad information.
And then we're going to talk about how to actually make
the argument, because the point they're getting at is good.
And this is one of those situations
where you have to be careful, because the worst
thing you can do for an idea you support is defend it poorly
or make the argument poorly.
Because you can end up in a situation
where your bad information gets called out,
and then you're sitting there staring at your phone,
fact-checking yourself, and you think
you're wrong when you're right.
So it has to do with a recent video.
In a recent video, I talked about the precision
of US weapons.
And that led to people saying, then why do
we keep hitting civilians?
And there were two common themes,
and they're both memes at this point.
And they're not accurate.
The first is the US is getting worse
when it comes to civilian casualties.
That's not true.
The US is actually getting better.
If you look at it decade by decade,
and you look at rates and all of that stuff,
the US is getting better.
The problem is the US was starting from such
a horrifyingly bad position.
Getting a little bit better doesn't actually make it good.
So just forget about that.
That isn't the idea that the US is getting worse
when it comes to protecting civilians during wartime.
That's not true.
It's getting better, but it's still not good.
The next is that drones cause 90% civilian casualties.
And this is an incredibly common belief.
And if you believe this, don't feel bad.
Major outlets have put this information out.
I believed it for years.
Then I sat down and actually read the source on it.
That's not what it says.
90% are not the intended target.
And that sounds like that would mean civilians.
Let me give you two scenarios to demonstrate how this works.
So bad guy X, he makes stuff, puts it by the roads
to hurt people, teaches other people how to do it.
This is a bad person, needs to go, gets designated.
In scenario one, they remove him as he is driving in his car.
And as he's hit, he's driving past a wedding.
Nine extra people go.
Those nine, those are civilians.
Scenario two, they don't hit him in the car.
They wait for him to get to a training camp and do it there.
He's teaching nine people.
Those nine people are gone too.
90% are not the target.
Those nine, they're combatants.
Those are clean.
The problem is the civilians and the unintended combatants,
they get thrown into the same pile.
So there aren't good numbers when it
comes to drones and civilians.
They don't really exist.
You've had people try to put a good figure on it,
and they're in good faith.
They're really trying to come up with the number.
The problem is you're talking about something where you,
accuracy is hazy when it comes to counting and figuring out
who was who and what was intended and what wasn't,
when you aren't part of the group of people
that designated it.
So all of those numbers are fuzzy.
But here's the thing.
None of this has to do with the precision of the weapons.
The weapons worked.
They went exactly where they were supposed to.
It has nothing to do with that.
So forget about that part two.
Normally, I'm a person who says, use numbers, use facts,
use figures that can demonstrate patterns and themes,
and it makes it really easy for people to understand.
That's the way that you would prefer
to do something like this.
The problem is solid, good, indisputable numbers.
They don't really exist here.
What we know is that they're bad enough for the US
to categorize them in a way that is hazy.
That's intentional.
Throwing in combatants who just weren't
the intended target in with the civilians,
that's designed to make it hard.
So skip the statistics.
Go to the anecdotes, because they actually
reveal the real problem, because it's not
the precision of the weapons.
Think about the wedding, weddings, the hospital,
aid workers.
The weapons functioned.
They went exactly where they were supposed to,
and these are indisputable civilians.
What's the actual problem here?
It's not the weapon not functioning.
The precision of the weapon was fine.
The precision of the intelligence,
the information that led them to say, yes, hit it,
that's the problem.
And if you're talking about this,
I'm assuming you want it fixed.
That's where you have to go.
It's the lack of intelligence or the bad intelligence
that led to an approval for something that never
should have been approved.
That's where the issues really come in, in large numbers.
Yes, there are scenarios where somebody is driving by,
but those are fewer and further between than the bad
intelligence.
So you can start with all of those stories
that we're all familiar with and work out from there
and explain, this is what happened.
They didn't have information and they approved it,
and this occurred to civilians.
This shows we need to alter the way this is done.
That's how you can get to where you're going.
Because if you say 90% civilians and somebody
knows the documentation and is aware of the information,
they're going to call you out on it.
And then you're going to look it up and you're just
going to be like, oh, well, maybe I'm wrong about this.
No, you're right.
You're right.
There's a huge issue.
But it just can't be demonstrated
with those numbers.
So use the anecdotes.
Point to the intelligence failures.
Point to the reality that politicians
are more likely to use drones because then they
don't have a risk of losing Americans, boots on the ground.
They want to avoid that.
They don't want to use special operations to go in and do
this.
They'd rather do it from the air.
And because of that, sometimes they're
willing to do it with less than accurate intelligence, which
leads to these failures.
Now, one thing that's worth noting,
and we don't know how successful it's going to be yet.
So under Bush, free for all.
Under Obama, tightened down a little bit, but not good.
Rules of engagement were still very, very loose.
Under Trump, it got worse.
He actually made them incredibly loose,
so much so that we still don't have anything even remotely
resembling accurate numbers.
Biden has recently put out new guidance on this.
I did a video about it.
And it looks good.
It does.
It looks good.
The problem is we don't know whether or not that's actually
going to be adhered to because we've
seen good guidance in the past that wasn't adhered to.
So we won't know whether Biden's attempt to rein this in
is successful for a couple of years.
I don't see any harm in continuing
to make the argument that it needs to be reined in,
and there needs to be better intelligence,
and there needs to be a better approval process and designation
process because that's where the failures occur.
It's not the weapons.
Those weapons are hitting exactly
where they're supposed to.
So alter your argument because the point you're trying to make
is 100% valid.
We have an issue with this.
Sure, better than World War II, better than Vietnam,
but still not good.
There's a lot of room for improvement.
So that's where you should go with it.
Don't get caught up in bad information
when you're trying to make a good point.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}