---
title: Let's talk about Trump as Speaker of the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y9_QSu9JFmA) |
| Published | 2022/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on the possibility of former President Donald J. Trump becoming Speaker of the House if the Republican Party retakes the House.
- Considers the qualifications required for the role, sarcastically pointing out Trump's supposed expertise in meticulous planning and attention to detail.
- Raises concerns about Trump potentially using his position to further divide the Republican Party and create discord.
- Mocks the idea of Trump handling committees and investigations effectively, suggesting it could backfire and tie him even more closely to the Republican Party.
- Criticizes the notion that the American people overwhelmingly support Trump and suggests that making him Speaker might lead to backlash against the Republican Party.
- Satirically imagines a scenario where Trump becomes president again through impeachment if he were to be Speaker of the House.
- Expresses personal dismay and disbelief at the idea, suggesting that it could be disastrous for the Republican Party and a boon for the Democrats.
- Concludes by humorously stating that the Republican Party gifting the Democrats two years of Trump in a position of power could lead to a disastrous 2024 election.

### Quotes

- "Owning the libs on social media is not actually a valid electoral strategy."
- "It will be the greatest gift the Republican Party could ever give to the Democratic Party."
- "There are a whole lot more stable geniuses out there than I believed."

### Oneliner

Beau sarcastically dissects the idea of Trump becoming Speaker of the House, predicting disastrous consequences for the Republican Party and delight for the Democrats.

### Audience

Political commentators and voters.

### On-the-ground actions from transcript

- Contact your representatives to express your views on the potential consequences of Trump becoming Speaker of the House (suggested).
- Organize or support political campaigns that prioritize unity and effective governance (implied).

### Whats missing in summary

The full transcript provides a satirical take on the implications of Trump becoming Speaker of the House, shedding light on potential political chaos and division within the Republican Party.

### Tags

#Trump #RepublicanParty #SpeakerOfTheHouse #PoliticalSatire #ElectionStrategy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about the possibility of former President Donald J.
Trump becoming Speaker of the House in the event that the Republican Party retakes the House.
There have been a number of political commentators on that side suggest this as a move.
There's been a lot of talk about it on social media.
And we're just going to kind of go through it because they are, they, the commentators
are talking about the meltdowns that are sure to occur if Trump becomes Speaker of the House.
They're right.
They're right.
If that happens, yeah, there's going to be meltdowns.
Absolutely.
And beyond that, let's look at the qualifications.
As a non-voting member, like a non-voting Speaker of the House, Trump's primary job
would be scheduling and logistics and the details and setting the agenda and running
that calendar.
And I mean, let's be honest.
When you think meticulous planning, attention to detail, heavy logistics, you think Trump.
I mean, I do.
He has the qualification there.
Another big part of it, because he would be in charge of that calendar, is making deals.
And he has his name on a book called The Art of the Deal.
He probably knows something about it.
I can't see how he would misuse that.
It's not like he would use that position and that control of the calendar to get Republicans
to kind of swing towards his faction of the Republican Party and so greater discord within
the Republican Party.
He wouldn't do that.
And if they didn't go along with it, it's not like he'd use his newfound position to
get out in front of the press and talk bad about those Republicans.
Because I mean, Trump's not a petty man.
He's never done anything petty in his life.
So he's got it there too.
And then when you think about the committees, you know, Trump constantly running committees
with investigations or whatever, and those being in the news constantly tying Trump's
name to the Republican Party, I don't see how that could go wrong either.
I mean, this is a political figure that the American people respond very well to.
They support him.
It's not like he's some has-been politician that's led the Republican Party to loss after
loss after loss or anything.
This is a man with a lot of political promise.
I mean, I don't really see how it could go bad giving him the gavel.
It makes sense.
And there's no way that the American people, after tying Trump's presence to the Republican
Party, would be upset with the Republican Party for once again disregarding their vote
and voice and putting that man in a position of power.
I mean, that's not going to happen either because he's so well-liked.
That's why he kept getting elected.
I mean, that's why he's Trump, because everybody loves him.
He's not a divisive figure.
He's not somebody that's going to drive turnout against the Republican Party in any way.
This makes complete sense.
It really does.
And these very astute political commentators, the other thing that they're talking about
is the fact that if he's Speaker of the House and they impeach Biden and Harris, well then
he'll become president.
And they're right there too.
In fact, there's no way to stop it.
I mean, they put him as Speaker of the House and then they impeach the president and vice
president.
It'll fly through the Senate.
It's not like the Democratic Party has control there and would stop it.
It's just going to go straight through.
So there you go.
Trump's president again.
It makes complete sense.
I mean, I hate to admit it, but it does.
And they're right.
They're right about the meltdowns because I personally, if they did this, I would meltdown.
I would cry.
I'm about to cry now.
I would feel very owned if they did this because it would just, it would destroy my world.
Here in the real world, owning the libs on social media is not actually a valid electoral
strategy.
If they do this, it will be the greatest gift the Republican Party could ever give to the
Democratic Party.
I would love to see a 2024 election after two years of Trump in a position that commands
the eye of the press and in an official capacity after everything that's happened.
I think it would go great for the Republican Party.
There's no way it would backfire wildly and tie the Republican Party to every single investigation
that is going on dealing with Trump and the companies and all of those different investigations
that we've talked about.
There's no way that that just becomes synonymous with the Republican Party.
There are a whole lot more stable geniuses out there than I believed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}