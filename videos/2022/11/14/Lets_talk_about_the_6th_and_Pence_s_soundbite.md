---
title: Let's talk about the 6th and Pence's soundbite....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IH0DOwRSEBU) |
| Published | 2022/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau raises the significance of the date "the sixth" post the Capitol events, now replacing historical references like D-Day with images of the Capitol.
- He acknowledges Pence's role in standing in the gap during the Capitol events and recognizes the gravity of his actions.
- Beau stresses that Pence's recent statements laying blame on Trump are not enough; he needs to explain his actions and decisions in his words to the American people.
- He urges Pence to share the full truth about what was at stake that day and why he chose to stay, rather than leaving through safer options.
- Beau points out that Pence needs to address the American people directly, with clarity and honesty, to have a lasting impact on the country.
- He mentions that if Pence fails to tell his story, history will be crafted by others, and his actions may lose their significance over time.
- Beau underlines the importance of Pence's role in combating authoritarianism and nationalism, urging him to take decisive actions beyond mere statements.
- He suggests that speaking openly to the committee and being transparent about his concerns and motivations could have a profound effect on the nation.
- Beau warns that if Pence continues to delay, his historical legacy may diminish as others shape his narrative.
- He concludes by expressing the urgency of Pence's situation, indicating that time is running out for him to solidify his place in history.

### Quotes

- "Had he made different decisions that day, we would be living in a very different world right now."
- "He's running out of time."
- "He has to tell the American people what happened, and what was at risk, and why it was so important for him to stay."

### Oneliner

Beau stresses Pence's need to transparently explain his actions during the Capitol events to secure his place in history and combat authoritarianism.

### Audience

American citizens

### On-the-ground actions from transcript

- Speak openly and honestly about critical events (implied)
- Share personal motivations and risks taken with transparency (implied)
- Take decisive actions beyond statements to combat authoritarianism (implied)

### Whats missing in summary

The emotional depth and urgency conveyed by Beau in urging Pence to tell his story directly and honestly.

### Tags

#Pence #CapitolEvents #AmericanHistory #Authoritarianism #Urgency


## Transcript
Well, howdy there, internet people. It's Bo again.
Today we're going to talk about the sixth. I want to stop right there for a second.
What are you picturing? The sixth.
That's all that was said.
I would imagine you're picturing a specific event off of those two words, the sixth.
That should let you know the gravity of the situation that day.
You weren't thinking of the sixth of this month.
You were thinking of a very specific event.
An event that is part of American history, it's recent, we're probably not viewing
it as American history, but it's going to be a defining event, a major event.
It changed the way we speak, the sixth.
Prior to that day, prior to the date you're thinking of at the Capitol, if it was 1998,
2010, 2018, and somebody said the 6th, a bunch of history buffs sitting around talking, and
somebody said the 6th.
They would have been talking about June, but not anymore.
The events that occurred at the Capitol have replaced D-Day in that terminology.
sixth doesn't conjure up Normandy anymore. It conjures up images of the capital.
Now, whether you like him or not, on that day there was a gap in the defenses of the
American experiment. There was a gap in the defenses of the Republic, and Pence
stood in that gap. At that moment, on that day, he did his part, and nothing I'm
about to say can take that away. He did that. Had he made different decisions
that day, we would be living in a very different world right now. If he had
decided to leave, if he had decided that it wasn't worth it or to play ball, the
world would look very different. Now, Pence gave an interview and it was his
strongest statement yet. He laid the blame for what happened at the
Capitol at Trump's feet and he said that he endangered me and my family and
everybody else there. Strongest language yet from him. But it's not enough. It's
not enough. Pence has had the opportunity to write his paragraph in the history
books. When this is talked about there will be a section about him and he's
had the chance to to write it to say hey this is why I stayed. I know most people
want to know why he didn't get in the limo. I think most of us know why he
didn't get in the limo. I want to know why he didn't take any of the other
hundred ways that he could have gotten away from that building. Why he felt it
was important enough to stay there at personal risk and complete the actions
of that day. That's what people need to know. That's what the American people
need to know and he has to tell them and it can't be through a sound bite and it
can't be through a book. He was in the executive branch, he has to act like it.
He has to tell the American people what happened in his words or somebody else
is going to tell the story. The longer he waits the more the the luster of his
actions that day tarnishes. He already waited until it was politically
advantageous for him to really pin it on Trump. I mean, right after the midterms
Trump been knocked down and Pence walks up and just kicks him once, you know.
And that's not a good look, that's not the actions that fit the myth that will be crafted
about the man who stood in the gap.
If he wants that place in history, he has to tell his story, he has to tell the American
people what happened, and what was at risk, and why it was so important for him to stay
and complete the actions of that day.
He can say that, you know, it takes courage to uphold the law, and I get it, but he has
to explain to the American people what was at risk in no uncertain terms.
He just has to put it out there.
If he did that and was honest about what he was worried about and what was at stake, I
think it would have a remarkable impact on this country.
He completed the actions of that day.
He didn't complete the mission because the mission is to stop the same kind of authoritarian
nationalism that was spreading on the last, the sixth. He has to do that. His
little sound bites, that's not enough. He should have spoken to the committee
openly. The longer he waits, the more his star fades. And the thing about American
history is that historians will know the truth. They'll know how critical he was
to the events of that day. But most Americans they don't learn American
history. They learn American mythology and nobody's writing his myth. It will be
transferred to somebody else.
He's running out of time.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}