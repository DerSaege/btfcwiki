---
title: Let's talk about Meidas Touch, Stonekettle, and myself....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d22EomnrUYo) |
| Published | 2022/11/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the different approaches used by Midas Touch and Stone Kettle in analyzing data related to polling places.
- Midas Touch utilized data analysis, including early voting numbers and registrations, to make predictions.
- Stone Kettle's approach involved pointing out the limitations of polling, such as only reaching those willing to answer unknown phone numbers.
- Beau shares his method of analyzing YouTube analytics and social media engagement to predict an increase in voter turnout among younger demographics.
- Criticizes the media's heavy reliance on polls, suggesting a need for new polling methods and less emphasis on polling results.
- Advocates for journalists to give candidates a platform to speak freely about their platforms, allowing viewers to make informed decisions.
- Believes that democracy requires informed citizenship and accurate polling to capture the whole picture.
- Notes that younger voters, often overlooked in polls, showed up unexpectedly in the recent election, catching pollsters off guard.

### Quotes

- "The analytics that YouTube provides is scary."
- "Democracy requires advanced citizenship."
- "Polls have to be accurate, but they were only measuring part of the picture."

### Oneliner

Beau shares insights on different data analysis approaches, media reliance on polls, and the need for informed citizenship in democracy.

### Audience

Media consumers

### On-the-ground actions from transcript

- Analyze data from various sources to understand trends and patterns in your community (implied).
- Encourage journalists to provide platforms for candidates to speak directly about their platforms (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of different data analysis approaches, the limitations of polling, and the importance of informed citizenship in democracy.

### Tags

#DataAnalysis #Polling #Media #Democracy #InformedCitizenship


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Midas Touch and Stone
Kettle and myself.
I've had a number of questions related to this topic.
Both Stone Kettle and Midas Touch were brought up.
So we're going to go through it because it
shows the different means that can be
used to reach a conclusion.
The question is, hey, what did y'all
know that the giant polling places didn't?
Because all of us at some point in time
had referenced the concept of unlikely voters
and called into question the polls.
They probably didn't use the term unlikely voters.
They probably said something else.
So Midas Touch, I know what they did.
They used data analysis.
I want to say they pulled early voting
numbers with their registrations and extrapolated from that.
Now, my understanding is that they
took a lot of static for doing it that way
and took a lot of heat for it because it's unconventional.
If it was me, I would be filming a giant I told you
so video right now.
With Stone Kettle, I mean, I don't know.
My guess, just normal cynicism.
Something along the lines of, I don't
know how to soften his language, something
along the lines of the only people included in the polls
would be those willing to answer a phone
call from an unknown number in this day and age,
only that would be said in a much more
colorful and humorous fashion.
I don't know that he put any analysis into it
beyond they're not talking to everybody
and they're acting as though they are,
which when you're talking about polling,
that's a pretty important aspect of it.
Pointing that out probably led him to the conclusion
that, hey, younger people are typically more liberal.
Again, with him, I don't know for certain.
Me?
Y'all told me.
Y'all told me.
The analytics that YouTube provides is scary.
And I noticed that not just on mine,
but on a lot of more progressive channels,
the demographics, the younger demographics,
were increasing in watch time.
That's what really led me to start looking at it
and then looking at the engagement on social media.
That's how I did it.
Not quite as scientific, but it led me to the belief
that there were going to be people showing up that nobody
was counting on, because the demographics involved
typically don't vote in large numbers.
So now they are.
So this shows various means to reach a conclusion.
The media in the US, generally speaking,
they put a lot of emphasis on polls.
And the reason they do that is because they're typically right.
They are typically correct.
However, when there are glaring oversights,
such as conducting the poll using a behavior that
isn't common among a certain demographic,
or discounting a generation of people,
because typically they don't vote in high numbers,
and just kind of forgetting about the fact
that they were directly attacked, that can skew them.
And that's what's happened.
They're going to have to come up with a new method of polling.
Calling people's phones, that's not going to work.
And I would also suggest that journalists
should stop putting so much emphasis on polls,
because that in and of itself is an advertising mechanism,
that bandwagon mentality.
I don't know, maybe bring the candidates on,
ask them questions.
Let them talk about their platform in their words.
And then allow the viewer to fact check it,
or see if it's even worth fact checking.
A lot of the candidates, had they
been allowed to just speak, their turnouts probably
would have been even lower.
There are a number of candidates who didn't win,
but captured a decent percentage,
that if people knew what they were really like when they
weren't reading a script, they probably
wouldn't have voted for them.
Democracy requires advanced citizenship.
Polls, it's a guideline, but the polls have to be accurate.
And they were accurate in measuring
what they were measuring, but they were only
measuring part of the picture.
And that is why a whole lot of younger people,
who are often forgot about, showed up.
And the pollsters weren't ready for it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}