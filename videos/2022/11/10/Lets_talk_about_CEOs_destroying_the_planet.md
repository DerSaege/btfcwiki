---
title: Let's talk about CEOs "destroying the planet"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BigMDFd8eME) |
| Published | 2022/11/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Report released by CEOs before COP 27, focusing on climate change and agriculture.
- CEOs from major companies like Bayer, Mars, McDonald's, PepsiCo involved.
- Report stresses the need to change current practices to prevent planetary destruction.
- Critics skeptical about companies' true intentions, labeling it as PR move.
- Despite potential PR motives, the report's release can still have a positive impact.
- CEO of Mars draws criticism for vague statement on human and planetary health interconnection.
- Some believe the report can serve as a wake-up call for those denying or ignoring climate change.
- The timing of the report's release before COP 27 raises suspicions of PR tactics.
- The report can be used to drive public discourse and influence policymakers.
- Beau encourages using the report to hold representatives accountable and spark change.

### Quotes

- "We have to change the way we're doing things because we are destroying the planet."
- "The interconnection between human health and planetary health is more evident than ever before."
- "Still no action. So those who believe it's PR, they've got good grounds to believe it."

### Oneliner

CEOs release report on agriculture and climate change, sparking skepticism but offering potential for positive impact on public discourse and policy.

### Audience

Climate activists, policymakers, environmental advocates.

### On-the-ground actions from transcript

- Contact your representatives to advocate for sustainable agricultural practices (implied).
- Use the report to raise awareness and drive public discourse on climate change (implied).

### What's missing in summary

The detailed examples and nuances of how large companies' actions impact climate change and the importance of public awareness and advocacy. 

### Tags

#CEOs #Agriculture #ClimateChange #PublicDiscourse #PolicyChange


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a report
that came out from a network of CEOs all over the world
and what it has to do with large agriculture
and what it has to do with you.
The report is being released right before COP 27,
which is the giant UN meeting dealing with climate change.
It's put out by the Sustainable Market Initiative, SMI.
This is a group of heavy hitters.
We're talking Bayer, Mars, McDonald's, PepsiCo.
Huge companies.
Huge companies that have a whole lot to do with large agriculture.
For those that don't understand or may never have been told the scope,
about half of all habitable land is used for agriculture or pasture for animals.
And about 70% of fresh water is used for that.
It's huge. It is huge.
This report basically says,
hey, we have to change the way we're doing things
because we are destroying the planet.
Quote, destroying the planet.
That's, I mean, that's a bold statement.
That's a big statement coming from large companies
who make a whole lot of money off of these products,
which leads to part of the criticism.
And it's not like it's bad faith criticism.
The suggestion is that none of these companies actually care.
That they're going to do anything about this report.
That it's really just PR.
They very well might be right,
but while the companies may have produced the report for public relations purposes,
the report is produced.
It's out there.
It's going to be available.
And it includes destroying the planet.
That might be something that could be used to guide local land use.
It might be something that could be used to wake people up
because you still have a large group of people who,
for whatever reason, don't believe it's happening
or don't believe that it has anything to do with people or whatever.
One quote that went along with this kind of shows how disconnected these CEOs are.
And this is from the CEO of Mars.
The interconnection between human health and planetary health
is more evident than ever before.
I mean, what's that even mean?
Were there people running massive companies
who believed that if the Earth died, we'd be okay?
Is that what I'm taking away from that sentence?
Given statements like that, yeah, it makes sense.
The criticism that suggests this is all PR makes sense,
especially when you think about the fact that it's being released right before COP 27.
COP 27, the big UN meeting on climate change.
Do you know why it's called COP 27?
Because it's the 27th meeting.
Quarter of a century.
Still no action.
So those who believe it's PR, they've got good grounds to believe it.
But that doesn't mean that the report isn't useful
because it can be injected into public conversation about this.
It can demonstrate to those who don't want to hear it
that even the large companies making billions are saying this is what's happening.
It could be used as a turning point.
It might be something you want to draw attention to
when it comes to your representatives in D.C. or whatever country you're in.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}