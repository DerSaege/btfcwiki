---
title: Let's talk about Starlink and regulations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PaNRQf5WpVQ) |
| Published | 2022/11/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the Government Accountability Office (GAO) and its role in ensuring government entities follow federal processes and auditing them.
- Notes the GAO's determination that mega constellations of satellites should undergo environmental review before being licensed by the FCC.
- Mentions the potential issues like debris, more space junk, and light pollution associated with these satellite constellations.
- Points out that attention will likely be on Musk and his companies due to their leadership in this area.
- Emphasizes that the FCC has been providing exemptions to these constellations from environmental reviews but may now change that based on GAO's findings.
- Speculates on the impact of environmental regulations on projects like Starlink, foreseeing issues with light pollution.
- Musk's companies express concerns about the potential negative impact on American innovation and increased costs due to environmental regulations.
- Astronomers are excited about the news, as they have long been concerned about the implications of satellite constellations on stargazing.
- Raises the larger question of how much we are willing to alter the environment for technological advancement and convenience.
- Raises concerns about how satellite constellations could obscure the night sky and whether people have a right to see natural constellations.

### Quotes

- "How much are we willing to adapt our environment?"
- "Do you, do we, have a right to see the night sky?"

### Oneliner

Beau introduces the GAO's call for environmental assessments on satellite constellations, pondering the trade-offs between technological progress and preserving the night sky.

### Audience

Technology enthusiasts, environmentalists, stargazers

### On-the-ground actions from transcript

- Contact local astronomy clubs to learn more about light pollution and ways to advocate for clear night skies (suggested)
- Join environmental organizations working on light pollution mitigation efforts (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the regulatory implications for satellite constellations and raises thought-provoking questions about balancing technological innovation with environmental preservation.


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
the Government Accountability Office, the GAO, and what that office has to do
with satellites and the environment. The GAO is basically an outfit that is responsible for
making sure that government entities follow federal processes and kind of audit them.
They have determined that mega constellations of satellites should undergo environmental review
before they are licensed by the FCC. So they will look at whether or not these constellations are
likely to cause debris, more space junk, light pollution, stuff like this.
As this news comes out, everybody is certainly going to turn to one person who is already in
the headlines. Everybody's going to be looking at Musk and his companies because they're kind of the
leader. They're the best known in this right now. I would point out it will impact other companies.
It will impact other projects. The FCC, for their part, agrees. Up until now they've been giving
these types of constellations kind of a categorical exemption to the environmental
review process. But the GAO has kind of gotten everybody on the same page. The FCC appears
to be waiting for the White House counsel to kind of put out a little bit more guidance on this.
But at this point, it appears that the Starlink project is going to have to start undergoing
environmental review. Most people believe that it is highly likely that there's going to be a light
pollution issue. The debris, that's kind of up in the air. There's a little bit more debate about
that. But most people are fairly certain there's going to be a light pollution issue. Musk's
companies, in a filing, they basically said that if this went forward it would hobble or doom
American innovation when it comes to this sort of technology. I mean, it's definitely going to
make it cost more. I think there might be a little bit of hyperbole in that statement.
But they will probably have to find some way to mitigate the light pollution issues,
which will undoubtedly make this process more expensive. But I don't think it's going to stop it.
Now, astronomers are incredibly excited about this news. This has been a concern of theirs
for years. This isn't a recent development just because of everything that has been going on.
The GAO's review of this started a while ago, and there has been debate about it for years.
It's just now kind of coming to the forefront at a time when Musk certainly doesn't need it
to start making headlines. But this is one of those questions that humanity is going to have
to face. How much are we willing to adapt our environment? And how much of our environment
are we willing to give up for the sake of convenience and technological innovation?
If enough of these constellations go up, you won't see the real constellations. You'll see
the satellites. The light from them will obscure the view. So the question is, do you, do we,
do you, do we have a right to see the night sky? Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}