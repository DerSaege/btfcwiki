---
title: Let's talk about a new way of looking at small actions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BuiG49rMNRk) |
| Published | 2022/11/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing the concept of changing the world through small steps and a new way to frame it.
- Encouraging small acts that may seem insignificant but collectively can make a difference.
- Emphasizing the idea of doing what you can, where you can, for as long as you can to make a positive impact.
- Comparing the impact of small present actions on the future to the concept of changing the present by altering something in the past.
- Noting how time travel tropes in TV shows and movies often illustrate how small actions can greatly impact outcomes.
- Presenting a unique perspective by linking present actions to future consequences through the lens of time travel tropes.
- Encouraging reflection on the significance of individual actions and their potential long-term effects on the world.
- Posing a rhetorical question about the potential outcomes if certain actions were taken decades ago.
- Humorously mentioning the desire for time travel to fix past mistakes, adding a lighthearted touch to the thought-provoking concept.

### Quotes

- "Be the change you want to see in the world."
- "If somebody had done it 50 years ago, what would the outcome be today?"
- "Time travel. When? Really doesn't matter."

### Oneliner

Beau introduces changing the world through small steps, linking present actions to future consequences akin to time travel tropes, urging reflection on individual impact.

### Audience

Change-makers, Future-thinkers

### On-the-ground actions from transcript

- Encourage and practice small acts of kindness and positive change in your daily life (exemplified).
- Take a moment to think about how your present actions might shape the future in a significant way (suggested).

### Whats missing in summary

The full transcript dives deeper into the idea of individual agency and the potential long-term impact of seemingly small actions on the world.

### Tags

#ChangeTheWorld #SmallSteps #TimeTravel #Impact #Reflection


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about changing the world
one small step at a time.
And we're going to talk about a new, new to me anyway,
interesting way to frame it.
It's a concept that you're definitely
familiar with whether you realize it or not.
You've heard it on this channel.
The general idea is that if enough people engage
in small acts that at the time may even seem insignificant,
it can change the world.
You can make the world better by these little baby steps.
On this channel, you've probably heard me say,
do what you can, when you can, where you can,
for as long as you can.
Be the change you want to see in the world.
Pay it forward, so on and so forth, forever.
I saw a new way to frame this.
I wish I knew where this came from.
Sadly, I don't.
But here it is.
When people travel to the past, they
worry about radically changing the present
by doing something small.
Few people think that they can radically change the future
by doing something small in the present.
Pretty profound for a meme, but it's accurate.
It's accurate.
If you think about TV shows and movies,
there are just piles of tropes that deal with time travel
and deal with the idea that the most insignificant action that
occurs when you're traveling through time
can just greatly alter what you return to when
you come back to the present.
I mean, it's the plot of like two dozen movies.
I have never seen it framed through the lens of comparing
that to doing something in the present,
knowing that it's going to impact
the future in a radical way.
All of those sayings, all of those little cliches
about that, that's all what they're getting at.
But this presentation of it taps into something
that most people are familiar with.
Most people are familiar with those rules of time travel.
And they can apply it.
It makes it more relatable in this really bizarre way.
Again, sadly, I don't know where this came from.
But if you ever doubt what you do,
whether what you do has any significance,
whether it really makes an impact when you're out there
trying to make the world better, however you choose to do it,
keep this in mind.
If somebody had done it 50 years ago,
what would the outcome be today?
So just something to kind of think over
until we actually get time travel
and can go back and fix the mistakes.
What do we want?
Time travel.
When?
Really doesn't matter.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}