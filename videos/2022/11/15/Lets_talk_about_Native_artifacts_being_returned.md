---
title: Let's talk about Native artifacts being returned....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nOH1FE7YV_8) |
| Published | 2022/11/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 150 artifacts were returned to the Oglala and Cheyenne River Sioux from a museum in Massachusetts.
- These artifacts were believed to have been taken from a grave site at Wounded Knee and include clothing, weapons, and personal effects.
- The museum, although not legally obligated, returned the artifacts because it was the right thing to do.
- The incident at Wounded Knee in 1890 resulted from a lack of understanding by the U.S. of Native culture, leading to a tragic massacre.
- Museums, colleges, and the federal government possess around 850,000 artifacts, with more than 100,000 being human remains, still not returned to their rightful owners.
- The theft of Native culture is an ongoing issue with numerous human remains and artifacts not where they belong.
- Despite the clear ownership of these artifacts, delays in returning them are often due to financial reasons or the belief that the original owners don't need them back.
- The return of these artifacts is critical, and stories like this need more attention to drive further action.
- The rightful owners of the artifacts are indisputable, and it's imperative to continue shedding light on this issue.
- The return process may be prolonged due to various reasons, but efforts must persist until justice is served.

### Quotes

- "The theft of Native culture is an ongoing issue."
- "More than 100,000 articles that are human remains are in the possession of museums."
- "These stories will happen more and more often, hopefully in larger quantities."
- "There's no dispute about who the rightful owners of this property are."
- "It is vital to keep the focus on it."

### Oneliner

Beau sheds light on the ongoing issue of artifacts taken from Native grave sites, stressing the importance of their return and the need for continued awareness and action.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Contact museums and institutions to urge the return of artifacts and human remains (suggested)
- Support initiatives advocating for the repatriation of stolen cultural items (implied)

### Whats missing in summary

The emotional weight and historical significance conveyed by Beau's storytelling.

### Tags

#ArtifactReturn #NativeCulture #Repatriation #CulturalHeritage #Awareness


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the return
of some artifacts.
And how many times I'm going to make a video like this?
Because I had a friend ask a question.
I found the answer.
And I think this may shed a little bit of light
on why this is so important and why talking about it
is so important.
Okay, so in this instance, 150 artifacts were or have been returned to the Oglala and Cheyenne
River Sioux from a museum in Massachusetts.
These artifacts are believed to have been taken from a grave site at Wounded Knee.
The artifacts include clothing, weapons, and personal effects.
The museum has had them a long time.
Now my understanding is that this museum is a private museum, which means they're not
legally obligated to do what they did, but they're doing it because it's the right thing
to do.
The incident at Wounded Knee occurred in 1890, and this was brought about because the U.S.
did not understand what was happening, what was being witnessed. They weren't
woke. They didn't understand the culture. They saw a ghost dance and thought it
was a prelude to an attack. So they attempted to disarm this group of
natives. That process went awry and it led to a massacre that has stained the
United States ever since. Now when I said I was going to do a video about this to
a friend of mine he's like how many of these videos you're gonna do didn't this
just happen like three months ago? Didn't something real similar? I mean how many
more of these do you have in you? So in the possession of museums and colleges
and the federal government are about 850,000 artifacts. 150 artifacts were
recently returned. There are more than 850,000 artifacts that still are in the
hands of people who don't own them. They're still somewhere they don't
belong. More importantly, of that, more than a hundred thousand of them are human
remains. When we talk about the theft of Native culture, we often talk about it in
In the past tense, it's not.
It's going on right now.
More than 100,000 articles that are human remains are in the possession of museums,
colleges, and the federal government, rather than being where they should.
This issue is huge.
These stories, hopefully, they're going to happen more and more often, and hopefully
in larger quantities.
There's not any dispute about where this stuff should be.
There's no dispute about who the rightful owners of this property are.
It's just taking time because, well, in some cases, it's revenue.
In some cases, it's just a belief that those people don't really need it back.
This story will go on as long as it has to.
These types of videos will continue to be made because it is important to keep the focus
on it.
Yeah, a lot is being done, a lot more has to be done.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}