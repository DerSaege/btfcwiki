---
title: Let's talk about Trump, NY, and the monitor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uAfdCL-tU1Y) |
| Published | 2022/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on a legal development in a $250 million lawsuit in New York brought by the attorney general.
- Concerns about Team Trump potentially moving assets outside of New York jurisdiction.
- Judge appointing a monitor to oversee asset moves and corporate restructuring.
- The judge's decision to appoint an independent monitor due to persistent misrepresentations in Trump's financial statements.
- The appointment of a monitor aims to prevent further fraud or illegality.
- Attorney general's office sees this as a win.
- Trump's legal team facing setbacks in the case.
- The attorney general is determined to continue the pursuit of justice despite any obstacles.
- This case could potentially lead to the collapse of the Trump business empire.
- Trump may have to sell properties if the attorney general wins, possibly below their actual value.

### Quotes

- "No further fraud or illegality."
- "The attorney general basically said that no amount of lawsuits, delays, anything like that is going to stop them."
- "This is one that could cause the Trump business empire to truly collapse."

### Oneliner

Beau provides an update on a legal development in a $250 million lawsuit in New York, where a monitor is appointed due to concerns about asset moves and persistent misrepresentations in Trump's financial statements, potentially leading to the collapse of the Trump business empire.

### Audience

Legal observers

### On-the-ground actions from transcript

- Support transparency in financial dealings by public figures (exemplified)
- Stay informed about legal developments impacting public figures and potential consequences (exemplified)

### Whats missing in summary

Detailed analysis of the specific legal aspects and implications of the case

### Tags

#LegalUpdate #TrumpLawsuit #AttorneyGeneral #AssetValuation #BusinessEmpire


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to provide yet another update
on yet another legal development in yet another Trump case.
This time dealing with the $250 million lawsuit
in New York brought by the attorney general up there.
This deals with the valuations of assets.
The attorney general was concerned that Team Trump
might decide to move assets outside of the jurisdiction
of New York and ask the judge to appoint a monitor
to make certain that that didn't happen.
The judge agreed and now Team Trump will have to provide
14 days notice to both the judge and the attorney general
before making any major moves as far as assets goes
and provide 30 days notice to the monitor
when corporate restructuring or anything like that.
This is definitely a win for the attorney general's office up there.
Not just did the judge agree with what the attorney general wanted.
There's a specific part in the opinion that I want to read here.
The judge said that because of the, as it's quoted here,
persistent misrepresentations throughout every one of Mr. Trump's financial statements
between 2011 and 2021, the court finds that the appointment
of an independent monitor is the most prudent and narrowly tailored mechanism
to ensure there is no further fraud or illegality.
That's quite a passage.
No further fraud or illegality.
If I was involved in this case and the judge said that,
I would have concerns.
This case doesn't seem to be going well for the Trump team.
The attorney general basically said that no amount of lawsuits, delays,
anything like that is going to stop them
and that they're just going to keep going until they,
I believe she used the term, pursuit of justice.
So this is another setback for the Trump legal team.
This time dealing with not a federal case, but a case up in New York.
This is one that could cause the Trump business empire to truly collapse.
I mean, we're talking about a whole lot of money here.
And if there is a, if the attorney general does win this case,
Trump will be in a position where basically he's going to have to sell properties
and because everybody will know that he has to sell them,
he probably won't even get what they're actually worth.
So this is, again, one more development.
It's been a pretty busy few days for the Trump legal team.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}