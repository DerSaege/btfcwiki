---
title: Let's talk about voting while left....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BSO1gAPTsvA) |
| Published | 2022/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the question of why leftists should vote, pointing out comments that question the differences between political parties and their economic policies.
- Distinguishes between leftists and liberals on the channel, focusing on economic perspectives.
- Illustrates the economic perspective by categorizing both parties as right-wing from a leftist viewpoint, citing lack of support for worker control.
- Raises the scenario of attending a place where people wear "vote blue no matter who" shirts and questions whether one feels comfortable leaving a friend alone based on the crowd's political affiliations.
- Emphasizes the differences in treatment towards marginalized groups by the political parties.
- Encourages reflection on what drove individuals to embrace a leftist ideology, likely rooted in an egalitarian outlook and a desire to alleviate suffering.
- Acknowledges the economic similarities between parties but stresses that there are more aspects to the electoral process and having a leftist perspective.
- Concludes by leaving the decision to vote or not based on individual considerations and perspectives.

### Quotes

- "The parties aren't the same."
- "You have that difference that's pretty remarkable between the two parties."
- "But they're not identical."

### Oneliner

Beau explains why leftists should vote by delineating the differences in treatment towards marginalized groups by political parties, beyond just economic policies.

### Audience

Leftist voters

### On-the-ground actions from transcript

- Have open and honest discussions with friends and peers about the importance of voting and the distinctions between political parties (suggested).
- Advocate for marginalized groups and ensure their voices are heard in the electoral process (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on why leftists should vote, focusing on the treatment of marginalized groups by political parties and the broader aspects of having a leftist ideology.

### Tags

#Voting #Leftists #PoliticalParties #EconomicPolicies #MarginalizedGroups


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about voting while left
because I've gotten a lot of questions and comments
that they share a certain theme and they're making the same point.
They use different analogies to make the point,
but they're making the same point.
And the people making the point, they have two things in common.
They share two things that are pretty identifiable.
The first is that they have my skin tone.
The second is that they're leftists.
And understand on this channel, when we say leftists,
we mean leftists, not liberal.
So the point they're making and the question is,
why should I vote? The parties are the same.
Different analogies, you know, two wings of the same bird,
both serve the same master, you know, stuff like that.
But the general point is the parties are the same.
You're saying that because they're the same
if you look at them through a leftist economic lens.
They're both right wing.
Republicans, just roll with me on this,
understand the Democratic Party is right wing economically.
They don't support worker control of the means of production.
Okay. So when you look at it through that lens,
through that very broad lens, that economic lens,
sure, they're the same.
And in a lot of other ways, they're the same.
They will both advance policies that you're going to see
as very harmful as a leftist.
They're going to both support over-policing.
There's a whole bunch of ways that they're the same.
However, because you're saying, you know,
tell me why I should vote,
you're not philosophically opposed to the electoral process.
You just don't see the point because economically they're the same.
But are they the same in all ways?
Are they identical?
I'm going to give you a scenario, put you in a story.
And there's a question at the end of it.
Answering that question will tell you whether or not the parties are the same.
You go and you pick up your friend from Pride.
And your friend is decked out, representing, rainbow shirt,
rainbow glasses, headband, everything.
And as you're driving back, you decide,
y'all are going to stop off at a hole in the wall,
someplace you've never been before.
As you start to walk through the door, you realize,
you got to go hit the ATM.
When you look through the door,
you see that everybody in there is wearing a vote blue no matter who shirt.
Do you feel comfortable leaving your friend there alone when you go to the ATM?
Then play out that exact same scenario,
only when you look through the door, it's a bunch of people wearing red hats.
Do you feel comfortable leaving them then?
I'm willing to bet those answers are different.
I'm willing to bet it's not yes to both.
Then replay that scenario with your black friend,
with your trans friend, with your friend that's a new arrival to this country.
The parties aren't the same.
When you get down to their treatment of those that are different,
those that have been othered.
Odds are if you're a leftist, at some point,
you looked around the world and you decided to become a leftist.
You weren't born one as much as everybody on the internet likes to pretend.
You know, they came out of the womb, quoting theory and held all of the right positions.
You probably didn't.
At some point, you looked around and something changed for you.
For me, I got tired of watching people suffer.
I'm willing to bet whatever it is that drove you to embrace a left-leaning ideology
had something to do with an egalitarian outlook on the world.
You have that difference that's pretty remarkable between the two parties.
Whether that's enough for you to go out and vote,
that's between you and your friends.
But they're not identical.
I get what you're saying, and you're not wrong when it comes to the economic ideals that they advance.
But there's more to the electoral process than just that.
And I would also suggest there's more to having a left-leaning outlook than just economic theory.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}