---
title: Let's talk about Dan Crenshaw's moment of truth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7xw3p1dF-H4) |
| Published | 2022/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines Republican Congressman Dan Crenshaw's statements and their implications within the party.
- Crenshaw admitted behind closed doors that the election claims were always a lie.
- Those supporting such claims are being laughed at by the politicians they trust.
- The tactics of using lies to rile people up are still being employed by political personalities.
- People who believed in these lies face serious consequences while the politicians continue their careers.
- The Republican Party leadership needs to address this issue seriously.
- True believers in the rhetoric need to question the truth behind it.
- Beau challenges MAGA supporters to entertain the idea that Crenshaw is telling the truth about the lies.
- There is no evidence because it was always a lie to rile people up for power.

### Quotes

- "He's saying behind closed doors, those people that you're supporting, those people that a lot of Republicans are still supporting, out there pushing these narratives today. They're laughing at those people who believe them."
- "They admit it was a lie. Just a political tool."
- "That it's not some plot spanning the globe, that it's just a group of people using rhetoric to rile others up for their own pursuit of power."

### Oneliner

Beau exposes how Republican leaders knowingly used lies to manipulate supporters and gain power, leaving believers facing consequences.

### Audience

Republican Party members

### On-the-ground actions from transcript

- Question the truth behind political rhetoric (suggested)
- Challenge beliefs and leaders within the party (suggested)

### Whats missing in summary

Deeper insights into the repercussions faced by those who believed in the lies and the importance of questioning political narratives. 

### Tags

#RepublicanParty #DanCrenshaw #PoliticalLies #Manipulation #Consequences


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Republican Congressman
Dan Crenshaw and something that he said
that really shines a light on the personalities that
are rising stars within the Republican Party
and what it means for the people who believed them
and what it means that they're still using those tactics
to rile people up.
Dan Crenshaw, in an interview, speaking
of all of the claims about the election, it was always a lie.
The whole thing was always a lie.
And it was a lie meant to rile people up.
It was a lie meant to rile people up.
All those claims.
He's saying behind closed doors, those people
that you're supporting, those people that a lot of Republicans
are still supporting, out there pushing these narratives today.
He's saying that behind closed doors,
they're laughing at those people who believe them.
They're laughing at those people who believe them.
They admit it was a lie.
Just a political tool.
They're still using this tactic, as many of their supporters
have been sentenced to years in a cage,
and some of their most diehard supporters
are currently on trial looking at even longer sentences.
It was always a lie.
That's what they're saying.
That's what the Republicans are saying.
No apologies to the people who they led down that road,
who they riled up.
To the contrary, they're setting the stage
to try to do it again.
To try to use another group of people
who put their trust in those political personalities
to use his term, because he didn't want
to call them politicians.
They put their trust in these political personalities,
and these people are using it to further their own careers,
their own positions of power, while those who believe them,
those who trust them, they're standing in court.
They don't get to see their family for years.
They throw away their entire lives.
And according to Crenshaw, they all know,
all those up at the top, they all know it was always a lie.
No apology, nothing.
Just continuing business as usual,
using the same tactics so another group of people
can throw their lives away over something
that was always a lie.
It's something that the Republican Party really
needs to deal with.
This is something that the leadership
of the Republican Party really needs to address.
It's something that the rank and file of the Republican Party
should probably take into consideration
when they're looking at these ballots.
And it's something that those who are true believers
in this rhetoric need to take a moment and just think,
what if he's telling the truth?
Because I know the response from the MAGA faithful
is going to be to call Crenshaw a rhino
and say that he's lying.
But just for a moment, entertain the idea
that he's telling the truth, that it was always a lie,
that those politicians knew it was always a lie,
and that's why there's no evidence.
That's why they always say that their uncle or their brother
or whoever has the evidence, but you never get to see it.
They never actually put it out there.
That's why they kept losing in court,
that it's not some plot spanning the globe,
that it's just a group of people using rhetoric
to rile others up for their own pursuit of power.
What's more likely?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}