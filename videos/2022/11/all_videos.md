# All videos from November, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-11-30: Let's talk about plans for retaking the House in 2024.... (<a href="https://youtube.com/watch?v=69EG_GdYtvA">watch</a> || <a href="/videos/2022/11/30/Lets_talk_about_plans_for_retaking_the_House_in_2024">transcript &amp; editable summary</a>)

Beau decodes the strategic message behind the House Majority Pack memo targeting specific districts, potentially urging cooperation over confrontation for vulnerable Republicans.

</summary>

"If you want to get off this list, you better be one of the people to cross the aisle every once in a while."
"It might also be a message to the people in these districts, to those representing these districts."
"Probably a smart play."
"I think this is more of a message."
"Conventional wisdom is just them trying to energize the base and say, hey, we're going to get the House back and all of that."

### AI summary (High error rate! Edit errors on video page)

Analyzing a memo from the House Majority Pack and its significance in targeting specific districts for the 2024 elections.
The memo outlines districts in various states, including Arizona, California, Iowa, Michigan, Nebraska, New Jersey, New York, Oregon, Pennsylvania, and Virginia.
Speculations arise on the purpose of releasing the plan early – beyond simply showing preparedness for the elections.
It may serve as a strategic message to both the constituents and representatives in these targeted districts.
With the slim Republican majority in the House, even a few crossovers could impact passing or blocking legislation.
Vulnerable district representatives might find it advantageous to cooperate with Democrats to avoid heavy opposition in future elections.
The memo could be a subtle warning to Republicans in these districts to cooperate occasionally to avoid being targeted for removal.
Beau suggests that Republicans crossing the aisle might be mentioned positively in the coming years to strengthen their position within a potentially shifting voter base.
The underlying message of the memo appears to be more about incentivizing cooperation than just a strategic plan for the elections.
Beau interprets the memo as a calculated move to influence behavior and potentially reduce the need to invest resources in defeating vulnerable Republicans.

Actions:

for political analysts, democratic party members,
Reach out to representatives in targeted districts to understand their stance and encourage bipartisan cooperation (implied).
Support political candidates who prioritize collaboration and bipartisanship in vulnerable districts (implied).
</details>
<details>
<summary>
2022-11-30: Let's talk about a new way of looking at small actions.... (<a href="https://youtube.com/watch?v=BuiG49rMNRk">watch</a> || <a href="/videos/2022/11/30/Lets_talk_about_a_new_way_of_looking_at_small_actions">transcript &amp; editable summary</a>)

Beau introduces changing the world through small steps, linking present actions to future consequences akin to time travel tropes, urging reflection on individual impact.

</summary>

"Be the change you want to see in the world."
"If somebody had done it 50 years ago, what would the outcome be today?"
"Time travel. When? Really doesn't matter."

### AI summary (High error rate! Edit errors on video page)

Introducing the concept of changing the world through small steps and a new way to frame it.
Encouraging small acts that may seem insignificant but collectively can make a difference.
Emphasizing the idea of doing what you can, where you can, for as long as you can to make a positive impact.
Comparing the impact of small present actions on the future to the concept of changing the present by altering something in the past.
Noting how time travel tropes in TV shows and movies often illustrate how small actions can greatly impact outcomes.
Presenting a unique perspective by linking present actions to future consequences through the lens of time travel tropes.
Encouraging reflection on the significance of individual actions and their potential long-term effects on the world.
Posing a rhetorical question about the potential outcomes if certain actions were taken decades ago.
Humorously mentioning the desire for time travel to fix past mistakes, adding a lighthearted touch to the thought-provoking concept.

Actions:

for change-makers, future-thinkers,
Encourage and practice small acts of kindness and positive change in your daily life (exemplified).
Take a moment to think about how your present actions might shape the future in a significant way (suggested).
</details>
<details>
<summary>
2022-11-30: Let's talk about Starlink and regulations.... (<a href="https://youtube.com/watch?v=PaNRQf5WpVQ">watch</a> || <a href="/videos/2022/11/30/Lets_talk_about_Starlink_and_regulations">transcript &amp; editable summary</a>)

Beau introduces the GAO's call for environmental assessments on satellite constellations, pondering the trade-offs between technological progress and preserving the night sky.

</summary>

"How much are we willing to adapt our environment?"
"Do you, do we, have a right to see the night sky?"

### AI summary (High error rate! Edit errors on video page)

Introduces the Government Accountability Office (GAO) and its role in ensuring government entities follow federal processes and auditing them.
Notes the GAO's determination that mega constellations of satellites should undergo environmental review before being licensed by the FCC.
Mentions the potential issues like debris, more space junk, and light pollution associated with these satellite constellations.
Points out that attention will likely be on Musk and his companies due to their leadership in this area.
Emphasizes that the FCC has been providing exemptions to these constellations from environmental reviews but may now change that based on GAO's findings.
Speculates on the impact of environmental regulations on projects like Starlink, foreseeing issues with light pollution.
Musk's companies express concerns about the potential negative impact on American innovation and increased costs due to environmental regulations.
Astronomers are excited about the news, as they have long been concerned about the implications of satellite constellations on stargazing.
Raises the larger question of how much we are willing to alter the environment for technological advancement and convenience.
Raises concerns about how satellite constellations could obscure the night sky and whether people have a right to see natural constellations.

Actions:

for technology enthusiasts, environmentalists, stargazers,
Contact local astronomy clubs to learn more about light pollution and ways to advocate for clear night skies (suggested)
Join environmental organizations working on light pollution mitigation efforts (implied)
</details>
<details>
<summary>
2022-11-29: Let's talk about why Musk's antics matter.... (<a href="https://youtube.com/watch?v=FNS3l8yvvrE">watch</a> || <a href="/videos/2022/11/29/Lets_talk_about_why_Musk_s_antics_matter">transcript &amp; editable summary</a>)

Elon Musk's public sparring with Apple and Twitter reveals the power of extreme wealth to shape markets and influence national discourse, raising concerns about income inequality and concentrated power.

</summary>

"Musk's actions could lead to higher phone prices due to supply issues and competition."
"His pursuit of platforming controversial voices may not be well-received."
"Musk's decisions can have ripple effects on job markets and income inequality."
"Concentration of wealth in few hands can lead to disproportionate influence over the world."
"Beau questions the impact of individuals with extreme wealth on society."

### AI summary (High error rate! Edit errors on video page)

Elon Musk made controversial statements about Apple on Twitter, impacting their relationship.
Musk criticized Apple's advertising on Twitter and their app store standards publicly.
He hinted at starting his own phone company if Twitter is removed from app stores.
Musk's actions could lead to higher phone prices due to supply issues and competition.
Musk's wealth and influence allow him to shape markets and impact national discourse.
His pursuit of platforming controversial voices may not be well-received.
Musk's decisions can have ripple effects on job markets and income inequality.
Concentration of wealth in few hands can lead to disproportionate influence over the world.
Beau questions the impact of individuals with extreme wealth on society.
Musk's potential actions raise concerns about income inequality and concentrated power.

Actions:

for social activists, tech enthusiasts,
Monitor the actions and decisions of wealthy individuals that could impact society (implied).
</details>
<details>
<summary>
2022-11-29: Let's talk about Mike the Pillow Guy wanting to run the GOP.... (<a href="https://youtube.com/watch?v=-ZlQ1MLW5d8">watch</a> || <a href="/videos/2022/11/29/Lets_talk_about_Mike_the_Pillow_Guy_wanting_to_run_the_GOP">transcript &amp; editable summary</a>)

Mike Lindell's bid for the Republican Party chair spot underscores the GOP's struggle with misinformation and potential voter suppression within their base.

</summary>

"If knowledge is knowing that Frankenstein wasn't the monster but was the doctor, wisdom is knowing that the doctor was the monster."
"They will still have problems with their primaries. They will still have people refusing to accept very clear results because they haven't come down on this forcefully."
"The Republican Party has created a situation in which they will end up suppressing their own vote."

### AI summary (High error rate! Edit errors on video page)

Mike Lindell, known as the "pillow guy," is vying to become the new chair of the Republican Party, a position that requires knowledge and wisdom.
Lindell has personally reached out to all 168 voting members to address their concerns, with some interactions lasting up to four hours.
Despite incumbent McDaniel likely having the votes to retain the position, there's uncertainty about Lindell's acceptance of the results.
Lindell has a history of spreading false election information and facing FBI scrutiny over his phone.
The GOP's tolerance of baseless claims and lack of forceful correction has allowed for potential leadership shifts like Lindell's bid.
The party's failure to confront attempts to undermine democratic institutions could lead to voter suppression within their own base.
The GOP's reluctance to challenge misinformation could result in disillusionment among voters and decreased participation in elections.

Actions:

for voters, party members,
Contact local Republican officials to express concerns about leadership decisions (suggested)
Educate fellow voters on the importance of challenging misinformation within political parties (implied)
</details>
<details>
<summary>
2022-11-29: Let's talk about MTG and the marketplace of ideas.... (<a href="https://youtube.com/watch?v=XlfkKYNqfO4">watch</a> || <a href="/videos/2022/11/29/Lets_talk_about_MTG_and_the_marketplace_of_ideas">transcript &amp; editable summary</a>)

Beau dissects the flawed concept of "corporate communism" while discussing the rejection of objectionable ideas in the marketplace of ideas through a capitalist lens.

</summary>

"Opening Twitter for everyone's speech doesn't promote one side. It opens the town square and levels the playing field."
"Their product didn't sell. In fact, it was seen as objectionable to the majority of the people in the marketplace."
"So your ideas are bad and you should feel bad."
"It's not communism. It's not corporate communism because that doesn't exist. It's just good old capitalism."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Exploring the reaction to advertisers pulling out from Twitter due to recent changes.
Representative Marjorie Taylor Greene's tweet accusing corporations of "corporate communism."
Greene's claim that Elon Musk opening Twitter for free speech levels the playing field.
Beau's disagreement with the notion that all ideas deserve a level playing field.
Dissecting the concept of "corporate communism" and why it doesn't hold up.
Pointing out the inclusion of companies like American Express and BlackRock on Greene's list.
The essence of capitalism in the marketplace of ideas and companies choosing to distance themselves from objectionable ideas.
Distinguishing between corporate influence by representatives and true fascism.
The marketplace analogy, where the rejection of certain ideas is a result of capitalism, not communism.
The rejection of objectionable ideas by the majority in the marketplace.
Why the term "corporate communism" is flawed and doesn't apply in this context.
Linking Greene's accusations to power dynamics and governmental influence on corporations.
The role of capitalism in companies making decisions based on public perception and association.
Clarifying that the rejection of certain ideas is a capitalist concept, not corporate communism.
Summarizing the misunderstanding around the concept of "corporate communism."

Actions:

for social media users,
Support platforms that uphold free speech and diverse viewpoints (exemplified)
Educate others on the nuances between capitalism and communism in corporate settings (exemplified)
</details>
<details>
<summary>
2022-11-28: Let's talk about ten years from now and Sci-Fi.... (<a href="https://youtube.com/watch?v=eLj16xvYp3o">watch</a> || <a href="/videos/2022/11/28/Lets_talk_about_ten_years_from_now_and_Sci-Fi">transcript &amp; editable summary</a>)

A NASA official predicts people will live on the moon within this decade, marking a significant leap in space exploration and technological progress.

</summary>

"Certainly in this decade, we are going to have people living for durations… They will have habitats. They will have rovers on the ground."
"To me, that is just wild."
"We're kind of on the verge of a new era when it comes to this sort of exploration."

### AI summary (High error rate! Edit errors on video page)

Beau talks about a NASA official's statement regarding people living on the moon in this decade.
The official from the Orion Lunar Space Program mentioned habitats and rovers on the moon's surface for extended durations.
Technological advances are propelling us towards what was previously considered science fiction.
The expectation is that by 2030, or by 2032 at the latest, people will be living on the moon.
This progress signifies the first step towards real space exploration, expanding beyond current limitations.
Beau expresses his surprise at the idea of people living on the moon and how it will impact technological advancements on Earth.
The increased space exploration will likely lead to more innovations benefiting daily life.
There is a belief in a forthcoming new era of exploration with vast possibilities beyond current understanding.
Beau concludes by saying this marks a significant moment in space exploration.
The focus is on the rapid rate of technological advancements guiding us toward what was once deemed science fiction.

Actions:

for space enthusiasts, scientists,
Stay updated on space exploration developments and research (suggested)
Support initiatives that advance space exploration and technology (implied)
Encourage STEM education and interest in space sciences (implied)
</details>
<details>
<summary>
2022-11-28: Let's talk about Trump's meeting.... (<a href="https://youtube.com/watch?v=DgnJRtWHpuA">watch</a> || <a href="/videos/2022/11/28/Lets_talk_about_Trump_s_meeting">transcript &amp; editable summary</a>)

Former President Trump's meeting with controversial figures, framed as accidental, prompts Beau to urge acceptance of Trump's consistent messaging and followers' awareness.

</summary>

"Trump is who he is, and he's telling everybody, but none dare call it fascism."
"I think the sooner we stop acting like this is surprising, that this is shocking, the better."
"He's aware of the message he puts out. He's aware of who his followers are, even when he accidentally meets them."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's meeting with controversial figures, Kanye West and Nick Fuentes, made headlines.
Fuentes, openly calling for dictatorship, was part of the meeting that Trump framed as accidental.
Trump's framing of the meeting involved downplaying his knowledge of meeting Fuentes.
Beau questions the coincidence of Trump accidentally meeting figures like Fuentes but not civil rights activists.
Beau points out that the meeting wasn't surprising or shocking, given Trump's previous actions and statements.
The media coverage of the meeting described the figures as controversial rather than explicitly labeling them as fascist or racist.
Beau argues that it's time to stop pretending to be surprised by Trump's actions and accept who he is.
He stresses the need to acknowledge the message Trump consistently puts out and his awareness of his followers.
Beau concludes by urging people to stop acting surprised by Trump's behavior and to recognize the reality of the situation.

Actions:

for observers and critics,
Acknowledge and accept the consistent messaging and actions of political figures (implied)
Stop pretending to be surprised by the behavior of public figures (implied)
</details>
<details>
<summary>
2022-11-28: Let's talk about Herschel Walker's statement.... (<a href="https://youtube.com/watch?v=GzizGsTvRjY">watch</a> || <a href="/videos/2022/11/28/Lets_talk_about_Herschel_Walker_s_statement">transcript &amp; editable summary</a>)

Beau challenges Walker's belief that young people haven't earned the right to change the country, advocating for their voices and perspectives based on informed decisions and future implications.

</summary>

"Nobody has ever died for the national anthem. They died for the people around them."
"The younger crop, they have every right to try to change this country, because they're going to be the ones who pay the price if it doesn't change."
"Maybe they have different ideas because they're better informed."

### AI summary (High error rate! Edit errors on video page)

Ignored Herschel Walker's campaign due to lack of policy statements, resembling stand-up comedy routines.
Walker expressed the belief that young people born after 1990 haven't earned the right to change the country.
Beau disagrees with Walker's sentiment, stating that people don't die for the national anthem but for those around them.
Beau mentions friends with prosthetics, a diverse group that got their injuries in their early 20s.
American KIA are typically young, and statistics show the average age of individuals in wars is not in the age group Walker mentioned.
Beau argues that the younger generation has every right to try to change the country as they will bear the consequences if it remains unchanged.
He suggests that the younger generation, having grown up with unlimited access to information, may have different ideas because they are better informed.
Beau points out that people who grew up with the internet have the right to exercise their voting rights.
He questions the impact of Walker's statement on the election, particularly in a state like Georgia with numerous military installations.
Beau implies that Walker's views may not resonate well in areas like Columbus, which have a keen interest in building a better future rather than clinging to an idealized past.

Actions:

for voters, young activists,
Challenge outdated beliefs and encourage informed decision-making (implied)
Support and empower young voices in shaping the future of the country (implied)
</details>
<details>
<summary>
2022-11-27: Let's talk about finding your network after Twitter.... (<a href="https://youtube.com/watch?v=XSzdcskD4VM">watch</a> || <a href="/videos/2022/11/27/Lets_talk_about_finding_your_network_after_Twitter">transcript &amp; editable summary</a>)

Beau advises on maintaining continuity and finding community leaders to reestablish social networks when transitioning between social media platforms.

</summary>

"Make yourself easy to find for people that are looking for you."
"Y'all have something in common. Y'all have something that y'all share."
"It's just people helping people."
"Twitter was never really your friend or your home. It was just a company."
"Just make yourself identifiable to those within your community, and they'll find you."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of reestablishing social networks when a social media platform disappears, specifically addressing communities within Twitter.
Advises maintaining continuity by using the same profile picture and handle when transitioning to a new social media platform.
Suggests hashtagging your community or group to make it easier for others to find you on new platforms.
Recommends interacting with individuals who have large followings within your community to reconnect with familiar faces.
Encourages creating cross-platform communities to strengthen networks and increase resources.
Acknowledges the fear and concern within disabled Twitter communities regarding losing their network.
Emphasizes the shared passion and common interests that bind communities together, making it easier to reconnect.
Urges individuals to establish their community on multiple social media outlets to prepare for any platform changes.
Reminds people that while social media platforms may change, the supportive community will remain strong.
Advocates for helping one another and maintaining connections during platform transitions.

Actions:

for social media users,
Maintain continuity by using the same profile picture and handle on new social media platforms (implied)
Hashtag your community or group to make it easier for others to find you on different platforms (implied)
Interact with individuals who have large followings within your community to reconnect with familiar faces (implied)
Establish your community on multiple social media outlets to prepare for any platform changes (implied)
</details>
<details>
<summary>
2022-11-27: Let's talk about a split in the Biden admin.... (<a href="https://youtube.com/watch?v=ma9VNvyh6jU">watch</a> || <a href="/videos/2022/11/27/Lets_talk_about_a_split_in_the_Biden_admin">transcript &amp; editable summary</a>)

There is no split in Biden's team on Ukraine; it's about understanding the military perspective amidst a protracted conflict.

</summary>

"Nothing about Ukraine without Ukraine."
"War is a continuation of politics by other means."
"Russia has two options. They withdraw or they get themselves embroiled into a years-long war."
"If this goes on, it's going to be a long war."
"Ukraine can't push Russia out anytime soon."

### AI summary (High error rate! Edit errors on video page)

There is a perceived split within the Biden administration regarding foreign policy on Ukraine.
The Biden administration's policy is "Nothing about Ukraine without Ukraine."
General Milley's suggestion for Ukraine to negotiate is not a split in the administration, but a military perspective.
Milley believes that Russia has two options: withdraw or face a prolonged and devastating war.
Ukraine has entered a protracted conflict, making it difficult for Russia to advance further.
Milley gives Russia less than a 1% chance of winning in the long run.
Media misreported Milley's statement about Ukraine not pushing Russia out anytime soon.
The situation in Ukraine is turning into a hammer and anvil scenario for Russia.
Russia faces a high likelihood of losing if the conflict continues.
Milley emphasized the need for a political option due to the current situation in Ukraine.

Actions:

for foreign policy enthusiasts,
Contact local representatives to advocate for a diplomatic resolution (suggested)
Join organizations supporting peace efforts in Ukraine (implied)
</details>
<details>
<summary>
2022-11-27: Let's talk about Georgia's runoff.... (<a href="https://youtube.com/watch?v=BxWxdco5yVA">watch</a> || <a href="/videos/2022/11/27/Lets_talk_about_Georgia_s_runoff">transcript &amp; editable summary</a>)

Georgia's runoff election dynamics shift as Republicans grapple with support for Walker, influenced by Trump's divisive impact, prompting uncertainty about voter turnout and party loyalty.

</summary>

"A vote for Walker is a vote for Trump."
"Georgia isn't MAGA Central anymore."
"I wouldn't worry about anything Walker says. I wouldn't respond to anything."
"Because there are growing factions within the Republican Party that understand how damaging Trump is to the party as a whole."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Georgia's runoff election is causing speculation and interest due to unique circumstances.
Uncertainty surrounds voter turnout and party support in the runoff election.
Walker and Warnock are the key contenders, with many leaning towards Warnock irrespective of party affiliation.
The previous motivation for supporting Walker, Republican Senate control, is no longer relevant.
Walker's candidacy is not particularly strong, especially with internal Republican division regarding Trump.
Many Republicans are weary of supporting Walker as they view it as supporting Trump.
Recent shifts in Georgia's political landscape indicate less support for Trump and his rhetoric.
Advises Warnock's campaign to focus on voter turnout rather than engaging with Walker's statements.
Anticipates lower Republican turnout due to reluctance to support Trump.
Acknowledges the damage Trump's influence has had on the Republican Party, particularly evident in past election results.

Actions:

for georgia voters,
Focus on maximizing voter turnout for the upcoming runoff election (suggested).
Acknowledge and address concerns within the Republican Party regarding Trump's influence (implied).
</details>
<details>
<summary>
2022-11-26: Let's talk about the presumed Speaker of the House.... (<a href="https://youtube.com/watch?v=W3ZMIRqx7d4">watch</a> || <a href="/videos/2022/11/26/Lets_talk_about_the_presumed_Speaker_of_the_House">transcript &amp; editable summary</a>)

Beau McGahn delves into Kevin McCarthy's political theatrics, revealing his weak leadership and the Republican Party's misguided priorities, setting them up for failure in 2024.

</summary>

"The real reason is the space laser lady. That's what it's really about."
"He's not leading, he's the errand boy now, and that's how it is shaping up."
"The Republican Party has still not learned its lesson, and it's setting itself up for another failure in 2024."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of Kevin McCarthy as the possible presumptive Speaker of the House.
McCarthy has been involved in political theatrics post-election, making grand promises.
Beau mentions McCarthy's actions of initiating investigations and removing members of Congress from committees, including Omar, Schiff, and Swalwell.
The real reason behind McCarthy's moves is linked to Marjorie Taylor Greene, who holds influence over him due to her demand for revenge as the "space laser lady."
Despite appearing powerful with his actions, McCarthy is actually a figure of weakness within his own party, becoming an errand boy to maintain his Speaker position.
McCarthy's focus on investigations and statements about Biden's son are not driven by genuine belief but rather by the need to appease the extreme base of the Republican Party.
The Republican Party's emphasis on social media metrics as a gauge of public opinion alienates many normal Republicans who are concerned about real issues like inflation.
McCarthy's lack of control over his own party indicates that the House's control by Republicans is superficial, leading to potential chaos in the upcoming years.
The absence of a unified mission and party within the Republicans sets them up for failure in 2024, as they prioritize social media presence over sound policy ideas.

Actions:

for political observers,
Contact local representatives to express concerns about real issues like inflation (implied)
Support political candidates with strong policy ideas over social media influencers (implied)
</details>
<details>
<summary>
2022-11-26: Let's talk about Poland, HIMARS, and Russia.... (<a href="https://youtube.com/watch?v=xQeCPkFtJQ8">watch</a> || <a href="/videos/2022/11/26/Lets_talk_about_Poland_HIMARS_and_Russia">transcript &amp; editable summary</a>)

Beau addresses Russia's foreign policy capabilities, showcasing NATO's response to the invasion of Ukraine and the impact on military dynamics in Eastern Europe.

</summary>

"NATO has expanded. NATO has solidified its resolve."
"The reality is the Russian military, the commanders, took money that should have been spent on defense and spent it on their yachts and their houses."
"Knowing is half the battle. The other half is violence."

### AI summary (High error rate! Edit errors on video page)

Addressing Russia's capabilities and successes in foreign policy prompted by a viewer's message questioning Russia's standing.
Explaining how NATO's response to Russia's invasion of Ukraine led to increased armament orders from Poland.
Detailing the significance of the HIMARS rocket system in the conflict between Russia and Ukraine.
Pointing out that Russia's actions in Ukraine have backfired geopolitically, accelerating NATO expansion and military modernization in Eastern Europe.
Emphasizing that Russia's military technology lags behind NATO's advancements.
Noting that Ukraine is intentionally not provided with the best NATO weapons to prevent potential repercussions.
Criticizing Russian military leaders for misusing defense funds on personal luxuries instead of military advancements.
Clarifying that the information presented is not propaganda but a reflection of the current geopolitical realities.

Actions:

for policy analysts, military strategists.,
Contact organizations supporting Ukraine (implied).
Stay informed on geopolitical developments (implied).
</details>
<details>
<summary>
2022-11-26: Let's talk about Arizona, water, and a new market.... (<a href="https://youtube.com/watch?v=0MhmqY9uNrw">watch</a> || <a href="/videos/2022/11/26/Lets_talk_about_Arizona_water_and_a_new_market">transcript &amp; editable summary</a>)

Arizona community faces water crisis as Scottsdale cuts off supply, signaling a looming national challenge demanding urgent reprioritization.

</summary>

"Effective January 1st, Scottsdale is going to bar trucks from exporting water to those outside the city limits."
"I think we know it [black market for water] will happen with 700 homes."
"The United States needs to adjust its priorities, and it needs to do it quickly."

### AI summary (High error rate! Edit errors on video page)

Arizona’s Rio Verde Foothills faces a water crisis as Scottsdale plans to cut off water supply to the rural community.
Scottsdale will bar trucks from exporting water to areas outside the city limits, leaving 700 homes in Rio Verde without reliable water sources.
Despite warnings from Scottsdale to address their water supply issues, the rural community has not been proactive in finding a solution.
Options for Rio Verde include developing their own water system, trucking in water from further away, or potentially resorting to a black market for water.
Lack of local leadership and focus on divisive issues rather than community needs exacerbates the impending crisis.
The situation in Rio Verde is a warning for communities nationwide to address climate-related challenges and prioritize real issues over political distractions.

Actions:

for communities, policymakers, activists,
Develop community-based water solutions in at-risk areas (exemplified)
Advocate for local leaders to prioritize real issues over divisive politics (exemplified)
Prepare for potential water crises by establishing alternative water sources (exemplified)
</details>
<details>
<summary>
2022-11-25: Let's talk about a new item in your grocery store.... (<a href="https://youtube.com/watch?v=FMuq9MaX64M">watch</a> || <a href="/videos/2022/11/25/Lets_talk_about_a_new_item_in_your_grocery_store">transcript &amp; editable summary</a>)

FDA approves lab-grown chicken, sparking debates on climate impact and ethical consumption, with potential long-term benefits.

</summary>

"This will probably fuel a lot of anxiety in more anti-science circles."
"I think that this is probably going to be a huge benefit to everybody over the long haul."
"I'm pretty cool with something that came out of a much cleaner environment."

### AI summary (High error rate! Edit errors on video page)

Historic first: FDA approves lab-grown chicken for sale, soon in grocery stores.
Upside's "cultured chicken" gets the green light, awaiting USDA inspection.
Carbon footprint of lab-grown meat may be high due to current energy infrastructure.
Transition to lab-grown meat could significantly reduce climate change impact in the long term.
Public response to lab-grown meat availability remains to be seen.
Beau supports lab-grown meat due to cleaner production environment and ethical considerations.
Predicts public anxiety and debate over lab-grown meat in the future.
Views lab-grown meat as a positive long-term benefit for everyone.
Beau expresses willingness to consume lab-grown meat based on his experiences on ranches and slaughterhouses.
FDA open to discussing similar approvals with other producers.

Actions:

for climate-conscious consumers,
Support and advocate for sustainable food production practices (implied)
Stay informed about advancements in food technology and their environmental impact (implied)
</details>
<details>
<summary>
2022-11-25: Let's talk about Hakeem Jeffries.... (<a href="https://youtube.com/watch?v=A6kHh2eRx_4">watch</a> || <a href="/videos/2022/11/25/Lets_talk_about_Hakeem_Jeffries">transcript &amp; editable summary</a>)

Representative Hakeem Jeffries, a potential future Speaker of the House, brings a pragmatic approach that may shift the Democratic Party slightly leftward.

</summary>

"He'll be more comfortable cutting deals."
"In this position, he's going to be a corporate dim."
"It looks really bad if the leader of your party in the House loses their election."

### AI summary (High error rate! Edit errors on video page)

Speculates on Representative Hakeem Jeffries as Pelosi's potential replacement, likely the favorite for the position.
Jeffries is poised to lead the Democratic Party in the House and could become the first black Speaker of the House by 2025.
Outlines Jeffries' qualifications, including his educational background and political experience in Congress.
Views Jeffries' potential leadership during a Republican-dominated House positively, noting Pelosi's presence as a resource until 2025.
Expects Jeffries to redefine the term "corporate dim" by being pragmatic and open to incremental change while still leaning towards progressive beliefs.
Suggests Jeffries could be even more powerful than expected due to the fractured nature of the Republican Party and potential infighting.
Notes Jeffries' strong support in his blue district, enhancing his longevity in a leadership role.

Actions:

for politically engaged viewers,
Support progressive candidates in your local elections (implied)
Stay informed about political shifts and party leadership changes (implied)
</details>
<details>
<summary>
2022-11-25: Let's talk about Biden being old and running in 2024.... (<a href="https://youtube.com/watch?v=GSNkVNKFDYk">watch</a> || <a href="/videos/2022/11/25/Lets_talk_about_Biden_being_old_and_running_in_2024">transcript &amp; editable summary</a>)

Beau examines Biden's potential re-election, arguing against early announcements and stressing the importance of performance over age in politics.

</summary>

"Two years is a long time."
"Maybe suddenly people don't care that he's old."
"He shouldn't announce, he shouldn't say that."
"Keep the Republican Party focused on him."
"I don't expect this announcement to occur."

### AI summary (High error rate! Edit errors on video page)

Exploring the possibility of President Biden running for re-election in 2024 and concerns about his age.
Refuting the idea that Biden is too old for re-election, pointing out that two years is a long time in politics.
Speculating on why Biden might not announce early that he's not running again.
Noting that the Republican Party is focused on attacking Biden, indicating they see him as a threat in 2024.
Emphasizing that Biden not announcing his decision keeps the Republican Party focused on him and spares potential candidates from negative attention.
Arguing that Biden's age won't be the deciding factor in his re-election bid, but rather his performance and agenda.
Suggesting that Biden keeping the focus on himself could give his endorsement more weight if he turns things around politically.
Stating that even if Biden has decided not to run, he shouldn't announce it early to protect other potential Democratic candidates.
Speculating that Biden, a seasoned politician, wouldn't announce he's not running even if he had decided early in his presidency.
Concluding that despite concerns about Biden's age, an announcement about not running in 2024 is unlikely.

Actions:

for political analysts,
Keep abreast of political developments and analyze candidates (implied)
Stay engaged in political discourse and debates (implied)
</details>
<details>
<summary>
2022-11-24: Let's talk about ignoring Trump tantrums.... (<a href="https://youtube.com/watch?v=XyqJjjcTY58">watch</a> || <a href="/videos/2022/11/24/Lets_talk_about_ignoring_Trump_tantrums">transcript &amp; editable summary</a>)

Trump's behavior cannot be ignored like a child's tantrum; it must be countered due to its wider impact on others and political ramifications.

</summary>

"Trump's rhetoric must be countered using emotional appeals or factual arguments because it affects a wider audience and cannot be left unaddressed."
"The decision to stop talking about Trump lies with the Republican Party and his followers, not with individuals tired of discussing him."
"Trump's demands extend beyond mere tantrums, posing a threat to the rights of millions, making it imperative to address and counter his actions."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's behavior as temper tantrums is common, with the suggestion to ignore it like a child throwing a fit at home.
Trump's rhetoric and tantrums impact others and cannot be disregarded like a child having a tantrum in isolation.
A more fitting analogy is Trump as a disruptive student in a classroom, where his behavior influences others.
Ignoring Trump's behavior leads to his followers chanting along with him, escalating to drastic actions like storming the principal's office.
Trump's rhetoric must be countered using emotional appeals or factual arguments because it affects a wider audience and cannot be left unaddressed.
The decision to stop talking about Trump lies with the Republican Party and his followers, not with individuals tired of discussing him.
Trump's influence diminishes when his supporters recognize the harm in following him, leading to his behavior being inconsequential and easier to ignore.
Despite Biden's victory, significant portions of the Republican Party still support Trump, necessitating continued engagement to counter his rhetoric.
The analogy underscores the influence of all voters, including easily influenced individuals, in the political landscape.
Trump's demands extend beyond mere tantrums, posing a threat to the rights of millions, making it imperative to address and counter his actions.

Actions:

for political activists and engaged citizens.,
Convince Republican Party members to re-evaluate their support for Trump (implied).
</details>
<details>
<summary>
2022-11-24: Let's talk about a Thanksgiving Q&A.... (<a href="https://youtube.com/watch?v=4srGMIICtN8">watch</a> || <a href="/videos/2022/11/24/Lets_talk_about_a_Thanksgiving_Q_A">transcript &amp; editable summary</a>)

Beau shares personal anecdotes, answers viewer questions, and hints at upcoming content changes in a Thanksgiving special Q&A session.

</summary>

"It's like one of those based on a true story things, but in the movie that's based on a true story, there's a ghost and a demon and a god and a ghost."
"It's made up. It's a myth. The reality of what occurred is so far removed from the image that we have."
"Yeah, if you look at that list of the stuff we're not supposed to discuss, that's what's getting people killed."
"The school that I went to, heavily weighted tests. So yeah, I was very much the troublemaker."
"So I hope y'all enjoyed this and I hope y'all have a good couple of days. Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau hosts a Thanksgiving special, answering questions from Twitter in a long format video.
The questions cover a wide range of topics, from historical influences on Thanksgiving to personal anecdotes.
Beau explains the humorous origins of items in his workshop and shares insights on past experiences and future plans.
Beau touches on topics like playing tabletop role-playing games, relationships, favorite food recipes, and more.
He candidly shares personal stories about animals, family dynamics, and even his own experiences with troublemaking in school.
Beau provides a glimpse into his life, including interactions with his pets and reflections on past decisions.
The transcript captures Beau's casual, off-the-cuff style and his willingness to share personal stories with his audience.
Beau hints at upcoming changes in his content workflow, promising more behind-the-scenes footage and outtakes.
The audience gets to see a mix of humor, personal reflections, and Beau's genuine engagement with viewer questions.
The transcript showcases Beau's down-to-earth personality and his dedication to creating engaging content for his audience.

Actions:

for content creators and casual viewers,
Watch Beau's videos for insights into community gardening and other DIY projects (suggested)
Support creators like Beau who share personal stories and engaging content (exemplified)
</details>
<details>
<summary>
2022-11-24: Let's talk about Pence's presidential strategy.... (<a href="https://youtube.com/watch?v=CJwQJM73iQE">watch</a> || <a href="/videos/2022/11/24/Lets_talk_about_Pence_s_presidential_strategy">transcript &amp; editable summary</a>)

Former Vice President Pence's doomed strategy to navigate the Trump midterm effect by attempting to appeal to both Trump supporters and moderate voters risks tarnishing his legacy.

</summary>

"The Republican Party is going to have to face facts. They have to cut ties with Trump."
"If this is what he's going to deploy, he will lose and he will destroy any legacy he had left."

### AI summary (High error rate! Edit errors on video page)

Former Vice President Pence is attempting to elevate himself to the presidency in 2024 by adopting a specific strategy to navigate the Trump midterm effect.
Pence criticized the execution of a search warrant against the former president and tried to portray himself as friendly towards Trump while pointing out policy differences.
The strategy Pence is using to appeal to both Trump supporters and more moderate voters is doomed to fail due to his lukewarm support for Trump.
If Pence supports Trump in the primary, he won't gain anything because Trump supporters will vote for Trump anyway.
Pence's attempt to navigate the primary without alienating Trump supporters and the general without completely endorsing Trump is seen as a failing strategy.
The Republican Party needs to cut ties with Trump and actively oppose him to regain control of their party.
Pence's actions on January 6th will be overlooked and forgotten if he continues with his current strategy.
By trying to maintain a delicate balance between Trump supporters and moderates, Pence risks losing and tarnishing any remaining legacy he has.

Actions:

for political analysts, republican voters,
Advocate for the Republican Party to cut ties with Trump (implied)
Actively oppose Trump within the Republican Party (implied)
</details>
<details>
<summary>
2022-11-24: Let's talk about COP 27 and the fund.... (<a href="https://youtube.com/watch?v=9S9bzn4fFK0">watch</a> || <a href="/videos/2022/11/24/Lets_talk_about_COP_27_and_the_fund">transcript &amp; editable summary</a>)

Beau at COP 27: Ambiguity surrounds the creation of a fund for climate change impacts, with no clear plan for oversight or emission cuts.

</summary>

"Lack of clarity on how the fund will be managed raises concerns about corruption and oversight."
"There is no tougher agreement on reducing emissions or cutting fossil fuel use."
"The devil's in the details and we don't have those yet."
"We have one really good thing that came out of it, one really bad thing, and a lot of ambiguity."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

COP 27 discussed the creation of a loss and damage fund to mitigate climate change impacts on less developed countries.
Wealthy industrialized nations will contribute to the fund.
The fund aims to provide immediate assistance when climate disasters strike.
Lack of clarity on how the fund will be managed raises concerns about corruption and oversight.
Despite the agreement on the fund, details on its functioning are not publicly available.
The agreement at COP 27 includes sticking to limiting global warming to 1.5 degrees.
However, there is no tougher agreement on reducing emissions or cutting fossil fuel use.
Wealthier countries are hesitant about the necessary emissions cuts to meet the 1.5-degree goal.
While the fund may incentivize emission cuts, specifics on implementation are lacking.
The outcomes of COP 27 present a mix of positive and negative aspects, with significant ambiguity remaining.

Actions:

for climate activists, policymakers,
Monitor the developments and details of the loss and damage fund (suggested).
Advocate for transparency and oversight mechanisms in the management of climate change funds (implied).
Push for stronger agreements on reducing emissions and cutting fossil fuel use (implied).
</details>
<details>
<summary>
2022-11-23: The Roads to ruining Thanksgiving dinner.... (<a href="https://youtube.com/watch?v=EoyKP_2gpYM">watch</a> || <a href="/videos/2022/11/23/The_Roads_to_ruining_Thanksgiving_dinner">transcript &amp; editable summary</a>)

Beau offers strategic insights on navigating contentious topics during family gatherings, challenging misconceptions and promoting informed dialogues, acknowledging the deep political divide in the country.

</summary>

"This year definitely, you know, nationalize the stuffing, use a little baby guillotine to cut the cranberry sauce."
"The country is incredibly divided. Most of it is because a certain subset of people who are very interested in controlling low information citizens have put out a lot of information to manipulate them emotionally."
"Just pretend you don't get it. Pretend you do not get the joke."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the importance of discussing uncomfortable topics like politics, religion, and money, despite traditional advice against it.
He suggests tactics for navigating potential conflicts over Thanksgiving dinner, such as addressing right-wing viewpoints on border security and Hunter Biden.
Beau explains the nuances of certain issues, like student debt relief and energy independence, to help facilitate informed and respectful dialogues.
He delves into debunking misconceptions around Trump, the Democratic Party, socialism, and communism, providing strategies to counter misleading narratives.
Beau offers advice on handling sensitive situations, like dealing with family members who tell racist jokes, by encouraging engaging but non-confrontational responses.
He touches on contentious topics like trans rights, government spending on divisive issues, and gas prices, providing insights and counterarguments to facilitate constructive debates.
Beau acknowledges the challenges of engaging with family members with differing views, recognizing the divisive nature of current political discourse.
He underscores the importance of understanding the manipulation of information and emotional tactics in shaping opinions, while also acknowledging the limitations in changing deeply entrenched beliefs within some individuals.

Actions:

for family members navigating political differences,
Nationalize stuffing and use creative ways to bring up uncomfortable topics (suggested)
Address right-wing viewpoints with informed counterarguments (exemplified)
Educate on nuanced issues like student debt relief and energy independence (implied)
Counter misinformation about Trump and Democratic Party narratives (exemplified)
Encourage engaging responses to racist jokes to prompt reflection (exemplified)
</details>
<details>
<summary>
2022-11-23: Let's talk about the Klamath River running free.... (<a href="https://youtube.com/watch?v=ER_i1lEY1hk">watch</a> || <a href="/videos/2022/11/23/Lets_talk_about_the_Klamath_River_running_free">transcript &amp; editable summary</a>)

The Klamath River's dam removal project marks a significant win for the Yurok people, preserving their culture and way of life by ensuring the salmon's return.

</summary>

"The salmon or the buffalo. It's not just a food source. It's an integral part of the culture."
"If the salmon go, the Yurok go."
"This is a win."

### AI summary (High error rate! Edit errors on video page)

California and Oregon's Klamath River is set for a groundbreaking $500 million project to remove four dams, the largest dam removal and river restoration project ever.
The Klamath River has been constrained for over 100 years, but the dams' removal will commence this summer.
National news often simplifies the issue as farmers needing water versus endangered salmon, but the real story centers on the Yurok, a Native American group for whom salmon is life.
Salmon isn't just a food source for the Yurok; it's a vital part of their culture and existence.
Removing the dams will likely result in a healthier salmon population, preserving not just the fish but also the Yurok way of life.
The Yurok people have been fighting for dam removal for at least a decade, with the understanding that if the salmon disappear, so will they.
The significance of the salmon to the Yurok can be compared to the cultural importance of the buffalo to Native Americans, as seen in "Dances with Wolves."
This project represents a rare win, with the final major hurdle cleared, marking the end of a long fight for river restoration.
By restoring the river, not only the fish but also a way of life is being preserved.
The approval for dam removal marks the end of one story and the beginning of a new chapter in the Klamath River's restoration process.

Actions:

for environmental activists, indigenous rights advocates,
Support Indigenous-led environmental restoration projects (exemplified)
Advocate for similar dam removal projects in other regions (exemplified)
</details>
<details>
<summary>
2022-11-23: Let's talk about a theory about the Special Counsel.... (<a href="https://youtube.com/watch?v=fgPcb6uqwc0">watch</a> || <a href="/videos/2022/11/23/Lets_talk_about_a_theory_about_the_Special_Counsel">transcript &amp; editable summary</a>)

Beau speculates on the special counsel's broader responsibilities beyond Trump and suggests Jack Smith might play a significant role in making charging decisions related to various investigations.

</summary>

"If it's just the decision to indict Trump, it doesn't make sense to bring in a special counsel just for that reason."
"That tracks with Garland. Garland has a long history of no leaks, don't explain anything, go about your investigation, then announce once everything is decided."
"Smith may be the decider."

### AI summary (High error rate! Edit errors on video page)

Speculates on the special counsel's responsibilities and Jack Smith's role.
Questions the media's focus on Trump's candidacy announcement as the sole reason for the special counsel.
Mentions the complex background investigations related to January 6th.
Suggests that the special counsel's role may involve a broader scope beyond Trump's involvement.
Considers the possibility of Smith making charging decisions on various individuals and investigations.
Points out the lack of extensive information on other investigations happening in the background.
Notes Garland's history of conducting investigations without leaks.
Raises the idea of Smith being the decisive figure in these matters.

Actions:

for political analysts,
Analyze and stay informed about the evolving situation around the special counsel's investigations (implied).
</details>
<details>
<summary>
2022-11-23: Let's talk about Trump, Evangelicals, and numbers.... (<a href="https://youtube.com/watch?v=Pjw3l6O5dtc">watch</a> || <a href="/videos/2022/11/23/Lets_talk_about_Trump_Evangelicals_and_numbers">transcript &amp; editable summary</a>)

Evangelicals turning on Trump post-midterms impacts declining church memberships due to intertwining of politics and religion.

</summary>

"Donald Trump has to go."
"The problem isn't that you're turning on Trump. The problem is that you ever supported him to begin with."
"When it is this transparent, all of these church leaders, they become politicians."
"You want to talk about failing church memberships? It tends to be a result of failing church leadership."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Evangelicals are turning on Trump, distancing themselves from the former president since the midterms.
Mike Evans and James Robinson, prominent figures, criticize Trump for his actions and behavior.
Everett Piper from The Washington Times bluntly states that Trump has to go, predicting destruction in 2024 if he's the nominee.
The focus is not on Trump's declining poll numbers but on the impact on declining church memberships.
Right-wing talking points often reference decreasing church memberships, with Beau attributing it to the support of Trump.
Beau criticizes the church for preaching hate instead of the gospel and using political power to enforce beliefs.
Inserting religious organizations into politics leads to distrust, hypocrisy, and manipulation, undermining their core mission.
Beau points out the hypocrisy of church leaders who had a change of heart about Trump post-midterms but didn't rule out supporting him later.
The intertwining of church and state leads to church leadership behaving like politicians, causing failing church memberships.
Beau suggests failing church leadership is a significant factor in declining church memberships.

Actions:

for religious community members,
Reassess church leadership roles and behaviors (suggested)
Advocate for separation of church and state (implied)
</details>
<details>
<summary>
2022-11-22: Let's talk about the house, Hunter, and buyer's remorse.... (<a href="https://youtube.com/watch?v=uQSKUui1vnU">watch</a> || <a href="/videos/2022/11/22/Lets_talk_about_the_house_Hunter_and_buyer_s_remorse">transcript &amp; editable summary</a>)

Republicans in the House focus on investigating Hunter Biden, potentially alienating voters and lacking substance in their actions.

</summary>

"Putting Republicans in the House and give them control, you get a clown show."
"Nobody cares."
"If whatever they uncover is less than what Trump did, nobody's gonna care."
"This is going to be incredibly annoying for the Biden administration."
"This is going to be a lot like other punitive hearings that have come from Republicans."

### AI summary (High error rate! Edit errors on video page)

Republicans promised to address issues but focused on launching an investigation into Hunter Biden, leading to buyer's remorse among some voters.
Putting Republicans in control means getting a clown show in the House for the next two years.
The presumed hearings and subpoenas into Hunter Biden may not reveal anything substantial.
Even if shady stuff is found, it's unlikely to significantly impact public opinion since Hunter Biden is not an elected official.
Any wrongdoing uncovered must be more severe than what Trump did to gain public attention.
Republicans seem focused on hearings and investigations rather than addressing voter concerns.
The Biden administration and the Democratic Party will find these actions annoying, but it may benefit Fox News.
There's little likelihood of finding anything illegal connecting to President Biden through these investigations.
Hunter Biden may have skirted the edge of legality in his dealings, but nothing substantial may be uncovered.
Punitive hearings from Republicans often lack substance and may push voters away.

Actions:

for voters,
Contact your elected representatives to express your concerns about focusing on investigations rather than addressing real issues (suggested).
Join local political organizations to stay informed and engaged in the political process (implied).
Organize community events to raise awareness about the importance of addressing pressing problems over political theatrics (implied).
</details>
<details>
<summary>
2022-11-22: Let's talk about Poland changing your opinion on Ukraine.... (<a href="https://youtube.com/watch?v=fyaG64rGWoU">watch</a> || <a href="/videos/2022/11/22/Lets_talk_about_Poland_changing_your_opinion_on_Ukraine">transcript &amp; editable summary</a>)

Beau questions NATO's lack of war with Russia despite an ideal pretext, revealing the true nature of Russia's actions as imperialism.

</summary>

"NATO doesn't want to go to war with Russia, right?"
"Their pretext falls apart when you acknowledge that NATO will never get a better opportunity to go to war with Russia than they have right now."
"Russian command's decision to invade Ukraine was imperialism."

### AI summary (High error rate! Edit errors on video page)

Questions why NATO isn't at war with Russia despite a situation in Poland involving a Russian rocket.
Points out discrepancies in the presentation of facts regarding the incident in Poland.
Mentions the ideal pretext for NATO to go to war with Russia but questions why it didn't happen.
Suggests that NATO's reluctance to go to war with Russia indicates their doctrine of nuclear deterrence.
Analyzes Russia's pretext for invading Ukraine as a lie, considering NATO's stance on going to war.
Describes Russia's actions as imperialism with a capitalist bent, contrasting them with American foreign policy.
Argues that Russia's invasion of Ukraine was driven by imperialism, with other justifications being mere pretexts.
Encourages viewers to reconsider their views on the Russian invasion with the new information presented.

Actions:

for policy analysts, activists,
Analyze and question international relations (implied)
Reassess beliefs about geopolitical conflicts (implied)
</details>
<details>
<summary>
2022-11-22: Let's talk about Artemis.... (<a href="https://youtube.com/watch?v=kz8RMoi07mE">watch</a> || <a href="/videos/2022/11/22/Lets_talk_about_Artemis">transcript &amp; editable summary</a>)

Beau talks about the recent liftoff event of Artemis 1, marking a step towards human space exploration, with optimism for the future journey to Mars.

</summary>

"Artemis 1 took off with the Orion capsule sitting on top of it."
"Yes, the Earth is round."
"It's one that should eventually lead to people being on Mars."
"They have restarted in earnest."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduction to the recent liftoff event after a decade of planning.
Artemis 1, delayed by hurricanes and technical issues, finally took off with the Orion capsule.
The capsule, carrying sensors-equipped mannequins, is on a 25-day journey orbiting the moon.
The mission aims to break distance records from Earth and pave the way for human return to the lunar surface.
Photos transmitted back from the mission, while not visually striking, mark a significant milestone.
The successful launch and flight progress according to plan, with photos showing the roundness of Earth.
Artemis program, mirroring Apollo missions, signifies progress towards establishing a spacefaring civilization.
The Orion capsule is scheduled to return on December 11 by splashing down in the Pacific Ocean.
Beau addresses the debate around the cost and benefits of such space programs.
Despite financial considerations, the goal of becoming a spacefaring civilization is invaluable.
This mission represents a small step towards eventual human presence on Mars.
The initiative signifies a renewed effort in humanity's quest for interplanetary exploration.
Beau concludes with optimism on the progress and potential of space exploration efforts.

Actions:

for space enthusiasts, science enthusiasts,
Follow updates on space missions and advancements (implied)
Support space exploration initiatives through advocacy or donations (implied)
</details>
<details>
<summary>
2022-11-21: Let's talk about who's winning and losing over Twitter.... (<a href="https://youtube.com/watch?v=WZU1ZOr298Q">watch</a> || <a href="/videos/2022/11/21/Lets_talk_about_who_s_winning_and_losing_over_Twitter">transcript &amp; editable summary</a>)

Elon Musk loses control as alternative social networks gain traction, leaving Facebook's metaverse project behind.

</summary>

"One, tried to reinvent the creator economy."
"It doesn't appear that people are ready for an idea like metaverse."
"Facebook is just a little bit ahead of its time."

### AI summary (High error rate! Edit errors on video page)

Examines the impact of upheaval at Twitter on various social media platforms.
Elon Musk is perceived to be losing control amidst the Twitter chaos.
Other social networks like Co-host, tribal, counter-social, and mastodon are gaining new subscribers.
People are not gravitating towards Facebook's metaverse project as their new social media home.
Many believe in the concept of a metaverse in the future, but current trends show otherwise.
The alternative networks being discussed share similarities but offer different blends of features.
People seem hesitant about embracing the idea of a metaverse due to issues in its development process.
Facebook's new project is not receiving as much attention as other social media platforms.
The future of social media might see significant changes due to Musk's influence on Twitter.
Facebook may be ahead of its time with its new project, potentially facing challenges in gaining popularity.

Actions:

for social media enthusiasts,
Join alternative social networks like Co-host, tribal, counter-social, or mastodon (implied)
Stay updated on the developments in social media platforms (implied)
</details>
<details>
<summary>
2022-11-21: Let's talk about explaining Twitter to the working class.... (<a href="https://youtube.com/watch?v=BjI7tQ5Wm9o">watch</a> || <a href="/videos/2022/11/21/Lets_talk_about_explaining_Twitter_to_the_working_class">transcript &amp; editable summary</a>)

Beau explains Twitter's situation using a construction scenario, challenging assumptions about the working class's understanding.

</summary>

"Just because we talk slow doesn't mean we think slow."
"Your boss is asking you to show up for work and you say no? And you're already making 125 grand a year."
"If you are part of the think peace crowd in these magazines, maybe talk to the working class."

### AI summary (High error rate! Edit errors on video page)

Critiques the notion that it's impossible to explain Twitter's situation to the working class.
Mentions reading think pieces claiming the working class can't comprehend certain concepts.
Questions the idea of dividing the working class into "laptop class" and working class.
Illustrates a scenario involving a construction worker to explain complex changes at work.
Expresses skepticism towards those who believe the working class cannot grasp certain economic concepts.
Emphasizes the importance of directly engaging with the working class rather than making assumptions.

Actions:

for working-class individuals,
Talk directly to the working class to understand their perspectives (suggested)
Bridge the gap between different socioeconomic groups through open communication (implied)
</details>
<details>
<summary>
2022-11-21: Let's talk about Colorado, stats, and speculation.... (<a href="https://youtube.com/watch?v=SdSmVsAeTvg">watch</a> || <a href="/videos/2022/11/21/Lets_talk_about_Colorado_stats_and_speculation">transcript &amp; editable summary</a>)

Beau speculates on the domestic violence connection in the recent Colorado incident, urging exploration of the link to find effective solutions in addressing mass incidents.

</summary>

"When it comes to mass incidents of this sort, these statistics are wild."
"This is where you need to look."
"There's a link. It needs to be explored."
"The solution is there."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Speculated on a potential domestic violence connection in recent Colorado incident causing pushback.
Many mass incidents have a domestic violence connection, with statistics showing as high as 68.2%.
Noted that domestic violence is underreported and a significant factor in mass incidents.
Advocated for closing all loopholes related to domestic violence in gun control legislation.
Encouraged individuals with past domestic violence incidents to support exploring the link between domestic violence and mass incidents.
Believed that restorative justice is possible but emphasized the need for safeguards.
Stressed the importance of studying the domestic violence connection to find effective solutions.
Asserted that addressing domestic violence links may be more impactful than focusing on specific weapon types.
Mentioned a fundraiser for domestic violence shelters coming up soon.

Actions:

for advocates, activists, community members.,
Support exploring the link between domestic violence and mass incidents (suggested).
Advocate for closing all loopholes related to domestic violence in gun control legislation (implied).
Get ready to participate in the upcoming fundraiser for domestic violence shelters (implied).
</details>
<details>
<summary>
2022-11-20: Let's talk about whether the Trump Special Counsel is partisan.... (<a href="https://youtube.com/watch?v=NSntjYCFBHo">watch</a> || <a href="/videos/2022/11/20/Lets_talk_about_whether_the_Trump_Special_Counsel_is_partisan">transcript &amp; editable summary</a>)

Beau explains the dynamics of the special counsel investigation on Trump and the 2024 election, debunking partisan claims and outlining Democrats' strategies.

</summary>

"It's not a witch hunt. The Democratic Party is not afraid of Trump. He's a losing loser who lost."
"The Republican Party has to accept the fact. Trump is their Hillary Clinton."
"Read the news, read the sources, go back and look through it. Don't listen to politicians."
"Trump is losing in polls of the Republican Party to see who would get the nomination."
"The special counsel has nothing to do with the 2024 election and Trump's candidacy for president."

### AI summary (High error rate! Edit errors on video page)

Explains the Republican Party's reaction to news of a special counsel regarding Trump and the 2024 election.
Points out that Republicans believe the special counsel is partisan because they fear Trump's potential in 2024.
Mentions how Democrats are not afraid of Trump in the 2024 election.
Notes that Trump is losing in polls for the Republican nomination and drives voter turnout for Democrats.
Describes Democrats elevating Trump-endorsed candidates in primaries to beat them in general elections.
Emphasizes that Trump is not a significant threat for the 2024 election.
Asserts that the special counsel investigation is unrelated to Trump's candidacy for president in 2024.
Advises to look at news sources rather than listening to politicians to understand the situation.
Suggests that Democrats might prefer Trump as a Republican candidate due to his divisive nature and negative voter turnout impact.
Compares Trump to Hillary Clinton, indicating internal party support but lack of electability at a broader level.

Actions:

for politically engaged citizens,
Read news sources and dig into information to understand situations (suggested).
Disregard solely political narratives and seek a deeper understanding through research (suggested).
</details>
<details>
<summary>
2022-11-20: Let's talk about Trump being back on Twitter.... (<a href="https://youtube.com/watch?v=ywZMLDak42U">watch</a> || <a href="/videos/2022/11/20/Lets_talk_about_Trump_being_back_on_Twitter">transcript &amp; editable summary</a>)

Elon Musk reinstates Trump on Twitter, but it could spell disaster for both Trump's brand and the Republican Party.

</summary>

"Be careful what you wish for."
"Trump is a losing brand."
"It's good for Trump, it's good for that cult of personality, but for the Republican Party as a whole, this is just all bad."
"His rhetoric might actually cause people who are kind of like him to embrace wilder rhetoric which will also make them unelectable."
"I hope we can sell them another hill at the same cost."

### AI summary (High error rate! Edit errors on video page)

Elon Musk reinstated former President Donald J. Trump's Twitter account, sparking mixed reactions.
The phrase "he's back" is trending on Twitter, with some users excited about Trump's return.
If Trump rejoins Twitter, he will destroy his social media network, which relies solely on his presence.
His return may lead to a series of bad business decisions, consistent with his track record.
Trump's comeback could be disastrous for the Republican Party, as his brand is losing and lacks broad electoral support.
Many Republicans mistakenly believe that their most vocal social media supporters represent the entire party.
Trump's tweets may influence other Republicans to adopt unpopular positions out of fear of backlash.
Elon Musk's actions may inadvertently contribute to a Democratic victory in 2024.
Trump's Twitter presence could lead to infighting within the Republican Party, alienating those who don't support him enough.
Trump's irresponsible Twitter use may further damage the Republican Party and polarize voters.
Despite some initial excitement, Trump's return to Twitter could have negative long-term consequences for the Republican Party.
Trump's engagement on Twitter is primarily driven by ego and can push supporters towards extreme positions.
While some may be drawn back to the "MAGA" movement, it's unlikely to secure an election victory.
Moderate Republicans and independent voters have already rejected Trump's rhetoric and extreme positions.
The Republican Party's perceived victory in Trump's return may actually lead to electoral losses in the future.

Actions:

for political analysts, social media users,
Monitor and critically analyze political figures' social media presence (implied)
Encourage responsible social media use by public figures (implied)
Advocate for constructive political discourse on social media platforms (implied)
</details>
<details>
<summary>
2022-11-20: Let's talk about Colorado and what we can expect.... (<a href="https://youtube.com/watch?v=gLvpP7UM2IU">watch</a> || <a href="/videos/2022/11/20/Lets_talk_about_Colorado_and_what_we_can_expect">transcript &amp; editable summary</a>)

Recent events prompt speculation on Colorado incident, linking hateful rhetoric to real-world consequences and potential legal accountability.

</summary>

"It costs nothing to be part of the solution."
"Putting out positive messaging that doesn't lead to violence doesn't increase your overhead."
"They're doing it because they feel it's good for their political base or it'll make them money, drive engagement, whatever."
"Many of them have responded to the fact-checks. And they continue to push out false information."
"I think it's reasonable to expect that a lot of the personalities that have pushed this kind of hateful rhetoric are going to end up in court."

### AI summary (High error rate! Edit errors on video page)

Recent events in Colorado prompt speculation and assumptions in the media about why they occurred.
The club that was attacked has been the target of hateful and false rhetoric from political commentators and politicians.
The arrested suspect's background may reveal a connection to domestic violence (DV) and provide insight into the motive behind the attack.
The suspect's social media consumption of hateful rhetoric may be used as a mitigating factor in sentencing.
Individuals spreading false information that influenced real-world consequences may face legal consequences, similar to the Jones case.
Commentators and politicians spreading hateful rhetoric often do so knowingly for political gain or profit, despite being aware of its falsehood.
The business model of spreading misinformation is not sustainable in the long term, as shown in the Jones case.
Putting out positive messaging that does not incite violence comes at no additional cost and can be part of the solution.
Previous targeted rhetoric towards specific demographics has contributed to negative outcomes.
Beau anticipates that the current situation may differ from past cases due to the suspect's arrest and the Jones case's precedent.

Actions:

for media consumers,
Fact-check and challenge hateful rhetoric in your community (implied).
Support positive messaging that fosters unity and understanding (implied).
</details>
<details>
<summary>
2022-11-19: Let's talk about whether a Democratic tactic worked.... (<a href="https://youtube.com/watch?v=G1nBHzGvTMY">watch</a> || <a href="/videos/2022/11/19/Lets_talk_about_whether_a_Democratic_tactic_worked">transcript &amp; editable summary</a>)

Democratic Party strategically elevates certain Republican candidates in key places for electoral success but should be cautious about adopting it as a long-term strategy to prevent unintended consequences.

</summary>

"It worked incredibly well. It handed them some incredibly significant victories."
"The all-encompassing strategy is to deliver for the people that put you into office."
"We have to draw that line between tactic and strategy."

### AI summary (High error rate! Edit errors on video page)

Democratic Party's tactic of elevating certain Republican candidates during primaries to ensure an easier win was employed in six high-profile places.
The tactic was used in Arizona, Illinois, Maryland, Michigan, New Hampshire, and Pennsylvania.
This strategy worked well, handing the Democrats some significant victories and contributing to stopping a red wave.
However, there are risks associated with adopting this tactic as a long-term strategy.
Elevating extreme candidates may condition Republicans to support their radical policies even after the election solely based on party loyalty.
While it made sense in specific instances, adopting it widely could have negative consequences.
The only scenario where this tactic might make sense as a strategy is if Trump wins the nomination and decides to run again in 2024.
It's vital to distinguish between a tactic, used situationally, and a strategy, which should focus on delivering for the people consistently.

Actions:

for political strategists,
Analyze electoral tactics used in key places to understand their impact (implied).
</details>
<details>
<summary>
2022-11-19: Let's talk about whether Republicans have it figured out yet.... (<a href="https://youtube.com/watch?v=UGlqT_LZEsw">watch</a> || <a href="/videos/2022/11/19/Lets_talk_about_whether_Republicans_have_it_figured_out_yet">transcript &amp; editable summary</a>)

Republicans doubling down on social media battles and conspiracy theories rather than policies will lead to continued failures at the ballot box.

</summary>

"They haven't learned their lesson."
"A segment of the Republican Party equates retweets to votes."
"They are all edge and no point."
"It's their policies. It's their rhetoric."
"A lot like when they put that recording out from Biden to his son."

### AI summary (High error rate! Edit errors on video page)

Republicans are still struggling to understand what is causing their underwhelming performance at the ballot box.
There is a faction within the Republican Party that intends to double down on their current strategies, believing it will lead to electoral success.
They believe that engaging in social media battles and honing the libs will translate to victory, similar to Trump's initial win.
Some Republicans see subpoenas as the path to victory, focusing on issues like investigating Biden's son and spreading conspiracy theories.
Beau compares this strategy to making Trump Speaker of the House - ultimately a misguided approach.
The focus on social media games and conspiracy theories rather than policies will not resonate with voters, leading to continued failures at the ballot box.
A significant portion of the Republican Party equates social media engagement to electoral success, which is a flawed belief.
Beau predicts that Republicans will continue to make the same mistakes, scapegoating and alienating people.
The attempt to smear Biden by releasing recordings only humanized him, showing a lack of understanding from the Republican Party.
Beau concludes by expressing doubt that the Republican Party will learn from their errors.

Actions:

for politically engaged voters,
Organize community events to raise awareness about the importance of policies over social media battles (implied)
Support candidates who prioritize substantial policies and rhetoric over divisive strategies (implied)
</details>
<details>
<summary>
2022-11-19: Let's talk about Jack Smith and who he is.... (<a href="https://youtube.com/watch?v=ZkgL5cVBeFI">watch</a> || <a href="/videos/2022/11/19/Lets_talk_about_Jack_Smith_and_who_he_is">transcript &amp; editable summary</a>)

Beau introduces Jack Smith, hinting at his potential role in upcoming news, showcasing his extensive legal background, particularly his apolitical stance, making him a promising candidate for those seeking prosecutions.

</summary>

"This isn't the background of somebody who is looking to score chips in the big game and get political favors later."
"If you were actually looking to prosecute, this is kind of the background you would look for."

### AI summary (High error rate! Edit errors on video page)

Introducing Jack Smith, a significant figure soon to be in the news with an impressive background.
Jack Smith grew up in New York, attended Harvard Law, and worked with the New York District Attorney's Office.
He later moved on to the US Attorney's Office in Brooklyn and then to the International Criminal Court.
Smith also served as chief of Public Integrity for five years and became Assistant United States Attorney for the Middle District of Tennessee.
In 2018, he took an appointment at The Hague regarding Kosovo and now holds a new appointment as special counsel.
While at Public Integrity, Smith was known for being apolitical, which is a critical aspect of his character.
His reputation suggests he is not someone looking to gain political favors or play political games.
Smith's background and experience make him a potential candidate for those hoping for a prosecution, particularly involving Trump.
Despite uncertainties, Smith's background indicates seriousness rather than a mere show of authority.
The possibility of Smith being involved in prosecuting Trump, especially in cases regarding documents, seems promising.

Actions:

for legal enthusiasts,
Stay informed about the developments involving Jack Smith and his potential role in legal proceedings (implied).
</details>
<details>
<summary>
2022-11-18: Let's talk about how to help the animals in Kenya.... (<a href="https://youtube.com/watch?v=Bh6a4IW6h68">watch</a> || <a href="/videos/2022/11/18/Lets_talk_about_how_to_help_the_animals_in_Kenya">transcript &amp; editable summary</a>)

Beau addresses how to support animals impacted by drought in Africa, recommending David Sheldrick's Wildlife Trust for donations or symbolic adoptions.

</summary>

"You want to assist, you want to help these animals that very well may not make it because of the drought, look to David Sheldrick's Wildlife Trust."
"When funding is the problem, you absolutely can [help by providing money]. And in this case, the funding is the problem."
"If this is something that is motivating you, you want to get involved, you want to help, you want to try to make a difference when it comes to these animals."

### AI summary (High error rate! Edit errors on video page)

Addressing how to help with the drought situation in Africa, specifically focusing on animals like elephants and hippos.
Recommending support for David Sheldrick's Wildlife Trust, a massive organization known for its work with elephants but extends help to various animals.
Mentioning the Trust's efforts in building solar-powered water distribution plants and trucking in water to support wildlife during the drought.
Noting the importance of funding in addressing the crisis, especially due to the lack of political will.
Encouraging people to support the Trust through donations or symbolic adoptions of animals like elephants, rhinos, or giraffes.
Pointing out that the Trust has a wish list of needed items and is currently caring for an influx of orphaned elephants.
Emphasizing the impact of grass scarcity on hippos due to the water shortage in the region.

Actions:

for animal conservation supporters,
Support David Sheldrick's Wildlife Trust through donations or symbolic animal adoptions (suggested).
</details>
<details>
<summary>
2022-11-18: Let's talk about a Special Counsel for Trump and what it means.... (<a href="https://youtube.com/watch?v=E71WM1RbflI">watch</a> || <a href="/videos/2022/11/18/Lets_talk_about_a_Special_Counsel_for_Trump_and_what_it_means">transcript &amp; editable summary</a>)

Attorney General Merrick Garland's appointment of a special counsel to oversee investigations into former President Donald J. Trump has sparked uncertainty and speculation about the potential outcomes and implications.

</summary>

"We don't know what this means because it can mean kind of one of two things, and they are wildly different outcomes."
"The pieces that are available can be used to frame two wildly different stories and two wildly different futures."
"Humanity has made that mistake with people like him before."

### AI summary (High error rate! Edit errors on video page)

Attorney General Merrick Garland is expected to appoint a special counsel to oversee criminal investigations into former President Donald J. Trump.
There are two possible outcomes of appointing a special counsel, with vastly different implications, one related to Trump's documents case and the other to January 6th.
Garland has been focused on keeping the Department of Justice (DOJ) apolitical, but the perception of special counsels as political may complicate matters.
The Republican Party could portray the special counsel as a political fishing expedition, affecting how it is perceived by the country.
The transparency and distance between Garland, the special counsel, and the administration could either protect the presidency or lead to a thorough investigation of Trump.
It is uncertain whether the special counsel's appointment is to protect the institution of the presidency or to hold Trump accountable.
Despite expectations of a lengthy process, a special counsel does not necessarily have to slow down the investigations.
Keeping Trump in the headlines is inevitable as long as he remains a prominent figure in national politics.
The appointment of a special counsel has occurred, marking a critical point in the ongoing investigations into Trump.
The final outcome of these developments remains uncertain, with various possible scenarios depending on how events unfold.

Actions:

for political analysts, concerned citizens,
Monitor developments in the investigations into former President Donald J. Trump (implied)
Stay informed about the implications of the special counsel's appointment (implied)
</details>
<details>
<summary>
2022-11-18: Let's talk about 50 million spent by the GOP.... (<a href="https://youtube.com/watch?v=IpTTr11mcaA">watch</a> || <a href="/videos/2022/11/18/Lets_talk_about_50_million_spent_by_the_GOP">transcript &amp; editable summary</a>)

The Republican Party spent $50 million on anti-LGBTQ advertising, targeting trans kids, showcasing a playground bully mentality to manipulate public opinion and divide the country.

</summary>

"They did it to manipulate you."
"The propaganda is bad and you should feel bad."
"If you're looking down at those people, you're not paying attention to them as they beg you for money."

### AI summary (High error rate! Edit errors on video page)

Addresses political advertising and the exorbitant spending on specific issues.
Focuses on the Republican Party spending a minimum of $50 million on anti-LGBTQ advertising.
Points out that $50 million was spent solely on advertising and doesn't include other platforms like social media.
Reveals that the target of the advertising was trans kids, with a small number of kids starting hormone therapy.
Emphasizes the discrepancy between the amount spent and the actual number of affected kids.
Breaks down the cost to bully each kid, which amounts to around $12,000.
Criticizes this behavior as playground bully mentality and propaganda.
Condemns those who support such divisive and harmful tactics.
Calls out the manipulation and shame associated with supporting these actions.
Concludes by urging viewers to be aware of manipulation and not fall for divisive propaganda.

Actions:

for voters, activists, allies,
Stand against divisive propaganda (exemplified)
Support LGBTQ youth (exemplified)
</details>
<details>
<summary>
2022-11-17: Let's talk about where Texas, Georgia, and Florida dems messed up.... (<a href="https://youtube.com/watch?v=ZN0nOXwqpGk">watch</a> || <a href="/videos/2022/11/17/Lets_talk_about_where_Texas_Georgia_and_Florida_dems_messed_up">transcript &amp; editable summary</a>)

Beau explains the Democratic Party's underperformance in Southern governor races, citing simple explanations like being pro-gun in Texas and failing to motivate young voters in Florida.

</summary>

"If you are going to play in Texas, you have to have a fiddle in the band."
"Principle does not win a race in Texas."
"Crist couldn't drive younger voter turnout."
"Kemp isn't MAGA."
"Is it really this simple? Yeah, it really is."

### AI summary (High error rate! Edit errors on video page)

Explains locations where the Democratic Party underperformed in three governor races in the South.
Points out the simple explanations for the underperformance in Texas, Florida, and Georgia.
Mentions the importance of having a fiddle in the band to run in Texas and the significance of being pro-gun in the state.
Talks about the impact of an old former Republican candidate in Florida and how it failed to motivate young voters.
Describes how Kemp's stance against Trump's pressure in Georgia resonated with voters and earned him respect.
Suggests that the Democratic Party might not have been able to do anything differently to win in Georgia.
Concludes that the reasons for underperformance are straightforward and not easily changeable.

Actions:

for political analysts,
Analyze voter demographics and preferences to tailor candidates accordingly (implied).
Focus on motivating and engaging young voters through candidate selection and messaging (implied).
Understand the local political landscape and preferences to strategize effectively (implied).
</details>
<details>
<summary>
2022-11-17: Let's talk about getting more conservative as you get older.... (<a href="https://youtube.com/watch?v=f4ftaEkkjiE">watch</a> || <a href="/videos/2022/11/17/Lets_talk_about_getting_more_conservative_as_you_get_older">transcript &amp; editable summary</a>)

Beau dismantles the myth that people naturally become more conservative with age, revealing how societal progress shapes political ideologies.

</summary>

"People don't become more conservative. Not as a general trend. That's not a thing."
"The world, as a trend, becomes more progressive, more accepting, more open."
"Conservatives are back there at the backside of society, and they're getting just dragged into the future."
"The conservative party slowly became more progressive. And then they eventually line up."
"The longer they hold on to this, the worse it's going to get."

### AI summary (High error rate! Edit errors on video page)

Addresses the misconception that people become more conservative as they age.
Explains that older demographics used to become more conservative due to amassing wealth, but this is no longer the case.
Notes that getting older doesn't mean gaining more wealth or capital in the current socio-political climate.
Points out that the Republican Party, despite the image of being fiscally conservative, has shifted away from this stance.
Describes how the Republican Party has embraced culture war issues, moving away from fiscal conservatism.
Talks about the progressive trend in society over time and how conservatives are dragged into the future.
Mentions how individuals who do not evolve with society do not become more conservative, but rather remain static in their beliefs.
Explains that the conservative party has slowly become more progressive, making older individuals identify more with them.
Comments on the Republican Party's resistance to progress and how it alienates voters, particularly the younger demographic.
Concludes that the Republican Party's outdated positions and failure to advance are causing them to lose elections and alienate their base.

Actions:

for voters, political observers,
Reassess political affiliations based on current stances and not historical perceptions (implied).
Encourage ongoing education and evolution of personal beliefs to stay in line with societal progress (implied).
</details>
<details>
<summary>
2022-11-17: Let's talk about Trump, DeSantis, and the future.... (<a href="https://youtube.com/watch?v=D3DgPj5Ty1k">watch</a> || <a href="/videos/2022/11/17/Lets_talk_about_Trump_DeSantis_and_the_future">transcript &amp; editable summary</a>)

The Republican Party faces change as DeSantis navigates the post-midterm landscape with potential national aspirations, balancing the legacy of Trump and business relationships.

</summary>

"The Republican Party requires change, acknowledged by those transitioning from Trump to DeSantis."
"DeSantis faces the challenge of not being as charismatic as Trump but is recognized as smarter."
"Republicans in Florida misinterpreted their midterms success, assuming a mandate when it was actually due to low enthusiasm."
"His relationship with businesses may need adjustment, as his current posture towards large companies could be detrimental on a national stage."
"DeSantis might need to break away from the perception of being Trump Jr. and distancing himself from Trump to succeed in the national arena."

### AI summary (High error rate! Edit errors on video page)

The state of the Republican Party is under scrutiny post-midterms, with Trump losing and DeSantis winning.
A poll reveals that 41% of Republicans prefer DeSantis as the 2024 nominee, 39% favor Trump, and 8% support neither.
The Republican Party requires change, acknowledged by those transitioning from Trump to DeSantis.
DeSantis faces the challenge of not being as charismatic as Trump but is recognized as smarter.
DeSantis needs to make decisions in Tallahassee with a national perspective to secure the nomination and potential win in 2024.
Republicans in Florida misinterpreted their midterms success, assuming a mandate when it was actually due to low enthusiasm.
DeSantis may face pressure to implement more restrictions to appease Florida Republicans, although this could harm his chances in a national election.
His relationship with businesses may need adjustment, as his current posture towards large companies could be detrimental on a national stage.
DeSantis navigating the transition from state to national politics involves considerations like handling the Trump factor and gaining business support.
DeSantis might need to break away from the perception of being Trump Jr. and distancing himself from Trump to succeed in the national arena.

Actions:

for political analysts, republican voters,
Monitor DeSantis' decisions and positions and how they may impact his potential presidential run (implied).
Stay informed about shifts in the Republican Party and potential candidates for the 2024 election (implied).
</details>
<details>
<summary>
2022-11-16: Let's talk about people being unhappy with the direction of the country.... (<a href="https://youtube.com/watch?v=UGRtj_4p5Bc">watch</a> || <a href="/videos/2022/11/16/Lets_talk_about_people_being_unhappy_with_the_direction_of_the_country">transcript &amp; editable summary</a>)

Beau dives into why people unhappy with the country's direction still voted for the same party and calls for a return to normal conservatism within the Republican Party to attract voters.

</summary>

"But one stat keeps messing me up."
"The only way forward for the Republican Party is to get rid of that faction, is to go back to being normal Republicans, to being conservatives, not authoritarian nationalists."
"They see the attacks on democratic institutions, and they reject that."
"Had that been, do you think that the Democratic Party or the Republican Party is more likely to offer a sustainable future, people would have gone with the Democratic Party based on the results from the midterms."
"So they answer that question in that way, but they're not blaming the party in power because it's not the party in power."

### AI summary (High error rate! Edit errors on video page)

Explains why people's dissatisfaction with the direction of the country did not impact the election results as expected.
Mentions being a Republican but happy when Trump lost, and questions why people unhappy with the country's direction still voted for the same party.
Talks about the preference for candidates like Caramand, who represent a more traditional Republican ideology.
Criticizes the Republican Party for moving towards authoritarian nationalist tendencies, driving voters away.
Emphasizes the need for the Republican Party to return to being normal conservatives rather than authoritarian nationalists to attract voters.
Suggests that the Republican Party's failure to learn this lesson may lead to continued Democratic Party victories.
States that many independents are rejecting the Republican Party due to attacks on democratic institutions.
Points out that the phrasing of questions can influence responses and lead to conclusions favoring the Democratic Party based on recent election results.
Acknowledges the widespread dissatisfaction among Americans regarding attacks on democratic institutions.
Concludes by encouraging viewers to ponder these thoughts and wishes them a good day.

Actions:

for independents, republicans, democrats,
Support candidates who embody traditional conservative values (suggested)
Advocate for the elimination of authoritarian nationalist tendencies within political parties (implied)
Engage in constructive political discourse and reject attacks on democratic institutions (exemplified)
</details>
<details>
<summary>
2022-11-16: Let's talk about a review of Trump's announcement.... (<a href="https://youtube.com/watch?v=iCaXSjfmGLs">watch</a> || <a href="/videos/2022/11/16/Lets_talk_about_a_review_of_Trump_s_announcement">transcript &amp; editable summary</a>)

Beau doubts Trump's rebranding attempts will revive his support, expecting potential shifts towards attacking Republicans and unsettling campaign themes.

</summary>

"I was kind of expecting Trump to come out swinging and take shots at the governor of Florida, of Cruz, and anybody who kind of indicated they weren't going to support him."
"Every promise was also a weird admission of failure on his part."
"It's going to be much harder to trick them."
"That's a little creepy."
"One out of ten, I wouldn't watch again."

### AI summary (High error rate! Edit errors on video page)

Trump officially announced his candidacy, but Fox News cut away from his speech, deeming it boring.
Trump seemed restrained, reading off a teleprompter with only a few deviations.
His team appears to be trying to rebrand him to appeal to a wider audience, potentially risking his base.
Despite attempts to tone down his rhetoric, Beau doubts this strategy will boost Trump's dwindling support.
Trump made promises during his speech, including fixing supply chains and terminating the Green New Deal, but they were seen as admissions of failure.
Beau expected Trump to attack fellow Republicans who weren't supporting him to rally his base, which did not happen.
There's a suggestion that Trump may shift focus to attacking the Democratic Party instead.
Beau finds the use of the term "glory" in Trump's speech unsettling and hints at a potentially ominous campaign theme.
Overall, Beau rates Trump's announcement speech poorly and expresses disappointment in its content and delivery.

Actions:

for political analysts,
Analyze political speeches for underlying messages and implications (implied)
Stay informed about political developments and strategies (implied)
</details>
<details>
<summary>
2022-11-16: Let's talk about Poland, Ukraine, and Russia.... (<a href="https://youtube.com/watch?v=WXKS6d2g0tU">watch</a> || <a href="/videos/2022/11/16/Lets_talk_about_Poland_Ukraine_and_Russia">transcript &amp; editable summary</a>)

Beau breaks down recent events in Poland, Ukraine, and Russia, speculates on various scenarios, and shares his perspective on the potential outcomes without major worry about escalation.

</summary>

"I don't foresee this escalating to the point that a lot of people are suggesting."
"Given Ukraine's successes on the battlefield, I have been a strong proponent of the fact that I really think Ukraine can do this on their own."
"That was something that he was very concerned about and was a talking point that was issued as far as why NATO couldn't expand."

### AI summary (High error rate! Edit errors on video page)

Talks about the recent events involving Poland, Ukraine, and Russia.
Mentions the likely scenarios and reactions following the incident.
Shares his perspective of not being overly worried about the situation.
Explains that something fell into Poland, resulting in the death of two Polish citizens.
Points out Poland's NATO membership and potential escalation due to the incident.
Speculates on the possibility of Poland invoking Article 5 and its consequences.
Addresses the debate over what actually happened, referencing photos of S-300 wreckage.
Considers the scenario where Ukraine may have been involved in the incident.
Expresses hope that Ukraine was responsible for the missiles landing in Polish territory.
Emphasizes the significance of Ukraine's success in the conflict with Russia.
Predicts increased air defense placement in NATO countries near Russia post-incident.
Shares his overall lack of major concern about the situation escalating to a large-scale war.

Actions:

for world citizens,
Stay informed and follow updates on the situation (implied)
Support efforts towards peace and resolution in the region (implied)
Advocate for diplomatic solutions to prevent further escalation (implied)
</details>
<details>
<summary>
2022-11-16: Let's talk about Kari Lake's impact on the Arizona GOP.... (<a href="https://youtube.com/watch?v=cUBsb87a9tI">watch</a> || <a href="/videos/2022/11/16/Lets_talk_about_Kari_Lake_s_impact_on_the_Arizona_GOP">transcript &amp; editable summary</a>)

Arizona's election outcomes are molding the state's GOP, potentially foreshadowing national political shifts and internal party struggles.

</summary>

"Arizona's election results shape the GOP, hinting at national shifts."
"Removing the MAGA movement in Arizona may impact the national scene."
"Deep-rooted far-right ideologies persist in traditional Republican circles."
"Arizona provides insight into future GOP tactics and faction clashes."
"Watch Arizona for cues on the Republican Party's upcoming moves."

### AI summary (High error rate! Edit errors on video page)

Arizona's recent election results are shaping the state's GOP and may foretell national political shifts.
Carrie Lake, a Trump-like candidate, lost in Arizona, impacting the Republican Party.
Post-election baseless claims and refusal to concede are surfacing in Arizona.
Traditional Republicans in Arizona aim to remove the influence of the MAGA movement post-losses.
Arizona carries significant support for far-right ideologies.
National Republicans may follow Arizona's lead in sidelining the MAGA crowd.
Calls for the GOP chair in Arizona to resign are part of efforts to distance from Trumpist ideology.
Removing MAGA's public face doesn't erase underlying far-right leanings within the party.
Arizona's political developments offer insights into future GOP strategies and faction dynamics.
The future of the Republican Party may be influenced by how Arizona handles internal conflicts.

Actions:

for political observers, republicans,
Monitor Arizona's political developments for insights into future GOP strategies (implied).
Stay informed about internal conflicts within the Republican Party (implied).
</details>
<details>
<summary>
2022-11-15: Let's talk about the Marriage Protection Act.... (<a href="https://youtube.com/watch?v=fnznBp0q6e0">watch</a> || <a href="/videos/2022/11/15/Lets_talk_about_the_Marriage_Protection_Act">transcript &amp; editable summary</a>)

Senate debates Respect for Marriage Act as Republicans may cross party lines to support the bill, acknowledging the need for legislation protecting same-sex and interracial marriages.

</summary>

"This isn't a perfect bill. It's a start."
"Because of the aggressive nature of the Republican Party, we need a law to protect same-sex and interracial marriages."
"That's something that is like actually important legislation."

### AI summary (High error rate! Edit errors on video page)

Senate's closing days and a particular piece of legislation that seems likely to pass through a filibuster.
The Respect for Marriage Act is the focus of attention.
Mitch McConnell appears noncommittal about his stance on the bill.
Some senators are confident about having enough votes to overcome the filibuster.
Schumer confirms the upcoming vote on the bill.
Lack of public support statements from senators despite confidence in the votes.
Need for Republicans to support a bill protecting same-sex and interracial marriages.
Importance of a law due to Supreme Court decisions.
Politicians keeping their votes secret during the lame-duck period to avoid backlash.
Acknowledgment that the bill is not perfect but a step in the right direction.
Hope for smooth transitions if the bill passes through the Senate.
Mention of future content discussing conservatism and aging.
Emphasizing the necessity of legislation to protect marriages.
Reminder of the importance of considering current political positions regarding marriage protection laws.

Actions:

for legislative observers,
Contact your senators to express support for the Respect for Marriage Act (suggested).
Stay informed about the progress of the bill and potential impact on marriage laws (implied).
</details>
<details>
<summary>
2022-11-15: Let's talk about Native artifacts being returned.... (<a href="https://youtube.com/watch?v=nOH1FE7YV_8">watch</a> || <a href="/videos/2022/11/15/Lets_talk_about_Native_artifacts_being_returned">transcript &amp; editable summary</a>)

Beau sheds light on the ongoing issue of artifacts taken from Native grave sites, stressing the importance of their return and the need for continued awareness and action.

</summary>

"The theft of Native culture is an ongoing issue."
"More than 100,000 articles that are human remains are in the possession of museums."
"These stories will happen more and more often, hopefully in larger quantities."
"There's no dispute about who the rightful owners of this property are."
"It is vital to keep the focus on it."

### AI summary (High error rate! Edit errors on video page)

150 artifacts were returned to the Oglala and Cheyenne River Sioux from a museum in Massachusetts.
These artifacts were believed to have been taken from a grave site at Wounded Knee and include clothing, weapons, and personal effects.
The museum, although not legally obligated, returned the artifacts because it was the right thing to do.
The incident at Wounded Knee in 1890 resulted from a lack of understanding by the U.S. of Native culture, leading to a tragic massacre.
Museums, colleges, and the federal government possess around 850,000 artifacts, with more than 100,000 being human remains, still not returned to their rightful owners.
The theft of Native culture is an ongoing issue with numerous human remains and artifacts not where they belong.
Despite the clear ownership of these artifacts, delays in returning them are often due to financial reasons or the belief that the original owners don't need them back.
The return of these artifacts is critical, and stories like this need more attention to drive further action.
The rightful owners of the artifacts are indisputable, and it's imperative to continue shedding light on this issue.
The return process may be prolonged due to various reasons, but efforts must persist until justice is served.

Actions:

for history enthusiasts, activists,
Contact museums and institutions to urge the return of artifacts and human remains (suggested)
Support initiatives advocating for the repatriation of stolen cultural items (implied)
</details>
<details>
<summary>
2022-11-15: Let's talk about Iran and the news.... (<a href="https://youtube.com/watch?v=vHQd4vpeWvY">watch</a> || <a href="/videos/2022/11/15/Lets_talk_about_Iran_and_the_news">transcript &amp; editable summary</a>)

Beau clarifies misinformation on alleged mass death sentences in Iran, urging caution and accurate reporting amidst concerns over severe punishments.

</summary>

"There have not been 15,000 sentences handed down like that."
"So while it hasn't happened, that doesn't mean that the Iranian government isn't going to use that penalty to deter further demonstrations."
"But most of it looks like it's inaccurate due to it being out of context."
"I wouldn't want to make a mistake in reporting."
"I'd wait for somebody who is really covering this to talk about it or a fact-check from a straight-up news organization."

### AI summary (High error rate! Edit errors on video page)

Providing context and clarifying inaccurate information regarding Iran.
Reports of 15,000 people sentenced to death in Iran are false.
Only one person has received such a sentence for allegedly torching a government building.
The 15,000 number comes from a UN estimate of detainees during demonstrations, with only 2,000 charged.
A letter from Iran's parliament to the judicial branch recommends severe punishment without specifying details.
Comparing the situation to US politicians urging judges to be tough on demonstrators.
Human rights advocates express concern about potential severe punishments for charged individuals.
No expectation of 15,000 sentences, but anticipation of more severe punishments.
Cautioning against misinformation and the need for fact-checking before reacting.
Emphasizing the importance of accurate reporting and avoiding uproar based on incomplete information.

Actions:

for global citizens,
Wait for verified information from reliable news sources (suggested)
Fact-check before reacting or spreading information (suggested)
</details>
<details>
<summary>
2022-11-14: Let's talk about the 6th and Pence's soundbite.... (<a href="https://youtube.com/watch?v=IH0DOwRSEBU">watch</a> || <a href="/videos/2022/11/14/Lets_talk_about_the_6th_and_Pence_s_soundbite">transcript &amp; editable summary</a>)

Beau stresses Pence's need to transparently explain his actions during the Capitol events to secure his place in history and combat authoritarianism.

</summary>

"Had he made different decisions that day, we would be living in a very different world right now."
"He's running out of time."
"He has to tell the American people what happened, and what was at risk, and why it was so important for him to stay."

### AI summary (High error rate! Edit errors on video page)

Beau raises the significance of the date "the sixth" post the Capitol events, now replacing historical references like D-Day with images of the Capitol.
He acknowledges Pence's role in standing in the gap during the Capitol events and recognizes the gravity of his actions.
Beau stresses that Pence's recent statements laying blame on Trump are not enough; he needs to explain his actions and decisions in his words to the American people.
He urges Pence to share the full truth about what was at stake that day and why he chose to stay, rather than leaving through safer options.
Beau points out that Pence needs to address the American people directly, with clarity and honesty, to have a lasting impact on the country.
He mentions that if Pence fails to tell his story, history will be crafted by others, and his actions may lose their significance over time.
Beau underlines the importance of Pence's role in combating authoritarianism and nationalism, urging him to take decisive actions beyond mere statements.
He suggests that speaking openly to the committee and being transparent about his concerns and motivations could have a profound effect on the nation.
Beau warns that if Pence continues to delay, his historical legacy may diminish as others shape his narrative.
He concludes by expressing the urgency of Pence's situation, indicating that time is running out for him to solidify his place in history.

Actions:

for american citizens,
Speak openly and honestly about critical events (implied)
Share personal motivations and risks taken with transparency (implied)
Take decisive actions beyond statements to combat authoritarianism (implied)
</details>
<details>
<summary>
2022-11-14: Let's talk about bad information and good points.... (<a href="https://youtube.com/watch?v=7i-QPb-JsUk">watch</a> || <a href="/videos/2022/11/14/Lets_talk_about_bad_information_and_good_points">transcript &amp; editable summary</a>)

Addressing misconceptions on civilian casualties, Beau navigates through flawed data to underscore the significance of accurate arguments and intelligence in addressing the issue effectively.

</summary>

"The precision of the weapon was fine. The precision of the intelligence, the information that led them to say, yes, hit it, that's the problem."
"Alter your argument because the point you're trying to make is 100% valid."
"Don't get caught up in bad information when you're trying to make a good point."

### AI summary (High error rate! Edit errors on video page)

Addressing the need to debunk misinformation on a specific topic concerning civilian casualties.
Acknowledging the importance of presenting arguments accurately to support valid points.
Explaining the misconception that the US is worsening in protecting civilians during wartime, when in reality, it is improving.
Clarifying the misconception that drones cause 90% civilian casualties and providing scenarios to demonstrate the fallacy.
Emphasizing the challenge of determining accurate numbers regarding drones and civilian casualties due to the complex nature of targeting.
Pointing out that the issue is not the precision of the weapons but rather the intelligence failures leading to civilian casualties.
Advocating for focusing on anecdotes and intelligence failures rather than unreliable statistics to address the problem effectively.
Mentioning the preference for politicians to use drones due to avoiding risks associated with boots on the ground, leading to compromised intelligence.
Describing the historical changes in rules of engagement under different administrations, noting Trump's loosening of regulations.
Expressing cautious optimism about Biden's new guidance on drone strikes but underscoring the uncertainty of its effectiveness without adherence over time.

Actions:

for advocates, activists, educators,
Advocate for better intelligence and approval processes in military operations (implied).
</details>
<details>
<summary>
2022-11-14: Let's talk about Trump as Speaker of the House.... (<a href="https://youtube.com/watch?v=Y9_QSu9JFmA">watch</a> || <a href="/videos/2022/11/14/Lets_talk_about_Trump_as_Speaker_of_the_House">transcript &amp; editable summary</a>)

Beau sarcastically dissects the idea of Trump becoming Speaker of the House, predicting disastrous consequences for the Republican Party and delight for the Democrats.

</summary>

"Owning the libs on social media is not actually a valid electoral strategy."
"It will be the greatest gift the Republican Party could ever give to the Democratic Party."
"There are a whole lot more stable geniuses out there than I believed."

### AI summary (High error rate! Edit errors on video page)

Speculates on the possibility of former President Donald J. Trump becoming Speaker of the House if the Republican Party retakes the House.
Considers the qualifications required for the role, sarcastically pointing out Trump's supposed expertise in meticulous planning and attention to detail.
Raises concerns about Trump potentially using his position to further divide the Republican Party and create discord.
Mocks the idea of Trump handling committees and investigations effectively, suggesting it could backfire and tie him even more closely to the Republican Party.
Criticizes the notion that the American people overwhelmingly support Trump and suggests that making him Speaker might lead to backlash against the Republican Party.
Satirically imagines a scenario where Trump becomes president again through impeachment if he were to be Speaker of the House.
Expresses personal dismay and disbelief at the idea, suggesting that it could be disastrous for the Republican Party and a boon for the Democrats.
Concludes by humorously stating that the Republican Party gifting the Democrats two years of Trump in a position of power could lead to a disastrous 2024 election.

Actions:

for political commentators and voters.,
Contact your representatives to express your views on the potential consequences of Trump becoming Speaker of the House (suggested).
Organize or support political campaigns that prioritize unity and effective governance (implied).
</details>
<details>
<summary>
2022-11-13: Let's talk about what you need to know in Georgia and a PSA.... (<a href="https://youtube.com/watch?v=69U7WiviG0A">watch</a> || <a href="/videos/2022/11/13/Lets_talk_about_what_you_need_to_know_in_Georgia_and_a_PSA">transcript &amp; editable summary</a>)

Both parties focus on Georgia's runoff, expect mudslinging, and obstacles to voting.

</summary>

"Both parties are going to just dump untold amounts of money into it."
"The results should trend towards the Democratic Party, as long as people show up."
"Just be aware of the obstacles that the state establishment is likely to throw up in your way."

### AI summary (High error rate! Edit errors on video page)

Talks about the control of the Senate with all eyes on Georgia.
Mentions the possibility of the Democratic Party maintaining control without Georgia, but it's slim.
Emphasizes the significant impact of the runoff in Georgia on Senate control.
Points out the massive amounts of money both parties will invest in the Georgia runoff.
Expects efforts to discourage voting and lots of mudslinging due to the high stakes.
Notes the national attention on Georgia's runoff election.
Provides information on Georgia's absentee ballot application deadlines.
Believes that if people show up, the results will favor the Democratic Party.
Points out reasons why the Democratic Party may have an edge in Georgia.
Warns about historical efforts in Georgia to suppress certain votes and dissuade people from voting.

Actions:

for georgia voters,
Submit absentee ballot applications by deadlines (implied)
Be aware of potential obstacles and efforts to discourage voting (implied)
</details>
<details>
<summary>
2022-11-13: Let's talk about the GOP distancing from Trump.... (<a href="https://youtube.com/watch?v=dpRMRN1H5q4">watch</a> || <a href="/videos/2022/11/13/Lets_talk_about_the_GOP_distancing_from_Trump">transcript &amp; editable summary</a>)

Conservative media and the Republican Party are distancing from Trump not due to morals but because he lost, revealing a power-driven agenda over principles.

</summary>

"It's not a moral position. It's not an ethical one. It's not a philosophical one. He lost."
"They care about power and Trump can't deliver anymore."
"If you want to follow people who will accept everything that Trump did as long as they're winning."
"He's just not their ticket to power anymore."

### AI summary (High error rate! Edit errors on video page)

Conservative media and the Republican Party are considering parting ways with Trump.
The shift is not due to moral or ethical reasons but because he lost.
Trump's brand is fading, and his ability to rally supporters is diminishing.
The decision to distance from Trump is driven by a desire for power, not values.
Those seeking power for the far right prioritize power over principles.
They are willing to overlook Trump's actions as long as they are winning.
The shift signifies a focus on maintaining power rather than condemning Trump's behavior.
The country's direction depends on whether people support those who prioritize power over values.
Individuals have the choice to lead towards a better future that doesn't rely on divisive tactics.
The decision to break ties with Trump is solely based on his loss of influence rather than a moral stance.

Actions:

for voters, activists,
Question who you support based on values, not just winning (implied).
Lead towards a better future without relying on divisive tactics (implied).
Advocate for leaders who prioritize principles over power (implied).
</details>
<details>
<summary>
2022-11-13: Let's talk about Trump in heels and Kari Lake.... (<a href="https://youtube.com/watch?v=ZciFgggRndI">watch</a> || <a href="/videos/2022/11/13/Lets_talk_about_Trump_in_heels_and_Kari_Lake">transcript &amp; editable summary</a>)

Analyzing the Cary Lake race in Arizona reveals the false reporting of desired outcomes as facts by the MAGA faction and the rejection of authoritarian nationalism in the United States.

</summary>

"You keep acting like MAGA has failed. What about Governor-elect Cary Lake? She's trumping hills, boy."
"This isn't Democrats doing this to you. This is the Republican Party saying they're just not that into you."
"The Lake race is very much a symbol of that."
"It was way too close for it to be a sign that an authoritarian nationalist philosophy is something the United States wants to embrace."
"Historically, the United States has always rejected that type of philosophy."

### AI summary (High error rate! Edit errors on video page)

Analyzing the Cary Lake race in Arizona, where votes are still being counted.
Messages from a MAGA faction expecting Cary Lake's victory were received early.
The MAGA faction falsely reports desired outcomes as facts, leading followers to believe in inevitable victory.
Disillusionment occurs when election results do not match predictions, leading to baseless claims of election system issues.
Normal Republicans won in some states where election-denying MAGA Republicans lost on the same ticket.
The rejection of ultra-nationalism post-Trump is evident in Cary Lake's close race.
Even if Cary Lake wins, it won't signify a continued victory for MAGA.
The United States historically rejects authoritarian nationalist philosophies.

Actions:

for voters, political analysts,
Stay informed about political factions and their narratives (suggested)
Reject baseless claims and misinformation (suggested)
Advocate for transparent and fair election processes (suggested)
</details>
<details>
<summary>
2022-11-12: Let's talk about the end of a telescopic era.... (<a href="https://youtube.com/watch?v=VFfL94Pp-bQ">watch</a> || <a href="/videos/2022/11/12/Lets_talk_about_the_end_of_a_telescopic_era">transcript &amp; editable summary</a>)

Beau shares the end of an era as the iconic Arecibo telescope collapses, leaving room for a new symbol to represent humanity's curiosity about space.

</summary>

"It's the end of an era."
"Human curiosity is not something that just stops."
"Something else will have to be built and take over the mantle of the icon of searching the sky."

### AI summary (High error rate! Edit errors on video page)

Introduction to space news about the Arecibo telescope in Puerto Rico.
The Arecibo telescope was featured in movies like James Bond's Golden Eye and Contact.
The telescope identified 191 near-Earth asteroids, with 70 potentially hazardous ones.
Assures that "potentially hazardous" means more than 4.5 million miles away.
Earth is clear of hazardous asteroids for the next 100 years based on current NASA knowledge.
The telescope also provided information on asteroids with water.
Data from the telescope was used in the recent DART test to redirect an asteroid.
Despite its collapse in December 2020, the telescope will not be repaired or replaced by the National Science Foundation.
The end of the era of the iconic Arecibo telescope symbolizing humanity's curiosity about space.
Anticipation for a new symbol to take over the mantle of sky exploration.

Actions:

for space enthusiasts, astronomers.,
Support initiatives promoting space exploration and research (implied).
Stay informed about new developments in space technology and telescopes (implied).
</details>
<details>
<summary>
2022-11-12: Let's talk about doctors vs dirty energy.... (<a href="https://youtube.com/watch?v=4tx_s-UdMXs">watch</a> || <a href="/videos/2022/11/12/Lets_talk_about_doctors_vs_dirty_energy">transcript &amp; editable summary</a>)

Doctors link health issues to climate change caused by dirty energy companies, urging urgent transition away from harmful practices.

</summary>

"The dirty energy companies, their smartest move is to transition themselves as fast as possible."
"The situation here is pretty similar to tobacco."
"The dirty energy companies, they have a decision to make and they're running out of time to make it."

### AI summary (High error rate! Edit errors on video page)

Doctors are attributing various health issues to climate change caused by dirty energy companies.
Mosquitoes have an extended range due to changing temperatures, leading to an increase in Zika and dengue cases in the U.S.
Particulate air pollution caused 1.2 million deaths worldwide, including 11,840 in the United States.
Extreme heat has led to 98 million cases of hunger globally.
Heat-related deaths in the U.S. have increased by 68%, rising to 74% for those over 65.
Climate change and emissions are causing a wide array of health issues globally.
Dirty energy companies, subsidized by governments, are making massive profits while contributing to medical problems.
The Lancet releases annual reports detailing the impact of dirty energy companies on health.
Transitioning away from dirty energy is imperative for these companies, akin to the tobacco industry facing lawsuits.
Urgent decisions are needed from dirty energy companies as time is running out for change.

Actions:

for climate advocates, policymakers, activists.,
Read the Lancet report on the impact of dirty energy companies on health (suggested).
Advocate for transitioning away from dirty energy sources in your community (implied).
</details>
<details>
<summary>
2022-11-12: Let's talk about Republicans losing more than the youth.... (<a href="https://youtube.com/watch?v=5QOwb_7cTf0">watch</a> || <a href="/videos/2022/11/12/Lets_talk_about_Republicans_losing_more_than_the_youth">transcript &amp; editable summary</a>)

Examining Republican missteps and the impact of young voters on the lack of a red wave, Beau outlines how the party's own lies and misleading rhetoric contributed to their defeat, urging a shift away from authoritarianism and Trumpism to correct their course.

</summary>

"People like their rights. They don't like being told what to do."
"A party that constantly rails about personal freedom should understand that."
"The Republican Party didn't get the red wave in large part because of its own lies."
"The only way to correct this is to move away from the authoritarian rhetoric."
"Until the Republican Party does that, they will lose."

### AI summary (High error rate! Edit errors on video page)

Examines Republican missteps leading to the lack of a red wave in the recent election.
Young people played a significant role in shaping the outcome.
Overturning Roe v. Wade resulted in negative voter turnout.
Republican Party's failure to understand personal freedom and rights had an impact.
Advocating against public health precautions reduced their voting base.
Failure to counter misinformation cost them voters.
Backing baseless claims about the election led to decreased voter turnout.
Republican policies focused on opposing Biden rather than presenting alternatives.
The party's reliance on lies and misleading rhetoric contributed to their defeat.
Urges the Republican Party to move away from authoritarianism and Trumpism to correct their course.

Actions:

for political observers, voters,
Move away from authoritarian rhetoric and Trumpism (suggested)
Advocate for policies beyond simply opposing Biden (implied)
</details>
<details>
<summary>
2022-11-11: Let's talk about republicans coping with the midterm news.... (<a href="https://youtube.com/watch?v=YK5-wmNQNds">watch</a> || <a href="/videos/2022/11/11/Lets_talk_about_republicans_coping_with_the_midterm_news">transcript &amp; editable summary</a>)

Republicans must distance themselves from Trumpism and extreme rhetoric to avoid further losses in future elections.

</summary>

"Hitching themselves to Trump is a losing proposition."
"The Republican Party needs to distance itself from MAGA and authoritarian leadership."
"Trump is a liability."
"The younger, smarter Republicans will start distancing themselves from that rhetoric."
"The party's failure to address the current situation may lead to further losses in the future."

### AI summary (High error rate! Edit errors on video page)

Republicans are struggling to cope with the disappointing results from the midterms.
Many Republicans believe that the party could still gain control of the House or Senate and are clinging to Trumpism for hope.
However, hitching themselves to Trump seems like a losing proposition as he is now a political liability.
The Democratic Party may find it easier to sway Republicans to cross the aisle now that Trump is no longer a leading political figure.
The Republican Party's failure to hold Trump accountable and their alignment with MAGA has led them to continuous losses.
First-term representatives are unlikely to get re-elected by obeying Trump and following the same rhetoric.
The presumptive nominee is predicted to lead to the same outcomes unless the party changes its course.
The Republican Party needs to distance itself from MAGA and authoritarian leadership to have a chance at winning again.
Beau suggests that Republicans need to step away from extreme rhetoric and recognize Trump as a liability.
The party's failure to address the current situation may lead to further losses in the future.

Actions:

for republicans,
Distance yourself from extreme rhetoric and recognize Trump as a liability (suggested).
Take action to protect yourself and your political standing (implied).
</details>
<details>
<summary>
2022-11-11: Let's talk about humpty dumpty Trumpty.... (<a href="https://youtube.com/watch?v=kKCPmOQEnlE">watch</a> || <a href="/videos/2022/11/11/Lets_talk_about_humpty_dumpty_Trumpty">transcript &amp; editable summary</a>)

The Republican Party's attempt to blame Trump for their losses reveals the deeper issue of Trumpism and enabling, ultimately leading to voters pushing him off the wall.

</summary>

"They put him on the wall. They elevated him. They backed him. They enabled him."
"Thinking that your most vocal supporters are representative of the entire country, that's on y'all."
"He was pushed by voters that are tired of him and tired of you."

### AI summary (High error rate! Edit errors on video page)

Republican Party trying to blame Trump for their midterm losses.
Trump was elevated and enabled by political personalities and politicians.
Trump shares blame, but the real issue is Trumpism.
Republican Party making the same mistake again.
Leaders following along out of fear of Trump's tweets.
Elitist attitude of blaming Trump for falling off the wall.
Voters pushed Trump off the wall by being tired of him.
Trump led the party to multiple losses after winning one election.
Enabling and encouraging Trump's behavior contributed to the current situation.
Real problem lies in following and enabling Trumpism, not just Trump himself.

Actions:

for voters, political personalities,
Hold political personalities and politicians accountable for enabling and elevating Trump (implied).
Recognize the dangers of Trumpism and work towards dismantling it within political parties (implied).
Support leaders who prioritize the interests of the entire country over fear of a tweet (implied).
</details>
<details>
<summary>
2022-11-11: Let's talk about how the republicans plan to raise the voting age.... (<a href="https://youtube.com/watch?v=R-rpDVlX-JY">watch</a> || <a href="/videos/2022/11/11/Lets_talk_about_how_the_republicans_plan_to_raise_the_voting_age">transcript &amp; editable summary</a>)

Analyzing the Republican strategy to raise the voting age to 21 reveals their power-hungry motives, disregarding the rights of citizens and alienating younger, more progressive voters.

</summary>

"They don't care about the Republic. They have no concept."
"Power for power's sake."
"Denying people their rights, it's not gonna work."
"Not representing their constituents, but ruling them."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Analyzing the Republican strategy to avoid a repeat of the midterms.
The plan includes raising the voting age to 21, revealing their priorities.
Beau finds this plan brilliant as it exposes the Republican Party's true motives for power.
Republicans are willing to deny people their rights to maintain power.
Beau suggests openly admitting to raising the voting age to show their intentions.
Pointing out the irony of not raising the age to buy a rifle but wanting to raise the voting age.
Younger Americans are more progressive and less likely to conform to Republican ideals.
Negative voter turnout is a result of the Republican Party's outdated tactics.
Referencing the 26th Amendment's background in empowering young voters.
Beau criticizes the Republican Party for valuing power over representing constituents.

Actions:

for american voters,
Advocate for fair voting rights (exemplified)
Support voter education initiatives (implied)
</details>
<details>
<summary>
2022-11-10: Let's talk about who outside the US won the midterms.... (<a href="https://youtube.com/watch?v=skube6UON0g">watch</a> || <a href="/videos/2022/11/10/Lets_talk_about_who_outside_the_US_won_the_midterms">transcript &amp; editable summary</a>)

Beau examines the international implications of the US election, from Russia's loss to Biden's signal of stable foreign policy, reflecting shifts in global dynamics.

</summary>

"The biggest loser, Russia."
"Biden coming in in that statement, you know, we're back, baby, or something like that, that was a signal."
"It shows a little bit more stability in the United States."

### AI summary (High error rate! Edit errors on video page)

The impact of the US election on foreign powers is often overlooked, with some countries admitting to attempting to influence the outcome.
Russia was a major loser in the election, as they were hoping for Republicans to win and cut aid to Ukraine, which did not happen.
Russian leadership may become demoralized and lose resolve due to the election outcome.
The election could impact the outcome of a war and potentially bring it to a close faster.
Changes in American foreign policy require careful planning, logistics, and thought, which were lacking in the Trump administration.
Biden's administration signals a return to more consistent and stable American foreign policy.
The midterm election was seen as a test by European administrations to gauge the stability of the US as an ally.
The election outcome showed more stability in the United States, contrary to nationalist rhetoric weakening the country internationally.
The stance on China differs between the Republican and Democratic parties, with Republicans vilifying China and Democrats being tougher but softer in rhetoric.
The election results have implications for foreign policy, positioning the US differently on the international stage.

Actions:

for policy analysts, global relations experts,
Analyze and understand the international implications of US elections (implied)
Stay informed about foreign policy changes and their impacts (implied)
</details>
<details>
<summary>
2022-11-10: Let's talk about Meidas Touch, Stonekettle, and myself.... (<a href="https://youtube.com/watch?v=d22EomnrUYo">watch</a> || <a href="/videos/2022/11/10/Lets_talk_about_Meidas_Touch_Stonekettle_and_myself">transcript &amp; editable summary</a>)

Beau shares insights on different data analysis approaches, media reliance on polls, and the need for informed citizenship in democracy.

</summary>

"The analytics that YouTube provides is scary."
"Democracy requires advanced citizenship."
"Polls have to be accurate, but they were only measuring part of the picture."

### AI summary (High error rate! Edit errors on video page)

Explains the different approaches used by Midas Touch and Stone Kettle in analyzing data related to polling places.
Midas Touch utilized data analysis, including early voting numbers and registrations, to make predictions.
Stone Kettle's approach involved pointing out the limitations of polling, such as only reaching those willing to answer unknown phone numbers.
Beau shares his method of analyzing YouTube analytics and social media engagement to predict an increase in voter turnout among younger demographics.
Criticizes the media's heavy reliance on polls, suggesting a need for new polling methods and less emphasis on polling results.
Advocates for journalists to give candidates a platform to speak freely about their platforms, allowing viewers to make informed decisions.
Believes that democracy requires informed citizenship and accurate polling to capture the whole picture.
Notes that younger voters, often overlooked in polls, showed up unexpectedly in the recent election, catching pollsters off guard.

Actions:

for media consumers,
Analyze data from various sources to understand trends and patterns in your community (implied).
Encourage journalists to provide platforms for candidates to speak directly about their platforms (implied).
</details>
<details>
<summary>
2022-11-10: Let's talk about CEOs "destroying the planet".... (<a href="https://youtube.com/watch?v=BigMDFd8eME">watch</a> || <a href="/videos/2022/11/10/Lets_talk_about_CEOs_destroying_the_planet">transcript &amp; editable summary</a>)

CEOs release report on agriculture and climate change, sparking skepticism but offering potential for positive impact on public discourse and policy.

</summary>

"We have to change the way we're doing things because we are destroying the planet."
"The interconnection between human health and planetary health is more evident than ever before."
"Still no action. So those who believe it's PR, they've got good grounds to believe it."

### AI summary (High error rate! Edit errors on video page)

Report released by CEOs before COP 27, focusing on climate change and agriculture.
CEOs from major companies like Bayer, Mars, McDonald's, PepsiCo involved.
Report stresses the need to change current practices to prevent planetary destruction.
Critics skeptical about companies' true intentions, labeling it as PR move.
Despite potential PR motives, the report's release can still have a positive impact.
CEO of Mars draws criticism for vague statement on human and planetary health interconnection.
Some believe the report can serve as a wake-up call for those denying or ignoring climate change.
The timing of the report's release before COP 27 raises suspicions of PR tactics.
The report can be used to drive public discourse and influence policymakers.
Beau encourages using the report to hold representatives accountable and spark change.

Actions:

for climate activists, policymakers, environmental advocates.,
Contact your representatives to advocate for sustainable agricultural practices (implied).
Use the report to raise awareness and drive public discourse on climate change (implied).
</details>
<details>
<summary>
2022-11-09: Let's talk about why there wasn't a red wave.... (<a href="https://youtube.com/watch?v=3z0VrkCBrv8">watch</a> || <a href="/videos/2022/11/09/Lets_talk_about_why_there_wasn_t_a_red_wave">transcript &amp; editable summary</a>)

At the midterm election, Beau analyzes the absence of a red wave, criticizes Republican strategies, and underscores the importance of younger voters in shaping the political landscape.

</summary>

"The divide will grow more and more pronounced."
"The only way back for the Republican Party is to reject Trumpism."
"They believe all people are created equal and that people have certain unalienable rights."
"Those younger voters, those unlikely voters, they've proven they're going to show up."

### AI summary (High error rate! Edit errors on video page)

Election night analysis from a critical perspective.
Absence of a red wave despite expectations.
Reasons why Republicans did not sweep the election.
Negative voter turnout due to Republican actions.
Embracing Trumpism as a liability for the Republican Party.
Democratic Party's success in mobilizing young voters.
Potential impact of young voters in future elections.
Urges the Republican Party to reject Trumpism for a comeback.
Contrasting values between younger and older voters.
Emphasis on actual freedom and equality in governance.

Actions:

for voters, activists, analysts,
Reject Trumpism and authoritarianism within the Republican Party (suggested)
Mobilize and empower young voters for future elections (implied)
</details>
<details>
<summary>
2022-11-09: Let's talk about how to get the candidates you want.... (<a href="https://youtube.com/watch?v=XKYLTxfD-3o">watch</a> || <a href="/videos/2022/11/09/Lets_talk_about_how_to_get_the_candidates_you_want">transcript &amp; editable summary</a>)

Beau explains why electoralism is the least effective form of civic engagement and advocates for utilizing all tools available to create deep systemic change by changing societal thought.

</summary>

"Electoralism is the least effective form of civic engagement."
"To change society, you have to change thought."
"You can't rely on electoralism. You have to use every tool at your disposal."

### AI summary (High error rate! Edit errors on video page)

Talks about civic engagement and electoralism.
Mentions the challenge of getting a candidate who truly represents your beliefs through the primary and general election.
States that electoralism is the least effective form of civic engagement.
Suggests changing society through other forms of civic engagement like capacity building, direct service, research, advocacy, and education.
Emphasizes the importance of changing societal thought to shift the Overton window.
Lists various forms of civic engagement including personal responsibility, philanthropy, and participation and association.
Advocates for using all forms of civic engagement to create deep systemic change.
Stresses the need to build a power structure outside of existing ones to leverage for societal change.

Actions:

for community members, activists, voters,
Build capacity at the local level to support chosen candidates (suggested)
Engage in direct service to immediately impact outcomes (exemplified)
Conduct research to provide necessary information for civic engagement (implied)
Advocate through petitions and demonstrations to alter public opinion (exemplified)
Demonstrate personal responsibility by setting an example of desired change (exemplified)
Engage in philanthropy to address issues needing financial support (exemplified)
Leverage contacts and networks on a social level to influence change (implied)
</details>
<details>
<summary>
2022-11-09: Let's talk about DeSantis, Trump, and information.... (<a href="https://youtube.com/watch?v=Yif8sCjnkio">watch</a> || <a href="/videos/2022/11/09/Lets_talk_about_DeSantis_Trump_and_information">transcript &amp; editable summary</a>)

Beau questions Trump's ethics in potentially using damaging information against DeSantis, revealing a lack of transparency and prioritizing political gain over Republican voters' interests.

</summary>

"If there is something and Trump knows about it and Ron DeSantis is doing something that Republicans would find morally objectionable or perhaps it's damaging the state of Florida, how can you trust Trump at that point?"
"He's flat out saying he is putting his personal political interests above that of Republican voters."
"I guess having an informed public, having informed voters, I guess that's not important to Trump."
"If you have any questions about who Trump is and you're a Republican, he just told you."

### AI summary (High error rate! Edit errors on video page)

Trump's ambiguous stance on potentially running against DeSantis and threatening to reveal damaging information about him.
Speculation on whether Trump's tactic is a form of coercion against DeSantis.
Trump's claim of possessing information about DeSantis that could be damaging to Republicans.
Questioning the ethics of withholding damaging information until politically advantageous.
Doubts on trusting Trump if he prioritizes his political interests over those of Republican voters.
Trump's perceived disregard for an informed public and preference for uneducated voters.
Implication that Trump's actions reveal his character to Republicans.
Overall, Beau criticizes Trump's potential manipulative tactics and lack of transparency.

Actions:

for republican voters,
Question Trump's motives and transparency (implied)
</details>
<details>
<summary>
2022-11-08: Let's talk about paywalls and democracy.... (<a href="https://youtube.com/watch?v=Aau7ofIdR28">watch</a> || <a href="/videos/2022/11/08/Lets_talk_about_paywalls_and_democracy">transcript &amp; editable summary</a>)

Beau shares insights on paywalls, democracy, and informed citizenship, questioning the impact of limiting access to information on societal discourse and decision-making.

</summary>

"If you're in an information desert because you lack funds, eventually you'll drink the sand."
"I don't think this person's wrong."
"Democracy, the American experiment. It's advanced citizenship."
"My titles at this point have become a meme about how confusing they are."
"I know that just the title can stop somebody from listening and getting information that they might really need."

### AI summary (High error rate! Edit errors on video page)

Expresses his stance against putting content behind paywalls due to his unique goals and approach.
Acknowledges that other creators have their reasons for structuring their content with paywalls.
Mentions the confusion surrounding his video titles, which have become a meme.
Points out how putting content behind paywalls can prevent people from accessing information they might need.
Talks about a thread tagging him where he couldn't access articles without paying, contrasting it with free access to questionable content on other platforms.
Questions the impact of paywalls on democracy and informed citizenship.
Suggests that providing better insight and facts at a cost leads to a more emotionally charged audience.
Recognizes that some creators rely on paywalls for their business models while expressing gratitude for his supportive audience.
Believes that limited options may lead people to choose bad ones due to information scarcity.
Shares a metaphorical statement about drinking sand in an information desert due to lack of funds.

Actions:

for content creators, viewers,
Support content creators who provide valuable insights without paywalls (exemplified).
Advocate for accessible information and fight against information scarcity (exemplified).
</details>
<details>
<summary>
2022-11-08: Let's talk about elephant, zebras, and drought.... (<a href="https://youtube.com/watch?v=Uw9uwNSQXO4">watch</a> || <a href="/videos/2022/11/08/Lets_talk_about_elephant_zebras_and_drought">transcript &amp; editable summary</a>)

Addressing the impacts of climate change on animals in Kenya reveals the urgent need to take action to prevent ongoing problems for both wildlife and local communities.

</summary>

"This is one of those things that we really shouldn't ignore, but we probably will."
"It's clear from this that there will be ongoing problems, not just with animals, but with people getting food, water in this region."

### AI summary (High error rate! Edit errors on video page)

Addressing the impacts of climate change on animals in Kenya, specifically elephants, giraffes, and zebras.
Noting how animals can serve as indicators of shifting climates before people are severely impacted.
Sharing a report from the Wildlife Service in Kenya that details the loss of various animal species due to drought.
Mentioning the staggering numbers of animals lost, including endangered species like grevy's zebras.
Expressing concern over the lack of a plan to address the situation and the challenges of providing water and salt licks to the remaining animals.
Emphasizing the importance of not overlooking the signs of environmental distress and the need to take action.
Describing how the loss of these animals will lead to ongoing problems for both wildlife and local communities in accessing food and water.
Implying that ignoring these warning signs could have severe consequences for the region's ecosystem and inhabitants.
Noting that the current situation is a clear indication of the impact of climate change on the area.
Drawing attention to the broader implications of the drought, affecting both animals and humans in the region.

Actions:

for conservationists, environmentalists, wildlife advocates,
Support local wildlife conservation efforts to protect endangered species like grevy's zebras (suggested).
Join initiatives working to address climate change impacts on wildlife and habitats (implied).
</details>
<details>
<summary>
2022-11-08: Let's talk about China, Russia, and Ukraine.... (<a href="https://youtube.com/watch?v=A-sd3SgRAm0">watch</a> || <a href="/videos/2022/11/08/Lets_talk_about_China_Russia_and_Ukraine">transcript &amp; editable summary</a>)

China strategically positions itself against nuclear threats in Europe, showcasing diplomatic leadership and ensuring Russia's military weakening in Ukraine.

</summary>

"Nations don't have friends. They have interests."
"China is succeeding in these moves."
"It's good for Russia to stay in Ukraine and continue to degrade their military."
"They have to stand against that."
"China didn't come out and make this statement because they suddenly thought Russia was going to do it."

### AI summary (High error rate! Edit errors on video page)

China publicly opposing threats to use nuclear weapons prompts questions about their motivations and the dynamic with Russia regarding Ukraine.
Nations don't have friends, they have interests that can create temporary alliances.
China's recent statement to Russia to halt tactical actions in Ukraine is a shift indicating they want Russia to continue fighting to weaken themselves.
China's motivation lies in maintaining their geopolitical position and economic interests in Europe by ensuring Russia's military degradation.
China's stance against nuclear threats in Europe is to protect their economic interests and position themselves favorably on the international stage.
The Chinese government's statement costs them nothing since Russia is unlikely to act on nuclear threats, but it garners goodwill from Europe.
The fractured friendship between Russia and China shows that national interests supersede any claimed alliances.
China's strategic moves aim to position themselves as a significant global power opposite the West, succeeding in their diplomatic maneuvers.
China's public stance is more about diplomacy and international perception than genuine concern over Russian actions.
China's actions reveal a calculated strategy to enhance their standing on the world stage by showcasing leadership and diplomatic prowess.

Actions:

for diplomacy enthusiasts,
Monitor international developments and understand the underlying motivations of global powers (suggested)
Support diplomatic efforts that prioritize peace and stability in conflict zones (implied)
</details>
<details>
<summary>
2022-11-07: Let's talk about the economic power of the parties.... (<a href="https://youtube.com/watch?v=eV9iCmX15HA">watch</a> || <a href="/videos/2022/11/07/Lets_talk_about_the_economic_power_of_the_parties">transcript &amp; editable summary</a>)

Beau addresses misconceptions about conservative economic power and exposes the economic disparity between Republican and Democratic counties in the US.

</summary>

"Not nice word about me. Yeah, you bunch of not nice words about me are brainwashing the young."
"Your math is wrong. Your belief is wrong. Your ideas are really bad."
"The reason large companies are catering to the more diverse, more tolerant, more accepting viewpoints is because that's where the money is."

### AI summary (High error rate! Edit errors on video page)

Addresses a comment on economic power and political leanings in the US.
Mentions a message received about conservative economic power and implications.
Breaks down the GDP share between Republicans and Democrats based on county wins.
Analyzes the economic disparity between Republican and Democratic counties.
Suggests that the perception of conservative economic power is manufactured and not real.
Points out that companies cater to diverse and tolerant viewpoints due to economic reasons.

Actions:

for political analysts, concerned citizens,
Fact-check economic claims and share accurate information (implied)
Support policies that address economic disparities (implied)
</details>
<details>
<summary>
2022-11-07: Let's talk about a future message from your daughter.... (<a href="https://youtube.com/watch?v=mhAE4V2ESzw">watch</a> || <a href="/videos/2022/11/07/Lets_talk_about_a_future_message_from_your_daughter">transcript &amp; editable summary</a>)

Beau receives a heartfelt message about a father's radicalization, death, and the impact of propaganda-driven ideology on family dynamics.

</summary>

"MAGA was a bomb that soothed him."
"I guarantee you, your daughter will write a message to me like that."

### AI summary (High error rate! Edit errors on video page)

Receives a message about a father who was once loving but became radicalized online, switching political affiliations and using hateful speech.
The father's sudden death from a health condition, not drugs, is attributed to propaganda-driven MAGA brainwashing.
The father, living in poverty without health insurance, found solace in identifying with the MAGA ideology.
The message expresses gratitude for Beau's videos, which helped the father reconnect with reality and himself.
MAGA became intertwined with the father's identity and allowed him to look down on others based on visible differences like race.
The daughter credits Beau for preventing arguments and influencing her father positively before his death.
Beau warns those who relate to the story not to let political differences lead to family alienation and regrets, urging them to reconsider their path.

Actions:

for family members, individuals vulnerable to radicalization.,
Reach out to loved ones showing signs of radicalization, provide support, and encourage critical thinking (suggested).
Offer resources for mental health assistance and healthcare access for those in need (suggested).
Facilitate open dialogues and education on the dangers of extremist ideologies within communities (suggested).
</details>
<details>
<summary>
2022-11-07: Let's talk about Russian troops saying no... (<a href="https://youtube.com/watch?v=QRYd_dZzgMk">watch</a> || <a href="/videos/2022/11/07/Lets_talk_about_Russian_troops_saying_no">transcript &amp; editable summary</a>)

Russian soldiers strike over unpaid wages, risking government stability and equipment shortages.

</summary>

"We refuse to participate in the special military operation and will seek justice until we are paid the money promised by our government."
"This is a situation the Russian government is gonna have to trade very lightly with."
"If he continues to double down, not just will there be even more waste in terms of life. It will bring down his regime."

### AI summary (High error rate! Edit errors on video page)

Russian soldiers formed a union and went on strike due to unpaid wages, risking their lives for the government.
The soldiers refuse to participate in a special military operation until they are paid what was promised.
The situation is at the integrated training facility in Ilyanovsk, potentially involving around 100 to 1000 soldiers.
This unrest could cause significant issues for the Russian government, leading to shortages and discontent.
The government's inability to pay mobilization costs and lack of equipment is becoming apparent.
Putin may need to address these issues to avoid further waste of life and potential regime downfall.

Actions:

for activists, human rights advocates,
Support Russian soldiers' rights (exemplified)
Raise awareness about the soldiers' strike (exemplified)
Advocate for fair treatment of conscripted soldiers (exemplified)
</details>
<details>
<summary>
2022-11-06: Let's talk about seeing Trump's name more.... (<a href="https://youtube.com/watch?v=HKyri8bFYO4">watch</a> || <a href="/videos/2022/11/06/Lets_talk_about_seeing_Trump_s_name_more">transcript &amp; editable summary</a>)

Trump's legal battles and potential 2024 announcement set to dominate headlines post-midterms, particularly around November 14th.

</summary>

"Delay, delay, delay."
"Immediately after the elections, expect a lot of news about Trump, particularly around the 14th."
"Most Americans are just at the point where they're like, don't go away, man."

### AI summary (High error rate! Edit errors on video page)

Trump's name likely to appear more in headlines after the midterms due to several converging events.
Committee extended deadline for Trump to turn over documents for subpoena to November 6th.
Trump under subpoena for deposition testimony starting on November 14th.
Trump known for delaying tactics to avoid compliance with subpoenas.
Trump's aides hinting at his announcement to run in 2024 shortly after the midterms, possibly on November 14th.
Department of Justice expected to be very active post-election.
Trump's potential announcement may complicate DOJ activities but not halt them.
Trump likely to use delay tactics to his advantage in legal matters.
Anticipate a surge of news about Trump, especially around November 14th.
Multiple investigations into the former president likely to intensify post-midterms.

Actions:

for political analysts, news followers,
Stay informed about the latest developments in Trump's legal and political activities (suggested).
Engage in critical analysis and discourse regarding the impact of Trump's potential actions on the political landscape (suggested).
</details>
<details>
<summary>
2022-11-06: Let's talk about holding your vote.... (<a href="https://youtube.com/watch?v=bLWAd7BKBo4">watch</a> || <a href="/videos/2022/11/06/Lets_talk_about_holding_your_vote">transcript &amp; editable summary</a>)

Some voters question withholding their vote from the Democratic Party, but active engagement in primaries for candidate alignment is key to real change, as voting alone isn't enough.

</summary>

"Voting is the least effective form of civic engagement."
"Politicians they're not saviors."
"You change society, the laws follow."
"You replace that employee."
"Society changes first, and laws follow."

### AI summary (High error rate! Edit errors on video page)

Some voters are considering withholding their vote for the Democratic Party because they believe it didn't deliver for them in 2020.
Holding your vote might inadvertently benefit the Republican Party, which could be detrimental to your interests.
Voting is just one form of civic engagement among many, and politicians are not saviors.
Radical change cannot be expected from the Democratic Party without actively participating in primaries to push for candidates who truly represent your causes.
Voting alone is not sufficient to shape policy; it requires active engagement in the electoral process to ensure your goals are met.
Frustration with the Democratic Party's performance is understandable, but as a voter, you have the power to set expectations and demand change.
Instead of withholding your vote, the focus should be on actively engaging in the primary process to support candidates who share your beliefs.
Society changes first, and laws follow suit; getting candidates who believe in your values through primaries is key to effecting real change.
The Democratic Party may prioritize political expediency over enacting meaningful change desired by its supporters.
Ultimately, voters have the power to hold politicians accountable and demand better representation by actively participating in the electoral process.

Actions:

for voters,
Get involved in the primary process to support candidates who truly represent your beliefs (implied).
</details>
<details>
<summary>
2022-11-06: Let's talk about Trump settling in.... (<a href="https://youtube.com/watch?v=G1ZP-G_z8SA">watch</a> || <a href="/videos/2022/11/06/Lets_talk_about_Trump_settling_in">transcript &amp; editable summary</a>)

Trump settled a case involving disparaging remarks, showcasing his delaying legal strategy, despite claiming he never settles.

</summary>

"I don't settle cases. I don't do it because that's why I don't get sued very often, because I don't settle, unlike a lot of other people."
"Trump's overall legal strategy in action."
"Delay, delay, delay. It's what Trump does."
"Rich and powerful people can put their name on buildings. But the sidewalks, well those will always belong to the people."

### AI summary (High error rate! Edit errors on video page)

Trump settled a case dating back to 2015 involving disparaging remarks about people from Mexico.
Demonstrators were allegedly assaulted by Trump's private security during a protest.
Michael Cohen reportedly witnessed Trump instructing security to "get rid of the protesters."
A sign carried by demonstrators read, "make America racist again," with claims that it was taken back into the building as a trophy.
Despite Trump's claim of never settling cases, his team settled this one, showcasing his legal strategy.
Trump's delaying tactics in legal cases were evident, with him being deposed in 2020 and settling right as the jury selection began in 2022.
Both sides claimed victory after the settlement.
The lawyer for the demonstrators emphasized that while rich and powerful people can put their names on buildings, sidewalks will always belong to the people.

Actions:

for legal observers,
Contact legal aid organizations for assistance (implied)
Support demonstrators' rights by joining protests or providing aid (implied)
</details>
<details>
<summary>
2022-11-05: Let's talk about voting while left.... (<a href="https://youtube.com/watch?v=BSO1gAPTsvA">watch</a> || <a href="/videos/2022/11/05/Lets_talk_about_voting_while_left">transcript &amp; editable summary</a>)

Beau explains why leftists should vote by delineating the differences in treatment towards marginalized groups by political parties, beyond just economic policies.

</summary>

"The parties aren't the same."
"You have that difference that's pretty remarkable between the two parties."
"But they're not identical."

### AI summary (High error rate! Edit errors on video page)

Addresses the question of why leftists should vote, pointing out comments that question the differences between political parties and their economic policies.
Distinguishes between leftists and liberals on the channel, focusing on economic perspectives.
Illustrates the economic perspective by categorizing both parties as right-wing from a leftist viewpoint, citing lack of support for worker control.
Raises the scenario of attending a place where people wear "vote blue no matter who" shirts and questions whether one feels comfortable leaving a friend alone based on the crowd's political affiliations.
Emphasizes the differences in treatment towards marginalized groups by the political parties.
Encourages reflection on what drove individuals to embrace a leftist ideology, likely rooted in an egalitarian outlook and a desire to alleviate suffering.
Acknowledges the economic similarities between parties but stresses that there are more aspects to the electoral process and having a leftist perspective.
Concludes by leaving the decision to vote or not based on individual considerations and perspectives.

Actions:

for leftist voters,
Have open and honest discussions with friends and peers about the importance of voting and the distinctions between political parties (suggested).
Advocate for marginalized groups and ensure their voices are heard in the electoral process (implied).
</details>
<details>
<summary>
2022-11-05: Let's talk about Trump, NY, and the monitor.... (<a href="https://youtube.com/watch?v=uAfdCL-tU1Y">watch</a> || <a href="/videos/2022/11/05/Lets_talk_about_Trump_NY_and_the_monitor">transcript &amp; editable summary</a>)

Beau provides an update on a legal development in a $250 million lawsuit in New York, where a monitor is appointed due to concerns about asset moves and persistent misrepresentations in Trump's financial statements, potentially leading to the collapse of the Trump business empire.

</summary>

"No further fraud or illegality."
"The attorney general basically said that no amount of lawsuits, delays, anything like that is going to stop them."
"This is one that could cause the Trump business empire to truly collapse."

### AI summary (High error rate! Edit errors on video page)

Providing an update on a legal development in a $250 million lawsuit in New York brought by the attorney general.
Concerns about Team Trump potentially moving assets outside of New York jurisdiction.
Judge appointing a monitor to oversee asset moves and corporate restructuring.
The judge's decision to appoint an independent monitor due to persistent misrepresentations in Trump's financial statements.
The appointment of a monitor aims to prevent further fraud or illegality.
Attorney general's office sees this as a win.
Trump's legal team facing setbacks in the case.
The attorney general is determined to continue the pursuit of justice despite any obstacles.
This case could potentially lead to the collapse of the Trump business empire.
Trump may have to sell properties if the attorney general wins, possibly below their actual value.

Actions:

for legal observers,
Support transparency in financial dealings by public figures (exemplified)
Stay informed about legal developments impacting public figures and potential consequences (exemplified)
</details>
<details>
<summary>
2022-11-05: Let's talk about Dan Crenshaw's moment of truth.... (<a href="https://youtube.com/watch?v=7xw3p1dF-H4">watch</a> || <a href="/videos/2022/11/05/Lets_talk_about_Dan_Crenshaw_s_moment_of_truth">transcript &amp; editable summary</a>)

Beau exposes how Republican leaders knowingly used lies to manipulate supporters and gain power, leaving believers facing consequences.

</summary>

"He's saying behind closed doors, those people that you're supporting, those people that a lot of Republicans are still supporting, out there pushing these narratives today. They're laughing at those people who believe them."
"They admit it was a lie. Just a political tool."
"That it's not some plot spanning the globe, that it's just a group of people using rhetoric to rile others up for their own pursuit of power."

### AI summary (High error rate! Edit errors on video page)

Examines Republican Congressman Dan Crenshaw's statements and their implications within the party.
Crenshaw admitted behind closed doors that the election claims were always a lie.
Those supporting such claims are being laughed at by the politicians they trust.
The tactics of using lies to rile people up are still being employed by political personalities.
People who believed in these lies face serious consequences while the politicians continue their careers.
The Republican Party leadership needs to address this issue seriously.
True believers in the rhetoric need to question the truth behind it.
Beau challenges MAGA supporters to entertain the idea that Crenshaw is telling the truth about the lies.
There is no evidence because it was always a lie to rile people up for power.

Actions:

for republican party members,
Question the truth behind political rhetoric (suggested)
Challenge beliefs and leaders within the party (suggested)
</details>
<details>
<summary>
2022-11-04: The roads to North Dakota with Cara Mund.... (<a href="https://youtube.com/watch?v=spT1ZpNOZoo">watch</a> || <a href="/videos/2022/11/04/The_roads_to_North_Dakota_with_Cara_Mund">transcript &amp; editable summary</a>)

Beau introduces Cara Mund, an independent candidate from North Dakota challenging the Republican incumbent, focusing on her journey, policy differences, campaign challenges, and commitment to representing North Dakotans authentically.

</summary>

"People vote, PACs don't vote."
"The easy road is not always the honorable road."
"Pick a candidate that is going to align with your views because we don't know what's on the horizon."
"For the people that have discouraged me or said there was no way I could possibly do it, they just make me work harder to prove them wrong."

### AI summary (High error rate! Edit errors on video page)

Introducing a candidate from North Dakota, Cara Mund, an independent running for the U.S. House against the Republican incumbent after the Democratic candidate suspended their campaign.
Cara Mund shares her journey from starting a charity fashion show at 14 to becoming Miss America, attending Brown University and Harvard Law School, and working for a Republican senator in Washington, D.C.
Mund explains her decision to split from the Republican Party due to changes in partisanship, restrictive party rules, and differences in policy, especially on women's reproductive rights.
The biggest policy difference between Mund and the Republican incumbent is her pro-choice stance, influenced by concerns about women's healthcare and the recent Dobbs decision.
Mund criticizes the incumbent's voting record, particularly on infrastructure, January 6th committee participation, and women's reproductive health, questioning his allegiance to the party over the people.
Despite being an underdog in a red state, Mund is motivated by the opposition's increased spending, her strategic campaign approach, and the support she receives from individual donors over PAC money.
Encouraging North Dakotans to vote for candidates who genuinely represent them and prioritize making an impact over seeking power or pleasing voters with superficial gestures.
Acknowledging the challenges of running as an independent but staying hopeful and determined to bring change to North Dakota by challenging the status quo.
Planning to focus on legislation related to the Farm Bill, codifying Roe v. Wade, affordable prescription drugs, and capping insulin prices if elected to Congress.
Emphasizing her unique campaign approach, dedication to representing North Dakotans, and determination to prove skeptics wrong by working hard and staying true to her values.

Actions:

for voters in north dakota,
Support Cara Mund's campaign by donating directly to her campaign fund (suggested)
Encourage friends and family in North Dakota to research Cara Mund's platform and voting record before the election (implied)
Ensure North Dakotans are aware of the importance of voting and encourage them to participate in the election (implied)
</details>
<details>
<summary>
2022-11-04: Let's talk about a question on unions.... (<a href="https://youtube.com/watch?v=GYZuf25fprw">watch</a> || <a href="/videos/2022/11/04/Lets_talk_about_a_question_on_unions">transcript &amp; editable summary</a>)

Beau explains why unions are necessary, even for well-paid workers, by shedding light on the power of collective bargaining to secure fair compensation.

</summary>

"Union workers deserve more because they are the ones making the money."
"Divided you beg, united you bargain."
"Support and join unions to get your fair share of the value you produce."

### AI summary (High error rate! Edit errors on video page)

Addressing the question of whether unions are for everybody and why one should support or join them.
Responding to a viewer questioning the necessity of unions when well-paid workers go on strike for more.
Explaining the perception of unions always wanting more and workers being dissatisfied despite being well-paid.
Pointing out that companies always strive to make more profit each year, so why shouldn't workers want a share of that success?
Clarifying that unions help workers negotiate for a fair share of the company's increased profits.
Emphasizing that unions are formed not when conditions are great, but when they need improvement.
Asserting that union workers deserve more because they are the ones generating the wealth for the company.
Advocating for supporting and possibly joining unions to ensure workers receive their fair share of the value they produce.
Mentioning that even the Starbucks union, though new, may eventually follow suit in seeking fair compensation.
Encouraging viewers to understand and back the collective bargaining power of unions to improve workers' lives.

Actions:

for workers, supporters,
Support a union (suggested)
Join a union (suggested)
</details>
<details>
<summary>
2022-11-04: Let's talk about Trump, special counsels, advice, and delays.... (<a href="https://youtube.com/watch?v=XIERuksebe0">watch</a> || <a href="/videos/2022/11/04/Lets_talk_about_Trump_special_counsels_advice_and_delays">transcript &amp; editable summary</a>)

Department of Justice considers special counsel and advisors for Trump cases, preparing for post-election activity, despite concerns about political appearances and unnecessary delays.

</summary>

"Two things: One is they're definitely going after Trump."
"I personally think that's a bad idea."
"The only delay at this point that makes sense to me is waiting for the verdict on the current sedition trial."
"But a delay in putting together a special counsel's office and all of that stuff, it's just going to make the process run longer."
"The normal process is probably what they need to stick to."

### AI summary (High error rate! Edit errors on video page)

Department of Justice considering special counsel for Trump cases and bringing in high-end advisors.
Special counsel may be used to negate political appearances if Trump announces candidacy.
Beau believes using a special counsel could be viewed as political, deviating from the norm.
Bringing in high-end advisors suggests upcoming activity shortly after the election.
Expect a flood of activity, more searches, and potentially indictments of high-profile individuals.
The Department of Justice is closing the circle around Trump, preparing for action after the election.
Unofficial rule of not taking action right before an election to avoid impropriety.
Advisers are there for the possibility of going after Trump through normal processes.
Special counsel is being considered in case Trump declares his 2024 candidacy.
Beau expresses concerns about potential delays and the country's ability to afford them.
Waiting for the verdict on the current sedition trial before proceeding might be wise.
Beau advocates for sticking to the normal process and avoiding unnecessary delays.
The documents case is not seen as complicated by Beau.
Beau suggests that the Department of Justice needs to focus on the normal process with the advisers they have.

Actions:

for legal analysts, political commentators,
Wait for the verdict on the current sedition trial before proceeding (suggested)
Stick to the normal process with the advisers in place (implied)
</details>
<details>
<summary>
2022-11-04: Let's talk about Musk, Twitter, and being brand safe.... (<a href="https://youtube.com/watch?v=byazeV3gfis">watch</a> || <a href="/videos/2022/11/04/Lets_talk_about_Musk_Twitter_and_being_brand_safe">transcript &amp; editable summary</a>)

Elon Musk's actions with Twitter reveal the critical importance of brand safety and advertiser perception in the online space, impacting platforms' success and growth.

</summary>

"When you are on social media, you are the product."
"If Musk wants advertisers to stay on Twitter, Twitter has to be brand safe."
"The reason that all of the right-wing social media networks that start don't really go anywhere is because nobody wants to advertise on them."
"Understand when you are on social media, you're the product."
"Advertisers are trying to reach you."

### AI summary (High error rate! Edit errors on video page)

Elon Musk's recent purchase of Twitter led to various problems, including advertisers pausing advertisements.
Musk claimed activists were pressuring advertisers, causing a drop in revenue for Twitter, even though content moderation remained the same.
Advertisers are concerned about the context in which their ads appear online, as it can impact their brand image.
Advertisers avoid advertising on platforms with hateful or controversial content to prevent being associated with it inadvertently.
Musk's marketing approach should prioritize brand perception, as any negative associations with Twitter could impact advertisers' decisions.
Advertisers follow trends and demographics, shifting towards more diverse and tolerant audiences to grow their brand successfully.
Large companies tailor their advertising messages to different demographics, including those related to orientation, identity, and race.
Musk's subscription-based Twitter model relies on maintaining a brand-safe environment to attract large advertisers.
Right-wing social media networks struggle to attract advertisers due to bigoted content, limiting their growth and improvements.
Musk's understanding of branding and the importance of brand safety for advertisers is critical for the success of platforms like Twitter.

Actions:

for social media users,
Support diverse and tolerant content on social media platforms by engaging with and sharing positive messages (implied).
Encourage brands to prioritize brand safety and inclusivity in their advertising strategies (implied).
</details>
<details>
<summary>
2022-11-03: Let's talk about a voting PSA.... (<a href="https://youtube.com/watch?v=kTZJPdtmuxQ">watch</a> || <a href="/videos/2022/11/03/Lets_talk_about_a_voting_PSA">transcript &amp; editable summary</a>)

Beau provides a public service announcement: Verify voting locations, as misleading text messages could lead to voter suppression, impacting the voice of unlikely voters critical in this election.

</summary>

"Don't trust the text messages."
"Make sure you are checking with real sources about where you're supposed to go vote if you plan to."
"It's the unlikely voters who are going to rule this election."
"Make sure you know where to go, you have a plan to get there, and all of that stuff."
"There are certainly going to be malicious actors trying to suppress the vote, and this is a good way to do it."

### AI summary (High error rate! Edit errors on video page)

Providing a public service announcement regarding misleading text messages about voting locations.
Urging people to fact-check information received via text messages with real sources.
Warning about a company sending inaccurate voting information in Kansas, New Jersey, Illinois, North Carolina, and Virginia.
Mentioning NBC's report about a similar issue in Oregon with the same company.
Advising recipients of unsolicited texts to block the number, potentially missing correction texts.
Emphasizing the importance of verifying voting locations to avoid unintentional or intentional voter suppression.
Noting that this election's outcome relies on unlikely voters, making informed choices based on accurate information critical.
Encouraging individuals, especially those who don't normally vote, to ensure their voting information is correct and have a plan to cast their vote.
Pointing out the potential impact of misinformation via text messages on voters who are less familiar with the voting process.
Stressing the significance of taking control of one's voting information to prevent malicious actors from suppressing votes.

Actions:

for voters,
Verify your voting location with official sources (suggested).
Block unsolicited numbers sending voting information and seek accurate sources (suggested).
Ensure you have a plan to cast your vote and know where to go (suggested).
</details>
<details>
<summary>
2022-11-03: Let's talk about a message for this week.... (<a href="https://youtube.com/watch?v=J0wkWzR8Suw">watch</a> || <a href="/videos/2022/11/03/Lets_talk_about_a_message_for_this_week">transcript &amp; editable summary</a>)

Beau stresses the urgent need to reject violent rhetoric as a path to power and confront the threat to democracy before it impacts everyone.

</summary>

"Anywhere is a threat to freedom everywhere."
"This is freedom and tyranny."
"Those people should not get your vote."

### AI summary (High error rate! Edit errors on video page)

Urges the audience to understand a critical message within a week due to current events.
Addresses political violence and the quest for power through symbolic targets and candidates.
Emphasizes the importance of not allowing violent rhetoric to be a path to power.
Warns that those currently unaffected will eventually become targets.
Stresses the impact of authoritarianism on free enterprise and small businesses.
Condemns those who deny election results and use violent rhetoric for perpetuating dangerous ideologies.
Expresses the failure to send a strong enough message in the 2020 election.
Calls for a rejection of election deniers and promoters of violent rhetoric to protect freedom.

Actions:

for every concerned citizen,
Reject election deniers and promoters of violent rhetoric (exemplified)
Educate others on the dangers of authoritarianism (exemplified)
Advocate for democracy and freedom in your community (suggested)
</details>
<details>
<summary>
2022-11-03: Let's talk about B52s and 3 Australian questions.... (<a href="https://youtube.com/watch?v=lckEg2tkXl4">watch</a> || <a href="/videos/2022/11/03/Lets_talk_about_B52s_and_3_Australian_questions">transcript &amp; editable summary</a>)

The US deploying B-52s to Australia aims to deter China, showcasing Australia's longstanding alliance with the US in an international strategic move.

</summary>

"Australia is one of the most steadfast allies the United States has."
"It's the international poker game where everybody's cheating."
"We're a nuclear power. We're very comfortable with this."

### AI summary (High error rate! Edit errors on video page)

The US plans to deploy six B-52s to Australia, which are part of the US nuclear arsenal and will be stationed there with an operations center.
This move is aimed at deterring China from challenging Australian interests in the region and potentially influencing a future conflict involving Taiwan.
The deployment of these aircraft is not an indication of imminent war, as the most immediate projections suggest any conflict with China is still a couple of years away.
Australia is not financially responsible for this deployment; the US has devoted funds to upgrading Australian defense capabilities, including developing the necessary infrastructure for the aircraft.
While Australia is perceived as a US ally, the presence of these aircraft serves as a form of deterrence and mutual assured destruction rather than making Australia a new target.
Australia's long history of military alliances and support for the US has made it a reliable ally, and Chinese war planners are aware of the implications of any conflict involving Australia.
The symbolic presence of these aircraft in Australia provides a counterweight to Chinese nuclear power and acts as a strong deterrent against any interference with Australian interests.
The overall strategy involving these aircraft can be likened to an international poker game with strategic moves and deterrence tactics.
The US is comfortable with strategic weapons in other countries, being a nuclear power itself, but not all nations view such deployments favorably.
While the B-52s may not initially be equipped with nuclear weapons, their capability to carry them raises concerns for some observers.

Actions:

for international relations analysts,
Monitor diplomatic relations and escalations in the Asia-Pacific region (implied)
Stay informed about international defense alliances and strategies (implied)
</details>
<details>
<summary>
2022-11-02: Let's talk about a diesel shortage.... (<a href="https://youtube.com/watch?v=8PgJ8KdOpKE">watch</a> || <a href="/videos/2022/11/02/Lets_talk_about_a_diesel_shortage">transcript &amp; editable summary</a>)

There is a diesel shortage leading to price hikes, advocating for true energy independence through renewable sources, not fossil fuels manipulation.

</summary>

"Energy independence is renewable. It is clean. That's what energy independence is."
"Those slogans that you're hearing from people who roll coal, it's short-sighted."
"Energy independence is clean. And it's renewable. That's the solution."

### AI summary (High error rate! Edit errors on video page)

There is a diesel shortage, but not as catastrophic as portrayed, leading to increased prices due to seasonal factors compounded by global oil market disruptions caused by the war in Ukraine.
Diesel prices rising will impact the cost of other goods as transportation costs increase.
The solution lies in energy independence, which has been misconstrued by nationalists as solely producing oil and gas domestically, rather than embracing renewable and clean energy sources.
Energy independence involves transitioning to renewables like solar, wind, and electric vehicles, which are already feasible technologies.
Building infrastructure for renewable energy is key to achieving true energy independence and reducing dependence on manipulated finite resources.
Transitioning to clean energy is vital for economic and environmental sustainability, contrary to short-sighted slogans advocating for continued reliance on fossil fuels.

Actions:

for energy-conscious citizens,
Transition to renewable energy sources like solar and wind power by investing in solar panels for homes or supporting wind energy initiatives (exemplified)
Advocate for the adoption of electric vehicles and support companies that are already using electric trucks for transportation (exemplified)
Educate others on the importance of clean energy and dispel misconceptions around the feasibility of electric-powered vehicles (suggested)
</details>
<details>
<summary>
2022-11-02: Let's talk about US aid and reading more than the headline.... (<a href="https://youtube.com/watch?v=YIiubiKcAA4">watch</a> || <a href="/videos/2022/11/02/Lets_talk_about_US_aid_and_reading_more_than_the_headline">transcript &amp; editable summary</a>)

Beau clarifies the US move to pre-position gravity bombs in Europe as part of modernizing the strategic arsenal, not an escalation.

</summary>

"This isn't more. It's new ones coming in to replace retired ones. So it's not an escalation. It's not a provocation."
"The US still does, in fact, own the skies and does, in fact, have just warehouses full of PGMs at its disposal."

### AI summary (High error rate! Edit errors on video page)

The United States is pre-positioning specific gravity bombs in Europe, but this move is unrelated to Ukraine and has been planned for years.
The gravity bombs being sent are not considered precision weapons by US standards, but they are guided and can hit targets within about 30 meters.
The specific type of gravity bomb being sent is the B61-12, which is a dial-a-yield nuclear weapon that can produce blasts of different sizes.
This modernization of America's strategic arsenal has been in progress since around 2012-2014, with the current shipment scheduled to Europe since 2018.
The purpose of sending these new gravity bombs to Europe is part of the process of replacing retired weapons and modernizing the US arsenal, not an escalation or provocation.
The move to send these gravity bombs may have been slightly accelerated, possibly as a subtle message or due to readiness, but it isn't a response to a lack of precision weapons.
The US still possesses precision guided munitions (PGMs) and dominates the skies with warehouses full of such weapons.

Actions:

for foreign policy analysts,
Monitor and analyze developments in US strategic arsenal modernization (implied)
</details>
<details>
<summary>
2022-11-02: Let's talk about Twitter and check marks.... (<a href="https://youtube.com/watch?v=_zARivqLfDQ">watch</a> || <a href="/videos/2022/11/02/Lets_talk_about_Twitter_and_check_marks">transcript &amp; editable summary</a>)

Beau explains Twitter's verification system, Musk's subscription fee proposal, and the potential impact on content creators and platform longevity.

</summary>

"Being verified provides a few benefits. The one that people are after most is just clout."
"Musk apparently wants to change this to a subscription fee so people can pay for the privilege of generating him ad revenue."
"If he goes through with this, I think that this will lead to a loss of relevancy for Twitter over the long haul."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of verified accounts on Twitter and the benefits they provide.
Elon Musk reportedly plans to charge $20 a month for Twitter verification.
Beau personally finds the verified notifications useful for keeping track of large accounts and interactions.
Beau considers the $240 yearly fee for verification worth it for research purposes but acknowledges it may not be for everyone.
Musk's plan to change Twitter's revenue generation to a subscription fee raises concerns about the platform's longevity and appeal to new content creators.
Beau believes newer content creators may opt for different platforms if Twitter's business model changes.
He expresses skepticism about Musk's plan and its potential negative impact on Twitter's relevancy.
Beau mentions considering other social media platforms due to the proposed changes on Twitter.
He questions the effectiveness of Musk's subscription-based approach in the long term.

Actions:

for content creators,
Advocate for platforms that support content creators and offer fair compensation (implied).
Support and migrate to social media platforms that prioritize content creators' interests and longevity (implied).
</details>
<details>
<summary>
2022-11-01: Let's talk about the GOP expecting Trump's indictment.... (<a href="https://youtube.com/watch?v=SMbBvOIs8dg">watch</a> || <a href="/videos/2022/11/01/Lets_talk_about_the_GOP_expecting_Trump_s_indictment">transcript &amp; editable summary</a>)

Republican insiders anticipate Donald Trump's post-election indictment, raising questions about support and potential political ramifications, urging preparation for possible societal divides.

</summary>

"Republican insiders expect Donald Trump to be indicted shortly after the election."
"Support may dwindle as evidence emerges."
"Anticipation of Trump's indictment is significant."

### AI summary (High error rate! Edit errors on video page)

Republican insiders and strategists expect Donald Trump to be indicted shortly after the election, within a timeframe of a couple of weeks to 90 days.
They believe the documents case against Trump is strong, and Attorney General Garland will need to act swiftly post-election to avoid criticism of influencing a potential presidential run.
Some anticipate that Trump's indictment could galvanize support behind him, but Beau questions this, suggesting that support may dwindle as evidence emerges.
Beau predicts that Republicans who have been pushed around by Trump may use his indictment as an opportunity to settle scores.
He doubts that Trump's allies within the Republican Party will stick by him, speculating that they might leverage his indictment for their own political advantage.
Beau suggests that Trump's indictment could benefit some Republicans eyeing the 2024 nomination, potentially leading to limited support for Trump within the Republican Establishment.
There is speculation within the Justice Department about handling the documents case and January 6th related issues together to avoid multiple federal indictments against the former president.
Beau notes that Attorney General Garland is unpredictable in his moves, hinting that there may not be a definitive timeline for Trump's potential indictment.
The anticipation of Trump's indictment within the Republican Party is seen as significant, prompting the need for the United States to prepare for potential repercussions and divisions among citizens.
Beau points out that while an indictment may prompt some to re-evaluate their support for Trump, it could also reinforce the belief among others that there is a conspiracy against him.

Actions:

for political analysts,
Prepare for potential societal divides and heightened tensions post-election (implied).
Stay informed about developments related to potential indictments and political implications (implied).
</details>
<details>
<summary>
2022-11-01: Let's talk about an update on the Pelosi situation.... (<a href="https://youtube.com/watch?v=e7OLFRhH318">watch</a> || <a href="/videos/2022/11/01/Lets_talk_about_an_update_on_the_Pelosi_situation">transcript &amp; editable summary</a>)

Beau sheds light on the suspect's political motivations behind targeting Pelosi and the potential for federal charges, urging a deeper understanding of the situation beyond false narratives and posing questions.

</summary>

"I'd like to point everybody back to that academic definition."
"This type of activity, it tends to escalate over time."
"No, a lot of them were making assertions, that's not asking questions."

### AI summary (High error rate! Edit errors on video page)

Providing an update on the incident involving Pelosi in California, shedding light on the suspect's motivations.
The suspect expressed a desire to break Pelosi's knees to show consequences for her actions and viewed her as a leader of Democratic lies.
DOJ released information indicating the suspect's political motivations and the potential for federal charges due to the severity of the statements.
Beau references the academic and legal definitions of domestic terrorism in relation to the suspect's actions.
Suggests that the suspect talking could lead to further revelations about who influenced his beliefs regarding Pelosi.
Calls out individuals spreading false narratives and posing as merely asking questions while making assertions.

Actions:

for concerned citizens,
Contact DOJ for updates on the case (suggested)
Stay informed about the developments in the investigation (implied)
</details>
<details>
<summary>
2022-11-01: Let's talk about Ukrainian grain and the Russian Navy.... (<a href="https://youtube.com/watch?v=_epuZEQx-Hg">watch</a> || <a href="/videos/2022/11/01/Lets_talk_about_Ukrainian_grain_and_the_Russian_Navy">transcript &amp; editable summary</a>)

Russia cuts off Ukrainian grain to Africa in response to successful Ukrainian naval drone attack, illustrating imperialistic tendencies and risking international backlash.

</summary>

"This war is lost. Russia lost."
"They haven't really got to the occupation part of it yet."
"Russia is protecting the grain shipments from themselves."
"A lot of them were in countries that bordered areas that desperately need this food."
"Russia is now going to allow people in the global south to starve so they can annex territory."

### AI summary (High error rate! Edit errors on video page)

Russia is cutting off Ukrainian grain shipments to Africa as a consequence of Ukraine's successful use of unmanned naval drones against the Russian Navy.
The Russian government decided to withdraw from a deal allowing Ukrainian grain to reach countries in Africa, which are in desperate need of food.
Ukraine's military operation against the Russian Navy, using naval drones, was described as a humiliating failure for Russia.
Although the military effectiveness is debatable, the attack showcased a proof of concept, leading to a rewriting of naval textbooks.
Russia claims to have repelled the attack, but Beau expresses skepticism regarding their statements.
Russia's decision to block grain shipments to Africa can be seen as an act of imperialism to punish Ukraine for their military operation.
Russia justifies its actions by claiming to protect the Ukrainian grain shipments, although it seems like they are actually protecting them from themselves.
This move by Russia is aimed at gaining leverage on the international stage and punishing Ukraine by causing food shortages in Africa.
The conflict between Russia and Ukraine is still in the invasion phase, with the occupation phase yet to come.
The technology used by Ukraine against the Russian fleet could potentially be accessed by non-state actors like a Ukrainian resistance movement.

Actions:

for global citizens,
Contact organizations providing aid in Africa to support areas affected by the cut-off grain shipments (exemplified)
Join or support Ukrainian humanitarian efforts to mitigate the impact of Russia's actions (exemplified)
</details>
<details>
<summary>
2022-11-01: Let's talk about US personnel in Ukraine.... (<a href="https://youtube.com/watch?v=dJ3ZUjw0wjw">watch</a> || <a href="/videos/2022/11/01/Lets_talk_about_US_personnel_in_Ukraine">transcript &amp; editable summary</a>)

The US is enhancing aid to Ukraine to address Russian concerns about weapons ending up in illicit markets, ensuring oversight and proper use.

</summary>

"Russia literally asked for this. They wanted this oversight. They wanted Americans who would be willing to travel to Ukraine to keep track of them."
"The best way to make sure it doesn't end up on the black market is to make sure that it's used properly."
"This probably wasn't the smartest outrage to manufacture by the Russian government."

### AI summary (High error rate! Edit errors on video page)

The United States is working on improving the aid provided to Ukraine amidst concerns from the Russian government about weapons ending up in illicit markets.
Russia expressed genuine concerns about weapons provided by the US potentially making their way off the battlefield and into underground markets.
In response, the US plans to have weapons inspectors in Ukraine to keep records and ensure proper use of military aid.
US personnel will oversee Ukrainian training to prevent misuse of the weapons.
The plan aims to address Russian concerns and prevent aid from ending up on the black market.
Russia actually requested this oversight to ensure better record-keeping and prevent weapons from being misused.
Proper use of the aid is seen as key to preventing it from reaching illicit markets.
The Russian government's outrage over this oversight may not have been the wisest move.
The US is taking steps to be a better steward of military aid to Ukraine and ensure it is used appropriately.
The goal is to avoid weapons provided for Ukraine ending up in unauthorized hands.

Actions:

for foreign policy analysts,
Ensure proper oversight and record-keeping of aid (exemplified)
Monitor training and usage of military aid to prevent misuse (exemplified)
</details>
