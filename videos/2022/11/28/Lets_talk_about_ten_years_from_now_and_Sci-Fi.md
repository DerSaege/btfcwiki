---
title: Let's talk about ten years from now and Sci-Fi....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eLj16xvYp3o) |
| Published | 2022/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about a NASA official's statement regarding people living on the moon in this decade.
- The official from the Orion Lunar Space Program mentioned habitats and rovers on the moon's surface for extended durations.
- Technological advances are propelling us towards what was previously considered science fiction.
- The expectation is that by 2030, or by 2032 at the latest, people will be living on the moon.
- This progress signifies the first step towards real space exploration, expanding beyond current limitations.
- Beau expresses his surprise at the idea of people living on the moon and how it will impact technological advancements on Earth.
- The increased space exploration will likely lead to more innovations benefiting daily life.
- There is a belief in a forthcoming new era of exploration with vast possibilities beyond current understanding.
- Beau concludes by saying this marks a significant moment in space exploration.
- The focus is on the rapid rate of technological advancements guiding us toward what was once deemed science fiction.

### Quotes

- "Certainly in this decade, we are going to have people living for durations… They will have habitats. They will have rovers on the ground."
- "To me, that is just wild."
- "We're kind of on the verge of a new era when it comes to this sort of exploration."

### Oneliner

A NASA official predicts people will live on the moon within this decade, marking a significant leap in space exploration and technological progress.

### Audience

Space enthusiasts, scientists

### On-the-ground actions from transcript

- Stay updated on space exploration developments and research (suggested)
- Support initiatives that advance space exploration and technology (implied)
- Encourage STEM education and interest in space sciences (implied)

### Whats missing in summary

Importance of supporting scientific advancements and space exploration for future innovation and discovery.

### Tags

#SpaceExploration #NASA #MoonLiving #TechnologicalAdvancements #STEM


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about the moon
and a statement from a NASA official that I think
might surprise most people.
Recently, I did that video talking
about Artemis and the launch.
And one of the comments that really caught my eye
was somebody saying something to the effect of, wow,
Bo really nerds out when he's talking about science fiction.
And that's true.
I do.
But this isn't science fiction.
It's just science.
It's fact.
It's happening.
So the person over the Orion Lunar Space Program
gave an interview.
He was talking with somebody over on BBC.
And they said this, certainly in this decade,
we are going to have people living for durations,
depending on how long we will be on the surface.
They will have habitats.
They will have rovers on the ground.
Goes on to say, we are going to be sending people
down to the surface, and they are
going to be living on that surface and doing science.
That surface is going to be the moon.
That surface is the moon.
He's talking about the moon.
In this decade.
Now, I don't know if that means this decade meaning 10 years
or by 2030.
But either way, 2030 or 2032, I mean,
I did not really anticipate saying
that people were living on the moon,
that they were staying on the moon, that they had habitats,
that this was an ongoing thing, and people were staying there
for extended durations.
But that's where we're at.
We focus on technological advances all the time.
Every time a new phone comes out, there's a line.
Those technological advances are building
towards science fiction, what we consider
to be science fiction, at a just quickening rate all the time.
There is an official with NASA who's
over the program that would do it,
who has given an interview expressing
their full expectation that within this decade,
there will be people living on the moon.
To me, that is just wild.
It's the first step towards real space exploration,
getting out there way beyond what I think
most people are picturing.
We have a good idea of all of the limitations
and how far we can really go.
But there's probably a lot more room for exploration
within our current capabilities than people might realize.
We're kind of on the verge of a new era
when it comes to this sort of exploration.
And I'm of the firm belief that as that exploration speeds up
and we start doing it more and more,
it will create more technological advances
that we end up living with every day here.
Sorry to nerd out again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}