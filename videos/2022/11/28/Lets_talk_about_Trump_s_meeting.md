---
title: Let's talk about Trump's meeting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DgnJRtWHpuA) |
| Published | 2022/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's meeting with controversial figures, Kanye West and Nick Fuentes, made headlines.
- Fuentes, openly calling for dictatorship, was part of the meeting that Trump framed as accidental.
- Trump's framing of the meeting involved downplaying his knowledge of meeting Fuentes.
- Beau questions the coincidence of Trump accidentally meeting figures like Fuentes but not civil rights activists.
- Beau points out that the meeting wasn't surprising or shocking, given Trump's previous actions and statements.
- The media coverage of the meeting described the figures as controversial rather than explicitly labeling them as fascist or racist.
- Beau argues that it's time to stop pretending to be surprised by Trump's actions and accept who he is.
- He stresses the need to acknowledge the message Trump consistently puts out and his awareness of his followers.
- Beau concludes by urging people to stop acting surprised by Trump's behavior and to recognize the reality of the situation.

### Quotes

- "Trump is who he is, and he's telling everybody, but none dare call it fascism."
- "I think the sooner we stop acting like this is surprising, that this is shocking, the better."
- "He's aware of the message he puts out. He's aware of who his followers are, even when he accidentally meets them."

### Oneliner

Former President Trump's meeting with controversial figures, framed as accidental, prompts Beau to urge acceptance of Trump's consistent messaging and followers' awareness.

### Audience

Observers and critics

### On-the-ground actions from transcript

- Acknowledge and accept the consistent messaging and actions of political figures (implied)
- Stop pretending to be surprised by the behavior of public figures (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the media coverage and public reaction to Former President Trump's meeting, urging a shift from surprise to acceptance in understanding his actions.

### Tags

#Trump #Meeting #Controversy #Acceptance #MediaCoverage


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about former president
Donald J. Trump's meeting, the one that has been
in headlines all over the holidays.
We're gonna briefly go over the meeting,
we'll talk about Trump's framing of it,
and then we'll talk about why, despite repeated requests,
I didn't make a video about it over the weekend.
Okay, so, if you don't know what happened, have no idea what I'm talking about because
you were busy with your family or whatever, the former president of the United States
had a meeting with some controversial figures.
How the media is pretty much covering it.
He met with Kanye West, who I'm fairly certain most people watching this channel understand
what's going on there, and a man named Nick Fuentes, who I'm not going to go
through his bio, just hit Wikipedia. Suffice it to say that most people
watching this channel would call him a racist fascist. And to be honest, I mean
I'm sure that there are terms that he would like used instead, but I don't
think he would actually be offended by that.
He is very open about who he is.
He doesn't attempt to hide it.
He is a person who openly calls for dictatorship.
Okay, so the meeting took place.
The word went out that Trump was, I believe, quote, impressed with Fuentes, and the backlash
started the normal cycle. Trump put out this little statement. So I helped a
seriously troubled man who just happens to be black, Kanye West, who has been
decimated in his business and virtually everything else and who has always been
good to me by allowing his request for a meeting at Mar-a-Lago alone so that I can
give him very much needed advice. Advice is in quotations for some reason. He
shows up with three people, two of which I didn't know. The other a political
person who I haven't seen in years. I told him don't run for office. A total
waste of time. Can't win. Fake news. Went crazy. I mean big surprise. Trump didn't
them. I'm willing to bet, you know, Fuentes only got coffee, right? So that's Trump's
framing of it. It was an accident. He didn't know he was going to meet Fuentes. Okay. I
would ask, can anybody remember Trump accidentally meeting, I don't know, Lee Merritt, somebody
Anybody who fights for civil rights?
Nah.
Or is it just one type of person that he accidentally runs into?
Some might call that an indicator.
I didn't rush out to make a video about this for a very simple reason.
It's not news.
It's not.
treated it as breaking news, but is it? I mean, are you surprised by this
development? Is this shocking to you? Or is this something that you're like, okay,
yeah, a guy who certainly appears to have attempted a self-coup hung out with a
dude who openly calls for dictatorship. Yeah, that kind of tracks. The coverage
of it as they attempted to garner outrage was very lukewarm. Controversial
figures. It is how it was framed. Most places didn't really go into how
controversial. They didn't call them a fascist. They didn't call them a racist.
Just a controversial figure. Far right. It's not surprising. It's Trump. It's
mask off, but it's not surprising. It's not news. He's telling everybody who he is
And he's been telling everybody. Believe him. I think the sooner we stop acting
like this is surprising, that this is shocking, the better. We can just accept
what we're dealing with. I don't believe if we all gasp hard enough, he'll
suddenly realize the errors of his ways.
He's aware of the message he puts out.
He's aware of who his followers are, even when he accidentally meets them.
It's time to stop pretending that this is surprising.
It's not.
Trump is who he is, and he's telling everybody, but none dare call it fascism.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}