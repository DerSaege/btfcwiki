---
title: Let's talk about Herschel Walker's statement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GzizGsTvRjY) |
| Published | 2022/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ignored Herschel Walker's campaign due to lack of policy statements, resembling stand-up comedy routines.
- Walker expressed the belief that young people born after 1990 haven't earned the right to change the country.
- Beau disagrees with Walker's sentiment, stating that people don't die for the national anthem but for those around them.
- Beau mentions friends with prosthetics, a diverse group that got their injuries in their early 20s.
- American KIA are typically young, and statistics show the average age of individuals in wars is not in the age group Walker mentioned.
- Beau argues that the younger generation has every right to try to change the country as they will bear the consequences if it remains unchanged.
- He suggests that the younger generation, having grown up with unlimited access to information, may have different ideas because they are better informed.
- Beau points out that people who grew up with the internet have the right to exercise their voting rights.
- He questions the impact of Walker's statement on the election, particularly in a state like Georgia with numerous military installations.
- Beau implies that Walker's views may not resonate well in areas like Columbus, which have a keen interest in building a better future rather than clinging to an idealized past.

### Quotes

- "Nobody has ever died for the national anthem. They died for the people around them."
- "The younger crop, they have every right to try to change this country, because they're going to be the ones who pay the price if it doesn't change."
- "Maybe they have different ideas because they're better informed."

### Oneliner

Beau challenges Walker's belief that young people haven't earned the right to change the country, advocating for their voices and perspectives based on informed decisions and future implications.

### Audience

Voters, Young Activists

### On-the-ground actions from transcript

- Challenge outdated beliefs and encourage informed decision-making (implied)
- Support and empower young voices in shaping the future of the country (implied)

### Whats missing in summary

The full transcript provides additional context on Herschel Walker's statements and Beau's insightful analysis on the younger generation's right to create change based on informed decision-making.

### Tags

#YouthEmpowerment #VotingRights #InformativeActivism #ChallengingBeliefs #FutureLeaders


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Herschel Walker.
You know, for the most part, I have kind of ignored his campaign.
His statements, they haven't been policy statements.
He's basically kind of been doing stand-up comedy routines as he goes around.
So there wasn't much to talk about.
However, he said something that definitely found its way under my skin.
So we're going to kind of go through a concept real quick.
He was doing an interview, and he was asked what he thought about those young people,
specifically defined as those born after 1990.
You know, the people who had grown up with the computers and the internet, those who
want to change America.
And he said that, he answered this question by saying that we haven't done a good enough
job of explaining to our kids that, well, they haven't earned the right to change this
country because there are people who had died, who gave up their life for the flag, the national
anthem, and freedom.
Now I have other videos with commentary on that sentiment.
Short version, I'm of the belief that nobody has ever died for the national anthem.
They died for the people around them.
They're friends.
That's why they died.
And incidentally, I have a few friends who have prosthetics.
They don't all know each other, but if you were to bring them all together, it would
look like one of those almost impossibly diverse friend groups from a TV show, you know, where
they try to represent everybody.
That's what it would look like.
They don't have much in common, except for the fact that they all got those prosthetics
when they were in their early 20s.
When you picture an American KIA, do they have gray hair?
They don't, right?
They're young.
They are young.
In fact, while I have never seen a statistic that covers all of America's wars, I've seen
the average age for the individual wars.
And there's not one that is, I guess, in the age group that has earned it.
There's a whole lot of people who will always be 19 or 24.
The younger crop, they have every right to try to change this country, because they're
going to be the ones who pay the price if it doesn't change.
They might be at the whim of somebody who would invoke the actions of people of their
age group while saying that their interests don't deserve to be represented.
And incidentally, if you're ever going to ask a question like this, I would point out
another way to say those who grew up with the Internet would be those who grew up with
an almost unlimited access to information.
Maybe they want to change things.
Maybe they have different ideas because they're better informed.
They have absolutely every right to exercise their right to vote.
Now as far as how this is going to impact the election, I'm not sure I would have made
this statement if I was a politician in a state with as many military installations
as Georgia.
I'm fairly certain Herschel Walker is not going to pull the area around Columbus now.
They have a very vested interest in creating a better future instead of trying to hang
on to some idealized version of the past that never existed anyway.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}