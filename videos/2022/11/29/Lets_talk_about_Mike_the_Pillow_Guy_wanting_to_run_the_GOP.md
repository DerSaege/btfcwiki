---
title: Let's talk about Mike the Pillow Guy wanting to run the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-ZlQ1MLW5d8) |
| Published | 2022/11/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mike Lindell, known as the "pillow guy," is vying to become the new chair of the Republican Party, a position that requires knowledge and wisdom.
- Lindell has personally reached out to all 168 voting members to address their concerns, with some interactions lasting up to four hours.
- Despite incumbent McDaniel likely having the votes to retain the position, there's uncertainty about Lindell's acceptance of the results.
- Lindell has a history of spreading false election information and facing FBI scrutiny over his phone.
- The GOP's tolerance of baseless claims and lack of forceful correction has allowed for potential leadership shifts like Lindell's bid.
- The party's failure to confront attempts to undermine democratic institutions could lead to voter suppression within their own base.
- The GOP's reluctance to challenge misinformation could result in disillusionment among voters and decreased participation in elections.

### Quotes

- "If knowledge is knowing that Frankenstein wasn't the monster but was the doctor, wisdom is knowing that the doctor was the monster."
- "They will still have problems with their primaries. They will still have people refusing to accept very clear results because they haven't come down on this forcefully."
- "The Republican Party has created a situation in which they will end up suppressing their own vote."

### Oneliner

Mike Lindell's bid for the Republican Party chair spot underscores the GOP's struggle with misinformation and potential voter suppression within their base.

### Audience

Voters, Party Members

### On-the-ground actions from transcript

- Contact local Republican officials to express concerns about leadership decisions (suggested)
- Educate fellow voters on the importance of challenging misinformation within political parties (implied)

### Whats missing in summary

The full transcript provides a deeper analysis of the potential consequences of the GOP's failure to address misinformation and its impact on voter participation.


## Transcript
Well howdy there internet people, it's Bill again.
So today we are going to talk about the selection of the new chair of the Republican Party.
An interesting figure has decided to throw their hat into the ring.
The pillow guy.
The pillow guy.
Mike Lindell has said that he will be attempting to get the votes to be the new chair of the
Republican Party.
A person who will be making all the decisions for the Republican Party.
This is a position that requires a lot of knowledge and a lot of wisdom.
Often times, knowledge gets summed up as knowing that Frankenstein wasn't the monster, he was
the doctor.
Now Lindell has set out to do his due diligence and reach out personally to all 168 voting
members in an attempt to address their concerns, which he does admit that some of them have.
And he has pointed out that one of the conversations took four hours.
Now realistically, I'm fairly certain that McDaniel, the incumbent, actually already
has the votes to keep the position.
But you never know.
But I think a more important question is whether or not Lindell is going to accept the results.
For anybody who doesn't know who we're talking about, this is the same Mike Lindell who had
the FBI want to take a look at his phone and constantly spread false information about
the elections and always promised to have the evidence of the malfeasance two weeks
from Thursday or whatever for the last two years.
This is a person who, while unlikely, could become the next chair of the GOP.
If knowledge is knowing that Frankenstein wasn't the monster but was the doctor, wisdom
is knowing that the doctor was the monster.
And that's the situation the GOP is in.
They have allowed all of these bogus claims to circulate.
They have leaned into them when they thought it would be useful.
They didn't correct them.
They didn't come down forcefully as elements within the Republican Party attempted to subvert
basic democratic institutions within the United States.
Because of that, a lot of people have found their home in the Republican Party.
And again, while it's unlikely, it's not impossible that Lindell becomes the chair.
More importantly, this shows that this isn't over.
They will still have problems with their primaries.
They will still have people refusing to accept very clear results because they haven't came
down on this forcefully.
They haven't pushed back enough.
As this process continues and more and more of their base becomes disillusioned with the
electoral process, they're going to stop voting.
The Republican Party has created a situation in which they will end up suppressing their
own vote.
And it's all because they lack the courage to stand up to a tweet.
This will probably become a pretty well-covered story.
Again, I stress, it is incredibly unlikely that Lindell gets this slot.
But it isn't impossible.
Meet your monster.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}