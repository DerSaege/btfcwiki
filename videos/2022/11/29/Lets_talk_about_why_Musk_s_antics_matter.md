---
title: Let's talk about why Musk's antics matter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FNS3l8yvvrE) |
| Published | 2022/11/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elon Musk made controversial statements about Apple on Twitter, impacting their relationship.
- Musk criticized Apple's advertising on Twitter and their app store standards publicly.
- He hinted at starting his own phone company if Twitter is removed from app stores.
- Musk's actions could lead to higher phone prices due to supply issues and competition.
- Musk's wealth and influence allow him to shape markets and impact national discourse.
- His pursuit of platforming controversial voices may not be well-received.
- Musk's decisions can have ripple effects on job markets and income inequality.
- Concentration of wealth in few hands can lead to disproportionate influence over the world.
- Beau questions the impact of individuals with extreme wealth on society.
- Musk's potential actions raise concerns about income inequality and concentrated power.

### Quotes

- "Musk's actions could lead to higher phone prices due to supply issues and competition."
- "His pursuit of platforming controversial voices may not be well-received."
- "Musk's decisions can have ripple effects on job markets and income inequality."
- "Concentration of wealth in few hands can lead to disproportionate influence over the world."
- "Beau questions the impact of individuals with extreme wealth on society."

### Oneliner

Elon Musk's public sparring with Apple and Twitter reveals the power of extreme wealth to shape markets and influence national discourse, raising concerns about income inequality and concentrated power.

### Audience

Social Activists, Tech Enthusiasts

### On-the-ground actions from transcript

- Monitor the actions and decisions of wealthy individuals that could impact society (implied).

### Whats missing in summary

Analysis of the potential consequences of Musk's actions on tech industry dynamics and societal structures.

### Tags

#ElonMusk #Apple #Twitter #WealthInequality #TechIndustry


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Elon Musk
and Twitter and Apple and all of those people
who have been sitting there saying, I'm not on Twitter.
Why should I care about any of this?
You're about to find out why.
So Musk decided to hop on Twitter
and make quite a few statements about Apple.
Now it is important to note the relationship here.
Apple is a client of Twitter when it comes to advertising.
Like many advertisers,
they might have become less than enthusiastic.
I did not check Musk's claims here
because the factuality of the claims don't actually matter.
It's the fact that he put all of this out here
in the way that he did.
Musk tweets out,
Apple has mostly stopped advertising on Twitter.
Do they hate free speech in America?
Not something a wise, traditional business person
would say about a client.
Not something they would put out there publicly like this.
Goes on to talk about how he has issues
with the way Apple has standards to be in their app store
and talks about the way the app store works
and says that Apple has also threatened
to withhold Twitter from its app store,
but won't tell us why.
And he tweeted a meme that indicates
he is ready to go to war with Apple.
Now, basically what's going on here is
Musk is finding out that Apple
and Google through the Play Store,
I'm not sure he's aware of this part yet,
but they have standards when it comes to the moderation
and what can be on apps in their app store.
And he's having trouble
because he might end up running afoul of this.
So he has decided to take to Twitter
to bad mouth one of the companies, an advertiser,
one of the companies Twitter needs to survive.
For those who constantly want to frame
anything Musk does as 4D chess,
please remember if Twitter doesn't have advertisers,
Twitter ceases to be.
If the advertisers don't have Twitter
or don't like Twitter,
don't wanna use Twitter because it's not brand safe,
they simply provide that money to Twitter's competition.
This isn't a smart business move,
but this is where it starts to impact everybody else.
It appears that Musk is toying with the idea
of starting his own phone company now.
Basically, if Twitter gets removed from the app stores,
he will launch his own phone company.
This is the way he's looking at it.
Now, we can sit there and look at it and say,
I don't know that people are gonna pay hundreds of dollars
for a phone to get an app
that they then would have to pay $8 a month for
when they didn't wanna pay the $8 a month to begin with.
And the app is becoming less useful
because of some policy decisions.
We can sit here and say that looks like a bad business idea.
Here's the thing,
Musk is bond villain rich.
He's incredibly wealthy.
He very well might start his own phone company,
start a company with the intent of rivaling
the Droid and iPhone systems.
I don't think it would be successful, but I mean, who knows?
If that happens, what occurs immediately?
There are already supply issues with components for phones.
The prices are already high.
So when another company starts coming in,
buying up lots of components,
it is likely that the prices go up,
at least in the short term.
Now, the other makers of phones,
they will probably try to nip this in the bud, so to speak.
They may drop prices.
They may shift their policies
to make themselves more attractive to people
because they'll want to drive their competition
out of business, how capitalism works.
That's how capitalism works.
The reality is that while Musk's antics
are in some ways humorous
and in some ways really bad for the country,
the amount of power coupons that he has
allows him to influence national discussion,
purchasing habits, and a whole lot of other stuff.
I know many of us are looking at this and just saying,
you know, if you had a friend like this behaving in this way,
you'd be like, buy a sports car,
you know, have a normal midlife crisis.
But it can't be discounted like that
because Musk does have a lot of money.
He has the ability to shift markets
and to alter a lot of landscapes.
Now, my personal opinion is that doing this
in pursuit of what is going to be perceived
as wanting to platform racists
is not going to be well-received.
You know, I've never met Musk.
For all I know, he truly is a free speech absolutist.
But outside looking in, when you just glance at this,
well, he wants to bring all of the conspiracy theory
and racist people back on Twitter.
That's the look.
I don't know that that's a successful brand.
Now, it very well may be so bad
that it damages companies he has that are actually good,
like Starlink or something like that.
But all of this ends up impacting everybody,
people off of Twitter.
A decision by Musk to launch a phone company
because of his sudden obsession with Twitter,
this is something that has a lot of ripple effects.
This is something that would alter job markets in Asia
just because of the amount of money this person has.
When we talk about income inequality
and we talk about wealth being concentrated
in fewer and fewer hands,
one of the things that we have to remember
is not just does it hurt the people on the bottom
because they don't have power coupons,
they don't have the money they need to live.
Those people at the top have a disproportionate amount
of influence over how the world functions
and their maybe rash decisions can impact hundreds of thousands,
if not millions of lives.
Maybe a system that permits that isn't necessarily the best one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}