---
title: Let's talk about MTG and the marketplace of ideas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XlfkKYNqfO4) |
| Published | 2022/11/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the reaction to advertisers pulling out from Twitter due to recent changes.
- Representative Marjorie Taylor Greene's tweet accusing corporations of "corporate communism."
- Greene's claim that Elon Musk opening Twitter for free speech levels the playing field.
- Beau's disagreement with the notion that all ideas deserve a level playing field.
- Dissecting the concept of "corporate communism" and why it doesn't hold up.
- Pointing out the inclusion of companies like American Express and BlackRock on Greene's list.
- The essence of capitalism in the marketplace of ideas and companies choosing to distance themselves from objectionable ideas.
- Distinguishing between corporate influence by representatives and true fascism.
- The marketplace analogy, where the rejection of certain ideas is a result of capitalism, not communism.
- The rejection of objectionable ideas by the majority in the marketplace.
- Why the term "corporate communism" is flawed and doesn't apply in this context.
- Linking Greene's accusations to power dynamics and governmental influence on corporations.
- The role of capitalism in companies making decisions based on public perception and association.
- Clarifying that the rejection of certain ideas is a capitalist concept, not corporate communism.
- Summarizing the misunderstanding around the concept of "corporate communism."

### Quotes

- "Opening Twitter for everyone's speech doesn't promote one side. It opens the town square and levels the playing field."
- "Their product didn't sell. In fact, it was seen as objectionable to the majority of the people in the marketplace."
- "So your ideas are bad and you should feel bad."
- "It's not communism. It's not corporate communism because that doesn't exist. It's just good old capitalism."
- "Y'all have a good day."

### Oneliner

Beau dissects the flawed concept of "corporate communism" while discussing the rejection of objectionable ideas in the marketplace of ideas through a capitalist lens.

### Audience

Social media users

### On-the-ground actions from transcript

- Support platforms that uphold free speech and diverse viewpoints (exemplified)
- Educate others on the nuances between capitalism and communism in corporate settings (exemplified)

### Whats missing in summary

The full transcript provides a comprehensive breakdown of the term "corporate communism" and its misapplication in the context of marketplace dynamics, offering insights into capitalism's role in idea rejection.

### Tags

#CorporateCommunism #MarketplaceofIdeas #Capitalism #FreeSpeech #Misconceptions


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about
the marketplace of ideas. We're going to talk about the reaction of certain people
to the news that many advertisers are, let's just say, not enthusiastic about advertising
on Twitter since some of the changes have gone into effect.
One of my favorite responses to this comes from Representative Marjorie Taylor Greene.
I know there's a lot of people like, wow, he just used her real name. Wait for it. Okay,
so she tweeted out a list of companies that are alleged to have pulled all of their advertising.
I'm going to be honest, I did in fact check the list because it's, I mean, it's Marjorie Taylor
Greene. But the tweet that goes along with it is actually worthy of discussion for once.
This is what she says. This is corporate communism. Corporations using their economic power
to force their political agendas. They need to go back to the customer is king mentality,
not corporations are king. Elon Musk opening Twitter for everyone's speech doesn't promote
one side. It opens the town square and levels the playing field. And then from here it just
goes to like a conspiracy theory laden rant. But that first part, it's actually worthy of discussion.
Let's start at the end. Opening Twitter for everyone's speech doesn't promote one side.
It opens the town square and levels the playing field. The assumption here is that ideas deserve
a level playing field. They don't. That's not a thing. They don't. Ideas do not deserve equal
treatment. Some ideas are inherently wrong. They are incorrect. They do not deserve a level playing
field with a correct idea. As an example, there might be somebody who believes that space lasers
started wildfires in California. That person's opinion should not have a level playing field
against say, I don't know, an arson investigators. It's not the same. The ideas can't be treated the
same because they aren't of the same quality. They don't get a level playing field.
Now let's go up to this idea at the beginning. This is corporate communism. That's not a thing.
That's not a thing. That is not a thing. That's made up. Those are just two words stuck together
that don't actually mean anything. Communism historically can be generally defined as the
pursuit of a society that is looking for three things. It needs to be stateless, classless,
and moneyless. A corporation's job is to make money. These two things don't go together.
It's not actually a term. It's made up. It's a bad idea that doesn't deserve a level playing
field with other people who, I don't know, maybe understand how systems of government work,
or economic theory, or general ideology. It's just not there. It's not on the same level.
Aside from that, I want to point out that this list,
I'm fairly certain that American Express, which is on the list, is not down with the abolition
of money, getting rid of currency. I don't think they're on board with this. I have never seen a
statement from American Express on this topic, but if they ever wrote one, I'm fairly certain it
would say something along the lines of, nah man, that's totally uncool. It would mess up our entire
business model. The idea that American Express is somehow supportive of communism is a bad idea.
It doesn't deserve a level playing field with, I don't know, American Express is a company that
literally depends on money to function, and kind of class as well, because keeping up with the Joneses
causes a lot of credit card usage. Another entity on her list, on this list that she retweeted here,
is BlackRock. I don't know if everybody is familiar with this company, but if you took
the most capitalist inclinations of every company throughout history, and just distilled them down
into one company, that company would be called BlackRock. Her corporate communist list here is,
it includes one of the most hyper-capitalist entities on the face of the planet.
It's not a valid idea. It doesn't deserve a level playing field. Doesn't even make sense.
The problem with what they're facing here is that when it comes to the market of ideas,
the marketplace of ideas, their product didn't sell. That's what's occurring. Their product didn't sell.
In fact, it was seen as objectionable to the majority of the people in the marketplace,
to the point where the marketplace was like, yeah, we don't want that here. And the market owner
threw away the spoiled idea. Now a new market owner has brought the spoiled idea back,
and a bunch of other companies who have their products in that marketplace, they're like,
I can't have my product, my idea, next to the spoiled idea, because the spoiled idea might
make my product have the same stink. So they're removing their products from that marketplace of ideas.
That is literally what's happening. Companies are so aware of the fact that these ideas are
objectionable to most Americans, that they don't want their product next to those ideas.
They don't want to be seen together. They don't want them associated with each other.
It's pure capitalism. That's what's happening. It's not corporate communism, because that doesn't exist.
Corporations using their economic power to force their political agendas.
Apparently she believes this is corporate communism. This is just
how power coupons work. And she knows this because she's part of government.
While this isn't a philosophy, ideology, or system of government, it's kind of close to what's happening.
It's kind of close to the idea of the state, through, I don't know, some representative,
trying to force a corporation to do things in interest of the state.
And by that I mean the beliefs of the representative. That is a philosophy,
like a representative trying to get corporations to do something in service of the state.
If that was like countrywide, if that was the system of government,
that would be fascism. The blending of corporate and state power.
But that would only occur if it was a representative of the government
trying to force corporations to do their bidding. Sometimes through legal means.
Most times through legal means. Not a corporation dealing with another corporation.
That's just business. Just capitalism. So your ideas are bad and you should feel bad.
That's why they weren't in the marketplace of ideas. The new market owner is pulling ideas out of a box.
Pulling ideas out of a dumpster and putting them next to ideas that weren't seen as objectionable.
And the owners of the ideas that weren't spoiled don't want their ideas next to the
spoiled ideas on the shelves. So that's why they pulled their advertising.
It's not communism. It's not corporate communism because that doesn't exist. It's just good old capitalism.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}