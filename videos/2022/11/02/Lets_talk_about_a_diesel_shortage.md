---
title: Let's talk about a diesel shortage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8PgJ8KdOpKE) |
| Published | 2022/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There is a diesel shortage, but not as catastrophic as portrayed, leading to increased prices due to seasonal factors compounded by global oil market disruptions caused by the war in Ukraine.
- Diesel prices rising will impact the cost of other goods as transportation costs increase.
- The solution lies in energy independence, which has been misconstrued by nationalists as solely producing oil and gas domestically, rather than embracing renewable and clean energy sources.
- Energy independence involves transitioning to renewables like solar, wind, and electric vehicles, which are already feasible technologies.
- Building infrastructure for renewable energy is key to achieving true energy independence and reducing dependence on manipulated finite resources.
- Transitioning to clean energy is vital for economic and environmental sustainability, contrary to short-sighted slogans advocating for continued reliance on fossil fuels.

### Quotes

- "Energy independence is renewable. It is clean. That's what energy independence is."
- "Those slogans that you're hearing from people who roll coal, it's short-sighted."
- "Energy independence is clean. And it's renewable. That's the solution."

### Oneliner

There is a diesel shortage leading to price hikes, advocating for true energy independence through renewable sources, not fossil fuels manipulation.

### Audience

Energy-conscious citizens

### On-the-ground actions from transcript

- Transition to renewable energy sources like solar and wind power by investing in solar panels for homes or supporting wind energy initiatives (exemplified)
- Advocate for the adoption of electric vehicles and support companies that are already using electric trucks for transportation (exemplified)
- Educate others on the importance of clean energy and dispel misconceptions around the feasibility of electric-powered vehicles (suggested)

### Whats missing in summary

The importance of investing in renewable energy infrastructure and educating the public to combat misinformation and push for sustainable energy solutions.

### Tags

#DieselShortage #EnergyIndependence #RenewableEnergy #ClimateAction #CleanEnergy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about diesel
and whether there's a shortage.
And if there is, how bad it is, what the solutions are.
We're going to talk about energy independence
and what that really means.
OK, so first, is there a diesel shortage?
Yes, there is.
It's not quite the doomsday scenario that is being painted,
but it's bad.
And diesel prices are going to go up.
Why?
There's multiple reasons.
Most of them are seasonal.
They're things that happen every year.
But now that is coinciding with two major factors.
The first is a lack of refining capacity
inside the United States.
So if you're screaming, drill, baby, drill,
understand that's not going to help.
It's refining capacity.
The other big issue is the disturbance in the global oil
market because of the war in Ukraine,
because of Russia's invasion that has disturbed the oil
market, making prices go up.
So diesel is going to get more expensive.
Because of that, other things will probably
get more expensive because diesel
is used to transport stuff.
So that cost, that transportation cost,
gets added into things.
So what's the solution?
Energy independence.
However, that term has been co-opted by nationalists.
It's gotten to the point where people
think energy independence means producing
all of the oil and gas and diesel and everything
inside the United States.
That's not energy independence.
You're still dependent on a finite resource that
is being manipulated by a bunch of companies that
want to make a bunch of money.
That's not energy independence.
Energy independence is renewable.
It is clean.
That's what energy independence is.
We have to build the infrastructure for it.
And when you talk about electric vehicles
and powering via solar or wind or any of the other renewable
methods, any of the other clean energies,
you have people say, well, you can't run a semi on electric.
Yes, you can.
Anheuser-Busch has been doing it for years.
I think they have like 100 electric semis.
Pepsi started doing it.
It's already happening.
The technology exists.
They're making a hybrid Abrams now.
Those big mining trucks, the giant dump trucks
you see at mines, they're electric.
The technology is there.
We have to make the transition.
We have to build that infrastructure.
That's energy independence.
Those slogans that you're hearing from people who
roll coal, it's short-sighted.
And it's really just a way to manipulate the base
to get them cheering and chanting,
go USA, while they completely undermine
the economic and environmental future of the country.
Energy independence is clean.
And it's renewable.
That's the solution.
Anything else is not really a fix.
These markets are going to continue
to become more volatile.
They are going to continue to go up in price
because all of these oil companies
want to beat last year's numbers.
The easiest way to do that is to create artificial scarcity
and charge you more per gallon.
Those telling you that the solution is to drill and get
the oil here, they're just playing you.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}