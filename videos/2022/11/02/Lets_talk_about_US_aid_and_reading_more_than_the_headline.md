---
title: Let's talk about US aid and reading more than the headline....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YIiubiKcAA4) |
| Published | 2022/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is pre-positioning specific gravity bombs in Europe, but this move is unrelated to Ukraine and has been planned for years.
- The gravity bombs being sent are not considered precision weapons by US standards, but they are guided and can hit targets within about 30 meters.
- The specific type of gravity bomb being sent is the B61-12, which is a dial-a-yield nuclear weapon that can produce blasts of different sizes.
- This modernization of America's strategic arsenal has been in progress since around 2012-2014, with the current shipment scheduled to Europe since 2018.
- The purpose of sending these new gravity bombs to Europe is part of the process of replacing retired weapons and modernizing the US arsenal, not an escalation or provocation.
- The move to send these gravity bombs may have been slightly accelerated, possibly as a subtle message or due to readiness, but it isn't a response to a lack of precision weapons.
- The US still possesses precision guided munitions (PGMs) and dominates the skies with warehouses full of such weapons.

### Quotes

- "This isn't more. It's new ones coming in to replace retired ones. So it's not an escalation. It's not a provocation."
- "The US still does, in fact, own the skies and does, in fact, have just warehouses full of PGMs at its disposal."

### Oneliner

Beau clarifies the US move to pre-position gravity bombs in Europe as part of modernizing the strategic arsenal, not an escalation.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor and analyze developments in US strategic arsenal modernization (implied)

### Whats missing in summary

Detailed analysis and implications of US strategic arsenal modernization efforts.

### Tags

#US #ForeignPolicy #StrategicArsenal #Modernization #NuclearWeapons


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the United States
moving some stuff to Europe, pre-positioning some stuff
in Europe, what that means.
We were going to talk about this subject anyway,
but we also got a message that allows
us to reiterate the very important lesson of don't just
read the headline and allow that to confirm whatever
bias or belief you may have.
OK, so here's the message.
You made a big deal about Russia using unguided weapons
and said they'd only use them if they ran out
of precision guided munitions.
I read the US was sending gravity bombs to Europe.
Is the US out of guided weapons?
Does the US still own the sky?
Or are you ready to admit Russia is the same as the US
militarily?
Or will you just ignore this because it doesn't fit
your Russia is bad narrative?
OK, yes, true enough.
The United States is sending gravity bombs,
a specific set of gravity bombs to Europe.
This doesn't actually have anything to do with Ukraine.
This has been something that has been scheduled for literally
years.
But they're gravity bombs.
They're not precision by US standards.
But keep in mind, US standards for precision guidance
is they can hit the balcony they're aiming at.
They're not just falling, though.
They have fins on them that guide them to the target.
And I want to say they can get it within 30 meters
of the intended target.
The specific type is B61-12.
Now, 30 meters isn't accurate by US standards.
There are some countries who used
to be viewed as near peers who would consider that precision.
But in this case, it's kind of relative
because this doesn't have anything to do with Ukraine,
even though these are going to Europe.
With this particular device, if I am standing in a brick home
and it lands 30 meters away, completely misses the house,
do you know what I become?
A fireball.
At 100 meters away, I become a fireball.
I become a fireball then, too.
If it lands a mile and a half away,
the building I'm in will collapse.
I don't make it.
At two miles away, survivability starts to increase,
but I'm covered in third-degree burns.
At four and a half miles away, more or less,
there is increased survivability,
but I'm definitely not having a good day from shattered glass,
flying debris, so on and so forth.
The difference between these munitions and the type
that we were talking about at the time
when we were talking about precision-guided munitions
is that a B61-12 is a dial-a-yield.
It's a nuke.
It's a nuclear weapon.
Accuracy becomes very relative when
you're talking about an area of destruction that's, I don't
know, nine or 10 miles across.
If it's off by a few meters, it really doesn't matter.
The numbers I just gave were for the upper end of the dial.
It's not actually a dial, by the way.
But this device can be set to produce
a specific yield, a specific size of a blast.
The high end is 50 kilotons.
I think this one goes down to fractional sizes,
but I know it goes down to like 5, 3, 1.
It may even go down lower than that.
There are a bunch of different models
of this particular device.
So why is this happening?
This is the modernization of America's strategic arsenal.
This has been on the books since, I don't know,
2012, 2014, somewhere in there, this process.
I want to say these have been scheduled to head over there
since, I don't know, 2018, something like that.
This is a modernization process, though.
This isn't more.
It's new ones coming in to replace retired ones.
So it's not an escalation.
It's not a provocation.
They did do it like a month early, I think.
I want to say they were going to do this in December.
And there may be a little bit of a nudge
there saying, maybe you should stop
talking about tactical devices, because we have some, too,
and ours are way better.
But it may also just be they're ready now,
so they're shipping them over.
This doesn't really have a lot to do
with a lack of precision weapons.
There are two different categories of device.
One is for taking out armor.
One is for taking out miles.
So that's why it's a little bit different.
Rest assured, the United States still does, in fact,
own the skies and does, in fact, have just warehouses
full of PGMs at its disposal.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}