---
title: Let's talk about Twitter and check marks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_zARivqLfDQ) |
| Published | 2022/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of verified accounts on Twitter and the benefits they provide.
- Elon Musk reportedly plans to charge $20 a month for Twitter verification.
- Beau personally finds the verified notifications useful for keeping track of large accounts and interactions.
- Beau considers the $240 yearly fee for verification worth it for research purposes but acknowledges it may not be for everyone.
- Musk's plan to change Twitter's revenue generation to a subscription fee raises concerns about the platform's longevity and appeal to new content creators.
- Beau believes newer content creators may opt for different platforms if Twitter's business model changes.
- He expresses skepticism about Musk's plan and its potential negative impact on Twitter's relevancy.
- Beau mentions considering other social media platforms due to the proposed changes on Twitter.
- He questions the effectiveness of Musk's subscription-based approach in the long term.

### Quotes

- "Being verified provides a few benefits. The one that people are after most is just clout."
- "Musk apparently wants to change this to a subscription fee so people can pay for the privilege of generating him ad revenue."
- "If he goes through with this, I think that this will lead to a loss of relevancy for Twitter over the long haul."

### Oneliner

Beau explains Twitter's verification system, Musk's subscription fee proposal, and the potential impact on content creators and platform longevity.

### Audience

Content Creators

### On-the-ground actions from transcript

- Advocate for platforms that support content creators and offer fair compensation (implied).
- Support and migrate to social media platforms that prioritize content creators' interests and longevity (implied).

### Whats missing in summary

The full transcript provides in-depth analysis and commentary on Twitter's verification system, Musk's proposed changes, and the potential consequences for content creators and platform sustainability.

### Tags

#Twitter #Verification #ElonMusk #ContentCreators #SocialMedia


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Twitter and check marks.
And the reported new plan from Elon Musk
on how to generate more revenue for Twitter
and whether or not it's going to work, because I've
had a whole bunch of questions come in about whether or not
I am going to keep my check mark.
For people who don't use Twitter,
let me give you a little catch up on this.
This social media platform, Onnit,
there are accounts that are called verified.
And they have a little check mark next to their name.
Being verified provides a few benefits.
The one that people are after most is just clout.
Look, I'm an important account type of thing.
Another is the ability to limit impersonation.
You know, down below on YouTube, you
see those accounts that show up.
They look kind of like me vaguely.
And they say, hey, contact me at Telegram or WhatsApp
or whatever services I don't use.
Those are not me, and they will never be me.
But it provides a little bit of a hedge against that on Twitter.
And then the third one is basically notifications
from other verified accounts go into their own little tab
so you can keep track of them more easily.
That's what it does.
Musk is reportedly planning on charging people $20 a month,
so $240 a year for the privilege of being verified.
Tons of questions about this, and they range.
Now, the reality is you have to look at this
from three different points of view.
The first one, is it worth it to me?
The clout that comes from a check mark literally don't care.
I didn't even apply for it.
That's not something I care about.
The impersonation thing, not really a huge issue for me.
That account is pretty big.
I don't think, I think most people would be able to tell
the difference between the brand new account
with eight followers and mine.
The verified notifications, and for those who are on Twitter,
if you don't know this, when you hit your little bell icon
on your phone app, you see all and mentions show up.
If you're verified, you have a third tab,
and it is just other large accounts.
This is incredibly useful for me.
I provide a lot of commentary on conversations and happenings
between public figures,
and this is a really easy way to keep track of them.
It's also useful for me if somebody,
you know, some right-wing goon starts talking about me,
or somebody says something nice about me,
knowing when it happens.
That's useful to me.
Is it worth $240 a year for me?
Yeah, because it's research for content.
It really is for me, but I'm the exception.
I don't think it's worth that for most people,
but that's not the whole story,
and that doesn't necessarily mean I'm going to subscribe.
The other thing you have to look at
is the business standpoint for Twitter,
which this is unique because apparently Elon Musk
believes that he can alter the way
the creator economy works.
If you look at all the other social media platforms
that have successful business models,
those that are around and growing
and have been around a while,
they all have one thing in common.
They pay the content creators.
YouTube, TikTok, Facebook,
even Twitter was actually experimenting with it.
I had an offer to get monetized not too long ago on Twitter,
but I didn't want to put anything behind a paywall,
so I didn't do it,
but even they were moving that route.
That's how all of these social media networks
make their money.
Make no mistake about it.
When you're on a social media site, you are the product.
The company is selling your eyeballs to advertisers.
That's how they really make their money.
Musk apparently wants to change this to a subscription fee
so people can pay for the privilege
of generating him ad revenue.
I don't know how that's going to go over
with a lot of people.
Even if it works in the short term
for content creators who are established,
who have been around,
what does it say for the longevity of the site?
And this is that third point of view.
The new creator.
Every day, there are new content creators coming up.
Hundreds, thousands each year.
Some of them, they may not even have the $240
when they're first starting out.
So what are they going to do?
They're not going to want to go to a site
that runs in the exact opposite way
of everything that they're used to,
and they may not actually even have the resources to do it.
So what do they do?
They go to a new app.
They go somewhere else.
The newer content creators won't go to Twitter.
They'll go to a new site.
What happens then?
When you look at a site like Twitter, what's its draw?
This isn't a site where you try to keep in touch
with your kind of racist aunt.
This is a site where you're consuming content all day.
If the content creators are not there,
if the hot new content creators go somewhere else,
so will the new users.
It limits the longevity.
It limits the longevity.
And think about how often I have mentioned
Twitter on this channel.
I saw this on Twitter.
Or I got a message via Twitter.
Or there was a tweet, something like that.
These new content creators, they will
be talking about a different platform like that
to all of their subscribers off of that platform.
And they'll help it grow.
If he goes through with this, I think
that this will lead to a loss of relevancy for Twitter
over the long haul.
This could be something that ends up Myspacing it.
But it is definitely going to cause newer users
to go somewhere else, which will eventually
cause the more established creators that are on Twitter
to go somewhere else.
I am looking at other social media platforms.
So am I going to do it personally?
I don't know yet.
Do I think this is a good idea?
No, because out of the people that I
know who have a check mark, I think
I might be the only one that it would actually make sense
to pay for the service.
And it's not for the check mark.
It's to be able to filter out the large accounts
and keep track of them separately.
So I don't know if this is something
that is going to be receptive.
I've seen a whole lot of people say,
I am not doing this already.
And it makes sense, because somebody like Stephen King,
the author, he was talking to me.
He's like, I'm not doing this.
It's not about the money.
It's about the fact that he is able to do it.
It's about the fact that he is a draw for Twitter.
He has millions of subscribers that
go there to interact with him.
And those subscribers see the ads that Twitter shows.
And that's where their revenue comes from.
I get it from his point of view.
Yeah, I think this is a horrible move.
He's trying to go against the way the creator economy works.
And it kind of shows a misunderstanding
of how social media platforms work,
because other platforms have tried this, a subscription
model.
It doesn't work.
I don't see how this would work in the long term.
Sure, a nice bump in revenue shortly after he takes over,
that may look good.
But over the long term, this very well
could just kill the site.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}