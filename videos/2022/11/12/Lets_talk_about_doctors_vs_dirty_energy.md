---
title: Let's talk about doctors vs dirty energy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4tx_s-UdMXs) |
| Published | 2022/11/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Doctors are attributing various health issues to climate change caused by dirty energy companies.
- Mosquitoes have an extended range due to changing temperatures, leading to an increase in Zika and dengue cases in the U.S.
- Particulate air pollution caused 1.2 million deaths worldwide, including 11,840 in the United States.
- Extreme heat has led to 98 million cases of hunger globally.
- Heat-related deaths in the U.S. have increased by 68%, rising to 74% for those over 65.
- Climate change and emissions are causing a wide array of health issues globally.
- Dirty energy companies, subsidized by governments, are making massive profits while contributing to medical problems.
- The Lancet releases annual reports detailing the impact of dirty energy companies on health.
- Transitioning away from dirty energy is imperative for these companies, akin to the tobacco industry facing lawsuits.
- Urgent decisions are needed from dirty energy companies as time is running out for change.

### Quotes

- "The dirty energy companies, their smartest move is to transition themselves as fast as possible."
- "The situation here is pretty similar to tobacco."
- "The dirty energy companies, they have a decision to make and they're running out of time to make it."

### Oneliner

Doctors link health issues to climate change caused by dirty energy companies, urging urgent transition away from harmful practices.

### Audience

Climate advocates, policymakers, activists.

### On-the-ground actions from transcript

- Read the Lancet report on the impact of dirty energy companies on health (suggested).
- Advocate for transitioning away from dirty energy sources in your community (implied).

### Whats missing in summary

The full transcript provides more in-depth information and statistics on the health impacts of climate change caused by dirty energy companies.


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about doctors
talking about climate change and really pointing the finger at the dirty energy companies.
There is a thing that comes out in Lancet every year.
And the 2022 Lancet countdown has a lot of information about how the fossil fuel industry, the dirty energy companies,
how their behaviors that are leading to their record profits are impacting everybody else.
As an example, because of temperature changes and the amount of rainfall and all of this stuff that has changed,
mosquitoes have an extended range. They can move further than they used to,
which is leading to an uptick in Zika and dengue here in the U.S.
Particulate air pollution is, it took out, I want to say 1.2 million people worldwide.
11,840 of those in the United States. Extreme heat has caused 98 million cases of hunger.
There has been a 68 percent increase in heat related deaths in the U.S.
And for those over 65, it's 74 percent.
There is a wide array of health issues that we are experiencing now that can be attributed to climate change,
that can be attributed to emissions, that can be attributed to the dirty energy companies,
the companies that are being subsidized by governments, the companies that are raking in massive profits
while you're having trouble at the pump.
Not just are these companies overcharging, not just are these companies squeezing entire economies,
they're also contributing to medical issues for millions of people.
Lancet puts something out like this every year.
This year it was really direct. It's worth reading if you have the time, if you have the energy.
And there's a lot more facts and figures in there.
Those were some that I found interesting,
mainly because most of this we had talked about on the channel at some point in time.
But there's more.
The dirty energy companies, their smartest move is to transition themselves as fast as possible.
I would strongly suggest if you really take a look at this stuff,
what you're going to find out is that the situation here is pretty similar to tobacco.
And I think we all remember those lawsuits and how that played out.
The dirty energy companies, they have a decision to make and they're running out of time to make it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}