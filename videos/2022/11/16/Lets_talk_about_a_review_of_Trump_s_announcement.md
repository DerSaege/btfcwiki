---
title: Let's talk about a review of Trump's announcement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iCaXSjfmGLs) |
| Published | 2022/11/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump officially announced his candidacy, but Fox News cut away from his speech, deeming it boring.
- Trump seemed restrained, reading off a teleprompter with only a few deviations.
- His team appears to be trying to rebrand him to appeal to a wider audience, potentially risking his base.
- Despite attempts to tone down his rhetoric, Beau doubts this strategy will boost Trump's dwindling support.
- Trump made promises during his speech, including fixing supply chains and terminating the Green New Deal, but they were seen as admissions of failure.
- Beau expected Trump to attack fellow Republicans who weren't supporting him to rally his base, which did not happen.
- There's a suggestion that Trump may shift focus to attacking the Democratic Party instead.
- Beau finds the use of the term "glory" in Trump's speech unsettling and hints at a potentially ominous campaign theme.
- Overall, Beau rates Trump's announcement speech poorly and expresses disappointment in its content and delivery.

### Quotes

- "I was kind of expecting Trump to come out swinging and take shots at the governor of Florida, of Cruz, and anybody who kind of indicated they weren't going to support him."
- "Every promise was also a weird admission of failure on his part."
- "It's going to be much harder to trick them."
- "That's a little creepy."
- "One out of ten, I wouldn't watch again."

### Oneliner

Beau doubts Trump's rebranding attempts will revive his support, expecting potential shifts towards attacking Republicans and unsettling campaign themes.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze political speeches for underlying messages and implications (implied)
- Stay informed about political developments and strategies (implied)

### Whats missing in summary

Insight into the potential impact of Trump's rebranding efforts and the challenges he faces in reshaping his image.

### Tags

#Trump #PoliticalAnalysis #RepublicanParty #CampaignStrategy #Rebranding


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Trump's announcement.
He has officially announced his candidacy.
I am recording this immediately after watching it.
Well, I didn't watch all of it because Fox News cut away because it was that boring.
And I know there's been a lot of talk about how Fox and those companies are not going
to support him this time, but they didn't cut away to like, you know, talk about DeSantis.
They cut away and showed Biden.
I imagine that there's not a ketchup bottle in Mar-a-Lago that's safe tonight.
So Trump was basically reading off a teleprompter.
He only went off script a few times.
It appears that somebody has put a leash on him and explained that he can't win if he's
Trump.
So they're going to try to rehabilitate his image.
The problem with that is if they do that, he loses his base.
So what they tried to do was insert a lot of rhetoric that would appeal to that base
and just make it low key.
You know, he used the term globalist.
There was a lot of fear mongering about immigrants and just normal Trump stuff, but very low
key, low energy, very boring.
I don't think this is it.
I don't think that this is something that is going to reinvigorate his failing numbers.
I was kind of expecting Trump to come out swinging and take shots at the governor of
Florida, of Cruz, and anybody who kind of indicated they weren't going to support him.
That probably would have been the route that would have got his base motivated again.
However, whoever is now in control of what he says, because he didn't write that, thinks
that they have a better shot at actually getting somewhere and maybe surviving the primary
if they tone him down and try to recast him as a normal politician.
That's going to be a tall order.
The American people are not going to forget what happened and in the process of this,
he's probably going to lose his base.
He did make a number of promises.
You know, he said he was going to fix the supply chains.
I'd remind everybody he's the one that kind of messed him up.
He said that he would build the wall that he built, but they didn't finish or something
like that.
Every promise was also a weird admission of failure on his part.
He did promise to terminate the Green New Deal, which I found super interesting.
Yeah, it didn't go well for him.
And I mean, I'm going to be honest, there was a part of me that was really looking forward
to him just him going full Trump on the Republican Party.
And that may still be in store because I have a feeling when he finds out that everybody
just got bored with what he was talking about and turned away, he will reinvigorate his
campaign by going after Republicans.
Basically, whoever's writing this speech, whoever is handling him now, they're probably
somebody who at one point in time he would call a rhino, but they understand that attacking
the Republican Party isn't going to get him ahead.
The problem is going after the Democratic Party.
That's what all Republicans do.
And the fear mongering and all of that, it's not going to cut it, especially now that the
voters are going to be...
These are people who witnessed him the first time.
It's going to be much harder to trick them.
I do find a really unsettling fact in it appears they're testing out the term glory to use
during their campaign.
It was something that he threw out quite a few times that he was going to run to restore
glory and all of this.
That's a little creepy.
Overall, one out of ten would not watch again.
Sad.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}