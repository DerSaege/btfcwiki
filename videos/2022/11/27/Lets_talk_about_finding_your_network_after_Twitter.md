---
title: Let's talk about finding your network after Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XSzdcskD4VM) |
| Published | 2022/11/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of reestablishing social networks when a social media platform disappears, specifically addressing communities within Twitter.
- Advises maintaining continuity by using the same profile picture and handle when transitioning to a new social media platform.
- Suggests hashtagging your community or group to make it easier for others to find you on new platforms.
- Recommends interacting with individuals who have large followings within your community to reconnect with familiar faces.
- Encourages creating cross-platform communities to strengthen networks and increase resources.
- Acknowledges the fear and concern within disabled Twitter communities regarding losing their network.
- Emphasizes the shared passion and common interests that bind communities together, making it easier to reconnect.
- Urges individuals to establish their community on multiple social media outlets to prepare for any platform changes.
- Reminds people that while social media platforms may change, the supportive community will remain strong.
- Advocates for helping one another and maintaining connections during platform transitions.

### Quotes

- "Make yourself easy to find for people that are looking for you."
- "Y'all have something in common. Y'all have something that y'all share."
- "It's just people helping people."
- "Twitter was never really your friend or your home. It was just a company."
- "Just make yourself identifiable to those within your community, and they'll find you."

### Oneliner

Beau advises on maintaining continuity and finding community leaders to reestablish social networks when transitioning between social media platforms.

### Audience

Social media users

### On-the-ground actions from transcript

- Maintain continuity by using the same profile picture and handle on new social media platforms (implied)
- Hashtag your community or group to make it easier for others to find you on different platforms (implied)
- Interact with individuals who have large followings within your community to reconnect with familiar faces (implied)
- Establish your community on multiple social media outlets to prepare for any platform changes (implied)

### Whats missing in summary

The full transcript provides detailed guidance on reestablishing social networks when transitioning between social media platforms, focusing on continuity, community leaders, and cross-platform presence.

### Tags

#SocialMedia #CommunityBuilding #OnlineNetworks #Transition #CrossPlatform


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to answer some questions
about how to reestablish your social network,
your network of people, how to find your network
if the social media platform that you're on
disappears like somebody snapped their fingers.
For those who may not understand the significance of this
for some people, Twitter, as an example,
there are a lot of communities that have sprung up
within Twitter that are very specific
and they are made up of people who in many cases
may not be able to easily replicate that network,
or at least they think they won't be able to.
And they may be in a situation
where the interaction on the computer,
it's the only place they can find the camaraderie.
This is a big issue for what is known as disabled Twitter.
Again, it's not actually a different platform.
It's just a subset within Twitter.
Okay, so there's a lot of panic over this.
Don't, this is way easier than you think it is.
Okay, so first thing,
what can you do to make sure people find you?
Maintain continuity.
Use the same profile picture,
use the same screen name if you can.
When you get to your new social media network,
maintain continuity.
Same profile picture, same handle if you're able to.
If you can't, in your bio,
put on Twitter as at whatever it is.
And then hashtag your group.
Hashtag the community that you are a part of on Twitter.
You know, hashtag actually autistic.
When people leave Twitter and go to new platforms,
they're gonna look for that.
They're going to look for those people
that they interacted with on Twitter.
So just start there,
make yourself easy to find
for people that are looking for you.
The other thing is go through the people
that you are following.
Those people who have large platforms
within your community.
Make sure you kind of maybe screenshot your follow list
or screenshot at least those people
who are big names in whatever your community is.
The people that became thought leaders
or developed platforms however they did it.
When they get to the new social media sites,
they're gonna try to reestablish their platform.
Because they already have followings,
there will be people from your circle
that are looking for them.
And they'll be easier to find.
Then interact with those posts.
You will see your friends from Twitter down below.
Even if they didn't find you and you couldn't find them,
you'll bump into them again.
The beauty of establishing networks like this
is that y'all have something in common.
Y'all have something that y'all share.
And it's something you're passionate about.
That doesn't go away just because
some billionaire made an impulse buy.
It'll be fine.
You will find your group again.
Just do those two things.
That those things right there are going to help a whole lot.
Make yourself easy to find.
And then find and interact with the people
who already have large followings in your community.
Because believe me, they'll get over there
and they will start building their following again.
You'll find your people there.
Another thing that I would suggest
is when you get to wherever you're going,
make it something that is cross-platform.
No longer be actually autistic as a hashtag on Twitter.
Make it a group that exists,
a community that exists on several social media outlets.
One, it strengthens your community,
increases your resources,
all of that stuff.
And it gives people a way to find you
in the event that something like this happens.
When you're talking about social media,
especially with something like Twitter,
where it isn't like Facebook,
where everybody's expected to be their projected self,
the self that they put out to the world.
Twitter was a place that allowed for handles
that weren't really you,
where you could talk about issues
that you might not talk about
on something that was tied to your name.
And you could do it publicly
and you could get interaction
from other people who are dealing with the same stuff.
You will be able to recapture that.
I know for a lot of people,
they're very much at home on Twitter
and on their social media platform.
And this is scary,
because I know for disabled Twitter,
this is like a huge thing
because of how hard it is
to find the people
that share your experiences.
Having that network already there,
I'm sure it is incredibly comforting.
I have seen the tweets.
Y'all are worried and it makes sense,
but it's much easier to replicate than you might think.
And there's nothing saying that right now,
before Twitter spirals,
you can't go to these sites,
to the other social media networks
and establish your community,
whatever it is, as kind of like a reception center
when people show up and start from there.
There's nothing stopping you from doing that now.
I love the fact that people are trying to make plans
to maintain that community,
because those communities translate
to being able to get stuff done.
And it's just people helping people.
Like every message that has come in
has been from a group
that their real purpose is helping each other.
So don't get down about it.
It's just a company.
Twitter was never really your friend or your home.
It was just a company.
A useful platform, but if it goes away,
believe me, something will replace it.
Y'all will be fine.
Just make yourself identifiable
to those within your community,
and they'll find you.
Look for the people
who are going to establish their followings quickly.
Follow them, find your friends through them,
and maintain continuity.
That's a big part of it.
Just so y'all know,
when y'all change your profile pictures,
I don't know who you are anymore.
It takes me a moment to place you.
So that's where I would go.
Definitely an internet generation problem,
but I don't mean that to downplay it.
I know this is really important for some people.
So just take the steps to identify yourselves,
and y'all will be all right.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}