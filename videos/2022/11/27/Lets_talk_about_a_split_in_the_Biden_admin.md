---
title: Let's talk about a split in the Biden admin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ma9VNvyh6jU) |
| Published | 2022/11/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There is a perceived split within the Biden administration regarding foreign policy on Ukraine.
- The Biden administration's policy is "Nothing about Ukraine without Ukraine."
- General Milley's suggestion for Ukraine to negotiate is not a split in the administration, but a military perspective.
- Milley believes that Russia has two options: withdraw or face a prolonged and devastating war.
- Ukraine has entered a protracted conflict, making it difficult for Russia to advance further.
- Milley gives Russia less than a 1% chance of winning in the long run.
- Media misreported Milley's statement about Ukraine not pushing Russia out anytime soon.
- The situation in Ukraine is turning into a hammer and anvil scenario for Russia.
- Russia faces a high likelihood of losing if the conflict continues.
- Milley emphasized the need for a political option due to the current situation in Ukraine.

### Quotes

"Nothing about Ukraine without Ukraine."

"War is a continuation of politics by other means."

"Russia has two options. They withdraw or they get themselves embroiled into a years-long war."

"If this goes on, it's going to be a long war."

"Ukraine can't push Russia out anytime soon."

### Oneliner

There is no split in Biden's team on Ukraine; it's about understanding the military perspective amidst a protracted conflict.

### Audience

Foreign policy enthusiasts

### On-the-ground actions from transcript

- Contact local representatives to advocate for a diplomatic resolution (suggested)
- Join organizations supporting peace efforts in Ukraine (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the military perspective on the Ukraine conflict and the potential outcomes based on current situations.

### Tags

#Ukraine #BidenAdministration #ForeignPolicy #MilitaryPerspective #Diplomacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about a perceived
and even reported in some cases, break,
a gap, a division, a split within the Biden administration
when it comes to foreign policy on Ukraine.
And we're going to explain why the appearance
of this split exists.
Okay, so the Biden administration's policy
when it comes to Ukraine and whether or not Ukraine
should negotiate, it's simple.
Nothing about Ukraine without Ukraine.
That is like official policy.
That is how they are looking at it.
And that is the right move.
And for once, when I'm saying that,
I'm not just talking about the international poker game
where everybody's cheating, like morally,
that's the right move.
Those things rarely align in foreign policy,
but this is the correct move.
And all of the diplomats, everybody is behind this policy.
And then there's General Milley,
who a couple of times recently has stated
that he thinks there's a political option on the table,
that maybe it is time for Ukraine to negotiate.
This is being reported as a split
in the Biden administration.
It's not what it is.
General Milley is not a diplomat.
He's not supposed to be.
He's the chairman of the Joint Chiefs.
This isn't his job.
He's not a diplomat.
When he is responding and he says this,
it comes from most professional warriors' understanding
that war is a continuation of politics by other means.
His statement really boils down to this.
At this point, Russia has two options.
They withdraw or they get themselves embroiled
into a years-long war that is going to be way worse
than what they've been dealing with.
That's what he's saying.
So if he was a Ukrainian general,
this would be the moment he goes to Zelensky
and be like, yeah, maybe we should talk to him
because they have to know at this point,
we're coming from a position of strength right now.
And if he was a Russian general,
he would be advising Putin and saying, hey,
I mean, we can keep fighting,
but best case scenario, we keep some dirt
and we are going to pay way too high a price for it.
And that's the reality.
At this point, Ukraine has entered that protracted conflict.
If you remember before the war ever started,
we ran through the scenarios.
And interestingly enough,
the Ukrainian military has kind of pushed
through all of them at this point,
but we are definitely now in the protracted stage.
This just gets worse and worse and worse for Russia.
It doesn't get better.
It does not improve.
So that's where he is coming from.
He's coming from the position of a military commander,
looking at the assets on the ground,
looking at the way the war has shaped up and saying,
hey, there might actually be a political option here,
not that he's in contact with Ukraine
and telling them they need to negotiate.
So that's a misreporting of what's being said.
And I kind of think this is good faith.
I think this is a good faith mistake
because when he's talking about it, I mean, he is,
he's like, yeah, there should definitely be
a political option on the table right now.
And the reason he's saying that is,
you don't have to be a military genius
to look at that map and see how things
are gonna play out in the future.
So his kind of adamant nature
that there should be a political option on the table
is because he understands the map.
That's where that's coming from.
The other thing that is being reported
that I don't necessarily think is in good faith
is he said that Ukraine was not going to push Russia out
anytime soon.
When the media started reporting on that phrase,
Ukraine isn't going to push Russia out.
That's not what he said.
You have to include the whole sentence on that one.
Anytime soon, because it is now moving into a situation
where there is a higher concentration of Russian troops
in a smaller area, because they have lost so much territory.
So it becomes harder to move forward.
At the same time, it's virtually impossible
for Russia to move forward.
So it becomes a hammer and anvil situation.
There are only so many times
that the Russian military hammer can hit the Ukrainian anvil
before the hammer breaks.
And that's the situation.
If Russia pursues this, it is going to be a long war now.
And I don't see a high likelihood
of Russia ever really winning.
Milley, in the same conference
where they pulled that quote from,
he said that he gives Russia winning
less than a 1% chance.
But they pulled the quote that said,
you know, Ukraine can't push Russia out
and cut it off there,
rather than including the anytime soon part.
Everybody needs to understand if this goes on,
it's going to be a long war.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}