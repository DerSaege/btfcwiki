---
title: Let's talk about what you need to know in Georgia and a PSA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=69U7WiviG0A) |
| Published | 2022/11/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the control of the Senate with all eyes on Georgia.
- Mentions the possibility of the Democratic Party maintaining control without Georgia, but it's slim.
- Emphasizes the significant impact of the runoff in Georgia on Senate control.
- Points out the massive amounts of money both parties will invest in the Georgia runoff.
- Expects efforts to discourage voting and lots of mudslinging due to the high stakes.
- Notes the national attention on Georgia's runoff election.
- Provides information on Georgia's absentee ballot application deadlines.
- Believes that if people show up, the results will favor the Democratic Party.
- Points out reasons why the Democratic Party may have an edge in Georgia.
- Warns about historical efforts in Georgia to suppress certain votes and dissuade people from voting.

### Quotes

- "Both parties are going to just dump untold amounts of money into it."
- "The results should trend towards the Democratic Party, as long as people show up."
- "Just be aware of the obstacles that the state establishment is likely to throw up in your way."

### Oneliner

Both parties focus on Georgia's runoff, expect mudslinging, and obstacles to voting.

### Audience

Georgia voters

### On-the-ground actions from transcript

- Submit absentee ballot applications by deadlines (implied)
- Be aware of potential obstacles and efforts to discourage voting (implied)

### Whats missing in summary

Importance of active voter participation in the Georgia runoff. 

### Tags

#SenateControl #GeorgiaRunoff #VotingObstacles #DemocraticParty #BeInformed


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about control of the Senate
and what's next and how all eyes are on Georgia.
At time of filming, there is still a slim possibility
that the Democratic Party can maintain control of the Senate
without Georgia, but it's slim,
which means that runoff is going to matter a lot.
And both parties are going to just dump untold amounts of money into it
because it will decide which party controls the Senate.
There will probably be a concerted effort
to discourage people from voting.
There will probably be a lot of mudslinging,
exactly what you would expect from an election
when the stakes are this high.
The entire country will be watching what's going on in Georgia
and spending money to influence the outcome.
If you live in Georgia, this is something you need to know.
In accordance with Georgia law,
applications for absentee ballots must be received by your election office
11 days before any election primary or runoff.
Please note, due to state holiday,
applications for the December 6, 2020 runoff
must be received by your local office by November 28, 2022,
eight days prior to the election.
And it gives you a... it looks like it's a method to request.
I'll have this link down below.
The results of this should, as long as people show up,
they will trend towards the Democratic Party.
And there's a bunch of reasons for this.
The main one, Kemp won't be on the ticket.
And Walker's numbers were probably brought up a lot
by people who were really showing up to vote for Kemp.
So that should provide an edge.
But you can't count on that.
That's not a guarantee.
And it being Georgia, there is a long history of trying to make sure
that certain votes don't count.
So be aware that that's probably going to be at play.
And they're probably going to try to dissuade people from voting.
So you can vote however you want.
If this is something that you're going to choose to do,
just be aware of the obstacles that the state establishment
is likely to throw up in your way.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}