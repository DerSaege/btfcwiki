---
title: Let's talk about Trump in heels and Kari Lake....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZciFgggRndI) |
| Published | 2022/11/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the Cary Lake race in Arizona, where votes are still being counted.
- Messages from a MAGA faction expecting Cary Lake's victory were received early.
- The MAGA faction falsely reports desired outcomes as facts, leading followers to believe in inevitable victory.
- Disillusionment occurs when election results do not match predictions, leading to baseless claims of election system issues.
- Normal Republicans won in some states where election-denying MAGA Republicans lost on the same ticket.
- The rejection of ultra-nationalism post-Trump is evident in Cary Lake's close race.
- Even if Cary Lake wins, it won't signify a continued victory for MAGA.
- The United States historically rejects authoritarian nationalist philosophies.

### Quotes

- "You keep acting like MAGA has failed. What about Governor-elect Cary Lake? She's trumping hills, boy."
- "This isn't Democrats doing this to you. This is the Republican Party saying they're just not that into you."
- "The Lake race is very much a symbol of that."
- "It was way too close for it to be a sign that an authoritarian nationalist philosophy is something the United States wants to embrace."
- "Historically, the United States has always rejected that type of philosophy."

### Oneliner

Analyzing the Cary Lake race in Arizona reveals the false reporting of desired outcomes as facts by the MAGA faction and the rejection of authoritarian nationalism in the United States.

### Audience

Voters, political analysts

### On-the-ground actions from transcript

- Stay informed about political factions and their narratives (suggested)
- Reject baseless claims and misinformation (suggested)
- Advocate for transparent and fair election processes (suggested)

### Whats missing in summary

Full understanding of the implications of the Cary Lake race and the rejection of authoritarian nationalism.

### Tags

#CaryLake #MAGA #RepublicanParty #ElectionResults #Nationalism


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
Cary Lake out in Arizona and what we can kind of infer from how things are going there.
Because at time of filming, that race still isn't finished. Votes aren't all counted.
But there was an expectation from a certain subset of the Republican Party for this race.
And I got messages very early on after the polls had closed about that race.
And we're going to go through one of them and then kind of use that to illustrate a
point. You keep acting like MAGA has failed. What about Governor-elect Cary Lake?
She's trumping hills, boy. And she's winning. And I wait to see you cry.
We had a few setbacks overall, but MAGA is winning and will stay winning.
And when she takes office, she'll... and then a giant list of stuff about the election and
cleaning it up and all of this stuff. Now I'm assuming at the time this message
was sent, Lake actually was winning. At time of filming, she's down by 31,000 votes.
I don't know that I'll be crying about that. But this kind of leads into something.
This faction of the Republican Party, the MAGA faction, they're pundits.
They report what they want to happen as fact. And when those commentators do that, the base,
the people listening to them, they believe it. So much so that they are certain of victory
going into an election when all signs suggest otherwise.
And then when something doesn't go the way they had hoped, they use that as evidence
that there's something wrong with the election system.
But that's not evidence. That's just you hoping against hope that
the inevitable doesn't occur. There are a number of states in which a normal
Republican won and the election-denying, baseless, claim-supporting MAGA Republican lost on the
same ticket. That means you have Republicans and Independents
voting for the normal Republicans and then crossing party lines on the other ones.
This is the base. This isn't Democrats doing this to you.
This is the Republican Party saying they're just not that into you.
That's not the end of Trumpism as a political force.
It very well might be the end of Trump himself as a political force.
But Trumpism will live on for a couple more election cycles.
And those who are opposed to that form of nationalism, they're going to have to be on
guard. But we also have to be on guard for those
people who have been led so far down that road that they truly believe that it was supposed
to go the other way. That Trump in the hills should have won because
that's what their commentators told them. That's what they were predicting.
And not even with the caveat that it's a prediction.
Just this is what's going to happen. And then when it doesn't, it's an even larger
letdown. I would also point out that even if Lake
does pull it off at this point, which seems unlikely, but it's not impossible, that doesn't
mean that MAGA is winning and will continue to win.
She is Trump in the hills and it's a nail biter in an area that really should have gone
for her. They didn't.
The ultra nationalist stuff, after Trump got elected, within those first two years, people
saw what it was and they rejected it. And they're going to keep rejecting it.
The Lake race is very much a symbol of that. There were a lot of people who truly believed
she was going to win. The conservative right wing and particularly
the far right were just fawning all over her. But at this point, it doesn't look like she's
going to pull it out. And even if she does, it was way too close
for it to be a sign that an authoritarian nationalist philosophy is something the United
States wants to embrace. They don't.
Historically, the United States has always rejected that type of philosophy.
In fact, we joined a whole world war over it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}