---
title: Let's talk about Colorado and what we can expect....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gLvpP7UM2IU) |
| Published | 2022/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent events in Colorado prompt speculation and assumptions in the media about why they occurred.
- The club that was attacked has been the target of hateful and false rhetoric from political commentators and politicians.
- The arrested suspect's background may reveal a connection to domestic violence (DV) and provide insight into the motive behind the attack.
- The suspect's social media consumption of hateful rhetoric may be used as a mitigating factor in sentencing.
- Individuals spreading false information that influenced real-world consequences may face legal consequences, similar to the Jones case.
- Commentators and politicians spreading hateful rhetoric often do so knowingly for political gain or profit, despite being aware of its falsehood.
- The business model of spreading misinformation is not sustainable in the long term, as shown in the Jones case.
- Putting out positive messaging that does not incite violence comes at no additional cost and can be part of the solution.
- Previous targeted rhetoric towards specific demographics has contributed to negative outcomes.
- Beau anticipates that the current situation may differ from past cases due to the suspect's arrest and the Jones case's precedent.

### Quotes

- "It costs nothing to be part of the solution."
- "Putting out positive messaging that doesn't lead to violence doesn't increase your overhead."
- "They're doing it because they feel it's good for their political base or it'll make them money, drive engagement, whatever."
- "Many of them have responded to the fact-checks. And they continue to push out false information."
- "I think it's reasonable to expect that a lot of the personalities that have pushed this kind of hateful rhetoric are going to end up in court."

### Oneliner

Recent events prompt speculation on Colorado incident, linking hateful rhetoric to real-world consequences and potential legal accountability.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Fact-check and challenge hateful rhetoric in your community (implied).
- Support positive messaging that fosters unity and understanding (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of hateful rhetoric from media personalities and politicians on real-world consequences, potentially leading to legal repercussions.

### Tags

#Colorado #HateRhetoric #LegalAccountability #MediaInfluence #PositiveMessaging


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about what we can expect to happen from here.
If you don't know, something happened in Colorado.
And in the media, there are a bunch of assumptions and speculation about why.
The thing is, a lot of them are safe.
They're pretty safe assumptions.
They are speculation at this point, and we need to note that.
We don't know that it's true.
But they're relatively safe to operate under the assumption that it is true, until some
evidence shows up to suggest that it isn't.
So this club that was attacked, clubs like this have become the target of hateful and
false rhetoric from political commentators and politicians.
The person who appears to be responsible at this point, they were arrested.
See that's a little different than normal.
So what are we going to find out about this person?
Odds are we will find out that there's a DV connection.
Why?
Because there almost always is.
We will probably also gain a little bit more insight into what prompted them.
Why that place was selected.
Because in this case, the person was caught.
They were caught.
Which means even if they don't go to trial, they will likely have a defense when it comes
to sentencing.
What was used as a mitigating factor, something a defense presented as a mitigating factor
when it came to January 6th?
Their social media diet.
How they consumed hateful and false rhetoric.
That's something that the defense is probably going to present.
Now the politicians and commentators who put out this hateful rhetoric, they probably won't
be held criminally responsible.
It's the way of the world.
However, in this case, because this has been going on a while, there have been a lot of
the people putting out this rhetoric, these claims, that know they're false.
They've been fact-checked.
Many of them have responded to the fact-checks.
And they continue to push out false information that the defense is certainly going to try
to show contributed to the actions of the suspect.
Knowingly putting out false information that led to real-world consequences.
See the thing is, a lot of these commentators and politicians, they don't actually believe
what they're saying.
They know that it isn't true when they put it out.
They know it's not true.
They're doing it because they feel it's good for their political base or it'll make them
money, drive engagement, whatever.
But they know it's false.
They're putting it out anyway and it leads to real-world consequences.
That sounds a whole lot like the Jones case.
That sounds a whole lot like the Jones case.
I think it's reasonable to expect that a lot of the personalities that have pushed this
kind of hateful rhetoric are going to end up in court.
And I don't know that there's going to be much of a defense because in many cases the
commentators themselves were de-platformed for spreading misinformation or they were
fact-checked and they just went with it anyway because it was good for a buck.
The Jones case demonstrated that this business model isn't actually profitable in the long
term.
And I have a feeling that there are going to be people who find that out.
It costs nothing to be part of the solution.
It costs nothing to put out positive messaging that doesn't lead to violence.
It doesn't increase your overhead.
This rhetoric we have seen that has targeted this demographic, we know that it's contributing
to this.
It's worth remembering that it wasn't even that long ago I did a video talking about
how Elements Aligned with the Republican Party put out $50 million in ads during the midterms
to denigrate this particular group of people.
I do not think that this story is going to play out like many of the ones before.
I think there's going to be a difference.
One, because the person was caught and two, because of the precedent set by the Jones case.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}