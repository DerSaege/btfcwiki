---
title: Let's talk about Trump settling in....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G1ZP-G_z8SA) |
| Published | 2022/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump settled a case dating back to 2015 involving disparaging remarks about people from Mexico.
- Demonstrators were allegedly assaulted by Trump's private security during a protest.
- Michael Cohen reportedly witnessed Trump instructing security to "get rid of the protesters."
- A sign carried by demonstrators read, "make America racist again," with claims that it was taken back into the building as a trophy.
- Despite Trump's claim of never settling cases, his team settled this one, showcasing his legal strategy.
- Trump's delaying tactics in legal cases were evident, with him being deposed in 2020 and settling right as the jury selection began in 2022.
- Both sides claimed victory after the settlement.
- The lawyer for the demonstrators emphasized that while rich and powerful people can put their names on buildings, sidewalks will always belong to the people.

### Quotes

- "I don't settle cases. I don't do it because that's why I don't get sued very often, because I don't settle, unlike a lot of other people."
- "Trump's overall legal strategy in action."
- "Delay, delay, delay. It's what Trump does."
- "Rich and powerful people can put their name on buildings. But the sidewalks, well those will always belong to the people."

### Oneliner

Trump settled a case involving disparaging remarks, showcasing his delaying legal strategy, despite claiming he never settles.

### Audience

Legal observers

### On-the-ground actions from transcript

- Contact legal aid organizations for assistance (implied)
- Support demonstrators' rights by joining protests or providing aid (implied)

### Whats missing in summary

Details on the specific disparaging remarks made by Trump and the impact on the demonstrators.

### Tags

#Trump #LegalStrategy #Settlement #Demonstrators #DelayingTactics


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
another one of Trump's legal developments. There's a lot of them going on. This is one
that will probably kind of get lost in the coverage, but it's worth mentioning. This
one dates back to 2015. But before we get started, I want to read a quote. I don't settle
cases. I don't do it because that's why I don't get sued very often, because I don't
settle, unlike a lot of other people. Truly a master of the English language. Given the
fact that I am starting off with a quote about Trump saying how he never settles cases, you
would probably guess that this is a video about him settling a case. And you would be
right. So in 2015, he made some disparaging remarks about people from Mexico. This prompted
demonstrations. One of them took place in front of one of his buildings. The plaintiffs
allege that his private security, well, they assaulted him. And there is video of part
of what occurred. And it appears that Michael Cohen was ready to say, or perhaps had said
on video, that he witnessed Trump saying, get rid of the protesters, to his security.
One of the signs being carried by the demonstrators said, make America racist again. And from
some of what we have gathered and what has been reported, that was carried back into
the building as a trophy. So Trump team has settled this case. Not truly surprising, given
that despite his statements, he actually has a decent record of settling cases. The reason
I want to kind of point this one out is because it shows Trump's overall legal strategy in
action. This began in 2015. He wasn't deposed until 2020, because while he was playing golf
during all that time, he said he was too busy and couldn't be bothered with the conversation,
I guess. But he was deposed in 2020. And then right as the jury is being selected in 2022,
well the case gets settled. Delay, delay, delay. It's what Trump does. It is his, it's
his style. It's his thing when it comes to legal cases. So don't be surprised if that's
what he continues to do. Of course, both sides in this case, because it was settled, both
sides are claiming victory in some way. The lawyer for the plaintiffs, for the demonstrators,
did say something kind of poetic once the news broke, and that was that rich and powerful
people, they can put their name on buildings. But the sidewalks, well those will always
belong to the people. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}