---
title: Let's talk about holding your vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bLWAd7BKBo4) |
| Published | 2022/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Some voters are considering withholding their vote for the Democratic Party because they believe it didn't deliver for them in 2020.
- Holding your vote might inadvertently benefit the Republican Party, which could be detrimental to your interests.
- Voting is just one form of civic engagement among many, and politicians are not saviors.
- Radical change cannot be expected from the Democratic Party without actively participating in primaries to push for candidates who truly represent your causes.
- Voting alone is not sufficient to shape policy; it requires active engagement in the electoral process to ensure your goals are met.
- Frustration with the Democratic Party's performance is understandable, but as a voter, you have the power to set expectations and demand change.
- Instead of withholding your vote, the focus should be on actively engaging in the primary process to support candidates who share your beliefs.
- Society changes first, and laws follow suit; getting candidates who believe in your values through primaries is key to effecting real change.
- The Democratic Party may prioritize political expediency over enacting meaningful change desired by its supporters.
- Ultimately, voters have the power to hold politicians accountable and demand better representation by actively participating in the electoral process.

### Quotes

- "Voting is the least effective form of civic engagement."
- "Politicians they're not saviors."
- "You change society, the laws follow."
- "You replace that employee."
- "Society changes first, and laws follow."

### Oneliner

Some voters question withholding their vote from the Democratic Party, but active engagement in primaries for candidate alignment is key to real change, as voting alone isn't enough.

### Audience

Voters

### On-the-ground actions from transcript

- Get involved in the primary process to support candidates who truly represent your beliefs (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on the limitations of voting and the importance of actively engaging in the electoral process to drive meaningful change.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk about the idea
of holding your vote because I've had a number of questions
come in from people who basically feel
like the Democratic Party didn't deliver for them
after they went out in 2020.
And they're all basically saying the Democratic Party would not
of one if it wasn't for my demographic and they're all women and they're all
right to be honest. So the idea is that the Democratic Party didn't deliver, okay.
So why should you vote for them again? Okay. I think that there is room to
to discuss whether or not the Democratic Party did deliver with the razor-thin margin, however
that's not really the question because that's how you feel about it and I don't know what
your expectations for them were.
Your expectations might have been much higher than mine, so you definitely feel like they
didn't deliver.
Okay, so if you hold your vote, what happens?
If you're somebody who would normally vote and you choose not to, to in some way punish
the Democratic Party, you are in some way giving an edge to the Republican Party, a
party that you know is coming for you.
If you vote for them, you get an ineffective ally or at least somebody who isn't an active
harm based on your perception of it.
That's one way to look at it.
Another is to remember what voting is.
I feel really uncomfortable getting the number of questions I'm getting about, you know,
give me a reason to go vote.
Voting is the least effective form of civic engagement.
That's what goes on every couple of years in addition to all of the other forms of civic
engagement that a person could be involved in.
Voting isn't the end all be all.
politicians they're not saviors. The Democratic Party is not a tool for
radical change and if that's what you were expecting because nobody really
went into specifics about what they expected the Democratic Party to deliver
on. If you're expecting radical change you can't get it that way. You can't get
it through the Democratic Party unless you radically change the Democratic
party through the primaries, and you get your candidate, somebody who you know is
down for your cause, whatever your cause may be, and you get them on the ballot.
Showing up every couple of years, I mean yeah that's okay, that is part of
the electoral process, but if you're wanting to shape policy and you're
wanting to use the electoral process to deliver your goals. You have to make sure
you get a candidate there that actually even has the intention. Because for a lot
of people, yeah, the Democratic Party, they're not an active harm, but they're
not your savior. If you hold the vote, you might end up with an active harm. You
might end up with a candidate who is literally coming for your rights.
But again, I'm really uncomfortable answering questions about voting.
I don't endorse candidates.
I don't think that that's my place.
But if you want some food for the conversation, that's how I would look at it.
I've never viewed voting as the ultimate tool to fixing society.
You change society, the laws follow.
And you're going to have to get your candidate, a candidate who actually believes in what
you believe on the ballot, you have to get them through the primary process.
Otherwise, you're going to end up with a party that is very much about doing what is politically
tenable overall, not really about creating the change that I think the people who sent
messages want.
So I get it, you're frustrated.
You definitely didn't get what you wanted based on the messages.
They didn't deliver to your expectations and you're the voter, you're their boss.
You get to have expectations.
If you're running a business and you're the boss and one of your employees doesn't deliver,
You don't start advertising for the other competing business.
You replace that employee.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}