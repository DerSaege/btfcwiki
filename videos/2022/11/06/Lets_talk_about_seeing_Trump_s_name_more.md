---
title: Let's talk about seeing Trump's name more....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HKyri8bFYO4) |
| Published | 2022/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's name likely to appear more in headlines after the midterms due to several converging events.
- Committee extended deadline for Trump to turn over documents for subpoena to November 6th.
- Trump under subpoena for deposition testimony starting on November 14th.
- Trump known for delaying tactics to avoid compliance with subpoenas.
- Trump's aides hinting at his announcement to run in 2024 shortly after the midterms, possibly on November 14th.
- Department of Justice expected to be very active post-election.
- Trump's potential announcement may complicate DOJ activities but not halt them.
- Trump likely to use delay tactics to his advantage in legal matters.
- Anticipate a surge of news about Trump, especially around November 14th.
- Multiple investigations into the former president likely to intensify post-midterms.

### Quotes

- "Delay, delay, delay."
- "Immediately after the elections, expect a lot of news about Trump, particularly around the 14th."
- "Most Americans are just at the point where they're like, don't go away, man."

### Oneliner

Trump's legal battles and potential 2024 announcement set to dominate headlines post-midterms, particularly around November 14th.

### Audience

Political analysts, news followers

### On-the-ground actions from transcript

- Stay informed about the latest developments in Trump's legal and political activities (suggested).
- Engage in critical analysis and discourse regarding the impact of Trump's potential actions on the political landscape (suggested).

### Whats missing in summary

Insights on the potential implications of Trump's legal battles and political aspirations post-midterms.

### Tags

#Trump #LegalBattles #DOJ #Elections #2024Election


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about how it is incredibly likely
that we start seeing Trump's name in headlines even more frequently right after the midterms
because we have a bunch of things converging directly after the midterms.
The first is the 6th, the committee extended his deadline to start turning over documents
for the subpoena.
And they say, we have informed the former president's counsel that he must begin producing
records no later than next week and he remains under subpoena for deposition testimony starting
on November 14th.
Remember that date?
And it goes on to say it's going to be continuing on subsequent days as necessary.
The subpoena is pretty broad.
So Trump has a lot of stuff that he is supposed to turn over.
However, it's Trump.
The tactic?
Delay, delay, delay.
OK, but November 14th.
Trump's team, the aides, have leaked the idea that Trump will announce that he is running
in 2024 shortly after the midterms.
Want to take a guess as to what the date they kind of hinted at was?
November 14th.
So either that is just a scheduling conflict or you can expect a very Trump show.
On top of that, with all of the moves that the Department of Justice has been making,
we can expect just a flood of DOJ activity right after the election.
Now the question is whether or not they're going to do that in the following six days
right after the election.
If he does announce, it's going to complicate things for the Department of Justice.
It's not going to stop things.
It's just going to delay them.
Trump's team knows that and given Trump's habit and typical legal strategy of delay,
delay, delay, we can probably expect him to try to use that to his advantage.
So what this means is immediately after the elections, expect a lot of news about Trump,
particularly around the 14th.
That's when I would imagine that a lot of stuff is going to start to break in the multiple
investigations into the former president.
I know that most people are not super excited about that news.
I think most Americans are just at the point where they're like, don't go away, man.
Just go away.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}