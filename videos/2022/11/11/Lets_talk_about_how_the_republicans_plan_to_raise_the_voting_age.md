---
title: Let's talk about how the republicans plan to raise the voting age....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R-rpDVlX-JY) |
| Published | 2022/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the Republican strategy to avoid a repeat of the midterms.
- The plan includes raising the voting age to 21, revealing their priorities.
- Beau finds this plan brilliant as it exposes the Republican Party's true motives for power.
- Republicans are willing to deny people their rights to maintain power.
- Beau suggests openly admitting to raising the voting age to show their intentions.
- Pointing out the irony of not raising the age to buy a rifle but wanting to raise the voting age.
- Younger Americans are more progressive and less likely to conform to Republican ideals.
- Negative voter turnout is a result of the Republican Party's outdated tactics.
- Referencing the 26th Amendment's background in empowering young voters.
- Beau criticizes the Republican Party for valuing power over representing constituents.

### Quotes

- "They don't care about the Republic. They have no concept."
- "Power for power's sake."
- "Denying people their rights, it's not gonna work."
- "Not representing their constituents, but ruling them."
- "Y'all have a good day."

### Oneliner

Analyzing the Republican strategy to raise the voting age to 21 reveals their power-hungry motives, disregarding the rights of citizens and alienating younger, more progressive voters.

### Audience

American voters

### On-the-ground actions from transcript

- Advocate for fair voting rights (exemplified)
- Support voter education initiatives (implied)

### Whats missing in summary

Full context and depth of Beau's analysis on the Republican Party's strategy and motivations.

### Tags

#RepublicanStrategy #VotingAge #PowerHungry #ProgressiveVoters #26thAmendment


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about the Republican strategy,
how they plan to avoid having a repeat of these midterms,
because they've come up with a plan.
And I personally, I hope they do it.
I hope they try to enact this,
because I think it's brilliant.
It will show the American people
the true level of thought in the Republican Party.
You have a number of prominent Republicans, conservatives,
and those within the MAGA faction
advocating to raise the voting age to 21.
Yeah, okay.
I mean, point of order.
First, understand, that's actually part of the Constitution.
Now, that's gonna be a little bit harder to change
than you might imagine.
But overall, I think it's a brilliant plan,
because it's gonna show who you are.
It will show the country
exactly what the Republican Party has become,
a party that cares about power for power's sake.
They don't care about the Republic.
They have no concept.
They want power, and that's it.
And they're willing to deny people their rights,
their constitutionally protected rights, to achieve it.
They're saying it openly.
I also think it would be a great idea, just a grand idea,
for the Republican Party to openly admit
that they will raise the voting age to 21
and deny people their rights to maintain their own power.
But there's no way they'll raise the age
to buy a rifle to 21 to protect kids.
Do it. Tell the American people this.
Open. It's fantastic.
I also don't know what they think it's gonna achieve.
Three years?
It's not like the 21-year-olds
are suddenly gonna be Republicans.
Younger Americans are more tolerant.
They are more accepting.
They are more looking towards the future.
That's what the Republican Party has to understand.
That hateful rhetoric, it's not gonna work.
Denying people their rights, it's not gonna work.
That's what drove negative voter turnout this time.
They learned nothing.
I also want to give a little bit of a backdrop.
As they complain about the young people,
you know, influencing policy,
that was actually the point.
The 26th Amendment, I want to say March of 1971,
it was passed, ratified in July of 1971.
1971.
The draft.
At 18 years old, this country can summon someone
and send them off to the grinder.
But apparently the Republican Party doesn't trust them enough to vote.
I think it would be a wonderful idea
for the Republican Party to admit that's where they're coming from.
Power for power's sake.
Not representing their constituents, but ruling them.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}