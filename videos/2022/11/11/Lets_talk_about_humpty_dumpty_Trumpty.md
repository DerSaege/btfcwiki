---
title: Let's talk about humpty dumpty Trumpty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kKCPmOQEnlE) |
| Published | 2022/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Party trying to blame Trump for their midterm losses.
- Trump was elevated and enabled by political personalities and politicians.
- Trump shares blame, but the real issue is Trumpism.
- Republican Party making the same mistake again.
- Leaders following along out of fear of Trump's tweets.
- Elitist attitude of blaming Trump for falling off the wall.
- Voters pushed Trump off the wall by being tired of him.
- Trump led the party to multiple losses after winning one election.
- Enabling and encouraging Trump's behavior contributed to the current situation.
- Real problem lies in following and enabling Trumpism, not just Trump himself.

### Quotes

- "They put him on the wall. They elevated him. They backed him. They enabled him."
- "Thinking that your most vocal supporters are representative of the entire country, that's on y'all."
- "He was pushed by voters that are tired of him and tired of you."

### Oneliner

The Republican Party's attempt to blame Trump for their losses reveals the deeper issue of Trumpism and enabling, ultimately leading to voters pushing him off the wall.

### Audience

Voters, political personalities

### On-the-ground actions from transcript

- Hold political personalities and politicians accountable for enabling and elevating Trump (implied).
- Recognize the dangers of Trumpism and work towards dismantling it within political parties (implied).
- Support leaders who prioritize the interests of the entire country over fear of a tweet (implied).

### Whats missing in summary

Full understanding of the dynamics and consequences of Trumpism and enabling behavior in politics.

### Tags

#RepublicanParty #Trumpism #Accountability #PoliticalLeadership #Voters


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Humpty Dumpty Trumpty
and how the Republican Party
seems to be
angling to put
everything on Trump
when it comes to their dismal
results
in the midterms.
That they're looking to
kind of place everything on him.
Humpty Dumpty Trumpty
fell off the wall.
Yeah, I mean that happened, but not in the way
I think a lot of them are picturing.
Because here's the thing.
You want to compare Trump to an egg in an elevated position?
It matches.
But there's two things
we know about an egg
that is sitting up like that.
The first is that nobody knows why he's there.
And the second is that he didn't get there by himself.
A whole lot of the political personalities and politicians
that are now
basically trying to pin everything on Trump,
they put him on the wall.
They put him up there. They elevated him. They backed him. They enabled him.
They said nothing
when
his rhetoric inflamed the country.
They said nothing
when he said things that were not true.
That limited
your voting base
when he downplayed certain events.
They said nothing
as he espoused hateful rhetoric
that alienated
massive
amounts of Americans.
Y'all put him up there.
Trump shares
a whole lot of the blame for this.
Absolutely.
But Trump's a person.
The real problem is Trumpism.
Following along with that.
Enabling that.
Letting this country be
ripped up
the way
it has been.
That's part of the real issue there.
And the thing is the Republican Party is going to make the same mistake again
because they don't get it.
I don't think they've learned their lesson yet.
Trump is responsible
for a whole lot of this.
A whole lot of the responsibility also
lays on those people pointing their fingers at Trump right now.
Y'all enabled this.
Y'all encouraged this.
He won
one election
and then led the party to loss after loss after loss.
And y'all,
leaders,
are
just following along. Y'all are following down the path
because you're afraid of a tweet.
That's on y'all.
Thinking that your most vocal supporters are representative of the entire country,
that's on y'all.
The other thing
that is worth noting
is the elitist attitude
behind this idea
that he fell off the wall.
Nah. Humpty Dumpty did not fall.
He was pushed
by voters
that are tired of him
and tired of you.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}