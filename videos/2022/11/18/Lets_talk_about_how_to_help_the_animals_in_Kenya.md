---
title: Let's talk about how to help the animals in Kenya....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Bh6a4IW6h68) |
| Published | 2022/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing how to help with the drought situation in Africa, specifically focusing on animals like elephants and hippos.
- Recommending support for David Sheldrick's Wildlife Trust, a massive organization known for its work with elephants but extends help to various animals.
- Mentioning the Trust's efforts in building solar-powered water distribution plants and trucking in water to support wildlife during the drought.
- Noting the importance of funding in addressing the crisis, especially due to the lack of political will.
- Encouraging people to support the Trust through donations or symbolic adoptions of animals like elephants, rhinos, or giraffes.
- Pointing out that the Trust has a wish list of needed items and is currently caring for an influx of orphaned elephants.
- Emphasizing the impact of grass scarcity on hippos due to the water shortage in the region.

### Quotes

- "You want to assist, you want to help these animals that very well may not make it because of the drought, look to David Sheldrick's Wildlife Trust."
- "When funding is the problem, you absolutely can [help by providing money]. And in this case, the funding is the problem."
- "If this is something that is motivating you, you want to get involved, you want to help, you want to try to make a difference when it comes to these animals."

### Oneliner

Beau addresses how to support animals impacted by drought in Africa, recommending David Sheldrick's Wildlife Trust for donations or symbolic adoptions.

### Audience

Animal conservation supporters

### On-the-ground actions from transcript

- Support David Sheldrick's Wildlife Trust through donations or symbolic animal adoptions (suggested).

### Whats missing in summary

Details about the specific ways David Sheldrick's Wildlife Trust uses funds to support wildlife during the drought.

### Tags

#AnimalConservation #DroughtSupport #DavidSheldricksWildlifeTrust #Donations #SymbolicAdoptions


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about how you can help,
what you can do about the drought.
Because almost immediately after that video went out,
I had people sending messages,
basically saying that I seemed frustrated.
And I said that I didn't know what could be done.
How can people help?
I mean, I am frustrated.
But when you're talking about this region of Africa,
and you're talking about animals,
if you want to support any efforts that are going on,
there's always one outfit you can turn to to support.
And that is David Sheldrick's Wildlife Trust.
This is just a massive organization.
And I have supported them for years.
They do amazing work.
They are best known for their support of elephants.
However, it's not limited to just elephants,
even though that's what they're best known for.
This is an organization that, if I'm not mistaken,
I want to say a couple of years ago,
they built solar-powered water distribution plants.
And I think some of them were actually in Savo,
along with troughs, all kinds of stuff.
I think that currently, they're trucking in water.
And I've seen photos, because I follow them on social media,
I've seen photos of them distributing
like bales of grass to hippos.
Because without the water, the grasses are gone.
And without the grasses, well, the hippos are gone.
So they're trying to mitigate there.
They're doing everything that they can.
I know that on their website, they
have a wish list of stuff that they need.
I've also seen on their social media
that they're experiencing an influx of orphaned elephants
that they're going to have to care for.
So if you are motivated to get involved
in this particular situation, that's where I would go.
That is the organization that I would support.
A lot of times, people say, oh, well, you can't just
throw money at an issue.
Well, when funding is the problem, you absolutely can.
And in this case, the funding is the problem.
The lack of money to help is really
a large part of the issue, because the political will
isn't there.
So if you're looking to get involved in this,
if you're looking to assist, if you
want to help these animals that very well may not
make it because of the drought, look to David Sheldrick's
Wildlife Trust.
They have just options, tons of options, on how you can help.
They also have a cool program that I've
done before that is you adopt an elephant or a rhino,
or I think they even have giraffes.
Again, it's like the wolf thing that we talked about recently.
It's a symbolic adoption, but you get little updates.
And it's an interesting thing.
It might be a good Christmas present for people
who are big into wildlife.
And it'll help.
They make really good use of the funds that they have.
So if this is something that is really motivating you,
my guess is people saw some of the aerial images.
If this is something that is motivating you,
you want to get involved, you want to help,
you want to try to make a difference when
it comes to these animals.
I would say that this is your best route.
David Sheldrick's Wildlife Trust is the organization
I would trust with my money.
So anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}