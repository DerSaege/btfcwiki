---
title: Let's talk about a Special Counsel for Trump and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E71WM1RbflI) |
| Published | 2022/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Attorney General Merrick Garland is expected to appoint a special counsel to oversee criminal investigations into former President Donald J. Trump.
- There are two possible outcomes of appointing a special counsel, with vastly different implications, one related to Trump's documents case and the other to January 6th.
- Garland has been focused on keeping the Department of Justice (DOJ) apolitical, but the perception of special counsels as political may complicate matters.
- The Republican Party could portray the special counsel as a political fishing expedition, affecting how it is perceived by the country.
- The transparency and distance between Garland, the special counsel, and the administration could either protect the presidency or lead to a thorough investigation of Trump.
- It is uncertain whether the special counsel's appointment is to protect the institution of the presidency or to hold Trump accountable.
- Despite expectations of a lengthy process, a special counsel does not necessarily have to slow down the investigations.
- Keeping Trump in the headlines is inevitable as long as he remains a prominent figure in national politics.
- The appointment of a special counsel has occurred, marking a critical point in the ongoing investigations into Trump.
- The final outcome of these developments remains uncertain, with various possible scenarios depending on how events unfold.

### Quotes

- "We don't know what this means because it can mean kind of one of two things, and they are wildly different outcomes."
- "The pieces that are available can be used to frame two wildly different stories and two wildly different futures."
- "Humanity has made that mistake with people like him before."

### Oneliner

Attorney General Merrick Garland's appointment of a special counsel to oversee investigations into former President Donald J. Trump has sparked uncertainty and speculation about the potential outcomes and implications.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Monitor developments in the investigations into former President Donald J. Trump (implied)
- Stay informed about the implications of the special counsel's appointment (implied)

### Whats missing in summary

Insights on the potential consequences and impact of the investigations into former President Donald J. Trump could be better understood by watching the full video.

### Tags

#AttorneyGeneral #SpecialCounsel #Investigations #DonaldTrump #PoliticalAnalysis


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
Attorney General Merrick Garland
appointing a special counsel and what that means.
Because if you missed the news,
Garland is expected to appoint a special counsel
to oversee the criminal investigations
into former President Donald J. Trump,
both of them that we know of.
So, as soon as this news broke,
you had a bunch of commentators telling you what it means.
We don't know what it means.
There are a lot of people right now
who I feel are reporting their hopes as fact.
We don't know what this means,
because it can mean kind of one of two things,
and they are wildly different outcomes.
So let's go over what we know.
When it comes to the cases, you have the documents case.
Based on everything that is publicly available
to include Trump's statements, his own statements,
that's kind of open and shut.
The material facts, they're not really in dispute.
What's in dispute is Trump's belief
that it was okay for him to do this.
A special counsel makes no sense in that case.
Now you look at January 6th.
That's an entirely different species.
That's a case with a lot of moving parts,
with a whole lot of communications,
with a whole lot of people who,
who their stories have to be understood
and compiled to get a clear picture.
A special counsel kind of does make sense on that one.
What do we know about Garland?
Garland has been a stickler
for making sure that DOJ remain apolitical.
It shouldn't be partisan.
This has been a big thing for him.
The thing is, and Garland certainly knows this
because he's very conscious of perception,
because of the last few,
special counsels are perceived as political.
So it seems like an odd move there.
So, because of the last few,
there is no doubt that the Republican Party
will be able to successfully paint the special counsel
as an exploratory fishing expedition
and they're just snooping around.
They'll be able to do that for their base.
Now, whether or not that plays to the rest of the country,
we don't know that yet.
So at that point, you have a lot of transparency.
You have a lot of distance between the special counsel
and Garland and the administration.
So there could be, this could be the prelude
to the institution of the presidency being protected,
meaning Trump's gonna get a slap on the wrist, if anything.
The alternative to that is that Garland is completely aware
of how a special counsel is perceived
and he doesn't like it.
So the transparency, the distance, all of that,
yeah, that's to make sure DOJ remains apolitical.
And at the same time, it could be used to put teeth
back into the special counsel,
back into that idea, that mechanism.
And if that's the case, well, they're gonna sink Trump.
The thing is, we don't know which it is.
I don't think anybody knows.
Even if Garland's expectation is to put teeth
back into the special counsel,
there's no way for him to know
that it's gonna play out that way.
The good news for everybody is that,
as expected shortly after the midterms,
was off by a couple of days,
but we're about to see a flurry of movement.
And here it is.
This isn't actually what I was expecting, to be honest.
I kind of expected them to move forward
with the documents case and eventually
just throw it to a jury.
The special counsel, it doesn't necessarily
need to slow things down.
That's something that a lot of people are suggesting,
is that this is gonna drag on and on and on
because of the last few.
It doesn't have to work that way.
There is no expectation that a special counsel
takes a really, really long time to do something.
That's not a requirement.
So it doesn't necessarily have to slow everything down.
But it is going to keep this in the headlines,
which we're gonna be talking about that soon.
As long as Trump is a figure on the national scene,
he has to be talked about.
Can't ignore him.
Can't think that he has been contained.
Humanity has made that mistake with people like him before.
So the news is out.
There's gonna be a special counsel, at least
all reporting suggests that.
But as far as what we can infer from it, not a lot.
Because the pieces that are available,
they can be used to frame two wildly different stories
and two wildly different futures.
So at this point, we know this has occurred.
They are in what could be perceived as an endgame now.
But we don't know what the final scene's gonna be on this one.
And I don't think that...
I don't think those who are saying 100% certain
it's gonna go this way,
I think they may be letting their hopes
get in the way of their better analytical practices.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}