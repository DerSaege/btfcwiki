---
title: Let's talk about 50 million spent by the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IpTTr11mcaA) |
| Published | 2022/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses political advertising and the exorbitant spending on specific issues.
- Focuses on the Republican Party spending a minimum of $50 million on anti-LGBTQ advertising.
- Points out that $50 million was spent solely on advertising and doesn't include other platforms like social media.
- Reveals that the target of the advertising was trans kids, with a small number of kids starting hormone therapy.
- Emphasizes the discrepancy between the amount spent and the actual number of affected kids.
- Breaks down the cost to bully each kid, which amounts to around $12,000.
- Criticizes this behavior as playground bully mentality and propaganda.
- Condemns those who support such divisive and harmful tactics.
- Calls out the manipulation and shame associated with supporting these actions.
- Concludes by urging viewers to be aware of manipulation and not fall for divisive propaganda.

### Quotes

- "They did it to manipulate you."
- "The propaganda is bad and you should feel bad."
- "If you're looking down at those people, you're not paying attention to them as they beg you for money."

### Oneliner

The Republican Party spent $50 million on anti-LGBTQ advertising, targeting trans kids, showcasing a playground bully mentality to manipulate public opinion and divide the country.

### Audience

Voters, Activists, Allies

### On-the-ground actions from transcript

- Stand against divisive propaganda (exemplified)
- Support LGBTQ youth (exemplified)

### Whats missing in summary

Deeper insights into the impact of political advertising on vulnerable communities and the importance of standing up against such divisive tactics.

### Tags

#PoliticalAdvertising #LGBTQ #Anti-bullying #CommunitySupport #Propaganda


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about political advertising
and how much money gets spent to reach out on specific issues.
We're going to talk about one in particular because I saw some reporting on it
and it was really hard to wrap my mind around.
So we're going to talk about the money and what it gets used for
and why it gets used the way it does.
Reporting suggests that elements within the Republican Party
spent a minimum of $50 million dollars
in the run-up to the midterms. A minimum.
This is just the stuff they tracked.
$50 million dollars to reach out on one specific issue.
Can you guess what that issue is?
The advertising was anti-LGBTQ.
Specifically, going after kids.
Going after trans kids.
$50 million dollars.
And this is just the advertising buys.
This is just money spent on advertising that they tracked.
This doesn't include all the times they tweeted about it.
This doesn't include the times they talked about it in some sympathetic outlet
putting on the news.
$50 million dollars to tell people who to hate.
Who to look down at.
Take a guess as to how many kids started hormone therapy last year.
Just off the top of your head.
About 4,200.
I want to say that the exact number is 4,231.
4,200.
Less than a hundred kids in each state.
$50 million dollars on this issue.
It's wild.
The way they talk about it, you would think those numbers would be a whole lot higher.
And it's funny because we've actually seen people quote numbers that are way higher than
this.
Because of how much they've talked about it, they themselves are now starting to believe
it is really widespread.
4,200.
Less than a hundred kids in each state.
$50 million dollars.
You know another cool breakdown?
With 4,200 as the baseline, how much did they spend to bully each kid?
About $12,000.
They spent $12,000 per kid to villainize them.
Scapegoat them.
To tell people it was okay to look down at them.
Kick down at them.
That's the family you can point at because they're different.
Playground bullies all grown up.
It's pathetic.
The thing is, that's exactly what it is.
This isn't an issue they actually care about.
It's playground bully mentality.
They're different.
They don't have enough friends.
They don't exist in big enough numbers to take a stand by themselves.
So it's okay.
Other them.
Blame them.
Point at them.
And hope that the bully doesn't turn to you.
It's that same playground mentality.
It's garbage.
$50 million dollars wasted dividing the country.
Intentionally.
Going after kids.
The propaganda is bad and you should feel bad and if you voted for any of these people
who engaged in this, you should be ashamed of yourself.
They did it to manipulate you.
If you're looking down at something that they have deemed as different, if you're looking
at those people, you're not paying attention to them as they beg you for money.
They really need your support so they can tell you who to hate.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}