---
title: Let's talk about whether a Democratic tactic worked....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G1nBHzGvTMY) |
| Published | 2022/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democratic Party's tactic of elevating certain Republican candidates during primaries to ensure an easier win was employed in six high-profile places.
- The tactic was used in Arizona, Illinois, Maryland, Michigan, New Hampshire, and Pennsylvania.
- This strategy worked well, handing the Democrats some significant victories and contributing to stopping a red wave.
- However, there are risks associated with adopting this tactic as a long-term strategy.
- Elevating extreme candidates may condition Republicans to support their radical policies even after the election solely based on party loyalty.
- While it made sense in specific instances, adopting it widely could have negative consequences.
- The only scenario where this tactic might make sense as a strategy is if Trump wins the nomination and decides to run again in 2024.
- It's vital to distinguish between a tactic, used situationally, and a strategy, which should focus on delivering for the people consistently.

### Quotes

- "It worked incredibly well. It handed them some incredibly significant victories."
- "The all-encompassing strategy is to deliver for the people that put you into office."
- "We have to draw that line between tactic and strategy."

### Oneliner

Democratic Party strategically elevates certain Republican candidates in key places for electoral success but should be cautious about adopting it as a long-term strategy to prevent unintended consequences.

### Audience

Political Strategists

### On-the-ground actions from transcript

- Analyze electoral tactics used in key places to understand their impact (implied).

### Whats missing in summary

The full transcript provides an in-depth analysis of the Democratic Party's electoral strategy involving elevating Republican candidates and the potential risks associated with adopting this tactic as a long-term strategy.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
the Democratic Party's strategy tactic.
We've had this conversation before on this channel.
Their tactic of elevating certain candidates,
certain Republican candidates during the primaries
because they believed they would be easier to beat.
What the Democratic Party did was find some way
to assist the Trump-endorsed, MAGA, election-denying,
extreme Republican candidate during the Republican primaries
to elevate them.
They used this tactic in six high-profile places,
six places that matter.
And those places were Arizona, Illinois, Maryland, Michigan,
New Hampshire, and Pennsylvania.
So now we get to the part after the election.
Did it work?
It worked in Arizona, Illinois, Maryland, Michigan, New
Hampshire, and Pennsylvania.
Yeah, it worked.
It worked really well.
It worked incredibly well.
It handed them some incredibly important victories.
And it played a part in stopping that red wave.
So should this tactic become strategy?
No.
No, there's a risk associated with this.
The first is the immediate, that the far-right,
Trump-endorsed, MAGA candidate actually wins.
In the current political climate,
heading into the midterms, that was really unlikely.
So it made sense to do it then.
Now, as far as adopting it as a strategy
that they're going to use a lot, that's a different story.
Because the other risk is that by elevating
these candidates, you condition Republicans
to actually be used to and become
in favor of their wild policies, because they
want to support their policies.
We saw time and time again throughout the election
that there were a whole bunch of people
who voted solely based on the letter after the person's name,
whether they were a Republican or a Democrat.
Once you vote for somebody, if they win and they take office,
there is a habit to want to support their policies,
even if they are less than adequate.
So they shouldn't adopt this as a strategy, something
that they do across the board.
But as a tactic in key places, it makes sense.
The only time it would make sense to do it as a strategy
is if, well, if Trump actually wins the nomination
and decides to run again in 2024.
Then it would make sense as a strategy.
But other than that, it worked.
It delivered a lot of key wins.
But we have to draw that line between tactic and strategy.
Tactic is something that you can use occasionally,
something that is immediate, on the ground,
dependent on the situation at the time,
versus a strategy that is all-encompassing.
The all-encompassing strategy is to deliver for the people
that put you into office.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}