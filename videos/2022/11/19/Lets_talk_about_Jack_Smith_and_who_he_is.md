---
title: Let's talk about Jack Smith and who he is....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZkgL5cVBeFI) |
| Published | 2022/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing Jack Smith, a significant figure soon to be in the news with an impressive background.
- Jack Smith grew up in New York, attended Harvard Law, and worked with the New York District Attorney's Office.
- He later moved on to the US Attorney's Office in Brooklyn and then to the International Criminal Court.
- Smith also served as chief of Public Integrity for five years and became Assistant United States Attorney for the Middle District of Tennessee.
- In 2018, he took an appointment at The Hague regarding Kosovo and now holds a new appointment as special counsel.
- While at Public Integrity, Smith was known for being apolitical, which is a critical aspect of his character.
- His reputation suggests he is not someone looking to gain political favors or play political games.
- Smith's background and experience make him a potential candidate for those hoping for a prosecution, particularly involving Trump.
- Despite uncertainties, Smith's background indicates seriousness rather than a mere show of authority.
- The possibility of Smith being involved in prosecuting Trump, especially in cases regarding documents, seems promising.

### Quotes

- "This isn't the background of somebody who is looking to score chips in the big game and get political favors later."
- "If you were actually looking to prosecute, this is kind of the background you would look for."

### Oneliner

Beau introduces Jack Smith, hinting at his potential role in upcoming news, showcasing his extensive legal background, particularly his apolitical stance, making him a promising candidate for those seeking prosecutions.

### Audience

Legal enthusiasts

### On-the-ground actions from transcript

- Stay informed about the developments involving Jack Smith and his potential role in legal proceedings (implied).

### Whats missing in summary

Full context and depth of analysis.

### Tags

#Legal #Prosecution #JackSmith #Background #Apolitical


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about Jack Smith
and who he is. This is a person who is going to be in the news a lot. So let's just go ahead and run
through his background and kind of go from there. When you have a new character enter the show,
it's always nice to get a little bit of a backstory. First, I mean Jack Smith, man,
that's got to be hard. Every person he meets has got to be like, yeah, sure you are, and I'm Jim
Doe. Okay, Jack Smith grew up in New York, Harvard Law, started a legal career with the New York
District Attorney's Office after being in ADA, started working with the US Attorney's Office
in Brooklyn. So he's a fed now. There, did a lot of high-profile cases, some you would definitely
recognize. After that, went to work for the ICC, International Criminal Court. Then in 2010,
went to work with Public Integrity there at the Department of Justice. Was chief of Public
Integrity for five years, where they got a lot of politicians. They got a lot of politicians. He is
not afraid to indict a politician. Also got Jeffrey Sterling, who's a CIA guy who shared national
defense information. In 2015, became Assistant United States Attorney for the Middle District
of Tennessee. And then in 2018, took an appointment at The Hague looking into the unpleasantness in
Kosovo. Did very well there, and it's a four-year appointment. Got a second on May 8th of this year,
but now has the new appointment as special counsel. So, we talked about the two options,
protecting the institution of the presidency, showing that the special counsel has teeth.
This background, probably not the person I would bring in to protect the institution of the
presidency. Now, when Smith was at Public Integrity, very much developed a reputation as being apolitical.
There was a hearing involving the IRS that he was called in for, and he was called in for
that he was called in for, and he was basically asked questions about targeting on political basis,
and one of the answers was something to the effect of,
no, I was never asked anything like that, and nobody who knows me would even consider asking me that.
So, based on the background, if you were actually looking to prosecute,
this is kind of the background you would look for.
Doesn't mean anything, because we still don't know what's going on yet,
but this isn't the background of somebody who is looking to score chips in the big game
and get political favors later. So, that is hopeful for those that are hoping for a
prosecution of Trump, particularly on the documents case, this might be your guy.
Again, we don't know that, but it seems unlikely that somebody would get pulled from the Hague
to put on a show, and it seems unlikely that somebody with this background would agree to
leave a posting with the Hague to put on a show. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}