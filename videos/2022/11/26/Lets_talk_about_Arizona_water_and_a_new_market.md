---
title: Let's talk about Arizona, water, and a new market....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0MhmqY9uNrw) |
| Published | 2022/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Arizona’s Rio Verde Foothills faces a water crisis as Scottsdale plans to cut off water supply to the rural community.
- Scottsdale will bar trucks from exporting water to areas outside the city limits, leaving 700 homes in Rio Verde without reliable water sources.
- Despite warnings from Scottsdale to address their water supply issues, the rural community has not been proactive in finding a solution.
- Options for Rio Verde include developing their own water system, trucking in water from further away, or potentially resorting to a black market for water.
- Lack of local leadership and focus on divisive issues rather than community needs exacerbates the impending crisis.
- The situation in Rio Verde is a warning for communities nationwide to address climate-related challenges and prioritize real issues over political distractions.

### Quotes

- "Effective January 1st, Scottsdale is going to bar trucks from exporting water to those outside the city limits."
- "I think we know it [black market for water] will happen with 700 homes."
- "The United States needs to adjust its priorities, and it needs to do it quickly."

### Oneliner

Arizona community faces water crisis as Scottsdale cuts off supply, signaling a looming national challenge demanding urgent reprioritization.

### Audience

Communities, policymakers, activists

### On-the-ground actions from transcript

- Develop community-based water solutions in at-risk areas (exemplified)
- Advocate for local leaders to prioritize real issues over divisive politics (exemplified)
- Prepare for potential water crises by establishing alternative water sources (exemplified)

### Whats missing in summary

The full transcript provides additional context on the water crisis in Arizona and the need for proactive community and governmental responses to prevent similar situations nationwide.

### Tags

#Arizona #WaterCrisis #CommunityAction #ClimateChange #Priorities


## Transcript
Well, howdy there, internet people. It's Beau again.
So today, we are going to talk about Arizona and water and an emerging market and how we
can kind of expect situations similar to this to develop around the country in the near
future, even though they may not be identical to this.
There is a community in Arizona, a rural community called the Rio Verde Foothills.
Now traditionally, this area gets their water from Scottsdale, from a nearby city.
Has to be trucked in.
And that's what's been happening for years.
Even though Scottsdale for years has been saying, hey, y'all need to work out your own
thing.
Well, effective January 1st, Scottsdale is going to bar trucks from exporting water to
those outside the city limits.
So this rural community, which has 2,200 homes, is going to be cut off from their normal water
supply.
Out of those 2,200 homes, 500 of them rely on trucked in water solely.
There's an additional 200 that they have a well, but their well isn't completely reliable,
so they have to supplement with trucked in water.
That's 700 homes.
This is all Maricopa County.
Now what can the people in Rio Verde, in those foothills, what can they do?
Well, they could try to develop their own water system, which there's an idea for that.
However, when they tried to do it in the past, it didn't go well, because a lot of the people
in the community didn't want to basically give up what they would have to give up to
make it happen.
Another option would be to have people truck it in from further away, which is going to
make that water cost a whole lot more.
And those are the options that are really being reported on in the media.
What's another one?
One that people probably don't want to speculate about, but I think we know it would happen
with 700 homes.
Somebody from Scottsdale is going to do it anyway.
I mean, that seems pretty likely, right?
Seems as though, I mean, that'd be kind of hard to enforce.
I would imagine that shortly after January 1st, sometime in 2023, we will have a black
market for water in the United States.
I think it's going to happen.
I would point out that this is a situation that people knew, they knew this was coming.
But a lot of the local leaders there were more focused on other things, things they
could use to divide people, divide communities, rather than bring them together.
They're not addressing this.
Now understand, no matter where you live, there is something similar to this that could
occur near you.
There is a climate issue that your community needs to be working to figure out a plan for,
and more importantly, working to mitigate everywhere.
This is everywhere.
It's not just in the desert.
The thing is, most of the areas that are going to be heavily impacted, they are areas where
the politicians have gotten involved in culture war stuff, rather than focusing on real issues.
The United States needs to adjust its priorities, and it needs to do it quickly, because it's
about to get wild out here.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}