---
title: Let's talk about Poland, HIMARS, and Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xQeCPkFtJQ8) |
| Published | 2022/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Russia's capabilities and successes in foreign policy prompted by a viewer's message questioning Russia's standing.
- Explaining how NATO's response to Russia's invasion of Ukraine led to increased armament orders from Poland.
- Detailing the significance of the HIMARS rocket system in the conflict between Russia and Ukraine.
- Pointing out that Russia's actions in Ukraine have backfired geopolitically, accelerating NATO expansion and military modernization in Eastern Europe.
- Emphasizing that Russia's military technology lags behind NATO's advancements.
- Noting that Ukraine is intentionally not provided with the best NATO weapons to prevent potential repercussions.
- Criticizing Russian military leaders for misusing defense funds on personal luxuries instead of military advancements.
- Clarifying that the information presented is not propaganda but a reflection of the current geopolitical realities.

### Quotes

- "NATO has expanded. NATO has solidified its resolve."
- "The reality is the Russian military, the commanders, took money that should have been spent on defense and spent it on their yachts and their houses."
- "Knowing is half the battle. The other half is violence."

### Oneliner

Beau addresses Russia's foreign policy capabilities, showcasing NATO's response to the invasion of Ukraine and the impact on military dynamics in Eastern Europe.

### Audience

Policy analysts, military strategists.

### On-the-ground actions from transcript

- Contact organizations supporting Ukraine (implied).
- Stay informed on geopolitical developments (implied).

### Whats missing in summary

Beau's engaging explanation of Russia's foreign policy and the repercussions of its actions in Ukraine.

### Tags

#Russia #ForeignPolicy #NATO #Ukraine #Geopolitics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Russia's capabilities
and successes, or lack thereof, when it comes to foreign policy.
And we're going to do this because I
got a message that raised some questions,
and they deserve to be answered.
And we're just going to kind of go through it.
And see where we end up.
OK.
So here's the message.
Your cute video about Russia says nothing,
as does your constant song that Russia has already
lost the wider geopolitic war and worked
against its own security.
You never give example.
Two, you say Russia has fallen out of second place
because you say it is losing to Ukraine.
It is not losing to Ukraine.
It is fighting against the best armament NATO has to give.
And then there's a slur about Zelensky.
OK, first, next time just send it in Russian.
Seriously.
OK.
So I never give examples about how the invasion of Ukraine
has already cost Russia geopolitically.
The real purpose, the stated purposes, even the pretext,
acknowledge this, that part of Russia's reasoning
for doing this was to provide a buffer against NATO.
NATO expanded.
It didn't deter membership in NATO.
It increased it.
Aside from that, it stilled the resolve of NATO members,
primarily those in Eastern Europe.
A good example of this, and we are
going to talk about one system called HIMARS.
You are about to learn more about this system
than you will ever need to know.
But it's important to illustrate the point.
Poland, in 2019, they ordered 20 HIMARS.
They haven't taken possession of them yet.
So prior to Russia pulling this little stunt
and invading Ukraine, Poland wanted 20 HIMARS.
This is a rocket system.
During the war in Ukraine, NATO provided 16 HIMARS initially
to Ukraine.
There are more on the way.
And these 16 systems were devastating to Russia.
And they helped Ukraine push Russia all around the map.
Because of the invasion of Ukraine,
Poland is currently in negotiations
to order hundreds of these systems, hundreds.
I want to say the current number they're looking at is 200.
There was a time when they were looking at five.
They'll probably order more later
after they get the first couple hundred.
Keep in mind, 16 was enough to just be absolutely devastating
to the Russian war effort.
OK.
And then, so there's some examples.
And then we look at the best armament NATO has to give.
The HIMARS, let's talk about that,
because a lot of attention is going to that.
Because a lot of attention is going to that.
Do you know why it exists?
Because during the first Gulf War,
the US military kind of realized the predecessor system,
which is a tracked version, it's way heavier.
It's harder to move as far as airlifts and stuff like that.
So they wanted one that had higher mobility.
That's that HIMARS part of this, HIMARS,
high mobility artillery rocket system.
So the HIMARS was developed, the first time
it appeared in public, was in 1993.
That's the best armament, the high tech stuff.
Now granted, the system has been upgraded since 1993.
But this is not like cutting edge stuff.
More importantly, if you were to Google and look
at the range of this thing, it will be anywhere from 5 miles
to like 190 miles, I think, because it
depends on the type of munition that
is loaded into the back of this.
For those that don't know what this is,
it's basically a truck with a box on the back
that launches rockets.
And it has range.
However, the versions that Ukraine has,
they're limited in range.
Even though this is a system that
was developed in the 90s, before the turn of the century,
the versions that were given to Ukraine,
the munitions that are going along with it,
they don't have the range that US systems do,
the best armaments that NATO has.
The Polish ones will have that, by the way,
because they're NATO members.
So it's just not true.
This was a bad move geopolitically.
It sped the expansion of NATO.
NATO members closer to Russia are spending way more
on defense.
It was a failed move geopolitically.
The only thing that Russia can even
hope to accomplish at this point is good arms,
and what they're trying to accomplish at this point
is good old-fashioned imperialism,
take dirt and hold it.
And they're not even doing well at that.
The weapons that Ukraine is getting
are intentionally kind of capped.
They're literally not the best that NATO has,
and that's on purpose.
There's a risk of, in this case, with the HIMARS,
to do with the worry that if we provided Ukraine
with the longer-range stuff, they
would hit targets inside of Russia, which
is kind of against NATO doctrine.
But for the most part, the reason
they're not giving the higher-end stuff
is there is a concern about Russia capturing
some of it on a battlefield.
I mean, that seems unlikely at this point,
but it's something that could happen,
and wouldn't want it reverse engineered.
The reality is the Russian military, the commanders,
took money that should have been spent on defense
and spent it on their yachts and their houses
and their mistresses instead of spending it on defense.
The system that is all over the news, its base system
came out in 1993.
Think about how much it's been improved since then.
The Russian military has nothing even close to it.
Think about your phone and how much your phone
has changed since 1993.
It's not propaganda.
It's not propaganda.
I know if you're Russian, especially
if you're inside of Russia, you're seeing a view of this,
and then you get a glimpse of the outside world,
and they don't match.
The outside world's statements, it's not propaganda.
This is what's happening.
NATO has expanded.
NATO has solidified its resolve.
NATO, especially those in Eastern Europe,
are modernizing their militaries.
It is what it is.
And no, Ukraine is not getting the best
that NATO has to offer.
So now you know.
Knowing is half the battle.
The other half is violence.
Conducted by rocket systems that have ranges of 100 miles
or more and are accurate to within meters.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}