---
title: Let's talk about the presumed Speaker of the House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=W3ZMIRqx7d4) |
| Published | 2022/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau McGahn says:

- Beau introduces the topic of Kevin McCarthy as the possible presumptive Speaker of the House.
- McCarthy has been involved in political theatrics post-election, making grand promises.
- Beau mentions McCarthy's actions of initiating investigations and removing members of Congress from committees, including Omar, Schiff, and Swalwell.
- The real reason behind McCarthy's moves is linked to Marjorie Taylor Greene, who holds influence over him due to her demand for revenge as the "space laser lady."
- Despite appearing powerful with his actions, McCarthy is actually a figure of weakness within his own party, becoming an errand boy to maintain his Speaker position.
- McCarthy's focus on investigations and statements about Biden's son are not driven by genuine belief but rather by the need to appease the extreme base of the Republican Party.
- The Republican Party's emphasis on social media metrics as a gauge of public opinion alienates many normal Republicans who are concerned about real issues like inflation.
- McCarthy's lack of control over his own party indicates that the House's control by Republicans is superficial, leading to potential chaos in the upcoming years.
- The absence of a unified mission and party within the Republicans sets them up for failure in 2024, as they prioritize social media presence over sound policy ideas.

### Quotes

- "The real reason is the space laser lady. That's what it's really about."
- "He's not leading, he's the errand boy now, and that's how it is shaping up."
- "The Republican Party has still not learned its lesson, and it's setting itself up for another failure in 2024."

### Oneliner

Beau McGahn delves into Kevin McCarthy's political theatrics, revealing his weak leadership and the Republican Party's misguided priorities, setting them up for failure in 2024.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact local representatives to express concerns about real issues like inflation (implied)
- Support political candidates with strong policy ideas over social media influencers (implied)

### Whats missing in summary

Analysis of the potential impact on future elections and American politics.

### Tags

#KevinMcCarthy #RepublicanParty #Politics #Leadership #Influence #Elections


## Transcript
Well, howdy there, there are no people, it's Bo McGahn.
So today we are going to talk about the possible presumptive,
maybe continued speaker of the House, Kevin McCarthy.
McCarthy has engaged in a lot of political theater
since the election, promising a whole lot of stuff.
From investigations that nobody cares about
to the removal of members of Congress from committees.
That is kind of the latest one at Time of Filming.
I'm sure there will be more.
Those he is talking about removing
include members of Congress Omar, Schiff, and Swalwell.
Why?
Why does he want them removed from committee positions?
I'm sure there's a pretext.
He's going to say something about each of them.
But what's the real reason?
The real reason is the space laser lady.
That's what it's really about.
She has kind of demanded revenge.
The Speaker of the House is beholden to the space laser lady.
That's what's going on.
Marjorie Taylor Greene.
What it really demonstrates more than anything is that the Republican Party is fractured
and the Speaker doesn't have any power.
I know that doesn't make sense because he's talking about removing people from committees
and all of this stuff.
He doesn't have any power within his own party, within his own section of Congress.
Because of that, in order to forge an alliance to maintain the position of Speaker of the
house, he kind of has to do what everybody says. So rather than being the most powerful
member in the house, he becomes the errand boy for those that have absolutely no chance
of succeeding on the national stage, which should not be the position the speaker's in.
that that's the position that McCarthy finds himself in.
That's why you're getting these declarations about an investigation into Biden's kid and
all this stuff.
It's not even that McCarthy believes this is something the American people care about.
It's that this is what he has to say to keep the Republicans supporting him.
He's not leading, he's the errand boy now, and that's how it is shaping up.
Given the belief within the Republican Party now that retweets and shares and likes on
social media actually amount to a good representation of how America feels, they believe that, so
They're going to act like that.
The thing is, nobody, nobody cares about this stuff other than their most extreme base.
Most independents, even, I don't even want to say moderate conservatives, I think a lot
of normal Republicans, those that are just outside of the MAGA faction, they do not care
about this.
They want to hear about what the Republican Party plans to do about inflation, all of
the stuff that they campaigned on.
But now that they've been elected, after spending all of this time talking about, well, we have
to do this and we have to do this, there's been no announcement because they don't actually
have a plan.
They understand that when it comes to inflation, Biden is doing everything he can.
So they're not going to jump into this because they'll end up making mistakes and making
it worse, and it'll be evident.
The Republican Party in the House, it has control of the House.
But because it doesn't have control of its own party, it really doesn't.
It has control of the House in name only.
The next two years in the House are going to be chaotic at best.
They don't have a unified mission, they don't have a unified party, and now it is becoming
ever more apparent that McCarthy as Speaker would be beholden to the whims of those people
who have strong social media presences rather than those who have strong policy ideas, even
from a conservative standpoint.
The Republican Party has still not learned its lesson, and it's setting itself up for
another failure in 2024.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}