---
title: Let's talk about getting more conservative as you get older....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f4ftaEkkjiE) |
| Published | 2022/11/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the misconception that people become more conservative as they age.
- Explains that older demographics used to become more conservative due to amassing wealth, but this is no longer the case.
- Notes that getting older doesn't mean gaining more wealth or capital in the current socio-political climate.
- Points out that the Republican Party, despite the image of being fiscally conservative, has shifted away from this stance.
- Describes how the Republican Party has embraced culture war issues, moving away from fiscal conservatism.
- Talks about the progressive trend in society over time and how conservatives are dragged into the future.
- Mentions how individuals who do not evolve with society do not become more conservative, but rather remain static in their beliefs.
- Explains that the conservative party has slowly become more progressive, making older individuals identify more with them.
- Comments on the Republican Party's resistance to progress and how it alienates voters, particularly the younger demographic.
- Concludes that the Republican Party's outdated positions and failure to advance are causing them to lose elections and alienate their base.

### Quotes

- "People don't become more conservative. Not as a general trend. That's not a thing."
- "The world, as a trend, becomes more progressive, more accepting, more open."
- "Conservatives are back there at the backside of society, and they're getting just dragged into the future."
- "The conservative party slowly became more progressive. And then they eventually line up."
- "The longer they hold on to this, the worse it's going to get."

### Oneliner

Beau dismantles the myth that people naturally become more conservative with age, revealing how societal progress shapes political ideologies.

### Audience

Voters, political observers

### On-the-ground actions from transcript

- Reassess political affiliations based on current stances and not historical perceptions (implied).
- Encourage ongoing education and evolution of personal beliefs to stay in line with societal progress (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of how societal progress influences political ideologies, urging individuals to critically analyze their beliefs in the context of evolving social norms.

### Tags

#Conservatism #Progressivism #RepublicanParty #SocietalChange #PoliticalBeliefs


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about misunderstanding information
and whether or not people get more conservative as they get older.
This is a widespread belief.
People believe that as you get older, you become more conservative,
and they're basing this on the idea that older demographics
tend to vote for the conservative party.
Here's the thing. That's not true.
I know it's a widespread belief. Just stick with me.
People don't become more conservative.
Not as a general trend. That's not a thing.
Misunderstanding the information.
There was a time in the United States where you could count on a
portion of the demographic becoming more conservative as time goes on
because they amassed more money and they wanted to be fiscally conservative.
There are two problems with this transition now.
First is that because of, I mean, all of this,
everything that's going on in the United States,
getting older doesn't mean that you get more money.
It doesn't mean that you have more access to capital.
So there aren't as many people looking to become more fiscally conservative.
Aside from that, the Republican Party,
while they still have the image of being fiscally conservative, they're not.
They've dropped that. That's not a thing that they are anymore.
And somebody who might be considering changing parties,
they would look into it and see if they actually were fiscally conservative.
And they'd find out they weren't.
The Republican Party has gone all in on culture war stuff,
and they have eight years to realize they are way wrong on this.
And if they don't, they're going to become completely politically irrelevant.
People don't get more conservative when it comes to social issues as time goes on.
It's not a thing.
We talk about it on the channel a lot.
On a long enough timeline, we win, right?
The world, as a trend, becomes more progressive, more accepting, more open.
That's what occurs.
Conservatives are back there at the backside of society,
and they're getting just dragged into the future,
kicking and screaming, clawing at the ground.
And progressives are up at the front, charging ahead.
So somebody who, let's say they were born in the late 90s or late 80s,
they have a set of values that was the norm at that time.
Those who don't continue to progress, those who don't continue to evolve,
they don't become more conservative.
They just don't change.
So as they get older, they still have the same positions that they had when they were younger.
And those were probably progressive ideas,
because younger people are more open to new ideas.
But they just have those static ideas from the 80s and 90s, those that they grew up on.
As they get older, what actually happens is that the conservative party has been dragged along.
So they now accept those ideas.
That's their new position.
The conservative party reflects the ideas that that person who didn't evolve,
didn't change, didn't move forward with the rest of society,
it reflects their beliefs from when they were younger.
So they start to identify more with the conservative party.
They didn't become more conservative.
The conservative party slowly became more progressive.
And then they eventually line up.
Let's take that example, 80s and 90s.
When they were born, the conservative position,
I mean, they kind of just stopped arguing against desegregating.
That's not a conservative position that is widely defended today.
There aren't a lot of people advocating for segregation today,
because the conservative party has moved forward slowly.
Not as much as the progressives are charging forward.
But that's what normally occurs.
That's why you see that trend with older people voting in a more conservative fashion.
They don't become more conservative.
They stayed where they were at and the conservative party moved to them.
The thing is, the Republican Party has dug its hills in.
When they threw all of their support behind Trump
and allowed him to guide the culture war,
Trump is older and his positions were even older than him.
They were really out of date.
And that's what they've locked into.
And now they're like, well, we're not moving from here.
So those people who didn't evolve,
they're going to identify more with the Democratic Party than with the Republican Party,
because the Republican Party isn't allowing itself to be pulled into the future.
They're regressive.
They're trying to go further into the past rather than move towards the future.
So it's alienating voters.
They have a long string of positions that are counter to the widely held beliefs of 30 and 40 year olds.
So as time moves on and those 30 and 40 year olds become 50 and 60 year olds,
they may not move to the conservative party.
When it comes to stuff like orientation, identity, gender rights, equal rights,
races, all of the normal stuff that the Republican Party uses to motivate its much older base,
those who grew up during a time where segregation was still being debated.
As the Republican Party clings to that, that's not going to be effective on the older voters,
even though they're more conservative than the most progressive of the Democratic Party.
They're still going to be more in line with the Democratic Party than the conservative position of the 1970s.
That's the trouble that the Republican Party is facing.
They have refused to advance and society has.
The longer they hold on to this, the worse it's going to get.
The more elections they are going to lose and they're going to lose them in bigger and bigger numbers
because their older base isn't being replaced because they haven't moved forward.
Their investment into the culture wars, their investment into scapegoating people based on immutable characteristics,
it's finally really starting to punish them.
And that punishment gets more severe as more of the Republican base ages out.
I would imagine that a lot of people watching this channel can remember when Will and Grace came on the air.
It was a big thing.
If you don't remember or you're not old enough, it was a big thing.
Now it's super common.
However, the idea of marriage for that community is being debated in the Senate still.
It's wildly popular across the United States, but they barely have the votes.
It looks like they do, by the way, but they barely have the votes to get it through in the Senate,
to get over the filibuster because the Republican Party didn't advance its position in all that time.
That's what's going to hold them back.
They're counting on people becoming more conservative.
That doesn't happen.
The conservative party slowly becomes more progressive.
So they line up with the values that older people had when they were younger because those older people didn't evolve.
They didn't move forward.
And that's what the conservative party is.
Conservatives defend old progressive ideas.
And the Republican Party isn't doing it right now.
So when you hear that people get more conservative as they get older, think about it in your own life.
Do you know people who were tolerant and accepting and progressive throughout their entire lives?
And then when they hit 40, they just turned into raging bigots?
No, they just stopped advancing.
They just stopped evolving.
It's very rare for somebody to actually become more conservative like the way people are describing.
That's not really a thing.
The conservative party has a huge game of catch up to play.
Trump probably isn't the person to lead that.
Those who modeled themselves after Trump, they're not the people to lead that.
Republicans have to move forward.
They have to get out of the late 70s, early 80s, and in some cases, like pre-1964.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}