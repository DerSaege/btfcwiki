---
title: Let's talk about Trump, DeSantis, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=D3DgPj5Ty1k) |
| Published | 2022/11/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The state of the Republican Party is under scrutiny post-midterms, with Trump losing and DeSantis winning.
- A poll reveals that 41% of Republicans prefer DeSantis as the 2024 nominee, 39% favor Trump, and 8% support neither.
- The Republican Party requires change, acknowledged by those transitioning from Trump to DeSantis.
- DeSantis faces the challenge of not being as charismatic as Trump but is recognized as smarter.
- DeSantis needs to make decisions in Tallahassee with a national perspective to secure the nomination and potential win in 2024.
- Republicans in Florida misinterpreted their midterms success, assuming a mandate when it was actually due to low enthusiasm.
- DeSantis may face pressure to implement more restrictions to appease Florida Republicans, although this could harm his chances in a national election.
- His relationship with businesses may need adjustment, as his current posture towards large companies could be detrimental on a national stage.
- DeSantis navigating the transition from state to national politics involves considerations like handling the Trump factor and gaining business support.
- DeSantis might need to break away from the perception of being Trump Jr. and distancing himself from Trump to succeed in the national arena.

### Quotes

- "The Republican Party requires change, acknowledged by those transitioning from Trump to DeSantis."
- "DeSantis faces the challenge of not being as charismatic as Trump but is recognized as smarter."
- "Republicans in Florida misinterpreted their midterms success, assuming a mandate when it was actually due to low enthusiasm."
- "His relationship with businesses may need adjustment, as his current posture towards large companies could be detrimental on a national stage."
- "DeSantis might need to break away from the perception of being Trump Jr. and distancing himself from Trump to succeed in the national arena."

### Oneliner

The Republican Party faces change as DeSantis navigates the post-midterm landscape with potential national aspirations, balancing the legacy of Trump and business relationships.

### Audience

Political analysts, Republican voters

### On-the-ground actions from transcript

- Monitor DeSantis' decisions and positions and how they may impact his potential presidential run (implied).
- Stay informed about shifts in the Republican Party and potential candidates for the 2024 election (implied).

### Whats missing in summary

Insights on how DeSantis might strategically handle his relationship with Trump and the Republican Party's transition towards a potential more moderate stance.

### Tags

#RepublicanParty #DeSantis #Trump #2024Election #PoliticalStrategy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about the state of the Republican Party based on polling
and what DeSantis is going to do.
So a unique situation has developed.
After the midterms, it's pretty clear that Trump lost and DeSantis won.
A poll says that 41% of Republicans want DeSantis to be the 2024 nominee, 39% want Trump, and
8% don't want either.
So what that means is 8% have a very clear picture of the state of the Republican Party,
41% can be reached, and 39% have lost the plot.
That's really what that means.
The Republican Party, it has to change.
Those who were willing to jump from Trump to DeSantis understand that.
They may not understand how much things need to change, though.
But a very interesting development out of this is the position that DeSantis finds himself in.
DeSantis isn't as, I don't want to use the word charismatic, but it is what it is.
He doesn't get the crowd moving.
He doesn't inspire the loyalty that Trump does.
But DeSantis is smarter, like a lot smarter.
So he has to know that everything that is happening in Tallahassee, well that's now
national news.
And he has to frame all of his decisions through a national lens now.
Because if he wants, not just the nomination, but if he wants to win in 2024, he has to
make decisions that don't give him a record that would cause him to lose.
And immediately after the midterms, he has an issue.
Because Republicans in Florida have misread the situation.
They think they got a mandate when the reality is it was low enthusiasm because there wasn't
a candidate to drive the younger vote.
And Florida didn't go just wild when it came to restricting family planning, two things
that really motivated people.
But now they want to.
They want to engage in more restrictions.
To appease Florida Republicans, DeSantis is probably going to feel a lot of pressure to
go along with that.
However, if he wants to win the presidency in 2024, he can't.
I don't think that his advisors are going to miss the fact that if he comes out in favor
of something that is very restrictive, that, well, that's it.
His presidential hopes are gone.
I think that they'll be able to see that.
His relationship, DeSantis' relationship with business is probably going to have to change.
It seems unlikely that he'll be able to gather the business support that he needs if he maintains
his current posture towards large companies.
There's been a lot of news about some of his, let's just say, disagreements with big companies.
That could come back to haunt him in a national election.
So there are the normal considerations that a political figure who is trying to make the
jump from the state scene to the national scene, that they have to take into consideration.
And then there's the whole Trump problem.
Trump being in Florida can make things uncomfortable for DeSantis, and Trump still commands almost
40% of the Republican base.
This is going to be an issue for DeSantis.
At the same time, people like McConnell, people who are very familiar with the national scene,
they're probably going to be trying to sway everybody away from both Trump and DeSantis.
Because DeSantis is going to be viewed on the national scene as Trump Jr., and he's
definitely not going to like that.
But that's probably how the media is going to frame him.
That relationship between Trump and DeSantis is going to become a liability.
The only way to break it is to go head to head with Trump, which is not something anybody
in the Republican Party wants to do right now.
I personally think that because he's smarter, DeSantis would actually come out on top if
he actually took Trump on.
But I think he's probably going to try to abide by Reagan's 11th commandment, and he's
going to try to avoid getting into it too much with Trump, and just hope that other
circumstances deal with Trump for him, and then he can move on to the primary.
But the key element here is that you have a majority of the Republican Party ready to
break from Trump now, and basically get ready to hear DeSantis' name a lot.
Probably not as much as you heard Trump's, but you're going to hear it a lot.
He doesn't make the same number of unforced errors that Trump made.
So it's probably not going to be around-the-clock coverage the way Trump got, but his name is
going to probably rise in usage in media coverage for the next couple of years.
And that's assuming that McConnell's crew, the national Republicans, don't get their
way and divert the Republican base to somebody much, much more moderate, which I think is
going to be their goal.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}