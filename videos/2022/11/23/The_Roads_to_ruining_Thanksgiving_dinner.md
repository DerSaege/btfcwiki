---
title: The Roads to ruining Thanksgiving dinner....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EoyKP_2gpYM) |
| Published | 2022/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the importance of discussing uncomfortable topics like politics, religion, and money, despite traditional advice against it.
- He suggests tactics for navigating potential conflicts over Thanksgiving dinner, such as addressing right-wing viewpoints on border security and Hunter Biden.
- Beau explains the nuances of certain issues, like student debt relief and energy independence, to help facilitate informed and respectful dialogues.
- He delves into debunking misconceptions around Trump, the Democratic Party, socialism, and communism, providing strategies to counter misleading narratives.
- Beau offers advice on handling sensitive situations, like dealing with family members who tell racist jokes, by encouraging engaging but non-confrontational responses.
- He touches on contentious topics like trans rights, government spending on divisive issues, and gas prices, providing insights and counterarguments to facilitate constructive debates.
- Beau acknowledges the challenges of engaging with family members with differing views, recognizing the divisive nature of current political discourse.
- He underscores the importance of understanding the manipulation of information and emotional tactics in shaping opinions, while also acknowledging the limitations in changing deeply entrenched beliefs within some individuals.

### Quotes

- "This year definitely, you know, nationalize the stuffing, use a little baby guillotine to cut the cranberry sauce."
- "The country is incredibly divided. Most of it is because a certain subset of people who are very interested in controlling low information citizens have put out a lot of information to manipulate them emotionally."
- "Just pretend you don't get it. Pretend you do not get the joke."

### Oneliner

Beau offers strategic insights on navigating contentious topics during family gatherings, challenging misconceptions and promoting informed dialogues, acknowledging the deep political divide in the country.

### Audience

Family members navigating political differences

### On-the-ground actions from transcript

- Nationalize stuffing and use creative ways to bring up uncomfortable topics (suggested)
- Address right-wing viewpoints with informed counterarguments (exemplified)
- Educate on nuanced issues like student debt relief and energy independence (implied)
- Counter misinformation about Trump and Democratic Party narratives (exemplified)
- Encourage engaging responses to racist jokes to prompt reflection (exemplified)

### Whats missing in summary

In-depth analysis of various strategies to address controversial topics within family settings while acknowledging the challenges of altering entrenched beliefs. Full transcript provides detailed insights for effective communication. 

### Tags

#FamilyGatherings #PoliticalDiscourse #ChallengingMisconceptions #NavigatingDivides #InformedDialogues


## Transcript
Well, howdy there internet people. It's Beau again.
So today we are going to talk about talking points, I guess.
We're going to talk about how to discuss certain topics
at an upcoming meal.
It has become a tradition on this channel
to take questions from y'all
and figure out the best way to discuss them
over a family meal and in the process probably ruin Thanksgiving dinner.
Normally when we do this, the questions are very specific
and personal in a way. You know, how to deal with a personal dynamic that's
going on.
This year it's pretty much how to deal with
certain things that you know your relatives are going to say
over Thanksgiving dinner when it comes to
the normal political scene in the United States.
A lot of these I actually have individual videos
on, but we'll go through all of them at once
because the reality is for years
our betters have told us not to discuss
politics and religion and money and all of this stuff.
The reality is when you look at the world today
and you look at what is causing
unnecessary loss, they're rooted in those topics.
It's important to discuss them no matter how uncomfortable
it may be at times.
So, this year definitely, you know,
nationalize the stuffing, use a little baby guillotine to cut the cranberry
sauce,
whatever you need to do if you are so inclined
to broach these topics. It is
interesting because one of the big ones, I guess,
is that right-wing pundits
are still talking about border security
and a lot of people expressed concerns that their right-wing
relatives will be bringing up that topic
and the topic of how to deal with them.
Seems like a good place to start, what with Thanksgiving and all.
I would
play into the holiday
and I would ask if they believe
everything that they're saying. This holiday
is about, in theory according to the myth,
welcoming newcomers. Ask them
if they think people should be fed
and help to prosper and if not,
why did you have to drive across two states to get here to have this meal
with them?
The next one is
obviously stuff dealing with Hunter Biden. Now,
I have a video going over
the Hunter Biden
Ukraine thing and it goes pretty in-depth, establishes the timeline,
shows that everything they've been told about the prosecutor going after him and
all that stuff just isn't true.
So maybe start there.
I would also point out that nobody cares.
He has a substance issue. This is well known. This isn't a scandal. It's
not a surprise.
Everybody's aware of this. There are things that to me,
when it comes to Ukraine and the business dealings there, to me they seem
shady.
They probably should be considered
unethical. The one thing that they're not is illegal
by any of the stuff that we have seen thus far.
None of it's actually illegal. Maybe it should be,
but it's not. And then as kind of a
closing argument, I would definitely remind them
that you have decided that you will not be voting for Hunter Biden.
And then you can always
make the comparison and just point out that unless
whatever it is that the Republican Party believes they're going to find
when it comes to Hunter Biden, unless it is more dramatic
than the stuff that Trump did, nobody's going to care and all it is going to do
is turn normal, independent, moderate voters
away from them.
The student debt relief is one that came in a lot
with the expectation that people are going to talk about the working class
paying for the rich. This might be
the time to discuss class and point out
that in the US under the capitalist system there are two classes.
There's the working class and the owner class.
If somebody still has debt from college,
they're working class. They may be well-paid,
they may be white collar, but they're still
working class. Somebody is signing their check.
They are exchanging their hours, their time,
or their productivity in exchange
for wages. They're working class.
And given the
constant right-wing talking point any time it comes to taxes,
I would remind them that according to the Republican Party, working class folks
don't pay any taxes anyway.
It's all the rich folk. That's why they deserve
the tax breaks and why under
Trump's plan the working class is going to go up.
Start there.
Drill, baby, drill. The idea of
producing oil at home. During this conversation
it is almost a certainty that they will use the term
energy independence. Explain to them what that means.
Explain that oil is a global market and
if the US produces a bunch, those multinational companies that they're
going to bat for right now,
they're not going to keep it here. They will sell it
wherever it will generate the most profit, because they're the owner class.
They don't care about the working class.
And I would point out that energy independence,
as far as national security, as far as true independence,
will never be something that is dug out of the ground.
It's renewable.
You may need to mine the components to make it,
but the energy itself will be renewable.
Until that occurs, the US is not energy independent because it's linked to a
global market.
This is one of my favorites because it is just completely untrue in every way,
shape, and form.
The idea that everything that Trump is going through
is because the Democratic Party
doesn't want him to run in 2024. The Democratic Party
absolutely wants him to run in 2024. Trump
is not a winning brand
electorally. You could make the argument that
that sentence could have ended before that word, but
when it comes to electoral politics, Trump isn't
a winning brand. That's one of the reasons they didn't have a red wave,
is because they didn't break from Trump. They
echoed those claims that everybody's already rejected.
Trump has been rejected by the voters. He will be rejected again. The Democratic
Party is so
certain of this that during Republican primaries,
they funded, they used their own resources,
the Democratic Party went out of their way to
elevate Trump-tied candidates because they knew they would be
easy to beat. If they won their primary
and then had to face a Democratic candidate, the Democratic candidate
would win
because Trump is a losing brand. Not just did they do this,
they were right. It worked.
Part of the reason they didn't have a red wave.
And then when you're talking about red wave,
there's a few people, and these are definitely gonna be the toughest
conversations,
who expect their family members to be talking
about wild claims about voting
and that type of stuff. That's hard
because if you're dealing with people who have bought
into that type of stuff, you're going to have to forego
normal argumentation. They're not going to,
any statistics you give them, they're going to say that they're false.
So you have to use anecdotal
and make them draw their own conclusions
to get them to to waver from this. The way to start
would be, hey, the Republican Party,
because they didn't protect themselves during
the public health issue, they lost more people.
That helped sway the vote. Because of their bad policies when it came to
public health
and because of pushing back against basic protective measures,
they lost more people. It was easier for the Democratic Party to beat them.
Then go into
the Trump-linked
candidates. Dive into those and talk about how the Democratic Party
elevated them so they could beat them.
They will probably be much more
receptive to this information than most people
because this is kind of an underhanded thing that the Democratic Party did.
People who believe that they're manipulating the votes
would be more likely to say,
oh, they did that, did they? And get mad about it.
But then when you get to the part where, yeah, they elevated them in the primary
and then stomped them in the general,
it makes them hopefully
face reality that Trump,
Trumpism, MAGA, the America First thing,
it's not a winning brand when it comes to
the average American. Nationalism
is politics for basic people.
People are certain
that they're going to encounter family members
who believe that anything the government does is socialism
and also have no idea
what the difference between socialism and communism
is. I have a couple videos on this.
One is a phone call between myself
and a person with a PhD in this exact thing.
And she can
break down the differences between the various types of socialism
better than just about anybody. So I would use that,
maybe talk about it a little bit,
and then show them the video. Another one
is a video titled, Let's Talk About the Understanding
of, or the Importance of Understanding Basic Philosophies.
And it goes through a
presumably right-wing person's positions
and explains that if they're not following
their betters, those people who get them angry
over on right-wing media, their solutions,
not just are they left-wing, they're incredibly left-wing.
Most times
in the US when people say communism, what they really mean
is totalitarianism. They mean
the security state
aspects of the Soviet Union. That's what they're talking about.
Ask them to list those elements
and then ask them
if any of those elements aren't occurring today
in the United States under the capitalist system.
This is one of the more personal ones, and this came in from a few people.
People definitely have talked about this before, but
always worth going over again. You have a family member
who likes to tell racist jokes. The best way to deal with this
is pretend you don't get it.
Pretend you do not get the joke. Don't get upset,
not initially anyway. Don't get combative.
You know, they tell the joke and it always starts off the same way,
looking over their shoulders to make sure whoever they don't feel comfortable
hearing it isn't around. Once they tell it,
just be like, what? I don't get it,
and drag it out of them. Make them explain
the stereotype. Make them explain
whatever it is they're trying to get at, what they think
should be funny. And most times when you get them to
explain it, first, the joke isn't funny.
Second, most people are embarrassed by it.
It probably isn't going to change their attitude.
I mean, at this point, most of these messages are talking about
family members who are older.
You're probably not going to change the way they look at the world,
but you can change the way they behave around you,
and this is a good way to do it. Because even if
what you say in the conversation that is definitely going to follow this after
you drag it out of them, even if what you say doesn't have an impact,
they won't behave that way around you. They won't tell those jokes around you
because they know you're going to ruin them by pretending not to get them.
So, even if you can't
truly alter their beliefs,
you can alter the way they act. And that's just as good.
Transports.
Yeah, of course. Of course that's going to come up at Thanksgiving because it's a
national,
you know, topic. Pull the stats.
Pull the numbers where this is
actually at play. You have states that passed laws about this
that couldn't identify a single person in the state
that it would apply to. I would also
go back to the video that came out recently talking about how much the
Republican Party,
or elements aligned with the Republican Party, spent
turning this into a midterm issue and break down the math.
Fifty million dollars to deal with,
I want to say it was 4,200 kids. I can't remember off the top of my head,
but I want to say it's around 4,200 kids nationwide.
Twelve thousand dollars
a kid they spent to bully them.
To turn this into an issue. To make sure
that your family's going to be talking about it.
Spending that much money to teach people who to hate.
Who it's okay to look down their noses at. Get into the money
and then ask how much your relatives
donated to these people who then turned around and used that money
not to advance some agenda that would help them,
but just to pick on children. And then the last one
is gas prices. This is where you
encourage electric cars. That's where you go with it.
And you'll get a bunch of arguments.
You know you can't afford it right now.
And that's fine. That's going to be a lot of people. But the more it's used,
the more end up on the secondary market,
the less expensive they become. The other one that you're going to hear
is about how they're not powerful enough.
That's when you point to the Abrams. There is a concept in the works right
now
that is a hybrid Abrams. An Abrams
is the main battle tank of the US military.
It is the backbone of US armor and they're going to be turning it
into an electric vehicle. I don't think
Uncle Roscoe needs more power than an Abrams does.
So those are the talking points for the most
questions that came in most often.
I wish you luck for people who have to deal with this.
I mean it is what it is. Everybody has those family members. You're not alone.
Just remember that out of all of these that I went through,
each one had at least 10 people ask that question.
You're not alone. The country is incredibly divided.
Most of it is because a certain subset
of people who are very interested in controlling
low information citizens
have put out a lot of information to manipulate them emotionally.
They don't have the information in a lot of cases.
And then sometimes your relatives just a bigot and there's nothing you can do
about it.
But for those you can reach, good luck. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}