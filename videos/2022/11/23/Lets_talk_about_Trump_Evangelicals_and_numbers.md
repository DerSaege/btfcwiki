---
title: Let's talk about Trump, Evangelicals, and numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Pjw3l6O5dtc) |
| Published | 2022/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Evangelicals are turning on Trump, distancing themselves from the former president since the midterms.
- Mike Evans and James Robinson, prominent figures, criticize Trump for his actions and behavior.
- Everett Piper from The Washington Times bluntly states that Trump has to go, predicting destruction in 2024 if he's the nominee.
- The focus is not on Trump's declining poll numbers but on the impact on declining church memberships.
- Right-wing talking points often reference decreasing church memberships, with Beau attributing it to the support of Trump.
- Beau criticizes the church for preaching hate instead of the gospel and using political power to enforce beliefs.
- Inserting religious organizations into politics leads to distrust, hypocrisy, and manipulation, undermining their core mission.
- Beau points out the hypocrisy of church leaders who had a change of heart about Trump post-midterms but didn't rule out supporting him later.
- The intertwining of church and state leads to church leadership behaving like politicians, causing failing church memberships.
- Beau suggests failing church leadership is a significant factor in declining church memberships.

### Quotes

- "Donald Trump has to go."
- "The problem isn't that you're turning on Trump. The problem is that you ever supported him to begin with."
- "When it is this transparent, all of these church leaders, they become politicians."
- "You want to talk about failing church memberships? It tends to be a result of failing church leadership."
- "It's just a thought."

### Oneliner

Evangelicals turning on Trump post-midterms impacts declining church memberships due to intertwining of politics and religion.

### Audience

Religious community members

### On-the-ground actions from transcript

- Reassess church leadership roles and behaviors (suggested)
- Advocate for separation of church and state (implied)

### Whats missing in summary

The full transcript provides a deeper dive into the relationship between politics, religion, and declining church memberships.

### Tags

#Evangelicals #DonaldTrump #ChurchLeadership #ReligionInPolitics #SeparationOfChurchAndState


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about numbers.
We're going to talk about how numbers are changing and we're going to talk about former President Donald J. Trump
and how there's yet another demographic shifting away from him,
trying to put some distance between themselves and the former president since the midterms.
We're going to talk about why that's happening and how that event influences those numbers.
We're talking about evangelicals.
Evangelicals are turning on Trump.
There was a piece in HuffPost. It was pretty good.
Mike Evans said, Donald Trump can't save America, can't even save himself.
Now, if you don't know who that is, if you're not familiar with this community,
this is one of the evangelicals who met with Trump at the White House.
He went on to say that he used us. He used us. Trump used the evangelicals.
And we'll probably come back to that. And then you have James Robinson, another big figure, said,
if Mr. Trump can't stop his little petty issues, how does he expect people to stop major issues?
But it was Everett Piper writing in The Washington Times that really just laid it on out there
and was just completely honest about it.
The take home of this past week is simple. Donald Trump has to go.
He went on to say, if he's our nominee in 2024, we will get destroyed.
He used us. Yeah, sounds like y'all used each other.
I would imagine most people think this video is about to turn into how this is going to impact Trump's numbers,
his failing poll numbers. But that's not what it's about.
It's about how this impacts the failing church numbers.
One of the big right wing talking points, one of the things they tend to point to,
is the constantly decreasing church memberships.
The people who are members of a church, the people who identify in that way,
understand this has a lot to do with it.
After the results last week, that's the take home after last week.
See, I was unaware that God's favor hinged on the midterms.
You really think the people that fill your pews can't see through that?
Y'all used us. No, y'all used each other. And that's the problem.
The problem isn't that you're turning on Trump, and that's what's causing church membership to decline.
The problem is that you ever supported him to begin with.
The problem is that you started, well, preaching hate instead of the gospel.
You started trying to use the power and violence of the state to enforce your beliefs,
rather than what the Bible says to do.
I'm fairly certain the instruction manual doesn't tell you to go about it that way.
And people see through it. That's why you have failing church numbers.
That's why there is less church membership, because this is really transparent.
The fact that, and it's not just evangelicals to be clear,
there are a lot of religious organizations who have inserted themselves into politics
to a degree that they are undermining their core mission.
When you think about politics, do you think trust? Do you think love?
Do you think anything that your religion is actually supposed to advocate?
Or do you think deceit, hypocrisy, manipulation?
If you tie yourselves to politics in this way, this is what you get.
And when it is this transparent, all of these church leaders, and it's not just these named,
who suddenly had a change of heart about the former president right after the midterms.
I mean, the congregation, the flock, they see that.
And they probably also note that in these interviews,
most of them didn't rule out supporting him if he won the nomination.
There's a reason the separation of church and state exists,
and it isn't just the sanctity of the state.
It isn't just stopping the church from influencing the state.
If your church becomes embroiled in politics to this level,
the church leadership, well, they become politicians
with all of the hypocrisy and deceit that goes along with it.
You want to talk about failing church memberships?
It tends to be a result of failing church leadership.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}