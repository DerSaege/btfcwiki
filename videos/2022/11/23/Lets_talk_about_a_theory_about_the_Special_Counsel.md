---
title: Let's talk about a theory about the Special Counsel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fgPcb6uqwc0) |
| Published | 2022/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on the special counsel's responsibilities and Jack Smith's role.
- Questions the media's focus on Trump's candidacy announcement as the sole reason for the special counsel.
- Mentions the complex background investigations related to January 6th.
- Suggests that the special counsel's role may involve a broader scope beyond Trump's involvement.
- Considers the possibility of Smith making charging decisions on various individuals and investigations.
- Points out the lack of extensive information on other investigations happening in the background.
- Notes Garland's history of conducting investigations without leaks.
- Raises the idea of Smith being the decisive figure in these matters.

### Quotes

- "If it's just the decision to indict Trump, it doesn't make sense to bring in a special counsel just for that reason."
- "That tracks with Garland. Garland has a long history of no leaks, don't explain anything, go about your investigation, then announce once everything is decided."
- "Smith may be the decider."

### Oneliner

Beau speculates on the special counsel's broader responsibilities beyond Trump and suggests Jack Smith might play a significant role in making charging decisions related to various investigations.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze and stay informed about the evolving situation around the special counsel's investigations (implied).

### Whats missing in summary

Exploration of the potential implications of the special counsel's investigations and the significance of Jack Smith's role in the decision-making process.

### Tags

#SpecialCounsel #Trump #JackSmith #Investigations #ChargingDecisions


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the special counsel
and an emerging theory about the responsibilities
that are going to be thrust upon Jack Smith.
It's a little outside of the conventional wisdom
when you look at what's being reported in the media.
The general take in the media is that the special counsel
is being brought in solely because Trump announced his candidacy
and the special counsel will now be making the charging decision
when it comes to the documents case in January 6th.
And yeah, that makes sense, but maybe that's not all of it.
When we first talked about the special counsel,
I said it didn't make any sense as far as the documents case,
but when it came to January 6th, that does make sense
because there's a lot of moving parts to that case.
We need to remember that throughout this entire time,
in the background, the feds have also been looking into
other things related to January 6th
that weren't necessarily Trump.
They looked into the fake electors.
They looked into the Green Bay sweep
or whatever they called it.
They looked into the general plan
to obstruct those congressional proceedings.
And the feds have been doing all of this in the background
this entire time.
So this may be all rolled into one
and this may be all under Smith's purview.
So it may not be as simple as the documents case
involving the involvement with January 6th.
It may be him making the charging decisions
about all of these other people,
all of the wide-ranging investigations
that have been occurring in the background.
We hear little bits and pieces of it every once in a while.
You know, so-and-so had their phone taken.
This place got searched.
The FBI interviewed these people.
This could fall under his investigation
and it's his decision on who to charge in it.
That makes way more sense to me anyway.
If it's just the decision to indict Trump,
it doesn't make sense to bring in a special counsel
just for that reason.
Because no matter how the special counsel
goes about their job,
the Republican Party's gonna claim it's partisan.
So it didn't really make any sense.
But if that person is there to sift through everything
involving the 6th from the fake electors
to any pressure campaigns
that might have occurred with state officials
and all of this falls into that office,
the special counsel's office,
all of it makes a whole lot more sense.
Again, it's a theory,
and I've only seen it in a couple of places.
I think it's interesting.
I think it makes more sense.
I don't know that it's true.
I have no evidence to suggest that it's true.
But it tracks a lot more than just bringing him in
to make the charging decision,
especially because we really haven't heard much
about what's going on with these other investigations.
That tracks with Garland.
Garland has a long history of no leaks,
don't explain anything, go about your investigation,
then announce once everything is decided.
Smith may be the decider.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}