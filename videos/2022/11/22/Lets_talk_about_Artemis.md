---
title: Let's talk about Artemis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kz8RMoi07mE) |
| Published | 2022/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to the recent liftoff event after a decade of planning.
- Artemis 1, delayed by hurricanes and technical issues, finally took off with the Orion capsule.
- The capsule, carrying sensors-equipped mannequins, is on a 25-day journey orbiting the moon.
- The mission aims to break distance records from Earth and pave the way for human return to the lunar surface.
- Photos transmitted back from the mission, while not visually striking, mark a significant milestone.
- The successful launch and flight progress according to plan, with photos showing the roundness of Earth.
- Artemis program, mirroring Apollo missions, signifies progress towards establishing a spacefaring civilization.
- The Orion capsule is scheduled to return on December 11 by splashing down in the Pacific Ocean.
- Beau addresses the debate around the cost and benefits of such space programs.
- Despite financial considerations, the goal of becoming a spacefaring civilization is invaluable.
- This mission represents a small step towards eventual human presence on Mars.
- The initiative signifies a renewed effort in humanity's quest for interplanetary exploration.
- Beau concludes with optimism on the progress and potential of space exploration efforts.

### Quotes

- "Artemis 1 took off with the Orion capsule sitting on top of it."
- "Yes, the Earth is round."
- "It's one that should eventually lead to people being on Mars."
- "They have restarted in earnest."
- "Y'all have a good day."

### Oneliner

Beau talks about the recent liftoff event of Artemis 1, marking a step towards human space exploration, with optimism for the future journey to Mars.

### Audience

Space enthusiasts, Science enthusiasts

### On-the-ground actions from transcript

- Follow updates on space missions and advancements (implied)
- Support space exploration initiatives through advocacy or donations (implied)

### Whats missing in summary

Details about the specific technologies and advancements involved in the Artemis 1 mission.

### Tags

#SpaceExploration #ArtemisMission #NASA #HumanSpaceflight #MarsMission


## Transcript
Well, howdy there internet people.
Let's go again.
So today we're going to talk about an event that
has been in the works for a decade
and has been hoped for for decades longer.
A liftoff occurred, and it was a liftoff
that was delayed by months in two hurricanes
and some technical glitches.
But Artemis 1 took off with the Orion capsule
sitting on top of it.
It begins its 25 day, 1.2 million mile journey.
Inside the capsule are some mannequins,
and they have sensors on them.
There are no people on it right now.
The Orion capsule will orbit the moon,
and it will set a record as far as distance from Earth.
This is the first step in returning humanity
to the lunar surface.
The launch and the flight so far has gone according to plan.
It has already returned some photos.
They're not particularly beautiful.
They're not special when you're looking at the actual photos.
Just in case you're curious, yes, the Earth is round.
But they seem a little different.
It's not anything we haven't seen before,
but these are a little different because they are the first
photos transmitted back from a program designed
to put people on the moon.
This is the first step in that.
And that is the first step in getting
to another world, which is the ultimate goal here.
The capsule should return on December 11,
and it will splash down in the Pacific Ocean
off the coast of California.
The if you're not familiar with the mythology,
Artemis is the twin to Apollo.
It's kind of fitting.
There are a lot of people who, when
they talk about programs like this,
they talk about the cost.
And they talk about what the immediate benefit is.
And I have a lot of videos that talk
about all of the spinoffs that come from NASA
and how NASA pretty much always pays for itself.
But at the same time, we have to ask ourselves
if we really can quantify into dollars becoming
a spacefaring civilization.
This is a small step, right?
But it's one that should eventually
lead to people being on Mars.
There was a long break in our attempts
to become a spacefaring civilization.
But they have restarted in earnest.
And so far, things look pretty good.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}