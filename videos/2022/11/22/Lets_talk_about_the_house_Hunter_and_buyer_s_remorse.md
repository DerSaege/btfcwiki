---
title: Let's talk about the house, Hunter, and buyer's remorse....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uQSKUui1vnU) |
| Published | 2022/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans promised to address issues but focused on launching an investigation into Hunter Biden, leading to buyer's remorse among some voters.
- Putting Republicans in control means getting a clown show in the House for the next two years.
- The presumed hearings and subpoenas into Hunter Biden may not reveal anything substantial.
- Even if shady stuff is found, it's unlikely to significantly impact public opinion since Hunter Biden is not an elected official.
- Any wrongdoing uncovered must be more severe than what Trump did to gain public attention.
- Republicans seem focused on hearings and investigations rather than addressing voter concerns.
- The Biden administration and the Democratic Party will find these actions annoying, but it may benefit Fox News.
- There's little likelihood of finding anything illegal connecting to President Biden through these investigations.
- Hunter Biden may have skirted the edge of legality in his dealings, but nothing substantial may be uncovered.
- Punitive hearings from Republicans often lack substance and may push voters away.

### Quotes

- "Putting Republicans in the House and give them control, you get a clown show."
- "Nobody cares."
- "If whatever they uncover is less than what Trump did, nobody's gonna care."
- "This is going to be incredibly annoying for the Biden administration."
- "This is going to be a lot like other punitive hearings that have come from Republicans."

### Oneliner

Republicans in the House focus on investigating Hunter Biden, potentially alienating voters and lacking substance in their actions.

### Audience

Voters

### On-the-ground actions from transcript

- Contact your elected representatives to express your concerns about focusing on investigations rather than addressing real issues (suggested).
- Join local political organizations to stay informed and engaged in the political process (implied).
- Organize community events to raise awareness about the importance of addressing pressing problems over political theatrics (implied).

### Whats missing in summary

Analysis of the potential long-term consequences of the Republicans' focus on investigations instead of addressing voter concerns.

### Tags

#Republicans #HunterBiden #House #Investigations #VoterConcerns


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about buyer's remorse
and Republicans in the House and the Biden family
and how it all ties together.
Shortly after news broke
that Republicans were gonna take the House,
they came out and they promised
that they were gonna do all of those things
that they complained about.
They were gonna fix all those problems.
They were gonna address inflation
and they were gonna help people who needed help.
They were gonna help veterans
and they were gonna do something about crime.
And no, of course not.
They came out and said
they were gonna launch an investigation into Hunter Biden.
And normal Republicans and independents
started feeling buyer's remorse at that exact moment.
But here's the thing.
Y'all voted to feed gizmo after midnight.
You got gremlins.
It's how it works.
You voted to put Republicans in the House
and give them control, you get a clown show.
That's what you're gonna get for the next two years.
Nothing you were concerned about is going to be addressed
because they don't have a plan to deal with any of it.
But let's talk about the presumed hearings,
those subpoenas that they're threatening.
What are they gonna find out?
I mean, really, when you think about it,
what are they gonna find out?
They'll find out that Hunter Biden
has a substance abuse issue, which we all already knew,
which would definitely mean that very few people
are gonna vote for Hunter Biden.
Nobody cares.
Nobody cares.
They will look into Ukraine and those allegations.
Now, about three years ago in 2019,
I put out a video on this.
I dove into this topic and talked about what he did.
There was stuff that I consider to be shady,
maybe a little unethical, nothing that was illegal.
And more importantly, nothing that implicated is that.
My guess is that we're gonna find out
the exact same type of thing happened with China.
Shady, maybe unethical, but probably not illegal.
And even if it is, they would still have to find some way
to tie it to President Biden.
That all seems really unlikely.
They're gonna find out he did some shady stuff.
Yeah, but Hunter Biden is not an elected official,
so nobody's gonna care.
Aside from that, let's say that there is something
that somehow connects to President Biden.
I don't actually think there will be,
but let's pretend that there is.
Let's pretend it's true.
Whatever it is has to be more severe than what Trump did.
It shouldn't work this way.
It really shouldn't.
The standard shouldn't be, to quote Obama's ethics advisor,
the standard shouldn't be less of a grifter than Trump.
But the reality is, in public opinion,
that is exactly what it's gonna be.
If whatever they uncover is less than what Trump did,
nobody's gonna care.
Nobody's going to care.
So not just are the Republicans in the House
apparently dead set on ignoring anything
that the American populace cares about,
that the voters that swung their way during the midterms,
that they care about,
not just are they intent on ignoring all of that
in favor of hearings and investigations,
they're doing it with very little likelihood of any reward.
This is going to be incredibly annoying
for the Biden administration.
It's going to be annoying for the Democratic Party.
It's gonna be great for Fox News.
I don't want to go as far as to call this a gift,
like Trump announcing his candidacy.
It's not a gift to the Democratic Party,
but I don't think it's gonna hurt them
because I honestly don't think there's anything there
that either the public doesn't already know about
or is, I don't think there's gonna be anything illegal
that connects to President Biden.
Now, maybe Hunter Biden violated some laws,
maybe, when it comes to influence-peddling type stuff,
but as far as I could tell in Ukraine, and I was looking,
he didn't.
He ran right up to the edge of what was legal,
but he didn't cross.
Now, it is important to note
that when it comes to political lobbying,
to me, there's a whole lot of things that are legal
that I don't necessarily think should be.
So my guess is that's what they're gonna find out
when they start looking at his dealings in China as well.
I don't foresee some massive scandal unfolding from this.
This is going to be a lot like other punitive hearings
that have come from Republicans.
A lot of allegation, not a lot of substance,
and I think in the process,
they are going to push a lot of voters away.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}