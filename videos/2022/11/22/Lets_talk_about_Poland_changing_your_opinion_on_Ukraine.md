---
title: Let's talk about Poland changing your opinion on Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fyaG64rGWoU) |
| Published | 2022/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions why NATO isn't at war with Russia despite a situation in Poland involving a Russian rocket.
- Points out discrepancies in the presentation of facts regarding the incident in Poland.
- Mentions the ideal pretext for NATO to go to war with Russia but questions why it didn't happen.
- Suggests that NATO's reluctance to go to war with Russia indicates their doctrine of nuclear deterrence.
- Analyzes Russia's pretext for invading Ukraine as a lie, considering NATO's stance on going to war.
- Describes Russia's actions as imperialism with a capitalist bent, contrasting them with American foreign policy.
- Argues that Russia's invasion of Ukraine was driven by imperialism, with other justifications being mere pretexts.
- Encourages viewers to reconsider their views on the Russian invasion with the new information presented.

### Quotes

- "NATO doesn't want to go to war with Russia, right?"
- "Their pretext falls apart when you acknowledge that NATO will never get a better opportunity to go to war with Russia than they have right now."
- "Russian command's decision to invade Ukraine was imperialism."

### Oneliner

Beau questions NATO's lack of war with Russia despite an ideal pretext, revealing the true nature of Russia's actions as imperialism.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Analyze and question international relations (implied)
- Reassess beliefs about geopolitical conflicts (implied)

### Whats missing in summary

Deeper understanding of the geopolitical implications and motivations behind international conflicts.

### Tags

#Geopolitics #Russia #NATO #Imperialism #InternationalRelations


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
what happened in Poland and we're going to ask a question.
One of the clearest signs of intelligence is the ability to change your position, change your opinion
when you get new information.
So we're going to talk about Poland.
I'm going to ask a question and then we're going to talk about what the answer to that question means.
Okay, so the question about what happened in Poland.
Why isn't NATO at war with Russia right now?
I know there's a whole lot of people right now saying,
because I think they said it was a Ukrainian rocket.
Yeah, but how do you know that?
They told you, right?
They told you.
Let me give you some facts as they could have been presented.
A weapon of Russian manufacture that the Russian military has pressed into service as a rocket
used to attack things on the ground, even though it isn't designed for that,
making it incredibly inaccurate, landed in Poland.
Just a few miles from a known Russian target.
These are facts.
So why wasn't it presented that way?
Because that's not true.
That's not what happened.
Sure, sure.
Because there's no way Western powers would manufacture a pretext to go to war, right?
Of course they would.
And if you don't believe they would, we have a game of hide and seek going on in Iraq.
So, for some reason, NATO passed on the opportunity to use a 100% perfect pretext,
a pretext that would allow them to frame going to war with Russia as a defensive war.
Why didn't they use it?
Is it because the Russian military is in great shape right now?
No, of course not.
The Russian military is retreating on its knees.
They're 100,000 down.
They have issues with forced generation.
Their economy isn't doing well.
They have civil unrest at home.
All of their high-tech, quote, toys have already been captured by Western nations,
and the reverse engineering process has begun figuring out how to break them.
The reality is there will never be a better time with a better pretext for NATO to go to war with Russia.
So why didn't it happen?
And I'm not advocating for it.
I'm not going full patent here.
I'm asking, why didn't it happen?
Because there's really only one answer.
NATO doesn't want to go to war with Russia, right?
I mean, that's really the only possible answer.
It's the ideal time with the ideal pretext.
So they don't want to go to war.
Okay, probably because of their nuclear deterrent, right?
Mutually assured destruction and all that.
And yeah, that's it.
It's NATO doctrine, and it was NATO doctrine now and five years ago, 10 years ago, before the war.
It's public.
NATO knows it.
Most civilians know it, and Russia knew it.
So with the acknowledgement that NATO has no intention of going to war with Russia,
to the point that they would pass on the ideal situation,
what does that say about the Russian pretext for invading Ukraine?
It says they were lying, and they knew they were lying.
If you look at the statements and all of the idea that, well, it's a NATO provocation,
that's not NATO doctrine.
And NATO really doesn't break from this.
They have no desire to get into a shooting war with Russia.
It's not a thing, to the point where they would pass on the ideal opportunity to do it.
You couldn't create a scenario in which NATO would have more of an advantage.
And they passed, because it's not doctrine.
And Russia knew that.
So everything they said was a lie, and they knew it was a lie.
So when Putin ordered this little adventure that has cost 100,000 of his own people,
he knew it was a lie.
If you now can acknowledge that their pretext was a lie, what do you have to go on then?
What's left?
You have their actions, right?
You can look to the other propaganda, and them talking about creating greater Russia,
and all of that stuff.
But maybe that's just propaganda to build support at home.
All you really have to go on is their actions.
And what are their actions?
After years and years of covert operations to destabilize a smaller power near them,
they invaded and tried to cut it up and turn it into smaller countries and rucify it.
It's imperialism with a capitalist bent.
That's what their actions are.
And that's what it was.
There are a lot of people who look at American foreign policy and want to point out its failings.
When people do that with me, you are not preaching to the choir, you are preaching to the preacher.
The list is way longer than most people know.
However, that doesn't mean that anybody who opposes US foreign policy is automatically the good guy.
They could also be a bad guy.
They could also be engaged in empire building.
They could also be engaged in imperialism, in economic, that soft form of imperialism that we've talked about.
In this case, it's wide out in the open.
Invading, planting flags, old school imperialism.
That is what Russia did.
Their pretext falls apart when you acknowledge that NATO will never get a better opportunity
to go to war with Russia than they have right now.
And they passed on it.
All of their talk about NATO and NATO aggression, it falls apart.
Russia, the Russian military is in a very precarious situation.
The only reason that NATO doesn't want to do this, NATO isn't doing this, is because of their nuclear power.
And that nuclear deterrent, it's always going to work.
It will always work.
And it would work before.
Russian command's decision to invade Ukraine was imperialism.
Everything else was just a pretext, manufactured just as easily as any American pretext.
Take the moment to acknowledge these simple concepts and run through it yourself.
If you are still one of those people who think that the Russian invasion was somehow justified,
run through it with the new information.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}