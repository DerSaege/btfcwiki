---
title: Let's talk about DeSantis, Trump, and information....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yif8sCjnkio) |
| Published | 2022/11/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's ambiguous stance on potentially running against DeSantis and threatening to reveal damaging information about him.
- Speculation on whether Trump's tactic is a form of coercion against DeSantis.
- Trump's claim of possessing information about DeSantis that could be damaging to Republicans.
- Questioning the ethics of withholding damaging information until politically advantageous.
- Doubts on trusting Trump if he prioritizes his political interests over those of Republican voters.
- Trump's perceived disregard for an informed public and preference for uneducated voters.
- Implication that Trump's actions reveal his character to Republicans.
- Overall, Beau criticizes Trump's potential manipulative tactics and lack of transparency.

### Quotes

- "If there is something and Trump knows about it and Ron DeSantis is doing something that Republicans would find morally objectionable or perhaps it's damaging the state of Florida, how can you trust Trump at that point?"
- "He's flat out saying he is putting his personal political interests above that of Republican voters."
- "I guess having an informed public, having informed voters, I guess that's not important to Trump."
- "If you have any questions about who Trump is and you're a Republican, he just told you."

### Oneliner

Beau questions Trump's ethics in potentially using damaging information against DeSantis, revealing a lack of transparency and prioritizing political gain over Republican voters' interests.

### Audience

Republican voters

### On-the-ground actions from transcript

- Question Trump's motives and transparency (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's potential manipulation tactics and lack of transparency, urging Republican voters to question his ethics and priorities.

### Tags

#Trump #DeSantis #Republican #Transparency #Manipulation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Trump, DeSantis, things that apparently we don't
know and what might be the thing that worries Trump the most.
Recently he was asked by some reporters what he thought of the prospect of running against
Ron DeSantis.
And he said if he runs, he runs, trying to cast the bravado that Trump is famous for.
Shortly thereafter, he told somebody at the Wall Street Journal, if he did run, I will
tell you things about him that won't be very flattering.
I know more about him than anybody other than perhaps his wife, who is really running his
campaign.
He has also recently publicly referred to DeSantis as Ron DeSanctimonious.
That little passage, though, if he does run, I'll release this information.
I mean, some might see that as an attempt to kind of coerce him in a way.
But Republicans should ask themselves the question.
Whatever this information is, it's information that would be damaging in the eyes of a Republican.
That's the information that Trump claims he has about DeSantis.
It's information that would bother Republicans because he's going to be running against DeSantis
in a primary.
So it's just Republicans running.
So whatever this information is, it's information that would change the average Republican's
view of DeSantis.
Something bad.
Not very flattering.
So the question is, what exactly is Trump holding onto?
What is he keeping from Republican voters that, I mean, theoretically, they should know,
right?
I mean, if it's something that would alter their vote and make somebody vote for him
instead of DeSantis, it seems like something that would be material.
It seems like something that Republicans should know about.
Not something that somebody should withhold and keep that information in their back pocket
until it's politically advantageous for them to release it.
Because if there is something and Trump knows about it and Ron DeSantis is doing something
that Republicans would find morally objectionable or perhaps it's damaging the state of Florida,
how can you trust Trump at that point?
He's flat out saying he is putting his personal political interests above that of Republican
voters.
This information would be tailored to them because it's to win a primary.
But I guess having an informed public, having informed voters, I guess that's not important
to Trump.
I guess, as he has said, he loves the uneducated.
And as we have pointed out, it's his habit to keep those voters in the dark, to not let
them have access to information that he could use to personally benefit from later.
If you have any questions about who Trump is and you're a Republican, he just told you.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}