---
title: Let's talk about how to get the candidates you want....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XKYLTxfD-3o) |
| Published | 2022/11/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about civic engagement and electoralism.
- Mentions the challenge of getting a candidate who truly represents your beliefs through the primary and general election.
- States that electoralism is the least effective form of civic engagement.
- Suggests changing society through other forms of civic engagement like capacity building, direct service, research, advocacy, and education.
- Emphasizes the importance of changing societal thought to shift the Overton window.
- Lists various forms of civic engagement including personal responsibility, philanthropy, and participation and association.
- Advocates for using all forms of civic engagement to create deep systemic change.
- Stresses the need to build a power structure outside of existing ones to leverage for societal change.

### Quotes

- "Electoralism is the least effective form of civic engagement."
- "To change society, you have to change thought."
- "You can't rely on electoralism. You have to use every tool at your disposal."

### Oneliner

Beau explains why electoralism is the least effective form of civic engagement and advocates for utilizing all tools available to create deep systemic change by changing societal thought.

### Audience

Community members, activists, voters

### On-the-ground actions from transcript

- Build capacity at the local level to support chosen candidates (suggested)
- Engage in direct service to immediately impact outcomes (exemplified)
- Conduct research to provide necessary information for civic engagement (implied)
- Advocate through petitions and demonstrations to alter public opinion (exemplified)
- Demonstrate personal responsibility by setting an example of desired change (exemplified)
- Engage in philanthropy to address issues needing financial support (exemplified)
- Leverage contacts and networks on a social level to influence change (implied)

### Whats missing in summary

Importance of utilizing various forms of civic engagement to create lasting societal change.

### Tags

#CivicEngagement #Electoralism #SocietalChange #CommunityBuilding #Activism


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
civic engagement and electoralism and a person finding out that a phrase that I
say on this channel a lot is true. So recently I talked about electoralism and
talked about how it was one of the forms of civic engagement and that if you
wanted to use it you had to be involved from the beginning and you had to get
your candidate, the candidate that actually believes what you believe
through the primary. And I had a couple of people but one person in particular
sent a message saying hey but if that happens then they won't win in the
general. That's probably true. For most people watching this channel that is
probably true because for most people watching this channel your core beliefs
are outside of the Overton window in the United States, meaning the the frame of
normal political belief, of the average political belief. It goes back to that
idea that there are a lot of Republican candidates who realized they couldn't
win a primary without Trump but they might not be able to win a general with
him because the beliefs are so outside of the Overton window. For a lot of
people watching this channel a candidate who truly reflects your beliefs is going
to be in the same situation, which is why electoralism is the least effective form
of civic engagement. How do you change that? How do you change that fact, the
fact that the candidate you want might be unelectable in large parts of the US?
You have to use the other types of civic engagement. You have to change society.
The law and the lawmakers will follow. You have to shift thought. The other
types of civic engagement are capacity building, that's community networking,
building the infrastructure at a local level to a get your chosen candidate
through, but more importantly not be so dependent on the government in general
and therefore be less impacted when they make bad decisions. You develop that
capacity at the local level. Another is direct service. There are a lot of terms
for this for most the people who watch this channel. This is what gets the good.
This is devoting your personal time and energy to immediately affecting an
outcome. Research. This is making sure that all of the other forms of civic
engagement have the intelligence, have the information they need to present the
case to those people who are dead center of that Overton window and shift them.
Change thought. Get them to think in a more progressive, more accepting, more
free manner. Advocacy and education. This is petitions, demonstrations, things
designed to alter public opinion. Shift to the Overton window. You have
electoralism, which we've talked about. You have personal responsibility, which
is setting the example. You're being the change you want to see in the world.
You're behaving as if the Overton window has already shifted and that's how you
live your life. Worst-case scenario, at the bare minimum, at least you're not
part of the problem. You have philanthropy. Throwing money at it. And
people always joke about that, but trust me, if the problem is a lack of funds,
throwing money at it can be super helpful at times. And then you have
participation and association. This is a very similar to capacity building. You're
leveraging contacts and networks, but this is more on a social level. This is,
I'm not going to associate with this because of what it represents. I am going
to associate with this because of what it represents. And it helps shift the
window. To change society, you have to change thought. Once you do that, the
lawmakers that you want could get elected and then they can change the law.
But if you do it well enough, you don't even have to worry about the law, because
the majority of society has already shifted their view and you're
actually creating that change. So if you want that deep systemic change that a
whole lot of people watching this channel want, you can't rely on
electoralism. You have to use every tool at your disposal. You have to use all of
these forms of civic engagement. And you have to build that power structure that
exists outside of the ones that already do. And then you can leverage that to
shift thought and to eventually get the world you want. Anyway, it's just a
thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}