---
title: Let's talk about a message for this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J0wkWzR8Suw) |
| Published | 2022/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges the audience to understand a critical message within a week due to current events.
- Addresses political violence and the quest for power through symbolic targets and candidates.
- Emphasizes the importance of not allowing violent rhetoric to be a path to power.
- Warns that those currently unaffected will eventually become targets.
- Stresses the impact of authoritarianism on free enterprise and small businesses.
- Condemns those who deny election results and use violent rhetoric for perpetuating dangerous ideologies.
- Expresses the failure to send a strong enough message in the 2020 election.
- Calls for a rejection of election deniers and promoters of violent rhetoric to protect freedom.

### Quotes

- "Anywhere is a threat to freedom everywhere."
- "This is freedom and tyranny."
- "Those people should not get your vote."

### Oneliner

Beau stresses the urgent need to reject violent rhetoric as a path to power and confront the threat to democracy before it impacts everyone.

### Audience

Every concerned citizen

### On-the-ground actions from transcript

- Reject election deniers and promoters of violent rhetoric (exemplified)
- Educate others on the dangers of authoritarianism (exemplified)
- Advocate for democracy and freedom in your community (suggested)

### Whats missing in summary

The emotional depth and urgency conveyed by Beau's message can be best experienced in the full transcript.

### Tags

#CommunitySafety #Democracy #Authoritarianism #PoliticalViolence #Freedom


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a pretty important message.
And one that I really think people need to get and understand in the next week.
Not a lot of time to grasp this concept.
Given the current climate, there are a lot of people asking what they can do.
And we're going to talk about that too.
Over the last few days, there have been a number of acts of political violence
directed at symbolic targets, directed at candidates, directed at the other.
And people want to know what they can do to stop it.
The answer is really simple.
This is occurring because there is a certain subset of people that see it as a path to power.
They see it as their way to gain power.
That rhetoric that inflames this.
The way you can help is to make sure it's not a path to power.
We need to stop pretending that we're not looking at what we're looking at.
We need to stop acting like we don't know how this ends.
They see this as a path to power.
And sure, right now there's a whole bunch of people that are like,
none of that impacts me.
That doesn't have anything to do with me.
I'm not one of those other demographics.
Yeah, that's fine.
Just understand that you're missing a word.
Yet.
They're not coming after you yet.
They will.
We know how this story plays out.
We've seen it before.
They came for the Socialists and I said nothing because I was not a Socialist.
That's not the most famous line, but that's how that actually starts.
I know a lot of people want to make this election about the economy.
I get that.
The economy is important.
I understand.
You need to understand.
There is no free enterprise under an authoritarian state.
Doesn't exist.
If you're a small business person, you need to understand.
You're the competition of the people who are going to be in power.
Because they're going to be buddies with the big companies.
You don't get any protection.
They're not going to look out for you.
They're going to look out for those people who can give them tens of thousands of dollars
in campaign contributions.
That's probably not you.
We have to stop pretending like this isn't happening.
If you vote for somebody who is an election denier, you vote for somebody who uses violent
rhetoric, you vote for somebody who excused or downplayed the events of the 6th, you are
voting for this to continue.
That's what's happening.
Because you're telling them it's a winning strategy.
You're telling them this is the path to power.
This is the way.
And eventually it will come back on you.
We know how this ends.
We've seen this story over and over again.
If you vote for somebody who is denying election results, saying they won't concede, you are
giving up on the United States.
You are giving up on the idea of representative democracy and you are encouraging an authoritarian
state.
People say, you know, this is the most important election of our lifetime.
Nah.
2016 was the most important election of our lifetime and collectively the country failed.
Did better in 2020, but not by the landslide necessary to send the message.
Here we are two years later, people still excusing it.
People willing to overlook it.
Understand it will come back on you.
Anywhere is a threat to freedom everywhere.
Don't forget that.
Those who denied the election, those who use violent rhetoric, those who downplayed the
events of the sixth, those people should not get your vote.
This isn't conservative liberal.
This is freedom and tyranny.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}