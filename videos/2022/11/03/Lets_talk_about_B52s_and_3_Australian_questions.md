---
title: Let's talk about B52s and 3 Australian questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lckEg2tkXl4) |
| Published | 2022/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US plans to deploy six B-52s to Australia, which are part of the US nuclear arsenal and will be stationed there with an operations center.
- This move is aimed at deterring China from challenging Australian interests in the region and potentially influencing a future conflict involving Taiwan.
- The deployment of these aircraft is not an indication of imminent war, as the most immediate projections suggest any conflict with China is still a couple of years away.
- Australia is not financially responsible for this deployment; the US has devoted funds to upgrading Australian defense capabilities, including developing the necessary infrastructure for the aircraft.
- While Australia is perceived as a US ally, the presence of these aircraft serves as a form of deterrence and mutual assured destruction rather than making Australia a new target.
- Australia's long history of military alliances and support for the US has made it a reliable ally, and Chinese war planners are aware of the implications of any conflict involving Australia.
- The symbolic presence of these aircraft in Australia provides a counterweight to Chinese nuclear power and acts as a strong deterrent against any interference with Australian interests.
- The overall strategy involving these aircraft can be likened to an international poker game with strategic moves and deterrence tactics.
- The US is comfortable with strategic weapons in other countries, being a nuclear power itself, but not all nations view such deployments favorably.
- While the B-52s may not initially be equipped with nuclear weapons, their capability to carry them raises concerns for some observers.

### Quotes

- "Australia is one of the most steadfast allies the United States has."
- "It's the international poker game where everybody's cheating."
- "We're a nuclear power. We're very comfortable with this."

### Oneliner

The US deploying B-52s to Australia aims to deter China, showcasing Australia's longstanding alliance with the US in an international strategic move.

### Audience

International Relations Analysts

### On-the-ground actions from transcript

- Monitor diplomatic relations and escalations in the Asia-Pacific region (implied)
- Stay informed about international defense alliances and strategies (implied)

### Whats missing in summary

The full transcript provides deeper insights into the strategic implications of the US deployment in Australia and the dynamics of international relations. 

### Tags

#US #Australia #China #DefenseAlliance #InternationalRelations


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Australia
and six airplanes that have caused some waves
on the international scene.
And we are going to answer three different questions
from three different Australians dealing with these airplanes.
And it's entertaining in a way, because in the United States,
this really hasn't gotten any coverage at all,
because we're the US and we're just used to this type of stuff.
But there are some concerns, so we'll kind of go through it.
OK, so if you have no idea what I'm talking about,
the United States is planning on deploying six B-52s
to Australia.
They'll be stationed there.
They will have an operations center and all the facilities
they need to run there.
If you watched the video talking about the NATO exercise,
you already know what I'm about to say.
But for those that didn't, B-52s are part
of the US strategic arsenal, the US nuclear arsenal.
These aircraft will be nuclear capable.
Doesn't mean they're going to have nukes on them,
but they would be capable of delivering them
if the need arose.
OK, so the first question, and definitely the most pressing,
does this mean war is imminent?
No, no.
This is actually a move for deterrence
to change China's thinking when it comes to challenging
Australian interests in the region
and probably playing into a future Taiwan conflict.
The most immediate projections I have seen as far as China
making a move on Taiwan, that's still a couple of years away.
So war is definitely not imminent.
Why should Australia pay for this?
That's the cool part.
You're not.
The United States, I want to say it's like a billion dollars
that the US has devoted to upgrading Australian defense
capabilities.
And a big chunk of that was developing the infrastructure
for these aircraft.
As far as I know, there isn't a financial commitment
from Australia on this.
Now, there may be minor things like fencing or something
like that, but my understanding is
that the US is footing the bill for all of this.
Doesn't this make us a target for being a perceived US ally?
No, but also yes.
The six aircraft, that doesn't change the math.
But China definitely looks at Australia as a perceived US
ally because it is.
Australia is one of the most steadfast allies
the United States has.
But again, the idea behind this is mad.
It's mutually assured destruction.
It's deterrence.
It's putting the capability there in a way
for the United States to say, yeah, you're
a near peer and everything, but Australia's
been with us for 100 years.
In the United States, people tend
to forget when they talk about the greatest allies the US has.
Australia gets overlooked because they're
kind of like the quiet guy in the back of the bar.
Always has your back, but never runs their mouth.
That's Australia as far as the defense side of things goes.
And just as a real quick rundown, Afghanistan,
Australia was there.
Iraq and Syria against IS, Australia was there.
2003, Iraq, Australia was there.
The first time in Iraq, Australia was there.
Somalia, Australia was there.
Vietnam, Korea, World War II, this just goes back.
There are not many military alliances in the world
that are as reliable as that.
So Chinese war planners are very aware of this fact.
They know that if they plan to fight Australia,
they have to plan to fight the United States.
And if they plan to fight the United States,
they have to plan to fight Australia.
So the presence of these six aircraft,
that's not going to change the math.
It was that way before, and it will be that way
once they arrive.
China is going to be upset by it because introducing
that kind of strategic capability,
it provides a deterrence, and it provides a counterweight
to the Chinese nuclear power that exists.
Australia is not a nuclear power.
However, as we discussed in that video about the NATO exercise,
the United States has protocols for delivering nuclear weapons
to countries that are not nuclear powers
if the need arises.
And after 100 years of having each other's backs,
I'm fairly certain that Australia
would be a country the US would provide them to.
The presence of these planes is kind of symbolic of that.
And it also, I mean, it's 6B-52s.
That can't be underestimated.
That would be a pretty strong deterrent
when it comes to China and any designs
they may have on interfering with Australian interests.
It's the international poker game
where everybody's cheating.
And this is the United States sliding a couple wild cards
to Australia.
Don't know if you need them, but if you do,
they're there type of thing.
So to my knowledge, Australia isn't paying for it.
This definitely doesn't mean that we're going to be
a country that's going to be a nuclear power.
It definitely doesn't mean that war is imminent.
And the Chinese war planners would
have targeted Australia with or without this,
just due to history.
So those are the questions.
And for Americans, I understand a whole lot of countries,
having strategic weapons in their country.
That's not something, you know, in the US,
we're very used to that.
We're a nuclear power.
We're very comfortable with this.
Not all countries look favorably on it.
And while these planes will probably not
be equipped with nukes, they're very capable of it.
And that might be alarming to a lot of people.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}