---
title: Let's talk about a voting PSA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kTZJPdtmuxQ) |
| Published | 2022/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing a public service announcement regarding misleading text messages about voting locations.
- Urging people to fact-check information received via text messages with real sources.
- Warning about a company sending inaccurate voting information in Kansas, New Jersey, Illinois, North Carolina, and Virginia.
- Mentioning NBC's report about a similar issue in Oregon with the same company.
- Advising recipients of unsolicited texts to block the number, potentially missing correction texts.
- Emphasizing the importance of verifying voting locations to avoid unintentional or intentional voter suppression.
- Noting that this election's outcome relies on unlikely voters, making informed choices based on accurate information critical.
- Encouraging individuals, especially those who don't normally vote, to ensure their voting information is correct and have a plan to cast their vote.
- Pointing out the potential impact of misinformation via text messages on voters who are less familiar with the voting process.
- Stressing the significance of taking control of one's voting information to prevent malicious actors from suppressing votes.

### Quotes

- "Don't trust the text messages."
- "Make sure you are checking with real sources about where you're supposed to go vote if you plan to."
- "It's the unlikely voters who are going to rule this election."
- "Make sure you know where to go, you have a plan to get there, and all of that stuff."
- "There are certainly going to be malicious actors trying to suppress the vote, and this is a good way to do it."

### Oneliner

Beau provides a public service announcement: Verify voting locations, as misleading text messages could lead to voter suppression, impacting the voice of unlikely voters critical in this election.

### Audience

Voters

### On-the-ground actions from transcript

- Verify your voting location with official sources (suggested).
- Block unsolicited numbers sending voting information and seek accurate sources (suggested).
- Ensure you have a plan to cast your vote and know where to go (suggested).

### Whats missing in summary

Importance of staying vigilant against voter suppression tactics.

### Tags

#VoterSuppression #Election2022 #Misinformation #PublicService #VotingRights


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to provide a little bit
of a public service announcement.
And it's going to be important for a whole lot of people.
At time of filming, all coverage is
focused on what happened to the Pelosi family.
Because of that, when that coverage clears,
this might be overshadowed.
And this is going to be really important for a lot of people.
If you received a text telling you
where to go vote, what your drop-off location is,
stuff like that, don't trust it.
Don't trust the information in it.
Fact check it with a real source.
Don't go off of what showed up in your text messages.
A company has been sending out messages.
And a lot of the information has been inaccurate.
This is confirmed in Kansas, New Jersey, Illinois, North
Carolina, and Virginia.
NBC is reporting that last month, the same company
had a similar issue in Oregon.
Now, the company is saying it's an accident, it's an oversight,
and they sent out corrective texts.
However, if you get an unsolicited text,
you might block that number.
It might get filtered out.
So you may never get the correction.
Make sure that you are checking with real sources
about where you're supposed to go vote if you plan to.
You don't want to have your vote either unintentionally
or intentionally suppressed.
This is just one example.
Undoubtedly, there are companies and groups of people
who are doing this intentionally.
So it's something to be aware of.
Don't trust the text messages.
Sure, use the reminder to make sure
that all your information is up to date and everything
like that.
But don't go off of the locations provided.
It could definitely cause issues and cause your vote
to not matter or make it to where you couldn't vote,
where your vote won't count.
This election is one that the voice of the average person,
people who don't normally vote, is
what's going to be the deciding factor.
It's the unlikely voters who are going
to rule this election, who are going
to determine the future course of this country, who
is in political office.
It's going to be determined not by the people in the polls.
It's going to be determined by the unlikely voter, the person
that doesn't normally show up.
And for those people who don't normally show up,
if they're getting bad information via text,
it might cause a much larger problem
than with those who do normally vote,
because they know where to go.
Make sure you check the information you receive.
Make sure you know where to go, you
have a plan to get there, and all of that stuff.
Because on top of accidents, there
are certainly going to be malicious actors trying
to suppress the vote, and this is a good way to do it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}