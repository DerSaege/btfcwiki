---
title: Let's talk about Russian troops saying no...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QRYd_dZzgMk) |
| Published | 2022/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russian soldiers formed a union and went on strike due to unpaid wages, risking their lives for the government.
- The soldiers refuse to participate in a special military operation until they are paid what was promised.
- The situation is at the integrated training facility in Ilyanovsk, potentially involving around 100 to 1000 soldiers.
- This unrest could cause significant issues for the Russian government, leading to shortages and discontent.
- The government's inability to pay mobilization costs and lack of equipment is becoming apparent.
- Putin may need to address these issues to avoid further waste of life and potential regime downfall.

### Quotes

- "We refuse to participate in the special military operation and will seek justice until we are paid the money promised by our government."
- "This is a situation the Russian government is gonna have to trade very lightly with."
- "If he continues to double down, not just will there be even more waste in terms of life. It will bring down his regime."

### Oneliner

Russian soldiers strike over unpaid wages, risking government stability and equipment shortages.

### Audience

Activists, Human Rights Advocates

### On-the-ground actions from transcript

- Support Russian soldiers' rights (exemplified)
- Raise awareness about the soldiers' strike (exemplified)
- Advocate for fair treatment of conscripted soldiers (exemplified)

### Whats missing in summary

The emotional impact on the soldiers and the potential consequences of the government's actions.

### Tags

#RussianSoldiers #Strike #UnpaidWages #GovernmentInstability #Putin


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about some issues that the Russian government is having when
it comes to forced generation and getting troops ready to go where they want to send
them.
emerged last week that there are a number of soldiers who are very unhappy
and so unhappy they have basically formed a soldiers union and gone on
strike. These are conscripts, they're draftees, and the numbers involved vary.
There is a little bit of footage, easily a hundred. The main complaint is that at
least at the moment, at the time all of this reporting broke, the Russian
government owes them like 200,000 roubles, 195,000 roubles, something like
that. It's about three grand, a little more than 3,000 US, and that they haven't
paid it. They put out a statement, the soldiers, we are risking our own lives
to die for your safety and peaceful life. We refuse to participate in the special
military operation and will seek justice until we are paid the money promised by
our government headed by the president of the Russian Federation." Again a direct
shout out to Putin. Now to put this into perspective this is occurring at the
The integrated training facility in Ilyanovsk, to Americanize this for a second.
It's binning.
This is not some tiny outpost out in the middle of nowhere.
The reporting, again, there's no way to verify this, but I've seen numbers as low as 100,
as high as 1,000.
footage shows a hundred more or less. I didn't sit there and count but about a
hundred. They're chanting all kinds of things. It is very much a one for all and
all for one type of mood. This is a situation the Russian government is
gonna have to trade very lightly with. When they're going through training
they're handed something to train with. If you have a bunch of disgruntled people
that you have pulled back because they already have military service, perhaps
arming them without addressing their grievances isn't the best idea. That
That might lead to shortage of instructors down the road.
If the Russian government is having trouble paying the mobilization costs, just the checks
for the basic stuff when they first start, they're not going to be able to keep this
up much longer.
They don't have the equipment.
We've seen that.
out of equipment, they're running out of supplies, they're running out of ammo, they're having
to get shells from North Korea, and they've lost support from their major allies.
At some point, Putin has to call this.
If he doesn't, if he continues to double down, not just will there be even more waste in
terms of life. It will bring down his regime. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}