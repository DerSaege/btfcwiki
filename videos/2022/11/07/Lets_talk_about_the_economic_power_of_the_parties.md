---
title: Let's talk about the economic power of the parties....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eV9iCmX15HA) |
| Published | 2022/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Addresses a comment on economic power and political leanings in the US.
- Mentions a message received about conservative economic power and implications.
- Breaks down the GDP share between Republicans and Democrats based on county wins.
- Analyzes the economic disparity between Republican and Democratic counties.
- Suggests that the perception of conservative economic power is manufactured and not real.
- Points out that companies cater to diverse and tolerant viewpoints due to economic reasons.

### Quotes

- "Not nice word about me. Yeah, you bunch of not nice words about me are brainwashing the young."
- "Your math is wrong. Your belief is wrong. Your ideas are really bad."
- "The reason large companies are catering to the more diverse, more tolerant, more accepting viewpoints is because that's where the money is."

### Oneliner

Beau addresses misconceptions about conservative economic power and exposes the economic disparity between Republican and Democratic counties in the US.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Fact-check economic claims and share accurate information (implied)
- Support policies that address economic disparities (implied)

### Whats missing in summary

Deeper insights on the impact of economic disparities on political perceptions and societal divisions.

### Tags

#EconomicPower #PoliticalLeanings #USPolitics #IncomeInequality #FactChecking


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a little bit of serendipity
and the relative economic power of various political leanings within the United States.
Recently, under a video, one of y'all left a comment about something that I just found fascinating,
and it was a breakdown of economic power.
And I followed the chain of events and kind of looked into it,
and I thought it was really interesting.
A couple hours later, I got a message that I'm going to read,
and then we're going to talk about how the two things fit together.
You said the conservative demographic was shrinking so companies could ignore us.
You're wrong. Not nice word about me.
Yeah, you bunch of not nice words about me are brainwashing the young.
But remember, not nice thing about me, it's us,
the red hat wearing Trump loving flag waving Americans that have all the money.
Your giant list of slurs.
Like it's huge. It's actually impressive.
There's a part of me that actually wanted to read it, but I'm pretty sure YouTube would get mad.
One of these I haven't heard since my uncle who went to Vietnam passed away.
Unless you are a straight white man, there is something in here for you.
Okay, so your giant list of slurs don't own anything.
Not the word here.
Liberals in big cities are barely scraping pie.
You're poor and should be.
Wow. So what do y'all think? Y'all think that's true?
I mean, I'm doing a video about it. You know it's not.
Okay, so the general thesis in this masterpiece is that the MAGA crowd specifically has all the money.
That's the impression that is being cast by that message.
We don't have information specific to the red hat wearing Trump loving flag waving Americans,
but we do have information related to Republicans in general.
See, the information that somebody randomly posted dealt with the share of the GDP
that is loosely attributed to both parties.
So in the 2020 election, Biden won 520 counties.
Okay, roughly. There's some that didn't get counted in this.
Trump won 2,564, giving Trump a 2,000 county advantage.
Now, based on that, it would certainly seem like the Republican side of things
should have a pretty big share of the GDP.
That's the gross domestic product for the person who sent this message.
That's the amount of money that's made in the country.
Those 2,564 counties total 29% of the GDP.
The 520 that voted for Biden account for 71% of the GDP.
So what does that tell you?
I think that perhaps the reason Republican candidates are so eager to scapegoat
and lead people to other in a way that would lead to that giant list of slurs
is so those red hat wearing Trump loving flag waving Americans don't realize how bad off they are.
Give them somebody to look down at, to kick down at, and they don't realize
even though we're talking about a 2,000 county advantage, they only have 29% of the GDP.
I think that's far more likely.
The reason you have that perception is because they created it. It's not real.
The economic disparity here is huge.
I looked into it because when I saw the comment saying that it was true and they rounded it up,
they said that Republicans had 30%.
I didn't actually believe it. I was curious.
So I started looking into it. And sure enough, it's true.
That economic divide is pretty pronounced,
especially given the amount of advantage, the advantage that the Republican Party has
just by sheer number of counties.
I get that the Democratic Party generally has control of the cities
and there's a lot more economic activity in cities.
But I mean that's a massive difference when you're talking about that number of counties,
2,564 to 520.
You would think they'd make some of that up just by sheer volume of counties.
Your math is wrong. Your belief is wrong.
Your ideas are really bad.
The reason large companies are catering to the more diverse, more tolerant,
more accepting viewpoints is because that's where the money is.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}