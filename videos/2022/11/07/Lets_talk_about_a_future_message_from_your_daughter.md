---
title: Let's talk about a future message from your daughter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mhAE4V2ESzw) |
| Published | 2022/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Receives a message about a father who was once loving but became radicalized online, switching political affiliations and using hateful speech.
- The father's sudden death from a health condition, not drugs, is attributed to propaganda-driven MAGA brainwashing.
- The father, living in poverty without health insurance, found solace in identifying with the MAGA ideology.
- The message expresses gratitude for Beau's videos, which helped the father reconnect with reality and himself.
- MAGA became intertwined with the father's identity and allowed him to look down on others based on visible differences like race.
- The daughter credits Beau for preventing arguments and influencing her father positively before his death.
- Beau warns those who relate to the story not to let political differences lead to family alienation and regrets, urging them to reconsider their path.

### Quotes
- "MAGA was a bomb that soothed him."
- "I guarantee you, your daughter will write a message to me like that."

### Oneliner
Beau receives a heartfelt message about a father's radicalization, death, and the impact of propaganda-driven ideology on family dynamics.

### Audience
Family members, individuals vulnerable to radicalization.

### On-the-ground actions from transcript
- Reach out to loved ones showing signs of radicalization, provide support, and encourage critical thinking (suggested).
- Offer resources for mental health assistance and healthcare access for those in need (suggested).
- Facilitate open dialogues and education on the dangers of extremist ideologies within communities (suggested).

### Whats missing in summary
The emotional weight and depth of the daughter's gratitude towards Beau for his influence on her father's life and their relationship.

### Tags
#Radicalization #FamilyDynamics #PoliticalIdeology #CommunitySupport #MentalHealth


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, I'm going to read a message.
I'm going to read a message.
Under normal circumstances, I get a message
very similar to this, probably every other week, maybe
every three weeks, similar in tone, maybe not as poignant,
but similar.
Over the last couple of weeks, I've
been getting at least one a day, because it's
close to the election, you know?
And it's bringing it all out.
I ask permission to read this one,
because it is incredibly well-written.
And I know that somebody like the dad is watching this.
I'm going to read this.
All his life, he's a very loving person,
but became radicalized online.
He switched from Democrat to GOP.
He started using evangelical language,
even though we're Catholic.
He started saying hateful things, even the N-word,
which is very unlike him.
Underneath all that, my dad was still in there somewhere.
I would have him watch your videos.
And it seemed to help him come back to reality
and to himself a little.
He liked your videos, because he assumed,
based on how you looked, that you were like him.
His death was sudden, and it sounded like a drug overdose.
All of a sudden, his total personality change
and weird behavior made sense.
We suspected drugs, since he was acting so different
and unlike himself.
Deaths of despair are on the rise.
Well, it turns out it wasn't drugs.
It was a health condition.
He never got checked out, because he
didn't have health insurance.
And he couldn't afford it.
As for the personality change, yeah,
that was just your basic MAGA brainwashing.
No substances, just propaganda.
I still consider his death a death of despair.
He was living in poverty and cancer.
He was in poverty and couldn't afford health care.
He didn't have much, but he had MAGA.
MAGA was a bomb that soothed him.
It became entwined in his identity
that he could other people and look down on those people.
When you don't have a lot to be proud of,
you look for differences to cling to and feel superior.
Any difference will do.
Race and skin color are highly visible differences.
And that's the difference you latch on to.
Thank you for making your videos, Beau.
You saved me and my dad from having many fights.
And it truly did have an impact on him.
I appreciate you.
I'm willing to bet that if you see yourself in this situation,
if you relate to the dad in this story,
and undoubtedly from your point of view,
it's somehow different.
They just don't get it or whatever.
I'm willing to bet that if you see yourself
in this situation, if you relate to the dad in this story,
it's somehow different.
I'm willing to bet you've never met the people
that you're throwing your family away for.
And make no mistake about it, if you continue
and you don't go back to who you were,
I guarantee you, your daughter will write a message to me
like that.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}