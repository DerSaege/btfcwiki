---
title: Let's talk about the objection to unpacking the court....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U5v56Rxc77Q) |
| Published | 2022/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Exploring the idea of expanding and unpacking the court, addressing objections.
- Acknowledging the possibility of the Republican Party expanding the court if given the chance.
- Emphasizing the fight against authoritarianism and the role of the Democratic Party in slowing the descent.
- Suggesting that expanding the court could buy some time in the worst-case scenario.
- Urging people to stay engaged in the political process despite challenges.
- Warning against expecting fairness from those in power, who prioritize power over constituents.
- Staying vigilant in the fight against authoritarianism, even after Trump's presidency.
- Encouraging persistence and engagement in the face of setbacks, acknowledging the long-term nature of the struggle.

### Quotes

- "It's a fight, and it's going to be a long one because the United States has slid very, very far down that slope into authoritarianism."
- "Don't throw your hands up, though. There are a whole lot of people who are counting on us."
- "You need to wrap your minds around that right now."
- "They care about power for power's sake."
- "That slide towards authoritarianism [...] it didn't stop the day he left office. We're still there."

### Oneliner

Beau addresses objections to court expansion, advocating for Democratic Party as a temporary safeguard against authoritarianism and urging continued engagement in the ongoing political struggle.

### Audience

Political activists, Democratic supporters

### On-the-ground actions from transcript

- Stay engaged in the political process (implied)
- Advocate for court expansion as a temporary measure to slow authoritarian descent (implied)

### Whats missing in summary

The full transcript provides in-depth analysis on the implications of court expansion in the context of authoritarianism and urges sustained political engagement despite challenges.

### Tags

#CourtExpansion #Authoritarianism #DemocraticParty #PoliticalEngagement #FightForDemocracy


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
expanding, unpacking the court.
And we're going to talk about
the big objection
that comes up anytime somebody suggests this.
And that objection is that
well if you do that,
obviously the Republican Party, next time they have power, they have control
over the House, the Senate,
and the White House.
Well they're just going to do the same thing and they'll expand it themselves.
Yeah, yeah, that's true.
Very well, might happen that way.
Not a guarantee,
but yeah, that could happen.
Okay, so
there are themes that I have talked about on this channel for years,
years, talked about them so much
I'm willing to bet that I don't even have to finish the sentences.
We're going to talk about the fourteen
people already, characteristics of fascism.
Joe Biden is not
a savior.
We are in a slide to
authoritarianism, right?
This objection
saying that
that the Republicans might just go ahead and expand it even further,
it acknowledges all of that.
It says, yes, these themes are true.
So from there,
you have to acknowledge that you're in a fight against authoritarianism.
Historically speaking,
when the centrist party
just kind of has a lukewarm response
to the emergence of authoritarian groups,
what happens?
The authoritarians take power.
That's what happens.
Now,
let's look at it another way.
The worst case scenario,
the Democratic Party expands the courts.
In just a few years, the Republican Party
manages to have majorities everywhere and they expand it too.
The worst case scenario,
if things go wrong,
you end up with what you have now.
I don't know that that's a really strong argument against it.
It doesn't seem like it.
There are
some realities that people have to face.
Going back and forth like that, yeah,
yeah, absolutely. It's a sign that we are headed towards a failed state.
That's a fact.
And it's a sign that we are deep into that slide towards authoritarianism.
These things, I think people know that.
The thing that is going to be hard for a lot of people who watch this channel
to really just admit to themselves
is that the political entity
that currently exists in the United States
that is your best shot
of slowing that descent
into authoritarianism
is the Democratic Party.
You don't have to like it.
I don't.
But it's the reality.
You can talk about what they could have done here,
should have done there,
so on and so forth.
But the real question here,
if the Democratic Party had made the appointments
of the people who just voted
to take away your rights,
would we be having this conversation right now?
Because they would have appointed different people, right?
They may not be as aggressive
or as progressive
as you want them to be,
but they are the parachute.
They are what is slowing the descent.
If they expand the court,
it buys a few years.
Worst case scenario.
So to me, at this point,
it seems like it's really the only option.
And then to that objection,
take it out of the political for a second.
Understand that there are a whole bunch
of more rulings
that are going to be coming down the pike.
I want you to picture yourself talking
to one of your friends who wants to get married.
That because of the court's push
against marriage equality,
may not be able to soon.
And say, oh, well,
I know that we could expand the court.
We could try to go that route.
But in a few years,
it'll just go back the other way anyway.
So there's no point in trying.
You would never say that.
You would never say that.
You would never say that.
It's a fight, and it's going to be a long one
because the United States has slid very,
very far down that slope into authoritarianism.
Saying something won't work
because the other side might have some response to it,
that's a guaranteed way to lose.
It's the best move right now.
People want to talk about impeaching the judges.
Sure. I mean, you could try.
I'm fairly certain that that is just a non-starter.
You're not going to get the votes for it.
It'd be a whole lot easier to get the votes to expand it.
And people, again, they want to talk about
turning it into federal legislation.
Yeah, that might help, maybe.
Or the court just strikes that down, too,
for a different reason.
You're not dealing with people who care about the rule of law.
You're not.
You need to let that go.
Don't expect them to play fair.
They're not going to.
They care about power for power's sake.
They do not care about their constituents.
They don't care about any of that.
Their rhetoric, none of that matters.
They care about power.
The American people and the Democratic Party,
especially, need to wake up to that.
The threat that everybody felt while Trump was in office,
it was visible, that slide towards authoritarianism.
We could see it. We could read the tweets.
It didn't stop the day he left office.
We're still there.
We still have to fight back.
And yeah, sometimes it's going to be two steps forward
and one step back, and that's fine.
Don't throw your hands up, though.
There are a whole lot of people who are counting on us,
those people who aren't personally impacted by these decisions,
to stay in the game, to not throw our hands up.
They don't have the numbers by themselves.
That's why they're being targeted.
It is very important that those of us who look at these
rulings and are like, man, that is horrible, but aren't
personally scared, aren't personally impacted,
it is more important than ever for us to stay in the game.
Don't throw your hands up because the solution isn't a
permanent one, because we're in for a long fight,
and you need to wrap your minds around that right now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}