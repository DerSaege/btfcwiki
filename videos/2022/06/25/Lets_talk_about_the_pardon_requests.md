---
title: Let's talk about the pardon requests....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QiZG8SLaxKE) |
| Published | 2022/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Focuses on the pardons discussed in a recent hearing regarding requests made to Trump by specific individuals like Andy Biggs, Mo Brooks, Matt Gates, Gohmert, Scott Perry, and the Space Laser Lady from Georgia.
- Raises attention to the email from Brooks requesting pardons for himself and Gates, with the additional suggestion to pardon all congressmen and senators who voted against electoral college vote submissions of Arizona and Pennsylvania.
- Suggests two interpretations of Brooks' suggestion: either believing the act warrants a pardon or providing cover for their own pardons.
- Points out that individuals seeking pardons have been denying, deflecting, or downplaying the committee's claims, implying a tactical error.
- Warns that denials and dismissals may be refuted soon by the committee, as those who requested pardons are unlikely to have forgotten.
- Emphasizes that while the pardons are entertaining and sensationalized, the real focus should be on the pressure campaign and the committee's objectives.
- Acknowledges the attention-grabbing nature of the pardons but stresses the importance of understanding the broader context and objectives of the committee.

### Quotes

- "The pressure campaign, that's where the real goods are as far as what the committee is seeking to do."
- "The pardons, it's probably the most interesting, it is the most entertaining, no doubt."
- "Denials and dismissals, they need to be remembered because I have a feeling that all of these statements will be refuted in pretty short order."

### Oneliner

Beau sheds light on the sensationalized pardons discussed at a recent hearing, urging attention towards the broader context of the pressure campaign pursued by the committee.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to express your concerns about the handling of pardons and pressure campaigns (suggested).
  
### Whats missing in summary

Deeper insights into the potential ramifications of denying or deflecting claims made by committees and the importance of transparency in political processes.

### Tags

#Pardons #PressureCampaign #CommitteeHearing #PoliticalAnalysis #Transparency


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a specific piece of the last hearing.
We're going to talk about the pardons.
There are a lot of people kind of focusing in on the named individuals who the committee
is saying requested pardons from Trump.
And those names are Andy Biggs from Arizona, Mo Brooks from Alabama, Matt Gates from Florida,
Gohmert from Texas, Scott Perry of Pennsylvania, and the Space Laser Lady from Georgia.
These are the people that everybody's focused in on.
But there's something else.
There was a case made that I think people should pay a little bit of attention to.
When Brooks sent his email requesting a pardon for himself and Gates, he suggested that every
congressman and senator who voted to reject the electoral college vote submissions of
Arizona and Pennsylvania also get a pardon.
Now there's two ways to read that.
One is that he believes that act alone warrants needing a pardon.
I mean sure, maybe, depending on how broad you wanted to make this, but I'm pretty sure
that they have some kind of legislative immunity for their votes.
Another way to look at it would be that they were trying to provide cover for their own pardons.
Everybody gets a pardon.
That way we don't stand out.
That makes more sense to me.
Now one of the things that's occurring here is that pretty much everybody that has been
claimed by the committee as seeking a pardon, they've all done something to deny, deflect,
downplay, or otherwise diminish this story.
Many saying that it's just not true.
I'm going to suggest that's a tactical error.
If there is anything that we have kind of seen from the committee thus far, it's that
any time somebody comes out to say, oh that's not true, after the committee presents something
like this, it doesn't take long for them to reach back into their back pocket and be like,
oh really, because I got the receipt right here.
And I imagine we're going to see that pretty soon.
The people who requested pardons, they know whether or not they requested pardons.
That's probably not something that you would let slip your mind.
So the denials and dismissals, they need to be remembered because I have a feeling that
all of these statements will be refuted in pretty short order.
The pardons, it's probably the most interesting, it is the most entertaining, no doubt.
But it's not really the most important piece.
This is just the most sensationalized.
We'll definitely get people talking about it again.
But the pressure campaign, that's where the real goods are as far as what the committee
is seeking to do.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}