---
title: Let's talk about what they're going after next....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FEmw7k1MPIA) |
| Published | 2022/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentioned previous videos about upcoming court decisions.
- Received questions about being alarmist.
- Explains why certain cases will be reviewed.
- References cases like Griswold, Lawrence, and Obergefell.
- States the intentions of the court as written in rulings.
- Points out the impact on freedoms like marriage equality and birth control.
- Emphasizes that it's not alarmist or fear-mongering, it's based on documented intentions.
- Addresses the election of an authoritarian under Trump and subsequent Supreme Court appointments.
- Urges not to overlook the court's objectives.
- Warns about the potential consequences of ignoring the court's stated direction.

### Quotes

- "This isn't me like making this stuff up. It's in the ruling."
- "They're telling you, this is what we're coming for next."
- "When somebody tells you who they are, believe them."
- "They're demonstrating it very clearly where they want to go from here."
- "They said it, not me."

### Oneliner

Beau explains why upcoming court decisions on key cases signal a threat to established freedoms, cautioning against overlooking their intentions.

### Audience

Activists, Voters, Concerned Citizens

### On-the-ground actions from transcript

- Challenge potential threats by mobilizing community advocacy efforts (suggested)
- Stay informed about legal developments and implications for civil liberties (implied)

### Whats missing in summary

Deeper insights into the potential consequences of judicial decisions and the importance of vigilance in safeguarding rights.

### Tags

#CourtDecisions #CivilLiberties #Activism #Authoritarianism #CommunityAdvocacy


## Transcript
Well howdy there internet people. It's Beau again. So today we're going to talk about what they're
going to go after next. I have mentioned in previous videos things that will be coming
down from the court, things that they want to review. I have had a number of questions
sent in about that, asking how do I know that they're going to go after those,
and basically asking me if I think that I'm being a little alarmist, and maybe even perhaps
fear-mongering. Okay, so we're going to go over why I think they're going to review certain cases
that establish a lot of freedoms that people enjoy today. They said they were going to.
It's really that simple. This isn't me like making this stuff up. It's in the ruling.
For that reason, in future cases we should reconsider all of this court's due process
precedents, including Griswold, Lawrence, and Obergefell. Obergefell, that's marriage equality.
That is marriage equality. Lawrence v. Texas, that's deciding whether or not the state has the right to
make being gay against the law. The act, you know, coming into your bedroom in a surveillance state
that as exists today. And then Griswold determines whether or not you can buy birth control.
It's not alarmist. It's not fear-mongering. They wrote it down. They're telling you,
this is what we're coming for next. The United States has to acknowledge at this point it elected
an authoritarian under Trump, and that authoritarian put authoritarians on the Supreme Court.
This isn't speculation. They wrote it down. This is what they're coming for next.
This isn't the time to look the other way on this. There were a whole lot of people saying,
oh, they'd never overturn Roe, and here we are. And if we're not careful, in a few months,
we'll be saying, oh, I never really thought they'd do that about these cases as well.
They're telling you, when somebody tells you who they are, believe them.
They're demonstrating it very clearly where they want to go from here.
And this court seems very intent on making sure that state governments can fully regulate
anything you do in a bedroom. That seems to be the goal here.
And you can look away. You can pretend that it's not true all you want.
They said it, not me.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}