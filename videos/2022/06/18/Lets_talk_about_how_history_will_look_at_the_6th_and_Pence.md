---
title: Let's talk about how history will look at the 6th and Pence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P2jWSFZbSk8) |
| Published | 2022/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the historical significance of the events of January 6th and the ongoing hearings.
- Reminds that historians will document the actual story, ensuring it's available for those interested.
- Mentions the tendency to personify events in American history, focusing on one person rather than the collective.
- Suggests following the hashtag H-A-T-H on Twitter for historians at the hearings.
- Predicts that Mike Pence may be remembered as the hero who saved the American experiment by not cooperating with the attempted coup on January 6th.
- Emphasizes the critical nature of Pence's narrative from that day, which will shape his legacy.
- Urges Pence to explain his actions on January 6th to the American people without spin.
- Points out the importance of Pence continuing to show courage and accountability to prevent future authoritarian threats.
- Stresses the need for Pence to publicly address why he stayed on January 6th despite the risks.
- Warns that without accountability for the events of January 6th, there is a risk of similar incidents happening again.

### Quotes

- "This was a major historical event. It will probably be as transformative for the United States as Pearl Harbor was."
- "History is not done with Vice President Pence."
- "He's through the door. If he doesn't and there is no accountability, there is no message sent, they will try again."

### Oneliner

The historical significance of January 6th and the critical role of Mike Pence in shaping its narrative and preventing future authoritarian threats underscore the need for accountability and courageous leadership.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact historians at the hearings to stay informed and follow the hashtag H-A-T-H on Twitter (implied)
- Hold leaders accountable for their actions and demand transparency from public figures (implied)

### What's missing in summary

Importance of acknowledging historical events beyond partisan lenses and ensuring accountability for actions that shape the nation's narrative. 

### Tags

#USHistory #MikePence #Accountability #AmericanDemocracy #Authoritarianism


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the history of the 6th.
It's not written yet, but we already
know the possible storylines.
We know where it goes from here.
It's one of those things that's happening during the hearings
is I watch people's responses to it.
I have to wonder if people really
understand the historical significance of what
occurred on the 6th and what's happening right now.
A lot of people are looking at this
through a very partisan, very limited lens.
They're thinking in terms of the next election
rather than in terms of American history.
This was a major historical event.
It will probably be as transformative
for the United States as Pearl Harbor was.
And I don't know that people are really viewing it that way.
And the thing is there are historians
that are going to document the actual story.
They're going to make sure that the real story is
available for those who want to read it.
In fact, on Twitter, you could follow it.
Hashtag H-A-T-H, historians at the hearings.
But the thing is that's the history
that will be written, recorded, and read
by a very limited number of people
because what will be remembered isn't American history.
It's American mythology because we as a country
tend to personify events.
One person, right?
And that's how we remember stuff.
Who charged up San Juan Hill?
Those who know the answer just said Teddy Roosevelt.
Name any of the other more than 8,000 Americans
that were involved in that battle.
Probably can't.
I mean, Teddy wasn't even in command,
but that's American mythology, right?
This will be no different.
The real history will be there.
It'll be recorded.
But in the public's eye, for most people,
it will be the story of one man.
And it's not Donald Trump.
We don't tell the story through the eyes of the villain.
All right?
Who's the hero?
Who will be remembered as the hero
in one of the possible futures, one of those histories
that is written?
It's Mike Pence.
You may not like it.
It doesn't matter.
He very well might go down in history
as the man who saved the American experiment
because he wouldn't cooperate with the attempted coup.
Single-handedly, with the opposition
who wanted to take him out 40 feet away,
he stayed and made sure that everything
was conducted and concluded.
That's going to be the story, one possible history.
Mike Pence.
But that version of American mythology,
that's not set in stone yet because that's
assuming that the American experiment actually
was saved that day.
And it very well might have been.
Or it could be viewed as the beginning of the end.
January 6th could be viewed as the beginning of the end,
in which case Pence gets remembered
as the man who showed courage the day of,
but then couldn't find that courage again
to really put down the authoritarian threat
to the republic.
History is not done with Vice President Pence.
His story will either be one of heroism and courage
or one of sporadic completion of duty.
His story, his narrative of that day is critical.
He has to tell the American people what
happened with no spin.
He has to explain why he wouldn't
leave with the Secret Service.
And just so you know, there's more than one
possible answer to that, none of which
paint him in a bad light.
I mean, as much as most of the people who watch this channel
don't want to give credit of any kind to Pence,
had he gone the other way, I would
be willing to bet you wouldn't be watching YouTube right now.
I don't think Americans understand
how close we were, really.
And mythology aside, the fact that it
didn't spiral out of control really
does have a lot to do with Pence.
But history still has its eyes on him.
He still has more to do.
He has to explain what happened.
He has to tell the American people
why it was so important to get it done right then,
why it was so important to make sure
that everything that was supposed
to happen on the 6th was done.
6th happened on the 6th, why he couldn't leave,
despite the risk.
I mean, understand, people are making a big deal about him
possibly being worried about what might
happen if he got in the car.
I imagine he's probably pretty worried about what
would happen if he stayed.
There were other ways to leave, and he didn't.
That's a really important piece of this puzzle,
and it's one we don't have the answer to yet.
We don't have the answer to that question, and we need it.
This is an event that it will be way more than just
a couple paragraphs in the history books.
And Pence gets to write his character.
He gets to explain exactly what happened,
but he has to do it in public.
Memoirs or a book later, it's not
going to cut it because the path that is laid out for him,
it's one of driving those final nails into that rising
tide of authoritarianism.
And he's in a position to do it, but he
has to show the same amount of courage he showed on the 6th.
He can't stop now.
He's through the door.
If he doesn't and there is no accountability,
there is no message sent, they will try again.
And maybe next time, the person who has to make the decision,
the person who is standing in their way,
decides to go along with it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}