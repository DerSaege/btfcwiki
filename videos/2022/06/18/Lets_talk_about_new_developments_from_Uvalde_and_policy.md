---
title: Let's talk about new developments from Uvalde and policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LEmygVjhVHo) |
| Published | 2022/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the new information released regarding an incident in Texas involving two Uvalde PD officers and a deputy.
- The officers had the chance to intercept a gun before it entered a school but didn't take the shot, fearing hitting a child.
- Raises concerns about the department's lack of transparency and hiring an outside law firm to avoid releasing footage.
- Emphasizes that simply having a weapon does not make someone an operator or shooter; extensive training is necessary.
- Criticizes the idea of arming teachers, pointing out the unrealistic expectations and lack of proper training.
- Mentions the unrealistic portrayal of firearms ownership and the misconception that having the right tool guarantees success.
- Expresses skepticism about arming teachers as a solution, citing the challenges even trained officers faced in similar situations.
- Urges people to understand the importance of training and proficiency in handling firearms before considering arming teachers.
- Questions the myths and misguided policies around arming teachers, stressing the need for realistic solutions.
- Encourages thoughtfulness and reflection on the implications of arming untrained individuals in critical situations.

### Quotes

- "Your policy shouldn't be guided by myths."
- "Without the training, the tool means nothing."
- "Arming teachers isn't the solution."

### Oneliner

Beau talks about the need for proper training and caution against arming unprepared individuals in critical situations, like teachers in schools.

### Audience

Policy makers, educators

### On-the-ground actions from transcript

- Ensure proper training for individuals in critical situations (implied).

### Whats missing in summary

The full video provides a more in-depth analysis of the risks and implications of arming untrained individuals in critical scenarios.

### Tags

#Texas #GunSafety #PolicyDebates #Training #SchoolSafety


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we are going to talk a little bit more
about what happened out there in Texas,
some new information that has been released.
And we're gonna talk about how that new information
should serve as a little bit of a cautionary tale,
and it should inform our policy decisions,
policies that are currently being debated.
So if you don't know, a deputy out there has said that there were two Uvalde PD officers
who had the opportunity to intercept the gun before he entered that school.
They had the opportunity to take the shot, and one of them was armed with an AR.
They didn't, according to them, because they were worried they might hit a kid that was
around. Okay. All right, fine. I know that there's a lot of people who, given
the less than forthcoming nature of the department thus far, are probably just
brushing all that off. They're like, yeah, whatever. You failed again. I get it. I
really do. Especially given the fact that it appears that the department there
plans on hiring an outside law firm to argue that, well, they shouldn't have to
release anything like footage because it would be embarrassing. I get it, but let's
say for a moment that it's true because I think it very well could be. Having
that thing in your hand does not make you an operator. It doesn't make you a
shooter, it never has. The people who have to do this, who have to actually make
entry and shoot in situations like that, they have to be trained. That they have
to know that their rounds are going to go exactly where they want them to. None
of this oh it hit the six ring that's okay no because in real life the six
rings a kid you have to be able to put your mag into a 3x5 card if not you have
no business doing this it's that simple sounds like a criticism of the cops
right? This goes for teachers too. In the mythical scenario that people are
presenting, where the teacher, you know, somehow rushes forward and saves the
today, that requires them to do exactly what those cops couldn't.
That requires them to be able to step out into a hallway or inside of the classroom
even and fire over the heads or in between the gap of scattering, screaming, scrambling
children.
They're probably not up to it.
Part of the mythology that goes along with all of the masculine garbage that has now
been associated with firearms ownership in the United States is that, well, if you have
the right tool, no, no, that's not how it works.
the right tool doesn't mean that you can automatically do it. It's actually really
hard. And your range day once every three months, once a month, once a week doesn't
cut it. This should be a clear indication that arming teachers will not have the
result that you want it to, the one that you're imagining. Because what you're
doing is putting them in a situation that they didn't sign up for, that even
cops apparently couldn't handle. I wish that we could assemble a panel
of real shooters and ask them what the most difficult scenario they can imagine
is. Because in casual conversation this is something that comes up. The top two
are always the same. A school or a stadium for the same reasons. Your
Your policy shouldn't be guided by myths, and that's what's happening.
Arming teachers isn't the solution.
Sure, in one case out of a thousand, it might turn the tide if everything goes in the teacher's
favor, but that's not a solution, not given the fact that there's probably going to be
far more negligent discharges. Without the training, the tool means nothing. This
incident, this little piece of information needs to really be digested
by those people who think that, well it's really that simple, they didn't go in
because they didn't have ARs. Well, apparently that's not true. And had they
had the level of training necessary, they wouldn't have had to go in because they
could have taken the shot outside. And again, I'm not trashing the
cop because he might have been right. She might have been right. If you can't put
your rounds, where they're supposed to go, you don't even need to consider this.
And I mean far more than what you're probably imagining.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}