---
title: Let's talk about New Mexico, foreshadowing, and the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0pJcejUlPiU) |
| Published | 2022/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The situation in New Mexico's Otero County foreshadows future election uncertainties unless the committee and the Department of Justice provide a clear resolution.
- The Commission in Otero County initially decided not to certify the election, mirroring desires some had for Pence during the 2020 election.
- Despite no evidence of wrongdoing in the primary election, the issue has morphed into a political stance and talking point.
- Repetition of claims can lead people to believe they are valid, influencing politicians to conform to the pressure.
- Politicians reluctantly certified the election after pressure from the Supreme Court.
- The prevalence of conspiracy theories and grandstanding could pose a significant threat to the United States during the midterms and future elections.
- Understanding the events leading up to January 6th is vital, as misinformation continues to influence individuals across the country.
- The situation has transcended beyond Trump and become a dangerous political position with wide-reaching implications.
- It is imperative for the committee and the Department of Justice to take action in this evolving issue.

### Quotes

- "Understanding the events leading up to January 6th is vital."
- "Repetition of claims can lead people to believe they are valid."
- "The situation has transcended beyond Trump."
- "It's imperative for the committee and the DOJ to take action."
- "This issue is far more dangerous than most people think."

### Oneliner

The situation in New Mexico's Otero County foreshadows election uncertainties unless resolved, with conspiracy theories morphing into political stances, posing a significant threat to the US.

### Audience

Politically Aware Citizens

### On-the-ground actions from transcript

- Contact local representatives to advocate for clear resolutions in election disputes (implied).
- Join organizations working to combat misinformation and conspiracy theories in politics (implied).

### Whats missing in summary

The emotional impact of misinformation spreading and its potential to undermine democratic processes. 

### Tags

#ElectionUncertainties #ConspiracyTheories #PoliticalStances #Misinformation #CommitteeActions


## Transcript
Well, howdy there, Internet people.
Let's bow again.
So today we're going to talk about something that happened in New Mexico
and what it has to do with the committee and the midterms and the 2024 election.
Now, the situation in New Mexico, in Otero County, has been resolved, more or
less, but it's foreshadowing for what we can expect down the road, unless the
committee in the Department of Justice provide a clear resolution here. The
Commission in Otero County said they weren't going to certify the election.
They did what I guess some people wanted Pence to do. They weren't going to
certified. Now, legal mechanisms in New Mexico, let's just say they changed their
mind, and at the end of it two of the three voted to certify. The thing is the
the primary election there, there was no evidence of anything, but it's become a
political stance now. It's not even just a conspiracy theory. It's something that
has evolved into a talking point. And we've discussed it on the channel before
how sometimes, especially in relation to foreign policy, something will be said
and it makes sense at the time or maybe it doesn't make sense at the time. But
it's said enough that eventually people believe it's the right move and then
they start expecting politicians to behave in that manner and that's kind of
what happened. It didn't make sense when the claims first came out but it was
repeated so often that politicians caved to that pressure. Now the Supreme Court
there basically did force their hand and they begrudgingly went ahead and
certify it, but we can expect this to play out in the midterms and in the next
election. People wanting to grandstand, some of them who may genuinely believe
the conspiracy theories and some of them just doing it for their own political
ends, but on a wide enough scale that is something that can actually undermine
the United States. This cast a great deal of importance on what's happening
up on the hill, what's happening during these hearings, and why it's important
for Americans to understand what actually occurred in the run-up to the
sixth and how all of those people were tricked because it's still happening.
There are still people around the country who are operating under that
same assumption and to this day there's still no evidence. We're in a
situation where the committee and the DOJ have to act. There's no other way to
look at it. This issue has evolved beyond just Trump. It's now a political
position and it's one that is far more dangerous than I think most people are
giving it credit for. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}