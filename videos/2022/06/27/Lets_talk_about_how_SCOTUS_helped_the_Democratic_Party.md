---
title: Let's talk about how SCOTUS helped the Democratic Party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Tz_GifQJ3No) |
| Published | 2022/06/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent Supreme Court decision favors Democratic Party long term, unrelated to fundraising.
- Republican Party votes against American people to blame Democratic Party when in power.
- Democrats allow Republican policies to show themselves rather than sabotage.
- Decision reverts power back to states, benefiting Democratic Party long term but harming Americans in Republican-dominated states.
- More babies due to bans lead to decreased tax revenue and increased need for social safety nets and schools.
- Increase in poverty, income inequality, and crime predicted in Republican-dominated states.
- Brain drain expected as innovators leave states with old ideas.
- Republican-dominated states may struggle with economic stability compared to Democratic-dominated states.
- Long-term effects will be pronounced and visible.
- Democratic Party benefits as Republican Party alienates own voters.

### Quotes

- "The Democratic Party ends up punishing Republican voters and turning them against the Republican Party."
- "Long term, as far as the institution of the Democratic Party, this is good."

### Oneliner

Recent Supreme Court decision benefits Democratic Party long term by allowing Republican policies to harm Americans in their own states, leading to visible economic differences and potential voter shift.

### Audience

Political analysts, Democratic Party members

### On-the-ground actions from transcript

- Monitor the effects of the Supreme Court decision on states with bans (implied)
- Stay informed about economic stability and viability in Republican-dominated states (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how a recent Supreme Court decision may impact the long-term political landscape, specifically affecting Republican-dominated states and the Democratic Party's position.

### Tags

#SupremeCourt #DemocraticParty #RepublicanParty #PoliticalAnalysis #EconomicImpact


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about how a recent Supreme
Court decision is really, really good for the Democratic
Party long term, and it has nothing to do with fundraising.
I mentioned this in a recent video, and I definitely
should have expanded on it, because a lot of questions.
Okay, so we know that the Republican Party often votes against the American people, casts
votes that are specifically designed to make it harder for the average American, so they
can then turn around and blame it on the Democratic Party if the Democratic Party is in power.
see this with the price gouging bill when it came to gas, the baby formula thing, right?
So they vote against the solution and then they blame the Democratic Party for it, right?
The Democratic Party doesn't really do this.
They don't go out of their way to trip up the Republican Party at the expense of the
American people.
They tend to just allow Republican policies to show themselves.
And that's what's going to happen here.
In a lot of ways, long term, the Democratic Party is going to come out way on top because
of this.
But it happens at the expense of Americans living in Republican-dominated states.
This decision reverted the power back to the states.
The states that are banning it are Republican-dominated.
What happens?
are focusing rightfully on the harm these bans are going to cause. But step
back for a second. Pretend that isn't your concern. Pretend you
don't actually care about people as individuals for a moment. What happens? So
there's more babies, right? Well, going along with more babies means there's
more mothers who aren't in the workforce, right? So there's a decrease in tax
revenue, but there's more mothers who aren't working. So there's going to be
more of a need for social safety nets. There are also more kids, which means
there's more of a need for school. However, as previously covered, there's
going to be less tax revenue. So what's the only option? Cut services. So what
happens? You have an increase in poverty, an increase in income inequality, which
leads to an increase in crime. That's just one aspect. When it comes to people
People who are more forward-thinking, people who are innovators, do you think they want
to live in a state that is dominated by a bunch of old failed ideas?
No, they're hopping the first bus out of town, right?
Going somewhere else.
So the state then suffers brain drain.
Now if you're a business that is looking to set up a new facility in an age that is
very technologically driven, do you want to go to a place where there's no innovators
and the education system is strained and underfunded?
Probably not, right?
This is going to take about 10 years to show itself, but this will happen.
It's not a maybe.
This is going to occur and it will be incredibly pronounced.
You will see a big difference when it comes to the economic stability and the viability
of Republican-dominated states versus Democratic-dominated states, right?
It's like that healthcare thing that we talked about.
If you live in an area that is primarily democratic, you live longer.
There's going to be a whole lot of stuff like that.
The difference is this is going to be super visible.
So long term, as far as the institution of the Democratic Party, this is good.
This is good news for them.
the Republican Party ends up punishing Republican voters and turning them
against the Republican Party. And the whole time the Democratic Party gets to
sit there and say, no this is a horrible idea. You'll see, I don't know, probably two
three years for the first bit to really start happening. Where you're going to
start to see the strain on social safety nets. It'll be, you know, six, seven years
before you really start to see the strain on schools. And then it's just, from
there, it just happens really quick. This will happen. It's not a maybe. So I don't
think that the Democratic Party will play softball on this simply because of
this. But for the more cynical in the Democratic Party establishment, oh believe
me they've already ran these numbers and it will factor into their decision-making.
making.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}