---
title: Let's talk about scientists trying to get your attention....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FX29FR-1m3s) |
| Published | 2022/06/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Union of Concerned Scientists coined the term "danger season" for the period from May to October, marked by extreme heat, hurricanes, wildfires, and drought.
- This term aims to draw attention to the challenges faced during the summer, largely due to climate change.
- There's a growing concern that people, especially policymakers, aren't taking the situation seriously enough.
- Scientists are resorting to attention-grabbing methods like "danger season" to make people, particularly in the U.S., pay attention to climate-related issues.
- The use of gimmicks like this indicates a sad reliance on marketing tactics to communicate critical scientific data.
- Scientists face pushback from politicians who prioritize business interests over climate change warnings.
- "Danger season" headlines not only serve as a warning for the upcoming months but also criticize policymakers' lack of action towards necessary changes.
- Policymakers are failing to mobilize the significant changes needed to combat the escalating climate crisis.
- Scientists are forced to use attention-grabbing strategies due to the resistance they face when presenting straightforward data and scenarios.
- This situation underscores the need for urgent action and a massive shift to address and mitigate the impacts of climate change.

### Quotes

- "Danger season."
- "They're trying to get the American people, in particular, to pay attention."
- "Elements of the scientific community are trying to get attention and draw attention to world-changing events through methods like this."
- "It's a sad commentary that elements of the scientific community are trying to get attention and draw attention to world-changing events through methods like this."
- "When you see danger season in headlines, that's what it's referencing."

### Oneliner

The Union of Concerned Scientists coins "danger season" to draw attention to the critical impact of climate change, criticizing policymakers' inaction.

### Audience

Climate advocates, concerned citizens

### On-the-ground actions from transcript

- Spread awareness about climate change impacts and the concept of "danger season" (suggested)
- Advocate for policy changes that prioritize environmental preservation (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the challenges faced during the summer due to climate change and the struggle scientists encounter in effectively communicating these issues.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the end of summer.
Not really, but kind of.
The Union of Concerned Scientists has come up
with a new term for the period from May to October,
which is currently highlighted by extreme heat, hurricanes,
wildfires, drought.
They're calling it danger season.
Danger season.
Insert danger zone jokes here.
The idea is to draw attention to the fact
that during the summer, people in the United States
will be faced with more challenges, most of which
are a direct result of climate change.
And this is an attempt to draw attention to the situation
the world is in, as if running out of water and the drought
and the hurricanes and the heat, wildfires,
wouldn't do that enough.
But there is a growing feeling that people are not
taking the situation as seriously as perhaps they
should.
More importantly, that policymakers aren't.
So this is, it's a gimmick to be honest.
It's a gimmick that scientists are trying
to use to get people's attention.
That they are screaming, just look up.
They're trying to get the American people,
in particular, to pay attention.
And this is a method that they've chosen.
It is a sad commentary that elements
of the scientific community are trying to get attention
and draw attention to world-changing events
through methods like this.
Because they can't simply just come out and say,
hey, this is what's going on.
Because politicians will push back against it in an effort
to preserve business interests.
So rather than just present the data
and present the scenarios that are likely
and how things might play out, they're
having to resort to marketing.
And that's what it is.
So when you see danger season in headlines,
that's what it's referencing.
Not just is it a warning saying, hey,
we're moving into this period for May to October.
It's going to be a constant commentary on the lack of will
that policymakers have to trigger that mobilization
and that massive amount of change
that we're going to need to mitigate and make it
through this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}