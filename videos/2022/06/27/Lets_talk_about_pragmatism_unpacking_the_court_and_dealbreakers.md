---
title: Let's talk about pragmatism, unpacking the court, and dealbreakers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RhCd_8EuH44) |
| Published | 2022/06/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the pragmatic aspects of unpacking the court and adding justices.
- Democratic Party's current ability to expand the court is limited due to Senate control.
- Energized Democratic base heading into midterms due to unintentional Republican actions.
- Possibility of Democratic Party gaining control in the midterms and subsequently expanding the court.
- Comparing Democratic Party's potential dilemma to that of Trump supporters and racism.
- Beau views civility politics, unwillingness to restore rights, and inherent misogyny as deal-breakers.
- Emphasizing the importance of including more people under the blanket of freedom.
- Tradition and high ideals mean nothing if they result in stripping away people's rights.
- Democratic Party may face criticism for not acting post-midterms if they gain the necessary Senate seats.
- Beau stresses the importance of acting in alignment with being the land of the free and home of the brave.

### Quotes

- "The Democratic Party is about to have that same kind of situation if they pick up seats in that Senate."
- "If the Democratic Party gains the seats in the Senate it needs and then doesn't act, there's an issue."
- "The tradition is worthless as people's rights get stripped away."
- "That's the tradition that matters, more so than some rule in the Senate."
- "Those who lost their rights, they don't have the luxury of caring about your traditions."

### Oneliner

Exploring the pragmatic aspects of expanding the court and the Democratic Party's potential dilemma post-midterms on restoring rights and traditions.

### Audience

Democrats, Political Activists

### On-the-ground actions from transcript

- Support Democratic candidates in the upcoming midterms to potentially shift Senate control (implied).
- Advocate for the expansion of the court to restore rights and uphold traditions (implied).

### Whats missing in summary

The full transcript delves deeper into the implications of expanding the court and the pressing need for action to protect people's rights and traditions.

### Tags

#CourtExpansion #DemocraticParty #MidtermElections #RightsRestoration #PoliticalActivism


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the pragmatic aspects
when it comes to the idea of unpacking the court,
adding justices, expanding the court,
whatever term you want to use for it.
Because there are a whole lot of people
who are now definitely looking at that as the route to go.
And I got to be honest, it's the only route I see.
It's not the one that I choose.
It's literally the only one that seems really viable to me.
But then you are faced with the pragmatic aspects of it.
Does the Democratic Party have the ability to do that right now?
They don't. They don't.
I know somebody's going to say,
they control the House and the Senate.
No, they really don't.
They don't control the Senate.
They control it in name, on paper,
but they really need two or three more seats
to really have control.
So we're heading into the midterms,
and the Republican Party has unintentionally,
I'm sure, energized the Democratic base.
If the Democratic Party walks out of the midterms,
holding on to the House,
picking up just a few seats in the Senate,
well, then pragmatically, yeah, they can do it.
But then we have to wonder if they will, right?
Because there's that question.
Because in a lot of ways,
it is beneficial for the Democratic Party,
for the Republican Party to have done this.
It helps them a lot.
What is going to happen is that those people
who are Democratic Party members,
those who support the Democratic Party,
those who would say, vote blue no matter who,
those who don't even like the Democratic Party
but view them as harm reduction,
those who reluctantly caucus with the Democratic Party,
everybody is going to find themselves in a situation
that Trump supporters found themselves in.
Not every Trump supporter was a racist,
but every Trump supporter decided that racism
wasn't a deal-breaker for them.
The Democratic Party is about to have
that same kind of situation
if they pick up seats in that Senate.
For me, civility politics,
overriding the rights of half the country,
an unwillingness to use every possible avenue
to restore those rights,
and the inherent misogyny that exists in this decision,
those are deal-breakers for me.
Those are deal-breakers for me.
If the Democratic Party gains the seats in the Senate it needs
and then doesn't act, there's an issue,
and it goes far beyond normal partisan politics.
And that's the way we have to look at it,
because this isn't politics as usual.
You are talking about the rights of half the country.
Right now, there are a few prominent Democrats
who are saying,
well, we really don't want to expand the court.
And I get it.
That's the smart move to say that politically right now,
prior to the midterms.
However, if you mean that, that's a deal-breaker.
The tradition is worthless as people's rights
get stripped away.
It means nothing.
The filibuster means nothing.
All of those high ideals mean absolutely nothing,
because the highest ideal is that we keep including
more and more people under that blanket of freedom,
under all of those promises that were made
in those founding documents.
Those people who didn't get included
when the documents were made,
well, later on they got at it.
Taking them out,
removing them from that blanket of freedom,
from those protections,
and leaving them out in the cold?
No.
That is, that's the ideal that you have to protect.
That's the tradition that matters,
more so than some rule in the Senate.
If you want to be the land of the free
and the home of the brave,
the beacon of hope and progress and all of that,
you have to act like it.
Now, there are some people who are going to be
calling out the Democratic Party for not doing it now.
Pragmatically, they can't.
They don't have the numbers.
But there's a decent chance that they get the numbers
during the midterms.
And if they don't act, well, that's a deal breaker.
I get it.
At the moment, the right thing to say politically is,
oh, we would never do that.
I understand.
But if you mean that,
you're alienating a whole lot of people.
Those who lost their rights,
they don't have the luxury of caring about your traditions.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}