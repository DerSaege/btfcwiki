---
title: Let's talk about the definition of boyfriend....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2uYTxcEYDTg) |
| Published | 2022/06/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Democratic and Republican Parties in the Senate have reached a compromise on key points regarding gun legislation.
- One critical piece of legislation to focus on is the "boyfriend loophole."
- The definition of a dating partner is a key aspect that will impact the effectiveness of the legislation.
- Narrowing the scope of the definition, such as adding "serious" before dating partner, could render the legislation ineffective.
- The goal should be to have a broad definition that includes situations where individuals exhibit concerning behavior towards their partners.
- Craft legislation that prevents those who cannot control their behavior towards their partners from owning guns.
- The effectiveness of the legislation hinges on the inclusivity and wide scope of the definition of a dating partner.
- Failure to create an encompassing definition will result in lives being put at risk.
- This legislation has the potential to save numerous lives and should not be underestimated.
- The importance of this legislation surpasses that of an assault weapons ban.

### Quotes

- "You want to create a situation where if Jimmy and Susie go out on one date and Susie's like, hey, you know, I'm just not that into you. And Jimmy's like, cool, no problem. And then three months later, Susie's out on a date with somebody else and Jimmy has a few in him and he gets mad and he goes up, starts yelling and screaming and hits her once. He never gets to touch a gun again. That's the point you're trying to get to."
- "This is your chance to draft real legislation that will save lives, and lots of them."

### Oneliner

The Democratic and Republican compromise on gun legislation must focus on a broad definition of dating partners to prevent tragedies.

### Audience

Legislators, activists, advocates

### On-the-ground actions from transcript

- Advocate for broad definitions in legislation to prevent loopholes (implied)
- Raise awareness about the importance of inclusive definitions in gun legislation (implied)

### Whats missing in summary

The nuances and detailed examples Beau provides in the full transcript are missing in this summary.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about legislation
and definitions and boyfriends
and what the definition of a boyfriend is
because it's about to matter.
If you don't know, the Democratic Party
and the Republican Party in the Senate,
they have worked out a compromise.
They have worked out key points that they are willing to talk about
when it comes to legislation, when it comes to gun legislation.
Now, there's a whole bunch of different items.
It's similar to the House bill in the sense
that it's just kind of like all over the place,
and that's all fine and good.
There's one piece that I want to focus on.
It's the boyfriend loophole.
At least for the moment,
it appears that an agreement has been reached
to close the boyfriend loophole.
The definition of dating partner is going to matter a lot.
Over the last couple of weeks, we've talked about how
definitions in legislation can mean the difference between
it mattering and it being completely ineffective.
You know, we talked about the New York bill.
We talked about the House gun control bill, right?
And it was definitions in those pieces of legislation
that kind of render them pointless.
The same thing could happen here.
And I have real concern because all of a sudden,
we're seeing the word serious show up.
Serious dating partner.
What that tells me is that they're already trying to narrow the scope.
That's not the point.
The point of this is for it to be as broad as possible.
You want to create a situation where if Jimmy and Susie go out on one date
and Susie's like, hey, you know, I'm just not that into you.
And Jimmy's like, cool, no problem.
And then three months later, Susie's out on a date with somebody else
and Jimmy has a few in him and he gets mad
and he goes up, starts yelling and screaming and hits her once.
He never gets to touch a gun again.
That's the point you're trying to get to.
That's what you're trying to achieve.
Narrowing the scope of that definition is failure.
Every instance that you narrow it just a little bit,
you narrow that definition, you try to make it a little bit more narrow in scope.
That means you're leaving people outside that are going to die.
You have the chance to craft the most effective,
important, life-saving gun legislation in decades.
Don't mess it up.
You can't fail here.
This will be far more important.
It will save far more lives than that assault weapons ban ever did.
And it all hinges on that definition.
Your goal should be to create a situation where those people
who cannot control their behavior when it comes to those closest to them can't own.
That's the goal.
No exemptions to this.
It doesn't matter what their profession is.
It doesn't matter the length of dating.
None of this matters.
You've got to create a definition that matters here,
that counts, that is very inclusive, that is very wide.
If you don't, you're setting people up to be left outside of it,
and it will cost lives.
This is your chance to draft real legislation that will save lives,
and lots of them.
Don't fail here.
This part is far more important than anything else in that bill.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}