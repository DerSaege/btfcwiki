---
title: Let's talk about Day 2 of the hearings will attempt to show....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eC7pcMjY9_Q) |
| Published | 2022/06/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of the hearings and what to expect from them.
- The hearings seem to follow a template of telling them what you're going to tell them, telling them, and then telling them what you've told them.
- Day one focused on setting the stage for the hearings, while day two seems to be delving into the phase of presenting information.
- The objective appears to be proving that Trump knowingly made false claims about the election.
- Trump is believed to have been aware that his claims were untrue, despite being informed otherwise by many.
- Former Trump campaign manager and a former Fox News political director are among those who will testify, along with other key figures.
- BJ Pock, a former US attorney, might have surprises to share during the hearings.
- The intent seems to be establishing that Trump was lying when he made election claims, linking his statements to subsequent actions.
- The hearings are anticipated to be lengthy, requiring viewers to keep track of various details and pieces of information.
- Viewers are encouraged to watch the hearings to determine if the objectives are met.

### Quotes

- "Trump knew that all of his claims about the election were false."
- "He knew he was lying is their position."
- "There are going to be some things that the American people really don't know."
- "BJ has some surprises for us."
- "Trump knew he was lying when he made these claims."

### Oneliner

Beau outlines what to expect from the hearings, aiming to prove that Trump knowingly lied about the election, with key witnesses providing insight.

### Audience

Political observers

### On-the-ground actions from transcript

- Watch the hearings closely to understand the presented information and draw your conclusions (implied).

### Whats missing in summary

Insights on the potential impact of the hearings and the importance of public engagement with the proceedings.

### Tags

#Hearings #Trump #ElectionClaims #BJPock #PoliticalObservers


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about what to expect
from the hearings today.
So far, it looks like they're following the template.
Tell them what you're going to tell them.
Tell them.
Tell them what you've told them.
Day one was definitely, tell them
what you're going to tell them.
Day two looks like the start of that second part, the tell them
phase, right?
OK.
So what it looks like they're laying out
is that Trump knew that all of his claims about the election
were false.
He knew they weren't true when he was making them.
That appears to be what they're going to try to demonstrate.
That he had been told, that everybody told him this.
He knew he was lying is their position.
And that's what they're going to try to show.
You will hear from Trump's former campaign manager,
a former Fox News political director.
If I'm not mistaken, it's the one
who got canned after correctly calling the election, which
I guess that's something you don't want on a news outlet.
Anyway, you'll hear from an election attorney who is
probably just going to provide context.
You will hear from a former US attorney, BJ Pock,
US attorney for the Northern District of Georgia, I think.
Might be middle.
The interesting thing is that Pock resigned,
I want to say the day after, maybe two days after,
that phone call where Trump was calling down to Georgia
wanting to find the votes.
I've said that I think there are going to be some things
that the American people really don't know,
most Americans aren't aware of, that get brought up
in these hearings.
If I had to guess, I have a feeling
BJ has some surprises for us.
And that's just a hunch.
But if I was going to be looking for new information,
I'd be paying attention there.
And then a former Philadelphia city commissioner.
Honestly, I have no idea why.
That one I'm not sure about.
But it looks like they're going to try to establish
that Trump knew he was lying when he made these claims.
And then through the rest of the hearings,
they'll be able to point back to that
and say he said this, which caused this action,
and he knew this to be untrue.
That seems to be their route.
Now, keep in mind, these hearings
are going to go on a while.
So you're going to have to kind of keep
a lot of these little bits and pieces in mind.
It seems to be what they're going
to try to demonstrate today.
I can't say that for sure.
But that looks like where it's headed.
Now, whether or not they're able to demonstrate that,
that's kind of up to you.
You've got to watch and find out.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}