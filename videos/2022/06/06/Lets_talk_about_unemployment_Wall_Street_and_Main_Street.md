---
title: Let's talk about unemployment, Wall Street, and Main Street....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=quG_sg3Rd-0) |
| Published | 2022/06/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the economy, unemployment, inflation, and concerns of big business.
- Despite global inflation issues, the United States economy is doing well.
- Big business is worried that the economy might be doing too well because of low unemployment rates.
- Employers have to compete for workers when unemployment is low, leading to higher wages.
- Big business is concerned that paying employees more will lead to inflation.
- Beau criticizes big businesses for not paying their employees more despite posting record profits.
- The media overlooks the fact that what benefits Wall Street might not be best for Main Street.
- Big business is worried about having to pay a living wage and raise their overhead costs.
- The system in the US operates on a carrot-and-stick approach for social mobility.
- Big business wants more people without jobs to avoid paying higher wages.

### Quotes

- "What's best for Wall Street may not be what's best for Main Street."
- "If your company is posting record profits, paying your employees more is just the thing you're supposed to do."

### Oneliner

Big business is worried about low unemployment leading to higher wages, illustrating the disconnect between Wall Street and Main Street.

### Audience

Workers, general public

### On-the-ground actions from transcript

- Advocate for fair wages for all workers (suggested)
- Support policies that prioritize workers over corporate profits (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of low unemployment on big businesses and the disconnect between corporate interests and worker well-being.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk a little bit
about the economy and unemployment and inflation
and the concerns of big business.
And it should prove pretty interesting to us common folk
because it illustrates something at the core
of the American system and something
that news outlets tend to overlook when they're providing
us with information.
OK, so despite the global inflation issues,
the United States economy is doing pretty well.
It's doing pretty well.
In fact, big business is starting
to worry that it might be doing too well.
The numbers of unemployed are at like a half century low.
People are at work.
People have jobs.
And that's actually the problem for big business.
Their concern is that if there isn't a high enough
rate of unemployment, well, that means everybody has a job.
That means that employers have to compete
to get you to work for them.
What's the main way they compete?
Well, they have to pay you more.
And that's bad for them.
The talking point that has gone out
is that if unemployment continues to drop
and big business has to pay people more, well,
that's just going to make inflation worse.
Because if we have to pay people more, well,
then we're going to charge more.
We have to.
We don't have a choice.
Yeah, you do when you're posting record profits.
You absolutely have a choice.
That's entirely on you.
If your company is posting record profits, as many are,
paying your employees more is just the thing
you're supposed to do.
It's not something that causes you to raise prices.
But what this illustrates very clearly,
and this is something that is very lacking in media coverage
about the economy, is the simple fact that what's
best for Wall Street may not be what's best for Main Street.
It may not be what's best for you.
Right now, big business is concerned
that there aren't enough people without jobs.
That's their concern, because it's
likely to raise their overhead.
They might have to pay people a living wage.
And that is kind of the basis of the system in the United States.
It's that carrot and the stick.
If you play within the system well enough, well, hey,
you might be that one in a million
that gets some social mobility.
If you don't, well, here's the stick.
And it's used to move you and your family out into the street.
Right now, the concern among many in big business,
many who are talking about the economy,
is that not enough people are getting the stick.
Not enough people don't have jobs.
That's their worry.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}