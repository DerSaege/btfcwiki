---
title: Let's talk about what one conservative institution can teach another....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Re-WnUYY1WA) |
| Published | 2022/06/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A poll of Catholics reveals surprising departures from church leadership on key issues like Roe v. Wade and LGBTQ+ communion.
- 68% of Catholics want Roe v. Wade upheld, contrasting with the US Conference of Catholic Bishops' call to pray for its overturn.
- 77% believe LGBTQ+ individuals should receive communion, and 65% support openly gay priests.
- Despite these views, 68% of Catholics attend services less than once a month, with decreasing church attendance and worsening opinions.
- Beau suggests that conservative institutions, like the Republican Party, are losing touch by promoting extreme positions to energize a shrinking base.
- The strategy of scapegoating certain demographics for political gain may alienate the majority and lead to decreased engagement.
- Older individuals, who tend to support socially regressive positions, may age out in a few years, affecting the party dynamics.
- Beau predicts a potential split within the Republican Party as normal conservatives may seek alternatives rather than joining the Democrats.
- Most Republicans view Democrats as left-wing, but understanding their similarities to past Republicans could shift perceptions.
- Beau warns that in five years, the implications of current party strategies will become significant, leading to potential shifts within the Republican Party.

### Quotes

- "The longer this goes on, the less likely the majority of people are to be involved with that institution."
- "Scapegoating people, it energizes an ever-shrinking minority of people."
- "You're going to start to see the same type of thing play out within the Republican Party."

### Oneliner

A warning to conservative institutions: alienating the majority through extreme positions risks losing engagement and could lead to a party split.

### Audience

Conservative voters

### On-the-ground actions from transcript

- Re-evaluate messaging and policies to avoid alienating the majority (implied)
- Encourage open dialogues within conservative institutions to address shifting views (implied)

### Whats missing in summary

The full transcript provides deeper insights into the potential consequences of alienating the majority through extreme positions, especially within conservative institutions.

### Tags

#ConservativeInstitutions #RepublicanParty #CatholicChurch #PoliticalStrategy #PartySplit


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about what one
conservative institution in the United States can teach another conservative
institution in the United States and how that lesson is probably something that
neither institution wants to admit needs to be taught and why this actually
spells good news for everybody else. Okay so there was a poll conducted of
Catholics and it focused mainly on asking Catholics their position in
relationship to the stated position of church leadership and the results were
really surprising and then the outcome got really interesting. Okay so the US
Conference of Catholic Bishops told Catholics to pray for the Supreme Court
to overturn Roe versus Wade. 68% of Catholics want Roe versus Wade upheld.
That's huge. When it comes to receiving communion, 77% of Catholics think that
LGBTQ people should be able to receive communion. These are huge departures.
65% believe it's time for openly gay priests. There are a bunch of other
things like this and they all pretty much run right in this range, 66 to 75%
in that area. But the part that really matters and the part that
includes the lesson is that today 68% of Catholics attend services less than once
per month and that 37% say they attend church less than they used to and that
26% say that they have a worsening opinion of the Catholic Church. So what
does this say for other conservative institutions? The current playbook right
now is to come up with more and more extreme positions in an attempt to
motivate and energize the base. The problem is what this is telling us is
that that base is ever-shrinking. So this ends up being a self-defeating strategy.
Sure, you have a very loud energized base when it comes to these incredibly
socially regressive ideas, but it's tiny. This strategy that the Republican Party
is using in an attempt to build buzz, sure, the Republican Party will probably
tolerate it for a little while. Normal Republicans, you know, people that are
generally just actually conservative, not far-right. Anyway, so what this shows is
that the longer this goes on, the less likely the majority of people are to be
involved with that institution. I'm going to suggest that somebody's relationship
to their church might be a little bit stronger than their relationship to a
political party. The worsening opinion, the less attendance, it signals less
engagement. 68% of people attend church, of Catholics attend church less than
once a month. These numbers should be a wake-up call to all conservative
institutions. Scapegoating people, it energizes an ever-shrinking minority of
people. It's also worth noting that when it comes to stuff like this, although in
this poll I didn't see any breakdown of it, the people who generally stay
associated with organizations that scapegoat more and more and adopt more
and more socially regressive positions tend to be older, in most cases much
older, like the kind that are going to age out in a few years. This surge of
trying to scapegoat different demographics for the sake of political
gain, it might burn itself out through alienating the majority of people and
through the bulk of the highly energized, well, just no longer being here. This is a
lesson that the Republican Party needs to pay attention to because what very
well might happen is a split within the Republican Party because most normal
conservatives, they're not going to go and join the Democratic Party.
They'll go somewhere else because regardless of their opinion on the
socially regressive ideas, most Republicans in the United States
believe that Democrats are truly left-wing and they don't want to be
associated with that. If they understood that most Democrats today are like Bush
era Democrats, or Bush era Republicans, they might have a differing opinion of
that, but that's not where we're at. Five years, this is going to matter. You're going to
start to see the same type of thing play out within the Republican Party. Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}