---
title: Let's talk about recent Biden Foreign Policy moves....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gIlszGbZPvc) |
| Published | 2022/06/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines Biden's foreign policy in the context of international relations.
- Mentions three separate foreign policy events: NATO exercise, North Korea missile launch, and Summit of Americas.
- NATO exercise involves 7,000 troops, 45 ships, 75 planes, and multiple countries.
- Views these events as demonstrating resolve or showing lines to adversarial nations.
- Questions Biden administration's decision to exclude Cuba, Venezuela, and Nicaragua from the Summit of Americas.
- Suggests that engaging in diplomatic talks with non-adversarial nations is more beneficial.
- Points out the importance of including all nations in dialogues, even if not allies.
- Criticizes the administration's approach in trying to isolate certain countries.
- Expresses doubt in the effectiveness of the administration's strategy in the international poker game.
- Advocates for open communication and engagement over isolation in foreign policy decisions.

### Quotes

- "Sometimes it is best to show your hand a little bit, take a hard line, call."
- "Engage in a show of force, take a hard line, call."
- "It's a friendly game right now. Everybody have a seat."

### Oneliner

Beau examines Biden's foreign policy moves in a high-stakes international poker game, advocating for open communication over isolation.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Engage in diplomatic talks with non-adversarial nations (suggested).
- Advocate for inclusive dialogues in international relations (implied).

### Whats missing in summary

Deeper insights into the nuances and implications of current foreign policy decisions.

### Tags

#ForeignPolicy #BidenAdministration #Diplomacy #InternationalRelations #Inclusivity


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk a little bit about Biden's foreign policy and what
he's doing at that international poker game where everybody's cheating.
And we're going to talk about knowing when to hold them and knowing when to fold them.
There are three foreign policy events that are occurring right now that
are totally unrelated, but they're also kind of related in
stance. And maybe they shouldn't be. So the first thing we have is ball tops. It's a NATO exercise.
It is training maritime forces. You're talking about 7,000 troops, 45 ships, 75 planes,
14 NATO countries, plus Sweden and Finland. Yeah, believe me, Russia is looking at that really hard
right now and this is definitely something that is demonstrating NATO's
resolve when it comes to the security assurances it has provided. We're
already participating in these exercises and this isn't a new thing but the the
fact that this exercise wasn't put off is it's not a provocation it's not an
escalation, but it's definitely an adversarial move. It's kind of like, yeah, watch this.
Which, when you're talking about a militarily adversarial nation like Russia, makes sense.
A hard-line stance? I get it. Then you have North Korea. North Korea fired off eight missiles out
into the ocean again. The United States and South Korea, they fired 8-2. They
fired 8 off into the waters. Again, not an escalation, not a
provocation, but definitely adversarial, showing a line, you know, this is
we're calling type of thing. Again, militarily adversarial nation makes
sense. Then you have the Summit of Americas and the Biden administration is
creating a lot of tension around its reluctance to have three countries
present Cuba, Venezuela, and Nicaragua. These aren't militarily adversarial
nations. I'm not sure that a hardline approach here is the best move. It's
creating tension where there doesn't really need to be any. When you are
talking about establishing dialogue and you want to have something called the
Summit of Americas. You have to have everybody represented. Just because
somebody's represented, a nation is represented at this table, that doesn't
mean you're allies with them. It doesn't mean you're going to be launching
missiles into the ocean with each other or conducting joint exercises. It just
means you're sitting down at a table. You're doing that anyway in that
metaphorical poker game. Not allowing them to come is also sitting
down at the table with them. It's a diplomatic move. So I'm not sure that's
the right move. I have to be honest. The United States is trying to undo a lot
of damage on the foreign policy scene. Talking to anybody who isn't militarily
adversarial is probably the right move. I don't think you're gonna find a lot of
exceptions to that because competitor nations, yeah they're willing to talk to
anybody. And the main focus with this is the Biden administration wants to push
that democracy versus authoritarianism framing for the new near-peer contest
Cold War II, whatever you want to call it. And that's part of the reasoning, but
it doesn't really line up with reality. I mean there there are other nations
that if we're talking about authoritarianism and that being the
disqualifying factor, there are other nations that maybe shouldn't be coming.
These are just countries that are historically adversarial or have a lot
of political baggage associated with them in domestic politics that doesn't
have anything to do with that international poker game. I have been
pretty impressed with Biden's foreign policy team. This may be one of the first
things that I'm just kind of like, why are you doing this? It's conversation. You can
talk to anybody. And if you're going to isolate them, then best case scenario, let's say the
summit goes great, and every nation signs on and it's like, hey, yeah, we're going to
be the the pro-democracy front, what about these three countries? You are
granting and just giving a foothold to the opposition in that near-peer
contest. I don't think that was a great move. Sometimes it is best to show your
hand a little bit, engage in a show of force, take a hard line, call.
And sometimes it's best to just kind of, well, it's a friendly game right now.
Everybody have a seat.
Anyway, it's just a thought.
y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}