---
title: Let's talk about bulls, bears, and the market....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KP-IMosSktU) |
| Published | 2022/06/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concepts of bulls and bears in the market, particularly focusing on the recent bear market on Wall Street.
- Describes a bear market as when the S&P 500 drops more than 20% from its peak.
- Shares historical data on how long bear markets typically last, around 13 months heading down and 27 months to recover.
- Advises the average person not to panic about a bear market leading to a recession, as it's about a little more than a 50% chance.
- Points out that those heavily reliant on their 401k, like boomers, may need to be more conservative with their spending during a bear market.
- Emphasizes that Wall Street indicators are not always reflective of the real economy, especially due to factors like low unemployment rates and speculation on borrowing and spending.
- Acknowledges the likelihood of an upcoming economic downturn but underlines the uncertainty of its depth and duration.
- Notes that despite some economic concerns, other indicators have been positive, contrary to what news coverage may suggest.
- Encourages patience and observation regarding the evolving economic situation.

### Quotes

- "Stocks are going up. A bear lays down and hibernates. They're going down."
- "Wall Street isn't Main Street."
- "People aren't going to be able to borrow as much, therefore they'll spend less, therefore the economy will go down, and they're kind of speculating on that."

### Oneliner

Beau explains bulls, bears, and a potential economic downturn, advising against panicking and differentiating Wall Street from Main Street amid market uncertainties.

### Audience

Investors, Savers, Workers

### On-the-ground actions from transcript

- Monitor your investments regularly for any necessary adjustments (implied)
- Seek financial advice if needed to navigate market uncertainties (implied)
- Stay informed about economic trends and indicators affecting your financial well-being (implied)

### Whats missing in summary

Detailed explanations on historical bear market durations and economic indicators' impacts.

### Tags

#MarketInsights #EconomicDownturn #Investing #FinancialAdvice #WallStreet


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about bulls and bears
and markets and what it means for you.
Oh my.
Okay, so first to start off with, I'm not an economist.
This isn't really my wheelhouse, but this seems like it's worth mentioning.
Okay, so Wall Street's in a bear market.
The S&P 500 has hit a bear market.
What does that mean?
We have heard these terms a lot, but what is it?
Easiest way to think about it is a bull charges ahead.
Stocks are going up.
A bear lays down and hibernates.
They're going down.
It's a little bit more complicated than that.
It means more than 20% off the previous high or low.
Okay, and that's just happened.
It's 21% off of its peak, which was in January.
How long do bear markets last?
There's a whole bunch of different ways that people define this stuff,
and the best answer to that I have is that it looks like they last about 13 months
headed down and then 27 months to get back to where they were.
I want to say that was based off of from the end of World War II.
There's another one that goes back to, I want to say 1929,
and I think that one says it takes nine or ten months to go down.
For the average person, do you need to panic?
No.
It's a little bit more than 50-50 as far as does a bear market mean a recession's coming.
A little bit more than 50% of the time, the answer to that question is yes.
All right.
People who should probably be a little bit more conservative with their spending,
in particular, if you're like a boomer, like a literal boomer,
who's relying on your 401k, you're probably going to take a hit.
But as we have talked about many times on this channel, Wall Street isn't Main Street.
A lot of the things that are driving this, they're not the normal indicators.
You know, we're at, I mean, almost record low unemployment.
A lot of this has to do with the stock market looking at interest rates and saying,
hey, people aren't going to be able to borrow as much, therefore they'll spend less,
therefore the economy will go down, and they're kind of speculating on that.
That's what seems to be driving a whole lot of this.
Now, we don't know how shallow or how deep it's going to be, how long it's going to last,
but this does suggest that there is at least a little bit of an economic downturn coming.
And I know for a whole lot of people here, like I thought we were in one,
not as much as you might think by the news coverage.
You know, people have been focused on inflation,
but a lot of the other indicators were actually pretty good.
So we're just going to have to wait and see what happens.
But this seemed like something that, well, it, it bared mentioning.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}