---
title: Let's talk about pardons, votes, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OOGE4ZmveAg) |
| Published | 2022/06/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Committee suggested lawmakers sought pardons from Trump after Capitol events.
- Pardons imply wrongdoing.
- Beau wants comparison between pardon seekers' votes on impeachment.
- Beau suspects some sought pardons then voted against impeachment.
- Committee presenting strong case, likely to show people sought pardons.
- Beau hopes committee questions lawmakers about their votes.
- Sean Hannity suggested Trump pardon Hunter Biden to change the story post-Capitol fallout.
- Political parties often try to change the story when faced with bad news.
- Hannity's suggestion reveals real-life attempts to manipulate media coverage.
- Beau urges people to be mindful of manipulation tactics used by politicians.

### Quotes

- "Pardons imply wrongdoing."
- "Maybe [we should] have in the back of our minds, given the way they're talking about the hearings now."
- "It does kind of imply that you might have done something wrong if you are preemptively seeking a pardon."
- "I'd really like to see that."
- "That does happen."

### Oneliner

Committee suggests lawmakers sought pardons, Beau wants comparison with impeachment votes, and Hannity's suggestion to pardon Hunter reveals real-life media manipulation tactics.

### Audience

Voters, citizens, activists

### On-the-ground actions from transcript

- Question lawmakers about their votes (suggested)
- Be mindful of media manipulation tactics (implied)

### Whats missing in summary

Full insight into the connections between seeking pardons and voting behavior, as well as the impact of media manipulation on public perception.

### Tags

#Pardons #Impeachment #MediaManipulation #PoliticalTactics #Voters


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about pardons and votes.
You know, the committee has suggested that there are lawmakers who sought pardons from
Trump after the events there at the Capitol.
And we talked about it when this rumor first surfaced.
You know, I was like, you want a pardon? For what?
It does kind of imply that you might have done something wrong if you are preemptively
seeking a pardon.
But one of the things that's kind of been rolling around in my head since then is an
overlap that I would like to see.
I would like to see a comparison between those people who sought pardons from the Trump administration,
those lawmakers, and how they voted on the impeachment.
I'd really like to see that.
Because I have a feeling that there's going to be a whole bunch of people who preemptively
sought pardons based on the activities who then voted against impeaching him.
That seems odd.
And you know, so far, the committee has done a pretty good job of presenting its case.
You know, the stuff that has come up that they have, I don't want to say leaked, but
they've mentioned before publicly, they're doing a pretty good job of demonstrating that
it's true.
So based on that, I have to assume that pretty soon they will demonstrate that people did
in fact seek pardons.
And I really do hope that the committee does ask some questions about their votes.
Because I think that would be pretty enlightening.
The other pardon that came up was something that apparently came from Sean Hannity, telling
Trump that he should pardon Hunter.
That's unique.
You know, something that comes up pretty often is the idea that when bad news hits for a
political party, that they try to change the story.
This certainly appears to demonstrate that that's not just something that happens in
movies and TV shows, that that's very much a real thing.
That because of the fallout of what happened on the 6th, Hannity was like, you know what?
Pardon Hunter Biden, that'll change the story.
You know, that's kind of the vibe you get from that.
It's worth noting that it is real, that that does happen.
And when you're trying to decide whether or not the new story outweighs the old, maybe
remember that.
Maybe remember how cynical this group of people is when it comes to the stories they're willing
to feed the American people.
The way they're going to attempt to manipulate media coverage of an event like the 6th.
It seems like something we should all kind of have in the back of our minds, given the
way they're talking about the hearings now.
Or not talking about them.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}