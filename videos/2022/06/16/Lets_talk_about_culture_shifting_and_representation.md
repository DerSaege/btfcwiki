---
title: Let's talk about culture shifting and representation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NbYkwC54_K0) |
| Published | 2022/06/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a message sparked by a Patterson video, focusing on culture rather than workplace competition.
- Expressing uneasiness about the cultural power shifting away from white men towards more women and people of color in pop culture icons.
- Acknowledging the need for change and appreciating those leading the charge for change.
- Emphasizing the importance of representation for future generations in the fight for change.
- Explaining why white men may not be pop culture leaders in the fight for change due to advocating for the status quo.
- Sharing a personal anecdote about encouraging his son and his friend to participate in a march and the importance of being the representation you seek.
- Encouraging individuals to get involved in the fight for change, even if not taking a leadership role.
- Stating that the lack of white men in pop culture leadership roles is due to many choosing to maintain the status quo and not actively participating in the movement for change.

### Quotes

- "Nobody looks more like you than you."
- "You want your kid, your grandkid to have representation of people that look like you in the fight? You be that representation."
- "Just get involved. That's all it takes."

### Oneliner

Beau addresses the shift in cultural power away from white men in pop culture icons, urging individuals to actively participate in the fight for change to ensure representation for future generations.

### Audience

Creators and consumers of pop culture.

### On-the-ground actions from transcript

- Be the representation you seek in the fight for change (exemplified).
- Get involved in marches, protests, or community events advocating for change (exemplified).

### Whats missing in summary

The full transcript delves into the importance of individuals taking action to ensure representation and change in pop culture leadership roles.

### Tags

#PopCulture #Representation #Change #Community #Activism


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about yet another message
sparked by that Patterson video.
Got to be honest, didn't see that being a real big
conversation starter, but here we are.
This one isn't so much about, you know,
the competition in the workplace, but rather culture.
It's a long message, but I'm gonna read the whole thing
because there's kind of a lot in it.
Starts off, thank the person who admitted they were scared
even though they knew it was right,
because being 100% honest,
that thought had crept into my head too.
But there's something that I notice far more
and seems more important for me to understand.
When I look at pop culture leaders,
they don't look like me anymore.
Logically, I know white men are still way overrepresented,
but I just feel the cultural power slipping away.
It's not really scary, but I'm uneasy with it.
Pop culture icons are becoming more often women
and more brown.
I know that sounds racist.
That's not really what I mean.
I like them.
I love that they're leading the charge for change
that I know we need.
I guess I just want my kids and their kids
to at least see some representation in that fight for change.
I don't want them to think they're the bad guy
and you don't need to say,
imagine how they feel without representation
for all those years.
I know, I know this is irrational too.
Just hoping for a similar talk to help with this, I guess.
I mean, there's a lot in this, right?
Okay.
So why aren't white men, the pop culture leaders
that are leading the fight for change?
Because a whole lot of white men
are advocating for the status quo.
They're not leading.
Leading requires movement.
If you don't want change, if you want the status quo,
by definition, you're not really a leader, are you?
And pop culture tends to reflect those that are leading,
those that are progressing in some way.
So by default, because so many white men
have taken the position of we like the status quo,
there's not a lot of them.
I mean, let's just be honest for a second.
I'm willing to bet at least a third
of the people subscribed to this channel,
tuned in the first time because they took one look at me
in that thumbnail and were like,
that dude's about to say some racist stuff.
And they tuned in to hate watch, right?
So that's why you don't see a lot of white men.
But then you have this part
that I found really interesting.
I love that they're leading the charge for change
that I know we need.
I guess I just want my kids and their kids
to at least see some representation
in that fight for change.
I don't want them to think they're the bad guy.
About a year ago, and I could be kind of off
on this time frame,
but I think it was about a year ago.
I'm sitting on the porch and my son
and his friend are talking loudly and I can hear them.
The friend wants to go to some march
and they're worried that their parents won't let them go.
And not because the kid is like not even old enough
to drive and the march was in DC or whatever.
They were worried that their parent
wouldn't approve of them going.
And I asked my son,
cause my son was just like, well, ask them.
The worst they could do is say no.
And the kid's like, yeah, what would your dad do
if you asked him to go to this?
He's like, my dad was in Ferguson.
He'd probably drive us.
What better representation that looks like you
could there be than you?
Nobody looks more like you than you.
You want your kid, your grandkid to have representation
of people that look like you in the fight?
You be that representation.
It's really that simple.
Yeah, you may not be on CNN,
but you show up and then that's all you can do.
That's all it takes.
When you're talking about the cultural power,
that leadership role slipping away,
it's not being taken, it's being abdicated.
It's not that people are stepping up and say, no,
white folks don't lead here.
Don't try to help us achieve change.
It's that they're not stepping up to the level
that we probably should.
But if your real concern here is your kids feeling
bad about themselves because people that looked like them
weren't in the fight, the simple answer
is you get in the fight.
That's it.
That's all you have to do.
And your kids and grandkids will look back at you
the way people look at the Freedom Riders.
Just get involved.
That's all it takes.
And you don't need to take a leadership role.
That's the other part behind this.
You don't need to take a leadership role.
Just back them up.
I hope that helps.
I mean, the reason you're seeing less white men
is because white men, not everybody obviously,
but a whole lot have decided they want the status quo.
Therefore, they're not really in pop culture.
They're in that right-wing ecosystem.
There's representation there, a lot of it,
where you don't want it.
But because they've kind of given up that role,
it's not there.
Nobody's stopping them.
And then as far as representation for your family,
you be that representation.
There is no better person.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}