---
title: Let's talk about Texas Republicans and Orwell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KiAJbEXutbY) |
| Published | 2022/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Convention in Texas questioned 2020 election results without evidence.
- Trump's inner circle admitted disbelief in the claims.
- Texas Republicans reject fundamental truths like math without evidence.
- Orwellian comparison to 1984 novel where truth is dictated by the party.
- Party demands blind faith, rejecting evidence and basic truths.
- Texas Republicans cling to election lies like the Alamo, fearing judgment.
- Politicians won't admit error to maintain voter trust.
- Voters comply to gain party favor, even if they know the truth.
- Blind loyalty to party over truth and facts.
- Self-reinforcing cycle of misinformation and blind faith.

### Quotes

- "How do you really know what year it is?"
- "They will say that two plus two is five."
- "They're going to do what the party says."
- "They will never question their bettors."
- "They reject something as fundamental as math with no evidence."

### Oneliner

Texas Republicans reject 2020 election results, demand blind faith, and Orwellian loyalty to the party over truth.

### Audience

Texas voters

### On-the-ground actions from transcript

- Challenge misinformation within your community (suggested)
- Support politicians who prioritize truth and evidence (implied)

### Whats missing in summary

The emotional intensity and the gravity of choosing loyalty to a party over truth can be best understood by watching the full transcript.

### Tags

#RepublicanConvention #Texas #ElectionResults #Orwellian #BlindLoyalty


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the Republican Convention in Texas again
because the hits just keep on coming.
We will also talk a little bit about understanding literature.
In the latest installment from the Texas Republican Convention, a resolution was advanced
that called into question the results of the 2020 election.
Specifically, it says that they reject the results.
And it says that Biden is an illegitimate president.
They make this claim with no evidence to back up their position.
After all of this time, they make this claim while video surfaces in the committee hearings
of Trump's inner party, his inner circle, admitting that they didn't believe it.
They didn't believe these claims.
They knew they were baseless.
But the Republican Party in Texas demands that their party members reject something
as fundamental as math with no evidence.
Basically on the premise of, well, how do we really know that that's what it adds up to?
It's wild.
It is wild.
But in some ways, it's entertaining as well.
A lot of Republican figures, they are fans of saying that things are Orwellian.
They say this primarily because they don't understand Orwell.
One of the most dystopian features of the novel 1984 is the fact that we don't know
that it's set in 1984 for sure.
It's a guess because the party demanded that the party members reject something as fundamental
as math with no evidence.
The party says it, and therefore that's the position you took because how could you really
know otherwise, right?
How do you really know what year it is?
In the end, the party would announce that 2 and 2 made 5, and you would have to believe it.
It was inevitable that they should make that claim sooner or later.
The logic of their position demanded it.
And what was terrifying was not that they would kill you for thinking otherwise, but
that they might be right.
For after all, how do we know that 2 and 2 make 4?
Or that the force of gravity works, or that the past is unchangeable?
If both the past and the external world exist only in the mind, and if the mind itself is
controllable, well what then?
George Orwell.
That's Orwellian, demanding that Republicans in the state of Texas accept the position
that they should reject the 2020 elections with no evidence.
That is Orwellian.
That is calling into question something as fundamental as math and saying don't believe
their eyes, believe the party.
Put your faith in the party.
It's not like Texas Republicans have a habit of just, well, looking away from problems.
You know, that's why the energy grid is fixed.
That's why there's a climate plan, right?
For many Republicans, the claims about the election, they're the Alamo.
They have to uphold that lie.
They have to continue to say, oh, I'm a true believer.
I believe the party.
Because the moment they let it slip that they were conned, well you're going to call their
judgment into question.
Understand those politicians who were most outspoken about these claims that have no
evidence, they'll be the last ones to admit they were wrong.
They'll be the last ones to admit that they caved to pressure, that they didn't really
know what they were talking about, because if they do that and their voters know that
they got played, why would you elect somebody like that again?
It's self-reinforcing.
Most people, I would imagine, that voted for this, they know that it's not true.
But they want to be inner party members themselves one day.
They're going to do what the party says.
They will say that two plus two is five.
And they will never question their bettors and those people who fundamentally subvert
basic truth.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}