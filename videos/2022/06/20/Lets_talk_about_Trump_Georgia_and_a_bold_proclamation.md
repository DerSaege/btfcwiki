---
title: Let's talk about Trump, Georgia, and a bold proclamation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6WMcjpxtrrs) |
| Published | 2022/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculates on what might be on Donald Trump's mind, particularly focusing on Georgia.
- Mentions Nick Ackerman, a former assistant prosecutor during Watergate, making bold statements about potential legal trouble for Trump.
- Ackerman believes that a three-year felony in Georgia that Trump violated could lead to his imprisonment.
- Points out the significance of tape-recorded evidence that prosecutors love due to its inability to be cross-examined.
- Suggests that the evidence from the January 6th committee combined with tapes leaves Trump defenseless in Georgia.
- Foresees Georgia as a potential prosecution that could result in Trump going to jail.
- Notes the increasing interest of state-level prosecutors in cases related to Trump’s activities.
- Predicts more state-level prosecutions if the Department of Justice (DOJ) does not take action.
- Indicates that state prosecutors may pursue investigations into Trump campaign activities if the DOJ does not intervene.
- Emphasizes the importance of paying attention to cases beyond Washington D.C. and congressional hearings.

### Quotes

- "There is a really neat three-year felony in Georgia that Donald Trump has violated."
- "prosecutors love tape-recorded evidence because you cannot cross-examine it."
- "I think there might be more investigations into the Trump campaign's activities at the state level."
- "It's just a thought, God have a good day."

### Oneliner

Beau speculates on potential legal trouble for Trump in Georgia, pointing to tape-recorded evidence and state-level prosecutions as critical factors in the case.

### Audience

Legal analysts, political watchdogs

### On-the-ground actions from transcript

- Watch for developments in state-level prosecutions related to Trump's activities (implied).
- Stay informed about investigations beyond federal hearings (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of potential legal challenges faced by Donald Trump, focusing on Georgia and state-level prosecutions.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about what Donald Trump
might be thinking about, and while the committee
is heating up, it might be Georgia that's on Trump's mind.
There was a pretty bold proclamation made by Nick Ackerman,
who was a, I'll say, an assistant prosecutor
prosecutor during the whole Watergate thing.
Somebody probably knows a little bit about some tapes, right?
This is what he had to say.
If you are asking which of these cases right now, which one is
going to send Donald Trump to prison, that is the case.
There is a really neat three-year felony in Georgia
that Donald Trump has violated.
prosecutors love tape-recorded evidence because you cannot cross-examine it.
What is significant with those tapes is that when you put it in context of all of the evidence
that the January 6th committee has uncovered, you put that together, Donald Trump has zero
defense in Georgia.
If I had to put my money on one prosecution that's going to go forward here that will
send Donald Trump to jail, it's Georgia.
That is a bold statement right there.
I mean, it's hard to argue with the logic.
I mean, the fact is that as the committee talks more and more about the activities
that are, that occurred at the state level, there are going to be more and
more prosecutors that are at that level who become interested for a number of
reasons. Some may actually have pursuit of justice in mind. Some see a really
easy win that's high-profile. Some will have future political ambitions. I don't
think that the state-level prosecutions... I don't think we've heard the last of
those. I think there will probably be more that pop up along the way. Now this
is assuming DOJ does not move forward. If DOJ moves forward, state prosecutors
may just kind of shrug it all off and say, well the feds have it. If DOJ doesn't
move forward, I think there might be more investigations into the Trump campaigns
activities at the state level and with the evidence that the committee is
putting out publicly, they're doing most of the work. All the prosecutors have to
do is reconstruct that evidence with their own chain and they can move
forward. So it's something to be aware of and watch for because while everybody
is focused on DC and the hearings, it's important to remember there are a lot of
other cases out there. There are a lot of other investigations going on. So anyway
It's just a thought, God have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}