---
title: Let's talk about how Republicans create demand for strict gun laws....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7uCisENQg_o) |
| Published | 2022/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the chain of events and cause and effect with the Republican Party's stance on gun regulation.
- Points out the importance of closing the "boyfriend loophole" in existing laws that prevent domestic abusers from owning firearms.
- Notes the Republican opposition to closing this loophole, allowing abusers to own guns.
- Criticizes the manipulation of individuals by feeding them talking points that contradict their true beliefs.
- Addresses common arguments against closing the loophole, such as false accusations and Second Amendment rights.

### Quotes

- "They've got you conned."
- "You are once again supporting things in opposition of your own interests, in opposition of your stated beliefs."
- "Their property rights over a firearm outweigh your family members' lives."

### Oneliner

Beau explains how the Republican Party creates the demand for strict gun regulation by opposing closing the "boyfriend loophole," manipulating individuals, and prioritizing property rights over lives.

### Audience

American citizens

### On-the-ground actions from transcript

- Advocate for the closure of the "boyfriend loophole" in existing laws to prevent domestic abusers from owning firearms (suggested).
- Challenge misinformation and manipulation by engaging in informed debates and spreading accurate information (implied).

### Whats missing in summary

Understanding the tactics used to manipulate individuals into supporting policies that contradict their beliefs.

### Tags

#GunRegulation #RepublicanParty #DomesticAbuse #Manipulation #SecondAmendment


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about chain of events, cause and effect, how things happen.
And we're going to use something that is in the news right now to kind of highlight this.
And we're going to demonstrate pretty clearly how the Republican Party actually creates
the demand for strict gun regulation.
I'm not joking.
Right now, the two parties are in talks and the idea is to create laser focused legislation
that doesn't run afoul of any of the normal talking points, that would actually do something.
The most important part of this package is closing something called the boyfriend loophole.
What this is, it's a loophole in an already existing law that says domestic abusers, well
they can't own firearms.
Something that nobody in the gun crowd actually complains about.
They're just going to close the loophole in it.
And the Republicans have decided that that's just a bridge too far.
They're willing to go to bat to make sure that abusers can own guns.
They want to make sure that the guy that slaps your daughter can walk out and buy a gun the
next day.
And they're going to feed you the talking points to support it.
Because they have you trained.
I know y'all.
Y'all do not support the idea of abusers owning firearms.
Let's be honest for a second.
If somebody hit my fill in the familial relationship that you want to.
Daughter, ex-girlfriend, cousin, whatever.
What would you do?
We've all heard these conversations, right?
And whatever you're going to say, oh it's far worse than disarming them.
You don't believe this.
They've just got you conned.
They've got you to the point where you will say whatever they tell you to.
You as an individual do not believe that these folks should own guns.
So let's run through some of the talking points here.
It's not just women.
I've told y'all, encouraged y'all to say that.
You know domestic abuse, well that doesn't, it's not just men that do the hitting.
Sometimes women do.
Yeah, yeah I mean that's true.
That's a fact.
However, this is a response to mass incidents.
That's why this legislation is coming up.
You don't have a whole lot of women walking into the place and shooting the joint up,
do you?
It's a red herring.
More importantly, the legislation, it's not actually like gendered.
It's called the boyfriend loophole, but it would apply to everybody.
And they know that.
They're just lying to you because they think you're dumb.
Because they have you conned.
Because they have you so far down this road, you will oppose things that you actually support
just to please them, your betters.
What about false accusations?
Sure.
I mean, that's again, that's a real thing.
Those happen with any crime.
The complaint here is something that should be leveled against the entire justice system,
right?
The legal system.
But you're too busy being trained to back the blue to actually level that criticism.
And that 17% number that you're throwing around, just to be clear, that wasn't that they looked
at a whole bunch of cases and found out that 17% of them were false accusations.
They walked up to people and were like, hey, do you know anybody who was falsely accused?
That's where that number comes from.
Not exactly a benchmark of scientific research.
And then you have the big one.
Second Amendment shall not be infringed, right?
Except for felons.
Because of the behavior, because of something they did, right?
Which is the exact same thing.
The one type of legislation you don't argue about, this is the same thing.
More importantly, when you look at those polls, and you look at the way the average American
feels, you find out real quick that the average American cares as much about the Second Amendment
as you do about the rest of the Constitution.
Quick without Googling, what's the Seventh Amendment?
Didn't think so.
Here's the thing.
If this doesn't go through, if this incredibly focused piece of legislation does not go through,
what's the only alternative?
To make it wider.
Any act of violence.
That way it removes the Republican talking point of, we want to make sure abusers have
guns.
I don't even understand it.
But that's what it comes down to.
That means that time that you and Jimmy stepped outside the pool hall to settle your differences
and you both caught a charge, yep, no guns for you.
Now, when you look at those polls, there will be gun legislation.
It's up to you as to whether or not it's effective like this would be.
Or it's just overly broad.
Like what's going to be the next step?
Because I would love to see the Republican Party stand out there and say, violent criminals
should own guns.
Because that's what they'd have to do.
That's the argument they'd have to make.
They have y'all conned.
You are once again supporting things in opposition of your own interests, in opposition of your
stated beliefs.
Think about it for real for a moment.
In the culture of the gun crowd, somebody hits your daughter, your sister, your mother.
What do you say you're going to do?
But taking their gun, well that's just a little bit too much, right?
Their property rights over a firearm outweigh your family members' lives.
Because some guy in DC told you to believe that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}