---
title: Let's talk about Patterson's comment on race....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mGUEcEwRtNg) |
| Published | 2022/06/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing James Patterson's comments on white men facing challenges in the publishing industry and in writing jobs in film and TV.
- Exploring the reasons behind the perceived difficulties faced by older white writers in getting hired.
- Touching on the concept of systemic racism and how it affects perceptions and opportunities.
- Challenging the idea that white men are experiencing a new form of racism, suggesting it's actually a reduction in privilege.
- Emphasizing that acknowledging systemic racism exists doesn't equate to experiencing oppression.
- Encouraging openness to new ideas and perspectives to grow and adapt in changing environments.

### Quotes

- "Imagine if you were on the end it's working against."
- "The problem is you have lost just a little bit of privilege and you're mistaking it for oppression."
- "All people are trying to do is level that playing field."

### Oneliner

Beau delves into systemic racism, challenging the notion of white men facing new racism and urging openness to change and growth.

### Audience

Listeners, Learners, Allies

### On-the-ground actions from transcript

- Challenge your own perceptions and biases (implied)
- Educate yourself on systemic racism and privilege (implied)
- Support initiatives that aim to level the playing field in various industries (implied)

### Whats missing in summary

In-depth exploration of systemic racism's impact on perceptions and opportunities in the context of white privilege and competition.

### Tags

#SystemicRacism #WhitePrivilege #Opportunity #Growth #Challenges


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about James Patterson.
He's an author, and he said some stuff
that has definitely prompted a lot of discussion.
And the thing is, I've seen a lot of people definitely,
there was definitely a pushback against what he said,
but I also saw a lot of people who kind of echoed it.
So we're going to use this as a teachable moment
kind of go through what he said and some possible explanations for it okay so
what did he say basically the white guys can't get a break in the publishing
industry which is really surprising if you were to I don't know look at the
bestseller list but he's also talking about writing jobs in film and TV and you
know so on and so forth and he said that it was just another form of racism
What's that all about?
Can you get a job?
Yes.
Is it harder?
Yes.
It's even harder for older writers.
You don't meet many 52-year-old white males."
And for those who don't know, this guy, he's written, I do not know how many books, a lot.
Incredibly prolific.
His name is on the cover of a whole lot of books.
guy is worth more than half a billion dollars, B, okay?
Okay, so let's go through this.
You have the idea that it's harder for white men to get hired, and specifically older white
guys.
Let's start with that, because that one's pretty easily addressed.
Other white men might have older ideas that are dated, meaning they're not as marketable.
A lot may have chosen not to keep up with the times.
Another option is that companies are concerned about perhaps them saying something that could
limit future earning potential.
But that's a small thing.
That's not the real problem, right?
The idea is that it's harder now for white men, why?
I'm not going to deny your perception and say that it isn't harder, okay?
We're just going to leave that out of it for a second.
Why do you feel like it's harder?
Was there a law that was passed?
Say hey, don't hire old white guys for writing jobs.
Of course not.
Don't hire white guys for writing jobs.
Nope, didn't happen.
The idea is that it's harder just because it's a general attitude has changed, right?
It shifted and all of a sudden, without the law telling people this, they're just like,
we don't want your kind around here no more, right?
And it's across this entire industry, by your perception.
Welcome to the concept of systemic racism.
But don't think you're right.
You've got a long way to go.
You have acknowledged that without a law, there is a factor that involves race that
makes things harder or easier.
You can see that now.
That's what you're experiencing.
But what if I told you what you're experiencing isn't just another form of racism?
In fact, you're not experiencing racism, you're experiencing the lack of it working in your
favor as much.
That's what you're experiencing.
It isn't that people won't hire white folk anymore.
That's ridiculous.
It's that other people are getting more of a fair shake, closer to a fair shake.
That's what it is.
It means you have to get better.
There's more competition now because those walls aren't up anymore.
I mean they still are, but they're missing some bricks and people are slipping through.
And just that little bit of reduction in the racism that was working for you, you notice
it and all of a sudden it's really hard to compete.
Imagine if you were on the end it's working against.
That's why these comments get the backlash that they do.
It isn't just, haha, old guy said something silly.
No, it's that it's a denial while at the same time admitting it.
You're saying that it's all of a sudden really hard for a white guy.
It's actually not.
That's not what it is.
It's just not as easy as it used to be because you don't have the systemic racism
working in your favor as much as it used to. Make no mistake about it, you're still
on the winning team on this one. The racism that exists in the United
States is still working in your favor. Just not as much. So there's more
competition because there are more people who are getting a fairer shake.
That's it. It's not just another form of racism. It's a lack of as much racism. If
If you're somebody who has ever said this, understand you're acknowledging that systemic
racism exists.
The problem is you have lost just a little bit of privilege and you're mistaking it for
oppression.
The people who have actually been on the downside of the way the system works in this country,
they have dealt with so much worse and all people are trying to do is level that playing
field.
And yeah, it means that it's going to be harder to compete.
But I think that over time, if you allow it, it'll make you better because you're going
to be exposed to new ideas, like this one.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}