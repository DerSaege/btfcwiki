---
title: Let's talk about overcoming fear of change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=q5CFHg3ALtY) |
| Published | 2022/06/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the idea of changing the system to combat institutional and systemic racism in the United States.
- Points out that the white working class may fear losing their comparative advantage over marginalized groups.
- Explains that the current system creates a false sense of security for the working class based on comparison rather than actual well-being.
- Emphasizes that kicking down on those with less power is not the solution to societal problems.
- Predicts a positive shift towards unity and improved conditions for all once scapegoating ends.
- Advocates for competition as a means of personal and societal growth, contrasting it with the current system of division by race.
- Uses personal anecdotes and examples to illustrate the benefits of healthy competition and growth.
- Encourages stepping out of comfort zones and challenging oneself for improvement.
- Posits that ending systemic racism will lead to a stronger, more united front addressing societal issues.
- Concludes with a message of hope and encouragement for positive change.

### Quotes

- "If you're kicking down, you're not actually looking in the direction of where the problems originate."
- "Ending systemic racism will improve the lives of the entire working class."
- "It doesn't actually, harder is probably not the right word. It's more competitive, but in a good way."
- "If you've ever been the smartest person in the room, you're in the wrong room."
- "Don't let it seem scary. It's not. It's a good thing."

### Oneliner

Beau addresses systemic racism, unity through competition, and the benefits of ending scapegoating for societal growth and improvement.

### Audience

Working-class individuals

### On-the-ground actions from transcript

- Foster unity and cooperation within your community to address societal issues (exemplified)
- Challenge yourself and others to step out of comfort zones for growth and improvement (exemplified)

### Whats missing in summary

Beau's passionate delivery and personal anecdotes can provide a deeper understanding and connection to the message.

### Tags

#SystemicRacism #Competition #Unity #SocietalChange #Growth


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk a little bit more
about, well, that Patterson video
and specifically about a question that came in
because of it.
In that video, I said that as we change the system,
it's gonna get harder, but then it'll get better.
You know?
And I got a message that basically said, hey, you know,
I understand it's the moral thing to do,
it's the right thing to do.
I am totally on board with, you know,
getting down the institutional racism in this country
and just ripping the systemic racism apart.
But I have to admit, I know that it benefits me.
And at times losing that edge is scary.
Nah, nah, you'll get that all wrong.
Um, here's the thing.
Right now, the white working class in the United States,
they feel like they're doing okay, but they're not.
They only feel like they're doing okay
because it's by comparison.
They're doing better than those people.
Whoever those people are, whoever's been othered,
that gives them a comparison that makes them feel like,
hey, it's not so bad.
The reality is they're still two missed paychecks away
from being out in the street.
It's that bit about kicking down, you know?
If you're kicking down, you're not actually looking
in the direction of where the problems originate.
People that have less institutional power,
less resources, less money than you do,
they're not the source of your problem.
When that's gone and there's not somebody to kick down at,
where are people gonna look?
You'll see a resurgence in the labor movement.
You'll see better pay, better conditions, better benefits,
better social safety nets, because there won't be
that group to kick down at.
So it's gonna be everybody saying, hey,
none of us are actually doing really well under this system,
and it's going to do wonders for the working class,
and everybody will be better off because of it,
because people will be focusing
on where the problems originate rather than just a scapegoat.
Right?
And then aside from that, when you really get
to the competition aspect, which was part of it
that really kind of hit home for a lot of people,
because it is gonna be harder to compete,
the thing is, that's good.
That's good.
It makes you better.
It makes everybody better.
That's good.
I love competitions that better yourself or better the world.
This will do both.
You know, when I was young and not really bright,
there was a place near here that had fight night,
and that's exactly what it sounds like.
They had a ring, and you could go and just sign the waiver
and sign up, and they'd find somebody in your weight class,
and you could step in the ring and do your thing.
Or you could just pick somebody, and if you both agreed to it,
they'd put you in the ring together.
And I did that way more than I should have,
and I would always pick somebody that I was pretty sure
was going to win, and they normally did,
but that's the only way you get better.
That's the only way you get better.
For something that is hopefully more relatable to most people,
if you've ever been the smartest person in the room,
you're in the wrong room.
You're in the wrong room.
If you're the smartest person in the room,
you are in the wrong room, because you're not
going to get any competition there.
You're not going to be driven further that way.
The competition already exists.
Under our current system, the competition already exists.
It's just divided up by weight class, right?
But in this case, it's race.
If you step outside that, you get rid of those barriers,
you just get better.
It doesn't actually, harder is probably not the right word.
It's more competitive, but in a good way,
a way that actually makes you as an individual better
and makes the country better.
And it will.
Ending systemic racism will improve the lives
of the entire working class, because then there
will be a more united front looking at where
the problems originate.
Don't let it seem scary.
It's not.
It's a good thing.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}