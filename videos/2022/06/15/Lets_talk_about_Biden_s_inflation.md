---
title: Let's talk about Biden's inflation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U5OfMa6NfX0) |
| Published | 2022/06/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains inflation in the United States is a global issue, not solely Biden's fault.
- Cites data from Deutsche Bank showing the US inflation rate at 8.6%, better than the Netherlands but worse than Germany.
- Notes that Japan and China seem immune to high inflation due to historical factors and price controls.
- Mentions Turkey's high inflation rate at 74% and double-digit inflation in many large economies south of the US border and in Africa.
- Questions why blame is continuously pinned on Biden when inflation is a global phenomenon.
- Criticizes channels or pundits who misinform viewers by falsely attributing inflation solely to Biden, urging viewers to fact-check.
- Warns against echo chambers and information silos that manipulate and control information for their viewers' beliefs.
- Encourages viewers to seek out accurate information and question sources intentionally spreading misinformation.

### Quotes

- "It's a global phenomenon. It's happening everywhere."
- "Why do you watch somebody who thinks so little of you?"
- "If you have information sources that are constantly referring to this as something that Biden did, here are your facts."
- "Why continue to be informed by an organization or a person who you know is intentionally misinforming you?"
- "Y'all have a good day."

### Oneliner

Beau clarifies global inflation, challenges blaming Biden, and urges viewers to question sources spreading misinformation.

### Audience

Viewers seeking clarity on inflation.

### On-the-ground actions from transcript

- Fact-check information sources (implied).

### Whats missing in summary

Full context and detailed analysis.

### Tags

#Inflation #GlobalPhenomenon #Misinformation #FactChecking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about inflation
in the United States.
Bidenflation, right?
It's at a 40-year high in the United States.
Okay.
So something we've talked about on the channel
is that it's a global thing.
It's not just confined to the United States.
The idea of pinning it on Biden seems a little silly.
And I've said this, but now we have like really hard data
to back it up from Deutsche Bank.
They looked at 111 countries.
Where's the United States?
About middle, about middle.
We're running around 8.6%,
which means we're doing better than the Netherlands,
worse than Germany, okay, as far as inflation goes.
The only places that really seem to be immune to this
are Japan and China.
Japan historically has low inflation, like all the time.
In fact, they have trouble hitting
the percentage of inflation they want most times.
China has price controls.
So that helps a whole lot.
Turkey has the worst inflation in the G20 at 74%.
Most of the large economies to the south of our border
or in Africa are running in double digits.
So here's the thing.
If the US is just doing average, as far as inflation goes,
it's running in the middle of the pack, so to speak,
how's it Biden's fault?
It's a global phenomenon.
It's happening everywhere.
More importantly, if you are watching a channel
or a pundit who is continuously trying to pin this on Biden,
why do you watch somebody who thinks so little of you,
who thinks you're that easy to manipulate,
that you can't find this out on your own,
that you are just incapable of understanding basic numbers,
or do they just believe you won't look it up yourself?
A lot of echo chambers, information silos have developed,
and they have become so confident in their control over you
that, well, they don't think you'll look elsewhere
for information.
They think that they have to spoon feed you
and that you'll believe whatever they tell you.
If you have information sources
that are constantly referring to this
as something that Biden did, here are your facts.
Here's the data.
You can look it up.
Why would you continue to be informed by an organization
or a person who you know is intentionally misinforming you,
intentionally framing something in a way that is untrue,
and that, well, it's detrimental to you?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}