---
title: Let's talk about Day 3 of the hearings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RJ0eiQ48KjM) |
| Published | 2022/06/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Committee established Trump knew his claims were false.
- Trump was aware plan to pressure Pence was illegal.
- Eastman, behind the plan, openly stated it required a violation of federal law.
- Ignorance of the law isn't an excuse; judges frown upon knowingly breaking the law.
- Pence was in actual danger; informant confirmed his capture could have been the end.
- Eastman was fine with potential riots if the plan succeeded.
- Eastman sought a pardon from Giuliani post-events.
- Eastman considered appealing decisions in Georgia on the 7th.
- Questions raised on why individuals who pushed back on the plan didn't go public.
- Raises concerns on whether Trump's endorsers could potentially overturn an election.

### Quotes

- "Ignorance of the law doesn't cut it."
- "If you actually succeed in this, you'll cause riots around the country."
- "At some point when the conversations turn to there's going to be riots around the country, it seems as though that should be widely broadcast."

### Oneliner

Committee reveals Trump's knowledge of false claims, illegal plans, endangerment of Pence, and post-event actions; raises questions on public disclosures and potential election interference.

### Audience

Congress, Investigators, Activists

### On-the-ground actions from transcript

- Contact Congress to push for transparency and accountability (suggested).
- Join or support organizations advocating for democracy and election integrity (implied).

### Whats missing in summary

Detailed analysis of testimonies and implications for future investigations.

### Tags

#Trump #CapitolRiots #ElectionInterference #Accountability #Democracy


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about day three
of the hearings. Just kind of a brief overview on what happened. Okay. Key thing to remember
is that the committee has pretty well established that Trump knew his claims were false.
The stuff that he was running around the country saying, he had been told that they weren't true.
That's important to remember. On day three, we find out that he knew the plan to pressure Pence
was illegal. Not just was he told this by White House lawyers, but Eastman, his own person,
the person behind the plan, is reported to have said in front of him that it required a violation
of federal law. Ignorance of the law doesn't cut it. You know, that's no excuse. But knowing
something is illegal and doing it anyway, that's something the judges tend to frown upon.
We found out that Pence was actually in danger. There was an informant who flat out said, hey,
yeah, if they had caught him, that would have been the end. That seems important.
We find out that Eastman was cool with riots. There was a conversation that took place,
was reported to have taken place, that he was told, hey, you know, if you actually succeed
in this, you'll cause riots around the country. And the reported general attitude was,
I mean, you want to make an omelet, you got to break some eggs, right?
I guess the quote that was given was something along the lines of, you know, in history,
there's been violence used to, I guess, preserve democracy or the republic, something along those
lines, feeding into the claims that they know are false. Afterward, Eastman wanted a pardon.
He reportedly emailed Giuliani asking for one. Yeah, that also seems important.
And going along with this, I guess on the 7th, he brought up appealing decisions in Georgia.
It does appear that Pence's staff and the White House lawyers kind of pushed back on the whole thing.
So these are the key elements that we'll probably see pop up again. There was some other stuff,
but this is the most important stuff. But we do have questions about... I have questions anyway.
There's a whole lot of people who have provided testimony saying that they pushed back on this
plan. Why didn't they really go public? I mean, really go public. I mean, I understand the idea
of being the adult in the room, but at some point when the conversations turn to there's going to be
riots around the country, it seems as though that should be something that should be widely broadcast.
I have questions there. And then one of the other questions I have, I think I'm going to save for
an entirely separate video, but I also have to wonder if the American people understand,
given all of the information that we have, do you think that Trump's endorsements, those people he's
saying, hey, put this person in office, do you think that they'll push back against a plan like this?
Or do you think that these are people that would be more likely to help overturn an election?
That seems to be something that isn't really part of the hearing,
but it should certainly be a question that's being asked.
So far, the committee's doing a really good job of basically laying out a case.
Now, again, the committee has the power to refer or not, but it's still DOJ's final decision,
and we're not hearing a whole lot there. But given what we've seen
just so far, there certainly seems to be cause to perhaps look at this case in another venue.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}