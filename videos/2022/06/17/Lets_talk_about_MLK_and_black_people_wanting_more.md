---
title: Let's talk about MLK and black people wanting more....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TCm5Q_Rer_Y) |
| Published | 2022/06/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Continuing a recent series of talks about tensions in the country.
- Addressing a message about radicalism and race.
- Emphasizing that radical black individuals seek more than just equality.
- Urging white Americans to recognize the need for radical societal changes for justice.
- Pointing out the lack of effort from white individuals to overcome racial ignorance.
- Stating that substantial investment in black Americans is necessary for progress.
- Describing the resistance to adjusting to black neighbors and genuine school integration among white Americans.
- Explaining the root causes of contemporary racial tensions.
- Stressing that racial and economic injustices require a radical redistribution of power.
- Quoting Martin Luther King's 1967 speech to illustrate longstanding radical ideas.
- Encouraging further exploration of the original vision for racial equality beyond surface-level knowledge.

### Quotes

- "White Americans must recognize that justice for black people cannot be achieved without radical changes in the structure of our society."
- "The problems of racial injustice and economic injustice cannot be solved without a radical redistribution of political and economic power."

### Oneliner

Beau addresses the need for radical societal changes to achieve justice for black individuals, drawing from Martin Luther King's 1967 speech to challenge perceptions on racial equality.

### Audience

Activists, allies, educators

### On-the-ground actions from transcript

- Educate on racial history and the original vision for equality (suggested)
- Advocate for societal changes to address racial and economic injustices (implied)

### Whats missing in summary

Exploration of how historical perspectives can inform current activism and advocacy efforts.

### Tags

#RaceRelations #RacialJustice #SocialChange #MartinLutherKing #Activism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to continue our conversation that's been going on the last couple days,
talk a little bit more about tensions in the country, because I got another message, and
it's got some unpleasant stuff in it, we're going to skip over that.
I've been listening to you talk about race the last few days, and while you bring up
some good points, you seem to not understand how radical black people have become.
They aren't seeking the equality Martin Luther King spoke of.
They want everything changed.
I listened with an open mind, maybe start a dialogue with your black viewers and ask
them why they've strayed so far from the original vision and keep demanding more.
You think what you hear today is radical, huh?
Nah, not at all.
I'll give you radical if you want though.
White Americans must recognize that justice for black people cannot be achieved without
radical changes in the structure of our society.
Whites, and we need to say this frankly, because they're not putting any effort to reeducate
themselves out of their racial ignorance.
It is an aspect of their sense of superiority that the white people of America believe they
have so little to learn.
The reality of substantial investment to assist black Americans into the 21st century, the
idea of adjusting to black neighbors and genuine school integration is still a nightmare for
all too many white Americans.
These are the deepest causes for contemporary abrasions between races.
Loose and easy language about equality, resonant resolutions about brotherhood, yeah they fall
pleasantly on the ear and all that.
But I would imagine for them, there's a little bit of a credibility gap, something they can't
overlook because they remember that with each modest advance, the white population promptly
raises the argument that black people have come far enough.
Each step forward accents an ever-present tendency to backlash.
The evils of capitalism are as real as the evils of militarism and racism.
The problems of racial injustice and economic injustice cannot be solved without a radical
redistribution of political and economic power.
How's that for radical?
Probably a little hard to listen to.
It's hard to say too because the calling to speak is often a vocation of agony and some
people probably just got it.
You know that part where I'm talking about how somebody pops up to say that they've come
far enough, I imagine they hit a little close to home.
Thing is that wasn't for you.
Literally that was not for you.
I mean not directly anyway, it wasn't in response to your message because it was written in
1967 by Martin Luther King.
He wrote that knowing you would exist today.
You might want to think about the fact that everything I just said, with the exception
of when my hand was in the frame, was a quote from him in 1967.
It's not that these ideas are radical, they've been around half a century.
I had to make minor changes to it because he used a word for black people that I don't
and I had to change 20th century to 21st century because it's taken that long.
They're not demanding more.
It's the same thing for half a century.
Maybe you should do a little bit of reading and find out what that original vision really
was because it is way more than I have a dream.
Anyway it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}