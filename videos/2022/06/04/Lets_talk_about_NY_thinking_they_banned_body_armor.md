---
title: Let's talk about NY thinking they banned body armor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vBVMrIHuY3M) |
| Published | 2022/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New York may need subject matter experts for legislation due to recent headlines about banning body armor.
- The bill defines body armor as soft, obsolete Kevlar, not modern hard plate armor.
- Regulating hard body armor like AR-500 steel is challenging and impractical.
- Legislation banning body armor seems to be a symbolic, ineffective measure.
- Understanding subject matter is vital for effective legislation.
- The bill is on its way to the governor despite banning outdated body armor.
- Modern body armor consists of hard plates, not soft, multi-layer fabric.
- Soft body armor became obsolete in the early 2000s due to its ineffectiveness against rifles.
- Legislation will be ineffective if it does not grasp the subject matter.
- Lack of understanding led to passing legislation that bans something rarely used.

### Quotes

- "Your legislation will be worthless if you don't actually understand the subject matter."
- "This legislation was designed to be a feel-good measure."
- "It's just a thought."
- "Understanding subject matter is vital for effective legislation."
- "Lack of understanding led to passing legislation that bans something rarely used."

### Oneliner

New York's legislation on body armor lacks understanding of modern armor, rendering it ineffective and symbolic rather than practical.

### Audience

Legislators, policymakers, concerned citizens

### On-the-ground actions from transcript

- Contact subject matter experts to inform legislation (suggested)
- Advocate for more comprehensive and informed legislation (suggested)

### Whats missing in summary

The nuances and detailed explanations provided by Beau in the full transcript. 

### Tags

#Legislation #BodyArmor #NewYork #SubjectMatterExperts #PolicyMaking


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about how the state of New York
might need access to subject matter experts,
or at least some people who have a basic understanding
of what they're trying to legislate.
This is a topic that has come up a lot
over the last couple of weeks.
People have wondered,
why do I really need to know about this stuff?
We just need to pass the law.
Well, because if you don't,
you may pass something that is less than effective.
Now, I do want to leave open the possibility
that the state of New York passed an amended version of this
that alleviates this problem,
and it's just not reflected online yet,
but I find that unlikely.
Maybe it happened, though.
We'll leave the door open for that.
We'll give them the benefit of the doubt.
So you've probably seen the headlines.
New York has banned body armor.
Did they, though?
I mean, there's a bill here, and sure, it says,
a person is guilty of the unlawful purchase of a body vest
when not being engaged or employed
in an eligible profession.
They knowingly purchase or take possession of a body vest
as such item is defined in subdivision 2 of section 270.20.
So what does that say?
For the purposes of this section,
a body vest means a bullet-resistant, soft body armor
providing, as a minimum standard,
the level of protection known as threat level 1,
which shall mean at least seven layers
of bullet-resistant material providing protection,
and it goes on, but it doesn't matter
because anybody who knows about this stuff
started laughing at the word soft.
Congratulations, New York.
You banned Kevlar, a product that is relatively obsolete,
that most people haven't used in like 20 years.
Modern body armor isn't soft.
It's a plate.
It's a hard plate.
That's why those things are called plate carriers,
the things with all the pouches all over them
that have the hard armor inside.
The definition of body armor, body vest, in the New York Code,
that doesn't cover anything that actually gets used anymore.
This was a completely wasted piece of legislation.
And before you all decide to go and amend this,
I just want to point something out.
The hard body armor, the type that gets used today,
I don't know of any way you could actually regulate it.
Most of it is like AR-500.
AR, in this case, means abrasion-resistant.
It's a type of steel.
That's what typically gets used.
I have no idea how you would attempt to regulate that.
Now, I do want to say, for the record,
I don't actually think that this type of legislation
is a good idea, because the same thing that's
going to stop the bad person from getting the body armor
is going to stop your kid's bullet-resistant backpack
as well.
This legislation was designed to be a feel-good measure.
I don't see how it could have meant to have been effective
without a change in the definition,
because Kevlar is pretty out of circulation.
That's not really what people use.
That's not what people go out and buy.
They buy those plate carriers with all the MOLLE gear,
and inside is a hard plate.
It's not multi-layer soft fabric.
That stuff kind of ended, I don't know,
early 2000s, in that range.
That's when that kind of fell out of favor,
and people started moving towards the hard plate armor,
because the soft body armor really
wasn't that effective with rifles.
So this is why, as much as it may pain you
to actually learn about some of this, this is why you have to.
Your legislation will be worthless
if you don't actually understand the subject matter,
and that certainly appears to be what happened here,
because according to the Senate website up there,
this is on its way to the Senate.
The Senate website up there, this is on its way to the governor.
And the text that's attached, it's
banning something that nobody uses.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}