---
title: Let's talk about masculinity and whether it's toxic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5_Dc_A-REhM) |
| Published | 2022/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Masculinity in the US is often shaped by historical figures and popular culture, with men aspiring to emulate heroes like Patrick Henry, Paul Revere, and Alvin York.
- Focusing solely on the superficial aspects of masculinity can lead to toxic behavior, characterized by amplifying hyper-masculinity and neglecting the deep, nurturing side.
- The concept of toxic masculinity does not imply that all masculinity is toxic; rather, it points to the dangers of fixating on specific negative traits.
- The term "toxic masculinity" did not originate from feminists but actually emerged from a men's movement known as mythopoetics, aiming to redefine masculinity.
- Business interests and popular culture often perpetuate shallow and harmful stereotypes of masculinity, promoting a skewed tough guy image.
- Beau references the movie "Fight Club" as an example of popular culture that may be misinterpreted as celebrating hyper-masculinity while actually critiquing it.
- Many historical masculine icons were multifaceted individuals with creative talents and nurturing qualities, often overshadowed by their more aggressive personas.
- Beau suggests that toxic masculinity is akin to consuming poisonous parts of a delicacy, discarding the valuable aspects that should be embraced.
- The damaging effects of toxic masculinity are felt not only by men but by society as a whole, perpetuating harmful stereotypes and behaviors.
- Beau encourages a deeper exploration of masculinity beyond superficial portrayals, advocating for a more balanced and inclusive understanding of what it means to be masculine.

### Quotes

- "Toxic masculinity is the exact opposite. You're throwing away the parts that you should fill yourself with and you're just eating the part that will eventually kill you."
- "Men want to be like that. People like Patrick Henry, you know, give me liberty or give me death."
- "Those aspects, the creative, nurturing side, that gets tossed away and you're just left with the violence, with the tough guy image."
- "Focusing on a certain part of it [masculinity] is [toxic]. It's spoiled milk. Not all milk is spoiled."
- "We're focusing on this shallow part and amplifying it, becoming hyper-masculine, toxically masculine, and we're missing the deep masculine."

### Oneliner

Exploring the roots of toxic masculinity, Beau delves into the importance of embracing both the nurturing and assertive aspects of masculinity to combat damaging stereotypes.

### Audience

Men, Masculinity Explorers

### On-the-ground actions from transcript

- Challenge toxic masculinity in your circles by embracing a more balanced and inclusive definition of masculinity (implied).
- Support men's movements that aim to redefine masculinity beyond toxic stereotypes (exemplified).

### Whats missing in summary

Deeper insights on the societal impacts of toxic masculinity and the importance of redefining traditional notions of manhood for a healthier future.

### Tags

#Masculinity #ToxicMasculinity #GenderRoles #Men #Mythopoetics


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about masculinity.
What it is, what it isn't.
Is it good, is it bad, is it toxic?
We're gonna talk about what that means.
So in the United States, and this is true in most places,
but definitely in the United States,
people's image of masculinity is very much based
on historical figures and their portrayal in popular culture.
And people try to emulate that.
Men want to be like that.
People like Patrick Henry, you know,
give me liberty or give me death.
Paul Revere, Alvin York, people like this,
historical figures that have gotten a treatment
in popular culture that makes them into heroes,
masculine figures, people you want to be like, right?
That's where it comes from.
Now, one of the problems, and to be clear,
there's nothing wrong with any of that, okay?
One of the problems is if you just focus
on the superficial, if you just focus on part of something,
particularly if it's the wrong part,
well then it can turn bad, become toxic.
Toxic doesn't mean, in this context,
when people say toxic masculinity,
it doesn't mean that masculinity is toxic.
It means that focusing on a certain part of it is.
It's spoiled milk.
Not all milk is spoiled.
That's the way that term works.
This isn't a term that came from feminists,
which is the general idea.
It didn't, it's the exact opposite.
It came from the mythopoetics, which was a men's movement.
It's a men's movement.
These were men who looked around and were like,
we're messing up.
We're messing up.
We're focusing on this shallow part and amplifying it,
becoming hyper-masculine, toxically masculine,
and we're missing the deep masculine,
the creative, caring, nurturing side.
The term didn't come from feminists.
It came from men's movements,
people who wanted to create more masculine figures.
And then in the United States and in a lot of countries,
there's a habit of business interests amplifying this.
Do we really need gendered soap?
Is that a thing?
You won't get clean with that girly soap, stuff like that.
And again, it focuses on the wrong aspect.
It focuses on that tough guy image,
the portrayal, just a portion.
You know, those people I listed at the beginning,
there's nothing wrong with wanting to emulate those people.
Just don't be shallow about it.
And a lot of popular culture depictions are shallow.
A pop culture reference
that I think most people would get is Fight Club.
That is seen, that's actually listed
as like the ultimate guy's movie.
I think a lot of people may be missing the point
of that film.
Sure, it's masculine, a whole bunch of guys, you know,
fighting in basements, living together, doing guy stuff.
Sure.
But it's kind of providing a lot of the same critiques
that the mythopoetics did.
Think of the scene where they're in the car,
Tyler's driving, he lets go of the wheel.
What do you wish you had done before you died?
The two men in the back without hesitation,
paint a self portrait, build a house.
The creative aspect is neglected
and it's just the superficial image.
When you think about those figures I listed,
nobody is gonna say,
oh, those aren't masculine figures, right?
Patrick Henry was a flute player.
To be clear, he was a literal flute player.
He was a musician.
He played several instruments.
Paul Revere, best known as an artist.
Alvin York didn't wanna fight.
Those aspects, the creative, nurturing side,
that gets tossed away
and you're just left with the violence,
with the tough guy image.
And that is damaging to men and to society as a whole.
If you look at most great masculine icons
in American history,
you're gonna find out that most of them
were very creative people.
They had a whole lot of other interests
and generally they used that aggressive nature
to help people.
Maybe not always in the best ways,
but they thought they were.
That's how they viewed their actions.
Toxic masculinity is a lot like a lot of delicacies
around the world.
They come from venomous or poisonous items
and those, when prepared correctly,
they're taking that part out.
Toxic masculinity is the exact opposite.
You're throwing away the parts
that you should fill yourself with
and you're just eating the part
that will eventually kill you.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}