---
title: Let's talk about time marching on and a secret I kept for thirty years....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7IqLlfBBkH8) |
| Published | 2022/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Growing up in Tennessee in the early 90s, he befriended a girl in the neighborhood and developed a crush on her.
- The girl's father was irrationally angry and controlling, leading to uncomfortable situations.
- During a date to the movies with the girl, they watched "The Bodyguard," which caused her father to explode in anger afterward.
- The father's outburst was triggered by the interracial relationship portrayed in the movie, leading to a tense car ride home.
- The father expressed racist views and used derogatory terms, making Beau extremely uncomfortable.
- Following this incident, the girl's father started driving her to school, changing their dynamic.
- Despite the uncomfortable experience, Beau never spoke about it until now, recognizing parallels with modern parenting attitudes.
- Beau reconnects with the girl years later through social media and learns that she married a black man, a fact that he imagines upset her father greatly.
- Beau concludes with a message of hope, suggesting that over time, hate and prejudice diminish as each generation progresses.

### Quotes

- "There's obvious parallels between that and the attitudes of a lot of parents today."
- "On a long enough timeline, we win."
- "Each generation is better than the last."

### Oneliner

Beau in Tennessee shares a poignant story of past prejudice and hope for a better future, reminding us that hate fades with time.

### Audience

Parents, educators, youth advocates

### On-the-ground actions from transcript

- Support interracial relationships openly and actively in your community (implied).
- Challenge and address racist beliefs and behaviors when encountered (implied).

### Whats missing in summary

The deep emotional impact of experiencing racism and prejudice firsthand, and the enduring hope for a more inclusive future.

### Tags

#Prejudice #Racism #Hope #InterracialRelationships #Parenting


## Transcript
Well, howdy there internet people.
It's Bubba again.
So today we're gonna talk about time marching on
and I'm going to tell you a secret
that I have kept for 30 years.
When I was growing up, this is the early 90s
and I'm living in Tennessee.
There's a girl that lives on the street
and we became friends when I moved into the neighborhood.
And man, did I have a crush on this girl.
Her dad was irrationally angry though, all the time.
I didn't have those words to describe it back then
but I knew he had a short fuse
and I knew he was very controlling.
I first noticed this when we're in class
and we're all reading one book.
I think it was A Raisin in the Sun.
And she's reading something else
because her father, one of those
who's always complaining about something,
had just lost his mind up at the school
and she had an entirely separate reading list
approved by him because what we were gonna read
wasn't appropriate.
Wild.
But he liked me well enough.
In fact, he drove us to our first date.
He took us up to the movie theater.
And we go up to the window and buy the tickets
for whatever it was we said we were gonna go watch.
And then we went inside and we cut the corner
and went into the movie, The Bodyguard.
It worked.
Plan went off without a hitch
until we walked out of the movie when it was over
and he was standing in the lobby.
And he started screaming, ranting, raving, yelling,
telling her she was gonna get the belt,
telling me he was gonna call my parents.
I'm sitting there thinking, yeah, what my parents do.
I was not gonna go watch whatever it was that we said.
But this yelling went on for an extended period.
I mean, keep in mind, different time.
This wasn't totally abnormal,
but even by the standards of back then,
this was a little too much.
And then we leave and we start walking towards the car.
And we get in the car, his whole tone changes with me.
It's no longer this yelling, ranting,
raving kind of attitude.
It's very paternal.
And he's saying, you've got to be more careful.
There are things you don't know.
Well, okay, I'm listening.
And I look at her and she's already embarrassed.
Like it has switched from the demeanor she had in the theater
was scared, cause I'm getting in trouble.
When we got in the car and he started talking,
it did, it turned into embarrassment.
And he told me that I needed to be more careful
about what I was exposing her to,
because we needed to stick together.
And I should know as a good young white man,
that that's not appropriate for young ladies to watch.
Now, the young white man thing kind of caught my ear,
but at this point in history,
parents were freaking out about violence in movies.
So I was still a little confused
until he started talking about the interbreeding
that went on in that film.
That's what he was upset about,
that a white man and a black woman
had a relationship in that movie.
That's why it was off limits.
And he kind of goes on and the more he talks,
the more it turns back into anger.
And I heard the N word more in that car ride
than I probably have in my entire life in person.
And then we get back to the street and I go home.
He never calls my parents.
And the next day, she's not at the bus stop.
From that day forward, he drove her to school,
but it didn't matter because once he dropped her off,
it was a different time.
We didn't have to take up defensive positions
as soon as we got to school.
So she walks in the front doors through the cafeteria,
comes out back where we all hang out before school.
And as soon as she walks up, as soon as she sees me,
she kind of pulls me aside.
She's like, did you tell anybody?
Like, no, I wouldn't even know how to begin to explain that.
And she was, she was just totally embarrassed by it.
And I don't believe I have ever told that story
until right now.
Don't think I ever talked about it again.
There's obvious parallels between that
and the attitudes of a lot of parents today.
There are parents today who are ranting and raving
about things that in 20, 30 years,
it's just going to seem so weird
that people were upset by it.
And I'm not saying this to say, you know,
don't worry about it.
It's fine.
It'll all sort itself out because, I mean,
I know how uncomfortable I was hearing it.
I can't imagine it being directed to me, you know?
But there is a little bit of a sequel to this story.
Five or six years ago, social media did that thing.
Hey, you may know this person.
Hey, I know that person.
And we had that one weird conversation
that you have with people that you went to school with
when you were younger and then haven't seen
in a quarter century.
You basically just explain what you did in the meantime.
She went to UT, University of Tennessee,
and she got married, has a couple of kids.
And I know you want to know.
Yeah, he's black.
Yeah, her dad is spinning so fast in his grave,
you could hook him up and power the entire country.
But it does go to show that on a long enough timeline,
we win.
On a long enough timeline, the hate,
the ridiculous beliefs, it all fades away.
Each generation is better than the last.
I know it's no consolation now,
but at least it's a light at the end of the tunnel.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}