---
title: Let's talk about Socrates, gamers, and you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=X-WjO4c5V-c) |
| Published | 2022/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of the triple filter test attributed to Socrates, questioning if statements are true, good, and useful before speaking.
- Shares a modified version of the triple filter test: questioning if statements are true, moral, and necessary before speaking.
- Emphasizes the importance of filtering information before spreading gossip, especially in real-life organizing.
- Mentions a specific group of players of the video game War Thunder who leaked classified information on military equipment in forums.
- Recounts instances where schematics of military equipment like the Challenger tank and France's main battle tank were leaked due to personal disputes within the game community.
- Advises individuals, especially those involved in organizing, to avoid engaging in unnecessary conflicts or gossip that may hinder their mission.
- Raises awareness about the impact of personality conflicts and gossip on organizing efforts across the country.
- Stresses the significance of applying the triple filter test to avoid damaging consequences in the long run.

### Quotes

- "Is it true? Is it moral? And is it necessary?"
- "Your ego can often cause you to talk a little bit more than you should."
- "Does that personality conflict help or hinder you?"
- "There are large organizing efforts that are being really hindered by people not applying that triple filter."
- "It's something that could be damaging over the long haul."

### Oneliner

Beau shares insights on applying the triple filter test to prevent spreading damaging information and gossip, particularly relevant for those involved in organizing efforts.

### Audience

Organizers, gamers, activists

### On-the-ground actions from transcript

- Apply the triple filter test in your communication and decision-making processes (implied).
- Avoid engaging in unnecessary conflicts and spreading gossip that may hinder collective efforts (implied).
- Prioritize the mission and goals over personal conflicts and ego-driven interactions (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of the consequences of sharing unnecessary information and engaging in conflicts within communities, urging individuals to prioritize truth, morality, and necessity in their actions and speech. 

### Tags

#Socrates #TripleFilterTest #Organizing #Gossip #CommunityPolicing


## Transcript
Well, howdy there internet people. It's Beau again.
So today we're going to talk about what Socrates
can teach
people who play a specific video game
and all of us, really.
Especially those people who are involved in
real-life organizing.
There's a
test.
It's called the triple filter test
that's attributed to him. To be honest, I have no idea if this is real or something
that just became part of his mythology later.
But the idea
is that before you say something
you should ask yourself three questions.
Is it true?
Is it good?
And is it useful?
And if the
answer to those three questions isn't yes,
well you don't say it.
Now when I heard this,
it was, is it true?
Is it moral?
And is it necessary?
Necessary is a much higher bar to meet
than useful,
which might explain why I don't talk a whole lot.
And if you are somebody who is
organizing
and you are dealing with a lot of people,
this is something to keep in mind.
A lot of times
there is, for lack of a better word, gossip.
And whether you repeat that
is up to you.
Whether or not you participate in that
is up to you.
The question is, is it necessary?
Or is it even useful?
Does it help?
That's something to just keep in mind.
Now, there is a specific group of people who play a video game called War Thunder
that definitely need to keep this in mind
because,
well, something happened.
This game,
it simulates combined arms operations
and it uses
semi-realistic specs when it comes to various pieces of military equipment
that are actually in the inventories of militaries all over the world.
I say semi-realistic specifications
because apparently they weren't accurate enough
for somebody
and they had issue
with how some Chinese equipment was being depicted.
So in an effort to get the game developers
to change the specs
of this particular equipment,
they posted
the schematics
and a photo of the part
in the game's forums.
And intelligence agencies all over the world thank you for your service.
That's funny in and of itself.
The thing is, that's not the first time it happened
with that same video game.
In July of 2021,
somebody posted basically all the information for the Challenger main battle tank.
It's the United Kingdom's tank
to the same forum.
And it was basically over an argument.
And it was somebody trying to prove their point.
And they posted information that got the Ministry of Defense involved.
Not to be outdone,
just a few months later,
somebody posted the specs to France's
main battle tank
in the same forum
for the same reason.
Your ego
can often cause you to
talk a little bit more than you should
and share information.
Playing that game of who's in the know,
it's
something that
I mean it should in theory be trained out of people
who have access to classified information.
But
it's something to keep in mind if you're organizing.
Because there are
there's a lot of people who are involved,
which means there's a lot of personality conflicts.
And if your goal
is organizing, if your goal
is mission first,
if you're really just trying to help and trying to accomplish
whatever you have set out to accomplish,
does that
personality conflict
help or hinder you?
Does that gossip
help or hinder?
Do you want to be a part of that?
There are
large organizing efforts
that are taking place all over the country
and some are being
really hindered
by people not applying
that triple filter.
It's
something that could be damaging
over the long haul.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}