---
title: Let's talk about the DHS bulletin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3hNvqU9UV54) |
| Published | 2022/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Gyan says:

- A DHS bulletin warns of an elevated risk until November 30th, pointing out certain events that may serve as catalysts for violence.
- Foreign groups are a passing mention in the bulletin, with Americans being seen as the main potential threat to societal stability.
- The catalysts identified include general racism, xenophobia, Supreme Court decisions, and the upcoming election.
- Hostile foreign nations are amplifying conspiracy theories that could incite violent events in the U.S.
- The bulletin predicts that potential events will target soft, crowded, low-security locations.
- Beau finds this bulletin to be accurate, in alignment with private assessments except for the frequency of potential catalyst events.
- The key advice given is to stay aware, be alert, know your surroundings, and have an exit plan without succumbing to fear.

### Quotes

- "It's Americans that are going to rip the country apart."
- "Know where you're at and how to get out."
- "Be aware, be alert, know how to get out of the location that you're in."
- "That's going to be the most likely place anyway."
- "Y'all have a good day."

### Oneliner

A DHS bulletin warns of an elevated risk until November 30th, pinpointing potential catalysts for violence and urging Americans to stay alert and prepared in crowded, low-security locations.

### Audience

Citizens

### On-the-ground actions from transcript

- Stay aware and alert in crowded, low-security locations (implied).
- Know your surroundings and have an exit plan (implied).

### Whats missing in summary

The urgency and importance of being vigilant and prepared in potential target locations.


## Transcript
Well, howdy there, internet people.
It's Bo Gyan.
So today, we are gonna talk about that DHS Bulletin
that went out, what it means,
and some interesting highlights from it.
Short version of this.
You need to be on elevated alert
until, I don't know, Christmas.
says this bulletin is set to expire November 30th.
And what it does is it points out an elevated risk
due to certain events that will be occurring between now
and then, and how those events may be used as a catalyst.
They serve as a catalyst for violence.
One of the things that's interesting
is that foreign groups, it's a passing mention.
It's a passing mention.
They're not high on the priority list right now.
No, it's Americans that are going to rip the country apart.
The main catalysts they see, general racism and xenophobia,
which is always there, the committee,
Supreme Court decisions, and the election.
It also points out how hostile foreign nations
are amplifying conspiracy theories that
are likely to be a catalyst, something
that can bring these types of events about.
And of course, these are the same theories
that get echoed by people who call themselves patriots.
You might want to consider that.
Just consider that aspect of it.
Consider the idea that what you are echoing is the same thing that hostile foreign nations
that want to see the country ripped apart are promoting and amplifying and maybe work
backward from there.
So what's it going to look like?
What are the events going to be like?
It's going to be soft targets, crowded, packed, low security locations where it isn't expected.
So everything we've seen recently, the supermarket, all of that stuff, that's what it's going
to be.
That's what they are predicting.
You know, normally when I cover one of these bulletins, I have a lot of critiques of it.
To me, this seems pretty spot on.
This lines up with the private assessments for once, so I know normally when I'm going
over something like this, I'm like, yeah, they forgot this or that, or they're hyping
up this for a political reason or whatever, no, this seems pretty accurate.
The private assessments I've seen basically just have more events that could serve as
catalyst. That's really the only difference. So short version, at least
until the end of November, be aware. Be alert. Know where you're at and how to
get out. Those are the key things that you can do without letting them
win without living your life in fear. So be aware, be alert, know how to get out
of the location that you're in, and just generally pay attention if you are in a
location that is crowded, that is low security, because that's where something
like that might happen. That's going to be the most likely place anyway.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}