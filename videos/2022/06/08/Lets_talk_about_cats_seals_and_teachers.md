---
title: Let's talk about cats, seals, and teachers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZMAosp6c2no) |
| Published | 2022/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how to use tourniquets and chest seals for emergency situations.
- Emphasizes the importance of proper training and staying updated on best practices.
- Shows the differences between various tourniquet types, like the Cat Generation 7.
- Advises on storing equipment securely and conveniently for easy access during emergencies.
- Demonstrates how to apply a tourniquet correctly and efficiently.
- Describes the purpose and application of a chest seal to prevent air from entering the chest cavity.
- Urges the audience to seek more comprehensive training beyond the basics he covers.
- Gives practical tips on how to improvise with available materials.
- Encourages taking courses like Stop the Bleed to enhance emergency response skills.
- Stresses the importance of continuous learning and preparedness in life-threatening situations.

### Quotes

- "This isn't training. You need real training."
- "You want to stay up to date because what is considered best practices changes over time."
- "This is how you become a superhero. This is how you save lives."
- "Please get more training. This isn't enough."
- "You can never have enough gauze. Ever."

### Oneliner

Beau explains the basics of using tourniquets and chest seals for emergencies, stressing the need for proper training and continuous learning.

### Audience

Emergency responders, teachers, concerned citizens

### On-the-ground actions from transcript

- Take a Stop the Bleed course (suggested)
- Ensure secure and accessible storage for emergency equipment (implied)
- Seek additional training beyond the basics covered (implied)

### Whats missing in summary

The full transcript provides a detailed demonstration of using tourniquets and chest seals for emergency situations, along with practical advice on improvisation and the importance of continuous learning.

### Tags

#EmergencyResponse #FirstAid #Training #Preparedness #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about how to be a superhero.
Now today's gonna be very different
than most of the videos on this channel.
This video is at the request of some teachers
who either have heard about equipment
but have no idea what it looks like or how to use it,
or some that were given equipment
and never told how to use it.
So we will be going over tourniquets.
I do have one of the Cat Generation 7 tourniquets here.
We will compare that to another tourniquet
when it comes to pediatrics to see if the study
holds up anecdotally right here in front of you.
I'll go ahead and tell you now it does.
We have a chest seal.
Show you what it looks like.
We're gonna open one up, show you what it looks like,
how generally to apply it.
One thing that I wanna be clear about
is that this is the beginning of your journey.
It's not the end, okay?
This isn't enough.
This isn't training.
You need real training.
This is to familiarize us with the equipment.
That's it.
You can get stop the bleed courses.
You can even look on YouTube
for people who are way better at this,
who are way more up to date than I am.
And once you have training, you want to stay up to date.
You definitely wanna stay up to date
because what is considered best practices
changes over time.
You know, I first learned CPR like forever ago.
I don't know, it was in the late 1900s.
The only thing that's the same today as, you know, then
is the songs you sing
to make sure you get the right beats per minute.
If you're curious, it's either staying alive
or another one bites the dust.
Aren't those fitting?
So just given something that simple, CPR,
and what is considered best practices when it comes to that,
stuff like this is gonna change.
You want to stay up to date.
You want to give the people that you're trying to help
the best chance.
You want to have as many options available to you.
So don't view this as a training course.
It's not.
This is not sufficient.
I feel like I've said that enough now.
Okay, one of the questions that I got from somebody
was apparently they just gave you the supplies.
What do you want to store it in?
I would use something like this.
This is a normal IFAC pouch, right?
This is one similar to the one that the military uses.
Has the MOLLE straps up front.
And you could use that to attach
additional tourniquet holders
and put more tourniquets on the outside, which I would do,
given the situation you're trying to prepare for.
And then I would take it and use these rings
to mount it to something, okay?
Mount it to a desk, to a wall,
something that would be accessible
in that type of situation.
Inside this only goes the trauma stuff.
The Boo-Boo First Aid Kit for minor stuff,
that can be somewhere else.
This, you want to know where this is at all times.
You don't want a janitor to pick it up and move it
or a substitute teacher or a student.
You don't want to spend minutes looking for this
when seconds count.
You want to know where this is.
So have it somewhere secured.
Now, one of the things about it is when you say that,
people are like, well, what if I need to move it?
It's okay because of the way they're designed.
The bag separates from the mount via Velcro, okay?
So this is the type that I would use.
Okay, so this, I already tore my glove.
This is a cat tourniquet.
This one is generation seven.
So the band goes around the limb, okay?
This is tightened and it restricts flow.
Then you take it, you clip it into that.
Really hoping you can see all of this.
Slides right into there.
Then you just tie, says time, write the time down.
Hopefully you won't be in the situation long enough
to leave this there to where the time matters.
But I'm starting to have my doubts.
So general idea, the loop over the limb,
tighten it and then you use this
to tighten and restrict flow even further.
Clip it in and strap it.
Okay, this is a PVC pipe.
It is roughly the size of a child's arm, child's limb.
Because, you know, that's what we're talking about here.
So it goes over, tighten it, right?
Okay, now you would spin this.
The thing is, this is the generation seven.
It doesn't spin because the PVC pipe doesn't compress.
Which means this is pretty much already,
I mean, this is already flush up against the pipe.
You can't spin it anymore.
Okay, so one thing while I have it here,
I wanna show you something.
When you're talking about small limbs,
even if you were to spin it,
if you were to spin it up and wind it up way over here,
it wouldn't be able to clip in.
And that's one of the main things
about these tourniquets that is so good.
Once you clip it in,
you don't have to stand right beside the patient, right?
So what you wanna do is make sure it's over,
way over near the clips before you start spinning.
All right, okay, so the generation seven,
very, very tight just by using the strap.
You're not even using the windup part.
This one is like Bob's tourniquet.
I don't even know what brand this is.
I don't know where this came from.
I think Keith left this in my truck.
It is definitely not a generation seven.
Does it function the same way?
It does in the sense that you put the band over
and tighten with it.
Okay, but here's the thing,
as tight as you can get it, you can still spin.
So that study,
clip it in, close that.
That study that says that generation seven
works better on smaller limbs,
that report, all of that information
is definitely backed up by this visually.
So if you have any questions about whether or not
that's true or it's just marketing,
yeah, it certainly appears to be true
just from a real quick accidental test with a PVC pipe.
Okay, this is Woody.
Woody has a hole.
Okay, so what we're going to do
is we're going to stop it.
This is a chest seal.
People have asked what it actually does.
And it creates an artificial chest wall, okay?
If there's a puncture, basically anywhere from the neck
to kind of the belly button,
it runs the risk of creating a situation
where air goes into the chest cavity
and then it doesn't allow the lungs to expand.
That's really bad.
So what you want to do is stop that from happening.
Now, the reason I have gloves on
is because if you were to run into this situation,
your glove will help create a seal for a moment anyway.
Now, when it comes to the actual chest seals,
you have the pack.
Has written instructions on the front.
On the back, it has army instructions
through little cartoons.
All right, so you open the thing up.
All right, tear at the little red lines.
Inside, hopefully, there should be some gauze and the seal.
Now, when you open this, open it carefully.
And we'll explain why in a second
because it answers another one of your questions.
So here is your seal.
This is how it comes out.
All right.
And there is the gauze right inside.
So you take the gauze, wipe up anything that's there.
You want it dry so it can stick, right?
Make sense?
You have the red tab and a clear tab.
You separate them.
This part, you can set aside.
Also could save it in a situation, and we'll get to that.
This is the dressing.
One side is super sticky, one side isn't.
The side that is super sticky goes over the hole.
You hopefully can see that there is a circular shape
in the center of it, right?
With some lines on it.
And you can see that there's a little hole
in the center of it, right?
With some lines going out.
The circular shape goes directly over the hole.
You press it down like that.
You want to get it as flat as possible.
And you want to make sure that it sticks.
Now those vents allow air or fluid to come out
of the chest cavity.
But when you breathe in, it sticks to you.
So it doesn't allow anything else to go in.
This is, in my opinion, would be the best kind
to use in this type of situation.
Now, if something went in this side,
what might have happened on the other side?
It might have come out.
So always make sure that you roll the person over.
Because you're probably going to have to apply one there too.
And keep in mind that it may not be
where you think it would be.
The exit may not be directly behind it.
Because it could have come in at an angle.
It could have hit something.
So make sure you check both sides, all right?
Okay.
Now, I said to save the packaging.
That addresses one of the big questions that came up.
People who got kits, who had, you know,
schools or whatever, give them kits.
Apparently they have like two, maybe four,
of these in there.
That's probably not enough.
If you were to look for videos on improvised chest seals,
they would say, find a piece of plastic and some tape.
This and this, and each one of these,
if you're doing entry and exit, okay?
This is going to give you, if you save the packaging,
you have an additional six seals.
So save the packaging and make sure you have some tape
in your kit.
Now, when you put these on,
there are different ways to do it.
Some say, you know, you could do three sides.
Some say just to do the corners.
Some say do all four sides.
I'm not going to tell you what I prefer,
because I really do want to stress the fact
that this isn't training.
This isn't enough.
There are options and there are ways to improvise this stuff
and there's a bunch of information on it online
and on YouTube from people who are far more qualified
than me.
Look to them.
This is really just more to pique curiosity
and answer a couple of questions.
This is how you become a superhero.
This is how you save lives.
This is how you can make the world a better place.
This training, if I haven't mentioned it,
Stop the Bleed is a really good one.
There's a whole bunch that are available.
Sign up.
It's normally not expensive.
Some of them even have online courses to take.
This isn't enough.
This is just a basic understanding of it.
You really do need more.
If you are going, one thing I do want to point out,
because as I say that, I know somebody isn't going to.
If you use something like this that isn't vented,
the seal that we put on,
the chest seal that we put on was vented.
That's what those lines coming out from the inner circle were.
If it's not vented, it has to be burped.
So that's something you need to look into.
You have to study more.
This isn't enough.
Please.
You're really talking about maybe eight hours of training.
And it will make the,
it could very well be the difference between life and death
for a whole lot of people.
So I hope that answers the questions
as far as the chest seals and the tourniquets.
These are the two big pieces of equipment
that could be real lifesavers in a situation like that.
I hope that answers the questions.
It's not enough.
You need more.
And you need to know where to apply the tourniquets.
And the mannequin is really the only way to show that,
because trying to describe it,
if I say above the wound,
depending on how people take that,
it could be taken incorrectly.
So please get more training.
If you are looking to beef up your kit in your class,
these are the items that you're probably going to need more of.
And gauze.
You can never have enough gauze.
Ever.
If you can find it, get combat gauze.
It has an agent on it to help stop bleeding.
But any kind of gauze is good.
But just beginning of the journey, not the end.
All right?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}