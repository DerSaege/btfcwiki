# All videos from June, 2022
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2022-06-30: Let's talk about people wanting more people.... (<a href="https://youtube.com/watch?v=lHGOO4dXUvg">watch</a> || <a href="/videos/2022/06/30/Lets_talk_about_people_wanting_more_people">transcript &amp; editable summary</a>)

Powerful figures express concerns over declining birth rates, but fail to address systemic issues causing people to choose not to have kids. It's time to take responsibility and make meaningful changes to the broken system.

</summary>

"The people who created the system are complaining about the effects of the system."
"Y'all built this. Y'all put people in this situation. You don't get to whine about it now. Fix it."
"The system that we have is broke. It's broke, and it's broke so bad, it is showing itself in a whole bunch of different ways."

### AI summary (High error rate! Edit errors on video page)

Various powerful figures are expressing concerns over declining birth rates and population collapse, including Elon Musk, politicians, and wealthy individuals.
Studies show reasons why people are choosing not to have kids, including medical issues, financial concerns, lack of a partner, and global issues like climate change.
The top reasons women give for terminating pregnancies are related to work, education, and financial constraints.
There is a disconnect between those who created the system and those now complaining about its effects.
Beau challenges the powerful individuals to take responsibility for the system they have created and make changes to address the issues causing declining birth rates.
The current system prioritizes economic growth and consumption over addressing fundamental issues like poverty and sustainability.
Beau calls out those with influence and power, including those in elected office, to stop perpetuating systems that keep people in poverty and contribute to declining birth rates.

Actions:

for policy makers, influential individuals,
Advocate for policies that support families and address the root causes leading people to choose not to have children (suggested).
Support initiatives that aim to reduce financial barriers to starting a family (implied).
Challenge elected officials to prioritize addressing poverty and promoting sustainable living conditions (implied).
</details>
<details>
<summary>
2022-06-30: Let's talk about an update on Ukraine and Russia.... (<a href="https://youtube.com/watch?v=y5Yy7SWkUSM">watch</a> || <a href="/videos/2022/06/30/Lets_talk_about_an_update_on_Ukraine_and_Russia">transcript &amp; editable summary</a>)

Beau provides an update on the protracted conflict in Ukraine, skeptical of a quick resolution due to economic pressures and strategic stalemate.

</summary>

"Lines are gonna move back and forth. This is going to continue for a while."
"It's protracted, it's ingrained, and it's gonna keep going for a while."
"Russia is voluntarily engaging in that same practice. They're keeping the numbers at levels that look good on paper, but they're doing it through capital controls and financial repression that will eventually show themselves."
"I am skeptical of that happening. It could, but in most scenarios in which that would occur, it would be pretty bad for the Ukrainian side."
"This is probably going to go on for a while now."

### AI summary (High error rate! Edit errors on video page)

Providing an update on the situation in Ukraine, which is unfolding as previously forecasted.
Describing how the conflict in Ukraine has devolved into a protracted situation.
Mentioning the commitment of the West, especially the United States, to support Ukraine with supplies, including a missile defense system.
Noting that Ukrainian civilians are being targeted by Russians hitting shopping malls with rockets.
Revealing that Ukrainian special forces are conducting operations inside Russian territory, opening the door for potential guerrilla engagement.
Pointing out that sanctions have led to Russia's first major default and economic challenges.
Criticizing Russia's economic data, suggesting it is manipulated to appear better than reality.
Mentioning unemployment statistics in Russia may be artificially low due to pressure on businesses to retain unnecessary employees.
Predicting a significant economic contraction for Russia based on outside expert analysis.
Comparing Russia's economic situation to the mishandling of public health issues, where delayed consequences become more severe over time.
Expressing skepticism about the war in Ukraine ending by the end of the year, as suggested by Zelensky.
Speculating that the conflict may continue until a breakthrough occurs either in the supply chain for Ukraine or the Russian economy.

Actions:

for analysts, policymakers, activists,
Monitor the situation in Ukraine closely and advocate for diplomatic resolutions (implied).
Stay informed about the conflict's economic implications and support initiatives that aid affected populations (implied).
</details>
<details>
<summary>
2022-06-30: Let's talk about Saul Alinsky, rules, and manipulation.... (<a href="https://youtube.com/watch?v=N-Rh6M-mt1I">watch</a> || <a href="/videos/2022/06/30/Lets_talk_about_Saul_Alinsky_rules_and_manipulation">transcript &amp; editable summary</a>)

Beau explains the misinterpretation and manipulation of Saul Alinsky's Rules for Radicals to create fear and propaganda.

</summary>

"It is presently happening at an alarming rate in the US."
"This isn't Alinsky's work. It's just not."
"In political jargon, a useful idiot is a derogatory term..."

### AI summary (High error rate! Edit errors on video page)

Explains about manipulation and Saul Alinsky, mentioning Alinsky's Rules for Radicals and its influence on current political control.
Mentions a newspaper clipping warning about "useful idiots" influenced by Alinsky's writings.
Lists the eight levels of control from Alinsky's book, including healthcare, poverty, debt, gun control, welfare, education, religion, and class warfare.
Notes the misinterpretation of Alinsky's work, which was actually about empowering the have-nots, opposite to the newspaper clipping's claims.
Talks about the demonization of Alinsky's work by attaching false power structures to it, creating fear and propaganda.
Describes how the term "useful idiot" is used in political jargon to refer to those unknowingly promoting a cause.
Concludes by discussing the spread of propaganda through fear-mongering tactics and the manipulation of information.

Actions:

for citizens, activists, educators,
Fact-check and challenge misinformation spread through fear tactics (suggested)
Educate others on the actual intentions of political terms and figures (exemplified)
</details>
<details>
<summary>
2022-06-29: Let's talk about what we can learn from Boebert and MTG.... (<a href="https://youtube.com/watch?v=c8daBlBEOtw">watch</a> || <a href="/videos/2022/06/29/Lets_talk_about_what_we_can_learn_from_Boebert_and_MTG">transcript &amp; editable summary</a>)

Be the change by considering running for office to shape the future of the country and move the Democratic Party left.

</summary>

"Maybe you should run."
"I think that you're probably better than Lauren Boebert or the space laser lady."
"I think you could be more effective."
"I think you could probably better the country."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

People feeling down about electoralism.
Two common responses: Democrats won't do anything or aren't progressive enough.
Beau poses a question: Why not run for office at local, state, or federal levels?
Challenges notions of lacking qualifications or establishment barriers.
Mentions individuals like Lauren Boebert and "space laser lady" who ran successfully.
Points out that they weren't establishment favorites but still won.
Encourages the idea of moving the Democratic Party left through personal involvement.
Suggests considering running for office as a form of civic engagement.
Emphasizes the potential for individuals to make a difference through running for office.
Beau encourages the audience to think about the impact they could have by running for office.
Stresses that running for office is a way to actively shape the future of the country.
Acknowledges that not everyone may be suited for electoral politics but urges those who are to take action.
Concludes with a positive encouragement for individuals to give running for office a try.

Actions:

for potential candidates for office.,
Run for office at local, state, or federal levels (suggested).
Engage in civic participation by considering a political candidacy (implied).
</details>
<details>
<summary>
2022-06-29: Let's talk about Trump's denials from the committee.... (<a href="https://youtube.com/watch?v=fS1soNwtQ-M">watch</a> || <a href="/videos/2022/06/29/Lets_talk_about_Trump_s_denials_from_the_committee">transcript &amp; editable summary</a>)

Beau analyzes Trump's denials in response to serious allegations, focusing on image over substance in a narrow-scope committee hearing.

</summary>

"I figured there might be a denial about the whole coup thing, something related to that."
"He focused on crowd size and temper tantrum allegations."
"The overall theme of this hearing is way beyond him throwing food."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's denials in response to testimony presented in a surprise committee hearing.
Trump denies knowing Cassidy Hutchinson and questions who was running the White House if he didn't.
Raises skepticism about Trump's denial of not noticing an attractive woman working down the hall.
Expresses doubts about Trump's denial of never complaining about crowd size.
Comments on Trump's denial regarding making room for people with guns to watch his speech.
Notes Trump's denial of trying to grab the steering wheel of the White House limousine.
Mentions Trump's denial of throwing food as another false allegation.
Expresses surprise at the lack of denial regarding more significant allegations like the coup attempt.
Points out Trump's focus on denying allegations related to his image rather than substance.
Comments on the narrow scope of the committee hearing and the compelling case being built against Trump.

Actions:

for political observers,
Stay informed about committee hearings and testimonies (suggested)
Engage with the political process and hold leaders accountable (implied)
</details>
<details>
<summary>
2022-06-29: Let's talk about Elmo and a phrase I never thought I say.... (<a href="https://youtube.com/watch?v=yNjSjBzcs9I">watch</a> || <a href="/videos/2022/06/29/Lets_talk_about_Elmo_and_a_phrase_I_never_thought_I_say">transcript &amp; editable summary</a>)

Elmo's vaccination sparks outrage among political commentators, distracting from vital issues like polio vaccination gaps.

</summary>

"Elmo got vaccinated, therefore Sesame Street has gone woke."
"Those doctors don't know what they're talking about, right?"
"Vaccination gaps are causing an issue with polio."

### AI summary (High error rate! Edit errors on video page)

Elmo from Sesame Street got vaccinated, leading political commentators to spark outrage and claim Sesame Street has gone "woke."
Some are pushing back against Elmo getting vaccinated, fearing it might encourage children to get vaccinated.
These critics, who are political commentators, failed to see through Trump for years and supported his actions, including a coup attempt.
Despite claiming politics as their expertise, they now want to influence opinions on vaccination.
Polio cases, which had decreased significantly, are now on the rise due to vaccination gaps.
A virologist emphasized the importance of ensuring everyone is vaccinated for polio, even in places like London.
Beau urges people to focus on polio vaccination alongside debates about Elmo and trusting political commentators over doctors.
The anti-science and anti-education stance adopted by many is contributing to global health risks.
Beau stresses the importance of getting vaccinated for polio, advocating against misinformation spread by political commentators.
He questions the credibility and motives of those who prioritize outrage over trivial issues while neglecting serious health concerns like polio.

Actions:

for parents, educators, advocates,
Ensure everyone is vaccinated for polio (suggested)
Focus on promoting accurate health information and vaccination efforts in communities (implied)
</details>
<details>
<summary>
2022-06-28: Let's talk about rumors and the surprise hearing today.... (<a href="https://youtube.com/watch?v=SwXLtjSLzpw">watch</a> || <a href="/videos/2022/06/28/Lets_talk_about_rumors_and_the_surprise_hearing_today">transcript &amp; editable summary</a>)

A surprise hearing with secret witnesses and potential explosive revelations hints at early election meddling plans, setting the stage for ongoing dramatic developments and compelling television.

</summary>

"They're trying to tell a very sprawling story with new developments all the time."
"In lack of a, for lack of a better word, it makes for good TV."
"Today we will find out who has been naughty and nice."
"I just realized I didn't change shirts between the last video and this one."
"It looks like it's going to be fun."

### AI summary (High error rate! Edit errors on video page)

A surprise hearing was called by the committee with secret content and witnesses, causing a flurry of rumors.
Leading contender for a surprise witness is Cassidy Hutchinson, a former aide to Mark Meadows.
Cassidy Hutchinson may have inside information due to her close proximity to the inner circle of the White House.
The committee has obtained footage from a documentarian suggesting plans to alter the election outcome were made early on.
There are hints that the footage contradicts Ivanka Trump's testimony to the committee.
The committee faces challenges in storytelling with ongoing developments, such as Eastman having his phone taken by the feds recently.
Beau anticipates more surprise hearings from the committee to keep the public engaged.
The unfolding drama of the hearings makes for compelling television.
Beau humorously notes his unchanged shirt between videos and teases about finding out who has been "naughty or nice" in the hearing.

Actions:

for journalists, political analysts,
Stay updated on the unfolding developments in the hearings (implied)
</details>
<details>
<summary>
2022-06-28: Let's talk about a million new republican voters.... (<a href="https://youtube.com/watch?v=YEqWZB4qBoE">watch</a> || <a href="/videos/2022/06/28/Lets_talk_about_a_million_new_republican_voters">transcript &amp; editable summary</a>)

A million party switches spark concern, but Beau sees organized political strategy, not mass defection.

</summary>

"This isn't something I'm concerned about."
"This isn't an organic thing because people are mad."
"This is political shenanigans at work."

### AI summary (High error rate! Edit errors on video page)

A report claimed a million people switched from Democrat to Republican, causing concern.
The report mentioned a shift from Denver to Atlanta, which could also be described as Colorado to Georgia.
In reality, the switch was driven by organized efforts to vote in Republican primaries against Trumpist candidates.
In Georgia, nearly 40,000 people voted in Republican primaries to support candidates opposing Trump-backed ones.
The total number of party switches was around 400,000 votes, with Georgia accounting for a significant portion.
Beau believes the numbers are accurate but lacking in explanation about why people switched parties.
One person mentioned they began moving away from their original party affiliation five or six years ago through the Libertarian Party.
Beau doesn't see the party switches as a major concern, suggesting it was organized to remove Trumpist elements.
He notes that recent events like rights stripping and unpopular Supreme Court decisions happened after many party switches occurred.
Beau views the situation as political maneuvering rather than a mass exodus from the Democratic party.

Actions:

for political observers,
Join or support efforts to address political maneuvering and strategizing within parties (implied)
</details>
<details>
<summary>
2022-06-27: Let's talk about scientists trying to get your attention.... (<a href="https://youtube.com/watch?v=FX29FR-1m3s">watch</a> || <a href="/videos/2022/06/27/Lets_talk_about_scientists_trying_to_get_your_attention">transcript &amp; editable summary</a>)

The Union of Concerned Scientists coins "danger season" to draw attention to the critical impact of climate change, criticizing policymakers' inaction.

</summary>

"Danger season."
"They're trying to get the American people, in particular, to pay attention."
"Elements of the scientific community are trying to get attention and draw attention to world-changing events through methods like this."
"It's a sad commentary that elements of the scientific community are trying to get attention and draw attention to world-changing events through methods like this."
"When you see danger season in headlines, that's what it's referencing."

### AI summary (High error rate! Edit errors on video page)

The Union of Concerned Scientists coined the term "danger season" for the period from May to October, marked by extreme heat, hurricanes, wildfires, and drought.
This term aims to draw attention to the challenges faced during the summer, largely due to climate change.
There's a growing concern that people, especially policymakers, aren't taking the situation seriously enough.
Scientists are resorting to attention-grabbing methods like "danger season" to make people, particularly in the U.S., pay attention to climate-related issues.
The use of gimmicks like this indicates a sad reliance on marketing tactics to communicate critical scientific data.
Scientists face pushback from politicians who prioritize business interests over climate change warnings.
"Danger season" headlines not only serve as a warning for the upcoming months but also criticize policymakers' lack of action towards necessary changes.
Policymakers are failing to mobilize the significant changes needed to combat the escalating climate crisis.
Scientists are forced to use attention-grabbing strategies due to the resistance they face when presenting straightforward data and scenarios.
This situation underscores the need for urgent action and a massive shift to address and mitigate the impacts of climate change.

Actions:

for climate advocates, concerned citizens,
Spread awareness about climate change impacts and the concept of "danger season" (suggested)
Advocate for policy changes that prioritize environmental preservation (implied)
</details>
<details>
<summary>
2022-06-27: Let's talk about pragmatism, unpacking the court, and dealbreakers.... (<a href="https://youtube.com/watch?v=RhCd_8EuH44">watch</a> || <a href="/videos/2022/06/27/Lets_talk_about_pragmatism_unpacking_the_court_and_dealbreakers">transcript &amp; editable summary</a>)

Exploring the pragmatic aspects of expanding the court and the Democratic Party's potential dilemma post-midterms on restoring rights and traditions.

</summary>

"The Democratic Party is about to have that same kind of situation if they pick up seats in that Senate."
"If the Democratic Party gains the seats in the Senate it needs and then doesn't act, there's an issue."
"The tradition is worthless as people's rights get stripped away."
"That's the tradition that matters, more so than some rule in the Senate."
"Those who lost their rights, they don't have the luxury of caring about your traditions."

### AI summary (High error rate! Edit errors on video page)

Exploring the pragmatic aspects of unpacking the court and adding justices.
Democratic Party's current ability to expand the court is limited due to Senate control.
Energized Democratic base heading into midterms due to unintentional Republican actions.
Possibility of Democratic Party gaining control in the midterms and subsequently expanding the court.
Comparing Democratic Party's potential dilemma to that of Trump supporters and racism.
Beau views civility politics, unwillingness to restore rights, and inherent misogyny as deal-breakers.
Emphasizing the importance of including more people under the blanket of freedom.
Tradition and high ideals mean nothing if they result in stripping away people's rights.
Democratic Party may face criticism for not acting post-midterms if they gain the necessary Senate seats.
Beau stresses the importance of acting in alignment with being the land of the free and home of the brave.

Actions:

for democrats, political activists,
Support Democratic candidates in the upcoming midterms to potentially shift Senate control (implied).
Advocate for the expansion of the court to restore rights and uphold traditions (implied).
</details>
<details>
<summary>
2022-06-27: Let's talk about how SCOTUS helped the Democratic Party.... (<a href="https://youtube.com/watch?v=Tz_GifQJ3No">watch</a> || <a href="/videos/2022/06/27/Lets_talk_about_how_SCOTUS_helped_the_Democratic_Party">transcript &amp; editable summary</a>)

Recent Supreme Court decision benefits Democratic Party long term by allowing Republican policies to harm Americans in their own states, leading to visible economic differences and potential voter shift.

</summary>

"The Democratic Party ends up punishing Republican voters and turning them against the Republican Party."
"Long term, as far as the institution of the Democratic Party, this is good."

### AI summary (High error rate! Edit errors on video page)

Recent Supreme Court decision favors Democratic Party long term, unrelated to fundraising.
Republican Party votes against American people to blame Democratic Party when in power.
Democrats allow Republican policies to show themselves rather than sabotage.
Decision reverts power back to states, benefiting Democratic Party long term but harming Americans in Republican-dominated states.
More babies due to bans lead to decreased tax revenue and increased need for social safety nets and schools.
Increase in poverty, income inequality, and crime predicted in Republican-dominated states.
Brain drain expected as innovators leave states with old ideas.
Republican-dominated states may struggle with economic stability compared to Democratic-dominated states.
Long-term effects will be pronounced and visible.
Democratic Party benefits as Republican Party alienates own voters.

Actions:

for political analysts, democratic party members,
Monitor the effects of the Supreme Court decision on states with bans (implied)
Stay informed about economic stability and viability in Republican-dominated states (implied)
</details>
<details>
<summary>
2022-06-26: Let's talk about the SCOTUS NY carry ruling.... (<a href="https://youtube.com/watch?v=wCH89SE-BIM">watch</a> || <a href="/videos/2022/06/26/Lets_talk_about_the_SCOTUS_NY_carry_ruling">transcript &amp; editable summary</a>)

Beau clarifies the Supreme Court's ruling on carrying things, debunking permitless carry misconceptions and explaining the shift from may issue to shall issue terms for concealed carry licenses, hinting at broader implications for gun laws.

</summary>

"Most states have shall issue. This applies to, I want to say, five states."
"The states that have may issue can still regulate concealed licenses. They just have to do it in a more objective way."
"Their interpretation of bear leads me to believe that things like rifle racks like back in the day in pickup trucks, those may be coming back."
"It's not going to be the outcome from this."

### AI summary (High error rate! Edit errors on video page)

Explains the Supreme Court's ruling on carrying things, clarifying misconceptions around permitless carry.
Draws a comparison between may issue and shall issue terms using the example of driver's licenses.
Details that states with may-issue licenses for concealed carry will have to rewrite their laws to make them shall-issue, with objective and quantified requirements.
Mentions that the ruling doesn't mean anyone can walk in and get a license; requirements must still be met.
Suggests that the ruling impacts only a few states, not a significant number.
Indicates that the court's interpretation of "keep and bear" is broader than previous courts, potentially affecting other gun laws at state or local levels.
Notes that bans on high capacity magazines may not survive this court's challenges.
Speculates on potential broadening interpretations of "bear" by the court, hinting at rifle racks returning but not causing mass arming in New York.
Concludes by stating that the ruling isn't as groundbreaking as some portray it, and states with may-issue laws can still regulate licenses objectively.

Actions:

for legal enthusiasts, gun policy advocates,
Contact your state representatives to stay informed about any changes in concealed carry licensing laws (implied).
Join local gun policy advocacy groups to understand and potentially influence future developments in gun legislation (implied).
</details>
<details>
<summary>
2022-06-26: Let's talk about SCOTUS and Miranda.... (<a href="https://youtube.com/watch?v=pA3vXnYMMzU">watch</a> || <a href="/videos/2022/06/26/Lets_talk_about_SCOTUS_and_Miranda">transcript &amp; editable summary</a>)

The Supreme Court ruling removed the civil remedy for police not reading Miranda rights, reinforcing the universal advice to stay silent and ask for a lawyer when dealing with law enforcement.

</summary>

"Do not talk to the cops. Ask for a lawyer, then shut up."
"You still have a right to remain silent. Use it. Do not talk to the cops."
"Shut up and ask for a lawyer. That's what they will all tell you."

### AI summary (High error rate! Edit errors on video page)

Explains the recent Supreme Court ruling related to Miranda rights, clarifying that it removed the civil remedy for police failing to read the Miranda rights.
Quotes law professors and criminal defense lawyers advising people to remain silent and ask for a lawyer when dealing with the police, regardless of Miranda rights.
Emphasizes the importance of not talking to the police, even if Miranda rights are completely overruled in the future.
Notes the significance of the ruling that limits suing cops who ignore Miranda rights, urging people to understand the advice of remaining silent and asking for a lawyer for protection.
Conveys the unanimous advice from lawyers to always remain silent and request a lawyer when interacting with law enforcement, as it is the only thing lawyers universally agree on.

Actions:

for everyday citizens,
Memorize and internalize the advice to remain silent and ask for a lawyer when interacting with law enforcement (implied).
</details>
<details>
<summary>
2022-06-26: Let's talk about Biden banning mines.... (<a href="https://youtube.com/watch?v=ntSKphia-WQ">watch</a> || <a href="/videos/2022/06/26/Lets_talk_about_Biden_banning_mines">transcript &amp; editable summary</a>)

Beau explains the shifting foreign policies from Obama to Trump to Biden, focusing on anti-personnel mines and the modernization of the military.

</summary>

"It is modernizing it."
"It's not weakening the military."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains the evolution of foreign policy from President Obama to Trump to Biden and the impact of inconsistent policies on the nation.
Details how the Biden administration plans to lead the U.S. to comply with the Ottawa Convention of 1997, which banned anti-personnel mines.
Contrasts President Obama's restrictive policy on anti-personnel mines with Trump's decision to relax it.
Mentions the Biden administration's alignment with Obama's stance, heavily restricting the use of anti-personnel mines except on the Korean Peninsula.
Notes that the U.S. does not have fields on the Korean Peninsula but has committed to defending South Korea.
States that the Biden administration will supply and possibly deploy anti-personnel mines to South Korea while destroying any unused mines from the U.S. stockpile.
Acknowledges potential pushback against restricting tools for troops and the evolving technology of anti-personnel mines that go inert after a set period.
Emphasizes that the U.S. military doctrine focuses on mobility, making anti-personnel mines largely obsolete.
Supports the idea of moving towards compliance with international treaties like the Ottawa Convention to modernize the military and improve effectiveness.
Encourages further steps by the Biden administration to potentially address cluster munitions compliance in the future.

Actions:

for policy analysts,
Advocate for compliance with international treaties like the Ottawa Convention (suggested).
Support modernization efforts within the military (suggested).
</details>
<details>
<summary>
2022-06-25: Let's talk about what they're going after next.... (<a href="https://youtube.com/watch?v=FEmw7k1MPIA">watch</a> || <a href="/videos/2022/06/25/Lets_talk_about_what_they_re_going_after_next">transcript &amp; editable summary</a>)

Beau explains why upcoming court decisions on key cases signal a threat to established freedoms, cautioning against overlooking their intentions.

</summary>

"This isn't me like making this stuff up. It's in the ruling."
"They're telling you, this is what we're coming for next."
"When somebody tells you who they are, believe them."
"They're demonstrating it very clearly where they want to go from here."
"They said it, not me."

### AI summary (High error rate! Edit errors on video page)

Mentioned previous videos about upcoming court decisions.
Received questions about being alarmist.
Explains why certain cases will be reviewed.
References cases like Griswold, Lawrence, and Obergefell.
States the intentions of the court as written in rulings.
Points out the impact on freedoms like marriage equality and birth control.
Emphasizes that it's not alarmist or fear-mongering, it's based on documented intentions.
Addresses the election of an authoritarian under Trump and subsequent Supreme Court appointments.
Urges not to overlook the court's objectives.
Warns about the potential consequences of ignoring the court's stated direction.

Actions:

for activists, voters, concerned citizens,
Challenge potential threats by mobilizing community advocacy efforts (suggested)
Stay informed about legal developments and implications for civil liberties (implied)
</details>
<details>
<summary>
2022-06-25: Let's talk about the pardon requests.... (<a href="https://youtube.com/watch?v=QiZG8SLaxKE">watch</a> || <a href="/videos/2022/06/25/Lets_talk_about_the_pardon_requests">transcript &amp; editable summary</a>)

Beau sheds light on the sensationalized pardons discussed at a recent hearing, urging attention towards the broader context of the pressure campaign pursued by the committee.

</summary>

"The pressure campaign, that's where the real goods are as far as what the committee is seeking to do."
"The pardons, it's probably the most interesting, it is the most entertaining, no doubt."
"Denials and dismissals, they need to be remembered because I have a feeling that all of these statements will be refuted in pretty short order."

### AI summary (High error rate! Edit errors on video page)

Focuses on the pardons discussed in a recent hearing regarding requests made to Trump by specific individuals like Andy Biggs, Mo Brooks, Matt Gates, Gohmert, Scott Perry, and the Space Laser Lady from Georgia.
Raises attention to the email from Brooks requesting pardons for himself and Gates, with the additional suggestion to pardon all congressmen and senators who voted against electoral college vote submissions of Arizona and Pennsylvania.
Suggests two interpretations of Brooks' suggestion: either believing the act warrants a pardon or providing cover for their own pardons.
Points out that individuals seeking pardons have been denying, deflecting, or downplaying the committee's claims, implying a tactical error.
Warns that denials and dismissals may be refuted soon by the committee, as those who requested pardons are unlikely to have forgotten.
Emphasizes that while the pardons are entertaining and sensationalized, the real focus should be on the pressure campaign and the committee's objectives.
Acknowledges the attention-grabbing nature of the pardons but stresses the importance of understanding the broader context and objectives of the committee.

Actions:

for politically engaged citizens,
Contact your representatives to express your concerns about the handling of pardons and pressure campaigns (suggested).
</details>
<details>
<summary>
2022-06-25: Let's talk about the objection to unpacking the court.... (<a href="https://youtube.com/watch?v=U5v56Rxc77Q">watch</a> || <a href="/videos/2022/06/25/Lets_talk_about_the_objection_to_unpacking_the_court">transcript &amp; editable summary</a>)

Beau addresses objections to court expansion, advocating for Democratic Party as a temporary safeguard against authoritarianism and urging continued engagement in the ongoing political struggle.

</summary>

"It's a fight, and it's going to be a long one because the United States has slid very, very far down that slope into authoritarianism."
"Don't throw your hands up, though. There are a whole lot of people who are counting on us."
"You need to wrap your minds around that right now."
"They care about power for power's sake."
"That slide towards authoritarianism [...] it didn't stop the day he left office. We're still there."

### AI summary (High error rate! Edit errors on video page)

Exploring the idea of expanding and unpacking the court, addressing objections.
Acknowledging the possibility of the Republican Party expanding the court if given the chance.
Emphasizing the fight against authoritarianism and the role of the Democratic Party in slowing the descent.
Suggesting that expanding the court could buy some time in the worst-case scenario.
Urging people to stay engaged in the political process despite challenges.
Warning against expecting fairness from those in power, who prioritize power over constituents.
Staying vigilant in the fight against authoritarianism, even after Trump's presidency.
Encouraging persistence and engagement in the face of setbacks, acknowledging the long-term nature of the struggle.

Actions:

for political activists, democratic supporters,
Stay engaged in the political process (implied)
Advocate for court expansion as a temporary measure to slow authoritarian descent (implied)
</details>
<details>
<summary>
2022-06-24: Let's talk about unpacking the Supreme Court decision.... (<a href="https://youtube.com/watch?v=JMrXA0oXy9g">watch</a> || <a href="/videos/2022/06/24/Lets_talk_about_unpacking_the_Supreme_Court_decision">transcript &amp; editable summary</a>)

Supreme Court decision will devastate states, targeting poor individuals, necessitating the addition of justices to dilute extreme views and prevent worsening situations.

</summary>

"It's time to unpack the court. That's your solution. It's really your only solution."
"The solution is to add more justices."
"Their only solution is to unpack the court, to get rid of these extremists that were put on there, right?"
"The outcomes, the impacts, the effects of those decisions on your state, they're not good."
"Adding more justices is the solution, to dilute the votes of these justices."

### AI summary (High error rate! Edit errors on video page)

Supreme Court decision will fundamentally alter many states, reverting everything back to the state level.
Republican states with laws on family planning will see devastating effects like increased teen pregnancy and poverty.
Impacts will include loss of mothers, child marriage, and malicious prosecutions.
Poor individuals won't have means to escape these laws by moving.
Wealthy individuals will find ways to circumvent these laws.
Predicts emergence of a discrete family planning industry catering to those seeking to avoid state restrictions.
This decision targets poor people, not impacting the wealthy.
Calls for unpacking the court by adding more justices as the only solution.
Argues that current justices were appointed under false pretenses and need to be diluted.
Lack of faith in the Supreme Court demands a change in its composition.
Adding more justices is the way to address issues like marriage equality and other imminent threats.
Diluting the votes of current justices is necessary to prevent worsening situations in the future.

Actions:

for voters, activists, lawmakers,
Advocate for adding more justices to the Supreme Court (suggested)
Support efforts to unpack the court and dilute the votes of current justices (suggested)
</details>
<details>
<summary>
2022-06-24: Let's talk about polar bear, good news, and bad news.... (<a href="https://youtube.com/watch?v=953CcvaIWq0">watch</a> || <a href="/videos/2022/06/24/Lets_talk_about_polar_bear_good_news_and_bad_news">transcript &amp; editable summary</a>)

Beau delves into the unique case of Greenland polar bears to caution against viewing their survival as a universal solution for polar bears facing climate change, stressing the need for comprehensive action to mitigate the impacts.

</summary>

"These polar bears are adapted to living in an environment that looks like the future."
"Greenland is unique. We project large declines of polar bears across the Arctic and this study does not change that very important message."
"Not that polar bears everywhere are going to be okay. Just that this particular group might be able to make it through it."

### AI summary (High error rate! Edit errors on video page)

Receives a message about polar bears in Greenland surviving in a location they're not supposed to be.
Conducts quick research to understand the situation and implications of the polar bears' presence in Greenland.
Discovers that the Greenland polar bear population is genetically diverse and geographically distinct from others.
Notes that these bears are smaller, produce fewer offspring, and can survive with 100 days of ice sheets compared to 180 days needed elsewhere for hunting seals.
Acknowledges the potential adaptability of these bears to climate change due to their unique circumstances.
Quotes a researcher who tracked the Greenland bears for nine years, pointing out that the situation is unique and not indicative of a solution for polar bears across the Arctic.
Expresses skepticism towards viewing the Greenland polar bears as a symbol of hope for the entire species, stressing the need for comprehensive climate change action.
Emphasizes the importance of developing a comprehensive plan and taking significant steps to mitigate climate change effects.
Concludes that until concrete actions are taken, positive news regarding climate change will likely be limited and not without challenges.

Actions:

for climate change activists,
Develop and support comprehensive plans to mitigate climate change impacts (implied)
Advocate for urgent climate action and policy changes (implied)
</details>
<details>
<summary>
2022-06-24: Let's talk about Day 5 of the hearings.... (<a href="https://youtube.com/watch?v=-j4P8-NBUQs">watch</a> || <a href="/videos/2022/06/24/Lets_talk_about_Day_5_of_the_hearings">transcript &amp; editable summary</a>)

Day five of hearings reveals Trump's pressure campaign involving DOJ; law enforcement intensifies investigations, potentially altering election outcomes.

</summary>

"The committee is laying out a case. I mean they're laying out what could be an indictment."
"There's going to be a lot of events that might alter the outcome of elections."
"The Department of Justice appears very interested in the fake electors side of things."
"Law enforcement activity is picking up. It's gaining momentum."
"There's certainly a hard time overcoming this."

### AI summary (High error rate! Edit errors on video page)

Day five of the hearings focused on Trump's pressure campaign to involve the Department of Justice in his baseless claims.
Testimony revealed Trump's consideration of firing the acting Attorney General to install Jeffrey Clark in an effort to overturn the election.
High-profile resignations were threatened if the DOJ was misused in this manner, leading Trump to back off.
The committee's testimony outlines a potentially indictable offense, showcasing a strong case against Trump and his associates.
Law enforcement activities, including searches and subpoenas, are intensifying beyond the committee's actions.
Federal law enforcement, not just the committee, is actively pursuing investigations into the fake elector scheme.
Governor Kemp is expected to provide testimony regarding Trump's election influence attempts in Georgia.
The Department of Justice appears keen on investigating fake electors and is aggressively pursuing leads.
Events unfolding in the lead-up to the midterms may significantly impact election outcomes as more information comes to light.
A list of pardon requests is set to be released, adding to the ongoing developments in the investigation.

Actions:

for investigators, activists, voters,
Contact local representatives to advocate for transparent investigation into election influence (implied).
Join community groups discussing election integrity and involvement (exemplified).
Attend public hearings related to election interference investigations (implied).
</details>
<details>
<summary>
2022-06-23: Let's talk about pals at DARPA being too quiet.... (<a href="https://youtube.com/watch?v=j73fYkHl-4k">watch</a> || <a href="/videos/2022/06/23/Lets_talk_about_pals_at_DARPA_being_too_quiet">transcript &amp; editable summary</a>)

Beau clarifies the truth behind using fish to track submarines and delves into DARPA's program exploring aquatic life sounds for national security tracking, hinting at futuristic scientific developments from current speculations.

</summary>

"That's silly. That's not true. But also yes it kind of is true."
"What if I told you there's some truth to that statement? And it's not all just a movie trope."
"It's quiet. Too quiet."
"Normally when you're talking about DARPA, if you're not familiar with this agency, just understand if there was an agency that was going to like reverse engineer some crashed alien spaceship, it would be them."
"So anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Addressing the idea of the Navy using fish to track submarines, which he clarifies as both untrue and true in a strange way.
Explaining the concept of ambient noise and its significance in detecting potential threats.
Describing DARPA's program called PALS (Persistent Aquatic Living Sensors) aimed at utilizing aquatic life sounds to track opposition naval forces.
Detailing the process of filtering out organic ambient noise underwater and the potential use of this noise for tracking purposes.
Mentioning the use of a computer to analyze biological entities' reactions to submarines, large ships, or underwater drones.
Speculating on the likelihood of DARPA having already completed the program, given their typical operational procedures.
Comparing DARPA to an agency capable of reverse engineering crashed alien spaceships and hinting at their advanced technologies.
Suggesting that the information publicly shared about the PALS program may soon transition from speculation to established scientific fact.

Actions:

for science enthusiasts, national security analysts.,
Stay updated on DARPA's advancements in tracking technologies (implied).
</details>
<details>
<summary>
2022-06-23: Let's talk about a judge in Kentucky and name recognition.... (<a href="https://youtube.com/watch?v=1Jh-FqkMqCI">watch</a> || <a href="/videos/2022/06/23/Lets_talk_about_a_judge_in_Kentucky_and_name_recognition">transcript &amp; editable summary</a>)

A judge's removal for abuse of power in Kentucky sparks concerns about future election prospects based on name recognition, shedding light on the pivotal role of incumbency and familiarity in politics.

</summary>

"Repeated and systemic abuse of power."
"If she wins, that's a pretty clear sign that name recognition is incredibly important."
"It is definitely going to end up being a unique legal situation up there that will eventually probably make national news."

### AI summary (High error rate! Edit errors on video page)

Judge Julie Halls Gordon was removed from her judgeship in Kentucky for repeated and systemic abuse of power.
Despite being removed two months ago, she will still be on the ballot in Davis County, Kentucky.
The only ways to bar her from future office in Kentucky are through an act of the General Assembly or losing her law license.
Concerns exist that she might get elected solely based on name recognition, as anyone who files will be on the ballot.
Name recognition plays a critical role in elections, with incumbents often benefiting from it.
The debate about the importance of name recognition in elections is ongoing, with no doubt about its significance.
If Judge Gordon wins, it could signify the paramount importance of name recognition in elections.
The potential scenario of her being elected to a new judgeship raises questions about legal implications and removal from office.
Her situation might lead to a unique legal case that could attract national attention.

Actions:

for voters, legal experts,
Monitor local elections and candidates for positions of power (implied).
</details>
<details>
<summary>
2022-06-23: Let's talk about a TV show, survival, and generational conflicts.... (<a href="https://youtube.com/watch?v=LE_LDs8A_5k">watch</a> || <a href="/videos/2022/06/23/Lets_talk_about_a_TV_show_survival_and_generational_conflicts">transcript &amp; editable summary</a>)

Beau questions the mockery in teaching survival skills and advocates for progress and self-reliance in shaping future generations.

</summary>

"Most complaints about the younger generation are really just admissions of failure for the older generation."
"Yeah, I want everybody to have basic survival skills for an emergency, but it shouldn't be your life."
"Society is supposed to progress."
"Each generation feels the younger generation is going to be the one that brings down the world."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A TV show about putting millennials through survival school, but they are actually 20-21-year-old kids, not millennials, tapping into generational nonsense.
The show focuses on poking fun at a group of people rather than teaching actual survival skills.
Beau questions why there is a push for younger generations to have the same skillset as the older generation, seeing it as a lack of progress.
He shares a personal anecdote about a friend feeling like a failure for not teaching his son survival skills like hunting, only doing it once with him.
Beau believes in progress and wants his kids to have a better skill set than he does, not the same.
He references a quote about studying different subjects for progress and questions why society is stuck in perpetuating macho nonsense.
Beau believes in basic survival skills but not at the expense of mockery, as it hinders true learning and self-reliance.
He concludes by reflecting on how each generation believes the younger one will bring about downfall but is actually shaped by the previous generation.

Actions:

for educators, parents, society,
Foster learning environments that prioritize teaching practical skills without mockery (implied).
Encourage progress and self-reliance in education and skill development (implied).
</details>
<details>
<summary>
2022-06-22: Let's talk about what happens if Trump is acquitted.... (<a href="https://youtube.com/watch?v=DMgJF95If6U">watch</a> || <a href="/videos/2022/06/22/Lets_talk_about_what_happens_if_Trump_is_acquitted">transcript &amp; editable summary</a>)

Beau stresses the importance of pursuing justice over political gains in the case of Trump's potential indictment and acquittal, cautioning against the dangers of ignoring accountability for short-term benefits.

</summary>

"Decisions like this are supposed to be about pursuit of justice."
"Stop thinking solely about the next election."
"It is far more dangerous to have a clear indication that behavior like this will go completely unpunished."

### AI summary (High error rate! Edit errors on video page)

Talks about the possibility of Trump being indicted and then acquitted, with changing rhetoric from "it doesn't matter" to "even if you indict him, you're not going to convict him."
Emphasizes that decisions should be about justice, not the next election, and questions the logic of not holding Trump accountable.
Points out how Republicans are trying to out-Trump Trump in states like Texas and Missouri, raising concerns for the 2024 election.
Urges to view the situation as an existential threat to the republic rather than through a partisan lens.
Warns against advocating to ignore accountability for short-term political gains, stressing the importance of pursuing justice.

Actions:

for politically-engaged citizens,
Advocate for accountability in politics (implied)
Encourage a focus on justice over short-term political gains (implied)
</details>
<details>
<summary>
2022-06-22: Let's talk about important news on the Colorado River.... (<a href="https://youtube.com/watch?v=3kvVmXdnSb4">watch</a> || <a href="/videos/2022/06/22/Lets_talk_about_important_news_on_the_Colorado_River">transcript &amp; editable summary</a>)

The Bureau of Reclamation has set a 60-day deadline for southwestern states to cut Colorado River water usage by 2 to 4 million acre feet, signaling the urgent need for action in response to climate change impacts on water supplies.

</summary>

"We are facing the growing reality that water supplies for agriculture, fisheries, ecosystems, industry, and cities are no longer stable due to climate change."
"Climate issues aren't waiting. They're here."

### AI summary (High error rate! Edit errors on video page)

The Bureau of Reclamation has given states in the southwestern United States 60 days to come up with a plan to cut water usage from the Colorado River by 2 to 4 million acre feet.
An acre foot is equivalent to almost 326,000 gallons of water, which is enough for 10 people for a year.
Cutting 2 to 4 million acre feet of water equates to impacting the water usage of 20 to 40 million people.
States impacted by this water usage cut include Colorado, New Mexico, Utah, Arizona, California, and Nevada.
Lake Powell might stop producing electricity by early 2024 if water levels continue to drop.
Assistant Secretary of the U.S. Department of Interior for Water and Science warns about unstable water supplies for agriculture, fisheries, ecosystems, industry, and cities due to climate change.
States must come up with a plan to significantly reduce water usage as a response to climate change.
Failure to create a plan will result in the federal government imposing one.
Climate issues are here and action needs to be taken urgently.
The situation requires immediate attention and action.

Actions:

for states, environmentalists, activists,
Collaborate with local communities and organizations to develop and implement water conservation strategies (suggested).
Support and participate in initiatives that aim to reduce water usage in your area (exemplified).
</details>
<details>
<summary>
2022-06-22: Let's talk about Day 4 of the hearings and security.... (<a href="https://youtube.com/watch?v=qU3YDcfCB5Y">watch</a> || <a href="/videos/2022/06/22/Lets_talk_about_Day_4_of_the_hearings_and_security">transcript &amp; editable summary</a>)

Beau provides a structured recap of the hearings, revealing hard evidence of Trump and crew's involvement and the lack of internal security measures.

</summary>

"They're following that template very well."
"They're coming from Republicans, in some cases, lifelong Republicans."
"Their carelessness, their lackadaisical attitude when it comes to basic tradecraft."

### AI summary (High error rate! Edit errors on video page)

Recap of day four of hearings regarding events on the 6th and leading up to it.
Committee following a structured approach: tell them what you're going to tell them, tell them, tell them what you told them.
Evidence presented that Trump and crew knew election claims were false and actions violated federal law.
Evidence showing Trump and crew behind state-level push for alternate electors on the 6th.
Surprising aspect: Republicans pointing fingers at Trump and crew, not Democrats.
Remarkable information coming from Republicans, not Democrats, impacting those who need to hear it.
Lack of internal security procedures concerning; many inappropriate direct communications took place.
Expect more surprising uncovered communications showing carelessness and lack of security precautions.
Committee doing a good job presenting hard evidence to persuade people.
Need for someone to tie together the presented information into a compelling narrative for the American people.

Actions:

for committee members, concerned citizens.,
Follow the hearings closely to stay informed and engaged (implied).
Share information and evidence from the hearings with others to raise awareness (implied).
Advocate for accountability and transparency in political processes (implied).
</details>
<details>
<summary>
2022-06-21: Let's talk about that ad in Missouri and a conflation of terms.... (<a href="https://youtube.com/watch?v=4v_ap9tH5-E">watch</a> || <a href="/videos/2022/06/21/Lets_talk_about_that_ad_in_Missouri_and_a_conflation_of_terms">transcript &amp; editable summary</a>)

Beau examines a controversial political ad for a Missouri Senate candidate, revealing dangerous rhetoric and intentional conflation of hunting terms with people.

</summary>

"Get your rhino hunting permit."
"Tagging and bagging? Nah, that has to do with people."
"It's pretty dangerous rhetoric. It's pretty inflammatory."
"Those people who hear what I heard and what the guys at the burger joint heard, they know what they're being told."
"There's going to be a lot of questions about what was meant."

### AI summary (High error rate! Edit errors on video page)

Analysis of a controversial political ad for a Missouri Senate candidate involving military imagery and a shotgun.
Republican candidate was reported to law enforcement for the threatening nature of the ad.
The ad mentions a "rhino hunting permit" with no tagging or bagging limit, which are hunting terms related to animals, not people.
Beau's observation at a burger joint reveals the intentional conflation of hunting terms with people, suggesting dangerous rhetoric.
Alluding to historical radicalization on the right targeting less extreme individuals within their own group.
The use of such imagery and rhetoric could have dangerous implications beyond just political discourse.

Actions:

for voters, activists, analysts,
Contact local media to raise awareness about the dangerous implications of using inflammatory rhetoric (implied)
Engage in community dialogues about the impact of political messaging on social cohesion and safety (implied)
</details>
<details>
<summary>
2022-06-21: Let's talk about gas taxes and prices.... (<a href="https://youtube.com/watch?v=Zyn8wqqA_SU">watch</a> || <a href="/videos/2022/06/21/Lets_talk_about_gas_taxes_and_prices">transcript &amp; editable summary</a>)

Beau breaks down the federal gas tax, challenges American perspectives, and advocates for transitioning to alternative energy sources amidst global price comparisons.

</summary>

"One of the things that is just such an American way of looking at the world is that pretty much everything has to do with the president."
"The right move is to transition."
"This seems like an opportune time to do it, to make as many leaps in that direction as we can."

### AI summary (High error rate! Edit errors on video page)

Explains the federal tax on gas and potential holiday proposed by the Biden administration.
Points out that states could have provided relief on gas prices themselves.
Calculates the potential savings from the proposed federal tax holiday, estimating around $0.15 to $0.20 per gallon for most people.
Contrasts U.S. gas prices with prices around the world, showcasing higher prices in countries like Belgium, Denmark, and Hong Kong.
Challenges the American perspective that everything is tied to the president, suggesting a need for a transition away from gas usage.
Emphasizes the importance of transitioning to alternative energy sources and reducing reliance on gas in the long term.
Acknowledges that the federal gas tax holiday could make a difference for some individuals but may not be a significant game-changer overall.
Notes that despite criticisms, the U.S. gas prices are relatively lower compared to prices in many other countries.

Actions:

for american citizens,
Advocate for sustainable energy transitions (implied)
Research and support local initiatives promoting alternative energy sources (implied)
</details>
<details>
<summary>
2022-06-21: Let's talk about Texas leaving the union.... (<a href="https://youtube.com/watch?v=qYVzl3NIBb8">watch</a> || <a href="/videos/2022/06/21/Lets_talk_about_Texas_leaving_the_union">transcript &amp; editable summary</a>)

Republicans in Texas entertain secession, but historical and practical realities make it a questionable endeavor, viewed by Beau as dishonest posturing.

</summary>

"It is the Republican Party posturing, putting out a political talking point that they believe their voters will buy."
"Why is it that the America First movement keeps trying to rip America apart?"

### AI summary (High error rate! Edit errors on video page)

Republicans in Texas are considering a party platform that includes the right to secede from the United States.
Beau supports the right to self-determination and believes in compensating citizens who do not want to leave.
The federal government has made it clear through historical events like the Civil War and the Texas v. White case that there is no legal right for a state to secede from the Union.
The only ways for a state to leave the Union are through revolution or with the consent of other states.
Beau mentions that the idea of Texas leaving the U.S. may prevent the Republican Party from ever holding power nationally again.
He dismisses the idea of a revolution due to the impracticality of winning an armed conflict against the U.S. military.
Beau views the Republican Party's stance on secession as dishonest posturing to appeal to their base rather than a genuine movement.
He questions why a movement claiming to put "America First" is advocating for actions that could tear America apart.

Actions:

for texans, political activists,
Research and understand the historical context and legal implications of secession for better-informed political engagement (suggested)
Engage in open dialogues with fellow Texans and political allies to debunk misinformation and political posturing (implied)
</details>
<details>
<summary>
2022-06-20: Let's talk about how Republicans create demand for strict gun laws.... (<a href="https://youtube.com/watch?v=7uCisENQg_o">watch</a> || <a href="/videos/2022/06/20/Lets_talk_about_how_Republicans_create_demand_for_strict_gun_laws">transcript &amp; editable summary</a>)

Beau explains how the Republican Party creates the demand for strict gun regulation by opposing closing the "boyfriend loophole," manipulating individuals, and prioritizing property rights over lives.

</summary>

"They've got you conned."
"You are once again supporting things in opposition of your own interests, in opposition of your stated beliefs."
"Their property rights over a firearm outweigh your family members' lives."

### AI summary (High error rate! Edit errors on video page)

Explains the chain of events and cause and effect with the Republican Party's stance on gun regulation.
Points out the importance of closing the "boyfriend loophole" in existing laws that prevent domestic abusers from owning firearms.
Notes the Republican opposition to closing this loophole, allowing abusers to own guns.
Criticizes the manipulation of individuals by feeding them talking points that contradict their true beliefs.
Addresses common arguments against closing the loophole, such as false accusations and Second Amendment rights.

Actions:

for american citizens,
Advocate for the closure of the "boyfriend loophole" in existing laws to prevent domestic abusers from owning firearms (suggested).
Challenge misinformation and manipulation by engaging in informed debates and spreading accurate information (implied).
</details>
<details>
<summary>
2022-06-20: Let's talk about Trump, Georgia, and a bold proclamation.... (<a href="https://youtube.com/watch?v=6WMcjpxtrrs">watch</a> || <a href="/videos/2022/06/20/Lets_talk_about_Trump_Georgia_and_a_bold_proclamation">transcript &amp; editable summary</a>)

Beau speculates on potential legal trouble for Trump in Georgia, pointing to tape-recorded evidence and state-level prosecutions as critical factors in the case.

</summary>

"There is a really neat three-year felony in Georgia that Donald Trump has violated."
"prosecutors love tape-recorded evidence because you cannot cross-examine it."
"I think there might be more investigations into the Trump campaign's activities at the state level."
"It's just a thought, God have a good day."

### AI summary (High error rate! Edit errors on video page)

Speculates on what might be on Donald Trump's mind, particularly focusing on Georgia.
Mentions Nick Ackerman, a former assistant prosecutor during Watergate, making bold statements about potential legal trouble for Trump.
Ackerman believes that a three-year felony in Georgia that Trump violated could lead to his imprisonment.
Points out the significance of tape-recorded evidence that prosecutors love due to its inability to be cross-examined.
Suggests that the evidence from the January 6th committee combined with tapes leaves Trump defenseless in Georgia.
Foresees Georgia as a potential prosecution that could result in Trump going to jail.
Notes the increasing interest of state-level prosecutors in cases related to Trump’s activities.
Predicts more state-level prosecutions if the Department of Justice (DOJ) does not take action.
Indicates that state prosecutors may pursue investigations into Trump campaign activities if the DOJ does not intervene.
Emphasizes the importance of paying attention to cases beyond Washington D.C. and congressional hearings.

Actions:

for legal analysts, political watchdogs,
Watch for developments in state-level prosecutions related to Trump's activities (implied).
Stay informed about investigations beyond federal hearings (implied).
</details>
<details>
<summary>
2022-06-20: Let's talk about Texas Republicans and Orwell.... (<a href="https://youtube.com/watch?v=KiAJbEXutbY">watch</a> || <a href="/videos/2022/06/20/Lets_talk_about_Texas_Republicans_and_Orwell">transcript &amp; editable summary</a>)

Texas Republicans reject 2020 election results, demand blind faith, and Orwellian loyalty to the party over truth.

</summary>

"How do you really know what year it is?"
"They will say that two plus two is five."
"They're going to do what the party says."
"They will never question their bettors."
"They reject something as fundamental as math with no evidence."

### AI summary (High error rate! Edit errors on video page)

Republican Convention in Texas questioned 2020 election results without evidence.
Trump's inner circle admitted disbelief in the claims.
Texas Republicans reject fundamental truths like math without evidence.
Orwellian comparison to 1984 novel where truth is dictated by the party.
Party demands blind faith, rejecting evidence and basic truths.
Texas Republicans cling to election lies like the Alamo, fearing judgment.
Politicians won't admit error to maintain voter trust.
Voters comply to gain party favor, even if they know the truth.
Blind loyalty to party over truth and facts.
Self-reinforcing cycle of misinformation and blind faith.

Actions:

for texas voters,
Challenge misinformation within your community (suggested)
Support politicians who prioritize truth and evidence (implied)
</details>
<details>
<summary>
2022-06-19: Let's talk about what's next for the committee.... (<a href="https://youtube.com/watch?v=-hohQA0JF28">watch</a> || <a href="/videos/2022/06/19/Lets_talk_about_what_s_next_for_the_committee">transcript &amp; editable summary</a>)

The committee is poised to reveal how Trump influenced officials and manipulated grievances, potentially unraveling a conspiracy and strengthening DOJ's efforts.

</summary>

"One of those very rare signs that we get that the Department of Justice is actively working behind the scenes."
"You want to talk about something that is going to bring a conspiracy home, that's it."
"I have a feeling we're going to see that soon, and they'll bring it up to further break down the resolve of those holdouts."
"It'll help the committee and it will also help the Department of Justice."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The committee has followed a template well, showing evidence that Trump knew his election claims were false and that pressuring Pence was illegal.
The Department of Justice wants the transcripts for upcoming investigations and prosecutions.
Local prosecutors may also request evidence from the committee for state-level cases.
The committee is likely to show that Trump sought to influence officials at the state and local levels through a campaign.
They might demonstrate that grievances used to push the January 6th narrative were manufactured at the state level by the White House.
Trump's orbit pressured state and local officials to support these claims.
The committee may address Trump's circle's campaign to pressure the Department of Justice into declaring the election corrupt.
They may break the chronological approach to focus on events at the White House during January 6th, including text messages.
This shift could reignite public interest and potentially weaken opposition.
Text messages from high-profile individuals may reveal discord and further undermine deniers of the election outcome.
The committee's public presentation may help secure interviews and cooperation for the Department of Justice.
Speculations hint at upcoming revelations until the end of the month.

Actions:

for committee members, prosecutors, viewers,
Contact local prosecutors for potential state-level case evidence (implied)
Stay informed about upcoming committee revelations (implied)
</details>
<details>
<summary>
2022-06-19: Let's talk about the TX GOP and Crenshaw.... (<a href="https://youtube.com/watch?v=zgTn4gI_YKk">watch</a> || <a href="/videos/2022/06/19/Lets_talk_about_the_TX_GOP_and_Crenshaw">transcript &amp; editable summary</a>)

Texas GOP divisions escalate as Dan Crenshaw's far-right label sparks assault, revealing a worrying shift towards extremism beyond Trump, echoing 1930s Europe.

</summary>

"If you ever find yourself in a situation where Dan Crenshaw is to your left, you need to examine your life choices."
"This is something far, far, far to the right of that. It's something that would be more at home in 1930s in Europe."
"Dan Crenshaw is to the left of this faction of the Republican Party. That should be concerning for everybody in the country."

### AI summary (High error rate! Edit errors on video page)

Texas Republican Convention and divisions within the Republican Party are being discussed again.
Dan Crenshaw, a Purple Heart recipient and extreme Republican, faced verbal attacks and light assault at the Convention for being labeled a "Republican in name only."
Crenshaw's far-right views were criticized, with implications that being further right than him means veering into something beyond traditional Republicanism.
The Republican Party's shift towards authoritarianism is evident, progressing beyond Trump.
Crenshaw's positioning within the party indicates a concerning shift towards extreme right-wing ideologies reminiscent of 1930s Europe.
The embrace of conspiracies and baseless claims within the Republican Party is troubling.
Crenshaw's relative left-leaning stance within his faction should raise alarms among conservatives.
The current political discourse has shifted to extremes, posing a threat to the traditional norms of American politics.

Actions:

for conservatives, republicans,
Support and stand up for political figures facing attacks within their own party (exemplified)
Challenge extreme ideologies and baseless claims within political parties (exemplified)
Advocate for maintaining traditional norms and values within political discourse (exemplified)
</details>
<details>
<summary>
2022-06-19: Let's talk about Texas, Trump, and Log Cabin Republicans.... (<a href="https://youtube.com/watch?v=gY3GHC5WIXs">watch</a> || <a href="/videos/2022/06/19/Lets_talk_about_Texas_Trump_and_Log_Cabin_Republicans">transcript &amp; editable summary</a>)

Division within Texas GOP over exclusion of LGBTQ Republicans shows futile attempts to prove loyalty to a party fundamentally opposed to their existence.

</summary>

"Y'all are failing to see horror on horror's face."
"They view you as their cultural enemy."
"There is no amount of trying to show that you're different."
"You have been othered by your party."
"You cannot demonstrate that you're different. They don't care."

### AI summary (High error rate! Edit errors on video page)

Division within the Texas state GOP, certain Republicans, and Trump due to exclusion of log cabin Republicans from a convention.
Log cabin Republicans are the LGBTQ branch within the Republican party.
Trump intervened, supporting log cabin Republicans and causing them to possibly feel better.
Log cabin Republicans tweeted about not agreeing with the left's views on sex and gender.
Beau points out that the left acknowledges the difference between sex and gender, unlike what the tweet suggests.
Log cabin Republicans are trying to prove they are "one of the good ones" to the GOP, but Beau argues that this effort is futile.
The Republican Party is embracing authoritarianism, evident in Trump's actions and their treatment of certain groups like the LGBTQ community.
Beau warns that attempting to show loyalty to a group that fundamentally opposes your existence is counterproductive.
The GOP views log cabin Republicans as their cultural enemy regardless of their party affiliation.
Beau cautions against working against your own interests by trying to ingratiate yourself into a group that doesn't believe you should exist.

Actions:

for lgbtq republicans,
Reassess your alignment and support within a party that may not fully accept or support you (implied).
Take stock of whether your actions are truly in your best interest as an individual (implied).
</details>
<details>
<summary>
2022-06-18: Let's talk about new developments from Uvalde and policy.... (<a href="https://youtube.com/watch?v=LEmygVjhVHo">watch</a> || <a href="/videos/2022/06/18/Lets_talk_about_new_developments_from_Uvalde_and_policy">transcript &amp; editable summary</a>)

Beau talks about the need for proper training and caution against arming unprepared individuals in critical situations, like teachers in schools.

</summary>

"Your policy shouldn't be guided by myths."
"Without the training, the tool means nothing."
"Arming teachers isn't the solution."

### AI summary (High error rate! Edit errors on video page)

Talking about the new information released regarding an incident in Texas involving two Uvalde PD officers and a deputy.
The officers had the chance to intercept a gun before it entered a school but didn't take the shot, fearing hitting a child.
Raises concerns about the department's lack of transparency and hiring an outside law firm to avoid releasing footage.
Emphasizes that simply having a weapon does not make someone an operator or shooter; extensive training is necessary.
Criticizes the idea of arming teachers, pointing out the unrealistic expectations and lack of proper training.
Mentions the unrealistic portrayal of firearms ownership and the misconception that having the right tool guarantees success.
Expresses skepticism about arming teachers as a solution, citing the challenges even trained officers faced in similar situations.
Urges people to understand the importance of training and proficiency in handling firearms before considering arming teachers.
Questions the myths and misguided policies around arming teachers, stressing the need for realistic solutions.
Encourages thoughtfulness and reflection on the implications of arming untrained individuals in critical situations.

Actions:

for policy makers, educators,
Ensure proper training for individuals in critical situations (implied).
</details>
<details>
<summary>
2022-06-18: Let's talk about how history will look at the 6th and Pence.... (<a href="https://youtube.com/watch?v=P2jWSFZbSk8">watch</a> || <a href="/videos/2022/06/18/Lets_talk_about_how_history_will_look_at_the_6th_and_Pence">transcript &amp; editable summary</a>)

The historical significance of January 6th and the critical role of Mike Pence in shaping its narrative and preventing future authoritarian threats underscore the need for accountability and courageous leadership.

</summary>

"This was a major historical event. It will probably be as transformative for the United States as Pearl Harbor was."
"History is not done with Vice President Pence."
"He's through the door. If he doesn't and there is no accountability, there is no message sent, they will try again."

### AI summary (High error rate! Edit errors on video page)

Talking about the historical significance of the events of January 6th and the ongoing hearings.
Reminds that historians will document the actual story, ensuring it's available for those interested.
Mentions the tendency to personify events in American history, focusing on one person rather than the collective.
Suggests following the hashtag H-A-T-H on Twitter for historians at the hearings.
Predicts that Mike Pence may be remembered as the hero who saved the American experiment by not cooperating with the attempted coup on January 6th.
Emphasizes the critical nature of Pence's narrative from that day, which will shape his legacy.
Urges Pence to explain his actions on January 6th to the American people without spin.
Points out the importance of Pence continuing to show courage and accountability to prevent future authoritarian threats.
Stresses the need for Pence to publicly address why he stayed on January 6th despite the risks.
Warns that without accountability for the events of January 6th, there is a risk of similar incidents happening again.

Actions:

for american citizens,
Contact historians at the hearings to stay informed and follow the hashtag H-A-T-H on Twitter (implied)
Hold leaders accountable for their actions and demand transparency from public figures (implied)
</details>
<details>
<summary>
2022-06-18: Let's talk about New Mexico, foreshadowing, and the committee.... (<a href="https://youtube.com/watch?v=0pJcejUlPiU">watch</a> || <a href="/videos/2022/06/18/Lets_talk_about_New_Mexico_foreshadowing_and_the_committee">transcript &amp; editable summary</a>)

The situation in New Mexico's Otero County foreshadows election uncertainties unless resolved, with conspiracy theories morphing into political stances, posing a significant threat to the US.

</summary>

"Understanding the events leading up to January 6th is vital."
"Repetition of claims can lead people to believe they are valid."
"The situation has transcended beyond Trump."
"It's imperative for the committee and the DOJ to take action."
"This issue is far more dangerous than most people think."

### AI summary (High error rate! Edit errors on video page)

The situation in New Mexico's Otero County foreshadows future election uncertainties unless the committee and the Department of Justice provide a clear resolution.
The Commission in Otero County initially decided not to certify the election, mirroring desires some had for Pence during the 2020 election.
Despite no evidence of wrongdoing in the primary election, the issue has morphed into a political stance and talking point.
Repetition of claims can lead people to believe they are valid, influencing politicians to conform to the pressure.
Politicians reluctantly certified the election after pressure from the Supreme Court.
The prevalence of conspiracy theories and grandstanding could pose a significant threat to the United States during the midterms and future elections.
Understanding the events leading up to January 6th is vital, as misinformation continues to influence individuals across the country.
The situation has transcended beyond Trump and become a dangerous political position with wide-reaching implications.
It is imperative for the committee and the Department of Justice to take action in this evolving issue.

Actions:

for politically aware citizens,
Contact local representatives to advocate for clear resolutions in election disputes (implied).
Join organizations working to combat misinformation and conspiracy theories in politics (implied).
</details>
<details>
<summary>
2022-06-17: Let's talk about sharks, screens, shifting thought, and Hollywood.... (<a href="https://youtube.com/watch?v=dyq5b-UL9GI">watch</a> || <a href="/videos/2022/06/17/Lets_talk_about_sharks_screens_shifting_thought_and_Hollywood">transcript &amp; editable summary</a>)

The power of storytelling in Hollywood could reshape cultural perceptions towards gun safety and save lives through innovative portrayals.

</summary>

"They just need the will, right?"
"It will have a marked effect."
"This is the kind of out-of-the-box thinking that we need."

### AI summary (High error rate! Edit errors on video page)

Mentioning the impact of movies about sharks, like "Jaws," influencing people to fear sharks and avoid water.
Noting that after almost 50 years, people's opinions about sharks could still be influenced by those movies.
Proposing the idea of Hollywood altering the portrayal of firearms in movies to shift cultural attitudes towards gun safety.
Describing a letter signed by 200 directors and producers in Hollywood acknowledging the power of storytelling to affect change.
Emphasizing the historical ability of Hollywood to shift societal perceptions without the need for laws.
Speculating on the potential positive effects of changing the portrayal of firearms in media to reduce violence and toxic masculinity.
Pointing out that Hollywood has the capacity to alter cultural perceptions through storytelling.
Expressing hope that Hollywood's efforts to shift perceptions around firearms will lead to saving lives.
Advocating for innovative thinking and positive changes in media portrayals to have a significant impact on society.

Actions:

for media influencers, hollywood professionals,
Reach out to Hollywood to advocate for more responsible portrayals of firearms in movies (implied)
Support efforts to shift cultural attitudes towards gun safety through storytelling in media (implied)
</details>
<details>
<summary>
2022-06-17: Let's talk about MLK and black people wanting more.... (<a href="https://youtube.com/watch?v=TCm5Q_Rer_Y">watch</a> || <a href="/videos/2022/06/17/Lets_talk_about_MLK_and_black_people_wanting_more">transcript &amp; editable summary</a>)

Beau addresses the need for radical societal changes to achieve justice for black individuals, drawing from Martin Luther King's 1967 speech to challenge perceptions on racial equality.

</summary>

"White Americans must recognize that justice for black people cannot be achieved without radical changes in the structure of our society."
"The problems of racial injustice and economic injustice cannot be solved without a radical redistribution of political and economic power."

### AI summary (High error rate! Edit errors on video page)

Continuing a recent series of talks about tensions in the country.
Addressing a message about radicalism and race.
Emphasizing that radical black individuals seek more than just equality.
Urging white Americans to recognize the need for radical societal changes for justice.
Pointing out the lack of effort from white individuals to overcome racial ignorance.
Stating that substantial investment in black Americans is necessary for progress.
Describing the resistance to adjusting to black neighbors and genuine school integration among white Americans.
Explaining the root causes of contemporary racial tensions.
Stressing that racial and economic injustices require a radical redistribution of power.
Quoting Martin Luther King's 1967 speech to illustrate longstanding radical ideas.
Encouraging further exploration of the original vision for racial equality beyond surface-level knowledge.

Actions:

for activists, allies, educators,
Educate on racial history and the original vision for equality (suggested)
Advocate for societal changes to address racial and economic injustices (implied)
</details>
<details>
<summary>
2022-06-17: Let's talk about Day 3 of the hearings.... (<a href="https://youtube.com/watch?v=RJ0eiQ48KjM">watch</a> || <a href="/videos/2022/06/17/Lets_talk_about_Day_3_of_the_hearings">transcript &amp; editable summary</a>)

Committee reveals Trump's knowledge of false claims, illegal plans, endangerment of Pence, and post-event actions; raises questions on public disclosures and potential election interference.

</summary>

"Ignorance of the law doesn't cut it."
"If you actually succeed in this, you'll cause riots around the country."
"At some point when the conversations turn to there's going to be riots around the country, it seems as though that should be widely broadcast."

### AI summary (High error rate! Edit errors on video page)

Committee established Trump knew his claims were false.
Trump was aware plan to pressure Pence was illegal.
Eastman, behind the plan, openly stated it required a violation of federal law.
Ignorance of the law isn't an excuse; judges frown upon knowingly breaking the law.
Pence was in actual danger; informant confirmed his capture could have been the end.
Eastman was fine with potential riots if the plan succeeded.
Eastman sought a pardon from Giuliani post-events.
Eastman considered appealing decisions in Georgia on the 7th.
Questions raised on why individuals who pushed back on the plan didn't go public.
Raises concerns on whether Trump's endorsers could potentially overturn an election.

Actions:

for congress, investigators, activists,
Contact Congress to push for transparency and accountability (suggested).
Join or support organizations advocating for democracy and election integrity (implied).
</details>
<details>
<summary>
2022-06-16: Let's talk about pardons, votes, and Trump.... (<a href="https://youtube.com/watch?v=OOGE4ZmveAg">watch</a> || <a href="/videos/2022/06/16/Lets_talk_about_pardons_votes_and_Trump">transcript &amp; editable summary</a>)

Committee suggests lawmakers sought pardons, Beau wants comparison with impeachment votes, and Hannity's suggestion to pardon Hunter reveals real-life media manipulation tactics.

</summary>

"Pardons imply wrongdoing."
"Maybe [we should] have in the back of our minds, given the way they're talking about the hearings now."
"It does kind of imply that you might have done something wrong if you are preemptively seeking a pardon."
"I'd really like to see that."
"That does happen."

### AI summary (High error rate! Edit errors on video page)

Committee suggested lawmakers sought pardons from Trump after Capitol events.
Pardons imply wrongdoing.
Beau wants comparison between pardon seekers' votes on impeachment.
Beau suspects some sought pardons then voted against impeachment.
Committee presenting strong case, likely to show people sought pardons.
Beau hopes committee questions lawmakers about their votes.
Sean Hannity suggested Trump pardon Hunter Biden to change the story post-Capitol fallout.
Political parties often try to change the story when faced with bad news.
Hannity's suggestion reveals real-life attempts to manipulate media coverage.
Beau urges people to be mindful of manipulation tactics used by politicians.

Actions:

for voters, citizens, activists,
Question lawmakers about their votes (suggested)
Be mindful of media manipulation tactics (implied)
</details>
<details>
<summary>
2022-06-16: Let's talk about culture shifting and representation.... (<a href="https://youtube.com/watch?v=NbYkwC54_K0">watch</a> || <a href="/videos/2022/06/16/Lets_talk_about_culture_shifting_and_representation">transcript &amp; editable summary</a>)

Beau addresses the shift in cultural power away from white men in pop culture icons, urging individuals to actively participate in the fight for change to ensure representation for future generations.

</summary>

"Nobody looks more like you than you."
"You want your kid, your grandkid to have representation of people that look like you in the fight? You be that representation."
"Just get involved. That's all it takes."

### AI summary (High error rate! Edit errors on video page)

Addressing a message sparked by a Patterson video, focusing on culture rather than workplace competition.
Expressing uneasiness about the cultural power shifting away from white men towards more women and people of color in pop culture icons.
Acknowledging the need for change and appreciating those leading the charge for change.
Emphasizing the importance of representation for future generations in the fight for change.
Explaining why white men may not be pop culture leaders in the fight for change due to advocating for the status quo.
Sharing a personal anecdote about encouraging his son and his friend to participate in a march and the importance of being the representation you seek.
Encouraging individuals to get involved in the fight for change, even if not taking a leadership role.
Stating that the lack of white men in pop culture leadership roles is due to many choosing to maintain the status quo and not actively participating in the movement for change.

Actions:

for creators and consumers of pop culture.,
Be the representation you seek in the fight for change (exemplified).
Get involved in marches, protests, or community events advocating for change (exemplified).
</details>
<details>
<summary>
2022-06-16: Let's talk about bulls, bears, and the market.... (<a href="https://youtube.com/watch?v=KP-IMosSktU">watch</a> || <a href="/videos/2022/06/16/Lets_talk_about_bulls_bears_and_the_market">transcript &amp; editable summary</a>)

Beau explains bulls, bears, and a potential economic downturn, advising against panicking and differentiating Wall Street from Main Street amid market uncertainties.

</summary>

"Stocks are going up. A bear lays down and hibernates. They're going down."
"Wall Street isn't Main Street."
"People aren't going to be able to borrow as much, therefore they'll spend less, therefore the economy will go down, and they're kind of speculating on that."

### AI summary (High error rate! Edit errors on video page)

Explains the concepts of bulls and bears in the market, particularly focusing on the recent bear market on Wall Street.
Describes a bear market as when the S&P 500 drops more than 20% from its peak.
Shares historical data on how long bear markets typically last, around 13 months heading down and 27 months to recover.
Advises the average person not to panic about a bear market leading to a recession, as it's about a little more than a 50% chance.
Points out that those heavily reliant on their 401k, like boomers, may need to be more conservative with their spending during a bear market.
Emphasizes that Wall Street indicators are not always reflective of the real economy, especially due to factors like low unemployment rates and speculation on borrowing and spending.
Acknowledges the likelihood of an upcoming economic downturn but underlines the uncertainty of its depth and duration.
Notes that despite some economic concerns, other indicators have been positive, contrary to what news coverage may suggest.
Encourages patience and observation regarding the evolving economic situation.

Actions:

for investors, savers, workers,
Monitor your investments regularly for any necessary adjustments (implied)
Seek financial advice if needed to navigate market uncertainties (implied)
Stay informed about economic trends and indicators affecting your financial well-being (implied)
</details>
<details>
<summary>
2022-06-15: Let's talk about overcoming fear of change.... (<a href="https://youtube.com/watch?v=q5CFHg3ALtY">watch</a> || <a href="/videos/2022/06/15/Lets_talk_about_overcoming_fear_of_change">transcript &amp; editable summary</a>)

Beau addresses systemic racism, unity through competition, and the benefits of ending scapegoating for societal growth and improvement.

</summary>

"If you're kicking down, you're not actually looking in the direction of where the problems originate."
"Ending systemic racism will improve the lives of the entire working class."
"It doesn't actually, harder is probably not the right word. It's more competitive, but in a good way."
"If you've ever been the smartest person in the room, you're in the wrong room."
"Don't let it seem scary. It's not. It's a good thing."

### AI summary (High error rate! Edit errors on video page)

Addresses the idea of changing the system to combat institutional and systemic racism in the United States.
Points out that the white working class may fear losing their comparative advantage over marginalized groups.
Explains that the current system creates a false sense of security for the working class based on comparison rather than actual well-being.
Emphasizes that kicking down on those with less power is not the solution to societal problems.
Predicts a positive shift towards unity and improved conditions for all once scapegoating ends.
Advocates for competition as a means of personal and societal growth, contrasting it with the current system of division by race.
Uses personal anecdotes and examples to illustrate the benefits of healthy competition and growth.
Encourages stepping out of comfort zones and challenging oneself for improvement.
Posits that ending systemic racism will lead to a stronger, more united front addressing societal issues.
Concludes with a message of hope and encouragement for positive change.

Actions:

for working-class individuals,
Foster unity and cooperation within your community to address societal issues (exemplified)
Challenge yourself and others to step out of comfort zones for growth and improvement (exemplified)
</details>
<details>
<summary>
2022-06-15: Let's talk about Patterson's comment on race.... (<a href="https://youtube.com/watch?v=mGUEcEwRtNg">watch</a> || <a href="/videos/2022/06/15/Lets_talk_about_Patterson_s_comment_on_race">transcript &amp; editable summary</a>)

Beau delves into systemic racism, challenging the notion of white men facing new racism and urging openness to change and growth.

</summary>

"Imagine if you were on the end it's working against."
"The problem is you have lost just a little bit of privilege and you're mistaking it for oppression."
"All people are trying to do is level that playing field."

### AI summary (High error rate! Edit errors on video page)

Addressing James Patterson's comments on white men facing challenges in the publishing industry and in writing jobs in film and TV.
Exploring the reasons behind the perceived difficulties faced by older white writers in getting hired.
Touching on the concept of systemic racism and how it affects perceptions and opportunities.
Challenging the idea that white men are experiencing a new form of racism, suggesting it's actually a reduction in privilege.
Emphasizing that acknowledging systemic racism exists doesn't equate to experiencing oppression.
Encouraging openness to new ideas and perspectives to grow and adapt in changing environments.

Actions:

for listeners, learners, allies,
Challenge your own perceptions and biases (implied)
Educate yourself on systemic racism and privilege (implied)
Support initiatives that aim to level the playing field in various industries (implied)
</details>
<details>
<summary>
2022-06-15: Let's talk about Biden's inflation.... (<a href="https://youtube.com/watch?v=U5OfMa6NfX0">watch</a> || <a href="/videos/2022/06/15/Lets_talk_about_Biden_s_inflation">transcript &amp; editable summary</a>)

Beau clarifies global inflation, challenges blaming Biden, and urges viewers to question sources spreading misinformation.

</summary>

"It's a global phenomenon. It's happening everywhere."
"Why do you watch somebody who thinks so little of you?"
"If you have information sources that are constantly referring to this as something that Biden did, here are your facts."
"Why continue to be informed by an organization or a person who you know is intentionally misinforming you?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains inflation in the United States is a global issue, not solely Biden's fault.
Cites data from Deutsche Bank showing the US inflation rate at 8.6%, better than the Netherlands but worse than Germany.
Notes that Japan and China seem immune to high inflation due to historical factors and price controls.
Mentions Turkey's high inflation rate at 74% and double-digit inflation in many large economies south of the US border and in Africa.
Questions why blame is continuously pinned on Biden when inflation is a global phenomenon.
Criticizes channels or pundits who misinform viewers by falsely attributing inflation solely to Biden, urging viewers to fact-check.
Warns against echo chambers and information silos that manipulate and control information for their viewers' beliefs.
Encourages viewers to seek out accurate information and question sources intentionally spreading misinformation.

Actions:

for viewers seeking clarity on inflation.,
Fact-check information sources (implied).
</details>
<details>
<summary>
2022-06-14: Let's talk about the committee and making referrals.... (<a href="https://youtube.com/watch?v=5cGhdQFpdO4">watch</a> || <a href="/videos/2022/06/14/Lets_talk_about_the_committee_and_making_referrals">transcript &amp; editable summary</a>)

Beau explains the debate over committee referrals regarding DOJ indictments and stresses the importance of Americans understanding the process, regardless of the political implications.

</summary>

"It's very important for the American people to hear about what happened."
"It's actually a power that Congress has."
"It's not a have to. It's a we would like you to type of thing."
"That's part of this that can't get lost in the debate over the referrals."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Referrals from the committee were a topic of debate after initial news reported they wouldn't happen, later corrected to say no decision had been made yet.
There are two contrasting schools of thought regarding issuing referrals: one suggesting it's unnecessary for the Department of Justice (DOJ) to need a referral and could be viewed as nonpartisan, while the other argues that it's vital for Americans to understand the process and might be seen positively as a Democratic Party action.
The choice on referrals is complex due to political implications, with considerations on potential views of the American people, political parties, and DOJ's independence.
The hope is for DOJ to act independently, but the timing and details of their actions remain uncertain.
Despite the focus on referrals, the importance of Americans engaging with the hearings to understand the events is emphasized.
Ultimately, whether referrals are issued or not, it doesn't guarantee DOJ indictment and shouldn't be cause for panic, as it's more of a political strategy rather than a lack of evidence.

Actions:

for congressional observers,
Watch and understand the hearings to grasp the significance of the events (implied).
</details>
<details>
<summary>
2022-06-14: Let's talk about an update on Patrick Lyoya.... (<a href="https://youtube.com/watch?v=H5XWTtZJoQ8">watch</a> || <a href="/videos/2022/06/14/Lets_talk_about_an_update_on_Patrick_Lyoya">transcript &amp; editable summary</a>)

Beau provides an update on the case of Patrick Leoya, expressing surprise at charges filed against the officer for manslaughter and second-degree murder, indicating a shifting attitude towards prosecuting such cases.

</summary>

"The officer escalated at every possible opportunity."
"It's worth noting that the prosecution does have an uphill battle here."
"I've got to be honest, that's surprising."

### AI summary (High error rate! Edit errors on video page)

Provides an update on the case of Patrick Leoya, a previous video that deeply affected him.
Describes a communication issue at the beginning of the police engagement.
Suggests that the officer initiated contact and escalated the situation.
Mentions Patrick reaching for a taser, which is often used to justify police actions.
Expresses surprise at charges being filed against the officer for manslaughter and second-degree murder.
Notes the prosecutor's uphill battle but acknowledges a shift in attitudes towards presenting cases like this to a jury.
Speculates that there may be more information known by the prosecutor and state cops.
Concludes with uncertainty about the case's outcome.

Actions:

for advocates and justice seekers,
Support advocacy groups working towards police accountability (implied)
Educate others on the importance of holding law enforcement accountable (implied)
Stay informed about developments in cases of police misconduct (implied)
</details>
<details>
<summary>
2022-06-14: Let's talk about $250 million, Trump, and the bigger story.... (<a href="https://youtube.com/watch?v=Yu41FzX7VJc">watch</a> || <a href="/videos/2022/06/14/Lets_talk_about_250_million_Trump_and_the_bigger_story">transcript &amp; editable summary</a>)

Trump raised $250 million under false pretenses, deceiving working-class donors, while the committee aims to show his knowledge of baseless election claims in a broader conspiracy narrative.

</summary>

"Trump's team raised $250 million under false pretenses."
"The committee aims to demonstrate that Trump knew his election claims were baseless."
"Don't just get lost in the daily story. Use it like a jigsaw puzzle piece."

### AI summary (High error rate! Edit errors on video page)

Trump's team raised $250 million in the guise of an official election defense fund, although there wasn't one.
Most of the money raised actually went to Trump's PAC, Mark Meadows' charity, Trump's hotel collection, and the rally company for January 6th.
The committee aims to demonstrate that Trump knew his election claims were baseless from the start.
Trump's actions raise questions about taking money under false pretenses and not using it for the stated purpose.
Trump may have deceived working-class donors by making false claims about the election and misusing the raised funds.
The committee is expected to reveal more information linking back to previously disclosed details, forming a wider narrative.
Trump's pattern of deceiving working-class donors might be typical, but doing so knowingly under false pretenses adds another layer of deceit.
Watching the hearings requires connecting new information to the bigger picture the committee is painting.
The hearings aim to establish a comprehensive conspiracy rather than focusing solely on daily stories.
The committee's strategy involves revealing puzzle pieces that, when put together, show a larger scheme at play.

Actions:

for political analysts, investigators,
Follow updates from the committee hearings and connect new information to previously disclosed details (implied).
</details>
<details>
<summary>
2022-06-13: Let's talk about the definition of boyfriend.... (<a href="https://youtube.com/watch?v=2uYTxcEYDTg">watch</a> || <a href="/videos/2022/06/13/Lets_talk_about_the_definition_of_boyfriend">transcript &amp; editable summary</a>)

The Democratic and Republican compromise on gun legislation must focus on a broad definition of dating partners to prevent tragedies.

</summary>

"You want to create a situation where if Jimmy and Susie go out on one date and Susie's like, hey, you know, I'm just not that into you. And Jimmy's like, cool, no problem. And then three months later, Susie's out on a date with somebody else and Jimmy has a few in him and he gets mad and he goes up, starts yelling and screaming and hits her once. He never gets to touch a gun again. That's the point you're trying to get to."
"This is your chance to draft real legislation that will save lives, and lots of them."

### AI summary (High error rate! Edit errors on video page)

The Democratic and Republican Parties in the Senate have reached a compromise on key points regarding gun legislation.
One critical piece of legislation to focus on is the "boyfriend loophole."
The definition of a dating partner is a key aspect that will impact the effectiveness of the legislation.
Narrowing the scope of the definition, such as adding "serious" before dating partner, could render the legislation ineffective.
The goal should be to have a broad definition that includes situations where individuals exhibit concerning behavior towards their partners.
Craft legislation that prevents those who cannot control their behavior towards their partners from owning guns.
The effectiveness of the legislation hinges on the inclusivity and wide scope of the definition of a dating partner.
Failure to create an encompassing definition will result in lives being put at risk.
This legislation has the potential to save numerous lives and should not be underestimated.
The importance of this legislation surpasses that of an assault weapons ban.

Actions:

for legislators, activists, advocates,
Advocate for broad definitions in legislation to prevent loopholes (implied)
Raise awareness about the importance of inclusive definitions in gun legislation (implied)
</details>
<details>
<summary>
2022-06-13: Let's talk about Trump's Alabama problem.... (<a href="https://youtube.com/watch?v=nj_YdM58xgA">watch</a> || <a href="/videos/2022/06/13/Lets_talk_about_Trump_s_Alabama_problem">transcript &amp; editable summary</a>)

Trump's bad week includes his social media platform issues and a failed endorsement in Alabama, revealing his declining influence and lack of substantive agenda.

</summary>

"It's having the overall impact of showing a lot of the Trumpist base that the emperor has no clothes."
"Donald Trump is the only man in American politics who could get conned by Mitch McConnell twice in an Alabama Senate race."
"The rejection of Trump's endorsements demonstrate his dwindling influence."

### AI summary (High error rate! Edit errors on video page)

Trump's bad week is not just about the hearings but also about his social media platform and an endorsement issue in Alabama.
Trump's social media platform, Truth Social, is reportedly banning anyone talking about the hearings, affecting its marketing.
In Alabama, Trump initially endorsed Mo Brooks, but withdrew it when Brooks didn't support Trump's election claims; the endorsement went to Katie Britt.
Trump loyalists are urging followers to vote for Brooks despite Trump's endorsement of Britt, painting her as a McConnell bot.
This move reveals that Trump's endorsements may be driven by pettiness or a desire to associate with potential winners.
Brooks noted Trump's inconsistency in endorsing Katie Britt after calling her unqualified for the Senate previously.
The rejection of Trump's endorsements shows his diminishing influence, debunking the media narrative that his endorsements are significant.
If Brooks wins despite Trump's withdrawal of support, it indicates Trump's weakening hold.
This development exposes the lack of a substantive agenda behind Trump's authoritarian leadership style, revealing it as mere pettiness and rhetoric.
The base's disillusionment with Trumpism may help prevent the rise of a more polished and dangerous version of Trump in the future.

Actions:

for political observers, voters,
Inform fellow voters about the dynamics behind political endorsements in order to make informed decisions (implied)
</details>
<details>
<summary>
2022-06-13: Let's talk about Day 2 of the hearings will attempt to show.... (<a href="https://youtube.com/watch?v=eC7pcMjY9_Q">watch</a> || <a href="/videos/2022/06/13/Lets_talk_about_Day_2_of_the_hearings_will_attempt_to_show">transcript &amp; editable summary</a>)

Beau outlines what to expect from the hearings, aiming to prove that Trump knowingly lied about the election, with key witnesses providing insight.

</summary>

"Trump knew that all of his claims about the election were false."
"He knew he was lying is their position."
"There are going to be some things that the American people really don't know."
"BJ has some surprises for us."
"Trump knew he was lying when he made these claims."

### AI summary (High error rate! Edit errors on video page)

Overview of the hearings and what to expect from them.
The hearings seem to follow a template of telling them what you're going to tell them, telling them, and then telling them what you've told them.
Day one focused on setting the stage for the hearings, while day two seems to be delving into the phase of presenting information.
The objective appears to be proving that Trump knowingly made false claims about the election.
Trump is believed to have been aware that his claims were untrue, despite being informed otherwise by many.
Former Trump campaign manager and a former Fox News political director are among those who will testify, along with other key figures.
BJ Pock, a former US attorney, might have surprises to share during the hearings.
The intent seems to be establishing that Trump was lying when he made election claims, linking his statements to subsequent actions.
The hearings are anticipated to be lengthy, requiring viewers to keep track of various details and pieces of information.
Viewers are encouraged to watch the hearings to determine if the objectives are met.

Actions:

for political observers,
Watch the hearings closely to understand the presented information and draw your conclusions (implied).
</details>
<details>
<summary>
2022-06-12: Let's talk about how political parties determine your health.... (<a href="https://youtube.com/watch?v=rYVSV0SywJk">watch</a> || <a href="/videos/2022/06/12/Lets_talk_about_how_political_parties_determine_your_health">transcript &amp; editable summary</a>)

New research shows that political affiliation in the U.S. significantly impacts health outcomes, with Democratic counties experiencing a 22% decrease in mortality rate compared to 11% in Republican counties, revealing the life-extending impact of policy and lifestyle choices.

</summary>

"Democratic Party policies and that lifestyle make you live longer."
"Republican policies, fake masculinity, being a tough guy, all of that stuff, it's literally killing you."

### AI summary (High error rate! Edit errors on video page)

New research from Brigham and Women's Hospital reveals differing health outcomes based on political affiliation in the U.S.
The study analyzed data from 2001 to 2019, covering 3,000 counties across all 50 states.
Living in a county that voted Democratic was associated with a 22% decrease in mortality rate, while Republican counties saw an 11% decrease.
This mortality gap started to widen around 2010 with the implementation of the Affordable Care Act, as Democratic states expanded Medicaid, offering health insurance to low-income individuals.
Factors such as vaccination, mask-wearing, and social distancing were not considered in this study, focusing solely on normal policy and lifestyle choices.
People affiliated with the Democratic Party tend to be more health-conscious, leading healthier lives and having better access to insurance.
Republican counties had 73 more COVID-19 deaths per 100,000 people during the public health issue, indicating a significant impact of political policies on health outcomes.
Republican policies and attitudes like fake masculinity and ignoring health advice contribute to faster mortality rates.
The data collected over nearly 20 years consistently shows that Democratic policies and lifestyle choices lead to longer life expectancy.
Bringing up these findings may be valuable when discussing health choices with those who disregard advice.

Actions:

for health advocates, policymakers,
Share the research findings on how political affiliation affects health outcomes (implied)
Encourage open dialogues and education on the importance of health-conscious decisions (implied)
</details>
<details>
<summary>
2022-06-12: Let's talk about gas prices this summer.... (<a href="https://youtube.com/watch?v=lXBMBPyX1VQ">watch</a> || <a href="/videos/2022/06/12/Lets_talk_about_gas_prices_this_summer">transcript &amp; editable summary</a>)

Inflation is on the rise, with fuel prices expected to climb, impacting everything else, creating economic challenges for the average person while benefiting Wall Street.

</summary>

"It's better to have this information early."
"Now might not be the time to splurge."
"This is not good economic news for the average person."
"Higher prices mean a higher share is going to be profit."
"It's another thing that highlights the disparity between what's good for Wall Street and what's good for Main Street."

### AI summary (High error rate! Edit errors on video page)

Inflation is expected to continue throughout the summer, with economists predicting oil prices to rise to $140 a barrel.
Current prices are based on around $120 a barrel, leading to a ripple effect on fuel and everything else.
The rising fuel prices may result in demand destruction, causing people to make decisions like not driving or vacationing.
Factors like food insecurity, difficulties in obtaining grain, the California drought affecting produce prices, all point towards prices continuing to climb.
It's advised to hold off on splurging and keep some reserve due to the economic outlook.
While the overall economy seems stable, the inflation trend is concerning.
Hope is placed on oil prices declining at the end of summer to balance things out, but there are no guarantees.
The rise in prices is partly attributed to companies aiming for higher profits and pushing prices as far as the market allows.
The economic situation favors Wall Street with higher prices translating to increased profits, but it's detrimental to the average person.
This situation underscores the disparity between what benefits Wall Street and what benefits Main Street.

Actions:

for economic consumers,
Keep a reserve for potential price hikes (suggested)
Stay informed about economic trends and plan spending accordingly (implied)
</details>
<details>
<summary>
2022-06-12: Let's talk about China, pigs, and science fiction.... (<a href="https://youtube.com/watch?v=xI648CdtHOk">watch</a> || <a href="/videos/2022/06/12/Lets_talk_about_China_pigs_and_science_fiction">transcript &amp; editable summary</a>)

Scientists in China are using artificial intelligence to clone pigs en masse for consumption, raising ethical concerns and prompting reflections on innovative solutions for food security and climate change.

</summary>

"This isn't lab-grown meat. This is whole pigs being cloned by artificial intelligence to then be turned into meat."
"The AI is much better at producing the desired result."
"We know that we're going to have to come up with some innovative ways to deal with food security."
"I'm not sure that this is the way to go about it, to be honest."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Scientists in China are cloning pigs en masse using artificial intelligence for consumption due to high demand for pork.
China faces a shortage of pork supply, worsened by sick pigs, prompting the use of AI to clone pigs for food security.
This process involves cloning whole pigs rather than lab-grown meat, raising ethical concerns.
The AI technology is more successful at cloning pigs than humans due to its efficiency in producing desired results.
The complex process of cloning is better executed by AI, as human efforts often result in damaged cells and unsuccessful cloning.
Beau acknowledges the ethical dilemmas and concerns arising from this AI-enabled mass pig cloning process.
The blend of science fiction elements with real-world implications sparks debates and reflections on future technological advancements.
Beau hints at the necessity for innovative solutions to address food security and climate change challenges.
Despite sharing the information, Beau remains unsure if mass pig cloning through AI is the ideal approach to tackling these issues.
Beau expresses his intention to follow up on the story's progress and continue sharing insights with his audience.

Actions:

for environmentalists, ethicists, innovators,
Monitor advancements in food security technology and advocate for sustainable solutions (implied).
Stay informed about ethical implications of AI in food production and participate in relevant debates and discussions (implied).
</details>
<details>
<summary>
2022-06-11: Let's talk about an unintentional act in Parkland.... (<a href="https://youtube.com/watch?v=VMO4Uk7vlWI">watch</a> || <a href="/videos/2022/06/11/Lets_talk_about_an_unintentional_act_in_Parkland">transcript &amp; editable summary</a>)

School resource officer finds guns unattended in school, unintentionally brought in, warning against arming teachers and allowing weapons in schools.

</summary>

"Pushing this on them is a bad idea."
"Allowing these weapons into the schools is a bad idea."
"This will go bad."
"It's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

School resource officer finds guns unattended in a school locker room, belonging to the principal.
The guns were unintentionally brought in, left unsecured in the locker room.
The incident sheds light on the dangers of arming teachers.
If guns are allowed in schools, incidents like this can become frequent and dangerous.
Parents were not notified until 24 hours later.
This incident serves as a warning against arming teachers and bringing weapons into schools.
Allowing firearms in schools poses serious risks.
Incidents of mishandling firearms in schools are not isolated.
People don't understand how to properly secure firearms.
Pushing the responsibility of handling weapons onto teachers is a bad idea.

Actions:

for educators, parents, advocates,
Educate others on the dangers of arming teachers and bringing weapons into schools (implied).
Advocate for proper firearm safety education in schools (implied).
</details>
<details>
<summary>
2022-06-11: Let's talk about Ukraine, grain, and Africa.... (<a href="https://youtube.com/watch?v=zaKMmQegwyM">watch</a> || <a href="/videos/2022/06/11/Lets_talk_about_Ukraine_grain_and_Africa">transcript &amp; editable summary</a>)

Russia's invasion of Ukraine disrupts grain supply, leading to famine in Africa, showcasing the hidden costs of imperialism on vulnerable nations.

</summary>

"The cost of imperialism is a cost that never gets talked about when wealthy countries play their little masters of the universe games."
"The coverage that is going to come out of Africa is directly tied to Putin's invasion of Ukraine."
"The lack of aid in Africa is directly tied to the disruption of the grain supply chain caused by that invasion."
"The sad state of affairs and the reality of the imperialist nature of large powers."
"These games that get played on the foreign policy scene impact countries that are seldom thought of and never visited."

### AI summary (High error rate! Edit errors on video page)

Russia's invasion of Ukraine disrupted the world's grain supply, impacting Africa first, particularly the horn of Africa like Somalia.
The disrupted grain supply combined with a drought in Africa has led to hundreds dying from famine with the situation expected to worsen.
The cost of imperialism is often ignored, with wealthy and powerful countries not considering the consequences on poorer nations.
The lack of aid in Africa is directly linked to Putin's invasion of Ukraine and the resulting disruption in the grain supply chain.
The imperialist actions of powerful countries have far-reaching consequences on vulnerable populations in distant places.
The coverage of Africa's crisis is tied to Putin's invasion, revealing the detrimental effects of global power dynamics.
Aid efforts are hindered by the focus on money and other priorities, leaving many without food and facing dire circumstances.
The foreign policy games played by powerful nations have real and devastating impacts on countries rarely acknowledged or visited.
The situation in Africa serves as a stark reminder of the hidden costs of imperialism and global power struggles.
Beau's reflection serves to shed light on the often overlooked repercussions of international conflicts on the most vulnerable populations.

Actions:

for policy makers, activists, global citizens,
Provide direct aid to famine-affected regions in Africa (suggested)
Raise awareness about the impact of global power struggles on vulnerable populations (implied)
Support organizations working to address food insecurity in Africa (exemplified)
</details>
<details>
<summary>
2022-06-11: Let's talk about Biden, canyons, water bottles, and a plan.... (<a href="https://youtube.com/watch?v=Ov98megg4QY">watch</a> || <a href="/videos/2022/06/11/Lets_talk_about_Biden_canyons_water_bottles_and_a_plan">transcript &amp; editable summary</a>)

The Biden administration's environmental initiatives, including the Hudson Canyon Marine Sanctuary and phasing out single-use plastics, are overshadowed by the comprehensive Ocean Climate Action Plan, focusing on expert-led strategies for resilience and conservation.

</summary>

"We need a plan first."
"If you put a group of experts together and you empower them, they'll produce results."
"That to me is definitely a bigger story than the water bottles."

### AI summary (High error rate! Edit errors on video page)

The Biden administration's focus on the Hudson Canyon Marine Sanctuary and eliminating single-use plastics in national parks is getting media attention.
The Sanctuary, the largest underwater canyon in the US Atlantic, is intended to protect biodiversity and fulfill Biden's campaign promise of protecting 30% of US territory.
The plan includes public comment periods to define protected areas and establish rules.
The initiative to phase out single-use plastics in national parks, including water bottles, by 2032 is part of Biden's climate change strategy.
The bigger story, overshadowed by media focus, is the Ocean Climate Action Plan aiming to address ocean and climate issues comprehensively.
The Plan involves interagency collaboration to develop strategies for resilience, conservation, adaptation, and mitigation, such as green shipping and renewable energy.
Beau stresses the importance of creating a detailed plan before taking action and empowering experts to produce results.

Actions:

for environmental advocates,
Support the public comment period for defining the protected areas of the Hudson Canyon Marine Sanctuary (implied)
Stay informed about the Ocean Climate Action Plan and advocate for comprehensive strategies for resilience and conservation (implied)
</details>
<details>
<summary>
2022-06-10: Let's talk about if medical training is worth it.... (<a href="https://youtube.com/watch?v=2jlT505wB3E">watch</a> || <a href="/videos/2022/06/10/Lets_talk_about_if_medical_training_is_worth_it">transcript &amp; editable summary</a>)

Beau talks about staying positive, providing immediate aid, and never giving up the fight, stressing that every effort counts, even if the odds seem against you.

</summary>

"It absolutely matters."
"You're buying minutes."
"You never give up the fight."
"Move through it because somebody, one of them will surprise you."
"It's worth trying every other time if you just win once."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of staying positive and whether it matters.
Mentions the significance of doing what you can, where you can, and for as long as you can.
Expresses frustration over delays in resolving events that could have led to different outcomes if aid had been applied sooner.
Emphasizes the role of providing aid to buy time for professional medical help to arrive.
Recounts stories illustrating the impact of immediate aid in critical situations.
Advocates for not giving up the fight, even when circumstances seem dire.
Encourages training and preparedness for potentially worsening situations.
Urges to keep trying because winning even once makes all the efforts worth it.

Actions:

for community members,
Provide immediate aid in emergencies (exemplified)
Seek training in first aid and emergency response (suggested)
</details>
<details>
<summary>
2022-06-10: Let's talk about how California's water is your problem.... (<a href="https://youtube.com/watch?v=WzeLrMKDdbY">watch</a> || <a href="/videos/2022/06/10/Lets_talk_about_how_California_s_water_is_your_problem">transcript &amp; editable summary</a>)

Beau warns of the present impacts of climate change, urging urgent innovation and infrastructure changes to combat its effects, stressing the economic necessity of transitioning away from dirty energy.

</summary>

"It's here. It's happening now. We're experiencing the negative impacts right now, this minute."
"We have to innovate. We don't have a choice."
"You're experiencing it now. You're paying for it now."
"We have to alter the infrastructure of the entire country."

### AI summary (High error rate! Edit errors on video page)

California is experiencing a severe drought, with Governor Newsom urging urban residents to reduce water consumption by 15%, yet water usage increased by 19% in March and 18% in April.
Some areas are installing flow restrictors to conserve water due to the worsening drought, impacting private wells in rural areas.
The lack of water in California has led to around 400,000 acres of farmland not being planted, affecting the production of a significant portion of veggies, fruits, and nuts in the US.
The effects of climate change are already evident, with negative impacts being felt in the present moment, despite many choosing to ignore the reality.
It is emphasized that moving away from dirty energy and prioritizing environmental sustainability is not just a moral choice but also an economic imperative.
The drought in California, exacerbated by climate change, underscores the urgent need for innovation, transitioning away from dirty energy, and cutting emissions.
Beau stresses the necessity of making choices now that prioritize sustainable practices and infrastructure development to mitigate the worsening impacts of climate change.
Other states are also facing challenges related to water scarcity, with some like Colorado resorting to cloud seeding as a potential solution.
The push for continued reliance on fossil fuels and drilling as an answer to economic problems is debunked, with Beau advocating for urgent innovation and transitioning to cleaner energy sources.
Ultimately, Beau asserts that the current climate crisis demands immediate action in transforming the country's infrastructure to combat the escalating effects of climate change.

Actions:

for environmental advocates, policymakers,
Transition to sustainable energy sources to combat climate change (implied)
Support innovation and infrastructure development for environmental sustainability (implied)
</details>
<details>
<summary>
2022-06-10: Let's talk about heat domes in the Southwest.... (<a href="https://youtube.com/watch?v=RHXMbTH3jYs">watch</a> || <a href="/videos/2022/06/10/Lets_talk_about_heat_domes_in_the_Southwest">transcript &amp; editable summary</a>)

Be prepared for an imminent heat dome impacting 25 million people in the southwestern US; hydrate, stay cool, and watch out for your neighbors in triple-digit temperatures.

</summary>

"Hydrate, hydrate, hydrate. Drink a lot of water. Not soda, water."
"Don't underestimate the heat. It's a big deal."
"Wear light baggy and light colored and lightweight clothing."
"It causes a lot of loss when you're thinking about it."
"Don't underestimate it. It's serious."

### AI summary (High error rate! Edit errors on video page)

Alerting about an upcoming heat dome over the southwestern United States impacting around 25 million people.
Temperatures expected to be 20 degrees above normal, with many places hitting triple digits, including major cities like Austin, San Antonio, Phoenix, and Vegas.
Warning about the dangers of underestimating extreme heat, citing the loss of 231 lives in British Columbia due to a heat dome last year.
Advising people in the affected areas to stay hydrated by drinking water and utilizing water's cooling effect, like using wet rags or cold compresses.
Suggesting limiting outdoor activities, wearing light and loose clothing, eating light, and checking on neighbors who may struggle in the extreme heat.
Emphasizing the need to adapt and mitigate the impacts of climate change as these extreme heat events become more frequent.
Cautioning that even small temperature increases can have significant consequences in already extreme climates like in Vegas.
Urging people in the southwestern United States to take the heat dome seriously and not underestimate its severity.

Actions:

for residents in the southwestern united states,
Stay hydrated by drinking water and using cooling techniques (suggested)
Limit outdoor activities and wear light clothing (suggested)
Eat light and stay cool in extreme heat (suggested)
Regularly checking on vulnerable neighbors (suggested)
Adapt and mitigate climate change impacts (suggested)
</details>
<details>
<summary>
2022-06-09: Let's talk about where America stands as the hearings start.... (<a href="https://youtube.com/watch?v=vJq4SYCBeR8">watch</a> || <a href="/videos/2022/06/09/Lets_talk_about_where_America_stands_as_the_hearings_start">transcript &amp; editable summary</a>)

More than half of Americans believe Trump should face criminal charges post-January 6th, potentially impacting his political future.

</summary>

"More than half of Americans see criminal liability for the former president."
"Those who believe he should be charged are probably really unlikely to believe he should be in charge."

### AI summary (High error rate! Edit errors on video page)

Taking stock before hearings on Trump's actions post-January 6th insurrection.
Initial poll post-January 6th: 54% of Americans believed Trump should face criminal charges.
Recent poll shows 52% still support criminal charges against Trump.
Only 42% believe Trump should not be charged.
Committee can't charge Trump but can refer to Department of Justice.
Rumors suggest committee wants consensus before making referral.
Polls may influence committee's decision on referral.
Department of Justice may or may not prosecute after referral.
Majority of Americans see criminal liability for Trump.
Committee's actions may lead numbers supporting criminal charges to increase.
Trump's desire for White House may be cooled by public opinion supporting criminal charges.
Trump's efforts as Republican kingmaker have not gained significant traction.
More than half of Americans believing Trump should be charged does not bode well for his presidential ambitions.
Public perception and committee's presentation to determine future course of action.

Actions:

for concerned citizens, political observers.,
Monitor and participate in future polls to gauge public sentiment on Trump's actions (implied).
</details>
<details>
<summary>
2022-06-09: Let's talk about the House gun control bill.... (<a href="https://youtube.com/watch?v=PlMqCWFx9Dc">watch</a> || <a href="/videos/2022/06/09/Lets_talk_about_the_House_gun_control_bill">transcript &amp; editable summary</a>)

The House passed a bill with various unrelated components, including a section on banning large capacity ammunition feeding devices, but its chances of passing the Senate are slim due to the broad scope and definitions.

</summary>

"The House did something."
"I can't believe anybody actually thought that was going to get through the Senate."
"It's thoughts and prayers."
"But hey, now they get to say, hey, we tried."
"They clumped all this stuff together and sent it all up at once."

### AI summary (High error rate! Edit errors on video page)

The House passed a bill that is a collection of unrelated components addressing various issues.
The bill contains both good and bad elements, with some parts being effective and others pointless.
One particular section of the bill focuses on banning large capacity ammunition feeding devices.
Despite exemptions and loopholes, the bill faces challenges in the Senate and is unlikely to pass.
The definition of "large capacity ammunition feeding device" is critical, limiting magazines to 10 rounds in the bill.
Beau believes the bill is already defeated in the Senate, with slim chances of passing.
Combining different elements into one bill may hinder its overall success in legislation.
Addressing issues separately could have increased the bill's chances of approval.
The bill includes provisions for grants for safe storage, encouraging firearm security measures.

Actions:

for legislative observers,
Advocate for bills to be broken down into separate components for better chances of approval (implied)
Support grants for safe storage initiatives to encourage firearm security (implied)
Stay informed and engaged in legislative processes to understand the impact of bills (implied)
</details>
<details>
<summary>
2022-06-09: Let's talk about Fox not airing the committee.... (<a href="https://youtube.com/watch?v=8-MlH_yoRG8">watch</a> || <a href="/videos/2022/06/09/Lets_talk_about_Fox_not_airing_the_committee">transcript &amp; editable summary</a>)

Fox News avoids airing committee hearings to maintain false narratives, urging viewers to rely on their biased coverage instead of seeking the truth.

</summary>

"Don't get sidetracked. Allow the committee to do their job and present their evidence."
"Do not let Fox News determine what gets talked about on social media."

### AI summary (High error rate! Edit errors on video page)

Fox News won't air the committee hearing due to false talking points and easily disproven facts.
People shouldn't be surprised that Fox News acts as a PR wing for the Republican Party.
If Fox News truly believed in a partisan witch hunt, they should broadcast it with a live fact checker.
Fox News likely won't fact-check because they may lack fact-checkers and anticipate no debunked information.
Fox's strategy is to indoctrinate viewers to rely solely on them for talking points.
Fox may manipulate clips from the committee to fit their narrative and lack evidence in their arguments.
Beau advises not to interact with misleading arguments on social media.
The focus should remain on determining accountability for the events of the day.
Beau believes the committee may present surprising evidence kept hidden so far.
It's vital not to let Fox News control the social media narrative; focus on getting to the truth.
The public must support holding those responsible accountable to avoid negative consequences.

Actions:

for media consumers,
Support the committee's process and allow them to present their evidence (suggested)
Avoid engaging with misleading arguments on social media (implied)
</details>
<details>
<summary>
2022-06-08: Let's talk about the DHS bulletin.... (<a href="https://youtube.com/watch?v=3hNvqU9UV54">watch</a> || <a href="/videos/2022/06/08/Lets_talk_about_the_DHS_bulletin">transcript &amp; editable summary</a>)

A DHS bulletin warns of an elevated risk until November 30th, pinpointing potential catalysts for violence and urging Americans to stay alert and prepared in crowded, low-security locations.

</summary>

"It's Americans that are going to rip the country apart."
"Know where you're at and how to get out."
"Be aware, be alert, know how to get out of the location that you're in."
"That's going to be the most likely place anyway."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A DHS bulletin warns of an elevated risk until November 30th, pointing out certain events that may serve as catalysts for violence.
Foreign groups are a passing mention in the bulletin, with Americans being seen as the main potential threat to societal stability.
The catalysts identified include general racism, xenophobia, Supreme Court decisions, and the upcoming election.
Hostile foreign nations are amplifying conspiracy theories that could incite violent events in the U.S.
The bulletin predicts that potential events will target soft, crowded, low-security locations.
Beau finds this bulletin to be accurate, in alignment with private assessments except for the frequency of potential catalyst events.
The key advice given is to stay aware, be alert, know your surroundings, and have an exit plan without succumbing to fear.

Actions:

for citizens,
Stay aware and alert in crowded, low-security locations (implied).
Know your surroundings and have an exit plan (implied).
</details>
<details>
<summary>
2022-06-08: Let's talk about cats, seals, and teachers.... (<a href="https://youtube.com/watch?v=ZMAosp6c2no">watch</a> || <a href="/videos/2022/06/08/Lets_talk_about_cats_seals_and_teachers">transcript &amp; editable summary</a>)

Beau explains the basics of using tourniquets and chest seals for emergencies, stressing the need for proper training and continuous learning.

</summary>

"This isn't training. You need real training."
"You want to stay up to date because what is considered best practices changes over time."
"This is how you become a superhero. This is how you save lives."
"Please get more training. This isn't enough."
"You can never have enough gauze. Ever."

### AI summary (High error rate! Edit errors on video page)

Explains how to use tourniquets and chest seals for emergency situations.
Emphasizes the importance of proper training and staying updated on best practices.
Shows the differences between various tourniquet types, like the Cat Generation 7.
Advises on storing equipment securely and conveniently for easy access during emergencies.
Demonstrates how to apply a tourniquet correctly and efficiently.
Describes the purpose and application of a chest seal to prevent air from entering the chest cavity.
Urges the audience to seek more comprehensive training beyond the basics he covers.
Gives practical tips on how to improvise with available materials.
Encourages taking courses like Stop the Bleed to enhance emergency response skills.
Stresses the importance of continuous learning and preparedness in life-threatening situations.

Actions:

for emergency responders, teachers, concerned citizens,
Take a Stop the Bleed course (suggested)
Ensure secure and accessible storage for emergency equipment (implied)
Seek additional training beyond the basics covered (implied)
</details>
<details>
<summary>
2022-06-08: Let's talk about Socrates, gamers, and you.... (<a href="https://youtube.com/watch?v=X-WjO4c5V-c">watch</a> || <a href="/videos/2022/06/08/Lets_talk_about_Socrates_gamers_and_you">transcript &amp; editable summary</a>)

Beau shares insights on applying the triple filter test to prevent spreading damaging information and gossip, particularly relevant for those involved in organizing efforts.

</summary>

"Is it true? Is it moral? And is it necessary?"
"Your ego can often cause you to talk a little bit more than you should."
"Does that personality conflict help or hinder you?"
"There are large organizing efforts that are being really hindered by people not applying that triple filter."
"It's something that could be damaging over the long haul."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of the triple filter test attributed to Socrates, questioning if statements are true, good, and useful before speaking.
Shares a modified version of the triple filter test: questioning if statements are true, moral, and necessary before speaking.
Emphasizes the importance of filtering information before spreading gossip, especially in real-life organizing.
Mentions a specific group of players of the video game War Thunder who leaked classified information on military equipment in forums.
Recounts instances where schematics of military equipment like the Challenger tank and France's main battle tank were leaked due to personal disputes within the game community.
Advises individuals, especially those involved in organizing, to avoid engaging in unnecessary conflicts or gossip that may hinder their mission.
Raises awareness about the impact of personality conflicts and gossip on organizing efforts across the country.
Stresses the significance of applying the triple filter test to avoid damaging consequences in the long run.

Actions:

for organizers, gamers, activists,
Apply the triple filter test in your communication and decision-making processes (implied).
Avoid engaging in unnecessary conflicts and spreading gossip that may hinder collective efforts (implied).
Prioritize the mission and goals over personal conflicts and ego-driven interactions (implied).
</details>
<details>
<summary>
2022-06-07: Let's talk about the committee's challenges.... (<a href="https://youtube.com/watch?v=QNljbzOotfA">watch</a> || <a href="/videos/2022/06/07/Lets_talk_about_the_committee_s_challenges">transcript &amp; editable summary</a>)

Committee faces the challenge of distilling a complex event for the American public, stressing the importance of storytelling and simplicity to avoid undermining accountability efforts.

</summary>

"Tell them what you're going to tell them. Tell them what you told them."
"People will be watching this, looking for anything to validate their preconceived notions."
"Be ready to absorb the whole story and look at all of the connections."

### AI summary (High error rate! Edit errors on video page)

Committee preparing for a public hearing to enlighten the American populace on a complex event.
Multiple factions colliding and working together towards a common goal.
Committee's challenge to distill and explain the complex event to the American people before they lose interest.
Importance of storytelling by telling the audience what they are going to hear, telling them, and then summarizing.
Risks involved in deviating from the storytelling plan and getting too complex.
Need for simplicity to avoid undermining the investigations and pursuit of accountability.
Emphasizes staying informed about new developments as politically oriented investigations may bring surprising moments.
Warning about potential defense tactics by those trying to justify the actions.
Stress on absorbing the whole story and understanding all connections.
The importance of the committee turning the investigation into a consumable story for it to be effective.

Actions:

for committee members, investigators, public,
Stay informed about new developments to understand the full story (implied)
Be ready to challenge preconceived notions when surprising moments arise (implied)
</details>
<details>
<summary>
2022-06-07: Let's talk about beliefs and the test of time.... (<a href="https://youtube.com/watch?v=2aemgKflFHQ">watch</a> || <a href="/videos/2022/06/07/Lets_talk_about_beliefs_and_the_test_of_time">transcript &amp; editable summary</a>)

Beau explains how time exposes flaws in beliefs, leading to eventual judgment of present ideas as primitive, urging reflection on historical figures.

</summary>

"Nobody can stand the test of time because humanity marches forward."
"I cannot wait until the day when I am viewed as a conservative."
"The further humanity progresses, the worse we're going to look."
"Time will march on and our ideas will be 100% bad takes."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talks about the concept of time and how it exposes the flaws in beliefs.
Mentions how people from the past and present hold backward beliefs, which will be judged in the future.
Expresses anticipation for being viewed as conservative in the future due to outdated beliefs.
Speculates on future beliefs, such as society viewing current treatment of animals and the earth as barbaric.
Points out the habit of ignoring issues in other countries and prioritizing personal gain.
Emphasizes that humanity's progression will make current beliefs seem primitive in the future.
Encourages reflection on historical figures and the inevitability of current ideas being judged poorly in the future.

Actions:

for philosophical thinkers,
Challenge backward beliefs (implied)
Prioritize fixing societal issues over personal gain (implied)
Educate others on the importance of understanding and addressing issues (implied)
</details>
<details>
<summary>
2022-06-07: Let's talk about Lincoln the conservative.... (<a href="https://youtube.com/watch?v=u7S_0uYy9RY">watch</a> || <a href="/videos/2022/06/07/Lets_talk_about_Lincoln_the_conservative">transcript &amp; editable summary</a>)

Beau challenges misconceptions by illustrating President Lincoln's progressive legacy and dispelling the notion that conservatives can claim him.

</summary>

"Lincoln was a progressive, not a conservative."
"There is no way for the conservatives of today to claim him."
"Lincoln got fan mail from Karl Marx."
"He was a begrudging, pragmatic progressive."
"Lincoln is responsible for one of the single most progressive moments in American history."

### AI summary (High error rate! Edit errors on video page)

President Lincoln was both a conservative and a progressive, with a key message challenging the assumption that conservatives want the status quo.
Lincoln, despite being a Republican, was a progressive figure who fundamentally changed American society.
Lincoln's leadership played a significant role in recognizing people as individuals legally, marking a progressive position in history.
The common equating of Republicans with conservatives is misconstrued due to historical shifts in party ideologies.
A letter from Karl Marx congratulating Lincoln on his reelection indicates Lincoln's progressive stance.
The claim that Lincoln was a conservative is historically inaccurate, given his progressive actions and views.
The Union flag under Lincoln represented progressives, not conservatives.
The Republican party during Lincoln's time was more progressive than conservative.
Lincoln's legacy as a progressive figure is undeniable, especially considering his impact on American history.
The narrative that conservatives can claim Lincoln's legacy is debunked due to his progressive ideals and actions.

Actions:

for history enthusiasts,
Research and understand the historical context of President Lincoln's progressive actions (suggested)
Engage in discourse about historical figures and their political ideologies (exemplified)
</details>
<details>
<summary>
2022-06-06: Let's talk about what one conservative institution can teach another.... (<a href="https://youtube.com/watch?v=Re-WnUYY1WA">watch</a> || <a href="/videos/2022/06/06/Lets_talk_about_what_one_conservative_institution_can_teach_another">transcript &amp; editable summary</a>)

A warning to conservative institutions: alienating the majority through extreme positions risks losing engagement and could lead to a party split.

</summary>

"The longer this goes on, the less likely the majority of people are to be involved with that institution."
"Scapegoating people, it energizes an ever-shrinking minority of people."
"You're going to start to see the same type of thing play out within the Republican Party."

### AI summary (High error rate! Edit errors on video page)

A poll of Catholics reveals surprising departures from church leadership on key issues like Roe v. Wade and LGBTQ+ communion.
68% of Catholics want Roe v. Wade upheld, contrasting with the US Conference of Catholic Bishops' call to pray for its overturn.
77% believe LGBTQ+ individuals should receive communion, and 65% support openly gay priests.
Despite these views, 68% of Catholics attend services less than once a month, with decreasing church attendance and worsening opinions.
Beau suggests that conservative institutions, like the Republican Party, are losing touch by promoting extreme positions to energize a shrinking base.
The strategy of scapegoating certain demographics for political gain may alienate the majority and lead to decreased engagement.
Older individuals, who tend to support socially regressive positions, may age out in a few years, affecting the party dynamics.
Beau predicts a potential split within the Republican Party as normal conservatives may seek alternatives rather than joining the Democrats.
Most Republicans view Democrats as left-wing, but understanding their similarities to past Republicans could shift perceptions.
Beau warns that in five years, the implications of current party strategies will become significant, leading to potential shifts within the Republican Party.

Actions:

for conservative voters,
Re-evaluate messaging and policies to avoid alienating the majority (implied)
Encourage open dialogues within conservative institutions to address shifting views (implied)
</details>
<details>
<summary>
2022-06-06: Let's talk about unemployment, Wall Street, and Main Street.... (<a href="https://youtube.com/watch?v=quG_sg3Rd-0">watch</a> || <a href="/videos/2022/06/06/Lets_talk_about_unemployment_Wall_Street_and_Main_Street">transcript &amp; editable summary</a>)

Big business is worried about low unemployment leading to higher wages, illustrating the disconnect between Wall Street and Main Street.

</summary>

"What's best for Wall Street may not be what's best for Main Street."
"If your company is posting record profits, paying your employees more is just the thing you're supposed to do."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the economy, unemployment, inflation, and concerns of big business.
Despite global inflation issues, the United States economy is doing well.
Big business is worried that the economy might be doing too well because of low unemployment rates.
Employers have to compete for workers when unemployment is low, leading to higher wages.
Big business is concerned that paying employees more will lead to inflation.
Beau criticizes big businesses for not paying their employees more despite posting record profits.
The media overlooks the fact that what benefits Wall Street might not be best for Main Street.
Big business is worried about having to pay a living wage and raise their overhead costs.
The system in the US operates on a carrot-and-stick approach for social mobility.
Big business wants more people without jobs to avoid paying higher wages.

Actions:

for workers, general public,
Advocate for fair wages for all workers (suggested)
Support policies that prioritize workers over corporate profits (suggested)
</details>
<details>
<summary>
2022-06-06: Let's talk about recent Biden Foreign Policy moves.... (<a href="https://youtube.com/watch?v=gIlszGbZPvc">watch</a> || <a href="/videos/2022/06/06/Lets_talk_about_recent_Biden_Foreign_Policy_moves">transcript &amp; editable summary</a>)

Beau examines Biden's foreign policy moves in a high-stakes international poker game, advocating for open communication over isolation.

</summary>

"Sometimes it is best to show your hand a little bit, take a hard line, call."
"Engage in a show of force, take a hard line, call."
"It's a friendly game right now. Everybody have a seat."

### AI summary (High error rate! Edit errors on video page)

Examines Biden's foreign policy in the context of international relations.
Mentions three separate foreign policy events: NATO exercise, North Korea missile launch, and Summit of Americas.
NATO exercise involves 7,000 troops, 45 ships, 75 planes, and multiple countries.
Views these events as demonstrating resolve or showing lines to adversarial nations.
Questions Biden administration's decision to exclude Cuba, Venezuela, and Nicaragua from the Summit of Americas.
Suggests that engaging in diplomatic talks with non-adversarial nations is more beneficial.
Points out the importance of including all nations in dialogues, even if not allies.
Criticizes the administration's approach in trying to isolate certain countries.
Expresses doubt in the effectiveness of the administration's strategy in the international poker game.
Advocates for open communication and engagement over isolation in foreign policy decisions.

Actions:

for foreign policy analysts,
Engage in diplomatic talks with non-adversarial nations (suggested).
Advocate for inclusive dialogues in international relations (implied).
</details>
<details>
<summary>
2022-06-05: Let's talk about what it means to be a conservative.... (<a href="https://youtube.com/watch?v=pTibmKAakbU">watch</a> || <a href="/videos/2022/06/05/Lets_talk_about_what_it_means_to_be_a_conservative">transcript &amp; editable summary</a>)

Being conservative means defending the status quo and establishment, resisting change, and not truly supporting a better world.

</summary>

"If you are a conservative, by default you support the Pelosi's of this world."
"The worst reason in the world to do something is we've always done it that way."

### AI summary (High error rate! Edit errors on video page)

Defines what it means to be a conservative, focusing on maintaining the status quo and traditional values.
Points out that being conservative means defending the establishment and resisting change.
Notes the irony in conservatives claiming to want to restore the Constitution while opposing change.
Challenges the idea of wanting things to remain as they were in the past as defending the status quo.
Emphasizes that conservativism inherently supports the current system and establishments.
Urges letting go of the past and embracing change for improvement.

Actions:

for political activists,
Challenge traditional views (implied)
Embrace change for improvement (implied)
</details>
<details>
<summary>
2022-06-05: Let's talk about an analogy to help people understand the climate.... (<a href="https://youtube.com/watch?v=pRD66T8xzag">watch</a> || <a href="/videos/2022/06/05/Lets_talk_about_an_analogy_to_help_people_understand_the_climate">transcript &amp; editable summary</a>)

Beau addresses the urgent need for immediate action on escalating carbon dioxide levels, warning against delaying global mobilization to combat the environmental crisis.

</summary>

"Human civilization has never seen this level."
"The time for action was yesterday."
"Going down this road is being an appeaser. It's siding with the bad guys."
"You're on the wrong team, and history will judge you for it."
"The longer it gets ignored, the worse it will be, just like back then."

### AI summary (High error rate! Edit errors on video page)

Addresses the current state and future projections regarding carbon dioxide levels.
Emphasizes that human civilization has never experienced the current carbon dioxide levels before.
Mentions the significant impact of high carbon dioxide levels on sea levels, vegetation, and crops.
Points out that despite taking action, the situation will continue to worsen due to past milestones being surpassed.
Compares those ignoring the environmental crisis to appeasers in the 1930s, suggesting that delaying action worsens the outcome.
Calls for a global mobilization similar to World War II to address the environmental crisis.
Stresses the urgency of taking action immediately and not waiting for tomorrow.
Criticizes the denial of the crisis and the insistence on outdated technologies and dirty energy sources.
Warns that siding with inaction is akin to being on the wrong side of history.

Actions:

for global citizens,
Mobilize globally to address the environmental crisis (implied)
</details>
<details>
<summary>
2022-06-05: Let's talk about OPEC and gas prices.... (<a href="https://youtube.com/watch?v=Q1WNTbqLz0I">watch</a> || <a href="/videos/2022/06/05/Lets_talk_about_OPEC_and_gas_prices">transcript &amp; editable summary</a>)

OPEC's attempt to increase oil production may not alleviate high prices, potentially driving more towards electric vehicles and impacting the market shift.

</summary>

"The ultimate irony in this is that this extended period of high gas prices is a pressure pushing people away from their product."
"The more people purchase electric vehicles, the less expensive they become, the more of them enter the secondary market, and it just kind of snowballs."
"It's not really a silver lining, it's just something that I've noticed and been thinking about since they made the announcement that they were going to put out an additional 600,000 barrels a day."

### AI summary (High error rate! Edit errors on video page)

OPEC plans to increase oil production by over 600,000 barrels per day, but it won't likely impact prices significantly due to other global factors.
OPEC previously reduced production during the pandemic to maintain high prices artificially, leading to the current situation of high gas prices.
The prolonged period of high gas prices may drive more people towards electric vehicles as an alternative.
Consumers may opt for electric cars over traditional gasoline vehicles to avoid dependency on OPEC and potential future price manipulations.
OPEC's influence on the oil market has diminished, and their attempts to control prices may backfire, leading people to shift away from oil and gas.
The push towards electric vehicles could result in a positive environmental impact and trigger a significant shift in the market.
Beau expresses dissatisfaction with high gas prices but acknowledges the potential positive outcome of more people transitioning to electric vehicles.
The increase in OPEC's oil production may not lead to immediate relief in gas prices, as historical trends indicate their struggles in meeting production targets.
Issues within OPEC countries and production challenges may hinder their ability to achieve the stated production goals effectively.
Beau remains skeptical about OPEC's ability to stabilize gas prices despite the announced production increase.

Actions:

for consumers, environmentalists,
Research and invest in electric vehicles to reduce dependency on oil (exemplified)
Advocate for sustainable transportation options in the community (exemplified)
</details>
<details>
<summary>
2022-06-04: Let's talk about time marching on and a secret I kept for thirty years.... (<a href="https://youtube.com/watch?v=7IqLlfBBkH8">watch</a> || <a href="/videos/2022/06/04/Lets_talk_about_time_marching_on_and_a_secret_I_kept_for_thirty_years">transcript &amp; editable summary</a>)

Beau in Tennessee shares a poignant story of past prejudice and hope for a better future, reminding us that hate fades with time.

</summary>

"There's obvious parallels between that and the attitudes of a lot of parents today."
"On a long enough timeline, we win."
"Each generation is better than the last."

### AI summary (High error rate! Edit errors on video page)

Growing up in Tennessee in the early 90s, he befriended a girl in the neighborhood and developed a crush on her.
The girl's father was irrationally angry and controlling, leading to uncomfortable situations.
During a date to the movies with the girl, they watched "The Bodyguard," which caused her father to explode in anger afterward.
The father's outburst was triggered by the interracial relationship portrayed in the movie, leading to a tense car ride home.
The father expressed racist views and used derogatory terms, making Beau extremely uncomfortable.
Following this incident, the girl's father started driving her to school, changing their dynamic.
Despite the uncomfortable experience, Beau never spoke about it until now, recognizing parallels with modern parenting attitudes.
Beau reconnects with the girl years later through social media and learns that she married a black man, a fact that he imagines upset her father greatly.
Beau concludes with a message of hope, suggesting that over time, hate and prejudice diminish as each generation progresses.

Actions:

for parents, educators, youth advocates,
Support interracial relationships openly and actively in your community (implied).
Challenge and address racist beliefs and behaviors when encountered (implied).
</details>
<details>
<summary>
2022-06-04: Let's talk about masculinity and whether it's toxic.... (<a href="https://youtube.com/watch?v=5_Dc_A-REhM">watch</a> || <a href="/videos/2022/06/04/Lets_talk_about_masculinity_and_whether_it_s_toxic">transcript &amp; editable summary</a>)

Exploring the roots of toxic masculinity, Beau delves into the importance of embracing both the nurturing and assertive aspects of masculinity to combat damaging stereotypes.

</summary>

"Toxic masculinity is the exact opposite. You're throwing away the parts that you should fill yourself with and you're just eating the part that will eventually kill you."
"Men want to be like that. People like Patrick Henry, you know, give me liberty or give me death."
"Those aspects, the creative, nurturing side, that gets tossed away and you're just left with the violence, with the tough guy image."
"Focusing on a certain part of it [masculinity] is [toxic]. It's spoiled milk. Not all milk is spoiled."
"We're focusing on this shallow part and amplifying it, becoming hyper-masculine, toxically masculine, and we're missing the deep masculine."

### AI summary (High error rate! Edit errors on video page)

Masculinity in the US is often shaped by historical figures and popular culture, with men aspiring to emulate heroes like Patrick Henry, Paul Revere, and Alvin York.
Focusing solely on the superficial aspects of masculinity can lead to toxic behavior, characterized by amplifying hyper-masculinity and neglecting the deep, nurturing side.
The concept of toxic masculinity does not imply that all masculinity is toxic; rather, it points to the dangers of fixating on specific negative traits.
The term "toxic masculinity" did not originate from feminists but actually emerged from a men's movement known as mythopoetics, aiming to redefine masculinity.
Business interests and popular culture often perpetuate shallow and harmful stereotypes of masculinity, promoting a skewed tough guy image.
Beau references the movie "Fight Club" as an example of popular culture that may be misinterpreted as celebrating hyper-masculinity while actually critiquing it.
Many historical masculine icons were multifaceted individuals with creative talents and nurturing qualities, often overshadowed by their more aggressive personas.
Beau suggests that toxic masculinity is akin to consuming poisonous parts of a delicacy, discarding the valuable aspects that should be embraced.
The damaging effects of toxic masculinity are felt not only by men but by society as a whole, perpetuating harmful stereotypes and behaviors.
Beau encourages a deeper exploration of masculinity beyond superficial portrayals, advocating for a more balanced and inclusive understanding of what it means to be masculine.

Actions:

for men, masculinity explorers,
Challenge toxic masculinity in your circles by embracing a more balanced and inclusive definition of masculinity (implied).
Support men's movements that aim to redefine masculinity beyond toxic stereotypes (exemplified).
</details>
<details>
<summary>
2022-06-04: Let's talk about NY thinking they banned body armor.... (<a href="https://youtube.com/watch?v=vBVMrIHuY3M">watch</a> || <a href="/videos/2022/06/04/Lets_talk_about_NY_thinking_they_banned_body_armor">transcript &amp; editable summary</a>)

New York's legislation on body armor lacks understanding of modern armor, rendering it ineffective and symbolic rather than practical.

</summary>

"Your legislation will be worthless if you don't actually understand the subject matter."
"This legislation was designed to be a feel-good measure."
"It's just a thought."
"Understanding subject matter is vital for effective legislation."
"Lack of understanding led to passing legislation that bans something rarely used."

### AI summary (High error rate! Edit errors on video page)

New York may need subject matter experts for legislation due to recent headlines about banning body armor.
The bill defines body armor as soft, obsolete Kevlar, not modern hard plate armor.
Regulating hard body armor like AR-500 steel is challenging and impractical.
Legislation banning body armor seems to be a symbolic, ineffective measure.
Understanding subject matter is vital for effective legislation.
The bill is on its way to the governor despite banning outdated body armor.
Modern body armor consists of hard plates, not soft, multi-layer fabric.
Soft body armor became obsolete in the early 2000s due to its ineffectiveness against rifles.
Legislation will be ineffective if it does not grasp the subject matter.
Lack of understanding led to passing legislation that bans something rarely used.

Actions:

for legislators, policymakers, concerned citizens,
Contact subject matter experts to inform legislation (suggested)
Advocate for more comprehensive and informed legislation (suggested)
</details>
<details>
<summary>
2022-06-03: Let's talk about why they think arming teachers will work.... (<a href="https://youtube.com/watch?v=FVZ1c9O2L6Y">watch</a> || <a href="/videos/2022/06/03/Lets_talk_about_why_they_think_arming_teachers_will_work">transcript &amp; editable summary</a>)

Ohio's plan to arm teachers with minimal training doesn't address the root issue of school safety, posing more risks than benefits.

</summary>

"This scenario you have constructed is a fantasy that begins after the bad part happened."
"It reinforces the locked doors. Sure, but it doesn't stop the problem."
"It doesn't actually stop anything. It just reinforces what's already happening."
"This isn't a solution."
"Is it really a net win?"

### AI summary (High error rate! Edit errors on video page)

Ohio is considering arming teachers with 24 hours of training, which slightly changed Beau's opinion on the matter.
Beau's previous perception was that those supporting arming teachers didn't understand that most people aren't proficient in firearms.
Beau associated advocates of arming teachers with being proficient shooters themselves.
Beau encountered a local deputy known for his de-escalation skills, likened to Andy Griffith.
The deputy, a former Marine Special Operations member, dismissed the idea of arming teachers as a bad one in a colorful manner.
Beau questioned the practicality of the proposed solution of teachers standing by doors with students lined up against walls.
The proposed plan lacks depth and effectiveness, requiring minimal training and no firearms.
Beau emphasized the unrealistic nature of the proposed scenario in an active shooter situation.
Beau stressed the importance of law enforcement robbing shooters of the initiative upon entry to prevent further harm.
The proposed solution of arming teachers merely serves as an alarm and does not address the root issue.

Actions:

for educators, policymakers, concerned citizens,
Question the effectiveness and practicality of proposed school safety measures (suggested)
Advocate for comprehensive solutions addressing the root causes of school violence (implied)
</details>
<details>
<summary>
2022-06-03: Let's talk about when a bee isn't a bee but a fish.... (<a href="https://youtube.com/watch?v=wMiPyi3026k">watch</a> || <a href="/videos/2022/06/03/Lets_talk_about_when_a_bee_isn_t_a_bee_but_a_fish">transcript &amp; editable summary</a>)

California ruled bees as fish under their Endangered Species Act, providing protection and a touch of humor in an interesting legal twist.

</summary>

"bees are fish."
"bees in California will now be protected again."
"invertebrate is already listed."
"It's another little tool that should be pretty helpful."
"it's just a thought."

### AI summary (High error rate! Edit errors on video page)

California ruled bees are fish under the Endangered Species Act due to legal definitions.
Bees were classified as fish because they fall under the invertebrate category.
The appeals court's ruling regarding bees as fish under the Endangered Species Act in California is not a new concept.
Land-dwelling invertebrates have been protected similarly since the 1980s.
The legislature declined to clear up the confusion regarding bees being classified as fish.
Bees in California will now receive protection under the state act, especially from intentional harm.
This ruling provides an interesting backstory with some legal shenanigans involved.
The decision may serve as a helpful tool to protect bees from harm in the state.
The ruling adds a touch of humor to the situation, potentially softening the blow for large agriculture companies.
The protection of bees under this ruling is a positive outcome.

Actions:

for california residents, environmentalists, wildlife advocates,
Support local initiatives to protect bees (implied)
Take steps to prevent intentional harm to bees and their habitats (implied)
</details>
<details>
<summary>
2022-06-03: Let's talk about teachers in Texas and Ted Cruz.... (<a href="https://youtube.com/watch?v=5nT6LHDMO70">watch</a> || <a href="/videos/2022/06/03/Lets_talk_about_teachers_in_Texas_and_Ted_Cruz">transcript &amp; editable summary</a>)

Teachers in Texas resist Cruz's militarized approach, questioning the burden on educators tasked with protecting children.

</summary>

"Teachers are asking you to be soldiers, not police."
"He's asking you to be a teacher on top of five other jobs in the military."
"I don't think that our teachers need to be first responders and rangers and everything else that we're asking them to be."
"He is quite literally a meme for running away from a flurry of snowflakes."
"We're already asking them to literally shield our children with their bodies."

### AI summary (High error rate! Edit errors on video page)

Teachers in Texas are taking issue with Ted Cruz's approach of hardening schools and arming teachers.
Teachers are asking to be soldiers, not cops, as the police didn't prove effective.
Being a teacher in this scenario is likened to being a soldier in combat, requiring extensive training.
Dealing with students' issues may involve sending them to specialists with weeks of training.
Teachers are being asked to undertake roles that in the military require multiple individuals.
Ted Cruz is promoting arming teachers, essentially asking them to take on multiple military roles.
Cruz's approach puts teachers in the position of being first responders and asks too much of them.
Cruz's history and actions are critiqued, questioning his election and character.
Teachers are already tasked with significant responsibilities, including shielding children with their bodies.
Beau urges for a reconsideration of the extensive tasks being placed on teachers.

Actions:

for teachers, educators, activists,
Question the militarization of schools and the burden placed on teachers (implied)
Advocate for better policies that prioritize student safety without burdening teachers with excessive responsibilities (implied)
</details>
<details>
<summary>
2022-06-02: Let's talk about the Trump Train rolling on.... (<a href="https://youtube.com/watch?v=ImWvy7iKToE">watch</a> || <a href="/videos/2022/06/02/Lets_talk_about_the_Trump_Train_rolling_on">transcript &amp; editable summary</a>)

Trump is pushing away key supporters, making rebranding challenging and causing a divide among his base.

</summary>

"Trump's political career has seen people falling off the train or being run over."
"Trump wants their support but doesn't want to be publicly associated with them."
"Removing key elements from his core base will make rebranding difficult for Trump."

### AI summary (High error rate! Edit errors on video page)

Trump's political career has seen people falling off the train or being run over.
At a recent rally, a new dress code banned logos associated with Q and the Proud Boys.
People with VIP tickets from the Trump campaign were denied entry for wearing banned logos.
Trump no longer finds groups like QAnon and Proud Boys beneficial, so they must hide their identities or disappear.
Trump wants their support but doesn't want to be publicly associated with them, hence the ban on logos.
Private security at the rally indicated frustration with a hand gesture, industry code for aggression.
Removing key elements from his core base will make rebranding difficult for Trump.
Some supporters will understand the optics, while others may be disheartened by Trump's actions.
Trump is pushing away certain supporters, surprising those who believed he was on their side.
The scenario raises questions about the demographic Trump will target to replace these supporters.

Actions:

for political analysts, trump supporters,
Analyze shifting political demographics to understand potential impact (implied)
Stay informed about political developments to make informed decisions (implied)
</details>
<details>
<summary>
2022-06-02: Let's talk about some big news from the G7.... (<a href="https://youtube.com/watch?v=D71BdN51hlY">watch</a> || <a href="/videos/2022/06/02/Lets_talk_about_some_big_news_from_the_G7">transcript &amp; editable summary</a>)

G7 countries agree to cut funding for overseas fossil fuels, shift to clean energy, and push for coal phase-out without definitive dates, prompting a global impatience with environmental commitments.

</summary>

"No more gas, coal, oil, anything like that, starting from the end of this year."
"Coal is over. I wouldn't start a career in coal right now."
"International poker game where everybody's cheating."
"Overproduction of green energy to the point where they have to slow it down, that's good news."
"A lot of other countries are tired of waiting on the United States."

### AI summary (High error rate! Edit errors on video page)

The G7 countries have agreed to stop funding overseas development of fossil fuels by the end of this year, transitioning $33 billion from dirty energy to clean energy.
One significant agreement is getting coal out of the electricity sectors, with five countries aiming for 2030 as the end date, while the United States and Japan pushed for 2035.
Despite the pushback, the final agreement lacks a specific date, turning into an international poker game where countries are likely using each other's actions to pressure the US and Japan.
The United Kingdom recently produced an excess of wind electricity, leading to turbines being slowed down due to storage limitations, showing progress in green energy production.
The collective stance of other countries to move forward without waiting on the US signals a global impatience with delays in environmental commitments.

Actions:

for environmental advocates, policymakers,
Advocate for clean energy initiatives in your community to push for a faster transition away from coal and fossil fuels (implied).
Support and participate in local renewable energy projects to reduce reliance on dirty energy sources (implied).
</details>
<details>
<summary>
2022-06-02: Let's talk about 12 of your questions.... (<a href="https://youtube.com/watch?v=mLMuQu8wIZM">watch</a> || <a href="/videos/2022/06/02/Lets_talk_about_12_of_your_questions">transcript &amp; editable summary</a>)

Beau bulk addresses questions on firearms, advocating for solutions that prioritize rights, challenge authoritarianism, and confront toxic masculinity in gun culture.

</summary>

"People are dying. And that's their motivation."
"To be a man, you've got to have this gun."
"The gender roles in this country are blurry."

### AI summary (High error rate! Edit errors on video page)

Refuses to let the channel be dominated by content on firearms and gun control, opting to bulk questions together and address them collectively.
Questions proposed solutions like making all guns pink and glittery, banning semi-automatics, and creating public armories, but Beau urges consideration of Second Amendment rights and Supreme Court interpretations.
Beau explains why he doesn't advocate for banning all semi-automatics, citing concerns about authoritarianism and removing means of defense for vulnerable groups.
Suggests a way to craft an assault weapons ban with real teeth by listing exemptions rather than defining what constitutes an assault weapon.
Expresses reservations about proposals like liability insurance or taxing ammo, fearing they could disproportionately affect lower-income individuals.
Clarifies that while the AR-15 can be used as a varmint gun due to its chambered caliber, it is not the primary reason most people own it.
Addresses concerns about universal background checks potentially leading to a firearm registry and explains why he is not overly concerned about the government seizing firearms.
Talks about a decline in mass incidents before the assault weapons ban, cautioning against attributing causation to correlation.
Dismisses the claim that cops couldn't use a flashbang at a school due to civilians present, expressing frustration at the lack of clear answers in such situations.
Advocates for working with those motivated by reducing violence, even if they lack expertise in firearms, and criticizes toxic masculinity's impact on gun culture.

Actions:

for gun control advocates,
Advocate for solutions that prioritize rights and challenge authoritarianism (implied)
Address toxic masculinity within gun culture (implied)
</details>
<details>
<summary>
2022-06-01: Let's talk about teachers, cats, and gas stations.... (<a href="https://youtube.com/watch?v=t5JzV_yb1fQ">watch</a> || <a href="/videos/2022/06/01/Lets_talk_about_teachers_cats_and_gas_stations">transcript &amp; editable summary</a>)

Beau walks into a gas station, encounters a woman asking about first aid kits, and educates about pediatric first aid needs and CAT tourniquets.

</summary>

"You're gonna need more gauze and chest seals and tourniquets."
"If you are a teacher preparing a kit for the battlefield of your classroom..."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Enters a gas station and is recognized by the cashier.
Approached by a woman asking about an "iffic," which turns out to be an IFAK (Individual First Aid Kit).
Woman inquires if the kit is suitable for treating a gunshot wound and if it works on children.
Beau advises on the contents of the kit and the suitability for children, particularly regarding tourniquets.
Recommends additional supplies like gauze, chest seals, and tourniquets for treating children.
Conducts research on the efficacy of CAT tourniquets on pediatric cases.
Confirms that CAT generation 7 tourniquets have been successful in pediatric cases without issues.
Describes CAT tourniquets and recommends them as a valuable addition to first aid kits.
Suggests teachers prepare first aid kits similar to those used by soldiers for classroom emergencies.

Actions:

for teachers, first aid responders,
Assemble a first aid kit similar to those used by soldiers for emergencies in your classroom (suggested).
Ensure your first aid kit includes adequate supplies like gauze, chest seals, and CAT generation 7 tourniquets (implied).
</details>
<details>
<summary>
2022-06-01: Let's talk about disbanding Uvalde departments.... (<a href="https://youtube.com/watch?v=2h3BSsymV6M">watch</a> || <a href="/videos/2022/06/01/Lets_talk_about_disbanding_Uvalde_departments">transcript &amp; editable summary</a>)

Two Texas departments have stopped cooperating with an investigation, prompting calls for immediate disbandment to prevent unaccountability and disregard for public safety.

</summary>

"If this flies, if they are able to stonewall in an investigation in a situation like this, they are above the law."
"There is no way you can look at what happened and think the right decisions were made."
"If they don't believe they owe the public answers after what happened, they don't believe they owe the public anything."

### AI summary (High error rate! Edit errors on video page)

Two departments in Texas, the regular police department and the police department for the school, have decided to stop cooperating with the Texas Department of Public Safety's investigation.
The decision to stop cooperating came after the director of DPS criticized the delayed police entry into a classroom during a news conference.
Beau asserts that the delayed police entry was clearly the wrong decision and contrary to protocol.
He calls for the immediate disbandment of these departments to prevent creating unaccountable entities.
Beau believes that allowing these departments to stonewall an investigation makes them believe they are above the law.
He suggests that even if the departments start cooperating, disbandment is the only option for a positive outcome.
Beau recommends that the Texas Department of Public Safety should treat the situation as a criminal investigation.
He questions the accuracy of information being put out and suggests a probe within law enforcement may not be the right approach.
Beau insists that the decisions made during the incident were clearly wrong, and there is no justification for believing otherwise.
He warns that without action from the community, they will be under the control of an unaccountable department that doesn't believe it owes the public anything.

Actions:

for community members, advocates,
Disband the departments immediately (implied)
Advocate for treating the situation as a criminal investigation (implied)
</details>
<details>
<summary>
2022-06-01: Let's talk about Trump's Pulitzer letter.... (<a href="https://youtube.com/watch?v=SrTtXmStzPE">watch</a> || <a href="/videos/2022/06/01/Lets_talk_about_Trump_s_Pulitzer_letter">transcript &amp; editable summary</a>)

Former President Trump demands Pulitzer Board rescind awards based on false information, revealing dangers of believing propaganda.

</summary>

"The continuing publication and recognition of the prizes on the Board's website is a distortion of fact and a personal defamation."
"Trump really is at this point, he is turning into Grandpa Simpson."
"It can't be easy to be a Trump supporter right now."

### AI summary (High error rate! Edit errors on video page)

Former President Trump sent a letter to the Pulitzer Board demanding they rescind the prize awarded to the Washington Post and the New York Times in 2018 for their coverage of possible Russian collusion.
Trump's letter claims that the award was based on false and fabricated information published by the media outlets.
The letter warns of potential litigation if the Board does not take action, akin to finding votes.
Trump suggests the Board keep an eye on the ongoing criminal trial of Michael Sussman, a former attorney for the Clinton campaign.
Sussman was acquitted, contrary to what Trump believed the Durham investigation was going to produce.
Trump's actions are likened to Grandpa Simpson, writing letters to entities like the Pulitzer Board.
The Durham investigation was a significant hope for Trump supporters to deflect from the Mueller investigation.
Trump's aim was to discredit the origins of the investigations by claiming Sussman lied, but the jury found otherwise.
Falling for his own propaganda, Trump's letter illustrates the danger of believing inaccurate stories.
Being a Trump supporter amidst such revelations can be challenging.

Actions:

for political observers,
Monitor ongoing criminal trials (suggested)
Stay informed about political developments (suggested)
</details>
