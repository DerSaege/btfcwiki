---
title: Let's talk about rumors and the surprise hearing today....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SwXLtjSLzpw) |
| Published | 2022/06/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A surprise hearing was called by the committee with secret content and witnesses, causing a flurry of rumors.
- Leading contender for a surprise witness is Cassidy Hutchinson, a former aide to Mark Meadows.
- Cassidy Hutchinson may have inside information due to her close proximity to the inner circle of the White House.
- The committee has obtained footage from a documentarian suggesting plans to alter the election outcome were made early on.
- There are hints that the footage contradicts Ivanka Trump's testimony to the committee.
- The committee faces challenges in storytelling with ongoing developments, such as Eastman having his phone taken by the feds recently.
- Beau anticipates more surprise hearings from the committee to keep the public engaged.
- The unfolding drama of the hearings makes for compelling television.
- Beau humorously notes his unchanged shirt between videos and teases about finding out who has been "naughty or nice" in the hearing.

### Quotes

- "They're trying to tell a very sprawling story with new developments all the time."
- "In lack of a, for lack of a better word, it makes for good TV."
- "Today we will find out who has been naughty and nice."
- "I just realized I didn't change shirts between the last video and this one."
- "It looks like it's going to be fun."

### Oneliner

A surprise hearing with secret witnesses and potential explosive revelations hints at early election meddling plans, setting the stage for ongoing dramatic developments and compelling television.

### Audience

Journalists, political analysts

### On-the-ground actions from transcript

- Stay updated on the unfolding developments in the hearings (implied)

### Whats missing in summary

Insights into the potential implications of the upcoming hearings and their impact on ongoing investigations.

### Tags

#Hearings #Committee #SurpriseWitnesses #ElectionInterference #DramaticDevelopments


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the hearings
that are gonna take place in a few hours for y'all.
I'm recording this last night.
If you don't know, there was a surprise hearing
called by the committee and they kept the content
of the hearing and the witness list secret.
They didn't tell anybody what it was about.
This caused a flurry of rumors
in the journalistic community.
So we're going to go over those rumors.
We're going to talk about what we might be hearing
in a few hours.
At time of filming, the leading contender to be at least one
of the surprise witnesses is Cassidy Hutchinson.
Now, when the committee talked about congresspeople
who had requested pardons from the White House.
Many came out and dismissed, denied, and downplayed.
I said that was a bad idea because generally speaking,
thus far, when the committee has made an allegation,
they've got the receipts.
Cassidy Hutchinson is a former aide to Mark Meadows,
who is the former White House Chief of Staff.
She has a really big receipt book,
and that may come up today.
However, that's probably not the only reason she's coming in.
There is a tendency among incredibly powerful people
to forget about the help, meaning
aides, assistants, cleaners, security personnel.
And they say things in front of them
that perhaps they shouldn't.
And Cassidy Hutchinson had a lot of close proximity to the inner circle of the White
House at the time.
So this is probably a little bit more than just saying, hey, these politicians lied.
Now another set of rumors that has surfaced and I will tell you this is the second time
I recorded this video because this bit was rumor and then the Guardian decided to print
it so I can just talk about it directly without having to worry about getting sued, which
is nice.
So according to reporting from the Guardian, the committee has come into possession of
footage from a documentarian, which is pretty well known by this point. The
Guardian suggests that this has led the committee to wonder if the plan to
perhaps alter the outcome of the election was sketched out a little bit
earlier than originally known. Like around the time of the first
presidential debate before the election had even taken place. There is also the
suggestion in the reporting that some of the footage contradicts what Ivanka
Trump told the committee. Now we don't know if this is going to come up in the
hearing today, but I would expect to hear about it soon.
I don't think that this is going to be the last surprise hearing from the committee.
The committee has a really hard job.
They're trying to tell a very sprawling story with new developments all the time.
It's also worth noting while we're talking about new developments, Eastman, who was one
of the key reported advisors in this had his phone taken by the feds recently.
So they have a hard job, they have a sprawling story, and it's continually developing.
It's hard to keep people interested.
This type of development is dramatic.
It gets people talking about it, it peaks curiosity, everybody wonders who's the surprise
guest on the show tonight.
In lack of a, for lack of a better word, it makes for good TV.
So I would expect more of this.
I just realized I didn't change shirts between the last video, which was very sarcastic,
and this one.
So I guess today we will find out who has been naughty and nice.
I would definitely tune in, it looks like it's going to be fun.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}