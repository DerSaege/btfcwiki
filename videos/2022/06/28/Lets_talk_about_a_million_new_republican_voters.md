---
title: Let's talk about a million new republican voters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YEqWZB4qBoE) |
| Published | 2022/06/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A report claimed a million people switched from Democrat to Republican, causing concern.
- The report mentioned a shift from Denver to Atlanta, which could also be described as Colorado to Georgia.
- In reality, the switch was driven by organized efforts to vote in Republican primaries against Trumpist candidates.
- In Georgia, nearly 40,000 people voted in Republican primaries to support candidates opposing Trump-backed ones.
- The total number of party switches was around 400,000 votes, with Georgia accounting for a significant portion.
- Beau believes the numbers are accurate but lacking in explanation about why people switched parties.
- One person mentioned they began moving away from their original party affiliation five or six years ago through the Libertarian Party.
- Beau doesn't see the party switches as a major concern, suggesting it was organized to remove Trumpist elements.
- He notes that recent events like rights stripping and unpopular Supreme Court decisions happened after many party switches occurred.
- Beau views the situation as political maneuvering rather than a mass exodus from the Democratic party.

### Quotes

- "This isn't something I'm concerned about."
- "This isn't an organic thing because people are mad."
- "This is political shenanigans at work."

### Oneliner

A million party switches spark concern, but Beau sees organized political strategy, not mass defection.

### Audience

Political observers

### On-the-ground actions from transcript

- Join or support efforts to address political maneuvering and strategizing within parties (implied)

### Whats missing in summary

Beau's analysis and perspective on the reported party switches and their implications.

### Tags

#PartySwitches #Politics #RepublicanParty #DemocraticParty #TrumpistCandidates


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the news
that appears to be on everybody's mind,
at least if you're judging by my inbox,
and that's a million new Republican voters.
Dun, dun, dun.
Okay.
So a report came out that said a million people
had switched their party affiliation
from Democrat to Republican, right?
The article uses words like dire,
warning for Democrats, so on and so forth, right?
Uses terms like that.
So of course people are concerned.
Another term that's in it is this occurred
and then from Denver to Atlanta.
So yeah, yeah.
I mean, that's one way to say that, Denver to Atlanta.
Another way to say that would be from Colorado to Georgia.
And yet still another way to say that
would be from one location
where there was an organized push
to get people to vote in the Republican primaries
to defeat Trumpist candidates
to another location where there was an organized push
to get people to vote in the Republican primaries
to defeat Trumpist candidates.
We covered this when it was happening on the channel.
I wanna say in Georgia,
it was almost 40,000 from Georgia alone
that did this to, they voted in the Republican primary
to help Raffensperger and Kemp
beat the Trump-backed candidates, right?
And if I'm not mistaken,
I could be wrong about this part.
I don't even think that was the whole number.
I think that was like just from early voting
or maybe just from one day or something like that,
but it wasn't the totality just from Georgia.
And I know what you're saying.
You're probably like, well,
40,000 isn't a whole lot compared to a million.
Right, but a lot of party changing occurs normally.
It's a million to the Republican party,
630,000 or so to the Democratic party,
which means we're really only talking about
like 400,000 votes or voters, and Georgia alone
takes care of at least 10% of that.
I don't see this as a huge cause for concern.
I think that this is, these are accurate numbers,
don't get me wrong, but they're missing the why.
The one person that they talked to
said that they started switching and started moving away
five or six years ago
and went through the Libertarian Party first.
I don't think that this is the dire warning
people are making it out to be.
I would also point out that all of this would have occurred
prior to like, you know, the whole recent thing
of stripping half the country of its rights
and all of these Supreme Court decisions
that are wildly unpopular.
So while I understand the report that came out,
this isn't something I'm concerned about.
I don't see this as people leaving the Democratic party
in droves to go vote for Republicans.
This was in many ways organized by the Democratic party
to help get rid of the Trumpist elements.
I wanna say that the same thing's happening
in Wyoming as well as other states.
That this isn't an organic thing because people are mad.
This is political shenanigans at work.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}