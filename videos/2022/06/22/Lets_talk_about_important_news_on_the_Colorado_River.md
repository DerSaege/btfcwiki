---
title: Let's talk about important news on the Colorado River....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3kvVmXdnSb4) |
| Published | 2022/06/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Bureau of Reclamation has given states in the southwestern United States 60 days to come up with a plan to cut water usage from the Colorado River by 2 to 4 million acre feet.
- An acre foot is equivalent to almost 326,000 gallons of water, which is enough for 10 people for a year.
- Cutting 2 to 4 million acre feet of water equates to impacting the water usage of 20 to 40 million people.
- States impacted by this water usage cut include Colorado, New Mexico, Utah, Arizona, California, and Nevada.
- Lake Powell might stop producing electricity by early 2024 if water levels continue to drop.
- Assistant Secretary of the U.S. Department of Interior for Water and Science warns about unstable water supplies for agriculture, fisheries, ecosystems, industry, and cities due to climate change.
- States must come up with a plan to significantly reduce water usage as a response to climate change.
- Failure to create a plan will result in the federal government imposing one.
- Climate issues are here and action needs to be taken urgently.
- The situation requires immediate attention and action.

### Quotes

- "We are facing the growing reality that water supplies for agriculture, fisheries, ecosystems, industry, and cities are no longer stable due to climate change."
- "Climate issues aren't waiting. They're here."

### Oneliner

The Bureau of Reclamation has set a 60-day deadline for southwestern states to cut Colorado River water usage by 2 to 4 million acre feet, signaling the urgent need for action in response to climate change impacts on water supplies.

### Audience

States, Environmentalists, Activists

### On-the-ground actions from transcript

- Collaborate with local communities and organizations to develop and implement water conservation strategies (suggested).
- Support and participate in initiatives that aim to reduce water usage in your area (exemplified).

### Whats missing in summary

The full video provides more context on the specific impacts of climate change on water supplies and further details on the potential consequences of not addressing the current water usage challenges effectively.

### Tags

#WaterUsage #ColoradoRiver #ClimateChange #EnvironmentalAction #Urgency


## Transcript
Well, howdy there, Internet people. It's Beau again. Watch this video. It's important.
Today we're going to talk about why we have been talking about water usage in the southwestern
United States for the last six weeks or so. We have finally hit that point. The Bureau
of Reclamation has told the states, y'all have 60 days to come up with a plan to cut
water usage coming out of the Colorado River by 2 to 4 million acre feet by next year.
If you don't do it, we're going to do it. Now, what's an acre foot, right? An acre foot
is exactly what it sounds like. One acre of land covered one foot deep in water. Another
way to say that would be it's almost 326,000 gallons of water. Okay, so to put this in
perspective, the EPA on their site, they say that the average person uses 82 gallons of
water per day at home times 365 days. That is 29,930 for the purposes of our illustration,
30,000 gallons. One acre foot, 326,000 almost. For the purposes of our illustration, 300,000.
One acre foot is enough water for 10 people for a year. The feds want the states to cut
2 to 4 million acre feet of water, 20 to 40 million people. Now, that is an illustration.
Understand it's not all coming from residential usage, all right? But that is to show the
scale of what they're asking. Another way to look at this is they're asking for a range
of 2 to 4 million acre feet. The entire state of Arizona gets 2.8 million acre feet of water
from the Colorado River. So what they're asking to cut is something roughly the size of the
water usage of Arizona. The states that have been impacted, Colorado, New Mexico, Utah,
Arizona, California, and Nevada. This is a big deal. Now, the reason the feds are stepping
in to say this is because it looks like by early 2024, Lake Powell, well, it won't be
able to produce electricity if it keeps dropping, which will just make matters worse. Now, here's
the quote that you should pay attention to. This is from the Assistant Secretary of the
U.S. Department of Interior for Water and Science. We are facing the growing reality
that water supplies for agriculture, fisheries, ecosystems, industry, and cities are no longer
stable due to climate change. So, this is big news. The states have to pull a plan together
to figure out some way to reduce water usage by a lot. A lot. And the feds are saying,
if you don't come up with a plan, we're going to give you one. While the country debates
whether or not we should really move to respond to climate issues, understand the climate
issues aren't waiting. They're here. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}