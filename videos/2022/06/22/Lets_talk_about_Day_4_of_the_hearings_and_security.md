---
title: Let's talk about Day 4 of the hearings and security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qU3YDcfCB5Y) |
| Published | 2022/06/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recap of day four of hearings regarding events on the 6th and leading up to it.
- Committee following a structured approach: tell them what you're going to tell them, tell them, tell them what you told them.
- Evidence presented that Trump and crew knew election claims were false and actions violated federal law.
- Evidence showing Trump and crew behind state-level push for alternate electors on the 6th.
- Surprising aspect: Republicans pointing fingers at Trump and crew, not Democrats.
- Remarkable information coming from Republicans, not Democrats, impacting those who need to hear it.
- Lack of internal security procedures concerning; many inappropriate direct communications took place.
- Expect more surprising uncovered communications showing carelessness and lack of security precautions.
- Committee doing a good job presenting hard evidence to persuade people.
- Need for someone to tie together the presented information into a compelling narrative for the American people.

### Quotes

- "They're following that template very well."
- "They're coming from Republicans, in some cases, lifelong Republicans."
- "Their carelessness, their lackadaisical attitude when it comes to basic tradecraft."

### Oneliner

Beau provides a structured recap of the hearings, revealing hard evidence of Trump and crew's involvement and the lack of internal security measures.

### Audience

Committee members, concerned citizens.

### On-the-ground actions from transcript

- Follow the hearings closely to stay informed and engaged (implied).
- Share information and evidence from the hearings with others to raise awareness (implied).
- Advocate for accountability and transparency in political processes (implied).

### Whats missing in summary

Insights on the potential impact of compelling storytelling to convey the findings effectively.

### Tags

#Hearings #Trump #Republicans #InternalSecurity #Accountability


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to do a little recap on the hearings.
I guess this is day four of the hearings,
looking into what happened on the 6th
and the events leading up to it.
So far, they're doing a really good job,
much better than I actually thought they would, to be honest.
They're following that template very well.
Tell them what you're going to tell them.
Tell them what you told them.
Day one was all about telling them
what you're going to tell them.
Then they started bringing in the pieces of the puzzle.
And this is good, because a lot of the people who really
need to hear this information need it
in small, digestible pieces.
And then it can all be brought back together later on
in the tell them what you told them phase.
So far, the committee has presented their evidence
that Trump and Crewe knew the claims about the election
were false when they made them.
Then they presented their evidence
that Trump and Crewe knew that some of the actions
they were advocating be undertook
were in violation of federal law.
Today, they presented their evidence
that Trump and Crewe were behind the state-level push
to create the plausible deniability
for the activities of the 6th, those alternate electors.
The idea that the push for these alternate electors
came from DC, it's something that a lot of people assumed.
But having it clearly demonstrated,
I mean, that's a pretty big piece of evidence,
because that shows that it wasn't actually
the states calling the election into dispute.
They weren't disputing the election.
They weren't disputing the results.
They were just following their betters' orders,
or in some cases, pushing back pretty hard on the requests.
That's one of the surprises in the committee so far.
I think most people, myself included,
expected the majority of the hearings
to be the Democratic Party pointing fingers at Republicans.
That's not what's happening.
It's Republicans pointing their finger at Trump and Crewe.
This primarily, when you're looking at this,
the evidence being presented, the truly remarkable pieces
of information that are coming out,
they're not coming in the form of statements
from the Democratic Party or from members
of the Democratic Party.
They're coming from Republicans, in some cases,
lifelong Republicans.
And I think that's going to, as the hearings go on,
I think that will have a more and more marked impact
on the people that really need to hear this information.
I think the only other thing that I'm really surprised by
is some of the conversations that took place.
I mean, don't get me wrong.
Legally and morally, there's a whole bunch
of these conversations that never should have happened.
At the same time, I'm sitting here as, you know,
I used to be a security consultant.
I'm like, none of these people have ever heard of a cutout,
huh?
I mean, like, there are people that
talked about this who should have never spoken
to each other directly.
They always should have used an intermediary.
I mean, it's good that they're this bad at this,
but it's surprising.
I mean, they do have people on staff
that would know how to do this, and it
seems like they just didn't listen to them,
or they didn't trust them enough to tell them
what was going on, which is funny in and of itself.
The lack of internal security is a huge thing for Trump and crew
right now.
A lot of this never should have been something
that could have been pieced together
because these conversations should have never taken place
from a security aspect.
I'm not even talking about the legal and moral sides of it.
That's obvious.
So given that they haven't used the proper security
procedures that you would use in something
like this up to this point, I have
a feeling there's going to be even more conversations that
come out that are very surprising.
That seems to be almost a certainty, conversations that
never should have taken place that should have been done
through an intermediary that took place directly.
And that is going to be one of those things that
makes the case.
This is a group of people who truly do
believe they don't have to worry about basic security
precautions because they're above the law.
Their carelessness, their lackadaisical attitude
when it comes to basic tradecraft,
it might be more concerning to me because this shows
they really do.
They didn't think that this was anything to worry about.
They didn't think they had to worry about it at all,
that they were just completely above all of it.
Nobody was going to come after them.
That's something else.
So the hearings will go on.
I would expect more very frank conversations
between high-profile people.
I would expect a lot of texts and emails and documents
to come out that shouldn't exist.
And I really think that the committee
is doing a really good job of presenting their case
and showing people hard evidence that what they don't want
to believe is what happened.
Now, when they finish up, what they're really going to need
is somebody who can tell a story, somebody who can speak,
who can pull all of these little independent digestible pieces
of information that are being presented on each day
and tie it all together.
That's what they're going to need to really sell
this to the American people.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}