---
title: Let's talk about Lincoln the conservative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=u7S_0uYy9RY) |
| Published | 2022/06/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Lincoln was both a conservative and a progressive, with a key message challenging the assumption that conservatives want the status quo.
- Lincoln, despite being a Republican, was a progressive figure who fundamentally changed American society.
- Lincoln's leadership played a significant role in recognizing people as individuals legally, marking a progressive position in history.
- The common equating of Republicans with conservatives is misconstrued due to historical shifts in party ideologies.
- A letter from Karl Marx congratulating Lincoln on his reelection indicates Lincoln's progressive stance.
- The claim that Lincoln was a conservative is historically inaccurate, given his progressive actions and views.
- The Union flag under Lincoln represented progressives, not conservatives.
- The Republican party during Lincoln's time was more progressive than conservative.
- Lincoln's legacy as a progressive figure is undeniable, especially considering his impact on American history.
- The narrative that conservatives can claim Lincoln's legacy is debunked due to his progressive ideals and actions.

### Quotes

- "Lincoln was a progressive, not a conservative."
- "There is no way for the conservatives of today to claim him."
- "Lincoln got fan mail from Karl Marx."
- "He was a begrudging, pragmatic progressive."
- "Lincoln is responsible for one of the single most progressive moments in American history."

### Oneliner

Beau challenges misconceptions by illustrating President Lincoln's progressive legacy and dispelling the notion that conservatives can claim him.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Research and understand the historical context of President Lincoln's progressive actions (suggested)
- Engage in discourse about historical figures and their political ideologies (exemplified)

### Whats missing in summary

More in-depth exploration of the historical context and impact of President Lincoln's progressive actions.

### Tags

#PresidentLincoln #ProgressiveLegacy #ConservativeMisconceptions #PartyAffiliation #HistoricalContext


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk a little bit
about President Lincoln and being a conservative
and being a progressive.
And you may find out something about President Lincoln
that you don't know.
I've mentioned it on the channel before,
but it's been a really, really long time.
And we're gonna do this because I got a message.
You said conservatives want the status quo.
That's not always true.
Why don't you look at a $5 bill?
Lincoln changed a lot, didn't he?
Why don't you look at his party affiliation?
True enough, Lincoln was in fact a Republican.
But Lincoln was in fact a progressive.
Don't make me tap the shirt.
He absolutely was.
He was a begrudging, pragmatic progressive
that used the entire weight of the federal government
fundamentally alter the fabric of American society in what might be the single most progressive moment
in American history. Under his leadership, people who were not viewed as people,
legally speaking, in a whole lot of states, started to be viewed that way. That's an incredibly
progressive position. You believe that he was a conservative because he was a Republican and
today those two terms have begun to be equated. The party switched. I know there's a whole
lot of propaganda saying that that never happened, but it absolutely did. There are tons of videos
on YouTube that go over this. I actually, I think I have like three detailing how it
happened and when. But I have one little thing that should clearly demonstrate
that Lincoln was not a conservative. It's a letter. Sir, we congratulate the
American people upon your reelection by a large majority. If resistance to the
slave power was the reserved watchword of your first election, the triumphant
war cry of your re-election is death to slavery. From the commencement of the
Titanic American strife, the working men of Europe felt instinctively that the
Star-Spangled Banner carried the destiny of their class. And it goes on and on and
on because this guy was really long-winded. Do you know who wrote that?
Karl Marx. You know a lot of conservatives getting fan mail from Karl Marx or the organizations he represented?
I mean that doesn't seem like something that would happen, right?
President Lincoln got fan mail from Karl Marx.
I'm fairly certain that that might secure his position as at least somewhat progressive.
The habit of trying to equate Republican with conservative and therefore claim credit for
things that progressives did a while back. It's a long-running tradition, but
it's easily shot down. The people who wanted the status quo, the conservatives
under Lincoln, they had their own flag. They decided they were going to leave
the Union and that they started making all kinds of flags for themselves. Who
Who carries that flag today?
Is it progressives or is it conservatives?
Lincoln was a progressive.
This isn't really something that's up for debate.
Yeah, he was a Republican because the Republican party was the more progressive of the two
at the time.
That seems like it should go without saying.
Lincoln was a progressive, not a conservative.
The switch happened.
It's a thing.
Lincoln got fan mail from Karl Marx.
Lincoln is responsible for one of the single most progressive moments in American history.
There is no way for the conservatives of today to claim him.
That's just not historically accurate.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}