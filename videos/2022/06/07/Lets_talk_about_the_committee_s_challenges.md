---
title: Let's talk about the committee's challenges....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QNljbzOotfA) |
| Published | 2022/06/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Committee preparing for a public hearing to enlighten the American populace on a complex event.
- Multiple factions colliding and working together towards a common goal.
- Committee's challenge to distill and explain the complex event to the American people before they lose interest.
- Importance of storytelling by telling the audience what they are going to hear, telling them, and then summarizing.
- Risks involved in deviating from the storytelling plan and getting too complex.
- Need for simplicity to avoid undermining the investigations and pursuit of accountability.
- Emphasizes staying informed about new developments as politically oriented investigations may bring surprising moments.
- Warning about potential defense tactics by those trying to justify the actions.
- Stress on absorbing the whole story and understanding all connections.
- The importance of the committee turning the investigation into a consumable story for it to be effective.

### Quotes

- "Tell them what you're going to tell them. Tell them what you told them."
- "People will be watching this, looking for anything to validate their preconceived notions."
- "Be ready to absorb the whole story and look at all of the connections."

### Oneliner

Committee faces the challenge of distilling a complex event for the American public, stressing the importance of storytelling and simplicity to avoid undermining accountability efforts.

### Audience

Committee Members, Investigators, Public

### On-the-ground actions from transcript

- Stay informed about new developments to understand the full story (implied)
- Be ready to challenge preconceived notions when surprising moments arise (implied)

### Whats missing in summary

Importance of staying engaged and informed throughout the investigation process.

### Tags

#Committee #PublicHearing #Storytelling #Accountability #AmericanPublic


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about why I say today,
we're going to talk about in pretty much every video.
And we're going to talk about the big challenge
that the committee has.
You know, they've elected to put this on primetime
and they're going to hold the public hearing.
And they're hoping that this is going to enlighten
the American populace as to what happened.
I have kept up with the various investigations
and I have looked into part of it on my own,
the stuff that's more in my wheelhouse.
Just from that, which is the militant side of things, right?
You have multiple factions inside multiple groups
colliding with each other and working together
all to achieve the same goal.
From the committee standpoint, you have that.
You have the electors thing.
You have the objections.
You have the legal challenges.
You have all of the coordinating elements.
They have a very complex event that they have to distill
and explain to the American people
before those people get bored.
They have to tell a story.
The smart move here for the committee
is to tell them what you're going to tell them,
tell them, tell them what you told them.
They're going to come out and say,
today we're going to talk about this
and this is what you're going to hear.
And they're going to tell you the witnesses
that they're going to bring up
and what those witnesses are going to tell you.
And they're going to lay out the story.
So you already know the storyline.
And this is all part of the
tell them what you're going to tell them phase.
Then they bring the witnesses out
and the witnesses tell them.
And then they have to close with somebody who can speak.
Somebody who can get up there,
take everything that was said
and put it into that storyline.
Take this incredibly complex event
and boil it down and tell them what you told them.
If they deviate too far from that game plan,
if they try to get into the intricacies of it,
they're probably going to lose the American public.
The American public may see it
as something that was too complex
to really sink their teeth into.
Somebody may misspeak
and give those who want to defend these actions
a way to draw attention to it.
They have to keep this simple.
Tell them what you're going to tell them.
Tell them what you told them.
If they don't do that,
they risk undermining their own investigation,
the committee's investigations,
and the DOJ investigations.
There's a huge risk associated with this,
with putting this on primetime.
You know, a whole lot of people
have been waiting for this moment,
waiting for the public side of this to start.
Just be aware that there are a lot of risks
associated with this.
They very well could undermine
any pursuit of accountability with this.
They have to be careful.
And one of the things that you can do
is to make sure you're aware of the new developments,
because we have a general idea.
But because this investigation
is more politically oriented,
there are going to be surprising moments.
I'll be super surprised if there's not.
There should be moments
that are going to get sound bites,
moments that are going to be replayed.
You have to remember that there's more to it than that,
because one of those sound bites,
one of those things that gets replayed,
that could be one of those things
that the people who want to defend these actions latch onto
and say, no, it happened in this slightly different way
that doesn't actually change anything.
But that'll be the defense.
You have to be ready to absorb the whole story
and look at all of the connections.
That's, of course, assuming that the committee does its job,
and they do turn this into a story that is consumable.
If they don't, if they get lost in the details,
this is over before it starts.
You know, from the side of things
that I really looked into,
90% of the names that I'm familiar with,
they shouldn't even be mentioned in this.
Just key people, just key communications,
laying out the coordination between the different factions.
That's it.
Everything else, that's for later.
You have to get the storyline out there first.
People have to understand what occurred.
Tell them what you're going to tell them.
Tell them what you told them.
That communication technique is what the United States
needs right now,
because people will be watching this,
looking for anything to validate their preconceived notions.
And I have a feeling that there are going to be witnesses
who say things that might be surprising.
So no matter how you feel about this right now,
you need to be ready to check your own preconceived notions,
because some of them are probably going to be challenged.
So this starts this week, and in a lot of ways,
this is the beginning of renewal,
or it's the beginning of the end.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}