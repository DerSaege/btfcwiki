---
title: Let's talk about beliefs and the test of time....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2aemgKflFHQ) |
| Published | 2022/06/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the concept of time and how it exposes the flaws in beliefs.
- Mentions how people from the past and present hold backward beliefs, which will be judged in the future.
- Expresses anticipation for being viewed as conservative in the future due to outdated beliefs.
- Speculates on future beliefs, such as society viewing current treatment of animals and the earth as barbaric.
- Points out the habit of ignoring issues in other countries and prioritizing personal gain.
- Emphasizes that humanity's progression will make current beliefs seem primitive in the future.
- Encourages reflection on historical figures and the inevitability of current ideas being judged poorly in the future.

### Quotes

- "Nobody can stand the test of time because humanity marches forward."
- "I cannot wait until the day when I am viewed as a conservative."
- "The further humanity progresses, the worse we're going to look."
- "Time will march on and our ideas will be 100% bad takes."
- "Y'all have a good day."

### Oneliner

Beau explains how time exposes flaws in beliefs, leading to eventual judgment of present ideas as primitive, urging reflection on historical figures.

### Audience

Philosophical thinkers

### On-the-ground actions from transcript

- Challenge backward beliefs (implied)
- Prioritize fixing societal issues over personal gain (implied)
- Educate others on the importance of understanding and addressing issues (implied)

### Whats missing in summary

The emotional impact of realizing the impermanence of beliefs and the importance of continual growth and progress.

### Tags

#Time #Beliefs #Progression #Reflection #History


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about the test of time. Time marching on again. This wasn't really intentional
but there definitely seems to be a theme going on. After that video about Lincoln,
I had a lot of people just send me like lists of all of the backwards beliefs
that he held. And yeah, there's a bunch of them. And the general idea of most of
those messages was that he really shouldn't be considered a progressive
because, well, he didn't pass here on this particular belief. Yeah, I mean, people
from 150 years ago, they held backwards beliefs. So do you. So do you. 150 years
from now, people will look back on our beliefs and we will be seen as backwards.
It doesn't matter how progressive you see yourself today. It doesn't matter how
many purity tests you can pass. You will be viewed as backwards. You will get
something wrong guaranteed. Nobody can stand the test of time because humanity
marches forward. This is a good thing. This is without a doubt a very good
thing. I cannot wait until the day when I am viewed as a conservative. When my
beliefs would be conservative beliefs because they're so outdated. That's a
good thing. And we don't know what those beliefs will be. We don't know what we're
getting wrong. That's part of the mystery of life. We can guess and I've got some
pretty good ones. I mean, I would imagine that people in the future will look at
our treatment of animals as just barbaric. Our relationship with the earth as just
unbelievable. The way those of us in wealthy countries just kind of look the
other way when stuff is happening over there, you know, to those other countries.
Those countries that aren't wealthy. They're just, they're different. There's
no reason to try to help those people. We can just ignore it, right? I'm pretty
sure those will be three of them, but there's undoubtedly more that I don't
have a clue about. Neither do you. We will undoubtedly be judged by history and the
further humanity progressives, the worse we're going to look. But that's good.
That's good. That means that the Overton window of the future is far better than
it is today. The main beliefs of society 150 years from now are going to be so
advanced and so far beyond where we're at today that we are looked at as if
we barely understood anything at all. And that's good. That's how it should work.
So, when you are looking at historical figures, obviously there are some things
that are wrong and will always be wrong. And there are also things that we view
today as, well, that's definitely wrong. That's always been wrong. But it may not
have seemed that way at the time. And you have to remember, just like us in wealthy
countries today, there's a habit among people of misunderstanding things or
deprioritizing them, especially if our paycheck or lifestyle depends on us not
understanding them and not making it a priority to fix them. Time will march on
and our ideas will be 100% bad takes. And I can't wait for that day to come.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}