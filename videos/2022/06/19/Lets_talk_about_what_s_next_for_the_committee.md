---
title: Let's talk about what's next for the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-hohQA0JF28) |
| Published | 2022/06/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The committee has followed a template well, showing evidence that Trump knew his election claims were false and that pressuring Pence was illegal.
- The Department of Justice wants the transcripts for upcoming investigations and prosecutions.
- Local prosecutors may also request evidence from the committee for state-level cases.
- The committee is likely to show that Trump sought to influence officials at the state and local levels through a campaign.
- They might demonstrate that grievances used to push the January 6th narrative were manufactured at the state level by the White House.
- Trump's orbit pressured state and local officials to support these claims.
- The committee may address Trump's circle's campaign to pressure the Department of Justice into declaring the election corrupt.
- They may break the chronological approach to focus on events at the White House during January 6th, including text messages.
- This shift could reignite public interest and potentially weaken opposition.
- Text messages from high-profile individuals may reveal discord and further undermine deniers of the election outcome.
- The committee's public presentation may help secure interviews and cooperation for the Department of Justice.
- Speculations hint at upcoming revelations until the end of the month.

### Quotes

- "One of those very rare signs that we get that the Department of Justice is actively working behind the scenes."
- "You want to talk about something that is going to bring a conspiracy home, that's it."
- "I have a feeling we're going to see that soon, and they'll bring it up to further break down the resolve of those holdouts."
- "It'll help the committee and it will also help the Department of Justice."
- "It's just a thought, y'all have a good day."

### Oneliner

The committee is poised to reveal how Trump influenced officials and manipulated grievances, potentially unraveling a conspiracy and strengthening DOJ's efforts.

### Audience

Committee members, prosecutors, viewers

### On-the-ground actions from transcript

- Contact local prosecutors for potential state-level case evidence (implied)
- Stay informed about upcoming committee revelations (implied)

### Whats missing in summary

Insights on the specific evidence and dynamics driving the committee's investigations and potential impact on legal proceedings.

### Tags

#CommitteeInvestigation #DOJ #TrumpInfluence #ConspiracyRevealed #LegalProceedings


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about the committee
and where it goes from here.
What's next?
Because they've done a pretty good job
of following the template.
And they have demonstrated their evidence and their belief
that Trump knew his claims about the election were false.
Then, they demonstrated their evidence and belief
that Trump knew the pressure campaign against Pence
was illegal.
They've done a really good job of this.
So good, in fact, DOJ has reportedly been like,
yeah, we're going to need those transcripts.
And the reasoning they gave for requesting them
is that it might be relevant to upcoming investigations
and some prosecutions they have already decided to undertake.
One of those very rare signs that we get
that the Department of Justice
is actively working behind the scenes.
Now, personally, and I haven't seen this brought up,
but I have a hunch that there are going to be other requests
for the evidence that the committee has put together
from prosecutors at the local level,
not federal prosecutors, but prosecutors working on cases at the state level.
I would imagine that that's going to happen.
Okay, so where does the committee go from here?
They're doing well.
What are the next phases?
What we can kind of guess is that soon we will see their evidence and belief that Trump
Trump sought to influence state and local officials through a campaign, right?
Those in Trump's orbit at the White House, those advisors, they set out to do this.
Now my guess is what they're going to try to illustrate is that the grievances that
Trump used to push his January 6th narrative and try to halt the certification are grievances
that the White House arranged to have manufactured at the state level.
You want to talk about something that is going to bring a conspiracy home, that's it.
And I think that's what they're going to try to show.
the Trump White House pressured the state and local officials to generate these claims
to back them up.
So I think we'll see that probably this week.
And if they have time, they will probably also get into the campaign from Trump's circle
pressure the Department of Justice into declaring the election wrong, corrupt, broken, whatever.
This campaign, as far as I know, is pretty unsuccessful, but I mean there are surprises
in the hearing, so we'll see.
So I think we'll see that coming up shortly, because that would be one of the next puzzles
that needs to get into place, the next puzzle piece.
And then I'm waiting for something to recapture viewers' imaginations.
So I'm going to imagine that they'll probably break with the chronological approach and
talk about what was going on in the White House during the events of the 6th, focusing
very heavily on text messages from that day and shortly thereafter.
This is to, once again, reignite, oh, did you hear what the committee said?
Did you hear what came out of the committee and get people talking about it again?
And to, I don't know, knock the opposition around a little bit.
There are a lot of rumors about things that were said via text by pretty high-profile
people about each other, and these are people who at the moment are pretending to be allies,
and they said some things about each other that, well, you wouldn't say about your allies.
I have a feeling we're going to see that soon, and they'll bring it up to further break down
the resolve of those holdouts, those people who don't want to admit that, you know, everybody
knew it was a sham and they were just trying to cling to power after they were defeated.
I would imagine that's going to come pretty quickly because that will help, it'll help
the committee and it will also help the Department of Justice.
I have a feeling at this point the committee has realized that their public presentation
might help the Department of Justice get the interviews and cooperation that it needs.
So that's what I'm guessing we're going to see from now until the end of the month.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}