---
title: Let's talk about the TX GOP and Crenshaw....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zgTn4gI_YKk) |
| Published | 2022/06/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas Republican Convention and divisions within the Republican Party are being discussed again.
- Dan Crenshaw, a Purple Heart recipient and extreme Republican, faced verbal attacks and light assault at the Convention for being labeled a "Republican in name only."
- Crenshaw's far-right views were criticized, with implications that being further right than him means veering into something beyond traditional Republicanism.
- The Republican Party's shift towards authoritarianism is evident, progressing beyond Trump.
- Crenshaw's positioning within the party indicates a concerning shift towards extreme right-wing ideologies reminiscent of 1930s Europe.
- The embrace of conspiracies and baseless claims within the Republican Party is troubling.
- Crenshaw's relative left-leaning stance within his faction should raise alarms among conservatives.
- The current political discourse has shifted to extremes, posing a threat to the traditional norms of American politics.

### Quotes

- "If you ever find yourself in a situation where Dan Crenshaw is to your left, you need to examine your life choices."
- "This is something far, far, far to the right of that. It's something that would be more at home in 1930s in Europe."
- "Dan Crenshaw is to the left of this faction of the Republican Party. That should be concerning for everybody in the country."

### Oneliner

Texas GOP divisions escalate as Dan Crenshaw's far-right label sparks assault, revealing a worrying shift towards extremism beyond Trump, echoing 1930s Europe.

### Audience

Conservatives, Republicans

### On-the-ground actions from transcript

- Support and stand up for political figures facing attacks within their own party (exemplified)
- Challenge extreme ideologies and baseless claims within political parties (exemplified)
- Advocate for maintaining traditional norms and values within political discourse (exemplified)

### Whats missing in summary

The full transcript provides additional context on the concerning shift towards extreme right-wing ideologies within the Republican Party and the implications for the political landscape in the United States.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about
the Texas Republican Convention again,
and some more divisions within the Republican Party again.
This time it has to do with Dan Crenshaw.
Now, if you don't know who he is,
he is a Purple Heart recipient to Bronze Stars Ex-Seal.
He is, by any metric, an extreme Republican.
He is a far-right Republican.
He scored a perfect 100 when it comes to being anti-choice.
He is very much pro-gun.
He voted with Trump the majority of the time.
This guy is far-right.
He was attacked, both verbally and by some reports,
his staff and him were actually lightly assaulted
at the Republican Convention because he was a rhino.
He's a Republican in name only, not a real Republican.
That is wild.
If you ever find yourself in a situation
where Dan Crenshaw is to your left,
you need to examine your life choices.
He's about as far-right as Republicans go.
If you are to the right of him,
you're not a Republican anymore.
You are something else.
This is that slide into authoritarianism
that the Republican Party has been undergoing
for so many years.
And once again, it shows that it has progressed beyond Trump.
This is somebody who supported Trump the majority of the time,
not even a real Republican anymore.
Because that faction of the Republican Party
has moved so far to the right,
Dan Crenshaw is to their left.
That should be a sign.
That should let you know that you're not talking about something
that plays within the normal bounds of the Republic of the United States.
It's not something that is within the normal political discourse
within the U.S.
This is something far, far, far to the right of that.
It's something that would be more at home in 1930s in Europe.
That's what a lot of the Republican Party has embraced.
Those talking points, those conspiracies,
those baseless claims, and that attitude.
Dan Crenshaw is to the left of this faction of the Republican Party.
That should be concerning for everybody in the country
and particularly for real conservatives in the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}