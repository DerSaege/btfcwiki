---
title: Let's talk about Texas, Trump, and Log Cabin Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gY3GHC5WIXs) |
| Published | 2022/06/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Division within the Texas state GOP, certain Republicans, and Trump due to exclusion of log cabin Republicans from a convention.
- Log cabin Republicans are the LGBTQ branch within the Republican party.
- Trump intervened, supporting log cabin Republicans and causing them to possibly feel better.
- Log cabin Republicans tweeted about not agreeing with the left's views on sex and gender.
- Beau points out that the left acknowledges the difference between sex and gender, unlike what the tweet suggests.
- Log cabin Republicans are trying to prove they are "one of the good ones" to the GOP, but Beau argues that this effort is futile.
- The Republican Party is embracing authoritarianism, evident in Trump's actions and their treatment of certain groups like the LGBTQ community.
- Beau warns that attempting to show loyalty to a group that fundamentally opposes your existence is counterproductive.
- The GOP views log cabin Republicans as their cultural enemy regardless of their party affiliation.
- Beau cautions against working against your own interests by trying to ingratiate yourself into a group that doesn't believe you should exist.

### Quotes

- "Y'all are failing to see horror on horror's face."
- "They view you as their cultural enemy."
- "There is no amount of trying to show that you're different."
- "You have been othered by your party."
- "You cannot demonstrate that you're different. They don't care."

### Oneliner

Division within Texas GOP over exclusion of LGBTQ Republicans shows futile attempts to prove loyalty to a party fundamentally opposed to their existence.

### Audience

LGBTQ Republicans

### On-the-ground actions from transcript

- Reassess your alignment and support within a party that may not fully accept or support you (implied).
- Take stock of whether your actions are truly in your best interest as an individual (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the futile attempts of LGBTQ Republicans to gain acceptance within a party that fundamentally opposes their existence.

### Tags

#TexasGOP #LogCabinRepublicans #Trump #Authoritarianism #LGBTQCommunity


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Texas
and the GOP there and some divisions that have arisen
between the state GOP,
a certain demographic of Republicans, and Trump.
So if you don't know what happened,
the state GOP told log cabin Republicans
not to even worry about showing up.
Don't even come.
You're not setting up a table.
If you don't know who log cabin Republicans are,
they are the LGBTQ branch within the Republican party.
I know a whole lot of people are gonna be surprised
that they exist, but they do, okay?
They were told, don't worry about coming up
to this convention, to this meeting.
You're not setting up a table, blah, blah, blah, blah.
Trump stepped in and was like, what are y'all doing?
These people are on our side.
And that kind of, I guess in some ways,
made the log cabin Republicans feel better.
Here's the thing.
Y'all are failing to see horror on horror's face.
That's what's happening.
After this happens,
the log cabin Republicans Twitter account
puts out the statement, LGBT conservatives don't agree
with the left's notion that sex and gender are meaningless.
Sex and gender are meaningless.
Okay, first point of order.
The left, as you would call them,
understands that sex and gender are two different words
with different meanings, okay?
It's not the left that believes
the difference there is meaningless.
That's the right.
But skipping past that, what's the point of this tweet?
We're one of the good ones.
That's what you're trying to do, right?
Here's the thing, they don't care.
They don't care.
That rhetoric, all of those statements you hear
about the left and this demographic,
yeah, that's aimed at you too.
They do not care.
That rhetoric, at no point do they say,
oh, check for party affiliation.
No, they view you as their cultural enemy.
And you're sitting there trying your best to show,
oh no, I'm one of the good ones.
You can trust me.
They don't care.
And there is no bigger proof of this
than Trump stepping in, right?
It shows that it doesn't matter.
The Republican Party is embracing
authoritarianism in a big way.
In a way, they are getting their base to fall in line
and do what they're told is by othering people.
The LGBTQ community is one of the groups
that has been slated to be othered.
They don't care that you're Republicans.
They do not care.
With Trump stepping in, with you saying it's weak leadership,
that's not what it is.
They're just appealing to their base.
The authoritarianism has taken on a life of its own.
It's Dr. Frankenstein's monster.
It is beyond Trump.
It's the base now.
This was repeated, this rhetoric.
It went on so long that it's now just part of it.
It doesn't matter.
And as you sit there and work and try
to show that you're one of the good ones for them,
you are working to take away your own rights.
You are working to make sure that when
that mob with the pitchforks and torches comes,
that they're well amped up and that base is really energized
and understand when they come, they are not
checking your voter registration card
to see if you have an R by your name.
You need to wake up.
There is no amount of trying to show that you're different.
They know you're different.
And to them, that's the problem.
You're not going to be able to ingratiate yourself
into a group that the majority don't believe you should exist.
You need to stop.
And you need to take a long pause
and take stock of what you're doing
and whether or not it's in your interests as an individual.
I mean, we understand that the sake of power has in many ways
led you to believe that you're not
going to worry about the interest of a collective group.
But you as an individual, if it's in your interest, it's not.
If it's in your interest, it's not, I'll go ahead and tell you.
You have been othered by your party.
They use you as a scapegoat.
You cannot demonstrate that you're different.
They don't care.
Doesn't matter if you align with the Republican Party on every
single issue.
You have this thing, this immutable characteristic
that you cannot change and that they've decided is wrong.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}