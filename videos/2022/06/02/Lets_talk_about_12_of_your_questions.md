---
title: Let's talk about 12 of your questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mLMuQu8wIZM) |
| Published | 2022/06/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Refuses to let the channel be dominated by content on firearms and gun control, opting to bulk questions together and address them collectively.
- Questions proposed solutions like making all guns pink and glittery, banning semi-automatics, and creating public armories, but Beau urges consideration of Second Amendment rights and Supreme Court interpretations.
- Beau explains why he doesn't advocate for banning all semi-automatics, citing concerns about authoritarianism and removing means of defense for vulnerable groups.
- Suggests a way to craft an assault weapons ban with real teeth by listing exemptions rather than defining what constitutes an assault weapon.
- Expresses reservations about proposals like liability insurance or taxing ammo, fearing they could disproportionately affect lower-income individuals.
- Clarifies that while the AR-15 can be used as a varmint gun due to its chambered caliber, it is not the primary reason most people own it.
- Addresses concerns about universal background checks potentially leading to a firearm registry and explains why he is not overly concerned about the government seizing firearms.
- Talks about a decline in mass incidents before the assault weapons ban, cautioning against attributing causation to correlation.
- Dismisses the claim that cops couldn't use a flashbang at a school due to civilians present, expressing frustration at the lack of clear answers in such situations.
- Advocates for working with those motivated by reducing violence, even if they lack expertise in firearms, and criticizes toxic masculinity's impact on gun culture.

### Quotes

- "People are dying. And that's their motivation."
- "To be a man, you've got to have this gun."
- "The gender roles in this country are blurry."

### Oneliner

Beau bulk addresses questions on firearms, advocating for solutions that prioritize rights, challenge authoritarianism, and confront toxic masculinity in gun culture.

### Audience

Gun control advocates

### On-the-ground actions from transcript

- Advocate for solutions that prioritize rights and challenge authoritarianism (implied)
- Address toxic masculinity within gun culture (implied)

### Whats missing in summary

The full transcript provides in-depth insights into Beau's perspective on gun control, addressing various proposed solutions and underlying societal issues related to firearms.

### Tags

#GunControl #SecondAmendment #ToxicMasculinity #Firearms #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a dozen
of your questions, questions that y'all have sent in.
The questions aren't going to stop any time soon,
but I refuse to turn this channel
into a channel that is just dominated by content
about firearms and gun control.
I left that crowd a long time ago.
So what we're going to do is we're
going to bulk the questions together.
When they come in, and then I'll just run through them
and answer them as best I can.
If there's something that truly does warrant a full video,
I'll do it.
Or if there's a major development.
But other than that, this is how we're going to do it.
So it's not every single day.
OK, so the first question, and this
is actually a whole bunch of questions all kind of put
together.
Why can't we just, and then make all guns pink and glittery,
ban all semi-auto cells, reactivate the assault weapons
ban, stop ammo cells, have public armories,
so on and so forth.
Anytime you're going to propose something, start with this.
Does it violate the actual Second Amendment?
And then, if it doesn't, then you have to ask yourself,
does it violate the current Supreme Court's interpretation
of the Second Amendment?
If it does, I'm not saying you can't advocate for it.
I'm just saying that there are probably more effective uses
of your time.
I mean, advocating for something you know
won't actually be implemented.
It's thoughts and prayers.
And I think that there are better uses of time.
And I'm not saying that to be mean.
I know that a whole lot of these, I mean, some are jokes,
but a whole lot of these are good faith solutions.
And in my ideal world, I love the idea of a public armory.
I think that's a great one.
But that's not the world we live in.
And the Supreme Court has final say on this.
OK, so and then the next question
is, why don't you advocate for all semi-autos to be banned?
Me, personally.
Two reasons.
One, the one I just said, that would run
afoul of the Supreme Court.
So it's a non-starter, even if it was to get passed.
And then there's a second reason.
Howdy there, internet people.
It's Beau again.
Today we're going to talk about the 14 characteristics of,
and if you've been watching the channel,
you know what I'm about to say next.
This country is in a slide towards authoritarianism,
a big one.
I'm not comfortable with the idea of removing
what might be certain groups, those that would be scapegoated.
I'm not comfortable with the idea of removing their own weak
means of defense.
So when it comes to me personally,
I don't follow the NRA.
I don't follow gun owners of America
or any other of these gun groups on social media.
I follow armed equality.
There are certain demographics that if we slide
into authoritarianism, they're going to be at real risk.
So that's why I don't actually support it.
I think that there are ways to stop it
that would actually be more effective
that don't include that.
So that's why.
Could you craft an assault weapons ban with real teeth?
Could you craft an assault weapons ban with real teeth?
Yeah, yeah.
You start, you do it the exact opposite way
that they did the first one.
You're banning all semi-automatic rifles except,
and then you list the exemptions,
like except rifles that fire rimfire ammunition,
squirrel guns.
That's how you would do it.
You start from the other end.
Rather than trying to create a list of things
that would count as an assault weapon,
you start on the other side.
Now, do I support that?
Not really.
Would it survive a Supreme Court challenge?
No.
But that's how you would have to do it.
And you have to understand that the Second Amendment
is a huge block to getting the kind of gun control
that is the big talking point.
It doesn't stop effective gun control
because there's a whole lot of other things
that could be done.
But as far as the big blanket magic bullet answers,
the Second Amendment kind of blocks most of that,
especially under this court.
Okay.
What about, okay, so what about liability insurance
or taxing the ammo?
I don't think it's a good idea to create a situation
where only the wealthy have the ability to also do violence.
I don't think that's a good route,
especially given the fact that when you look at it,
most of the people involved,
with the exception of the teens,
when you're talking about the shooters,
with the exception of the teens,
they're people that absolutely would have been able
to afford the insurance.
I don't believe it would have stopped anything.
And what you end up doing is making it unavailable
to lower income people,
which disproportionately impacts certain groups.
And that's not something I'm okay with.
Is the AR-15 a varmint gun?
Okay, so that comes from the 223 round being a varmint round.
And it is.
That is true.
Is the AR-15 a varmint gun?
That's not really what causes it.
It's not the design of the gun that makes it more powerful.
It's the caliber that it's chambered in.
So the 223 round, yeah, it's a varmint rifle.
Okay, that's what it is.
And the AR, most ARs are chambered in that.
So it's not a disingenuous statement.
So yes, but also no.
It's not the design of the weapon.
It's the caliber of the round that's in it.
It's also worth noting that the real design for the AR
didn't have a little round.
It didn't have the 223 round.
It had a 308 round, a much more powerful round.
Some bean counters in the Pentagon
decided they wanted it done differently,
and the weapon was changed.
It's worth noting that the Army is currently
in the process of switching to a weapon that
is incredibly similar to the original design,
the more powerful design.
Perhaps they should have just let Eugene Stoner do his job.
But so is the AR-15 a varmint gun?
Yes, it can be used as a varmint gun.
It is a varmint rifle.
Is that why most people own it?
Absolutely not.
No.
That's silly.
But it's not a lie when people say that.
Could it be the looks under the ban
that kept psychos from buying it?
This is interesting to me.
In a recent video, if you don't know, in a recent video,
we went through the assault weapons ban.
I talked about how it didn't actually stop any of these
weapons from being sold.
It changed the way some of them look.
And this was a question that kind of popped up
in response to that.
And I got to be honest, I can't say that that may not
have had something to do with it in some of the instances.
So the AKs, as shown in that reference to video,
the video I linked to, it looked like a hunting rifle
when they were done with it.
AKs were still available, but it looked different.
So maybe they didn't want to buy it because they do,
they want to look tough.
OK.
I can kind of see that.
The problem is that ARs still looked like ARs.
It didn't change that.
They're almost indistinguishable, except for
people who really understand firearms,
you don't see a difference.
But it may have had something to do with those
looking at other platforms.
It's interesting.
I don't have a real answer to that.
I mean, could it be?
Yeah, it could be.
Do I know that?
No.
Why don't they want universal background checks?
And why don't you seem to care about this as an issue?
OK.
So it's not that the pro-gun crowd is actually
opposed to universal background checks.
And I know you're about to say, yes, they are.
No, they're not really.
That's not really what it's about.
They're concerned that universal background checks will
lead to a registry of all the firearms.
I know if you're a pro-gun control person,
you're like, yeah, that's a good idea.
They don't think so.
They watched Red Dawn one too many times.
And they saw that scene where it's like, oh, well,
go get the list of all the people who bought a firearm.
And we'll go round them up.
And that has led to this fear and this idea
that they don't want a registry.
And they see the universal background check
as the first step towards that.
The reason I don't care is because,
as I've told people who want to go after all
the guns in the country, do you have any idea
how long that's going to take?
I'm not worried about the government
trying to go door to door to seize firearms.
That's a matter of logistics.
And that doesn't matter if you're
talking about something that is a pro-gun or a pro-gun control
talking point.
The logistics of doing that are just, that's not a thing.
We'll be shooting at each other with lasers
by the time that gets finished.
It's the United States.
More guns than people.
So I don't see that as a realistic worry.
I don't think that the government having
a registry like that means that that registry is actionable,
that they would be able to do anything with it.
And then the flip side to that, when
you're talking about the worry about the registry,
is that most of these people who go out
and they have all of these guns, they bought them
with their debit card.
The registry already exists.
So it's an unrealistic fear to me.
I just don't see it as a real issue as far as opposing it.
When I talk about solutions that I think would work,
they include background checks.
Why didn't you bring up the fact that the steepest five-year
decline in mass incidents was before the ban?
Yeah, that's true.
It's true, and it's useful to showcase one thing.
But it's not quite the gotcha that a lot of people
would make it out to be if you put that information out there.
For those that don't know, during the assault weapons
ban, the number of mass incidents,
it ran pretty much flat, trended down a little.
But the sharpest decline was actually from,
I want to say it was 84 to 90, somewhere in that range.
There's a five-year period where it's just like, boom, boom,
boom, boom, boom, just straight down.
It just drops each year.
It's correlation.
It's not causation.
That goes both ways.
Everything that I see from everything
from before when the ban ended all the way back, all of that
is pretty consistent.
So the changes in it, given all of the information,
they're correlation.
It's not causative.
So I don't see the value in saying, oh, well,
it really happened.
The real decline was before the ban,
because it's not the lack of a ban that made it decline.
It's useful for pointing out that it is correlation.
And I will say it at times for that,
to be like, yeah, no, here.
But it's not actually a valid point in and of itself.
It's just a useful tool to help establish
correlation over causation.
Is it true that cops couldn't use a flashbang
at that school because there were civilians present?
No.
No, that is not true.
That is the exact opposite of true.
Yeah, you can absolutely use a flashbang in a room
where there are civilians.
Sure, there might be hearing damage.
Maybe, worst case scenario, they get a burn.
But I am 100% certain that the parents would prefer that.
When it comes to, and there's a whole bunch
of other questions about why didn't the cops do this,
I have talked to SWAT people.
I have talked to high-speed military people.
I have talked to contractor people.
Nobody has a clue.
Nobody has a clue.
They're not going to do it.
Nobody has a clue.
The array of options that were available is just endless.
Why it went down the way it did, nobody really has an answer.
There's going to be a probe and an investigation
into what happened.
And I just can't wait to see the results.
Because the answers that we were able to come up with
are, some are, let's just say they're all very accusatory.
So I can't wait to see why.
I can't wait to see why.
And I have a feeling that there are going
to be cops that need lawyers.
How can you be so patient with people
who don't know about guns but try to take them?
Because I don't think that people need
to be an expert on firearms.
I don't think that that should be an expectation.
I don't think it should be an expectation of society
that people within that society from the age of kindergarten
up have a functional knowledge of the difference
between cover and concealment.
I would suggest if that's a requirement for your society,
your society is broke.
They don't know a lot about guns.
Yes, so they propose ineffective stuff at times.
But you know what they do know?
People are dying.
And that's their motivation.
I am totally willing to work with that.
I actually have less patience for those people
who do understand guns but don't want to use that knowledge
to get to somewhere where people in kindergarten
don't need to know the difference between cover
and concealment.
Those people are far more irritating to me.
We have a problem pretending that we don't.
It isn't going to solve it.
It's just going to make it worse.
And yeah, ineffective stuff gets proposed all the time.
Yeah, absolutely.
And there are a lot of times when I do,
I've got to be like, yeah, that's totally not going to work.
And that may actually backfire.
But I would rather have that conversation than somebody
who is just bent on the idea that everybody
in this country from grade school up
needs to understand how to survive a firefight.
I'm my patience wears thin on the other side of that.
Why don't we have women shooters?
Just to appease people, we do.
There are a few.
The instances are so rare that this statement as a generality
is 100% fair.
My answer to that is that I believe
it is inextricably tied to toxic masculinity in this country.
And it is amplified within the gun culture of this country.
And it makes it even worse, creates that idea
that to be a man, you've got to have this gun.
And then men who do not have the coping skills necessary to deal
with everyday life find violence as an outlet.
And it is constantly reinforced to a major degree
within the gun crowd.
There's that Bushmaster ad, picture of an AR.
Consider your man card reissued, because that's
what makes you a man.
Or the ever-present meme of the AR bolt. It's just a gun part.
And up at the top, it says, if your boyfriend doesn't
know what this is, he's your girlfriend.
Ha.
So I think that it's toxic masculinity
that has a lot to do with it.
Now, just like anything else, if it isn't treated,
if you don't solve this problem, it will spread.
And eventually, you very well might have women shooters
as well.
Because that image of masculinity,
it's not actually defined by sex.
So that's a huge part of the gun culture.
It's not actually defined by sex.
That's a gender thing, which given the way the gun crowd
typically aligns, that's going to be a hard conversation
to have anyway.
But the gender roles in this country are blurry.
So you may eventually have a higher prevalence
of women shooters because of toxic masculinity.
So there are the questions.
And this is just how we're going to do this from now on.
Because I don't want the channel to just
be overrun with constant gun stuff.
So I hope that helps.
If you have more, when they come in, inevitably,
they're just going to go on a list like this.
And I'll put them out every so often.
So it may take longer than normal to get responses.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}