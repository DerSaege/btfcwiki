---
title: Let's talk about some big news from the G7....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=D71BdN51hlY) |
| Published | 2022/06/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The G7 countries have agreed to stop funding overseas development of fossil fuels by the end of this year, transitioning $33 billion from dirty energy to clean energy.
- One significant agreement is getting coal out of the electricity sectors, with five countries aiming for 2030 as the end date, while the United States and Japan pushed for 2035.
- Despite the pushback, the final agreement lacks a specific date, turning into an international poker game where countries are likely using each other's actions to pressure the US and Japan.
- The United Kingdom recently produced an excess of wind electricity, leading to turbines being slowed down due to storage limitations, showing progress in green energy production.
- The collective stance of other countries to move forward without waiting on the US signals a global impatience with delays in environmental commitments.

### Quotes

- "No more gas, coal, oil, anything like that, starting from the end of this year."
- "Coal is over. I wouldn't start a career in coal right now."
- "International poker game where everybody's cheating."
- "Overproduction of green energy to the point where they have to slow it down, that's good news."
- "A lot of other countries are tired of waiting on the United States."

### Oneliner

G7 countries agree to cut funding for overseas fossil fuels, shift to clean energy, and push for coal phase-out without definitive dates, prompting a global impatience with environmental commitments.

### Audience

Environmental advocates, policymakers

### On-the-ground actions from transcript

- Advocate for clean energy initiatives in your community to push for a faster transition away from coal and fossil fuels (implied).
- Support and participate in local renewable energy projects to reduce reliance on dirty energy sources (implied).

### Whats missing in summary

Further insights on the potential repercussions and challenges in navigating international agreements towards a sustainable energy future. 

### Tags

#G7 #CleanEnergy #CoalPhaseOut #GlobalCommitments #EnvironmentalAdvocacy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we have some good news for the environment.
The G7 came to some agreements.
One is probably going to get most of the attention,
but the other one is more interesting to me.
We'll start with the one that's going to get the attention.
The countries in the G7 have decided
that from the end of this year on, they will not fund overseas
development of fossil fuels.
No more gas, coal, oil, anything like that,
starting from the end of this year.
So this, I want to say it was $33 billion
that it would transfer from dirty energy to clean energy.
That's what's going to get the news.
Because it's definitive, and it has dates and numbers
and all of that stuff.
But there's something a little more important,
or at least I think it's a little more important.
Coal is over.
I would not start a career in coal right now.
So the countries have agreed to get coal out
of their electricity sectors.
There's no definitive date, though.
The thing is, we know what happened
and why there's not one, though.
We have a really good idea.
Five of the countries wanted that to be 2030.
No more coal in the electrical system,
the electrical grid past 2030.
The two holdouts were the United States and Japan,
which is not really surprising that the US is on that part.
But they said that they couldn't get it done until 2035.
Basically needed another five years.
What would normally happen in a situation like this
is that the other five countries would say fine,
and they would set the date of 2035.
Instead, the language that we've seen so far
says that it's a priority, and there's no date.
International poker game where everybody's cheating,
I'm pretty sure that the five countries,
other than the United States and Japan,
they were showing each other their cards.
And they're just going to go full steam ahead and get
rid of coal as soon as possible, and then
use that to guilt the United States and Japan
into acting faster.
That way they don't have a hard date to say,
oh no, we still have three more years.
No, we said we'd make it a priority.
Y'all need to hurry up.
It's interesting.
I think that's probably what we're looking at there,
and I like it.
This news comes right on the heels of the United Kingdom
producing so much electricity via wind
that they had to tell the turbines to slow down,
that they weren't using it all, and they didn't have
any more battery to store it.
Overproduction of green energy to the point
where they have to slow it down, that's good news.
The G7 countries saying, hey, we're
not going to fund overseas dirty energy projects anymore.
That's good news.
The commitment to get coal out of the energy sector,
out of the electricity sector, that's good news.
And I think it's really good news
that a lot of other countries are tired of waiting
on the United States, and that is certainly
what this appears like.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}