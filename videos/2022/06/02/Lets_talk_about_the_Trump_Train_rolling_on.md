---
title: Let's talk about the Trump Train rolling on....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ImWvy7iKToE) |
| Published | 2022/06/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's political career has seen people falling off the train or being run over.
- At a recent rally, a new dress code banned logos associated with Q and the Proud Boys.
- People with VIP tickets from the Trump campaign were denied entry for wearing banned logos.
- Trump no longer finds groups like QAnon and Proud Boys beneficial, so they must hide their identities or disappear.
- Trump wants their support but doesn't want to be publicly associated with them, hence the ban on logos.
- Private security at the rally indicated frustration with a hand gesture, industry code for aggression.
- Removing key elements from his core base will make rebranding difficult for Trump.
- Some supporters will understand the optics, while others may be disheartened by Trump's actions.
- Trump is pushing away certain supporters, surprising those who believed he was on their side.
- The scenario raises questions about the demographic Trump will target to replace these supporters.

### Quotes

- "Trump's political career has seen people falling off the train or being run over."
- "Trump wants their support but doesn't want to be publicly associated with them."
- "Removing key elements from his core base will make rebranding difficult for Trump."

### Oneliner

Trump is pushing away key supporters, making rebranding challenging and causing a divide among his base.

### Audience

Political analysts, Trump supporters

### On-the-ground actions from transcript

- Analyze shifting political demographics to understand potential impact (implied)
- Stay informed about political developments to make informed decisions (implied)

### Whats missing in summary

Insights on the potential long-term consequences of alienating core supporters from a political perspective.

### Tags

#Trump #Supporters #Rebranding #PoliticalAnalysis #Demographics


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Trump train rolling on and who it's rolling over
now.
It should come as no shock that throughout Trump's political career a lot of people have
kind of fallen off the train or wound up under its wheels.
At a rally recently there was a new dress code in effect.
People were not allowed to enter if they were wearing certain logos, namely anything associated
with Q or anything associated with the Proud Guys.
And they had an issue with this and there was footage of them having a conversation
with cops who were providing security and private security asking why they weren't
allowed in and flat out telling them it's because of what you're wearing.
And they're saying but we got VIP tickets from the Trump campaign.
At this point it shouldn't be a mystery, these groups have outlived their usefulness to Trump.
So now they have to either present themselves in a way that camouflages who they are so
he can continue to disavow these movements or they just have to go away because he doesn't
feel that they're beneficial to him anymore.
He wants this sort around, he likes these ideas, likes their support, but doesn't want
it to be publicly associated with them.
That's why the logos can't be there.
The option of turning the shirt inside out was offered but did not appear as though that
was taken.
If you have seen the footage or are going to watch it, just as a side note, in the very
beginning the private security, the guy in the green shirt, brings his hands up to the
middle of his chest and he's just kind of holding them there like that.
Just so y'all know, in case you are ever in a situation like this, that is industry code
for I wish you would.
I've had a bad day and I would really love to level you right now.
The private security was so done with that conversation by the time the footage that
I saw started.
I'm curious what demographic Trump is going to try to reach to replace.
These are two key elements within his core base.
Sending them packing, sending them away like this, is going to be a really hard rebrand
for Trump.
Some will understand the optics of it.
Others will probably be very disheartened by the actions of a man that they believed
was on their side.
That they believed didn't care what those libs thought.
And now he is pushing them off the train.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}