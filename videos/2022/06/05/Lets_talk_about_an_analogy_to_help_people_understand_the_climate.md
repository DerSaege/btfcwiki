---
title: Let's talk about an analogy to help people understand the climate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pRD66T8xzag) |
| Published | 2022/06/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the current state and future projections regarding carbon dioxide levels.
- Emphasizes that human civilization has never experienced the current carbon dioxide levels before.
- Mentions the significant impact of high carbon dioxide levels on sea levels, vegetation, and crops.
- Points out that despite taking action, the situation will continue to worsen due to past milestones being surpassed.
- Compares those ignoring the environmental crisis to appeasers in the 1930s, suggesting that delaying action worsens the outcome.
- Calls for a global mobilization similar to World War II to address the environmental crisis.
- Stresses the urgency of taking action immediately and not waiting for tomorrow.
- Criticizes the denial of the crisis and the insistence on outdated technologies and dirty energy sources.
- Warns that siding with inaction is akin to being on the wrong side of history.

### Quotes

- "Human civilization has never seen this level."
- "The time for action was yesterday."
- "Going down this road is being an appeaser. It's siding with the bad guys."
- "You're on the wrong team, and history will judge you for it."
- "The longer it gets ignored, the worse it will be, just like back then."

### Oneliner

Beau addresses the urgent need for immediate action on escalating carbon dioxide levels, warning against delaying global mobilization to combat the environmental crisis.

### Audience

Global citizens

### On-the-ground actions from transcript

- Mobilize globally to address the environmental crisis (implied)

### Whats missing in summary

Further details on the historical context of appeasement in the 1930s and the potential consequences of inaction.

### Tags

#EnvironmentalCrisis #ClimateChange #UrgentAction #GlobalMobilization #History


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to
talk about where we're at, where we're headed, where we've been, and we're going to
frame this in a manner in which I think those who are reluctant to accept where we're at
will understand. New report says that currently carbon dioxide levels are 50% above pre-industrial
levels, more than 50% above. Currently it is 421 parts per million for carbon dioxide. During the
period right before the industrial revolution it was 280 parts per million. It's a big increase.
What does this mean? Human civilization has never seen this.
Human civilization has never seen this level. The last time it happened was more than 4 million years
ago when sea levels were 16 to 82 feet higher than they are now. It was also a whole lot warmer,
which means a whole lot less vegetation, which means a whole lot less crops.
That's where we're at and where we've been. Where we're going, it's going to get worse.
It's going to get bad. Understand people have been talking about the climate and saying, hey,
if we don't act it's going to get bad. If we do act it's still going to get bad. If we start acting
in a dramatic fashion right now it's still going to get bad. We are past the point where we're
going to get bad. We are past a lot of milestones, a lot of tipping points that are going to change
things. The speed at which these increases are occurring, it outstrips what occurs naturally.
So what's happening is we've hit these levels and we're going to experience the effects of that,
but we're not going to experience them for, you know, another 10, 15 years.
No matter what, since that's already happened, since we already reached that level,
we're going to experience those effects, even if we change it today.
It takes time. It's that 18-wheeler that doesn't stop on a dime.
Okay, so let's do this another way. If you are somebody who is ignoring or neglecting
or doesn't want to accept the current situation with the environment and climate, you are an
appeaser in the 1930s. You are one of those people who are like, if we just ignore it,
he'll eventually get tired of spreading out his little empire. That's who you are. The longer
it gets ignored, the longer it takes the world to unite and respond, the worse it will be,
just like back then. The world is going to have a mobilization. They're going to have a mobilization.
This entire planet is going to have to mobilize in a similar fashion, just like during World War II.
The world is going to have to unite and act. We can do it now, but we're not going to be able to
do it tomorrow, where we know there's a problem, things are going to get bad,
or we can wait until he builds Fortress Europe. That's where we're at. The time for action was yesterday.
Today, we're already going to have bad moments. Things are going to get worse. Denying this,
screaming to continue the use of technologies that are outdated, continue the use of dirty energy
when we don't have to, when there are things that can replace it, even if they're not perfect.
Going down this road is being an appeaser. It's siding with the bad guys. You're on the wrong team,
and history will judge you for it. Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}