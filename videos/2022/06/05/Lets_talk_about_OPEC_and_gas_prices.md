---
title: Let's talk about OPEC and gas prices....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q1WNTbqLz0I) |
| Published | 2022/06/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- OPEC plans to increase oil production by over 600,000 barrels per day, but it won't likely impact prices significantly due to other global factors.
- OPEC previously reduced production during the pandemic to maintain high prices artificially, leading to the current situation of high gas prices.
- The prolonged period of high gas prices may drive more people towards electric vehicles as an alternative.
- Consumers may opt for electric cars over traditional gasoline vehicles to avoid dependency on OPEC and potential future price manipulations.
- OPEC's influence on the oil market has diminished, and their attempts to control prices may backfire, leading people to shift away from oil and gas.
- The push towards electric vehicles could result in a positive environmental impact and trigger a significant shift in the market.
- Beau expresses dissatisfaction with high gas prices but acknowledges the potential positive outcome of more people transitioning to electric vehicles.
- The increase in OPEC's oil production may not lead to immediate relief in gas prices, as historical trends indicate their struggles in meeting production targets.
- Issues within OPEC countries and production challenges may hinder their ability to achieve the stated production goals effectively.
- Beau remains skeptical about OPEC's ability to stabilize gas prices despite the announced production increase.

### Quotes

- "The ultimate irony in this is that this extended period of high gas prices is a pressure pushing people away from their product."
- "The more people purchase electric vehicles, the less expensive they become, the more of them enter the secondary market, and it just kind of snowballs."
- "It's not really a silver lining, it's just something that I've noticed and been thinking about since they made the announcement that they were going to put out an additional 600,000 barrels a day."

### Oneliner

OPEC's attempt to increase oil production may not alleviate high prices, potentially driving more towards electric vehicles and impacting the market shift.

### Audience

Consumers, Environmentalists

### On-the-ground actions from transcript

- Research and invest in electric vehicles to reduce dependency on oil (exemplified)
- Advocate for sustainable transportation options in the community (exemplified)

### Whats missing in summary

The detailed analysis and implications of OPEC's decision on oil prices and the shift towards electric vehicles.

### Tags

#OPEC #OilPrices #ElectricVehicles #MarketShift #EnvironmentalImpact


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk a little bit about OPEC and oil and electric cars and why
OPEC may not be a big player in the future.
OPEC has decided it's going to dump, I want to say it's a little bit more than 600,000
barrels per day onto the market.
The thing is, it's probably not going to change prices very much.
With the other things going on in the world, it's probably not going to be a huge shift.
We're not going to see a change in price at the pump right away, which means there will
be an extended period where prices are really high.
And we got to this point because OPEC wanted to keep prices high artificially.
During our public health issue, OPEC cut back production.
They rolled back what they were putting out to create artificial scarcity to keep the
price up.
Then as demand increased, they had trouble scaling back up, which put us in the situation
we're in.
The ultimate irony in this is that this extended period of high gas prices is a pressure pushing
people away from their product.
This is a force that is driving more and more people to look towards electric vehicles.
I mean, if you're going to buy a car right now, are you going to buy something that gets
20 miles to the gallon, or are you going to look at one of the new EVs?
I think a lot more people are going to start making that decision because they don't want
to be beholden to OPEC, who is very likely to engage in the same type of market manipulation,
the desire to create that artificial scarcity to keep prices up in the future, which means
when demand kicks back up in the future, after that slow period, prices will shoot through
the roof again.
OPEC used to be able to heavily influence the oil market with its production capability.
They don't have that anymore, not really, not to the same degree they used to.
So their desire to control everything and keep everybody paying a certain amount for
their product may be the force that completely undermines them.
There are new technologies, there are new things coming online, and they don't require
gasoline.
They don't require OPEC's product, and the more OPEC tries to control the market, especially
when they do it poorly, and the prices go up, the more people look away from oil and
from gas.
From a personal standpoint, I'm not real happy about this.
I don't like going to the gas station right now, but from an environmental standpoint,
I mean, this may be something that pushes people in a direction that triggers that avalanche,
because the more people purchase electric vehicles, the less expensive they become,
the more of them enter the secondary market, and it just kind of snowballs.
And this is something that may not have happened without OPEC making the mistakes that it did,
without the price of gas going up as much as it has.
It's not really a silver lining, it's just something that I've noticed and been thinking
about since they made the announcement that they were going to put out an additional 600,000
barrels a day.
I'm skeptical of that, because they haven't been making their production targets as it
is.
So we're going to have to wait and see, but my guess is, based on the last few months,
I don't see this announcement from OPEC as being something that is necessarily going
to bring gas prices down anytime soon.
It might stop them from going up more, but I don't see a drop coming right away, which
normally when OPEC tries to influence the market, historically it's been pretty successful.
But they've made a lot of mistakes, and it's going to be hard to catch up, because there
are issues inside OPEC countries, there are production issues that are causing them to
miss the targets they already have.
There's a lot of little things that are going to factor into them probably missing these
production goals, which is... it may be an announcement of an event that isn't going
to happen.
So I wouldn't expect relief on this one anytime soon.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}