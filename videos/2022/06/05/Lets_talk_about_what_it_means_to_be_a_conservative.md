---
title: Let's talk about what it means to be a conservative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pTibmKAakbU) |
| Published | 2022/06/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines what it means to be a conservative, focusing on maintaining the status quo and traditional values.
- Points out that being conservative means defending the establishment and resisting change.
- Notes the irony in conservatives claiming to want to restore the Constitution while opposing change.
- Challenges the idea of wanting things to remain as they were in the past as defending the status quo.
- Emphasizes that conservativism inherently supports the current system and establishments.
- Urges letting go of the past and embracing change for improvement.

### Quotes

- "If you are a conservative, by default you support the Pelosi's of this world."
- "The worst reason in the world to do something is we've always done it that way."

### Oneliner

Being conservative means defending the status quo and establishment, resisting change, and not truly supporting a better world.

### Audience

Political activists

### On-the-ground actions from transcript

- Challenge traditional views (implied)
- Embrace change for improvement (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the conservative mindset, illustrating the conflict between defending the status quo and advocating for a better world through change.

### Tags

#Conservatism #PoliticalIdeologies #Change #Establishment #StatusQuo


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk about what it means
to be a conservative.
Um, I got a message basically saying,
hey, you know, my dad watches you pretty often,
but he really gets mad when you suggest
that it's progressives who want a better world.
Conservatives want a better world too.
No, no, that's not what it means.
I'm sorry, you can get mad about that.
But the very definition of the word conservative
says otherwise.
Being a conservative means you're
looking at the status quo as good.
You're trying to maintain what exists.
It's pro-establishment.
It's pro all of this.
Conservative, averse to change or innovation
and holding traditional values in a political context,
favoring free enterprise, private ownership,
and socially traditional ideas.
It's about maintaining the establishment.
It's about maintaining the status quo.
You can dress it up in any revolutionary rhetoric
you want to, but it's just dressing.
The idea of being a conservative is to defend the system as it exists.
Well that's not true.
We want to change it.
We want to restore the Constitution.
News flash, that's the system as it exists.
Well no, no, see they went away from the Constitution.
Then the Constitution was powerless to stop it.
ultimate irony there is that if you really want to follow the ideas of the
people who put the Constitution out there, the people who brought this country
about in that manner, you probably need to understand that a whole lot of them
believe that the Constitution should be rewritten every 20 years. If your
Your position is that of things were better yesterday.
You are in fact defending the status quo.
You're defending the establishment.
If you are looking back to the 1950s, which is when people are normally looking back to
as this golden era, it wasn't, but if you look back, you are defending the status quo.
You're defending the establishment.
You want things to remain as they are, therefore you don't want a better world.
People can get mad about this, but that's just what it is.
It's your political position, not mine.
Don't get mad at me.
You can say, well, no, we want to adjust this thing and change this.
Which direction is it headed?
Are you trying to send us back to a quote, better time?
Or are you dealing with the realities of the world today and the fact that time marches
on?
Things change, it doesn't matter if you're reluctant to accept them, they will change.
The future will be different than the past, it is what it is.
a conservative, by definition means you want to uphold this system. Everything
you see. Everything that exists. If you are a conservative, by default you support
the Pelosi's of this world. If you are a conservative, by default you support all
of those big companies that are running you into the ground. That's what it means.
You have dressed it up and hung an American flag on it, but it doesn't
change the reality of your positions. You think things are fine the way they are.
Therefore, you don't support a better world. No amount of rhetoric, no amount of
window dressing, no amount of patriotic cloth is going to change that. It's just
what it is. If you don't like that it's because you know the current system
doesn't work. The current system is broken and things need to be improved. If
you want to do that you have to let go of the past. The worst reason in the
world to do something is we've always done it that way. Anyway it's just a
With that, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}