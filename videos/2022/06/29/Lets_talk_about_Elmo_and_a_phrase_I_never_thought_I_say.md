---
title: Let's talk about Elmo and a phrase I never thought I say....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yNjSjBzcs9I) |
| Published | 2022/06/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Elmo from Sesame Street got vaccinated, leading political commentators to spark outrage and claim Sesame Street has gone "woke."
- Some are pushing back against Elmo getting vaccinated, fearing it might encourage children to get vaccinated.
- These critics, who are political commentators, failed to see through Trump for years and supported his actions, including a coup attempt.
- Despite claiming politics as their expertise, they now want to influence opinions on vaccination.
- Polio cases, which had decreased significantly, are now on the rise due to vaccination gaps.
- A virologist emphasized the importance of ensuring everyone is vaccinated for polio, even in places like London.
- Beau urges people to focus on polio vaccination alongside debates about Elmo and trusting political commentators over doctors.
- The anti-science and anti-education stance adopted by many is contributing to global health risks.
- Beau stresses the importance of getting vaccinated for polio, advocating against misinformation spread by political commentators.
- He questions the credibility and motives of those who prioritize outrage over trivial issues while neglecting serious health concerns like polio.

### Quotes

- "Elmo got vaccinated, therefore Sesame Street has gone woke."
- "Those doctors don't know what they're talking about, right?"
- "Vaccination gaps are causing an issue with polio."

### Oneliner

Elmo's vaccination sparks outrage among political commentators, distracting from vital issues like polio vaccination gaps.

### Audience

Parents, educators, advocates

### On-the-ground actions from transcript

- Ensure everyone is vaccinated for polio (suggested)
- Focus on promoting accurate health information and vaccination efforts in communities (implied)

### Whats missing in summary

The full transcript provides more context on the dangers of misinformation and the importance of prioritizing public health over trivial controversies.


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about Elmo
from Sesame Street. And I'm going to utter a sentence I never thought I would have to say.
But here we are. If you missed it, Elmo, well, Elmo went out and got the shot.
You know, predictably, a whole bunch of political commentators are using this to
stoke and provoke outrage. Elmo got vaccinated, therefore Sesame Street has
gone woke. Now I'm not going to do one of those videos where I explain something
has kind of always been woke, because I've already done one on Sesame Street.
Instead, I want to talk about something else. The people who are pushing back
against the idea of a children's character getting vaccinated, because,
you know, they're concerned about it encouraging vaccination among children.
The people who are upset about this, the people who are trying to provoke
outrage over this, they are political commentators. By default, their area of
expertise should be politics. Yet, many of them are the same people who couldn't
see through Trump for years. For years. They bought everything that he sold.
Many of them continue to buy into it today. Some of them did it to the point where
they either intentionally, unintentionally, or unwittingly supported a coup attempt.
That's their field of study. That's their professed area of
expertise. And they weren't too good at that. But they want you to trust them.
Listen to their outrage over doctors. I don't know that that's a smart move.
Just saying. I want to point something else out. There's another disease that's
on the rise. And it's weird because it kind of went away for a while.
In 1988, there were 350,000 cases. By 2016, the number of cases was measured in the dozens.
Last year, it's back up to 600. And the reason it's being widely
attributed to vaccination gaps, people not being vaccinated, it's polio.
Polio. And I know that people, when they hear that, they're going to think,
well, you know, I don't live in Africa or South America or one of those other places,
so I don't need to worry about it. Yeah, they found it in the sewage of London.
It's so much of an issue that a virologist at the University of Saskatchewan
kind of came out and said, hey, y'all make sure that everybody's getting vaccinated for polio.
So while you are discussing whether or not Elmo is a positive influence on your child,
or perhaps the right-wing outrage political commentators are better people to take advice from
than your doctors, maybe also consider making sure everybody's vaccinated for polio.
Get vaccinated for polio. I have said a lot of things on this channel over the years
that I never thought I would have to say. Advocating for people to be vaccinated for polio,
that's pretty much at the top of the list. And in large part, it's because much of the world
has adopted an anti-science, anti-education, anti-intellectual stance.
And it is coming home. Don't worry about it.
Those doctors don't know what they're talking about, right?
Vaccination gaps are causing an issue with polio.
But sure, listen to the political commentator who today, during the surprise hearing about,
you know, what happened on the 6th, their concern is a character on Sesame Street getting vaccinated.
I'm sure they have your best interest at heart,
and they aren't just attempting to draw attention away from their previous mistakes.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}