---
title: Let's talk about what we can learn from Boebert and MTG....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c8daBlBEOtw) |
| Published | 2022/06/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People feeling down about electoralism.
- Two common responses: Democrats won't do anything or aren't progressive enough.
- Beau poses a question: Why not run for office at local, state, or federal levels?
- Challenges notions of lacking qualifications or establishment barriers.
- Mentions individuals like Lauren Boebert and "space laser lady" who ran successfully.
- Points out that they weren't establishment favorites but still won.
- Encourages the idea of moving the Democratic Party left through personal involvement.
- Suggests considering running for office as a form of civic engagement.
- Emphasizes the potential for individuals to make a difference through running for office.
- Beau encourages the audience to think about the impact they could have by running for office.
- Stresses that running for office is a way to actively shape the future of the country.
- Acknowledges that not everyone may be suited for electoral politics but urges those who are to take action.
- Concludes with a positive encouragement for individuals to give running for office a try.

### Quotes

- "Maybe you should run."
- "I think that you're probably better than Lauren Boebert or the space laser lady."
- "I think you could be more effective."
- "I think you could probably better the country."
- "Y'all have a good day."

### Oneliner

Be the change by considering running for office to shape the future of the country and move the Democratic Party left.

### Audience

Potential candidates for office.

### On-the-ground actions from transcript

- Run for office at local, state, or federal levels (suggested).
- Engage in civic participation by considering a political candidacy (implied).

### Whats missing in summary

The full transcript provides detailed encouragement and reasoning for individuals to actively participate in shaping politics by running for office.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So over the last few days, understandably, a lot of people
have been feeling down, especially those who are firmly
entrenched in the idea of electoralism.
And when you talk about the possibility of the Democratic
party gaining a wider majority.
You normally get two responses, and they're not
necessarily positive.
One is, well, they're not going to do anything anyway.
And then the other is, well, they're not aggressive or
progressive enough.
And I mean, I can't argue with that.
So I have a question.
And this isn't a gotcha.
It's a legitimate question.
I understand that this may not be for everybody.
But why don't you run?
Why don't you run for something?
Local, state, federal, why don't you run?
And when you ask that, you typically get either,
I don't have the qualifications, or the establishment
won't let me.
won't be able to win. I think you're smarter than Lauren Boebert and the
space laser lady. They did it. So maybe you could too. I mean understand when
When they got elected, they weren't the establishment favorites.
The RNC was not a huge fan of the MAGA crowd, but they got in the numbers and they moved
the Republican Party right.
Who's to say you can't do that with the Democratic Party?
Seems like it could be done.
And if you're somebody who is of the sort that really values electoralism, maybe it's
something you should consider.
I mean if there's anything that we have been shown over the last six years or so, it's
that anybody can get elected.
So maybe it's something you should look into.
And I'm not saying that as a call out or as some kind of gotcha.
I'm dead serious.
Maybe you should run.
What's the worst that happens?
You don't make it.
You introduce some points that maybe a challenger picks up, uses and becomes part of their platform.
Maybe you get elected and it's harder than you think, okay?
Or it could be like the Tea Party that then gave birth to the MAGA movement, which moved
the Republican Party right.
You could move the Democratic Party left.
You could make it more progressive, more aggressive.
It's not impossible.
There are a lot of types of civic engagement, you know.
I'm a huge fan of community building, community networks, that sort of thing.
That's where I fit in.
So this obviously isn't for everybody.
I'm not somebody who would be good in electoral politics, but maybe you are.
And if this is your concern, you literally are the solution to it.
run. I think that you're probably better than Lauren Boebert or the space laser
lady. I think you could be more effective. I think you could probably
better the country. Maybe try it. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}