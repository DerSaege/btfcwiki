---
title: Let's talk about Trump's denials from the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fS1soNwtQ-M) |
| Published | 2022/06/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Trump's denials in response to testimony presented in a surprise committee hearing.
- Trump denies knowing Cassidy Hutchinson and questions who was running the White House if he didn't.
- Raises skepticism about Trump's denial of not noticing an attractive woman working down the hall.
- Expresses doubts about Trump's denial of never complaining about crowd size.
- Comments on Trump's denial regarding making room for people with guns to watch his speech.
- Notes Trump's denial of trying to grab the steering wheel of the White House limousine.
- Mentions Trump's denial of throwing food as another false allegation.
- Expresses surprise at the lack of denial regarding more significant allegations like the coup attempt.
- Points out Trump's focus on denying allegations related to his image rather than substance.
- Comments on the narrow scope of the committee hearing and the compelling case being built against Trump.

### Quotes

- "I figured there might be a denial about the whole coup thing, something related to that."
- "He focused on crowd size and temper tantrum allegations."
- "The overall theme of this hearing is way beyond him throwing food."

### Oneliner

Beau analyzes Trump's denials in response to serious allegations, focusing on image over substance in a narrow-scope committee hearing.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed about committee hearings and testimonies (suggested)
- Engage with the political process and hold leaders accountable (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of Beau's analysis of Trump's denials in response to the surprise committee hearing, offering insights into the focus on image over substance.

### Tags

#Trump #CommitteeHearing #Denials #PoliticalAnalysis #Accountability #ImagevsSubstance


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Trump's denials.
We're going to talk about the things that the former president
is saying are false when it comes to the testimony presented
in the surprise committee hearing.
I've done some interviews in my day,
and something that has just always kind of tickled me
is what people choose to deny.
I've always found it very, very interesting and very telling
at times.
So we're going to go through Trump's denials.
He put them out on Truth Social, not under oath, but whatever.
We'll go through them because, I mean,
we should hear both sides of the story.
And I might have some commentary on some of this.
OK, so I hardly know who this person, Cassidy Hutchinson,
is other than I've heard very negative things about her.
OK, she was the chief of staff's aide.
If Trump didn't know who she was,
I want to know who was running the White House because it
surely wasn't him.
Aside from that, let's think about what we
know about the former president.
Do you really believe that the former president didn't
notice an attractive 20-something-year-old woman
right down the hall from him?
I am skeptical of this first denial.
Let's see.
Then he goes on to say he never complained about the crowd.
It was massive.
Given what we know about Trump's fixation
when it comes to crowd size, I have my doubts.
But OK, fine, he never complained about it.
I didn't want or request that we make room for people with guns
to watch my speech.
I don't actually think that was the allegation specifically,
but sure, whatever.
OK, good to know.
Her fake story that I tried to grab the steering
wheel of the White House limousine
in order to steer it to the Capitol building
is sick and fraudulent.
If I'm not mistaken, she was repeating what was told to her.
And sure, maybe not.
I mean, OK.
He's saying that's not true.
Fine.
And then he denies throwing the food.
Her story of me throwing food is also false.
All right.
Sure.
Fine.
Wait, that's it?
Weird.
Weird.
I figured there might be a denial about the whole coup
thing, something related to that.
I mean, that seems like that'd be important to get out.
But apparently, what matters to the former president
is people don't think that he throws temper tantrums
and that he wasn't worried about the crowd size.
There's not a denial in this batch of wannabe tweets
that says, I didn't want that to happen to the vice president,
or that I didn't want them to do that.
These are the allegations that were made today.
These are the ones that matter.
He doesn't say that he wasn't told beforehand
that it could go bad.
None of that was denied.
Just the stuff that relates to his image,
that's what he chose to deny, not the substance.
Now, he also called it a kangaroo court.
I would point out, this isn't a courtroom.
This isn't a courtroom.
I think that there may be, in the future,
the opportunity to issue denials in a more structured setting
if things keep going the way they are.
Because he is right about one thing.
We are hearing a narrow scope of what occurred.
I will give the former president that.
But as was said, even on Fox News, it's very compelling.
She's under oath.
He's on truth social.
The case that the January 6th committee is putting together
is compelling.
And thus far, they've had the receipts.
I think that the American people probably care more
about whether or not the president was OK
with the vice president getting hit
than they do about him throwing food in a temper tantrum.
This is one of those moments where the importance of image
comes into play, and you can see what matters to somebody.
There are very, very serious allegations
that were made in that testimony today.
He focused on crowd size and temper tantrum allegations.
The overall theme of this hearing
is way beyond him throwing food.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}