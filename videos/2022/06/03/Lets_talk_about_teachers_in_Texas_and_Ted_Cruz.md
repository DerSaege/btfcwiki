---
title: Let's talk about teachers in Texas and Ted Cruz....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5nT6LHDMO70) |
| Published | 2022/06/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Teachers in Texas are taking issue with Ted Cruz's approach of hardening schools and arming teachers.
- Teachers are asking to be soldiers, not cops, as the police didn't prove effective.
- Being a teacher in this scenario is likened to being a soldier in combat, requiring extensive training.
- Dealing with students' issues may involve sending them to specialists with weeks of training.
- Teachers are being asked to undertake roles that in the military require multiple individuals.
- Ted Cruz is promoting arming teachers, essentially asking them to take on multiple military roles.
- Cruz's approach puts teachers in the position of being first responders and asks too much of them.
- Cruz's history and actions are critiqued, questioning his election and character.
- Teachers are already tasked with significant responsibilities, including shielding children with their bodies.
- Beau urges for a reconsideration of the extensive tasks being placed on teachers.

### Quotes

- "Teachers are asking you to be soldiers, not police."
- "He's asking you to be a teacher on top of five other jobs in the military."
- "I don't think that our teachers need to be first responders and rangers and everything else that we're asking them to be."
- "He is quite literally a meme for running away from a flurry of snowflakes."
- "We're already asking them to literally shield our children with their bodies."

### Oneliner

Teachers in Texas resist Cruz's militarized approach, questioning the burden on educators tasked with protecting children.

### Audience

Teachers, educators, activists

### On-the-ground actions from transcript

- Question the militarization of schools and the burden placed on teachers (implied)
- Advocate for better policies that prioritize student safety without burdening teachers with excessive responsibilities (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges faced by teachers in Texas due to the militarized approach proposed by Ted Cruz. Watching the full video can offer a comprehensive understanding of Beau's perspective and insights.

### Tags

#Teachers #Texas #TedCruz #Education #Militarization #StudentSafety


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
teachers in Texas and Ted Cruz. Because I got to be honest, the teacher asking me
about that first aid kit, that got to me. It really did. And I came home and I was
just going to sit down, watch TV, relax for a little bit. And my phone goes off and of
course it's a news alert about how teachers in Texas are taking issue with
Ted Cruz's approach of hardening the schools and arming the teachers. And
they're saying that they came to teach, not police. Hang on now.
No, they're not asking you to be cops.
We saw that the cops didn't do any good.
They're asking you to be soldiers. So let's put it into that frame for a
second. You're a teacher, you're a drill instructor. If a recruit has an issue,
what would you do? You would send them to a behavioral health specialist, somebody
who has 20 weeks of training. If they're dealing with something a little deeper
of a spiritual nature, perhaps they're grieving, you would send them to
religious affairs, somebody with seven weeks of training. And if this started
causing real problems at home, the military has social workers. This isn't a
weeks of training thing. I want to say to... I think you have to be pursuing your
masters to even start. And that's all just dealing with the aftermath of what
they're asking you to deal with. In the moment, you're not being asked to be
police. This is straight-up combat. And they're asking you to be a soldier,
high-speed, actually. So we're not just talking about infantry school. We're
talking about having to go to RASP as well. Because you're talking about,
you know, a close-quarters run-and-gun battle while trying to evacuate and
secure 30 people by yourself. I mean, that's hard. Just, you know, so you know.
Now for that, they keep changing the lengths of training, but that's about 30
weeks. We'll call it 30 weeks. But then you also, as evidenced by the teacher
asking about the first aid stuff, you also have to be able to treat their
wounds, which is another 16 weeks. This is 73 weeks of training, and this is
something that would be performed by five other people in the military.
That's what he's asking you to do. Not police. He's asking you to be a teacher
on top of five other jobs in the military. And I get it. I understand the
desire to speak out, but at the same time, remember, it's Ted Cruz. I have no
idea how this man keeps getting elected out there, but it is Ted Cruz. And he's
standing there saying, well, we're going to arm the teachers. Basically saying that he's
willing to bet your life on it and encouraging you to stand your ground
against a flurry of bullets. But before you let that get to you, just remember, he
is quite literally a meme for running away from a flurry of snowflakes. Don't
let this man upset you. He is an opportunist. He is somebody who the
Republican Party trots out to make talking points like this because he's in
a very secure red area. I don't think that our teachers need to be first
responders and rangers and everything else that we're asking them to be
because we can't take very simple steps. That seems a little much to be asking.
We're already asking them to literally shield our children with their bodies.
I think we could stop there. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}