---
title: Let's talk about when a bee isn't a bee but a fish....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wMiPyi3026k) |
| Published | 2022/06/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- California ruled bees are fish under the Endangered Species Act due to legal definitions.
- Bees were classified as fish because they fall under the invertebrate category.
- The appeals court's ruling regarding bees as fish under the Endangered Species Act in California is not a new concept.
- Land-dwelling invertebrates have been protected similarly since the 1980s.
- The legislature declined to clear up the confusion regarding bees being classified as fish.
- Bees in California will now receive protection under the state act, especially from intentional harm.
- This ruling provides an interesting backstory with some legal shenanigans involved.
- The decision may serve as a helpful tool to protect bees from harm in the state.
- The ruling adds a touch of humor to the situation, potentially softening the blow for large agriculture companies.
- The protection of bees under this ruling is a positive outcome.

### Quotes

- "bees are fish."
- "bees in California will now be protected again."
- "invertebrate is already listed."
- "It's another little tool that should be pretty helpful."
- "it's just a thought."

### Oneliner

California ruled bees as fish under their Endangered Species Act, providing protection and a touch of humor in an interesting legal twist.

### Audience

California residents, Environmentalists, Wildlife advocates

### On-the-ground actions from transcript

- Support local initiatives to protect bees (implied)
- Take steps to prevent intentional harm to bees and their habitats (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the legal ruling classifying bees as fish under California's Endangered Species Act and the implications of this decision.

### Tags

#California #Bees #EndangeredSpecies #LegalRuling #Protection


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about when a bee isn't a bee.
OK, so go ahead and guess.
When is a bee not a bee?
The answer is when it's a fish, because it's in California.
That's not a joke, and it's not a riddle
that you don't understand.
That's a legal opinion.
OK, so as we all know, bees are in jeopardy.
California has its own Endangered Species Act.
Now, people who were hoping to use this to protect bees
ran into an issue.
And that is that the act protects
native species or subspecies of a bird, mammal, fish,
amphibian, reptile, or plant.
But which one does a bee fall under?
Apparently, it falls under fish, because fish
is defined as a wild fish, mollusk, crustacean,
invertebrate, amphibian, or part spawn or ovum
of any of those animals.
So invertebrate, a bee is in fact an invertebrate.
So the appeals court has ruled that for the purposes
of the Endangered Species Act in California, bees are fish.
Now, this sounds like some brilliant legal shenanigans.
And it is, don't get me wrong.
However, it's not new.
A cursory look, and you can find this opinion
being held going all the way back to the 1980s.
Land-dwelling invertebrates have been
protected in this exact manner.
It has happened so much that the appeals court has actually
reached out to legislators about it and said, hey,
like, why don't y'all clear this up?
And the legislature responded by saying,
invertebrate is already listed.
If we added something, it would just be confusing,
says the lawmakers about the law that
has bees classified as fish.
It would be more confusing.
If they think that, maybe we shouldn't let them touch it.
The good news here is that bees in California
will now be protected again.
So they'll be protected through the state act.
And this really pertains to people intentionally harming
them, destroying stuff.
It's another little tool that should be pretty helpful that
just happens to have a very interesting back story.
But the humor should take a little bit of the sting out
of the loss for the large agriculture companies
that were fighting this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}