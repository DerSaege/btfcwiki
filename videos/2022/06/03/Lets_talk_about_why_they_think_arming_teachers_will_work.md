---
title: Let's talk about why they think arming teachers will work....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FVZ1c9O2L6Y) |
| Published | 2022/06/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Ohio is considering arming teachers with 24 hours of training, which slightly changed Beau's opinion on the matter.
- Beau's previous perception was that those supporting arming teachers didn't understand that most people aren't proficient in firearms.
- Beau associated advocates of arming teachers with being proficient shooters themselves.
- Beau encountered a local deputy known for his de-escalation skills, likened to Andy Griffith.
- The deputy, a former Marine Special Operations member, dismissed the idea of arming teachers as a bad one in a colorful manner.
- Beau questioned the practicality of the proposed solution of teachers standing by doors with students lined up against walls.
- The proposed plan lacks depth and effectiveness, requiring minimal training and no firearms.
- Beau emphasized the unrealistic nature of the proposed scenario in an active shooter situation.
- Beau stressed the importance of law enforcement robbing shooters of the initiative upon entry to prevent further harm.
- The proposed solution of arming teachers merely serves as an alarm and does not address the root issue.

### Quotes

- "This scenario you have constructed is a fantasy that begins after the bad part happened."
- "It reinforces the locked doors. Sure, but it doesn't stop the problem."
- "It doesn't actually stop anything. It just reinforces what's already happening."
- "This isn't a solution."
- "Is it really a net win?"

### Oneliner

Ohio's plan to arm teachers with minimal training doesn't address the root issue of school safety, posing more risks than benefits.

### Audience

Educators, policymakers, concerned citizens

### On-the-ground actions from transcript

- Question the effectiveness and practicality of proposed school safety measures (suggested)
- Advocate for comprehensive solutions addressing the root causes of school violence (implied)

### Whats missing in summary

Detailed analysis of the potential risks and shortcomings of arming teachers and the importance of seeking holistic solutions for school safety.

### Tags

#SchoolSafety #ArmingTeachers #De-escalation #CommunityPolicing #GunViolence


## Transcript
Well, howdy there, Internet people. It's Beau again.
So, today, we're going to do a little update.
I got some more information on a subject,
and it slightly changed my opinion on something.
You know, there's a lot of talk.
In fact, Ohio looks like it is going to arm teachers
with 24 hours worth of training.
Now, those who supported this idea,
I've always written them off as just not understanding
that most people aren't proficient,
that most people don't have that mindset.
And that's always what I thought it was,
because anybody I've ever known who advocated this,
they were shooters.
You know, these are guys in the days when,
you know, they're feeling lucky.
They got the target out at 20 yards,
and they got their kid sitting in a chair at 10 yards,
popping off rounds right over their head and everything.
No, nobody's really doing that, because that's insane.
But these are people that most times
would hit the 9 or 10 ring.
You know, they're shooters.
So, I just always chalked it up to not understanding
that that wasn't everybody.
But this weird thing happened.
There's a local deputy, and he's a pretty well-respected guy.
I don't actually know him,
but I've seen him deal with people.
And he's Andy Griffith.
He is the king of de-escalation.
This is the guy that walks into the bar
in the middle of a fight, and he's like,
Jerry, you better put that pool cue down.
Look at y'all in here fighting over a $20 bet.
Now, all of y'all got to post $200 bond.
Y'all better sit down before y'all get in more trouble.
And this is the kind of cop he is.
Now, he's probably more likely to get away with that,
because word in town is that he was MARSOC
before MARSOC existed.
He was Marine Special Operations.
So, people are probably a lot less likely to test him,
you know?
So, there was a group of guys.
Picture them just like the group from King of the Hill.
And they're at a diner,
and they're talking about this idea.
And he's leaving the diner, and they ask him about it.
And he says it won't work, that it's a bad idea.
And they kind of explain how it would work,
and he...
Well, I'm not going to say exactly what he said.
He's a Marine.
It was a colorful way to say you are incredibly uneducated people.
And he walked off.
And this is a guy that is just known for being super polite.
This hurt their feelings.
So much so that hours later, they were still talking about it,
which is when I hear about it.
So, it got me curious, and I asked him,
how do you see this working?
It's simple.
You put the kids up against the wall,
out on the line of sight of the door,
and the teacher just stands there at the door.
And I wait.
I wait for the rest of it, because there has to be more.
That can't be it.
But nothing else comes.
That's the entirety of what they're talking about.
And I got to be honest, in that situation,
yeah, it doesn't take a whole lot of training.
It really doesn't.
I mean, standing at a doorframe waiting for a contact shot,
yeah, you could teach that in three days.
You really could.
I mean, you don't even need a gun for that.
You could use those long scissors.
You know, the ones that are silver with the black handles.
You don't even need a firearm to pull this off.
That makes sense.
And if you're nodding your head,
and this is what you're imagining,
this is the scenario you have pictured, I have questions.
First, does the teacher still teach?
Or is the teacher posted up by the door all the time?
Why are the kids hid?
Why is the teacher by the door?
More pointedly, what happened in the first classroom?
The one that serves as an alarm for everybody else.
Things don't go that well there, do they?
Because in that classroom, Miss Jones is bent over a desk,
helping your child learn to read when somebody walks in.
And she has milliseconds to pull that weapon,
flip the safety off, get a sight picture,
and fire over the heads of scattering, screaming children.
Miss Jones does not win that engagement
because the shooter has speed, surprise,
and violence of action.
This is why it's so important for cops
to press when they enter.
They have to rob the shooter of the initiative.
Otherwise, they continue about what they're doing.
That's why that's so important.
The reason you get pushback from people
who may have fired inside buildings
in situations like that, because what you are describing there
is just as crazy as having your kid sit
between you and the target.
It doesn't make sense, and you don't win.
This doesn't actually stop anything.
It just reinforces what's already happening.
It reinforces the locked doors.
Sure, but it doesn't stop the problem.
They just now serve as an alarm.
That's not winning.
That's not changing anything.
The scenario you have constructed
is a fantasy that begins after the bad part happened.
It doesn't actually stop it.
This isn't a solution.
So if it doesn't stop the actual problem,
but it introduces more firearms to a school
that could be taken, lost,
accidentally or negligently discharged,
is it really a net win?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}