---
title: Let's talk about teachers, cats, and gas stations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t5JzV_yb1fQ) |
| Published | 2022/06/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Enters a gas station and is recognized by the cashier.
- Approached by a woman asking about an "iffic," which turns out to be an IFAK (Individual First Aid Kit).
- Woman inquires if the kit is suitable for treating a gunshot wound and if it works on children.
- Beau advises on the contents of the kit and the suitability for children, particularly regarding tourniquets.
- Recommends additional supplies like gauze, chest seals, and tourniquets for treating children.
- Conducts research on the efficacy of CAT tourniquets on pediatric cases.
- Confirms that CAT generation 7 tourniquets have been successful in pediatric cases without issues.
- Describes CAT tourniquets and recommends them as a valuable addition to first aid kits.
- Suggests teachers prepare first aid kits similar to those used by soldiers for classroom emergencies.

### Quotes

- "You're gonna need more gauze and chest seals and tourniquets."
- "If you are a teacher preparing a kit for the battlefield of your classroom..."
- "Y'all have a good day."

### Oneliner

Beau walks into a gas station, encounters a woman asking about first aid kits, and educates about pediatric first aid needs and CAT tourniquets.

### Audience

Teachers, First Aid Responders

### On-the-ground actions from transcript

- Assemble a first aid kit similar to those used by soldiers for emergencies in your classroom (suggested).
- Ensure your first aid kit includes adequate supplies like gauze, chest seals, and CAT generation 7 tourniquets (implied).

### Whats missing in summary

The importance of being prepared with the right first aid supplies, especially when dealing with pediatric cases.

### Tags

#FirstAid #EmergencyPreparedness #PediatricCare #Teachers #CommunitySafety


## Transcript
Well, howdy there internet people. It's Bo again. So today we are going to uh we're talking about a
conversation that took place in the gas station and uh the conversation itself is very illuminating
like as soon as you realize where it's headed I mean it's there but there's also important
information. If you're a teacher there's definitely important information for you.
So I walk in the gas station and as soon as I open the door the cashier she knows me. I've
talked about her before on the channel and she just yells out oh hey he'll know. I'm like oh
great you know I figure I'm gonna have to give directions or something. So I walk over to her
and this woman who I have seen I don't know but I've seen her around walks up and uh she's like
do you know what an iffic is? I'm sorry a what? An iffic. She holds up her phone and she's showing
me an ifac. Yeah I know what an ifac is and she's like is this a good one? I look at it and I'm like
yeah I mean that's that's a pretty good base. I mean yeah that's that's all right and she's like
a base. I'm like yeah you end up customizing you end up adding stuff you know to according to your
preferences a lot of times. Does this have everything you would need to treat a gunshot wound?
Yeah yeah sitting there thinking weirdo yeah yeah it does because I don't see the obvious
turn that this conversation is about to take. Will this stuff work on children?
Like a punch to the gut. You're a teacher? Yeah. And I sit there and I think for a second I'm like
yeah all of this should work on on kids. I'm not sure about the cat about about the tourniquet
this black thing right here. And I was like you know those were designed for adults. I don't know
that they wouldn't but I don't know that they would so that would I would research it out a
little bit more. And you know she's like okay thank you and as she's starting to walk off I realize
something and I call back I'm like you're gonna need more gauze and chest seals and tourniquets.
The I in most acronyms stands for individual. You're gonna need a lot more of this.
And you tell her face just defeated if you don't know this stuff is not cheap.
So I didn't know whether or not the the tourniquets would work on kids. So I looked it up.
Anecdotally it looks like all cat tourniquets will. Okay but you're talking about this anecdotally
really isn't good enough for me. So I did find a report that was specific to cat generation 7
tourniquets. They were used in pediatric cases a bunch I can't remember the exact number but
enough for me to be like yeah they'll work. And the report says that they didn't have any issues
that they absolutely functioned. If you don't know what a cat is this is basically it's a band
that goes around the arm and it has this little plastic wind-up thing that tightens it to restrict
flow. They are all the rage right now. If you don't have one you probably should.
So if you are a teacher preparing a kit for the battlefield of your classroom you want
generation 7 tourniquets to go along with your your kit that is based off of what soldiers carry in war.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}