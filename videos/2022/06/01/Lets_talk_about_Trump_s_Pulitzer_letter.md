---
title: Let's talk about Trump's Pulitzer letter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SrTtXmStzPE) |
| Published | 2022/06/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump sent a letter to the Pulitzer Board demanding they rescind the prize awarded to the Washington Post and the New York Times in 2018 for their coverage of possible Russian collusion.
- Trump's letter claims that the award was based on false and fabricated information published by the media outlets.
- The letter warns of potential litigation if the Board does not take action, akin to finding votes.
- Trump suggests the Board keep an eye on the ongoing criminal trial of Michael Sussman, a former attorney for the Clinton campaign.
- Sussman was acquitted, contrary to what Trump believed the Durham investigation was going to produce.
- Trump's actions are likened to Grandpa Simpson, writing letters to entities like the Pulitzer Board.
- The Durham investigation was a significant hope for Trump supporters to deflect from the Mueller investigation.
- Trump's aim was to discredit the origins of the investigations by claiming Sussman lied, but the jury found otherwise.
- Falling for his own propaganda, Trump's letter illustrates the danger of believing inaccurate stories.
- Being a Trump supporter amidst such revelations can be challenging.

### Quotes

- "The continuing publication and recognition of the prizes on the Board's website is a distortion of fact and a personal defamation."
- "Trump really is at this point, he is turning into Grandpa Simpson."
- "It can't be easy to be a Trump supporter right now."

### Oneliner

Former President Trump demands Pulitzer Board rescind awards based on false information, revealing dangers of believing propaganda.

### Audience

Political observers

### On-the-ground actions from transcript

- Monitor ongoing criminal trials (suggested)
- Stay informed about political developments (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis and commentary on former President Trump's actions and their implications for his supporters.

### Tags

#FormerPresidentTrump #Propaganda #PulitzerBoard #PoliticalAnalysis #BelievingFalseInformation


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about former President Trump's request to the Pulitzer
Board.
The former president sent a letter to the organization that gives out the Pulitzer Prize
demanding that they rescind the prize they awarded to the Washington Post and the New
York Times in 2018 for their coverage of possible Russian collusion.
The letter says, there is no dispute that the Pulitzer Board's award to those media
outlets was based on false and fabricated information that they published.
The continuing publication and recognition of the prizes on the Board's website is a
distortion of fact and a personal defamation that will result in the filing of litigation
if the Board cannot be persuaded to do the right thing on its own, like find the votes.
But it does go on to suggest and instruct the Board to pay close attention to the developments
in the ongoing criminal trial of Michael Sussman.
Now I'm not a Pulitzer Prize winner or anything, but I know he got acquitted.
Sussman was the former attorney for the Clinton campaign back in 2016.
And Trump has apparently fallen for the bad habit of believing his own propaganda.
The Durham investigation did not produce what he said it was going to for years, and their
first criminal prosecution resulted in the key figure being acquitted.
Trump really is at this point, he is turning into Grandpa Simpson.
Writing letters to the Pulitzer Board is, I mean that's interesting.
I can't wait until he writes to Baskin Robbins to suggest that 31 flavors is too many and
too confusing and they need to remove a few.
This is a big upset for the Trump crowd.
They were hanging a lot of hopes on the Durham investigation.
They were hanging a lot of hopes on being able to deflect from the Mueller investigation
with this.
Because all of this stuff is going to come back up in any future campaigns.
So this was their moment to be able to say, look, that never even should have happened
because they lied.
You know, this attorney lied and that's what started all of this moving.
And the jury found that he did not lie.
Now I have to assume that this letter was sent prior to this assessment's acquittal.
I would hope anyway.
And this is the danger of falling for your own propaganda.
Of believing these stories that are put out to the public to sway their opinion.
Especially if you might know that they're less than accurate.
It can't be easy to be a Trump supporter right now.
That's all I have to say.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}