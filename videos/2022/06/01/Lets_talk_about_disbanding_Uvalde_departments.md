---
title: Let's talk about disbanding Uvalde departments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2h3BSsymV6M) |
| Published | 2022/06/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Two departments in Texas, the regular police department and the police department for the school, have decided to stop cooperating with the Texas Department of Public Safety's investigation.
- The decision to stop cooperating came after the director of DPS criticized the delayed police entry into a classroom during a news conference.
- Beau asserts that the delayed police entry was clearly the wrong decision and contrary to protocol.
- He calls for the immediate disbandment of these departments to prevent creating unaccountable entities.
- Beau believes that allowing these departments to stonewall an investigation makes them believe they are above the law.
- He suggests that even if the departments start cooperating, disbandment is the only option for a positive outcome.
- Beau recommends that the Texas Department of Public Safety should treat the situation as a criminal investigation.
- He questions the accuracy of information being put out and suggests a probe within law enforcement may not be the right approach.
- Beau insists that the decisions made during the incident were clearly wrong, and there is no justification for believing otherwise.
- He warns that without action from the community, they will be under the control of an unaccountable department that doesn't believe it owes the public anything.

### Quotes

- "If this flies, if they are able to stonewall in an investigation in a situation like this, they are above the law."
- "There is no way you can look at what happened and think the right decisions were made."
- "If they don't believe they owe the public answers after what happened, they don't believe they owe the public anything."

### Oneliner

Two Texas departments have stopped cooperating with an investigation, prompting calls for immediate disbandment to prevent unaccountability and disregard for public safety.

### Audience

Community members, advocates

### On-the-ground actions from transcript

- Disband the departments immediately (implied)
- Advocate for treating the situation as a criminal investigation (implied)

### Whats missing in summary

The emotional urgency and call to action conveyed by Beau in the full transcript.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about that department in Texas.
There's two departments in Texas.
The police department and then the police department for the school,
because they have decided apparently to stop cooperating
with the Texas Department of Public Safety investigation into what happened.
According to ABC sources, the decision to stop cooperating occurred soon after
the director of DPS, Colonel Steve McCraw, held a news conference Friday
during which he said the delayed police entry into the classroom
was the wrong decision and contrary to protocol.
Newsflash. It was the wrong decision and contrary to protocol.
With the outcome it had, how could it possibly have been the right decision?
That isn't a controversial statement.
The people of this community have one option and one option only right now,
and that is to disband these departments immediately.
If you don't, you are creating a department that is unaccountable.
They clearly believe that they are.
If this flies, if they are able to stonewall in an investigation
in a situation like this, they are above the law.
The only option that will have a positive outcome is to disband this department,
even if they start cooperating tomorrow.
As far as the Texas Department of Public Safety,
they should probably start proceeding with this as if it's a criminal investigation.
That seems like the logical choice at this point.
There seems to be an effort to put out information that is less than accurate.
I would suggest that maybe there is something that we don't know,
and a friendly probe within the thin blue line may not be the way to go about this.
It probably needs to shift.
This is an extreme reaction to a very, very simple statement.
There is no way you can look at what happened and think the right decisions were made.
Now, whether or not you think that those decisions made in the heat of the moment are excusable,
well, that's up for discussion.
But believing they were the right ones, that it wasn't the wrong decision,
I don't see how anybody can believe that, given the outcome.
If the people of this community don't act, they will be under the thumb of a department
that is accountable to nobody.
If they don't believe that they owe the public answers after what happened,
they don't believe they owe the public anything.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}