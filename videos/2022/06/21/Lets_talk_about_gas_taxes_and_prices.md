---
title: Let's talk about gas taxes and prices....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zyn8wqqA_SU) |
| Published | 2022/06/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the federal tax on gas and potential holiday proposed by the Biden administration.
- Points out that states could have provided relief on gas prices themselves.
- Calculates the potential savings from the proposed federal tax holiday, estimating around $0.15 to $0.20 per gallon for most people.
- Contrasts U.S. gas prices with prices around the world, showcasing higher prices in countries like Belgium, Denmark, and Hong Kong.
- Challenges the American perspective that everything is tied to the president, suggesting a need for a transition away from gas usage.
- Emphasizes the importance of transitioning to alternative energy sources and reducing reliance on gas in the long term.
- Acknowledges that the federal gas tax holiday could make a difference for some individuals but may not be a significant game-changer overall.
- Notes that despite criticisms, the U.S. gas prices are relatively lower compared to prices in many other countries.

### Quotes

- "One of the things that is just such an American way of looking at the world is that pretty much everything has to do with the president."
- "The right move is to transition."
- "This seems like an opportune time to do it, to make as many leaps in that direction as we can."

### Oneliner

Beau breaks down the federal gas tax, challenges American perspectives, and advocates for transitioning to alternative energy sources amidst global price comparisons.

### Audience

American citizens

### On-the-ground actions from transcript

- Advocate for sustainable energy transitions (implied)
- Research and support local initiatives promoting alternative energy sources (implied)

### Whats missing in summary

Detailed breakdown of gas prices around the world and the need for a shift towards sustainable energy solutions.

### Tags

#GasPrices #FederalTax #EnergyTransition #GlobalComparison #Sustainability


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about gas prices
and the federal tax on gas prices
and what gasoline really costs and so on and so forth.
We're going to do this
because the Biden administration is considering
a holiday of sorts.
When it comes to the federal tax on gas,
every gallon of gas that you buy,
there's a tax that goes to the feds.
I want to point out that this is also true of the states.
We've talked about it on the channel before.
Every governor that has been running around,
screaming about the gas prices, could have already
provided a little bit of relief on their own
without the federal government's involvement.
They just chose not to.
Not about solving the problem, about pointing fingers.
OK, so there are a lot of people who
are suggesting that this is going
to make a big, big difference in the cost
that people are actually going to end up paying.
So we'll run the numbers.
For most people, this will save somewhere between $0.15
and $0.20 per gallon.
that going to be a lot? If you look at the numbers, the average American uses
between 562 and 656 gallons of gas per year. Do the math, that means you'll end
up paying, I don't know, nine to eleven dollars less per month. Not exactly the
huge break that people kind of are insinuating this is going to be. Yeah, for
some people, especially if you are like pizza delivery driver or something like
that, yeah this is gonna matter. But for most people it isn't going to save you a
whole lot of money. Since we're talking about clearing up misconceptions when it
comes to gas, I do want to point something else out. I have the numbers
here for gas prices around the world. They are from May 30th. At that time, the average
in the US was $4.79. Where do we stack up around the world?
Australia. These numbers are in US dollars per gallon. Australia, $5.45. Belgium, $8.10.
Brazil, $5.78.
Canada, $6.49.
China, $5.50.
Denmark, $10.02.
Getting the picture.
And this is pretty consistent.
Germany, $8.80.
Hong Kong, all the way up to $11.21.
Ireland, $7.91.
It's very, very consistent.
pays less? The countries you would imagine pay less. Kuwait $1.30, Saudi
Arabia $2.35. The reality is this is a global thing. One of the things that is
just such an American way of looking at the world is that pretty much everything
has to do with the president. That's not the case. Now some things can be done to
mitigate like this, but the reality is we're using a lot of gas and we're using
something that over the long term is destroying us. It's going to get more
expensive as time goes on. The right move is to transition. That's what we
need to do. This seems like an opportune time to do it, to make as many leaps in
that direction as we can. The holiday on a federal gas tax, it's going to help
some people and for some people it may be the very difference between making it
not making it. I would point out again that the state gas taxes are normally
higher. So I'm not saying that this isn't something that should be done. I'm just
saying that with a lot of the fanfare it's getting, this isn't going to be the
game-changer a lot of people are picturing. But something else that's
important is that when it comes to the price of gas around the world, we're
actually doing pretty well. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}