---
title: Let's talk about that ad in Missouri and a conflation of terms....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4v_ap9tH5-E) |
| Published | 2022/06/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analysis of a controversial political ad for a Missouri Senate candidate involving military imagery and a shotgun.
- Republican candidate was reported to law enforcement for the threatening nature of the ad.
- The ad mentions a "rhino hunting permit" with no tagging or bagging limit, which are hunting terms related to animals, not people.
- Beau's observation at a burger joint reveals the intentional conflation of hunting terms with people, suggesting dangerous rhetoric.
- Alluding to historical radicalization on the right targeting less extreme individuals within their own group.
- The use of such imagery and rhetoric could have dangerous implications beyond just political discourse.

### Quotes

- "Get your rhino hunting permit."
- "Tagging and bagging? Nah, that has to do with people."
- "It's pretty dangerous rhetoric. It's pretty inflammatory."
- "Those people who hear what I heard and what the guys at the burger joint heard, they know what they're being told."
- "There's going to be a lot of questions about what was meant."

### Oneliner

Beau examines a controversial political ad for a Missouri Senate candidate, revealing dangerous rhetoric and intentional conflation of hunting terms with people.

### Audience

Voters, activists, analysts

### On-the-ground actions from transcript

- Contact local media to raise awareness about the dangerous implications of using inflammatory rhetoric (implied)
- Engage in community dialogues about the impact of political messaging on social cohesion and safety (implied)

### Whats missing in summary

The full transcript provides additional context on the potential impact of inflammatory political rhetoric on public safety and social divisions.

### Tags

#Missouri #SenateCandidate #PoliticalAd #InflammatoryRhetoric #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about that ad, that ad for
the candidate from Missouri for Senate.
I would imagine you've already seen it or at
least heard about it.
If you haven't, it opens up with a whole bunch of people
standing in front of this house.
And they're all kitted out, military garb.
Boom, the door gets sent in, distraction device gets thrown in, they move in in a slow and
deliberate clear, and then in walks a candidate holding a shotgun saying, get your rhino hunting
permit.
Man, that is out there.
That is something else.
Imagery was really clear, and it's obviously an inopportune time to use that imagery and
that kind of analogy. A lot of people took it to be very threatening. So much
so that the Republican candidate for Senate from Missouri had the cops
called on him by the Senate leader for the State Senate in Missouri, contacted
law enforcement about the ad. And some people may see that as an overreaction.
I'm not sure that it is. The Senate leader there in the state may have heard
the same thing I did because there's a conflation of terms and it doesn't seem
like a big deal at first until you really start to think about it. In the
add, the candidate says that the rhino hunting permit has no tagging and no
bagging limit. A bag limit and a tag limit, yeah, those are hunting terms. They
are. They relate to the number of animals you can take during a specified period
time. Tagging and bagging? Nah, that has to do with people. That has to do with
people. And at first I thought maybe I'm just being overly sensitive on this one.
Maybe it caught my ear and now I can't let it go. So I rolled up to the local
burger joint and there's a bunch of guys standing outside as there always is. Hey
what's the bagging limit this year? Everybody gets real quiet, kind of looks
at me like I've lost my mind. You mean the bag limit? Yeah, yeah. What's the
tagging and bagging limit? Tagging and bagging. You going after people? It's one
of those things that might be missed by most people. Those who he's speaking to,
they heard it. It's a dog whistle. And you could say that maybe it was just, you
know, a misunderstanding. Maybe that's true. Maybe this candidate from Missouri
doesn't hunt and nobody involved with making this video hunts and doesn't know
terminology. The alternative is that it was an intentional conflating of those
two terms. We've talked on the channel before about how historically when the
right becomes radicalized, a lot of times they go after people who are
theoretically on their side, just not as radical. That seems to be what's
happening here. And while for the moment it is political rhetoric, it's pretty
dangerous rhetoric. It's pretty inflammatory. And those
people who hear what I heard and what the guys at the burger joint heard, they
know what they're being told.
This will probably get a lot of press coverage, which may lead to more name recognition for
the candidate, which is why I haven't said the candidate's name, by the way.
I wouldn't expect this story to go away immediately, because I think there's going to be a lot
of questions about what was meant.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}