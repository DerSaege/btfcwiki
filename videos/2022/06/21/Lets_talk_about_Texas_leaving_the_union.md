---
title: Let's talk about Texas leaving the union....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qYVzl3NIBb8) |
| Published | 2022/06/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in Texas are considering a party platform that includes the right to secede from the United States.
- Beau supports the right to self-determination and believes in compensating citizens who do not want to leave.
- The federal government has made it clear through historical events like the Civil War and the Texas v. White case that there is no legal right for a state to secede from the Union.
- The only ways for a state to leave the Union are through revolution or with the consent of other states.
- Beau mentions that the idea of Texas leaving the U.S. may prevent the Republican Party from ever holding power nationally again.
- He dismisses the idea of a revolution due to the impracticality of winning an armed conflict against the U.S. military.
- Beau views the Republican Party's stance on secession as dishonest posturing to appeal to their base rather than a genuine movement.
- He questions why a movement claiming to put "America First" is advocating for actions that could tear America apart.

### Quotes

- "It is the Republican Party posturing, putting out a political talking point that they believe their voters will buy."
- "Why is it that the America First movement keeps trying to rip America apart?"

### Oneliner

Republicans in Texas entertain secession, but historical and practical realities make it a questionable endeavor, viewed by Beau as dishonest posturing.

### Audience

Texans, Political Activists

### On-the-ground actions from transcript

- Research and understand the historical context and legal implications of secession for better-informed political engagement (suggested)
- Engage in open dialogues with fellow Texans and political allies to debunk misinformation and political posturing (implied)

### Whats missing in summary

The full transcript provides additional context on the historical precedent of states' rights and the practical challenges of secession, offering a comprehensive view on the proposed Republican platform in Texas.

### Tags

#Texas #Secession #RepublicanParty #PoliticalPosturing #StateRights


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Republicans in Texas again.
One of the proposed party platforms for the Republican Party out there is, well, it's
reserving the right to leave the United States, and they want a referendum to determine whether
or not they should secede.
Okay, so we'll go through how I personally feel about this, and then we'll talk about
the real mechanisms of it and then kind of go from there.
Me personally, I'm actually a huge supporter of the right to self-determination.
You can get a super majority of a state to say, yeah, we want to leave.
You work out a mechanism to compensate those U.S. citizens who do not want to leave.
Compensate them for their time and their property and everything else because you can't forcibly
expatriate them, get them on their way, so on and so forth.
I mean, me personally, I don't have a problem with it.
Y'all have fun with that.
It doesn't bother me at all.
But that's me.
Yeah I'm not the federal government.
They might have a slightly different opinion.
They do have a slightly different opinion, and they've told Texas this twice, actually.
There was that whole Civil War thing that went on, and then there was the case of Texas
versus White in 1869, which flat out says there is no right to leave the Union.
Once a state joins, it's there forever.
The only two ways out are revolution or consent from the other states, from the U.S.
That's how you leave.
Now as far as consent from the rest of the country, I mean, you might be able to get
it, especially because if Texas was to leave, I mean, I don't think the Republican Party
would ever actually hold power in the U.S. again.
It'd make it really hard.
So you probably have a whole lot of people supporting it for that reason.
And there's probably a whole lot of people who are just tired of the politicians in Texas.
So you might have a shot of it that way.
If not, I mean, that's pretty much it.
The revolution route, the actual idea of winning an armed conflict, that's just a no-go.
That's not going to happen.
I know in the myth of the Republic of Texas or whatever, the way people look at it is,
we got all these military bases here.
No, the Department of Defense of the United States has all of those military bases there.
It's like a de facto occupation already.
Those military bases, while they exist in Texas, they are staffed by people who PCS'd
from like New Jersey.
You're not fighting in your revolution, dude.
So that really seems off the table.
This is a joke, to be honest.
It is the Republican Party posturing, putting out a political talking point that they believe
their voters will buy.
They believe that their voters aren't smart enough to Google it, to find out whether or
not this is even a thing.
That's what it is.
It's just one more dishonesty aimed at their own base that isn't grounded in reality.
And that's coming from somebody who's like, yeah, pack your stuff, I don't care, go.
It's just more posturing.
It's more of an attempt to wrap up their super establishment right wing rhetoric in some
kind of revolutionary cloth.
And that's not what they are.
It's incredibly dishonest.
And then I do have one more question.
Why is it that the America First movement keeps trying to rip America apart?
I mean that seems counterintuitive.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}