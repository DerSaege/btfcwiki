---
title: Let's talk about gas prices this summer....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lXBMBPyX1VQ) |
| Published | 2022/06/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Inflation is expected to continue throughout the summer, with economists predicting oil prices to rise to $140 a barrel.
- Current prices are based on around $120 a barrel, leading to a ripple effect on fuel and everything else.
- The rising fuel prices may result in demand destruction, causing people to make decisions like not driving or vacationing.
- Factors like food insecurity, difficulties in obtaining grain, the California drought affecting produce prices, all point towards prices continuing to climb.
- It's advised to hold off on splurging and keep some reserve due to the economic outlook.
- While the overall economy seems stable, the inflation trend is concerning.
- Hope is placed on oil prices declining at the end of summer to balance things out, but there are no guarantees.
- The rise in prices is partly attributed to companies aiming for higher profits and pushing prices as far as the market allows.
- The economic situation favors Wall Street with higher prices translating to increased profits, but it's detrimental to the average person.
- This situation underscores the disparity between what benefits Wall Street and what benefits Main Street.

### Quotes

- "It's better to have this information early."
- "Now might not be the time to splurge."
- "This is not good economic news for the average person."
- "Higher prices mean a higher share is going to be profit."
- "It's another thing that highlights the disparity between what's good for Wall Street and what's good for Main Street."

### Oneliner

Inflation is on the rise, with fuel prices expected to climb, impacting everything else, creating economic challenges for the average person while benefiting Wall Street.

### Audience

Economic consumers

### On-the-ground actions from transcript

- Keep a reserve for potential price hikes (suggested)
- Stay informed about economic trends and plan spending accordingly (implied)

### Whats missing in summary

Insights on specific strategies or resources to navigate the economic challenges discussed.

### Tags

#Inflation #Economy #PriceRise #ConsumerAlert #WallStreetvsMainStreet


## Transcript
Well, howdy there internet people. It's Beau again.
So today we are going to talk about inflation
and how it looks like it's gonna continue
throughout the summer.
Economists are suggesting that oil will continue to rise
and that it will hit $140 a barrel.
The prices you're seeing right now
are based on around $120 a barrel.
So the price of fuel will continue to go up,
which means the price of everything else
will continue to go up.
Not exactly good news for anybody.
There is the expectation that over the summer
it will get so high, fuel prices will get so high,
that it will actually end up resulting
in demand destruction, meaning people will make the decision
to not drive, not vacation, stuff like that,
based on this.
Given everything that's going on right now
when it comes to food insecurity
and the issues that are coming with trying to get grain,
fuel prices, the drought in California,
which is going to impact produce prices,
and everything else, it's safe to assume
that throughout the summer prices will continue to climb.
It's not good news,
but it's better to have this information early.
So if you're planning on splurging on anything,
now might not be the time to do it.
It might be a good idea to keep a little bit in reserve
if you have that ability.
I know a lot of people don't.
Other than the inflation aspect,
the economy seems to be doing okay.
So what we can hope for is that at the end of summer,
when oil prices start to decline,
that everything can start to even out then.
But there's no guarantee of that.
What we know right now is that it looks pretty certain
that we are in for another couple months
of steadily increasing prices,
some of which is caused by stuff like this.
Some of it is caused by companies demanding
to make more than they made last year
and continuing to, I don't want to say price gouge,
but continuing to get as much as the market will bear.
So this is not good economic news for the average person.
Now this is going to make Wall Street real happy
because higher prices means a higher share is going to be profit.
For normal people, this is really bad news.
It's another thing that highlights the disparity
when it comes to the coverage
and the difference between what's good for Wall Street
and what's good for Main Street.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}