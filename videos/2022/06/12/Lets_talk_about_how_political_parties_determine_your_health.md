---
title: Let's talk about how political parties determine your health....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rYVSV0SywJk) |
| Published | 2022/06/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New research from Brigham and Women's Hospital reveals differing health outcomes based on political affiliation in the U.S.
- The study analyzed data from 2001 to 2019, covering 3,000 counties across all 50 states.
- Living in a county that voted Democratic was associated with a 22% decrease in mortality rate, while Republican counties saw an 11% decrease.
- This mortality gap started to widen around 2010 with the implementation of the Affordable Care Act, as Democratic states expanded Medicaid, offering health insurance to low-income individuals.
- Factors such as vaccination, mask-wearing, and social distancing were not considered in this study, focusing solely on normal policy and lifestyle choices.
- People affiliated with the Democratic Party tend to be more health-conscious, leading healthier lives and having better access to insurance.
- Republican counties had 73 more COVID-19 deaths per 100,000 people during the public health issue, indicating a significant impact of political policies on health outcomes.
- Republican policies and attitudes like fake masculinity and ignoring health advice contribute to faster mortality rates.
- The data collected over nearly 20 years consistently shows that Democratic policies and lifestyle choices lead to longer life expectancy.
- Bringing up these findings may be valuable when discussing health choices with those who disregard advice.

### Quotes

- "Democratic Party policies and that lifestyle make you live longer."
- "Republican policies, fake masculinity, being a tough guy, all of that stuff, it's literally killing you."

### Oneliner

New research shows that political affiliation in the U.S. significantly impacts health outcomes, with Democratic counties experiencing a 22% decrease in mortality rate compared to 11% in Republican counties, revealing the life-extending impact of policy and lifestyle choices.

### Audience

Health advocates, policymakers

### On-the-ground actions from transcript

- Share the research findings on how political affiliation affects health outcomes (implied)
- Encourage open dialogues and education on the importance of health-conscious decisions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how political affiliation influences health outcomes, urging for a non-partisan approach towards health policies and choices.

### Tags

#Health #PoliticalAffiliation #MortalityGap #PolicyImpact #DemocraticParty #RepublicanParty


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about some new research out
of Brigham and Women's Hospital and how
it highlights differing health outcomes based
on political affiliation.
So the study analyzed information from 2001 to 2019.
It stopped before our giant public health issue
that was very partisan.
It analyzed 3,000 US counties in all 50 states.
And it did, in fact, discover what
they're calling a mortality gap.
During this period, if you lived in a county that
voted for the Democratic Party, your mortality rate
dropped by 22%.
If you lived in a county that voted for the Republican Party,
it dropped by 11%.
That's statistically significant.
So in the beginning, in Democratic counties,
it was 850 per 100,000.
By the end, it had dropped to 664.
For Republican counties, it started at 867,
and it ended at 771.
This is a pretty marked difference.
It is a big difference.
There's no other way to say that.
What the researchers found is that there was definitely
an increase in the widening of this gap around 2010,
when the Affordable Care Act, when that started,
because states that were Democratic,
they expanded Medicaid.
So they offered health insurance to low-income people.
Republican states didn't really do that.
And that was a big source of it.
Now, all of this is pre-public health issue, right?
So the vaccination, mask wearing, social distancing,
all of that stuff, that doesn't even factor into this yet.
That's not included in this.
This is just normal policy and life choices.
Generally speaking, the people who
make up the Democratic Party, they
tend to be more responsive to news about, well, health.
So they tend to lead healthier lives.
They have more access to insurance.
These are things that have been politicized,
but they really shouldn't be.
There shouldn't be a partisan divide here,
not one this significant.
Now, during our public health issue,
it's worth noting that Republican counties had
73 more deaths from COVID per 100,000 people.
So when this study is duplicated,
there will be another giant shift.
The gap will widen even more just based on that.
The Republican talking points, the Republican policies,
the fake masculinity, being a tough guy, all of that stuff,
it's literally killing you.
You are dying faster because of it.
This is a lot of data.
This is almost 20 years of data, a very wide set.
And it's incredibly consistent information.
Democratic Party policies and that lifestyle,
those choices, makes you live longer.
Might be something that is worth bringing up
the next time you're around people who like to bask
in just ignoring health advice.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}