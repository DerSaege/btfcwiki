---
title: Let's talk about Biden, canyons, water bottles, and a plan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ov98megg4QY) |
| Published | 2022/06/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration's focus on the Hudson Canyon Marine Sanctuary and eliminating single-use plastics in national parks is getting media attention.
- The Sanctuary, the largest underwater canyon in the US Atlantic, is intended to protect biodiversity and fulfill Biden's campaign promise of protecting 30% of US territory.
- The plan includes public comment periods to define protected areas and establish rules.
- The initiative to phase out single-use plastics in national parks, including water bottles, by 2032 is part of Biden's climate change strategy.
- The bigger story, overshadowed by media focus, is the Ocean Climate Action Plan aiming to address ocean and climate issues comprehensively.
- The Plan involves interagency collaboration to develop strategies for resilience, conservation, adaptation, and mitigation, such as green shipping and renewable energy.
- Beau stresses the importance of creating a detailed plan before taking action and empowering experts to produce results.

### Quotes

- "We need a plan first."
- "If you put a group of experts together and you empower them, they'll produce results."
- "That to me is definitely a bigger story than the water bottles."

### Oneliner

The Biden administration's environmental initiatives, including the Hudson Canyon Marine Sanctuary and phasing out single-use plastics, are overshadowed by the comprehensive Ocean Climate Action Plan, focusing on expert-led strategies for resilience and conservation.

### Audience

Environmental advocates

### On-the-ground actions from transcript

- Support the public comment period for defining the protected areas of the Hudson Canyon Marine Sanctuary (implied)
- Stay informed about the Ocean Climate Action Plan and advocate for comprehensive strategies for resilience and conservation (implied)

### Whats missing in summary

Details on specific actions individuals can take to support the Ocean Climate Action Plan and advocate for comprehensive environmental strategies.

### Tags

#BidenAdministration #Environment #OceanClimateActionPlan #SingleUsePlastics #Conservation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about canyons
and water bottles and the thing that seems to me
to be way more important than what the media is focusing on.
If you don't know, the Biden administration
put out this thing and it was like,
hey, this is what we're gonna do
to help out with the environment.
And it had a list of stuff.
The media is focusing mainly on the Hudson Canyon Marine
Sanctuary and the water bottles.
We're gonna talk about those two things,
but we're also going to cover something
that seems to be way more important.
Okay, so the Hudson Canyon Marine Sanctuary, what is it?
Well, the Hudson Canyon is the largest underwater canyon
in the US portion of the Atlantic.
Where is it?
Like a hundred miles from the mouth of the Hudson River.
Making it a marine sanctuary allows the federal government
to have a little bit more say in what goes on there,
protect biodiversity.
There's a lot of ocean life that exists there.
Wells, turtles, cold water, coral, all of that stuff.
All right.
Now this, if you're being cynical, you could say,
hey, he's trying to fulfill that campaign promise, right?
Because when he was campaigning,
he said that he was going to protect 30%
of the land and ocean that was US territory.
So this is a step towards doing that.
It's part of his climate change strategy, right?
Okay, so that's what that is.
That's probably going to happen.
There's, I want to say there's a public comment period
that has already started.
If not, it will be starting soon.
And that will help define the exact geographic area
that is going to be protected
and establish some basic rules.
Okay.
Okay, the other thing the media is focusing on
is getting rid of single use plastics in national parks.
I know there's a whole bunch of people right now going,
didn't that already?
Yeah, Obama did that.
Trump, for whatever reason, was like,
no, we're going to do it again.
We're going to have single use plastics
and got rid of that process.
The Biden administration is bringing it back.
So single use plastics should be phased out
of national parks by, I want to say 2032
under the current plan.
And this includes plastic water bottles,
you know, single serve water bottles, stuff like that.
It's not going to be sold there.
It's not going to have anything to do with
the national parks operations.
So this is what the media is focusing on.
In the same release, the Biden administration
kind of down near the bottom,
they're like, oh, by the way,
we're creating a whole of government plan
to deal with the ocean and climate change
called the Ocean Climate Action Plan.
It's an interagency thing
that is supposed to help develop a strategy
for resilience, conservation, adaptation, mitigation,
all of this stuff.
Basically putting together a team of experts
to come up with a strategy to develop a workable plan.
This would include everything from green shipping
to renewable energy out of the ocean
to managing the coastlines.
I mean, that seems like the bigger story to me.
Generally speaking, if you put a group of experts together
and you give them a plan or you give them a mission
and you empower them, they'll produce results.
And that seems like what they're trying to do.
That to me is definitely a bigger story
than the water bottles.
So hopefully the media will catch onto that
and that they'll start covering that a little bit more
and there will be some more positive public support
for that because, I mean, yeah, we need a plan.
And we need action, but we have to have a plan first.
These are the people who can put together a plan,
coordinate with the different government agencies
and make something happen.
So I would be on the lookout for that story
to kind of catch up.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}