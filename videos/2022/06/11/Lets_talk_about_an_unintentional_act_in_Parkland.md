---
title: Let's talk about an unintentional act in Parkland....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VMO4Uk7vlWI) |
| Published | 2022/06/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- School resource officer finds guns unattended in a school locker room, belonging to the principal.
- The guns were unintentionally brought in, left unsecured in the locker room.
- The incident sheds light on the dangers of arming teachers.
- If guns are allowed in schools, incidents like this can become frequent and dangerous.
- Parents were not notified until 24 hours later.
- This incident serves as a warning against arming teachers and bringing weapons into schools.
- Allowing firearms in schools poses serious risks.
- Incidents of mishandling firearms in schools are not isolated.
- People don't understand how to properly secure firearms.
- Pushing the responsibility of handling weapons onto teachers is a bad idea.

### Quotes

- "Pushing this on them is a bad idea."
- "Allowing these weapons into the schools is a bad idea."
- "This will go bad."
- "It's just a thought."
- "Y'all have a good day."

### Oneliner

School resource officer finds guns unattended in school, unintentionally brought in, warning against arming teachers and allowing weapons in schools.

### Audience

Educators, Parents, Advocates

### On-the-ground actions from transcript

- Educate others on the dangers of arming teachers and bringing weapons into schools (implied).
- Advocate for proper firearm safety education in schools (implied).

### Whats missing in summary

The full transcript provides more context on the specific incident and elaborates on the risks associated with arming teachers and bringing guns into school environments.

### Tags

#SchoolSafety #ArmingTeachers #GunControl #CommunityAdvocacy


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today, we're going to talk about something
that was unintentional,
but it sheds a whole lot of light
on something that may be happening in the future
that a lot of people are advocating for
that would be intentional.
And hopefully, this might help shift that conversation
just a wee bit.
So we're going to talk about something that happened down in Parkland
at a school.
No, not that one.
One three miles down the road, right?
Does have to do with kind of the same thing.
So a school resource officer doing his job,
walking around, checking stuff out,
just, you know,
looking around for anything that maybe shouldn't be there.
He's in the locker room.
He looks over and he sees a box.
Inside the box, there's a couple of guns.
So he does what he's supposed to do.
He takes it to his office, secures it,
calls up the detectives.
Detectives show up.
They're like, hey, guess what?
These belong to the principal.
Unattended in a locker room.
Now, when I say this, this is the official story that's going out.
And based on a couple of cursory calls,
this is actually what happened.
It does look like it was literally an accident,
that they weren't supposed to be brought in.
It appears that the principal had them secured in the vehicle
and somebody unloaded the vehicle
and took that box into the locker room.
They were unintentionally brought in.
Now, keep in mind, it's Florida,
so having it in your car,
that's like totally cool down here, you know.
So I understand that it is important to note legally
that this does look like it was unintentional,
because intent's kind of a big part of things.
And from everything I can tell, it was, in fact, unintentional.
But from the other side of things,
you understand that's worse, right?
I mean, you do get that.
These should have been secured in a vehicle outside.
And just through the normal course of events at a school,
they made it inside the school and then were left unattended.
What do you think's going to happen
when they're all over the school,
as some people suggest?
They got lucky.
They got lucky.
That's what happened.
It just happened that the SRO was on the ball for once,
you know.
I say for once, as in generally SROs.
I'm not talking about this cop in particular.
I don't know him.
But what if the SRO hadn't noticed it?
Who would have found it then?
Parents weren't notified about it until like 24 hours later.
This is something that will become a frequent occurrence
if people who push the idea of arming teachers gets their way.
And one day, it won't be the SRO who finds it.
One day, it won't be an accident that it's moved
from the location it's supposed to be in.
One day, it's going to go really, really badly.
This should be a warning sign,
and it's not an isolated incident.
If you actually looked at places
where they've started allowing this,
you find everything from one falling out,
you know, during class,
to them being left in bathrooms more than once.
It's not a good idea.
Most people do not understand the term
secure that firearm.
They don't really know what it means.
They think it means to know vaguely where it should be.
Teachers aren't warriors.
They shouldn't have to be.
Pushing this on them is a bad idea.
Allowing these weapons into the schools is a bad idea.
These weren't even supposed to be inside.
It was an accident.
This will go bad.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}