---
title: Let's talk about Ukraine, grain, and Africa....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zaKMmQegwyM) |
| Published | 2022/06/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia's invasion of Ukraine disrupted the world's grain supply, impacting Africa first, particularly the horn of Africa like Somalia.
- The disrupted grain supply combined with a drought in Africa has led to hundreds dying from famine with the situation expected to worsen.
- The cost of imperialism is often ignored, with wealthy and powerful countries not considering the consequences on poorer nations.
- The lack of aid in Africa is directly linked to Putin's invasion of Ukraine and the resulting disruption in the grain supply chain.
- The imperialist actions of powerful countries have far-reaching consequences on vulnerable populations in distant places.
- The coverage of Africa's crisis is tied to Putin's invasion, revealing the detrimental effects of global power dynamics.
- Aid efforts are hindered by the focus on money and other priorities, leaving many without food and facing dire circumstances.
- The foreign policy games played by powerful nations have real and devastating impacts on countries rarely acknowledged or visited.
- The situation in Africa serves as a stark reminder of the hidden costs of imperialism and global power struggles.
- Beau's reflection serves to shed light on the often overlooked repercussions of international conflicts on the most vulnerable populations.

### Quotes

- "The cost of imperialism is a cost that never gets talked about when wealthy countries play their little masters of the universe games."
- "The coverage that is going to come out of Africa is directly tied to Putin's invasion of Ukraine."
- "The lack of aid in Africa is directly tied to the disruption of the grain supply chain caused by that invasion."
- "The sad state of affairs and the reality of the imperialist nature of large powers."
- "These games that get played on the foreign policy scene impact countries that are seldom thought of and never visited."

### Oneliner

Russia's invasion of Ukraine disrupts grain supply, leading to famine in Africa, showcasing the hidden costs of imperialism on vulnerable nations.

### Audience

Policy Makers, Activists, Global Citizens

### On-the-ground actions from transcript

- Provide direct aid to famine-affected regions in Africa (suggested)
- Raise awareness about the impact of global power struggles on vulnerable populations (implied)
- Support organizations working to address food insecurity in Africa (exemplified)

### Whats missing in summary

The full transcript provides additional context on how international conflicts have tangible repercussions on distant and marginalized communities.

### Tags

#Russia #Ukraine #Imperialism #GlobalPower #Africa #GrainSupply #Famine #AidEfforts


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Russia and Ukraine, costs, grain, and Africa.
When this started, when Russia invaded Ukraine, we talked about how it was going to disrupt
the world's grain supply.
And we talked about how it was going to hit Africa first, it was going to hit poor countries
first.
That has started.
It's beginning in the horn of Africa, Somalia, in that area.
And there was a drought.
Now a drought is a drought, but there's not a lot of aid because the grain supply has
been disrupted.
They've already lost hundreds to famine and it will get worse.
When this really starts catching coverage everywhere as it spreads, this is Putin's
fault.
This is the cost of imperialism.
It's a cost that never gets talked about when wealthy countries, when powerful countries
play their little masters of the universe games.
The cost that is paid by those people who live in countries that have nothing to do
with it, who don't really have any chips in that international poker game where everybody's
cheating, those costs are almost always ignored.
And because they're ignored, we continue to make the same mistakes and we continue
to just forget about it.
Not a big deal.
We don't really have to worry about that because that's somewhere over there, those
far away places, those places that don't matter because they're not economically
powerful.
The coverage that is going to come out of Africa is directly tied to Putin's invasion
of Ukraine.
There's no other way to spin that.
The disruption of the grain supply is causing this.
The drought is the reason that locally they're not capable of producing their own, but without
the disruption caused by that invasion, aid would be there.
You'd have a lot more aid coming in.
But right now, countries are focused on more important things, you know, money, as hundreds
are lost because they don't have food.
That is a sad state of affairs and it is the reality of the imperialist nature of large
powers.
This is the cost.
It happens all the time.
These games that get played on the foreign policy scene, they impact countries that are
seldom thought of and never visited.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}