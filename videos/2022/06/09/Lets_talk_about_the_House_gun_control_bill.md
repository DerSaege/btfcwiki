---
title: Let's talk about the House gun control bill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PlMqCWFx9Dc) |
| Published | 2022/06/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The House passed a bill that is a collection of unrelated components addressing various issues.
- The bill contains both good and bad elements, with some parts being effective and others pointless.
- One particular section of the bill focuses on banning large capacity ammunition feeding devices.
- Despite exemptions and loopholes, the bill faces challenges in the Senate and is unlikely to pass.
- The definition of "large capacity ammunition feeding device" is critical, limiting magazines to 10 rounds in the bill.
- Beau believes the bill is already defeated in the Senate, with slim chances of passing.
- Combining different elements into one bill may hinder its overall success in legislation.
- Addressing issues separately could have increased the bill's chances of approval.
- The bill includes provisions for grants for safe storage, encouraging firearm security measures.

### Quotes

- "The House did something."
- "I can't believe anybody actually thought that was going to get through the Senate."
- "It's thoughts and prayers."
- "But hey, now they get to say, hey, we tried."
- "They clumped all this stuff together and sent it all up at once."

### Oneliner

The House passed a bill with various unrelated components, including a section on banning large capacity ammunition feeding devices, but its chances of passing the Senate are slim due to the broad scope and definitions.

### Audience

Legislative observers

### On-the-ground actions from transcript

- Advocate for bills to be broken down into separate components for better chances of approval (implied)
- Support grants for safe storage initiatives to encourage firearm security (implied)
- Stay informed and engaged in legislative processes to understand the impact of bills (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of a legislative bill's components, challenges in passing the Senate, and suggestions for improving the legislative process.

### Tags

#Legislation #BipartisanBill #GunControl #SenateChallenges #Advocacy


## Transcript
Well, howdy there, internet people.
Let's bow again.
So today, we are going to talk about that bill,
because, hey, one made it through the house.
People have been screaming, do something, do something,
do something, the house did something.
So we're gonna kinda go over it generally,
and then we're gonna dive into one particular section,
and then we'll go from there.
Okay, so what is the bill?
is a collection of stuff that is completely unrelated.
It is all over the spectrum when it comes
to what it's trying to address.
Because of that, you should probably
look at it section by section, rather than as a whole,
because a lot of it doesn't have anything
to do with each other.
Generally, some of it's objectively good.
Some of it really is good.
Some of it's straight out of that video of, hey,
This is stuff that'll work.
Some of it is totally pointless, and some of it
is just a dumpster fire.
So overall, it's just kind of there.
But looking at each section, it's a mix.
Some of it's not going to do anything at all.
Some of it would be very effective.
Some of it would do nothing.
There's a lot of pieces in it that have real merit.
But what we're going to do is we're
going to jump to one piece in particular in this.
It bans large capacity ammunition feeding devices.
Now, there are a bunch of exemptions.
And there are some loopholes in that,
which basically means there would be 30 round AR magazines
on the market forever.
The way this is written, just so you know.
But that doesn't really matter.
Because as we've talked about and as we definitely
saw when it came to the New Yorkville on body armor,
what really matters is the definition.
What is a large capacity ammunition feeding device?
Means a magazine, belt, drum, feed strip, helical feeding
device, because I guess it's the 1980s.
I guess there's a few still out there.
All right.
Or similar device, including any such device joined or coupled
with another in any manner that has an overall capacity of
or that can be readily restored, changed, or converted
to accept more than 10 rounds of ammunition, and this bill is dead in the Senate.
The good parts, the bad parts, the parts that would do nothing, it's all dead.
It's not going anywhere.
Ten rounds, the Senate that we currently have, the idea that a bill that is going to ban
magazines over 10 rounds is going to get through that Senate, that is rainbows and unicorns
and fairy tales.
It's thoughts and prayers.
It's not going to happen.
I would say that this bill is already defeated, they just haven't taken the vote yet.
But because it's politics and strange things happen, sure it has a 1% chance.
Most standard pistols today have magazines of 15 rounds.
Maybe if it was 20, maybe it would have gotten through, but probably not.
At 10, there's no way.
There is no way.
And the thing is, I have to believe that those people who read this, who put this together,
know that.
They know that.
But hey, now they get to say, hey, we tried.
We tried.
We clumped all this stuff together and sent it all up at once.
And now this one part is going to destroy the whole bill.
There was stuff in there about grants for safe storage.
assigning you know basically really encouraging locking up locking up and
securing your firearms had this stuff been done on separate bills that
probably would have got through the Senate some of the other stuff is even
some of the other stuff I think is good it's maybe but it had a fighting chance
With this? No. It's done. It's done.
But, I mean, they do get to say, hey, we did something. Right?
And that's, honestly, that's how I'm looking at this. This was a show.
I can't believe anybody actually thought that that was going to get through the Senate.
So, it's the definitions that matter.
you know if they had made it actual large capacity things that that most
people would view as large capacity maybe it had a shot defining it as ten
rounds that it's just not going to go it's just not going to pass when it comes
to stuff like this especially if you have stuff that is this wide of a
spectrum. Maybe take the time and have additional votes so it can be sent up
separately so some of it has a chance of getting through. Anyway, it's just a
With that, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}