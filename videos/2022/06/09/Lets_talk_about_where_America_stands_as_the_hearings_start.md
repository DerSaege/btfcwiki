---
title: Let's talk about where America stands as the hearings start....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vJq4SYCBeR8) |
| Published | 2022/06/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Taking stock before hearings on Trump's actions post-January 6th insurrection.
- Initial poll post-January 6th: 54% of Americans believed Trump should face criminal charges.
- Recent poll shows 52% still support criminal charges against Trump.
- Only 42% believe Trump should not be charged.
- Committee can't charge Trump but can refer to Department of Justice.
- Rumors suggest committee wants consensus before making referral.
- Polls may influence committee's decision on referral.
- Department of Justice may or may not prosecute after referral.
- Majority of Americans see criminal liability for Trump.
- Committee's actions may lead numbers supporting criminal charges to increase.
- Trump's desire for White House may be cooled by public opinion supporting criminal charges.
- Trump's efforts as Republican kingmaker have not gained significant traction.
- More than half of Americans believing Trump should be charged does not bode well for his presidential ambitions.
- Public perception and committee's presentation to determine future course of action.

### Quotes

- "More than half of Americans see criminal liability for the former president."
- "Those who believe he should be charged are probably really unlikely to believe he should be in charge."

### Oneliner

More than half of Americans believe Trump should face criminal charges post-January 6th, potentially impacting his political future.

### Audience

Concerned citizens, political observers.

### On-the-ground actions from transcript

- Monitor and participate in future polls to gauge public sentiment on Trump's actions (implied).

### Whats missing in summary

Insights on the potential impact of public opinion on the political landscape.

### Tags

#Trump #CapitolInsurrection #DepartmentOfJustice #PublicOpinion #PoliticalFuture


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to kind of take stock of where we're at before the hearings start.
We're going to see what the mood is in the country and where it kind of goes from here.
Because it has been a long time since the actual events of the 6th.
Right after the 6th, there was a poll conducted by Washington Post and ABC.
54% of Americans believed Trump should be criminally charged for his actions.
That's with everything still fresh in their mind, remembering all of the footage, the
shock of what occurred.
54%.
All of this time later, that poll was conducted again.
It's 52%.
After all this time, a majority of Americans still believe that Trump should be criminally
charged for his actions.
This is before the hearing.
And it's worth noting that only 42% said that he shouldn't be.
So first thing to point out with this, because that's what people keep talking about, is
that the committee can't charge him.
The committee can make a referral to the Department of Justice.
And they already have a pretty strong case when it comes to obstruction.
But the referral hasn't been made.
The general rumor is that they want to reach consensus on it.
And there's some debate over whether or not that referral should take place.
I'm going to suggest that the polls from this point forward are going to determine that.
Because while it shouldn't be a political thing, everything's influenced by politics
at this stage.
Now once the referral is made, the Department of Justice, they may prosecute.
They may not.
But the decision from the committee, it's probably going to be influenced by the polls.
If it hasn't been made yet, that's going to be the deciding factor.
So as we kind of look at where we're at now, more than half of Americans see criminal liability
for the former president.
Those numbers, if the committee does its job in any way, those numbers will probably only
go up.
Now whether or not they go up high enough to reach that consensus and generate a referral,
that's anybody's guess.
But there is one other thing.
The former president, Trump is sitting there and he wants that White House again so bad.
These numbers should really kind of cool him out a little bit.
He spent all of this effort trying to cast himself as kingmaker of the Republican Party
with little result.
He's not gaining status within the Republican Party.
And more than half of Americans believe he should be criminally charged.
That doesn't sound like the makings of a winning presidential run.
Those who believe he should be charged are probably really unlikely to believe he should
be in charge.
So this is where we stand right now.
We'll have to wait and see how the committee presents their case to the American people.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}