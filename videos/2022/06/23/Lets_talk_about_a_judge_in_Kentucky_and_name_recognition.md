---
title: Let's talk about a judge in Kentucky and name recognition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1Jh-FqkMqCI) |
| Published | 2022/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Judge Julie Halls Gordon was removed from her judgeship in Kentucky for repeated and systemic abuse of power.
- Despite being removed two months ago, she will still be on the ballot in Davis County, Kentucky.
- The only ways to bar her from future office in Kentucky are through an act of the General Assembly or losing her law license.
- Concerns exist that she might get elected solely based on name recognition, as anyone who files will be on the ballot.
- Name recognition plays a critical role in elections, with incumbents often benefiting from it.
- The debate about the importance of name recognition in elections is ongoing, with no doubt about its significance.
- If Judge Gordon wins, it could signify the paramount importance of name recognition in elections.
- The potential scenario of her being elected to a new judgeship raises questions about legal implications and removal from office.
- Her situation might lead to a unique legal case that could attract national attention.

### Quotes

- "Repeated and systemic abuse of power."
- "If she wins, that's a pretty clear sign that name recognition is incredibly important."
- "It is definitely going to end up being a unique legal situation up there that will eventually probably make national news."

### Oneliner

A judge's removal for abuse of power in Kentucky sparks concerns about future election prospects based on name recognition, shedding light on the pivotal role of incumbency and familiarity in politics.

### Audience

Voters, Legal Experts

### On-the-ground actions from transcript

- Monitor local elections and candidates for positions of power (implied).

### Whats missing in summary

Insights on the potential consequences of electing officials based solely on name recognition and the need for informed voting decisions.

### Tags

#Kentucky #Elections #AbuseOfPower #NameRecognition #LegalImplications


## Transcript
Well howdy there internet people. It's Beau again. So today we're going to talk about a judge in Kentucky
and what that can tell us about a lot of politicians we find in DC
and kind of wonder why they're still there. So a judge named Julie Halls Gordon
was removed from her judgeship by the Judicial Conduct Commission. Unanimous decision back in
April two months ago. She was removed for what they called repeated and systemic abuse of power.
And they said that that pattern occurred over her entire tenure as judge.
Even though she was removed two months ago, she'll be on the ballot.
It's happening in Davis County, Kentucky, which is spelled Davies if you're going to look it up.
And what happened is they had one family judge and now a second position has opened.
And basically anybody who fills out the paperwork is going to be on the ballot.
Now being removed for abuse of power, that seems like something that would bar you
from future office. However, in Kentucky that's not how it works and to change it you would need
a constitutional amendment. And the only ways that she could really be barred from future office
is by an act of the General Assembly or if like she lost her law license.
But there's a lot of concern that she might get elected because of name recognition
solely based on that fact. If she is on the ballot and people recognize her name
and a whole lot of other people are on the ballot because anybody who files will be on it,
she may end up coming out on top.
This phenomenon of name recognition really mattering in elections, it's debated to the degree
of how important it is, but nobody debates whether or not it's important.
For a lot of politicians, this is how they stay in office. That name recognition,
that's why incumbents are harder to beat because people know them.
Sometimes it's, you know, the devil you know and sometimes it's just a matter of the voters
not being informed about the candidates and just going off of the name that well seems familiar.
We're going to see exactly how important name recognition is here.
If she wins, that's a pretty clear sign that name recognition is incredibly important
and maybe more important than a lot of people give it credit for.
Now, people have asked like what's going to happen if she is elected to the new judgeship.
I honestly don't know. I'm not sure in how it works in Kentucky as to whether or not
the assembly could remove her from a different office for acts she committed, you know,
in the first judgeship. I'm not sure how that works. It is definitely going to end up being
a unique legal situation up there that will eventually probably make national news.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}