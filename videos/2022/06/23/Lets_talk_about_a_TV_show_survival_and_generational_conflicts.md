---
title: Let's talk about a TV show, survival, and generational conflicts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LE_LDs8A_5k) |
| Published | 2022/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A TV show about putting millennials through survival school, but they are actually 20-21-year-old kids, not millennials, tapping into generational nonsense.
- The show focuses on poking fun at a group of people rather than teaching actual survival skills.
- Beau questions why there is a push for younger generations to have the same skillset as the older generation, seeing it as a lack of progress.
- He shares a personal anecdote about a friend feeling like a failure for not teaching his son survival skills like hunting, only doing it once with him.
- Beau believes in progress and wants his kids to have a better skill set than he does, not the same.
- He references a quote about studying different subjects for progress and questions why society is stuck in perpetuating macho nonsense.
- Beau believes in basic survival skills but not at the expense of mockery, as it hinders true learning and self-reliance.
- He concludes by reflecting on how each generation believes the younger one will bring about downfall but is actually shaped by the previous generation.

### Quotes

- "Most complaints about the younger generation are really just admissions of failure for the older generation."
- "Yeah, I want everybody to have basic survival skills for an emergency, but it shouldn't be your life."
- "Society is supposed to progress."
- "Each generation feels the younger generation is going to be the one that brings down the world."
- "Y'all have a good day."

### Oneliner

Beau questions the mockery in teaching survival skills and advocates for progress and self-reliance in shaping future generations.

### Audience

Educators, parents, society

### On-the-ground actions from transcript

- Foster learning environments that prioritize teaching practical skills without mockery (implied).
- Encourage progress and self-reliance in education and skill development (implied).

### Whats missing in summary

The full transcript provides deeper insights into generational dynamics and the importance of progress in shaping future generations.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about a TV show
because like 15 of y'all sent me this link
to a review of a show that's coming out.
And the general concept of the show
is to take a bunch of millennials,
is the term they used,
and basically put them through survival school.
Seemed kind of interesting.
I would point out that these aren't actually millennials.
And this is my first sign that this is probably
not gonna be something I'm gonna watch.
They're not millennials.
These are 20, 21 year old kids.
They're using the term millennial
to tap into the culture nonsense,
the generational nonsense that's going on.
Just to go ahead and point something out,
those weak, whiny millennials you keep talking about,
that's the 19 year old who showed up in Iraq in 2003
and spent his entire career at war.
That's the weak, whiny millennial.
Just so you know, same generation.
It's not millennials.
They're younger than that.
And it's really just entitled people.
But still, the idea of taking a bunch of rich,
entitled people and putting them through survival school,
still mildly interesting to me, so I keep reading.
And then I get to one of the dramatic scenes
that it's talking about,
where they give the vegan the deer to skin.
And at that point, I'm like, yeah, okay,
so it's just overdramatic nonsense.
That's not actually school, not really.
I mean, if you've been to Sierra or something like that,
you've been through some kind of survival school,
what's easier, taking a deer or finding edible plants
in an area where deer live?
Probably easier to find the plants, right?
So it's not actually about teaching survival skills.
It's about poking fun at a group of people
to feed culture war stuff.
Not really something I would watch.
In the description of that scene,
it reminded me of a clip that went viral
a couple of years ago.
And actually I think I did a video about it
because it bothered me so much.
It's a young man, I don't know, late teens, early 20s,
and it's at some kind of family gathering.
And one of the older family members
gives him a manual can opener.
And they film him and everybody laughs
because he doesn't know how to use it.
Okay, just out of curiosity,
whose responsibility was it to teach him how to use it?
Most complaints about the younger generation
are really just admissions of failure
for the older generation.
And I think that's a wider perspective.
Why is there this drive for particularly men
to have their kids have the same skillset they do?
I mean, it kind of denotes a lack of progress, right?
If they have to know the same stuff you did.
I mean, society's not moving forward.
I had a recent conversation that is like hyper related
to this with a friend of mine.
He had just found out that his kid who is now 21, 22,
something like that,
that the only time he had taken and skinned a deer
was when he was 15, one time with his dad.
And he's sitting there just venting, going off about it.
He's like, I felt like I let him down.
I can't believe he doesn't do that,
doesn't really know how to do that.
Just that one time.
I'm sitting there and I'm letting him vent
because I can tell he's in a mood.
But eventually, I have to say, yeah, I remember.
You know, I remember how much you used to hunt.
You had all those stuffed deer all over your house.
He kind of looks at me.
No, no, no.
Yeah, you had them everywhere.
They were everywhere.
No, no, no, we did not.
We didn't have money for taxidermy.
And that's why you hunted so much, isn't it?
Your kid didn't have to do that, huh?
And I'm sitting there with that smirk on my face
because I was just Andy Griffithed him.
And he promptly tells me to shut up,
but I feel like I made my point.
I don't want my kids to have the same skill set I do.
I would feel like an abject failure if my kids
have that same skill set.
I want them to be better than me, not me.
Since most of the people who fall
for this idea of wanting to perpetuate the macho nonsense,
since most of them are people who like to quote the founders,
I have a quote for you.
I must study politics and war that our sons may
have the liberty to study mathematics and philosophy.
Our sons ought to study mathematics and philosophy,
geography, natural history and naval architecture,
navigation, commerce, and agriculture
in order to give their children a right
to study painting, poetry, music, architecture, statuary,
tapestry, and porcelain.
You're supposed to progress.
Society is supposed to progress.
Yeah, I want everybody to have basic survival
skills for an emergency, but it shouldn't be your life.
And when you're teaching people, it shouldn't be mockery.
That's not really a motivating thing.
That doesn't actually make them want to learn,
and it doesn't make it something that they're
going to carry with them.
They're just trying to get through it.
They miss out on the deeper aspects of it
when it comes to self-reliance and all of that stuff.
So now I'm probably not going to watch the series just based
on the link that I was sent, but it's interesting to me
that these discussions, they've taken place
for hundreds of years.
Each generation feels the younger generation
is going to be the one that brings down the world.
And each generation is created and molded
by the one before it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}