---
title: Let's talk about pals at DARPA being too quiet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=j73fYkHl-4k) |
| Published | 2022/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the idea of the Navy using fish to track submarines, which he clarifies as both untrue and true in a strange way.
- Explaining the concept of ambient noise and its significance in detecting potential threats.
- Describing DARPA's program called PALS (Persistent Aquatic Living Sensors) aimed at utilizing aquatic life sounds to track opposition naval forces.
- Detailing the process of filtering out organic ambient noise underwater and the potential use of this noise for tracking purposes.
- Mentioning the use of a computer to analyze biological entities' reactions to submarines, large ships, or underwater drones.
- Speculating on the likelihood of DARPA having already completed the program, given their typical operational procedures.
- Comparing DARPA to an agency capable of reverse engineering crashed alien spaceships and hinting at their advanced technologies.
- Suggesting that the information publicly shared about the PALS program may soon transition from speculation to established scientific fact.

### Quotes

- "That's silly. That's not true. But also yes it kind of is true."
- "What if I told you there's some truth to that statement? And it's not all just a movie trope."
- "It's quiet. Too quiet."
- "Normally when you're talking about DARPA, if you're not familiar with this agency, just understand if there was an agency that was going to like reverse engineer some crashed alien spaceship, it would be them."
- "So anyway, it's just a thought."

### Oneliner

Beau clarifies the truth behind using fish to track submarines and delves into DARPA's program exploring aquatic life sounds for national security tracking, hinting at futuristic scientific developments from current speculations.

### Audience

Science enthusiasts, national security analysts.

### On-the-ground actions from transcript

- Stay updated on DARPA's advancements in tracking technologies (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of DARPA's innovative program utilizing aquatic life sounds for national security tracking, offering insights into the potential future implications of this research.

### Tags

#NationalSecurity #DARPA #AquaticLife #SubmarineTracking #Innovation


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about fish and bugs and noise and what all of this has to
do with national security.
Got a message.
Is it true that the Navy is going to start using fish to track submarines?
No that's not true.
That's silly.
That's not true.
But also yes it kind of is true.
In a really weird way.
The easiest way I can explain this is...
It's quiet.
Too quiet.
You've heard that in a bunch of 80s action movies right?
What if I told you there's some truth to that statement?
And it's not all just a movie trope.
When you are walking in the woods, the animals and insects closest to you, they get quiet
because they're trying to hide from what they perceive as a predator.
You don't really notice this when it's just you or your group walking.
Because you have the ambient noise from the other animals and insects out there that are
a little bit further out.
You can still hear those.
But if you're walking along and in front of you there's also no noise, well that's too
quiet.
That means that there's probably another group of people up in front of you and you're about
to run smack into them.
So that cliche has an element of truth to it.
And that is what DARPA, not the Navy, DARPA is looking into in a program called PALS.
Persistent Aquatic Living Sensors.
The idea behind it is to monitor what we are currently filtering out.
When we have people listening for submarines and stuff like that, there's a bunch of organic
ambient noise that's underwater.
And right now it's being filtered out.
Not listening to it.
It's viewed as noise, distortion.
The idea is to use a computer to actually go through and listen to that.
And see if they can pinpoint certain biological entities.
If their reaction to a submarine or a large ship or an underwater drone is consistent.
And if it is, we can use those sounds as a method of tracking opposition naval forces.
So they're not like training fish to go and follow submarines, but they're listening to
the sounds that aquatic life makes in hopes of being able to track it.
Track submarines or whatever passes through based on the deviations.
So it's an interesting program.
I would point out that they've been talking about this for, I think I first heard about
it more than a year ago.
Which means they're probably done with it by now.
Normally when you're talking about DARPA, if you're not familiar with this agency, just
understand if there was an agency that was going to like reverse engineer some crashed
alien spaceship, it would be them.
Usually by the time they're talking about something like publicly, like they kind of
are with this, they're giving away the general idea of it.
Most times they've already accomplished it.
So that is probably something that we will hear about as science fact in the future rather
than speculation that it is right now.
So anyway, it's just a thought.
I hope you have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}