---
title: Let's talk about $250 million, Trump, and the bigger story....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yu41FzX7VJc) |
| Published | 2022/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's team raised $250 million in the guise of an official election defense fund, although there wasn't one.
- Most of the money raised actually went to Trump's PAC, Mark Meadows' charity, Trump's hotel collection, and the rally company for January 6th.
- The committee aims to demonstrate that Trump knew his election claims were baseless from the start.
- Trump's actions raise questions about taking money under false pretenses and not using it for the stated purpose.
- Trump may have deceived working-class donors by making false claims about the election and misusing the raised funds.
- The committee is expected to reveal more information linking back to previously disclosed details, forming a wider narrative.
- Trump's pattern of deceiving working-class donors might be typical, but doing so knowingly under false pretenses adds another layer of deceit.
- Watching the hearings requires connecting new information to the bigger picture the committee is painting.
- The hearings aim to establish a comprehensive conspiracy rather than focusing solely on daily stories.
- The committee's strategy involves revealing puzzle pieces that, when put together, show a larger scheme at play.

### Quotes

- "Trump's team raised $250 million under false pretenses."
- "The committee aims to demonstrate that Trump knew his election claims were baseless."
- "Don't just get lost in the daily story. Use it like a jigsaw puzzle piece."

### Oneliner

Trump raised $250 million under false pretenses, deceiving working-class donors, while the committee aims to show his knowledge of baseless election claims in a broader conspiracy narrative.

### Audience

Political analysts, investigators

### On-the-ground actions from transcript

- Follow updates from the committee hearings and connect new information to previously disclosed details (implied).

### Whats missing in summary

Full context and depth of analysis on Trump's fundraising tactics and the committee's investigative approach.

### Tags

#Trump #CommitteeHearings #ElectionClaims #Deception #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about $250 million, a quarter of a billion dollars that the Trump
crew raised.
And what happened to it and why this fits in and is important when you look at the way
the committee is laying out events.
It's not just another aha moment.
Okay so let's start with this.
Apparently the official election defense fund, the emails, and there were millions of them
that went out raising money for this, they generated around a quarter of a billion dollars.
Now according to the committee and according to reporting, there wasn't an official election
defense fund.
That wasn't actually a thing.
Looks like most of the money instead went to the Save America PAC, which is Trump's
PAC, which then went to Mark Meadows charity, which employs former Trump people, went to
the Trump hotel collection, and went to the company that put together the rally for the
6th.
So this is the information that's coming out and people are like, ah, giant grifting operation,
blah blah blah.
And this is kind of the first takeaway from it.
Sure, it certainly appears, based on the reporting and the committee, that the Trump world went
after small donors and got a whole lot of money from working class people.
But let's go to what the committee's laying out.
Remember, they're trying to demonstrate that he knew from the beginning that his claims
about the election were baseless.
Now that puts this whole thing into a different light.
If Trump knew his claims about the election were baseless, and then they raised money
under that guise, and then they never used it for it?
They never really used the money for that purpose?
Man, that alters things a little bit.
It's not just that he took this money in and didn't use it for the stated purpose.
I mean, depending on the fine print, that might actually be okay.
But if he's out there saying that this is why we're taking the money, but he knows that
isn't true, that the claims that he made about the election, that none of that actually happened.
If he knew that wasn't true, and he said that to the public and took money for it, and then
didn't use that money for that stated purpose, my guess would be because he knew it didn't
matter because he knew it didn't happen.
That adds a different level to it.
And I think that what we're going to see from this point forward in the committee is a whole
bunch of things that kind of bounce back and tie back to something that they have demonstrated
prior.
So you have to keep it kind of in a loop and make sure that any new piece of information,
you're tying it back to stuff that was previously disclosed.
You know, Trump running a thing that takes a whole bunch of money from working class
people and doesn't really deliver, I think that's just kind of normal for him.
But doing it in a deceitful manner over something that he knew wasn't true, there might be a
word for that somewhere in the legal dictionary.
So just remember this when you're watching the rest of the hearings, because you're going
to, I think this is going to be a recurring thing, I think you will see a piece of information
that in and of itself is a story.
Don't just get lost in the daily story.
Use it like a jigsaw puzzle piece and try to put it in there, because it looks like
they really are following that template and they told you they were going to establish
a wide-ranging, for lack of a better word, conspiracy.
And I think that's what they're trying to show.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}