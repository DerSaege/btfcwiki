---
title: Let's talk about the committee and making referrals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5cGhdQFpdO4) |
| Published | 2022/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Referrals from the committee were a topic of debate after initial news reported they wouldn't happen, later corrected to say no decision had been made yet.
- There are two contrasting schools of thought regarding issuing referrals: one suggesting it's unnecessary for the Department of Justice (DOJ) to need a referral and could be viewed as nonpartisan, while the other argues that it's vital for Americans to understand the process and might be seen positively as a Democratic Party action.
- The choice on referrals is complex due to political implications, with considerations on potential views of the American people, political parties, and DOJ's independence.
- The hope is for DOJ to act independently, but the timing and details of their actions remain uncertain.
- Despite the focus on referrals, the importance of Americans engaging with the hearings to understand the events is emphasized.
- Ultimately, whether referrals are issued or not, it doesn't guarantee DOJ indictment and shouldn't be cause for panic, as it's more of a political strategy rather than a lack of evidence.

### Quotes

- "It's very important for the American people to hear about what happened."
- "It's actually a power that Congress has."
- "It's not a have to. It's a we would like you to type of thing."
- "That's part of this that can't get lost in the debate over the referrals."
- "Y'all have a good day."

### Oneliner

Beau explains the debate over committee referrals regarding DOJ indictments and stresses the importance of Americans understanding the process, regardless of the political implications.

### Audience

Congressional observers

### On-the-ground actions from transcript

- Watch and understand the hearings to grasp the significance of the events (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the considerations surrounding committee referrals, DOJ indictments, and the importance of public engagement with the hearings.

### Tags

#CommitteeReferrals #DOJIndictments #PoliticalImplications #PublicEngagement #CongressionalOversight


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
referrals and whether or not they're going to happen from the committee. Because last night
there was, let's just say discussion over it. News broke said they weren't going to issue any
referrals. That the hearings weren't going to lead to referrals. Then the news came out, well,
they haven't made the decision yet. And it's gone back and forth. When news broke saying,
hey, they're not going to do referrals, my inbox blew up. So what we're going to do is we're going
to go over the two schools of thought on this. And to be honest, I don't know what the right move is.
I really don't. I've gone back and forth on it myself. Okay. So if they don't issue any referrals,
that means that it's not political. DOJ doesn't actually need a referral from the committee
to indict. They don't need the committee at all. Okay. So if they don't issue a referral and DOJ
indicts on their own, it looks nonpartisan. DOJ acting impartially, doing their job.
There's no political liability for the Democratic Party. And there's one less talking point for
Republicans to just say, oh, it's political. Okay. That's the thought process behind it.
Now, the flip side of that is that if Americans don't understand that the DOJ doesn't actually
need the committee to indict, they may view the hearings as pointless. And it's very important
for the American people to hear about what happened. Right. So that's a downside to it.
The other side is to issue the referrals. And that side says, well, A, it's actually a power
that Congress has. Not exercising it because it's going to hurt the Republican Party's feelings
probably isn't a good precedent to set. There's also the feeling that the referrals may not be
a political liability. They may be seen as a good thing that the Democratic Party did something.
So those are the two schools of thought on this, and this is why they're going back and forth.
I don't know what the right move here is. There's ideally DOJ acts on its own, like right in the
middle of the hearing. That would be the perfect scenario. But we don't know that they're going to
do that. Not issuing the referrals and then waiting like a week afterward to maybe allow DOJ
to act on their own and then issuing them later. That's an option, too, to try to force their hand
if they choose not to act. There's a whole parallel investigation that we're not hearing anything
about. We know that it's happening. We get little bits and pieces all over the place. FBI questioning
whoever, but we're not getting updates on it. At the same time, that's kind of how Garland's always
done things. So we don't know how that's progressing. They may be on the verge of indicting on their own.
There's a lot of information that we don't have. My take on this is that it's important
for Americans to watch these hearings, to understand what happened.
That's part of this that can't get lost in the debate over the referrals. People need to know.
And then from there, I mean, understand even if the committee makes a referral, DOJ may not indict.
It's not a have to. It's a we would like you to type of thing. So if they decide, if it does
become a firm stance that they're not going to issue referrals, don't panic. It's a political move,
not one saying they don't have evidence. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}