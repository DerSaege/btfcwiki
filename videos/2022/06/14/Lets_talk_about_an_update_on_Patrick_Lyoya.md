---
title: Let's talk about an update on Patrick Lyoya....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H5XWTtZJoQ8) |
| Published | 2022/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on the case of Patrick Leoya, a previous video that deeply affected him.
- Describes a communication issue at the beginning of the police engagement.
- Suggests that the officer initiated contact and escalated the situation.
- Mentions Patrick reaching for a taser, which is often used to justify police actions.
- Expresses surprise at charges being filed against the officer for manslaughter and second-degree murder.
- Notes the prosecutor's uphill battle but acknowledges a shift in attitudes towards presenting cases like this to a jury.
- Speculates that there may be more information known by the prosecutor and state cops.
- Concludes with uncertainty about the case's outcome.

### Quotes

- "The officer escalated at every possible opportunity."
- "It's worth noting that the prosecution does have an uphill battle here."
- "I've got to be honest, that's surprising."

### Oneliner

Beau provides an update on the case of Patrick Leoya, expressing surprise at charges filed against the officer for manslaughter and second-degree murder, indicating a shifting attitude towards prosecuting such cases.

### Audience

Advocates and Justice Seekers

### On-the-ground actions from transcript
- Support advocacy groups working towards police accountability (implied)
- Educate others on the importance of holding law enforcement accountable (implied)
- Stay informed about developments in cases of police misconduct (implied)

### Whats missing in summary

Additional context and details from Beau's emotional response and analysis of the case.

### Tags

#PoliceAccountability #Justice #Prosecution #CaseUpdate #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to provide an update on an older video.
It was a video where I go through and I break down
what happened in a video so you don't have to watch it.
It was one of those.
Because there have been some developments in that situation
and they are definitely worth going over.
So today we will provide an update on the case of Patrick Leoya.
If you remember, this is one that, I'm going to be honest, it got to me, it bothered me.
And if you don't know, I do a lot of these videos where I break down what happened in
police engagements.
In this one there was a communication issue right from the beginning.
And it seemed to me that the cop was aware that there was a communication issue.
It also appears that the officer initiated contact.
And then from there it seemed as though the officer escalated at every possible opportunity.
Then near the end, Patrick reaches out, gets that taser, kind of moves it away from him.
And in the video, in the initial video, I said, you know, we're not talking about the
justice system.
We're talking about the legal system.
And typically if the suspect reaches, makes contact, appears to reach, that becomes the
entire justification for everything that happened afterward.
And I didn't expect charges, you know.
I said that a prosecutor might decide to test a jury on this because attitudes are shifting.
But because of that one part, it seemed as though they wouldn't pursue charges.
Because generally speaking, they can use that to justify everything else.
Even if you're talking about a situation in which the cop or cops does everything else
wrong.
Typically that's what they fall back on.
So apparently the prosecutor and the state cops looked at that video and saw exactly
what I saw.
And they decided that the move at the taser did not override everything else.
They have filed charges four days ago, I think it was four days ago.
The lesser accompanying charge is manslaughter.
The main charge is murder too.
Second degree murder.
I've got to be honest, that's surprising.
I do a lot of these videos, way more than should be necessary.
This is the first decision that has really surprised me.
So the charges have been filed.
The officer is looking at life on the main charge.
It's worth noting that the prosecution does have an uphill battle here.
But it demonstrates that shifting attitude of prosecutors.
When it comes to being willing to even present a case like this to a jury.
So we don't know what's going to happen.
But it's already gone further than I expected.
And I have a feeling that there is something that that prosecutor and those state cops
know that we don't.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}