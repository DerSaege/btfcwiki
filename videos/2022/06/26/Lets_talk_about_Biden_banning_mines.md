---
title: Let's talk about Biden banning mines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ntSKphia-WQ) |
| Published | 2022/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the evolution of foreign policy from President Obama to Trump to Biden and the impact of inconsistent policies on the nation.
- Details how the Biden administration plans to lead the U.S. to comply with the Ottawa Convention of 1997, which banned anti-personnel mines.
- Contrasts President Obama's restrictive policy on anti-personnel mines with Trump's decision to relax it.
- Mentions the Biden administration's alignment with Obama's stance, heavily restricting the use of anti-personnel mines except on the Korean Peninsula.
- Notes that the U.S. does not have fields on the Korean Peninsula but has committed to defending South Korea.
- States that the Biden administration will supply and possibly deploy anti-personnel mines to South Korea while destroying any unused mines from the U.S. stockpile.
- Acknowledges potential pushback against restricting tools for troops and the evolving technology of anti-personnel mines that go inert after a set period.
- Emphasizes that the U.S. military doctrine focuses on mobility, making anti-personnel mines largely obsolete.
- Supports the idea of moving towards compliance with international treaties like the Ottawa Convention to modernize the military and improve effectiveness.
- Encourages further steps by the Biden administration to potentially address cluster munitions compliance in the future.

### Quotes

- "It is modernizing it."
- "It's not weakening the military."
- "It's just a thought."

### Oneliner

Beau explains the shifting foreign policies from Obama to Trump to Biden, focusing on anti-personnel mines and the modernization of the military.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Advocate for compliance with international treaties like the Ottawa Convention (suggested).
- Support modernization efforts within the military (suggested).

### What's missing in summary

Deeper insights into the potential ramifications of non-compliance with international treaties and the broader implications of modernizing military strategies.

### Tags

#ForeignPolicy #MilitaryModernization #Compliance #InternationalTreaties #USPolicy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about developments
in foreign policy from President Obama to Trump to Biden
and how inconsistent policies can kind of derail things.
So the Biden administration has announced its plan
to bring the United States to a path that will eventually lead to the U.S. being in
compliance with the Ottawa Convention of 1997.
This is a convention that banned the use of anti-personnel mines.
Now, President Obama had a pretty restrictive policy outside of South Korea, outside of
the Korean Peninsula.
They weren't really to be used anywhere else.
When Trump came into office, he decided to relax that.
I don't know that they were ever actually used anywhere,
but undoing everything that Obama did
was my keynote for him.
So the Biden administration comes in.
The Biden administration has adopted
a very similar stance to the Obama administration
when it comes to heavily restricting their use
anywhere except on the Korean Peninsula.
Now, the United States does not officially have any fields
on the Korean Peninsula.
However, it has said that it would help defend South Korea.
So in that very unique situation,
The Department of Defense feels that they might be useful.
So the administration has basically decided they're going to figure out what South Korea
needs, supply it, deploy it possibly, and any of the three million anti-personnel mines
that are in the U.S. stockpile that are not used will be destroyed.
That's good news.
Now there will undoubtedly be pushback, people saying, oh, you're limiting the tools of
the troops and all of that.
And people will talk about the newer technologies when it comes to anti-personnel mines, meaning
one of the large issues with these mines is that once you put them down, they're down.
doesn't know if it's a soldier or a kid playing soccer that steps on it. They're
bad. Now a lot of newer technologies are coming along that basically they go
inert after a set period of time and that's fine. I mean that's a good
step in all of that. But the reality is US doctrine is heavily focused on being
extremely mobile. From the US military standpoint, from its doctrine, from the
way it tries to wage wars, anti-personnel minds are kind of obsolete. The only
reason they are given this exemption for the Korean Peninsula is because of that
unique static situation that exists there. I personally don't really think
there'd be much use there either, but that's why that exemption exists. I don't
know, I don't think that the United States has deployed any personnel mines
in any significant way since like 1991, since the first Gulf War. They're an
obsolete tool, and it's better to just let them go. Just get rid of them, get on
a path that's going to put us in compliance with a treaty from 1997 that
most of the world is at least somewhat trying to abide by. We've seen them
deployed in Ukraine, which again, very surprising.
I don't know how effective they're really going to be.
It's not a modern doctrine.
So when you hear that pushback and say, oh, it's weakening the military, no, it's modernizing
it.
It is modernizing it.
We have far better tools to deny opposition forces a route, which is really what these
are for.
And those tools aren't persistent for decades, so it's modernizing the military.
It's not weakening it.
It would be nice if the Biden administration would continue this path and come into compliance
with the Ottawa Convention, maybe even move on to cluster munitions as well.
That could be a good move in the future.
But undoubtedly, as the current headlines and sensationalism about the hearing kind
of slows down, this will become a point of attack for the right.
But it is absolutely not weakening the military.
It's modernizing it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}