---
title: Let's talk about SCOTUS and Miranda....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pA3vXnYMMzU) |
| Published | 2022/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the recent Supreme Court ruling related to Miranda rights, clarifying that it removed the civil remedy for police failing to read the Miranda rights.
- Quotes law professors and criminal defense lawyers advising people to remain silent and ask for a lawyer when dealing with the police, regardless of Miranda rights.
- Emphasizes the importance of not talking to the police, even if Miranda rights are completely overruled in the future.
- Notes the significance of the ruling that limits suing cops who ignore Miranda rights, urging people to understand the advice of remaining silent and asking for a lawyer for protection.
- Conveys the unanimous advice from lawyers to always remain silent and request a lawyer when interacting with law enforcement, as it is the only thing lawyers universally agree on.

### Quotes

- "Do not talk to the cops. Ask for a lawyer, then shut up."
- "You still have a right to remain silent. Use it. Do not talk to the cops."
- "Shut up and ask for a lawyer. That's what they will all tell you."

### Oneliner

The Supreme Court ruling removed the civil remedy for police not reading Miranda rights, reinforcing the universal advice to stay silent and ask for a lawyer when dealing with law enforcement.

### Audience

Everyday citizens

### On-the-ground actions from transcript

- Memorize and internalize the advice to remain silent and ask for a lawyer when interacting with law enforcement (implied).

### Whats missing in summary

Importance of understanding and exercising rights during police encounters.

### Tags

#SupremeCourt #MirandaRights #LegalAdvice #PoliceEncounters #CivilRights


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about the
Supreme Court ruling that is related to Miranda. So Miranda is the thing you see in all the TV
shows. You have the right to remain silent. Anything you say can and will be used against
you in a court of law, so on and so forth, right? Okay, so the ruling removed the civil remedy
if a cop doesn't read you that. That's what this did. It didn't somehow overturn your right against
self-incrimination. So we're just going to clarify that today. I am not a lawyer, so I have some
quotes from some law professors and some lawyers. When it comes to the criminal defense lawyers,
the advice is pretty much the same. It is shut up Friday. Okay, so this is from a law professor.
A quick PSA for non-lawyers who are not following opinions closely. SCOTUS did not overrule Miranda
today. Instead, it held there was no civil remedy for plaintiffs who sue police for failure to give
Miranda. This court may well overrule Miranda one day. It didn't do it today. This is a criminal
defense lawyer. As a criminal defense lawyer, I'm not a fan of how our system violates individual
rights, but with that said, Miranda had nearly zero impact. People don't know to shut up,
no matter how many times we lawyers tell them to shut up. Now that you have no civil recourse
against cops ignoring your Miranda rights, it's more important than ever to remember to never
talk to cops. I believe this person is a PI who used to be a cop. Seriously, to my non-lawyer
peeps, even if Miranda is completely overruled, you still do not have to talk to the police. Do
not talk to the police at all, ever, except to say you want a lawyer. That's it. That's all.
PSA from your friendly public defender. You still have a right to remain silent. Use it. Do not talk
to the cops. Ask for a lawyer, then shut up. It's all pretty much the same. Every lawyer will tell
you the same thing. Don't talk to them, except to say, I want a lawyer. That's really what you need
to know. That should provide a little bit of protection against the new ruling. Just understand,
shut up, and ask for a lawyer. That's what you do. That's the advice from lawyers. That's what
they will all tell you. This is kind of a big deal, because this is one of those situations
where they remove a remedy. Like, so you can't sue the cop anymore, basically. So while the right
still exists, they didn't remove the Miranda process. They just made it to where if the cops
didn't follow it, well, you can't really do anything about it, which is kind of the same thing,
right? But if you have ever talked to a lawyer, they all give you the same advice. Shut up.
Doesn't matter. Doesn't matter if you're innocent, guilty. Doesn't matter. Just be quiet and ask
for a lawyer, and then say nothing else. So while this actually is pretty, to me, this is pretty
detrimental. I think this is a really bad ruling, and I think it will have real impacts for people.
Just, if you follow the two pieces of advice that every good lawyer will tell you, it's the only
thing lawyers agree on. If you ever ask a lawyer a question, you know that the next two words are
going to be, it depends. Except for this. This is the only thing they agree on. Shut up and ask for
a lawyer. So just remember that. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}