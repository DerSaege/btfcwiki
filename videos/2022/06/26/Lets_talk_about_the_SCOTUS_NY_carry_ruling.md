---
title: Let's talk about the SCOTUS NY carry ruling....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wCH89SE-BIM) |
| Published | 2022/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Supreme Court's ruling on carrying things, clarifying misconceptions around permitless carry.
- Draws a comparison between may issue and shall issue terms using the example of driver's licenses.
- Details that states with may-issue licenses for concealed carry will have to rewrite their laws to make them shall-issue, with objective and quantified requirements.
- Mentions that the ruling doesn't mean anyone can walk in and get a license; requirements must still be met.
- Suggests that the ruling impacts only a few states, not a significant number.
- Indicates that the court's interpretation of "keep and bear" is broader than previous courts, potentially affecting other gun laws at state or local levels.
- Notes that bans on high capacity magazines may not survive this court's challenges.
- Speculates on potential broadening interpretations of "bear" by the court, hinting at rifle racks returning but not causing mass arming in New York.
- Concludes by stating that the ruling isn't as groundbreaking as some portray it, and states with may-issue laws can still regulate licenses objectively.

### Quotes

- "Most states have shall issue. This applies to, I want to say, five states."
- "The states that have may issue can still regulate concealed licenses. They just have to do it in a more objective way."
- "Their interpretation of bear leads me to believe that things like rifle racks like back in the day in pickup trucks, those may be coming back."
- "It's not going to be the outcome from this."

### Oneliner

Beau clarifies the Supreme Court's ruling on carrying things, debunking permitless carry misconceptions and explaining the shift from may issue to shall issue terms for concealed carry licenses, hinting at broader implications for gun laws.

### Audience

Legal enthusiasts, gun policy advocates

### On-the-ground actions from transcript

- Contact your state representatives to stay informed about any changes in concealed carry licensing laws (implied).
- Join local gun policy advocacy groups to understand and potentially influence future developments in gun legislation (implied).

### Whats missing in summary

Further details on the potential impact of the broader court interpretation of "keep and bear" on specific state or local gun laws.

### Tags

#SupremeCourt #GunLaws #ConcealedCarry #LegalInterpretation #PolicyImpact


## Transcript
Well, howdy there, internet people.
It's Bill again.
So today, we are going to talk about the Supreme Court's
ruling concerning carrying things.
We're going to first talk about what it didn't do.
Then we're going to talk about what it actually did.
And we'll get to some of the downstream effects
and the hints that the court kind of threw out there.
But this ruling, in and of itself, pretty simple.
And most of the reactions are much ado about nothing.
It didn't do what a lot of people think it did.
First, it did not introduce permitless carry.
I've seen that on a couple of blogs
and in a couple of videos.
Your subscribers are not going to be happy with you.
That is not what happened.
At heart, this is really about two terms, may issue,
shall issue.
So what we're going to do is take it out
of concealed carry licenses and put it
into driver's licenses for a second.
When you go to get a driver's license in your state,
There are requirements.
You have to have a learner's permit for six months.
You need insurance.
Pass a written test and a driving test and a vision test
or whatever, right?
OK, so you go in, you do all of that,
and then the person at the DMV says, you know what?
You only live half a mile from work.
You don't really need a license.
That is may issue.
the way it actually works in your state is shall issue.
You meet all the requirements.
They have to give you the license.
It doesn't mean that there aren't requirements.
It just means they have to be objective, quantified,
measurable requirements.
And if they're met, you get the license.
What this ruling did is say that may issue licenses
for concealed carry aren't any good.
States will have to rewrite their laws.
States that have may-issue licenses
will have to rewrite their laws and make them shall-issue.
It doesn't mean that anybody who walks in and says, hey,
I want one gets one, though.
It means that whatever the requirements are,
they have to be objective, quantified, and if they're met,
license has to be given. The way I read this, if New York wanted to have quarterly
mental health exams as part of the requirements, I think they could. It's
not as dramatic as a lot of people are making it. It really does hinge on those
two terms. Most states have shall issue. This applies to, I want to say, five
states. So, not actually a huge deal, this particular ruling. However, from this you
can infer that the current court's interpretation of keep and bear is a
is a little bit broader than previous courts.
And because of that, there's a whole bunch
of other state or local level gun laws
that probably would not survive a challenge under this court.
But we already knew that.
If you've been watching this channel,
that's the first thing you're supposed to ask when you're
talking about gun legislation.
Will it survive this court?
And most won't.
So the permit thing actually isn't as huge of a deal
as people are making it out to be.
The states that have may issue can still
regulate concealed licenses.
They just have to do it in a more objective way.
That's it.
And then the other side to that is this court, things like
bans on high capacity magazines, probably wouldn't
make it through this court.
There's probably a lot of things that this court would
strike down that previous courts have allowed to stand.
Their interpretation of bear leads me to believe that things like rifle racks like back in the day
in pickup trucks, those may be coming back because their interpretation of bear seems to be pretty
broad, but I wouldn't panic or think that everybody in the state of New York is
now going to be armed. That's not going to be the outcome from this. So anyway,
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}