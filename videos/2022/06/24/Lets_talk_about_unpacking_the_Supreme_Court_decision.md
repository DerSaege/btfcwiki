---
title: Let's talk about unpacking the Supreme Court decision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JMrXA0oXy9g) |
| Published | 2022/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Supreme Court decision will fundamentally alter many states, reverting everything back to the state level.
- Republican states with laws on family planning will see devastating effects like increased teen pregnancy and poverty.
- Impacts will include loss of mothers, child marriage, and malicious prosecutions.
- Poor individuals won't have means to escape these laws by moving.
- Wealthy individuals will find ways to circumvent these laws.
- Predicts emergence of a discrete family planning industry catering to those seeking to avoid state restrictions.
- This decision targets poor people, not impacting the wealthy.
- Calls for unpacking the court by adding more justices as the only solution.
- Argues that current justices were appointed under false pretenses and need to be diluted.
- Lack of faith in the Supreme Court demands a change in its composition.
- Adding more justices is the way to address issues like marriage equality and other imminent threats.
- Diluting the votes of current justices is necessary to prevent worsening situations in the future.

### Quotes

- "It's time to unpack the court. That's your solution. It's really your only solution."
- "The solution is to add more justices."
- "Their only solution is to unpack the court, to get rid of these extremists that were put on there, right?"
- "The outcomes, the impacts, the effects of those decisions on your state, they're not good."
- "Adding more justices is the solution, to dilute the votes of these justices."

### Oneliner

Supreme Court decision will devastate states, targeting poor individuals, necessitating the addition of justices to dilute extreme views and prevent worsening situations.

### Audience

Voters, activists, lawmakers

### On-the-ground actions from transcript

- Advocate for adding more justices to the Supreme Court (suggested)
- Support efforts to unpack the court and dilute the votes of current justices (suggested)

### Whats missing in summary

Detailed explanation of the current Supreme Court decision and its potential ramifications.

### Tags

#SupremeCourt #Justice #PoliticalChange #StateLaws #Equality


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the Supreme Court decision.
And even though the court ruled on a number of cases over the last few days,
I have a feeling I don't have to specify which one I'm talking about.
Everybody's already going to know, because it's the one
that is going to fundamentally alter
a whole lot of states, because that's what happens due to this ruling.
Everything reverts back to the state level.
There are a number of states, particularly Republican states,
that have laws on the books already in case this happened.
So in those states, family planning will become a crime.
What else will happen in those states?
Mothers will be lost. Guaranteed. It's going to happen.
Teen pregnancy will increase, which means child marriage will increase.
Poverty will increase.
There will be malicious prosecutions.
It's going to be bad. It was always going to be bad.
Now if you live in one of those states,
what you're probably going to hear is, well, you need to move.
And that makes sense until you realize that the people who
don't have those power coupons, don't have that cash to circumvent these laws,
well, they also don't have the cash to move.
The people who this is going to impact the most
don't really have the means to remedy their own situation.
They can't just pack up and move.
Those who have the means to do that,
well, they also have the means to just get around the law.
I am certain that there will be a whole industry
that pops up within the security consulting industry
that caters to people who want to have discrete family planning decisions
outside of their state of residence.
This won't impact the rich.
This is another case of Republicans kicking down at poor people
to make themselves feel better.
This is going to be bad.
And not just for all the moral reasons.
I'm not talking about the whole bodily autonomy aspect of this.
I'm talking about the fact Republicans caught the car.
They're the dog that caught the car.
Now what are you going to do?
Because it's going to be your state that has these issues.
And that's just a fraction of them.
So what's the solution for the country?
The people who made this decision at the Supreme Court,
a whole lot of them lied to get their jobs.
They lied during those hearings.
They said things that weren't true.
And here we are because they were believed.
It's time to unpack the court.
That's your solution.
It's really your only solution.
The court was packed by people who said one thing and then did another,
who knew they were going to do the other thing when they said
what they said to Congress.
The solution is to add more justices.
This is the beginning.
They're going to go after other stuff that is going to impact you directly.
And I know somebody's going to say, well, that's going to undermine faith in the court.
Only one in four Americans has faith in the court.
It literally has never been this low.
You can't make it worse.
There is no faith in the Supreme Court.
And when you are talking about a court who is the highest court in the land,
the one that is supposed to make the interpretations of people's basic rights,
it's kind of important that they have at least some faith
from the people that they will be impacting.
It's that whole consent of the governed thing.
Right now they don't have it.
This is going to make it worse.
The solution is to add more justices.
That's the solution.
Understand, turning this into federal legislation,
sure, it'll help for a little bit on this particular issue,
but they've made it clear they're going after marriage equality.
They're going after a whole lot of other stuff.
Their only solution is to unpack the court,
to get rid of these extremists that were put on there, right?
But you can't do it that way.
You can't remove them.
So the solution is to add more.
You add more justices, you dilute their votes.
That's the solution.
And it's the only solution.
You can argue about whatever you want,
but that's how it's going to have to happen.
And if I was a member of the Republican Party,
I'd kind of be hoping that that's what the Democratic Party does.
Because as these decisions come down that sound so good in your rhetoric,
and they really motivate your base,
the outcomes, the impacts, the effects of those decisions on your state,
they're not good.
The solution is to add more justices, to unpack the court,
to dilute the votes of these justices.
And it's really the only solution.
Everything else is going to be a band-aid.
These people who are appointed for life,
they're going to have to dilute the votes.
And if they don't, it is going to get worse and worse and worse.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}