---
title: Let's talk about Day 5 of the hearings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-j4P8-NBUQs) |
| Published | 2022/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Day five of the hearings focused on Trump's pressure campaign to involve the Department of Justice in his baseless claims.
- Testimony revealed Trump's consideration of firing the acting Attorney General to install Jeffrey Clark in an effort to overturn the election.
- High-profile resignations were threatened if the DOJ was misused in this manner, leading Trump to back off.
- The committee's testimony outlines a potentially indictable offense, showcasing a strong case against Trump and his associates.
- Law enforcement activities, including searches and subpoenas, are intensifying beyond the committee's actions.
- Federal law enforcement, not just the committee, is actively pursuing investigations into the fake elector scheme.
- Governor Kemp is expected to provide testimony regarding Trump's election influence attempts in Georgia.
- The Department of Justice appears keen on investigating fake electors and is aggressively pursuing leads.
- Events unfolding in the lead-up to the midterms may significantly impact election outcomes as more information comes to light.
- A list of pardon requests is set to be released, adding to the ongoing developments in the investigation.

### Quotes

- "The committee is laying out a case. I mean they're laying out what could be an indictment."
- "There's going to be a lot of events that might alter the outcome of elections."
- "The Department of Justice appears very interested in the fake electors side of things."
- "Law enforcement activity is picking up. It's gaining momentum."
- "There's certainly a hard time overcoming this."

### Oneliner

Day five of hearings reveals Trump's pressure campaign involving DOJ; law enforcement intensifies investigations, potentially altering election outcomes.

### Audience

Investigators, Activists, Voters

### On-the-ground actions from transcript

- Contact local representatives to advocate for transparent investigation into election influence (implied).
- Join community groups discussing election integrity and involvement (exemplified).
- Attend public hearings related to election interference investigations (implied).

### Whats missing in summary

Insights on the potential impact of ongoing investigations and the importance of staying informed for upcoming election events.

### Tags

#Trump #DOJ #ElectionInfluence #Investigations #LawEnforcement #CommunityPolicing


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about day five of the hearings.
And the committee, I mean, they brought the goods when it came to Trump's pressure
campaign to get the Department of Justice to intervene and echo his baseless claims.
Centered primarily on a meeting that occurred in the Oval Office in December 2020, Trump
was contemplating firing the acting Attorney General, according to all of the testimony.
And that testimony suggests that he wanted to install Jeffrey Clark as the new acting
Attorney General.
So Clarke could then use the power of federal law enforcement to help persuade states to
play ball and to assist in the overturning of the election, for lack of a better word.
Provide cover for Trump's claims.
Say yeah, yeah, that's true.
announced the investigation. Very similar to the Zelensky thing, right? The perfect
phone call. Okay, so this was going on and basically he was told that a whole
bunch of high-profile people would resign if the Department of Justice was
used in this manner. And it appears that he backed off of it. We heard a little bit
about a theory involving Italian satellites. Not a thing. And we also heard
about how there might have been a push to have federal law enforcement seize
voting machines. Again, as a method of providing cover, backing up the claims
that didn't have any actual evidence. So you create the drama and then it seems
as though the claims are true. That appears to be the move. The testimony
that was provided, pretty convincing. I think that Trump and crew are gonna have
hard time overcoming this. The committee is laying out a case. I mean they're laying out what could
be an indictment. At this point that is definitely their plan. They're sticking to the template and
doing it well. Day five occurred with the backdrop of Jeffrey Clark mentioned earlier
here, having his home searched by the feds related to the apparent attempts.
There were also subpoenas and searches conducted all over the place related to the fake elector
scheme.
On top of this, Governor Kemp will be providing testimony to Georgia's probe into Trump's
activities in attempting to influence the election there. Overall, we're
starting to see law enforcement activity pick up. It's gaining momentum much, and
it's beyond the committee. This isn't the committee doing this. This is federal
law enforcement. For a long time, we've seen the committee's actions. We've
We've gotten little leaks from them.
We haven't heard anything from DOJ.
Now a lot of their activities are becoming very public.
And it certainly appears that the Department of Justice is very interested in the fake
electors side of things.
And that they seem to be pursuing that avenue pretty aggressively.
And we'll probably hear a lot more about that in the coming days and weeks.
And then we have to wait and find out what Georgia's probe uncovers.
It certainly appears that with the way this is shaping up throughout the run up to the
midterms, there's going to be a lot of events that might alter the outcome of elections.
as people find out more and more information
about their chosen candidate.
There was also a list about who requested a pardon.
And that video will be coming out a little bit later.
So that's just a brief overview.
And remember to fit all of this in to what has already
been demonstrated, the evidence that they've presented before,
as far as Trump being aware that the claims were false,
Trump being aware that the activities would violate
federal law, and now they're demonstrating a pressure
campaign against the Department of Justice.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}