---
title: Let's talk about polar bear, good news, and bad news....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=953CcvaIWq0) |
| Published | 2022/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a message about polar bears in Greenland surviving in a location they're not supposed to be.
- Conducts quick research to understand the situation and implications of the polar bears' presence in Greenland.
- Discovers that the Greenland polar bear population is genetically diverse and geographically distinct from others.
- Notes that these bears are smaller, produce fewer offspring, and can survive with 100 days of ice sheets compared to 180 days needed elsewhere for hunting seals.
- Acknowledges the potential adaptability of these bears to climate change due to their unique circumstances.
- Quotes a researcher who tracked the Greenland bears for nine years, pointing out that the situation is unique and not indicative of a solution for polar bears across the Arctic.
- Expresses skepticism towards viewing the Greenland polar bears as a symbol of hope for the entire species, stressing the need for comprehensive climate change action.
- Emphasizes the importance of developing a comprehensive plan and taking significant steps to mitigate climate change effects.
- Concludes that until concrete actions are taken, positive news regarding climate change will likely be limited and not without challenges.


### Quotes

- "These polar bears are adapted to living in an environment that looks like the future."
- "Greenland is unique. We project large declines of polar bears across the Arctic and this study does not change that very important message."
- "Not that polar bears everywhere are going to be okay. Just that this particular group might be able to make it through it."

### Oneliner

Beau delves into the unique case of Greenland polar bears to caution against viewing their survival as a universal solution for polar bears facing climate change, stressing the need for comprehensive action to mitigate the impacts.

### Audience

Climate change activists

### On-the-ground actions from transcript

- Develop and support comprehensive plans to mitigate climate change impacts (implied)
- Advocate for urgent climate action and policy changes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the unique situation of Greenland polar bears and underscores the broader implications for polar bear populations facing climate change challenges.

### Tags

#ClimateChange #PolarBears #Greenland #GeneticDiversity #Mitigation


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about ice sheets
and polar bears in Greenland
and having to do some quick research
and knowing when to defer.
I got a message and the message was,
hey, does the polar bears that were found in Greenland
mean that polar bears are gonna be okay
and they'll make it through climate change?
I have no idea what this message is about.
This is the first I had heard of this.
So I had to do some quick research.
To catch everybody up, a group of polar bears
was found in Greenland.
They're kind of not supposed to be here, OK?
And because they're surviving in a location that is not
place they normally live, it is kind of being heralded as evidence that maybe
the polar bears will be all right and make it through climate change.
So I did some quick research and found out that this particular
group of polar bears in Greenland is a
remote population that is genetically diverse,
it's separate from all the other ones and they are geographically separate. So they're genetically
distinct, they're not exactly the same, and they're living in a different place. The bears
themselves are apparently smaller and they produce fewer offspring. The reason people
got really excited was that the bears in Greenland can survive with a hundred
days of ice sheets. Now this is when they go and hunt seals. Everywhere else in
the world they need almost double that. They need about 180 days. The bears in
Greenland use freshwater icebergs as kind of like a fill-in. So all of that
kind of sounds hopeful. It seems like the bears would be able to adapt to the
changing climate. But that's me saying that. I understand that there's a genetic
difference. I understand that they had time to adapt. Maybe they wouldn't have
that time. So I started looking for quotes. This is from the person who tracked
the Greenland bears for nine years. These polar bears are adapted to living in an
environment that looks like the future. Again, sounds like good news. But most
bears in the Arctic don't have glacial ice. They don't have access to this, so
it can't be taken out of context like somehow this is a is like a life raft
for polar bears around the Arctic. It's not. Greenland is unique. We project large
declines of polar bears across the Arctic and this study does not change
that very important message. What this study does is show that we find this
isolated group living in this unique place. We're looking at where the Arctic
polar bears can as a species hang on, where they might persist. That doesn't
sound like good news. If anything, this sounds like a Noah's Ark for the species.
Not that polar bears everywhere are going to be okay. Just that this particular
group might be able to make it through it. So, no. I'm going to go with the
person that spent nine years tracking this exact group of bears and say that
this isn't the great news that it might seem to be on the surface. So while there
is a a safety net for the species it doesn't it doesn't mean that the polar
bears around the Arctic will adapt and do the exact same thing. In some cases
they won't even have the access to the situation, to the environment that
allowed those polar bears in Greenland to make it. There's not going to be a lot
of good news when it comes to climate change until we actually get our act
together and come up with a comprehensive plan and really start
mitigating. And eventually the good news will be things like rather than losing
70% of this species will only lose 30%. Even when the good news starts to
come it's probably not going to be great news. Anyway, it's just a thought. Y'all
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}