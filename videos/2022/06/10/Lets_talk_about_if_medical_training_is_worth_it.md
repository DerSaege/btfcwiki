---
title: Let's talk about if medical training is worth it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2jlT505wB3E) |
| Published | 2022/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of staying positive and whether it matters.
- Mentions the significance of doing what you can, where you can, and for as long as you can.
- Expresses frustration over delays in resolving events that could have led to different outcomes if aid had been applied sooner.
- Emphasizes the role of providing aid to buy time for professional medical help to arrive.
- Recounts stories illustrating the impact of immediate aid in critical situations.
- Advocates for not giving up the fight, even when circumstances seem dire.
- Encourages training and preparedness for potentially worsening situations.
- Urges to keep trying because winning even once makes all the efforts worth it.

### Quotes

- "It absolutely matters."
- "You're buying minutes."
- "You never give up the fight."
- "Move through it because somebody, one of them will surprise you."
- "It's worth trying every other time if you just win once."

### Oneliner

Beau talks about staying positive, providing immediate aid, and never giving up the fight, stressing that every effort counts, even if the odds seem against you.

### Audience

Community members 

### On-the-ground actions from transcript

- Provide immediate aid in emergencies (exemplified)
- Seek training in first aid and emergency response (suggested)

### Whats missing in summary

The emotional impact and depth of stories shared by Beau.

### Tags

#StayingPositive #AidMatters #EmergencyResponse #NeverGiveUp #Training


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about staying positive.
We're going to talk about whether or not it matters at all.
We're going to talk about doing what you can, when you can,
where you can for as long as you can,
and a whole bunch of other stuff.
Because in the comments of that video,
familiarizing the teachers with that equipment,
there were a lot of people asking if it even matters,
because that video went out the same day
the doc gave the testimony about how bad those injuries really
are.
All right?
It absolutely matters.
It absolutely matters.
When you look at that, I want to say 19 gone, 17 wounded.
If aid had been applied sooner, I
have a feeling those numbers would be very different.
That's what my gut tells me.
It's one of the reasons the delay in resolution
to that event is so infuriating to me,
because I don't think those numbers had to be that bad.
I think there could have been a lot more people with injuries
rather than just gone.
One of the things that you have to remember,
especially when you hear about how bad the damage is,
you're not trying to fix the damage.
We're not out in the field.
We're not out in the field.
Despite what teachers are having to learn, we're not in combat.
You're not trying to fix them.
You're just trying to buy them 20, 30 minutes
under normal circumstances so they can get somewhere
with real medical infrastructure.
Once those medics show up and start pumping them
full of fluids, the whole thing changes.
When I signed up to do a WUFR, a Wilderness First Responder,
it's like the MacGyver's First Aid course.
When I signed up for that years ago, I really did.
I thought by the time I was done with this course,
you could be attacked by a grizzly bear, shot,
and lit on fire, and I would be able to fix you
with some Q-tips and some duct tape.
By the time I was done with the class,
I realized I could probably keep you alive
till I got you to somebody who knew what they were doing.
That's all you got to do.
That's the goal.
You're not actually trying to solve any of this.
The professionals, the trauma surgeons,
they're going to do that.
You just got to make sure they can get there.
You're buying minutes.
Now, I have two quick stories that
are going to illustrate something.
One is a guy goes through a windshield,
hits the concrete at 70 miles an hour.
Neck.
This person's done.
They're done.
Medic thought otherwise.
Stubborn.
Worked on them.
Brought them back.
They were told they'd never walk again.
They're walking and talking today.
Totally normal.
Well, not normal, friend of mine.
They're as normal as they were before that happened.
But the injury, I can't believe that they're still alive.
I can't believe that they're still alive.
Another one, me and a security chief
are on the ground floor outside of a very, very tall building.
Somebody falls.
When they hit, I know this person
is going in the forever box.
They're done.
Security chief thinks otherwise.
Runs over, does everything they can.
Pumps on them everything.
Didn't work that time.
It's not always going to work, but it's
going to work some of the time, which means it's worth doing.
You don't have to win every time.
You can't.
But if you win once, it's worth it.
It's worth trying every other time if you just win once.
Don't succumb to it won't do any good.
Don't succumb to the idea that it's just pointless.
It's never pointless.
It's never pointless.
You never give up the fight.
That training, getting training like that,
it's going to matter because the wins
are probably going to get worse.
So remember to stay in the fight,
even if you think it's pointless,
even if the information that you've been told
says it doesn't matter, you're not going to do any good.
The information's a lie.
Move through it because somebody, one of them
will surprise you.
And that's all that matters.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}