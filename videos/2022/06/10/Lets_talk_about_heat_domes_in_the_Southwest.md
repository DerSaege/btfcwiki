---
title: Let's talk about heat domes in the Southwest....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RHXMbTH3jYs) |
| Published | 2022/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Alerting about an upcoming heat dome over the southwestern United States impacting around 25 million people.
- Temperatures expected to be 20 degrees above normal, with many places hitting triple digits, including major cities like Austin, San Antonio, Phoenix, and Vegas.
- Warning about the dangers of underestimating extreme heat, citing the loss of 231 lives in British Columbia due to a heat dome last year.
- Advising people in the affected areas to stay hydrated by drinking water and utilizing water's cooling effect, like using wet rags or cold compresses.
- Suggesting limiting outdoor activities, wearing light and loose clothing, eating light, and checking on neighbors who may struggle in the extreme heat.
- Emphasizing the need to adapt and mitigate the impacts of climate change as these extreme heat events become more frequent.
- Cautioning that even small temperature increases can have significant consequences in already extreme climates like in Vegas.
- Urging people in the southwestern United States to take the heat dome seriously and not underestimate its severity.

### Quotes

- "Hydrate, hydrate, hydrate. Drink a lot of water. Not soda, water."
- "Don't underestimate the heat. It's a big deal."
- "Wear light baggy and light colored and lightweight clothing."
- "It causes a lot of loss when you're thinking about it."
- "Don't underestimate it. It's serious."

### Oneliner

Be prepared for an imminent heat dome impacting 25 million people in the southwestern US; hydrate, stay cool, and watch out for your neighbors in triple-digit temperatures.

### Audience

Residents in the southwestern United States

### On-the-ground actions from transcript

- Stay hydrated by drinking water and using cooling techniques (suggested)
- Limit outdoor activities and wear light clothing (suggested)
- Eat light and stay cool in extreme heat (suggested)
- Regularly checking on vulnerable neighbors (suggested)
- Adapt and mitigate climate change impacts (suggested)

### Whats missing in summary

The full transcript provides detailed tips on staying safe during extreme heat events and underlines the importance of not underestimating the severity of heat waves.

### Tags

#HeatDome #HeatWave #ClimateChange #CommunitySafety #Hydration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a heat wave.
Well, really, a heat dome.
And there's one that is going to happen
over the southwestern United States this weekend.
So be prepared for it.
It's probably already, like, super hot.
It's going to get hotter.
And this is going to impact around 25 million people
in the United States.
Temperatures are expected to be 20 degrees above normal
for this time of year.
A whole lot of places are going to be in triple digits.
Major cities in Texas, Austin, San Antonio, Dallas.
Then you have Phoenix, Vegas, all the way out to California.
It's a big area.
Temperatures are going to get really, really high.
This is something that tends to get underestimated a lot.
It's worth noting that British Columbia had a heat dome
last year.
And they lost 231 people on June 29 alone, one day.
Don't underestimate the heat.
It's a big deal.
OK, so what can you do if you are down there?
First, hydrate.
Hydrate, hydrate, hydrate.
Drink a lot of water.
Not soda, water.
Drink water.
And when you're talking about water and thinking about water,
remember that water is cooling.
So lakes, pond, kiddie pool, tub, shower, any of this stuff
will help cool you off.
Water has a cooling effect.
You can also use wet rags or cold compresses.
You obviously want to limit your outdoor activity
as much as possible.
If you're in the house, been inside all day,
feel like you have to go do something,
go check on your neighbor, especially those who may not
necessarily be totally up to speed and capable of handling
it on their own.
Handling it on their own, definitely check on them.
Wear light baggy and light colored and lightweight clothing.
That will help a lot as well.
And then also eat light.
Don't eat a whole bunch of food.
This type of thing is going to happen more and more frequently.
I actually feel like I've done a video talking
about this exact subject before.
Last year.
It's going to happen more and more.
We are suffering the impacts of climate change.
And this type of stuff will continue to happen.
So we're going to have to learn to mitigate and adapt.
These things will help when this type of thing occurs.
Don't underestimate it.
It's serious.
It really does.
It causes a lot of loss when you're
thinking about it, especially somewhere like Vegas,
where it's really dry.
You may not think it's a huge difference when you're just
talking about four or five degrees.
It is.
It is, because you're already pushing the extremes.
So when it goes up from there, you can have real problems.
If you're in the southwestern United States,
don't underestimate this.
So y'all stay cool this weekend.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}