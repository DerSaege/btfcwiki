---
title: Let's talk about how California's water is your problem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WzeLrMKDdbY) |
| Published | 2022/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- California is experiencing a severe drought, with Governor Newsom urging urban residents to reduce water consumption by 15%, yet water usage increased by 19% in March and 18% in April.
- Some areas are installing flow restrictors to conserve water due to the worsening drought, impacting private wells in rural areas.
- The lack of water in California has led to around 400,000 acres of farmland not being planted, affecting the production of a significant portion of veggies, fruits, and nuts in the US.
- The effects of climate change are already evident, with negative impacts being felt in the present moment, despite many choosing to ignore the reality.
- It is emphasized that moving away from dirty energy and prioritizing environmental sustainability is not just a moral choice but also an economic imperative.
- The drought in California, exacerbated by climate change, underscores the urgent need for innovation, transitioning away from dirty energy, and cutting emissions.
- Beau stresses the necessity of making choices now that prioritize sustainable practices and infrastructure development to mitigate the worsening impacts of climate change.
- Other states are also facing challenges related to water scarcity, with some like Colorado resorting to cloud seeding as a potential solution.
- The push for continued reliance on fossil fuels and drilling as an answer to economic problems is debunked, with Beau advocating for urgent innovation and transitioning to cleaner energy sources.
- Ultimately, Beau asserts that the current climate crisis demands immediate action in transforming the country's infrastructure to combat the escalating effects of climate change.

### Quotes

- "It's here. It's happening now. We're experiencing the negative impacts right now, this minute."
- "We have to innovate. We don't have a choice."
- "You're experiencing it now. You're paying for it now."
- "We have to alter the infrastructure of the entire country."

### Oneliner

Beau warns of the present impacts of climate change, urging urgent innovation and infrastructure changes to combat its effects, stressing the economic necessity of transitioning away from dirty energy.

### Audience

Environmental advocates, policymakers

### On-the-ground actions from transcript

- Transition to sustainable energy sources to combat climate change (implied)
- Support innovation and infrastructure development for environmental sustainability (implied)

### What's missing in summary

The full transcript provides a detailed analysis of the economic implications and urgent need for action in response to the current impacts of climate change. Watching the full video can offer a comprehensive understanding of Beau's arguments and insights.

### Tags

#ClimateChange #Drought #EnvironmentalSustainability #Innovation #EconomicImpact


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to,
we're gonna talk about how climate change is here.
It is absolutely here.
And how it's already impacting your wallet.
So I think most people know about California.
They're having a drought and it's a bad one.
So much so that Governor Newsom basically begged people
who lived in urban areas
to cut their water consumption by 15%.
It's worth noting that in March, it went up 19%.
And I wanna say April was 18%.
Because of this,
some areas are actually installing flow restrictors
to cut down on water usage.
It's no longer a threat, it's actually happening.
Private wells in rural areas are going dry.
And a lot of that probably has to do
with commercial pumping of water.
But what does this have to do with you
if you live outside of California?
Well, about a third of the veggies
that are grown and harvested in the US come from California.
About two thirds of the fruits and nuts.
Because of the lack of water,
around 400,000 acres of farmland,
well, it didn't get planted
because they didn't have water
to actually produce the crops.
What do you think that's gonna do to prices where you live?
It's here.
It's here.
When we talk about climate change,
because we have been warned about it for so long,
we talk about it in the future tense.
We shouldn't be.
It's here, it's happening now.
We're experiencing the negative impacts
right now, this minute.
But we want to ignore it.
Large portions of this country
just pretend it's not happening.
If you wanna know why we have to move away
from dirty energy, this is why.
It is in your economic interest to do so.
If you wanna know why we have to maintain
a better relationship with the environment,
this is why. It's in your economic interest to do so.
This is the beginning.
Yeah, it's a drought,
and it's a drought that's made a whole lot worse
because of climate change.
That's not really something that's debatable.
And you still have people pushing the idea
that the answer to climate change
and the idea that the answer to our economic woes
is to drill, baby, drill.
No, it's not.
It's gonna make it worse.
We have to innovate.
We don't have a choice.
This will only get worse.
It will spread.
Other states are doing just as poorly.
I think Colorado's actually playing
with cloud seeding now.
We have to innovate.
We have to get away from dirty energy.
We have to cut emissions.
We do not have a choice.
You can have that gas guzzler,
or you can have fruit.
This is a choice that we have to make,
and it's innovations that have to come now,
and we have to start building the infrastructure for it now.
We don't have a choice.
It doesn't matter what you're talking head on Fox News says.
It doesn't matter what the politician
who is in the pocket of big oil says.
You're experiencing it now.
You're paying for it now.
If you want it to stop, there's really only one way.
We have to alter the infrastructure of the entire country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}