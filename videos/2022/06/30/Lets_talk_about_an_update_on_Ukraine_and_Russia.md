---
title: Let's talk about an update on Ukraine and Russia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=y5Yy7SWkUSM) |
| Published | 2022/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an update on the situation in Ukraine, which is unfolding as previously forecasted.
- Describing how the conflict in Ukraine has devolved into a protracted situation.
- Mentioning the commitment of the West, especially the United States, to support Ukraine with supplies, including a missile defense system.
- Noting that Ukrainian civilians are being targeted by Russians hitting shopping malls with rockets.
- Revealing that Ukrainian special forces are conducting operations inside Russian territory, opening the door for potential guerrilla engagement.
- Pointing out that sanctions have led to Russia's first major default and economic challenges.
- Criticizing Russia's economic data, suggesting it is manipulated to appear better than reality.
- Mentioning unemployment statistics in Russia may be artificially low due to pressure on businesses to retain unnecessary employees.
- Predicting a significant economic contraction for Russia based on outside expert analysis.
- Comparing Russia's economic situation to the mishandling of public health issues, where delayed consequences become more severe over time.
- Expressing skepticism about the war in Ukraine ending by the end of the year, as suggested by Zelensky.
- Speculating that the conflict may continue until a breakthrough occurs either in the supply chain for Ukraine or the Russian economy.

### Quotes

- "Lines are gonna move back and forth. This is going to continue for a while."
- "It's protracted, it's ingrained, and it's gonna keep going for a while."
- "Russia is voluntarily engaging in that same practice. They're keeping the numbers at levels that look good on paper, but they're doing it through capital controls and financial repression that will eventually show themselves."
- "I am skeptical of that happening. It could, but in most scenarios in which that would occur, it would be pretty bad for the Ukrainian side."
- "This is probably going to go on for a while now."

### Oneliner

Beau provides an update on the protracted conflict in Ukraine, skeptical of a quick resolution due to economic pressures and strategic stalemate.

### Audience

Analysts, policymakers, activists

### On-the-ground actions from transcript

- Monitor the situation in Ukraine closely and advocate for diplomatic resolutions (implied).
- Stay informed about the conflict's economic implications and support initiatives that aid affected populations (implied).

### What's missing in summary

Insights on the humanitarian impact of the conflict and ways to support Ukrainian civilians amidst the ongoing crisis.

### Tags

#Ukraine #Conflict #Economy #Sanctions #Geopolitics


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to provide a little update on Ukraine and
what's happening there.
Most of what is occurring is what we kind of laid out as something that can happen.
There haven't been a lot of changes.
During the initial forecast, we talked about the worst case scenario, and
that was that it devolved into a protracted conflict.
That's where we're at.
That's what's happened.
The West, and the United States in particular,
is very committed to keeping up supplies for Ukraine.
One of the big things that should be coming their way soon
is a distributed missile defense system.
This is important for Ukrainian civilians because Russians have taken to
hitting shopping malls with rockets.
The Ukrainian special forces, they have admitted now
that they are running operations inside of Russian territory.
We talked about that in the beginning as well.
And we said it was likely, and then a whole bunch of things started happening
that, I mean, we all kind of knew what was going on, but
there wasn't an official statement.
That statement has been made.
That in and of itself doesn't change a lot,
with the exception of it now opening the door for
guerrillas to engage in the same type of activity within Russian territory.
They will probably take that as a green light to do that.
Now, the sanctions have caused Russia's first real big default.
At the same time, it's kind of symbolic because even if Russia wanted to make
the payments, they couldn't because they've been cut out of so
many economic systems.
Russia is doing a really good job of cooking the books economically.
If you look at recent news, you'll find out that they have,
I want to say, 0.1% inflation.
Yeah, sure, I totally believe that.
At a time when the entire world is dealing with inflation,
Russia under sanctions is not.
Russia under sanctions is not.
Unemployment is going down according to official numbers.
This is probably linked to the government putting pressure
on business to keep people employed that they don't need.
There are a lot of examples of this.
I want to say it was McDonald's.
It might have been some other fast food chain.
But when the big company kind of worked its way out of Russia,
it was sold to a Russian-based company.
But as part of the contract, they had to keep everybody employed for
two years, whether they needed them or not.
These economic fixes to make Russia look good on paper so
they can report this over the news, I mean, that's all fine and good.
Good propaganda and all of that, but it doesn't reflect the reality.
If you look at the long-term pictures for
the Russian economy, outside experts that is based outside of Russia
will say that their economy will shrink somewhere between 8 and 30% this year.
That's gonna be hard to hide with a press release and creative accounting.
This is one of those things a lot like the response to the public health issue.
The longer the numbers get cooked,
the worse it is when it finally starts to show itself.
In the US, we never really shut down the way we should have.
And because of that, there was an economic lag where part of the people were staying
home, part of them were going out, it kept doors open, but the economy was still
dragging, and it kind of led to the situation we're in now,
rather than just dealing with it at the time, right?
Russia is voluntarily engaging in that same practice.
They're keeping the numbers at levels that look good on paper,
but they're doing it through capital controls and
financial repression that will eventually show themselves.
Now, the big question, how long is this gonna last?
A while.
A while.
Lines are gonna move back and forth.
This is going to continue for a while.
The clearest example of how long you can expect this to last
is that Zelensky said the goal, the goal, mind you,
is for the war to be over by the end of the year.
You know, troops home for Christmas and all that.
I am skeptical of that happening.
It could, but in most scenarios in which that would occur,
it would be pretty bad for the Ukrainian side.
I don't really see that as likely.
This is probably going to go on for a while now.
And at this point, it is a slugfest until somebody,
some side, either suffers a break in which something happens
with the supply chain for the Ukrainian side,
or the Russian economy actually shows itself.
It could go that way, and that could cause tides to turn
and things to kind of free up.
Or something unique happening on the ground,
a major strategic or tactical shift
that causes a domino effect.
Outside of that, this is just gonna go on a while.
It is going to continue to go on,
which means the economic pressure campaign will continue,
which means Europe will be still struggling
with getting energy resources,
which means the West will continue supplying weapons.
It's just, it's there.
It's protracted, it's ingrained,
and it's gonna keep going for a while.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}