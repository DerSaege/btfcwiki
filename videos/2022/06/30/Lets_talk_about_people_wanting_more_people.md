---
title: Let's talk about people wanting more people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lHGOO4dXUvg) |
| Published | 2022/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Various powerful figures are expressing concerns over declining birth rates and population collapse, including Elon Musk, politicians, and wealthy individuals.
- Studies show reasons why people are choosing not to have kids, including medical issues, financial concerns, lack of a partner, and global issues like climate change.
- The top reasons women give for terminating pregnancies are related to work, education, and financial constraints.
- There is a disconnect between those who created the system and those now complaining about its effects.
- Beau challenges the powerful individuals to take responsibility for the system they have created and make changes to address the issues causing declining birth rates.
- The current system prioritizes economic growth and consumption over addressing fundamental issues like poverty and sustainability.
- Beau calls out those with influence and power, including those in elected office, to stop perpetuating systems that keep people in poverty and contribute to declining birth rates.

### Quotes

- "The people who created the system are complaining about the effects of the system."
- "Y'all built this. Y'all put people in this situation. You don't get to whine about it now. Fix it."
- "The system that we have is broke. It's broke, and it's broke so bad, it is showing itself in a whole bunch of different ways."

### Oneliner

Powerful figures express concerns over declining birth rates, but fail to address systemic issues causing people to choose not to have kids. It's time to take responsibility and make meaningful changes to the broken system.

### Audience
Policy Makers, Influential Individuals

### On-the-ground actions from transcript

- Advocate for policies that support families and address the root causes leading people to choose not to have children (suggested).
- Support initiatives that aim to reduce financial barriers to starting a family (implied).
- Challenge elected officials to prioritize addressing poverty and promoting sustainable living conditions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the factors contributing to declining birth rates and challenges powerful individuals to take action to address these issues effectively.

### Tags

#DecliningBirthRates #SystemicIssues #PopulationCollapse #PolicyChange #WealthDisparity


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about why people aren't having kids.
There have been a lot of thought leaders lately,
a lot of very powerful people complaining about how few kids are being born.
And these range from conservative thought leaders to politicians
to some of the richest people on the planet.
And they're all talking about this idea of a population collapse.
You know, Elon Musk is a big driver behind this.
He's worried we're not going to have enough people to go to Mars.
These are the people who shape the state of the world.
These are some of the most powerful people in the world talking about this.
These are people who can truly alter the fabric of the way we live.
Do you think that they ever looked at why people aren't having kids?
There's lots of studies on it.
There's one in 2021 that said 44% of people who didn't already have kids,
aged 18 to 49, said that it was not too likely or not likely at all
that they would ever have kids.
That number is up 7% from 2018.
What were the reasons they gave?
19% said medical.
Now, I find it hard to believe that one in five
has a medical issue that would preclude them from having a kid.
So I think cost might be in there too, but that's a guess.
17% flat out said financial.
15% said no partner.
10% said the state of the world.
5% said climate change.
Seems like those people who are really powerful and super concerned about this,
maybe they should listen to this.
I'm just spitballing here.
If you look at the reasons that women give for terminating a pregnancy,
what are those numbers?
74% said it would interfere with work or education.
73% said they could not afford one right now.
Money. Both of those are money.
74% said it would interfere with work, education,
or taking care of another dependent.
But given 73% said that it was, they just couldn't afford it,
that means that we're only talking about 1% that were deterred
by having to take care of another dependent.
The numbers overlap there.
So the people who created the system are complaining about the effects of the system.
Y'all still have the power to change it.
Perhaps it was a really bad idea to depress wages,
to keep people right on the brink of poverty,
if you wanted to have more population growth.
But let's be real.
Why do you really want more population growth?
Because you need more consumers, right?
That's what it's really about,
because that's the only way you can sustain the growth in your stock portfolio.
It's the only way you can beat LY.
The system that we have is broke.
It's broke, and it's broke so bad,
it is showing itself in a whole bunch of different ways.
This is just one of the impacts.
This is just one of the effects from the system
that those who are complaining about the effects created.
Y'all built this.
Y'all put people in this situation.
You don't get to whine about it now.
Fix it.
Y'all have the money.
Y'all have the influence.
Y'all have the power.
Some of y'all are in elected office.
Stop trying to keep people on the brink of poverty.
And I'm willing to bet there'd be more kids.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}