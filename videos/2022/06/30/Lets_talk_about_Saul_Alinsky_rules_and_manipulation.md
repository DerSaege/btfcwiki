---
title: Let's talk about Saul Alinsky, rules, and manipulation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N-Rh6M-mt1I) |
| Published | 2022/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains about manipulation and Saul Alinsky, mentioning Alinsky's Rules for Radicals and its influence on current political control.
- Mentions a newspaper clipping warning about "useful idiots" influenced by Alinsky's writings.
- Lists the eight levels of control from Alinsky's book, including healthcare, poverty, debt, gun control, welfare, education, religion, and class warfare.
- Notes the misinterpretation of Alinsky's work, which was actually about empowering the have-nots, opposite to the newspaper clipping's claims.
- Talks about the demonization of Alinsky's work by attaching false power structures to it, creating fear and propaganda.
- Describes how the term "useful idiot" is used in political jargon to refer to those unknowingly promoting a cause.
- Concludes by discussing the spread of propaganda through fear-mongering tactics and the manipulation of information.

### Quotes

- "It is presently happening at an alarming rate in the US."
- "This isn't Alinsky's work. It's just not."
- "In political jargon, a useful idiot is a derogatory term..."

### Oneliner

Beau explains the misinterpretation and manipulation of Saul Alinsky's Rules for Radicals to create fear and propaganda.

### Audience

Citizens, Activists, Educators

### On-the-ground actions from transcript

- Fact-check and challenge misinformation spread through fear tactics (suggested)
- Educate others on the actual intentions of political terms and figures (exemplified)

### Whats missing in summary

The full transcript provides an in-depth analysis of how fear and propaganda are used to manipulate public perception, particularly in politics.

### Tags

#Manipulation #SaulAlinsky #Propaganda #PoliticalEducation #Misinformation


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today, we are going to talk about manipulation
and a person named Saul Alinsky.
It's a name you might have heard.
And the rules that came along from him.
We're going to do this because the other day,
I posted a video to Twitter, tweeted out a video,
and let's talk about whatever the title
video was and somebody replied with let's talk about you and had a little thumb or a little finger
pointing down and it had an image and it appears to be a a newspaper clipping i don't know if it
actually is but that's what it looks like and the headline is beware the useful idiots i guess that's
me. I'm the useful idiot here. And the article, I guess, it says, recall that
Hillary did her college thesis on his writings and Obama writes about him in
his books. Saul Alinsky died about 43 years ago, but his writings influenced
those in political control over our nation today. And it goes on and it gives
some biographical information, and it mentions his book, Rules for Radicals.
Anyone out there think that this stuff is happening today in the US? All eight
rules are currently at play. There are eight levels of control that must be
obtained before you're able to create a social state. The first one is health
care, control health care, and you control the people. Poverty, increase
poverty level as high as possible. Anybody who's familiar with Alinsky's
already laughing, right? Goes on, debt, increase the debt to an unsustainable
level, gun control, welfare, education, religion, class warfare, so on so on so
on, right? The useful idiots have destroyed every nation in which they have
seize power and control.
It is presently happening at an alarming rate in the US."
That's wild.
That's wild, especially if you've ever read Alinsky,
because this is a giant plan for the haves
to control the have-nots, which is the exact opposite of what
Alinsky wrote about.
He wrote about ways for the have-nots
to obtain power for themselves so they can advocate for themselves.
It's literally the exact opposite.
This isn't Olenski's work.
It's just not.
His rules for radicals, it's stuff like, power isn't what you have, it's what they think
you have.
It's stuff like that.
I've read it.
To me, I didn't see it as amazing, to be honest.
then again it also really wasn't written for me. Most of this stuff that it talks
about seems like it might be more applicable to urban areas. Not a whole
lot of help for me, not something that really spoke to me. But worth reading, no
doubt. But here's the thing, why was this written? Why did somebody take Saul
Alinsky, rules for radicals, and then attach these power structures to it. It
goes back, I found references to this type of scapegoating of Alinsky going
all the way back to like Newt Gingrich days. It was a way to signal European
socialism. Olensky's from Chicago, I think. But that was what it was there for. It was
there to give the conservative base, Republicans, a boogeyman. Somebody to fear this name that
sounds scary, rules for radicals, and it was meant to, well, scare people. And then, once
Once they had them scared, they could feed them stuff that was literally the exact opposite
of what he wrote and use that to demonize whoever they attached to it.
So in essence, it was basically a form of propaganda that they could count on people
to share without really knowing the cause or the true intentions of it, because it just
sounded scary, right?
That habit is still very alive today, and not just because somebody posted this.
I would point out that this has been debunked for years.
But it's still very common to take a term, a name, a term, demonize it, and then attach
it to people the way they did in this article, this purported article.
They wrote college theses about it, it wasn't mentioned in their books.
This is this horrible stuff, but none of that is actually the person, right?
But it doesn't matter because you tie it together, you create that association, and
you create that propaganda, and then people will spread it without really knowing what
it is.
One last thing.
In political jargon, a useful idiot is a derogatory term for a person perceived as
propagandizing for a cause without fully comprehending the cause's goals and who
is cynically used by the cause's leaders.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}