---
title: Let's talk about Russian oil and American costs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L3YtOnpTYH8) |
| Published | 2022/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States has decided to stop buying Russian oil, which could have significant impacts both in Russia and in the US.
- Biden's potential plans regarding the situation are unknown, leaving uncertainty about the future.
- Rising gas prices are expected, leading to financial strain for many individuals.
- Suggestions are provided for individuals to cope with the increasing gas prices, such as carpooling, biking, or walking.
- Social and community responsibility are emphasized in times of crisis like this.
- Beau encourages people to plan trips efficiently, cut down on driving, and reduce overall demand to help lower gas prices.
- It is mentioned that releasing reserves may not be a viable solution due to heightened tensions.
- Cooperation and mutual support at the local level are vital to alleviate the effects of the situation.
- Beau stresses the importance of individuals taking action to reduce usage, decrease demand, and control costs.
- Looking towards environmentalists for ideas on cutting usage and reducing carbon footprint is recommended.

### Quotes

- "It's time to start thinking about your neighbor. It's time to start cooperating."
- "Y'all definitely start trying to figure out a way to work together."
- "Start looking at stuff geared towards environmentalists."

### Oneliner

The United States stopping Russian oil imports leads to rising gas prices, urging individuals to reduce usage and cooperate to control costs.

### Audience

Individuals, Communities

### On-the-ground actions from transcript

- Plan trips efficiently and cut down on unnecessary driving (implied)
- Offer carpooling opportunities to reduce overall usage and costs (implied)
- Cooperate with neighbors for mutual support and assistance in daily tasks (implied)

### Whats missing in summary

The full transcript provides a detailed roadmap for individuals and communities to navigate rising gas prices through cooperation and reduced usage.

### Tags

#RussianOil #GasPrices #Cooperation #CommunityResponsibility #ReduceUsage


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we're going to talk about Russian oil.
The United States has decided to stop buying it.
When this subject came up initially,
I expressed my concerns about it,
about this particular strategy.
Talked about how it was going to impact the average Russian
over there who has nothing to do with it.
It's also going to have pretty significant impacts here.
Now, at time of filming, I'm not aware of anything that Biden has up his sleeve.
I know that there was a meeting between oil execs from the US and OPEC, I want to say
last night, don't know what happened.
Maybe some kind of deal was reached, and that's why this announcement was made today, but
I don't know that.
Absent Biden having something up his sleeve, this is going to hurt in the US.
prices are going to soar. My entire life I have noticed a behavior and that
behavior is at normally a pretty good indicator of people's financial
security. If you are somebody who doesn't fill your tank up all the way when you
go to the gas station under normal circumstances, right now you need to
start thinking about carpooling, if that's a possibility, or walking, riding
a bike, because gas prices are going to go up. When you have to drive, make sure
you're doing everything at once. Plan your trips, plan your week, because it's
not just gas prices. When the gas prices go up, transportation cost goes up, cost
of other products goes up. This is incredibly likely and it's it's gonna
hurt. Now if you are somebody who under normal circumstances fills your tank up
all the way, first understand you may not be able to do that. That may change. If
If you're on the line of that behavior,
you may start putting in $30.
If you are somebody who can afford this,
and it isn't going to drastically alter
your behavior, it still needs to.
Don't drive as much.
Figure something out.
Plan your trips.
Do all of the same behaviors.
It's just not as pressing for you.
But if you do it, it lowers demand.
Lower demand means prices don't go up as much.
That's how you can help the people who can't fill their tank up every time they go.
This is a time where social responsibility, community responsibility is going to matter.
When you're going to the grocery store, see if somebody needs to go with you.
Carpooling in functions that you know everybody needs to do, see if anybody wants to do it
with you at the same time.
We're going to have to cut demand.
As far as what you can do as an individual person, it's lower usage and cut demand, which
means drive less.
out of way to help in that regard.
And I know, well I can't do it this way, I mean I know it's going to be annoying for
a lot of people.
For somebody like me, way out in the middle of nowhere, carpooling, it doesn't necessarily
make sense when you first think about it because somebody has to drive five miles to get to
you.
But from there, you're going to drive 20 miles into town.
Better one car than two.
Maybe start to share some of those expenses.
We're going to have to start thinking about each other at the local level to alleviate
this.
Because absent some deal, some move that nobody's aware of yet, at least that I'm not, this
This is going to hurt.
The only thing that I'm aware of that is an option is releasing from the strategic reserves,
which doesn't really seem like a great idea in a time of heightened tensions.
If that's going to happen, it's going to have to be done little by little.
And I don't think it's going to make that big of an impact.
So this is something that you need to be prepared for.
And you've got to start thinking about it now,
because gas prices will probably shoot up through the roof,
maybe by tonight.
I know people are saying, well, they're already through the roof.
No, they're not yet.
They're not yet.
They're going to go up more.
The reason the prices went up was speculation
of something like this.
Now that it has occurred, it's going to go up even more.
It's time to start thinking about your neighbor.
It's time to start cooperating.
It's time to start doing what you
can to reduce your usage and therefore decrease demand
and help control costs.
That's all you can do as an individual.
If you want more ideas, start looking at stuff
geared towards environmentalists.
They put out stuff all the time on how to cut your usage,
cut your carbon footprint, and have suggestions
on different ways you can do it.
It's the same thing.
So whatever they're saying will protect the environment
is also going to reduce usage, reduce demand, reduce costs.
you know starting in 2020 it's been a real rough couple of years doesn't look
like we're gonna get a reprieve anytime soon so y'all y'all definitely start
trying to figure out a way to work together you're gonna need to anyway
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}