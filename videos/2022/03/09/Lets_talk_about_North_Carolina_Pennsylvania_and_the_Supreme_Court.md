---
title: Let's talk about North Carolina, Pennsylvania, and the Supreme Court....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=k7hpnjr7-FU) |
| Published | 2022/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Three Supreme Court cases, two decided, one pending, involving North Carolina, Pennsylvania, and Wisconsin, all about redistricting for elections.
- Republican Party pushing for Supreme Court permission to redraw maps as they see fit.
- North Carolina case involved heavily gerrymandered maps rejected by state courts.
- Republicans argued that only state legislature should draw maps, not state courts.
- Acceptance of this doctrine could lead to extreme gerrymandering and less representation for voters.
- Similar situation in Pennsylvania with the governor vetoing maps drawn by state legislature.
- Wisconsin case likely to have a similar outcome as the Supreme Court ruled against Republicans in the other cases.
- Republican Party aiming to control state-level power to influence federal elections.
- Possibility of maps being drawn based on party affiliation rather than voter preference.
- Concerns about less responsive government if gerrymandering continues unchecked.

### Quotes

- "Republican Party pushing for Supreme Court permission to redraw maps as they see fit."
- "Acceptance of this doctrine could lead to extreme gerrymandering and less representation for voters."
- "Republican Party aiming to control state-level power to influence federal elections."

### Oneliner

Three Supreme Court cases on redistricting show Republican Party's push for extreme gerrymandering and control over elections.

### Audience

Voters, election watchdogs

### On-the-ground actions from transcript

- Watchdog groups: Monitor closely for further developments on redistricting cases (implied).

### Whats missing in summary

Detailed analysis on the potential long-term impacts of unchecked gerrymandering and lack of representation.

### Tags

#SupremeCourt #Redistricting #Gerrymandering #RepublicanParty #Elections


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about three Supreme Court
cases, two of which have already been decided.
And then the third one is kind of a duplicate
of the first two.
It's dealing with the same issues.
So we can kind of guess as to what the outcome is
going to be on that one.
The states involved are North Carolina, Pennsylvania,
and Wisconsin.
Wisconsin is the one that is not decided yet.
All three cases revolve around redistricting, drawing the maps for the elections.
In all three cases, the Republican Party is attempting to persuade the Supreme Court to
basically allow them to do whatever they want.
In North Carolina, they drew up some maps.
The Republicans drew up some maps and the state courts stepped in and were kind of like,
hey, these are even more gerrymandered than we've come to expect and kind of tossed them.
I didn't kind of toss them, they did.
The Republicans went to the Supreme Court arguing that the state legislature alone has
the power to draw these maps, and that the state courts shouldn't be involved in it.
They shouldn't get a say.
This is a fringe legal theory, is how it gets described.
If this doctrine was accepted, and this is how things worked, basically whoever was in
power at the time this doctrine was accepted, they would be able to redraw the maps as
as they saw fit.
And rather than voters choosing their representatives,
the representatives would quite literally
choose their voters based on demographic information.
It would lead to even more gerrymandering,
and it would lead to even less representation
because the maps would be drawn to make sure
that loyal voters of certain parties, whichever one's in power, would be central to each district.
So the results would be predetermined.
In Pennsylvania, kind of similar situation, but a little different.
In that case, the state legislature drew up some maps, the governor vetoed it, and then
the Supreme Court stepped in, the state Supreme Court stepped in and was like, okay, well,
do the maps and kind of use ones that were very similar to the previous ones.
Wisconsin, kind of a carbon copy of these cases.
The Supreme Court sided with the Democratic Party is how it works.
They ruled against the Republicans.
They didn't accept these theories and rejected hearing them.
So that's over.
We can expect the same thing to happen with the Wisconsin case.
So for the moment, it doesn't look like this theory is going to take hold.
However, there were a couple of dissenting opinions that expressed interest in it.
So it's something that people who are election watchdogs, probably something you should watch
because there's going to be more when it comes to this.
The Republican Party is very set on controlling things at a state level so they can alter
the outcome of federal elections.
That seems to be the strategy.
And if we're not careful, we're going to end up with maps that don't really give anybody
a say.
It will be determined by party affiliation.
and an even less responsive state and federal government.
Anyway, it's just a thought.
Y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}