---
title: Let's talk about why analysts are sure Russia won't win....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XeolyxyzuX4) |
| Published | 2022/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the concept of showing his work regarding the math behind a geopolitical situation.
- Mentions the importance of minimal resistance when occupying an area.
- References a 2003 Rand Corporation study that determined the number of troops needed to successfully occupy an area.
- States that one soldier per 50 civilians is the ratio required for successful occupation.
- Notes that Russia's current ratio in Ukraine is four troops per 50 civilians, significantly lower than what's needed.
- Estimates that Russia needs about 800,000 troops to occupy Ukraine, but realistically they don't have close to that number.
- Points out that even if Russia shipped more troops in, they still wouldn't have enough for the occupation.
- Suggests that Ukraine can win by engaging in minimal resistance over time.
- Expresses doubt in Putin's ability to hold Ukraine due to lack of troops and financial strain.
- Concludes that the math indicates Putin will lose unless significant changes occur.

### Quotes

- "If there are not 20 soldiers per 1000, the occupation fails."
- "The math says Putin will lose."
- "All Ukraine has to do is keep fighting and they win."

### Oneliner

Beau explains the math behind why Putin will likely lose in Ukraine due to insufficient troop numbers and resistance dynamics.

### Audience

Military analysts, geopolitical strategists

### On-the-ground actions from transcript

- Contact organizations supporting Ukrainian resistance (implied)
- Support humanitarian efforts in Ukraine (implied)
- Stay informed on the situation in Ukraine and advocate for diplomatic solutions (implied)

### Whats missing in summary

Detailed analysis of the potential long-term impacts of the ongoing conflict in Ukraine.

### Tags

#Geopolitics #Mathematics #Russia #Ukraine #Occupation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about math and me showing my work.
I've had a request to show my work on something.
And it's funny because I went back and looked and I have talked about this over and over
and over again on this channel and I never explained it.
I've used this math on this channel before but never actually broke it down.
So we're going to go over it today and it will explain why there are some people who
are very comfortable making quote bold proclamations.
And the message that I got, it starts off with a giant list of names and those names
that I recognized were defense analysts.
These people have all said that Russia cannot win.
And there's another list of names and my name is on this list.
And it says these people have said that Russia can take it but can't hold it.
How can you be so sure?
Asking me to show my work.
Okay so we have talked about this concept in relationship to the wild theory about the
UN coming here, the idea that Trump was going to use the military to maintain control.
We've talked about it in relationship to Iraq and Afghanistan.
It's come up before but I've never given the math.
In 2003, Rand Corporation did a study and it attempted to determine the number of troops
you need to successfully occupy an area with minimal resistance.
And I want you to keep that part in mind.
Minimal.
I don't have a way to quantify this mathematically but just looking at Ukraine, I don't think
that's going to be minimal.
If you're not familiar with Rand Corporation, if you ever want to know anything about the
military, they've done a study on it.
They did it right.
And by anything, I mean anything from how many troops does it take to occupy an area
to is this piece of equipment really that effective to do trans soldiers impact readiness.
They have done a study on it.
So what did they determine back in 2003?
You need one soldier per 50 civilians.
Now normally, because of the way people do math, this is written as 20 per 1,000.
How accurate is this number?
I want to say it's 90 something percent.
There are a few outliers but not many.
If there are not 20 soldiers per 1,000, the occupation fails.
It's really that simple.
There are not a lot of outliers and when they occur, Bosnia is one.
They didn't hit 20.
I want to say it was 18, 17, somewhere in there.
They're normally incredibly close.
And then there are some situations where the population isn't the whole country.
It's like confined to a geographic area.
So the number of troops that were used were much less.
But if you were to just take the population of that geographic area, it would be about
20 per 1,000.
And because I know somebody's going to ask, I'll go ahead and tell you in Afghanistan,
it wasn't even close.
Wasn't even close at the highest troop levels.
It wasn't even close.
Okay.
So 20 per 1,000, what is Russia's current ratio?
Four.
Four the number of troops they have committed thus far, four.
And that's actually giving them the benefit of the doubt that they haven't suffered a
lot of losses.
I'm fairly certain they have.
And that they actually have a little bit more troops than we know about.
It's at four.
That is not enough.
So what would it take troop number wise?
Now keep in mind, currently the estimates are between 150 and 200,000, how many they
have in country.
If you were to apply this ratio and assume that 10% of the population of Ukraine is no
longer present through various means, whether they are just no longer with us or they become
refugees, however it happens, you reduce the population by 10%.
Russia needs about 800,000 troops.
They don't have it.
Realistically they don't have a quarter of what they need to occupy Ukraine.
Now somebody could say that, well, they could ship more troops in.
I mean they could.
Russia's total armed forces, this is to include the Navy, Air Force, stuff like that, is just
a little more than a million.
They don't have the troops.
Even if they had a million to put in, they're not going to leave their home country undefended.
They do not have the troops.
If Putin is intent on occupying Ukraine, A, it's going to be a long, long fight, as long
as he's willing to put up with it.
And B, Ukraine will win.
All they have to do is keep engaging in minimal resistance, and on a long enough timeline
they will succeed.
They do not have the number of troops for the occupation.
Even if they committed the number of troops necessary, financially it would break them
because it wouldn't happen quickly.
These are long things.
And by traditional wisdom, Putin still has a little bit of time to truly break the resolve
of the country.
I personally think that the one month thing is outdated.
I don't think you have that long.
I think the resistance in Ukraine has already stalled.
I do not believe he'll be able to hold it.
This is why you have so many people just saying, this is what's going to happen.
And it would take something really, really, really bizarre to alter this.
Something unexpected or something unthinkable, you know, deployment of certain things against
civilians, stuff like that, might do it for a time.
But the other side to that is normally when popular resistance is broken through that
kind of force, it creates another type of campaign that would probably operate deeper
in Russia, which is something Putin doesn't want.
So the math says Putin will lose.
And there isn't much he can do to alter that math.
Until you start seeing giant call-ups and massive troop recruitment, all Ukraine has
to do is keep fighting and they win.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}