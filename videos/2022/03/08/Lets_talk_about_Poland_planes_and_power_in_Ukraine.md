---
title: Let's talk about Poland, planes, and power in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wYxCvMI-vHE) |
| Published | 2022/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the deal between Poland, Ukraine, and the United States involving the transfer of aircraft.
- Clarifies why the US is giving F-16s to Poland instead of Ukraine.
- Counters the commentary suggesting that this trade will weaken US air power.
- Provides a breakdown of the numbers regarding the world's largest air forces.
- Stresses the US's unmatched capability in air power and its ability to combat multiple adversaries simultaneously.
- Addresses the significance of the doctrine adopted by the US military post-World War II.
- Emphasizes that the transfer of F-16s to Poland will not significantly impact US air power.
- Points out the strategic importance of pre-positioning aircraft in allied countries.
- Dismisses concerns about degrading US defense capabilities due to this trade.
- Condemns misleading commentary based on inaccurate perceptions of military strength.

### Quotes

- "U.S. warfighting capability is second to none."
- "This isn't going to degrade U.S. air power, period, full stop, end of story."
- "It's not an issue."
- "These pundits bought that propaganda and they believed it."
- "Anybody who's providing commentary suggesting otherwise is probably somebody you should ignore on all military matters."

### Oneliner

Beau clarifies a trade deal involving aircraft between Poland, Ukraine, and the US, debunking commentary that it weakens US air power and stressing America's unmatched military strength.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Support NATO initiatives to enhance defense capabilities in allied countries (implied)

### Whats missing in summary

A deep dive into the intricacies of military strategy and international alliances.

### Tags

#Military #USForeignPolicy #AircraftTrade #DefenseCapabilities #NATO


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about a deal, a trade in
airplanes, and the commentary that has come out on those
airplanes and that deal.
We're going to do this because it's wrong.
The commentary is wrong.
And I'm not talking about it in the sense of it's a
different political view, or I view it
through a different lens.
It is objectively false.
And it needs to be clarified immediately,
because there are a lot of self-appointed military pundits
providing commentary on this that don't know anything.
In the process of this, you're going
to find out why the United States defense budget is
so bloated.
If you have no idea what I'm talking about,
Poland has agreed to make available their entire fleet
of MiG-29s to Ukraine.
It's going through a roundabout method,
but that's where they're going to end up.
The United States is going to backfill Poland
and give them F-16s to replace the outgoing MiG-29s.
Why doesn't the US just give the F-16s to Ukraine?
Because they don't know how to fly them.
But they do know how to fly the MiG-29s.
Commentary has surfaced,
suggesting that this is somehow going to degrade US air power.
That's a joke.
That's laughable.
The easiest way to demonstrate this is to first explain doctrine.
After World War II the United States adopted the doctrine of we have to be
able to fight our two largest competitors at the same time because
during World War II they had Germany and Japan and they had to deal with this
giant run-up in production just to be able to hold their own. So the US was
never going to be in that situation again.
So the doctrine is to be able to fight the two largest competitors at the same time.
And when it comes to air power, the U.S. has definitely succeeded in that.
To be clear, nobody can touch the U.S. in the sky, nobody.
And this isn't American exceptionalism, it's just fact.
The U.S. has the capability to fight an air war against Russia and China at the same time
and when.
To go over some numbers real quick,
the world's largest Air Force, I mean, after hearing that,
you know it's the US Air Force.
And this is total number of aircraft.
So the world's largest Air Force is obviously the US Air Force,
especially with that lead in.
The world's second largest air fleet is the US Army.
Third is the Russian Air Force.
The numbers here are US Air Force 5,217, US Army 4,409, Russian Air Force 3,863, fourth
largest air fleet, the US Navy, 2,464.
Then you have China at 1,991, India at 1,715, and the Marines, US Marines at 1,157.
Nobody can touch the US in the sky.
Now this is total aircraft.
If you wanted to go to air power and just talk about combat aircraft, the world's largest
air force would be the U.S. Air Force.
The world's second largest or most powerful would be the U.S. Navy.
It's not even close, and this is why DoD's budget is so huge.
It's to maintain this doctrine, a doctrine that really didn't need to be maintained,
But here we are.
So here are these numbers.
So how many F16s are we talking about?
Well, to know that, we'd have to know how many MIGs Poland is going to transfer out.
Now, it looks like they're trying to maintain some kind of security and they're not actually
putting the number in the reporting.
However, you can't start keeping a secret once it matters in the information age.
We happen to know that the entire fleet of MiG-29s in possession of Poland is 27.
Not 2,700, not 270, 27.
So the US will be transferring 27 F-16s.
Out of those numbers you just heard, do you think that that's really going to degrade
US air power?
It might be, what is that, 0.25%.
It is a fraction of a percent of the U.S. Air Fleet.
Now the idea that somehow this is going to cause significant issues, we also have to
think about what's happening.
If the U.S. was to go to war with somebody right now where it would really need to go
all in, who would it be?
Russia.
Where are these planes, these F-16s going?
Poland.
An ally.
Who would be in the fight with us?
Fine.
Don't look at it as giving it to them.
it as pre-positioning them. I would point out that the numbers for the Russian
Air Force here that I gave, those are significantly lower because the Russian
Air Force has been running into US-made stingers in Ukraine. It has the, that
That capability has helped level the playing field.
The MiG-29s will help shift the balance of power there.
It's not going to turn the tide all by itself, but it's going to matter.
And this is something that NATO feels it can do.
It has the means, therefore it has the responsibility.
It can provide this, and it can help.
The reason this commentary is coming out is because a whole bunch of right-wing pundits
watch those PR videos that were put out by Russia and China talking about how masculine
their military was.
I would point out that one of those videos was produced about the VDV, Russian airborne
troops, the troops that are getting pushed all over Ukraine by farmers with tractors.
PR videos are commercials.
They're not real.
These pundits bought that propaganda and they believed it and they think that there is a
serious imbalance.
There's not.
U.S. warfighting capability is second to none.
It's the one thing that U.S. really does well, especially when you are talking about a conventional
fight.
This is not going to degrade U.S. air power, period, full stop, end of story.
This isn't going to be a defense issue.
And believe me, these planes, they'll be replaced.
We have plenty.
It's not an issue.
Anybody who's providing commentary suggesting otherwise is probably somebody you should
ignore on all military matters from here on out.
Because if they don't understand this, which has been standing doctrine for the United
States for more than half a century, they probably don't know what they're talking
about.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}