---
title: Let's talk about NATO, oil, Venezuela, and Ukraine...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K3bkV2EN_o4) |
| Published | 2022/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Reads a message from a leftist critiquing his views on imperialism and sanctions.
- Responds to the message by discussing NATO expansion, imperialism, and capitalist aggression.
- Explains the importance of understanding military strategy and the capitalist economy.
- Criticizes the idea that Russia is synonymous with leftism, pointing out Russia's capitalist nature.
- Advises engaging in productive discourse without resorting to insults and understanding others' perspectives.

### Quotes
- "If you want to reach somebody, you have to kind of meet them where they're at."
- "Russia is a capitalist oligarchy. It is right-wing."
- "A lot of this is being parroted by people on the right."

### Oneliner
Beau addresses leftist criticisms on imperialism and sanctions, discussing NATO expansion, capitalist aggression, and the misconception of Russia as left-wing.

### Audience
Online activists and political commentators

### On-the-ground actions from transcript
- Engage in productive discourse with those holding different views (exemplified)
- Avoid insulting others in opening paragraphs to facilitate constructive dialogues (implied)

### Whats missing in summary
The full transcript provides detailed insights on addressing leftist criticisms, understanding military strategy, and engaging in respectful discourse.

### Tags
#Imperialism #Leftism #Capitalism #Discourse #Sanctions


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to do one of those videos
where I read a message.
If you are new to the channel, if you just signed up
because of the recent unpleasantness,
a common thing that occurs on this channel
is for me to read a message from one of y'all.
My social media is pretty open,
and I read everything that I can.
Occasionally, there are messages that come in
that, if read and talked about, could be beneficial.
A lot of times, it's arguments that you might encounter out
in the wild, out on social media.
And I provide an alternative viewpoint.
So it's not just these videos aren't just funny,
because I'm dunking on someone.
It provides people the tools to have these conversations.
and hopefully opens people's minds
if they're viewing this the same way.
And every once in a while, I get one message
that has a whole bunch of them in it.
So that's what we're going to do today.
Hey, Beau, it's me again, an actual leftist
and anti-imperialist.
You're a nice guy and well-informed for someone
from the southern US.
But let me help you understand the situation
school you on some basic facts. Facts that if you ever read any theory, you'd
already know. Well golly, okay, do tell. I was unaware that leftism was like
super classist, but okay. It goes on. NATO, not Russia, is the source of the
aggression. Their imperialist eastward expansion caused this. How would you feel
if Mexico joined a Russian-backed alliance. Please adjust your take accordingly."
This is one that we see a lot. It's NATO's fault because NATO's expanding in the direction of
Russia. It is a valid national security concern for Russia. That's not in dispute. But this goes
to a whole new level. How would you feel if Mexico joined a Russian-backed
alliance? Why do we get a say in it? I would point out that if you are
suggesting that a sovereign nation is really subordinate to a bordering
nation, then they're a client, a colony. That's imperialism. This is an imperialist
take. This isn't anti-imperialism, it is imperialism. And before you talk about
spheres of influence and all that stuff, understand that's all imperialism because
the question is, does Mexico get a say in what the United States does? Right? The
answer is no. Does Ukraine get to control Russia? Shouldn't they, if it's equal, if
If it's not imperialist, Russia would also have to consider Ukraine.
But they don't, why?
Because one side has the military power.
If a sovereign nation is subordinate to another through force, implied up until recently,
this is imperialism.
know you already oppose sanctions that would hurt the average Russian, but your
messaging is off. Here are better arguments. The possible oil sanctions
against Russia are yet another misstep in America's capitalist aggression,
forcing it to beg from actual threats much closer to home like Venezuela. I do
want to point out that Russia hasn't been a communist country in a really long
time. They're capitalist. And siding with the capitalist Russian billionaires over
Venezuela is definitely a unique leftist take. It shows the failure of capitalist
colonial military thinking that is so accustomed to pillaging resources. You
seem to understand a little about military strategy. Would relying on other
countries for strategic resources be a smart move when we could simply produce
more at home? I think you know the answer is no. I'm moved to quote the great
military philosopher Lil' Kim. Why spend mine when I can spend yours?
from a long-term military standpoint, depleting other nations' natural resources before tapping
into your own is actually a sound military strategy, and I know I don't know much about
this stuff, but it's been low-key U.S. policy for, I don't know, 50 years.
Because at the end, when their resources are depleted and you still have yours, you maintain
all the power.
If you can purchase it openly, that's a pretty sound strategy.
President Biden is simply too committed to green energy to produce more oil.
While green energy is good, his position is based on the capitalist desire for constant
innovation. Now again, as you pointed out, I haven't read much theory and don't know a whole
lot about this, but I'm fairly certain that in the United States we have a capitalist economy
that isn't planned, meaning that it's not Biden who needs to produce more oil. It would be the
oil companies who are currently sitting on thousands of leases giving them permission
to extract oil that they're not using. I've heard that there is some theory that discusses how
capitalist societies and the way it works is often big companies will lower supply to
increase prices. That might be at play here. As far as the war, Russia will win. You fail to
understand even the basics of Russian culture is Venetia. Let's be clear, none
of this is a leftist take. None of it. You're not a leftist, you're a Russophile.
You love Russia. I understand, I get it, right? It's a cool place, but Russia
hasn't been a leftist country in a really long time. The two things are not
interchangeable. A lot of this take is is boiled down to the idea that Russia is
somehow synonymous with leftism. Russia is a capitalist oligarchy. It is right
wing. There's no getting around that. These takes that are coming across like
this, and a lot of this, which is interesting, a lot of this is being
parroted by people on the right, the points themselves. The framing is a
little different, but the idea is still there. This whole thing is based on a
desire to play into the Soviet version of leftist thought and hold the Soviet
Union up. The Soviet Union is gone. If the Soviet Union still existed, Russia and
Ukraine wouldn't be a war. So when you're doing this and you're encountering this
kind of stuff online, always take a moment to step back because where the
person says they're coming from may not actually be their real position and they
may not even know it themselves. If you want to reach somebody, you have to
kind of meet them where they're at. I would also avoid in the opening
paragraph insulting them, but I mean that's just me. There could be other ways
to do that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}