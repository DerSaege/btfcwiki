---
title: Let's talk about Trump, Eastman, and the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cvEUK_iYoWw) |
| Published | 2022/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the context behind the news involving the committee, Trump, and Eastman.
- John Eastman, a key figure in Trump's election amendment efforts, invoked attorney-client privilege.
- The committee argued that this privilege should not apply, suggesting evidence of a crime already exists.
- The committee believes there is a good faith basis for a criminal conspiracy involving Trump and his campaign.
- Allegations include felony obstruction of official proceedings and conspiracy to defraud the United States.
- A smoking gun is an email from Eastman to Pence, suggesting a minor violation of the law to delay election certification.
- Trump's aides reportedly informed him that his legal actions post-election were futile.
- This indicates that Trump's persistence despite being aware of the situation could be seen as corrupt behavior.
- The committee's filing signals their intent to pursue criminal prosecution against Trump.
- While there are hurdles to overcome, the committee is determined and confident in their evidence.
- The focus seems to be on moving towards criminal prosecution rather than preserving the institution of the presidency.
- There is a long road from the current filing to actual prosecution, but the intention is clear.

### Quotes

- "They believe they already have evidence of a crime."
- "We think Trump broke the law."
- "I have to say that I would be worried."
- "Y'all have a good day."

### Oneliner

Beau explains the context behind the committee's filing, alleging criminal behavior by Trump, signaling potential legal troubles ahead.

### Audience

Legal analysts, political commentators

### On-the-ground actions from transcript

- Contact legal experts for insights on the implications of the committee's filing (suggested).
- Stay informed about the developments in the legal proceedings against Trump (implied).

### Whats missing in summary

Insights on the potential impact of these legal proceedings on future political landscapes. 

### Tags

#LegalProceedings #Trump #CommitteeFiling #CriminalConspiracy #Obstruction


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're gonna talk about the big news
involving the committee and Trump and Eastman.
And I have a feeling this is gonna be
all over the news today,
but generally when something like this happens,
there's a lack of context.
So what we're gonna do is we're gonna go through
a brief overview of how all of these statements
that you're gonna see flash up today, how they came about.
So John Eastman is a lawyer and a central figure
in Trump's play to amend the election.
I'm gonna use the nicest terms possible here.
He attempted to invoke attorney-client privilege.
The committee put out a filing trying to explain
why that privilege shouldn't apply.
And in it, they say sufficient information
already exists to reject this.
That indicates they believe
they already have evidence of a crime.
This is further backed up by this quote,
saying that the committee has quote,
good faith basis for concluding the president
and members of his campaign engaged in a criminal conspiracy
to defraud the United States.
Now there are a couple,
there's a few different things in the filing
that indicate there are crimes
that the committee believes Trump has some culpability for.
The two that matter are felony obstruction
of official proceedings and a conspiracy
to defraud the United States.
Those are the two that are actually gonna matter
at the end of the day.
There are some smoking guns that came out in this.
The first one is an email from Eastman to Pence.
In it, there's a line where Eastman is alleged
to have told Pence's lawyer
to engage in a relatively minor violation of the law.
And he wanted Pence to adjourn for 10 days
to provide, I'm not sure how he phrased it,
to provide time for an audit or something like that.
But the point was to delay the certification
of the election.
Another way to say that might be
obstruct official proceedings.
Generally speaking, judges are totally uncool
with encouraging somebody to violate the law in writing.
That that's probably gonna come up in any hearings on this.
There's also the fact that Trump's aides, allegedly,
told him that he lost
and that all of his legal shenanigans,
they weren't gonna work.
The reason this matters is because it goes to show
that Trump should have been informed
that all of this wasn't going to fly,
which means his attempts to push it were corrupt.
Engaging in these activities corruptly
is part of the elements
if you're trying, when it gets to criminal prosecution.
This is the first type of stuff we're seeing like this
come out of the committee that is just flat out saying,
we think Trump broke the law
in an official filing like this.
Going through it,
there's definitely stuff for the Trump camp
to be worried about.
Now, it's important to note
that there is a very long path
from this filing to actual criminal prosecution
of the former president.
But what this shows
is that's where the committee's trying to go.
They're trying to go down that path.
And they certainly believe
that they have the evidence to prove it already,
and they're trying to get more.
That's what the filing indicates.
And I have to say that
I would be worried.
I would be concerned.
So we still have a lot of hurdles
that would have to be overcome in this.
And don't ever underestimate
the longstanding tradition
of attempting to preserve the institution of the presidency.
I think that is something
that is gonna come up at some point.
It does appear that the committee
is not really concerned about that.
It appears that they really are
trying to move towards criminal prosecution
because they're certainly alleging it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}