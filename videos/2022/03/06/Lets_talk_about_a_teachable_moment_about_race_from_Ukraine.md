---
title: Let's talk about a teachable moment about race from Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DD3XEatgJiw) |
| Published | 2022/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Talks about Ukraine as a teachable moment for the rest of the world, especially the United States.
- Mentions reports and strong evidence of discrimination in Ukraine based on nationality and skin color.
- Notes the lack of widespread media coverage on these discriminatory actions.
- Criticizes Western media for heavily supporting Ukraine and potentially overlooking critical issues.
- Questions the West's response to Ukraine compared to potential responses in other regions like Africa.
- Addresses the concept of global white supremacy and its impact on perceptions and actions worldwide.
- Points out the immediate unity of the Western world in response to Ukraine, raising questions about consistency in other regions.
- Calls for a critical examination of media portrayals of different parts of the world as "semi-civilized."
- Suggests that the West's strong response to Ukraine is influenced by familiarity and identification with the people there.
- Urges acknowledgment of biases and inequalities beyond borders and advocates for addressing them promptly.

### Quotes

- "Beyond America's borders, do not live a lesser people. Beyond Europe's borders, do not live a lesser people."
- "There are a lot of times when dynamic situations unfold that you wait until afterward to talk about stuff like this. This isn't one of them."
- "It's something we need to fix."

### Oneliner

Beau addresses discrimination in Ukraine, questions Western media bias, and challenges global perceptions of white supremacy, urging immediate reflection on biases and inequalities beyond borders.

### Audience

World Citizens

### On-the-ground actions from transcript

- Challenge biases in media portrayals (implied)
- Educate others on global inequalities and biases (implied)
- Advocate for fair and unbiased media coverage (implied)

### Whats missing in summary

Insights on the importance of recognizing and addressing systemic biases in global reactions and media representations.

### Tags

#Ukraine #Discrimination #MediaBias #GlobalInequalities #WhiteSupremacy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Ukraine.
But we're going to talk about a teachable moment
for the rest of the world, particularly those
in the United States who don't understand the concept.
So we're going to talk about some things that
have occurred there.
We're going to talk a lot about the media coverage of what's
going on in Ukraine.
And we're going to compare and contrast it
to some other things.
Mainly we're going to talk about Ukraine
and race.
There have been reports and there is incredibly strong evidence
to suggest that
people in Ukraine
who aren't Ukrainian,
people who came from other countries,
when they tried to leave, when they tried to flee,
that at the border
they were pushed back and Ukrainians got out first.
This is also especially true
if those people were a different color,
if they had a different skin tone.
They got pushed back.
This was reported on
but uh...
not really widely
and there are a number of reasons for that.
One is simply that Western media has gone all in on supporting Ukraine, and this, well,
I mean, that might help undermine that.
So it just gets glossed over.
Another reason is that for a lot of people covering it, well, it's not really that important,
right?
I mean, it's better there than wherever they came from.
And you can say that that's not really what they're thinking.
However, if you really listen to the commentary, things you
will hear is that this could be any city in Europe.
We see AKs and RPGs in Europe.
This isn't Afghanistan.
And this could be any semi-civilized country.
These are things that have been said on the media, in the media.
And I think a lot of people don't hear it, and there's a reason for that.
It just kind of gets filtered out, but it's definitely there.
And we've had questions about why Ukraine?
Why has the West responded in the way that it has?
Now there is one reason, and it's proximity.
It's in Europe, so European nations are definitely going to pay more attention to it.
That's a legitimate foreign policy national security reason.
But then you think about those other statements.
You think about the coverage.
Do you honestly believe that if Russia had done this exact same thing in any country
that ended in a stand or any country in Africa that the West would have responded this way?
I don't believe that, not for a second.
Because after all, it's normal for there to be AKs and RPGs over there with those other
people, right? It's in the coverage. It is in the coverage. So this is a moment to
acknowledge that and to think about the fact that in the United States we hear
academics, activists, people who are talking about race relations, we hear
them talk about a concept called global white supremacy, right? And for a lot of
people in the U.S. eyes glaze over because it doesn't even make sense. You
know, a whole lot of the world, I mean there's not even a whole lot of white
people there. How can it be a global thing? Because the major powers, those
that have the force, that have the economic power to shape the world, have
it ingrained there. Therefore, it filters through to the rest of the world and that
impacts their standard of living, what they're capable of accomplishing because a lot of
them do not have the economic power or the military power to compete or to say no. It
still exists. It's been this way for a really long time. This is a clear example
when you look at how united the Western world became immediately when that
footage started coming out. If this was somewhere else, do you think it would be
the same? If you're answering honestly. And you can see this in media coverage
in a lot of different ways. A good one is when something happens in Africa.
Right? What's the footage that's used? Or the image in a newspaper or online?
It's typically of some village. Next time you see that happen, type in that country,
get the capital name, and then go to Google Images.
There is a habit of still portraying
other parts of the world as semi-civilized.
This is something that the world as a whole
is going to have to deal with at some point.
The sooner the better and this is a teachable moment.
We can see it play out in real time.
Make no mistake, if Russia did this in a Central African country, the West would not be considering
escalations that might lead to nuclear war over it.
They would be whining about why they should even get involved.
It's not really our problem, is it?
But because of that familiarity, because of that habit of looking out for those you identify
with most, in Ukraine, the West is all in.
I don't think anybody can look at this and deny it if they're being honest.
Beyond America's borders, do not live a lesser people.
Beyond Europe's borders, do not live a lesser people.
This is a moment where we can see it happening.
There are a lot of times when dynamic situations unfold that you wait until afterward to talk
about stuff like this.
This isn't one of them because this concept, when it gets brought up in the United States,
people reject it out of hand.
But this shows it happening real time right now.
And sure, there are legitimate reasons for some of it.
proximity matters and the fact that it's Russia and those early mistakes, its
foreign policy, what's foreign policy about? Power. Opposition nations,
competitors to Russia, saw their moment to weaken it. Sure, that's a reason too.
But when you listen to the coverage, it's pretty clear what a motivating
factor is, those people are like us. They're not like the others. It's something
need to fix. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}