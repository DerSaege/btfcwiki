---
title: Let's talk about the new Russia-US hotline....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1Yh-l0J5LHc) |
| Published | 2022/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States and Russia have established a direct line of communication over Ukraine to de-escalate tensions.
- The hotline between DC and Moscow, similar to the famous red phone, has always existed but is now being used for military-to-military communications.
- This communication channel can be used for scenarios like providing real-time intelligence to the Ukrainian military or clarifying misunderstandings about military activities.
- In case of emergencies, like an aircraft issue outside Ukraine, the US might use the hotline to inform Russia of sending in pararescue teams.
- The goal of this direct military communication is to prevent great power wars and protect civilians from indiscriminate acts.
- The channel is a form of frank military-to-military diplomacy, with diplomats sometimes being involved as well.
- Having this communication channel in place is a preventive measure to avoid NATO direct involvement and potential escalation into unwanted conflicts.
- Back channels like these are vital in avoiding wars, de-escalating situations, and maintaining conflicts at a skirmish level.
- Establishing this communication line is considered a positive step that should have happened earlier to prevent misunderstandings and potential conflicts.

### Quotes

- "Back channels like these are vital in avoiding wars, de-escalating situations, and maintaining conflicts at a skirmish level."
- "The goal of this direct military communication is to prevent great power wars and protect civilians from indiscriminate acts."

### Oneliner

The US and Russia establish a direct military communication channel to de-escalate tensions and prevent unwanted conflicts, showing the significance of back channels in avoiding wars and protecting civilians.

### Audience

Diplomats, Military Personnel, Peace Advocates

### On-the-ground actions from transcript

- Establish direct lines of communication within your community or organization to prevent misunderstandings and conflicts (exemplified)
- Advocate for the use of back channels for de-escalation and conflict prevention in international relations (suggested)

### Whats missing in summary

The full transcript provides a detailed insight into the importance of direct military communication channels in preventing conflicts and the significance of back channels in diplomatic relations.

### Tags

#MilitaryCommunication #DeEscalation #BackChannels #PreventConflict #Diplomacy


## Transcript
Well, howdy there internet people. It's Bill again. So today we're going to talk about clear
communications. The United States and Russia have set up a direct line of communication
over Ukraine with the goal of de-escalating tensions as things move along. When this is
brought up, people immediately think of the red phone, you know, from Kennedy's day. It was never
really red, I don't think, anyway. That hotline, the DC-Moscow hotline, it never went away. It
still exists. This is something a little bit different. This is probably going to be used
more for military-to-military communications, and it's not uncommon. We've actually talked about
this type of back channel before when Trump got upset because Milley contacted his Chinese
counterpart to calm things down. That's their job. The idea of people in command at that level in the
military, their job is to protect the United States. Avoiding a great power war certainly
falls under that. This is the same type of thing. It will be used to communicate very directly
between the militaries. So how could it be used? One example, right now the US is feeding real-time
intelligence to the Ukrainian military. A lot of it they're getting from aircraft that are flying
outside of the borders of Ukraine. However, if something was to go wrong with one of them,
outside Ukraine, the US might pick up this phone and say, hey, we're sending in pararescue guys,
blah blah blah. There's going to be 18 of them. They're going to this location, picking up the
crew, and they're leaving. Do not mess with them. Something like that. Or, given the current state
in Russia and the likelihood of rampant paranoia, if the Russians were to pick up
something they deemed as irregular submarine activity from the United States, they might call
and say, hey, what is this? And the United States military could assure their counterpart and say,
no, no, that's not what that is. In fact, those aren't even those kinds of subs. Those are something
else. Another thing would be if Russia did engage in a lot of indiscriminate acts that were causing
widespread losses among civilians, they might get a call from the US, and the generals be like, hey,
my boss has kind of had enough of this. You've got to tone down. And it's, in some ways, it's
diplomacy, but it's very frank, military to military conversations. At times, actual diplomats
might be involved. But that's the general idea of it. Now, another aspect to this is that this is
something that would have to be in place before NATO got involved directly. I still don't see that
as something that's occurring, and I don't think that that's an intentional step here. I don't think
they're leading up to that by getting this channel of communication open. But before NATO got involved
directly, this would have to exist just to avoid, attempt to avoid the possibility of it spiraling
into something that nobody wants. Overall, these kind of back channels, they're good. They are good.
They can avoid war. They can de-escalate situations. They can turn something that could
have spiraled into a war and keep it at a skirmish level type of thing. It's a good move. It's
something that should have happened a while ago. I'm surprised it didn't, but it is now in place.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}