---
title: Let's talk about mom and social constructs...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=79P26WSAUXo) |
| Published | 2022/03/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of starting with a basic understanding of social constructs to explain concepts like transgenderism.
- Emphasizes that once people understand social constructs, they tend to become more accepting.
- Suggests finding an example from an older person's past where a social construct shifted to help them understand the concept.
- Uses the example of public pools and segregation to illustrate the shift in social constructs.
- Mentions the significance of understanding that many beliefs are just peer pressure from past bigots.
- Recommends focusing on the fluidity and arbitrary nature of social constructs like race to broaden understanding.
- Points out that many accepted beliefs are not grounded in anything concrete but are societal perceptions.
- Encourages starting with the core concept of social constructs before delving into more specific issues.
- Urges individuals to recognize their place in history and how their views may be judged in the future.
- Stresses the need to confront uncomfortable truths about the past and present to move towards a more accepting society.

### Quotes

- "A lot of these things that you believe are just something that can't be changed, it's really just peer pressure from dead bigots."
- "This is why gender roles and gender norms and all of this stuff, it's kind of up in the air because we get to determine what it is."
- "People have to understand that a whole lot of the things we accept as something that is just set in stone and cannot be changed is made up."
- "You want that basic understanding and the understanding that it's OK."
- "They may not be a great ally, they may not get out there and join the fight or anything like that, but they're going to be accepting."

### Oneliner

Starting with a basic understanding of social constructs can pave the way for acceptance and broader societal change, as Beau explains.

### Audience

All individuals seeking to foster understanding and acceptance in their communities.

### On-the-ground actions from transcript

- Share stories of social construct shifts with older individuals to help them understand and accept new concepts (exemplified).
- Encourage open dialogues about the fluidity of social constructs, like race, to broaden perspectives and challenge ingrained beliefs (exemplified).
- Initiate educational sessions or workshops on social constructs to deepen community understanding and acceptance (suggested).

### Whats missing in summary

The full transcript provides detailed insights on using social constructs to foster acceptance and understanding, especially among older generations, with practical examples and strategies.

### Tags

#SocialConstructs #Acceptance #Understanding #Community #Change


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk a little bit more about social constructs,
because I got a message asking for a little bit of advice on how to kind of expand on a topic.
But before I get into it, I would like to say hi to Crow.
Okay, so the question here, it starts off with, you know, hey,
I need an easily digestible way to reach out to my mom.
Easily digestible, that's an important part.
Then it goes on to say, you know, hey, most of my friends have rainbow bumper stickers.
I'm paraphrasing here, obviously.
And then it gets to the important part, the meat of this question.
My mom knows I'm not exactly straight,
but is trying in a very good faith effort to understand these concepts and all.
She's 54 and I can compare her to Hank Hill.
She's set in her ways and at times is really out of her element,
but wants to keep up with how the world keeps changing faster and faster.
How can I explain transgenderism to my mom
with a basic understanding of gender being a social construct?
Also, how do I tie in, stop right there,
the easily digestible pieces, right?
And you already got the one that matters.
Instinctively, you know what matters here, the social construct aspect.
Everything else, that's extra and in large part irrelevant.
Because if you can take a basic understanding of what a social construct is
and expand it and increase the knowledge around that,
the details really kind of cease to be that important.
Because what tends to happen is people get to the point where they're like,
well, they're not hurting anybody.
And if people have a good understanding of what a social construct is,
that becomes their attitude.
They may not be a great ally.
They may not get out there and join the fight or anything like that,
but they're going to be accepting.
And to start with, that's all you really want.
You want that basic understanding and the understanding that it's OK.
That's the key part.
Now, this person may decide, yeah, I do want to become an expert on this topic.
I do want to join the fight.
I do want to be an ally.
But you can't start there.
You have to get them accepting of it first.
So social construct, you're absolutely right.
That's where you start.
And you increase the understanding of that topic.
So basic definition of what a social construct is,
it's an idea created by society that society accepts.
It's that simple.
Now, when you're talking about older people,
it's harder because these ideas, they're ideas that they have
lived with their entire life.
They believe them to be set in stone.
And when you sit there and break it down and explain it to them,
and you're like, hey, a lot of these things that you believe
are just something that can't be changed,
it's really just peer pressure from dead bigots,
there's an obstacle that you have to overcome there.
Because when you break it down, that's what it is.
When you're talking about social issues and things changing
and people being less than accepting or putting up
resistance, they are literally succumbing to peer pressure
from dead bigots.
That's what it is.
So you have to get them over that.
What's the easiest way?
I actually have some success in this area.
What I have found is when you're talking to older people,
you find a social construct that shifted
in the early part of their life, something that was one way
and then became another during the first 15
years of their life.
Because if you can do that, odds are that person
accepts the post-change version of that social construct,
but they are aware of the pre-change version.
They experienced it themselves.
And if you can find something like that
and use that to explain the whole concept,
it becomes easier.
Because they probably saw their parents or older people
in their life put up resistance to that idea that they accept.
So based on your mom's age, public pools
is where I would go.
I'd be willing to bet that your mom probably
has fond memories of public pools,
because they used to be everywhere,
but now they're not.
Why?
Yeah, peer pressure from dead bigots.
When segregation ended, when the federal government stepped in
and was like, hey, this is done.
We're done with it.
This is over.
There were a lot of people at the state and local level,
well, they didn't really want it to be over yet.
And one of the hot button issues was public pools.
For some reason, they didn't want
to be in the pool with those other people.
So a lot of people, a lot of locations,
they drained their pools.
They got rid of their public pools
as a way of not allowing it.
So that started the decline, and now they're
nowhere near as prevalent as they used to be.
And that shift, that was the end of a social construct,
the end of segregation.
So segregation was also a legal construct.
It was a whole lot of things.
But even once the law had changed,
there were still those holdouts.
There were still those holdouts who
clung to the peer pressure from dead bigots.
There's really no other way to say that.
That's what it is.
And the politicians, well, they played into it
just like the politicians of the past.
They knew that was a way to rally a base
and get them looking down at this other group of people.
So it didn't matter what the politicians did.
Start with that.
If that doesn't work, think two games
that she probably played in school.
Well, she probably didn't play them because of gender roles.
She probably wasn't allowed to play it.
But her brothers, her cousins, boyfriend, somebody did.
Smear the...
Yeah, that was totally normal.
That was a totally normal thing for a teacher
to yell out on a playground.
When your mom was a kid, I know it was when she was a kid
because it still was when I was a kid.
That was normal because it was still OK.
The social construct said it was still OK
to other that group of people.
And in this case, kind of justify violence against them.
That would be a good one.
Think two things that changed.
Now, luckily, your mom's age, there are tons of options.
Another good example of a social construct
that people have a hard time accepting as a social construct
is race in general.
It's not really a thing.
It's this concept that society has accepted.
The strongest evidence of this, because most people who
don't want to believe that it's a social construct or white,
is the fluid definition of white throughout the years.
There were times when certain groups of people
who are considered white today weren't before because they
needed to expand that group, the in-group,
so they could other a different group of people.
They needed the numbers.
It is.
It's peer pressure from dead people.
And if you want to get to that society where people have
the maximum amount of freedom for the maximum amount
of people, and that's how the society is structured,
people have to understand that a whole lot of the things
we accept as something that is just set in stone
and cannot be changed is made up.
It's not grounded in anything.
It's just a perception.
This is the part to focus on.
Once you get somebody to this point,
their whole attitude changes to all kinds of issues.
And it's also useful for people to understand
where they're going to be at in the history books,
how they're going to be viewed in the future.
I mean, people are so embarrassed of it today.
They don't want their kids learning history
because they'll realize that grandma wasn't always
the sweet old lady with cookies.
She might have done some really messed up stuff.
She believed some messed up things.
That push against teaching history,
against teaching about what happened in this country,
is based on people acknowledging on some level
that they were on the wrong side of history.
There are people today who are out there.
They are out there echoing the peer pressure
from dead bigots.
And they're going to be on the wrong side of history too.
Focus on that.
Focus on the social construct.
This is what it is.
This is why they don't really matter.
This is why we can change it really easily.
This is why gender roles and gender norms
and all of this stuff, it's kind of up in the air
because we get to determine what it is.
Start with that.
The rest of it, that can come afterward.
Get her to where she truly understands
that a lot of the things that she may believe
or may hold as an important value
is just her succumbing to peer pressure from dead bigots.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}