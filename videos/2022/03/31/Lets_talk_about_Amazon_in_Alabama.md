---
title: Let's talk about Amazon in Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rpPj-AeqzwI) |
| Published | 2022/03/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Update on union news in Bessemer, Alabama regarding Amazon's second vote, results possibly coming in today.
- National Labor Relations Board found Amazon unfairly influenced the last vote, prompting a new one.
- Union expected to do better this time since they were able to talk to people without pandemic restrictions.
- Amazon faces more union votes in New York City, likely to succeed due to local support.
- Grievances at Amazon not primarily about pay (employees make more than $15/hour), but about working conditions and standard of living.
- Similar to Warrior Met coal mine workers still on strike, Amazon workers seek better conditions and the promise of a decent living.
- Union activity organizing in the U.S. gaining momentum beyond expected places like Amazon and Starbucks.
- People are looking beyond just dollar amounts, seeking a better standard of living and ownership opportunities.
- Hopeful for a union win at Amazon in Bessemer, with potential for success in New York as well.
- Momentum is building for unionization at Amazon regardless of the outcome in Bessemer.

### Quotes

- "It's fight for 15 in a union."
- "They want the dream, the promise that they were handed."
- "I'm hopeful for the vote in Amazon."
- "I think that the momentum has started and we're going to see a unionized Amazon in the near future."
- "Y'all have a good day."

### Oneliner

Update on Bessemer, Alabama union vote on Amazon, reflecting growing momentum for union activity in the U.S., beyond wage fights towards better living standards.

### Audience

Workers, activists, supporters

### On-the-ground actions from transcript

- Support union organizing efforts in your area (implied)
- Stay informed about labor rights and support workers' rights movements (implied)

### Whats missing in summary

Details on the potential impacts of Amazon unionization on workers' rights and company practices.

### Tags

#UnionNews #Amazon #LaborRights #WorkerEmpowerment #LivingWage


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we have some union news.
As early as today, the results from the union vote
in Bessemer, Alabama, regarding Amazon, could be coming in.
This would be the second vote.
The National Labor Relations Board
found that Amazon unfairly influenced the last one.
So there's a new vote.
It's also expected that the union will do slightly better,
at least in this one,
because they were actually able to talk to people.
They weren't trying to do it
in the middle of a massive public health issue.
Now, one of the things about this vote that's interesting
is that it's just the beginning for Amazon.
They have more votes coming up in New York City,
which are probably a lot more likely to succeed,
given the general climates of the area. It's also interesting because they
don't pay bad, not really, not for that area. They make more than $15 an hour,
but the gripes are not really about pay. It's about conditions, standard of
living, stuff like that. It's a lot like Warrior Met, the coal mine that we
helped out around Christmas. Those workers, by the way, are still on strike
right now. Union activity organizing in the U.S., it's gaining momentum. It's
happening in a lot of places you might not expect, you know, obviously Amazon,
Starbucks, places like that, and it's probably going to keep growing because
as the gap between the haves and the have-nots, as it continues to just get
wider and wider, people are beginning to think beyond just dollar amounts.
You know, it's not fight for 15 anymore.
It's fight for 15 in a union.
And they want that standard of living.
They want the dream, the promise that they were handed.
They want to be able to own a home to provide for their kids, to work for a company that
doesn't put them in a situation where they end up relying on government benefits, where
we end up subsidizing these massive companies that are making billions of dollars in profit.
I'm hopeful for the vote in Amazon.
I think it's possible that the union wins, but I'm also aware that it's Bessemer, Alabama.
So if it is a win here, it's almost guaranteed to win in New York.
But even if they lose here, I don't think this is the end of it.
I think that the momentum has started and we're going to see a unionized Amazon in the
near future.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}