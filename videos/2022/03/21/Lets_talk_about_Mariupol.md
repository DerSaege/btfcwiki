---
title: Let's talk about Mariupol....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Wi_kDPvDs9Q) |
| Published | 2022/03/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the situation in Mary Opal, a city in Ukraine that is currently surrounded and under attack by Russian forces.
- Describes the importance of the city to Russia, mentioning the need for a win and a potential route to Russia proper if conquered.
- Points out that Russia's hesitation to move in is due to the challenges of urban combat and the inability of their conscripts to handle it.
- Details the motivations of Ukrainian forces in defending Mary Opal, which include preventing a template of destroying cities from succeeding and delaying Russian advances to other key locations.
- Compares the Ukrainian defense strategy to historical examples like the Battle of Bunker Hill to illustrate the concept of strategic losses leading to eventual victory.
- Emphasizes that the Ukrainians are not fighting to win battles but to break Russian resolve through prolonged resistance.
- Raises concerns about the potential consequences of Russia successfully taking Mary Opal and the domino effect it could have on other cities.

### Quotes

- "It's the Alamo for your dad."
- "If you can lose like that long enough, you win."
- "Russia is able to take this city because they leveled it, they'll try to duplicate it."

### Oneliner

Beau explains the high-stakes situation in Mary Opal, Ukraine, where Ukrainian forces are sacrificing lives to resist Russian aggression and break their resolve.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support Ukrainian relief efforts (suggested)
- Raise awareness about the situation in Mary Opal and Ukraine (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the conflict in Mary Opal, shedding light on the strategic motivations of both Russian and Ukrainian forces, as well as the potential implications of the ongoing battle.

### Tags

#Ukraine #Russia #Conflict #Resistance #GlobalSolidarity


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Mary Opal
and why it's shaping up the way it is and what's in it for both sides
and why Americans have a skewed vision of stuff like this
when they look at anything that resembles this.
And we're going to talk about some possible outcomes.
The reason we're going to do this is because I got a message from a woman
who is currently at college, but she's back home right now with her family for a week.
During her time at college, she has realized that her father's presentation
of what it would be like when the time comes, quote,
and if you're not familiar with that, it's a dog whistle talking about Civil War,
she's realized that his presentation is less than realistic.
And she's been able to talk to him about it, you know,
now that she's back home for a little bit.
She said the one thing she didn't have a response for was her dad saying that the people in Mary Opal,
well, they just didn't know what they were doing.
They were doing it wrong.
They should have taken off to the hills as soon as the Russian showed up
and gone to a different city and all of this stuff.
And that what they're doing now is dumb.
That makes sense, not because it's true, but because an American said it.
Where do we fight our wars?
You know what, let's keep this simple.
That's a really long lost.
Where do we not fight?
Here.
We're protected, the Atlantic and the Pacific.
The United States does not fight wars on its home soil.
So because of that, our idea of conflict is very one-sided.
The US is on the offensive, pressing the attack.
We don't have a lot of stories about defending something because we don't have to do it very often.
Because of that, people don't understand the motivations.
So we're going to go into that a little bit.
But first, context.
Mary Opal.
What has the media said?
It's a city in Ukraine that's surrounded and it's a port.
That's probably all I've said.
I haven't seen much deep discussion of it.
It's not small.
That's important here.
We're talking about something the size of New Orleans, Pittsburgh, Wichita, Anaheim, Honolulu.
It's about that size.
Not a little place.
Okay.
Now, a large portion of the population is no longer present.
They either became refugees or they are just no longer with us.
Why is this place important to Russia?
Well, for a couple of reasons.
First, they need a win.
Their offensive has failed.
They are stalemated across the country.
Those maps aren't moving.
They have lost the initiative.
The offensive has failed.
So they need something to give.
They need a win.
The other reason is, hopefully by this point, they've realized conquering all of Ukraine,
that's not in the cards.
So they may be setting themselves up for lesser victory conditions, meaning if they can take
Mariupol, maybe at the negotiation table, they can get Ukraine to let them keep it,
which would give them a route over land from areas that they got back in 2014 to Russia
proper.
So those are the two main reasons for them.
So why haven't they moved in?
They've been sitting here deliberately targeting civilians, leveling a city.
Why haven't they just gone in?
Because they can't.
Not moving in has probably been the smartest move the Russians have made this entire war.
Maybe the only smart move.
Those conscripts are not up to it.
It's urban combat.
Street to street, building to building, room to room.
And behind every corner could be a Ukrainian.
Once they move in on foot, those conscripts are going to have serious doubts because fire
can come from anywhere.
So rather than accept the failure, rather than accept the stalemate, the defeat, they
have chosen to level a city in hopes of forcing it to surrender.
Now the Ukrainian forces, what's in it for them?
Is this strategically important dirt?
Not anymore.
It's a destroyed city that's not going to be rebuilt until after the war.
The city itself isn't important strategically anymore.
But that's not why they're there.
They're there for two reasons.
One, to show that that operational template of destroying a city isn't going to work.
Because if this works for the Russians here, the next stop is Odessa and they're going
to do the exact same thing.
So the Ukrainians have decided to stay and they are going to make them pay for every
square inch.
Street to street, building to building, room to room.
And hopefully cause enough loss that Russia decides this template isn't going to work
so they'll try something else.
They can't let this succeed.
The other reason is if they were to take off to another city, disappear into the hills,
whatever, all of those Russian troops are now free.
So they can go to Odessa or they can go north to the capital, shore up troops there.
It is stalled right now because Russia opened up four main accesses of advance and they
don't have the troops for it.
If they can free up troops and move them somewhere else, it might reinvigorate their effort.
So they can't.
Ukrainians can't give up.
It's not that they're dumb, it's that they're quite literally sacrificing their lives.
It's the Alamo for your dad.
The idea at this point for the Ukrainians, remember they don't have to win.
They don't have to win the battles.
They just have to fight and eventually break Russian resolve.
Another good example for Americans is Bunker Hill.
It's a story that's in everybody's elementary school textbook when it comes to history and
social studies.
You heard about this battle.
It has become part of American mythology, so much so that most people don't know we
lost.
We lost at Bunker Hill, but we made them pay for every square inch.
After losing, Major General Nathaniel Grain said, I wish we could sell them another hill
at the same price, because if you can lose like that long enough, you win.
It's the same thing.
It's just not a concept Americans are familiar with, because we're never on this side of
things.
But make no mistake about it, when the time comes, it would happen.
It's very different when it's actually occurring.
It's not over there anymore.
The other thing that we have to acknowledge about this is that if it works, if this succeeds,
and Russia is able to take this city because they leveled it, they'll try to duplicate
it.
If they do, this will be the moment that we look back on in 10 or 15 years and say, man,
we should have done something then.
We should have known how bad it was going to get.
We won't.
We can't do anything now, because A, there is a chance that the Ukrainians pull it off.
I don't think the Russian troops can do it, but there is the chance that Russia just decides
to continue to level it.
If that works, Odessa is next.
And if that succeeds, they'll keep duplicating it, and we will see loss that we haven't seen
in a very, very long time.
And it will be one of those things where later on, everybody's going to be wondering why
we didn't act sooner.
And this will be that point.
But hopefully, the Ukrainians can pull it off.
If they can make them pay, if they can lure them in, then I think they've got a really
good chance of having their own bunker hill, their own loss that is a win strategically,
even though it's going to cost a whole lot of people.
It doesn't matter what happens from here now, what decisions get made on both sides around
that area, there's going to be a lot of loss.
But right now, Ukraine is the anvil, and they just have to take the hits and break the hammer.
That's where they're at.
So at the end, yeah, this is another fantasy.
In real life, you're not always on the offensive.
You don't always have the ability to move, and if you do, you may lose the war.
Sometimes you're that person who's in a city that you have to stay there knowing what's
going to happen in the hopes of something else breaking for your side on the other side
of the country.
They're not dumb, they are sacrificing their lives.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}