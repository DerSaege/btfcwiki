---
title: Let's talk about Russian reinforcements, contracts, and conscripts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Lfvm09_Dtyo) |
| Published | 2022/03/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the structure of the Russian military and details about contract troops and conscripted troops.
- Contract troops, around 350,000 to 410,000, are the core and elite units of the Russian military.
- Conscripted troops, around 260,000 additional forces drafted twice a year, are not well-trained and only serve for a year.
- Russia's total forces on the high end are around 670,000, not all of which are combat-ready.
- Due to commitments and reserves needed for defensive purposes, Russia may only have about 150,000 troops available for offensive actions.
- Russian reserves of 2 million consist mainly of individuals who were in the military and are still young enough to be recalled.
- Only around 5,000 out of 2 million Russian reserves actively train regularly, unlike traditional reserves seen in other countries.
- Russia lacks the numbers and trained personnel required for a successful large-scale military operation in Ukraine.
- A potential massive call-up could train more troops, but the success of such training remains questionable.
- The likelihood of Russia effectively running a professional military operation with its current resources seems bleak.

### Quotes

- "They don't have the numbers."
- "It's math at this point."
- "The only choice Russia is going to have is to do what they've been doing in Mariupol."

### Oneliner

Beau breaks down the Russian military's numbers, revealing challenges in mounting a successful operation in Ukraine due to lack of trained personnel and numbers.

### Audience

Military analysts

### On-the-ground actions from transcript

- Monitor developments in the conflict in Ukraine (implied)
- Stay informed about military capabilities and limitations (implied)

### Whats missing in summary

Insights on the potential geopolitical impacts of Russia's military limitations.

### Tags

#RussianMilitary #Conscripts #ContractTroops #MilitaryReserves #UkraineConflict


## Transcript
Well, howdy there, Internet people. It's Beau again. So after that last video, almost immediately,
people were like, hey, well, what if they just send in reinforcements? So we're going
to talk a little bit about the Russian military, how it's structured, and we're going to talk
about conscripts and contract, and we're going to talk about the reserves, which isn't like
our reserves, and we're going to go over the numbers and talk about what they might still
have back there. Okay, so we'll start with the contract troops. They're like our military.
Volunteers want to be there at least somewhat, and historically they have somewhere between
350,000 and 410,000. These are the people that make up the core of the Russian military.
They're the elite units. They're the paratroopers, the VDV, and then they make up the core of
the other units. The other units then get filled in with conscripts. The conscripted
troops, they draft them twice a year, once in spring, once in the fall, each time 130,000
troops normally, so 260,000 additional troops that can be used. These people are not well
trained. They're only in the military for a year and they don't want to be here, and
it shows. So that gives them a total on the absolute high end of 670,000 total forces.
Now keep in mind, not all of those are combat troops. They've already committed around 200,000,
tracking them down high end to 470,000. Even if all of those were combat troops, with the
number of troops they would need to keep in reserve to protect Russia itself, especially
given Putin's apparent paranoia and his likelihood of believing there be a counterattack from
NATO, they probably only have about 150,000 troops left to commit. That might break the
offensive free and get them moving again, but that's not enough to subdue Ukraine. It's
certainly not enough to hold it. So now let's go to their reserves. Russian reserves have
always been a paper tiger. The number is huge. They have like 2 million. The thing is, it's
not reserves like our reserves. Their reserves, it simply means you were in the military and
you're still young enough to be called back. Out of that 2 million, I want to say 1 in
10 at some point get some kind of refresher. As far as reserves like we think of, people
who actually go and continue to train, you know, one weekend a month, two weeks a year
type of thing, out of that 2 million, it's about 5,000. They don't have it. They don't
have the troops. They're going to have to call up a lot of troops to get anywhere in
Ukraine. They're going to have to call up a whole lot more troops. But the problem with
that is, as they move units forward with conscripts, they're going to have to have some of their
contract people in those units so they know what they're doing, at least have some idea.
As they burn through those, eventually they're going to run out of people that know what
they're doing. They don't have the numbers. It's math at this point. They do not have
the numbers. If they did a giant call up, they could get them trained up in four months
or so to the standard that they have, which hasn't been successful. At this point, as
far as taking it and behaving like a professional military, it seems pretty bleak, which means
the only choice Russia is going to have is to do what they've been doing in Mariupol.
The idea of actually running a professional military on what they have left is pretty
unlikely. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}