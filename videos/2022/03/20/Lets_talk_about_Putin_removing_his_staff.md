---
title: Let's talk about Putin removing his staff....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ITvTWN_dcmg) |
| Published | 2022/03/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Putin has been purging individuals in the upper echelons of Russian power, detaining generals and spy chiefs.
- Putin's actions suggest scapegoating rather than addressing major failures within his staff.
- When rulers like Putin conduct purges, there are typically three outcomes: promoting yes-men, appointing unqualified but trusted individuals, or micromanaging everything.
- Historically, such purges rarely solve underlying issues and can even make the ruler vulnerable to removal.
- The paranoia within Putin's inner circle may lead to drastic measures, potentially affecting Ukraine.
- Putin's recent actions indicate a focus on maintaining power rather than solving critical issues like the situation in Ukraine.

### Quotes

- "Putin is enacting a purge."
- "Historically speaking, these purges like this don't actually solve anything."
- "He's playing the normal authoritarian goon game."
- "All of this paranoia makes him a little bit more dangerous."
- "He might be willing to sign off on more brutal tactics in Ukraine."

### Oneliner

Putin's purge in Russia may backfire, as scapegoating and paranoia overshadow addressing critical failures, potentially leading to dangerous outcomes in Ukraine.

### Audience

Political analysts, international observers

### On-the-ground actions from transcript

- Monitor the situation in Russia and Ukraine closely to understand potential escalations (implied).
- Support diplomatic efforts to address the instability in the region (implied).

### Whats missing in summary

Insights into the potential impact of Putin's actions on regional stability and international relations.

### Tags

#Russia #Putin #PoliticalInstability #Authoritarianism #InternationalRelations


## Transcript
Well, howdy there internet people, it's Bo again
So today we're going to talk about a little bit of poetic justice that is going on in Russia
over the last week Putin has been
Getting rid of people in the upper echelons of
Russian power
The inner circle is crumbling a couple of generals some spy chiefs
there's a whole bunch of people that are being detained, I think is the word
they're using. Some are put on house arrest, some we don't know where they went.
It's a purge. Putin is enacting a purge. Now, it would be wise of Putin to
reshuffle his staff. There were major failures and that's a smart move, but
that's not what he's doing. He's not bringing people in to do the job.
He's in scapegoat mode. He's pulling people in and say, oh you're under arrest
because you wasted gas. Something like that. Looking for people to blame this on,
which indicates he isn't really ready to solve the problem. He understands that it
was a failure but he doesn't he's not at the point where he's going to bring in
professionals. That's the appearance at this time. When rulers go on little
purges like this there's normally only a few ways it works out. The first is they
They go in, they get rid of the people, and then the next row in the hierarchical structure
of the organization, they rise up and they take that spot.
The problem is that doesn't solve anything for the ruler.
That person will also be a yes man.
And on top of that, they'll have heard what their boss said, so they know what Putin wants
to hear, and they're terrified because they don't want to be detained down an
elevator shaft or whatever. So that route rarely works. It's uncommon for
that method of bringing people up to actually put somebody in the position
that knows what they're doing. It's typically another functionary who
is there through political connections, not there through skill. Another thing
that rulers do is they remove the people and then they handpick people that they
trust and put them in positions. But again, those people that they trust, they
don't necessarily know anything about the agency they're now running. They
lack that institutional memory. They're not familiar with, in some cases, what
it's even supposed to do. So that tends to also have bad results. And then the
third option is the one where the ruler himself, in this case Putin, decides to
micromanage everything. Putin's arrogant enough to possibly try that. That also
doesn't seem to go well. Historically speaking, these purges like this don't
actually solve anything. It just shuffles things around and at time actually opens
the ruler up to being removed, especially with the micromanaging thing.
when when that happens there's always a functionary for each agency you know
there's one for the army the Air Force whatever and that person doesn't have
any power they just do whatever the ruler says they enact those orders but
when something goes wrong those are the people who get blamed because of that
the feeling of being powerless and suffering the consequences for the
ruler's mistakes, that often leads to a palace coup. So Putin may be setting
himself up to be removed right now through his own paranoia, which is the
funny part about this because I think that a lot of this rests on him
being worried that his inner circle is going to betray him and remove him. So
he's gonna bring in new people who in some situations may be even more likely
to do that. There are rumors, I haven't actually been able to confirm this,
There's some reporting on it, but they're not great outlets at time of filming.
I haven't seen anybody confirm it, that he's removed some of his staff, as in like housekeepers,
maids, stuff like that.
If that's true, he's definitely hit that paranoia phase of this.
So it doesn't look like he's making the moves to actually solve the situation in Ukraine.
It looks as though he's still in create the image that everything is fine, scapegoat these
people, secure your position, and he's playing the normal authoritarian goon game.
The problem is he's doing this in a period where there's a lot of upheaval.
And as the economy worsens in Russia, he may be putting himself out of a job.
When all of that came up initially, it seemed a little far-fetched, but he's making a lot
of bad moves, which are pushing those options to be a little bit more likely.
I'm not gonna say that that's gonna happen anytime soon, but it's a whole lot more likely
post-purge than it was when all this started.
So that's where we're at.
Now at the same time, all of this paranoia makes him a little bit more dangerous.
to the level of the unthinkable, but he might be willing to sign off on more brutal tactics
in Ukraine.
So anyway, it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}