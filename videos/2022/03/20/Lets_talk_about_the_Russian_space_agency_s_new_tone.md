---
title: Let's talk about the Russian space agency's new tone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C5kzZzhfrJw) |
| Published | 2022/03/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russian Space Agency enacted sanctions against the West, particularly Americans, regarding rocket engines and maintenance.
- SpaceX offered rockets in response to Russian sanctions, diminishing the impact.
- Russian Space Agency softened its tone, sending written appeals to NASA and other space agencies to lift "illegal sanctions."
- Sanctions are affecting Russia's space sector, revealing its dependence on external components.
- Beau couldn't find evidence of illegal sanctions but identified a special economic operation.
- The sensitivity of the space industry to sanctions indicates potential impacts on other sectors.
- Russian government's stance may need to soften to prevent harm to the average Russian citizen.

### Quotes

- "We got rockets."
- "Couldn't find sanctions, but I did find a special economic operation."
- "This is actually an industry that's pretty sensitive to stuff like this."

### Oneliner

Russian Space Agency softens stance amidst impacts of sanctions, revealing industry vulnerabilities and potential wider consequences.

### Audience

Space enthusiasts, policymakers.

### On-the-ground actions from transcript

- Contact NASA, the Canadian Space Agency, and the European Space Agency to advocate for lifting sanctions (suggested).
- Monitor developments in the Russian space sector and related industries (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the evolving situation with the Russian Space Agency and the implications of sanctions on the space industry and broader sectors.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about some new developments
when it comes to the Russian Space Agency.
And we'll kind of catch everybody up
on what has happened since the last time we covered them.
If you missed it, it wasn't that long ago
that the Russian Space Agency took a pretty tough stance
when it comes to everything that's going on
and basically enacted their own sanctions on the West,
said they wouldn't provide rocket engines or maintenance,
and in general, said a whole bunch of really not nice things
about the West, and particularly Americans,
when it comes to their available technology.
Of course, predictably, SpaceX stepped up and was like,
yeah, don't worry about it.
We got rockets.
And then the Russian Space Agency was like,
yeah, well, we'll leave an American astronaut
on the International Space Station.
And SpaceX was like, I mean, I guess
we can give him a ride if we need to, but that seems silly.
And it kind of went back and forth like that.
But see, this weird thing has happened.
It appears that the Russian Space Agency has
softened its tone a little bit.
Roscosmos sends written appeals to NASA, the Canadian Space
Agency, and the European Space Agency
with a demand to lift illegal sanctions from our enterprises.
I mean, that seems weird.
That seems odd, because last I heard,
all we had were broomsticks to fly up there on.
What's happening is that the sanctions are taking hold.
They're starting to have impacts, particularly
in the space sector in Russia.
Russia is finding out that that particular industry is not
quite as independent as they once believed.
But see, the other part about it that seems odd to me
is I went and looked, and I couldn't
find any illegal sanctions.
I couldn't find any sanctions at all,
kind of like trying to look for that war in Ukraine.
You don't find a war.
You find a special military operation.
Couldn't find sanctions, but I did
find a special economic operation.
The funny thing is, I'm willing to bet
that when the not war ends in Ukraine,
the economic operation would probably end as well.
At least most of it.
I'm sure the West is going to keep some in place
for punitive measures.
So this is actually an industry that's
pretty sensitive to stuff like this.
There are a lot of components that have
come from a lot of places.
So the fact that this is kind of the first sign
we're seeing that these sanctions are having an impact
isn't really surprising.
Other industries, other sectors, they're
not going to be far behind.
So we can expect to see more of this.
And hopefully, the Russian government
will soften its stance overall before it really
starts to hurt the average Russian there.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}