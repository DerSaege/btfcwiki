---
title: Let's talk about Russia and Tucker....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JxGwyAJCmxI) |
| Published | 2022/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Information and Telecommunications Support in Russia issues memos to media, including talking points and propaganda guidance.
- Mention of Tucker Carlson in one of the memos as a popular Fox News host who criticizes the actions of the United States, NATO, and Western leadership.
- Russians believe Tucker Carlson uses fragments from his broadcasts to criticize Western actions and support Russian views.
- Beau speculates on Tucker Carlson's influence, pointing out that even though he may not be an agent of influence, Russians perceive him that way.
- Contrarians like Tucker Carlson, who go against the mainstream support for Ukraine, are seen as patriots by some Americans but potentially as allies by nations opposed to the United States.
- Clips of Tucker Carlson have started appearing on Chinese propaganda channels, despite his strong criticism of China.
- Beau suggests that Tucker Carlson's influence might challenge some people's beliefs and help break information silos that emerged around 2016.

### Quotes

- "This little bit of information is something you may want to file away."
- "Nations diametrically opposed to the United States view him as an ally."
- "Clips of him have started showing up on Chinese propaganda channels as well."
- "That thing that triggers cognitive dissonance, that thing that forces them to break out of the information silo."
- "It's just a thought."

### Oneliner

The Russian memo linking Tucker Carlson to propaganda challenges beliefs and could break information silos.

### Audience

Informative citizens.

### On-the-ground actions from transcript

- Research and critically analyze media sources for potential propaganda influences (implied).

### Whats missing in summary

The full transcript may provide more context on Tucker Carlson's broadcasts and the implications of media influence on public perception. 

### Tags

#TuckerCarlson #RussianMemo #Propaganda #MediaInfluence #InformationSilo


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
Tucker Carlson and a Russian memo. A memo that came out about Tucker Carlson. The memo
was issued by the Department of Information and Telecommunications Support. This is part
of the security apparatus there. It basically hands the media talking points, tells them
what to cover, helps them produce propaganda. Tucker Carlson was mentioned in one of these
memos. It is essential to use as much as possible fragments of broadcasts of the popular Fox
News host Tucker Carlson, who sharply criticizes the actions of the United States and NATO,
their negative role in unleashing the conflict in Ukraine, the defiantly provocative behavior
from the leadership of the Western countries and NATO towards the Russian Federation and
towards President Putin personally. Oh, tuck-yo-rose. Look, I'm not saying that Tucker Carlson is
an agent of influence. I'm really not. I can't prove that. I don't know that. I'm not even
saying that he parrots pro-Kremlin, anti-American, pro-Russian imperialism talking points. I'm
not saying that either. I'm just saying that the Russians think that he does. This little
bit of information is something you may want to file away because most Americans at this
point are firmly in support of Ukraine. There are a lot of people in this country who look
to contrarians like Tucker Carlson and view them as patriots. It might be worth pointing
out that nations diametrically opposed to the United States, nations who have threatened
unthinkable consequences, view him as an ally. View him as somebody that they can use to
put on their propaganda to show that while Americans are really behind them, it might
help unravel some of the polarization that has occurred in this country. So while I personally
find this pretty entertaining, it's also worth noting that clips of him have started showing
up on Chinese propaganda channels as well, which is funny given how much he acts like
China is the biggest enemy. While I find this funny, there are some people who this may
be the conflicting thing. That thing that triggers cognitive dissonance, that thing
that forces them to examine their closely held beliefs and maybe break out of the information
silo that they fell into sometime around 2016. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}