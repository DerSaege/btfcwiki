---
title: Let's talk about China, supply chains, and not being done....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tIzN2LqjRS4) |
| Published | 2022/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- China's megacity Shenzhen, with a population of 17-18 million, is going back on lockdown due to community spread of a "stealth variant."
- Despite reporting 66 new cases on Sunday, totaling over 400 cases in the last couple of weeks, they believe they caught it relatively early.
- Shenzhen is a vital tech manufacturing hub, and its lockdown could lead to disruptions in the global supply chain.
- The impact could affect tech companies like Apple initially but may escalate to other sectors as well.
- This disruption adds to the existing inflation concerns globally.
- The geopolitical situation with Russia and Ukraine reaching out to China for help adds another layer of uncertainty to China's decisions.
- The revenue loss from the lockdown may influence China's future choices.
- The supply chain interruption could lead to higher prices as supply diminishes.
- Beau suggests everyone should prepare for interruptions in products that rely on electricity.
- He plans to monitor the situation and provide updates as more information becomes available.

### Quotes

- "Shenzhen is pretty much the factory for every tech company."
- "Everybody around the world should just go ahead and get prepared for another interruption."
- "The revenue loss from the lockdown may influence China's decisions."
- "Prices will go up, exacerbating the inflation issue."
- "We will keep an eye on this, I'll keep an eye on this."

### Oneliner

China's megacity Shenzhen goes back on lockdown due to a "stealth variant," impacting global tech supply chains and potentially worsening inflation, with broader implications on revenue and geopolitics.

### Audience

Global citizens, tech industry players

### On-the-ground actions from transcript

- Monitor news updates on the situation in Shenzhen and its impact globally (suggested)
- Prepare for potential disruptions in tech products and anticipate price increases (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of how Shenzhen's lockdown can affect global tech supply chains, inflation, revenue, and geopolitical decisions. Watching the full video can offer a deeper understanding of these interconnected issues.

### Tags

#China #Shenzhen #Lockdown #TechIndustry #SupplyChain #Inflation #GlobalImpact


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about some news coming out of China and how it is going to impact the rest of
the world. There have been a lot of people who have decided they are just
done with our current public health issue. Well the public health issue
decided it may not be done with us. The megacity of Shenzhen, and by megacity I
want to say it's 17, 18 million people, it's huge, is shutting down. It is shutting
down. It's going back on lockdown. Now they're doing this because of community
spread of what they're calling the stealth variant. Now the good news is it
appears, at least by what they're reporting, that they've caught it
relatively early. On Sunday there were 66 new cases, which brings their total up to
a little more than 400 in the last couple of weeks. If those are accurate
numbers, it may not be a huge deal, but they have already determined to shut the
city down for a week. Why does this matter?
Shenzhen is pretty much the factory for every tech company. So a week disruption
there leads to disruptions in other places. We're looking at another supply
chain interruption. This one seems like it might be more or less limited to
tech companies. Apple in particular will end up getting hit, but as the dominoes
fall it may start impacting other areas as well. This is also, on top of it being
a supply chain issue, this is not good for inflation. And then on top of that,
with the geopolitical scene going on in Ukraine and Russia, and Russia reaching
out to China for help, this may influence China's decision there.
This is going to be a loss of revenue. How that plays into China making its decisions
is yet to be seen. But everybody around the world should just go ahead and get
prepared for another interruption when it comes to basically anything that
plugs into a wall. And from that we can also assume that because supply will be
low, prices will go up. So it's probably going to exacerbate the inflation issue
that's already present. We will keep an eye on this, I'll keep an eye on this, and
provide an update as soon as we know something. They've said they're going to do it for a week,
and then they'll test everybody, I guess three times, and see where it goes from there.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}