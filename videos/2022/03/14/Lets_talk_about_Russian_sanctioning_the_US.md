---
title: Let's talk about Russian sanctioning the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=u8Nz_HL2E_c) |
| Published | 2022/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia has become the most sanctioned country globally, surpassing Iran, with 5,500 different sanctions.
- The chief of the Russian space agency announced that they will stop delivering rocket engines to US customers for space.
- NASA's program called Artemis is set to return humans to the moon.
- Artemis allows people to have their names flown around the moon by signing up on their website.
- This initiative aims to involve young people and kids in space exploration.
- It serves as a unique diversion from the constant issues on Earth.
- People overwhelmed by world events can find hope and distraction in projects like Artemis.
- Beau suggests signing up for Artemis' boarding pass if you have kids or are interested.
- He believes the US will manage fine despite Russia's sanctions and it won't impact NASA significantly.
- Beau encourages viewers to find moments of positivity and cheer in the midst of the relentless news cycle.

### Quotes

- "There are slivers of hope. There are things that can cheer you up and can divert your attention from the constant news cycle."
- "So if you have kids, it might be something worth doing with them, getting them a boarding pass for it."
  
### Oneliner

Russia, the most sanctioned country, retaliates against the US while NASA's Artemis program offers hope and diversion through lunar involvement for all, especially the youth.

### Audience

Space enthusiasts, parents, curious minds.

### On-the-ground actions from transcript

- Sign up for NASA's Artemis program to have your name flown around the moon (suggested).
- Involve kids in space exploration by getting them a boarding pass for Artemis (suggested).

### Whats missing in summary

Details on how individuals can sign up for NASA's Artemis program and get involved in space exploration firsthand.

### Tags

#Russia #Sanctions #NASA #Artemis #SpaceExploration #Hope


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about Russia
sanctioning the United States in a way of sorts.
There aren't a lot of ways for Russia
to respond as far as sanctions go.
They have quickly become the most sanctioned country
on the planet, surpassing even Iran.
Last count, I want to say it was 5,500 different sanctions
on Russia.
The chief of the Russian space agency
has announced that they will no longer be delivering rocket
engines to US customers for space
and said that Americans could fly up there on their brooms
if need be.
I am sure that SpaceX is heartbroken by this news.
In related news, when it comes to space exploration,
NASA has a program called Artemis.
And it is the program that is slated
to return us to the moon.
Right?
With everything going on, there's
probably room for some hope, some diversion
from all of the constant issues here on Earth.
And Artemis has a unique little thing.
You can have your name flown around the moon.
You can sign up on the website.
I'll try to remember to put the link down below,
but it's easy to find.
Basically, they put your name on a flash drive.
They're calling it a boarding pass.
It seems like a really interesting way
to get young people, get kids involved in the curiosity
aspects of it, kind of pique that interest
in reaching for the stars.
So if you are currently just overwhelmed with everything
that's going on in the world, there are slivers of hope.
There are things that can cheer you up
and can divert your attention from the constant news cycle.
So if you have kids, it might be something
worth doing with them, getting them a boarding pass for it.
And if you're just interested in it,
it might be something worth looking into.
But as far as the sanctions go from Russia,
I'm fairly certain that the United States will be just fine.
And this won't present any major issues for NASA's endeavors.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}