---
title: Let's talk about foreign policy's new normal and anti-Russian sentiment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FnbOJKriTi8) |
| Published | 2022/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes the new normal in American foreign policy involving a three-way power struggle between China, the United States, and Russia.
- Mentions how the presence of a near-peer like Russia influences American foreign policy decisions, shifting focus away from other opposition nations.
- Talks about the historical trend of American politicians using external threats to rally votes and consolidate voter support by othering certain groups.
- Points out the negative impact of anti-Russian sentiment on American society and foreign policy, urging against playing into it.
- Suggests reinstating programs like the Sister City program to foster solidarity between people caught in the crossfire of international tensions.
- Warns against politicians exploiting anti-Russian sentiment to prolong conflicts and stay in power, potentially leading to a prolonged Cold War scenario.

### Quotes

- "Just once, it would be great to get through a major foreign policy shift without having to apologize to a demographic of Americans."
- "Don't play into this. It's not the Russian people, it's certainly not Russian Americans, it's the Russian leadership."

### Oneliner

Be cautious of playing into anti-Russian sentiment in American foreign policy to avoid repeating historical mistakes and perpetuating harmful othering dynamics.

### Audience

Foreign policy observers

### On-the-ground actions from transcript

- Reinstate programs like the Sister City program to show solidarity with people caught in the crossfire (suggested).
- Avoid perpetuating anti-Russian sentiment in American society and politics to prevent further harm (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how anti-Russian sentiment can impact American foreign policy decisions and societal dynamics, urging caution and advocating for a more nuanced approach.

### Tags

#AmericanForeignPolicy #AntiRussianSentiment #SisterCityProgram #Solidarity #PoliticalCaution


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the new normal
in American foreign policy,
and how that new normal is going to lead
to the United States engaging in a habit
that it has pretty much always engaged in,
that has always turned out to be a mistake.
So maybe we can stop it this time.
What's going on right now? This is the new normal. Three-way power struggle
between China, the United States, and Russia. Tensions flaring and then
subsiding and then flaring again. This is the new normal. This is how it is going
to be for the foreseeable future, absent something very dramatic happening. This
This is going to do a lot of things.
First, because there's now a near peer to serve as the boogeyman on the American foreign
policy scene, a lot of countries that have been seen as opposition nations over the last
few years, well, they're going to kind of fall out of those slots.
You're already starting to see this with Venezuela as an example, a country that has been painted
as like some mortal enemy of the United States, which is funny in its own way.
They're no longer going to be seen that way because, well, the politicians don't need
it because now they have a nuclear-capable Russia to serve as the external threat.
And that's what happens.
The foreign policy decisions, they're generally made by State Department and the Executive
Branch.
Yeah, Congress gets involved every once in a while, but not really.
They tend to go with the advice of experts normally.
But what also happens is they use the American foreign policy stage and what's going on as
a method to get votes.
They play into certain sentiments that develop.
And they use that to take a group of people and other them so they can kind of consolidate
the rest of the voters around the idea that this group is bad.
This leads to a othering of Americans who happen to come from whatever the country is
that's the competitor at that point in time.
You look at this historically and you'll see it.
Ask an Arab-American how they feel about the last 20 years.
If they feel that they were slighted during this period.
Or you could go back to Vietnam.
You could go back to the first Cold War with Russia, the Red Scare.
Go back to World War II.
We put Japanese-Americans in camps.
Just once, it would be great to get through a major foreign policy shift without having
to apologize to a demographic of Americans.
We have to be on guard against anti-Russian sentiment, and I know that's hard right now
because the footage is coming in and it's being played up in a lot of ways and there's
There's that desire, red team, blue team, go type of thing, and you want to be a part
of the American position or the British position or whatever your home country is.
The reality is that hurts.
It hurts.
Ideally foreign policy should be about making the world a better place.
It's not.
But in most cases, when you're talking about near peers, the power requires a little bit of stability.
And what happens when politicians realize that anti-Russian sentiment is on the rise?
Oh, believe me, they're going to lean into that.
And they're going to talk about how, while they may be infiltrators, they may not really be loyal Americans.
And you'll see it happen.
It's always happened.
This country has a really long history of this.
They use it to get votes.
They other a group and consolidate everybody else around the idea that that group is now
the bad group.
That's the group that we're going to mock.
That's the group that it's okay to make fun of.
the group that it's okay to keep away, to be suspicious of, to vandalize their shops.
That has already happened in the United States.
We don't need that.
It doesn't help.
It only hurts.
If you want the American position to be strong, don't do that.
Because then State Department has to work overtime cleaning it up.
Because when politicians try to catch votes by playing into this anti-Russian sentiment,
how are they going to do it?
They're going to introduce legislation that targets something that's Russian, that has
nothing to do with any foreign policy goal that actually exists.
It's just them trying to rally Americans behind them using Russia as a cover.
The sanctions that have been put in place by the US government, by Western nations,
that's enough.
Department doesn't need your help in punishing Russians, and the average
Russian over there doesn't have anything to do with it. The Russian American here
certainly doesn't. I found out recently that a lot of towns and cities are
suspending their Sister City program. That was actually put in place during
the Cold War to show that the people were the same, that no matter what the leaders
did, no matter how big the ideological gap, that the people were the same, that there
could be some kind of solidarity among people who were caught in the crossfire.
I think that might be something that we should bring back.
That idea that they have as little control, even less control, over their government than
we do.
They are out there protesting.
It's not the Russian people that are the problem, it's the Russian leadership.
Don't get the two mixed up.
play into the emerging anti-Russian sentiment. It doesn't help. It doesn't
help the United States. It doesn't help America at home. It doesn't help on the
foreign policy scene. And again, I would just love once for us to get through
something like this without there being specials on the news about how horrible
a group of Americans was traded, simply because of where they showed up from.
This is going to last.
This is the new normal.
It's going to be at least 10 years, will be at least 10 years like this, unless that
anti-Russian sentiment takes hold.
then politicians will use it, and they will keep it going because it's what will keep
them in power.
And then, rather than a 10-year contest, we'll have a Cold War that lasts half a century.
Don't play into this.
It's not the Russian people, it's certainly not Russian Americans, it's the Russian leadership.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}