---
title: Let's talk about new subscribers and my philosophy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eDuo0S1Dl_M) |
| Published | 2022/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Welcoming new subscribers interested in foreign policy coverage, history, philosophy, politics, and social issues.
- Shares a disturbing message received regarding their stance on BLM with racial slurs.
- Clarifies his social and political stance, challenging stereotypes associated with his Southern background.
- Expresses disgust towards memes mocking progressive ideas, wishing Biden was as cool as portrayed.
- Advocates for social progressiveness, stating the importance of using means to help and not turning against those with less institutional power.
- Emphasizes the changing world and the need to adapt, offering advice to viewers to challenge themselves in understanding social issues.
- Encourages viewers to watch videos on social issues, suggesting it may shift perspectives and prevent bigotry.

### Quotes

- "Do not let my accent fool you. We are not on the same team."
- "If you have the means at hand to help, you have the responsibility to do so."
- "Maybe it stops you from being a bigot and that would be a good start."

### Oneliner

Beau challenges stereotypes, advocates for social progressiveness, and encourages viewers to broaden perspectives on social issues to combat bigotry.

### Audience

Viewers

### On-the-ground actions from transcript

- Watch one video a week on social issues to challenge yourself (suggested)
- Research and look into topics discussed to broaden understanding (suggested)

### Whats missing in summary

The emotional impact of challenging stereotypes and advocating for social progressiveness.

### Tags

#SocialIssues #Progressiveness #ChallengeStereotypes #Education #Community


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk a little bit about the channel and the new subscribers.
We're going to welcome you all because a whole lot of you all have signed up over the last
couple of weeks.
Most of you all have signed up for foreign policy coverage, coverage of Ukraine, and
we talk about foreign policy on the channel a lot.
We also talk about history and philosophy and politics and social issues.
It's that last one that has given some people some pause and it has led to some very colorful
and interesting messages.
My favorite thus far is one that said, I thought I had found my new favorite YouTube channel
but after watching a video you did on BLM I realized you're just another n-word lover.
I live in the South, like the Deep South, the rural South, the part of the South they
go to to film horror movies.
I haven't heard that phrase spoken in like 20 years.
So let me clear the air on this.
Do not let my accent fool you.
We are not on the same team.
I drive a truck, it does not have a Trump bumper sticker, I do not own a hood, and I
do not like Fox News.
Those memes that you share with your conservative buddies from high school on Facebook, you
know the ones I'm talking about, the ones that are like, I don't know, Biden is going
to give free gas and reparations to newly married interracial gay couples who are holding
BLM signs while they're trying to adopt a trans refugee child who's then going to go
to school with our kids and read the books we want to ban.
Those memes, you see them and you're disgusted.
I'm disgusted too, but you're disgusted because you don't want that to happen and I'm disgusted
because Biden's not actually that cool.
I want the Biden from your memes.
I am very socially progressive.
That's not a secret.
There's like 2,000 videos detailing that.
My philosophy is pretty simple.
If you have the means at hand to help, you have the responsibility to do so.
You should always punch up and never kick down.
Those people who have less institutional power than you do are never the source of your problem
and those people who are trying to convince you that they are generally have their hands
in your pockets while you're looking down at whoever they have decided to other this
week.
The reality is the world is changing and it's changing quickly.
You can keep up or you can get left behind.
It doesn't really matter to us.
But if you liked the foreign policy stuff, if you liked those videos, understand I bring
the same level of research to the videos on social issues as well.
My advice would be to maybe watch one a week.
Challenge yourself.
I know it's going to be really hard, but challenge yourself.
Maybe it gives you something to think about.
Maybe it helps shift your world view a little bit.
Maybe it gives you something to look into and research and you can prove me wrong on
something.
At the very least, maybe it stops you from being a bigot and that would be a good start.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}