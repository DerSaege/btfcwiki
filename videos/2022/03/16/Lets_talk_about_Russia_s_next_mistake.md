---
title: Let's talk about Russia's next mistake....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yfNgZCt9vYo) |
| Published | 2022/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Russia is making a mistake by starting a conflict with unforeseen impacts.
- Russian troops are not performing well and are now in the conventional phase of the conflict.
- They lack the skill set for the heavy unconventional phase.
- Ukrainian strategy is to continue fighting to break Russian resolve.
- Russia seems to be considering a scorched earth strategy, risking unintended consequences.
- Shift in strategy may lead to incidents within Russia itself.
- World powers are supporting Ukraine, which may change if extreme actions are taken.
- Individuals seeking vengeance may carry out extreme measures, not the Ukrainian government.
- Russian command's failure in this conflict may have long-lasting consequences.
- Devastation and cyclical consequences could result from Russia's new strategy.

### Quotes

- "Russia is making a mistake by starting a conflict with unforeseen impacts."
- "Ukrainian strategy is to continue fighting to break Russian resolve."
- "Individuals seeking vengeance may carry out extreme measures, not the Ukrainian government."

### Oneliner

Russia's mistake in starting a conflict with unforeseen impacts leads to a risky scorched earth strategy, jeopardizing Russian resolve and potentially causing long-lasting devastation.

### Audience

World Powers

### On-the-ground actions from transcript

- Stop the chain of events by preventing extreme measures from being taken (implied)
- Russia needs to change its strategy to avoid devastating consequences (implied)

### Whats missing in summary

The emotional impact and detailed analysis of potential consequences are best understood by watching the full transcript.

### Tags

#Russia #Ukraine #Conflict #Military #Strategy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about Russia's next mistake.
And to be honest, it looks like they've already
decided to make this mistake.
And they are starting to enact it as you're watching this.
And I believe that this is something
that is likely to have some unforeseen impacts down
the road.
At this point, this has been going on a while, the Russian command is aware of the fact that
their troops, they're not doing well.
This isn't going the way they planned.
That's why they're trying to bring in people from outside, and they're still in the first
phase of this.
They're in the conventional phase of this right now.
They haven't even started dealing with the heavy unconventional stuff, the hit and run
stuff, the stuff the US was dealing with in Afghanistan and Iraq for 20 years.
They're not to that point yet.
What they've been dealing with thus far has been pretty conventional, and they're not
doing well, which means when it gets to that second phase, to that unconventional phase,
these troops, they're not up to it.
They do not have the skill set for it.
So because of that, Russian command understands that all Ukraine has to do to win is fight.
They don't even have to fight well.
They don't have to win any battles.
They just have to continue to fight and eventually it will break Russian resolve.
The Ukrainians find themselves in the situation that the Afghans or the Vietnamese were in.
Sure, Russia has the watches.
They've got the clocks.
Ukraine has the time.
All they have to do is fight.
So the only way to change this would be to shift things dramatically and break Ukrainian
resolve.
So it appears that Russian command has kind of decided to go scorched earth, care a little
bit less about where things fall, level cities.
That is something that is likely to have unintended consequences.
This isn't a country on the other side of the world.
This is a country that shares a border with them.
This is a country that in an attempt to manufacture a pretext, Russia distributed passports in,
passports, those people who are caught up in Russia's strategy shift here, or more
importantly, those people whose families get caught up in this strategy shift.
They are not far from Russia's home front.
It would not be surprising to me if because of this Russia had incidents at home.
I hope it doesn't happen because like all of this, it will be the civilians who get
caught up in it.
I also don't think it's a good idea because on the world stage, the world powers are lining
up behind Ukraine.
But if something like that happens, if that T word gets thrown out there, well, that may
change things.
But the thing is, the people who would do this, it wouldn't be the Ukrainian government.
They wouldn't order that.
This is going to be an individual or group of individuals.
People who, well, they don't have the ability to go home to their family once it's over,
so they have nothing to lose.
They're not going to be people looking for strategy.
going to be people looking for vengeance.
This move stands a really strong chance of going down as the worst failure of Russian
command in a long string of failures in this conflict.
The unintended and unforeseen that can come from this can last a really, really long time.
It can become cyclical, it can cause a lot of devastation.
Set aside the moral aspects of Russia's new strategy.
Set all of that aside for a second.
Set aside the geopolitical stuff where they're going to lose standing on the international
stage for years and years to come.
Set all of that aside.
Set aside the lives.
From the Russian military standpoint, this is something that could bring the war home.
Could bring it to their towns, to their cities.
And if that happens, it will be nobody's fault.
But Putin's?
It will be nobody's fault, but Russia's high command.
This is something they should see coming.
likelihood of people wanting to take things into their own hands after their
hometown is destroyed is pretty high. Russia needs to stop this. The chain of
events that follows. Once those dominoes start falling, it's really hard to stop
them from from continuing. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}