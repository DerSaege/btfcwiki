---
title: Let's talk about Ukraine's personnel problem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aoGFfr1ahGQ) |
| Published | 2022/03/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a specific group in Ukraine, the Ukrainian resistance, labeled as fascists due to their ideology.
- Makes a comparison to the United States, hypothetical scenario of civilians organizing due to the military being incapacitated.
- Points out that the groups leaning into organizing in the scenario are mostly far-right-wing with aesthetics similar to those at the US Capitol on January 6th.
- Raises the possibility of a similar scenario occurring in the US if faced with a conflict like Ukraine's.
- Explores the challenges of cleaning up such groups historically and proposes potential solutions for Ukraine.
- Expresses concern over the attention and coverage given to the group, potentially turning them into heroes through media portrayal.
- Provides statistics on the group's size, political presence, and electoral success to contextualize their impact in Ukraine.
- Identifies two main reasons for the heightened attention on this group: Russian propaganda and the inherent danger of fascism.
- Advises focusing on uplifting left-wing, progressive groups to counterbalance the narrative surrounding the fascist group.
- Emphasizes the importance of not ignoring the fascist group despite their relatively small numbers due to the inherent risks associated with fascism.

### Quotes

- "Fascists are a lot like lead in your drinking water. There is no acceptable safe level."
- "There's no acceptable safe level."
- "This is something that actually occurs way more than, I guess, people realize."
- "No acceptable safe level."
- "Rather than looking at them and sharing articles about them, there are left-wing, progressive groups there as well. Get the press talking about those."

### Oneliner

Beau sheds light on the dangers of fascist groups, the intersection of ideology and reality, and the importance of countering their narrative with progressive voices in Ukraine.

### Audience

Activists, Progressives, Community Leaders

### On-the-ground actions from transcript

- Elevate left-wing, progressive groups in Ukraine through sharing and discussing their work (suggested)
- Counterbalance media coverage by promoting progressive voices and initiatives in Ukraine (suggested)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the risks associated with fascist groups, the intersection of ideology and reality, and the potential impacts on post-war scenarios. Viewing the complete transcript offers a deeper understanding of the nuances surrounding these issues.

### Tags

#Fascism #Ukraine #ProgressiveGroups #RussianPropaganda #MediaCoverage


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about something
I didn't want to talk about, to be honest, something
I was hoping I wasn't going to have to talk about.
But here we are.
We're going to talk about a specific group, a battalion,
a regiment.
And we're going to talk about how something like that
even happens to begin with.
We're going to talk about how it gets cleaned up.
We're going to talk about some numbers and some context
and put it into focus.
We're going to talk about one of the real dangers
of this situation.
And we're also going to talk about why so many people are
leaning into this storyline.
If you have no idea what I'm talking about,
in Ukraine, fighting with the Ukrainian resistance,
there is a group.
And they are fascists.
And I don't mean that as an insult.
I mean they're literally fascists.
That's their ideology.
Not good, right?
OK.
People in the US, a lot of them have just kind of leaned
into this idea of, wow, that's an anomaly.
How did this happen?
And they're making a big deal out of it.
And that's rooted in American exceptionalism,
under the idea that that wouldn't happen here.
It absolutely would.
So to kind of show how this occurs,
we're going to use the US as an example.
We're going to say, country X, it doesn't matter,
the Dominican Republic.
The Dominican Republic invades the United States.
They catch us.
And the military is just sleeping on the job.
And the military gets hit so bad that they call up and say,
hey, civilians need to organize.
Now the good news is it's the US.
It wouldn't be hard to get a whole bunch of people who
would be willing to do that.
I mean, how many photos have you seen
on social media of somebody wearing a plate carrier
with all their Mali gear, carrying more equipment
to the range than a lot of Marines
had in Afghanistan while holding a rifle like this?
A lot.
So it wouldn't be hard.
Quick question, what's the ideology of those people,
most of them, with those photos?
Far right wing, right?
Yeah.
The groups that would be able to organize the fastest
would be the same groups that were at the US
Capitol on the 6th, because they already have the structure.
They already have that hierarchical thing going on.
And they already have the aesthetics
of wanting to do this.
They have that image.
So they're fashion.
A lot of them are that.
So when they organize, and they would be the first ones,
and they show up, and they talk to DOD,
what does the Department of Defense say?
Do they look at them and say, oh, y'all
are a little too fashy for us.
Go over there to the sidelines and wait?
Or do they say, this is the geographic area
you're supposed to defend.
Here's some ammo.
Take some of those rockets with you when you go.
It absolutely would happen here.
No doubts.
Absolutely.
In the situation, if the United States was ever
in the situation that Ukraine is,
this exact scenario would play out.
What makes it more strange is that, let's say,
the geographic area they were assigned to defend
was Apalachicola, Florida to Mobile Bay.
When the call for civilians goes out, those who answer,
if they're from that geographic area,
what unit might they end up in, even if they do not
share that ideology?
OK.
So now we understand how it happens.
The idea that this is an anomaly, something
that would only happen in Ukraine is just silly.
It absolutely would happen here.
So one thing that I've got to point out
is that it boggles my mind that this is the thing that people
think is weird.
Understand, right now, you have people
who served in the US Marines fighting in units in Ukraine
who are getting advice from people from Afghanistan
who the Marines were fighting against on how to build stuff
and set them by the roads.
That's occurring as we speak.
To me, that is far more of a stranger,
politics make strange bedfellows type of thing than this.
But here we are.
So how do you clean this mess up?
Because the reality is this is bad.
These are, in fact, fascists.
And they're now armed, well-armed.
Historically, this gets cleaned up
in a couple of different ways.
One is that the group just dissipates.
Just after the conflict, they're just like, yeah,
we're done with this.
I don't think that's incredibly likely in this case in Ukraine.
Another is that right after the conflict,
during all the celebrations, a government
engages in mass arrests.
Or some governments, historically,
have sought more permanent solutions en masse
to groups like this.
I don't think that would happen in Ukraine.
But it is something that has happened in the past.
Now, if you're looking at Ukraine's situation
in particular, what would be the smart move?
Because Ukraine is a country that could be a powerhouse
in Europe.
But it has to show that it is a European country that
respects the rule of law and all of that stuff.
What would be a good way to show that?
Prosecuting some of your own fighters
for violation of the laws of armed conflict after the war.
Perhaps the leadership of a certain group.
That might be a good way to clean this up.
What are they going to do?
No clue.
No clue.
What's the danger?
The real danger is all of the articles coming out
about this group.
All of the attention that they're getting.
Because these articles are being read,
more journalists show up.
And now they're basically writing human interest stories.
Hey, check out this completely relatable young fascist here.
That's not good.
But the more specific danger, the thing
that I would be worried about is one of those journalists
being there when a member of this group
engages in some feat of battlefield heroics.
They climb up on top of a burning T-72
and pull an Audie Murphy.
And the journalist is there to get it on film.
They get it on video.
They get photos of it.
And that gets published.
And it turns that person into a war hero
who can then achieve electoral success after the war.
That, to me, is the biggest danger.
Because that spreads that message.
Now, let's talk about numbers.
Because with as much coverage as this is getting,
it seems like it's a big issue.
All right, in 2017, this group reached its peak
with 2,500 members.
Today, in 2022, it's about 900 and falling
because there's a war.
So that's a lot of people.
What percentage of the military is this?
Ukraine, prior to this, they had about 200,000 troops.
So you're looking at their peak, 1 and 1
1.5%, 2% to now about half a percent of their combatants.
Not a huge number, right?
What about their political party?
Because they have a political wing that goes along with it.
How well did they do?
2.15% of the vote.
They didn't get a single seat in the unicameral house
that Ukraine has.
Not one, 2.15%.
The presence of this does not actually demonstrate
that Ukraine is a fashion country.
So that's kind of a view of how it can happen,
how you clean it up, dangers, context
to show how widespread it is.
With all of this information, why
are people leaning into this storyline?
There's two reasons.
One is that it is part of the Russian propaganda effort.
They set out to paint Ukraine in this light.
The other is that this group is fascists.
So people are concerned about it because fascists
are a lot like lead in your drinking water.
There is no acceptable safe level.
So that's why it has drawn a lot of attention.
The problem is the more attention it gets,
it increases the likelihood that a bad outcome occurs
at this moment during the war.
It can set the stage for them to gain a lot of popularity
after the war.
This is one of those things where it's
the intersection of ideology and reality.
This is something that actually occurs way more than, I guess,
people realize.
Groups that are not really on the same team
being on the same team temporarily.
That intersection, ideology and reality,
it is a bumpy intersection, and there's a lot of car accidents
there.
This is one of them.
So what can you do?
Because obviously, you don't want to help them,
but you also don't want to not pay attention, right?
Because that second reason, no acceptable safe level.
My advice would be rather than looking at them
and sharing articles about them,
there are left wing, progressive groups there as well.
Highlight those instead.
Get the press talking about those.
That way, if there is a war hero that comes out of it,
it's one of them.
Somebody who might be able to ride that and take Ukraine
to a more progressive place.
But at the same time, while looking at the numbers,
looking at how successful they've
been in elections, looking at the small percentages involved,
it's not really the big threat it's being made out to be.
There's no acceptable safe level.
So don't stop paying attention to it.
Just maybe provide some information
that might be more beneficial when you're out sharing stuff.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}